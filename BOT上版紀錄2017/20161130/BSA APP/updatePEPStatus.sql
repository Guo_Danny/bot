USE [PBSA]
GO

/*select distinct cus.* from Customer cus 
left join [OFAC].[dbo].[SDNTable] sdn on cus.name = sdn.name
where sdn.listtype = 'PEP'
*/

update Customer set PEP = 1,lastOper = 'PrimeAdmin' where id in 
(
select distinct cus.id from Customer cus 
join [OFAC].[dbo].[SDNTable] sdn on cus.name = sdn.name
where sdn.listtype = 'PEP'
)
