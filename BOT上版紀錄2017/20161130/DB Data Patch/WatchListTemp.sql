USE [PBSA]
GO
ALTER TABLE dbo.Watchlist NOCHECK CONSTRAINT ALL

--1. AcctXOvrY
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'AcctXOvrY') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'AcctXOvrY', --[WLCode]
	N'Detects accounts that had transactions using a specified activity type that exceeded a designated dollar amount over a period of specified days', --[Title]
	N'Detects accounts that had transactions using a specified activity type that exceeded a designated dollar amount over a period of specified days.  Designed to be run Post-EOD.', --[Desc]
	1, N'USR_AcctXOvrY', --[SPName]
	N'Rule', 
	4, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@activityTypeList" Alias="List of Activity Types. -ALL- for all" Value="2334,2344,2354,2364,2374,2384,2414" /><Param Name="@day" Alias="Number of Days" Value="31" /><Param Name="@minimumAmount" Alias="Minimum Amount" Value="250000" /><Param Name="@AccountList" Alias="Accounts (Comma Separated or ''-ALL-'')" Value="-ALL-" /><Param Name="@BranchList" Alias="Branches (Comma Separated or ''-ALL-'')" Value="-ALL-" /><Param Name="@DeptList" Alias="Departments (Comma Separated or ''-ALL-'')" Value="-ALL-" /><Param Name="@CorrespondentBankOnly" Alias="Correspondent Banks Only (Yes/No)" Value="No" /></Params>', --[Params]
	0, 1, N'User Defined', 
	2, --[UseSysDate]
	0, 
	N'AcctXOverY' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects accounts that had transactions using a specified activity type that exceeded a designated dollar amount over a period of specified days', --Title
	[Desc] = N'Detects accounts that had transactions using a specified activity type that exceeded a designated dollar amount over a period of specified days.  Designed to be run Post-EOD.', --[Desc]
	WLType = 1, 
	SPName = N'USR_AcctXOvrY', --SPName
	SuspType = 'Rule', 
	Schedule = 4, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@activityTypeList" Alias="List of Activity Types. -ALL- for all" Value="2334,2344,2354,2364,2374,2384,2414" /><Param Name="@day" Alias="Number of Days" Value="31" /><Param Name="@minimumAmount" Alias="Minimum Amount" Value="250000" /><Param Name="@AccountList" Alias="Accounts (Comma Separated or ''-ALL-'')" Value="-ALL-" /><Param Name="@BranchList" Alias="Branches (Comma Separated or ''-ALL-'')" Value="-ALL-" /><Param Name="@DeptList" Alias="Departments (Comma Separated or ''-ALL-'')" Value="-ALL-" /><Param Name="@CorrespondentBankOnly" Alias="Correspondent Banks Only (Yes/No)" Value="No" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 2, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'AcctXOverY' --TemplateWLCode 
	WHERE WLCode = 'AcctXOvrY';  --WLCode

--2. AcctXCaY
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'AcctXCaY') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'AcctXCaY', --[WLCode]
	N'Detects accounts that have cash transactions that exceed amount within a specified number of days', --[Title]
	N'Detects accounts that have cash transactions that exceed a designated   dollar amount within a specified number of days. Designed to be run Post-EOD.', --[Desc]
	1, N'USR_AcctXCaY', --[SPName]
	N'Rule', 
	4, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@day" Alias="Number of Days" Value="31" /><Param Name="@CTRAmount" Alias="CTR Amount" Value="25000" /></Params>', --[Params]
	0, 1, N'User Defined', 
	0, --[UseSysDate]
	0, 
	N'AcctXCashY' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects accounts that have cash transactions that exceed amount within a specified number of days', --Title
	[Desc] = N'Detects accounts that have cash transactions that exceed a designated   dollar amount within a specified number of days. Designed to be run Post-EOD.', --[Desc]
	WLType = 1, 
	SPName = N'USR_AcctXCaY', --SPName
	SuspType = 'Rule', 
	Schedule = 4, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@day" Alias="Number of Days" Value="31" /><Param Name="@CTRAmount" Alias="CTR Amount" Value="25000" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 0, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'AcctXCashY' --TemplateWLCode 
	WHERE WLCode = 'AcctXCaY';  --WLCode

--3. DomToActive
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'DomToActive') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'DomToActive', --[WLCode]
	N'Detects accounts that have been dormant then become active', --[Title]
	N'Detects accounts that have been dormant for at least a specified number of days and then becomes active. Designed to be run Pre-EOD.', --[Desc]
	1, N'USR_DomToActive', --[SPName]
	N'Rule', 
	1, --Schedule 
	1, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@day" Alias="Day" Value="90" /><Param Name="@branchlist" Alias="Branches" Value="-ALL-" /><Param Name="@deptlist" Alias="Departments" Value="-ALL-" /><Param Name="@ThresholdAmt" Alias="ThresholdAmount" Value="50000" /><Param Name="@ActvTypeList" Alias="Comma seperated list of activity types to be included. -ALL- for all" Value="2334,2344,2354,2364,2374,2384,1034,1044,1014,1024,2414" /><Param Name="@ExcludeActvTypeList" Alias="Comma seperated list of activity types to be excluded. -NONE- for none." Value="-NONE-" /></Params>', --[Params]
	0, 1, N'User Defined', 
	0, --[UseSysDate]
	0, 
	N'DormToAct' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects accounts that have been dormant then become active', --Title
	[Desc] = N'Detects accounts that have been dormant for at least a specified number of days and then becomes active. Designed to be run Pre-EOD.', --[Desc]
	WLType = 1, 
	SPName = N'USR_DomToActive', --SPName
	SuspType = 'Rule', 
	Schedule = 1, --Schedule 
	IsPreEOD = 1, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@day" Alias="Day" Value="90" /><Param Name="@branchlist" Alias="Branches" Value="-ALL-" /><Param Name="@deptlist" Alias="Departments" Value="-ALL-" /><Param Name="@ThresholdAmt" Alias="ThresholdAmount" Value="50000" /><Param Name="@ActvTypeList" Alias="Comma seperated list of activity types to be included. -ALL- for all" Value="2334,2344,2354,2364,2374,2384,1034,1044,1014,1024,2414" /><Param Name="@ExcludeActvTypeList" Alias="Comma seperated list of activity types to be excluded. -NONE- for none." Value="-NONE-" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 0, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'DormToAct' --TemplateWLCode 
	WHERE WLCode = 'DomToActive';  --WLCode

--4. LoanRepayY
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'LoanRepayY') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'LoanRepayY', --[WLCode]
	N'Detects a loan taken from product (account type) X and Y% is repaid within Z months.', --[Title]
	N'Detects a loan taken from product (account type) X and Y% is repaid within Z months.  Designed to be run Pre-EOD.', --[Desc]
	1, N'USR_LoanRepayY', --[SPName]
	N'Rule', 
	4, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@activityTypeIn" Alias="List of Deposit Activity Types (Repayment). -ALL- for all" Value="-ALL-" /><Param Name="@activityTypeOut" Alias="List of Withdrawal Activity Types (Loan). -ALL- for all" Value="2014,2024,2034,2044,2094,2104,2114,2124" /><Param Name="@productType" Alias="List of Products (Account Types) X. -ALL- for all" Value="-ALL-" /><Param Name="@percent" Alias="Y Percent" Value="10" /><Param Name="@month" Alias="Z Months" Value="1" /><Param Name="@RiskClass" Alias="List of RiskClasses. -ALL- for all" Value="-ALL-" /></Params>', --[Params]
	0, 1, N'User Defined', 
	2, --[UseSysDate]
	0, 
	N'LoanRepayYP' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects a loan taken from product (account type) X and Y% is repaid within Z months.', --Title
	[Desc] = N'Detects a loan taken from product (account type) X and Y% is repaid within Z months.  Designed to be run Pre-EOD.', --[Desc]
	WLType = 1, 
	SPName = N'USR_LoanRepayY', --SPName
	SuspType = 'Rule', 
	Schedule = 4, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@activityTypeIn" Alias="List of Deposit Activity Types (Repayment). -ALL- for all" Value="-ALL-" /><Param Name="@activityTypeOut" Alias="List of Withdrawal Activity Types (Loan). -ALL- for all" Value="2014,2024,2034,2044,2094,2104,2114,2124" /><Param Name="@productType" Alias="List of Products (Account Types) X. -ALL- for all" Value="-ALL-" /><Param Name="@percent" Alias="Y Percent" Value="10" /><Param Name="@month" Alias="Z Months" Value="1" /><Param Name="@RiskClass" Alias="List of RiskClasses. -ALL- for all" Value="-ALL-" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 2, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'LoanRepayYP' --TemplateWLCode 
	WHERE WLCode = 'LoanRepayY';  --WLCode

--5. NCustActivt
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'NCustActivt') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'NCustActivt', --[WLCode]
	N'Monitors Activity for new customers', --[Title]
	N'Monitors new customer activity for an initial period and displays the aggregated values for amounts and counts. When the customer completes the initial period, the system will suggest the profile values for In/Out amounts and counts based on average during the initial period. Accepts comma separated risk class codes. Use ''-ALL-'' for all Risk Classes. Designed to run Pre-Eod.', --[Desc]
	1, N'USR_NCustActivt', --[SPName]
	N'Rule', 
	1, --Schedule 
	1, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@RiskClassList" Alias="Risk Class" Value="-ALL-" /><Param Name="@InitialTimeFrame" Alias="Initial Period (days)" Value="90" /><Param Name="@AvgPeriod" Alias="Average Period (days)" Value="30" /><Param Name="@GenerateAlertsBeforeInitialTimeFrame" Alias="Generate Alerts for activity before the Initial Period(1 for Yes 0 for No)" Value="1" /><Param Name="@MinimumAmount" Alias="Minimum Amount" Value="150000" /></Params>', --[Params]
	0, 1, N'User Defined', 
	2, --[UseSysDate]
	0, 
	N'NCustProAct' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Monitors Activity for new customers', --Title
	[Desc] = N'Monitors new customer activity for an initial period and displays the aggregated values for amounts and counts. When the customer completes the initial period, the system will suggest the profile values for In/Out amounts and counts based on average during the initial period. Accepts comma separated risk class codes. Use ''-ALL-'' for all Risk Classes. Designed to run Pre-Eod.', --[Desc]
	WLType = 1, 
	SPName = N'USR_NCustActivt', --SPName
	SuspType = 'Rule', 
	Schedule = 1, --Schedule 
	IsPreEOD = 1, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@RiskClassList" Alias="Risk Class" Value="-ALL-" /><Param Name="@InitialTimeFrame" Alias="Initial Period (days)" Value="90" /><Param Name="@AvgPeriod" Alias="Average Period (days)" Value="30" /><Param Name="@GenerateAlertsBeforeInitialTimeFrame" Alias="Generate Alerts for activity before the Initial Period(1 for Yes 0 for No)" Value="1" /><Param Name="@MinimumAmount" Alias="Minimum Amount" Value="150000" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 2, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'NCustProAct' --TemplateWLCode 
	WHERE WLCode = 'NCustActivt';  --WLCode

--6. StrCashDepCtr
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'StrCashDepC') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'StrCashDepC', --[WLCode]
	N'Detects structuring of cash deposits to avoid CTR reporting', --[Title]
	N'Detects structuring of cash deposits to an account in order to avoid CTR reporting.  An Alert or SA case is created for each account that has cash deposits being structured   and the total amount deposited exceeds a specified maximum  dollar amount over a certain    time period.  Designed to be run Post-EOD on a monthly or quarterly schedule.', --[Desc]
	1, N'USR_StrCashDepC', --[SPName]
	N'Rule', 
	4, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@CTRAmount" Alias="CTR Amount" Value="25000" /><Param Name="@LastNoOfMonths" Alias="Last No Of Months" Value="8" /><Param Name="@MinTotalCTRAmount" Alias="Min Total CTR Amount" Value="25000" /><Param Name="@Tolerance" Alias="Tolerance" Value="20" /></Params>', --[Params]
	0, 1, N'User Defined', 
	2, --[UseSysDate]
	0, 
	N'StructCTR' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects structuring of cash deposits to avoid CTR reporting', --Title
	[Desc] = N'Detects structuring of cash deposits to an account in order to avoid CTR reporting.  An Alert or SA case is created for each account that has cash deposits being structured   and the total amount deposited exceeds a specified maximum  dollar amount over a certain    time period.  Designed to be run Post-EOD on a monthly or quarterly schedule.', --[Desc]
	WLType = 1, 
	SPName = N'USR_StrCashDepC', --SPName
	SuspType = 'Rule', 
	Schedule = 4, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@CTRAmount" Alias="CTR Amount" Value="25000" /><Param Name="@LastNoOfMonths" Alias="Last No Of Months" Value="8" /><Param Name="@MinTotalCTRAmount" Alias="Min Total CTR Amount" Value="25000" /><Param Name="@Tolerance" Alias="Tolerance" Value="20" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 2, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'StructCTR' --TemplateWLCode 
	WHERE WLCode = 'StrCashDepC';  --WLCode

--7. ActXAmtCnt
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'ActXAmtCnt') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'ActXAmtCnt', --[WLCode]
	N'Detects accounts with more than X transactions that have aggregate greater than or equal to dollar threshold within a specific time frame.', --[Title]
	N'Detects acounts that had more than X Transactions,using a specified activity type(cash,wires etc),that has aggregate equal or greater   to dollar threshold, within X time frame.An alert or SA case is generated for each account that has more than X transactions and the total  amount equals or exceeds the threshold amount over a certain time period.Designed to be run Post-EOD on a weekly,monthly or quarterly schedule.', --[Desc]
	1, N'USR_ActXAmtCnt', --[SPName]
	N'Rule', 
	4, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@LastNoOfDays" Alias="Last No Of Days" Value="31" /><Param Name="@ActivityTypeList" Alias="List of activity Types Separated by comma.-ALL- for All." Value="1044,1024,2334,2344,2354,2364,2374,2384,2214,2224,2414" /><Param Name="@ThresholdAmt" Alias="Threshold Amount" Value="1000000" /><Param Name="@NoOfTransactions" Alias="No Of Transactions" Value="1" /></Params>', --[Params]
	0, 1, N'User Defined', 
	2, --[UseSysDate]
	0, 
	N'ActXCntAmt' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects accounts with more than X transactions that have aggregate greater than or equal to dollar threshold within a specific time frame.', --Title
	[Desc] = N'Detects acounts that had more than X Transactions,using a specified activity type(cash,wires etc),that has aggregate equal or greater   to dollar threshold, within X time frame.An alert or SA case is generated for each account that has more than X transactions and the total  amount equals or exceeds the threshold amount over a certain time period.Designed to be run Post-EOD on a weekly,monthly or quarterly schedule.', --[Desc]
	WLType = 1, 
	SPName = N'USR_ActXAmtCnt', --SPName
	SuspType = 'Rule', 
	Schedule = 4, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@LastNoOfDays" Alias="Last No Of Days" Value="31" /><Param Name="@ActivityTypeList" Alias="List of activity Types Separated by comma.-ALL- for All." Value="1044,1024,2334,2344,2354,2364,2374,2384,2214,2224,2414" /><Param Name="@ThresholdAmt" Alias="Threshold Amount" Value="1000000" /><Param Name="@NoOfTransactions" Alias="No Of Transactions" Value="1" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 2, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'ActXCntAmt' --TemplateWLCode 
	WHERE WLCode = 'ActXAmtCnt';  --WLCode

--8. N3rdPartyY
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'N3rdPartyY') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'N3rdPartyY', --[WLCode]
	N'Detects a deposit W received and Y% is transferred within Z months to an unrelated third party.', --[Title]
	N'Detects a deposit W received and Y% is transferred within Z months to an unrelated third party.  Designed to be run Pre-EOD.', --[Desc]
	1, N'USR_N3rdPartyY', --[SPName]
	N'Rule', 
	1, --Schedule 
	1, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@productType" Alias="Product (Account Type) X" Value="Q,K" /><Param Name="@percent" Alias="Y Percent" Value="50" /><Param Name="@month" Alias="Z Months" Value="1" /><Param Name="@RiskClass" Alias="Riskclass" Value="-ALL-" /></Params>', --[Params]
	0, 1, N'User Defined', 
	0, --[UseSysDate]
	0, 
	N'Unrel3rdpty' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects a deposit W received and Y% is transferred within Z months to an unrelated third party.', --Title
	[Desc] = N'Detects a deposit W received and Y% is transferred within Z months to an unrelated third party.  Designed to be run Pre-EOD.', --[Desc]
	WLType = 1, 
	SPName = N'USR_N3rdPartyY', --SPName
	SuspType = 'Rule', 
	Schedule = 1, --Schedule 
	IsPreEOD = 1, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@productType" Alias="Product (Account Type) X" Value="Q,K" /><Param Name="@percent" Alias="Y Percent" Value="50" /><Param Name="@month" Alias="Z Months" Value="1" /><Param Name="@RiskClass" Alias="Riskclass" Value="-ALL-" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 0, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'Unrel3rdpty' --TemplateWLCode 
	WHERE WLCode = 'N3rdPartyY';  --WLCode



--10. PayMulToBen
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'PayMulToBen') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'PayMulToBen', --[WLCode]
	N'Detects multiple originators sending to a single Beneficiary', --[Title]
	N'Detects multiple originators sending transactions using specified activity types   to a single Beneficiary over a specified time period and the total of these transactions   is within a designated dollar amount range.  The alert/case will display   the account information in the description since the account may not be held at   the processing institution. Filtered by country and account types.  Designed to be run Post-EOD.', --[Desc]
	1, N'USR_PayMulToBen', --[SPName]
	N'Rule', 
	3, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@ActivityTypeList" Alias="List of Activity Types. -ALL- for all" Value="2354,2364,2384,1044" /><Param Name="@CountryList" Alias="List Of Countries. -ALL- for all" Value="-ALL-" /><Param Name="@AccountTypeList" Alias="List Of Account Types. -ALL- for all" Value="-ALL-" /><Param Name="@minimumAmount" Alias="Minimum Amount" Value="20000" /><Param Name="@maximumAmount" Alias="Maximum Amount  (-1 for no limit)" Value="-1" /><Param Name="@minimumCount" Alias="Minimum Count" Value="2" /><Param Name="@day" Alias="Number of Days" Value="7" /><Param Name="@recvpay" Alias="Receive (1) or Pay (2)" Value="2" /><Param Name="@branchList" Alias="List of Branches. -ALL- for all" Value="-ALL-" /><Param Name="@deptList" Alias="List of Departments. -ALL- for all" Value="-ALL-" /></Params>', --[Params]
	0, 1, N'User Defined', 
	2, --[UseSysDate]
	0, 
	N'PayMulByOrd' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects multiple originators sending to a single Beneficiary', --Title
	[Desc] = N'Detects multiple originators sending transactions using specified activity types   to a single Beneficiary over a specified time period and the total of these transactions   is within a designated dollar amount range.  The alert/case will display   the account information in the description since the account may not be held at   the processing institution. Filtered by country and account types.  Designed to be run Post-EOD.', --[Desc]
	WLType = 1, 
	SPName = N'USR_PayMulToBen', --SPName
	SuspType = 'Rule', 
	Schedule = 3, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@ActivityTypeList" Alias="List of Activity Types. -ALL- for all" Value="2354,2364,2384,1044" /><Param Name="@CountryList" Alias="List Of Countries. -ALL- for all" Value="-ALL-" /><Param Name="@AccountTypeList" Alias="List Of Account Types. -ALL- for all" Value="-ALL-" /><Param Name="@minimumAmount" Alias="Minimum Amount" Value="20000" /><Param Name="@maximumAmount" Alias="Maximum Amount  (-1 for no limit)" Value="-1" /><Param Name="@minimumCount" Alias="Minimum Count" Value="2" /><Param Name="@day" Alias="Number of Days" Value="7" /><Param Name="@recvpay" Alias="Receive (1) or Pay (2)" Value="2" /><Param Name="@branchList" Alias="List of Branches. -ALL- for all" Value="-ALL-" /><Param Name="@deptList" Alias="List of Departments. -ALL- for all" Value="-ALL-" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 2, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'PayMulByOrd' --TemplateWLCode 
	WHERE WLCode = 'PayMulToBen';  --WLCode

--11. WithDraw
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'WithDraw') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'WithDraw', --[WLCode]
	N'Detect transactions of activity type after cash deposits', --[Title]
	N'Detects transactions of a selected activity type after cash deposits,   where most of the deposited cash is withdrawn in a specified time period.   Designed to be run Post-EOD on a montly or quarterly schedule.', --[Desc]
	1, N'USR_WithDraw', --[SPName]
	N'Rule', 
	4, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@ActivityTypeList" Alias="List of activity Types Separated by comma. -ALL- for All." Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@LastNoOfMonths" Alias="Last No Of Months" Value="1" /><Param Name="@Tolerance" Alias="Tolerance" Value="20" /></Params>', --[Params]
	0, 1, N'User Defined', 
	2, --[UseSysDate]
	0, 
	N'WithDrawDep' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detect transactions of activity type after cash deposits', --Title
	[Desc] = N'Detects transactions of a selected activity type after cash deposits,   where most of the deposited cash is withdrawn in a specified time period.   Designed to be run Post-EOD on a montly or quarterly schedule.', --[Desc]
	WLType = 1, 
	SPName = N'USR_WithDraw', --SPName
	SuspType = 'Rule', 
	Schedule = 4, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@ActivityTypeList" Alias="List of activity Types Separated by comma. -ALL- for All." Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@LastNoOfMonths" Alias="Last No Of Months" Value="1" /><Param Name="@Tolerance" Alias="Tolerance" Value="20" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 2, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'WithDrawDep' --TemplateWLCode 
	WHERE WLCode = 'WithDraw';  --WLCode



--16. NoUsedAcct
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'NoUsedAcct') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'NoUsedAcct', --[WLCode]
	N'Detects Activity using Transaction codes that are not active.', --[Title]
	N'Provides automated monitoring of activity types that have not been designated for use.   Customer, account, type and value are specified each time a transaction is detected designated for a type other than those specified.', --[Desc]
	1, N'USR_NoUsedAcct', --[SPName]
	N'Rule', 
	1, --Schedule 
	1, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@ActivityTypeList" Alias="List of Active ActivityTypes" Value="2344,2364" /><Param Name="@MinAmount" Alias="Minimum Amount for the activity to be monitored" Value="0" /></Params>', --[Params]
	0, 1, N'User Defined', 
	0, --[UseSysDate]
	0, 
	N'UnusedAct' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects Activity using Transaction codes that are not active.', --Title
	[Desc] = N'Provides automated monitoring of activity types that have not been designated for use.   Customer, account, type and value are specified each time a transaction is detected designated for a type other than those specified.', --[Desc]
	WLType = 1, 
	SPName = N'USR_NoUsedAcct', --SPName
	SuspType = 'Rule', 
	Schedule = 1, --Schedule 
	IsPreEOD = 1, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@ActivityTypeList" Alias="List of Active ActivityTypes" Value="2344,2364" /><Param Name="@MinAmount" Alias="Minimum Amount for the activity to be monitored" Value="0" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 0, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'UnusedAct' --TemplateWLCode 
	WHERE WLCode = 'NoUsedAcct';  --WLCode

--17. CustCntAmtI
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'CustCntAmtI') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'CustCntAmtI', --[WLCode]
	N'Detects Customers with more than X transactions and/or have aggregate greater than or equal to dollar threshold within a specific time frame.', --[Title]
	N'Detects Customers that had more than X Transactions, using a specified activity type(cash,wires etc), that has aggregate equal or greater to dollar threshold, within specified time frame. An alert or SA case is generated for each customer that has more than X transactions and/or the total amount equals or exceeds the threshold amount over a certain time period. Filtered by Customer Type and Risk Class. Designed to be run Post-EOD on a weekly, monthly or quarterly schedule.', --[Desc]
	1, N'USR_CustCntAmtI', --[SPName]
	N'Rule', 
	4, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@LastNoOfDays" Alias="Last No Of Days" Value="30" /><Param Name="@ActivityTypeList" Alias="List of activity Types Separated by comma.-ALL- for All." Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@ThresholdAmt" Alias="Threshold Amount" Value="300000" /><Param Name="@NoOfTransactions" Alias="No Of Transactions" Value="1" /><Param Name="@RiskClassList" Alias="List of Risk Classes separated by comma. -ALL- for All." Value="-ALL-" /><Param Name="@CustTypeList" Alias="List of Customer Types separated by comma. -ALL- for All." Value="I" /><Param Name="@AmtCompareOper" Alias="Amount/Count Comparison (1 for OR, 2 for AND)" Value="2" /><Param Name="@branchList" Alias="List of Branches separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" /></Params>', --[Params]
	0, 1, N'User Defined', 
	2, --[UseSysDate]
	0, 
	N'CustCntAmt' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects Customers with more than X transactions and/or have aggregate greater than or equal to dollar threshold within a specific time frame.', --Title
	[Desc] = N'Detects Customers that had more than X Transactions, using a specified activity type(cash,wires etc), that has aggregate equal or greater to dollar threshold, within specified time frame. An alert or SA case is generated for each customer that has more than X transactions and/or the total amount equals or exceeds the threshold amount over a certain time period. Filtered by Customer Type and Risk Class. Designed to be run Post-EOD on a weekly, monthly or quarterly schedule.', --[Desc]
	WLType = 1, 
	SPName = N'USR_CustCntAmtI', --SPName
	SuspType = 'Rule', 
	Schedule = 4, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@LastNoOfDays" Alias="Last No Of Days" Value="30" /><Param Name="@ActivityTypeList" Alias="List of activity Types Separated by comma.-ALL- for All." Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@ThresholdAmt" Alias="Threshold Amount" Value="300000" /><Param Name="@NoOfTransactions" Alias="No Of Transactions" Value="1" /><Param Name="@RiskClassList" Alias="List of Risk Classes separated by comma. -ALL- for All." Value="-ALL-" /><Param Name="@CustTypeList" Alias="List of Customer Types separated by comma. -ALL- for All." Value="I" /><Param Name="@AmtCompareOper" Alias="Amount/Count Comparison (1 for OR, 2 for AND)" Value="2" /><Param Name="@branchList" Alias="List of Branches separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 2, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'CustCntAmt' --TemplateWLCode 
	WHERE WLCode = 'CustCntAmtI';  --WLCode

--17.1 CustCntAmtE
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'CustCntAmtE') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'CustCntAmtE', --[WLCode]
	N'Detects Customers with more than X transactions and/or have aggregate greater than or equal to dollar threshold within a specific time frame.', --[Title]
	N'Detects Customers that had more than X Transactions, using a specified activity type(cash,wires etc), that has aggregate equal or greater to dollar threshold, within specified time frame. An alert or SA case is generated for each customer that has more than X transactions and/or the total amount equals or exceeds the threshold amount over a certain time period. Filtered by Customer Type and Risk Class. Designed to be run Post-EOD on a weekly, monthly or quarterly schedule.', --[Desc]
	1, N'USR_CustCntAmtE', --[SPName]
	N'Rule', 
	4, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@LastNoOfDays" Alias="Last No Of Days" Value="30" /><Param Name="@ActivityTypeList" Alias="List of activity Types Separated by comma.-ALL- for All." Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@ThresholdAmt" Alias="Threshold Amount" Value="2000000" /><Param Name="@NoOfTransactions" Alias="No Of Transactions" Value="5" /><Param Name="@RiskClassList" Alias="List of Risk Classes separated by comma. -ALL- for All." Value="-ALL-" /><Param Name="@CustTypeList" Alias="List of Customer Types separated by comma. -ALL- for All." Value="E" /><Param Name="@AmtCompareOper" Alias="Amount/Count Comparison (1 for OR, 2 for AND)" Value="2" /><Param Name="@branchList" Alias="List of Branches separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" /></Params>', --[Params]
	0, 1, N'User Defined', 
	2, --[UseSysDate]
	0, 
	N'CustCntAmt' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects Customers with more than X transactions and/or have aggregate greater than or equal to dollar threshold within a specific time frame.', --Title
	[Desc] = N'Detects Customers that had more than X Transactions, using a specified activity type(cash,wires etc), that has aggregate equal or greater to dollar threshold, within specified time frame. An alert or SA case is generated for each customer that has more than X transactions and/or the total amount equals or exceeds the threshold amount over a certain time period. Filtered by Customer Type and Risk Class. Designed to be run Post-EOD on a weekly, monthly or quarterly schedule.', --[Desc]
	WLType = 1, 
	SPName = N'USR_CustCntAmtE', --SPName
	SuspType = 'Rule', 
	Schedule = 4, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@LastNoOfDays" Alias="Last No Of Days" Value="30" /><Param Name="@ActivityTypeList" Alias="List of activity Types Separated by comma.-ALL- for All." Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@ThresholdAmt" Alias="Threshold Amount" Value="2000000" /><Param Name="@NoOfTransactions" Alias="No Of Transactions" Value="5" /><Param Name="@RiskClassList" Alias="List of Risk Classes separated by comma. -ALL- for All." Value="-ALL-" /><Param Name="@CustTypeList" Alias="List of Customer Types separated by comma. -ALL- for All." Value="E" /><Param Name="@AmtCompareOper" Alias="Amount/Count Comparison (1 for OR, 2 for AND)" Value="2" /><Param Name="@branchList" Alias="List of Branches separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 2, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'CustCntAmt' --TemplateWLCode 
	WHERE WLCode = 'CustCntAmtE';  --WLCode

--18. WirBusCustI
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'WirBusCustI') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'WirBusCustI', --[WLCode]
	N'Detect large wire transfers in a single day per customer', --[Title]
	N'Detects wire transfers over a specified amount in a single day per   customer. Designed to be run Pre-EOD.', --[Desc]
	1, N'USR_WirBusCustI', --[SPName]
	N'Rule', 
	1, --Schedule 
	1, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@MinAmount" Alias="Minimum Amount" Value="500000" /><Param Name="@MaxAmount" Alias="Maximum Amount" Value="-1" /><Param Name="@ActvTypeList" Alias="Activity Types,Comma Seperated(Or -ALL-)" Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@CustTypeList" Alias="Customer Types,Comma Separated(Or -ALL-)" Value="I" /><Param Name="@AcctTypeList" Alias="Account Types,Comma Separated(Or -ALL-)" Value="-ALL-" /></Params>', --[Params]
	0, 1, N'User Defined', 
	1, --[UseSysDate]
	0, 
	N'WirBusCust' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detect large wire transfers in a single day per customer', --Title
	[Desc] = N'Detects wire transfers over a specified amount in a single day per   customer. Designed to be run Pre-EOD.', --[Desc]
	WLType = 1, 
	SPName = N'USR_WirBusCustI', --SPName
	SuspType = 'Rule', 
	Schedule = 1, --Schedule 
	IsPreEOD = 1, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@MinAmount" Alias="Minimum Amount" Value="500000" /><Param Name="@MaxAmount" Alias="Maximum Amount" Value="-1" /><Param Name="@ActvTypeList" Alias="Activity Types,Comma Seperated(Or -ALL-)" Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@CustTypeList" Alias="Customer Types,Comma Separated(Or -ALL-)" Value="I" /><Param Name="@AcctTypeList" Alias="Account Types,Comma Separated(Or -ALL-)" Value="-ALL-" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 1, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'WirBusCust' --TemplateWLCode 
	WHERE WLCode = 'WirBusCustI';  --WLCode

--18.1 WirBusCustE
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'WirBusCustE') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'WirBusCustE', --[WLCode]
	N'Detect large wire transfers in a single day per customer', --[Title]
	N'Detects wire transfers over a specified amount in a single day per   customer. Designed to be run Pre-EOD.', --[Desc]
	1, N'USR_WirBusCustE', --[SPName]
	N'Rule', 
	1, --Schedule 
	1, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@MinAmount" Alias="Minimum Amount" Value="1000000" /><Param Name="@MaxAmount" Alias="Maximum Amount" Value="-1" /><Param Name="@ActvTypeList" Alias="Activity Types,Comma Seperated(Or -ALL-)" Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@CustTypeList" Alias="Customer Types,Comma Separated(Or -ALL-)" Value="E" /><Param Name="@AcctTypeList" Alias="Account Types,Comma Separated(Or -ALL-)" Value="-ALL-" /></Params>', --[Params]
	0, 1, N'User Defined', 
	1, --[UseSysDate]
	0, 
	N'WirBusCust' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detect large wire transfers in a single day per customer', --Title
	[Desc] = N'Detects wire transfers over a specified amount in a single day per   customer. Designed to be run Pre-EOD.', --[Desc]
	WLType = 1, 
	SPName = N'USR_WirBusCustE', --SPName
	SuspType = 'Rule', 
	Schedule = 1, --Schedule 
	IsPreEOD = 1, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@MinAmount" Alias="Minimum Amount" Value="1000000" /><Param Name="@MaxAmount" Alias="Maximum Amount" Value="-1" /><Param Name="@ActvTypeList" Alias="Activity Types,Comma Seperated(Or -ALL-)" Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@CustTypeList" Alias="Customer Types,Comma Separated(Or -ALL-)" Value="E" /><Param Name="@AcctTypeList" Alias="Account Types,Comma Separated(Or -ALL-)" Value="-ALL-" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 1, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'WirBusCust' --TemplateWLCode 
	WHERE WLCode = 'WirBusCustE';  --WLCode

--19. PEPACT
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'PEPACT') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'PEPACT', --[WLCode]
	N'Detects Activity in PEP/FEP/MSB Accounts', --[Title]
	N'Provides automated monitoring of all PEP/FEP/MSB customer activity  by utilizing the PEP/FEP/MSB Flag on the Customer Records.Additional constraints  include Customer types, RiskClass and Activity types.Designed to be run Pre-EOD.', --[Desc]
	1, N'USR_PEPACT', --[SPName]
	N'Rule', 
	1, --Schedule 
	1, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@PEPFEPMSB" Alias="PEP/FEP/MSB" Value="PEP" /><Param Name="@CustTypeList" Alias="Comma separated list of Customer Types (-ALL- for all)" Value="-ALL-" /><Param Name="@RiskClassList" Alias="Comma separated list of Risk Classes (-ALL- for all)" Value="-ALL-" /><Param Name="@ActivityTypeList" Alias="Comma separated list of Activity Types (-ALL- for all)" Value="2334,2344,2354,2364,2374,2384,2414" /></Params>', --[Params]
	0, 1, N'User Defined', 
	0, --[UseSysDate]
	0, 
	N'PepFepMsb' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects Activity in PEP/FEP/MSB Accounts', --Title
	[Desc] = N'Provides automated monitoring of all PEP/FEP/MSB customer activity  by utilizing the PEP/FEP/MSB Flag on the Customer Records.Additional constraints  include Customer types, RiskClass and Activity types.Designed to be run Pre-EOD.', --[Desc]
	WLType = 1, 
	SPName = N'USR_PEPACT', --SPName
	SuspType = 'Rule', 
	Schedule = 1, --Schedule 
	IsPreEOD = 1, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@PEPFEPMSB" Alias="PEP/FEP/MSB" Value="PEP" /><Param Name="@CustTypeList" Alias="Comma separated list of Customer Types (-ALL- for all)" Value="-ALL-" /><Param Name="@RiskClassList" Alias="Comma separated list of Risk Classes (-ALL- for all)" Value="-ALL-" /><Param Name="@ActivityTypeList" Alias="Comma separated list of Activity Types (-ALL- for all)" Value="2334,2344,2354,2364,2374,2384,2414" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 0, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'PepFepMsb' --TemplateWLCode 
	WHERE WLCode = 'PEPACT';  --WLCode

--20. PepAcctAct
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'PepAcctAct') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'PepAcctAct', --[WLCode]
	N'Detects Activity in PEP/FEP/MSB Accounts over the last X days', --[Title]
	N'Provides automated monitoring of all PEP/FEP/MSB customer activity within the specified period by utilizing the PEP/FEP/MSB Flag on the Customer Records. Additional constraints include Customer types, RiskClasses,Activity types and total amount equals or exceeds a specified threshold amount. Designed to be run Post-EOD.', --[Desc]
	1, N'USR_PepAcctAct', --[SPName]
	N'Rule', 
	4, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@PEPFEPMSB" Alias="PEP/FEP/MSB" Value="PEP" /><Param Name="@CustTypeList" Alias="Comma separated list of Customer Types(-ALL- for all)" Value="-ALL-" /><Param Name="@RiskClassList" Alias="Comma separated list of Risk Classes(-ALL- for all)" Value="-ALL-" /><Param Name="@ActivityTypeList" Alias="Comma separated list of Activity Types(-ALL- for all)" Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@MinTotalAmount" Alias="The threshold amount the sum of activity should exceed. (-1 if no minimum)" Value="-1" /><Param Name="@Days" Alias="Number of days" Value="30" /></Params>', --[Params]
	0, 1, N'User Defined', 
	0, --[UseSysDate]
	0, 
	N'PFMActHist' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects Activity in PEP/FEP/MSB Accounts over the last X days', --Title
	[Desc] = N'Provides automated monitoring of all PEP/FEP/MSB customer activity within the specified period by utilizing the PEP/FEP/MSB Flag on the Customer Records. Additional constraints include Customer types, RiskClasses,Activity types and total amount equals or exceeds a specified threshold amount. Designed to be run Post-EOD.', --[Desc]
	WLType = 1, 
	SPName = N'USR_PepAcctAct', --SPName
	SuspType = 'Rule', 
	Schedule = 4, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@PEPFEPMSB" Alias="PEP/FEP/MSB" Value="PEP" /><Param Name="@CustTypeList" Alias="Comma separated list of Customer Types(-ALL- for all)" Value="-ALL-" /><Param Name="@RiskClassList" Alias="Comma separated list of Risk Classes(-ALL- for all)" Value="-ALL-" /><Param Name="@ActivityTypeList" Alias="Comma separated list of Activity Types(-ALL- for all)" Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@MinTotalAmount" Alias="The threshold amount the sum of activity should exceed. (-1 if no minimum)" Value="-1" /><Param Name="@Days" Alias="Number of days" Value="30" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 0, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'PFMActHist' --TemplateWLCode 
	WHERE WLCode = 'PepAcctAct';  --WLCode

--21. 1MthExcAvgI
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'1MthExcAvgI') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'1MthExcAvgI', --[WLCode]
	N'Detect Accounts with transactions exceeding average monthly total by specified amount within 1 calendar month.', --[Title]
	N'Detects Accounts that had transactions using a specified activity type and the total of these transactions for the month exceeds the average monthly volume of specified period (in months) by specified amount. Designed to be run Post-EOD on a weekly, monthly or quarterly schedule.', --[Desc]
	1, N'USR_1MthExcAvgI', --[SPName]
	N'Rule', 
	4, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@activityTypeList" Alias="List of Activity Types separated by Comma. -ALL- for All." Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@CustTypeList" Alias="List of Customer Types separated by Comma. -ALL- for All." Value="I" /><Param Name="@RiskClassList" Alias="List of Risk Classes separated by Comma. -ALL- for All." Value="-ALL-" /><Param Name="@TypeOfBusinessList" Alias="List of Type of Businesses separated by Comma. -ALL- for All." Value="-ALL-" /><Param Name="@BranchList" Alias="List of Branches separated by Comma. -ALL- for All." Value="-ALL-" /><Param Name="@RecvPay" Alias="Receive/Pay flag. 1 - for Receive, 2 - Pay, 3 - both" Value="3" /><Param Name="@NumMonths" Alias="Number of Months." Value="6" /><Param Name="@ExcAmtThreshold" Alias="Exceed Average Amount Threshold" Value="50000" /><Param Name="@InclLastMth" Alias="Include Last Month in Average 1-Yes 0-No" Value="0" /></Params>', --[Params]
	0, 1, N'User Defined', 
	2, --[UseSysDate]
	0, 
	N'1MthExcAvg' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detect Accounts with transactions exceeding average monthly total by specified amount within 1 calendar month.', --Title
	[Desc] = N'Detects Accounts that had transactions using a specified activity type and the total of these transactions for the month exceeds the average monthly volume of specified period (in months) by specified amount. Designed to be run Post-EOD on a weekly, monthly or quarterly schedule.', --[Desc]
	WLType = 1, 
	SPName = N'USR_1MthExcAvgI', --SPName
	SuspType = 'Rule', 
	Schedule = 4, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@activityTypeList" Alias="List of Activity Types separated by Comma. -ALL- for All." Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@CustTypeList" Alias="List of Customer Types separated by Comma. -ALL- for All." Value="I" /><Param Name="@RiskClassList" Alias="List of Risk Classes separated by Comma. -ALL- for All." Value="-ALL-" /><Param Name="@TypeOfBusinessList" Alias="List of Type of Businesses separated by Comma. -ALL- for All." Value="-ALL-" /><Param Name="@BranchList" Alias="List of Branches separated by Comma. -ALL- for All." Value="-ALL-" /><Param Name="@RecvPay" Alias="Receive/Pay flag. 1 - for Receive, 2 - Pay, 3 - both" Value="3" /><Param Name="@NumMonths" Alias="Number of Months." Value="6" /><Param Name="@ExcAmtThreshold" Alias="Exceed Average Amount Threshold" Value="50000" /><Param Name="@InclLastMth" Alias="Include Last Month in Average 1-Yes 0-No" Value="0" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 2, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'1MthExcAvg' --TemplateWLCode 
	WHERE WLCode = '1MthExcAvgI';  --WLCode

--21.1 1MthExcAvgE
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'1MthExcAvgE') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'1MthExcAvgE', --[WLCode]
	N'Detect Accounts with transactions exceeding average monthly total by specified amount within 1 calendar month.', --[Title]
	N'Detects Accounts that had transactions using a specified activity type and the total of these transactions for the month exceeds the average monthly volume of specified period (in months) by specified amount. Designed to be run Post-EOD on a weekly, monthly or quarterly schedule.', --[Desc]
	1, N'USR_1MthExcAvgE', --[SPName]
	N'Rule', 
	4, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@activityTypeList" Alias="List of Activity Types separated by Comma. -ALL- for All." Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@CustTypeList" Alias="List of Customer Types separated by Comma. -ALL- for All." Value="E" /><Param Name="@RiskClassList" Alias="List of Risk Classes separated by Comma. -ALL- for All." Value="-ALL-" /><Param Name="@TypeOfBusinessList" Alias="List of Type of Businesses separated by Comma. -ALL- for All." Value="-ALL-" /><Param Name="@BranchList" Alias="List of Branches separated by Comma. -ALL- for All." Value="-ALL-" /><Param Name="@RecvPay" Alias="Receive/Pay flag. 1 - for Receive, 2 - Pay, 3 - both" Value="3" /><Param Name="@NumMonths" Alias="Number of Months." Value="6" /><Param Name="@ExcAmtThreshold" Alias="Exceed Average Amount Threshold" Value="250000" /><Param Name="@InclLastMth" Alias="Include Last Month in Average 1-Yes 0-No" Value="0" /></Params>', --[Params]
	0, 1, N'User Defined', 
	2, --[UseSysDate]
	0, 
	N'1MthExcAvg' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detect Accounts with transactions exceeding average monthly total by specified amount within 1 calendar month.', --Title
	[Desc] = N'Detects Accounts that had transactions using a specified activity type and the total of these transactions for the month exceeds the average monthly volume of specified period (in months) by specified amount. Designed to be run Post-EOD on a weekly, monthly or quarterly schedule.', --[Desc]
	WLType = 1, 
	SPName = N'USR_1MthExcAvgE', --SPName
	SuspType = 'Rule', 
	Schedule = 4, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@activityTypeList" Alias="List of Activity Types separated by Comma. -ALL- for All." Value="2334,2344,2354,2364,2374,2384,1024,2414" /><Param Name="@CustTypeList" Alias="List of Customer Types separated by Comma. -ALL- for All." Value="E" /><Param Name="@RiskClassList" Alias="List of Risk Classes separated by Comma. -ALL- for All." Value="-ALL-" /><Param Name="@TypeOfBusinessList" Alias="List of Type of Businesses separated by Comma. -ALL- for All." Value="-ALL-" /><Param Name="@BranchList" Alias="List of Branches separated by Comma. -ALL- for All." Value="-ALL-" /><Param Name="@RecvPay" Alias="Receive/Pay flag. 1 - for Receive, 2 - Pay, 3 - both" Value="3" /><Param Name="@NumMonths" Alias="Number of Months." Value="6" /><Param Name="@ExcAmtThreshold" Alias="Exceed Average Amount Threshold" Value="250000" /><Param Name="@InclLastMth" Alias="Include Last Month in Average 1-Yes 0-No" Value="0" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 2, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'1MthExcAvg' --TemplateWLCode 
	WHERE WLCode = '1MthExcAvgE';  --WLCode

--22. KeywordAct
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'KeywordAct') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'KeywordAct', --[WLCode]
	N'Keyword Search on Party Names/Notes/Instructions fields of archived activity.', --[Title]
	N'Performs a keyword search on Party Names/Notes/Instructions fields of archived activity table.  An Alert or Case is generated where a match is found in the activity for a given customer/account combination.  Designed to be run daily, weekly, monthly or quarterly on a Post-EOD Schedule', --[Desc]
	1, N'USR_KeywordAct', --[SPName]
	N'Rule', 
	4, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@KeywordList" Alias="List of Keywords separated by comma" Value="ISIS,ISIL,AI Qaeda,Saqqez,Jihad" /><Param Name="@ActivityTypeList" Alias="List of Activity Types separated by comma. -ALL- for All." Value="2334,2344,2354,2364,2374,2384,1034,1024,2414" /><Param Name="@RecvPay" Alias="1 for Deposits, 2 for Withdrawals, 3 for All" Value="3" /><Param Name="@MinTotalAmount" Alias="The threshold amount the sum of activity should exceed" Value="-1" /><Param Name="@LastNoOfDays" Alias="No of Days" Value="31" /></Params>', --[Params]
	0, 1, N'User Defined', 
	2, --[UseSysDate]
	0, 
	N'Keyword' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Keyword Search on Party Names/Notes/Instructions fields of archived activity.', --Title
	[Desc] = N'Performs a keyword search on Party Names/Notes/Instructions fields of archived activity table.  An Alert or Case is generated where a match is found in the activity for a given customer/account combination.  Designed to be run daily, weekly, monthly or quarterly on a Post-EOD Schedule', --[Desc]
	WLType = 1, 
	SPName = N'USR_KeywordAct', --SPName
	SuspType = 'Rule', 
	Schedule = 4, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@KeywordList" Alias="List of Keywords separated by comma" Value="ISIS,ISIL,AI Qaeda,Saqqez,Jihad" /><Param Name="@ActivityTypeList" Alias="List of Activity Types separated by comma. -ALL- for All." Value="2334,2344,2354,2364,2374,2384,1034,1024,2414" /><Param Name="@RecvPay" Alias="1 for Deposits, 2 for Withdrawals, 3 for All" Value="3" /><Param Name="@MinTotalAmount" Alias="The threshold amount the sum of activity should exceed" Value="-1" /><Param Name="@LastNoOfDays" Alias="No of Days" Value="31" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 2, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'Keyword' --TemplateWLCode 
	WHERE WLCode = 'KeywordAct';  --WLCode



--27. BankHRCty
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.Watchlist  WHERE WLCode = N'BankHRCty') --WLCode
	INSERT [dbo].[Watchlist] (
	[WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], 
	[CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], 
	[ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], 
	[OverrideExemption], [TemplateWLCode]) 
	VALUES (
	N'BankHRCty', --[WLCode]
	N'Detects transactions, for which any of the country party or the addressfield is from the given list of countries and creates alert/case at the customer level.', --[Title]
	N'Detects transactions, for which any of the country party or the address field is from the given list of countries, and creates alert/case at the customer level. A transaction is included in the evaluation based on its qualification with: specified transaction types, a specified time period, the amount is in a specified range,the country field on the transaction is not in the specified country fields of the customer, and the cumulative amount of all the transactions meeting these constraints is in a specified range and the cumulative count is not less than the specified count.  Additional constraints include: Customer Type, Risk Class, Account Types, Activity Types, Cash/Non-Cash, Branch and Department. The alert/case will display the account information in the description since the account may not be held at the processing institution, and the distinct countries from the list that were found in the transactions. Designed to be run Post-EOD. When using Start Date and End Date parameters, Disable the rule or Reset parameter after rule executes', --[Desc]
	1, N'USR_BankHRCty', --[SPName]
	N'Rule', 
	4, --Schedule 
	0, NULL, NULL, NULL, N'UNISYSAD', 
	getdate(), N'UNISYSAD', getdate(), 
	NULL, NULL, 1, N'', 0, 15, 0, 1, 1, 
	N'<Params><Param Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month" /><Param Name="@StartDate" Alias="Start Date for Lookback MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" /><Param Name="@EndDate" Alias="End Date for Lookback MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" /><Param Name="@minIndCount" Alias="Minimum transaction count" Value="1" /><Param Name="@minIndAmt" Alias="Minimum individual transaction amount" Value="1000000" /><Param Name="@maxIndAmt" Alias="Maximum individual transaction amount. For no maximum use -1" Value="-1" /><Param Name="@minSumAmount" Alias="Minimum aggregated transaction amount." Value="0" /><Param Name="@maxSumAmount" Alias="Maximum aggregated transaction amount. For no maximum use -1" Value="-1" /><Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All." Value="B" /><Param Name="@accountTypeList" Alias="List of Account Types separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. Use -ALL- for All." Value="1014" /><Param Name="@countryList" Alias="List of Countries separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@UseMonitor" Alias="Use Monitor Field in Country Table. 1 for Yes, 0 for No" Value="0" /><Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="3" /><Param Name="@branchList" Alias="List of Branches separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@cashType" Alias="Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both" Value="0" /><Param Name="@ExcludeTranbyCustCtryCode" Alias="Exclude Transactions on Customer Country.&#xD;&#xA;          0 - Do not exclude,1 - By customer country,&#xD;&#xA;          2 - By Any customer country" Value="0" /><Param Name="@IncludeAddressFields" Alias="Include Address Fields to match on country.&#xD;&#xA;          0 - Do not Include,1 - Include" Value="0" /><Param Name="@UseRound" Alias="Indicate if only the high round amounts have to considered.&#xD;&#xA;          0 - Do not round,1 - Round" Value="0" /><Param Name="@Precision" Alias="Precision for rounding. Specify 3 for 1000, 2 for 100" Value="3" /></Params>', --[Params]
	0, 1, N'User Defined', 
	2, --[UseSysDate]
	0, 
	N'HghRiskCtry' --[TemplateWLCode]
	)
else
	UPDATE dbo.Watchlist SET 
	Title = N'Detects transactions, for which any of the country party or the addressfield is from the given list of countries and creates alert/case at the customer level.', --Title
	[Desc] = N'Detects transactions, for which any of the country party or the address field is from the given list of countries, and creates alert/case at the customer level. A transaction is included in the evaluation based on its qualification with: specified transaction types, a specified time period, the amount is in a specified range,the country field on the transaction is not in the specified country fields of the customer, and the cumulative amount of all the transactions meeting these constraints is in a specified range and the cumulative count is not less than the specified count.  Additional constraints include: Customer Type, Risk Class, Account Types, Activity Types, Cash/Non-Cash, Branch and Department. The alert/case will display the account information in the description since the account may not be held at the processing institution, and the distinct countries from the list that were found in the transactions. Designed to be run Post-EOD. When using Start Date and End Date parameters, Disable the rule or Reset parameter after rule executes', --[Desc]
	WLType = 1, 
	SPName = N'USR_BankHRCty', --SPName
	SuspType = 'Rule', 
	Schedule = 4, --Schedule 
	IsPreEOD = 0, 
	OwnerBranch = NULL, OwnerDept = NULL, OwnerOper = NULL, CreateOper = 'UNISYSAD', 
	CreateDate = getdate(), LastOper = 'UNISYSAD', LastModify = getdate(), 
	LastEval = null, LastEvalStat = null, RuleType = 1, RuleText = '', RuleFormat = 0, ExecTime = 15, 
	CaseScore = 0, UseAssignedScore = 1, IsLicensed = 1, 
	Params = N'<Params><Param Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month" /><Param Name="@StartDate" Alias="Start Date for Lookback MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" /><Param Name="@EndDate" Alias="End Date for Lookback MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" /><Param Name="@minIndCount" Alias="Minimum transaction count" Value="1" /><Param Name="@minIndAmt" Alias="Minimum individual transaction amount" Value="1000000" /><Param Name="@maxIndAmt" Alias="Maximum individual transaction amount. For no maximum use -1" Value="-1" /><Param Name="@minSumAmount" Alias="Minimum aggregated transaction amount." Value="0" /><Param Name="@maxSumAmount" Alias="Maximum aggregated transaction amount. For no maximum use -1" Value="-1" /><Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All." Value="B" /><Param Name="@accountTypeList" Alias="List of Account Types separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. Use -ALL- for All." Value="1014" /><Param Name="@countryList" Alias="List of Countries separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@UseMonitor" Alias="Use Monitor Field in Country Table. 1 for Yes, 0 for No" Value="0" /><Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="3" /><Param Name="@branchList" Alias="List of Branches separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@cashType" Alias="Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both" Value="0" /><Param Name="@ExcludeTranbyCustCtryCode" Alias="Exclude Transactions on Customer Country.&#xD;&#xA;          0 - Do not exclude,1 - By customer country,&#xD;&#xA;          2 - By Any customer country" Value="0" /><Param Name="@IncludeAddressFields" Alias="Include Address Fields to match on country.&#xD;&#xA;          0 - Do not Include,1 - Include" Value="0" /><Param Name="@UseRound" Alias="Indicate if only the high round amounts have to considered.&#xD;&#xA;          0 - Do not round,1 - Round" Value="0" /><Param Name="@Precision" Alias="Precision for rounding. Specify 3 for 1000, 2 for 100" Value="3" /></Params>', --Params
	CreateType = 0, IsUserDefined = 1, Category = 'User Defined', 
    UseSysDate = 2, --[UseSysDate]
	OverrideExemption = 0, 
	TemplateWLCode = N'HghRiskCtry' --TemplateWLCode 
	WHERE WLCode = 'BankHRCty';  --WLCode
