USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[USR_N3rdPartyY]    Script Date: 11/15/2016 11:46:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USR_N3rdPartyY](@WLCode SCode, @testAlert INT,
	@productType SCode,
	@percent  INT,
	@month  INT,
	@RiskClass SCode)
AS
/* RULE AND PARAMETER DESCRIPTION
Detects a deposit W received and Y% is transferred within Z months to an unrelated third party.

	@productType = account type e.g. checking, check,
	@percent = % of withdrawn from received
	@month = period to check,
	@RiskClass = risk class to filter on
*/
SET NOCOUNT ON

-- /*  Declarations */
DECLARE @iWLcode SCode, 
	@iTestAlert INT,
	@iProductType SCode,
	@iPercent  INT,
	@iMonth  INT ,
	@iRiskClass SCode

--speeds up select statements if use of local variable
SELECT @iWLcode = @WLcode , 
	@iTestAlert = @TestAlert ,
	@iProductType = 	@ProductType ,
	@iPercent  = ABS(@Percent)  ,
	@iMonth = ABS(@Month) ,
	@iRiskClass = @RiskClass

DECLARE	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,  --0 for alert, 1 for case
	@stat INT,
	@trnCnt INT,
	@minDate INT,
	@cur CURSOR


Declare @account LONGNAME,
	@currDate INT,
	@recvAmt MONEY,
	@date int,
	@payAmt MONEY


-- THE FOLLOWING SECTION DEFINES ALL THE TEMPORARY TABLES.

-- This table contains summary of the withdrawals
DECLARE	@WDSummary TABLE (
	Account VARCHAR(40),
	Amount MONEY
)

-- This table contains the withdrawals entries
DECLARE	@WDEntries TABLE (
	Account VARCHAR(40),
	TranNo  int,
	Amount  MONEY
)

-- This table contains the deposit entries
DECLARE	@DepEntries TABLE (
	Account VARCHAR(40),
	TranNo  int,
	Amount  MONEY
)

-- This tables to hold the accounts.
DECLARE @accts TABLE (
	Account VARCHAR(40),
	Amount money
)

--  TEMPORARY TABLE DEFINITION ENDS HERE.


declare @startDate datetime
declare @endDate datetime
declare @bookDate int

SET @stat = 0
Declare @txt VARCHAR(1000)
IF @testAlert = 0 BEGIN
	SET @txt = 'Started Rule Evaluation'
	Exec BSA_InsEvent 'PRIME', 'Evl', 'Rule', @WLCode , @txt
END
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_N3rdPartyY
ELSE
	-- Already in a transaction
	SAVE TRAN USR_N3rdPartyY
/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @WLType = WLType  
FROM WatchList (NOLOCK) WHERE WLCode = @iWLCode

SET @minDate = dbo.ConvertSqlDateToInt(
	DATEADD(m, -1 * @iMonth, CONVERT(VARCHAR, GETDATE())))
SET @currDate = dbo.ConvertSqlDateToInt(GETDATE())


INSERT INTO @WDEntries(Account, TranNo, Amount)
	SELECT a.Account, a.TranNo, a.baseamt
	FROM Activity a (NOLOCK) INNER JOIN 
		Account acct
		ON a.Account = acct.Id 
		INNER JOIN Customer cust 
		ON a.Cust = Cust.Id AND cust.RiskClass = case when @iRiskClass = '-ALL-' then cust.RiskClass else @iRiskClass end
	WHERE a.bookdate > @minDate
		AND CHARINDEX(acct.Type ,@iProductType) > 0
                AND a.recvpay = 2 		-- Get only withdrawals
		AND (a.Bene <> cust.TIN OR ISNULL(a.Bene, '') = '')
		AND a.Account IS NOT NULL 


-- Get all the accounts into temporary @aacts table so you left with unique values.
-- This has to be after the first retrieval of withdrawals for current activity
INSERT INTO @accts (account)
SELECT DISTINCT Account FROM @WDEntries

/*
  Get all the WithDrawals activity to temporary withdrawal table from 
  ActivityHist (RecvType =2). We need to get the activity for those 
  accounts which exists in @WDEntries.	
  Store transcation numbers Also. Ofcourse for a given period of dates
*/

INSERT INTO @WDEntries(Account, TranNo, Amount)
	SELECT ah.Account, ah.TranNo, ah.baseamt 
	FROM ActivityHist ah (NOLOCK) INNER JOIN 
				@accts WD
					ON ah.Account = WD.account
        		INNER JOIN Customer cust 
		ON ah.Cust = Cust.Id AND cust.RiskClass = case when @iRiskClass = '-ALL-' then cust.RiskClass else @iRiskClass end
	WHERE ah.bookdate > @minDate 
                AND ah.recvpay = 2 		-- Get the withdrawals
		AND (ah.Bene <> cust.TIN OR ISNULL(ah.Bene, '') = '')

-- Select the records into the summary table , where it has account and associates 
-- withdrawal summary.

INSERT INTO @WDSummary (Account,  Amount)
SELECT Account, SUM(Amount) from @WDEntries group by Account

--  Get all the deposits from activity
insert into @DepEntries (Account, TranNo, Amount) 
	SELECT a.Account,  a.TranNo, a.baseamt 
	FROM Activity a (NOLOCK) INNER JOIN 
				@accts WD
					ON a.Account = WD.account
			INNER JOIN Customer cust 
		ON a.Cust = Cust.Id AND cust.RiskClass = case when @iRiskClass = '-ALL-' then cust.RiskClass else @iRiskClass end
	WHERE a.bookdate > @minDate 
                AND a.recvpay = 1 		-- Get the deposits

--  Get all the deposits from activity history
insert into @DepEntries (Account, TranNo, Amount) 
	SELECT ah.Account,  ah.TranNo, ah.baseamt 
	FROM ActivityHist ah (NOLOCK) INNER JOIN 
				@accts WD
					ON ah.Account = WD.account
			INNER JOIN Customer cust 
		ON ah.Cust = Cust.Id AND cust.RiskClass = case when @iRiskClass = '-ALL-' then cust.RiskClass else @iRiskClass end
	WHERE ah.bookdate > @minDate 
                AND ah.recvpay = 1 		-- Get the deposits

-- Clear Temporary accounts table for reuse.
delete @accts


insert into @accts (Account, Amount)
select A.Account, A.Amount from
@wdsummary A  where exists (select B.Account from @DepEntries B 
where A.account = B.Account and  
(B.Amount * @iPercent <= A.Amount * 100))


IF @testAlert = 1 BEGIN
 		SELECT @startDate = GETDATE()
		INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
					LASTMODIFY, CUST, ACCOUNT, IsTest)
		select @WLCODE, 'Account:  ''' + a.Account 
				+ '''' +  ' had ' +  CONVERT(VARCHAR, @iPercent) + '% or more amount'  +
				' withdrawn ( ' + CONVERT(VARCHAR,a.Amount ) + ' ) in the last ' +  
				CONVERT(VARCHAR, @imonth) + ' months to an unrelated third party.' , 0, 
			GETDATE(), NULL, NULL, NULL, a.Account, 1 from
		@accts a
		SELECT @STAT = @@ERROR	
		SELECT @endDate = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
	
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   SELECT 'ALERT', ALERTNO, TRANNO 
			FROM (
					SELECT Account, TranNo, Amount FROM @depentries
					UNION 
					SELECT Account, TranNo, Amount FROM @wdentries
					)  a 
				INNER JOIN @accts t
					ON a.account = t.account 				
				INNER JOIN ALERT 
					ON t.Account = ALERT.Account 
			WHERE			
			ALERT.WLCODE = @WLCODE AND
			ALERT.CREATEDATE BETWEEN @startDate AND @endDate

 		SELECT @STAT = @@ERROR 
		IF @STAT <> 0  GOTO ENDOFPROC
	END ELSE BEGIN
		IF @WLTYPE = 0 BEGIN
		SELECT @startDate = GETDATE()
 		INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
					LASTMODIFY, CUST, ACCOUNT)
		select @WLCODE, 'Account:  ''' + a.Account 
				+ '''' +  ' had ' +  CONVERT(VARCHAR, @iPercent) + '% or more amount'  +
				' withdrawn ( ' + CONVERT(VARCHAR,a.Amount ) + ' ) in the last ' +  
				CONVERT(VARCHAR, @imonth) + ' months to an unrelated third party.'   , 0, 
			GETDATE(), NULL, NULL, NULL, a.Account from
		@accts a
	 
		SELECT @STAT = @@ERROR	
		SELECT @endDate = GETDATE()
 		IF @STAT <> 0  GOTO ENDOFPROC
		END ELSE IF @WLTYPE = 1 BEGIN
			SELECT @startDate = GETDATE()
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			WLCODE, WLDESC, CREATETIME )
			SELECT	NULL, @currDate,
			(select top 1 Cust from AccountOwner where A.Account = Account), 
			Account,
			NULL, 'Rule', NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, 0, 0, 0, 0, 
			NULL, NULL, NULL, NULL, 0, NULL, NULL,
			@WLCODE, 'Account:  ''' + a.Account 
				+ '''' +  ' had ' +  CONVERT(VARCHAR, @iPercent) + '% or more amount'  +
				' withdrawn ( ' + CONVERT(VARCHAR,a.Amount ) + ' ) in the last ' +  
				CONVERT(VARCHAR, @imonth) + ' months to an unrelated third party.' , GETDATE() 
			FROM @accts A 
		SELECT @STAT = @@ERROR	
		SELECT @endDate = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
		END
		
		IF @WLTYPE = 0 BEGIN
 		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
 		   SELECT 'ALERT', ALERTNO, TRANNO 
			FROM (
					SELECT Account, TranNo, Amount FROM @depentries
					UNION 
					SELECT Account, TranNo, Amount FROM @wdentries
					) a 
				INNER JOIN @accts t
					ON a.account = t.account 				
				INNER JOIN ALERT 
					ON t.Account = ALERT.Account 
			WHERE			
			ALERT.WLCODE = @WLCODE AND
			ALERT.CREATEDATE BETWEEN @startDate AND @endDate

 		SELECT @STAT = @@ERROR 
 		END ELSE IF @WLTYPE = 1 BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   SELECT 'SUSPACT', RECNO, TRANNO 
			FROM (
					SELECT Account, TranNo, Amount FROM @depentries
					UNION 
					SELECT Account, TranNo, Amount FROM @wdentries
					) a 
				INNER JOIN @accts t
					ON a.account = t.account 				
				INNER JOIN suspiciousactivity s
					ON t.Account = S.Account 
			WHERE			
			S.WLCODE = @WLCODE AND
			S.CREATETIME BETWEEN @startDate AND @endDate
		SELECT @STAT = @@ERROR 
		END
	END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_N3rdPartyY
	IF @testAlert = 0 BEGIN
		SET @txt = 	'Error occured while evaluating rule'
		Exec BSA_InsEvent 'PRIME', 'Evl', 'Rule', @WLCode , @txt
	END
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_N3rdPartyY
	IF @testAlert = 0 BEGIN
		SET @txt = 	'Succesfully finished evaluating rule'			
		Exec BSA_InsEvent 'PRIME', 'Evl', 'Rule', @WLCode , @txt
	END
RETURN @stat


GO
