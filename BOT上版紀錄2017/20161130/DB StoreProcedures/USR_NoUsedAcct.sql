USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[USR_NoUsedAcct]    Script Date: 11/15/2016 11:46:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[USR_NoUsedAcct] (    @WLCode			        SCode, 
                                    @testAlert              INT, 
                                    @ActivityTypeList       VARCHAR(8000),
                                    @MinAmount              MONEY
                                )
AS


DECLARE @startAlertDate         DATETIME,
        @endAlertDate           DATETIME,
        @tranCount              INT,
        @status                 INT,
        @WLType                 INT,
	    @bookDate		        INT,
	    @ActivityType		    INT,
	    @ActivityName		    VARCHAR(40)


--Cleanup input.
SET @ActivityTypeList = LTRIM(RTRIM(@ActivityTypeList))

IF @ActivityTypeList = '-ALL-' OR ISNULL(@ActivityTypeList,'') = '' 
    SET @ActivityTypeList = NULL
ELSE
    /* We need to remove white space from the comma separated list.*/
    SET @ActivityTypeList = ',' + Replace(@ActivityTypeList,' ','') + ','

DECLARE @tempTable TABLE ( CustId       VARCHAR(35),
                           BookDate     INT,
                           ActivityType INT,
                           TranNo       INT,
                           BaseAmt      MONEY)

SET NOCOUNT ON
SET @status = 0
             
/********************** BEGIN RULE PROCEDURE ********************************/

--Save current transaction count.
SET @tranCount = @@TRANCOUNT
IF @tranCount = 0
    --Start new transaction if no transaction was already started.
    BEGIN TRANSACTION USR_NoUsedAcct
ELSE    
    SAVE TRANSACTION USR_NoUsedAcct

SELECT @WLType = WLType  
  FROM dbo.WatchList (NOLOCK) 
 WHERE WLCode = @WLCode

INSERT INTO @tempTable (CustId, BookDate, ActivityType, TranNo, BaseAmt)
    SELECT  Customer.[Id],
            Activity.BookDate,
            Activity.Type,
            Activity.TranNo,
            Activity.BaseAmt
      FROM  dbo.Activity INNER JOIN
            dbo.Customer ON Activity.Cust = Customer.[Id] INNER JOIN
            dbo.ActivityType ON Activity.Type = ActivityType.Type
     WHERE  CHARINDEX(','+ LTRIM(RTRIM(CONVERT(VARCHAR,ActivityType.Type))) + ','
                       , @ActivityTypeList) = 0 
       AND  @ActivityTypeList IS NOT NULL 
       AND  Activity.BaseAmt >= @MinAmount


IF @testAlert = 1 
BEGIN
    SELECT @startAlertDate = GETDATE()
    INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, 
                        LASTOPER, LASTMODIFY, CUST, ACCOUNT, IsTest) 
    SELECT @WLCode, 
           'There are ' + CONVERT(VARCHAR, COUNT(ActivityType)) 
           + ' transaction(s) involving ' + CONVERT(VARCHAR, COUNT(DISTINCT CustId)) 
           + ' customer(s) using new activity type ' 
           + CONVERT(VARCHAR, ActivityType) + ' for a total of ' 
           + CONVERT(VARCHAR, SUM(BaseAmt)), 
           0, GETDATE(), NULL, NULL, NULL, 
           NULL, 1 --We will set IsTest flag to 1.
      FROM @tempTable t
     GROUP
        BY ActivityType --We will insert one alert per activity type 
                        --regarless of number of customers/number of transactions. 

    SELECT @status = @@ERROR
    SELECT @endAlertDate = GETDATE()
    IF @status <> 0
        GOTO ENDOFPROC

    INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
    SELECT 'Alert', 
            AlertNo,
            TranNo
      FROM @tempTable t,
           Alert
     WHERE Alert.WLCode = @WLCode
       AND Alert.CreateDate BETWEEN @startAlertDate AND @endAlertDate
       /*Following two conditions picks the right alertno from Alert table to 
       join tempTable with. We group the activities by activitytype and put 
       Cust as NULL, so Alert.Cust IS NULL condition is addded. The second 
       condition is added because we are inserting record in Alert based on 
       ActivityType but there is no column in Alert table to store the 
       ActivityType.*/
       AND Alert.Cust IS NULL 
       AND Alert.[Desc] LIKE '%using new activity type ' 
       + CONVERT(VARCHAR, t.ActivityType) + '%'

    SELECT @status = @@ERROR    
END /* End of If @testAlert = 1 */
ELSE
BEGIN
    IF @WLType = 0 --Alert Generation
    BEGIN
        SELECT @startAlertDate = GETDATE()
        INSERT INTO Alert ( WLCode, [DESC], Status, CreateDate,
                            LastOper, LastModify, Cust, Account)
        SELECT @WLCode, 
               'There are ' + CONVERT(VARCHAR, COUNT(ActivityType)) 
               + ' transaction(s) involving ' 
               + CONVERT(VARCHAR, COUNT(DISTINCT CustId)) 
               + ' customer(s) using new activity Type ' 
               + CONVERT(VARCHAR, ActivityType) + ' for a total of ' 
               + CONVERT(VARCHAR, SUM(BaseAmt)), 
               0, GETDATE(), NULL, NULL, NULL, NULL
          FROM @tempTable t       
         GROUP
            BY ActivityType


        SELECT @status = @@ERROR
        SELECT @endAlertDate = GETDATE()

        IF @status <> 0 GOTO EndOfProc             
    END /* End for @WLType = 0 - WatchListType = Alert */
    ELSE IF @WLType = 1 -- Case Generation
    BEGIN
        SELECT @startAlertDate = GETDATE()
        SELECT @BookDate =  dbo.ConvertSqlDateToInt(GETDATE())

        INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
            ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
            RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
            ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
            EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
            OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
            REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
            WLCode, WLDESC, CREATETIME )
        SELECT DISTINCT NULL, @bookDate, NULL, NULL,
               NULL, 'RULE', NULL, NULL, NULL, NULL,
               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
               NULL, NULL, 0, 0, 0, 0, 
               NULL, NULL, NULL, NULL, 0, NULL, NULL, @WLCode,             
               'There are ' + CONVERT(VARCHAR, COUNT(ActivityType)) 
               + ' transaction(s) involving ' 
               + CONVERT(VARCHAR, COUNT(DISTINCT CustId)) 
               + ' customer(s) using new activity Type ' 
               + CONVERT(VARCHAR, ActivityType) + ' for a total of ' 
               + CONVERT(VARCHAR, SUM(BaseAmt))
               , GETDATE() 
        FROM @tempTable t
       GROUP
          BY ActivityType
        
        SELECT @status = @@ERROR
        SELECT @endAlertDate = GETDATE()

        IF @status <> 0 GOTO EndOfProc             
    END /* End for @WlType = 1 - Case */


    /*Insert record in SASActivity table for alert/case.*/
    IF @WLTYPE = 0 
    BEGIN
        INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
             SELECT 'Alert', AlertNO, TRANNO 
               FROM @tempTable t, 
                    Alert
              WHERE Alert.WLCode = @WLCode
                AND Alert.CreateDate BETWEEN @startAlertDate AND @endAlertDate
                AND Alert.Cust IS NULL 
                AND Alert.[Desc] LIKE '%using new activity type ' 
                    + CONVERT(VARCHAR, t.ActivityType) + '%'

 
        SELECT @status = @@ERROR
    END /*End for IF @WLType = 0 -- Alert */ 
    ELSE IF @WLTYPE = 1 
    BEGIN
         INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
              SELECT 'SUSPACT', RECNO, TRANNO 
                FROM @tempTable t,
                     Suspiciousactivity S
               WHERE S.WLCode = @WLCode 
                 AND S.CREATETIME BETWEEN @startAlertDate AND @endAlertDate
                 AND S.Cust IS NULL 
                 AND S.WLDesc LIKE '%using new activity type ' 
                     + CONVERT(VARCHAR, t.ActivityType) + '%'
 
        SELECT @status = @@ERROR
    END /* End for IF @WLType = 1 (Case) */ 

END /* End of Else FOR If @testAlert = 1 */
/********************** END RULE PROCEDURE ***********************************/

EndOfProc:
IF (@status <> 0) 
BEGIN 
  ROLLBACK TRAN USR_NoUsedAcct
  RETURN @status
END 

IF @tranCount = 0
  COMMIT TRAN USR_NoUsedAcct

RETURN @status



GO
