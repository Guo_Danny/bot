USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[USR_PEPACT]    Script Date: 11/15/2016 11:46:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USR_PEPACT] (@WLCode SCode, @testAlert INT, @PEPFEPMSB VARCHAR(12),
			@CustTypeList VARCHAR(8000),
			@RiskClassList VARCHAR(8000),
			@ActivityTypeList VARCHAR(8000))
AS

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@bookDate INT,
	@trnCnt INT

DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME
DECLARE @IndvSelection VARCHAR(10)
DECLARE	@Pos INT
DECLARE @List VARCHAR(100) 
DECLARE @CustomerExemptionsList VARCHAR(8000)

	CREATE TABLE #tmp (
			  Code CHAR(3),
			  flag BIT)

	--Insert the values into the temp table based on the user input

	SET @PEPFEPMSB = LTRIM(RTRIM(@PEPFEPMSB))+ ','
	SET @Pos = CHARINDEX(',', @PEPFEPMSB, 1)

	-- Logic for seperating into individual values based upon the User Selection 

	IF REPLACE(@PEPFEPMSB, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
	 		SET @IndvSelection = LTRIM(RTRIM(LEFT(@PEPFEPMSB, @Pos - 1)))
  			IF @IndvSelection <> ''
				INSERT INTO #tmp VALUES (@indvSelection, 1)

			SET @PEPFEPMSB = RIGHT(@PEPFEPMSB, LEN(@PEPFEPMSB) - @Pos)
			SET @Pos = CHARINDEX(',', @PEPFEPMSB, 1)
		END
	END	 
	
-- Temporary table of Activity Types that have not been specified as Exempt
DECLARE @ActType TABLE (
	Type	INT
)

DECLARE	@TT TABLE (
	AlertText VARCHAR(20),
	CustId VARCHAR(35),
	CustName VARCHAR(40),
	BookDate INT, 
	ActNo VARCHAR(35),
	TranNo INT
)

SET NOCOUNT ON
SET @stat = 0

-- CALL BSA_fnListParams for each of the Paramters that support comma separated values
SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)
SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)

-- Get the list of exemptions that are exempted on customer from rules
SELECT @CustomerExemptionsList = dbo.BSA_GetExemptionCodes('Customer','Rule')
IF ltrim(rtrim(@CustomerExemptionsList)) = ''
	SET @CustomerExemptionsList = NULL

--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_FepPepMsb
ELSE
	-- Already in a transaction
	SAVE TRAN USR_FepPepMsb
/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @description = [Desc], @WLType = WLType  
FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

INSERT INTO @ActType
	SELECT 	Type  FROM vwRuleNonExmActType
	WHERE	(@ActivityTypeList IS NULL 
		OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

INSERT INTO @TT(AlertText,CustId, CustName, BookDate, ActNo, TranNo)
 SELECT 
	AlertText = dbo.GetFepMsbPep(C.FEP,C.MSB,C.PEP) , 
	C.[Id],--Customer Id
	C.[Name], --Customer Name
        A.BookDate,--TransDate
	A.Account,--Account No
	A.TranNo --Transaction No.
FROM	Activity A INNER JOIN
	Customer  C ON A.Cust = C.[Id]
	INNER JOIN @ActType act ON A.Type = act.Type
WHERE 	(C.MSB = (Select flag from #tmp where code = 'MSB') 
	OR   C.FEP = (Select flag from #tmp where code = 'FEP')
	OR   C.PEP = (Select flag from #tmp where code = 'PEP')) 
	AND (ISNULL(@CustTypeList, '') = '' OR CHARINDEX(',' + ltrim(rtrim(C.Type)) + 
		',',   @CustTypeList ) > 0) 
	AND (ISNULL(@RiskClassList, '') = '' OR CHARINDEX(',' + ltrim(rtrim(C.RiskClass)) + 
		',',   @RiskClassList ) > 0)
	AND	(C.Exemptionstatus IS NULL OR @CustomerExemptionsList IS NULL OR
		CHARINDEX(',' + LTRIM(RTRIM(C.Exemptionstatus)) + ',', @CustomerExemptionsList ) = 0)
		
IF @testAlert = 1 BEGIN
	SELECT @STARTALRTDATE = GETDATE()
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT, IsTest) 
 	SELECT DISTINCT @WLCode, 'Customer: '+ CustName +' with ID:'+ CustId + ' is a '+ t.AlertText +' and has activity', 0, 
		GETDATE(), NULL, NULL, CustId, NULL, 1 
  	FROM @TT t

	SELECT @STAT = @@ERROR	
	SELECT @ENDALRTDATE = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	SELECT 'Alert', AlertNO, TRANNO 
	FROM @TT t, Alert
	WHERE   t.CustId = Alert.Cust   
		AND Alert.WLCode = @WLCode 
		AND Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE

	SELECT @STAT = @@ERROR 
END ELSE BEGIN
	IF @WLTYPE = 0 BEGIN
		SELECT @STARTALRTDATE = GETDATE()
 
		INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT) 
	 	SELECT DISTINCT @WLCode, 'Customer: '+ CustName +' with ID:'+ CustId + ' is a '+ t.AlertText +' and has activity', 0, 
			GETDATE(), NULL, NULL, CustId, NULL 
	  	FROM @TT t
 
		SELECT @STAT = @@ERROR	
		SELECT @ENDALRTDATE = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
	END ELSE IF @WLTYPE = 1 BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		SELECT @BookDate =  dbo.ConvertSqlDateToInt(GETDATE())
		INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			WLCode, WLDESC, CREATETIME )
 
		SELECT	DISTINCT NULL, @bookDate, CustId, NULL,
			NULL, 'RULE', NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, 0, 0, 0, 0, 
			NULL, NULL, NULL, NULL, 0, NULL, NULL,
			@WLCode, 'Customer: '+ CustName +' with ID:'+ CustId + ' is a '+ t.AlertText +' and has activity' , GETDATE() 
		FROM @TT t
 
		SELECT @STAT = @@ERROR	
		SELECT @ENDALRTDATE = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
	END
	
	IF @WLTYPE = 0 BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   SELECT 'Alert', AlertNO, TRANNO 
		   FROM @TT t, Alert 
 		   WHERE t.CustId = Alert.Cust   
			AND Alert.WLCode = @WLCode
			AND Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
 
		SELECT @STAT = @@ERROR 
	END ELSE IF @WLTYPE = 1 BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   SELECT 'SUSPACT', RECNO, TRANNO 
			FROM @TT t, SUSPICIOUSACTIVITY S 
			WHERE t.CustId = s.Cust    
			AND S.WLCode = @WLCode 
			AND S.CREATETIME BETWEEN @STARTALRTDATE AND @ENDALRTDATE
 
		SELECT @STAT = @@ERROR 
	END
END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_PEPACT
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_PEPACT
RETURN @stat



GO
