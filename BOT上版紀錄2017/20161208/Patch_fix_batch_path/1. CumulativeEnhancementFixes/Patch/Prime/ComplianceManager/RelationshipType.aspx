<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RelationshipType.aspx.vb" Inherits="CMBrowser.RelationshipType" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Relationship Type</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="js/nav.js" type="text/javascript"></script>
		<script language="javascript" type="text/javascript" >
		function chkIncludeRisk(isChkPrimaryClicked)
        {
            try
            {
                var chkPrimary = document.getElementById("<%= ChkPrimaryRel.ClientID %>");
                var chkInclude = document.getElementById("<%= chkIncludeInRiskAnalysis.ClientID %>");
                var hdnInclude = document.getElementById("<%= hdnIncludeInRisk.ClientID %>").value;
                if(hdnInclude == "1")
                    chkInclude.checked = true;
                if(chkPrimary.checked)
                {
                    chkInclude.checked = true;
                    chkInclude.disabled = true;
                }
                else
                {
                    if(isChkPrimaryClicked)
                    {
                        chkInclude.checked = false;
                        chkInclude.disabled = false;                
                    }
                }
            }
            catch(e){}	
        }
		</script>
	</HEAD>
	<body class="formbody" MS_POSITIONING="FlowLayout" >
		<form id="Form1" method="post" runat="server" >
			<table id="PageLayout" cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td>
						<asp:label id="clblRecordMessage" runat="server" cssClass="RecordMessage" width="808px">Record Message</asp:label>
						<asp:label id="lblErrMessage" CssClass="usererrorsummary" runat="server"></asp:label>
					</td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" Width="616px" Font-Size="X-Small" Height="24px"></asp:validationsummary></td>
					</TD></tr>
				<tr>
					<td>
						<TABLE id="Table2" style="WIDTH: 816px; HEIGHT: 40px" cellSpacing="0" cellPadding="0" width="816"
							border="0">
							<TR align="center">
								<TD align="right"><asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder></TD>
								<td align="left"><uc1:ucrecordscrollbar id="RecordScrollBar" runat="server"></uc1:ucrecordscrollbar></td>
							</TR>
						</TABLE>
						<uc1:uctabcontrol id="MainTabControl" runat="server"></uc1:uctabcontrol>
					</td>
				</tr>
				<tr>
					<td>
						<asp:label id="clblRecordHeader" runat="server" width="804px" CSSClass="paneltitle"></asp:label>
					</td>
				</tr>
			</table>
			<asp:panel id="Panel1" runat="server" Width="744px" Height="176px" Cssclass="Panel" BorderColor="Silver"
				BorderWidth="1px" BorderStyle="None">
				<TABLE id="Table41" cellSpacing="0" cellPadding="0" width="300" border="0">
					<TR>
						<TD>
							<asp:label id="Label5" runat="server" CssClass="LABEL" Width="190px">Code:</asp:label></TD>
						<TD>
							<asp:textbox id="txtCode" runat="server" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label1" runat="server" CssClass="LABEL">Name:</asp:label></TD>
						<TD>
							<asp:textbox id="txtName" runat="server" CssClass="TEXTBOX" Width="254px"></asp:textbox></TD>
						</TD></TR>
					<TR>
						<TD style="HEIGHT: 4px">
							<asp:label id="Label3" runat="server" CssClass="LABEL" Width="190px">Score:</asp:label></TD>
						<TD style="HEIGHT: 4px">
							<asp:textbox id="txtScore" runat="server" CssClass="TEXTBOX"></asp:textbox>
						<TD style="HEIGHT: 4px"></TD>
						<TD style="HEIGHT: 4px"></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 4px">
							<asp:label id="Label2" runat="server" CssClass="LABEL" Width="190px">Primary Relationship:</asp:label></TD>
						<TD style="HEIGHT: 4px">
							<asp:CheckBox id="ChkPrimaryRel" runat="server" onclick="chkIncludeRisk(1);"></asp:CheckBox></TD>
						<TD style="HEIGHT: 4px"></TD>
						<TD style="HEIGHT: 4px"></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 4px">
							<asp:label id="Label4" runat="server" CssClass="Label">Enabled for Auto Discovery:</asp:label></TD>
						<TD style="HEIGHT: 4px">
							<asp:CheckBox id="chkEnabledForAutoDiscovery" runat="server"></asp:CheckBox></TD>
						<TD style="HEIGHT: 4px"></TD>
						<TD style="HEIGHT: 4px"></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 4px">
							<asp:label id="Label6" runat="server" CssClass="Label" Width="190px">Include in Risk Analysis:</asp:label></TD>
						<TD style="HEIGHT: 4px">
							<asp:CheckBox id="chkIncludeInRiskAnalysis" runat="server"></asp:CheckBox></TD>
						<TD style="HEIGHT: 4px"></TD>
						<TD style="HEIGHT: 4px"></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 4px"></TD>
						<TD style="HEIGHT: 4px"></TD>
						<TD style="HEIGHT: 4px"></TD>
						<TD style="HEIGHT: 4px"></TD>
					</TR>
				</TABLE>
				<TABLE id="Table1" style="WIDTH: 344px; HEIGHT: 8px" cellSpacing="0" cellPadding="0" width="344"
					border="0">
					<TR>
						<TD style="WIDTH: 145px; HEIGHT: 5px"></TD>
						<TD style="HEIGHT: 5px"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 145px"></TD>
						<TD></TD>
					</TR>
				</TABLE>
			</asp:panel>
			<asp:panel id="Panel2" runat="server" Width="712px" Height="15px" cssclass="Panel"></asp:panel><asp:panel id="cPanelValidators" runat="server" Width="864px" Height="46px"></asp:panel>
			<input type="hidden" id="hdnIncludeInRisk" runat="server" value="1" />
			</form>
	</body>
	<script language="javascript" type="text/javascript">
		 chkIncludeRisk(0);
    </script>
</HTML>
