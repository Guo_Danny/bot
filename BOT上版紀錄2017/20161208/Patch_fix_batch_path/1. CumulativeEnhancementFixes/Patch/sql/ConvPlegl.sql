/*
**  File Name:        ConvPlegl.sql
**
**  Functional Description:
**
**       This module contains SQL patches
**		 for version 10.0.0
**
**  Creation Date:    08/04/2014
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2011                                                   ***
*** Metavante Corporation.                                               ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|----------------------------------------------------        
08/04/2014		SS		  R140433016: Modified LEGL_ProcessXMLRequest to fix the default value 
                          issue in the DOB Field for the Legal request in XML format.

*/


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Starting Conversion of Plegl Database to version 10.0.0'
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

Use Plegl
GO

Set ANSI_NULLS ON

Go

Set NOCOUNT ON 
Go


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LEGL_ProcessXMLRequest]'))
DROP PROCEDURE dbo.LEGL_ProcessXMLRequest
GO

CREATE PROCEDURE dbo.LEGL_ProcessXMLRequest
(
            @XMLREQUEST TEXT,
            @StatCode INT,
            @sRequestID VARCHAR(35) OUTPUT,
            @Oper VARCHAR(20)
)
With Encryption As
            SET XACT_ABORT ON  
            SET NOCOUNT ON
            DECLARE

                        @RequestType VARCHAR (11) ,
                        @RequestorID VARCHAR(35) ,
                        @AgencyId VARCHAR(35) ,
                        @RequestorName VARCHAR(40) ,
                        @Title VARCHAR(40) ,
                        @Address1 VARCHAR(120) ,
                        @Address2 VARCHAR(120) ,
                        @City VARCHAR(40) ,
                        @State VARCHAR(40) ,
                        @Zip VARCHAR(15) ,
                        @Country VARCHAR(40) ,
                        @ContactPhone VARCHAR(20) ,
                        @Email VARCHAR(50) ,
                        @Fax VARCHAR(20) ,
                        @RespondToID VARCHAR(35) ,
                        @RespondToAgencyId VARCHAR(35) ,
                        @RespondToName VARCHAR(40) ,
                        @RespondToTitle VARCHAR(40) ,
                        @RespondToAddress1 VARCHAR(120) ,
                        @RespondToAddress2 VARCHAR(120) ,
                        @RespondToCity VARCHAR(40) ,
                        @RespondToState VARCHAR(40) ,
                        @RespondToZip VARCHAR(15) ,
                        @RespondToCountry VARCHAR(40) ,
                        @RespondToContactPhone VARCHAR(20) ,
                        @RespondToEmail VARCHAR(50) ,
                        @RespondToFax VARCHAR(20) ,
                        @ResponseDueDate VARCHAR(20) ,
                        @ExpirationDate VARCHAR(20) ,
                        @SearchQuery VARCHAR(2000),
                        @Name VARCHAR(180),
						@FirstName  VARCHAR(180),
						@MiddleName VARCHAR(180),
						@LastName   VARCHAR(180),
                        @Suffix VARCHAR(10),
                        @SearchAddress1 VARCHAR(120) ,
                        @SearchAddress2 VARCHAR(120) ,
                        @SearchCity VARCHAR(40),
                        @SearchState VARCHAR(40) ,
                        @SearchZip VARCHAR(15) ,
                        @SearchCountry VARCHAR(40) ,
                        @Phone            VARCHAR(20),
                        @SearchRemarks1 VARCHAR(255) ,
                        @DOB GenericDate,
                        @TIN VARCHAR(20),
                        @ResponseFormat VARCHAR(35) ,
                        @OriginalRequestID VARCHAR(35) ,
                        @Description VARCHAR(40) ,
                        @RequestDate VARCHAR(40) ,
                        @LongDescription VARCHAR(1000) ,
                        @SearchType VARCHAR(5),
                        @RequestID                  ObjectID,
                        @StatusCode                int,
                        @FormatID                    ObjectID, -- Format ID Local Variable(To hold the Format ID Value generated)
                        @RequestTypeID                       SCode,
                        @RequestTypeDesc      VARCHAR(40), 
                        @DurationUnit               VARCHAR(11),
                        @FormatDesc               LongName,
                        @CreateOper                SCode,
                        @DatePart                    VARCHAR(200),
                        @WhereString               VARCHAR(4000),
                        @QueryString                VARCHAR(2000),
                        @QueryParams             VARCHAR(1000),
                        @OFACEntNum                        INT, 
                        @Count                                    INT,
                        @intPointer                   INT,
              @retRequestOFACEntNum int

            DECLARE @STRSQL VARCHAR(1000)
	    
	    DECLARE @params varchar (1000)
	    DECLARE @filterDate  varchar(8)

            -- Start standard stored procedure transaction header
            DECLARE @trnCnt int
            DECLARE @stat int

            EXEC LEGL_InsEvent @Oper, 'Del', 'ReqQuery' ,'', @XMLREQUEST

            SELECT @trnCnt = @@trancount           -- Save the current trancount

            IF @trnCnt = 0
                        -- Transaction has not begun
                        BEGIN TRAN LEGL_ProcessXMLRequest
            ELSE
                        -- Already in a transaction

            SAVE TRAN LEGL_ProcessXMLRequest

/*          Create temp table for storing the values from XML */
            CREATE TABLE #RequestXML 
            (
                        RequestType VARCHAR (11) NULL,
                        RequestorID VARCHAR(35) NULL,
                        AgencyId VARCHAR(35) NULL,
                        RequestorName VARCHAR(40) NULL,
                        Title VARCHAR(40) NULL,
                        Address1 VARCHAR(120) NULL,
                        Address2 VARCHAR(120) NULL,
                        City VARCHAR(40) NULL,
                        State VARCHAR(40) NULL,
                        Zip VARCHAR(15) NULL,
                        Country VARCHAR(40) NULL,
                        ContactPhone VARCHAR(20) NULL,
                        Email VARCHAR(50) NULL,
                        Fax VARCHAR(20) NULL,
                        RespondToID VARCHAR(35) NULL,
                        RespondToAgencyId VARCHAR(35) NULL,
                        RespondToName VARCHAR(40) NULL,
                        RespondToTitle VARCHAR(40) NULL,
                        RespondToAddress1 VARCHAR(120) NULL,
                        RespondToAddress2 VARCHAR(120) NULL,
                        RespondToCity VARCHAR(40) NULL,
                        RespondToState VARCHAR(40) NULL,
                        RespondToZip VARCHAR(15) NULL,
                        RespondToCountry VARCHAR(40) NULL,
                        RespondToContactPhone VARCHAR(20) NULL,
                        RespondToEmail VARCHAR(50) NULL,
                        RespondToFax VARCHAR(20) NULL,
                        ResponseDueDate VARCHAR(20) NULL,
                        ExpirationDate VARCHAR(20) NULL,
                        SearchQuery VARCHAR(2000) NULL,
                        [Name] VARCHAR(180) NULL,
						FirstName  VARCHAR(180) NULL,
						MiddleName VARCHAR(180) NULL,
						LastName   VARCHAR(180) NULL,
                        Suffix VARCHAR(10) NULL,
                        SearchAddress1 VARCHAR(120) NULL,
                        SearchAddress2 VARCHAR(120) NULL,
                        SearchCity VARCHAR(40) NULL,
                        SearchState VARCHAR(40) NULL,
                        SearchZip VARCHAR(15) NULL,
                        SearchCountry VARCHAR(40) NULL,
                        SearchRemarks1 VARCHAR(255) NULL,
                        DOB VARCHAR(20) NULL,
                        TIN VARCHAR(20) NULL,
                        Phone VARCHAR(20) NULL,
                        ResponseFormat VARCHAR(35) NULL,
                        OriginalRequestID VARCHAR(35) NULL,
                        Description VARCHAR(40) NULL,
                        RequestDate VARCHAR(40) NULL,
                        LongDescription VARCHAR(1000) NULL,
                        SearchType VARCHAR(5) NULL
            )

            SELECT @stat = @@error
            -- Evaluate results of the transaction
            IF @stat <> 0
            BEGIN
                        ROLLBACK TRAN LEGL_ProcessXMLRequest
                        RETURN @stat
            END                             

/*          Create Document Handle for XML */
            EXEC sp_xml_preparedocument @intPointer OUTPUT, @xmlRequest
            
	    SELECT @stat = @@error

            -- Evaluate results of the transaction
            IF @stat <> 0
            BEGIN
                        ROLLBACK TRAN LEGL_ProcessXMLRequest
                        RETURN @stat
            END  
                           
/*          Insert Data from XML into the temp table. (The temp table will have 
            repeating values for Request information) */

            INSERT INTO #RequestXML 
            SELECT 
			
			RequestType, RequestorID, AgencyId, RequestorName,
            Title, Address1, Address2, City, [State], Zip,
            Country, ContactPhone, Email, Fax, RespondToID,
            RespondToAgencyId, RespondToName, RespondToTitle,
            RespondToAddress1, RespondToAddress2, RespondToCity,
            RespondToState, RespondToZip, RespondToCountry,
            RespondToContactPhone, RespondToEmail, RespondToFax,
			ResponseDueDate, ExpirationDate, SearchQuery, [Name],
			FirstName, MiddleName, LastName, Suffix, SearchAddress1,
            SearchAddress2, SearchCity, SearchState, SearchZip,
            SearchCountry, SearchRemarks1, NULLIF(DOB,''), TIN, Phone,
            ResponseFormat, OriginalRequestID, Description,
            RequestDate, LongDescription, SearchType 
			
			
			
            FROM OPENXML (@intPointer, '/LegalRequest/SearchInfo/SearchList/SearchGroup/Address',2)
                                  WITH(   RequestType VARCHAR (11) '../../../../RequestType',
                                                RequestorID VARCHAR(35) '../../../../RequestAgency/RequestorID',
                                                AgencyId VARCHAR(35) '../../../../RequestAgency/RequestorInfo/AgencyID',
                                                RequestorName VARCHAR(40) '../../../../RequestAgency/RequestorInfo/RequestorName',
                                                Title VARCHAR(40) '../../../../RequestAgency/RequestorInfo/Title',
                                                Address1 VARCHAR(120) '../../../../RequestAgency/RequestorInfo/Address1',
                                                Address2 VARCHAR(120) '../../../../RequestAgency/RequestorInfo/Address2',
                                                City VARCHAR(40) '../../../../RequestAgency/RequestorInfo/City',
                                                State VARCHAR(40) '../../../../RequestAgency/RequestorInfo/State',
                                                Zip VARCHAR(15) '../../../../RequestAgency/RequestorInfo/Zip',
                                                Country VARCHAR(40) '../../../../RequestAgency/RequestorInfo/Country',
                                                ContactPhone VARCHAR(20) '../../../../RequestAgency/RequestorInfo/ContactPhone',
                                                Email VARCHAR(50) '../../../../RequestAgency/RequestorInfo/Email',
                                                Fax VARCHAR(20) '../../../../RequestAgency/RequestorInfo/Fax',
                                                RespondToID VARCHAR(35) '../../../../ResponseAgency/RequestorInfo/RequestorID',
                                                RespondToAgencyId VARCHAR(35) '../../../../ResponseAgency/RequestorInfo/AgencyId',
                                                RespondToName VARCHAR(40) '../../../../ResponseAgency/RequestorInfo/RequestorName',
                                                RespondToTitle VARCHAR(40) '../../../../ResponseAgency/RequestorInfo/Title',
                                                RespondToAddress1 VARCHAR(120) '../../../../ResponseAgency/RequestorInfo/Address1',
                                                RespondToAddress2 VARCHAR(120) '../../../../ResponseAgency/RequestorInfo/Address2',
                                                RespondToCity VARCHAR(40) '../../../../ResponseAgency/RequestorInfo/City',
                                                RespondToState VARCHAR(40) '../../../../ResponseAgency/RequestorInfo/State',
                                                RespondToZip VARCHAR(15) '../../../../ResponseAgency/RequestorInfo/Zip',
                                                RespondToCountry VARCHAR(40) '../../../../ResponseAgency/RequestorInfo/Country',
                                                RespondToContactPhone VARCHAR(20) '../../../../ResponseAgency/RequestorInfo/ContactPhone',
                                                RespondToEmail VARCHAR(50) '../../../../ResponseAgency/RequestorInfo/Email',
                                                RespondToFax VARCHAR(20) '../../../../ResponseAgency/RequestorInfo/Fax',
                                                ResponseDueDate VARCHAR(20) '../../../../ResponseDueDate',
                                                ExpirationDate VARCHAR(20) '../../../../ExpirationDate',
                                                SearchQuery VARCHAR(2000) '../../../../SearchInfo/SearchQuery',
                                                [Name] VARCHAR(180) '../Name',
												FirstName  VARCHAR(180) '../FirstName',
												MiddleName VARCHAR(180) '../MiddleName',
												LastName   VARCHAR(180) '../LastName',
                                                Suffix VArchar(20) '../Suffix',
                                                SearchAddress1 VARCHAR(120) 'Address1',
                                                SearchAddress2 VARCHAR(120) 'Address2',
                                                SearchCity VARCHAR(40) 'City',
                                                SearchState VARCHAR(40) 'State',
                                                SearchZip VARCHAR(15) 'Zip',
                                                SearchCountry VARCHAR(40) 'Country',
                                                SearchRemarks1 VARCHAR(255) '../Remarks1',
                                                DOB VARCHAR(20) '../DOB',
                                                TIN varchar(20) '../TIN',
                                                Phone   VARCHAR(20) '../Phone',
                                                ResponseFormat VARCHAR(35) '../../../../ResponseFormat',
                                                OriginalRequestID VARCHAR(35) '../../../../OriginalRequestID',
                                                Description VARCHAR(40) '../../../../Description',
                                                RequestDate VARCHAR(40) '../../../../RequestDate',
                                                LongDescription VARCHAR(1000) '../../../../LongDescription',
                                                SearchType VARCHAR(5) '../../../../SearchType'

                                                )

 
            SELECT * FROM #RequestXML

            SELECT @stat = @@error
            -- Evaluate results of the transaction

            IF @stat <> 0
            BEGIN
                        ROLLBACK TRAN LEGL_ProcessXMLRequest
                        RETURN @stat

            END                             

            SET @DatePart = replace(replace(LTRIM(RTRIM(convert(char, getdate(),2)))+

                                         LTRIM(RTRIM(convert(char, getdate(),114))),':',''),'.','')
            SET @FormatID  = 'FMT' + @DatePart

            --SET @RequestID = 'R'+ Replace(Replace(LTRIM(RTRIM(convert(char, getdate(),114))),':',''),'.','')
            SET @DurationUnit = 'Days'
            SET @CreateOper = @oper
            SET @StatusCode = @StatCode
            SELECT @Count = COUNT(RequestID) FROM Requests

 

ReCalculateRequestID:

            SET @RequestID = 'R' + CAST(DATEPART(yyyy,GETDATE())AS VARCHAR)+CAST(@Count AS VARCHAR)

            if exists ( SELECT * FROM Requests WHERE RequestId = @RequestId )

            begin
                        Select @count = @Count + 1
                        goto ReCalculateRequestID

            End

            --Select @requestID
            SELECT TOP 1 
                        @RequestType=RequestType ,
                        @RequestorID=RequestorID ,
                        @AgencyId=AgencyId ,
                        @RequestorName=RequestorName ,
                        @Title=Title ,
                        @Address1=Address1 ,
                        @Address2=Address2 ,
                        @City=City ,
                        @State=State ,
                        @Zip=Zip ,
                        @Country=Country ,
                        @ContactPhone=ContactPhone ,
                        @Email=Email ,
                        @Fax=Fax ,
                        @RespondToID=RespondToID ,
                        @RespondToAgencyId=RespondToAgencyId ,
                        @RespondToName=RespondToName ,
                        @RespondToTitle=RespondToTitle ,
                        @RespondToAddress1=RespondToAddress1 ,
                        @RespondToAddress2=RespondToAddress2 ,
                        @RespondToCity=RespondToCity ,
                        @RespondToState=RespondToState ,
                        @RespondToZip=RespondToZip ,
                        @RespondToCountry=RespondToCountry ,
                        @RespondToContactPhone=RespondToContactPhone ,
                        @RespondToEmail=RespondToEmail ,
                        @RespondToFax=RespondToFax ,
                        @SearchQuery=SearchQuery,
                        @ResponseDueDate=ResponseDueDate ,
                        @ExpirationDate=ExpirationDate ,
                        @ResponseFormat=ResponseFormat ,
                        @OriginalRequestID=OriginalRequestID ,
                        @Description=Description ,
                        @RequestDate=RequestDate ,
                        @LongDescription=LongDescription ,
                        @SearchType=SearchType
                        FROM #RequestXML

 

            IF @RequestType IS NULL

                        SET @RequestType = '314a'

 

            IF @AgencyID IS NULL

            BEGIN

                        UPDATE #RequestXML SET AgencyID = 'FINCEN'

                        SET @AgencyID = 'FINCEN'

            END

 

            SELECT @RequestorID = MIN (RequestorID) from #RequestXML

 

            IF @RequestorID IS NULL

            BEGIN

                        IF @RequestorName IS NULL
                        BEGIN
                                    SELECT @RequestorID=RequestorID FROM AgencyContacts WHERE
                                                AgencyContacts.AgencyID = @AgencyID
                                                AND AgencyContacts.PrimaryContact = 'Y'
                        END
                        ELSE
                        BEGIN
                                    SET @RequestorID = 'R'+ @DatePart

                                    EXEC LEGL_InsAgencyContacts @RequestorID
                                                ,@AgencyID
                                                ,@RequestorName
                                                ,@Title
                                                ,@Address1
                                                ,@Address2
                                                ,@City
                                                ,@State
                                                ,@Zip
                                                ,'N'
                                                ,@ContactPhone
                                                ,@Fax
                                                ,@EMail
                                                ,@CreateOper
                                    SELECT @stat = @@error

                                    -- Evaluate results of the transaction

                                    IF @stat <> 0
                                    BEGIN

                                                ROLLBACK TRAN LEGL_ProcessXMLRequest
                                                RETURN @stat

                                    END                             

                        END
                        UPDATE #RequestXML SET RequestorID = @RequestorID

            END

            IF @RespondToID IS NULL
            BEGIN
                        IF @RespondToName IS NULL
                        BEGIN

                                    SET @RespondToID = @RequestorID

                        END
                        ELSE

                        BEGIN
                                    SET @RespondToID = 'R'+ @DatePart

                                    EXEC LEGL_InsAgencyContacts @RespondToID
                                                ,@AgencyID
                                                ,@RespondToName
                                                ,@RespondToTitle
                                                ,@RespondToAddress1
                                                ,@RespondToAddress2
                                                ,@RespondToCity
                                                ,@RespondToState
                                                ,@RespondToZip
                                                ,'N'
                                                ,@RespondToContactPhone
                                                ,@RespondToFax
                                                ,@RespondToEMail
                                                ,@CreateOper
                                    SELECT @stat = @@error

                                    -- Evaluate results of the transaction

                                    IF @stat <> 0
                                    BEGIN
                                                ROLLBACK TRAN LEGL_ProcessXMLRequest
                                                RETURN @stat
                                    END
                        END
                        UPDATE #RequestXML SET RespondToID = @RespondToID
            END

            IF @RequestDate IS NULL
            BEGIN
                        SET @RequestDate = GetDate()
                        UPDATE #RequestXML SET RequestDate = @RequestDate
            END

            IF @ResponseDueDate IS NULL
            BEGIN
                        SELECT @ResponseDueDate=DATEADD(dd,ResponseDuration,@RequestDate) 
                                    FROM RequestType 
                                    WHERE  RequestTypeID = @RequestType
                        UPDATE #RequestXML SET ResponseDueDate = @ResponseDueDate
            END

            IF @ExpirationDate IS NULL
            BEGIN

                        SET @ExpirationDate = @ResponseDueDate
                        UPDATE #RequestXML SET ExpirationDate = @ResponseDueDate
            END

            IF @ResponseFormat IS NULL
            BEGIN
                        SET @ResponseFormat = 'XML'
                        UPDATE #RequestXML SET ResponseFormat = @ResponseFormat
            END

			--Use OFAC search for imported Legal Requests
			--SQL search is obsolete
			set @SearchType = 'OFAC'

            EXEC LEGL_InsRequests 
                                     @RequestID
                                    ,@Description
                                    ,@LongDescription
                                    ,@OriginalRequestID
                                    ,@ResponseFormat
                                    ,@RequestType
                                    ,@RequestorID
                                    ,@RespondToID
                                    ,@ResponseDueDate
                                    ,@ExpirationDate
                                    ,@RequestDate
                                    ,@StatusCode
                                    ,@SearchType
                                    ,'Y'
                                    ,@CreateOper
            SELECT @stat = @@error
            -- Evaluate results of the transaction
            IF @stat <> 0
            BEGIN
                        ROLLBACK TRAN LEGL_ProcessXMLRequest
                        RETURN @stat
            END

            IF @SearchQuery IS NOT NULL
            BEGIN
                       -- INSERT Into RequestQuery table
                        SELECT @WhereString=RIGHT(@SearchQuery,
                                    LEN(LTRIM(RTRIM(@SearchQuery)))-PATINDEX('%WHERE%',LTRIM(RTRIM(@SearchQuery)))+1)

                        SELECT @SearchQuery=SUBSTRING(@SearchQuery,0,
                                    (PATINDEX('%WHERE%',LTRIM(RTRIM(@SearchQuery)))))

 
                        EXEC LEGL_InsRequestQuery
                                     @RequestID
                                    ,NULL
                                    ,@SearchQuery
                                    ,@WhereString
                                    ,''
                                    ,@CreateOper

                        SELECT @stat = @@error

                        -- Evaluate results of the transaction

                        IF @stat <> 0

                        BEGIN

                                    ROLLBACK TRAN LEGL_ProcessXMLRequest

                                    RETURN @stat

                        END

            END

            ELSE

            BEGIN 

                -- INSERT Into RequestQuery table for Customer 

                SELECT @WhereString = ' Where ' 

                SELECT @SearchQuery = 'Select * from Customer ' 

                EXEC LEGL_InsRequestQuery 
                         @RequestID 
                        ,'Customer' 
                        ,@SearchQuery 

                        ,@WhereString 
                        ,'' 
                        ,@CreateOper 

                SELECT @stat = @@error 

                -- Evaluate results of the transaction 

                IF @stat <> 0 

                BEGIN 
                        ROLLBACK TRAN LEGL_ProcessXMLRequest 
                        RETURN @stat 
                END 

            	 -- add bookdate filter of 6 months ago from the requestDate
		--convert to ISO date standard
		set @filterDate= CONVERT(VARCHAR(8), DATEADD(mm,-6,@RequestDate), 112)
		set  @params='<?xml version="1.0"?><root><cri><fld value="BookDate"/><andor value=""/><cond value="&gt;="/><tbl value="Activity"/><col value="'+@filterDate +'"/><lp value=""/><rp value=""/></cri></root>'
               

                -- INSERT Into RequestQuery table for Activity 
                SELECT @WhereString = ' Where Activity.BookDate >= ''' +  @filterDate + ''''
		SELECT @SearchQuery = 'Select * from Activity ' 
                EXEC LEGL_InsRequestQuery 
                         @RequestID 
                        ,'Activity' 
                        ,@SearchQuery 
                        ,@WhereString 
                        ,@params 
                        ,@CreateOper 
 

                SELECT @stat = @@error 

                -- Evaluate results of the transaction 

                IF @stat <> 0 

                BEGIN 
                        ROLLBACK TRAN LEGL_ProcessXMLRequest 
                        RETURN @stat 
                END 
        END 

            SELECT
                                    Name,
									FirstName,
									MiddleName,
									LastName,
                                    Suffix,
                                    SearchAddress1,
                                    SearchAddress2,
                                    SearchCity,
                                    SearchState,
                                    SearchZip,
                                    SearchCountry,
                                    Phone,
                                    SearchRemarks1,
                                    CAST(CONVERT(VARCHAR,CAST(DOB AS DATETIME),112)AS INT) AS DOB,
                                    TIN
                        FROM #RequestXML

            DECLARE Cur_RequestOFAC SCROLL CURSOR FOR
                        SELECT
                                    Name,
									FirstName,
									MiddleName,
									LastName,
                                    ISNULL(Suffix, ''),
                                    SearchAddress1,
                                    SearchAddress2,
                                    SearchCity,
                                    SearchState,
                                    SearchZip,
                                    SearchCountry,
                                    Phone,
                                    SearchRemarks1,
                                    CAST(CONVERT(VARCHAR,CAST(DOB AS DATETIME),112)AS INT) AS DOB,
                                    TIN
                        FROM #RequestXML

            OPEN Cur_RequestOFAC

            FETCH FIRST FROM Cur_RequestOFAC INTO 
                                    @Name,
									@FirstName,
									@MiddleName,
									@LastName,
                                    @Suffix,
                                    @SearchAddress1,
                                    @SearchAddress2,
                                    @SearchCity,
                                    @SearchState,
                                    @SearchZip,
                                    @SearchCountry,
                                    @Phone,
                                    @SearchRemarks1,
                                    @DOB,
                                    @TIN

            SET @OFACEntNum = 0

 

            WHILE @@FETCH_STATUS <> -1

            BEGIN

                        SET @OFACEntNum = @OFACEntNum+1

                        EXEC LEGL_InsRequestOFAC
                                     @RequestID
                                    ,@OFACEntNum
                                    ,@Name
									,@FirstName		
									,@MiddleName	
									,@LastName		
                                    ,@Suffix
                                    ,NULL
                                    ,@SearchAddress1
                                    ,@SearchCity
                                    ,@SearchZip
                                    ,@SearchState
                                    ,@SearchCountry
                                    ,@Phone
                                    ,@SearchRemarks1
                                    ,NULL
                                    ,@DOB
                                    ,@TIN
                                    ,@CreateOper
                                    ,@retRequestOFACEntNum OUTPUT
 

                        FETCH NEXT FROM Cur_RequestOFAC INTO 
                                    @Name,
									@FirstName,
									@MiddleName,
									@LastName,
                                    @Suffix,
                                    @SearchAddress1,
                                    @SearchAddress2,
                                    @SearchCity,
                                    @SearchState,
                                    @SearchZip,
                                    @SearchCountry,
                                    @Phone,
                                    @SearchRemarks1,
                                    @DOB,
                                    @TIN 
            END

            CLOSE Cur_RequestOFAC

            DEALLOCATE Cur_RequestOFAC

            EXEC SP_XML_REMOVEDOCUMENT @intpointer

            SELECT @sRequestID = @RequestID

            DROP TABLE #RequestXML       

            IF @trnCnt = 0
                        COMMIT  TRAN LEGL_ProcessXMLRequest
            RETURN @stat
GO


PRINT ''
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT ' Completed Coversion of Plegl Database to version 10.0.0'
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT ''
GO
