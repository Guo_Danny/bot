|-----------------------------------------------------------------------------------------|
|				       :: INSTRUCTIONS ::                                 |   
|     				  				                          |
|-----------------------------------------------------------------------------------------|


Purpose of the Patch: 
---------------------
 
This patch address the following issues:

	1. R130547287 : Alerts not fitting user defined BSA Rule settings - Both rules use 
                        the Template �RepXinDDays�.
	2. R140341130 : Boston Private (BPB) is having issues with PDA.exe. 
	3. R140250508 : Performance Issue with SAR CTR 2012 Compliance Patch.
	4. R140068952 : Modified Cry_rulelist to handle params with more than 8000 
                        characters and with extended ascii characters.
	5. R131797427 : PRIME Application Post Upgrade Issue - OFAC Archived Case Display 
                        Issue.
	6. R140588731 : CTR Field 20 (ID Number) Issue.
	7. R140567096 : Import Sanctioned party 314a � Incorrect Modified Date value. 
	8. Defect 4131: BSA Web Services Max Connection Pool Issue.
	9. R140433016 : Legal reporter search issues with FINCEN - Extended Address
                        Matching.
       10. R140433016 : Legal reporter search issues with FINCEN - DOB Issue.
       11. R140978750 : Update the support information in Prime Compliance suite.
       12. R140181903 : PDA rules file configuration issue - When RCH file with cover 
                        payments are processed the incorrect information is extracted.
       13. R140888993 : Duplicate BSA alerts for rule ABBneEQOrgA.
       14. R141016033 : Asyncutil is not displayed.
       15. R141007968 : Receiving an error from FinCen when trying to submit a Batch CTR. 
       16. R141016244 : Issue with Program identification in real-time and manual 
                        OFAC cases.
       17. R141076548 : Prime version 10 Scan audit report -the scan Operator is not 
                        shown and it is spelled incorrectly.
       18. R140992605 : OFAC Image build prompt changes.
       19. R141171562 : Account Import Issue. Unable to delete account owner during 
                        account import.
       20. R141171587 : Unprocessed Account Issue. Error message displays when opening 
                        Unprocessed accounts records.
       21. PPL0136096 : To enhance the EDD system by providing options to include/not 
                        to include the Secondary account relationships in RiskScore 
                        generation process.
       22. Defect#4581: Unproc Accounts - Unable to Delete Primary Relationship / Other Relationship
       23. Defect#4582: Issue on adding a relationship in a unprocessed account record, 
			that has a non primary relationship without its related Party ID.



Description of changes:
----------------------

	1. Fixed the issue in creating alerts based upon the activity types in the rule
           USR_RepetitiveOverXinZDay.
	2. Modified PDA.exe for FundTechPBV5 interface to support input files transaction 
           version Payplus 6.0 and above.
	3. Modified BSA_ActivityBranchesForCase to improve performance when loading 
           SAR/CTR pdf. 
	4. Modified Cry_rulelist stored procedure to support extended ASCII Characters and 
	   parameter with more than 8000 Characters.
	5. Modified CMBrowser.dll to fix MatchParty details missing when opening an archived 
           Ofac Case. 
	6. The Item 20 - ID Number field becomes mandatory even after Checking the 
           'Unknown' check box.  This causes an error which indicates that an ID number is 
           required.  If OTHER under ITEM 20 is checked, modified the OTHER Checkbox events 
           to make IDNUMBER and COUNTRY non-mandatory.
	7. Modified OFS_ImportSDN stored procedure to correct ListCreateDate, EffectiveDate, 
	   LastModifDate and Program values during update.
	8. Modified BSA Web Service Methods to close/dispose SQL Server Connections properly 
	   after using those connections.
        9. Extended Address Matching with Request(City/State/Zip/Country) against Prime 
           Activity Address Field.  If the (City,State,Zip or Country) field on the party 
           record is populated, then the check for the field (City,State,Zip or Country) 
           on the fincen request in the address field will not happen.
       10. Modified LEGL_ProcessXMLRequest to fix the default value issue in the DOB Field 
           for the Legal request in XML format.
       11. Updated several files with the support information in Prime Compliance suite.
       12. pda.exe. Fixed offsets for 6.1.2 COVS records. Some of the start and end 
           positions were incorrect.  Fixed 6.12 COVS Instructions. They were not getting 
           populated when containing asterisks.
       13. Fixed the issue of duplicate alerts created in the rule 
           USR_SameByOrderBeneAndSameAmt.
       14. Fixed to display AsyncUtilServices in Process Manager. 
       15. Foreign CashIn EFile error fix in RegReport.cls.       
       16. Fixed the issue in Program Identification for Key Word Matching for OFAC Manual 
           Search.
       17. Fixed the issue with the scan operator which was inserting as NULL.	
       18. Created a new Option 'DspFimgLdis' in OFAC. For a user with the OFAC option 
           'DspFImgLDis' enabled and with the rights 'BLDIMG' and 'LOADDIST', the following 
           will be displayed.
		a) File Image Menu 
		b) Build File Image pop up screen during Logoff
		c) Links for Load Distribution and Build File Image.
	   Configured OFAC option DspFimgLdis=0 to disable File Image and Load Distribution
           links.
       19. Modified BSA_DelAccountOwner stored procedure to handle when relationship code
           and name are different. 
       20. Unprocessed Account Issue.  Modified GetAccountRelationships function to check 
           null value using NVS function to fix DBNull  
       21. Currently Prime considers both Primary and Secondary account relationships when 
           calculating the account type score. Based upon the IncludeInRisk option the 
           account type relationships may or may not be considered in Risk Score generation 
           process.
       22. Defect#4581: Modified values receiving data from encoded text to use 
                           HttpUtility.HtmlDecode text before using it
       23. Defect #4582: Getting the relationship correctly from dgAccountRelationships 
                         when adding a non primary relationship without related Party ID



Note: This patch should be installed and tested in a Test environment before it is deployed 
      to a Production environment.


Patch Name : 1 CumulativeEnhancementFixes
Patch Release Date: 09/11/2014
Version Applicable To: 10.0.0
Patch Pre-requisites: PCS 10.0 is installed.



INSTRUCTIONS:
-------------

      Download the attached "1 CumulativeEnhancementFixes.zip" file, and extract the contents 
      to a temporary folder.

      The following files will be extracted:

	1) ConvOfac.sql
	2) ConvPbsa.sql
	3) Convpcdb.sql
	4) ConvPlegl.sql
	5) About.htm
	6) Metavante.Prime.BSADatabaseLayer.txt	
	7) pda.txt
	8) CMBrowser.txt
	9) CTR03292012.pdf
       10) Batchfilter.txt
       11) Interop.PBSAObj.txt
       12) Pbsaobj.txt
       13) Readme.txt
       14) LegalObj.txt
       15) Interop.LegalObj.txt
       16) RequestBatch.txt
       17) ImportLegalRequests.txt
       18) PrimeDiag.txt
       19) CompMgrUtils.txt
       20) DependantDataImp.txt
       21) AcctImport.txt
       22) DataLoad.txt
       23) CompEndOfDay.txt
       24) RelationshipType.aspx
       25) Relationship Type Import Specifications.doc
       26) BSA Pre Installation Checklist.docx
       27) BSA Pre Installation Checklist.pdf
       28) OFAC  Pre Installation Checklist.docx
       29) OFAC  Pre Installation Checklist.pdf
       30) PCS Version 10 Release Notes.pdf
       31) Prime Server Installation Procedure.pdf 
       32) Prime Server Upgrade Procedure.pdf 


APPLICATION SERVER:
-------------------

	1. Stop all Prime services and IIS.

        2. Unregister PbsaObj.dll by running the following at a command prompt:

		"Regsvr32 /u %PRIME%\exe\PbsaObj.dll"

        3. Unregister LegalObj.dll by running the following at a command prompt:

		"Regsvr32 /u %PRIME%\exe\LegalObj.dll"

	4. Backup the following files from the path %PRIME%\Exe\
	   where %PRIME% refers to the PRIME installation folder

		a) Metavante.Prime.BSADatabaseLayer.dll	
		b) BatchFilter.exe
		C) Interop.PBSAObj.dll
		d) Pbsaobj.dll
		e) Pda.exe
		f) Interop.Legalobj.dll
		g) LegalObj.dll	
                h) RequestBatch.exe
                i) ImportLegalRequests.exe
                j) PrimeDiag.exe
                k) CompMgrUtils.exe
                l) DependantDataImp.exe
		m) AcctImport.exe
	        n) DataLoad.exe
		o) CompEndOfDay.exe
			
	      Note:

		Make backup by copying original files to another folder, created
		specially for backup purposes outside of the installation folder.
		Do not backup by renaming original files and leaving them in the
		installation folder. 

	5. Backup the following files from the path %PRIME%\Services\BSAReporter\BSAWebService\bin
	   where %PRIME% refers to the PRIME installation folder

		a) Metavante.Prime.BSADatabaseLayer.dll

	6. Backup the following files from the %PRIME%\ComplianceManager\bin\ folder to another backup folder 
	   where %PRIME% refers to the PRIME installation folder
		
                 a) CMBrowser.dll 
		 b) Interop.PBSAObj.dll	
		 c) Interop.Legalobj.dll
		 d) Metavante.Prime.BSADatabaseLayer.dll

	7. Backup the following file from the %PRIME%\ComplianceManager\pdf\ folder to another backup folder 
	   where %PRIME% refers to the PRIME installation folder
		
                    CTR03292012.pdf
	
	8. Backup the following file from the %PRIME%\ComplianceManager\ folder to another backup folder 
	   where %PRIME% refers to the PRIME installation folder
  		     About.htm
                     Web.config
                     RelationshipType.aspx

	9. Backup the following file from %PRIME%\Documentation\Knowledge Base Articles\BSA folder to backup folder
	
		Relationship Type Import Specifications.doc

	10. Backup the following file from %PRIME%\Documentation\Installation Documentation folder to backup folder
	
		 a) BSA Pre Installation Checklist.docx
       		 b) OFAC  Pre Installation Checklist.docx
       		 c) Prime Server Installation Procedure.pdf 
	         d) Prime Server Upgrade Procedure.pdf 
		 e) BSA Pre Installation Checklist.pdf
		 f) OFAC  Pre Installation Checklist.pdf     
        
 	11. Backup the following file from %PRIME%\Documentation\Release Notes folder to backup folder
	
		 PCS Version 10 Release Notes.pdf
	
	12. Rename the extension of the following extracted files from the patch

		a) Rename  Metavante.Prime.BSADatabaseLayer.txt to Metavante.Prime.BSADatabaseLayer.dll	
		b) Rename  CMBrowser.txt to CMBrowser.dll
		c) Rename  Interop.PBSAobj.txt to Interop.PBSAobj.dll
		d) Rename  Interop.Legalobj.txt to Interop.Legalobj.dll	
		e) Rename  pda.txt to pda.exe
		f) Rename  Batchfilter.txt to Batchfilter.exe
		g) Rename  Pbsaobj.txt to Pbsaobj.dll
		h) Rename  Legalobj.txt to Legalobj.dll
                i) Rename  RequestBatch.txt to RequestBatch.exe
                j) Rename  ImportLegalRequests.txt to ImportLegalRequests.exe
                k) Rename  PrimeDiag.txt to PrimeDiag.exe
                l) Rename  CompMgrUtils.txt to CompMgrUtils.exe
                m) Rename  DependantDataImp.txt to DependantDataImp.exe
		n) Rename  AcctImport.txt to AcctImport.exe
                o) Rename  DataLoad.txt to DataLoad.exe
		p) Rename  CompEndOfDay.txt to CompEndOfDay.exe
		

	13. Copy the following renamed files to %PRIME%\Exe\ folder

		a) Metavante.Prime.BSADatabaseLayer.dll	
		b) Batchfilter.exe
		c) pda.exe
		d) Legalobj.dll
		e) Pbsaobj.dll
		f) Interop.Legalobj.dll	
		g) Interop.PBSAObj.dll
                h) RequestBatch.exe
                i) ImportLegalRequests.exe
                j) PrimeDiag.exe
                k) CompMgrUtils.exe
                l) DependantDataImp.exe
		m) AcctImport.exe
	        n) DataLoad.exe
		o) CompEndOfDay.exe

	14. Copy the following renamed files to %PRIME%\Services\BSAReporter\BSAWebService\bin folder

		a) Metavante.Prime.BSADatabaseLayer.dll

	15. Copy the below renamed files to %PRIME%\ComplianceManager\bin\ folder

		a) CMBrowser.dll
		b) Interop.PBSAObj.dll	
		c) Interop.Legalobj.dll
		d) Metavante.Prime.BSADatabaseLayer.dll
 
	16. Copy the following file from patch to %PRIME%\ComplianceManager folder

		a) About.htm
                b) RelationshipType.aspx

	17. Copy the following file from patch to %PRIME%\ComplianceManager\pdf folder

		 CTR03292012.pdf

        18. Copy the following file from Patch to %PRIME%\Documentation\Knowledge Base Articles\BSA
	
		 Relationship Type Import Specifications.doc

	19. Copy the following file from Patch to %PRIME%\Documentation\Installation Documentation
	
		 a) BSA Pre Installation Checklist.docx
       		 b) OFAC Pre Installation Checklist.docx
       		 c) Prime Server Installation Procedure.pdf 
	         d) Prime Server Upgrade Procedure.pdf
		 e) BSA Pre Installation Checklist.pdf
       		 f) OFAC Pre Installation Checklist.pdf       
        
 	20. Copy the following file from Patch to %PRIME%\Documentation\Release Notes
	
		 PCS Version 10 Release Notes.pdf

	21. Find file Web.Config in the %PRIME%\ComplianceManager folder and make the following 
            two modifications:
	    
            Change this line:
            <add key="DefaultSupportEmailAddress" value="PrimeSupport@fisglobal.com"/> 
            
            To this line:
            <add key="DefaultSupportEmailAddress" value="fcs.support@fisglobal.com"/>		

            Change this line:
            <add key="DefaultSupportTelephoneNumber" value="800-207-2742 (when prompted press option 1 then 4)"/>  
            
            To this line:
            <add key="DefaultSupportTelephoneNumber" value="1.866.275.6868 (Option 2-5)"/>

	22. Register the new PbsaObj.dll by running the following at a command prompt:
	
		"Regsvr32 %PRIME%\exe\PbsaObj.dll"

	23. Register the new LegalObj.dll by running the following at a command prompt:
	
		"Regsvr32 %PRIME%\exe\LegalObj.dll"

			
	24. Restart all Prime services and IIS	


DATABASE SERVER:
----------------
	*Execute the following for all tenants:

	1. Execute ConvOfac.sql script submitted with this patch
 
	2. Execute ConvPbsa.sql script submitted with this patch
         
	3. Execute ConvPcdb.sql script submitted with this patch
	
	4. Execute ConvPLegl.sql script submitted with this patch
	
	5. Execute SQLSecurity.exe for OFAC,PBSA,Plegl and Pcdb database

