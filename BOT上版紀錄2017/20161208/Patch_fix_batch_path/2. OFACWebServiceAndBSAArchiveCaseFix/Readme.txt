|-----------------------------------------------------------------------------------------|
|				       :: INSTRUCTIONS ::                                 |   
|     				  				                          |
|-----------------------------------------------------------------------------------------|


Purpose of the Patch: 
---------------------
 
This patch address the following issues:

	1. R141500565 - OFAC MustMatchType was not working correctly in some situations
                        when using OFAC web service. It was producing some matches 
                        in error.
 
	2. R141403876 - Suspicious Activity Archive Case Report was showing no data. 
	


Description of changes:
----------------------

	1. OFAC MustMatchType was not working correctly in some situations when using 
	   OFAC web service. MustMatchType Flag value has been set and properly passed 
           for matching to produce valid matches on MustMatchType.


	2. Modified SAArchiveCaseReport.rpt to point to the correct 
	   CRY_SAArchCaseReport_Main stored procedure to display BSA Archived Cases.
 
Note: This patch should be installed and tested in a Test environment before it is deployed 
      to a Production environment.


Patch Name : 2 OFACWebServiceAndBSAArchiveCaseFix
Patch Release Date: 10/30/2014
Version Applicable To: 10.0.0
Patch Pre-requisites: Make sure the previous patch mentioned below are installed

	1 CumulativeEnhancementFixes



INSTRUCTIONS:
-------------

      Download the attached "2 OFACWebServiceAndBSAArchiveCaseFix.zip" file, and extract the contents 
      to a temporary folder.

      The following files will be extracted:

	1) SAArchiveCaseReport.rpt
	2) About.htm
	3) Readme.txt
        4) OFACReporterWS.txt

APPLICATION SERVER:
-------------------

	1. Stop OFAC Reporter Remoting service and IIS.

	2. Backup OFACReporterWS.dll from the x:\Prime\Services\OFACReporter\OFACSearchWebService\bin folder 
	   to another folder where x: is the drive where Prime is installed.

		Note: X is the prime installation directory. 
		Installation path could be different from "x:\Prime". 

	3. Rename OFACReporterWS.txt to OFACReporterWS.dll

	4. Copy the renamed file OFACReporterWS.dll to x:\Prime\Services\OFACReporter\OFACSearchWebService\bin folder 	
	  
	5. Back up the file SAArchiveCaseReport.rpt from the x:\prime\reports folder (if they exist) 	

	6. Copy the file SAArchiveCaseReport.rpt to the x:\prime\reports folder
	
	7. Backup the file About.htm from the x:\Prime\ComplianceManager\ folder to another backup folder 
	   
  	8. Copy the file About.htm from patch to x:\Prime\ComplianceManager folder

	9. Start OFAC Reporter Remoting service and IIS.

	

DATABASE SERVER:
----------------

	NONE
		