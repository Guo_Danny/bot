/*
**  File Name:		ConvPbsa.sql
**
**  Functional Description:
**
**  This module contains SQL patches for version 10.0.0
**
**  Facility	    The Prime Central Database
**  Creation Date:  07/22/2014
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2009 - 2011 											 ***
*** FIS                                                ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in FIS.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                   
				       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|----------------------------------------------------
07/22/2014		SS		Modified proc BSA_GenerateRFData, BSA_GetPrimaryRelations, 
						BSA_InsRelationshipType, BSA_UpdRelationshipType to introduce IncludeInRisk field.
07/22/2014		SM		R130547287: Fixed the issue in creating alerts based upon the activity types 
						in the rule USR_RepetitiveOverXinZDay
07/22/2014		SM		R140250508: Modified BSA_ActivityBranchesForCase to improve performance when loading SAR/CTR pdf.
07/22/2014		SM		R140068952: Modified Cry_rulelist to handle params with more than 8000 characters and with extended ascii characters.
07/22/2014		SM		R140888993: Fixed the issue of duplicate alerts created in the rule USR_SameByOrderBeneAndSameAmt.
08/18/2014		RT		R141171562: Modified BSA_DelAccountOwner stored procedure to handle when relationship code and name are different.						
12/01/2014		SM		I140733893: Added the correct SP in the update script for EDD_RiskModel.rpt
01/20/2015		SM 		I150008662: Rulelist reports did not display correctly when User defined rules are created through Rule Builder.
01/29/2015		SM		Defect 4890:Include In Risk Analysis field of Relationship type screen, should not default the value to 1,
									if the record has modifications in IncludeInRisk field.

*/
					 
USE PBSA
GO


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print '  Starting Conversion of Pbsa Objects To Version 10.0.0'
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

Set ANSI_NULLS ON
Go

Set NOCOUNT ON 
GO

If Not Exists(select 1 from sys.columns where name = 'IncludeInRisk')
Begin
	Alter Table RelationshipType Add IncludeInRisk Bit Not Null Default(1)
	Update RelationshipType Set IncludeInRisk = 1, LastOper='Prime'
End
GO

If Exists(Select * From sysobjects 
          where id = object_id(N'[dbo].[BSA_InsRelationshipType]') 
              and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
    Drop Procedure dbo.BSA_InsRelationshipType
Go

Create PROCEDURE dbo.BSA_InsRelationshipType(
			 @Code		SCode
			,@Name		LongName
			,@PrimaryRelation		smallint
			,@Score							int
			,@SPName						varchar(255)
			,@EnabledForAutoDiscovery	bit
			,@Source						int
			,@IncludeInRiskAnalysis	bit
			,@Oper		SCode
		)	WITH ENCRYPTION AS
	-- Start standard stored procedure transaction header
	DECLARE @trnCnt int
	SELECT @trnCnt = @@trancount	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN BSA_InsRelationshipType
	ELSE
		-- Already in a transaction
 	SAVE TRAN BSA_InsRelationshipType
	-- End standard stored procedure transaction header
	DECLARE @stat int
	INSERT INTO RelationshipType( Code,Name,PrimaryRelation, Score, SPName, EnabledForAutoDiscovery, Source, IncludeInRisk,
						CreateOper,CreateDate) VALUES (
		 @Code,@Name,@PrimaryRelation, @Score, @SPName, @EnabledForAutoDiscovery, @Source, @IncludeInRiskAnalysis, RTrim(@oper), GetDate())
	SELECT @stat = @@error
	-- Evaluate results of the transaction
	IF @stat <> 0
	BEGIN
		ROLLBACK TRAN BSA_InsRelationshipType
		RETURN @stat
	END
	IF @trnCnt = 0
		COMMIT  TRAN BSA_InsRelationshipType
	RETURN @stat
GO

If Exists(Select * From sysobjects 
          where id = object_id(N'[dbo].[BSA_UpdRelationshipType]') 
              and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
    Drop Procedure dbo.BSA_UpdRelationshipType
Go

Create PROCEDURE dbo.BSA_UpdRelationshipType(
			 @Code		SCode
			,@Name		LongName
			,@PrimaryRelation		smallint
			,@Score							int
			,@SPName						varchar(255)
			,@EnabledForAutoDiscovery	bit
			,@Source						int
			,@IncludeInRiskAnalysis	bit
			,@Oper		SCode
			,@ts		timestamp
		) 	WITH ENCRYPTION AS
	-- Start standard stored procedure transaction header
	DECLARE @trnCnt int
	SELECT @trnCnt = @@trancount	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN BSA_UpdRelationshipType
	ELSE
		-- Already in a transaction
 	SAVE TRAN BSA_UpdRelationshipType
	-- End standard stored procedure transaction header
	DECLARE @stat int
	DECLARE @cnt int
	UPDATE RelationshipType
		SET 
			 Name = @Name
			,PrimaryRelation = @PrimaryRelation
			,LastOper =  RTrim(@oper)
			,Score		= @Score
			,SPName		= @SPName
			,EnabledForAutoDiscovery	= @EnabledForAutoDiscovery
			,Source						= @Source
			,IncludeInRisk	= @IncludeInRiskAnalysis
		WHERE Code = @Code AND Ts = @ts
	SELECT @stat = @@error, @cnt = @@rowcount
	-- Evaluate results of the transaction
	IF ( @stat = 0 and @cnt = 0 )
		SELECT @stat = 250001	-- Concurrent Update
	IF ( @stat <> 0 )
	BEGIN
		ROLLBACK TRAN BSA_UpdRelationshipType
		RETURN @stat
	END
	IF @trnCnt = 0
		COMMIT TRAN BSA_UpdRelationshipType
	RETURN @stat
GO

If Exists(Select * From sysobjects 
          where id = object_id(N'[dbo].[BSA_GetPrimaryRelations]') 
              and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
    Drop Procedure dbo.BSA_GetPrimaryRelations
Go

CREATE PROCEDURE dbo.BSA_GetPrimaryRelations 
 	WITH ENCRYPTION AS
	
	SELECT [Code], [Name], [CreateOper], [CreateDate], [LastOper], 
		[LastModify], [TS], [PrimaryRelation] ,Score, SPName, 
		EnabledForAutoDiscovery, IncludeInRisk, Source,LastProcessTime 
		FROM RelationshipType 
		WHERE PrimaryRelation = 1

GO

If Exists(Select * From sysobjects 
          where id = object_id(N'[dbo].[BSA_GenerateRFData]') 
              and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
    Drop Procedure dbo.BSA_GenerateRFData
Go

Create Procedure dbo.BSA_GenerateRFData( @filter Int, @filterData varchar(50), 
					@aggType Int, @Oper Scode, 
					@custs Int Output, @recs BigInt Output, @errs Int Output)
As
Begin
	Set Nocount On
	Declare			
		@sSql nVarChar(4000),
		@stat int,
		@txt varchar(200),
		@RFICode Varchar(35),
		@MinRFICode varchar(35),
		@CalcItemID Int,
		@Cust Varchar(35),
		@MinCust Varchar(35),
		@Score Int,
		@sp Varchar(50),
		@Val Varchar(1000),
		@optAutoAcceptSame bit,
		@custsAccepted Int,
		@custsSkipped INT, 
		@errsAccepted Int

	-- to perform in batches of 100,000 records (Cust + RFTCode + RFICode Combination
	Declare @LastCust varchar(35)
	Declare @LastRFTCode Varchar(11)
	Declare @LastRFICode Varchar(11) 
	Declare @RecExists Bit 
	Declare @RowsSel Int
	Declare @MinCalcItemID Int

	-- to store the list of customers to be processed
	Create Table #CustList
	(
		Cust varchar(35), 
		RFTCode VARCHAR(11), 
		RFICode VARCHAR(11), 
		Required Bit,
		Weight Int,
		DefScore Int,
		Source Int,
		SourceTable Varchar(50),
		SourceField Varchar(50),
		CalcItemID Int,
		OverRideITem Bit,
		CalculationType Char(3),
		Value Varchar(1000) Null,
		ScoreType Int Null,
		Score Decimal(18,2) Null
	)
	
	-- 2008 START  Recommended change
	Create Nonclustered Index IdxCustList on #CustList (cust, RFiCOde)
	-- 2008 END  Recommended change

	-- To store the values for each customer 
	Create Table #CustVal
	(
		Cust Varchar(35),
		RFICode Varchar(11),
		Value Varchar(1000),
		CalculationType Char(3) Null
	)

	Set @LastCust = ''
	Set @LastRFTCode = ''
	Set @LastRFICode = '' 
	Set @Errs = 0
	Set @custs = 0
	Set @recs = 0
	Set @recExists = 1
	Set @RowsSel = 0
	--Filter Types
	-- 0 ALL
	-- 1 For Customer Type
	-- 2 For Given Customer
	-- 3 For All New Customers or Recently modified customers
	-- 4 For All Customers created within a date range

	select @txt = 'Risk scores generation started on ' + CONVERT(VARCHAR, GETDATE())

	--With filter 3, If the review date is less than the create date, 
	--that means that items have been generated after model has been accepted.
	--if modify date is greater than the latest create date, that means a customer has
	--been modified after creation of items, so generate new items
	--If the lastreview is greater than the create, that means that no items
	--have been generated after the acceptance of the last risk.  If the lastmodify
	-- date is greater than the last review, that means the customer has been
	-- modified, so generate new items.
	--The first section of the query retrieves the modified customers and the second section
	--retrieves the new customers.

	-- for testing
	Select @txt = 'Start Risk Scoring with @Filter = ' + cast(@Filter as varchar) + ' And @Filterdata = ' + @filterdata
	Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', @txt

	While @RecExists = 1
	Begin
		If @filter <> 3 
		BEGIN
			-- To retrieve all the customer records satisfying the 
			-- selected criterion
			If @filter = 2  Begin
				Insert #CustList
				(Cust, RFTCode, RFICode, Required, Weight,
				DefScore, Source, SourceTable, SourceField,
				CalcItemID, OverRideITem, CalculationType, scoreType)
				Select @filterData, rfl.RFTCode,
					rfi.Code, rfi.Required, rfi.Weight, 
					rfi.DefScore, rfi.Source, 
					rfi.SourceTable, rfi.SourceField, 
					rfi.CalcItemId, rfl.OverRideItem,
					rfi.CalculationType, 1 ScoreType
				From 
					(
					Select	
						[id], Type, CreateDate
					From
						dbo.Customer c (NOLOCK)
					Where
						[Id]= @filterData and Closed <> 1
					) c
				Inner Join
					dbo.CustomerType ct (NOLOCK)
				On
					ct.Code = c.Type
				Inner Join
					dbo.RiskFactorList rfl (NOLOCK)
				On
					rfl.RFTCode = ct.RFTCode
				Inner Join
					dbo.RiskFactorItem rfi (NOLOCK)
				On
					rfi.Code = rfl.RFICode
				Where 
					(c.[id] > @LastCust 
					Or
					(c.[id] = @LastCust And rfl.RFTCode> @LastRFTCode 
					And rfi.Code >= @LastRFICode)
					)
				Order By rfl.RFTCode, rfi.Code
				Select @RowsSel = @@RowCount
				Select @recExists = Case When @RowsSel = 0 then 0 Else 1 End
			End Else
			Begin
				Insert #CustList
				(Cust, RFTCode, RFICode, Required, Weight,
				DefScore, Source, SourceTable, SourceField,
				CalcItemID, OverRideITem, CalculationType, scoreType)
				Select Top 5000000
					c.[ID], rfl.RFTCode,
					rfi.Code, rfi.Required, rfi.Weight, 
					rfi.DefScore, rfi.Source, 
					rfi.SourceTable, rfi.SourceField, 
					rfi.CalcItemId, rfl.OverRideItem,
					rfi.CalculationType, 1 ScoreType
				From 
					(
					Select	
						[id], Type, CreateDate
					From
						dbo.Customer c (NOLOCK)
					Where
						RFCalculate = 1 and Closed <> 1
					) c
				Inner Join
					dbo.CustomerType ct (NOLOCK)
				On
					ct.Code = c.Type
				Inner Join
					dbo.RiskFactorList rfl (NOLOCK)
				On
					rfl.RFTCode = ct.RFTCode
				Inner Join
					dbo.RiskFactorItem rfi (NOLOCK)
				On
					rfi.Code = rfl.RFICode
				Where 
					(c.[id] > @LastCust 
					Or
					(c.[id] = @LastCust And rfl.RFTCode> @LastRFTCode 
					And rfi.Code >= @LastRFICode)
					)
					And
					1 = Case 
					When @filter = 0 Then 1	-- All customers						
					When @filter = 1 And c.Type = @filterData Then 1 -- For Customer Type
					When @filter = 2 And c.[ID] = @filterData Then 1 -- For One Customer	
					When @filter = 4 And
						c.CreateDate >= LEFT(@filterData, CharIndex( '~', @filterData)-1)  And
						c.CreateDate <= RIGHT(@filterData, Len(@filterData) - CharIndex( '~', @filterData))  
					Then 1	-- For date/time range
					Else 0
					End
				Order By c.ID, rfl.RFTCode, rfi.Code
				
				Select @RowsSel = @@RowCount
				Select @recExists = Case When @RowsSel = 0 then 0 Else 1 End
			End
		End
		Else
		Begin
			-- select the new and recently modified customers
			Insert #CustList
			(Cust, RFTCode, RFICode, Required, 
			Weight,DefScore, Source, 
			SourceTable, SourceField, CalcItemID, 
			OverRideItem, CalculationType,ScoreType)
			Select Top 5000000 * 
			From
			(
			Select 
				c.[ID], rfl.RFTCode,rfi.Code, rfi.Required, 
				rfi.Weight, rfi.DefScore, rfi.Source, 
				rfi.SourceTable, rfi.SourceField, rfi.CalcItemId, 
				rfl.OverRideItem, rfi.CalculationType, 1 ScoreType
			From 
				(
				Select
					[id],
					Type,
					LastModify
				From
					dbo.Customer  (noLock)
				Where
					RFCalculate = 1 and Closed <> 1
				) C
			Inner Join
				dbo.CustomerType ct (noLock)
			On
				ct.Code = c.Type
			Inner Join
				RiskFactorList rfl (noLock)
			On
				rfl.RFTCode = ct.RFTCode	
			Inner Join
				RiskFactorItem rfi (noLock)
			On
				rfi.Code = rfl.RFICode
			Inner Join
				(
				Select 
					Cust,
					Max(CreateDate) CreateDate,
					Max(Coalesce(LastReview, 0)) LastReview
				From
					dbo.RiskFactorData  (nolock)
				Group By Cust
				) r
			On
				r.Cust = c.[id]
			Where
			(
				(r.LastReview < r.CreateDate And r.CreateDate < c.LastModify)
			Or
				(r.LastReview > r.CreateDate And r.LastReview < c.LastModify)
			)
			
			Union All
			
			Select 
				c.[ID], rfl.RFTCode,
				rfi.Code, rfi.Required, rfi.Weight, 
				rfi.DefScore, rfi.Source, 
				rfi.SourceTable, rfi.SourceField, 
				rfi.CalcItemId, rfl.OverRideItem,
				rfi.CalculationType, 1
			From 
				(
				Select
					[id],
					Type
				From
					dbo.Customer  (noLock)
				Where
					RFCalculate = 1 and Closed <> 1
				And
					[id] Not In
					(Select cust From RiskFactorData (nolock))
				) C
			Inner Join
				dbo.CustomerType ct (noLock)
			On
				ct.Code = c.Type
			Inner Join
				dbo.RiskFactorList rfl (noLock)
			On
				rfl.RFTCode = ct.RFTCode
			Inner Join
				dbo.RiskFactorItem rfi (noLock)
			On
				rfi.Code = rfl.RFICode
			) CustList
			Where
			(	CustList.[id] > @LastCust
			Or
				(CustList.[id] = @lastCust And CustList.RFTCode >= @LastRFTCode
				And CustList.Code > @LastRFICode)
			)
			Order by CustList.[id], CustList.RFTCode, CustList.Code

			Select @RowsSel = @@RowCount
			Select @recExists = Case When @RowsSel = 0 then 0 Else 1 End
	
		End

		If @RecExists = 0 
			Goto CompleteScoring

		Select @Lastcust = Max(Cust), @LastRFTCode = Max(RFTCode), @LastRFICode = Max(RFICode)
		From #custList

		-- for testing
		Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', 'Extracted the Customer list for Processing'
	
		-- to find the value for the customers for Source = 0
		Select @RFICode=MIN(RFICode) FROM #CustList Where Source = 0
		
		Set @MinRFICode=@RFICode
		While @MinRFICode IS NOT NULL
		Begin
			Set @ssql = ''
			Select @ssql =  
			Case When SourceTable = 'Customer' Then
				'Update #CustList
				Set Value = Cast(Customer.' + CustList.SourceField + ' as varchar(1000)), Score = RiskFactorValue.Score
				From Pbsa.dbo.Customer (nolock)
				Inner Join #CustList CustList (nolock) On Customer.[id] = CustList.Cust
				Left Outer Join Pbsa.dbo.RiskFactorValue (nolock) On RiskFactorValue.RFICode = CustList.RFICode 
				And 1 = 
					Case when RiskFactorValue.Score Is Not Null and 
							RiskFactorValue.Value = Cast(Customer.' + CustList.SourceField + ' As Varchar(2000)) Then  1
					Else
						Case When RiskFactorValue.Value in (''0'',''1'',''False'',''True'') Then
							Case When RiskFactorValue.Value = Cast(Customer.' +  CustList.SourceField + ' as varchar(2000)) Then 1
							When Convert(VarChar, Customer.' +  CustList.SourceField + ') = ''0'' And RiskFactorValue.Value = ''False'' Then 1			
							When Convert(VarChar, Customer.' +  CustList.SourceField + ') = ''1'' And RiskFactorValue.Value = ''True'' Then 1
							Else 0
							End 
						End 
					End 
				Where Source = 0 And CustList.SourceTable = ''Customer''
				 And  CustList.RFICode = ''' + @MinRFICode + '''' 
			When SourceTable = 'KYCData' Then
				'Update #CustList
				Set Value = Cast(KYCData.' + CustList.SourceField + ' as varchar(1000)), Score = RiskFactorValue.Score
				From Pbsa.dbo.KYCData (nolock)
				Inner Join #CustList CustList (nolock) On KYCData.customerid = CustList.Cust
				Left Outer Join Pbsa.dbo.RiskFactorValue (nolock) On RiskFactorValue.RFICode = CustList.RFICode 
				And 1 = 
					Case when RiskFactorValue.Score Is Not Null and 
							RiskFactorValue.Value = Cast(KYCData.' + CustList.SourceField + ' As Varchar(2000)) Then  1
					Else
						Case When RiskFactorValue.Value in (''0'',''1'',''False'',''True'') Then
							Case When RiskFactorValue.Value = Cast(KYCData.' +  CustList.SourceField + ' as varchar(2000)) Then 1
							When Convert(VarChar, KYCData.' +  CustList.SourceField + ') = ''0'' And RiskFactorValue.Value = ''False'' Then 1			
							When Convert(VarChar, KYCData.' +  CustList.SourceField + ') = ''1'' And RiskFactorValue.Value = ''True'' Then 1
							Else 0
							End 
						End 
					End 
				Where Source = 0 And CustList.SourceTable = ''KYCData''
				 And  CustList.RFICode = ''' + @MinRFICode + '''' 				
			When SourceTable = 'AccountType' Then
				'Insert #CustVal
				Select AccountOwner.Cust, CustList.RFICode, AccountType.' + CustList.SourceField + ', CustList.CalculationType From
					Pbsa.dbo.AccountOwner (nolock) Inner Join Pbsa.dbo.Account (nolock) On Account.ID = AccountOwner.Account  and Account.Closed <> 1
				Inner Join Pbsa.dbo.AccountType On AccountType.Code = Account.Type
				Inner Join Pbsa.dbo.RelationshipType on RelationshipType.Code = AccountOwner.Relationship
				Inner Join #CustList CustList (nolock) On AccountOwner.Cust = CustList.Cust
				Where CustList.Source = 0 And RelationshipType.IncludeInRisk = 1 And SourceTable = ''AccountType''
				And CustList.RFICode = ''' + @MinRFICode + ''''
			End
			from 
				#CustList CustList (nolock)
			Where 
				Source = 0 
			And
				RFICode = @MinRFICode
	
			Exec sp_Executesql @ssql
	
			Set @stat = @@Error
			If @stat <> 0 Goto ErrHandler
	
			goto GetNext
	
	ErrHandler:
			Select @errs = @errs + 1
			Select @txt = 'Error_GenerateRFData: #' + cast(@stat as varchar) + 
				' while Generating RF Data record for ' + 
				'Customer: ' + @cust + 
				', RFItem: ' + @MinRFICode 
			Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', @txt
	
	GetNext:
		-- for testing
		Set @txt = 'Processed RFICode ' + @MinRFICode 
		Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', @txt
			Select @RFICode=MIN(RFICode) FROM #CustList (nolock) WHERE 
				 Source = 0 And RFICode > @MinRFICode
			Set @MinRFICode = @RFICode
		END
	
		-- for testing
		Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', 'Populated CustVal'
	
		-- update the scores for the customers (source - 0 for aaccounttype risk items)
		Update #CustList
		Set Value = CustVal.Value, 
			Score =  CustVal.Score		
		From 
			#CustList CustList (nolock)
		Inner Join
			(
			Select Val.Cust, Val.RFICode,
				Case 
				When Val.CalculationType = 'Avg' Then
					Avg(RiskFactorValue.Score)
				When Val.CalculationType = 'Min' Then
					Min(RiskFactorValue.Score)
				When Val.CalculationType = 'Max' Then
					Max(RiskFactorValue.Score)
				When Val.CalculationType = 'Sum' Then
					Sum(RiskFactorValue.Score)
				End Score,
				Case When count(*) > 1 Then
					'Multivalued'
				Else
					Min(RiskFactorValue.Value)
				End Value
			From	
				#CustVal Val (nolock)
			Inner Join
				dbo.RiskFactorValue (nolock)
			On
				RiskFactorValue.RFICode = Val.RFICode
			And 
				RiskFactorValue.Value = Val.Value
			Group By Val.Cust, Val.RFICode, Val.CalculationType
			) CustVal
		On
			CustList.Cust = CustVal.Cust 
		And
			CustList.RFICode = CustVal.RFICode
		Where
			CustList.Source = 0
		And
			CustList.SourceTable = 'AccountType'
	
		-- for testing
		Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', 'Filled up values and scores for Source = 0'
		

				
	            -- loop through calculated items
		    Declare CalcItemCursor Cursor 
		    FAST_FORWARD
		    For 
		      Select distinct RFICOde, CalcItemId  From #CustList (nolock) where source =1
	
		    Declare @spParamCnt int
	
		    Open CalcItemCursor
		    Fetch Next From CalcItemCursor into @RFICode, @calcItemId
		    While (@@FETCH_STATUS <> -1) Begin
			Select 
				@sp = ProcName
			From 
				RFCalculatedItem
			Where 
				Id = @calcItemId
			
			Select @spParamCnt = 0
			select @spParamCnt = count(*)  from sysobjects o, syscolumns c
				where o.xtype='P' and o.id = c.id 
					and o.name = @sp
			
			if @spParamCnt = 1 Begin
				-- for each item call exec
				Execute @sp @RFICode
			End Else 
			Begin
				-- For existing calculated items with 3 parameters
				-- call the stored procedure for each customer
				Select @Cust=MIN(Cust) FROM #CustList (nolock) Where Source = 1
					and RFICode = @RFICode
				Set @MinCust = @Cust
				While @MinCust IS NOT NULL
				Begin
					Execute @sp @Cust, @val Output, @Score Output
					Select @stat = @@Error
					If @stat <> 0 Goto HandleErr
	
					goto GetNextCust
HandleErr:
					Select @errs = @errs + 1
					Select @txt = 'Error_GenerateRFData: #' + cast(@stat as varchar) + 
						' while Generating RF Data record for ' + 
						'Customer: ' + @Mincust + 
						', Calculated Item ID: ' + cast(@calcItemId as varchar)
					Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', @txt
	
GetNextCust:
					-- update the Score and value for source = 1
					Update #CustList
						Set Score = @Score, Value = @Val
					Where Cust = @MinCust
						And  CalcItemID = @calcItemId
					And Source = 1 and RFICode = @RFICode
	
					Select @Cust=MIN(Cust) FROM #CustList WHERE 
					 Source = 1 And (Cust > @MinCust) and RFICode = @RFICode
					Set @MinCust = @Cust
				END
			End
		
			Fetch Next From CalcItemCursor into @RFICode, @calcItemId	
		    End
		    Close CalcItemCursor
		    Deallocate CalcItemCursor


		-- in calc item, update custlist with value and score based on calcitemid
		
	
		-- for testing
		Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', 'Processed calculated items'
	
		-- set the score and scoretype for Source = 2
		Update #CustList
		Set 
			Score = IsNull(DefScore,0),
			ScoreType = Case When DefScore Is Null Then 0 Else 2 End,
			Value = Null
		Where
			Source = 2
		
		--  If Score is not present, check if the default score exists. 
		-- If default score exists, set the Score to default score
		Update #CustList
		Set 
			Score = IsNull(DefScore,0),
			ScoreType = Case When DefScore Is Null Then 0 Else 2 End
		Where
			Score Is Null
	
	
		-- delete those records with OverrideItem = 1 and without a score
		Delete #CustList
		Where OverrideItem = 1 And Score <= 0
	
		-- Records with Overrideitem overrides the other records
		-- for a customer. So delete those records with Override = 0
		-- if the customer has any other record with override = 1
	
		Delete  #CustList
		From
			#CustList (nolock)
		Inner Join
			#CustList OverRideRisk (nolock)
		On
			#CustList.Cust =  OverRideRisk.Cust
		And
			#CustList.RFTCode = OverRideRisk.RFTCode
		Where 
			#CustList.OverRideItem = 0
		And 
			OverRideRisk.OverrideItem = 1
	
		-- for testing
		Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', 'completed the modifications to data in temp table'
		
		-- Delete existing records from RiskFactorData for the 
		-- Customers under consideration
		DELETE dbo.RiskFactorData 
		From 
			dbo.RiskFactorData (nolock)
		Inner join
			#CustList custList
		On
			RiskFactorData.Cust = CustList.Cust
		WHERE (RiskFactorData.Status is Null 
		Or RiskFactorData.Status = 0)
	
	
		-- insert the values to RiskFactorData
		INSERT INTO dbo.RiskFactorData (Cust, RFTCode, RFICode, 
			[Value], SCore, ScoreType, CreateOper, CreateDate)
		SELECT Cust, RFTCode, RFICode, [Value], SCore, ScoreType, 
			@Oper, GetDate()
		FROM 
			#CustList (nolock)
	
		Select @recs = @@ROWCOUNT + @recs

        ------------------------------------------------------
		--Add proposed risk scores to the
		-- Risk Score History table
		------------------------------------------------------
		
		--Clear pending previously proposed scores
		delete from RiskScoreHist
		where DateAccepted is null and
			  CustomerId in (select distinct cust
							 From #CustList (nolock))

		--Get EDD Option, Maximum number of
		-- Risk Score History records per customer
		Declare @MaxHistRecords int
		select @MaxHistRecords = Convert(int, dbo.BSA_fnGetEDDOption('MaxRSHist'))


		--Clear old history scores, for customers where
		--MaxRSHist will be exceeded
		delete from [RiskScoreHist]
		where ScoreId in 
		(Select ScoreID  FROM [RiskScoreHist] AllHist
		inner join
			(SELECT [CustomerId],      
			min([CreateDate]) OldestCreateDate   
			FROM [PBSA].[dbo].[RiskScoreHist]
			where CustomerId in (select distinct cust
							 From #CustList (nolock))
			Group by CustomerId
			having count(*) >= @MaxHistRecords) OldHist
		on AllHist.CustomerId = OldHist.CustomerId and
		AllHist.CreateDate = OldHist.OldestCreateDate )


		--Insert new proposed scores
		insert into RiskScoreHist
			(CustomerID, RiskScore, RiskClass, 
			  CreateOper, CreateDate, AcceptOper, DateAccepted)
		SELECT [Cust], [Score], [ProposedRiskClass],	
				@Oper, GetDate(), NULL, NULL					
		FROM [CustProposedRiskClass]
		where Cust in (select distinct cust
							 From #CustList (nolock))
		
	
		Select @Custs = @custs + Count(Distinct Cust) From #CustList

		select @txt = 'Successfully completed Risk scores generation on ' + 
					CONVERT(VARCHAR, GETDATE()) + '.' + 
					  char(13) + char(10) +
					  '      Total Records: ' + Cast(@recs as varchar) +
					  char(13) + char(10) +
					  '  Total Customers: ' + Cast(@custs as varchar) +
					  char(13) + char(10) +
					  '         Total Errors: ' + Cast(@errs as varchar)
		Exec BSA_InsEvent @oper, 'Cre', 'RFData', '', @txt
		truncate table #CustList
		
		-- Autoaccept if riskclasses are unchanged
		Select @optAutoAcceptSame = enabled from OptionTbl where Code = 'RFAcptSame'
		If IsNull(@optAutoAcceptSame, 0) = 1
		 exec BSA_AcceptAllRFScores @oper, 1, @custsAccepted output, @custsSkipped output, @errsAccepted output
		
	End	

CompleteScoring:

	-- for testing
	Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', 'Completed Risk Score generation'
	Drop table #CustList
	Drop Table #CustVal

	Return 0
End
Go

If exists (Select * From sysobjects 
	Where id = object_id(N'BSA_RelationshipTypeUpd') and
	OBJECTPROPERTY(id, N'IsTrigger') = 1)
	Drop Trigger BSA_RelationshipTypeUpd
Go

Create TRIGGER BSA_RelationshipTypeUpd ON RelationshipType WITH ENCRYPTION FOR UPDATE AS
 
	IF @@rowcount > 0 
	BEGIN
		UPDATE RelationshipType SET LastModify = getdate()
			FROM Inserted I, RelationshipType R WHERE I.Code = R.Code
		INSERT INTO Event (TrnTime, Oper, Type, ObjectType, ObjectId , LogText, EvtDetail) 
		SELECT getDate(), I.LastOper, 'Mod', 'AcctRelType', convert(varchar, I.Code), '',
			 CASE
				WHEN ((I.Code IS NULL AND D.Code IS NOT NULL) OR
					(I.Code IS NOT NULL AND D.Code IS  NULL) OR
					(I.Code != D.Code))
				THEN '| Code OLD: '+ltrim(rtrim(isnull(convert(char,D.Code),''))) +
					' NEW: '+ltrim(rtrim(isnull(convert(char,I.Code),''))) 
				ELSE ''
			END 
			+CASE
				WHEN ((I.Name IS NULL AND D.Name IS NOT NULL) OR
					(I.Name IS NOT NULL AND D.Name IS  NULL) OR
					(I.Name != D.Name))
				THEN '| Name OLD: '+ltrim(rtrim(isnull(convert(char,D.Name),''))) +
					' NEW: '+ltrim(rtrim(isnull(convert(char,I.Name),''))) 
				ELSE ''
			END 
			+CASE
				WHEN ((I.CreateOper IS NULL AND D.CreateOper IS NOT NULL) OR
					(I.CreateOper IS NOT NULL AND D.CreateOper IS  NULL) OR
					(I.CreateOper != D.CreateOper))
				THEN '| CreateOper OLD: '+ltrim(rtrim(isnull(convert(char,D.CreateOper),''))) +
					' NEW: '+ltrim(rtrim(isnull(convert(char,I.CreateOper),''))) 
				ELSE ''
			END 
			+CASE
				WHEN ((I.CreateDate IS NULL AND D.CreateDate IS NOT NULL) OR
					(I.CreateDate IS NOT NULL AND D.CreateDate IS  NULL) OR
					(I.CreateDate != D.CreateDate))
				THEN '| CreateDate OLD: '+ltrim(rtrim(isnull(convert(char,D.CreateDate),''))) +
					' NEW: '+ltrim(rtrim(isnull(convert(char,I.CreateDate),''))) 
				ELSE ''
			END 
			+CASE
				WHEN ((I.LastOper IS NULL AND D.LastOper IS NOT NULL) OR
					(I.LastOper IS NOT NULL AND D.LastOper IS  NULL) OR
					(I.LastOper != D.LastOper))
				THEN '| LastOper OLD: '+ltrim(rtrim(isnull(convert(char,D.LastOper),''))) +
					' NEW: '+ltrim(rtrim(isnull(convert(char,I.LastOper),''))) 
				ELSE ''
			END 
			+CASE
				WHEN ((I.LastModify IS NULL AND D.LastModify IS NOT NULL) OR
					(I.LastModify IS NOT NULL AND D.LastModify IS  NULL) OR
					(I.LastModify != D.LastModify))
				THEN '| LastModify OLD: '+ltrim(rtrim(isnull(convert(char,D.LastModify),''))) +
					' NEW: '+ltrim(rtrim(isnull(convert(char,I.LastModify),''))) 
				ELSE ''
			END 
			+CASE
				WHEN ((I.PrimaryRelation IS NULL AND D.PrimaryRelation IS NOT NULL) OR
					(I.PrimaryRelation IS NOT NULL AND D.PrimaryRelation IS  NULL) OR
					(I.PrimaryRelation != D.PrimaryRelation))
				THEN '| PrimaryRelation OLD: '+ltrim(rtrim(isnull(convert(char,D.PrimaryRelation),''))) +
					' NEW: '+ltrim(rtrim(isnull(convert(char,I.PrimaryRelation),''))) 
				ELSE ''
			END 
			+CASE
				WHEN ((I.Score IS NULL AND D.Score IS NOT NULL) OR
					(I.Score IS NOT NULL AND D.Score IS  NULL) OR
					(I.Score != D.Score))
				THEN '| Score OLD: '+ltrim(rtrim(isnull(convert(char,D.Score),''))) +
					' NEW: '+ltrim(rtrim(isnull(convert(char,I.Score),''))) 
				ELSE ''
			END 
			+CASE
				WHEN ((I.SPName IS NULL AND D.SPName IS NOT NULL) OR
					(I.SPName IS NOT NULL AND D.SPName IS  NULL) OR
					(I.SPName != D.SPName))
				THEN '| SPName OLD: '+ltrim(rtrim(isnull(convert(char,D.SPName),''))) +
					' NEW: '+ltrim(rtrim(isnull(convert(char,I.SPName),''))) 
				ELSE ''
			END 
			+CASE
				WHEN ((I.EnabledForAutoDiscovery IS NULL AND D.EnabledForAutoDiscovery IS NOT NULL) OR
					(I.EnabledForAutoDiscovery IS NOT NULL AND D.EnabledForAutoDiscovery IS  NULL) OR
					(I.EnabledForAutoDiscovery != D.EnabledForAutoDiscovery))
				THEN '| EnabledForAutoDiscovery OLD: '+ltrim(rtrim(isnull(convert(char,D.EnabledForAutoDiscovery),''))) +
					' NEW: '+ltrim(rtrim(isnull(convert(char,I.EnabledForAutoDiscovery),''))) 
				ELSE ''
			END
			+CASE
				WHEN ((I.IncludeInRisk IS NULL AND D.IncludeInRisk IS NOT NULL) OR
					(I.IncludeInRisk IS NOT NULL AND D.IncludeInRisk IS  NULL) OR
					(I.IncludeInRisk != D.IncludeInRisk))
				THEN '| IncludeInRisk OLD: '+ltrim(rtrim(isnull(convert(char,D.IncludeInRisk),''))) +
					' NEW: '+ltrim(rtrim(isnull(convert(char,I.IncludeInRisk),''))) 
				ELSE ''
			END			
			+CASE
				WHEN ((I.Source IS NULL AND D.Source IS NOT NULL) OR
					(I.Source IS NOT NULL AND D.Source IS  NULL) OR
					(I.Source != D.Source))
				THEN '| Source OLD: '+ltrim(rtrim(isnull(convert(char,D.Source),''))) +
					' NEW: '+ltrim(rtrim(isnull(convert(char,I.Source),''))) 
				ELSE ''
			END 
			+' |'
		FROM Inserted I, Deleted D 
		WHERE I.Code = D.Code
	END
GO

If exists (Select * From sysobjects 
	Where id = object_id(N'BSA_RelationshipTypeDel') and
	OBJECTPROPERTY(id, N'IsTrigger') = 1)
	Drop Trigger BSA_RelationshipTypeDel
Go

Create TRIGGER BSA_RelationshipTypeDel ON RelationshipType WITH ENCRYPTION FOR DELETE AS
 
	IF @@rowcount > 0 
	BEGIN
		INSERT INTO Event (TrnTime, Oper, Type, ObjectType, ObjectId , LogText,EvtDetail) 
		SELECT getDate(), D.CreateOper, 'Del', 'AcctRelType', convert(varchar, Code), '' ,
			  '|Code : '+ltrim(rtrim(isnull(convert(char,D.Code),''))) 
			+ '|Name : '+ltrim(rtrim(isnull(convert(char,D.Name),''))) 
			+ '|PrimaryRelation : '+ltrim(rtrim(isnull(convert(char,D.PrimaryRelation),''))) 
			+ '|Score: '+ltrim(rtrim(isnull(convert(char,D.Score),''))) 
			+ '|SPName : '+ltrim(rtrim(isnull(convert(char,D.SPName),''))) 
			+ '|EnabledForAutoDiscovery : '+ltrim(rtrim(isnull(convert(char,D.EnabledForAutoDiscovery),''))) 
			+ '|IncludeInRisk : '+ltrim(rtrim(isnull(convert(char,D.IncludeInRisk),'')))
			+ '|Source : '+ltrim(rtrim(isnull(convert(char,D.Source),''))) 
			+' |'
		FROM Deleted D
	END
GO

If exists (Select * From sysobjects 
	Where id = object_id(N'BSA_RelationshipTypeIns') and
	OBJECTPROPERTY(id, N'IsTrigger') = 1)
	Drop Trigger BSA_RelationshipTypeIns
Go

Create TRIGGER BSA_RelationshipTypeIns ON RelationshipType WITH ENCRYPTION FOR INSERT AS
 
	IF @@rowcount > 0 
	BEGIN
		INSERT INTO Event (TrnTime, Oper, Type, ObjectType, ObjectId , LogText, EvtDetail) 
		SELECT getDate(), I.CreateOper, 'Cre', 'AcctRelType', convert(varchar, Code), '',
			  '|Code : '+ltrim(rtrim(isnull(convert(char,I.Code),''))) 
			+ '|Name : '+ltrim(rtrim(isnull(convert(char,I.Name),''))) 
			+ '|PrimaryRelation : '+ltrim(rtrim(isnull(convert(char,I.PrimaryRelation),''))) 
			+ '|Score: '+ltrim(rtrim(isnull(convert(char,I.Score),''))) 
			+ '|SPName : '+ltrim(rtrim(isnull(convert(char,I.SPName),''))) 
			+ '|EnabledForAutoDiscovery : '+ltrim(rtrim(isnull(convert(char,I.EnabledForAutoDiscovery),''))) 
			+ '|IncludeInRisk : '+ltrim(rtrim(isnull(convert(char,I.IncludeInRisk),''))) 
			+ '|Source : '+ltrim(rtrim(isnull(convert(char,I.Source),''))) 
			+' |'
		FROM Inserted I
	END
GO

if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_RepetitiveOverXinZDays]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.USR_RepetitiveOverXinZDays
Go
--premium payment is from cashier check or money order
Create PROCEDURE dbo.USR_RepetitiveOverXinZDays (@WLCode SCode, @testAlert INT, 
	@activityTypeList VARCHAR(8000),
	@riskClasses VARCHAR(8000),
	@CustTypeList VARCHAR(8000),
	@productTypes VARCHAR(8000),
	@minimumAmount MONEY, 
	@minimumCount INT, 
	@day INT, 
	@cashorNonCash INT,
    @receivePay SMALLINT)

AS

/* RULE AND PARAMETER DESCRIPTION
	Detects repetitive transactions of the specified activity types 
	exceeding a designated amount within a selected time frame.
	Designed to be run Pre-EOD.

	@activityType = valid activity types delimited by comma ex: 101,102
	@riskClasses = Comma separated list of risk classes ex: High,Medium
	@CustTypeList = Comma seperated list of cusotmer types 
	@productTypes = Comma separated list of account types
	@minimumAmount = minimum amount threshold
	@minimumCount = minimum count threshold
	@day = period threshold, measured in days
	@cashOrNonCashorAll = specify whether to consider cash transactions or 
			      non cash transactions or all transactions of the 
			      specified activity types
	@receivePay = specify whether its a pay or receieve transaction
*/

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@minDate INT,
	@CustomerExemptionsList VARCHAR(8000),
	@AccountExemptionsList VARCHAR(8000)
DECLARE @StartAlrtDate  DATETIME
DECLARE @EndAlrtDate    DATETIME


DECLARE	@TT TABLE (
	Account varchar(40),
	BookDate INT,
	RecvPay SMALLINT,
	TranNo	INT
)

-- Temporary table of Activity Types that have not been specified as Exempt
DECLARE @ActType TABLE (
	Type	INT
)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_RepetitiveOverXinZDays 
ELSE
	-- Already in a transaction
	SAVE TRAN USR_RepetitiveOverXinZDays 
/* End standard stored procedure transaction header */

/*  standard Rules Header */
	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @RuleStartDate DATETIME
	
	SELECT @description = [Desc], @WLTYPE = WLTYPE, @RuleStartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList (NOLOCK)
	WHERE WLCode = @WlCode

Declare @BaseCurr char(3)
select @BaseCurr = IsNULL(BaseCurr,'') from SysParam

SET @minDate = dbo.ConvertSqlDateToInt(
	DATEADD(d, -1 * ABS(@day), CONVERT(VARCHAR, @RuleStartDate)))

SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)
SELECT @riskClasses = dbo.BSA_fnListParams(@riskClasses)
SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)
SELECT @productTypes = dbo.BSA_fnListParams(@productTypes)

-- Get the list of exemptions that are exempted on customer from rules
SELECT @CustomerExemptionsList = dbo.BSA_GetExemptionCodes('Customer','Rule')
IF ltrim(rtrim(@CustomerExemptionsList)) = ''
	SET @CustomerExemptionsList = NULL

-- Get the list of exemptions that are exempted on account from rules
SELECT @AccountExemptionsList = dbo.BSA_GetExemptionCodes('Account','Rule')
IF ltrim(rtrim(@AccountExemptionsList)) = ''
	SET @AccountExemptionsList = NULL


IF @cashorNonCash < 0 or @cashorNonCash >1
	SET @cashorNonCash = NULL

IF @receivePay < 1 or @receivePay >2
	SET @receivePay = NULL
	
INSERT INTO @ActType
	SELECT 	Type  FROM vwRuleNonExmActType
	WHERE	(@ActivityTypeList IS NULL 
		OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

INSERT INTO @TT(Account, BookDate, RecvPay, TranNo)
SELECT a.Account, a.BookDate, a.RecvPay, a.TranNo
	FROM CurrentArchActivity a (NOLOCK) INNER JOIN 
	Customer c ON a.cust = c.id  INNER JOIN 
	Account ac ON a.account = ac.id
	INNER JOIN @ActType act ON A.Type = act.Type
	WHERE ((@productTypes IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, ac.Type) + ',',@productTypes) > 0) )
	AND ((@riskClasses IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, c.RiskClass) + ',',@riskClasses) > 0) )
	AND ((@CustTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, c.Type) + ',',@CustTypeList) > 0) )
	AND	(c.Exemptionstatus IS NULL OR @CustomerExemptionsList IS NULL OR
		CHARINDEX(',' + LTRIM(RTRIM(c.Exemptionstatus)) + ',', @CustomerExemptionsList ) = 0)
	AND	(ac.Exemptionstatus IS NULL OR @AccountExemptionsList IS NULL OR
		CHARINDEX(',' + LTRIM(RTRIM(ac.Exemptionstatus)) + ',', @AccountExemptionsList ) = 0)
	AND a.BaseAmt >= @minimumAmount
	AND a.recvPay = ISNULL(@receivePay,a.recvPay) AND a.bookdate >= @minDate
	AND a.CashTran = ISNULL(@cashorNonCash, a.CashTran)
	AND a.Account IS NOT NULL
	AND EXISTS (
		SELECT Account From CurrentArchActivity b 
			INNER JOIN @ActType act ON b.Type = act.Type
			WHERE 
				b.bookdate >= @minDate AND
				b.recvpay = a.recvpay AND
				b.CashTran = ISNULL(@cashorNonCash, b.CashTran) AND
				b.account = a.account AND
				b.BaseAmt>=@minimumAmount 				
			GROUP BY Account			
			HAVING Count(b.TranNo) >= @minimumCount
			)
	GROUP BY a.Account, a.BookDate, a.RecvPay, a.TranNo


IF @testAlert = 1 BEGIN
	SELECT @StartAlrtDate = GETDATE()
	INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT, IsTest) 
	SELECT @WLCODE, 'Account policy:  ''' + Account + ''' has repetitive payment over ' + 
		DBO.BSA_InternalizationMoneyToString (@minimumAmount) + Space(1) + @BaseCurr + ' in ' + CONVERT(VARCHAR, @day) + ' days. ' , 0, 
		GETDATE(), NULL, NULL, NULL, Account, 1
	FROM @TT 
	GROUP BY Account 

	SELECT @STAT = @@ERROR	
	SELECT @EndAlrtDate = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'ALERT', ALERTNO, TRANNO 
		FROM @TT t, Alert 
		WHERE
		t.Account = Alert.Account AND
		Alert.WLCode = @WLCODE AND
		Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
	
	SELECT @STAT = @@ERROR 
	IF @STAT <> 0  GOTO ENDOFPROC
END ELSE BEGIN
	IF @WLTYPE = 0 BEGIN
	SELECT @StartAlrtDate = GETDATE()
	INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT) 
	SELECT @WLCODE, 'Account policy:  ''' + Account + ''' has repetitive payment over ' + 
		DBO.BSA_InternalizationMoneyToString (@minimumAmount)  + Space(1) + @BaseCurr + ' in ' + CONVERT(VARCHAR, @day) + ' days. ' , 0, 
		GETDATE(), NULL, NULL, NULL, Account 
	FROM @TT 
	GROUP BY Account 

	SELECT @STAT = @@ERROR	
	SELECT @EndAlrtDate = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC
	END ELSE IF @WLTYPE = 1 BEGIN
		SELECT @StartAlrtDate = GETDATE()
		INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			WLCODE, WLDESC, CREATETIME )
		SELECT	NULL, MIN(BookDate) , Null, Account,
			NULL, 'RULE', NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, 0, 0, 0, 0, 
			NULL, NULL, NULL, NULL, 0, NULL, NULL,
			@WLCODE, 'Account policy:  ''' + Account + ''' has repetitive payment over ' + 
		DBO.BSA_InternalizationMoneyToString (@minimumAmount)  + Space(1) + @BaseCurr +' in ' + CONVERT(VARCHAR, @day) + ' days. ' , GETDATE() 
	FROM @TT 
	GROUP BY Account

	SELECT @STAT = @@ERROR	
	SELECT @EndAlrtDate = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC
	END
	
	IF @WLTYPE = 0 BEGIN
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'ALERT', ALERTNO, TRANNO 
		FROM @TT t, Alert 
		WHERE
		t.Account = Alert.Account AND
		Alert.WLCode = @WLCODE AND
		Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
	
	SELECT @STAT = @@ERROR 
	END ELSE IF @WLTYPE = 1 BEGIN
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'SUSPACT', RECNO, TRANNO 
		FROM @TT t, SUSPICIOUSACTIVITY S 
		WHERE
		t.account = S.account AND 
		S.WLCODE = @WLCODE AND
		S.CREATETIME BETWEEN @StartAlrtDate AND @EndAlrtDate
	SELECT @STAT = @@ERROR 
	END
END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_RepetitiveOverXinZDays
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_RepetitiveOverXinZDays
RETURN @stat
Go



--- Branch data for Bene Transactions
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'BSA_ActivityBranchesForCase')
    DROP PROCEDURE [DBO].[BSA_ActivityBranchesForCase]
GO
Create PROCEDURE dbo.BSA_ActivityBranchesForCase(@ObjectID 	sCode)
With Encryption as

    Declare @CaseType varchar(11)
    Declare @Branch varchar(11)
    Declare @stat int
	Declare @paytran Int

    Select @caseType = SuspType 
	From SuspiciousActivity with (nolock)	
	Where recno =@ObjectID 

	set @paytran = 0
	If Exists (Select * from SuspiciousActivity with (nolock) where RecNo=@ObjectID )
		SELECT @Paytran=CASE When ActInActCnt>0 Then 1 Else 2 End
		FROM SuspiciousActivity with (nolock)
		WHERE recno = @ObjectID
	Else 
		SELECT @Paytran=CASE When ActInActCnt>0 Then 1 Else 2 End
		FROM SuspiciousActivityHist with (nolock)	
		WHERE recno = @ObjectID


    If   @caseType <> 'ExcCurrency'
    Begin 
		If Exists(Select Top 1 a.Branch
		From dbo.SAsActivity sas with (nolock)
		Join dbo.Activity a with (nolock) on sas.TranNo=a.TranNo
		Where sas.objectID=@ObjectID 
		and sas.Included=1)

		Select distinct a.Branch, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		From dbo.SAsActivity sas with (nolock)
		Join dbo.Activity a with (nolock) on sas.TranNo=a.TranNo
		Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch
		Where sas.objectID=@ObjectID 
		and sas.Included=1
        Order By a.BookDate

        Else
			Select distinct ah.Branch, ah.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			From dbo.SAsActivity sas with (nolock)
			Join dbo.ActivityHist ah with (nolock) on sas.TranNo=ah.TranNo
			Join PSEC.dbo.Branch b with (nolock) on b.Code = ah.Branch
			Where sas.objectID=@ObjectID 
			and sas.Included=1
			Order By ah.BookDate

        Select @stat = @@error
        Return @stat
	End

    If (Select Enabled From OptionTbl where code='BeneCTR')=1
    Begin 
		--Exeed currency. Trans not is sasactivity.
		If exists(Select Top 1 a.Branch
			From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
			on case when a.BeneCustID is NULL Then a.Cust Else a.BeneCustID End = sa.Cust
			AND a.BookDate = sa.BookDate
			Where sa.recno = @objecTID)

			Select distinct a.Branch, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
			on case when a.BeneCustID is NULL Then a.Cust Else a.BeneCustID End = sa.Cust AND a.BookDate = sa.BookDate 
			and (@paytran = a.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch
			and a.CashTran = 1
			Where sa.recno = @objecTID
			Order By a.BookDate

		Else
			Select distinct ah.Branch, ah.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		    From SuspiciousActivity sa  with (nolock) Join activityhist ah with (nolock)
		    on case when ah.BeneCustID is NULL Then ah.Cust Else ah.BeneCustID End = sa.Cust AND ah.BookDate = sa.BookDate 
			and (@paytran = ah.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = ah.Branch		    
			and ah.CashTran = 1
		    Where sa.recno = @OBJECTID
		    Order By ah.BookDate
    End
    Else
    Begin
		--Exeed currency. Trans not is sasactivity.
		If exists(Select Top 1 a.Branch
			From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
			on a.Cust = sa.Cust AND a.BookDate = sa.BookDate
			Where sa.recno = @objecTID)

			Select distinct a.Branch, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
			on a.Cust = sa.Cust AND a.BookDate = sa.BookDate 
			and (@paytran = a.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch		    
			and a.CashTran = 1
			Where sa.recno = @objecTID
			Order By a.BookDate

		Else
			Select distinct ah.Branch, ah.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		    From SuspiciousActivity sa  with (nolock) Join activityhist ah with (nolock)
		    on ah.Cust = sa.Cust AND ah.BookDate = sa.BookDate 
			and (@paytran = ah.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = ah.Branch		    
			and ah.CashTran = 1
		    Where sa.recno = @OBJECTID
		    Order By ah.BookDate
    End
							
	 Select @stat = @@error
     Return @stat	 
Go

if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_SameByOrderBeneAndSameAmt]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure [dbo].USR_SameByOrderBeneAndSameAmt
Go
CREATE PROCEDURE [dbo].USR_SameByOrderBeneAndSameAmt(@WLCode SCode, @testAlert INT,
	@minIndAmt money, 
	@minCount int,
	@activityTypeList varchar(8000),
	@riskClassList varchar(8000),
	@days int
	)
AS
/* RULE AND PARAMETER DESCRIPTION
Description:Detects activity when the name in the beneficiary data field is the
same as the name in the by order data field when transactions in the same 
amount occur in a specified time period and the amount exceeds the specified 
minimum amount. Checks for activity on current day and looks back for activity
for the specified period. Alerts are grouped based on ByOrder, Amount. 
Designed to be run Pre-EOD.


Parameters:		
	@WLCode [in] SCode = Rule Code				
	@testAlert = Test Alert or Not (1 for Test, 0 for No)
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
   	@minCount = The Minimum transaction count	
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.    	
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@days = Number of days to look back	(Calendar Days)
*/
	
/*  Declarations */
DECLARE	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@tranAmt MONEY,
	@activityCount  INT,
	@currentTransactionCount INT,
	@IndAmount  MONEY,
	@fromDate INT

DECLARE @SetDate DATETIME
DECLARE	@TT TABLE (
	tranAmt MONEY,
	tranNo INT,
	byOrder  VARCHAR(40) 
	
)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @currentTransactionCount = @@TRANCOUNT	-- Save the current trancount
IF @currentTransactionCount = 0
	-- Transaction has not begun
	BEGIN TRAN USR_SameByOrderBeneAndSameAmt
ELSE
	-- Already in a transaction
	SAVE TRAN USR_SameByOrderBeneAndSameAmt
/* End standard stored procedure transaction header */

-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList (NOLOCK)
WHERE WLCode = @WLCode

--Making the List variables Null, if value is '' or -ALL-
--Else Removing space and Trimming
IF (ISNULL(@riskClassList,'') = '' 
	OR UPPER(ISNULL(@riskClassList,'-ALL-')) = '-ALL-')
	SELECT @riskClassList = NULL
ELSE
	SELECT @riskClassList = ',' + 
		REPLACE(LTRIM(RTRIM(@riskClassList)),CHAR(32),'') + ','

IF (ISNULL(@activityTypeList,'') = '' 
	OR UPPER(ISNULL(@activityTypeList,'-ALL-')) = '-ALL-')
	SELECT @activityTypeList = NULL
ELSE
	SELECT @activityTypeList = ',' + 
		REPLACE(LTRIM(RTRIM(@activityTypeList)),CHAR(32),'') + ','

--Subtracting 1, to look at @days-1 days Transactions from ActivityHist Table
--and 1 day from Activity Table
SELECT @days = ABS(@days) - 1

SET @fromDate = dbo.ConvertSqlDateToInt(
	DATEADD(d, -1 * @days, CONVERT(VARCHAR, @SetDate))) 

--Exists clause is used to pick from history, only if corresponding 
--record(s) present in Activity

INSERT INTO @TT (tranAmt, tranNo, byOrder)
SELECT a.BaseAmt, a.tranNo, a.byOrder
FROM Activity a (NOLOCK), Customer (NOLOCK)
WHERE a.cust = Customer.Id AND
	a.bookdate >= @fromDate AND
	a.BaseAmt >= @minIndAmt AND
    a.byOrder IS NOT NULL and 
	a.bene IS NOT NULL AND  
	a.byOrder = a.bene AND
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
	(@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0)
Union
SELECT ah.BaseAmt, ah.tranNo, ah.byOrder
FROM ActivityHist ah (NOLOCK), Customer (NOLOCK)
WHERE ah.cust = Customer.Id AND
	ah.bookdate >= @fromDate AND 
	ah.BaseAmt >= @minIndAmt AND
    ah.byOrder IS NOT NULL and 
	ah.bene IS NOT NULL AND  
	ah.byOrder = ah.bene AND
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, ah.type) + ',',@activityTypeList) > 0) AND
	(@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0) AND
	Exists
	(SELECT a.tranNo
		FROM Activity a (NOLOCK), Customer (NOLOCK)
		WHERE
		a.bookdate >= @fromDate AND 
		a.Bene = ah.Bene AND
		a.ByOrder = ah.ByOrder AND
		a.cust = Customer.Id AND
		a.BaseAmt >= @minIndAmt AND
		a.baseamt=ah.baseamt AND
	    a.byOrder IS NOT NULL and 
		a.bene IS NOT NULL AND  
		a.byOrder = a.bene AND
		(@activityTypeList IS NULL OR 
			CHARINDEX(',' + 
				CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
		(@riskClassList IS NULL OR 
			CHARINDEX(',' + 
				LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0)
	)
DECLARE	@cur CURSOR
DECLARE @byOrder LONGNAME,
		@bookdate INT

SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())

SET @cur = CURSOR FAST_FORWARD FOR 
SELECT SUM(tranAmt) tranAmt, COUNT(tranNo) tranCnt, a.byOrder, a.TranAmt
FROM @TT a GROUP BY  a.byOrder, a.tranAmt
HAVING count(tranno) >= @minCount

OPEN @cur 
FETCH NEXT FROM @cur INTO @tranAmt, @activityCount , @byOrder, @IndAmount

WHILE @@FETCH_STATUS = 0 BEGIN

	SET @desc = @byOrder + '(ByOrder) is performing '
		+ CONVERT(VARCHAR, @activityCount ) + ' transaction(s) '  
		+ 'to self with the same amount ' + CONVERT(VARCHAR, @IndAmount)
		+ ' where the total amount is ' + CONVERT(VARCHAR, @tranAmt)
		+ ' over a period of ' + CONVERT(VARCHAR, @days + 1) + ' days'
	IF @testAlert = 1
	BEGIN
		EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
		NULL, NULL, 1
		IF @stat <> 0 GOTO EndOfProc
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 --Alert
		BEGIN
			EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
				NULL, NULL, 0
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE 
		IF @WLTYPE = 1 --Case
		BEGIN
			EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT, 
				@WLCode, @desc, @bookdate, Null, NULL
			IF @stat <> 0 GOTO EndOfProc   	
		END	
	END		
	IF (@WLTYPE = 0) OR (@testAlert = 1)
	BEGIN
		--Inserting records into SASACTIVITY for the @ID
		--obtained from API_InsAlert stored Procedure
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'Alert', @ID, TRANNO 
			FROM @TT t
			WHERE  t.byOrder = @byOrder AND
			t.tranAmt = @IndAmount
	
			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC
	END 
	ELSE 
	IF @WLTYPE = 1 --Case
	BEGIN
		--Inserting records into SASACTIVITY for the @ID
		--obtained from API_InsSuspiciosActivity stored Procedure
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'SUSPACT', @ID, TRANNO 
				FROM @TT t
			WHERE  t.byOrder = @byOrder AND
			t.tranAmt = @IndAmount

			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC 
	END
	FETCH NEXT FROM @cur INTO @tranAmt, @activityCount , @byOrder, @IndAmount
END --Cursor While loop Ends

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_SameByOrderBeneAndSameAmt
  RETURN @stat
END	

IF @currentTransactionCount = 0
  COMMIT TRAN USR_SameByOrderBeneAndSameAmt
RETURN @stat
Go

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES      
    WHERE ROUTINE_NAME = 'BSA_DelAccountOwner')      
    DROP PROCEDURE [DBO].[BSA_DelAccountOwner]      
 GO  

Create Procedure dbo.BSA_DelAccountOwner ( @account ObjectId, @cust ObjectId, 
					@relationship varchar(40), @oper SCode, @ts timestamp )
 With Encryption As
 
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_DelAccountOwner
  else
    -- Already in a transaction
	save tran BSA_DelAccountOwner
  -- End standard stored procedure transaction header
  
  declare	@stat		int,
			@txt		varchar(500),
			@cnt        int

  If Exists ( Select *
     FROM      
     AccountOwner with (NOLOCK) JOIN RelationshipType with (NOLOCK) ON AccountOwner.Relationship = RelationshipType.Code      
     WHERE      
      AccountOWNER.Account = @account AND      
      Cust = @cust and 
      (RelationshipType.Name = @relationship or RelationshipType.Code = @relationship)) begin
    -- Use Null TS to indicate bypass of concurrency check
	if @ts Is Null
	Delete w from AccountOwner w  JOIN RelationshipType t ON w.Relationship = t.Code Where w.Account = @account and w.Cust =  @cust and (t.Name = @relationship or t.Code = @relationship)
	else
	Delete w from AccountOwner w  JOIN RelationshipType t ON w.Relationship = t.Code Where w.Account = @account and w.Cust =  @cust and w.Ts = @ts and (t.Name = @relationship or t.Code = @relationship)
    Select @stat = @@error , @cnt = @@rowcount
  end else 
    Select @stat = 250006	-- Object does not exist 
  If ( @stat = 0 and @cnt = 0 )
		select @stat = 250001 -- concurent update
  If ( @stat <> 0 ) begin 
        rollback tran BSA_DelAccountOwner
		return @stat
  end		   
  Select @txt = @account + ' / ' + @cust
  Exec BSA_InsEvent @oper, 'Del', 'AcctOwn', @account, @txt

 -- add event to cust record
  Select @txt = 'Deleted ''' + @relationship + 
                '''relation to Account ' + @account

  Exec BSA_InsEvent @oper, 'Ann', 'Cust', @cust, @txt


  If @trnCnt = 0
		  commit tran BSA_DelAccountOwner	 
  return @stat
Go



if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[CRY_RiskModelMain]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.CRY_RiskModelMain
Go


Create Procedure [dbo].[CRY_RiskModelMain] ( @RFTCode SCode)
  With Encryption As

	Select RFTCode = rft.Code, RFTName = rft.Name, 
		RFTDes = rft.Description, 
		RFICode = rfi.Code, RFIName = rfi.Name, 
		RFIDes = rfi.Description, 
		rfi.Weight, rfi.DefScore, rfi.Required, 
		rfi.Source, rfi.Prompt, SourceTable, SourceField,
		CalcItemName = ci.Name
	  From RiskFactorType rft with(nolock)
	    join RiskFactorList rfl with(nolock) on rfl.RFTCode = rft.Code 
	    join RiskFactorItem rfi with(nolock) on rfi.Code = rfl.RFICode
	    left join RFCalculatedItem ci with(nolock) on ci.ID = rfi.CalcItemID
	 Where (Upper(@RFTCode) = 'ALL' Or rft.Code = @RFTCode) 
Go
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[CRY_RuleList]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure [dbo].[CRY_RuleList]
Go

CREATE PROCEDURE [dbo].[CRY_RuleList] ( @RuleType varchar(20), @Schedule varchar(20),
				@AlertCase varchar(20),@PrePostEod varchar(20))
WITH ENCRYPTION AS


-- Declaring variables

Declare @UserDefined 	int,
	@WLType 	int,
	@PreEod 	int,
	@Sched		varchar(20)

-- To check userdefined or template rule
If UPPER(@RuleType) = 'USERDEFINED' 
	SELECT @UserDefined = 1
Else if	UPPER(@RuleType) = 'TEMPLATE' 
	SELECT @UserDefined = 0
Else IF (ISNULL(@RuleType,'') = '' OR UPPER(ISNULL(@RuleType,'-BOTH-')) = '-BOTH-')
	SELECT @UserDefined = NULL


-- To check scheduled or unscheduled
if UPPER(@Schedule) = 'UNSCHEDULED'
	select @Sched = ',0,'
Else if UPPER(@Schedule) = 'SCHEDULED'
	select @Sched = ',1,2,3,4,5,'
Else IF (ISNULL(@Schedule,'') = '' OR UPPER(ISNULL(@Schedule,'-ALL-')) = '-ALL-')
	select @Sched = NULL

-- To Check Alert or Case
If UPPER(@AlertCase) = 'ALERT'
	select @WLType = 0
Else if UPPER(@AlertCase) = 'CASE'
	select @WLType = 1
Else IF (ISNULL(@AlertCase,'') = '' OR UPPER(ISNULL(@AlertCase,'-BOTH-')) = '-BOTH-')
	select @WLType = NULL


-- To Check whether its a pre-eod or post-eod rule
if UPPER(@PrePostEod) = 'POST-EOD'
	select @PreEod = 0
Else if UPPER(@PrePostEod) = 'PRE-EOD'
	select @PreEod = 1
Else IF (ISNULL(@PrePostEod,'') = '' OR UPPER(ISNULL(@PrePostEod,'-BOTH-')) = '-BOTH-')
	select @PreEod = NULL


-------------------------------------------------------------------
-- To get the parameter values from the params

--Declare @xmlText as nvarchar(max)
Declare @xmlText as nvarchar(max)
Declare @List as nvarchar(max)
DECLARE @hDoc int 
DECLARe @wlCode varchar(1000)
DECLARE @tt table (wlcode varchar(35), Alias varchar(1000), Value nvarchar(max) )
DECLARE @tt1 table (wlcode varchar(35), params nvarchar(max) )

DECLARE cur Cursor for
Select wlcode from watchlist with (NOLOCK)

OPEN cur
FETCH NEXT FROM cur into @wlCode

While @@FETCH_STATUS = 0 
Begin

	select @xmlText = Params from watchlist with(nolock) where wlcode = @wlCode and createtype != 1 

	exec sp_xml_preparedocument @hDoc OUTPUT, @xmlText

	INSERT INTO @tt 
	SELECT @wlCode, Alias, Alias + ' = ''' + Value + '''' as value
	FROM OPENXML (@hDoc, '/Params/Param')
	WITH (WlCode varchar(35), Alias varchar(1000), Value nvarchar(max))

	EXEC sp_xml_removedocument @hDoc

	Select @list = COALESCE(@List + char(10)+ char(13), '') + Value from @tt 
	Insert into @tt1 values (@wlcode, @list) 
	
	delete from @tt

	FETCH NEXT FROM cur into @wlCode	
	
	set @list = NULL
	set @xmlText = NULL

End

Close cur
DeAllocate cur

-------------------------------------------------------------------


-- The final query
SELECT Watchlist.[WLCode], (Isnull([Title],'') + char(10)+ char(13) + 
       ' ' + char(10)+ char(13) + Isnull([Desc],'')) as Title, 
	   Case WLType 	when 0 then 'Alert' 
			else 'Case' end as AlertORCase,
	   schedule, 
	   Case [Schedule] when 1 then 'Daily'
			   when 3 then 'Weekly'
			   when 5 then 'Quarterly'
			   when 4 then 'Monthly' end as Freqschedule, 
	   Case [IsPreEOD] when 1 then 'Pre-EOD'
			   else 'Post-EOD' end as EOD, 
	   Case [CreateType] when 1 then 'N/A'
		           else  tt.params End as params 
FROM  	Watchlist with(nolock) ,@tt1 tt 
Where   Watchlist.Wlcode = tt.wlcode
AND   	(@UserDefined 	IS NULL OR IsUserDefined 	= @UserDefined)
And 	(@Sched IS NULL 
		OR CHARINDEX(',' + CONVERT(VARCHAR, watchlist.schedule) + ',',@Sched) > 0) 
And 	(@WLType IS NULL OR WLType = @WLType)
and 	(@PreEod IS NULL OR IsPreEOD = @PreEod)
order by Watchlist.[WLCode]



Go

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO




Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Completed conversion of Pbsa objects To Version 10.0.0'
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

