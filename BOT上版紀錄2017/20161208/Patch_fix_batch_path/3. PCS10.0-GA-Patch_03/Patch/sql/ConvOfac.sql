/*
**  File Name:        ConvOFAC.sql
**
**  Functional Description:
**
**      This module contains SQL patches 
**		for Version 10.0.0.0
**
**  Creation Date:    12/12/2011
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2011                                                   ***
*** Metavante Corporation.                                               ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|----------------------------------------------------        
07/22/2014      SM       R140567096 : Modified OFS_ImportSDN to update 
                         ListCreateDate, EffectiveDate, LastModifDate and 
                         Program correctly during Import Sanctioned Party
07/22/2014		SS		 R140992605: Created new OFAC option DspFimgLdis to Display Build File Image and Load Distribution links
01/20/2015		SM		 Defect 4838: Modified WOFS_GetSearchResults to display Program Name as 'keyword', when not displayed 
						 as KEY WORD for MatchName during Manual Search.
	
					
*/
Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Starting Conversion of OFAC Database to version 10.0.0 '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

Use OFAC
GO

Set ANSI_NULLS ON
Go

Set NOCOUNT ON 
Go

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'OFS_ImportSDN')
 Begin
	DROP PROCEDURE [dbo].OFS_ImportSDN
 End
GO
CREATE PROCEDURE dbo.OFS_ImportSDN( @entNum int,      
            @name SanctionName,      
            @firstName SanctionName,      
            @middleName SanctionName,      
            @lastName SanctionName,      
            @type char (12),      
            @program char(15),      
            @title varchar(180),      
            @callSign char(8),      
            @vessType char(25),      
            @tonnage char(14),      
            @grt char(8),      
            @vessFlag varchar(40),      
            @vessOwner varchar(150) ,      
            @dob varchar(150) ,      
            @remarks text ,      
            @remarks1 varchar(255) ,      
            @remarks2 varchar(255) ,      
            @sdnType int,      
            @dataType int,      
            @userRec int ,      
            @country varchar(75),      
            @sex char,      
            @build varchar(15),      
            @complexion varchar(15),      
            @race varchar(15),      
            @eyes varchar(15),      
            @hair varchar(15),      
            @height int,      
            @weight int,      
            @listID varchar(15),      
            @listType varchar(15),      
            @primeAdded bit,      
            @standardOrder bit,      
            @effectiveDate datetime,      
            @expiryDate datetime,      
            @status int,      
            @listCreateDate datetime,      
            @listModifDate dateTime,      
			@oper Code,      
            @ignDrvNames BIT,       
  		    @retEntnum int OUTPUT,      
		    @retStatus int OUTPUT)      
WITH ENCRYPTION       
AS      
BEGIN      
  declare @event   varchar(3)      
  --Transaction       
  declare @trnCnt        int      
  -- Processing starts here:      
  declare @now datetime      
  declare @pos int      
  declare @stat int      
  declare @text varchar(255)      
  declare @maxNum int      
  
  select @now = GETDATE()      
  
  select @trnCnt = @@trancount  -- Save the current trancount      
  
  if @trnCnt = 0      
  -- Transaction has not begun      
  begin tran OFS_ImportSDN      
  else      
  -- Already in a transaction      
  save tran OFS_ImportSDN      
  
  Declare @list TABLE(Code VARCHAR(20))      
  insert into @list      
  SELECT code FROM ListType      
  WHERE code in      
  (      
  SELECT listtype from dbo.OFS_GetScopeForListType a       
   INNER JOIN psec.dbo.SEC_fnGetScopeForOperrights(@oper, 'MODOFACLST,DSPOFACLST') b --Rights hardcoded  
      ON a.branchdept = b.branchdept      
  )       
  AND enabled = 1 and isprimelist = 0      
      
	  if NOT Exists (Select Code from @list where code=@listType)      
	  Begin   
			Select @retStatus=201 --No Access Rights Value.  
			Select @retEntNum=0  
			SET @STAT = 1       
			GOTO ENDOFPROC      
	  END  
	  ELSE  
	  BEGIN   
		   If @type = ''      
		   select @type = 'other'      
		   Select ISNULL(@ignDrvNames, 1)      
		   If Exists(SELECT EntNum FROM SDNTable WHERE ListId=@listid AND userrec=@userRec)      
		   BEGIN      
				BEGIN TRY  
				  SELECT @EntNum=EntNum FROM SDNTable WHERE ListId=@listid AND userrec=@userRec      
				  UPDATE SDNTable SET      
				   [name]=@name,      
				   firstName=@firstName ,      
				   middleName=@middleName,      
				   lastName=@lastName ,      
				   type=@type ,      
				   program=UPPER(@program) ,      
				   title=@title ,      
				   callSign=@callSign ,      
				   vessType=@vessType ,      
				   tonnage=@tonnage ,      
				   grt=@grt ,      
				   vessFlag=@vessFlag ,      
				   vessOwner=@vessOwner ,      
				   dob=@dob ,      
				   remarks=@remarks ,      
				   remarks1=@remarks1 ,      
				   remarks2=@remarks2 ,      
				   sdnType=@sdnType ,      
				   dataType=@dataType ,      
				   userRec=@userRec ,      
				   IgnoreDerived = @ignDrvNames ,      
				   country=@country ,      
				   sex=@sex ,      
				   build=@build ,      
				   complexion=@complexion ,      
				   race=@race ,      
				   eyes=@eyes ,      
				   hair=@hair ,      
				   height=@height ,      
				   weight=@weight ,      
				   listID=@listID,      
				   listType=@listType ,      
				   primeAdded=@primeAdded ,      
				   standardOrder=@standardOrder ,      
				   expiryDate=@expiryDate ,      
				   status=3 , --Insert Status Value     
				   listModifDate=@listModifDate , 
				   LastModifDate=@now,   
				   lastoper=@oper      
				  WHERE EntNum = @EntNum      
				  delete from Notes where entnum = @EntNum      
				  delete from sdnaddrtable where entnum = @EntNum      
				  delete from sdnalttable where entnum = @EntNum      
				  delete from urls where entnum = @EntNum      
				  delete from dobs where entnum = @EntNum      
				  delete from relationship where entnum = @EntNum      
				  delete from relationship where relatedid = @EntNum      
				  Select @retStatus=203 --Update Values.     
				  Select @event = 'MOD'      
				END TRY  
				BEGIN CATCH  
				   SELECT ERROR_NUMBER()   
				   Select @stat = @@ERROR      
				   Select @retStatus=204  
				   GOTO ENDOFPROC  
				END CATCH  
		   END      
		   ELSE      
		   BEGIN      
			 BEGIN TRY  
				  Select @maxNum = Max(EntNum) From SDNTable       
				  If @maxNum Is NULL Set @maxNum = 0      
				  if @maxNum < 10000000      
				   Set @maxNum = 10000000      
				  else      
				   Set @maxNum = @maxNum +1      
				  Set @entNum = @maxNum      
			  
				  Insert into SDNTable (EntNum, [Name], FirstName,      
				   MiddleName, LastName,Title,      
				   Program, Type, CallSign,      
				   VessType, Tonnage, GRT,      
				   VessFlag, VessOwner, Dob,      
				   Remarks, Remarks1, Remarks2,SDNType,      
				   DataType, userRec, deleted,      
				   duplicRec, IgnoreDerived,      
				   ListID, ListType, Country,      
				   Sex, Build, Height, Weight,      
				   Race, Complexion, eyes, hair,      
				   PrimeAdded, StandardOrder, EffectiveDate,      
				   ExpiryDate, Status, ListCreateDate,      
				   ListModifDate, CreateDate, LastModifDate,      
				   LastOper)      
				  Values      
				   (@entNum, @name, NULLIF(@firstName,''),      
				   NULLIF(@middleName,''), NULLIF(@lastName,''), @title,      
				   UPPER(@program),  @type,  @callSign,      
				   @vessType, @tonnage, @grt,      
				   @vessFlag, @vessOwner, NULLIF(@dob,''),      
				   @remarks, @remarks1, @remarks2, @sdnType,      
				   @dataType, @userRec, 0,      
				   0, @ignDrvNames,@listId, @listType, @country,      
				   @sex, @build, NullIf(@height,0), NullIf(@weight,0),      
				   @race, @complexion, @eyes, @hair,@primeAdded,  
				   @standardOrder, @effectiveDate,@expiryDate, 2,  
				   @listCreateDate,@listModifDate, @now, @now, @oper )      
				  Select @retStatus=202 --Insert Status Value     
				  Select @event = 'CRE'      
			 END TRY  
			 BEGIN CATCH  
				  SELECT ERROR_NUMBER()   
				  Select @stat = @@ERROR      
				  Select @retStatus=204  
				  GOTO ENDOFPROC  
			 END CATCH  
			END      
	  
		Select @retEntNum = @entNum      
	        
		If @stat = 0 Begin      
			 Exec OFS_InsEvent @oper, @event, 'SanName', @name, Null      
			 select @stat = @@ERROR      
			 If @stat = 0 Begin      
				 Exec OFS_SetFileImageFlag      
			 End      
		End      
    
  End  
  EndofProc:      
  IF @trnCnt = 0 BEGIN      
	   COMMIT TRAN OFS_ImportSDN
	   END ELSE IF @stat <> 0 BEGIN      
	   ROLLBACK TRAN OFS_ImportSDN      
  END     
         
RETURN @stat      
END 
GO

-- DspFimgLdis: Option to Display File Image and Load Distribution links
IF Not EXISTS(select * From OptionTbl Where Code = 'DspFimgLdis')
	insert into OptionTbl ( Code, Name, Enabled, ModEnable, CreateOper,
		   CreateDate, LastOper, LastModify )
    values ( 'DspFimgLdis', 'Display File Image and Load Distribution links', 1, 0, 'Prime',
		   GetDate(), 'Prime', GetDate() )
go

if exists (select * from dbo.sysobjects where
    id = object_id(N'[dbo].[WOFS_GetSearchResults]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[WOFS_GetSearchResults]
GO

CREATE PROCEDURE [dbo].[WOFS_GetSearchResults]
    (
        @OperCode       SCode,
        @SysRef         MessageReference

    )

With Encryption As
    --Get All Checked Search Results
    select  distinct SDNTable.EntNum, SDNTable.[Name], OFACSearchResults.[Program], SDNTable.[Type],
    SDNTable.Remarks1, OFACSearchResults.MatchName, OFACSearchResults.MatchText,
	OFACSearchResults.Score
    FROM SDNTable with(nolock) left outer join OFACSearchResults with(nolock)
    on SDNTable.[Name] = OFACSearchResults.OriginalSDNName and 
    (SDNTable.[Program] = OFACSearchResults.[Program] or OFACSearchResults.[Program] = 'KEY WORD') and
    OFACSearchResults.OperCode = @OperCode and
    OFACSearchResults.SystemRef=@SysRef
    where SDNTable.EntNum in (select Convert(int, [RowId]) from PCDB.dbo.ViewCheckRows with(nolock)
    where ViewId =100 and OperCode=@OperCode and CheckStatus=1)
Go



PRINT ''
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT ' Completed Conversion of OFAC Database to version 10.0.0  '
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT ''
GO