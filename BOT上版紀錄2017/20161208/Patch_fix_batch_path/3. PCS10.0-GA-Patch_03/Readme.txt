|-----------------------------------------------------------------------------------------|
|				       :: INSTRUCTIONS ::                                 |   
|     				  				                          |
|-----------------------------------------------------------------------------------------|


Purpose of the Patch: 
---------------------
 
This patch address the following issues:

	I140763510   : Issue with Web Service "DELEGATED" attribute

	I140733893   : Unable to access the EDD_RiskModel.rpt

	Defect 4838  : Program Name is not identified for Key Word matching

	I150008662   : BSA Rule List Report Prime	               

	Defect 4558  : R141007968 : Receiving an error from FinCen when trying to 
		       submit a Batch CTR � Validation Error

        Defect 4562  : �2a� and '2b' Checked in a CTR form when copied �If Entity� is
                       in enabled state

	Defect 4586  : E-format file  is getting rejected

	Defect 4890  : PCS - EDDChanges - Include In Risk Analysis field of Relationship 
		       type screen,should not be modified by any patch, 
		       to default its value to 1


Description of changes:
----------------------

	I140763510   : OFAC Cases are not created with DELEGATED status when DELEGATED set 
		       to True in the OFAC Web Service. Now it has been corrected to create 
		       OFAC Cases with delegated status when DELEGATED attribute set to True
		       in OFAC Web Service.

	I140733893   : Added the correct SP in the update script for EDD_RiskModel.rpt	

	Defect 4838  : OFAC Manual search does not identify the Program Name for Key Word
		       Matching.It has been fixed in this patch to properly identify and
		       display �Key Word� as the Program Name for Key Word matching.    	

	I150008662   : CRY_Rulelist reports to display correctly when User defined rules
		       are created through Rule Builder.
		       Fixed display issue if Title and Description are empty.

	Defect 4558  : The CTR pdf will be saved successfully even the FX In and FX out amount
		       are empty.

        Defect 4562  : The option 'If Entity' remains disabled when �2a� or '2b' option is 
		       in a CTR form when it is copied to a new form.

	Defect 4586  : Additional lines are not created if the FX In and FX out amount are
		       empty. Hence, eFile will not get rejected.	

	Defect 4890  : This fix addresses the issue in Relationship Type screen to retain the 
		       modified Include In Risk field value, Patches sequelling the 
		       first patch default the value of Include In Risk field to 1 
		       for modified records also.
                       

Note: This patch should be installed and tested in a Test environment before it is deployed 
      to a Production environment.

Patch Name : PCS10.0-GA-Patch#03
Patch Release Date: 02/18/2015 (Updated 03/25/2015)
Version Applicable To: 10.0.0
Patch Pre-requisites: Make sure the previous patch mentioned below are installed

	1 CumulativeEnhancementFixes
	2 OFACWebServiceAndBSAArchiveCaseFix


INSTRUCTIONS:
-------------

      Download the attached "PCS10.0-GA-Patch#03.zip" file, and extract the contents 
      to a temporary folder.

      The following files will be extracted:

        1) OFACReporterWS.txt 
	2) ConvOFAC.sql
	3) ConvPBSA.sql
        4) CTR03292012.pdf
	5) PbsaObj.txt
	6) Interop.PBSAObj.txt
	7) CompEndOfDay.txt
	8) CompMgrUtils.txt
	9) DataLoad.txt
	10) About.htm
	11) Readme.txt
       

APPLICATION SERVER:
-------------------

	1. Stop the following services
		a) Prime Logger
		b) OFAC Reporter Remoting service
		c) IIS

	2. Rename the following extracted files.
		a) Rename OFACReporterWS.txt to OFACReporterWS.dll
		b) Rename PbsaObj.txt to PbsaObj.dll
		c) Rename Interop.PBSAObj.txt to Interop.PBSAObj.dll
		d) Rename CompEndOfDay.txt to CompEndOfDay.exe
		e) Rename CompMgrUtils.txt to CompMgrUtils.exe
		f) Rename DataLoad.txt to DataLoad.exe

	3. Unregister PBSAObj.dll by running the following at a command prompt:

	    "Regsvr32 /u x:\Prime\exe\PbsaObj.dll"

                Note: x is the prime installation directory. 
		Installation path could be different from "x:\Prime". 

	4. Backup OFACReporterWS.dll from the x:\Prime\Services\OFACReporter\OFACSearchWebService\bin folder 
	   to another folder where x: is the drive where Prime is installed.
		
	5. Copy the renamed file OFACReporterWS.dll to x:\Prime\Services\OFACReporter\OFACSearchWebService\bin folder 

	6. Backup the following file from X:\Prime\Exe to another Backup folder
		a) PbsaObj.dll
		b) Interop.PBSAObj.dll
		c) CompEndOfDay.exe
		d) CompMgrUtils.exe
		e) DataLoad.exe

	7. Copy the following renamed files to X:\Prime\Exe folder
		a) PbsaObj.dll
		b) Interop.PBSAObj.dll
		c) CompEndOfDay.exe
		d) CompMgrUtils.exe
		e) DataLoad.exe

	8. Register the new PBSAObj.dll by running the following at a command prompt:

		"Regsvr32 x:\Prime\exe\PbsaObj.dll"

	9. Backup Interop.PBSAObj.dll from X:\Prime\ComplianceManager\Bin to another Backup folder

	10.Copy the renamed Interop.PBSAObj.dll to X:\Prime\ComplianceManager\Bin folder			
	  
	11.Backup the file CTR03292012.pdf from the x:\prime\ComplianceManager\PDF\ to another Backup folder  	

	12.Copy the extracted PDF CTR03292012.pdf to the x:\prime\ComplianceManager\PDF\ folder
	
	13.Backup the file About.htm from the x:\Prime\ComplianceManager\ folder to another backup folder 
	   
  	14.Copy the extracted file About.htm from patch to x:\Prime\ComplianceManager folder

	15.Start the following services.
		a) Prime Logger
		b) OFAC Reporter Remoting service
		c) IIS	

DATABASE SERVER:
----------------

	*Execute the following for all tenants:

	1. Execute ConvOfac.sql script submitted with this patch
 
	2. Execute ConvPbsa.sql script submitted with this patch
  	
	3. Execute SQLSecurity.exe for OFAC and PBSA database

		