|-----------------------------------------------------------------------------------------|
|				       :: INSTRUCTIONS ::                                 |   
|     				  				                          |
|-----------------------------------------------------------------------------------------|


Purpose of the Patch: 
---------------------
 
This patch addresses the following issue:

	I150128093: Unable to make changes in Sanction Data Rule.


Description of changes:
----------------------

	I150128093: Currently in PCS, the values for the following fields are passed 
		    as empty while creating or modifying the Sanction Data Rule.
			
			a. Concatenated Names Parts 
		    	b. Branch and department in Associations, Excludes/Inequalities
			   and Word Tables.

	Defect 5341: while opening the fileimage, fix is provided to disable the fields 
		     under Association tab and Build Tab.

	This patch addresses the above issues.
		

Note: This patch should be installed and tested in a Test environment before it is deployed 
      to a Production environment.

Patch Name : PCS10.0-GA-Patch#04
Patch Release Date: 04/10/2015 
Version Applicable To: 10.0.0
Patch Pre-requisites: Make sure the previous patch mentioned below are installed

	1 CumulativeEnhancementFixes
	2 OFACWebServiceAndBSAArchiveCaseFix
	PCS10.0-GA-Patch#03


INSTRUCTIONS:
-------------

      Download the attached "PCS10.0-GA-Patch#04.zip" file, and extract the contents 
      to a temporary folder.

      The following files will be extracted:

        1) CMBrowser.txt
	2) About.htm
	3) Readme.txt
       

APPLICATION SERVER:
-------------------

	1. Stop IIS

	2. Backup the following file from the %PRIME%\ComplianceManager\bin\ folder to another
 	   backup folder, where %PRIME% refers to the PRIME installation folder
		
                a) CMBrowser.dll 

	3. Rename the extracted file CMBrowser.txt to CMBrowser.dll		
		
	4. Backup the file About.htm from the %PRIME%\ComplianceManager\ folder to another backup folder 
	   
  	5. Copy the extracted file About.htm from patch to %PRIME%\ComplianceManager folder

	6. Copy the below renamed file to %PRIME%\ComplianceManager\bin\ folder

		a) CMBrowser.dll
	
	7. Start IIS	


DATABASE SERVER:
----------------

	None

		