/*
**  File Name:      ConvPsec.sql
**
**  Functional Description:
**
**      This module contains SQL conversion procedures for Prime version 10.0.0
**
**  Facility        The Compliance Suite's Security (PSEC) Database
**  Creation Date:  07/23/2015
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2015                                                   ***
*** FIS                              ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
***************************************************************************
                         Maintenance History

------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification
------------|----------|----------------------------------------------------
 07/23/2015		SM		I150259105:Last Modify Date and Last Modify By should 
						not be updated during Approve Function. Approve Date 
						and Approve By are made not to be updated during 
						operator modification for second approval.
						I150126285: The security configuration changes are 
						blocked for deleted and disabled operators
						(deleted operators are always disabled).
*/


-------------------------------------------------------------------------------

Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print '  Starting Conversion of PSEC Objects To 10.0.0'
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

Use PSEC
Go

Set ANSI_NULLS ON
Go

Set NOCOUNT ON
Go

If exists (Select * From sysobjects 
	Where id = object_id(N'[dbo].[PSEC_OperatorUpd]') and
	OBJECTPROPERTY(id, N'IsTrigger') = 1)
	Drop Trigger dbo.PSEC_OperatorUpd 
Go

CREATE TRIGGER dbo.PSEC_OperatorUpd ON Operator WITH ENCRYPTION FOR UPDATE AS
 
	DECLARE	@code	code
	DECLARE	@oper	SCode
	declare @optOn  int
	DECLARE @LogText Varchar(6000)
	IF @@rowcount > 0 
	BEGIN
		Set @optOn = 0
		Select @optOn = Enabled from OptionTbl where OptCode = 'SecAppr'

		IF @optOn = 1 
		BEGIN		
		UPDATE Operator SET LastModify = getdate()
			FROM Inserted I, Operator R, Deleted D WHERE I.code = R.code and (I.Approved = 0 or (D.Approved = I.Approved and I.code = D.code))		
		END
		ELSE
		BEGIN
		UPDATE Operator SET LastModify = getdate()
			FROM Inserted I, Operator R WHERE I.code = R.code 
		END
		SELECT @code = code ,@oper = LastOper 
			FROM Inserted
		SELECT @LogText= ''
		SELECT TOP 1 
		@LogText=
		
			CASE
				WHEN isnull(I.UserName,'') != isnull(D.UserName,'')
				THEN '|UserName OLD: '+ltrim(rtrim(convert(char,isnull(D.UserName,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.UserName,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.Branch,'') != isnull(D.Branch,'')
				THEN '|Branch OLD: '+ltrim(rtrim(convert(char,isnull(D.Branch,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.Branch,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.Dept,'') != isnull(D.Dept,'')
				THEN '|Dept OLD: '+ltrim(rtrim(convert(char,isnull(D.Dept,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.Dept,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.Enable,'') != isnull(D.Enable,'')
				THEN '|Enable OLD: '+ltrim(rtrim(convert(char,isnull(D.Enable,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.Enable,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.DeleteF,'') != isnull(D.DeleteF,'')
				THEN '|DeleteF OLD: '+ltrim(rtrim(convert(char,isnull(D.DeleteF,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.DeleteF,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.SecurityPrMsk,'') != isnull(D.SecurityPrMsk,'')
				THEN '|SecurityPrMsk OLD: '+ltrim(rtrim(convert(char,isnull(D.SecurityPrMsk,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.SecurityPrMsk,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.CDBPrMsk,'') != isnull(D.CDBPrMsk,'')
				THEN '|CDBPrMsk OLD: '+ltrim(rtrim(convert(char,isnull(D.CDBPrMsk,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.CDBPrMsk,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.MessagePrMsk,'') != isnull(D.MessagePrMsk,'')
				THEN '|MessagePrMsk OLD: '+ltrim(rtrim(convert(char,isnull(D.MessagePrMsk,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.MessagePrMsk,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.Message2PrMsk,'') != isnull(D.Message2PrMsk,'')
				THEN '|Message2PrMsk OLD: '+ltrim(rtrim(convert(char,isnull(D.Message2PrMsk,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.Message2PrMsk,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.TestkeyPrMsk,'') != isnull(D.TestkeyPrMsk,'')
				THEN '|TestkeyPrMsk OLD: '+ltrim(rtrim(convert(char,isnull(D.TestkeyPrMsk,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.TestkeyPrMsk,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.OFACPrMsk,'') != isnull(D.OFACPrMsk,'')
				THEN '|OFACPrMsk OLD: '+ltrim(rtrim(convert(char,isnull(D.OFACPrMsk,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.OFACPrMsk,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.ReimbPrMsk,'') != isnull(D.ReimbPrMsk,'')
				THEN '|ReimbPrMsk OLD: '+ltrim(rtrim(convert(char,isnull(D.ReimbPrMsk,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.ReimbPrMsk,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.Name,'') != isnull(D.Name,'')
				THEN '|Name OLD: '+ltrim(rtrim(convert(char,isnull(D.Name,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.Name,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.EMail,'') != isnull(D.EMail,'')
				THEN '|EMail OLD: '+ltrim(rtrim(convert(char,isnull(D.EMail,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.EMail,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.LastLogin,'') != isnull(D.LastLogin,'')
				THEN '|LastLogin OLD: '+ltrim(rtrim(convert(char,isnull(D.LastLogin,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.LastLogin,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.CreateDate,'') != isnull(D.CreateDate,'')
				THEN '|CreateDate OLD: '+ltrim(rtrim(convert(char,isnull(D.CreateDate,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.CreateDate,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.LastOper,'') != isnull(D.LastOper,'')
				THEN '|LastOper OLD: '+ltrim(rtrim(convert(char,isnull(D.LastOper, '')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.LastOper,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.LastModify,'') != isnull(D.LastModify, '')
				THEN '|LastModify OLD: '+ltrim(rtrim(convert(char,isnull(D.LastModify, '')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.LastModify,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.Approved,'') != isnull(D.Approved,'')
				THEN '|Approved OLD: '+ltrim(rtrim(convert(char,isnull(D.Approved,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.Approved,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.ApproveOper,'') != isnull(D.ApproveOper,'')
				THEN '|ApproveOper OLD: '+ltrim(rtrim(convert(char,isnull(D.ApproveOper,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.ApproveOper,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.LastApprove,'') != isnull(D.LastApprove,'')
				THEN '|LastApprove OLD: '+ltrim(rtrim(convert(char,isnull(D.LastApprove,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.LastApprove,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.HintQuestion,'') != isnull(D.HintQuestion,'')
				THEN '|HintQuestion OLD: '+ltrim(rtrim(convert(char,isnull(D.HintQuestion,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.HintQuestion,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.HintAnswer,'') != isnull(D.HintAnswer,'')
				THEN '|HintAnswer OLD: '+ltrim(rtrim(convert(char,isnull(D.HintAnswer,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.HintAnswer,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.MaxAttempts,'') != isnull(D.MaxAttempts,'')
				THEN '|MaxAttempts OLD: '+ltrim(rtrim(convert(char,isnull(D.MaxAttempts,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.MaxAttempts,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.LockTime,'') != isnull(D.LockTime,'')
				THEN '|LockTime OLD: '+ltrim(rtrim(convert(char,isnull(D.LockTime,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.LockTime,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.Status,'') != isnull(D.Status,'')
				THEN '|Status OLD: '+ltrim(rtrim(convert(char,isnull(D.Status,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.Status,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.ExpirationDate,'') != isnull(D.ExpirationDate,'')
				THEN '|ExpirationDate OLD: '+ltrim(rtrim(convert(char,isnull(D.ExpirationDate,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.ExpirationDate,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.LastPwdChgDate,'') != isnull(D.LastPwdChgDate,'')
				THEN '|LastPwdChgDate OLD: '+ltrim(rtrim(convert(char,isnull(D.LastPwdChgDate,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.LastPwdChgDate,'')))) 
				ELSE ''
			END 
			+CASE
				WHEN isnull(I.LastPwdChgOper,'') != isnull(D.LastPwdChgOper,'')
				THEN '|LastPwdChgOper OLD: '+ltrim(rtrim(convert(char,isnull(D.LastPwdChgOper,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.LastPwdChgOper,'')))) 
				ELSE ''
			END
			+CASE
				WHEN isnull(I.ForcePasswordChange,'') != isnull(D.ForcePasswordChange,'')
				THEN '|ForcePasswordChange OLD: '+ltrim(rtrim(convert(char,isnull(D.ForcePasswordChange,'')))) +
					' NEW: '+ltrim(rtrim(convert(char,isnull(I.ForcePasswordChange,'')))) 
				ELSE ''
			END	
			+ '|'
		FROM Inserted I, Deleted D 
		WHERE I.code = D.code
		
		EXEC PSEC_InsEvent @oper, 'Mod', 'Operator' ,@code,@LogText
	END
GO



if exists (select * from sysobjects where id = object_id(N'[dbo].[PSEC_ModifyOper]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[PSEC_ModifyOper]
GO

CREATE PROCEDURE dbo.PSEC_ModifyOper @code Code,
                                 @userName varchar (255),
                                 @branch Code,
                                 @dept Code,
                                 @enable bit,
                                 @deleteF bit,
                                 @security int,
                                 @cdb int,
                                 @message int,
                                 @msg2 int,
                                 @testkey int,
                                 @ofac int,
                                 @reimb int,
                                 @name char (35),
                                 @eMail varchar (100),
                                 @oper Code,
                                 @secToken int,
                                 @pwd  varchar(100),
								 @forcePwdChg Bit,
                                 @hintQ varchar(255),
                                 @hintAns varchar(255),
								 @Approved bit
With Encryption As

	-- Start standard stored procedure transaction header
	DECLARE @trnCnt int
	SELECT @trnCnt = @@trancount	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN PSEC_ModifyOper
	ELSE
		-- Already in a transaction
 		SAVE TRAN PSEC_ModifyOper
	-- End standard stored procedure transaction header

	declare @app      bit
	declare @appOp    Code
	declare @appDate  datetime
  	declare @ModifiedByOper Code
	declare @optOn    int
  	declare @stat     int
	declare @now      datetime
	declare @ExpirationDate datetime, 
		@LastPwdChgDate datetime, 
		@LastPwdChgOper Code,
		@ExpPeriod int,
		@NumPwdHist int
	declare @Oldpwd varchar(100), @OldsecToken int

	select @now = GETDATE()
	Set @optOn = 1

	SELECT @Oldpwd = Password, @OldsecToken = SecurityToken from Operator where UserName = @Username
	Set @ModifiedByOper =  ISNULL(RTRIM(@oper),'PRIME')
	Select @app = Approved from Operator where Code = RTRIM(@code)

	Select @optOn = Enabled from OptionTbl where OptCode = 'PwdConfig'

	Set @LastPwdChgDate = NULL
	Set @LastPwdChgOper = NULL
	Set @ExpirationDate = NULL

	IF isnull(@pwd,'') <> isnull(@Oldpwd,'')
	BEGIN
		Set @LastPwdChgDate = @now
		Set @LastPwdChgOper = @oper
		If @optOn = 1 Begin
			SELECT @ExpPeriod = ParameterValue FROM OptionParameter 
				WHERE OptCode = 'PwdConfig' AND ParameterCode = 'PwdExpPer'
			If IsNull(@ExpPeriod ,0) > 0
				Set @ExpirationDate = DateAdd(dd, @ExpPeriod, getdate())
		END
	END
	
	  /* This stored proc is specifically used for modifying operator and update
	     Last Modify Time and Last Modify By. Last Approved Time and Last Approved By 
	     are updated using different stored procedure PSEC_ApproveOper. */

	
	UPDATE Operator
		SET Branch = @branch, UserName = RTRIM(@userName), Dept = @dept,
		Enable = @enable, DeleteF = @deleteF, SecurityPrMsk = @security,
		CDBPrMsk = @cdb, MessagePrMsk = @message, Message2PrMsk = @msg2,
		TestkeyPrMsk = @testkey, OFACPrMsk = @ofac, ReimbPrMsk = @reimb,
		Name = RTRIM(@name), EMail = NULLIF(RTRIM(@eMail), ''), LastModify = @now,
		LastOper = @ModifiedByOper,  SecurityToken = @secToken,
		Password = NullIF(@pwd, ''), HintQuestion = NullIF(@hintQ, ''), Approved = @Approved,
		HintAnswer = NullIF(@hintAns, ''), ForcePasswordChange = @forcePwdChg, 
		LastPwdChgDate=ISNULL(@LastPwdChgDate,LastPwdChgDate),
		LastPwdChgOper=isnull(@LastPwdChgOper,LastPwdChgOper), 
		ExpirationDate=isnull(@ExpirationDate,ExpirationDate)
	WHERE Code = RTRIM(@code)

	select @stat = @@ERROR
	if @stat <> 0
	begin
		ROLLBACK TRAN PSEC_ModifyOper
		RETURN @stat
	end

--	Insert password history records
	If @optOn = 1 AND (isnull(@pwd,'') <> isnull(@Oldpwd,''))
	BEGIN
--		Get the parameter for number of history records to be maintained
		SELECT @NumPwdHist = ParameterValue FROM OptionParameter 
			WHERE OptCode = 'PwdConfig' AND ParameterCode = 'NumPwdHist'
		If IsNull(@NumPwdHist ,0) > 0
		BEGIN
--			Increment SeqNo for all history
			UPDATE PasswordHist SET SeqNo = SeqNo+1
				WHERE Code = @Code
			SELECT @stat = @@error
			IF ( @stat <> 0 )
			BEGIN
				ROLLBACK TRAN PSEC_ModifyOper
				RETURN @stat
			END
--			Insert the new history record
			EXEC @stat = PSEC_InsPasswordHist @Code, 1, @OldPwd, @Oper
			IF ( @stat <> 0 )
			BEGIN
				ROLLBACK TRAN PSEC_ModifyOper
				RETURN @stat
			END

--			Delete remaining history records
			DELETE FROM PasswordHist WHERE Code = @Code And SeqNo > @NumPwdHist
			IF ( @stat <> 0 )
			BEGIN
				ROLLBACK TRAN PSEC_ModifyOper
				RETURN @stat
			END

		END
	END
	IF @trnCnt = 0
		COMMIT TRAN PSEC_ModifyOper
	
	return @stat

GO

If exists (Select * From sysobjects 
	Where id = object_id(N'[dbo].[PSEC_OptionUpd]') and
	OBJECTPROPERTY(id, N'IsTrigger') = 1)
	Drop Trigger dbo.PSEC_OptionUpd 
Go

CREATE TRIGGER dbo.PSEC_OptionUpd on OptionTbl WITH ENCRYPTION AFTER UPDATE AS
    
    Declare @ExpPeriod 	int
    Declare @OptCode Code
    Declare @Enabled bit

    IF UPDATE(Enabled) 
     BEGIN
	    select @OptCode=OptCode, @Enabled = Enabled from inserted
IF @OptCode ='PwdConfig'
	 	     BEGIN 
	 			IF @Enabled=1
	 			 BEGIN  	    	
	 				SELECT @ExpPeriod = ParameterValue FROM OptionParameter 
	 				WHERE OptCode = 'PwdConfig' AND ParameterCode = 'PwdExpPer'
	 				if @ExpPeriod = 0 or @ExpPeriod is null
	 				 BEGIN
	 					Update Operator Set ExpirationDate=NULL where enable = 1
	 				 END
	 				else
	 				 BEGIN
	 					Update Operator Set ExpirationDate=DateAdd(dd,@ExpPeriod, getdate()) where enable = 1			
	 				 END 
	 			 END     
	 			ELSE
	 			 BEGIN
	 				Update Operator Set ExpirationDate=NULL where enable = 1
	 	    	 END
	     END -- IF @OptCode ='PwdConfig'
		if @@error <> 0 
         BEGIN
        	RAISERROR('Error while updating Password Expiration Date of Operator.',11,1)
         END
	    IF @OptCode ='ACTIVEDRTY'
	     BEGIN 
			IF @Enabled=0
			 BEGIN  	    	
				UPDATE OptionParameter SET ParameterValue='0' WHERE ParameterCode = 'ACTDIRAUTT' And OptCode = 'ACTIVEDRTY'
				UPDATE OptionParameter SET ParameterValue='0' WHERE ParameterCode = 'ACTDIRAUTR' And OptCode = 'ACTIVEDRTY'
			 END
	     END -- IF @OptCode ='ACTIVEDRTY'
		if @@error <> 0 
         BEGIN
        	RAISERROR('Error while updating Active Directory Option Parameters.',11,1)
         END

	 END
     
GO
If exists (Select * From sysobjects 
	Where id = object_id(N'[dbo].[PSEC_OptParamUpd]') and
	OBJECTPROPERTY(id, N'IsTrigger') = 1)
	Drop Trigger dbo.PSEC_OptParamUpd 
Go

CREATE TRIGGER dbo.PSEC_OptParamUpd on OptionParameter WITH ENCRYPTION AFTER UPDATE AS
    
    Declare @ExpPeriod 	int

    IF UPDATE(ParameterValue) and (select parametercode from inserted)='PwdExpPer'
    BEGIN   	    	
	SELECT @ExpPeriod = ParameterValue FROM inserted	
	if @ExpPeriod = 0
	 BEGIN
		Update Operator Set ExpirationDate=NULL where enable = 1
	 END
	else
	  BEGIN
		Update Operator Set ExpirationDate=DateAdd(dd,@ExpPeriod, getdate()) where enable = 1	
	  END       
    END
     if @@error <> 0 
        BEGIN
        	RAISERROR('Error while updating Password Expiration Date of Operator.',11,1)
        END
GO
Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print '  Completed conversion of PSEC objects For Version 10.0.0'
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go
