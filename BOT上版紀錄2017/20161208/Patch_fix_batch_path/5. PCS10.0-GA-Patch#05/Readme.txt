|-------------------------------------------------------------------------------------------------|
|				        :: INSTRUCTIONS ::                                        |   
|     				  				                                  |
|-------------------------------------------------------------------------------------------------|


Purpose of the Patch:  
---------------------
 
This patch fixes the following issues:

	1. I150157534: Scan audit in batch filter run indicates error
	2. I150360354: All matches not are included on output file
	3. R141295898: v10 OFAC Scan Audit issue 
	4. I150183327: Exclude Name issue
        5. I150283085: Issue with OFAC case screen
	6. I150387737: CIF Filter Rule Processing Issue
	7. I150259105: Changes made to Password Config
	8. I150126285: Security Log Time                                                            
	9. Defect #5592: V9.1_Patch #31_Approve and disapprove button are enabled while 
           adding and operator, on clicking system error is thrown 
       10. Defect #5589: OFAC ScanAudit functionality not working

Description of changes:
-------------------------

	1. Fixed to display all the input records in OFAC Scan audit when PartyListType = 0 and 
	   updatecifdata is set as FALSE in batchfilter ini configuration.

	2. Fixed to display all the matched records in the Output file, for the input file having 
           Larger number of records (Millions of records).	

	3. Fixed the Bulk Insert load error for batchfilter when process more than 4Million records 
           and update the records in the Audit table. Batch Filter performance has been improved for
	   processing OFAC Scan Audit Records.
 
	4. When exclude names are created, the application throws an error 
           �Exclude Name does not exist in the database�.Fix provided to trim the spaces in 
	   Exclude name Text area.

	5. Search text entered in multiple lines are displayed in one line in the ofac case screen.
	   Fix has been provided to handle the CRLF appropriately for ofac case screen

	6. Previously Fixed Length CIF Rules were not processed thru PCS and it has been corrected to
   	   process successfully. KeyField1 and KeyField2 fields population and summary have been 
	   corrected in CIF Rules Processing thru PCS and CIF Filter utility.
                                                                                
	7. Currently Last Modify Time is getting updated for disabled and deleted operators when
	   password configuration options are modified. Now, it has been corrected to update 
	   operators appropriately when password configuration options are modified as below.
                  - Update all the enabled and disabled operators
                  - Do not update any deleted operators
                  - Update Last Modify Time for enabled operators
                  - Do not update Last Modify Time to disabled operators

	8. Currently Last Approve Time and Last Approved By are getting updated during Operator 
	   Modification. Also Last Modify Time and Last Modify By are getting updated during 
	   Approve / Disapprove actions in case of second approval. Now it has been corrected 
	   to display Last Approve Time, Last Approved By, Last Modify Time and Last Modify By 
	   appropriately based on Modify/Approve/Disapprove in case of second approval is ON/OFF.

	9. After adding a new operator, System error is thrown when the enabled approve and 
	   disapprove buttons are clicked. Given fix to disable the approve and disapprove 
	   button after a new operator is added.

       10. Fixed the issue of  OFAC scan audit table not being updated when the KeyField2 value
	   is null or empty.


Note: This patch should be installed and tested in a Test environment before it is deployed 
      to a Production environment.

Patch Name: PCS10.0-GA-Patch#05
Patch Release Date: 09/08/2015
Version Applicable To: 10.0
Patch Pre-requisites: Make sure all previous 4 patches mentioned below are installed

	1 CumulativeEnhancementFixes
	2 OFACWebServiceAndBSAArchiveCaseFix
	PCS10.0-GA-Patch#03
	04 PCS10.0-GA-Patch#04	


INSTRUCTIONS:
-------------

      Download the attached "PCS10.0-GA-Patch#05.zip" file, and extract the contents 
      to a temporary folder.

      The following files will be extracted:

	1) ConvPsec.sql
	2) Readme.txt
	3) About.htm
	4) CMBrowser.txt
	5) ConvOFAC.sql
	6) CifFilter.txt
	7) CompMgrUtils.txt
	8) Batchfilter.txt
	9) SFAX.txt
       10) sfa.txt
       11) tsf.txt
       12) OFACFilter.txt
       13) PriUtil.txt
       14) SanctionFilterEngine.txt
       15) ServerAPI.txt
       16) ofacsrv.txt
       17) ServerClasses.txt
       18) ServiceThreads.txt
       
	

APPLICATION SERVER:
-------------------

	1. Stop all Prime services and IIS.

	2. Backup the following file from the %PRIME%\ComplianceManager\bin\ folder to another backup folder 
	   where %PRIME% refers to the PRIME installation folder

		a) CMBrowser.dll
		
			
	      Note:
		Make backup by copying original files to another folder, created
		specially for backup purposes outside of the installation folder.
		Do not backup by renaming original files and leaving them in the
		installation folder. 

        3. Backup the following file from %PRIME%\ComplianceManager folder

		a) About.htm
		

	4. Backup the following files from %PRIME%\Exe folder

		a) CifFilter.exe
		b) CompMgrUtils.exe
		c) Batchfilter.exe
		d) SFAX.exe	
		e) sfa.dll
      		f) tsf.dll
       		g) OFACFilter.ocx
       		h) PriUtil.dll
       		i) SanctionFilterEngine.dll
		j) ServerAPI.dll
		k) ofacsrv.exe
		l) ServerClasses.dll
		m) ServiceThreads.dll
		
	
	5. Rename the extension of the following extracted files from the patch

		a) Rename CifFilter.txt to  CifFilter.exe
		b) Rename CMBrowser.txt to CMBrowser.dll
		c) Rename CompMgrUtils.txt to CompMgrUtils.exe
  	        d) Rename Batchfilter.txt to Batchfilter.exe
		e) Rename SFAX.txt to SFAX.exe
		f) Rename sfa.txt to sfa.dll
		g) Rename tsf.txt to tsf.dll
		h) Rename PriUtil.txt to PriUtil.dll
		i) Rename SanctionFilterEngine.txt to SanctionFilterEngine.dll
		j) Rename ServerAPI.txt to ServerAPI.dll
		k) Rename OFACFilter.txt to OFACFilter.ocx
		l) Rename ofacsrv.txt to ofacsrv.exe
		m) Rename ServerClasses.txt to ServerClasses.dll
		n) Rename ServiceThreads.txt to ServiceThreads.dll
		
		  	       		 
	6. Copy the below renamed file to %PRIME%\ComplianceManager\bin\ folder

		a) CMBrowser.dll
		
		
	7. Copy the following file from patch to %PRIME%\ComplianceManager folder

		a) About.htm
	

	8. Copy the below renamed files from patch to %PRIME%\Exe folder
		
		a) CifFilter.exe
		b) CompMgrUtils.exe
		c) Batchfilter.exe
		d) SFAX.exe
		e) sfa.dll
      		f) tsf.dll
       		g) OFACFilter.ocx
       		h) PriUtil.dll
       		i) SanctionFilterEngine.dll	
		j) ServerAPI.dll
		k) ofacsrv.exe
		l) ServerClasses.dll
		m) ServiceThreads.dll
	
	9. After completing the Database Server steps given below, restart all Prime services and IIS.


DATABASE SERVER:
----------------
	 
	*Execute the following for all tenants:

	1. Backup the following databases

			a) Psec
			b) OFAC
	
	2. Execute ConvPsec.sql, ConvOFAC.sql script submitted with this patch.
	
	3. Execute SQLSecurity.exe for OFAC,PSEC database.


