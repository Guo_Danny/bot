|--------------------------------------------------------------------------------------|
|	                        :: INSTRUCTIONS ::          		               |   
|     				                                 		       |
|--------------------------------------------------------------------------------------|


Purpose of the Patch:  
---------------------
This patch includes the following:
  
	1) Provides support for Swift 2015 Changes
 

Description of changes:
-------------------------

	Support for parsing of field 59F has been added per the following SWIFT change:

	1) For Message Type, MT101, 102, 102+, 103, 103 REMIT, 103+, MT 202 COV, 205 COV, 910
	Add letter option F to field 59a, Beneficiary Customer, in Payments messages. 



Note: This patch should be installed and tested in a Test environment before it is deployed 
      to a Production environment.
	
	
Patch Name: PCS10.0-GA-Patch#06
Patch Release Date: 10/21/2015
Version Applicable To: 10.0
Patch Pre-requisites: Make sure all previous 5 patches mentioned below are installed

	1 CumulativeEnhancementFixes
	2 OFACWebServiceAndBSAArchiveCaseFix
	PCS10.0-GA-Patch#03
	04 PCS10.0-GA-Patch#04	
	5  PCS10.0-GA-Patch#05



INSTRUCTIONS:
-------------
Download the attached PCS10.0-GA-Patch#06.zip file and extract the contents 
to a temporary folder. The following files will be extracted:	

	1) PSTBMain.txt
	2) SwiftRules.ini
	3) Readme.txt
	4) About.htm
	5) ConvPbsa.sql



APPLICATION SERVER:
-------------------

	1) Back up following files to the backup folder created outside 
	   of the installation folder of Prime Application Server:

		1) PSTBMain.exe from c:\Prime\Exe folder
		2) SwiftRules.ini from the location defined in <tenant path>\Config\PBSA\S2BParams.dat
		3) About.htm from the c:\Prime\ComplianceManager folder

		Note:
		  Installation path could be different from "c:\Prime". 

	2) Rename PSTBMain.txt included in this patch to PSTBMain.exe.

	3) Copy following files provided with this patch

		1) PSTBMain.exe to c:\Prime\Exe folder		
		2) Check the SwiftRules.ini file included with the patch. Based on your existing rules 
		configuration you can either copy the SwiftRules.ini to the location defined in 
		<tenant path>\Config\PBSA\S2BParams.dat if the SwiftRules.ini file inlcuded in this patch
		meets your organizations needs, or alternatively update your existing rules file to include 
		the 59F fields where needed (using the provided ini file as a reference).
		3) About.htm to the c:\Prime\ComplianceManager folder
		
		Note:
		  Installation path could be different from "c:\Prime". 



		  
DATABASE SERVER:
----------------
	*Note: Backup the PBSA database before executing the following steps.
	*Execute the following for all tenants:

	1. Execute ConvPbsa.sql script submitted with this patch.
	2. Execute SQLSecurity.exe for the PBSA database.

