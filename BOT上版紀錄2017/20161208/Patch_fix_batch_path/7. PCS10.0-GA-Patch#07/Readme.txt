|-------------------------------------------------------------------------------------------------|
|				        :: INSTRUCTIONS ::                                        |   
|     				  				                                  |
|-------------------------------------------------------------------------------------------------|


Purpose of the Patch:  
---------------------
 
This patch fixes the following issues:

	1. I150482342: CIBAR MQ Enhancement Changes : Updated MQ driver to parse 
		       the PrimeRef tag value from the XML input.
	2. I150384406: Alerts are created without any activity

Description of changes:
-------------------------

	1. MQ driver is enhanced to process the PrimeRef tag value from the XML input.
		
	   PrimeRef tag Value will be processed from input XML message only if �IncludePrimeRef � is 
	   set to �Y� in the ini file and PrimeRef contains value, otherwise Application ID or unique 
           identifier will be generated. PrimeRef tag value will be displayed in Reference field in 
	   OFAC Case screen.   

 	2. Increased the field size of groupby fields by collating the size of corresponding columns
	   in the database.	


Note: This patch should be installed and tested in a Test environment before it is deployed 
      to a Production environment.

Patch Name: PCS10.0-GA-Patch#07
Patch Release Date: 10/23/2015
Version Applicable To: 10.0
Patch Pre-requisites: Make sure all previous 6 patches mentioned below are installed

	1 CumulativeEnhancementFixes
	2 OFACWebServiceAndBSAArchiveCaseFix
	PCS10.0-GA-Patch#03
	PCS10.0-GA-Patch#04
	PCS10.0-GA-Patch#05
	PCS10.0-GA-Patch#06
			

INSTRUCTIONS:
-------------

      Download the attached "PCS10.0-GA-Patch#07.zip" file, and extract the contents 
      to a temporary folder.

      The following files will be extracted:

       	1) ServerAPI.txt
       	2) ServerClasses.txt
	3) Readme.txt
	4) About.htm
	5) Pbsaobj.txt
	6) Interop.PBSAObj.txt
	7) Client Interface Parameters.doc
	8) Overview of Ofac MQDriver.doc
	9) MqParams.txt
     
       
	

APPLICATION SERVER:
-------------------

	1. Stop all Prime services and IIS.

	
        2. Unregister PbsaObj.dll by running the following at a command prompt:

		"Regsvr32 /u %PRIME%\exe\PbsaObj.dll"



	3. Backup the following file from the %PRIME%\ComplianceManager\bin\ folder to another backup folder 
	   where %PRIME% refers to the PRIME installation folder

		a) Interop.PBSAObj.dll
		
			
	      Note:
		Make backup by copying original files to another folder, created
		specially for backup purposes outside of the installation folder.
		Do not backup by renaming original files and leaving them in the
		installation folder. 

        4. Backup the following file from %PRIME%\ComplianceManager folder

		a) About.htm
	
	5.Backup the following file from %PRIME%\Prime\%TENANT%\Samples\OFAC where %TENANT% refers the 
	  tenant installation folder. 	

		a) MqParams.ini

	6. Backup the following files from %PRIME%\Exe folder

	       	a) ServerAPI.dll
	       	b) ServerClasses.dll
		c) Pbsaobj.dll
		d) Interop.PBSAObj.dll

	7. Backup the following file from %PRIME%\Documents\OFAC\ folder to backup folder
	
		a) Client Interface Parameters.doc
		b) Overview of Ofac MQDriver.doc

	
	8. Rename the extension of the following extracted files from the patch


		a) Rename ServerAPI.txt to ServerAPI.dll
		b) Rename ServerClasses.txt to ServerClasses.dll
		c) Rename Pbsaobj.txt to Pbsaobj.dll
		d) Rename Interop.PBSAObj.txt to Interop.PBSAObj.dll
		e) Rename MqParams.txt to MqParams.ini
		
		  	       		 
	9. Copy the below renamed file from patch to %PRIME%\ComplianceManager\bin\ folder

		a)  Interop.PBSAObj.dll
		
		
		
	10. Copy the following file from patch to %PRIME%\ComplianceManager folder

		a) About.htm

	11. Copy the below renamed file from patch to %PRIME%\Prime\%TENANT%\Samples\OFAC
		
		a) MqParams.ini
	
	12. Copy the following file from Patch to %PRIME%\Documents\OFAC\ folder

		a) Client Interface Parameters.doc
		b) Overview of Ofac MQDriver.doc
		
		Note: If your organization wishes to implement the PrimeRef field in your environment 
			for MQ xml requests, please review these documents and the sample MqParams.ini 
			file mentioned earlier in this document.


	13. Copy the below renamed files from patch to %PRIME%\Exe folder
		
	       	a) ServerAPI.dll
	       	b) ServerClasses.dll
		c) Pbsaobj.dll
		d) Interop.PBSAObj.dll
	
	14. Register the new PbsaObj.dll by running the following at a command prompt:
	
		"Regsvr32 %PRIME%\exe\PbsaObj.dll"
	
	15. Restart all Prime services and IIS.


DATABASE SERVER:
----------------
	 
	None


