/*
**  File Name:		ConvPcdb.sql
**
**  Functional Description:
**
**      This module contains SQL patches
**		for version 10.0.0
**
**  Facility	    The Prime Central Database
**  Creation Date:  08/04/2014
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2009 - 2011 											 ***
*** FIS                                                ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in FIS.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                   
				       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|----------------------------------------------------
07/30/2014 		SM 		R141076548: Changed the misspelling of Scan Operator 
						name in Listviewcolumns.	
07/30/2014		SS		R140992605: Setting the Rights and License for Load 
						Distribution and Build File Image Tasks
08/12/2014		SS		R140978750 : Update the support information in Prime 
						Compliance suite 
09/03/2015		SS		OFAC Enhancement - Added new Custom settings option 
						'DATABNKOPT' to support Third Party Databank in OFAC
						Reporter

*/ 


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print '  Starting Conversion of Pcdb Objects To Version 10.0.0'
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

USE PCDB
GO

Set ANSI_NULLS ON
Go

Set NOCOUNT ON 
Go

--ListViewColumns

 If Exists (Select * from ListViewColumns Where ColumnName = 'ScanOper')
	update [ListViewColumns] set DisplayName  = 'Scan Operator' where ColumnName = 'ScanOper'
GO

-- Setting the Rights and License for Load Distribution
if exists(select TaskId from TaskTbl where TaskId in (104))
                update [TaskTbl] set rights = 'LOADDIST', License = 'OFAC,DspFimgLdis' where taskid = 104
Go
-- Setting the Rights and License for Build File Image
if exists(select TaskId from TaskTbl where TaskId in (105))
                update [TaskTbl] set rights = 'BLDIMG,LOADDIST', License = 'OFAC,DspFimgLdis' where taskid = 105
Go


----------------------------------------------------------
-- Custom Settings
----------------------------------------------------------

update CustomSettings
set [Value] = '1.866.275.6868 (Option 2-5)',
	 LastOper = 'Prime',
	 LastModifDate = GetDate()
where Code = 'SupportTelephoneNumber' and
						 [Key] = 'Tel:'

GO

update CustomSettings
set [Value] = 'fcs.support@fisglobal.com',
	 LastOper = 'Prime',
	 LastModifDate = GetDate()
where Code = 'SupportEmailAddress' and
						 [Key] = 'Email:'

GO

-----Adding support for OFACOption page
--Add record to PageTbl
IF NOT EXISTS(SELECT * FROM PageTbl WHERE PageId=2020)
	insert into PageTbl values (2020, 'OFACOption.aspx', NULL, NULL, 1) 
	GO
	
--Add record to ControlTbl
IF NOT EXISTS(SELECT * FROM ControlTbl WHERE ControlId=2020)
	INSERT INTO ControlTbl VALUES (2020, NULL, NULL,4, 2020, 4, NULL, NULL) 
	GO


--Update record in ViewColumns
update ViewColumns set controlid=2020 where viewid=112 and columnid=0 and tableid=127  
GO



--Add OFACOptions in CustomObject
IF NOT EXISTS(SELECT * FROM CustomObjects WHERE Code='OFACOptions')
	INSERT INTO CustomObjects
	(Code, Description, Enabled, CreateDate, CreateOper, LastModifDate, LastOper)
	values
	('OFACOptions', 'OFAC Options', 1, GETDATE(), 'PRIME', GETDATE(),'PRIME') 
	GO

--Add OFACOptions, DATABANKOPTION to CustomSettings 
IF NOT EXISTS(SELECT * FROM CustomSettings WHERE Code='OFACOptions' and Product='OFAC' and SettingCode='DATABNKOPT')
	insert into CustomSettings
	(Code, Product, RecordType, OperCode, [Key], Value, IsUserEditable, SettingCode, CreateDate, CreateOper, LastModifDate, LastOper)
	Values
	('OFACOptions','OFAC','SYSTEM','','DataBank Option � Configure system for usage of Prime DataBank Lists/User Defined Lists', 'DATABANK', 1, 'DATABNKOPT', GETDATE(), 'PRIME', GETDATE(), 'PRIME') 
	GO
	
	
Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Completed conversion of Pcdb objects To Version 10.0.0'
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go


