
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OfacOption.aspx.vb" Inherits="CMBrowser.OfacOption"  smartNavigation="True"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Option Parameter</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="js/nav.js"></script>
	</HEAD>
	<body class="formbody">
		<form id="Form1" method="post" runat="server">
			<table id="PageLayout" cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td><asp:label id="clblRecordMessage" runat="server" width="808px" cssClass="RecordMessage">Record Message</asp:label></td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" Height="24px" Font-Size="X-Small" Width="616px"></asp:validationsummary></td>
					</tr>
				<tr>
					<td>
						<TABLE id="Table2" style="WIDTH: 816px; HEIGHT: 40px" cellSpacing="0" cellPadding="0" width="816"
							border="0">
							<TR align="center">
								<TD align="right"><asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder></TD>
								<td align="left"><uc1:ucrecordscrollbar id="RecordScrollBar" runat="server" Visible="False"></uc1:ucrecordscrollbar></td>
							</TR>
						</TABLE>
						<uc1:uctabcontrol id="MainTabControl" runat="server"></uc1:uctabcontrol></td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordHeader" runat="server" width="804px" CSSClass="paneltitle"></asp:label></td>
				</tr>
			</table>
			<asp:panel id="Panel1" runat="server" Height="176px" Width="744px" BorderStyle="None" BorderWidth="1px"
				BorderColor="Silver" Cssclass="Panel">
				<TABLE id="Table41" cellSpacing="0" cellPadding="0" width="300" border="1">
					<TR>
						<TD align="right" width="50">
							<asp:label id="Label5" runat="server" CssClass="LABEL">Code:</asp:label></TD>
						<TD width="250">
							<asp:textbox id="txtCode" runat="server" CssClass="TEXTBOX" Enabled="False"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label1" runat="server" CssClass="LABEL">Name:</asp:label></TD>
						<TD>
							<asp:textbox id="txtName" runat="server" Width="360px" CssClass="TEXTBOX" Enabled="False"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:CheckBox id="chkEnabled" runat="server" CssClass="LABEL" Enabled="False"  AutoPostBack="True" Checked="false"></asp:CheckBox></TD>
						<TD>
							<asp:label id="Label10" runat="server" Font-Size="11px">Enabled</asp:label></TD>
					</TR>
					<TR>
						<TD colSpan="2">
							<asp:label id="Label2" runat="server" CssClass="LABEL">Option Parameters</asp:label></TD>
					</TR>
					<TR>
						<TD colSpan="2">
							<asp:DataGrid id="dgOptionParam" runat="server" Width="100%" BorderColor="#FF8000" CellPadding="1"
								AutoGenerateColumns="False">
								<ItemStyle CssClass="ViewGridCell"></ItemStyle>
								<HeaderStyle CssClass="GridHeader"></HeaderStyle>
								<Columns>
									<asp:BoundColumn DataField="SettingCode" HeaderText="Parameter Code"></asp:BoundColumn>
									<asp:BoundColumn DataField="Key" HeaderText="Parameter Name"></asp:BoundColumn>
									<asp:TemplateColumn HeaderText="ParameterValue">
										<ItemTemplate>
											<asp:CheckBox id="chkParamValue" runat="server" Visible="False"></asp:CheckBox>
											<asp:TextBox id="txtParamValue" MaxLength="6" runat="server" Width="100%" Visible="False" BorderColor="#FF8000"
												BorderStyle="Groove"></asp:TextBox>
											<asp:DropDownList ID="ddlParamValue" runat="server" Visible="false" CssClass="DROPDOWNLIST" >
											<asp:ListItem Text="Prime Databank & User Defined Lists" Value="DATABANK" ></asp:ListItem>
											<asp:ListItem Text="User Defined Lists Only" Value="USERDEFINED"></asp:ListItem>											
											</asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn Visible="False" DataField="Value" HeaderText="Parameter Value"></asp:BoundColumn>
								</Columns>
							</asp:DataGrid></TD>
					</TR>
				</TABLE>
				<table id="Table1" cellSpacing="0" cellPadding="0" border="0">
				<tr><td style="height: 15px"></td></tr>
				<tr>				    
					<td style="height: 15px"><asp:label id="lblWarningText" runat="server" width="808px" cssClass="RecordMessage" ForeColor="Red" Visible="False">Warning Text</asp:label></td>
				</tr>				
			</table>
			</asp:panel><asp:panel id="Panel2" runat="server" Height="15px" Width="712px" cssclass="Panel">
			</asp:panel><asp:panel id="cPanelValidators" runat="server" Height="46px" Width="864px"></asp:panel></form>
	</body>
</HTML>