/*
**  File Name:        ConvOFAC.sql
**
**  Functional Description:
**
**      This module contains SQL patches 
**		for Version 10.0.0.0
**
**  Creation Date:    12/12/2011
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2011                                                   ***
*** Metavante Corporation.                                               ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|----------------------------------------------------        
07/22/2014      SM       R140567096 : Modified OFS_ImportSDN to update 
                         ListCreateDate, EffectiveDate, LastModifDate and 
                         Program correctly during Import Sanctioned Party
07/22/2014		SS		 R140992605: Created new OFAC option DspFimgLdis to 
						 Display Build File Image and Load Distribution links
						 Scan audit events when run through batch filter.
					     I150360354: Fixed to display all matches in the Output file.
						 Defect #5589 : V9.1 - Patch#31 - OFAC Scan Audit 
						 Functionality is not working.
						 R141295898: Fixed Bulkinsert load conversion error 
						 when running batchfilter with 4+ million records.
09/22/2014      PWK      Add Dow Jones Import utility for Watchlist Data Files
01.06.2015		DEH		 Partial DOB Matching; Added two new columns to 
						 SanctionDataRule table: EnablePartialDOBMatching,
						 DOBToleranceInYears; Updated the following stored 
						 procs: OFS_InsSanctionDataRule, OFS_UpdSanctionDataRule,
						 OFS_SelectDistinctSanctionDataRule, OFS_SelectSanctionDataRule,
						 OFS_SanctiondataRuleUpd; Updated the following trigger: 
						 OFS_SanctiondataRuleUpd
01/20/2015		SM		 Defect 4838: Modified WOFS_GetSearchResults to display 
						 Program Name as 'keyword', when not displayed as 
						 KEY WORD for MatchName during Manual Search.
06/18/2015		DEH		 updated OFS_AddUpdateDowJonesSDN: set LastModifDate = 
						 @now in the Update statement and changed @program to 
						 Upper(@program) and changed stored proc transaction 
						 name to OFS_AddUpdateDowJonesSDN; Updated table indexes
						 for performance 
08/14/2015		SM	     I150157534: Fixed to display all the records in OFAC
						 Scan audit events when run through batch filter.
					     I150360354: Fixed to display all matches in the Output file.
						 Defect #5589 : V9.1 - Patch#31 - OFAC Scan Audit 
						 Functionality is not working.
						 R141295898: Fixed Bulkinsert load conversion error 
						 when running batchfilter with 4+ million records.
09/03/2015		SS		 OFAC Enhancement - Created a new OFAC Option 'DATABNKOPT'
09/18/2015		AS		 OFAC Enhancement - Created a new Function fn_IsPrimeDatabankDataExists
11/02/2015		PWK      OFAC Dow Jones GA - Modify and create new Dow Jones Max Date Methods
						 to support additional file formats.
12/14/2015		PWK      OFAC Dow Jones GA - Change Dow Jones SDN Range from 9 - 10 million
						 to 7 - 10 million, sharing ids with World Check.  Dow Jones is now
						 importing over 1 million records and needs the additional
						 available space.
01/05/2016      AS       Script to update �USER� in ListTypeAssociation Table, 
                         if it is not exist.
02/11/2016      PWK      Script to update OFS_SetLastSDNUpd (and add supporting function 
                         fn_IsNewestSDNFromDowJones), to allow support for
						 Incremental Build Images of Dow Jones Data.
*/
Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Starting Conversion of OFAC Database to version 10.0.0 '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

Use OFAC
GO

Set ANSI_NULLS ON
Go

Set NOCOUNT ON 
Go

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'OFS_ImportSDN')
 Begin
	DROP PROCEDURE [dbo].OFS_ImportSDN
 End
GO
CREATE PROCEDURE dbo.OFS_ImportSDN( @entNum int,      
            @name SanctionName,      
            @firstName SanctionName,      
            @middleName SanctionName,      
            @lastName SanctionName,      
            @type char (12),      
            @program char(15),      
            @title varchar(180),      
            @callSign char(8),      
            @vessType char(25),      
            @tonnage char(14),      
            @grt char(8),      
            @vessFlag varchar(40),      
            @vessOwner varchar(150) ,      
            @dob varchar(150) ,      
            @remarks text ,      
            @remarks1 varchar(255) ,      
            @remarks2 varchar(255) ,      
            @sdnType int,      
            @dataType int,      
            @userRec int ,      
            @country varchar(75),      
            @sex char,      
            @build varchar(15),      
            @complexion varchar(15),      
            @race varchar(15),      
            @eyes varchar(15),      
            @hair varchar(15),      
            @height int,      
            @weight int,      
            @listID varchar(15),      
            @listType varchar(15),      
            @primeAdded bit,      
            @standardOrder bit,      
            @effectiveDate datetime,      
            @expiryDate datetime,      
            @status int,      
            @listCreateDate datetime,      
            @listModifDate dateTime,      
			@oper Code,      
            @ignDrvNames BIT,       
  		    @retEntnum int OUTPUT,      
		    @retStatus int OUTPUT)      
WITH ENCRYPTION       
AS      
BEGIN      
  declare @event   varchar(3)      
  --Transaction       
  declare @trnCnt        int      
  -- Processing starts here:      
  declare @now datetime      
  declare @pos int      
  declare @stat int      
  declare @text varchar(255)      
  declare @maxNum int      
  
  select @now = GETDATE()      
  
  select @trnCnt = @@trancount  -- Save the current trancount      
  
  if @trnCnt = 0      
  -- Transaction has not begun      
  begin tran OFS_ImportSDN      
  else      
  -- Already in a transaction      
  save tran OFS_ImportSDN      
  
  Declare @list TABLE(Code VARCHAR(20))      
  insert into @list      
  SELECT code FROM ListType      
  WHERE code in      
  (      
  SELECT listtype from dbo.OFS_GetScopeForListType a       
   INNER JOIN psec.dbo.SEC_fnGetScopeForOperrights(@oper, 'MODOFACLST,DSPOFACLST') b --Rights hardcoded  
      ON a.branchdept = b.branchdept      
  )       
  AND enabled = 1 and isprimelist = 0      
      
	  if NOT Exists (Select Code from @list where code=@listType)      
	  Begin   
			Select @retStatus=201 --No Access Rights Value.  
			Select @retEntNum=0  
			SET @STAT = 1       
			GOTO ENDOFPROC      
	  END  
	  ELSE  
	  BEGIN   
		   If @type = ''      
		   select @type = 'other'      
		   Select ISNULL(@ignDrvNames, 1)      
		   If Exists(SELECT EntNum FROM SDNTable WHERE ListId=@listid AND userrec=@userRec)      
		   BEGIN      
				BEGIN TRY  
				  SELECT @EntNum=EntNum FROM SDNTable WHERE ListId=@listid AND userrec=@userRec      
				  UPDATE SDNTable SET      
				   [name]=@name,      
				   firstName=@firstName ,      
				   middleName=@middleName,      
				   lastName=@lastName ,      
				   type=@type ,      
				   program=UPPER(@program) ,      
				   title=@title ,      
				   callSign=@callSign ,      
				   vessType=@vessType ,      
				   tonnage=@tonnage ,      
				   grt=@grt ,      
				   vessFlag=@vessFlag ,      
				   vessOwner=@vessOwner ,      
				   dob=@dob ,      
				   remarks=@remarks ,      
				   remarks1=@remarks1 ,      
				   remarks2=@remarks2 ,      
				   sdnType=@sdnType ,      
				   dataType=@dataType ,      
				   userRec=@userRec ,      
				   IgnoreDerived = @ignDrvNames ,      
				   country=@country ,      
				   sex=@sex ,      
				   build=@build ,      
				   complexion=@complexion ,      
				   race=@race ,      
				   eyes=@eyes ,      
				   hair=@hair ,      
				   height=@height ,      
				   weight=@weight ,      
				   listID=@listID,      
				   listType=@listType ,      
				   primeAdded=@primeAdded ,      
				   standardOrder=@standardOrder ,      
				   expiryDate=@expiryDate ,      
				   status=3 , --Insert Status Value     
				   listModifDate=@listModifDate , 
				   LastModifDate=@now,   
				   lastoper=@oper      
				  WHERE EntNum = @EntNum      
				  delete from Notes where entnum = @EntNum      
				  delete from sdnaddrtable where entnum = @EntNum      
				  delete from sdnalttable where entnum = @EntNum      
				  delete from urls where entnum = @EntNum      
				  delete from dobs where entnum = @EntNum      
				  delete from relationship where entnum = @EntNum      
				  delete from relationship where relatedid = @EntNum      
				  Select @retStatus=203 --Update Values.     
				  Select @event = 'MOD'      
				END TRY  
				BEGIN CATCH  
				   SELECT ERROR_NUMBER()   
				   Select @stat = @@ERROR      
				   Select @retStatus=204  
				   GOTO ENDOFPROC  
				END CATCH  
		   END      
		   ELSE      
		   BEGIN      
			 BEGIN TRY  
				  Select @maxNum = Max(EntNum) From SDNTable       
				  If @maxNum Is NULL Set @maxNum = 0      
				  if @maxNum < 10000000      
				   Set @maxNum = 10000000      
				  else      
				   Set @maxNum = @maxNum +1      
				  Set @entNum = @maxNum      
			  
				  Insert into SDNTable (EntNum, [Name], FirstName,      
				   MiddleName, LastName,Title,      
				   Program, Type, CallSign,      
				   VessType, Tonnage, GRT,      
				   VessFlag, VessOwner, Dob,      
				   Remarks, Remarks1, Remarks2,SDNType,      
				   DataType, userRec, deleted,      
				   duplicRec, IgnoreDerived,      
				   ListID, ListType, Country,      
				   Sex, Build, Height, Weight,      
				   Race, Complexion, eyes, hair,      
				   PrimeAdded, StandardOrder, EffectiveDate,      
				   ExpiryDate, Status, ListCreateDate,      
				   ListModifDate, CreateDate, LastModifDate,      
				   LastOper)      
				  Values      
				   (@entNum, @name, NULLIF(@firstName,''),      
				   NULLIF(@middleName,''), NULLIF(@lastName,''), @title,      
				   UPPER(@program),  @type,  @callSign,      
				   @vessType, @tonnage, @grt,      
				   @vessFlag, @vessOwner, NULLIF(@dob,''),      
				   @remarks, @remarks1, @remarks2, @sdnType,      
				   @dataType, @userRec, 0,      
				   0, @ignDrvNames,@listId, @listType, @country,      
				   @sex, @build, NullIf(@height,0), NullIf(@weight,0),      
				   @race, @complexion, @eyes, @hair,@primeAdded,  
				   @standardOrder, @effectiveDate,@expiryDate, 2,  
				   @listCreateDate,@listModifDate, @now, @now, @oper )      
				  Select @retStatus=202 --Insert Status Value     
				  Select @event = 'CRE'      
			 END TRY  
			 BEGIN CATCH  
				  SELECT ERROR_NUMBER()   
				  Select @stat = @@ERROR      
				  Select @retStatus=204  
				  GOTO ENDOFPROC  
			 END CATCH  
			END      
	  
		Select @retEntNum = @entNum      
	        
		If @stat = 0 Begin      
			 Exec OFS_InsEvent @oper, @event, 'SanName', @name, Null      
			 select @stat = @@ERROR      
			 If @stat = 0 Begin      
				 Exec OFS_SetFileImageFlag      
			 End      
		End      
    
  End  
  EndofProc:      
  IF @trnCnt = 0 BEGIN      
	   COMMIT TRAN OFS_ImportSDN
	   END ELSE IF @stat <> 0 BEGIN      
	   ROLLBACK TRAN OFS_ImportSDN      
  END     
         
RETURN @stat      
END 
GO

-- DspFimgLdis: Option to Display File Image and Load Distribution links
IF Not EXISTS(select * From OptionTbl Where Code = 'DspFimgLdis')
	insert into OptionTbl ( Code, Name, Enabled, ModEnable, CreateOper,
		   CreateDate, LastOper, LastModify )
    values ( 'DspFimgLdis', 'Display File Image and Load Distribution links', 1, 0, 'Prime',
		   GetDate(), 'Prime', GetDate() )
go

-----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_GetDowJonesPFAMaxListDate]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_GetDowJonesPFAMaxListDate]
Go

CREATE PROCEDURE OFS_GetDowJonesPFAMaxListDate

WITH ENCRYPTION AS
BEGIN
SELECT  
	MAX(ListcreateDate) 
FROM 
	SDNTable 
 WHERE 
	ListID like 'DJ!_%' ESCAPE '!'
END
GO
-----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_GetDowJonesAMEMaxListDate]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_GetDowJonesAMEMaxListDate]
Go

CREATE PROCEDURE OFS_GetDowJonesAMEMaxListDate

WITH ENCRYPTION AS
BEGIN
SELECT  
	MAX(ListcreateDate) 
FROM 
	SDNTable 
 WHERE 
	ListID like 'DJAME!_%' ESCAPE '!'
END
GO
-----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_GetDowJonesACMaxListDate]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_GetDowJonesACMaxListDate]
Go

CREATE PROCEDURE OFS_GetDowJonesACMaxListDate

WITH ENCRYPTION AS
BEGIN
SELECT  
	MAX(ListcreateDate) 
FROM 
	SDNTable 
 WHERE 
	ListID like 'DJAC!_%' ESCAPE '!'
END
GO
----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_AddUpdateDowJonesSDN]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_AddUpdateDowJonesSDN]
Go

CREATE PROCEDURE OFS_AddUpdateDowJonesSDN( @entNum int,
                            @name SanctionName,
                            @firstName SanctionName,
                            @middleName SanctionName,
                            @lastName SanctionName,
                            @type char (12),
                            @program char(15),
                            @title varchar(180),
                            @callSign char(8),
                            @vessType char(25),
                            @tonnage char(14),
                            @grt char(8),
                            @vessFlag varchar(40),
                            @vessOwner varchar(150) ,
                            @dob varchar(150) ,
                            @remarks text ,
							@remarks1 varchar(255) ,
							@remarks2 varchar(255) ,
                            @sdnType int,
                            @dataType int,
                            @userRec int ,
                            @sex char,
                            @listID varchar(15),
                            @listType varchar(15),
                            @primeAdded bit,
                            --@standardOrder bit,
                            @effectiveDate datetime,
                            @expiryDate datetime,
                            @status int,
                            @listCreateDate datetime,
                            @listModifDate dateTime,
                            @oper Code,
                            @ignDrvNames BIT, 
							@retEntnum int OUTPUT)
WITH ENCRYPTION 
AS
BEGIN
  declare @event		 varchar(3)
  --Transaction 
  declare @trnCnt        int
  -- Processing starts here:
  declare @now datetime
  declare @pos int
  declare @stat int
  declare @text varchar(255)
  declare @maxCurrentNum int
  declare @minSDNIndex int = 7000000
  declare @maxSDNIndex int = 9999999

  select @now = GETDATE()

  select @trnCnt = @@trancount  -- Save the current trancount

  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_AddUpdateDowJonesSDN
  else
    -- Already in a transaction
    save tran OFS_AddUpdateDowJonesSDN

  --If @type = ''
   -- select @type = 'other'

    Select ISNULL(@ignDrvNames, 1)

If Exists(SELECT EntNum FROM SDNTable WITH(NOLOCK) WHERE ListId=@listid AND Program=@program AND [Type] = @type)
BEGIN
	SELECT @EntNum=EntNum FROM SDNTable WITH(NOLOCK) WHERE ListId=@listid AND Program=@program AND [Type] = @type
	UPDATE SDNTable SET
                            [name]=@name,
                            firstName=@firstName ,
                            middleName=@middleName,
                            lastName=@lastName ,
                            type=@type ,
                            program=UPPER(@program) ,
                            title=@title ,
                            callSign=@callSign ,
                            vessType=@vessType ,
                            tonnage=@tonnage ,
                            grt=@grt ,
                            vessFlag=@vessFlag ,
                            vessOwner=@vessOwner ,
                            dob=@dob ,
                            remarks=@remarks ,
							remarks1=@remarks1 ,
							remarks2=@remarks2 ,
                            sdnType=@sdnType ,
                            dataType=@dataType ,
                            userRec=@userRec ,
                            IgnoreDerived = @ignDrvNames ,
                            sex=@sex ,
                            listID=@listID ,
                            listType=@listType ,
                            primeAdded=@primeAdded ,
                            --standardOrder=@standardOrder ,
                            --effectiveDate=@effectiveDate ,
                            --expiryDate=@expiryDate ,
                            status=@status ,
                            --listCreateDate=@listCreateDate ,
                            listModifDate=@listModifDate ,
                            lastoper=@oper ,
                            LastModifDate=@now
		WHERE EntNum = @EntNum

  SELECT @STAT = @@ERROR
  IF @STAT <> 0  GOTO ENDOFPROC


	update Notes
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	update sdnaddrtable
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	update sdnalttable
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	update urls 
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	Update dobs
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	Update relationship
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC


   Select @event = 'MOD'
END
ELSE
BEGIN
		Select @maxCurrentNum = Max(EntNum) From SDNTable WHERE EntNum between @minSDNIndex and @maxSDNIndex
		Select @stat = @@ERROR

		If @stat <> 0 Begin
			return @stat
		End

		If @maxCurrentNum Is NULL Set @maxCurrentNum = 0
		if @maxCurrentNum < @minSDNIndex
		  Set @maxCurrentNum = @minSDNIndex
		else
		  Set @maxCurrentNum = @maxCurrentNum +1
		Set @entNum = @maxCurrentNum

		Insert into SDNTable (EntNum, [Name], FirstName,
				MiddleName, LastName,Title,
				Program,Type, 
				CallSign,
				VessType, Tonnage, GRT,
				VessFlag, VessOwner, Dob,
				Remarks, Remarks1, Remarks2, SDNType,
				DataType, userRec, deleted,
				duplicRec, IgnoreDerived,
				ListID, ListType,
				Sex, 
				PrimeAdded, --StandardOrder, 
				--EffectiveDate,ExpiryDate, 
				[Status], ListCreateDate,
				ListModifDate, CreateDate, LastModifDate,
				LastOper)
		Values
			   (@entNum, @name, NULLIF(@firstName,''),
				NULLIF(@middleName,''), NULLIF(@lastName,''), @title,
				UPPER(@program), @type
				, @callSign,
				@vessType, @tonnage, @grt,
				@vessFlag, @vessOwner, NULLIF(@dob,''),
				@remarks,
				@remarks1,
				@remarks2,				
				@sdnType,
				@dataType, @userRec, 0,
				0, @ignDrvNames,
				@listId, @listType,
				@sex,
				@primeAdded, --@standardOrder,
				 --@effectiveDate,@expiryDate, 
				@status, @listCreateDate,
				@listModifDate, @now, @now, @oper)
  Select @stat = @@ERROR
  IF @STAT <> 0  GOTO ENDOFPROC
		Select @event = 'CRE'
END
  Select @retEntNum = @entNum
    If @stat = 0 Begin
      Exec OFS_InsEvent @oper, @event, 'SanName', @name, Null
      select @stat = @@ERROR
      If @stat = 0 Begin
          Exec OFS_SetFileImageFlag
      End
  End
  
  EndofProc:
  IF @trnCnt = 0 BEGIN
	COMMIT TRAN OFS_AddUpdateDowJonesSDN
  END ELSE IF @stat <> 0 BEGIN
	ROLLBACK TRAN OFS_AddUpdateDowJonesSDN
  END 
  RETURN @stat
END
GO
----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_InsDowJonesRelationship]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_InsDowJonesRelationship]
Go

Create PROCEDURE OFS_InsDowJonesRelationship (
            @RelationshipId     IDType,
            @entnum             IDType,
            @relatedid          IDType,
            @relationshipPtoR   varchar(50),
            @relationshipRtoP   varchar(50),
            @status             int,
            @listCreateDate     Datetime,
            @listModifDate      Datetime,
            @oper               SCode
) WITH ENCRYPTION AS
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_InsWorldCheckRelationship
  else -- Already in a transaction
    save tran OFS_InsWorldCheckRelationship
  -- End standard stored procedure transaction header

  declare @now datetime
  declare @stat int

  declare @Existing  int 
  declare @ChildExisting int
  declare @Unchanged int 
  declare @minSDNIndex int = 7000000
  declare @maxSDNIndex int = 9999999

  set @Existing  = 0
  set @Unchanged = 0

  select @now = GETDATE()

  --World Check block. Persist ID, Set status
  if @entNum between @minSDNIndex and @maxSDNIndex
   begin

		if ( @RelationshipId != 0)
		 begin
			--Check by Primary Key
			select @Existing = count(RelationshipId)
			from Relationship WITH(NOLOCK)
			where EntNum = @entNum and 
				  RelationshipId = @RelationshipId and
				  RelatedId = @RelatedId and
				  RelationshipPToR  = @relationshipPtoR and
				  RelationshipRToP = @relationshipRtoP

			if @Existing = 0
			 begin
				--Insert new record
				set  @RelationshipId  = 0
			 end
			else
			 begin
				-- Existing record unchanged
				update Relationship
				set Status = 1 --unchanged
				where EntNum = @entNum and 
				      RelationshipId = @RelationshipId
			 end	
		  end

		if @Existing = 0
		 begin
			--Find record by Relationship ID
			select @Existing = count(RelationshipId)
			from Relationship WITH(NOLOCK)
			where EntNum = @entNum and 
				  Status = 4 and
				  RelatedId = @RelatedId
              
        
			if @Existing = 0
				set @status = 2 --added
			else
			 begin
				select @Unchanged = count(RelationshipId)
				from Relationship WITH(NOLOCK)
				where EntNum = @entNum and 
					  RelatedId = @RelatedId and
					  RelationshipPToR  = @relationshipPtoR and
					  IsNull(RelationshipRToP, '') = IsNull(@relationshipRtoP, '')

				if @Existing = 1
				 begin
					--Unique entry exists
					if @Unchanged  = 0
						update Relationship
						set  RelationshipPToR  = @relationshipPtoR,
							 RelationshipRToP  = @relationshipRtoP,
							 ListModifDate = @listModifDate,
							 LastModify =  @now, 
							 LastOper = @oper,
							 Status = 3 --Modified					 
						where EntNum  = @entNum and
							  Status = 4 and 
							  RelatedId = @RelatedId
					else
						update Relationship
						set  Status = 1 -- unchanged					 
						where EntNum  = @entNum and
							  Status = @status and 
							  RelatedId = @RelatedId
				   
                      
				  end
				else if @Existing > 1 and @Unchanged  > 1
				  begin
					--Existing entry is not unique but no changes
					update Relationship
					set  Status = 1 -- unchanged					 
					where EntNum = @entNum and 
						  Status = @status and
						  RelatedId = @RelatedId and 
						  RelationshipPToR  = @relationshipPtoR and
						  RelationshipRToP = @relationshipRtoP
				  end
				else
				  begin
					--Set new entry
					set @Existing = 0
					set @status = 2 --added
				  end
			end
		end
   end

  if  @Existing = 0
   begin
		if @RelationshipId = 0
		 begin
			--if there are no notes available in the notes table
			--then set the notesid to 1
			select @RelationshipId = max(RelationshipId) from relationship WITH(NOLOCK)
			select @RelationshipId = isnull(@RelationshipId, 10000000)
			if @RelationshipId < 10000000
				SET @RelationshipId = 10000000

			select @RelationshipId = @RelationshipId + 1
	     end

		-- Count the number of reverse relationship descriptions in the database
	     select @ChildExisting = count(RelationshipId)
				from Relationship WITH(NOLOCK)
				Where EntNum = @relatedid
				AND RelatedID = @entnum
	     
	     -- if there is only one reverse relationship:
	     --   * get its description and add it to our reverse description variable
	     --   * update this other entry with our current Description as its reverse Description
	     if @ChildExisting = 1
	     Begin
			
			Select @relationshipRtoP = RelationshipPToR From Relationship WITH(NOLOCK) Where EntNum = @relatedid
			 AND RelatedID = @entnum
	     
			Update Relationship
			Set RelationshipRToP = @relationshipPtoR
			Where EntNum = @relatedid
			 AND RelatedID = @entnum
	     End
	     

		insert into Relationship (RelationshipId, EntNum, RelatedID, RelationshipPToR,
                    RelationshipRToP, Status, ListCreateDate, ListModifDate,
                    CreateOper, CreateDate, LastOper, LastModify)
		values (@RelationshipId, @entnum, @relatedid, @relationshipPtoR,
			    @relationshipRtoP, @status, @listCreateDate,
				@listModifDate, @oper, @now, @oper, @now)
 end


  -- Start standard stored procedure transaction footer
  select @stat = @@ERROR
  if @stat = 0
  begin
    if @trnCnt = 0 commit tran OFS_InsWorldCheckRelationship
  end
  else
    rollback tran OFS_InsWorldCheckRelationship
  return @stat
  -- End standard stored procedure transaction footer
GO
----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_GetDowJonesEntNum]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_GetDowJonesEntNum]
Go

CREATE PROCEDURE OFS_GetDowJonesEntNum(
 @ListId VARCHAR(20)
)
WITH ENCRYPTION AS
BEGIN

select EntNum from SDNTable WITH(NOLOCK) where ListID = @ListId

END
GO
----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_CRYDowJonesImportStats]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_CRYDowJonesImportStats]
Go

CREATE procedure [dbo].[OFS_CRYDowJonesImportStats] @AsOfDate datetime
With Encryption
as
 Set NOCOUNT ON
begin
	declare  @stats table
	( 
      StatType varchar(30),
	  InputFile varchar(1000),
	  Total varchar(10),
	  Added varchar(10),
	  Modified varchar(10),
	  Deleted varchar(10),
	  Errors Int,
	  LogDate datetime
	)
	IF(@AsOfDate IS NOT NULL)
		Set @AsOfDate = DATEADD(SECOND, -1, Cast(DATEADD(DAY, 1, Cast(@AsOfDate As Date)) AS datetime))
	
	insert into @stats
		select 
		t.Name, 
		substring(e.LogText,0 ,patindex('%Total Records%',e.LogText)-2) as InputFile,
		substring(e.LogText, patindex('%Total Records%',e.LogText)+14 ,patindex('%Added%',e.LogText)- (patindex('%Total Records%',e.LogText)+16)) as Total,
		substring(e.LogText, patindex('%Added: %',e.LogText)+7 ,patindex('% Modified:%',e.LogText)- (patindex('%Added: %',e.LogText)+8)) as Added,
		substring(e.LogText, patindex('%Modified: %',e.LogText)+10 ,patindex('% Deleted:%',e.LogText)- (patindex('%Modified: %',e.LogText)+11)) as Modified,
		substring(e.LogText, patindex('%Deleted:%',e.LogText)+9 ,patindex('% Errors:%',e.LogText)- (patindex('%Deleted:%',e.LogText)+10)) as Deleted,
		substring(e.LogText, patindex('%Errors:%',e.LogText)+8 ,Len(e.LogText)- (patindex('%Errors:%',e.LogText)+7)) as Errors,
		e.LogDate
	from [SDNChangeLog] e WITH(NOLOCK)
		join ObjectType t on e.ObjectType = t.Code
	where ObjectType ='DowJones' and ObjectId='Various'
	AND LogDate <= coalesce(@AsOfDate, GETDATE())
	AND CHARINDEX('Total Records',e.LogText) > 0
	
	select StatType, InputFile,Msg = 
	Convert(Varchar(100), case
		when Errors=0 
			then 'All Records were loaded successfully'
		when Errors > 0 
			then 'All Records were not loaded successfully'
	end),
	Total,
	Added,
	Modified,
	Deleted, 
	Errors,
	LogDate
	from @stats
	Order By LogDate Desc
end

GO
----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_CRYDowJonesSancChangeList]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_CRYDowJonesSancChangeList]
Go

CREATE PROCEDURE [dbo].[OFS_CRYDowJonesSancChangeList] (@calling_oper AS Code)
With Encryption
 As
	 Set NOCOUNT ON

	Declare @lastSdnUpdDate datetime

 Select @lastSdnUpdDate = IsNull(LastSDNUpdate,0) from SDNDBStatus WITH(NOLOCK)

 Select [Name], Program, Type, Status, Deleted, ListType, DataType
    From SDNTable WITH(NOLOCK) WHERE 
	(IsNull(LastmodifDate,0) > @lastSdnUpdDate OR
		IsNull(CreateDate,0) > @lastSdnUpdDate) 
	AND (Status in (2,3,4))
	AND LISTTYPE IN
		(
			SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 135) 
			INTERSECT
			SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 100)
		)
	AND Program like 'DJ-%'	
     Order by ListType, Deleted
Go
----------------------------------------------------------------------------------------------
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 55 )
	INSERT INTO [NoteType] ([Code], [Name],[CreateOper],[LastModify],[LastOper])VALUES
	(55,'Country','Prime',GetDate(),'Prime')
Go

IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 54)
	INSERT INTO [NoteType] ([Code], [Name],[CreateOper],[LastModify],[LastOper])VALUES
	(54,'SanctionReferenceName','Prime',GetDate(),'Prime')
Go

IF Not Exists(SELECT * FROM [ObjectType] WHERE Code = 'DowJones')
INSERT INTO [ObjectType] ([Code], [Name],[CreateOper],[CreateDate],[LastOper])VALUES
	('DowJones','Dow Jones','Prime',GetDate(),'Prime')
Go
IF exists (SELECT * FROM [NoteType] WHERE Code = 56 AND [Name] = 'CountryType')
Delete from [NoteType] where Code =56 AND [Name] = 'CountryType' 
Go
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 56)
INSERT INTO [NoteType] ([Code],[Name],[CreateOper],[LastModify],[LastOper]) VALUES
    (56,'Date of Registration','Prime',GetDate(),'Prime')
GO
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 57)
INSERT INTO [NoteType] ([Code],[Name],[CreateOper],[LastModify],[LastOper]) VALUES
    (57,'Date of Nationalisation','Prime',GetDate(),'Prime')
GO
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 58)
INSERT INTO [NoteType] ([Code],[Name],[CreateOper],[LastModify],[LastOper]) VALUES
    (58,'Date of Privatisation','Prime',GetDate(),'Prime')
GO
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 59)
INSERT INTO [NoteType] ([Code],[Name],[CreateOper],[LastModify],[LastOper]) VALUES
    (59,'Date of Cessation','Prime',GetDate(),'Prime')
GO
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 60)
INSERT INTO [NoteType] ([Code],[Name],[CreateOper],[LastModify],[LastOper]) VALUES
    (60,'Additional Date','Prime',GetDate(),'Prime')
GO
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 61)
INSERT INTO [NoteType] ([Code],[Name],[CreateOper],[LastModify],[LastOper]) VALUES
    (61,'Reference Source','Prime',GetDate(),'Prime')
GO
----------------------------------------------------------------------------------------------
IF exists (SELECT * FROM [Report] WHERE Code = 'DJChgRpt' and Name = 'Dow Jones Data Load Difference Report')
Delete from [Report] where Code = 'DJChgRpt' and Name = 'Dow Jones Data Load Difference Report'
Go

IF Not Exists(SELECT * FROM [Report] WHERE Code = 'DJChgRpt' and Name = 'Dow Jones Data Load Difference Report')
insert into Report ( Code, Name, Category, FileName, Enabled,  ReportType,
            ReportLoc, CreateOper,CreateDate, LastOper, LastModify )
  values ( 'DJChgRpt', 'Dow Jones Data Load Difference Report',
           'Audit Reports', 'DJChgList.rpt', 1, 1, 1, 'Prime',
           GetDate(), 'Prime', GetDate() )           
GO    

IF exists (SELECT * FROM [Report] WHERE Code = 'DJDataLoad' and Name = 'Dow Jones Data Load Report')
Delete from [Report] where Code = 'DJDataLoad' and Name = 'Dow Jones Data Load Report'
Go
       
IF Not Exists(SELECT * FROM [Report] WHERE Code = 'DJDataLoad' and Name = 'Dow Jones Data Load Report')
insert into Report ( Code, Name, Category, FileName, Enabled,  ReportType,
            ReportLoc, CreateOper,CreateDate, LastOper, LastModify )
  values ( 'DJDataLoad', 'Dow Jones Data Load Report',
           'Audit Reports', 'DJDataLoad.rpt', 1, 1, 1, 'Prime',
           GetDate(), 'Prime', GetDate() ) 
GO

IF EXISTS (SELECT *
           FROM   dbo.sysobjects
           WHERE  id = Object_id(N'[dbo].[OFS_AddSDNAlt]')
                  AND Objectproperty(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[OFS_AddSDNAlt]

go

CREATE PROCEDURE dbo.Ofs_addsdnalt @entNum         INT,
                                   @altNum         INT,
                                   @altType        CHAR(8),
                                   @altName        SANCTIONNAME,
                                   @firstName      SANCTIONNAME,
                                   @middleNames    SANCTIONNAME,
                                   @lastName       SANCTIONNAME,
                                   @remarks        VARCHAR(200),
                                   @userRec        INT,
                                   @listId         VARCHAR(15),
                                   @status         INT,
                                   @ListCreateDate DATETIME,
                                   @listModifDate  DATETIME,
                                   @oper           CODE
WITH encryption
AS
    DECLARE @trnCnt INT
	DECLARE @minSDNIndex int = 7000000
	DECLARE @maxSDNIndex int = 9999999
    SELECT @trnCnt = @@trancount -- Save the current trancount
    --Do not proceed with null in Alt. Names
    IF @altName IS NULL
       AND @entNum BETWEEN @minSDNIndex AND @maxSDNIndex
      RETURN 0

    IF @trnCnt = 0
      -- Transaction has not begun
      BEGIN TRAN ofs_addsdnalt
    ELSE
      -- Already in a transaction
      SAVE TRAN ofs_addsdnalt

    -- Processing starts here:
    DECLARE @now DATETIME
    DECLARE @stat INT
    DECLARE @text VARCHAR(255)
    DECLARE @Existing INT
    DECLARE @Unchanged INT

    SET @Existing = 0
    SET @Unchanged = 0

    SELECT @now = Getdate()

    IF Len(Rtrim(Ltrim(@altType))) = 0
      SELECT @altType = 'a.k.a.'

    --World Check block. Persist ID, Set status
    IF @entNum BETWEEN @minSDNIndex AND @maxSDNIndex
      BEGIN
          IF ( @altNum != 0 )
            BEGIN
                --Check by Primary Key
                SELECT @Existing = Count(altnum)
                FROM   sdnalttable
                WHERE  entnum = @entNum
                       AND altnum = @altnum
                       AND Isnull(altname, '') = Isnull(@altName, '')
                       AND Isnull(alttype, '') = Isnull(@altType, '')
                       AND Isnull(firstname, '') = Isnull(@firstName, '')
                       AND Isnull(middlename, '') = Isnull(@middleNames, '')
                       AND Isnull(lastname, '') = Isnull(@lastName, '')
                       AND Isnull(listid, '') = Isnull(@ListID, '')
                       AND Isnull(remarks, '') = Isnull(@remarks, '')

                IF @Existing = 0
                  BEGIN
                      --Insert New record
                      SET @altNum = 0
                  END
                ELSE
                  BEGIN
                      -- Existing record unchanged
                      UPDATE sdnalttable
                      SET    status = 1 --unchanged
                      WHERE  entnum = @entNum
                             AND altnum = @altnum
                  END
            END

          IF @Existing = 0
            BEGIN
                --Find record by alt name
                SELECT @Existing = Count(altnum)
                FROM   sdnalttable
                WHERE  entnum = @entNum
                       AND status = 4
                       AND Isnull(altname, '') = Isnull(@altName, '')

                IF @Existing = 0
                  SET @status = 2 --added
                ELSE
                  BEGIN
                      SELECT @Unchanged = Count(altnum)
                      FROM   sdnalttable
                      WHERE  entnum = @entNum
                             AND status = 4
                             AND Isnull(altname, '') = Isnull(@altName, '')
                             AND Isnull(alttype, '') = Isnull(@altType, '')
                             AND Isnull(firstname, '') = Isnull(@firstName, '')
                             AND Isnull(middlename, '') =
                                 Isnull(@middleNames, '')
                             AND Isnull(lastname, '') = Isnull(@lastName, '')
                             AND Isnull(listid, '') = Isnull(@ListID, '')
                             AND Isnull(remarks, '') = Isnull(@remarks, '')

                      IF @Existing = 1
                        BEGIN
                            --Unique entry exists
                            IF @Unchanged = 0
                              UPDATE sdnalttable
                              SET    alttype = @altType,
                                     firstname = @firstName,
                                     middlename = @middleNames,
                                     lastname = @lastName,
                                     listid = @ListID,
                                     remarks = @remarks,
                                     listmodifdate = @listModifDate,
                                     lastmodifdate = @now,
                                     lastoper = @oper,
                                     status = 3 --Modified					 
                              WHERE  entnum = @entNum
                                     AND status = 4
                                     AND Isnull(altname, '') =
                                         Isnull(@altName, '')
                            ELSE
                              UPDATE sdnalttable
                              SET    status = 1 -- unchanged					 
                              WHERE  entnum = @entNum
                                     AND status = 4
                                     AND Isnull(altname, '') =
                                         Isnull(@altName, '')
                        END
                      ELSE IF @Existing > 1
                         AND @Unchanged > 1
                        BEGIN
                            --Existing entry is not unique but no changes
                            UPDATE sdnalttable
                            SET    status = 1 -- unchanged					 
                            WHERE  entnum = @entNum
                                   AND status = 4
                                   AND Isnull(altname, '') =
                                       Isnull(@altName, '')
                                   AND Isnull(alttype, '') =
                                       Isnull(@altType, '')
                                   AND Isnull(firstname, '') =
                                       Isnull(@firstName, '')
                                   AND Isnull(middlename, '') =
                                       Isnull(@middleNames, '')
                                   AND Isnull(lastname, '') =
                                       Isnull(@lastName, '')
                                   AND Isnull(listid, '') = Isnull(@ListID, '')
                                   AND Isnull(remarks, '') =
                                       Isnull(@remarks, '')
                        END
                      ELSE
                        BEGIN
                            --Set new entry
                            SET @Existing = 0
                            SET @status = 2 --added
                        END
                  END
            END

          IF EXISTS (SELECT altnum
                     FROM   sdnalttable
                     WHERE  entnum = @entNum
                            AND Isnull(altname, '') = Isnull(@altName, '')
                            AND Isnull(alttype, '') = Isnull(@altType, ''))
            BEGIN
                --Do not insert duplicated Alt Names with different case of letters
                IF Isnull(@status, 0) = 0 -- if unknown status
                  SET @status = 2 -- set new status
                UPDATE sdnalttable
                SET    alttype = @altType,
                       firstname = @firstName,
                       middlename = @middleNames,
                       lastname = @lastName,
                       listid = @ListID,
                       remarks = @remarks,
                       [status] = @status,
                       --listcreatedate = @listCreateDate,
                       listmodifdate = @listModifDate,
                       lastmodifdate = @now,
                       lastoper = @oper
                WHERE  entnum = @entNum
                       AND Isnull(altname, '') = Isnull(@altName, '')
                       AND Isnull(alttype, '') = Isnull(@altType, '')

                SET @Existing = 1
            END
      END

    IF @Existing = 0
      BEGIN
          -- the following if block is added for
          -- user entered alt names
          IF @altNum = 0
            BEGIN
                --if there are no altnames available in the sdnalttable table
                --then set the altnum to 10000000
                SELECT @altNum = Max(altnum)
                FROM   sdnalttable (nolock)

                SELECT @altNum = Isnull(@altNum, 10000000)

                IF @altNum < 10000000
                  SET @altnum = 10000000

                SELECT @altNum = @altNum + 1
            END

          IF Isnull(@status, 0) = 0 -- if unknown status
            SET @status = 2 -- set new status
          INSERT INTO sdnalttable
                      (entnum,
                       altnum,
                       alttype,
                       altname,
                       firstname,
                       middlename,
                       lastname,
                       listid,
                       remarks,
                       status,
                       listcreatedate,
                       listmodifdate,
                       createdate,
                       lastmodifdate,
                       lastoper)
          VALUES      (@entNum,
                       @altNum,
                       @altType,
                       @altName,
                       @firstName,
                       @middleNames,
                       @lastName,
                       @ListID,
                       @remarks,
                       @status,
                       @listCreateDate,
                       @listModifDate,
                       @now,
                       @now,
                       @oper )
      END

    SELECT @stat = @@ERROR

    IF @stat = 0
      BEGIN
          EXEC Ofs_setfileimageflag

          IF @trnCnt = 0
            COMMIT TRANSACTION ofs_addsdnalt
      END
    ELSE
      ROLLBACK TRANSACTION ofs_addsdnalt

    RETURN @stat

go

------------------------------------------------------
--      Add Columns 
------------------------------------------------------
if not exists (Select * From INFORMATION_SCHEMA.Columns
  Where table_name = 'SanctionDataRule' and column_name = 'EnablePartialDOBMatching' )
    Begin
        Alter Table SanctionDataRule 
        add EnablePartialDOBMatching bit Not Null
        Constraint DF_SanctionDataRule_EnablePartialDOBMatching Default (0)
    End
go

if not exists (Select * From INFORMATION_SCHEMA.Columns
  Where table_name = 'SanctionDataRule' and column_name = 'DOBToleranceInYears' )
    Begin
        Alter Table SanctionDataRule 
        add DOBToleranceInYears int Not Null
        Constraint DF_SanctionDataRule_DOBToleranceInYears Default (0)
    End
go

-- Update with two new columns: EnablePartialDOBMatching, DOBToleranceInYears
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'OFS_InsSanctionDataRule')
 Begin
	DROP PROCEDURE [dbo].OFS_InsSanctionDataRule
 End
GO

Create Procedure dbo.OFS_InsSanctionDataRule ( @code SCode, @name LongName,
                        @incremental int, @useAddr bit,
                        @useKeyword bit,
                        @branchList varchar(1500),
                        @departmentList varchar(100),
                        @programList varchar(1500),
                        @blockProgramList varchar(1500),
                        @typeList varchar(500),
                        @blockTypeList varchar(500),
                        @excludeBranchList varchar(1500),
                        @excludeDeptList varchar(100),
                        @encWordLen int,
                        @maxEncWords int, @minEncWords int, @minEncChars int,
                        @encFirstChar bit,
                        @ignTrailS bit, @combWords bit, @inEquality bit,
                        @anchorOnFirstName bit, @anchorOnLastName bit,
                        @includeInitials bit, @includeLastName bit,
                        @deriveAllNameCombinations bit,
                        @derivedNameWordsLimit int,
                        @interveningWords int,
                        @doScoring bit,
                        @compTags bit,
                        @annotText varchar(255),
                        @IncrListType int,
                        @ListChangeDays int,
                        @useExcludes bit,
                        @UseUnassignedExcludes bit,
                        @wordWrapMatch bit,
                        @enabled bit,
                        @listTypeList varchar(200),
                        @blockListTypeList varchar(200),
                        @combWords_ListType varchar(150),
						@UseAbbrev bit,
						@UseNoise bit,
						@UseUnassignedAbbreviations bit,
						@UseUnassignedNoiseWords bit,
						@UseUnassignedInEqualities bit,
						@IncludeAbbrBranchDeptList varchar(1500),
						@ExcludeAbbrBranchDeptList varchar(1500),
						@IncludeNoiseBranchDeptList varchar(1500),
						@ExcludeNoiseBranchDeptList varchar(1500),
						@IncludeInEqBranchDeptList varchar(1500),
						@ExcludeInEqBranchDeptList varchar(1500),
						@EnablePartialDOBMatching bit,
						@DOBToleranceInYears int,
                        @oper SCode)
With Encryption 
As

  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount  -- Save the current trancount
  If @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_InsSanctionDataRule
  else
    -- Already in a transaction
    save tran OFS_InsSanctionDataRule
  -- End standard stored procedure transaction header

  declare @stat int
  if (@BranchList = '')
    select @useUnassignedExcludes = 1 -- Include exclude names with unspecified branch
                                      -- and departments when no choices to include are made

  insert into SanctionDataRule 
			( Code, Name, Incremental, UseAddr, UseKeyword,
			BranchList, DepartmentList, ProgramList, BlockProgramList, TypeList,
			BlockTypeList, ExcludeBranchList, ExcludeDeptList,  ListTypeList, BlockListTypeList,
			EncWordLen, MaxEncWords, MinEncWords, MinEncChars,
			EncFirstChar, IgnTrailS, CombWords, Inequality,
			AnchorOnFirstName, AnchorOnLastName, IncludeInitials, IncludeLastName,
			DeriveAllNameCombinations, DerivedNameWordsLimit,InterveningWords,
			DoScoring, CompMsgTagsToExclTags, remarks, IncrListType,
			ListChangeDays, useExcludes, useUnassignedExcludes, WordWrapMatch,
			Enabled, CombWords_ListType, 
			UseAbbrev, UseNoise, UseUnassignedAbbreviations, UseUnassignedNoiseWords,
			UseUnassignedInEqualities, IncludeAbbrevBranchDeptList, ExcludeAbbrevBranchDeptList,
			IncludeNoiseBranchDeptList, ExcludeNoiseBranchDeptList, IncludeInEqBranchDeptList,
			ExcludeInEqBranchDeptList, EnablePartialDOBMatching, DOBToleranceInYears,
			CreateOper, CreateDate )
    values 
			(NullIf(RTrim(@code), ''), NullIf(RTrim(@name), ''), @incremental,
			@useAddr, @useKeyword, NullIf(RTrim(@branchList), ''),
			NullIf(RTrim(@departmentList), ''), NullIf(RTrim(@programList), ''),
			NullIf(RTrim(@blockProgramList), ''), NullIf(RTrim(@typeList), ''),
			NullIf(RTrim(@blockTypeList), ''), NullIf(RTrim(@excludeBranchList), ''),
			NullIf(RTrim(@excludeDeptList), ''), 
			NullIf(RTrim(@listTypeList), ''), NullIf(RTrim(@blockListTypeList), ''),
			@encWordLen, @maxEncWords,
			@minEncWords, @minEncChars,
			@encFirstChar, @ignTrailS, @combWords, @inEquality,
			@anchorOnFirstName , @anchorOnLastName, @includeInitials,
			@includeLastName, @deriveAllNameCombinations,
			@derivedNameWordsLimit,@interveningWords, @doScoring, @compTags,
			@annotText, @IncrListType, @ListChangeDays, @useExcludes, @useUnassignedExcludes,
			@wordWrapMatch, @Enabled, NullIf(RTrim(@CombWords_ListType), ''), 
			@UseAbbrev, @UseNoise, @UseUnassignedAbbreviations, @UseUnassignedNoiseWords,
			@UseUnassignedInEqualities, NullIf(RTrim(@IncludeAbbrBranchDeptList), ''), 
			NullIf(RTrim(@ExcludeAbbrBranchDeptList), ''),
			NullIf(RTrim(@IncludeNoiseBranchDeptList), ''), NullIf(RTrim(@ExcludeNoiseBranchDeptList), ''), 
			NullIf(RTrim(@IncludeInEqBranchDeptList), ''), NullIf(RTrim(@ExcludeInEqBranchDeptList), ''),
			@EnablePartialDOBMatching, @DOBToleranceInYears,
			NullIf(RTrim(@oper),''), GetDate() )

  Select @stat = @@error

  -- Evaluate results of the transaction
  If @stat <> 0 begin
    rollback tran OFS_InsSanctionDataRule
    return @stat
  end

  If @trnCnt = 0
  begin
    commit tran OFS_InsSanctionDataRule
    Exec OFS_SetFileImageFlag
  end
  return @stat
Go

-- Update with two new columns: EnablePartialDOBMatching, DOBToleranceInYears
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'OFS_UpdSanctionDataRule')
 Begin
	DROP PROCEDURE [dbo].OFS_UpdSanctionDataRule
 End
GO

Create Procedure dbo.OFS_UpdSanctionDataRule ( @code SCode, @name LongName,
                        @incremental int, @useAddr bit, @useKeyword bit,
                        @branchList varchar(1500),
                        @departmentList varchar(100),
                        @programList varchar(1500),
                        @blockProgramList varchar(1500),
                        @typeList varchar(500),
                        @blockTypeList varchar(500),
                        @excludeBranchList varchar(1500),
                        @excludeDeptList varchar(100), @encWordLen int,
                        @maxEncWords int, @minEncWords int, @minEncChars int,
                        @encMap char(128), @delimMap char(128),
                        @punctMap char(128), @encFirstChar bit,
                        @ignTrailS bit, @combWords bit,
                        @inEquality bit,
                        @anchorOnFirstName bit, @anchorOnLastName bit,
                        @includeInitials bit, @includeLastName bit,
                        @deriveAllNameCombinations bit,
                        @derivedNameWordsLimit int,
                        @interveningWords int,
                        @DoScoring bit,
                        @compTags bit,
                        @annotText varchar(255),
                        @IncrListType int,
                        @ListChangeDays int,
                        @useExcludes bit,
                        @useUnassignedExcludes bit,
                        @wordWrapMatch bit,
                        @enabled bit,
                        @oper SCode, 
                        @listTypeList varchar(200), 
                        @blockListTypeList varchar(200),
						@combWords_ListType varchar(150),
						@UseAbbrev bit,
						@UseNoise bit,
						@UseUnassignedAbbreviations bit,
						@UseUnassignedNoiseWords bit,
						@UseUnassignedInEqualities bit,
						@IncludeAbbrBranchDeptList varchar(1500),
						@ExcludeAbbrBranchDeptList varchar(1500),
						@IncludeNoiseBranchDeptList varchar(1500),
						@ExcludeNoiseBranchDeptList varchar(1500),
						@IncludeInEqBranchDeptList varchar(1500),
						@ExcludeInEqBranchDeptList varchar(1500),
						@EnablePartialDOBMatching bit,
						@DOBToleranceInYears int,
                        @ts timestamp )
With Encryption 
As

  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount  -- Save the current trancount
  If @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_UpdSanctionDataRule
  else
    -- Already in a transaction
    save tran OFS_UpdSanctionDataRule
  -- End standard stored procedure transaction header

  declare   @stat int,
            @cnt int

  if (@BranchList = '')
    select @useUnassignedExcludes = 1 -- Include exclude names with unspecified branch
                                      -- and departments when no choices to include are made

  Update SanctionDataRule
	Set		Name = NullIf(RTrim(@name), ''),
			[Incremental] = @incremental, UseAddr = @useAddr,
			UseKeyword = @useKeyword,
			UseExcludes = @useExcludes,
			UseUnassignedExcludes = @useUnassignedExcludes,
			enabled = @enabled,
			BranchList = NullIf(RTrim(@branchList), ''),
			DepartmentList = NullIf(RTrim(@departmentList), ''),
			ProgramList = NullIf(RTrim(@programList), ''),
			BlockProgramList = NullIf(RTrim(@blockProgramList), ''),
			TypeList = NullIf(RTrim(@typeList), ''),
			BlockTypeList = NullIf(RTrim(@blockTypeList), ''),
			ExcludeBranchList = NullIf(RTrim(@excludeBranchList), ''),
			ExcludeDeptList = NullIf(RTrim(@excludeDeptList), ''),
			ListTypeList = NullIf(RTrim(@listTypeList), ''),
			BlockListTypeList = NullIf(RTrim(@blockListTypeList), ''),
			EncWordLen = @encWordLen, MaxEncWords = @maxEncWords,
			MinEncWords = @minEncWords, MinEncChars = @minEncChars,
			EncMap = NullIf(RTrim(@encMap), ''),
			DelimMap = NullIf(RTrim(@delimMap), ''),
			PunctMap = NullIf(RTrim(@punctMap), ''),
			EncFirstChar = @encFirstChar, IgnTrailS = @ignTrailS,
			CombWords = @combWords,
			InEquality = @inEquality,
			AnchorOnFirstName = @anchorOnFirstName ,
			AnchorOnLastName = @anchorOnLastName,
			IncludeInitials = @includeInitials,
			IncludeLastName = @includeLastName,
			DeriveAllNameCombinations = @deriveAllNameCombinations,
			DerivedNameWordsLimit = @derivedNameWordsLimit,
			InterveningWords = @interveningWords,
			remarks = @annotText,
			DoScoring = @doScoring,
			CompMsgTagsToExclTags = @compTags,
			IncrListType = @IncrListType,
			ListChangeDays = @ListChangeDays,
			WordWrapMatch = @wordWrapMatch,
			combWords_ListType=NullIf(RTrim(@combWords_ListType), ''),
			UseAbbrev =@UseAbbrev,
			UseNoise =@UseNoise,
			UseUnassignedAbbreviations =@UseUnassignedAbbreviations, 
			UseUnassignedNoiseWords =@UseUnassignedNoiseWords,
			UseUnassignedInEqualities =@UseUnassignedInEqualities,
			IncludeAbbrevBranchDeptList =@IncludeAbbrBranchDeptList,
			ExcludeAbbrevBranchDeptList =@ExcludeAbbrBranchDeptList,
			IncludeNoiseBranchDeptList =@IncludeNoiseBranchDeptList, 
			ExcludeNoiseBranchDeptList =@ExcludeNoiseBranchDeptList,
			IncludeInEqBranchDeptList =@IncludeInEqBranchDeptList,
			ExcludeInEqBranchDeptList =@ExcludeInEqBranchDeptList,
			EnablePartialDOBMatching = @EnablePartialDOBMatching,
			DOBToleranceInYears = @DOBToleranceInYears,
			LastOper = NullIf(RTrim(@Oper), '')
    Where Code = @code and Ts = @ts
  Select @stat = @@error, @cnt = @@rowcount


  -- Evaluate results of the transaction
  If @stat <> 0 or @cnt = 0 begin
    if @@rowcount = 0 begin
      Select @stat = 250001 -- Concurrent Update
    end
    rollback tran OFS_UpdSanctionDataRule
    return @stat
  end

  If @trnCnt = 0
  begin
    commit tran OFS_UpdSanctionDataRule
    Exec OFS_SetFileImageFlag
  end
  return @stat
Go

-- Update with two new columns: EnablePartialDOBMatching, DOBToleranceInYears
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'OFS_SelectDistinctSanctionDataRule')
 Begin
	DROP PROCEDURE [dbo].OFS_SelectDistinctSanctionDataRule
 End
GO

CREATE PROCEDURE dbo.OFS_SelectDistinctSanctionDataRule
With Encryption 
As

  declare @stat int

  select distinct Code, Name, Incremental, UseAddr, UseKeyword, BranchList,
    DepartmentList, ProgramList, BlockProgramList, TypeList, BlockTypeList,
    ExcludeBranchList, ExcludeDeptList, ListTypeList, BlockListTypeList, EncWordLen, MaxEncWords,
    MinEncWords, MinEncChars, EncMap, DelimMap, PunctMap, EncFirstChar,
    IgnTrailS, CombWords, InEquality, AnchorOnFirstName, AnchorOnLastName,
    IncludeInitials, IncludeLastName, DeriveAllNameCombinations,
    DerivedNameWordsLimit,InterveningWords, DoScoring, CompMsgTagsToExclTags,
    Remarks, IncrListType, ListChangeDays, useExcludes, useUnassignedExcludes,
    WordWrapMatch, Enabled, CombWords_ListType, EnablePartialDOBMatching, DOBToleranceInYears,
    CreateOper, CreateDate, LastOper, LastModify, Ts
  from SanctionDataRule

  select @stat = @@ERROR
  return @stat
Go


-- Update with two new columns: EnablePartialDOBMatching, DOBToleranceInYears
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'OFS_SelectEnabledSanctionDataRule')
 Begin
	DROP PROCEDURE [dbo].OFS_SelectEnabledSanctionDataRule
 End
GO

CREATE PROCEDURE dbo.OFS_SelectEnabledSanctionDataRule (@code SCode)
With Encryption 
As

  declare @stat int

  if (isnull(@code, '') != '')
	  select Code, Name, Incremental, UseAddr, UseKeyword, BranchList, 
	      DepartmentList,ProgramList, BlockProgramList, TypeList, BlockTypeList,
	      ExcludeBranchList, ExcludeDeptList, ListTypeList, BlockListTypeList, 
	      EncWordLen, MaxEncWords, MinEncWords, MinEncChars, EncMap, DelimMap, PunctMap, 
	      EncFirstChar, IgnTrailS,CombWords, InEquality, AnchorOnFirstName, AnchorOnLastName,
	      IncludeInitials, IncludeLastName, DeriveAllNameCombinations,
	      DerivedNameWordsLimit,InterveningWords, DoScoring, CompMsgTagsToExclTags,
	      Remarks, IncrListType, ListChangeDays, useExcludes, useUnassignedExcludes,
	      WordWrapMatch, Enabled, CombWords_ListType, useAbbrev, useUnassignedAbbreviations, 
	      IncludeAbbrevBranchDeptList, ExcludeAbbrevBranchDeptList,useNoise, useUnassignedNoiseWords, 
	      IncludeNoiseBranchDeptList, ExcludeNoiseBranchDeptList,useUnassignedInequalities, 
	      IncludeIneqBranchDeptList, ExcludeIneqBranchDeptList, EnablePartialDOBMatching, DOBToleranceInYears,
	      CreateOper, CreateDate, LastOper, LastModify, Ts
	  from SanctionDataRule where Enabled = 1 and code = @code
  else
	  select Code, Name, Incremental, UseAddr, UseKeyword, BranchList, 
	      DepartmentList,ProgramList, BlockProgramList, TypeList, BlockTypeList,
	      ExcludeBranchList, ExcludeDeptList, ListTypeList, BlockListTypeList, 
	      EncWordLen, MaxEncWords, MinEncWords, MinEncChars, EncMap, DelimMap, PunctMap, 
	      EncFirstChar, IgnTrailS,CombWords, InEquality, AnchorOnFirstName, AnchorOnLastName,
	      IncludeInitials, IncludeLastName, DeriveAllNameCombinations,
	      DerivedNameWordsLimit,InterveningWords, DoScoring, CompMsgTagsToExclTags,
	      Remarks, IncrListType, ListChangeDays, useExcludes, useUnassignedExcludes,
	      WordWrapMatch, Enabled, CombWords_ListType, useAbbrev, useUnassignedAbbreviations, 
	      IncludeAbbrevBranchDeptList, ExcludeAbbrevBranchDeptList,useNoise, useUnassignedNoiseWords, 
	      IncludeNoiseBranchDeptList, ExcludeNoiseBranchDeptList,useUnassignedInequalities, 
	      IncludeIneqBranchDeptList, ExcludeIneqBranchDeptList, EnablePartialDOBMatching, DOBToleranceInYears,
	      CreateOper, CreateDate, LastOper, LastModify, Ts
	  from SanctionDataRule where Enabled = 1

  select @stat = @@ERROR
  return @stat
Go

-- Update with two new columns: EnablePartialDOBMatching, DOBToleranceInYears
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'OFS_SelectSanctionDataRule')
 Begin
	DROP PROCEDURE [dbo].OFS_SelectSanctionDataRule
 End
GO

CREATE PROCEDURE dbo.OFS_SelectSanctionDataRule (@code SCode)
With Encryption 
As

  declare @stat int

  if (isnull(@code, '') != '')
  select Code, Name, Incremental, UseAddr, UseKeyword, BranchList, 
      DepartmentList,ProgramList, BlockProgramList, TypeList, BlockTypeList,
      ExcludeBranchList, ExcludeDeptList, ListTypeList, BlockListTypeList, 
      EncWordLen, MaxEncWords, MinEncWords, MinEncChars, EncMap, DelimMap, PunctMap, 
      EncFirstChar, IgnTrailS,CombWords, InEquality, AnchorOnFirstName, AnchorOnLastName,
      IncludeInitials, IncludeLastName, DeriveAllNameCombinations,
      DerivedNameWordsLimit,InterveningWords, DoScoring, CompMsgTagsToExclTags,
      Remarks, IncrListType, ListChangeDays, useExcludes, useUnassignedExcludes,
      WordWrapMatch, Enabled, CombWords_ListType, useAbbrev, useUnassignedAbbreviations, 
      IncludeAbbrevBranchDeptList, ExcludeAbbrevBranchDeptList,useNoise, useUnassignedNoiseWords, 
      IncludeNoiseBranchDeptList, ExcludeNoiseBranchDeptList,useUnassignedInequalities, 
      IncludeIneqBranchDeptList, ExcludeIneqBranchDeptList, EnablePartialDOBMatching, DOBToleranceInYears,
      CreateOper, CreateDate, LastOper, LastModify, Ts
  from SanctionDataRule  where code = @code
  else
  select Code, Name, Incremental, UseAddr, UseKeyword, BranchList, 
      DepartmentList,ProgramList, BlockProgramList, TypeList, BlockTypeList,
      ExcludeBranchList, ExcludeDeptList, ListTypeList, BlockListTypeList, 
      EncWordLen, MaxEncWords, MinEncWords, MinEncChars, EncMap, DelimMap, PunctMap, 
      EncFirstChar, IgnTrailS,CombWords, InEquality, AnchorOnFirstName, AnchorOnLastName,
      IncludeInitials, IncludeLastName, DeriveAllNameCombinations,
      DerivedNameWordsLimit,InterveningWords, DoScoring, CompMsgTagsToExclTags,
      Remarks, IncrListType, ListChangeDays, useExcludes, useUnassignedExcludes,
      WordWrapMatch, Enabled, CombWords_ListType, useAbbrev, useUnassignedAbbreviations, 
      IncludeAbbrevBranchDeptList, ExcludeAbbrevBranchDeptList,useNoise, useUnassignedNoiseWords, 
      IncludeNoiseBranchDeptList, ExcludeNoiseBranchDeptList,useUnassignedInequalities, 
      IncludeIneqBranchDeptList, ExcludeIneqBranchDeptList, EnablePartialDOBMatching, DOBToleranceInYears,
      CreateOper, CreateDate, LastOper, LastModify, Ts
  from SanctionDataRule

  select @stat = @@ERROR
  return @stat
Go

-- Update with two new columns: EnablePartialDOBMatching, DOBToleranceInYears
IF EXISTS (SELECT * FROM sys.triggers 
      WHERE name = 'OFS_SanctiondataRuleUpd' AND type = 'TR')
   DROP TRIGGER [dbo].OFS_SanctiondataRuleUpd
GO

CREATE TRIGGER dbo.OFS_SanctiondataRuleUpd on dbo.SanctiondataRule 
With Encryption
 For Update As

  Update SanctiondataRule Set LastModify = getDate()
    From Inserted I, SanctiondataRule d Where  i.Code = d.Code

  Insert SdnChangeLog (LogDate, Oper, Type, ObjectType, ObjectId , LogText, EvtDetail)
   Select getDate(), i.LastOper, 'Mod', 'SanDataRule',
    RTrim(convert(varchar, i.Code)),  '',
Case
    WHEN ((i.[Code] Is Null And d.[Code] Is Not Null ) Or
    (i.[Code] Is Not Null And d.[Code] Is Null ) Or
    (i.[Code] != d.[Code]))
    THEN '| Code OLD: ' + Convert(varchar, IsNull(d.[Code],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[Code],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[Name] Is Null And d.[Name] Is Not Null ) Or
    (i.[Name] Is Not Null And d.[Name] Is Null ) Or
    (i.[Name] != d.[Name]))
    THEN '| Name OLD: ' + Convert(varchar, IsNull(d.[Name],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[Name],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[Incremental] Is Null And d.[Incremental] Is Not Null ) Or
    (i.[Incremental] Is Not Null And d.[Incremental] Is Null ) Or
    (i.[Incremental] != d.[Incremental]))
    THEN '| Incremental OLD: ' + Convert(varchar, IsNull(d.[Incremental],0)) +
    ' NEW: ' + Convert(varchar, IsNull(i.[Incremental],0))
    ELSE '' End +
Case
    WHEN ((i.[UseAddr] Is Null And d.[UseAddr] Is Not Null ) Or
    (i.[UseAddr] Is Not Null And d.[UseAddr] Is Null ) Or
    (i.[UseAddr] != d.[UseAddr]))
    THEN '| UseAddr OLD: ' + case when IsNull(d.[UseAddr],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[UseAddr],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[UseExcludes] Is Null And d.[UseExcludes] Is Not Null ) Or
    (i.[UseExcludes] Is Not Null And d.[UseExcludes] Is Null ) Or
    (i.[UseExcludes] != d.[UseExcludes]))
    THEN '| UseExcludes OLD: ' + case when IsNull(d.[UseExcludes],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[UseExcludes],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[UseAbbrev] Is Null And d.[UseAbbrev] Is Not Null ) Or
    (i.[UseAbbrev] Is Not Null And d.[UseAbbrev] Is Null ) Or
    (i.[UseAbbrev] != d.[UseAbbrev]))
    THEN '| UseAbbrev OLD: ' + case when IsNull(d.[UseAbbrev],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[UseAbbrev],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[UseNoise] Is Null And d.[UseNoise] Is Not Null ) Or
    (i.[UseNoise] Is Not Null And d.[UseNoise] Is Null ) Or
    (i.[UseNoise] != d.[UseNoise]))
    THEN '| UseNoise OLD: ' + case when IsNull(d.[UseNoise],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[UseNoise],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[UseUnassignedExcludes] Is Null And d.[UseUnassignedExcludes] Is Not Null ) Or
    (i.[UseUnassignedExcludes] Is Not Null And d.[UseUnassignedExcludes] Is Null ) Or
    (i.[UseUnassignedExcludes] != d.[UseUnassignedExcludes]))
    THEN '| UseUnassignedExcludes OLD: ' + case when IsNull(d.[UseUnassignedExcludes],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[UseUnassignedExcludes],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[UseUnassignedAbbreviations] Is Null And d.[UseUnassignedAbbreviations] Is Not Null ) Or
    (i.[UseUnassignedAbbreviations] Is Not Null And d.[UseUnassignedAbbreviations] Is Null ) Or
    (i.[UseUnassignedAbbreviations] != d.[UseUnassignedAbbreviations]))
    THEN '| UseUnassignedAbbreviations OLD: ' + case when IsNull(d.[UseUnassignedAbbreviations],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[UseUnassignedAbbreviations],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[UseUnassignedNoiseWords] Is Null And d.[UseUnassignedNoiseWords] Is Not Null ) Or
    (i.[UseUnassignedNoiseWords] Is Not Null And d.[UseUnassignedNoiseWords] Is Null ) Or
    (i.[UseUnassignedNoiseWords] != d.[UseUnassignedNoiseWords]))
    THEN '| UseUnassignedNoiseWords OLD: ' + case when IsNull(d.[UseUnassignedNoiseWords],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[UseUnassignedNoiseWords],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[UseUnassignedInEqualities] Is Null And d.[UseUnassignedInEqualities] Is Not Null ) Or
    (i.[UseUnassignedInEqualities] Is Not Null And d.[UseUnassignedInEqualities] Is Null ) Or
    (i.[UseUnassignedInEqualities] != d.[UseUnassignedInEqualities]))
    THEN '| UseUnassignedInEqualities OLD: ' + case when IsNull(d.[UseUnassignedInEqualities],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[UseUnassignedInEqualities],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[Enabled] Is Null And d.[Enabled] Is Not Null ) Or
    (i.[Enabled] Is Not Null And d.[Enabled] Is Null ) Or
    (i.[Enabled] != d.[Enabled]))
    THEN '| Enabled OLD: ' + case when IsNull(d.[Enabled],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[Enabled],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[UseKeyword] Is Null And d.[UseKeyword] Is Not Null ) Or
    (i.[UseKeyword] Is Not Null And d.[UseKeyword] Is Null ) Or
    (i.[UseKeyword] != d.[UseKeyword]))
    THEN '| UseKeyword OLD: ' + case when IsNull(d.[UseKeyword],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[UseKeyword],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[BranchList] Is Null And d.[BranchList] Is Not Null ) Or
    (i.[BranchList] Is Not Null And d.[BranchList] Is Null ) Or
    (i.[BranchList] != d.[BranchList]))
    THEN '| BranchList OLD: ' + Convert(varchar, IsNull(d.[BranchList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[BranchList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[IncludeAbbrevBranchDeptList] Is Null And d.[IncludeAbbrevBranchDeptList] Is Not Null ) Or
    (i.[IncludeAbbrevBranchDeptList] Is Not Null And d.[IncludeAbbrevBranchDeptList] Is Null ) Or
    (i.[IncludeAbbrevBranchDeptList] != d.[IncludeAbbrevBranchDeptList]))
    THEN '| IncludeAbbrevBranchDeptList OLD: ' + Convert(varchar, IsNull(d.[IncludeAbbrevBranchDeptList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[IncludeAbbrevBranchDeptList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[IncludeNoiseBranchDeptList] Is Null And d.[IncludeNoiseBranchDeptList] Is Not Null ) Or
    (i.[IncludeNoiseBranchDeptList] Is Not Null And d.[IncludeNoiseBranchDeptList] Is Null ) Or
    (i.[IncludeNoiseBranchDeptList] != d.[IncludeNoiseBranchDeptList]))
    THEN '| IncludeNoiseBranchDeptList OLD: ' + Convert(varchar, IsNull(d.[IncludeNoiseBranchDeptList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[IncludeNoiseBranchDeptList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[IncludeInEqBranchDeptList] Is Null And d.[IncludeInEqBranchDeptList] Is Not Null ) Or
    (i.[IncludeInEqBranchDeptList] Is Not Null And d.[IncludeInEqBranchDeptList] Is Null ) Or
    (i.[IncludeInEqBranchDeptList] != d.[IncludeInEqBranchDeptList]))
    THEN '| IncludeInEqBranchDeptList OLD: ' + Convert(varchar, IsNull(d.[IncludeInEqBranchDeptList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[IncludeInEqBranchDeptList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[ProgramList] Is Null And d.[ProgramList] Is Not Null ) Or
    (i.[ProgramList] Is Not Null And d.[ProgramList] Is Null ) Or
    (i.[ProgramList] != d.[ProgramList]))
    THEN '| ProgramList OLD: ' + Convert(varchar, IsNull(d.[ProgramList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[ProgramList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[BlockProgramList] Is Null And d.[BlockProgramList] Is Not Null ) Or
    (i.[BlockProgramList] Is Not Null And d.[BlockProgramList] Is Null ) Or
    (i.[BlockProgramList] != d.[BlockProgramList]))
    THEN '| BlockProgramList OLD: ' + Convert(varchar, IsNull(d.[BlockProgramList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[BlockProgramList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[ListTypeList] Is Null And d.[ListTypeList] Is Not Null ) Or
    (i.[ListTypeList] Is Not Null And d.[ListTypeList] Is Null ) Or
    (i.[ListTypeList] != d.[ListTypeList]))
    THEN '| ListTypeList OLD: ' + Convert(varchar, IsNull(d.[ListTypeList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[ListTypeList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[BlockListTypeList] Is Null And d.[BlockListTypeList] Is Not Null ) Or
    (i.[BlockListTypeList] Is Not Null And d.[BlockListTypeList] Is Null ) Or
    (i.[BlockListTypeList] != d.[BlockListTypeList]))
    THEN '| BlockListTypeList OLD: ' + Convert(varchar, IsNull(d.[BlockListTypeList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[BlockListTypeList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[TypeList] Is Null And d.[TypeList] Is Not Null ) Or
    (i.[TypeList] Is Not Null And d.[TypeList] Is Null ) Or
    (i.[TypeList] != d.[TypeList]))
    THEN '| TypeList OLD: ' + Convert(varchar, IsNull(d.[TypeList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[TypeList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[BlockTypeList] Is Null And d.[BlockTypeList] Is Not Null ) Or
    (i.[BlockTypeList] Is Not Null And d.[BlockTypeList] Is Null ) Or
    (i.[BlockTypeList] != d.[BlockTypeList]))
    THEN '| BlockTypeList OLD: ' + Convert(varchar, IsNull(d.[BlockTypeList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[BlockTypeList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[ExcludeBranchList] Is Null And d.[ExcludeBranchList] Is Not Null ) Or
    (i.[ExcludeBranchList] Is Not Null And d.[ExcludeBranchList] Is Null ) Or
    (i.[ExcludeBranchList] != d.[ExcludeBranchList]))
    THEN '| ExcludeBranchList OLD: ' + Convert(varchar, IsNull(d.[ExcludeBranchList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[ExcludeBranchList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[ExcludeAbbrevBranchDeptList] Is Null And d.[ExcludeAbbrevBranchDeptList] Is Not Null ) Or
    (i.[ExcludeAbbrevBranchDeptList] Is Not Null And d.[ExcludeAbbrevBranchDeptList] Is Null ) Or
    (i.[ExcludeAbbrevBranchDeptList] != d.[ExcludeAbbrevBranchDeptList]))
    THEN '| ExcludeAbbrevBranchDeptList OLD: ' + Convert(varchar, IsNull(d.[ExcludeAbbrevBranchDeptList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[ExcludeAbbrevBranchDeptList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[ExcludeNoiseBranchDeptList] Is Null And d.[ExcludeNoiseBranchDeptList] Is Not Null ) Or
    (i.[ExcludeNoiseBranchDeptList] Is Not Null And d.[ExcludeNoiseBranchDeptList] Is Null ) Or
    (i.[ExcludeNoiseBranchDeptList] != d.[ExcludeNoiseBranchDeptList]))
    THEN '| ExcludeNoiseBranchDeptList OLD: ' + Convert(varchar, IsNull(d.[ExcludeNoiseBranchDeptList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[ExcludeNoiseBranchDeptList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[ExcludeInEqBranchDeptList] Is Null And d.[ExcludeInEqBranchDeptList] Is Not Null ) Or
    (i.[ExcludeInEqBranchDeptList] Is Not Null And d.[ExcludeInEqBranchDeptList] Is Null ) Or
    (i.[ExcludeInEqBranchDeptList] != d.[ExcludeInEqBranchDeptList]))
    THEN '| ExcludeInEqBranchDeptList OLD: ' + Convert(varchar, IsNull(d.[ExcludeInEqBranchDeptList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[ExcludeInEqBranchDeptList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[ExcludeDeptList] Is Null And d.[ExcludeDeptList] Is Not Null ) Or
    (i.[ExcludeDeptList] Is Not Null And d.[ExcludeDeptList] Is Null ) Or
    (i.[ExcludeDeptList] != d.[ExcludeDeptList]))
    THEN '| ExcludeDeptList OLD: ' + Convert(varchar, IsNull(d.[ExcludeDeptList],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[ExcludeDeptList],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[EncWordLen] Is Null And d.[EncWordLen] Is Not Null ) Or
    (i.[EncWordLen] Is Not Null And d.[EncWordLen] Is Null ) Or
    (i.[EncWordLen] != d.[EncWordLen]))
    THEN '| EncWordLen OLD: ' + Convert(varchar, IsNull(d.[EncWordLen],0)) +
    ' NEW: ' + Convert(varchar, IsNull(i.[EncWordLen],0))
    ELSE '' End +
Case
    WHEN ((i.[MaxEncWords] Is Null And d.[MaxEncWords] Is Not Null ) Or
    (i.[MaxEncWords] Is Not Null And d.[MaxEncWords] Is Null ) Or
    (i.[MaxEncWords] != d.[MaxEncWords]))
    THEN '| MaxEncWords OLD: ' + Convert(varchar, IsNull(d.[MaxEncWords],0)) +
    ' NEW: ' + Convert(varchar, IsNull(i.[MaxEncWords],0))
    ELSE '' End +
Case
    WHEN ((i.[MinEncWords] Is Null And d.[MinEncWords] Is Not Null ) Or
    (i.[MinEncWords] Is Not Null And d.[MinEncWords] Is Null ) Or
    (i.[MinEncWords] != d.[MinEncWords]))
    THEN '| MinEncWords OLD: ' + Convert(varchar, IsNull(d.[MinEncWords],0)) +
    ' NEW: ' + Convert(varchar, IsNull(i.[MinEncWords],0))
    ELSE '' End +
Case
    WHEN ((i.[MinEncChars] Is Null And d.[MinEncChars] Is Not Null ) Or
    (i.[MinEncChars] Is Not Null And d.[MinEncChars] Is Null ) Or
    (i.[MinEncChars] != d.[MinEncChars]))
    THEN '| MinEncChars OLD: ' + Convert(varchar, IsNull(d.[MinEncChars],0)) +
    ' NEW: ' + Convert(varchar, IsNull(i.[MinEncChars],0))
    ELSE '' End +
Case
    WHEN ((i.[EncMap] Is Null And d.[EncMap] Is Not Null ) Or
    (i.[EncMap] Is Not Null And d.[EncMap] Is Null ) Or
    (i.[EncMap] != d.[EncMap]))
    THEN '| EncMap OLD: ' + Convert(varchar, IsNull(d.[EncMap],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[EncMap],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[DelimMap] Is Null And d.[DelimMap] Is Not Null ) Or
    (i.[DelimMap] Is Not Null And d.[DelimMap] Is Null ) Or
    (i.[DelimMap] != d.[DelimMap]))
    THEN '| DelimMap OLD: ' + Convert(varchar, IsNull(d.[DelimMap],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[DelimMap],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[PunctMap] Is Null And d.[PunctMap] Is Not Null ) Or
    (i.[PunctMap] Is Not Null And d.[PunctMap] Is Null ) Or
    (i.[PunctMap] != d.[PunctMap]))
    THEN '| PunctMap OLD: ' + Convert(varchar, IsNull(d.[PunctMap],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[PunctMap],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[EncFirstChar] Is Null And d.[EncFirstChar] Is Not Null ) Or
    (i.[EncFirstChar] Is Not Null And d.[EncFirstChar] Is Null ) Or
    (i.[EncFirstChar] != d.[EncFirstChar]))
    THEN '| EncFirstChar OLD: ' + case when IsNull(d.[EncFirstChar],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[EncFirstChar],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[IgnTrailS] Is Null And d.[IgnTrailS] Is Not Null ) Or
    (i.[IgnTrailS] Is Not Null And d.[IgnTrailS] Is Null ) Or
    (i.[IgnTrailS] != d.[IgnTrailS]))
    THEN '| IgnTrailS OLD: ' + case when IsNull(d.[IgnTrailS],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[IgnTrailS],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[CombWords] Is Null And d.[CombWords] Is Not Null ) Or
    (i.[CombWords] Is Not Null And d.[CombWords] Is Null ) Or
    (i.[CombWords] != d.[CombWords]))
    THEN '| CombWords OLD: ' + case when IsNull(d.[CombWords],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[CombWords],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[Inequality] Is Null And d.[Inequality] Is Not Null ) Or
    (i.[Inequality] Is Not Null And d.[Inequality] Is Null ) Or
    (i.[Inequality] != d.[Inequality]))
    THEN '| Inequality OLD: ' + case when IsNull(d.[Inequality],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[Inequality],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[AnchorOnFirstName] Is Null And d.[AnchorOnFirstName] Is Not Null ) Or
    (i.[AnchorOnFirstName] Is Not Null And d.[AnchorOnFirstName] Is Null ) Or
    (i.[AnchorOnFirstName] != d.[AnchorOnFirstName]))
    THEN '| AnchorOnFirstName OLD: ' + case when IsNull(d.[AnchorOnFirstName],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[AnchorOnFirstName],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[AnchorOnLastName] Is Null And d.[AnchorOnLastName] Is Not Null ) Or
    (i.[AnchorOnLastName] Is Not Null And d.[AnchorOnLastName] Is Null ) Or
    (i.[AnchorOnLastName] != d.[AnchorOnLastName]))
    THEN '| AnchorOnLastName OLD: ' + case when IsNull(d.[AnchorOnLastName],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[AnchorOnLastName],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[IncludeLastName] Is Null And d.[IncludeLastName] Is Not Null ) Or
    (i.[IncludeLastName] Is Not Null And d.[IncludeLastName] Is Null ) Or
    (i.[IncludeLastName] != d.[IncludeLastName]))
    THEN '| IncludeLastName OLD: ' + case when IsNull(d.[IncludeLastName],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[IncludeLastName],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[IncludeInitials] Is Null And d.[IncludeInitials] Is Not Null ) Or
    (i.[IncludeInitials] Is Not Null And d.[IncludeInitials] Is Null ) Or
    (i.[IncludeInitials] != d.[IncludeInitials]))
    THEN '| IncludeInitials OLD: ' + case when IsNull(d.[IncludeInitials],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[IncludeInitials],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[DeriveAllNameCombinations] Is Null And d.[DeriveAllNameCombinations] Is Not Null ) Or
    (i.[DeriveAllNameCombinations] Is Not Null And d.[DeriveAllNameCombinations] Is Null ) Or
    (i.[DeriveAllNameCombinations] != d.[DeriveAllNameCombinations]))
    THEN '| DeriveAllNameCombinations OLD: ' + case when IsNull(d.[DeriveAllNameCombinations],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[DeriveAllNameCombinations],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[DerivedNameWordsLimit] Is Null And d.[DerivedNameWordsLimit] Is Not Null ) Or
    (i.[DerivedNameWordsLimit] Is Not Null And d.[DerivedNameWordsLimit] Is Null ) Or
    (i.[DerivedNameWordsLimit] != d.[DerivedNameWordsLimit]))
    THEN '| DerivedNameWordsLimit OLD: ' + Convert(varchar, IsNull(d.[DerivedNameWordsLimit],0)) +
    ' NEW: ' + Convert(varchar, IsNull(i.[DerivedNameWordsLimit],0))
    ELSE '' End +
Case
    WHEN ((i.[InterveningWords] Is Null And d.[InterveningWords] Is Not Null ) Or
    (i.[InterveningWords] Is Not Null And d.[InterveningWords] Is Null ) Or
    (i.[InterveningWords] != d.[InterveningWords]))
    THEN '| InterveningWords OLD: ' + Convert(varchar, IsNull(d.[InterveningWords],0)) +
    ' NEW: ' + Convert(varchar, IsNull(i.[InterveningWords],0))
    ELSE '' End +
Case
    WHEN ((i.[DoScoring] Is Null And d.[DoScoring] Is Not Null ) Or
    (i.[DoScoring] Is Not Null And d.[DoScoring] Is Null ) Or
    (i.[DoScoring] != d.[DoScoring]))
    THEN '| DoScoring OLD: ' + case when IsNull(d.[DoScoring],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[DoScoring],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[CompMsgTagsToExclTags] Is Null And d.[CompMsgTagsToExclTags] Is Not Null ) Or
    (i.[CompMsgTagsToExclTags] Is Not Null And d.[CompMsgTagsToExclTags] Is Null ) Or
    (i.[CompMsgTagsToExclTags] != d.[CompMsgTagsToExclTags]))
    THEN '| CompMsgTagsToExclTags OLD: ' + case when IsNull(d.[CompMsgTagsToExclTags],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[CompMsgTagsToExclTags],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[WordWrapMatch] Is Null And d.[WordWrapMatch] Is Not Null ) Or
    (i.[WordWrapMatch] Is Not Null And d.[WordWrapMatch] Is Null ) Or
    (i.[WordWrapMatch] != d.[WordWrapMatch]))
    THEN '| WordWrapMatch OLD: ' + case when IsNull(d.[WordWrapMatch],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[WordWrapMatch],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN (((i.[Remarks] Is Null And d.[Remarks] Is Not Null ) Or
    (i.[Remarks] Is Not Null And d.[Remarks] Is Null ) Or
    (i.[Remarks] != d.[Remarks])) AND
    len(Convert(varchar(255), IsNull(i.[Remarks],'')))>=len(Convert(varchar(255), IsNull(d.[Remarks],''))))
    THEN '| Remarks ADDED: ' + right(Convert(varchar(255), IsNull(i.[Remarks],'')),
        len(Convert(varchar(255), IsNull(i.[Remarks],'')))-len(Convert(varchar(255), IsNull(d.[Remarks],''))))
    ELSE '' End +
Case
    WHEN ((i.[CreateOper] Is Null And d.[CreateOper] Is Not Null ) Or
    (i.[CreateOper] Is Not Null And d.[CreateOper] Is Null ) Or
    (i.[CreateOper] != d.[CreateOper]))
    THEN '| CreateOper OLD: ' + Convert(varchar, IsNull(d.[CreateOper],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[CreateOper],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[CreateDate] Is Null And d.[CreateDate] Is Not Null ) Or
    (i.[CreateDate] Is Not Null And d.[CreateDate] Is Null ) Or
    (i.[CreateDate] != d.[CreateDate]))
    THEN '| CreateDate OLD: ' + case when d.[CreateDate] is Null then 'Null' else
    Convert(varchar, d.[CreateDate]) End +
    ' NEW: ' + case when i.[CreateDate] is Null then 'Null' else
    Convert(varchar, i.[CreateDate]) End
    ELSE '' End +
Case
    WHEN ((i.[LastOper] Is Null And d.[LastOper] Is Not Null ) Or
    (i.[LastOper] Is Not Null And d.[LastOper] Is Null ) Or
    (i.[LastOper] != d.[LastOper]))
    THEN '| LastOper OLD: ' + Convert(varchar, IsNull(d.[LastOper],'Null')) +
    ' NEW: ' + Convert(varchar, IsNull(i.[LastOper],'Null'))
    ELSE '' End +
Case
    WHEN ((i.[LastModify] Is Null And d.[LastModify] Is Not Null ) Or
    (i.[LastModify] Is Not Null And d.[LastModify] Is Null ) Or
    (i.[LastModify] != d.[LastModify]))
    THEN '| LastModify OLD: ' + case when d.[LastModify] is Null then 'Null' else
    Convert(varchar, d.[LastModify]) End +
    ' NEW: ' + case when i.[LastModify] is Null then 'Null' else
    Convert(varchar, i.[LastModify]) End
    ELSE '' End +
Case
    WHEN ((i.[IncrListType] Is Null And d.[IncrListType] Is Not Null ) Or
    (i.[IncrListType] Is Not Null And d.[IncrListType] Is Null ) Or
    (i.[IncrListType] != d.[IncrListType]))
    THEN '| IncrListType OLD: ' + Convert(varchar, IsNull(d.[IncrListType],0)) +
    ' NEW: ' + Convert(varchar, IsNull(i.[IncrListType],0))
    ELSE '' End +
Case
    WHEN ((i.[CombWords_ListType] Is Null And d.[CombWords_ListType] Is Not Null ) Or
    (i.[CombWords_ListType] Is Not Null And d.[CombWords_ListType] Is Null ) Or
    (i.[CombWords_ListType] != d.[CombWords_ListType]))
    THEN '| IncrListType OLD: ' + Convert(varchar, IsNull(d.[IncrListType],0)) +
    ' NEW: ' + Convert(varchar, IsNull(i.[IncrListType],0))
    ELSE '' End +
Case
    WHEN ((i.[EnablePartialDOBMatching] Is Null And d.[EnablePartialDOBMatching] Is Not Null ) Or
    (i.[EnablePartialDOBMatching] Is Not Null And d.[EnablePartialDOBMatching] Is Null ) Or
    (i.[EnablePartialDOBMatching] != d.[EnablePartialDOBMatching]))
    THEN '| EnablePartialDOBMatching OLD: ' + case when IsNull(d.[EnablePartialDOBMatching],0) = 0 then 'False' else 'True' End +
    ' NEW: ' + case when IsNull(i.[EnablePartialDOBMatching],0) = 0 then 'False' else 'True' End
    ELSE '' End +
Case
    WHEN ((i.[DOBToleranceInYears] Is Null And d.[DOBToleranceInYears] Is Not Null ) Or
    (i.[DOBToleranceInYears] Is Not Null And d.[DOBToleranceInYears] Is Null ) Or
    (i.[DOBToleranceInYears] != d.[DOBToleranceInYears]))
    THEN '| DOBToleranceInYears OLD: ' + Convert(varchar, IsNull(d.[DOBToleranceInYears],0)) +
    ' NEW: ' + Convert(varchar, IsNull(i.[DOBToleranceInYears],0))
    ELSE '' End +
Case
    WHEN ((i.[ListChangeDays] Is Null And d.[ListChangeDays] Is Not Null ) Or
    (i.[ListChangeDays] Is Not Null And d.[ListChangeDays] Is Null ) Or
    (i.[ListChangeDays] != d.[ListChangeDays]))
    THEN '| ListChangeDays OLD: ' + Convert(varchar, IsNull(d.[ListChangeDays],0)) +
    ' NEW: ' + Convert(varchar, IsNull(i.[ListChangeDays],0))
    ELSE '' End
    From Inserted i, deleted d where  i.Code = d.Code
Go

---------------------------------------------------------------------------------------------------
if exists (select * from dbo.sysobjects where
    id = object_id(N'[dbo].[WOFS_GetSearchResults]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[WOFS_GetSearchResults]
GO

CREATE PROCEDURE [dbo].[WOFS_GetSearchResults]
    (
        @OperCode       SCode,
        @SysRef         MessageReference

    )

With Encryption As
    --Get All Checked Search Results
    select  distinct SDNTable.EntNum, SDNTable.[Name], OFACSearchResults.[Program], SDNTable.[Type],
    SDNTable.Remarks1, OFACSearchResults.MatchName, OFACSearchResults.MatchText,
	OFACSearchResults.Score
    FROM SDNTable with(nolock) left outer join OFACSearchResults with(nolock)
    on SDNTable.[Name] = OFACSearchResults.OriginalSDNName and 
    (SDNTable.[Program] = OFACSearchResults.[Program] or OFACSearchResults.[Program] = 'KEY WORD') and
    OFACSearchResults.OperCode = @OperCode and
    OFACSearchResults.SystemRef=@SysRef
    where SDNTable.EntNum in (select Convert(int, [RowId]) from PCDB.dbo.ViewCheckRows with(nolock)
    where ViewId =100 and OperCode=@OperCode and CheckStatus=1)
Go

-- 6.16.2015 Performance indexes added for Dow Jones EMEA list import
USE [OFAC]
GO
/****** Object:  Table [dbo].[SDNTable]    Script Date: 04/27/2015 10:32:43 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SDNTable]') AND name = N'ListId_Program_Type_SDNTable_IDX')
	CREATE NONCLUSTERED INDEX [ListId_Program_Type_SDNTable_IDX] ON [dbo].[SDNTable] 
	(
		[ListID] ASC,
		[Program] ASC,
		[Type] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SDNTable]') AND name = N'ListId_SDNTable_IDX')
	CREATE NONCLUSTERED INDEX [ListId_SDNTable_IDX] ON [dbo].[SDNTable] 
	(
		[ListID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	GO

/****** Object:  Table [dbo].[Relationship]    Script Date: 04/27/2015 10:32:43 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Relationship]') AND name = N'EntNum_RelatedId_Relationship_IDX')
	CREATE NONCLUSTERED INDEX [EntNum_RelatedId_Relationship_IDX] ON [dbo].[Relationship] 
	(
		[EntNum] ASC,
		[RelatedID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Relationship]') AND name = N'EntNum_RelatedId_Status_Relationship_IDX')
	CREATE NONCLUSTERED INDEX [EntNum_RelatedId_Status_Relationship_IDX] ON [dbo].[Relationship] 
	(
		[EntNum] ASC,
		[RelatedID] ASC,
		[Status] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	GO

/****** Object:  Table [dbo].[DOBs]    Script Date: 04/27/2015 10:32:43 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DOBs]') AND name = N'EntNum_DOB_Status_DOBs_IDX')
	CREATE NONCLUSTERED INDEX [EntNum_DOB_Status_DOBs_IDX] ON [dbo].[DOBs] 
	(
		[EntNum] ASC,
		[DOB] ASC,
		[Status] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	GO
-- 6.16.2015 Performance indexes added for Dow Jones EMEA list import

----------------------------------------------------------------------------------------------
GO

if not exists (select * from OptionTbl where Code='DATABNKOPT')
       INSERT INTO
       OptionTbl
       (Code, Name, Enabled, ModEnable, CreateOper, CreateDate, LastOper, LastModify)
       VALUES
       ('DATABNKOPT', 'DataBank Option � Configure system for usage of Prime DataBank Lists/User Defined Lists', 0, 1, 'Prime', getdate(), null, null)
GO
-- New Function fn_IsPrimeDatabankDataExists
IF EXISTS (SELECT * from dbo.sysobjects  WHERE id = object_id(N'fn_IsPrimeDatabankDataExists') AND  		          
              OBJECTPROPERTY(id, N'IsScalarFunction') = 1)		              
  DROP FUNCTION [dbo].[fn_IsPrimeDatabankDataExists]
GO		

CREATE FUNCTION dbo.fn_IsPrimeDatabankDataExists()  RETURNS int
WITH ENCRYPTION AS		

BEGIN
Declare @RetVal as Int

	if exists (Select Top 1 EntNum from dbo.SDNTable with (nolock) where ListType != 'USER')
		set @RetVal=1
	else
		set @RetVal =0
return @RetVal
End 

GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_BulkInsOfacAudit]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_BulkInsOfacAudit]
Go

CREATE PROCEDURE [dbo].[OFS_BulkInsOfacAudit] (
                   @bcpFile        varchar(250),
                   @formatFile     varchar(250)
	)
	With Encryption
	As

  -- Start standard stored procedure transaction header
  Declare @counter as int = 0
  Declare @batch_size as int = 25000
  Declare @stat   int

	BEGIN   
	
		Create table #tmpAudit (
			[ID]	[int]	Identity(1,1),
    		[CustId] [varchar](35) NOT NULL,
    		[CustName] [varchar](100) NULL,
    		[Source] [varchar](50) NULL,
    		[FileImage] [varchar](12) NULL,
    		[IsIncremental] [bit] NULL,	
    		[SancPartyMatches] [int] NULL,
    		[CaseReference] [varchar](100) NULL,
    		[ScanOper] [varchar](12) NULL,
    		[LastScanDate] [datetime] NULL,
    		[OrigScanDate] [datetime] NULL,
    		[Branch] [varchar](12) NULL,
    		[Department] [varchar](12) NULL,  
    		[Remarks] [Text] NULL,
    		[OrigRemarks] [Text] NULL
		)
		
		-- Bulk Insert records into temp table 
	
		EXEC('
        		BULK INSERT #tmpAudit 
        		FROM ''' + @bcpFile + '''
        		WITH (BATCHSIZE = ' + @batch_size + ',
            			CHECK_CONSTRAINTS,
       		 		FIELDTERMINATOR  = ''~'',
            			FORMATFILE = ''' + @formatFile + ''',
            			MAXERRORS = 0,
            			ROWTERMINATOR = ''\r\n'',
            			TABLOCK)
       				')

		Delete from #tmpAudit where (LTrim(RTrim(IsNull(CustId ,'')))= ''
		AND LTrim(RTrim(IsNull(CustName ,''))) = '')	
		
		/* Input records are processed batch by batch to process large number
		   of records without any issues. Batch size is set as 25000. While 
		   loop is used to process 25000 records at a time and loop through 
		   until all records are processed. */
		
		WHILE (@counter < (SELECT MAX(id) FROM #tmpAudit))
		BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			
			INSERT INTO OFACSCanAudit 
			SELECT  T1.[CustId], T1.[CustName], T1.[Source],
					T1.[FileImage], T1.[IsIncremental],	T1.[SancPartyMatches],
					T1.[CaseReference], T1.[ScanOper], T1.[LastScanDate],
					T1.[OrigScanDate], T1.[Branch], T1.[Department],  
					T1.[Remarks], T1.[OrigRemarks]
			FROM #tmpAudit T1
			LEFT JOIN OFACSCanAudit A1
			ON A1.CustId = T1.CustId AND A1.Source = T1.Source          
			WHERE A1.CustId IS NULL AND T1.CustID IS NOT NULL AND T1.ID in (
					SELECT Min(ID) from #tmpAudit with (nolock) where ID 
					BETWEEN @counter AND (@counter + @batch_size - 1) group by 
					#tmpAudit.CustID)and T1.ID BETWEEN @counter AND (@counter + @batch_size - 1)

			-- Merge records into OFACSCanAudit from temp table by matching CustId
			-- Perform update else insert records by matching CustId
			UPDATE A1
			SET A1.CustName = T1.CustName,
				A1.Source = T1.Source,
				A1.FileImage = T1.FileImage,
				A1.IsIncremental = T1.IsIncremental,
				A1.SancPartyMatches = T1.SancPartyMatches,
				A1.CaseReference = T1.CaseReference,
				A1.ScanOper = T1.ScanOper,
				A1.LastScanDate = T1.LastScanDate,
				A1.Branch = T1.Branch,
				A1.Department = T1.Department,
				A1.Remarks = T1.Remarks 
			FROM OFACSCanAudit A1 INNER JOIN #tmpAudit T1
			ON T1.CustId = A1.CustId AND T1.Source = A1.Source        
			AND T1.ID in (SELECT MAX(ID) from #tmpAudit with (nolock) where ID 
			BETWEEN @counter AND (@counter + @batch_size - 1) group by
			#tmpAudit.CustID)
			and T1.ID BETWEEN @counter AND (@counter + @batch_size - 1)

			Select @stat = @@ERROR
			
			COMMIT TRANSACTION
			SET @counter = @counter + @batch_size;
		END TRY
		BEGIN CATCH
			Select @stat = @@ERROR
			ROLLBACK TRANSACTION
			BREAK
		END CATCH
		END
		
	-- Finally drop temp table after merge process
	DROP TABLE #tmpAudit	
	RETURN @stat
	END

GO

-- Script needs to be applied to resolve OFAC Cases issue
Use OFAC
Go

If Not Exists(Select * from ListTypeAssociation with (nolock) where ListType ='USER')
                Insert into ListTypeAssociation values ('USER',NULL,NULL)
Go

-- New Function fn_IsNewestSDNFromDowJones
IF EXISTS (SELECT *
           FROM   dbo.sysobjects
           WHERE  id = object_id(N'fn_IsNewestSDNFromDowJones')
                  AND OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
    DROP FUNCTION [dbo].fn_IsNewestSDNFromDowJones;
GO

CREATE FUNCTION dbo.fn_IsNewestSDNFromDowJones
( )
RETURNS BIT
WITH ENCRYPTION
-- Function returns 1 if the last modified record is a Dow Jones
-- Record.  0 if it is not.
AS
BEGIN
    DECLARE @SDNCount AS INT;
    SELECT @SDNCount = (SELECT Count(*)
                        FROM   (SELECT   TOP 1 EntNum,
                                               ListType,
                                               ListID,
                                               Program,
                                               LastModifDate
                                FROM     dbo.SDNTable WITH (NOLOCK)
                                ORDER BY LastModifDate DESC) AS sdn
                        WHERE  ListType = 'USER'
                               AND ListID LIKE 'DJ%');
    RETURN CAST (@SDNCount AS BIT);
END
GO

-- Update to handle Dow Jones Data loads and Incremental Build Images
IF EXISTS (SELECT *
           FROM   dbo.sysobjects
           WHERE  id = object_id(N'[dbo].[OFS_SetLastSDNUpd]')
                  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].OFS_SetLastSDNUpd;


GO
CREATE PROCEDURE OFS_SetLastSDNUpd
@distDate DATETIME
WITH ENCRYPTION
AS
DECLARE @trnCnt AS INT;
SELECT @trnCnt = @@trancount; -- Save the current trancount
IF @trnCnt = 0
    -- Transaction has not begun
    BEGIN TRANSACTION OFS_SetLastSDNUpd;
ELSE
    -- Already in a transaction
    SAVE TRANSACTION OFS_SetLastSDNUpd;
-- Processing starts here:
DECLARE @ctr AS INT;
DECLARE @now AS DATETIME;
DECLARE @stat AS INT;
DECLARE @isDowJonesUpdate AS BIT;

-- Add one second to the Datetime to account for a Precision Loss
-- in the Incremental File Image Builder.
SELECT @now = DATEADD(ss,1,GETDATE());

SELECT *
FROM   SDNdbStatus;

SELECT @stat = @@ERROR,
       @ctr = @@ROWCOUNT;

SELECT @isDowJonesUpdate = dbo.fn_IsNewestSDNFromDowJones();       
       
IF @stat = 0
    BEGIN
        IF @ctr > 0
            -- Update the LastSDNUpdate with the date previous distribution was loaded
            -- and Distributiondate with the current distribution date
            -- when the current distribution in the system is not the same as
            -- one being currently loaded
            BEGIN
                UPDATE  SDNDbStatus
                    SET DistributionDate = @distDate,
                        LastSDNUpdate    = CurrSDNUpdate,
                        CurrSDNUpdate    = @now,
                        SDNUpdateCtr     = SDNUpdateCtr + 1,
                        MemImgBuildReq   = 1
                WHERE   DistributionDate <> @distDate;
                -- Do not Update the LastSDNUpdate with the previous distribution date
                -- and Distributiondate with the current distribution date
                -- when the current distribution in the system is same as
                -- one being currently loaded. This will help retain the incrementals.
				
				-- Check for Dow Jones because the CurrSDNUpdate must always be updated
				-- because this data is loaded over multiple loads.  Incremental Build
				-- Images need this date to equal the last load for Dow Jones.
                IF @isDowJonesUpdate = 1
                    BEGIN
                        UPDATE  SDNDbStatus
                            SET SDNUpdateCtr   = SDNUpdateCtr + 1,
                                MemImgBuildReq = 1,
                  
                                CurrSDNUpdate  = @now  
                        WHERE   DistributionDate = @distDate;
                    END
                ELSE
                    BEGIN
                        UPDATE  SDNDbStatus
                            SET SDNUpdateCtr   = SDNUpdateCtr + 1,
                                MemImgBuildReq = 1
                        WHERE   DistributionDate = @distDate;
                    END
            END
        ELSE
            INSERT  INTO SDNDbStatus (DistributionDate, DateInstalled, CurrSDNUpdate, SDNUpdateCtr, DbModifCtr, MemImgBuildCtr, MemImgBuildReq)
            VALUES                  (@distDate, @now, @now, 1, 0, 0, 1);
        SELECT @stat = @@ERROR;
        IF @stat = 0
            BEGIN
                IF @trnCnt = 0
                    COMMIT TRANSACTION OFS_SetLastSDNUpd;
            END
        ELSE
            ROLLBACK TRANSACTION OFS_SetLastSDNUpd;
    END
ELSE
    ROLLBACK TRANSACTION OFS_SetLastSDNUpd;
RETURN @stat;
GO

PRINT ''
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT ' Completed Conversion of OFAC Database to version 10.0.0  '
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT ''
GO