﻿|-----------------------------------------------------------------------------------------|
|				       :: INSTRUCTIONS ::                                 |   
|     				  				                          |
|-----------------------------------------------------------------------------------------|


Purpose of the Patch:  

1.	To release and expand the Dow Jones Watch list / Negative News / State Owned 
	Companies functionality in Prime
2.	Third Party Lists Support for OFAC Reporter and Extended Build File Image Utility 
	Changes.
3.	I150623506 : Addressed the UK date format issue.
4.	PPL0146863 - Add a new user-defined rule so that alerts and cases can be created 
	irrespective of activity taking place on the customer's accounts.
5.	Excluded Names Continue to appear as unconfirmed cases.
6.	Batch Filter Large Address Space Changes.
7.	Partial DOB matching and DOB tolerance functionality originally done for FIS EMEA.
8.	Script to Add ‘USER’ in ListTypeAssociation Table, if it does not exist.



Description of changes:
-------------------------
1.	This patch address the following issues: Dow Jones Import Functionality 
	User Interface including Help and Tool Tips, Auto Mode and Manual mode functionality 
	Reading and validating Configuration file (Including Org Code)
	User and User rights validation
	Reading the XML Content (Person, Entity and Association) and Content validation
	Writing the SDN Records and Related Data to database (Partial Import functionality)

2.	Previously, OFAC Reporter had supported only Prime Databank to update subscription 
	details and Build File Image utility consumed up to 2 GB memory to build file 
	Images. OFAC Reporter is now enhanced to support Third Party Lists without using 
	Prime Databank. Build File Image utility is also enhanced to consume more than 2 GB
	Memory to build larger size file images.

3.	There was an issue found while handling with the UK format date while 
	loading the Dow Jones file. Addressed UK date format issue with this patch. 

4.	This deploys a new SQL Stored Procedure named USR_SARFollowUp which is to be used 
	to create a user defined rule from in the BSA Rule screen, using the SQL Stored 
	Procedure option. The rule will create alerts/cases  (according to the user 
	defined rule definition) solely based on Customer data (activity is not required). 
	The filter conditions for this rule are ProbationReason = SARFILED, Onprobation = 1, 
	and the date range is starting from the current month till the end of the 
	number of months specified by the user in the LookForwardMonths parameter.

5.	The KeyField1 and KeyField2 are modified to hold the Field Offset values for Fixed Type to 
	address the issue in the creation of proper Exclude names from OFAC Case screen.
	
6.	Batch Filter has been enhanced to use the new larger address changes brought about by
	the ability to create larger file images.
	
7.	The new OFAC DOB Matching enhancements will be made available for the 
	OFAC Reporter Web Service and the Batch Filter process.
	The new functionality will be configurable at the tenant level via Sanction Data Rules.
	
8.	Script to Add ‘USER’ in ListTypeAssociation Table, if it does not exist.


Note: This patch should be installed and tested in a Test environment before it is deployed 
      to a Production environment.

INSTRUCTIONS:
-------------

Patch Name: PCS10.0-GA-Patch#08
Patch Release Date: 2/26/2016
Version Applicable To: 10.0
Patch Pre-requisites: 

	Microsoft .Net 4.5

	All 10.0 Prime GA Patches including:
		1 CumulativeEnhancementFixes
		2 OFACWebServiceAndBSAArchiveCaseFix
		PCS10.0-GA-Patch#03
		PCS10.0-GA-Patch#04
		PCS10.0-GA-Patch#05
		PCS10.0-GA-Patch#06
		PCS10.0-GA-Patch#07



INSTRUCTIONS:
-------------

      Download the attached "PCS10.0-GA-Patch#08.zip" file, and extract the contents 
      to a temporary folder.

      The following files will be extracted:

	1) ConvOfac.sql
	2) ImportDowJones.txt
	3) Readme.txt
	4) Metavante.Prime.BusinessLayer.DataSets.txt
	5) Metavante.Prime.OFACBusinessLayer.txt
	6) Metavante.Prime.OFACDatabaseLayer.txt
	7) PFA5_201407312200_D.xml		(Sample Watchlist Daily Data File)
	8) DowJones_config.xml
	9) DJDataLoad.rpt
	10) DJChgList.rpt
	11) HelperObj.txt
	12) Metavante.Prime.DJModels.txt
	13) FastMapper.txt
	14) About.htm
	15) DJRC_AMe_XML_201509302359_F.xml	(Sample Negative News Data File)
	16) DJRC_AC_XML_201509302360_F.xml	(Sample State Owned Companies Data File)
	17) BuildFileImg.txt
	18) Dow Jones Import Utility.pdf 
	19) BatchFilter.txt
	20) PFA5_201407312200_F.xml		(Sample Watchlist Data File)
	21) CifFilter.txt
	22) CMBrowser.txt
	23) Interop.OFACObj.txt
	24) LoadWords.txt
	25) OFACFilter.txt
	26) OfacObj.txt
	27) OfacRemoting.txt
	28) ofacsrv.txt
	29) PriUtil.txt
	30) SanctionedDataRule.aspx
	31) SanctionFilterEngine.txt
	32) SDNDbSrv.txt
	33) ServerAPI.txt
	34) ServerClasses.txt
	35) ServiceThreads.txt
	36) Sfa.txt
	37) SfaCom.txt
	38) SfaNet.txt
	39) DOB Partial Match.pdf
	40) AutoLoad.txt 
	41) ConvPcdb.sql
	42) AutoLoadEx.txt
	43) LoadandBuild.txt
	44) OfacOption.aspx 
	45) OFAC Rptr 3rd Party List Support.pdf
	46) SFAX.txt
	47) USR_SARFollowup.sql  
	48) SAR Follow Up Rule Stored Procedure.docx - Note: This document 
	    contains information  on using the new stored procedure and should be shared with client.

APPLICATION SERVER:
-------------------

	1. Stop Internet Information Services

	2. Stop all Prime Windows services including the following:
		a. OFAC Server
		b. SDN Database
		c. Prime Logger
		d. AsyncUtilServer

		Note:

		Make backup by copying original files to another folder, created
		specially for backup purposes outside of the installation folder.
		Do not backup by renaming original files and leaving them in the
		installation folder. 

	3. Backup the following files from the path %PRIME%\Exe\
	   where %PRIME% refers to the PRIME installation folder
	   Note: If the files do not exist, disregard.

		a) BatchFilter.exe
		b) BuildFileImg.exe
		c) CifFilter.exe
		d) Interop.OFACObj.dll
		e) LoadWords.exe
		d) OFACFilter.ocx
		e) OfacObj.dll
		f) ofacsrv.exe
		g) PriUtil.dll
		h) SanctionFilterEngine.dll
		i) SDNDbSrv.exe
		j) ServerAPI.dll
		k) ServerClasses.dll
		l) ServiceThreads.dll
		m) Sfa.dll
		n) SfaCom.dll
		o) SfaNet.dll
		p) ImportDowJones.exe (If it exists)
		q) Metavante.Prime.BusinessLayer.DataSets.dll
		r) Metavante.Prime.OFACBusinessLayer.dll
		s) Metavante.Prime.OFACDatabaseLayer.dll
		t) HelperObj.dll
		u) AutoLoad.exe
		v) AutoLoadEx.exe
		w) LoadandBuild.exe
		x) SFAX.exe
		y) Metavante.Prime.DJModels.dll
		z) FastMapper.dll


	4. Backup the following files from the path %PRIME%\ComplianceManager\bin\
		
		a) Metavante.Prime.BusinessLayer.DataSets.dll
		b) Metavante.Prime.OFACBusinessLayer.dll (If Exists)
		c) Metavante.Prime.OFACDatabaseLayer.dll (If Exists)
		d) CMBrowser.dll
		e) Interop.OfacObj.dll		
	
	5. Backup the following files from the path %PRIME%\ComplianceManager

		a) SanctionedDataRule.aspx	
		b) About.htm
		c) OfacOption.aspx (If Exists)	

	6. Backup the following files from %PRIME%\Services\OFACReporter\OFACSearchWebService\bin  

		a) Metavante.Prime.OFACBusinessLayer.dll
		b) Metavante.Prime.OFACDatabaseLayer.dll

	7. Backup the following files from the path %PRIME%\Documents\OFAC folder	   
	   Note: If the files do not exist, disregard.

		a) OFAC Rptr 3rd Party List Support.pdf
		b) Dow Jones Import Utility.pdf
		c) DOB Partial Match.pdf

	8. Backup the following files from the path %PRIME%\Reports\
	   where %PRIME% refers to the PRIME installation folder.  
	   Note: If these do not exist, disregard.

		a) DJDataLoad.rpt
		b) DJChgList.rpt

	9. Backup the following files from the path %PRIME%\%Tenant%\Services\OfacReporter\OFACSearchRemotingService
   		where %PRIME% refers to the PRIME installation folder and where %Tenant% refers to the tenant installation folder			
		a) OfacRemoting.exe
		b) Sfa.dll
		c) SfaNet.dll

	10. Unregister OFACFilter.ocx by running the following from
		command prompt: REGSVR32 /u %PRIME%\EXE\OFACFilter.ocx
		where %PRIME% refers to the PRIME installation folder

	11. Unregister OFACObj.dll by running the following from
		command prompt: REGSVR32 /u %PRIME%\EXE\OFACObj.dll
		where %PRIME% refers to the PRIME installation folder

	12. Rename the extension of the following extracted files from the patch
		a) BatchFilter.txt to BatchFilter.exe
		b) BuildFileImg.txt to BuildFileImg.exe
		c) CifFilter.txt to CifFilter.exe
		d) Interop.OFACObj.txt to Interop.OFACObj.dll
		e) LoadWords.txt to LoadWords.exe
		f) OFACFilter.txt to OFACFilter.ocx
		g) OfacObj.txt to OfacObj.dll
		h) ofacsrv.txt to ofacsrv.exe
		i) PriUtil.txt to PriUtil.dll
		j) SanctionFilterEngine.txt to SanctionFilterEngine.dll
		k) SDNDbSrv.txt to SDNDbSrv.exe
		l) ServerAPI.txt to ServerAPI.dll
		m) ServerClasses.txt to ServerClasses.dll
		n) ServiceThreads.txt to ServiceThreads.dll
		o) Sfa.txt to Sfa.dll
		p) SfaCom.txt to SfaCom.dll
		q) SfaNet.txt to SfaNet.dll
		r) CMBrowser.txt to CMBrowser.dll
		s) OfacRemoting.txt to OfacRemoting.exe
		t) Metavante.Prime.BusinessLayer.DataSets.txt to Metavante.Prime.BusinessLayer.DataSets.dll	
		u) Metavante.Prime.OFACBusinessLayer.txt to Metavante.Prime.OFACBusinessLayer.dll
		v) Metavante.Prime.OFACDatabaseLayer.txt to Metavante.Prime.OFACDatabaseLayer.dll
		w) ImportDowJones.txt to ImportDowJones.exe
		x) HelperObj.txt to HelperObj.dll	
		y) Metavante.Prime.DJModels.txt to Metavante.Prime.DJModels.dll
		z) FastMapper.txt to FastMapper.dll
		aa) AutoLoad.txt to AutoLoad.exe
		bb) AutoLoadEx.txt to AutoLoadEx.exe
		cc) LoadandBuild.txt to LoadandBuild.exe
		dd) SFAX.txt to SFAX.exe  

	13. Copy the following extracted file to %PRIME%\ComplianceManager
   		where %PRIME% refers to the PRIME installation folder		
	
		a) SanctionedDataRule.aspx	
		b) About.htm
		c) OfacOption.aspx

	14. Copy the following extracted file to %PRIME%\ComplianceManager\bin
   		where %PRIME% refers to the PRIME installation folder		
		a) Metavante.Prime.BusinessLayer.DataSets.dll
		b) Metavante.Prime.OFACBusinessLayer.dll
		c) Metavante.Prime.OFACDatabaseLayer.dll
		d) CMBrowser.dll
		e) Interop.OfacObj.dll	

	15. Copy the following extracted files to %PRIME%\%Tenant%\Services\OfacReporter\OFACSearchRemotingService
   		where %PRIME% refers to the PRIME installation folder and where %Tenant% refers to the tenant installation folder			
		a) OfacRemoting.exe
		b) Sfa.dll
		c) SfaNet.dll

	16. Copy the following renamed files to %PRIME%\Services\OFACReporter\OFACSearchWebService\bin  

		a) Metavante.Prime.OFACBusinessLayer.dll
		b) Metavante.Prime.OFACDatabaseLayer.dll

	17. Copy the following extracted files to %PRIME%\Exe
   		where %PRIME% refers to the PRIME installation folder		
	
		a) BatchFilter.exe
		b) BuildFileImg.exe
		c) CifFilter.exe
		d) Interop.OFACObj.dll
		e) LoadWords.exe
		f) OFACFilter.ocx
		g) OfacObj.dll
		h) ofacsrv.exe
		i) PriUtil.dll
		j) SanctionFilterEngine.dll
		k) SDNDbSrv.exe
		l) ServerAPI.dll
		m) ServerClasses.dll
		n) ServiceThreads.dll
		o) Sfa.dll
		p) SfaCom.dll
		q) SfaNet.dll
		r) Metavante.Prime.BusinessLayer.DataSets.dll	
		s) Metavante.Prime.OFACBusinessLayer.dll
		t) Metavante.Prime.OFACDatabaseLayer.dll
		u) ImportDowJones.exe
		v) HelperObj.dll	
		w) Metavante.Prime.DJModels.dll
		x) FastMapper.dll
		y) AutoLoad.exe
		z) AutoLoadEx.exe
		aa) LoadandBuild.exe
		bb) SFAX.exe   

	18. Register OFACFilter.ocx by running the following from
		command prompt: REGSVR32 %PRIME%\EXE\OFACFilter.ocx
		where %PRIME% refers to the PRIME installation folder
		
	19. Register OFACObj.dll by running the following from
		command prompt: REGSVR32 %PRIME%\EXE\OFACObj.dll
		where %PRIME% refers to the PRIME installation folder

	20. Start Internet Information Services

	21. Start all Prime Windows services including the following:
		a. OFAC Server
		b. SDN Database
		c. Prime Logger
		d. AsyncUtilServer

	22.Copy the following files to the folder  %PRIME%\Reports\

		a) DJDataLoad.rpt
		b) DJChgList.rpt

	  Please Note: If the following report files exist, please delete them:
		a) DowJonesLoad.rpt
		b) DowJonesSancChangeList.rpt

    	23. Copy the following files to the %PRIME%\Documents\OFAC folder

		a) OFAC Rptr 3rd Party List Support.pdf
		b) Dow Jones Import Utility.pdf
		c) DOB Partial Match.pdf
		Review this file for details on the functionality changes deployed by this patch.

	24. Create Environment Variable "DowJonesOrgCode" (or other variable name as desired). Below are the instructions
		
		a) Click on Start 
		b) Right Click on Computer, Select Properties 
		c) Click on "Advanced System Settings" 	
		d) Select the tab "Advanced"	
		e) Click on the button "Environment Variables"
		f) Click on "New" Button under the 'System Variables' section 
		g) Enter the Variable name as "DowJonesOrgCode" in the text box (value should be left blank by entering a space in necessary) and click OK button.	

		The EnvironmentVariableName in the configuration file should match the name of the variable created here.

	25. Copy the following Sample XML Input files to the Tenant folder (eg:%PRIME%\Tenants\PCST1).
		a) PFA5_201407312200_F.xml (Watchlist, Full File)
		b) PFA5_201407312200_D.xml  (Watchlist, Daily File)
		c) DJRC_AMe_XML_201509302359_F.xml (Negative News)
		d) DJRC_AC_XML_201509302360_F.xml (State Owned Companies)
	    	to the Tenant folder (eg:%PRIME%\Tenants\PCST1).
		
		If required the given sample XML file can be used for testing.


	26. Copy the XML Configuration file ("DowJones_config.xml") to the Tenant folder (eg:%PRIME%\Tenants\PCST1).


	27. Configure below items in the configuration file ("DowJones_config.xml") as needed in the environment.
		
		a) InputFile
		b) ErrorFolder
		b) BackupFolder




DATABASE SERVER:
----------------
  
	*Note: Backup the OFAC, PBSA And PCDB databases before executing the following steps.
	*Execute the following for all tenants:

	1. Execute "ConvOfac.sql" script submitted with this patch in OFAC database.

	2. Execute "ConvPcdb.sql" script submitted with this patch in PCDB database.

	3. Execute  USR_SARFollowup.sql provided with the patch in PBSA database.
	
	4. Execute "SQLSecurity.exe" on the OFAC, PCDB, and PBSA databases (SQLSecurity.exe already exists on the server).
