<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SanctionedDataRule.aspx.vb"
    Inherits="CMBrowser.SanctionedDataRule" SmartNavigation="True" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNote" Src="ucNote.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Sanction Data Rule</title>
    <meta content="False" name="vs_showGrid">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles.css" type="text/css" rel="stylesheet">
    <style type="text/css">.tgridheader { BORDER-RIGHT: white outset; PADDING-RIGHT: 2px; BORDER-TOP: white outset; PADDING-LEFT: 2px; FONT-WEIGHT: bold; FONT-SIZE: x-small; PADDING-BOTTOM: 2px; BORDER-LEFT: white outset; COLOR: white; PADDING-TOP: 2px; BORDER-BOTTOM: white outset; BACKGROUND-COLOR: #990000; TEXT-ALIGN: left }
	.lblgridcell { FONT-SIZE: 12px; TEXT-ALIGN: left; Font-Names: Verdana, Arial, Helvetica, sans-serif }
	.BoldRed { FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: red }
	.BoldBlack { FONT-WEIGHT: bold; FONT-SIZE: small; COLOR: black }
	.NormalRed { FONT-WEIGHT: normal; FONT-SIZE: x-small; COLOR: red }
	.NormalBlack { FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: black }
	.NormalLink { FONT-WEIGHT: normal; FONT-SIZE: 11px }
	.BoldBlue { FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: blue }
		</style>

    <script language="JavaScript" src="js/nav.js"></script>

    <script language="javascript" id="clientEventHandlersJS">
<!--

function chkAllComb_onclick()
{
	try
	{		
		var ddlMaxWords = document.getElementById("ddlMaximumDerivedWords");		
		var chkAllComb = document.getElementById("chkAllComb");		
		if (chkAllComb.checked)
			ddlMaxWords.disabled=false;
		else
			ddlMaxWords.disabled=true;
	}
	catch(ex){}

}

//-->
    </script>

    <link href="Styles.css" rel="stylesheet" type="text/css" />
    <link href="Styles.css" rel="stylesheet" type="text/css" />
</head>
<body language="javascript" class="formbody" ms_positioning="FlowLayout">
    <form id="Form1" method="post" runat="server">
        <table id="PageLayout" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td width="600">
                                <asp:ImageButton ID="imgPrime" runat="server" Width="808px" ImageUrl="images\compliance-manager.gif"
                                    AlternateText="Home"></asp:ImageButton></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="clblRecordMessage" runat="server" Width="808px" CssClass="RecordMessage">Record Message</asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:ValidationSummary ID="cValidationSummary1" runat="server" Height="24px" Font-Size="X-Small"
                        Width="616px"></asp:ValidationSummary>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="Table2" style="width: 816px; height: 40px" cellspacing="0" cellpadding="0"
                        width="816" border="0">
                        <tr align="center">
                            <td align="right">
                                <asp:PlaceHolder ID="ResetPlaceHolder" runat="server"></asp:PlaceHolder>
                                <asp:PlaceHolder ID="cPlaceHolder1" runat="server"></asp:PlaceHolder>
                            </td>
                            <td align="left">
                                <uc1:ucRecordScrollBar ID="RecordScrollBar" runat="server"></uc1:ucRecordScrollBar>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td width="5">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <uc1:ucTabControl ID="MainTabControl" runat="server"></uc1:ucTabControl>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="clblRecordHeader" runat="server" Width="804px" CssClass="paneltitle"></asp:Label></td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Panel ID="Panel1" runat="server" Height="176px" Width="744px" BorderStyle="None"
                        BorderWidth="1px" CssClass="Panel">
                        <table id="Table1" cellspacing="0" cellpadding="0" width="300" border="0">
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" CssClass="LABEL">Code:</asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtCode" MaxLength="11" runat="server" CssClass="TEXTBOX" ToolTip="Sanction Data Rule Code is mandatory"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label5" runat="server" Width="75px" CssClass="LABEL">Name:</asp:Label></td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtName" runat="server" Width="368px" CssClass="TEXTBOX" ToolTip="Sanction Data Rule Name is mandatory"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label8" runat="server" CssClass="LABEL" ToolTip="Control the number of matches returned">Match Level:</asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlMatchLevel" runat="server" CssClass="DropDownList" AutoPostBack="True">
                                    </asp:DropDownList></td>
                                <td>
                                </td>
                                <td colspan="3">
                                    <asp:CheckBox ID="chkEnabled" runat="server" CssClass="CheckBox" ToolTip="Enable or Disable this rule"
                                        Checked="True" Text="Enabled"></asp:CheckBox></td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td colspan="3">
                                    <asp:CheckBox ID="chkGenerateScore" runat="server" CssClass="CheckBox" ToolTip="Generate score for matches"
                                        Text="Generate Score"></asp:CheckBox></td>
                            </tr>
                        </table>
                        <br>
                        <asp:Label ID="Label1" runat="server" Width="804px" CssClass="paneltitle" Text="Included Party Types">Party Types</asp:Label>
                        <asp:DataGrid ID="dgPartyTypes" runat="server" Width="568px" BorderWidth="1px" BorderStyle="Solid"
                            CssClass="viewgrid" ToolTip="List of the available sanctioned party type. Check the checkbox to include and uncheck to exclude."
                            BackColor="White" BorderColor="#CC9966" CellPadding="0" GridLines="Vertical"
                            ForeColor="Black" AutoGenerateColumns="False">
                            <FooterStyle BackColor="#CCCCCC"></FooterStyle>
                            <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
                            <AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
                            <HeaderStyle CssClass="gridheader"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn>
                                    <HeaderStyle Width="1px" CssClass="gridheader"></HeaderStyle>
                                    <HeaderTemplate>
                                        Included
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkPartyType" Text="" TextAlign="left" Width="1px" runat="server"
                                            Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container.DataItem, "Included").ToString()) %>'></asp:CheckBox>
                                        <input id="hdfldPartyTypeId" runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.PartyTypeId").ToString())%>'
                                            name="Hidden1"/>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Available Party Types">
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    <ItemStyle Font-Size="11px"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="PartyType" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.PartyType").ToString())%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
                        </asp:DataGrid>
                        <table>
                            <tr>
                                <td width="365">
                                    &nbsp;</td>
                                <td>
                                    <asp:Button ID="cmdSelectAllParties" runat="server" ToolTip="Include All Party Types"
                                        Text="Select All"></asp:Button></td>
                                <td>
                                    <asp:Button ID="cmdUnselectAllParties" runat="server" ToolTip="Exclude All Party Types"
                                        Text="Unselect All"></asp:Button></td>
                            </tr>
                        </table>
                        <br>
                        <asp:Label ID="Label3" runat="server" Width="804px" CssClass="paneltitle" Text="Included Program Types">List Types and Programs</asp:Label>
                        <uc1:ucView ID="SDNListProg" runat="server"></uc1:ucView>
                    </asp:Panel>
                    <asp:Panel ID="Panel2" runat="server" Height="15px" Width="712px" CssClass="Panel">
                        <table id="Table11" cellspacing="0" cellpadding="0" width="300" border="0">
                            <tr>
                                <td>
                                    <asp:Label ID="Label10" runat="server" CssClass="LABEL" ToolTip="Control the number of matches returned">Match Level:</asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlMatchLevel2" runat="server" CssClass="DropDownList" AutoPostBack="True">
                                    </asp:DropDownList></td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkUseAddresses" runat="server" CssClass="CheckBox" ToolTip="Check to consider first line of sanctioned party address for filtering"
                                        AutoPostBack="True" Text="Use Addresses"></asp:CheckBox></td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkUseKeywords" runat="server" CssClass="CheckBox" ToolTip="Check to consider keywords for filtering"
                                        AutoPostBack="True" Text="Use Keywords"></asp:CheckBox></td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                            </tr>
                        </table>
                        <asp:Label ID="lblDerivedNames" runat="server" Width="804px" CssClass="paneltitle">Name Combination Options</asp:Label>
                        <table style="width: 800px; height: 120px" cellspacing="0" cellpadding="0" width="800"
                            border="0">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkFMLName" runat="server" CssClass="CheckBox" ToolTip='Ex. "Juan Raul Diaz Lopez" will create names "Juan Raul", "Juan Diaz", "Juan Lopez"'
                                        Text="Create 2 word combinations, beginning with first word of first name"></asp:CheckBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkLFMName" runat="server" CssClass="CheckBox" ToolTip='Ex. "Juan Raul Diaz Lopez" with last name "Diaz Lopez" will create names "Diaz Juan", Diaz Raul","Lopez Juan", "Lopez Raul", "Diaz Lopez Juan","Diaz Lopez Raul"'
                                        Text="Create word combinations, beginning with first word of last name"></asp:CheckBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkMiddleIntials" runat="server" CssClass="CheckBox" ToolTip='Ex. "Juan Raul Diaz Lopez" with last name "Diaz Lopez" will create names "J Raul Diaz Lopez", "Juan R Diaz Lopez","J R Diaz Lopez"'
                                        Text="Create combinations, replacing first and middle names with initials"></asp:CheckBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkLastname" runat="server" CssClass="CheckBox" ToolTip='Ex. "Juan Raul Diaz Lopez" with last name "Diaz Lopez" will create name "Diaz Lopez"'
                                        Text="Create names using just last name"></asp:CheckBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <input language="javascript" id="chkAllComb" title='Ex. "Juan Raul Diaz Lopez" creates names: all 2, 3, or more words names like "Juan Raul", "Raul Lopez", "Juan Raul Diaz", "Raul Diaz Lopez" and more...'
                                        style="width: 20px; height: 20px; margin-right: -7px;" onclick="return chkAllComb_onclick()"
                                        type="checkbox" size="20" value="" name="chkAllComb" runat="server" />
                                    <asp:Label ID="Label17" runat="server" Width="477px" Height="13px" CssClass="LABEL">Create all possible name combinations, using each word (left to right)</asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label4" Font-Names="Verdana" runat="server" Font-Size="11px" Font-Bold="True">Maximum number of words when creating combinations</asp:Label>
                                    <asp:DropDownList ID="ddlMaximumDerivedWords" runat="server" Width="88px" CssClass="DropDownList">
                                    </asp:DropDownList></td>
                            </tr>
                        </table>
                        <br>
                        <asp:Label ID="Label19" runat="server" Width="804px" CssClass="paneltitle" Text="Include Concatenated Name Parts">Include Concatenated Name Parts</asp:Label>
                        <div style="height: 122px; overflow-y: auto; overflow-x: none; width: 583px">
                            <asp:DataGrid ID="dgCombWords" runat="server" Width="568px" BorderWidth="1px" BorderStyle="Solid"
                                CssClass="viewgrid" ToolTip="List of the available list types for concatenated words. Check the checkbox to include and uncheck to exclude."
                                BackColor="White" BorderColor="#CC9966" CellPadding="0" GridLines="Vertical"
                                ForeColor="Black" AutoGenerateColumns="False">
                                <FooterStyle BackColor="#CCCCCC"></FooterStyle>
                                <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
                                <AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
                                <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderStyle Width="1px" CssClass="gridheader"></HeaderStyle>
                                        <HeaderTemplate>
                                            Included
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkListType" Text="" TextAlign="left" Width="1px" runat="server"
                                                Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container.DataItem, "Included").ToString()) %>'></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Available List Types">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle Font-Size="11px"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="ListType" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ListType").ToString())%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
                            </asp:DataGrid>
                        </div>
                        <table>
                            <tr>
                                <td width="365">
                                    &nbsp;</td>
                                <td>
                                    <asp:Button ID="cmdCombWordsIncludeAll" runat="server" ToolTip="Include All List Types"
                                        Text="Select All"></asp:Button></td>
                                <td>
                                    <asp:Button ID="cmdCombWordsExcludeAll" runat="server" ToolTip="Exclude All List Types"
                                        Text="Unselect All"></asp:Button></td>
                            </tr>
                        </table>
                        <br />
                        <asp:Label ID="lblNameParts" runat="server" Width="804px" CssClass="paneltitle">Algorithm Options</asp:Label>
                        <table id="Table5" cellspacing="0" cellpadding="0" width="300" border="0">
                            <tr>
                                <td>
                                    <asp:Label ID="Label6" runat="server" CssClass="LABEL" ToolTip="Minimum length of the search string for misspelling to be applicable. Set it higher value to return fewer matches">Min Misspelling characters:</asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtMinMisspellingChars" runat="server" CssClass="TEXTBOX" ToolTip="Minimum length of the search string for misspelling to be applicable. Set it higher value to return fewer matches"
                                        AutoPostBack="True"></asp:TextBox></td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" CssClass="LABEL" ToolTip="Length of the final translated word. Set it to a higher value to return lower matches">Translated Word Len:</asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlTranslatedWordLen" runat="server" CssClass="DropDownList"
                                        AutoPostBack="True">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 22px">
                                    <asp:Label ID="Label9" runat="server" CssClass="LABEL" ToolTip="Maximum number of words in the search string for misspeliing to be applicable. Set it to higher value to return lower matches.">Max Words:</asp:Label></td>
                                <td style="height: 22px">
                                    <asp:DropDownList ID="ddlMaxWords" runat="server" CssClass="DropDownList" AutoPostBack="True">
                                    </asp:DropDownList></td>
                                <td style="height: 22px">
                                    <asp:Label ID="Label11" runat="server" CssClass="LABEL" ToolTip="Minimum number of words in the search string for misspeliing to be applicable. Set it to higher value to return fewer matches.">Min Words:</asp:Label></td>
                                <td style="height: 22px">
                                    <asp:DropDownList ID="ddlMinWords" runat="server" CssClass="DropDownList" AutoPostBack="True">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="Label12" runat="server" CssClass="LABEL" ToolTip="Maximum allowed words between parts of a sanction name to generate a match">Max Intervening Words:</asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlMaxInterveningWords" runat="server" CssClass="DropDownList"
                                        AutoPostBack="True">
                                    </asp:DropDownList></td>
                                <td>
                                    <asp:Label ID="Label18" runat="server" CssClass="LABEL" ToolTip="Check to include words that split across the line boundary as one word.">Word Wrap Match:</asp:Label></td>
                                <td>
                                    <asp:CheckBox ID="chkWordWrap" runat="server" CssClass="CheckBox" Style="margin-left: -3px;">
                                    </asp:CheckBox></td>
                            </tr>
                        </table>
                        <br />
                        <asp:Label ID="lblDOBMatchingOptions" runat="server" Width="804px" CssClass="paneltitle">DOB Matching Options</asp:Label>
                        <table id="tblDOBMatchingOptions" cellspacing="0" cellpadding="0" width="300" border="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblEnablePartialDOBMatching" runat="server" CssClass="LABEL" ToolTip="Enable Partial DOB Matching.">Enable Partial DOB Matching:</asp:Label>
								</td>
                                <td>
                                    <asp:CheckBox ID="chkBxEnablePartialDOBMatching" runat="server" CssClass="CheckBox" Style="margin-left: -3px;">
                                    </asp:CheckBox>
								</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblDOBToleranceInYears" runat="server" CssClass="LABEL" ToolTip="DOB Tolerance In Years">DOB Tolerance In Years (0-99):</asp:Label>
								</td>
                                <td>
                                    <asp:TextBox ID="txtBxDOBToleranceInYears" runat="server" Width="30px" CssClass="TEXTBOX" ToolTip="DOB Tolerance In Years (0-99)"
                                        AutoPostBack="True"></asp:TextBox>
								</td>
							</tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="Panel3" runat="server" Height="15px" Width="712px" CssClass="Panel">
                        &nbsp;
                        <asp:ScriptManager ID="ScriptManager1" runat="server">
                        </asp:ScriptManager>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label13" runat="server" Width="804px" CssClass="paneltitle" Text="Branch/Department Associations:"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkAssignEnterprise" runat="server" CssClass="CheckBox" ToolTip="Assign/Remove enterprise scope"
                                                AutoPostBack="True" Text="Make available to all Branches and Departments" Checked="True"
                                                OnCheckedChanged="chkAssignEnterprise_CheckedChanged"></asp:CheckBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label33" runat="server" Width="64px" CssClass="LABEL">Branch:</asp:Label></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSDRBranch" runat="server" CssClass="DropDownList">
                                                            </asp:DropDownList></td>
                                                        <td>
                                                            <asp:Label ID="Label34" runat="server" Width="100px" Height="10px" CssClass="LABEL">Department:</asp:Label></td>
                                                        <td style="width: 211px">
                                                            <asp:DropDownList ID="ddlSDRDept" runat="server" CssClass="DropDownList">
                                                            </asp:DropDownList></td>
                                                        <td>
                                                            <asp:Button ID="cmdSDRAddBranchDept" runat="server" Width="64px" Text="Add"></asp:Button></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">
                                                            <asp:Label ID="lblSDRMsg" runat="server" Width="560px" Font-Size="8pt" ForeColor="Red"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 76px; height: 34px" colspan="4">
                                                            <asp:DataGrid ID="dgBranchDept" runat="server" Width="568px" BorderWidth="1px" BorderStyle="Solid"
                                                                CssClass="viewgrid" ToolTip="List of the branches and departments to which the List will be available"
                                                                BackColor="White" BorderColor="#CC9966" CellPadding="0" GridLines="Vertical"
                                                                ForeColor="Black" AutoGenerateColumns="False" OnItemCommand="SDRBranchDept_Clicked">
                                                                <FooterStyle BackColor="#CCCCCC"></FooterStyle>
                                                                <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
                                                                <AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
                                                                <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="Branch">
                                                                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                                                        <ItemStyle Font-Size="11px"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Branch" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Branch").ToString())%>'>
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Department">
                                                                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                                                        <ItemStyle Font-Size="11px"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Department" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Department").ToString())%>'>
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:ButtonColumn Text="Remove" CommandName="Remove">
                                                                        <ItemStyle Font-Size="11px"></ItemStyle>
                                                                    </asp:ButtonColumn>
                                                                </Columns>
                                                                <PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
                                                            </asp:DataGrid></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    <asp:Panel ID="Panel4" runat="server" Height="46px" Width="808px">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <table id="Table4" cellspacing="0" cellpadding="0" width="300" border="0">
                                    <tr>
                                        <td colspan="7">
                                            <asp:Label ID="Label32" runat="server" Width="804px" CssClass="paneltitle" Text="Exclude Names:">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="7">
                                            <asp:CheckBox ID="chkUseExcludes" runat="server" CssClass="CheckBox" ToolTip="Enable or disable exclude name processing."
                                                AutoPostBack="True" Checked="True" Text="Use Excludes"></asp:CheckBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 35px">
                                        </td>
                                        <td style="font-size: 11px; height: 35px" colspan="6">
                                            <asp:DropDownList ID="ddlIncludeExclude" runat="server" Width="100" CssClass="DropDownList"
                                                AutoPostBack="True">
                                            </asp:DropDownList>&nbsp;Include/Exclude names for the selected branches and departments
                                            only.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label15" runat="server" Width="64px" CssClass="LABEL">Branch:</asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlBranch" runat="server" CssClass="DropDownList">
                                            </asp:DropDownList></td>
                                        <td>
                                            <asp:Label ID="Label16" runat="server" Width="100px" Height="10px" CssClass="LABEL">Department:</asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="DropDownList">
                                            </asp:DropDownList></td>
                                        <td>
                                            <asp:Button ID="cmdAddBranchDept" runat="server" Width="64px" Text="Add"></asp:Button></td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 76px; height: 34px" colspan="6">
                                            <asp:Label ID="lblFilterExcludeMsg" runat="server" Width="560px" ForeColor="Red"
                                                Font-Size="8pt"></asp:Label>
                                            <asp:DataGrid ID="dgExcludeBranchDept" runat="server" Width="568px" BorderWidth="1px"
                                                BorderStyle="Solid" CssClass="viewgrid" ToolTip="List of the branches and departments to include or exclude"
                                                BackColor="White" BorderColor="#CC9966" CellPadding="0" GridLines="Vertical"
                                                ForeColor="Black" AutoGenerateColumns="False" OnItemCommand="ExcludeBranchDept_Clicked">
                                                <FooterStyle BackColor="#CCCCCC"></FooterStyle>
                                                <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
                                                <AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
                                                <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Status">
                                                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                                        <ItemStyle Font-Size="11px"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Status" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Status").ToString())%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Branch">
                                                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                                        <ItemStyle Font-Size="11px"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Branch" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Branch").ToString())%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Department">
                                                        <HeaderStyle CssClass="gridheader" />
                                                        <ItemStyle Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="Department" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Department").ToString())%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:ButtonColumn Text="Remove" CommandName="Remove">
                                                        <ItemStyle Font-Size="11px"></ItemStyle>
                                                    </asp:ButtonColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
                                            </asp:DataGrid></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <asp:CheckBox ID="chkUseUnassignedExcludes" runat="server" CssClass="CheckBox" ToolTip="Also consider exclude names that have not been assigned to any branch nor department"
                                                Text="Use unassigned Excludes"></asp:CheckBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <asp:CheckBox ID="chkApplyExcludeSwiftTags" runat="server" CssClass="CheckBox" ToolTip="Compare Exclude Names SWIFT tag information to message tags before applying exclude names"
                                                Text="Apply Exclude Names based on SWIFT tags"></asp:CheckBox></td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <table id="Table8" cellspacing="0" cellpadding="0" width="300" border="0">
                            <tr>
                                <td colspan="7">
                                    <asp:Label ID="Label28" runat="server" Width="804px" CssClass="paneltitle" Text="Inequalities:"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <%--                                                            <td>
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckBox1" runat="server" CssClass="CheckBox" ToolTip="Check to include Inequalities"
                                        AutoPostBack="True" Text="Use Inequalities"></asp:CheckBox></td>
                                <td>
                                </td>--%>
                                <td colspan="7">
                                    <asp:CheckBox ID="chkUseInequalities" runat="server" CssClass="CheckBox" ToolTip="Enable or disable inequalities."
                                        AutoPostBack="True" Text="Use Inequalities"></asp:CheckBox></td>
                            </tr>
                            <tr>
                                <td style="height: 35px">
                                </td>
                                <td style="font-size: 11px; height: 35px" colspan="6">
                                    <asp:DropDownList ID="ddlIneqIncludeExclude" runat="server" Width="100" CssClass="DropDownList"
                                        AutoPostBack="True">
                                    </asp:DropDownList>&nbsp;Include/Exclude inqualities for the following branches/departments
                                    only.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label29" runat="server" Width="64px" CssClass="LABEL">Branch:</asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlIneqBranch" runat="server" CssClass="DropDownList">
                                    </asp:DropDownList></td>
                                <td>
                                    <asp:Label ID="Label30" runat="server" Width="100px" Height="10px" CssClass="LABEL">Department:</asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlIneqDept" runat="server" CssClass="DropDownList">
                                    </asp:DropDownList></td>
                                <td>
                                    <asp:Button ID="cmdInEqAddBranchDept" runat="server" Width="64px" Text="Add"></asp:Button></td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 76px; height: 34px" colspan="6">
                                    <asp:Label ID="lblFilterInEqMsg" runat="server" Width="560px" ForeColor="Red" Font-Size="8pt"></asp:Label>
                                    <asp:DataGrid ID="dgInEqBranchDept" runat="server" Width="568px" BorderWidth="1px"
                                        BorderStyle="Solid" CssClass="viewgrid" ToolTip="List of the branches and departments to include or exclude"
                                        BackColor="White" BorderColor="#CC9966" CellPadding="0" GridLines="Vertical"
                                        ForeColor="Black" AutoGenerateColumns="False" OnItemCommand="InEqBranchDept_Clicked">
                                        <FooterStyle BackColor="#CCCCCC"></FooterStyle>
                                        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
                                        <AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
                                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Status">
                                                <HeaderStyle CssClass="gridheader" />
                                                <ItemStyle Font-Size="11px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="Status" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Status").ToString())%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Branch">
                                                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                                        <ItemStyle Font-Size="11px"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Branch" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Branch").ToString())%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Department">
                                                <HeaderStyle CssClass="gridheader" />
                                                <ItemStyle Font-Size="11px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="Department" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Department").ToString())%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:ButtonColumn Text="Remove" CommandName="Remove">
                                                <ItemStyle Font-Size="11px"></ItemStyle>
                                            </asp:ButtonColumn>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
                                    </asp:DataGrid></td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <asp:CheckBox ID="chkUseUnassignedInequality" runat="server" CssClass="CheckBox"
                                        ToolTip="Also consider inqualities that have not been assigned to any branch nor department"
                                        Text="Use unassigned Inequalities"></asp:CheckBox></td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="Panel5" runat="server" Height="46px" Width="808px">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <table id="Table6" cellspacing="0" cellpadding="0" width="300" border="0">
                                    <tr>
                                        <td colspan="7">
                                            <asp:Label ID="Label23" runat="server" Width="804px" CssClass="paneltitle" Text="Abbreviations:"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" style="height: 20px">
                                            <asp:CheckBox ID="chkUseAbbreviations" runat="server" CssClass="CheckBox" ToolTip="Enable or disable abbreviations."
                                                AutoPostBack="True" Checked="True" Text="Use Abbreviations" OnCheckedChanged="chkUseAbbreviations_CheckedChanged">
                                            </asp:CheckBox></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 35px">
                                        </td>
                                        <td style="font-size: 11px; height: 35px" colspan="6">
                                            <asp:DropDownList ID="ddlAbbrevIncludeExclude" runat="server" Width="100" CssClass="DropDownList"
                                                AutoPostBack="True">
                                            </asp:DropDownList>&nbsp;Include/Exclude abbreviations for the following branches/departments
                                            only.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label20" runat="server" Width="64px" CssClass="LABEL">Branch:</asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlAbbrevBranch" runat="server" CssClass="DropDownList">
                                            </asp:DropDownList></td>
                                        <td>
                                            <asp:Label ID="Label21" runat="server" Width="100px" Height="10px" CssClass="LABEL">Department:</asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlAbbrevDept" runat="server" CssClass="DropDownList">
                                            </asp:DropDownList></td>
                                        <td>
                                            <asp:Button ID="cmdAbbrevAddBranchDept" runat="server" Width="64px" Text="Add"></asp:Button></td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 76px; height: 34px" colspan="6">
                                            <asp:Label ID="lblFilterAbbrevMsg" runat="server" Width="560px" ForeColor="Red" Font-Size="8pt"></asp:Label>
                                            <asp:DataGrid ID="dgAbbrevBranchDept" runat="server" Width="568px" BorderWidth="1px"
                                                BorderStyle="Solid" CssClass="viewgrid" ToolTip="List of the branches and departments to include or exclude"
                                                BackColor="White" BorderColor="#CC9966" CellPadding="0" GridLines="Vertical"
                                                ForeColor="Black" AutoGenerateColumns="False" OnItemCommand="AbbrevBranchDept_Clicked">
                                                <FooterStyle BackColor="#CCCCCC"></FooterStyle>
                                                <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
                                                <AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
                                                <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Status">
                                                        <HeaderStyle CssClass="gridheader" />
                                                        <ItemStyle Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="Status" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Status").ToString())%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Branch">
                                                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                                        <ItemStyle Font-Size="11px"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Branch" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Branch").ToString())%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Department">
                                                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                                        <ItemStyle Font-Size="11px"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Department" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Department").ToString())%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:ButtonColumn Text="Remove" CommandName="Remove">
                                                        <ItemStyle Font-Size="11px"></ItemStyle>
                                                    </asp:ButtonColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
                                            </asp:DataGrid></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <asp:CheckBox ID="chkUseUnassignedAbbreviations" runat="server" CssClass="CheckBox"
                                                ToolTip="Also consider abbreviations that have not been assigned to any branch nor department"
                                                Text="Use unassigned Abbreviations" Checked="True"></asp:CheckBox></td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <table id="Table7" cellspacing="0" cellpadding="0" width="300" border="0">
                                    <tr>
                                        <td colspan="7">
                                            <asp:Label ID="Label24" runat="server" Width="804px" CssClass="paneltitle" Text="Noise Words:"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="7">
                                            <asp:CheckBox ID="chkUseNoiseWords" runat="server" CssClass="CheckBox" ToolTip="Enable or disable noise words."
                                                AutoPostBack="True" Checked="True" Text="Use Noise Words" OnCheckedChanged="chkUseNoiseWords_CheckedChanged">
                                            </asp:CheckBox></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 35px">
                                        </td>
                                        <td style="font-size: 11px; height: 35px" colspan="6">
                                            <asp:DropDownList ID="ddlNoiseIncludeExclude" runat="server" Width="100" CssClass="DropDownList"
                                                AutoPostBack="True">
                                            </asp:DropDownList>&nbsp;Include/Exclude noise words for the following branches/departments
                                            only.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label25" runat="server" Width="64px" CssClass="LABEL">Branch:</asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlNoiseBranch" runat="server" CssClass="DropDownList">
                                            </asp:DropDownList></td>
                                        <td>
                                            <asp:Label ID="Label26" runat="server" Width="100px" Height="10px" CssClass="LABEL">Department:</asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlNoiseDept" runat="server" CssClass="DropDownList">
                                            </asp:DropDownList></td>
                                        <td>
                                            <asp:Button ID="cmdNoiseAddBranchDept" runat="server" Width="64px" Text="Add"></asp:Button></td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 76px; height: 34px" colspan="6">
                                            <asp:Label ID="lblFilterNoiseMsg" runat="server" Width="560px" ForeColor="Red" Font-Size="8pt"></asp:Label>
                                            <asp:DataGrid ID="dgNoiseBranchDept" runat="server" Width="568px" BorderWidth="1px"
                                                BorderStyle="Solid" CssClass="viewgrid" ToolTip="List of the branches and departments to include or exclude"
                                                BackColor="White" BorderColor="#CC9966" CellPadding="0" GridLines="Vertical"
                                                ForeColor="Black" AutoGenerateColumns="False" OnItemCommand="NoiseBranchDept_Clicked">
                                                <FooterStyle BackColor="#CCCCCC"></FooterStyle>
                                                <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
                                                <AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
                                                <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Status">
                                                        <HeaderStyle CssClass="gridheader" />
                                                        <ItemStyle Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="Status" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Status").ToString())%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Branch">
                                                        <HeaderStyle CssClass="gridheader" />
                                                        <ItemStyle Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="Branch" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Branch").ToString())%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Department">
                                                        <HeaderStyle CssClass="gridheader" />
                                                        <ItemStyle Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="Department" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Department").ToString())%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:ButtonColumn Text="Remove" CommandName="Remove">
                                                        <ItemStyle Font-Size="11px"></ItemStyle>
                                                    </asp:ButtonColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
                                            </asp:DataGrid></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <asp:CheckBox ID="chkUseUnassignedNoise" runat="server" CssClass="CheckBox" ToolTip="Also consider noise words that have not been assigned to any branch nor department"
                                                Text="Use unassigned Noise Words" Checked="True"></asp:CheckBox></td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    <asp:Panel ID="Panel6" runat="server" Height="15px" Width="712px" CssClass="Panel">
                        <asp:Label ID="Label14" runat="server" Width="804px" CssClass="paneltitle">Build Options</asp:Label>
                        <table id="Table3" style="width: 536px; height: 72px" cellspacing="0" cellpadding="0"
                            width="536" border="0">
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbBuildOptionsFull" runat="server" CssClass="CheckBox" AutoPostBack="True"
                                        Text="Full Build" GroupName="BuildOptions"></asp:RadioButton></td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbBuildOptionsIncremental" runat="server" CssClass="CheckBox"
                                        AutoPostBack="True" Text="Incremental Build" GroupName="BuildOptions"></asp:RadioButton></td>
                                <td>
                                    <asp:Label ID="lblIncrementalHeader" runat="server" Width="200px" CssClass="LABEL">Select Names that have been modified</asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbBuildOptionsAuto" runat="server" CssClass="CheckBox" AutoPostBack="True"
                                        Checked="True" Text="Auto Build" GroupName="BuildOptions"></asp:RadioButton></td>
                                <td>
                                    <asp:DropDownList ID="ddlIncrementalBuild" runat="server" CssClass="DropDownList"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtIncrementalDays" runat="server" Width="55px" CssClass="TEXTBOX"></asp:TextBox>
                                    <asp:Label ID="lblIncrementalDays" runat="server" Font-Size="11px">days</asp:Label></td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="Panel7" runat="server" Height="46px" Width="808px">
                    </asp:Panel>
                    <asp:Panel ID="Panel8" runat="server" Height="46px" Width="808px">
                        <uc1:ucNote ID="RuleNotes" runat="server"></uc1:ucNote>
                    </asp:Panel>
                    <asp:Panel ID="cPanelValidators" runat="server" Height="46px" Width="864px">
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
