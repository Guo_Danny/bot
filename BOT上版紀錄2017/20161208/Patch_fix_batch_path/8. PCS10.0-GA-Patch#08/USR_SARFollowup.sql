/***************************************************************************
**  NOTE: Do not change the number Asterisks on the comment lines above
**  File Name:	USR_SARFollowup	
**
**  Functional Description: This stored proc allows the rule builder to 
							create alerts/cases irrespective of activity 
							taking place on the customer's accounts.
**  This module contains the Rule Script
**      
**  Facility	    BSA Reporter
**  Creation Date:  12/15/2014
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2014                                                   ***
*** FIS, Inc.                                                            ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
				       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|----------------------------------------------------
 12/15/2014   lc33265	 New stored proc allows the rule builder to 
						 create alerts/cases irrespective of activity 
						 taking place on the customer's accounts based on
						 the following criteria: 
						 a) ProbationReason = 'SARFILED'
						 b) Onprobation = 1
						 c) and retrieves data starting from the current
							month till the number of months forward 
							provided by the user.
						 
						 When LookForwardMonths is:
						 0 = end of current month
						 1 = end of next month
                         2 = end of the next two months after the current
                             month and so on.

NOTE: Do not change the number Asterisks on the comment lines below
****************************************************************************/
USE [PBSA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('USR_SARFollowup') is not null
	drop procedure [dbo].[USR_SARFollowup]
go
Create PROCEDURE [dbo].[USR_SARFollowup] (@WLCode SCode, @testAlert INT, @LookForwardMonths int)
AS
BEGIN

	DECLARE @Description VARCHAR(2000),
			@StartDate DATETIME,
			@TrnCnt INT,
			@WLType INT,
			@STAT INT
	
	SET NOCOUNT ON;
	SET @StartDate = GETDATE()
	
--- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */
	SET @TrnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @TrnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN USR_SARFollowup
	ELSE
		-- Already in a transaction
		SAVE TRAN USR_SARFollowup
	/* End stANDard stored procedure transaction header */
	
	/*****************************************/
	
	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam	
	SELECT @WLTYPE = WLTYPE, @StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList WITH (NOLOCK)
	WHERE WLCode = @WlCode;

-----------------------------------------------------------
	
	If @testAlert = 1
		begin
			INSERT INTO Alert
				 ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				   LASTMODIFY, CUST, ACCOUNT, IsTest) 
 			select @WLCode
 			     , Name + ' has a Probation Reason of "SAR Filed" and a Probation End Date of '+
				   dbo.BSA_ConvertIntToSqlDate(ProbationEndDate, 'mm/dd/yyyy')+'.'
			     , 0, GETDATE(), NULL, NULL, id, null , @testAlert 
 			  from [PBSA].[dbo].[Customer] WITH (NOLOCK)
			 where ProbationReason = 'SARFILED'
			   and onprobation = 1
	           and ProbationEndDate >= (year(@StartDate)*10000 + MONTH(@StartDate)*100+1)
	           and ProbationEndDate <= (CONVERT(VARCHAR(10), DATEADD(DAY, -(DAY(DATEADD(MONTH,0,@StartDate))),
								        DATEADD(MONTH, @lookforwardmonths + 1, @StartDate)), 112));
    
		SELECT @STAT = @@ERROR
		If @STAT <> 0  GOTO EndOFPROC
		end
	else
		begin
			If @WLTYPE = 0
				begin
					INSERT INTO Alert
						 ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
						   LASTMODIFY, CUST, ACCOUNT) 
 					select @WLCode
 						 , Name + ' has a Probation Reason of "SAR Filed" and a Probation End Date of '+
						   dbo.BSA_ConvertIntToSqlDate(ProbationEndDate, 'mm/dd/yyyy')+'.'
						 , 0, GETDATE(), NULL, NULL, id, null 
 					  from [PBSA].[dbo].[Customer] WITH (NOLOCK)
					 where ProbationReason = 'SARFILED'
					   and onprobation = 1
					   and ProbationEndDate >= (year(@StartDate)*10000 + MONTH(@StartDate)*100+1)
					   and ProbationEndDate <= (CONVERT(VARCHAR(10), DATEADD(DAY, -(DAY(DATEADD(MONTH,0,@StartDate))),
												DATEADD(MONTH, @lookforwardmonths + 1, @StartDate)), 112));
		    
					SELECT @STAT = @@ERROR
					If @STAT <> 0  GOTO EndOFPROC
				end
			else if @WLTYPE = 1
				begin
					insert into SuspiciousActivity
						  ( ProfileNo, BookDate, Cust, Account, 
							Activity, SuspType, StartDate, EndDate, RecurType, 
							RecurValue, ActCurrReportAmt, ActInActCnt, ActOutActCnt, 
							ActInActAmt, ActOutActAmt, CurrReportAmt, ExpAvgInActCnt, 
							ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt, InCntTolPerc, 
							OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr, ReviewState, 
							ReviewTime, ReviewOper, App, AppTime, AppOper, 
							WLCode, WLDesc, CreateTime )
					select null,dbo.ConvertSqlDateToInt(GETDATE()),id,null, NULL, 'RULE'
						  , null, null, null, null, null, null, null
						  , null, null, null, null, null, null, null
						  , 0, 0, 0, 0, null, null, null, null, 0, null, null 
						  , @WLCode, Name + ' has a Probation Reason of "SARFILED" and a Probation End Date of '+
									 dbo.BSA_ConvertIntToSqlDate(ProbationEndDate, 'mm/dd/yyyy')+'.'
						  , GETDATE()			
					  from [PBSA].[dbo].[Customer] WITH (NOLOCK)
					 where ProbationReason = 'SARFILED'
					   and onprobation = 1
					   and ProbationEndDate >= (year(@StartDate)*10000 + MONTH(@StartDate)*100+1)
					   and ProbationEndDate <= (CONVERT(VARCHAR(10), DATEADD(DAY, -(DAY(DATEADD(MONTH,0,@StartDate))),
												DATEADD(MONTH, @lookforwardmonths + 1, @StartDate)), 112));    
					SELECT @STAT = @@ERROR
					If @STAT <> 0  GOTO EndOFPROC
				end
		end
	
	EndOfProc:	
	IF (@STAT <> 0) 
		BEGIN 
			ROLLBACK TRAN USR_SARFollowup
			RETURN @STAT
		END	
	
	IF @trnCnt = 0
		COMMIT TRAN USR_SARFollowup
		RETURN @STAT
	end