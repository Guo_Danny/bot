--1.ActivityHist (527 Records)
delete ActivityHist;
go
dbcc checkident('ActivityHist', Reseed, 0)
go

--2.Customerhist (16 Records)
delete Customerhist;

--3.UnprocCustomer (1 Records)
delete UnprocCustomer;

--4.UnprocKYCData (1 Records)
delete UnprocKYCData;

--5.Watchlist (14 Records)
delete Watchlist
where IsUserDefined = 1

--6.Event (2 Records)
delete Event;
go

--7.KycDataHist (16 Records)
delete KycDataHist;
