USE [PBSA]
GO

declare @date datetime; set @date = getdate();

INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (1016, '0117 I3040 OR-Customer Outgoing Chat', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (1026, '0117 I3060 IR-Master file detail inq', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (1036, '0117 I1000 IR-Notice', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (1046, '0117 I1400 IR-Disengage payment clearan', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2016, '0117 L1000 revolving syndicated', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2026, '0117 L1000 revolving non-syndicated', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2036, '0117 L1000 non revolving syndicated', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2046, '0117 L1000 non-revolving non-syndicated', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2116, '0117 K1000 Cheque Withdraw', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2126, '0117 K1001 Cheque Withdraw-Lite', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2136, '0117 K1800 Cheque Withdraw (Multiple)', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2146, '0117 K2000 Cheque Deposit', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2156, '0117 K2001 Cheque Deposit-LITE', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2166, '0117 K2401 Collected Cheque Deposit', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2216, '0117 Q3100 Time Deposit New Account', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2226, '0117 Q4100 Time Deposit Termination', 0, NULL, 'UNISYSAD', @date, NULL, NULL, default)
