USE [PBSA]
GO

declare @date datetime; set @date = @date;

INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117I3040', 1016, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117I3060', 1026, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117I1000', 1036, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117I1400', 1046, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117L1000RESY', 2016, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117L1000RENS', 2026, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117L1000NRSY', 2036, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117L1000NRNS', 2046, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117K1000', 2116, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117K1001', 2126, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117K1800', 2136, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117K2000', 2146, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117K2001', 2156, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117K2401', 2166, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117Q3100', 2216, 'UNISYSAD', @date, NULL, NULL)
INSERT dbo.ActivityMap (Task, OrigActivity, PrimeCode, CreateOper, CreateDate, LastOper, LastModify) VALUES (NULL, '0117Q4100', 2226, 'UNISYSAD', @date, NULL, NULL)

