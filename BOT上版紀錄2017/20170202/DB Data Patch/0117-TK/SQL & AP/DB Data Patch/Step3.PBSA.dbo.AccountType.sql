USE [PBSA]
GO

declare @date datetime; set @date = getdate();

IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.AccountType  WHERE Code = 'K')
	INSERT dbo.AccountType VALUES ('K', 'Check Deposit', 1, 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default, NULL)
ELSE
	UPDATE dbo.AccountType SET Name = 'Check Deposit', RiskFactor = 1, CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL, ExemptionStatus = NULL  WHERE Code = 'K'

IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.AccountType  WHERE Code = 'L')
	INSERT dbo.AccountType VALUES ('L', 'Loan', 1, 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default, NULL)
ELSE
	UPDATE dbo.AccountType SET Name = 'Loan', RiskFactor = 1, CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL, ExemptionStatus = NULL  WHERE Code = 'L'

IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.AccountType  WHERE Code = 'V')
	INSERT dbo.AccountType VALUES ('V', 'Money Market', 1, 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default, NULL)
ELSE
	UPDATE dbo.AccountType SET Name = 'Money Market', RiskFactor = 1, CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL, ExemptionStatus = NULL  WHERE Code = 'V'

IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.AccountType  WHERE Code = 'Q')
	INSERT dbo.AccountType VALUES ('Q', 'Demand Deposit', 1, 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default, NULL)
ELSE
	UPDATE dbo.AccountType SET Name = 'Demand Deposit', RiskFactor = 1, CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL, ExemptionStatus = NULL  WHERE Code = 'Q'
