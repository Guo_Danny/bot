USE [PBSA]
GO

declare @date datetime; set @date = getdate();

IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.PaymentMethods  WHERE Code = 'BT')
	INSERT dbo.PaymentMethods VALUES ('BT', 'Financial Transfer', 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default)
ELSE
	UPDATE dbo.PaymentMethods SET Name = 'Financial Transfer', CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL  WHERE Code = 'BT'
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.PaymentMethods  WHERE Code = 'CE')
	INSERT dbo.PaymentMethods VALUES ('CE', 'Export commission', 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default)
ELSE
	UPDATE dbo.PaymentMethods SET Name = 'Export commission', CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL  WHERE Code = 'CE'
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.PaymentMethods  WHERE Code = 'DD')
	INSERT dbo.PaymentMethods VALUES ('DD', 'Demand Draft', 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default)
ELSE
	UPDATE dbo.PaymentMethods SET Name = 'Demand Draft', CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL  WHERE Code = 'DD'
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.PaymentMethods  WHERE Code = 'ID')
	INSERT dbo.PaymentMethods VALUES ('ID', 'Inward transfer-DD', 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default)
ELSE
	UPDATE dbo.PaymentMethods SET Name = 'Inward transfer-DD', CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL  WHERE Code = 'ID'
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.PaymentMethods  WHERE Code = 'IM')
	INSERT dbo.PaymentMethods VALUES ('IM', 'Inward transfer-MT', 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default)
ELSE
	UPDATE dbo.PaymentMethods SET Name = 'Inward transfer-MT', CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL  WHERE Code = 'IM'
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.PaymentMethods  WHERE Code = 'IO')
	INSERT dbo.PaymentMethods VALUES ('IO', 'Outward transfer-IO', 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default)
ELSE
	UPDATE dbo.PaymentMethods SET Name = 'Outward transfer-IO', CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL  WHERE Code = 'IO'
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.PaymentMethods  WHERE Code = 'IT')
	INSERT dbo.PaymentMethods VALUES ('IT', 'Inward transfer-TT', 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default)
ELSE
	UPDATE dbo.PaymentMethods SET Name = 'Inward transfer-TT', CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL  WHERE Code = 'IT'
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.PaymentMethods  WHERE Code = 'MT')
	INSERT dbo.PaymentMethods VALUES ('MT', 'Mail Transfer', 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default)
ELSE
	UPDATE dbo.PaymentMethods SET Name = 'Mail Transfer', CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL  WHERE Code = 'MT'
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.PaymentMethods  WHERE Code = 'OA')
	INSERT dbo.PaymentMethods VALUES ('OA', 'O/A', 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default)
ELSE
	UPDATE dbo.PaymentMethods SET Name = 'O/A', CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL  WHERE Code = 'OA'
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.PaymentMethods  WHERE Code = 'TT')
	INSERT dbo.PaymentMethods VALUES ('TT', 'Telegraphic transfer', 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default)
ELSE
	UPDATE dbo.PaymentMethods SET Name = 'Telegraphic transfer', CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL  WHERE Code = 'TT'
