USE [PBSA]
GO

INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (1017, '0210 I3040 OR-Customer Outgoing Chat', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (1027, '0210 I3060 IR-Master file detail inq', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (1037, '0210 I1000 IR-Notice', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (1047, '0210 I1400 IR-Disengage payment clearan', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2017, '0210 L1000 revolving syndicated', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2027, '0210 L1000 revolving non-syndicated', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2037, '0210 L1000 non revolving syndicated', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2047, '0210 L1000 non-revolving non-syndicated', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2117, '0210 K1000 Cheque Withdraw', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2127, '0210 K1001 Cheque Withdraw-Lite', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2137, '0210 K1800 Cheque Withdraw (Multiple)', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2147, '0210 K2000 Cheque Deposit', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2157, '0210 K2001 Cheque Deposit-LITE', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2167, '0210 K2401 Collected Cheque Deposit', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2217, '0210 Q3100 Time Deposit New Account', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
INSERT dbo.ActivityType (Type, Name, RiskFactor, ExemptionStatus, CreateOper, CreateDate, LastOper, LastModify, Ts) VALUES (2227, '0210 Q4100 Time Deposit Termination', 0, NULL, 'UNISYSAD', '20161110 10:57:51.78', NULL, NULL, default)
