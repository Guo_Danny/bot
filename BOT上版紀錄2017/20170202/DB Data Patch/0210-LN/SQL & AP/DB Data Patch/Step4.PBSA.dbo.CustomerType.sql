USE [PBSA]
GO

declare @date datetime; set @date = getdate();

IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.CustomerType  WHERE Code = 'B')
	INSERT dbo.CustomerType VALUES ('B', 'BANK', NULL, NULL, NULL, NULL, 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default)
ELSE
	UPDATE dbo.CustomerType SET Name = 'BANK', Description = NULL, RFTCode = NULL, CLTCode = NULL, CDDModelCode = NULL, CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL  WHERE Code = 'B'
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.CustomerType  WHERE Code = 'E')
	INSERT dbo.CustomerType VALUES ('E', 'LEGAL ENTITY', NULL, NULL, NULL, NULL, 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default)
ELSE
	UPDATE dbo.CustomerType SET Name = 'LEGAL ENTITY', Description = NULL, RFTCode = NULL, CLTCode = NULL, CDDModelCode = NULL, CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL  WHERE Code = 'E'
IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.CustomerType  WHERE Code = 'I')
	INSERT dbo.CustomerType VALUES ('I', 'INDIVIDUAL', NULL, NULL, NULL, NULL, 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default)
ELSE
	UPDATE dbo.CustomerType SET Name = 'INDIVIDUAL', Description = NULL, RFTCode = NULL, CLTCode = NULL, CDDModelCode = NULL, CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL  WHERE Code = 'I'
