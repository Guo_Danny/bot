USE [PBSA]
GO

declare @date datetime; set @date = getdate();

IF NOT EXISTS ( SELECT TOP 1 1 FROM dbo.RelationshipType  WHERE Code = 'OWNER')
	INSERT dbo.RelationshipType VALUES ('OWNER', 'OWNER', 'PRIMEADMIN', @date, 'PRIMEADMIN', NULL, default, 1, 0, '', 0, 1, NULL)
ELSE
	UPDATE dbo.RelationshipType SET Name = 'OWNER', CreateOper = 'PRIMEADMIN', CreateDate = @date, LastOper = 'PRIMEADMIN', LastModify = NULL, PrimaryRelation = 1, Score = 0, SPName = '', EnabledForAutoDiscovery = 0, Source = 1, LastProcessTime = NULL  WHERE Code = 'OWNER'
