USE [PBSA]
GO

--ILgTrans90
UPDATE dbo.Watchlist 
SET
[Desc] = 'Large Volume (Individuals) : 1 or aggregated transactions totaling $300,000(this amount is customizable) during a 3-calendar month period.'
WHERE WLCode = 'ILgTrans90'

--OLgTrans90
UPDATE dbo.Watchlist 
SET 
[Desc] = 'Large Volume (Legal entities (Corp or Company)) : 1 or aggregated transactions totaling $3,000,000(this amount is customizable) during a 3-calendar month period.'
WHERE WLCode = 'OLgTrans90'

--IMultiBene
UPDATE dbo.Watchlist 
SET 
[Desc] = 'Common Beneficiary/Originator Individuals : 3 or more different originators/beneficiaries when the aggregate of such activities is $100,000(this amount is customizable) or more during a calendar month.'
WHERE WLCode = 'IMultiBene'

--OMultiBene
UPDATE dbo.Watchlist 
SET 
[Desc] = 'Common Beneficiary/Originator Legal Entities : 3 or more different originators/beneficiaries when the aggregate of such activities is $1,000,000(this amount is customizable) or more during a calendar month.'
WHERE WLCode = 'OMultiBene'

--IMultiOrg
UPDATE dbo.Watchlist 
SET 
[Desc] = 'Common Beneficiary/Originator Individual : 3 or more different originators/beneficiaries when the aggregate of such activities is $100,000(this amount is customizable) or more during a calendar month.'
WHERE WLCode = 'IMultiOrg'

--OMultiOrg	
UPDATE dbo.Watchlist 
SET 
[Desc] = 'Common Beneficiary/Originator Legal Entities : 3 or more different originators/beneficiaries when the aggregate of such activities is $1,000,000(this amount is customizable) or more during a calendar month.'
WHERE WLCode = 'OMultiOrg'
	
--IHRCtry1Mth
UPDATE dbo.Watchlist 
SET 
[Desc] = 'High Risk Jurisdictions (Individuals) : $30,000(this amount is customizable) or more during a calendar month.' 
WHERE WLCode = 'IHRCtry1Mth'
	
--IHRCtry3Mth
UPDATE dbo.Watchlist 
SET 
[Desc] = 'High Risk Jurisdictions (Individuals) : $90,000(this amount is customizable) or more during a 3-calendar month period.'
WHERE WLCode = 'IHRCtry3Mth'
	
--OHRCtry1Mth
UPDATE dbo.Watchlist 
SET 
[Desc] = 'High Risk Jurisdictions (Legal Entities) : $300,000(this amount is customizable) or more during a calendar month.'
WHERE WLCode = 'OHRCtry1Mth'
	
--OHRCtry3Mth
UPDATE dbo.Watchlist 
SET 
[Desc] = 'High Risk Jurisdictions (Legal Entities) : $900,000(this amount is customizable) or more during a 3-calendar month period.'
WHERE WLCode = 'OHRCtry3Mth'

--IRndAmtAct
UPDATE dbo.Watchlist 
SET 
[Desc] = 'Large, Round Dollar Amounts (Individuals) : 5 or more round dollar transactions totaling $25,000(this amount is customizable) or more during a calendar month.'
WHERE WLCode = 'IRndAmtAct'

--ORndAmtAct
UPDATE dbo.Watchlist 
SET 
[Desc] = 'Large, Round Dollar Amounts (Legal Entities) : 5 or more round dollar transactions totaling $100,000(this amount is customizable) or more during a calendar month.'
WHERE WLCode = 'ORndAmtAct'

--LgBankHRCty
UPDATE dbo.Watchlist
SET 
[Desc] = 'Bank-to-Bank (High Risk Jurisdictions) : Analyze transactions of $500,000 or more(this amount is customizable) when the funds transfers involve a high risk jurisdiction as define by the Agency in its risk rating process.'
WHERE WLCode = 'LgBankHRCty'

--UBneEQByOrd
UPDATE dbo.Watchlist 
SET 
[Desc] = 'Detects activity when the name in the beneficiary data field is the same as the name in the by order data field when transactions in the same amount occur in a specified time period (1 month) and the amount exceeds the specified minimum amount ($50,000(this amount is customizable)).'
WHERE WLCode = 'UBneEQByOrd'

--U1MnthBetXY
UPDATE dbo.Watchlist 
SET 
[Desc] = 'Detects accounts that had transactions using specified activity types and the total of these transactions were between a designated dollar amount ($500,000(this amount is customizable)) range within 1 month.'
WHERE WLCode = 'U1MnthBetXY'

--UAcctRskCty
UPDATE dbo.Watchlist 
SET 
[Desc] = 'Detects if a new account is opened with a customer with a primary relationship whose country of origin is an NCCT Country. A new account is defined as one that was opened or created/imported (based on the Use Create Date parameter) after the last evaluation of this rule.'
WHERE WLCode = 'UAcctRskCty'

--UAcctMthVol
UPDATE dbo.Watchlist 
SET 
[Desc] = 'Detects accounts with transactions of specified activity types and exceeding certain transaction count, amount and average monthly volume by a certain multiple.'
WHERE WLCode = 'UAcctMthVol'




















