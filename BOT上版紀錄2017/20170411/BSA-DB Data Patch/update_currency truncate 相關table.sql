USE [PBSA]
GO

select * from SAsActivity

select * from Activity

select * from ActivityHist

select * from SuspiciousActivity

select * from SuspiciousActivityHist

select * from UnprocActivity

select * from ActivityDeleted

select * from Alert

select * from AlertHist



truncate table SAsActivity
truncate table Activity
truncate table ActivityHist
TRUNCATE table SuspiciousActivity
TRUNCATE table SuspiciousActivityHist
TRUNCATE table UnprocActivity
TRUNCATE table ActivityDeleted
TRUNCATE table Alert
TRUNCATE table AlertHist

select * from SysParam

update SysParam set BaseCurr = 'GBP'

update Watchlist
set LastEval = null 
where IsUserDefined = 1





