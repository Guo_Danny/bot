DECLARE @dat_s as datetime;
DECLARE @dat_e as datetime;

set @dat_s = cast('2016/9/1'as datetime);
set @dat_e = cast('2017/1/1'as datetime);

select year(ReqTime) yy,month(ReqTime) MM,Source,
case when ConfirmState is null then 'unconfirm' when ConfirmState = 1 then 'confirmed' when ConfirmState = 2 then 'waived' END as Confirm_state,
case when App = 0 then 'unapproved' when App = 1 then 'approved' end as apprev_state,
count(SeqNumb) cont from FilterTranTable
where ReqTime BETWEEN @dat_s and @dat_e
group by year(ReqTime),month(ReqTime),Source,ConfirmState,App
union
select year(ReqTime) yy,month(ReqTime) MM,Source,
case when ConfirmState is null then 'unconfirm' when ConfirmState = 1 then 'confirmed' when ConfirmState = 2 then 'waived' END as Confirm_state,
case when App = 0 then 'unapproved' when App = 1 then 'approved' end as apprev_state,
count(SeqNumb) cont from FilterTranHistTable
where ReqTime BETWEEN @dat_s and @dat_e
group by year(ReqTime),month(ReqTime),Source,ConfirmState,App
