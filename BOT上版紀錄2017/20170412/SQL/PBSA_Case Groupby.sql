DECLARE @dat_s as datetime;
DECLARE @dat_e as datetime;

set @dat_s = cast('2016/9/1'as datetime);
set @dat_e = cast('2017/1/1'as datetime);

select year(CreateTime) yy,month(CreateTime) MM,WLCode,
case when ReviewState is null then 'unconfirm' when ReviewState = 1 then 'waived' when ReviewState = 2 then 'Deferred' when ReviewState = 3 then 'confirm' END as Confirm_state,
case when App = 0 then 'unapproved' when App = 1 then 'approved' end as apprev_state,
count(RecNo) cont from SuspiciousActivity
where CreateTime BETWEEN @dat_s and @dat_e
group by year(CreateTime),month(CreateTime),WLCode,ReviewState,App
union
select year(CreateTime) yy,month(CreateTime) MM,WLCode,
case when ReviewState is null then 'unconfirm' when ReviewState = 1 then 'waived' when ReviewState = 2 then 'Deferred' when ReviewState = 3 then 'confirm' END as Confirm_state,
case when App = 0 then 'unapproved' when App = 1 then 'approved' end as apprev_state,
count(RecNo) cont from SuspiciousActivityHist
where CreateTime BETWEEN @dat_s and @dat_e
group by year(CreateTime),month(CreateTime),WLCode,ReviewState,App
