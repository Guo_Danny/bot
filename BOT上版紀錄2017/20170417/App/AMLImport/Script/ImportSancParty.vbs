
Dim Name, Name1 'As String
Dim Path 'As String
Dim objApp 'As Wscript.Shell
Dim outFile, outputFile 'As String
Dim fs 'As FileSystemObject
Dim FileName 'As String
Dim ErrorPath, ErrorFile 'As String
Dim PrimeExePath 'As String
Dim TenantCode 'As String
Dim BackUpPath 'As String

'********************************************************************************************************************************
'	Modify the follow for the specific server environment

   TenantCode = "BOTOSB"

'Note: All path statements must end with \

   Path = "d:\ftproot\NY\SanctionedParty\" 'Path of sanctioned party source files

   Name = "SanctionedParty.txt" 'files to import, uses same wildcard convention as DOS DIR command
   
   outputFile = "d:\Prime\BOTOSB\LOGS\OFAC\ImportSancParty.txt" 'Path and Name of file for log file.

   PrimeExePath = "d:\Prime\Exe\" 'Path to Prime\Exe folder

   BackUpPath = "d:\Prime\BOTOSB\Backup\OFAC\" 'Path to tenant backup folder for OFAC

   ErrorPath = "d:\ftproot\NY\Log\" 'Path of sanctioned party source files

   ErrorFile = "d:\Prime\BOTOSB\LOGS\OFAC\ImportSancParty_ErrorRecs.log" 'Path and Name of file for error file.

'********************************************************************************************************************************

   Set fs = CreateObject("Scripting.FileSystemObject")
   Set outFile = fs.OpenTextFile(outputFile, 8, True)  'Open the file to be created
   outFile.WriteBlankLines(1)

   outFile.WriteLine (Now & " Started Sanctioned Party Import")

   Set objApp = CreateObject("WScript.Shell") 

   On Error Resume Next

'Sanctioned Party

   FileName = Path & Name
   Name1 = Name & "." & Year(Date) & Right("0" & Month(Date),2) & Right("0" & Day(Date),2) & Right("0" & Hour(Now),2) & Right("0" & Minute(Now),2)

   If fs.FileExists(FileName) Then
      'Write to Log File
      outFile.WriteLine (Now & " Found " & FileName)

       'Write to Log File
      outFile.WriteLine (Now & " Copying " & FileName & " to " & BackUpPath)

      'Execute Statement
      fs.CopyFile FileName, BackUpPath
      If Err.Number <> 0 Then ErrHdlr Err, outFile
      
      'Write to Log File
      outFile.WriteLine(Now & " Executing " & PrimeExePath & "ImportSancParty.exe /o=" & TenantCode & " /a /i")
      
      'Execute Statement
      Return = objApp.Run(PrimeExePath & "ImportSancParty.exe /o=" & TenantCode & " /a /i", 1, True)
      If Err.Number <> 0 Then ErrHdlr Err, outFile
      
      'Write to Log File
       outFile.WriteLine (Now & " Deleting " & FileName)

      'Execute Statement
      fs.DeleteFile FileName
      If Err.Number <> 0 Then ErrHdlr Err, outFile

       'Write to Log File
      outFile.WriteLine (Now & " Moving " & BackUpPath & Name & " to " & BackUpPath & Name1)

      'Execute Statement
      fs.MoveFile BackUpPath & Name, BackUpPath & Name1
      If Err.Number <> 0 Then ErrHdlr Err, outFile

       'Write to Log File
      outFile.WriteLine (Now & " Copying " & ErrorFile & " to " & ErrorPath)

      'Execute Statement
      If fs.FileExists(ErrorFile) Then
         fs.CopyFile ErrorFile, ErrorPath
         If Err.Number <> 0 Then ErrHdlr Err, outFile
      End If
   End If
	   
   outFile.WriteLine (Now & " Completed")
   outFile.WriteLine ("------------------------------------------------------------------------------------------")
   outFile.WriteBlankLines(1)
   outFile.Close
   Set fs = Nothing

   
'******************************************************************************
Private Function ErrHdlr(ByRef objErr, ByRef LogFile)
    outFile.WriteBlankLines(1)
	LogFile.WriteLine (Now & " ERROR: " & CStr(objErr.Number) & " " & objErr.Description)
    outFile.WriteLine ("------------------------------------------------------------------------------------------")
    outFile.WriteBlankLines(1)
	LogFile.Close
	WScript.Quit 1
End Function
