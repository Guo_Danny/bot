USE [OFAC]
GO

--11/3,16,22,24
--source + CIF

DECLARE @listtype as char(10); set @listtype = '0114PEP'

/*
select distinct x.SeqNumb from (
	select a.SeqNumb
	from FilterTranTable a
	join MatchTable b ON a.SeqNumb = b.SeqNumb
	join SDNTable c ON b.entNum = c.EntNum
	where convert(char(10),a.ReqTime,111) in ('2016/11/03','2016/11/16','2016/11/22','2016/11/24')
		and a.Source = 'CIF'
		--and a.SeqNumb = '64623'
	group by a.SeqNumb
	HAVING count(c.ListType) = 1
) x
join MatchTable y on x.SeqNumb = y.SeqNumb
join SDNTable z on y.entNum = z.EntNum
where z.ListType = @listtype
*/

--select count(1) from FilterTranTable trantable
update FilterTranTable set ConfirmState= NULL,ConfirmOper = NULL,ConfirmTime = NULL,App = 0,AppOper = NULL,AppTime = NULL,AnnotTxt = ''
where SeqNumb IN
(
	select a.SeqNumb
	from FilterTranTable a
	where convert(char(10),a.ReqTime,111) in ('2017/03/31')
	and a.Source = 'CIF'
)
AND AppOper = 'UNISYSAD'

