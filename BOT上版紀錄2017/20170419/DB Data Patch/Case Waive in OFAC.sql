USE [OFAC]
GO

--11/3,16,22,24
--source + CIF

DECLARE @listtype as char(10); set @listtype = '0114PEP'

/*
select convert(char(10),ReqTime,111), count(1)
from FilterTranTable
where Source = 'CIF'
group by convert(char(10),ReqTime,111)

select a.SeqNumb
from FilterTranTable a
where convert(char(10),a.ReqTime,111) in ('2017/03/31')
and a.Source = 'CIF'
*/

--select count(1) from FilterTranTable trantable
update FilterTranTable set ConfirmState= 2,ConfirmOper = 'PrimeAdmin',ConfirmTime = getdate(),App = 1,AppOper = 'PrimeAdmin',AppTime = getdate(),AnnotTxt = 'Program change  caused cases, not sanction list or customer data caused cases, request for H.O. to batch waive and approve.  REQ000000104104'
where SeqNumb IN
(
	select a.SeqNumb
	from FilterTranTable a
	where convert(char(10),a.ReqTime,111) in ('2017/03/31')
	and a.Source = 'CIF'
)
AND AppTime is null

