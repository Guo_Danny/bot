USE [OFAC]
GO

begin tran
update ListType set Enabled = 1
where code in ('EU','UN')

COMMIT

begin tran
update OptionTbl set Enabled = 1
where Code in ('EU','UN')

COMMIT