USE [PBSA]
GO

/*
select * from [dbo].[Profile]

select * from SuspiciousActivityhist
where ProfileNo in (select RecNo from [dbo].[Profile])

select * from SuspiciousActivity
where ProfileNo in (select RecNo from [dbo].[Profile])

*/

begin tran 
delete SuspiciousActivityhist
where ProfileNo in (select RecNo from [dbo].[Profile])

COMMIT

begin tran 
delete SuspiciousActivity
where ProfileNo in (select RecNo from [dbo].[Profile])

COMMIT

begin tran 
delete [dbo].[Profile]

COMMIT
