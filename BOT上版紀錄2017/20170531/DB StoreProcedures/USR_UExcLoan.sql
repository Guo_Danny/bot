USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_UExcLoan]    Script Date: 6/1/2017 5:55:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[USR_UExcLoan] (
	@wlCode SCode, 
	@testAlert INT, 
	@riskClass VARCHAR(8000), 
	@activityType VARCHAR(8000), 
	@accountType VARCHAR(8000), 
	@minimumCount INT, 
	@minimumAmount MONEY, 
	@numberOfDays INT)
AS

SET NOCOUNT ON

DECLARE 
	@stat INT,
	@trnCnt INT,
	@WLType INT,  --0 for alert, 1 for case 
 	@minDateINT INT,
 	@minDate DATETIME,
	@currDate INT, 
	@startdate DATETIME,
	@enddate DATETIME
 

-- THE FOLLOWING SECTION DEFINES ALL THE TEMPORARY TABLES.

DECLARE	@activity TABLE (
	Account VARCHAR(40),
	TranNo  int,
	Amount  MONEY
)

-- This tables to hold the accounts.
DECLARE @accts TABLE (
	Account VARCHAR(40)
)

DECLARE	@activitySummary TABLE (
	Account VARCHAR(40),
	Amount MONEY,
	cnt INT
)
--  TEMPORARY TABLE DEFINITION ENDS HERE.

SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_UExcLoan
ELSE
	-- Already in a transaction
	SAVE TRAN USR_UExcLoan
/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @WLType = WLType  
FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

/* modify by danny 2017.05.30 get fromdate */
--SET @minDate = DATEADD(d, -1 * @numberOfDays, CONVERT(VARCHAR, GETDATE()))
--SET @minDateInt =  dbo.ConvertSqlDateToInt(@minDate)
DECLARE @SQLStartDate datetime, @SQLEndDate datetime
set @minDate = GETDATE()
exec dbo.BSA_GetDateRange @minDate, 'Month', 'PREV', 1, 
	@SQLStartDate OUTPUT, @SQLEndDate OUTPUT
SET @minDateInt = dbo.ConvertSqlDateToInt(@SQLStartDate)
SET @currDate = dbo.ConvertSqlDateToInt(GETDATE())


INSERT INTO @activity(Account, TranNo, Amount)
	SELECT a.Account, a.TranNo, a.baseamt
	FROM Activity a (NOLOCK) 
		INNER JOIN Account acct
			ON a.Account = acct.Id 
		INNER JOIN Customer cust 
			ON a.Cust = Cust.Id 
	WHERE 
		a.bookdate >= @minDateInt AND 
		a.Type IN (SELECT Id FROM dbo.BSA_fnIntegerStrSplitter(@activityType)) AND
		acct.Type IN (SELECT Id FROM dbo.BSA_fnStrSplitter(@accountType)) AND
		cust.RiskClass IN (SELECT Id FROM dbo.BSA_fnStrSplitter(@RiskClass )) AND
		a.Account IS NOT NULL 

INSERT INTO @accts (account)
SELECT DISTINCT Account FROM @activity

INSERT INTO @activity(Account, TranNo, Amount)
	SELECT ah.Account, ah.TranNo, ah.baseamt 
	FROM ActivityHist ah (NOLOCK) 
		INNER JOIN @accts acct
			ON ah.Account = acct.Account
      INNER JOIN Customer cust 
			ON ah.Cust = Cust.Id 
	WHERE 
		ah.bookdate >= @minDateInt AND 
		ah.Type IN (SELECT Id FROM dbo.BSA_fnIntegerStrSplitter(@activityType)) AND
		cust.RiskClass IN (SELECT Id FROM dbo.BSA_fnStrSplitter(@RiskClass )) 

 
INSERT INTO @ActivitySummary (Account, Amount, cnt)
SELECT Account, SUM(Amount), COUNT(tranno) 
FROM @activity 
GROUP BY Account
HAVING COUNT(TranNo) >= @minimumCount AND  SUM(Amount) >= @minimumAmount 

--If not test alert and is generate a case 
IF @WLTYPE = 1 AND @testAlert = 0 BEGIN
	SELECT @startDate = GETDATE()
	INSERT INTO SuspiciousActivity (profileno, bookdate, cust, account, 
	activity, susptype, startdate, enddate, recurtype, 
	recurvalue, actcurrreportamt, actinactcnt, actoutactcnt, 
	actinactamt, actoutactamt, currreportamt, expavginactcnt, 
	expavgoutactcnt, expmaxinactamt, expmaxoutactamt, incnttolperc, 
	outcnttolperc, inamttolperc, outamttolperc, descr, reviewstate, 
	reviewtime, reviewoper, app, apptime, appoper, 
	wlcode, wldesc, createtime)
	SELECT	NULL, @currDate, Null, Account,
	NULL, 'Rule', NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, 0, 0, 0, 0, 
	NULL, NULL, NULL, NULL, 0, NULL, NULL,
	@wlcode, 'Account ''' + Account + ''' had ''' + CONVERT(VARCHAR, cnt) + 
	''' transactions of total amount ''$' + CONVERT(VARCHAR, Amount) +
	''' between the period from ' + CONVERT(VARCHAR, @minDateInt) + ' to ' + CONVERT(VARCHAR, @currDate), 
	GETDATE() 
	FROM @ActivitySummary a

	SELECT @stat = @@ERROR	
	SELECT @endDate = GETDATE()
	IF @stat <> 0  GOTO ENDOFPROC

	INSERT INTO SasActivity (ObjectType, ObjectId, TranNo)
	SELECT 'SUSPACT', RecNo, TranNo 
	FROM @activity a 
		INNER JOIN @ActivitySummary acct
			ON a.account = acct.account 				
		INNER JOIN SuspiciousActivity s
			ON a.Account = s.Account 
	WHERE			
		S.wlcode = @wlCode AND
		S.CreateTime BETWEEN @startDate AND @endDate
END ELSE BEGIN
	SELECT @startDate = GETDATE()
	INSERT INTO ALERT ( wlcode, [desc], status, createdate, lastoper, 
			lastmodify, cust, account, istest)
	SELECT @wlCode, 
'Account ''' + Account + ''' had ''' + CONVERT(VARCHAR, cnt) + 
	''' transactions of total amount ''$' + CONVERT(VARCHAR, Amount) +
	''' between the period from ' + CONVERT(VARCHAR, @minDateInt) + ' to ' + CONVERT(VARCHAR, @currDate), 
		0, GETDATE(), NULL, NULL, NULL, 
		acct.Account, @testAlert
	FROM @ActivitySummary acct

	SELECT @stat = @@ERROR	
	SELECT @endDate = GETDATE()
	IF @stat <> 0  GOTO ENDOFPROC
	
	INSERT INTO SasActivity (ObjectType, ObjectId, TranNo)
   SELECT 'ALERT', AlertNo, TranNo
	FROM @activity a 
		INNER JOIN @ActivitySummary acct
			ON a.account = acct.account 				
		INNER JOIN Alert 
			ON a.Account = Alert.Account 
	WHERE			
		Alert.wlCode = @wlCode AND
		Alert.CreateDate BETWEEN @startDate AND @endDate
	SELECT @stat = @@ERROR 
	IF @stat <> 0  GOTO ENDOFPROC
END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_UExcLoan
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_UExcLoan
RETURN @stat



GO


