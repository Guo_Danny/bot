USE OFAC
GO

/*------------------------------------------------------------------------------------------------*/
IF OBJECT_ID (N'dbo.OFS_SelectViolationHistDetails') IS NOT NULL
   DROP PROCEDURE dbo.OFS_SelectViolationHistDetails
GO

CREATE PROCEDURE dbo.OFS_SelectViolationHistDetails(@calling_oper code, 
												@From_seqnumb int,
                                                @To_seqnumb int)
With Encryption As
  SET NOCOUNT ON

  Declare @stat int

CREATE TABLE #tmpListTypes(ListType VARCHAR(100))
INSERT INTO #tmpListTypes
SELECT * FROM fn_GetListTypesForOper(@calling_oper)

  CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
  INSERT INTO #OperBranchDept EXEC Psec.dbo.GetBranchDeptForOperByViewId @calling_oper, 115

  select filter.*, msg.*,match.*
  from FilterTranHistTable filter, MsgHistTable msg, MatchHistTable match
  where filter.seqnumb = msg.seqnumb and filter.seqnumb = match.seqnumb
        And (filter.seqnumb between @From_seqnumb and @To_seqnumb)
		And Exists(select * from #OperBranchDept cbd 
			where filter.branch = branch and filter.dept = dept)
		And NOT Exists(select * from FilterTranHistListTypes c Left Outer Join #tmpListTypes olt
								ON c.ListType = olt.ListType
						WHERE olt.ListType IS NULL and filter.seqnumb = c.seqnumb)

  if object_id('tempdb..#tmpListTypes') is not null 
	DROP TABLE #tmpListTypes

  if object_id('tempdb..#OperBranchDept') is not null 
	DROP TABLE #OperBranchDept

  select @stat = @@ERROR
  return @stat
GO

/*------------------------------------------------------------------------------------------------*/
IF OBJECT_ID (N'dbo.OFS_SelectViolationHistDetailsByDate') IS NOT NULL
   DROP PROCEDURE dbo.OFS_SelectViolationHistDetailsByDate
GO

CREATE PROCEDURE dbo.OFS_SelectViolationHistDetailsByDate(@calling_oper code, @FromDate DateTime,
                                                       @ToDate Datetime)
With Encryption As
	SET NOCOUNT ON

  Declare @stat int

CREATE TABLE #tmpListTypes(ListType VARCHAR(100))
INSERT INTO #tmpListTypes
SELECT * FROM fn_GetListTypesForOper(@calling_oper)

  CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
  INSERT INTO #OperBranchDept EXEC Psec.dbo.GetBranchDeptForOperByViewId @calling_oper, 115


  select filter.*, msg.*,match.*
  from FilterTranHistTable filter, MsgHistTable msg, MatchHistTable match
  where filter.seqnumb = msg.seqnumb and filter.seqnumb = match.seqnumb 
        And (filter.trantime between @FromDate and @ToDate)
		And Exists(select * from #OperBranchDept cbd 
			where filter.branch = branch and filter.dept = dept)
		And NOT Exists(select * from FilterTranHistListTypes c Left Outer Join #tmpListTypes olt
								ON c.ListType = olt.ListType
						WHERE olt.ListType IS NULL and filter.seqnumb = c.seqnumb)

  if object_id('tempdb..#tmpListTypes') is not null 
	DROP TABLE #tmpListTypes

  if object_id('tempdb..#OperBranchDept') is not null 
	DROP TABLE #OperBranchDept

  select @stat = @@ERROR
  return @stat
GO


/*------------------------------------------------------------------------------------------------*/
IF OBJECT_ID (N'dbo.OFS_ArchiveMatches') IS NOT NULL
   DROP PROCEDURE dbo.OFS_ArchiveMatches
GO

Create Proc dbo.OFS_ArchiveMatches @calling_oper Code, @startDate As DateTime, @endDate As DateTime

With Encryption As
	SET NOCOUNT ON

  CREATE TABLE #tmpListTypes(ListType VARCHAR(100))
  INSERT INTO #tmpListTypes
  SELECT * FROM fn_GetListTypesForOper(@calling_oper)

  CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
  INSERT INTO #OperBranchDept EXEC Psec.dbo.GetBranchDeptForOperByViewId @calling_oper, 115


 select MatchName,OriginalSDNName, SubString(MatchText,1,254)As MatchText
		from  MatchHistTable
    INNER JOIN FilterTranHistTable ON
        MatchHistTable.SeqNumb = FilterTranHistTable.SeqNumb
    Where
		(FilterTranHistTable.TranTime >= @startDate And
            FilterTranHistTable.TranTime <= @endDate) 
		And Exists(select * from #OperBranchDept cbd 
				where FilterTranHistTable.branch = branch and FilterTranHistTable.dept = dept)
		And NOT Exists(select * from FilterTranHistListTypes c Left Outer Join #tmpListTypes olt
								ON c.ListType = olt.ListType
						WHERE olt.ListType IS NULL and FilterTranHistTable.seqnumb = c.seqnumb)


  if object_id('tempdb..#tmpListTypes') is not null 
	DROP TABLE #tmpListTypes

  if object_id('tempdb..#OperBranchDept') is not null 
	DROP TABLE #OperBranchDept

GO

/*------------------------------------------------------------------------------------------------*/

 --FilterTranHistTable
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_FilterTranHistTableByOper]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure OFS_FilterTranHistTableByOper
Go

CREATE procedure OFS_FilterTranHistTableByOper 
(
	@Seqnumb	int
	,@Opercode	SCode
)
With Encryption As

SET NOCOUNT ON
BEGIN

	CREATE TABLE #tmpListTypes (ListType VARCHAR(100))
	-- Gets the List types available for the operator
	INSERT INTO #tmpListTypes 
	SELECT * From fn_GetListTypesForOper(@opercode)

	CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept EXEC Psec.dbo.GetBranchDeptForOperByViewId @Opercode, 115

	SELECT * FROM FilterTranHistTable
	WHERE Seqnumb = @Seqnumb
		And Exists(select * from #OperBranchDept cbd 
			where FilterTranHistTable.branch = branch and FilterTranHistTable.dept = dept)
		And NOT Exists(select * from FilterTranHistListTypes c Left Outer Join #tmpListTypes olt
								ON c.ListType = olt.ListType
						WHERE olt.ListType IS NULL and FilterTranHistTable.seqnumb = c.seqnumb)

	if object_id('tempdb..#tmpListTypes') is not null 
		DROP TABLE #tmpListTypes

	if object_id('tempdb..#OperBranchDept') is not null 
		DROP TABLE #OperBranchDept

END
go

/*------------------------------------------------------------------------------------------------*/

--FilterTranTable
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_FilterTranTableByOper]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure OFS_FilterTranTableByOper
Go

CREATE procedure OFS_FilterTranTableByOper 
(
	@Seqnumb	int
	,@Opercode	VARCHAR(10)
)
With Encryption As
	SET NOCOUNT ON

	CREATE TABLE #tmpListTypes (ListType VARCHAR(100))
	-- Gets the List types available for the operator
	INSERT INTO #tmpListTypes 
	SELECT * From fn_GetListTypesForOper(@opercode)

	CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept EXEC Psec.dbo.GetBranchDeptForOperByViewId @Opercode, 115

	SELECT * FROM FilterTranTable
	WHERE Seqnumb = @Seqnumb
		And Exists(select * from #OperBranchDept cbd where FilterTranTable.branch = branch and FilterTranTable.dept = dept)
		And NOT Exists(select * from vw_GetFilterTranListTypes c Left Outer Join #tmpListTypes olt
								ON c.ListType = olt.ListType
						WHERE olt.ListType IS NULL and FilterTranTable.seqnumb = c.seqnumb)

	if object_id('tempdb..#tmpListTypes') is not null 
		DROP TABLE #tmpListTypes

	if object_id('tempdb..#OperBranchDept') is not null 
		DROP TABLE #OperBranchDept

go
grant exec on OFS_FilterTranTableByOper to db_execproc
go


/*------------------------------------------------------------------------------------------------*/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_SDNTableByOper]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop PROCEDURE OFS_SDNTableByOper
Go

CREATE PROCEDURE OFS_SDNTableByOper 
(
	@EntNum	INT
	,@Opercode	SCode
)
With Encryption As

SET NOCOUNT ON
BEGIN
	SELECT * FROM SDNTable WHERE EntNum = @EntNum 
	and ListType in
		(
			SELECT Distinct ListType from PSEC..SEC_fnListTypesForOperByView(@OperCode, 135) 
			INTERSECT
			SELECT Distinct ListType from PSEC..SEC_fnListTypesForOperByView(@opercode, 100) 
		)

END
go

/*------------------------------------------------------------------------------------------------*/
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_NAME = N'OFS_GetListTypesForOper' 
)
   DROP PROCEDURE OFS_GetListTypesForOper

GO

Create Procedure OFS_GetListTypesForOper (@oper SCode, @Rights VARCHAR(200))
With Encryption  As

SET NOCOUNT ON
BEGIN

	SELECT * FROM ListType
	WHERE code in
		(
			SELECT Distinct listtype from psec.dbo.SEC_fnListTypesForOperByView(@oper, 135)
		) 
		AND isprimelist = 0

END		
go

/*------------------------------------------------------------------------------------------------*/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_SanctionListTypeByOper]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop PROCEDURE OFS_SanctionListTypeByOper
Go

CREATE PROCEDURE OFS_SanctionListTypeByOper 
(
	@Code	ListTypeId
	,@Opercode	SCode
)
With Encryption As

SET NOCOUNT ON
BEGIN
	SELECT * FROM ListType WHERE Code = @Code
	and Code in
		(
			SELECT Distinct ListType from PSEC..SEC_fnListTypesForOperByView(@OperCode, 135)
		)

END
go

/*------------------------------------------------------------------------------------------------*/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_CIFRulesByOper]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop PROCEDURE OFS_CIFRulesByOper
Go

CREATE PROCEDURE OFS_CIFRulesByOper 
(
	@Code	SCode
	,@Opercode	SCode
)
With Encryption As

SET NOCOUNT ON
BEGIN
	SELECT * FROM CifRules WHERE Code = @Code
	and Code in
		(
			SELECT Distinct CIFRule from PSEC..SEC_fnGetCIFRulesForOperByView(@OperCode, 111) 
		) and SDRDataCode in
		(
			SELECT Distinct SanctionDataRule from PSEC..SEC_fnGetSDataRulesForOperByView(@OperCode, 110)
		) 
       
END
go

/*------------------------------------------------------------------------------------------------*/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_SanctionDataRuleByOper]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop PROCEDURE OFS_SanctionDataRuleByOper
Go
CREATE PROCEDURE OFS_SanctionDataRuleByOper 
(
	@Code	SCode
	,@Opercode	SCode
)
With Encryption As

SET NOCOUNT ON
BEGIN
	SELECT * FROM SanctionDataRule WHERE Code = @Code
	and Code in
		(
			SELECT SanctionDataRule from PSEC..SEC_fnGetSDataRulesForOperByView(@OperCode, 110) 
		)

END
go

/*------------------------------------------------------------------------------------------------*/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OFS_GetSDRListByOper]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[OFS_GetSDRListByOper]
GO


CREATE PROCEDURE OFS_GetSDRListByOper @oper SCode

With Encryption AS
BEGIN
SET NOCOUNT ON

Select * from SanctionDataRule
WHERE Code in 
	(
		SELECT Distinct SanctionDataRule FROM psec.dbo.SEC_fnGetSDataRulesForOperByView(@oper, 110) 
	)

END

Go

/*------------------------------------------------------------------------------------------------*/
IF object_id(N'fn_GetListTypesForOper') IS NOT NULL
      DROP FUNCTION fn_GetListTypesForOper
GO

Create Function [dbo].fn_GetListTypesForOper (@opercode as VARCHAR(10))
RETURNS TABLE
as
      return
            SELECT ListType FROM PSEC..SEC_fnListTypesForOperByView(@opercode, 135) 
			-- Get all ListTypes based on the operrights(135 - SanctionListTypes)
			INTERSECT
			-- Get all all ListTypes based on the operrights(100 - SDNparties)
			SELECT ListType FROM PSEC..SEC_fnListTypesForOperByView(@opercode, 100) 

GO