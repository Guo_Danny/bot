USE PSEC
GO

IF OBJECT_ID (N'dbo.GetBranchDeptForOperByViewId') IS NOT NULL
   DROP PROCEDURE dbo.GetBranchDeptForOperByViewId
GO

CREATE PROCEDURE [dbo].[GetBranchDeptForOperByViewId] (@calling_oper VARCHAR(100), @viewId int)
With Encryption AS

SET NOCOUNT ON

	CREATE TABLE #list (Branch VARCHAR(10), Dept VARCHAR(10))
	/* 	This procedure will give the list of branch and department combinations for an operator for the view specified*/
	DECLARE @EnterpriseScope INT
	DECLARE @BranchScope INT
	DECLARE @DeptScope INT
	DECLARE @Rights VARCHAR(255)
	
	Select @Rights = Pcdb.dbo.CDB_fnGetRightsForViewId(@viewId) 

    select @EnterpriseScope=Count(*)
    from PSEC.dbo.operrights where [OPER] = @calling_oper And
    CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and Scope=1024

    if @EnterpriseScope>0
	BEGIN
		INSERT INTO #list 
		SELECT Branch.Code, Dept.Code
			FROM PSEC.dbo.Branch INNER JOIN PSEC.dbo.Dept On 1=1
		
		SELECT * FROM #list
		
		RETURN
	END

	select @BranchScope=Count(*)
	from PSEC.dbo.operrights
	where [OPER] = @calling_oper And
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)

    if @BranchScope>0
	BEGIN
		INSERT INTO #list
		select distinct objBranch, Dept.Code
		from PSEC.dbo.operrights INNER JOIN PSEC.dbo.Dept On 1=1
		where [OPER] = @calling_oper And
		CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)
	END

	select @DeptScope=Count(*)
	from PSEC.dbo.operrights
	where [OPER] = @calling_oper And
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)

    if @DeptScope>0
	BEGIN
		INSERT INTO #list
		select objBranch, objDept
		from PSEC.dbo.operrights 
		where [OPER] = @calling_oper And
		CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)
		and not exists 
			(SELECT * from #list l WHERE l.Branch = operrights.objBranch and l.Dept = operrights.objDept)
	END

	SELECT * FROM #list

	IF object_id('tempdb..#list') is not null 
		DROP TABLE #list

go

/*------------------------------------------------------------------------------------------------*/
IF object_id(N'SEC_fnListTypesForOperByView') IS NOT NULL
      DROP FUNCTION SEC_fnListTypesForOperByView

GO

CREATE FUNCTION [dbo].[SEC_fnListTypesForOperByView] (@calling_oper VARCHAR(100), @viewId int)
RETURNS @list TABLE(ListType VARCHAR(21))
AS
BEGIN 
	/* 	This function will give the list of branch and department combination for an operator for the view specified*/
	DECLARE @EnterpriseScope INT
	DECLARE @BranchScope INT
	DECLARE @DeptScope INT
	DECLARE @Rights VARCHAR(255)
	
	Select @Rights = Pcdb.dbo.CDB_fnGetRightsForViewId(@viewId) 


    select @EnterpriseScope=Count(*)
    from PSEC.dbo.operrights where [OPER] = @calling_oper And
    CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and Scope=1024

    if @EnterpriseScope>0

	BEGIN
       
		INSERT INTO @List
		SELECT DISTINCT ListType
			FROM OFAC.dbo.ListTypeAssociation
       
		RETURN
	END
	
	select @BranchScope=Count(*)
	from PSEC.dbo.operrights where [OPER] = @calling_oper And
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)

    if @BranchScope>0
	BEGIN
		
        INSERT INTO @List
			Select DISTINCT ListType
				FROM OFAC.dbo.ListTypeAssociation  Inner Join PSEC.dbo.operrights 
				on OFAC.dbo.ListTypeAssociation.Branch = PSEC.dbo.operrights.ObjBranch 
				where [OPER] = @calling_oper And
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)
			UNION
			Select DISTINCT ListType
				FROM OFAC.dbo.ListTypeAssociation where Branch IS NULL
		
	END 
	
	select @DeptScope=Count(*)
	from PSEC.dbo.operrights
	where [OPER] = @calling_oper And
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)

    if @DeptScope>0
	BEGIN
		
		INSERT INTO @List
			Select DISTINCT ListType
				FROM OFAC.dbo.ListTypeAssociation  Inner Join PSEC.dbo.operrights 
				on OFAC.dbo.ListTypeAssociation.Branch = PSEC.dbo.operrights.ObjBranch
				and OFAC.dbo.ListTypeAssociation.Dept = PSEC.dbo.operrights.ObjDept 
				where [OPER] = @calling_oper And
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)
			UNION
			Select DISTINCT ListType
				FROM OFAC.dbo.ListTypeAssociation Inner Join PSEC.dbo.operrights 
				on OFAC.dbo.ListTypeAssociation.Branch = PSEC.dbo.operrights.ObjBranch
			    and OFAC.dbo.ListTypeAssociation .Dept IS NULL
				where [OPER] = @calling_oper And
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64) 
			UNION
			Select DISTINCT ListType
				FROM OFAC.dbo.ListTypeAssociation Inner Join PSEC.dbo.operrights 
				on OFAC.dbo.ListTypeAssociation.Dept = PSEC.dbo.operrights.ObjDept
			    and OFAC.dbo.ListTypeAssociation.Branch IS Null 
				where [OPER] = @calling_oper And
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64) 
			UNION
			Select DISTINCT ListType
				FROM OFAC.dbo.ListTypeAssociation where Branch IS NULL and Dept IS NULL
		
	END

RETURN
END

GO

/*------------------------------------------------------------------------------------------------*/
IF object_id(N'SEC_fnGetSDataRulesForOperByView') IS NOT NULL
      DROP FUNCTION SEC_fnGetSDataRulesForOperByView

GO

CREATE FUNCTION [dbo].[SEC_fnGetSDataRulesForOperByView] (@calling_oper VARCHAR(100), @viewId int)
RETURNS @list TABLE(SanctionDataRule VARCHAR(21))
AS
BEGIN 
	/* 	This function will give the list of sanction data rules for an operator for the view specified*/
	DECLARE @EnterpriseScope INT
	DECLARE @BranchScope INT
	DECLARE @DeptScope INT
	DECLARE @Rights VARCHAR(255)
	
	Select @Rights = Pcdb.dbo.CDB_fnGetRightsForViewId(@viewId) 


    select @EnterpriseScope=Count(*)
    from PSEC.dbo.operrights where [OPER] = @calling_oper And
    CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and Scope=1024

    if @EnterpriseScope>0

	BEGIN
		INSERT INTO @List
		SELECT DISTINCT SanctionDataRule
			FROM OFAC.dbo.SanctionDataRuleAssociation
		RETURN
	END
	
	select @BranchScope=Count(*)
	from PSEC.dbo.operrights where [OPER] = @calling_oper And
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)

    if @BranchScope>0
	BEGIN
        INSERT INTO @List
			Select DISTINCT SanctionDataRule
				FROM OFAC.dbo.SanctionDataRuleAssociation  Inner Join PSEC.dbo.operrights 
				on OFAC.dbo.SanctionDataRuleAssociation.Branch = PSEC.dbo.operrights.ObjBranch 
				where [OPER] = @calling_oper And
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)
			UNION
			Select DISTINCT SanctionDataRule
				FROM OFAC.dbo.SanctionDataRuleAssociation where Branch IS NULL
	END 
	
	select @DeptScope=Count(*)
	from PSEC.dbo.operrights
	where [OPER] = @calling_oper And
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)

    if @DeptScope>0
	BEGIN
		INSERT INTO @List
			Select DISTINCT SanctionDataRule
				FROM OFAC.dbo.SanctionDataRuleAssociation  Inner Join PSEC.dbo.operrights 
				on OFAC.dbo.SanctionDataRuleAssociation.Branch = PSEC.dbo.operrights.ObjBranch
				and OFAC.dbo.SanctionDataRuleAssociation.Dept = PSEC.dbo.operrights.ObjDept 
				where [OPER] = @calling_oper And
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)
			UNION
			Select DISTINCT SanctionDataRule
				FROM OFAC.dbo.SanctionDataRuleAssociation Inner Join PSEC.dbo.operrights 
				on OFAC.dbo.SanctionDataRuleAssociation.Branch = PSEC.dbo.operrights.ObjBranch
			    and OFAC.dbo.SanctionDataRuleAssociation .Dept IS NULL
				where [OPER] = @calling_oper And
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64) 
			UNION
			Select DISTINCT SanctionDataRule
				FROM OFAC.dbo.SanctionDataRuleAssociation Inner Join PSEC.dbo.operrights 
				on OFAC.dbo.SanctionDataRuleAssociation.Dept = PSEC.dbo.operrights.ObjDept
			    and OFAC.dbo.SanctionDataRuleAssociation.Branch IS Null 
				where [OPER] = @calling_oper And
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64) 
			UNION
			Select DISTINCT SanctionDataRule
				FROM OFAC.dbo.SanctionDataRuleAssociation where Branch IS NULL and Dept IS NULL
	END

RETURN
END

GO
/*------------------------------------------------------------------------------------------------*/
IF object_id(N'SEC_fnGetCIFRulesForOperByView') IS NOT NULL
      DROP FUNCTION SEC_fnGetCIFRulesForOperByView

GO

CREATE FUNCTION [dbo].[SEC_fnGetCIFRulesForOperByView] (@calling_oper VARCHAR(100), @viewId int)
RETURNS @list TABLE(CIFRule VARCHAR(11))
AS
BEGIN 
	/* 	This function will give the list of sanction data rules for an operator for the view specified*/
	DECLARE @EnterpriseScope INT
	DECLARE @BranchScope INT
	DECLARE @DeptScope INT
	DECLARE @Rights VARCHAR(255)
	
	Select @Rights = Pcdb.dbo.CDB_fnGetRightsForViewId(@viewId) 


    select @EnterpriseScope=Count(*)
    from PSEC.dbo.operrights where [OPER] = @calling_oper And
    CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and Scope=1024

    if @EnterpriseScope>0

	BEGIN
		INSERT INTO @List
		SELECT DISTINCT CIFRule
			FROM OFAC.dbo.CIFRulesAssociation
		RETURN
	END
	
	select @BranchScope=Count(*)
	from PSEC.dbo.operrights where [OPER] = @calling_oper And
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)

    if @BranchScope>0
	BEGIN
        INSERT INTO @List
			Select DISTINCT CIFRule
				FROM OFAC.dbo.CIFRulesAssociation  Inner Join PSEC.dbo.operrights 
				on OFAC.dbo.CIFRulesAssociation.Branch = PSEC.dbo.operrights.ObjBranch 
				where [OPER] = @calling_oper And
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)
			UNION
			Select DISTINCT CIFRule
				FROM OFAC.dbo.CIFRulesAssociation where Branch IS NULL
	END 
	
	select @DeptScope=Count(*)
	from PSEC.dbo.operrights
	where [OPER] = @calling_oper And
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)

    if @DeptScope>0
	BEGIN
		INSERT INTO @List
			Select DISTINCT CIFRule
				FROM OFAC.dbo.CIFRulesAssociation Inner Join PSEC.dbo.operrights 
				on OFAC.dbo.CIFRulesAssociation.Branch = PSEC.dbo.operrights.ObjBranch
				and OFAC.dbo.CIFRulesAssociation.Dept = PSEC.dbo.operrights.ObjDept 
				where [OPER] = @calling_oper And
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)
			UNION
			Select DISTINCT CIFRule
				FROM OFAC.dbo.CIFRulesAssociation Inner Join PSEC.dbo.operrights 
				on OFAC.dbo.CIFRulesAssociation.Branch = PSEC.dbo.operrights.ObjBranch
			    and OFAC.dbo.CIFRulesAssociation .Dept IS NULL
				where [OPER] = @calling_oper And
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64) 
			UNION
			Select DISTINCT CIFRule
				FROM OFAC.dbo.CIFRulesAssociation Inner Join PSEC.dbo.operrights 
				on OFAC.dbo.CIFRulesAssociation.Dept = PSEC.dbo.operrights.ObjDept
			    and OFAC.dbo.CIFRulesAssociation.Branch IS Null 
				where [OPER] = @calling_oper And
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64) 
			UNION
			Select DISTINCT CIFRule
				FROM OFAC.dbo.CIFRulesAssociation where Branch IS NULL and Dept IS NULL

			
	END

RETURN
END

GO