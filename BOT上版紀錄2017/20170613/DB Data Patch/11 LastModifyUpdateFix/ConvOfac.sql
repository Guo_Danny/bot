/*
**  File Name:        ConvOFAC.sql
**
**  Functional Description:
**
**      This module contains SQL patches
**      for version 9.0.3
**
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2010                                                   ***
*** FIS						                                             ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|---------------------------------------------------- 
11/10/2010      VK		Functions OFS_fnListTypesForOperByView and
						OFS_fnGetSDataRulesForOperByView created.

11/11/2010      VK		Function OFS_fnGetCIFRulesForOperByView created.

11/11/2010      VK		fn_GetListTypesForOper, OFS_GetListTypesForOper,OFS_SanctionListTypeByOper,
						OFS_SDNTableByOper modified to use OFS_fnListTypesForOperByView function
						
11/12/2010		VK		OFS_GetSDRListByOper,OFS_SanctionDataRuleByOper modified to use
						OFS_fnGetSDataRulesForOperByView function

11/12/2010      VK		Procedure OFS_SanctionNamebyEntNum created.

11/15/2010		VK		OFS_CIFRulesByOper modified to use
						OFS_fnGetCIFRulesForOperByView function

11/15/2010		VK		OFS_FilterTranHistTableByOper, OFS_FilterTranTableByOper,
						OFS_ArchiveMatches modified to use fnGetBranchDeptForOperByViewId function

11/16/2010		VK		OFS_SelectViolationHistDetails, OFS_SelectViolationHistDetailsByDate
						modified to use fnGetBranchDeptForOperByViewId function

11/16/2010		VK		OFS_CheckSDNRightsByOper modified to use OFS_fnListTypesForOperByView function

11/19/2010		VK		OFS_CRYSancChangeList, CRY_DailyDetailedUserDefinedSDNReport, CRY_UserSancList
						modified to use OFS_fnListTypesForOperByView function.
						The OFS_CRYSancChangeList modification fixes the OFAC SancChgRpt report.
						The CRY_DailyDetailedUserDefinedSDNReport modification fixes the OFAC DDSDNReport report.
						The CRY_UserSancList modification fixes the OFAC UserSancLst report.

11/22/2010		VK		CRY_UserDefinedSDNChanges, CRY_SanctionDetails
						modified to use OFS_fnListTypesForOperByView function.
						The CRY_UserDefinedSDNChanges modification fixes the OFAC UDefSDNChge report.
						The CRY_SanctionDetails modification fixes the OFAC SancDet report.

11/22/2010		VK		OFS_GetCurOperReportData, CRY_CurrViolBySrc, CRY_CurrViolByStat
						modified to use temp table and fnGetBranchDeptForOperByViewId function instead of SEC_fnGetScopeForOper function.
						The OFS_GetCurOperReportData modification fixes the OFAC CurrOper report.
						The CRY_CurrViolBySrc modification fixes the OFAC CurrSrc report.
						The CRY_CurrViolByStat modification fixes the OFAC CurrStat report.

11/23/2010		VK		CRY_OFACViolStat, CRY_IncrSanExclList, CRY_SancExclList, CRY_ArchiveOperator
						modified to use temp table and fnGetBranchDeptForOperByViewId function instead of SEC_fnGetScopeForOper function.
						The CRY_OFACViolStat modification fixes the OFAC ViolStats report.
						The CRY_IncrSanExclList modification fixes the OFAC IncSancExcl report.
						The CRY_SancExclList modification fixes the OFAC SancExclLst report.
						The CRY_ArchiveOperator modification fixes the OFAC ArchOper report.
					
11/24/2010		VK		CRY_ArcStatsByStat, CRY_ArcStatsBySrc, CRY_MntStatsBySrc
						modified to use temp table and fnGetBranchDeptForOperByViewId function instead of SEC_fnGetScopeForOper function.
						The CRY_ArcStatsByStat modification fixes the OFAC ArchStat report.
						The CRY_ArcStatsBySrc modification fixes the OFAC ArchSrc report.
						The CRY_MntStatsBySrc modification fixes the OFAC MntViolStat report.

11/26/2010		VK		OFS_TopMatchAndCaseNumbers modified to use temp tables and INNER JOIN on SeqNumb 
						in combination with EXISTS function instead of "seqnumb IN()" routine.
						The OFS_TopMatchAndCaseNumbers modification fixes the OFAC CurMtchCase report.

12/09/2010		VK		Functions OFS_fnListTypesForOperByView, OFS_fnGetSDataRulesForOperByView,
						OFS_fnGetCIFRulesForOperByView had been moved to the top of the file
						to assure proper sequence of execution in order to maintain proper function-procedure dependancies.

12/30/2010		SH		Fixed problem with delete functionality of alternate names, address, keywords 
						to a given SDN party (I100430837)						
05/11/2012		JP		Fixed OFS_CompleteLoadDistribution to avoid updating LastModifyDate when status changed from 2 or 3 to 1
						R120334463 
*/


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Starting Conversion of OFAC Database				      '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO

Set NOCOUNT ON
Go

USE OFAC
GO

/*------------------------------------------------------------------------------------------------*/
IF object_id(N'OFS_fnListTypesForOperByView') IS NOT NULL
      DROP FUNCTION OFS_fnListTypesForOperByView

GO

CREATE FUNCTION [dbo].[OFS_fnListTypesForOperByView] (@calling_oper VARCHAR(100), @viewId int)
RETURNS @list TABLE(ListType VARCHAR(21))
AS
BEGIN 
	/* 	This function will give the list of branch and department combination for an operator for the view specified*/
	DECLARE @EnterpriseScope INT
	DECLARE @BranchScope INT
	DECLARE @DeptScope INT
	DECLARE @Rights VARCHAR(255)
	
	Select @Rights = Pcdb.dbo.CDB_fnGetRightsForViewId(@viewId) 

	DECLARE @OperRights TABLE (Oper VARCHAR(10), ObjBranch VARCHAR(10), ObjDept VARCHAR(10), [Right] VARCHAR(10), Scope INT)
	INSERT INTO	@OperRights
		SELECT Oper, ObjBranch, ObjDept, [Right], Scope FROM PSEC.dbo.operrights where [OPER] = @calling_oper 


    select @EnterpriseScope=Count(*)
    from @OperRights where 
    CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and Scope=1024

    if @EnterpriseScope>0

	BEGIN
       
		INSERT INTO @List
		SELECT DISTINCT ListType
			FROM OFAC.dbo.ListTypeAssociation
       
		RETURN
	END
	
	select @BranchScope=Count(*)
	from @OperRights where 
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)

    if @BranchScope>0
	BEGIN
		
        INSERT INTO @List
			Select DISTINCT ListType
				FROM OFAC.dbo.ListTypeAssociation l  Inner Join @OperRights o 
				on l.Branch = o.ObjBranch 
				where 
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)
			UNION
			Select DISTINCT ListType
				FROM OFAC.dbo.ListTypeAssociation where Branch IS NULL
		
	END 
	
	select @DeptScope=Count(*)
	from @OperRights
	where 
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)

    if @DeptScope>0
	BEGIN
		
		INSERT INTO @List
			Select DISTINCT ListType
				FROM OFAC.dbo.ListTypeAssociation l Inner Join @OperRights o 
				on l.Branch = o.ObjBranch
				and l.Dept = o.ObjDept 
				where 
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)
			UNION
			Select DISTINCT ListType
				FROM OFAC.dbo.ListTypeAssociation l Inner Join @OperRights o 
				on l.Branch = o.ObjBranch
			    and l.Dept IS NULL
				where 
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64) 
			UNION
			Select DISTINCT ListType
				FROM OFAC.dbo.ListTypeAssociation l Inner Join @OperRights o 
				on l.Dept = o.ObjDept
			    and l.Branch IS Null 
				where 
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64) 
			UNION
			Select DISTINCT ListType
				FROM OFAC.dbo.ListTypeAssociation where Branch IS NULL and Dept IS NULL
		
	END

RETURN
END


GO

/*------------------------------------------------------------------------------------------------*/
IF object_id(N'OFS_fnGetSDataRulesForOperByView') IS NOT NULL
      DROP FUNCTION OFS_fnGetSDataRulesForOperByView

GO

CREATE FUNCTION [dbo].[OFS_fnGetSDataRulesForOperByView] (@calling_oper VARCHAR(100), @viewId int)
RETURNS @list TABLE(SanctionDataRule VARCHAR(21))
AS
BEGIN 
	/* 	This function will give the list of sanction data rules for an operator for the view specified*/
	DECLARE @EnterpriseScope INT
	DECLARE @BranchScope INT
	DECLARE @DeptScope INT
	DECLARE @Rights VARCHAR(255)
	
	Select @Rights = Pcdb.dbo.CDB_fnGetRightsForViewId(@viewId) 

	DECLARE @OperRights TABLE (Oper VARCHAR(10), ObjBranch VARCHAR(10), ObjDept VARCHAR(10), [Right] VARCHAR(10), Scope INT)
	INSERT INTO	@OperRights
		SELECT Oper, ObjBranch, ObjDept, [Right], Scope FROM PSEC.dbo.operrights where [OPER] = @calling_oper 


    select @EnterpriseScope=Count(*)
    from @OperRights where 
    CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and Scope=1024

    if @EnterpriseScope>0

	BEGIN
		INSERT INTO @List
		SELECT DISTINCT SanctionDataRule
			FROM OFAC.dbo.SanctionDataRuleAssociation
		RETURN
	END
	
	select @BranchScope=Count(*)
	from @OperRights where 
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)

    if @BranchScope>0
	BEGIN
        INSERT INTO @List
			Select DISTINCT SanctionDataRule
				FROM OFAC.dbo.SanctionDataRuleAssociation s Inner Join @OperRights o 
				on s.Branch = o.ObjBranch 
				where 
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)
			UNION
			Select DISTINCT SanctionDataRule
				FROM OFAC.dbo.SanctionDataRuleAssociation where Branch IS NULL
	END 
	
	select @DeptScope=Count(*)
	from @OperRights
	where 
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)

    if @DeptScope>0
	BEGIN
		INSERT INTO @List
			Select DISTINCT SanctionDataRule
				FROM OFAC.dbo.SanctionDataRuleAssociation s Inner Join @OperRights o 
				on s.Branch = o.ObjBranch
				and s.Dept = o.ObjDept 
				where 
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)
			UNION
			Select DISTINCT SanctionDataRule
				FROM OFAC.dbo.SanctionDataRuleAssociation s Inner Join @OperRights o 
				on s.Branch = o.ObjBranch
			    and s.Dept IS NULL
				where 
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64) 
			UNION
			Select DISTINCT SanctionDataRule
				FROM OFAC.dbo.SanctionDataRuleAssociation s Inner Join @OperRights o 
				on s.Dept = o.ObjDept
			    and s.Branch IS Null 
				where 
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64) 
			UNION
			Select DISTINCT SanctionDataRule
				FROM OFAC.dbo.SanctionDataRuleAssociation where Branch IS NULL and Dept IS NULL
	END

RETURN
END

GO
/*------------------------------------------------------------------------------------------------*/

IF object_id(N'OFS_fnGetCIFRulesForOperByView') IS NOT NULL
      DROP FUNCTION OFS_fnGetCIFRulesForOperByView

GO

CREATE FUNCTION [dbo].[OFS_fnGetCIFRulesForOperByView] (@calling_oper VARCHAR(100), @viewId int)
RETURNS @list TABLE(CIFRule VARCHAR(11))
AS
BEGIN 
	/* 	This function will give the list of sanction data rules for an operator for the view specified*/
	DECLARE @EnterpriseScope INT
	DECLARE @BranchScope INT
	DECLARE @DeptScope INT
	DECLARE @Rights VARCHAR(255)
	
	Select @Rights = Pcdb.dbo.CDB_fnGetRightsForViewId(@viewId) 

	DECLARE @OperRights TABLE (Oper VARCHAR(10), ObjBranch VARCHAR(10), ObjDept VARCHAR(10), [Right] VARCHAR(10), Scope INT)
	INSERT INTO	@OperRights
		SELECT Oper, ObjBranch, ObjDept, [Right], Scope FROM PSEC.dbo.operrights where [OPER] = @calling_oper 

    select @EnterpriseScope=Count(*)
    from @OperRights where 
    CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and Scope=1024

    if @EnterpriseScope>0

	BEGIN
		INSERT INTO @List
		SELECT DISTINCT CIFRule
			FROM OFAC.dbo.CIFRulesAssociation
		RETURN
	END
	
	select @BranchScope=Count(*)
	from @OperRights where 
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)

    if @BranchScope>0
	BEGIN
        INSERT INTO @List
			Select DISTINCT CIFRule
				FROM OFAC.dbo.CIFRulesAssociation c Inner Join @OperRights o 
				on c.Branch = o.ObjBranch 
				where 
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=256)
			UNION
			Select DISTINCT CIFRule
				FROM OFAC.dbo.CIFRulesAssociation where Branch IS NULL
	END 
	
	select @DeptScope=Count(*)
	from @OperRights
	where 
	CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)

    if @DeptScope>0
	BEGIN
		INSERT INTO @List
			Select DISTINCT CIFRule
				FROM OFAC.dbo.CIFRulesAssociation c Inner Join @OperRights o 
				on c.Branch = o.ObjBranch
				and c.Dept = o.ObjDept 
				where 
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64)
			UNION
			Select DISTINCT CIFRule
				FROM OFAC.dbo.CIFRulesAssociation c Inner Join @OperRights o 
				on c.Branch = o.ObjBranch
			    and c.Dept IS NULL
				where 
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64) 
			UNION
			Select DISTINCT CIFRule
				FROM OFAC.dbo.CIFRulesAssociation c Inner Join @OperRights o 
				on c.Dept = o.ObjDept
			    and c.Branch IS Null 
				where 
				CHARINDEX(',' + [Right] + ',', ',' + @Rights + ',')>0  and (Scope=64) 
			UNION
			Select DISTINCT CIFRule
				FROM OFAC.dbo.CIFRulesAssociation where Branch IS NULL and Dept IS NULL

			
	END

RETURN
END

GO
/*------------------------------------------------------------------------------------------------*/

IF object_id(N'fn_GetListTypesForOper') IS NOT NULL
      DROP FUNCTION fn_GetListTypesForOper
GO

Create Function fn_GetListTypesForOper (@opercode as VARCHAR(10))
RETURNS TABLE
as
      return
            SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@opercode, 135) 
			-- Get all ListTypes based on the operrights(135 - SanctionListTypes)
			INTERSECT
			-- Get all all ListTypes based on the operrights(100 - SDNparties)
			SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@opercode, 100) 

GO

/*------------------------------------------------------------------------------------------------*/
IF OBJECT_ID (N'dbo.OFS_SelectViolationHistDetails') IS NOT NULL
   DROP PROCEDURE dbo.OFS_SelectViolationHistDetails
GO

CREATE PROCEDURE dbo.OFS_SelectViolationHistDetails(@calling_oper code, 
												@From_seqnumb int,
                                                @To_seqnumb int)
 With Encryption As
	 Set NOCOUNT ON

  Declare @stat int

CREATE TABLE #tmpListTypes(ListType VARCHAR(100))
INSERT INTO #tmpListTypes
SELECT * FROM fn_GetListTypesForOper(@calling_oper)

  CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
  INSERT INTO #OperBranchDept SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 115)

  select filter.*, msg.*,match.*
  from FilterTranHistTable filter, MsgHistTable msg, MatchHistTable match
  where filter.seqnumb = msg.seqnumb and filter.seqnumb = match.seqnumb
        And (filter.seqnumb between @From_seqnumb and @To_seqnumb)
		And Exists(select * from #OperBranchDept cbd 
			where filter.branch = branch and filter.dept = dept)
		And NOT Exists(select * from FilterTranHistListTypes c Left Outer Join #tmpListTypes olt
								ON c.ListType = olt.ListType
						WHERE olt.ListType IS NULL and filter.seqnumb = c.seqnumb)

  if object_id('tempdb..#tmpListTypes') is not null 
	DROP TABLE #tmpListTypes

  if object_id('tempdb..#OperBranchDept') is not null 
	DROP TABLE #OperBranchDept

  select @stat = @@ERROR
  return @stat
GO

/*------------------------------------------------------------------------------------------------*/
IF OBJECT_ID (N'dbo.OFS_SelectViolationHistDetailsByDate') IS NOT NULL
   DROP PROCEDURE dbo.OFS_SelectViolationHistDetailsByDate
GO

CREATE PROCEDURE dbo.OFS_SelectViolationHistDetailsByDate(@calling_oper code, @FromDate DateTime,
                                                       @ToDate Datetime)
 With Encryption As
	 Set NOCOUNT ON

  Declare @stat int

CREATE TABLE #tmpListTypes(ListType VARCHAR(100))
INSERT INTO #tmpListTypes
SELECT * FROM fn_GetListTypesForOper(@calling_oper)

  CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
  INSERT INTO #OperBranchDept SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 115)


  select filter.*, msg.*,match.*
  from FilterTranHistTable filter, MsgHistTable msg, MatchHistTable match
  where filter.seqnumb = msg.seqnumb and filter.seqnumb = match.seqnumb 
        And (filter.trantime between @FromDate and @ToDate)
		And Exists(select * from #OperBranchDept cbd 
			where filter.branch = branch and filter.dept = dept)
		And NOT Exists(select * from FilterTranHistListTypes c Left Outer Join #tmpListTypes olt
								ON c.ListType = olt.ListType
						WHERE olt.ListType IS NULL and filter.seqnumb = c.seqnumb)

  if object_id('tempdb..#tmpListTypes') is not null 
	DROP TABLE #tmpListTypes

  if object_id('tempdb..#OperBranchDept') is not null 
	DROP TABLE #OperBranchDept

  select @stat = @@ERROR
  return @stat
GO


/*------------------------------------------------------------------------------------------------*/
IF OBJECT_ID (N'dbo.OFS_ArchiveMatches') IS NOT NULL
   DROP PROCEDURE dbo.OFS_ArchiveMatches
GO

Create Proc dbo.OFS_ArchiveMatches @calling_oper Code, @startDate As DateTime, @endDate As DateTime

 With Encryption
 As
	SET NOCOUNT ON

  CREATE TABLE #tmpListTypes(ListType VARCHAR(100))
  INSERT INTO #tmpListTypes
  SELECT * FROM fn_GetListTypesForOper(@calling_oper)

  CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
  INSERT INTO #OperBranchDept SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 115)


 select MatchName,OriginalSDNName, SubString(MatchText,1,254)As MatchText
		from  MatchHistTable
    INNER JOIN FilterTranHistTable ON
        MatchHistTable.SeqNumb = FilterTranHistTable.SeqNumb
    Where
		(FilterTranHistTable.TranTime >= @startDate And
            FilterTranHistTable.TranTime <= @endDate) 
		And Exists(select * from #OperBranchDept cbd 
				where FilterTranHistTable.branch = branch and FilterTranHistTable.dept = dept)
		And NOT Exists(select * from FilterTranHistListTypes c Left Outer Join #tmpListTypes olt
								ON c.ListType = olt.ListType
						WHERE olt.ListType IS NULL and FilterTranHistTable.seqnumb = c.seqnumb)


  if object_id('tempdb..#tmpListTypes') is not null 
	DROP TABLE #tmpListTypes

  if object_id('tempdb..#OperBranchDept') is not null 
	DROP TABLE #OperBranchDept

GO

/*------------------------------------------------------------------------------------------------*/

 --FilterTranHistTable
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_FilterTranHistTableByOper]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure OFS_FilterTranHistTableByOper
Go

CREATE procedure OFS_FilterTranHistTableByOper 
(
	@Seqnumb	int
	,@Opercode	SCode
)
 With Encryption As
	
BEGIN
	SET NOCOUNT ON
	
	--temp table to improve performance
	CREATE TABLE #tmpListTypes (ListType VARCHAR(100))
	-- Gets the List types available for the operator
	INSERT INTO #tmpListTypes 
	SELECT * From fn_GetListTypesForOper(@opercode)

	CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@Opercode, 115)

	SELECT * FROM FilterTranHistTable
	WHERE Seqnumb = @Seqnumb
		And Exists(select * from #OperBranchDept cbd 
			where FilterTranHistTable.branch = branch and FilterTranHistTable.dept = dept)
		And NOT Exists(select * from FilterTranHistListTypes c Left Outer Join #tmpListTypes olt
								ON c.ListType = olt.ListType
						WHERE olt.ListType IS NULL and FilterTranHistTable.seqnumb = c.seqnumb)

	if object_id('tempdb..#tmpListTypes') is not null 
		DROP TABLE #tmpListTypes

	if object_id('tempdb..#OperBranchDept') is not null 
		DROP TABLE #OperBranchDept

END
go

/*------------------------------------------------------------------------------------------------*/

--FilterTranTable
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_FilterTranTableByOper]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure OFS_FilterTranTableByOper
Go

CREATE procedure OFS_FilterTranTableByOper 
(
	@Seqnumb	int
	,@Opercode	VARCHAR(10)
)
 With Encryption As
	 SET NOCOUNT ON
	--temp table to improve performance
	CREATE TABLE #tmpListTypes (ListType VARCHAR(100))
	-- Gets the List types available for the operator
	INSERT INTO #tmpListTypes 
	SELECT * From fn_GetListTypesForOper(@opercode)

	CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@Opercode, 115)

	SELECT * FROM FilterTranTable
	WHERE Seqnumb = @Seqnumb
		And Exists(select * from #OperBranchDept cbd where FilterTranTable.branch = branch and FilterTranTable.dept = dept)
		And NOT Exists(select * from vw_GetFilterTranListTypes c Left Outer Join #tmpListTypes olt
								ON c.ListType = olt.ListType
						WHERE olt.ListType IS NULL and FilterTranTable.seqnumb = c.seqnumb)

	if object_id('tempdb..#tmpListTypes') is not null 
		DROP TABLE #tmpListTypes

	if object_id('tempdb..#OperBranchDept') is not null 
		DROP TABLE #OperBranchDept

go
grant exec on OFS_FilterTranTableByOper to db_execproc
go

/*------------------------------------------------------------------------------------------------*/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_SDNTableByOper]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop PROCEDURE OFS_SDNTableByOper
Go

CREATE PROCEDURE OFS_SDNTableByOper 
(
	@EntNum	INT
	,@Opercode	SCode
)
With Encryption As

BEGIN
Set NOCOUNT ON
 
	SELECT * FROM SDNTable WHERE EntNum = @EntNum 
	and ListType in
		(
			SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@OperCode, 135) 
			INTERSECT
			SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@opercode, 100) 
		)

END
go

/*------------------------------------------------------------------------------------------------*/

if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_SanctionNamebyEntNum]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop PROCEDURE OFS_SanctionNamebyEntNum
Go

CREATE PROCEDURE OFS_SanctionNamebyEntNum
(
	@EntNum	INT
	,@Opercode	SCode
)
 With Encryption As

BEGIN
 SET NOCOUNT ON

	SELECT Entnum, Name, FirstName, MiddleName, LastName,
    ListId, ListType, Title, Program, Type, CallSign, VessType, Tonnage, GRT, 
    VessOwner, Dob, PrimeAdded, Sex, Height, Weight, Build, Eyes, VessFlag, 
    Hair, Complexion, Race, Country, Remarks1, Remarks2, EffectiveDate, 
    ExpiryDate, StandardOrder, SDNType, DataType, UserRec, Deleted, 
    DuplicRec, IgnoreDerived, Status, ListCreateDate, ListModifDate, 
    CreateDate, LastModifDate, LastOper, Remarks, ts
	FROM SDNTable WHERE EntNum = @EntNum 
	and ListType in
		(
			SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@OperCode, 135) 
			INTERSECT
			SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@opercode, 100) 
		)

END
go

/*------------------------------------------------------------------------------------------------*/
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_NAME = N'OFS_GetListTypesForOper' 
)
   DROP PROCEDURE OFS_GetListTypesForOper

GO

Create Procedure OFS_GetListTypesForOper (@oper SCode, @Rights VARCHAR(200))
 With Encryption As

BEGIN
Set NOCOUNT ON

	SELECT * FROM ListType
	WHERE code in
		(
			SELECT Distinct listtype from OFAC.dbo.OFS_fnListTypesForOperByView(@oper, 135)
		) 
		AND isprimelist = 0

END		
go

/*------------------------------------------------------------------------------------------------*/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_SanctionListTypeByOper]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop PROCEDURE OFS_SanctionListTypeByOper
Go

CREATE PROCEDURE OFS_SanctionListTypeByOper 
(
	@Code	ListTypeId
	,@Opercode	SCode
)
With Encryption As

BEGIN
Set NOCOUNT ON

	SELECT * FROM ListType WHERE Code = @Code
	and Code in
		(
			SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@OperCode, 135)
		)

END
go

/*------------------------------------------------------------------------------------------------*/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_CIFRulesByOper]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop PROCEDURE OFS_CIFRulesByOper
Go

CREATE PROCEDURE OFS_CIFRulesByOper 
(
	@Code	SCode
	,@Opercode	SCode
)
 With Encryption As

BEGIN
Set NOCOUNT ON

	SELECT * FROM CifRules WHERE Code = @Code
	and Code in
		(
			SELECT Distinct CIFRule from OFAC..OFS_fnGetCIFRulesForOperByView(@OperCode, 111) 
		) and SDRDataCode in
		(
			SELECT Distinct SanctionDataRule from OFAC..OFS_fnGetSDataRulesForOperByView(@OperCode, 110)
		) 
       
END
go

/*------------------------------------------------------------------------------------------------*/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_SanctionDataRuleByOper]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop PROCEDURE OFS_SanctionDataRuleByOper
Go

CREATE PROCEDURE OFS_SanctionDataRuleByOper 
(
	@Code	SCode
	,@Opercode	SCode
)
 With Encryption As

BEGIN
Set NOCOUNT ON

	SELECT * FROM SanctionDataRule WHERE Code = @Code
	and Code in
		(
			SELECT SanctionDataRule from OFAC..OFS_fnGetSDataRulesForOperByView(@OperCode, 110) 
		)

END
go

/*------------------------------------------------------------------------------------------------*/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OFS_GetSDRListByOper]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[OFS_GetSDRListByOper]
GO


CREATE PROCEDURE OFS_GetSDRListByOper @oper SCode

With Encryption AS
BEGIN
Set NOCOUNT ON

Select * from SanctionDataRule
WHERE Code in 
	(
		SELECT Distinct SanctionDataRule FROM OFAC.dbo.OFS_fnGetSDataRulesForOperByView(@oper, 110) 
	)

END

Go
/*------------------------------------------------------------------------------------------------*/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_CheckSDNRightsByOper]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop PROCEDURE dbo.OFS_CheckSDNRightsByOper

GO
CREATE PROCEDURE dbo.OFS_CheckSDNRightsByOper (@calling_oper AS Code, @EntNum int)

With Encryption AS
BEGIN
Set NOCOUNT ON

	DECLARE @HasRight BIT
		
	If Exists(SELECT EntNum FROM SDNTable
				Where EntNum = @EntNum
				And Listtype in 
				(
				-- Get List Types based on the operrights and view(135 - SanctionListTypes)
				SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@calling_oper, 135) 
				INTERSECT
				-- Get List Types based on the operrights and view(100 - SDNparties)
				SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@calling_oper, 100) 
				)
		 ) 			 
		SET @HasRight = 1
	ELSE	
		SET @HasRight = 0

	SELECT @HasRight As OperHasRight

END

GO


/*------------------------------------------------------------------------------------------------*/
IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[CRY_DailyDetailedUserDefinedSDNReport]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.CRY_DailyDetailedUserDefinedSDNReport
GO 
CREATE PROCEDURE dbo.CRY_DailyDetailedUserDefinedSDNReport(@calling_oper code,@firstinterval int)  
 
With Encryption AS  

BEGIN  
Set NOCOUNT ON
/**********  
Procedure Name:CRY_DailyDetailedUserDefinedSDNReport  
Description: This procedure provides the Daily user defined sanctioned party names that were added or deleted over a period Parameters (on a given day)
Parameters:  
	@firstinterval: Number of days to lookback as integer.
RETURNS TABLE  
********/  
select 'Created User Sanctioned Names'  Sanctioned_Name_Created, ''  [Name]   
 ,''[First Name],'' [Middle Name],''[Last Name],''[Ignore Derived Names],''[Type],'' [Created Date],'' [Last Modified Date],''[Last Operator]  
Union All  
SELECT   
 'Created User Sanctioned Names' Sanctioned_Name_Created,   
 Name [Name] ,  
 FirstName [First Name],  
 MiddleName [Middle Name],  
 LastName [Last Name],   
 case when IgnoreDerived=0 then 'No' else 'Yes' end [Ignore Derived Names],  
 type [Type],  
 convert(varchar, createDate,110) [Created Date],   
 convert(varchar,LastModifDate,110) [Last Modified Date],
 LastOper 
FROM   
 sdntable S  
WHERE   
 userRec = 1 And deleted=0
 and  
  ( createdate between DateAdd(day, -1*@firstinterval , GetDate()) and GetDate()  
  OR   
  (LastModifDate between DateAdd(day, -1 * @firstinterval, GetDate()) and GetDate()  
   and LastModifDate is not null))  
AND EXISTS 
	(
	SELECT L.Code From ListType L
		where L.IsPrimeList = 0 and L.code = S.ListType
	INTERSECT 
	SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@calling_oper, 135) 
	INTERSECT
	SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@calling_oper, 100)
	)
UNION ALL  
select 'Deleted User Sanctioned Names'  Sanctioned_Name_Created, ''  [Name]   
 ,''[First Name],'' [Middle Name],''[Last Name],''[Ignore Derived Names],''[Type],'' [Created Date],'' [Last Modified Date],''[Last Operator]
  
UNION ALL  
SELECT 'Deleted User Sanctioned Names' Sanctioned_Name_Created,   
 Name [Name]  ,  
 FirstName [First Name],  
 MiddleName [Middle Name],  
 LastName [Last Name],   
 case when IgnoreDerived=0 then 'No' else 'Yes' end [Ignore Derived Names],  
 type [Type],  
 convert(varchar, createDate,110) [Created Date],   
 convert(varchar,LastModifDate,110) [Last Modified Date],
 LastOper
FROM   
 sdntable  S 
WHERE   
 userrec = 1 and deleted = 1  
 and  
  (createdate between DateAdd(day, -1 * @firstinterval, GetDate()) and GetDate() 
  OR   
  LastModifDate between DateAdd(day, -1 * @firstinterval, GetDate())and GetDate()
  )  
AND EXISTS 
	(
	SELECT L.Code From ListType L
		where L.IsPrimeList = 0 and L.code = S.ListType
	INTERSECT 
	SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@calling_oper, 135) 
	INTERSECT
	SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@calling_oper, 100)
	) 
END  
go

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[OFS_CRYSancChangeList]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.OFS_CRYSancChangeList
GO

CREATE PROCEDURE dbo.OFS_CRYSancChangeList (@calling_oper AS Code)
 With Encryption As
	 Set NOCOUNT ON

	Declare @lastSdnUpdDate datetime

 Select @lastSdnUpdDate = IsNull(LastSDNUpdate,0) from SDNDBStatus

 Select [Name], Program, Type, Status, Deleted, ListType, DataType
    From SDNTable WHERE 
	(IsNull(LastmodifDate,0) > @lastSdnUpdDate OR
		IsNull(CreateDate,0) > @lastSdnUpdDate) 
	AND (Status in (2,3,4))
	AND LISTTYPE IN
		(
			SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 135) 
			INTERSECT
			SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 100)
		)
     Order by ListType, Deleted
Go

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[CRY_UserSancList]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.CRY_UserSancList
GO 

CREATE PROCEDURE dbo.CRY_UserSancList @calling_oper As Code                                
 With Encryption AS

BEGIN
Set NOCOUNT ON

  SELECT [Name], Program, Title, [Type], Deleted, LastOper,
	 ListType, UserRec, CreateDate, LastModifDate, DataType, EntNum, Remarks
   FROM   OFAC.dbo.SDNTable S
   WHERE  (UserRec=1 OR EXISTS (Select code from ListType where IsPrimeList = 0 and code = S.ListType))
	AND EXISTS 
		(
		SELECT L.Code From ListType L
			where L.code = S.ListType
		INTERSECT 
		SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@calling_oper, 135) 
		INTERSECT
		SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@calling_oper, 100)
		)	
END
GO

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[CRY_UserDefinedSDNChanges]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.CRY_UserDefinedSDNChanges
GO 

CREATE PROCEDURE dbo.CRY_UserDefinedSDNChanges(@calling_oper Code,@firstinterval int,@secondinterval int)  
  
With Encryption AS  

BEGIN  
Set NOCOUNT ON
/**********  
Procedure Name: CRY_UserDefinedSDNChanges 
Description: This Procedure provides the number of user defined sanctioned party names that were added or deleted in the past days.  
Parameters:  @firstInterval :Number of minimum days for lookup as integer. 
             @secondInterval:Number of maximum days for lookup as integer.
RETURNS TABLE  
********/  
  
Declare @ListType Table([ListType] ListTypeId)

Insert @ListType
SELECT L.Code From ListType L
			where L.IsPrimeList = 0
		INTERSECT 
		SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@calling_oper, 135) 
		INTERSECT
		SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(@calling_oper, 100)

	
SELECT 'User Defined Sanctioned Names Created' Sanctioned_Name_Created,   
 'Before ' + cast(@secondinterval as varchar) + ' days'  Period_Created,   
 cast(Count(*) as varchar) [Count]
FROM   
 sdntable S  
WHERE   
 userrec = 1 And deleted = 0
 and createdate < DateAdd(day, -1 * @secondinterval, GetDate()) 
AND ListType in (select ListType from @ListType L where L.ListType = S.ListType )
UNION ALL  
SELECT 'User Defined Sanctioned Names Created' Sanctioned_Name_Created,  
 'Between ' + cast(@firstinterval as varchar) + ' and ' + cast(@secondinterval as varchar) + ' days' Period_Created,   
 cast(Count(*) as varchar) [Count] 
FROM   
 sdntable S 
WHERE   
 userrec = 1  and deleted = 0
 and createdate between DateAdd(day, -1 * @secondinterval, GetDate()) and DateAdd(day, -1 * @firstinterval, GetDate())
AND ListType in (select ListType from @ListType L where L.ListType = S.ListType )
UNION ALL  
SELECT 'User Defined Sanctioned Names Created' Sanctioned_Name_Created,  
 'Last ' + cast(@firstinterval as varchar) + ' days' Period_Created,   
 cast(Count(*) as varchar) [Count]   
FROM   
 sdntable  S 
WHERE   
 userrec = 1  and deleted = 0
 and createdate > DateAdd(day, -1 * @firstinterval, GetDate())
AND ListType in (select ListType from @ListType L where L.ListType = S.ListType) 
union all  
SELECT  'User Defined Sanctioned Names Deleted' Sanctioned_Name_Created,
 'Before ' + cast(@secondinterval as varchar) + ' days' Period_Created,   
 cast(Count(*) as varchar) [Count]
FROM   
 sdntable S
WHERE   
 userrec = 1 and deleted = 1  
 and lastmodifdate < DateAdd(day, -1 * @secondinterval, GetDate()) 
AND ListType in (select ListType from @ListType L where L.ListType = S.ListType)
UNION ALL  
SELECT 'User Defined Sanctioned Names Deleted' Sanctioned_Name_Created,  
 'Between ' + cast(@firstinterval as varchar) + ' and ' + cast(@secondinterval as varchar) + ' days' Period_Created,   
 cast(Count(*) as varchar) [Count]
FROM   
 sdntable  S 
WHERE   
 userrec = 1 and deleted = 1  
 and lastmodifdate between DateAdd(day, -1 * @secondinterval, GetDate()) and DateAdd(day, -1 * @firstinterval, GetDate())
AND ListType in (select ListType from @ListType L where L.ListType = S.ListType)
UNION ALL  
SELECT 'User Defined Sanctioned Names Deleted' Sanctioned_Name_Created,  
 'Last ' + cast(@firstinterval as varchar) + ' days' Period_Created,   
 cast(Count(*) as varchar) [Count]
FROM   
 sdntable S  
WHERE   
 userrec = 1 and deleted = 1  
 and lastmodifdate > DateAdd(day, -1 * @firstinterval, GetDate()) 
AND ListType in (select ListType from @ListType L where L.ListType = S.ListType)

END  
GO

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[OFS_GetCurOperReportData]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.OFS_GetCurOperReportData
GO

Create procedure dbo.OFS_GetCurOperReportData @calling_oper AS Code
 With Encryption As
	 Set NOCOUNT ON

CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 115)

select TranTbl.*, OperTbl.[Name]
from FilterTranTable As TranTbl inner join PSEC.dbo.Operator As OperTbl
on TranTbl.ConfirmOper=OperTbl.[Code]
--Get the results based on the operrights for FilterTranTable
WHERE Exists(select * from #OperBranchDept cbd where TranTbl.branch = cbd.Branch and TranTbl.dept = cbd.Dept)

if object_id('tempdb..#OperBranchDept') is not null 
		DROP TABLE #OperBranchDept
Go

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[CRY_CurrViolBySrc]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.CRY_CurrViolBySrc
GO 

CREATE PROCEDURE dbo.CRY_CurrViolBySrc @calling_oper As Code
								
With Encryption AS

BEGIN
Set NOCOUNT ON

CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 115)

 SELECT FT.Branch, FT.Dept, FT.Source, FT.ConfirmState
 FROM   OFAC.dbo.FilterTranTable FT
	--Get the results based on the operrights for FilterTranTable
	WHERE EXISTS (select * from #OperBranchDept cbd where FT.Branch = cbd.Branch and FT.Dept = cbd.Dept)

if object_id('tempdb..#OperBranchDept') is not null 
		DROP TABLE #OperBranchDept
END
GO

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[CRY_CurrViolByStat]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.CRY_CurrViolByStat
GO 

CREATE PROCEDURE dbo.CRY_CurrViolByStat @calling_oper As Code
								
 With Encryption AS

BEGIN
Set NOCOUNT ON

CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 115)

 SELECT FT.ConfirmState
 FROM   OFAC.dbo.FilterTranTable FT
	--Get the results based on the operrights for FilterTranTable
 WHERE EXISTS (select * from #OperBranchDept cbd where FT.Branch = cbd.Branch and FT.Dept = cbd.Dept)

if object_id('tempdb..#OperBranchDept') is not null 
		DROP TABLE #OperBranchDept

END
GO

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[CRY_SanctionDetails]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.CRY_SanctionDetails
GO

CREATE PROCEDURE dbo.CRY_SanctionDetails (@calling_oper Code)
 With Encryption As

BEGIN
Set NOCOUNT ON

    declare @stat int
	
	declare @ltype table(ListType VARCHAR(20))

	Insert into @LType
		-- Get all branch and dept combinations for ListType based on the operrights(135 - SanctionListTypes)
		SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 135) 
		INTERSECT
		-- Get all branch and dept combinations for SDN based on the operrights(100 - SDNparties)
		SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 100)

    Select 1 as TableType, ListType, EntNum, Name, Null As AltName, Title,
        Program, Type, DataType, Deleted,
        convert(varchar(8000), Remarks) as Remarks, NULL as Address,
        NULL as Address2, NULL as Address3, NULL as Address4, NULL as City,
        NULL As State, NULL As PostalCode, NULL as Country
        From SDNTable Where ListType <> 'USER'
		AND  EXISTS (Select ListType from @LType Where ListType = SDNTable.ListType)
    UNION
    Select 2 as TableType, S.ListType, SA.EntNum, S.Name, SA.AltName, NULL,
        NULL, NULL, NULL, SA.Deleted, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL
        From SDNAltTable SA, SDNTable S Where S.EntNum = SA.Entnum And
            S.ListType <> 'USER'
			AND  EXISTS (Select ListType from @LType Where ListType = S.ListType)
    UNION
    Select 3 as TableType, S.ListType, SA.EntNum, S.Name, NULL, NULL, NULL,
        NULL, NULL, SA.Deleted, NULL, SA.Address, SA.Address2, SA.Address3,
        SA.Address4, SA.City, SA.State, SA.PostalCode, SA.Country
        From SDNAddrTable SA, SDNTable S where S.EntNum = SA.Entnum And
            S.ListType <> 'USER'
		AND  EXISTS (Select ListType from @LType Where ListType = S.ListType)
    Order By ListType, Name, EntNum, TableType, AltName

    select @stat = @@ERROR
    return @stat
END
GO

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects
    WHERE ID = object_id(N'[dbo].[CRY_OFACViolStat]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE CRY_OFACViolStat
GO

CREATE PROCEDURE CRY_OFACViolStat (@calling_oper Code,@startDate DATETIME, @endDate DATETIME)
 With Encryption AS
	 Set NOCOUNT ON
/******************************************************************************
Procedure Name: CRY_OFACViolStat
Description:    Lists the OFAC Violation Summary between given dates

Parameters: @StartDate = Start Date for the Report to be generated
        @EndDate = End Date for the Report to be generated
******************************************************************************/
DECLARE @ViolStats TABLE (TotalViolations INT,
        TotalReviewedViolations INT,
        TotalNonReviewedViolations INT,
        TotalConfirmedViolations INT,
        TotalWaivedViolations INT,
        TotalDelegatedViolations INT,
        TotalPendingApprovalViol INT,
        HistTotalViolations INT,
        HistTotalReviewedViolations INT,
        HistTotalNonReviewedViolations INT,
        HistTotalConfirmedViolations INT,
        HistTotalWaivedViolations INT,
        HistTotalDelegatedViolations INT,
        HistTotalPendingApprovalViol INT)

Declare @FilterTable TABLE(Seqnumb Int,confirmstate int,trantime datetime,App Bit)
Declare @FilterHistTable TABLE(Seqnumb Int,confirmstate int,trantime datetime,App Bit)


 CREATE TABLE #OperBranchDept_Current (Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept_Current SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 115)

CREATE TABLE #OperBranchDept_Archived (Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept_Archived SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 120)

--Get the current OFAC cases that oper has right to access
Insert into @FilterTable
Select seqnumb,confirmstate,trantime,App from FilterTranTable FT
WHERE EXISTS (select * from #OperBranchDept_Current cbd where FT.Branch = cbd.Branch and FT.Dept = cbd.Dept)

--Get the archived OFAC cases that oper has right to access
Insert into @FilterHistTable
Select seqnumb,confirmstate,trantime,App from FilterTranHistTable FH
WHERE EXISTS (select * from #OperBranchDept_Archived cbd where FH.Branch = cbd.Branch and FH.Dept = cbd.Dept)

if object_id('tempdb..#OperBranchDept_Current') is not null 
		DROP TABLE #OperBranchDept_Current

if object_id('tempdb..#OperBranchDept_Archived') is not null 
		DROP TABLE #OperBranchDept_Archived

INSERT INTO @ViolStats(TotalViolations,
        TotalReviewedViolations,
        TotalNonReviewedViolations,
        TotalConfirmedViolations,
        TotalWaivedViolations,
        TotalDelegatedViolations,
        HistTotalViolations,
        HistTotalReviewedViolations,
        HistTotalNonReviewedViolations,
        HistTotalConfirmedViolations,
        HistTotalWaivedViolations,
        HistTotalDelegatedViolations,
        TotalPendingApprovalViol,
        HistTotalPendingApprovalViol)
    SELECT  (
         SELECT COUNT(*)    --Total No. of OFAC Violations
         FROM @FilterTable
         WHERE trantime BETWEEN @startDate AND @endDate
        ) AS TotalViolations ,
        (
         SELECT COUNT(*)    --Total No. of Reviewed Violations
         FROM @FilterTable
         WHERE confirmstate IS NOT NULL
         AND trantime BETWEEN @startDate AND @endDate
        ) AS TotalReviewedViolations,
        (
         SELECT COUNT(*)    --Total No. of Non-Reviewed Violations
         FROM @FilterTable
         WHERE confirmstate IS NULL
         AND trantime BETWEEN @startDate AND @endDate
        ) AS TotalNonReviewedViolations,
        (
         SELECT COUNT(*)    --Total No. of Confirmed Violations
         FROM @FilterTable
         WHERE confirmstate= 1
         AND trantime BETWEEN @startDate AND @endDate
        ) AS TotalConfirmedViolations,
        (
         SELECT COUNT(*)    --Total No. of Waived Violations
         FROM @FilterTable
         WHERE confirmstate= 2
         AND trantime BETWEEN @startDate AND @endDate
        ) AS TotalWaivedViolations,
        (
         SELECT COUNT(*)    --Total No. of Delegated Violations
         FROM @FilterTable
         WHERE confirmstate= 3
         AND trantime BETWEEN @startDate AND @endDate
        ) AS TotalDelegatedViolations,
        (
         SELECT COUNT(*)    --Total No. of OFAC Violations in History
         FROM @FilterHistTable
         WHERE trantime BETWEEN @startDate AND @endDate
        ) AS HistTotalViolations ,
        (
         SELECT COUNT(*)    --Total No. of Reviewed Violations in History
         FROM @FilterHistTable
         WHERE confirmstate IS NOT NULL
         AND trantime BETWEEN @startDate AND @endDate
        ) AS HistTotalReviewedViolations,
        (SELECT COUNT(*)    --Total No. of Non-Reviewed Violations in History
         FROM @FilterHistTable
         WHERE confirmstate IS NULL
         AND trantime BETWEEN @startDate AND @endDate
        ) AS HistTotalNonReviewedViolations,
        (
         SELECT COUNT(*)    --Total No. of Confirmed Violations in History
         FROM @FilterHistTable
         WHERE confirmstate= 1
         AND trantime BETWEEN @startDate AND @endDate
        ) AS HistTotalConfirmedViolations,
        (
         SELECT COUNT(*)    --Total No. of Waived Violations in History
         FROM @FilterHistTable
         WHERE confirmstate= 2
         AND trantime BETWEEN @startDate AND @endDate
        ) AS HistTotalWaivedViolations,
        (
         SELECT COUNT(*)    --Total No. of Delegated Violations in History
         FROM @FilterHistTable
         WHERE confirmstate= 3
         AND trantime BETWEEN @startDate AND @endDate
        ) AS HistTotalDelegatedViolations,
        0 AS TotalPendingApprovalViol,
        0 AS HistTotalPendingApprovalViol

        IF EXISTS(SELECT *
            FROM OptionTbl
            WHERE OptionTbl.Code = 'AppWaivViol'
            AND OptionTbl.Enabled = 1)  -- If Second Approval is turned OFF
        BEGIN
            UPDATE @ViolStats SET TotalPendingApprovalViol = TotalPendingApprovalViol+
                (SELECT count(*) FROM  @FilterTable
                WHERE confirmstate = 2
                AND app = 0) -- Total No. of Violations Pending Approval

            UPDATE @ViolStats SET HistTotalPendingApprovalViol = HistTotalPendingApprovalViol+
                (SELECT count(*) FROM  @FilterHistTable
                WHERE confirmstate = 2
                AND app = 0) -- Total No. of Violations Pending Approval in History
        END
        IF EXISTS(SELECT *
            FROM OptionTbl
            WHERE OptionTbl.Code = 'AppConfViol'
            AND OptionTbl.Enabled = 1)  -- If Second Approval is turned OFF
        BEGIN
            UPDATE @ViolStats SET TotalPendingApprovalViol = TotalPendingApprovalViol+
                (SELECT count(*) FROM  @FilterTable
                WHERE confirmstate = 1
                AND app = 0) -- Total No. of Violations Pending Approval

            UPDATE @ViolStats SET HistTotalPendingApprovalViol = HistTotalPendingApprovalViol+
                (SELECT count(*) FROM  @FilterHistTable
                WHERE confirmstate = 1
                AND app = 0) -- Total No. of Violations Pending Approval in History
        END

  SELECT * FROM @ViolStats

GO

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[CRY_IncrSanExclList]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.CRY_IncrSanExclList
GO 

CREATE  PROCEDURE dbo.CRY_IncrSanExclList( 
		@BranchList VARCHAR(8000), 
        @DepartmentList VARCHAR(8000),
		@StatusList varchar(30),
		@calling_oper code 
)

With Encryption AS

BEGIN
Set NOCOUNT ON
/* PARAMETER DESCRIPTION
@BranchList Comma-Separated list of Branches or '-ALL-' (default),
@DepartmentList Comma-Separated list of Departments or '-ALL-' (default),
@StatusList Comma seprated list of status'
*/

DECLARE @LastSDNUpd DATETIME 
DECLARE @CurrSDNUpd DATETIME
--Declare @status int
--Get the Last and the current SDN update dates
-- SELECT @LastSDNUpd = LastSDNUpdate FROM SDNDbStatus
-- SELECT @CurrSDNUpd = CurrSDNUpdate  FROM SDNDbStatus

-- CALL OFS_fnListParams for each of the Paramters that support comma separated values
SELECT @BranchList = dbo.OFS_fnListParams(@BranchList)
SELECT @DepartmentList = dbo.OFS_fnListParams(@DepartmentList)
SELECT @StatusList = dbo.OFS_fnListParams(@StatusList)

	-- Temp table to get the exclude names for which @calling_oper has rights
	-- and also get the Exclude names from the exclude table and Normalize the names
	declare @ExclList TABLE(OrigExcludeName [varchar](78),
	ExcludeName VARCHAR(100),
	[Param1] [varchar](255),
	[Param2] [varchar](255) ,
	[Param3] [varchar](255) ,
	[CreateDate] DateTime ,
	[LastModifDate] DateTime ,
	[LastOperCode] varchar(10) ,
	[LastOperName] VARCHAR(100),
	[Branch] varchar(10),
	[Dept] varchar(10) ,
	[AnnotText] [varchar](255),
	[Approved] [bit] ,
	[Deleted] [bit],
	[Ts] bigint
	)

	-- OFS_GetExcludeList that returs excludenames for which @calling_oper has rights
	Insert into @ExclList(OrigExcludeName,param1,param2,param3,createdate,lastmodifdate,
		lastopercode,branch,dept,annottext,approved,deleted,ts)
	exec OFS_GetExcludeList @calling_oper

	-- Format the text and insert into the temp table
	--    1. Remove the excess space by calling the function OFS_RecurvsiveReplace
	--    2. If the name has a ; (CIF Exclude) then just take the name after the semi-colon
	-- 
	-- Note: If a semi-colon is present in a name, even if the name is not CIF exclude, then the name
	--       will be taken after the semi-colon. This is a limitation, as there is no other way to 
	--       identify a CIF exclude.
	UPDATE EN
	   set ExcludeName = 
		dbo.OFS_RecursiveReplace(SUBSTRING(EN.OrigExcludeName, 
				CHARINDEX( ';', EN.OrigExcludeName, 0) + 1 , 
					LEN(EN.OrigExcludeName) - 
						CHARINDEX( ';', EN.OrigExcludeName, 0)), '  ', ' ') ,
		LastOperName = Op.[Name],
		branch = ISNULL(EN.Branch, '-ALL-'), 
		Dept = ISNULL(EN.Dept, '-ALL-')
	FROM 
		@ExclList EN, 
		PSEC.dbo.operator Op 
	WHERE    
		Op.Code = EN.LastOpercode AND
		(ISNULL(@BranchList, '') = '' OR ISNULL(EN.Branch, '') = '' OR
			CHARINDEX(',' + ltrim(rtrim(EN.Branch)) + ',',  @BranchList ) > 0) AND
		(ISNULL(@DepartmentList, '') = '' OR ISNULL(EN.Dept, '') = '' OR
			CHARINDEX(',' + ltrim(rtrim(EN.Dept)) +  ',',   @DepartmentList ) > 0)


SELECT distinct 
    En.OrigExcludeName, SDN.[Name], SDN.ListType, SDN.Program,
	CASE SDN.Status
	WHEN 1 THEN 'UNCHANGED'
	WHEN 2 THEN 'NEW'
	WHEN 3 THEN 'MODIFIED'
	WHEN 4 THEN 'DELETED'
	END as 'Status',
    EN.LastModifDate ,
    EN.LastOperCode ,
    EN.LastOperName, 
    EN.CreateDate,
    EN.AnnotText,
    EN.Branch, 
    EN.Dept 
FROM sdntable SDN, @ExclList EN 
WHERE 
    EN.ExcludeName = SDN.[Name] AND
    (ISNULL(@StatusList, '') = '' OR
        CHARINDEX(',' + ltrim(rtrim(SDN.status)) +  ',',   @StatusList ) > 0) AND
	 -- get listtypes for which @calling_oper has rights
	SDN.ListType in 
	(
		-- Get all branch and dept combinations for ListType based on the operrights(135 - SanctionListTypes)
		SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 135) 
		INTERSECT
		-- Get all branch and dept combinations for SDN based on the operrights(100 - SDNparties)
		SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 100)
	)  
UNION ALL
SELECT distinct
    En.OrigExcludeName, SDNAlt.AltName, SDN.ListType, SDN.Program,
CASE SDN.Status 
	WHEN 1 THEN 'UNCHANGED'
	WHEN 2 THEN 'NEW'
	WHEN 3 THEN 'MODIFIED'
	WHEN 4 THEN 'DELETED'
	END as 'Status',
    EN.LastModifDate ,
    EN.LastOperCode ,
    EN.LastOperName, 
    EN.CreateDate,
    EN.AnnotText,
    EN.Branch, 
    EN.Dept 
FROM SDNTable SDN, sdnAltTable SDNAlt, @ExclList EN 
WHERE 
    SDN.EntNum = SDNAlt.EntNUM AND
    EN.ExcludeName = SDNAlt.AltName AND
    (ISNULL(@StatusList, '') = '' OR
        CHARINDEX(',' + ltrim(rtrim(SDN.status)) +  ',',   @StatusList ) > 0) AND
	 -- get listtypes for which @calling_oper has rights
	SDN.ListType in 
	(
		-- Get all branch and dept combinations for ListType based on the operrights(135 - SanctionListTypes)
		SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 135) 
		INTERSECT
		-- Get all branch and dept combinations for SDN based on the operrights(100 - SDNparties)
		SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 100)
	)  
ORDER BY EN.OrigExcludeName

END
GO

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[CRY_SancExclList]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.CRY_SancExclList
GO 

CREATE PROCEDURE dbo.CRY_SancExclList @calling_oper As Code                                
 With Encryption AS

BEGIN
Set NOCOUNT ON
	-- Temp table to get the exclude names for which @calling_oper has rights
	declare @ExclList TABLE([name] [varchar](78),
	[Param1] [varchar](255),
	[Param2] [varchar](255) ,
	[Param3] [varchar](255) ,
	[CreateDate] DateTime ,
	[LastModifDate] DateTime ,
	[LastOper] varchar(10) ,
	[Branch] varchar(10),
	[Dept] varchar(10) ,
	[AnnotText] [varchar](255),
	[Approved] [bit] ,
	[Deleted] [bit],
	[Ts] bigint)
	

	Insert into @ExclList
	-- OFS_GetExcludeList that returs excludenames for which @calling_oper has rights
	exec OFS_GetExcludeList @calling_oper 
	
  SELECT EN.name ExcludeName, SN.Name SDNName
  FROM  @ExclList EN INNER JOIN OFAC.dbo.SDNTable SN 
	ON EN.name=SN.Name
	-- get listtypes for which @calling_oper has rights
	WHERE SN.ListType in 
	(
		-- Get all branch and dept combinations for ListType based on the operrights(135 - SanctionListTypes)
		SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 135) 
		INTERSECT
		-- Get all branch and dept combinations for SDN based on the operrights(100 - SDNparties)
		SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 100)

	) 
  ORDER BY EN.name
END
GO

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[CRY_ArchiveOperator]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.CRY_ArchiveOperator
GO

CREATE PROCEDURE dbo.CRY_ArchiveOperator(@calling_oper Code,
									@StartDate DATETIME,
                                    @EndDate DATETIME
                                    )
 With Encryption AS

BEGIN
Set NOCOUNT ON

CREATE TABLE #OperBranchDept(Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 120)

SELECT FH.ConfirmState, FH.ReqTime,FH.ConfirmOper, Op.Name
	FROM   OFAC.dbo.FilterTranHistTable FH 
		--OperTblView - Sql View in OFAC db that reflects the Psec Operator table
	LEFT OUTER JOIN OFAC.dbo.OperTblView Op ON FH.ConfirmOper=Op.Code
	WHERE  (FH.ReqTime >= @StartDate AND FH.ReqTime < @EndDate) 
		AND (FH.ConfirmState in (1,2)) -- Only Waived or Confirmed cases
		--Get the results based on the operrights for FilterTranHistTable
		AND EXISTS (select * from #OperBranchDept cbd where FH.Branch = cbd.Branch and FH.Dept = cbd.Dept)

if object_id('tempdb..#OperBranchDept') is not null 
		DROP TABLE #OperBranchDept

END
GO

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[CRY_ArcStatsByStat]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.CRY_ArcStatsByStat
GO 

CREATE PROCEDURE dbo.CRY_ArcStatsByStat (@calling_oper Code,
									@StartDate DATETIME,
                                    @EndDate DATETIME
                                    )
 With Encryption AS
	 
BEGIN
SET NOCOUNT ON

  CREATE TABLE #OperBranchDept(Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 120)
	
  SELECT FH.ConfirmState, FH.ReqTime 
	FROM   OFAC.dbo.FilterTranHistTable FH 
	 WHERE  (FH.ReqTime>=@StartDate AND FH.ReqTime<@EndDate)
	--Get the results based on the operrights for FilterTranHistTable
	AND EXISTS (select * from #OperBranchDept cbd where FH.Branch = cbd.Branch and FH.Dept = cbd.Dept)

  if object_id('tempdb..#OperBranchDept') is not null 
		DROP TABLE #OperBranchDept

END
GO

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[CRY_ArcStatsBySrc]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.CRY_ArcStatsBySrc
GO 

CREATE PROCEDURE dbo.CRY_ArcStatsBySrc (@calling_oper Code,
									@StartDate DATETIME,
                                    @EndDate DATETIME
                                    )
 With Encryption AS

BEGIN
SET NOCOUNT ON

 CREATE TABLE #OperBranchDept(Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 120)

 SELECT FH.Branch, FH.Dept, FH.Source, FH.ConfirmState, FH.ReqTime
 FROM   OFAC.dbo.FilterTranHistTable FH
 WHERE  (FH.ReqTime >= @StartDate AND FH.ReqTime < @EndDate)
	--Get the results based on the operrights for FilterTranHistTable
	AND EXISTS (select * from #OperBranchDept cbd where FH.Branch = cbd.Branch and FH.Dept = cbd.Dept)

if object_id('tempdb..#OperBranchDept') is not null 
		DROP TABLE #OperBranchDept

END
GO

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[OFS_TopMatchAndCaseNumbers]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.OFS_TopMatchAndCaseNumbers
GO

Create Proc OFS_TopMatchAndCaseNumbers @calling_oper Code

With Encryption As
	SET NOCOUNT ON

CREATE TABLE #tmpListTypes(ListType VARCHAR(100))
INSERT INTO #tmpListTypes
SELECT * FROM fn_GetListTypesForOper(@calling_oper)

CREATE TABLE #OperBranchDept (Branch VARCHAR(10), Dept VARCHAR(10))
INSERT INTO #OperBranchDept SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 115)

Select MatchTable.SeqNumb, MatchName, OriginalSDNName,
        RTrim(LTrim(SubString(MatchText,1,254)))As MatchText
    From MatchTable INNER JOIN FilterTranTable FTH ON
        MatchTable.SeqNumb = FTH.SeqNumb
Where  MatchName In
 (Select MatchName From
		(Select Distinct Top 20  InnerDummy.MatchName,
        Count(*) As Total From (Select Distinct M.MatchName , M.SeqNumb From
        MatchTable As M where seqnumb in (select seqnumb from dbo.OFS_FnGetViolByOper(@calling_oper))) InnerDummy
		 Group By InnerDummy.MatchName Order By Total DESC )  As OuterDummy
)  and 
EXISTS (select * from #OperBranchDept cbd 
				where FTH.Branch = cbd.Branch and FTH.Dept = cbd.Dept)
		And NOT Exists(select * from vw_GetFilterTranListTypes c Left Outer Join #tmpListTypes olt
								ON c.ListType = olt.ListType
						WHERE olt.ListType IS NULL and FTH.seqnumb = c.seqnumb)

if object_id('tempdb..#tmpListTypes') is not null 
	DROP TABLE #tmpListTypes

  if object_id('tempdb..#OperBranchDept') is not null 
	DROP TABLE #OperBranchDept

GO

/*------------------------------------------------------------------------------------------------*/

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[CRY_MntStatsBySrc]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE dbo.CRY_MntStatsBySrc
GO

CREATE PROCEDURE dbo.CRY_MntStatsBySrc (@calling_oper AS Code,
									@StartDate DATETIME,
                                    @EndDate DATETIME,
                                    @BranchList  Varchar(255),
                                    @DeptList  Varchar(255),
                                    @SortBy Varchar(20)
									)
With Encryption AS

BEGIN
SET NOCOUNT ON

CREATE TABLE #OperBranchDept_Current (Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept_Current SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 115)

CREATE TABLE #OperBranchDept_Archived (Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept_Archived SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 120)


    IF LTRIM(RTRIM(@BranchList)) = '-ALL-' OR LTRIM(RTRIM(@BranchList)) = ''
            SELECT @BranchList = NULL
    ELSE
            SELECT @BranchList = ','+ REPLACE(@BranchList,' ','')+ ','

    IF LTRIM(RTRIM(@DeptList)) = '-ALL-' OR LTRIM(RTRIM(@DeptList)) = ''
            SELECT @DeptList = NULL
    ELSE
            SELECT @DeptList = ','+ REPLACE(@DeptList,' ','')+ ','

    SELECT yy, mm, yymm, source,
        CASE FT.ConfirmState
            WHEN 1 THEN 'Confirmed'
            WHEN 2 THEN 'Waived'            ELSE 'Unconfirmed'
        END AS status, COUNT(*) FTCount
    FROM(
        SELECT DATEPART (yy,trantime) yy, DATEPART(mm,trantime) mm,
            YEAR(trantime)*100 + MONTH(trantime) AS yymm,
            FH.Source, FH.ConfirmState,
            FH.TranTime, FH.Branch,
            FH.Dept
        FROM    dbo.FilterTranHistTable FH
		WHERE   FH.TranTime >= @StartDate
        AND FH.TranTime <= @EndDate
		AND EXISTS (select * from #OperBranchDept_Archived cbd where FH.Branch = cbd.Branch and FH.Dept = cbd.Dept)
		UNION ALL
        SELECT DATEPART (yy,trantime) yy, DATEPART(mm,trantime) mm,
            YEAR(trantime)*100 + MONTH(trantime) AS yymm,
            F.Source, F.ConfirmState,
            F.TranTime, F.Branch,
            F.Dept
         FROM dbo.FilterTranTable F
		 WHERE   F.TranTime >= @StartDate
        AND F.TranTime <= @EndDate
		AND EXISTS (select * from #OperBranchDept_Current cbd where F.Branch = cbd.Branch and F.Dept = cbd.Dept)
    ) AS FT
    WHERE  (@BranchList IS NULL
            OR CHARINDEX(',' + CONVERT(VARCHAR, FT.Branch) +',',@BranchList)>0)
        AND (@DeptList IS NULL
            OR CHARINDEX(',' + CONVERT(VARCHAR, FT.Dept) + ',', @DeptList) > 0)
    GROUP BY yy, mm, yymm,FT.Source,
        CASE FT.ConfirmState
        WHEN 1 THEN 'Confirmed'
        WHEN 2 THEN 'Waived'
        ELSE 'Unconfirmed'
        END
    ORDER BY  (CASE WHEN @SortBy='Date' THEN Convert(Varchar, FT.yymm)
                ELSE FT.Source END)

if object_id('tempdb..#OperBranchDept_Current') is not null 
		DROP TABLE #OperBranchDept_Current

if object_id('tempdb..#OperBranchDept_Archived') is not null 
		DROP TABLE #OperBranchDept_Archived

END
GO

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[OFS_CompleteLoadDistribution]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE OFS_CompleteLoadDistribution
GO

CREATE PROCEDURE dbo.OFS_CompleteLoadDistribution (
                            @isdiff bit
) With Encryption As

    declare @oper Code
    declare @trnCnt int

    select @oper = 'Prime'
    select @trnCnt = @@trancount  -- Save the current trancount

    if @trnCnt = 0
        -- Transaction has not begun
        begin tran OFS_CompleteLoadDistribution
    else
        -- Already in a transaction
        save tran OFS_CompleteLoadDistribution

    -- Processing starts here:
    declare @stat int

--******** Update Main tables from temp tables ********************************
/* TempLoad SDN tables contains the distribution records
    If ListModifDate is equal then just update the Status with the
    distribution record status else update the whole record except the
    deleted column

    If record is not in main table then insert the record

    Set the Status of all records in main table but not in temp table
    to deleted if not loading a differential file, else set the Status
    to unchanged
*/
--*****************************************************************************

    declare @newstatus int
    If @isdiff = 0
        set @newstatus = 4  --deleted
    Else
        set @newstatus = 1  --unchanged

    Create table #TempUserEntNums (
	EntNum int
    )

    Create table #TempUserAltNums (
	AltNum int
    )

    insert into #TempUserEntNums(EntNum)
    Select  distinct Entnum from SDNTable where UserRec = 1

    insert into #TempUserAltNums(AltNum)
    Select distinct AltNum from SDNAltTable, SDNTable where SDNAltTable.EntNum = SDNTable.Entnum
    and SDNTable.UserRec = 1

   -- For all lists that have been disabled, set the record statuses to deleted
    
	Update SDNTable Set Status = 4, LastModifDate = getDate()
        FROM SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and s.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update SDNAltTable Set Status = 4, LastModifDate = getDate()
        FROM SDNAltTable sa (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = sa.EntNum and sa.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update SDNAddrTable Set Status = 4, LastModifDate = getDate()
        FROM SDNAddrTable sa (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = sa.EntNum and sa.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update DOBs Set Status = 4, LastModify = getDate()
        FROM DOBs d (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = d.EntNum and d.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update Notes Set Status = 4, LastModify = getDate()
        FROM Notes n (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = n.EntNum and n.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update URLs Set Status = 4, LastModify = getDate()
        FROM URLs u (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = u.EntNum and u.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update Relationship Set Status = 4, LastModify = getDate()
        FROM Relationship r (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = r.EntNum and r.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update Keywords Set Status = 4, LastModify = getDate()
        FROM Keywords k (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = k.EntNum and k.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update KeywordsAlt Set Status = 4, LastModify = getDate()
        FROM KeywordsAlt k (nolock), SDNAltTable sa (nolock),
            SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            sa.AltNum = k.AltNum and sa.EntNum = s.EntNum and k.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

   -- Update entire record if distribution listmodifdate != last listmodifdate
   -- or (if status not equal to unchanged and listmodifdates are equal)

If Exists (Select EntNum from TempLoadSDNTable) Begin
   Update SDNtable Set
        [Name] = t.[Name],
        FirstName = NULLIF(t.FirstName,''),
        MiddleName = NULLIF(t.MiddleName,''),
        LastName = NullIf(t.LastName,''),
        Title = t.Title,
        Program = UPPER(t.Program),
        Type = t.Type,
        CallSign = t.CallSign,
        VessType = t.VessType,
        Tonnage = t.Tonnage,
        GRT = t.GRT,
        VessFlag = t.VessFlag,
        VessOwner = t.Vessowner,
        Dob = NullIF(t.DOB,''),
        Remarks = t.Remarks,
        Remarks1 = t.Remarks1,
        Remarks2 = t.Remarks2,
        SDNType = t.SDNType,
        DataType = t.DataType,
        userRec = t.userRec,
        duplicRec = t.duplicRec,
        IgnoreDerived = t.IgnoreDerived,
        ListID = t.ListId,
        ListType = t.ListType,
        Country = t.Country,
        Sex = t.Sex,
        Build = t.Build,
        Height = NullIf(t.Height,0),
        Weight = NullIf(t.Weight,0),
        Race = t.Race,
        Complexion = t.Complexion,
        eyes = t.eyes,
        hair = t.hair,
        PrimeAdded = t.PrimeAdded,
        StandardOrder = t.StandardOrder,
        EffectiveDate = t.EffectiveDate,
        ExpiryDate = t.ExpiryDate,
        Status = t.Status,
        ListCreateDate = t.ListCreatedate,
        ListModifDate = t.ListModifDate,
        LastModifDate = GetDate(),
        LastOper = t.LastOper
    From SDNTable s, TempLoadSDNTable t
        Where s.EntNum = t.EntNum And s.UserRec = 0 And
              ( (IsNull(s.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End
   -- Update status to distribution records status
   -- if distribution listmodifdate = last listmodifdate and status = unchanged
If Exists (Select EntNum from TempLoadSDNTable) Begin
   Update SDNTable Set Status = t.Status
        From SDNTable s, TempLoadSDNTable t
            Where s.EntNum = t.EntNum  and s.UserRec = 0 And
               IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0) And
               t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

-- Update record status of records not in temp table
  If (@newstatus=1)
	Begin
		Update SDNTable Set Status = @newstatus 
			FROM SDNTable s (nolock) left outer join TempLoadSDNTable t (nolock)
			on s.EntNum = t.EntNum
			Where t.EntNum is NULL and s.UserRec = 0 and s.Status in (2,3)
	End
  Else
	Begin
		Update SDNTable Set Status = @newstatus, LastModifDate = getDate()
			FROM SDNTable s (nolock) left outer join TempLoadSDNTable t (nolock)
			on s.EntNum = t.EntNum
			Where t.EntNum is NULL and s.UserRec = 0 and s.Status <> 4
	END
   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

-- Insert new records
If Exists (Select EntNum from TempLoadSDNTable) Begin
   Insert into SDNTable (EntNum, [Name], FirstName,
                        MiddleName, LastName,Title,
                        Program, Type, CallSign,
                        VessType, Tonnage, GRT,
                        VessFlag, VessOwner, Dob,
                        Remarks, Remarks1, Remarks2,SDNType,
                        DataType, userRec, deleted,
                        duplicRec, IgnoreDerived,
                        ListID, ListType, Country,
                        Sex, Build, Height, Weight,
                        Race, Complexion, eyes, hair,
                        PrimeAdded, StandardOrder, EffectiveDate,
                        ExpiryDate, Status, ListCreateDate,
                        ListModifDate, CreateDate, LastModifDate,
                        LastOper)
    Select t.EntNum, t.[Name], t.FirstName,
                        t.MiddleName, t.LastName, t.Title,
                        t.Program, t.Type, t.CallSign,
                        t.VessType, t.Tonnage, t.GRT,
                        t.VessFlag, t.VessOwner, t.Dob,
                        t.Remarks, t.Remarks1, t.Remarks2, t.SDNType,
                        t.DataType, t.userRec, t.deleted,
                        t.duplicRec, t.IgnoreDerived,
                        t.ListID, t.ListType, t.Country,
                        t.Sex, t.Build, t.Height, t.Weight,
                        t.Race, t.Complexion, t.eyes, t.hair,
                        t.PrimeAdded, t.StandardOrder, t.EffectiveDate,
                        t.ExpiryDate, t.Status, t.ListCreateDate,
                        t.ListModifDate, GetDate(), GetDate(),
                        t.LastOper
    From TempLoadSDNTable t (nolock) left Outer Join SDNTable s (nolock)
        on t.EntNum = s.EntNum
    where s.Entnum is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select AltNum from TempLoadAltTable) Begin
  Update SDNAlttable Set
        EntNum  = t.EntNum,
        AltType = t.AltType,
        AltName = t.AltName,
        FirstName = t.FirstName,
        MiddleName = t.MiddleName,
        LastName = t.LastName,
        ListId = t.ListId,
        Remarks = t.Remarks,
        status = t.status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModifDate = GetDate(),
        LastOper = t.LastOper
     From SDNAltTable s, TempLoadAltTable t
        Where s.AltNum = t.AltNum And
              ( (IsNull(s.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End
   -- Update status to distribution records status
   -- if distribution listmodifdate = last listmodifdate and status = unchanged
If Exists (Select AltNum from TempLoadAltTable) Begin
   Update SDNAltTable Set Status = t.Status, LastOper = t.LastOper
    From SDNAltTable s, TempLoadAltTable t
            Where s.AltNum = t.AltNum And
               IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End


  -- Update record status of records not in temp table
   If (@newstatus = 1)
	Begin
		Update SDNAltTable Set Status = @newstatus,
			LastOper = @oper
		From SDNAltTable s (nolock) 
                left outer join  TempLoadAltTable t (nolock) on s.AltNum = t.AltNum
                Where t.AltNum is NULL and s.Status in (2,3)
	End
	Else
	Begin
		Update SDNAltTable Set Status = @newstatus, LastModifDate = getDate(),
                LastOper = @oper
                From SDNAltTable s (nolock) 
                left outer join TempLoadAltTable t (nolock) on s.AltNum = t.AltNum
                left outer join #TempUserAltNums ta on s.AltNum = ta.AltNum
		Where t.AltNum is NULL and ta.AltNum is NULL and s.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution



   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select AltNum from TempLoadAltTable) Begin
   Insert into SDNAltTable (EntNum, AltNum, AltType, AltName, FirstName,
      MiddleName, LastName,Remarks, Status, ListCreateDate, ListModifDate,
      CreateDate, LastModifDate, LastOper)
   Select t.EntNum, t.AltNum, t.AltType, t.AltName, t.FirstName,
        t.MiddleName, t.LastName, t.Remarks, t.Status, t.ListCreateDate, t.ListModifDate,
        GetDate(), GetDate(), t.LastOper
   From TempLoadAltTable t (nolock) left Outer Join SDNAltTable s (nolock)
            on t.AltNum = s.AltNum
        where s.AltNum is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select AddrNum from TempLoadAddrTable)  Begin
    -- Update the address records
   Update SDNAddrTable Set
        EntNum = t.EntNum,
        Address = t.Address,
        City = t.City,
        Country = t.Country,
        Remarks = t.Remarks,
        ListId = t.ListId,
        FreeFormat = t.FreeFormat,
        Address2 = t.Address2,
        Address3 = t.Address3,
        Address4 = t.Address4,
        State = t.State,
        PostalCode = t.PostalCode,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModifDate = GetDate(),
        LastOper = t.LastOper
    From SDNAddrTable s, TempLoadAddrTable t
        Where s.AddrNum = t.AddrNum And
              ( (IsNull(s.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select AddrNum from TempLoadAddrTable) Begin
   Update SDNAddrTable Set Status = t.Status, LastOper = t.LastOper
    From SDNAddrTable s, TempLoadAddrTable t
            Where s.AddrNum = t.AddrNum And
               IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
   If (@newstatus = 1)
	Begin
		Update SDNAddrTable Set Status = @newstatus, 
			LastOper = @oper
		From SDNAddrTable s (nolock) 
                left outer join  TempLoadAddrTable t (nolock) on s.AddrNum = t.AddrNum
                Where t.AddrNum is NULL and s.Status in (2,3)
	End
	Else
	Begin
		Update SDNAddrTable Set Status = @newstatus, LastModifDate = getDate(),
			LastOper = @oper
		From SDNAddrTable s (nolock) left outer join  TempLoadAddrTable t (nolock) on s.AddrNum = t.AddrNum
                Left outer join #TempUserEntNums ta on s.EntNum = ta.EntNum
		Where t.AddrNum is NULL and ta.EntNum is NULL and s.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select AddrNum from TempLoadAddrTable) Begin
     Insert into SDNAddrTable (EntNum, AddrNum, Address, City, Country, Remarks,
        ListId, FreeFormat, Address2, Address3, Address4,
        State, PostalCode, Status, ListCreateDate, ListModifDate,
        CreateDate, LastModifDate, LastOper)
    Select t.EntNum, t.AddrNum, t.Address, t.City, t.Country, t.Remarks,
        t.ListId, t.FreeFormat, t.Address2, t.Address3, t.Address4,
        t.State, t.PostalCode, t.Status, t.ListCreateDate, t.ListModifDate,
        GetDate(), GetDate(), t.LastOper
    From TempLoadAddrTable t (nolock) left Outer Join SDNAddrTable s (nolock)
        on t.AddrNum = s.AddrNum
    where s.AddrNum is Null


   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsId from TempLoadKeywords)  Begin
    -- Update the address records
   Update Keywords Set
        EntNum = t.EntNum,
        Word = t.Word,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From Keywords k, TempLoadKeywords t
        Where k.keywordsId = t.keywordsId And
              ( (IsNull(k.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsId from TempLoadKeywords) Begin
   Update Keywords Set Status = t.Status, LastOper = t.LastOper
    From Keywords k, TempLoadKeywords t
            Where k.keywordsId = t.keywordsId And
               IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
	If (@newstatus=1)
	Begin
		Update Keywords Set Status = @newstatus, 
			LastOper = @oper
		From Keywords k (nolock) left outer join TempLoadKeywords t (nolock)
        on k.keywordsId = t.keywordsId
        Where t.keywordsId is NULL and k.Status in (2,3)
	End
	Else
	Begin
		Update Keywords Set Status = @newstatus, LastModify = getDate(),
			LastOper = @oper
		From Keywords k (nolock) left outer join TempLoadKeywords t (nolock) on k.keywordsId = t.keywordsId
                left outer join #TempUserEntNums ta on k.EntNum = ta.EntNum
		Where t.keywordsId is NULL and ta.EntNum is NULL and k.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select keywordsId from TempLoadKeywords) Begin
     Insert into keywords (KeywordsId, Entnum, Word, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
     Select t.KeywordsId, t.Entnum, t.Word, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadKeywords t (nolock) left Outer Join keywords k (nolock)
        on t.keywordsId = k.keywordsId
    where k.keywordsId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsAltId from TempLoadKeywordsAlt)  Begin
    -- Update the address records
   Update KeywordsAlt Set
        AltNum = t.AltNum,
        Word = t.Word,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From KeywordsAlt k, TempLoadKeywordsAlt t
        Where k.keywordsAltId = t.keywordsAltId And
              ( (IsNull(k.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsAltId from TempLoadKeywordsAlt) Begin
   Update KeywordsAlt Set Status = t.Status, LastOper = t.LastOper
    From KeywordsAlt k, TempLoadKeywordsAlt t
            Where k.keywordsAltId = t.keywordsAltId And
               IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
	If (@newstatus=1)
	Begin
		Update KeywordsAlt Set Status = @newstatus, 
			LastOper = @oper
		From KeywordsAlt k (nolock) left outer join TempLoadKeywordsAlt t (nolock)
        on k.keywordsAltId = t.keywordsAltId
        Where t.keywordsAltId is NULL and k.Status in (2,3)
	End
	Else
	Begin
		Update KeywordsAlt Set Status = @newstatus, LastModify = getDate(),
			LastOper = @oper
		From KeywordsAlt k (nolock) 
                left outer join TempLoadKeywordsAlt t (nolock) on k.keywordsAltId = t.keywordsAltId
                left outer join #TempUserAltNums ta on k.AltNum = ta.AltNum
		Where t.keywordsAltId is NULL and ta.AltNum is NULL and k.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select keywordsAltId from TempLoadKeywordsAlt) Begin
     Insert into keywordsAlt (KeywordsAltId, Altnum, Word,
        Status, ListCreateDate, ListModifdate,
        CreateOper, CreateDate, LastOper,
            LastModify)
     Select t.KeywordsAltId, t.Altnum, t.Word,
        t.Status, t.ListCreateDate, t.ListModifdate,
        t.CreateOper, GetDate(), t.LastOper,
            GetDate()
    From TempLoadKeywordsAlt t (nolock) left Outer Join KeywordsAlt k (nolock)
        on t.KeywordsAltId = k.KeywordsAltId
    where k.KeywordsAltId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select DOBsId from TempLoadDOBs)  Begin
    -- Update the address records
   Update DOBs Set
        EntNum = t.EntNum,
        DOB = t.DOB,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From DOBs d, TempLoadDOBs t
        Where d.DOBsId = t.DOBsId And
              ( (IsNull(d.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(d.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select DOBsId from TempLoadDOBs) Begin
   Update DOBs Set Status = t.Status, LastOper = t.LastOper
    From DOBs d, TempLoadDOBs t
            Where d.DOBsId = t.DOBsId And
               IsNull(d.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
	If (@newstatus=1)
	Begin
		Update DOBs Set Status = @newstatus,
			LastOper = @oper
		From DOBs d (nolock) left outer join TempLoadDOBs t (nolock)
        on d.DOBsId = t.DOBsId
        Where t.DOBsId is NULL and d.Status in (2,3) 
	End
	Else
	Begin
		Update DOBs Set Status = @newstatus, LastModify = getDate(),
			LastOper = @oper
		From DOBs d (nolock) 
                left outer join TempLoadDOBs t (nolock) on d.DOBsId = t.DOBsId
                left outer join #TempUserEntNums ta on d.EntNum = ta.EntNum
		Where t.DOBsId is NULL and ta.EntNum is NULL and d.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select DOBsId from TempLoadDOBs) Begin
     Insert into DOBs (DOBsId, Entnum, DOB, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.DOBsId, t.Entnum, t.DOB, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadDOBs t (nolock) left Outer Join DOBs d (nolock)
        on t.DOBsId = d.DOBsId
    where d.DOBsId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select NotesId from TempLoadNotes)  Begin
    -- Update the address records
   Update Notes Set
        EntNum = t.EntNum,
        Note = t.Note,
        NoteType = t.NoteType,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From Notes n, TempLoadNotes t
        Where n.NotesId = t.NotesId And
              ( (IsNull(n.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(n.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select NotesId from TempLoadNotes) Begin
   Update Notes Set Status = t.Status, LastOper = t.LastOper
    From Notes n, TempLoadNotes t
            Where n.NotesId = t.NotesId And
               IsNull(n.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
	If (@newstatus=1)
	Begin
		Update Notes Set Status = @newstatus,
		LastOper = @oper
		From Notes n (nolock) left outer join TempLoadNotes t (nolock)
        on n.NotesId = t.NotesId
        Where t.NotesId is NULL and n.Status in (2,3)
	End
	Else
	Begin
		Update Notes Set Status = @newstatus, LastModify = getDate(),
		LastOper = @oper
		From Notes n (nolock) 
                left outer join TempLoadNotes t (nolock) on n.NotesId = t.NotesId
                left outer join #TempUserEntNums ta on n.EntNum = ta.EntNum
		Where t.NotesId is NULL and ta.EntNum is NULL and n.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select NotesId from TempLoadNotes) Begin
     Insert into Notes (NotesId, Entnum, Note, NoteType, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.NotesId, t.Entnum, t.Note, t.NoteType, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadNotes t (nolock) left Outer Join Notes n (nolock)
        on t.NotesId = n.NotesId
    where n.NotesId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select URLsId from TempLoadURLs)  Begin
    -- Update the address records
   Update URLs Set
        EntNum = t.EntNum,
        URL = t.URL,
        [Description] = t.[Description],
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From URLs u, TempLoadURLs t
        Where u.URLsId = t.URLsId And
              ( (IsNull(u.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(u.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select URLsId from TempLoadURLs) Begin
   Update URLs Set Status = t.Status, LastOper = t.LastOper
    From URLs u, TempLoadURLs t
            Where u.URLsId = t.URLsId And
               IsNull(u.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
	If (@newstatus=1)
	Begin
		Update URLs Set Status = @newstatus,
		LastOper = @oper
		From URLs u (nolock) left outer join TempLoadURLs t (nolock)
        on u.URLsId = t.URLsId
        Where t.URLsId is NULL and u.Status in (2,3)
	End
	Else
	Begin
		Update URLs Set Status = @newstatus, LastModify = getDate(),
		LastOper = @oper
		From URLs u (nolock) 
                left outer join TempLoadURLs t (nolock) on u.URLsId = t.URLsId
                left outer join #TempUserEntNums ta on u.EntNum = ta.EntNum
		Where t.URLsId is NULL and ta.EntNum is NULL and u.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select URLsId from TempLoadURLs) Begin
     Insert into URLs (URLsId, Entnum, URL, [Description], Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.URLsId, t.Entnum, t.URL, t.[Description], t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadURLs t (nolock) left Outer Join URLs u (nolock)
        on t.URLsId = u.URLsId
    where u.URLsId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select RelationshipId from TempLoadRelationship)  Begin
    -- Update the address records
   Update Relationship Set
        EntNum = t.EntNum,
        RelatedID = t.RelatedID,
        RelationshipPToR = t.RelationshipPToR,
        RelationshipRToP = t.RelationshipRToP,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From Relationship r, TempLoadRelationship t
        Where r.RelationshipId = t.RelationshipId And
              ( (IsNull(r.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(r.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select RelationshipId from TempLoadRelationship) Begin
   Update Relationship Set Status = t.Status, LastOper = t.LastOper
    From Relationship r, TempLoadRelationship t
            Where r.RelationshipId = t.RelationshipId And
               IsNull(r.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
	If (@newstatus=1)
	Begin
		Update Relationship Set Status = @newstatus, 
		LastOper = @oper
		From Relationship r (nolock) left outer join TempLoadRelationship t (nolock)
        on r.RelationshipId = t.RelationshipId
        Where t.RelationshipId is NULL and r.Status in (2,3)
	End
	Else
	Begin
		Update Relationship Set Status = @newstatus, LastModify = getDate(),
		LastOper = @oper
		From Relationship r (nolock) 
                left outer join TempLoadRelationship t (nolock) on r.RelationshipId = t.RelationshipId
                left outer join #TempUserEntNums ta on r.EntNum = ta.EntNum
		Where t.RelationshipId is NULL and ta.EntNum is NULL and r.Status <> 4
	End
   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select RelationshipId from TempLoadRelationship) Begin
     Insert into Relationship (RelationshipId, Entnum, RelatedID,
        RelationshipPToR, RelationshipRToP, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.RelationshipId, t.Entnum, t.RelatedID,
        t.RelationshipPToR, t.RelationshipRToP, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadRelationship t (nolock) left Outer Join Relationship r (nolock)
        on t.RelationshipId = r.RelationshipId
    where r.RelationshipId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  if @isdiff = 0
  begin
      -- set status of undeleted records to unchanged
      Update SDNTable
        Set Status = 1 Where userRec = 1 and Deleted = 0

      Select @stat = @@error
      If (@stat <> 0)
          Goto AbortCompleteLoadDistribution
  end

--populate dob column in SDNTABLE from DOBs table.  DOB column should be shown as CSV value
update  sdntable set
dob=dt.dob_csv
from (
	select
		t1.entnum,
		stuff((
				select ',' + t.[dob]
				from dobs t
				where t.entnum = t1.entnum and
				t.status in (1,2,3)
				order by t.[dob]
				for xml path('')
				),1,1,'') as dob_csv
	from dobs t1
	group by entnum
) dt
inner join sdntable  s on
dt.entnum=s.entnum
where s.status  in (1,2,3)

/*
	update the status to modified in sdntable if the following condition is met: dobs table contains status of new or modified and status in Sdntable is NOT New.
	The LastModifDate should not be changed if the above conditions are met.
*/
UPDATE sdntable SET
[status]=3
FROM
( 
	SELECT MAX( d.status)AS NewMod, d.entnum 
	FROM dobs as d 
	WHERE d.status in (2,3)
	and 
	not exists
	(select * from sdntable sd where sd.entnum=d.entnum and sd.status=2)
	GROUP BY d.entnum
) AS dt
INNER JOIN sdntable AS st ON
st.entnum=dt.entnum

-- Set Keywords and Keywords Alternate to deleted 
--if the deleted keywords are introduced in the new distribution
Update Keywords 
Set Deleted = 1, 
	LastModify = GetDate() 
where Deleted = 0 and 
	  Word in (select word from Keywords where deleted = 1)

Update KeywordsAlt 
set Deleted = 1, 
	LastModify = GetDate() 
where Deleted = 0 and 
	  Word in (select word from KeywordsAlt where Deleted = 1)

 delete from TempLoadSDNTable
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

 delete from TempLoadAltTable
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadAddrTable
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadKeywords
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

 delete from TempLoadKeywordsAlt
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadDOBs
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadNotes
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadURLs
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadRelationship
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

 If @trnCnt = 0
    commit tran OFS_CompleteLoadDistribution

 return 0  -- Success

AbortCompleteLoadDistribution:
    if (@stat <> 0) Begin
        rollback tran OFS_CompleteLoadDistribution
        return @stat
    End
Go

Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print '  Completed Conversion of OFAC Database    '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go
