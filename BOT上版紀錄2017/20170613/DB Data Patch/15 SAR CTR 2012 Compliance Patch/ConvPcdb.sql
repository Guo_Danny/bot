/*
**  File Name:        ConvPcdb.sql
**
**  Functional Description:
**
**      This module contains SQL patches
**      for version 9.0.3
**
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2010                                                   ***
*** FIS						                                             ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|---------------------------------------------------- 
11/10/2010      VK		Modified WCDB_GetViewData
						
11/10/2010		VK		Modified WCDB_GetRightsWhereClause to use OFS_fnListTypesForOperByView, 
						OFS_fnListTypesForOperByView and OFS_fnGetCIFRulesForOperByView functions
09/10/2012		SS		Removed "ADD SFISAR" from PageTasks and tasktbl.
						Added Delete Task with MODREGRPT rights in Tasktbl to 
						perform delete operation on CTR, SAR and SFISAR Reports.
*/


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Starting Conversion of Pcdb Database				      '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO

Set NOCOUNT ON
Go

USE PCDB
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WCDB_GetViewData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[WCDB_GetViewData]
GO

Create PROCEDURE WCDB_GetViewData
	(
	@ViewId   		int,
	@PageNo 		int,
	@RowsPerPage 		int,
	@AllowCheck 		int,
	@OperCode 		Code,
	@QueryPlusMode		int,
	@ArchiveMode		int

	)
  With Encryption As
 	  SET NOCOUNT ON

Declare @SelectClause 		nvarchar(4000)
Declare @FormattingSelectClause	nvarchar(4000)
Declare @Top			int

Declare @TableId		int
Declare @TableName		nvarchar(512)
Declare @ArchiveTableName	nvarchar(512)
Declare @ViewAlias		nvarchar(512)
Declare @FromClause    		nvarchar(4000)

Declare @WhereClause 		nvarchar(2000) 
Declare @RightsWhereClause 	nvarchar(2000)

Declare @OrderBy		nvarchar(512)
Declare @OrderByReverse 	nvarchar(512)

Declare @ViewQuery     		nvarchar(4000)
Declare @ViewCount     		nvarchar(2000)
Declare @PageQuery 		nvarchar(4000)
Declare @Database		nvarchar(255)

Declare @SelectedViewId		int
Declare @ObjectId               int
Declare @IsColumnsDefined	int
Declare @ViewCategory		int

set @ViewCategory =0

/* Get View Defintion */
select * from ViewTbl
where ViewId = @ViewId

select @IsColumnsDefined = count(*)  from ViewColumns
where ViewId=@ViewId and (OperCode='Prime' or OperCode= @opercode)


if @IsColumnsDefined=0
  begin
	--Find default view for the given object
	select @ObjectId=NavigationObjectId, @ViewCategory = CategoryId
	from ViewTbl
	where ViewId=@ViewId

	if @ViewCategory in (13,16,17)
	  begin
		--Public User Defined view
		set @SelectedViewId = @ViewId
	  end
	else
	  begin


		select @SelectedViewId = ViewId
		from ViewTbl
		where NavigationObjectId = @ObjectId and 
		      NavigationObjectId = BusinessObjectID and 
		      CategoryId=0 --public default
	  end

			
  end
else
  begin
	set @SelectedViewId = @ViewId
  end


--Get Database Name
select @TableId	= TableId
from ViewTbl
where ViewId= @SelectedViewId

select @Database=DatabaseName
from ListViewTables
where TableId= @TableId


if @QueryPlusMode Not In (6, 7)
  begin
	--Get  View Column Formating 	
	if exists (select GridPosition from ViewColumns 
	where ViewId=@SelectedViewId and OperCode=@OPerCode and CategoryId =2) --Category 2 - private
	  begin
		--Custom Columns layout
		select GridPosition, ColumnId, ViewColumns.TableId, FormatTypeId, GetTotal, GetAverage, PageName, DataType, PageWindowType
		from ViewColumns inner join ListViewColumns 
		on  ViewColumns.ColumnId= ListViewColumns.[Position] and ViewColumns.TableId= ListViewColumns.TableId 
		left outer join ColumnFormats
		on ViewColumns.TableId = ColumnFormats.TableId and
		ViewColumns.ColumnId = ColumnFormats.[Position]
		left Outer join ControlTbl
		on ViewColumns.ControlId= ControlTbl.ControlId
		left Outer Join PageTbl
		on ControlTbl.HLinkPageId = PageTbl.PageId
		
		where ViewId = @SelectedViewId  and 
		(@IsColumnsDefined > 0 and OperCode=@OperCode and CategoryId = 2 or 
		 @IsColumnsDefined = 0 and OperCode='Prime'   and CategoryId in (0,1,10))
		order by GridPosition --always show primary column (GridPosition=0)
	  end
	else if @SelectedViewId < 1000
	  begin
		--Public Column layout for Prime Defined Views
		select GridPosition, ColumnId, ViewColumns.TableId, FormatTypeId, GetTotal, GetAverage, PageName, DataType, PageWindowType
		from ViewColumns inner join ListViewColumns 
		on  ViewColumns.ColumnId= ListViewColumns.[Position]  and ViewColumns.TableId= ListViewColumns.TableId 
		 left outer join ColumnFormats
		on ViewColumns.TableId = ColumnFormats.TableId and
		ViewColumns.ColumnId = ColumnFormats.[Position]
		left Outer join ControlTbl
		on ViewColumns.ControlId= ControlTbl.ControlId
		left Outer Join PageTbl
		on ControlTbl.HLinkPageId = PageTbl.PageId

		where ViewId = @SelectedViewId and OperCode='Prime' and CategoryId in (0,1,10)
		order by GridPosition
	  end 
	else
	 begin
		--Column layout for User Defined Views
		select GridPosition, ColumnId, ViewColumns.TableId, FormatTypeId, GetTotal, GetAverage, PageName, DataType, PageWindowType
		from ViewColumns inner join ListViewColumns 
		on  ViewColumns.ColumnId= ListViewColumns.[Position]  and ViewColumns.TableId= ListViewColumns.TableId 
		 left outer join ColumnFormats
		on ViewColumns.TableId = ColumnFormats.TableId and
		ViewColumns.ColumnId = ColumnFormats.[Position]
		left Outer join ControlTbl
		on ViewColumns.ControlId= ControlTbl.ControlId
		left Outer Join PageTbl
		on ControlTbl.HLinkPageId = PageTbl.PageId

		where (ViewId = @SelectedViewId and OperCode=@OperCode and 
		      CategoryId in (0,1,3,6,7,10)) or --private views
		      (ViewId = @SelectedViewId and @ViewCategory in (13,16,17) and
		      CategoryId in (0,1,2,10,13,16,17)) and
			  ViewColumns.OperCode in (select OperCode
			  from ViewTbl where ViewId = @SelectedViewId) --public views
			  --Take column layout  defined by the 
			  --creator of the public default view.			

		order by GridPosition
	  end 

	----Get View Grouping
	select GridPosition,  GroupingStatusId, OrderStatus
	from ViewGroupingTbl
	where ViewId=@ViewId and OperCode=@OperCode
	order by  GroupingStatusId

end

----Build Select and from clauses
if @PageNo=0
  begin
	----Last Page Optimization 
	set @Top 	=  @RowsPerPage
	
  end
else
  begin
	set @Top = @PageNo * @RowsPerPage
  end

exec WCDB_BuildSQL @ViewId, @OperCode, @Top, @RowsPerPage, @ArchiveMode,
		   @SelectClause  Output, @FormattingSelectClause Output, 
		   @TableName Output, @ArchiveTableName Output, @FromClause Output




----Build Where Clause
if @QueryPlusMode = 5 --View All
  begin
	--Check if public parameters exist
	--View all
	if exists (select * from ViewParameters where ViewId=@ViewId and CategoryId in (0,1,3,6,7,13,16,17) )
		exec WCDB_QueryPlus @ViewId, @OperCode, 4,	 @WhereClause Output --4 - view filtered
	else
		set @WhereClause = ''
  end
  else
   begin
	if @QueryPlusMode = 7 --Scroll Checked
	  begin
		--Add Query Plus where clause
		exec WCDB_ScrollChecked @ViewId, @OperCode, @PageNo, @WhereClause Output
	  end
	else
	  begin
 		if @viewID=133 --Hidden OFAC Search Results View ID = 133
			--Add Query Plus where clause from the Main OFAC Search View ID = 100
			exec WCDB_QueryPlus 100, @OperCode, @QueryPlusMode,	 @WhereClause Output
		else
			--Add Query Plus where clause
			exec WCDB_QueryPlus @ViewId, @OperCode, @QueryPlusMode,	 @WhereClause Output

	  end	
   end
if Len(@WhereClause)>0
  begin
	if CHARINDEX('where', @WhereClause)=0
		set @WhereClause =  ' where (' + @WhereClause + ') '
	
  end

---Add Rights Where Clause
-- ignore Rights Where for all My Task Views

if not @ViewId in (31,32,33,34,35,36,37,38)
 begin
	exec WCDB_GetRightsWhereClause @SelectedViewId, @OperCode, @ArchiveMode, @RightsWhereClause Output

	if Len(@RightsWhereClause)> 0 
  	 begin
	   if Len(@WhereClause)>0
	    begin
		set @WhereClause = @WhereClause + ' and ( ' + @RightsWhereClause + ') '	  
	    end
	else
	  begin
	     set @WhereClause = ' where ' + @RightsWhereClause 
	  end
	end
end


if @ArchiveMode = 1 and Len(@WhereClause)>0
 begin
	--Replace Current Table by Archive Table
	--'.' delimiter used to protect the case when 
	--table name incapsulated in the column name 
	--like in Alert.AlertNo
	set @WhereClause =Replace(@WhereClause, 
	@TableName + '.', @ArchiveTableName + '.' )

	--Cover the case when table name inclosed into brackets
	--Replace Current [Table] by [Archive Table]	
	set @WhereClause =Replace(@WhereClause, 
	'[' + @TableName + '].', '[' + @ArchiveTableName + '].' )	   		

 end


----Order by clause to view Page

set @OrderBy = ''

exec WCDB_GetOrderClause @ViewId, @OperCode,@OrderBy  Output, @OrderByReverse Output


----Build View Query
if @ArchiveMode = 1
	set @ViewAlias = @ArchiveTableName
else
	set @ViewAlias = @TableName



if @QueryPlusMode=9
 begin

	Declare @SystemRef varchar(2000)
	Declare @vwOFACSearchResults varchar(2000)
	Declare @vwResultsWhere		 varchar(2000)
	Declare @vwOperCodeWhere	 varchar(2000)


	--Hidden OFAC Search Results (View ID = 133) uses System Reference 
	--from the Main OFAC Search Results (View ID = 100)
	if @viewid=133 
		select  @SystemRef = ParameterValue
		from ViewParameters
		where ViewId =100 and OperCode = @OperCode and ParameterId = -2
    else
		select  @SystemRef = ParameterValue
		from ViewParameters
		where ViewId =@ViewId and OperCode = @OperCode and ParameterId = -2
	

	if @ViewId in (130, 131)
	 begin
		set @vwOFACSearchResults = '(select Distinct EntNum, ' +
								   'OriginalSDNName, MatchName ' +
								   'from OFACSearchResults'
     end
	else
	 begin 
		set @vwOFACSearchResults = '(select Distinct EntNum ' +
								   'from OFACSearchResults'
	  end

	set @vwResultsWhere = ''


	

	if @SystemRef <> '' and @SystemRef is not null
     begin
		set @vwResultsWhere = 
			' where OFACSearchResults.SystemRef = ' + 
				'''' +  @SystemRef + ''''
     end

	--Do not use Oper code for 
	--All Parties Inquiry Log (view id 129) and
	--All Names Inquiry Log   (view id 131)
	if @ViewID not in (129, 131)
	  begin
			set @vwOperCodeWhere = 'OFACSearchResults.OperCode = ' +
								   '''' + @operCode + ''' '
			if (@vwResultsWhere != '')
				set @vwResultsWhere = @vwResultsWhere +
					' and ' +  @vwOperCodeWhere 
			else
				set @vwResultsWhere = ' where '	+  @vwOperCodeWhere 

       end

	set @vwOFACSearchResults = @vwOFACSearchResults + 
			@vwResultsWhere + ') As vwOFACSearchResults ' 

	if @ViewId in (130, 131)
	 begin
		--Search through OFAC SDNNamesView
		set @FromClause=@FromClause + 
		' inner join ' + @vwOFACSearchResults +
		'on (vwOFACSearchResults.MatchName = SDNNamesView.[Name] or ' +
		'vwOFACSearchResults.OriginalSDNName = SDNNamesView.[Name]) and ' +
		'vwOFACSearchResults.EntNum = SDNNamesView.EntNum and ' +
		'OFAC.dbo.OFS_IsListTypeIncluded(''' + @SystemRef  + ''', SDNNamesView.ListType, SDNNamesView.SDNType) !=0 ' 

		set @SelectClause=Replace(@SelectClause, 'select', 'select distinct')
		set @FormattingSelectClause=Replace(@FormattingSelectClause, 'select', 'select distinct')
		set @ViewCount  =  'select count(*) from ('  + @SelectClause + @FromClause + @WhereClause + ') As ViewQuery'
		set @ViewCount  = Replace(@ViewCount, ' top ' + Convert(varchar, @Top), ' ')

	 end
	else
	 begin
		set @FromClause=@FromClause + 
		' inner join ' + @vwOFACSearchResults +
		'on vwOFACSearchResults.EntNum = SDNTable.EntNum and ' +
		'OFAC.dbo.OFS_IsListTypeIncluded(''' + @SystemRef  + ''', SDNTable.ListType, SDNTable.SDNType) != 0 ' 

		if @ViewId = 129 
		 begin
			--Do not show deleted items 
			--for All Parties Inquiry Log (view id 129)
			set @FromClause=@FromClause + ' and SDNTable.Status <> 4 '
		 end

		set @ViewCount  =  'select count(*) ' + @FromClause   + @WhereClause

	  end

 end
else if CharIndex( 'Documentation', @FromClause) > 0 and
		CharIndex( 'Attachment', @FromClause) >0 
  begin
		--Obtain correct count for Documentation join with Attachments
		set @ViewCount  =  'select count(Distinct Documentation.RecNo) ' + 
							@FromClause   + @WhereClause
  end
else
 begin
	set @ViewCount  =  'select count(*) ' + @FromClause   + @WhereClause
 end


	if @PageNo=1 
	  begin
		/* Optimization for the first page  */
		set @ViewQuery =  @FormattingSelectClause + @FromClause  + @WhereClause  + @OrderBy
	  end
	else if @PageNo=0 
	  begin
		/* Optimization for the last page */		
		set @ViewQuery =  @FormattingSelectClause + @FromClause   +  @WhereClause + @OrderByReverse 		 
	  end
	else
	  begin

		Declare @OrderByColumnExpression nvarchar(2000)
		exec WCDB_GetOrderbyColumnExpression @ViewId, @OperCode, 
		@SelectClause,	@OrderBy, @OrderByColumnExpression OUTPUT


		if @ArchiveMode = 1
		  begin
			--If search the Archive Table Only (@ArchiveMode = 1)
			--replace Table Name with Archived Table name in the
			--order by clause with column expressions
			set @OrderByColumnExpression =Replace(@OrderByColumnExpression, 
			@TableName + '.' , @ArchiveTableName + '.')
		  end

		set @ViewQuery = @SelectClause + @FromClause  +  @WhereClause  + @OrderByColumnExpression
		

		if @ViewAlias='AccountOwner' or @ViewAlias='[AccountOwner]'
		  begin
			set @FormattingSelectClause=Replace(@FormattingSelectClause, 'Account.', 'AccountOwner.')
			set @FormattingSelectClause=Replace(@FormattingSelectClause, 'Customer.', 'AccountOwner.')
		  end

		set  @PageQuery =  @FormattingSelectClause +
		'from  (' + @ViewQuery +  ') As '  + @ViewAlias + ' '   + @OrderByReverse 

	  end

	



If @tableid in (112, 113) --Creates a temporary table for OFAC Cases (Current and Archived)
Begin

    CREATE TABLE #tmpListTypes (ListType VARCHAR(100))
	-- Gets the List types available for the operator
	INSERT INTO #tmpListTypes 
	SELECT * From OFAC.dbo.fn_GetListTypesForOper(@opercode)

End

---Debug Query Builder
--select @ViewQuery

if @PageNo=1 
  begin
	 ----Optimized for the First Page 
	
	--high volume exec [PRIME-DEV-DORIS].pbsa.dbo.sp_executesql @ViewQuery 
	
	
	--Debug Block
--	select @ViewQuery	
--	select @TableId
--	select @ViewQuery
--	select @Database
			
	--exec pbsa.dbo.sp_executesql @ViewQuery
	exec WCDB_ExecQuery @ViewQuery,	@Database

	-----Get Records Total			
	if @QueryPlusMode = 7 --Scroll Checked
	  begin
		select Count(RowId)
		from ViewCheckRows
		where ViewId=@ViewId and OperCode=@OperCode
	  end
	else
	  begin

		--select @viewcount
		--high volume exec [PRIME-DEV-DORIS].pbsa.dbo.sp_executesql  @ViewCount 
		--exec pbsa.dbo.sp_executesql  @ViewCount 
		exec WCDB_ExecQuery @ViewCount,	@Database
		
	  end


 end
else
begin
 	if @PageNo=0
	  begin		
		 --Optimized for the Last Page 
		
		--High volume exec [PRIME-DEV-DORIS].pbsa.dbo.sp_executesql @ViewQuery
		--exec pbsa.dbo.sp_executesql @ViewQuery
		exec WCDB_ExecQuery @ViewQuery,	@Database

	  end
	else
	  begin
		
		--High Volume exec [PRIME-DEV-DORIS].pbsa.dbo.sp_executesql @PageQuery
		 --exec pbsa.dbo.sp_executesql @PageQuery
		exec WCDB_ExecQuery @PageQuery,	@Database

		--select @PageQuery
	 end
	
	select 0 -- number of records queried only for first page

end
If @tableid in (112, 113) --Deletes the temporary table createed for OFAC Cases (Current and Archived)
Begin
	if object_id('tempdb..#tmpListTypes') is not null 
		DROP TABLE #tmpListTypes
End

--CD10/28/2010 New code to improve performance for SDN Parties
If @tableid in (136, 156, 152) --Deletes the temporary table created for SDNParties (Current and Archived)
BEGIN
	if object_id('tempdb..#SDNPartiesListTypes') is not null 
		DROP TABLE #SDNPartiesListTypes

END


if @AllowCheck = 0
  begin
	select  '', 0
  end
else
  begin
	select RowId, CheckStatus
	from ViewCheckRows
	where ViewId=@ViewId and OperCode=@OperCode
	order by RowId
  end


--Get View Actions
select TaskTbl.TaskId, TaskTbl.TaskName, TaskTbl.ToolTipText, 
       TaskTbl.HLinkPageId, TaskTbl.HLinkMethodId,  TaskTbl.PageMode,
       PageTbl.PageName, PageTbl.PageType,
       TaskTbl.Rights, TaskTbl.License, PageWindowType, ViewTasks.PageId
from TaskTbl inner join PageTbl 
     on TaskTbl.HLinkPageId = PageTbl.PageId 
     inner join ViewTasks
     on TaskTbl.TaskId=ViewTasks.TaskId
where ViewTasks.ViewId=@ViewId
order by TaskTbl.SortOrder
GO

if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[WCDB_GetRightsWhereClause]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure WCDB_GetRightsWhereClause
GO

/*------------------------------------------------------------------------------------------------*/

CREATE procedure WCDB_GetRightsWhereClause 
( 
		@ViewId int,
		@OperCode Code, 
		@ArchiveMode		int,
		@RightsWhereClause nvarchar(2000) Output
)

  With Encryption AS
	  SET NOCOUNT ON

	Declare @Rights 	  varchar(255)
	Declare @BusinessObjectId int
	Declare @TableId	  int
	Declare @ScopeColumn	  int



set @RightsWhereClause = ''

select @BusinessObjectId = BusinessObjectId, @TableId=TableId
from ViewTbl
where ViewId=@ViewId


if @BusinessObjectId is not null
  begin


	select @Rights = Rights
	from BusinessObjectTbl
	where ObjectId=@BusinessObjectId


	if @Rights is not null
	 begin

		if @TableID in (3,4,81, 100, 111, 112,113, 114, 122, 148, 303)
		  begin
			-- 3 - Activity table;   4 - ActivityHist table;  81 - Unprocessed Activity table
			-- 111 OFAC Exclude Names Table
			-- 112 OFAC FilterTranHist Table
			-- 113 OFAC FilterTranHist Table
			-- 148 OFAC ViolStat View
			-- 303 PSec Operator Table

			set @ScopeColumn = 2
		  end
		else
		  begin
			set @ScopeColumn = 1
		  end

		--debug block
 		--select @OperCode 
		--select @Rights
		--select @TableId
		--select @ScopeColumn


		exec CDB_GetSQLRightsStmt @OperCode, @Rights, @TableId, @ScopeColumn, @RightsWhereClause OUTPUT

		if @RightsWhereClause is null or Len(@RightsWhereClause)= 0
		  begin

			set @RightsWhereClause = ' 1=2 ' --no rights to view requested data
		  end

	  end
		If @tableId in (136, 156)   -- Sanction Parties
			SET @RightsWhereClause = @RightsWhereClause + ' and ListType in
		(
			SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(''' + @OperCode + ''', 135) 
			INTERSECT
			SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(''' + @opercode + ''', 100) 
		)'

   	if @tableid in(152)  -- Sanction Parties
			SET @RightsWhereClause = @RightsWhereClause + ' ListType in
		(
			SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(''' + @OperCode + ''', 135) 
			INTERSECT
			SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(''' + @opercode + ''', 100) 
		)'

	if @tableid = 117 --Sanction List Type
		SET @RightsWhereClause = @RightsWhereClause + ' and Code in
		(
			SELECT Distinct ListType from OFAC..OFS_fnListTypesForOperByView(''' + @OperCode + ''', 135) 
		)'

	if @tableid = 131	--Sanction Data Rule
		SET @RightsWhereClause = @RightsWhereClause + ' and Code in
		(
			SELECT Distinct SanctionDataRule from OFAC..OFS_fnGetSDataRulesForOperByView(''' + @OperCode + ''', 110)
		)'

	if @tableid = 103	--CIF Rules
		SET @RightsWhereClause = @RightsWhereClause + ' and Code in
		(
			SELECT Distinct CIFRule from OFAC..OFS_fnGetCIFRulesForOperByView(''' + @OperCode + ''', 111) 
		)'
		 + ' and SDRDataCode in
		(
			SELECT Distinct SanctionDataRule from OFAC..OFS_fnGetSDataRulesForOperByView(''' + @OperCode + ''', 110)
		)'

	if @tableid = 113 and @ArchiveMode = 0	--OFAC Case (Current)
		SET @RightsWhereClause = '(' + @RightsWhereClause + ') and 
			NOT Exists(select * from vw_GetFilterTranListTypes c Left Outer Join #tmpListTypes olt
						ON c.ListType = olt.ListType
				WHERE olt.ListType IS NULL and FilterTranTable.seqnumb = c.seqnumb)
		'

	if (@tableid = 112) or (@tableid = 113 and @ArchiveMode = 1)	--OFAC Case (Archive)
		SET @RightsWhereClause = '(' + @RightsWhereClause + ') and 
			NOT Exists(select * from FilterTranHistListTypes c Left Outer Join #tmpListTypes olt
						ON c.ListType = olt.ListType
				WHERE olt.ListType IS NULL and FilterTranHistTable.seqnumb = c.seqnumb)
'

		
	if @tableid = 113 and @ArchiveMode = 2	--OFAC Case (Current and Archived)
		SET @RightsWhereClause = '(' + @RightsWhereClause + ') and 
			NOT Exists(select * from vw_GetFilterTranListTypes c Left Outer Join #tmpListTypes olt
						ON c.ListType = olt.ListType
				WHERE olt.ListType IS NULL and FilterTranTable.seqnumb = c.seqnumb)
		and
			NOT Exists(select * from FilterTranHistListTypes c Left Outer Join #tmpListTypes olt
						ON c.ListType = olt.ListType
				WHERE olt.ListType IS NULL and FilterTranTable.seqnumb = c.seqnumb)
		'
  end
GO
BEGIN TRANSACTION ViewReportsTran

DECLARE @Hlinkpageid INT
DECLARE @Hlinkmethodid INT
DECLARE @pagemodeid INT
DECLARE @sarviewid INT
DECLARE @sfisarviewid INT
DECLARE @ctrviewid INT
DECLARE @taskid INT
DECLARE @allRepviewid INT


IF EXISTS (SELECT taskid FROM tasktbl WHERE TaskName='Add SFISAR')
BEGIN

DELETE pagetasks WHERE taskid = (SELECT taskid FROM tasktbl WHERE TaskName='Add SFISAR')
DELETE tasktbl WHERE TaskName='Add SFISAR'

END

IF NOT EXISTS (SELECT taskid FROM TaskTbl WHERE TaskName = 'Delete' AND rights = 'MODREGRPT')
BEGIN

SELECT @Hlinkpageid = pageid FROM pagetbl WHERE pagename = 'PDFFormNew.aspx'
SELECT @Hlinkmethodid = hlinkmethodid FROM hlinkmethodtbl WHERE method = 'GetPopup'
SELECT @pagemodeid = pagemode FROM pagemodetbl WHERE pagemode = 4 AND description = 'Delete mode'

INSERT INTO [Pcdb].[dbo].[TaskTbl]
           ([TaskId]
           ,[TaskName]
           ,[HLinkPageId]
           ,[HLinkMethodId]
           ,[PageMode]
           ,[Rights]
           ,[License]
           ,[ToolTipText]
           ,[SortOrder]
           ,[CategoryId])
     VALUES
           (1925
			,'Delete'
           ,@Hlinkpageid
           ,@Hlinkmethodid
           ,@pagemodeid
           ,'MODREGRPT'
           ,NULL
           ,'Delete Existing Report'
           ,0
           ,0)
SELECT @taskid = taskid FROM TaskTbl WHERE TaskName = 'Delete' AND rights = 'MODREGRPT'
IF NOT EXISTS (SELECT taskid FROM viewtasks WHERE Taskid = @taskid)
BEGIN

SELECT @sarviewid = viewid FROM viewtbl WHERE viewname = 'SAR Reports'
SELECT @ctrviewid = viewid FROM viewtbl WHERE viewname = 'CTR Reports'
SELECT @sfisarviewid = viewid FROM viewtbl WHERE viewname = 'SFISAR Reports'
SELECT @allRepviewid = viewid FROM viewtbl WHERE viewname = 'All Reports'

DELETE viewtasks WHERE viewid = @sfisarviewid

INSERT INTO [Pcdb].[dbo].[ViewTasks]
           ([ViewId]
           ,[TaskId]
           ,[PageId])
     VALUES
           (@sarviewid
           ,@taskid
           ,0)

INSERT INTO [Pcdb].[dbo].[ViewTasks]
           ([ViewId]
           ,[TaskId]
           ,[PageId])
     VALUES
           (@ctrviewid
           ,@taskid
           ,0)


INSERT INTO [Pcdb].[dbo].[ViewTasks]
           ([ViewId]
           ,[TaskId]
           ,[PageId])
     VALUES
           (@sfisarviewid
           ,@taskid
           ,0)

INSERT INTO [Pcdb].[dbo].[ViewTasks]
           ([ViewId]
           ,[TaskId]
           ,[PageId])
     VALUES
           (@allRepviewid
           ,@taskid
           ,0)

END
END
IF @@ERROR <> 0 
ROLLBACK TRANSACTION ViewReportsTran
ELSE
COMMIT TRANSACTION ViewReportsTran
Go

Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print '  Completed Conversion of Pcdb Database    '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

