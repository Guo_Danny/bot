/*
**  File Name:        ConvPBSA.sql
**
**  Functional Description:
**
**      This module contains SQL patches
**      for version 9.0.3
**
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2010                                                   ***
*** FIS						                                             ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|---------------------------------------------------- 
10/13/2010      SK		Fixed Issue with Unprocessed Activity where data in User1 - User5 field
						was not moving from UnprocActivity table to Activity table
*/


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Starting Conversion of PBSA Database				      '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO

Set NOCOUNT ON 
Go

use pbsa
go

if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[BSA_ImpUnprocActivity]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure BSA_ImpUnprocActivity
Go

Create Procedure [dbo].[BSA_ImpUnprocActivity] 
    (@TranNo INT,
	@oper VARCHAR(11),
	@retCount INT OUTPUT)
 
 With Encryption 
 As
  declare @insertCount INT,
    @delCount INT
  -- Start standard stored procedure transaction header
  declare @trnCnt int,
      @stat int,
      @objectId ObjectID
  select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_ImpUnprocActivity
  else
    -- Already in a transaction
	save tran BSA_ImpUnprocActivity
  -- End standard stored procedure transaction header

  SET @stat = -1
  -- make sure TranNo is not null and is non-zero
  SET @TranNo = ISNULL(@TranNo, 0)

  -- create temporary table
  if exists (select * from tempdb..sysobjects 
    where [name] like '#tmpUnprocActivity%' and type = 'U')
    drop table #tmpUnprocActivity
  create table #tmpUnprocActivity
    (TranNo INT NOT NULL,
	 Type varchar(20) NULL,
	 Branch varchar(11) NULL,
     Dept varchar(11) NULL)

  INSERT INTO #tmpUnprocActivity
  Select tranno, 
   CASE ua.type 
    WHEN Convert(Varchar, at.Type) THEN ua.type
	WHEN am.OrigActivity THEN Convert(Varchar, am.PrimeCode)
	ELSE NULL -- Error, should not happen
   END,
   CASE ua.branch 
    WHEN b.Code THEN ua.branch
	WHEN bm.OrigBrch THEN bm.PrimeCode
	ELSE NULL -- Error, should not happen
   END,
   CASE ua.dept 
    WHEN d.Code THEN ua.Dept
	WHEN dm.OrigDept THEN dm.PrimeCode
	ELSE NULL -- Error, should not happen
   END
  FROM UnprocActivity ua
  Left Join ActivityType at
   On ua.type = Convert(Varchar, at.type)
  Left Join Psec.dbo.Branch b
   On ua.branch = b.Code
  Left Join psec.dbo.Dept d
   On ua.dept = d.code
  Left Join ActivityMap am 
   On ua.type = am.OrigActivity And 
    (ua.TaskCode = am.Task Or (am.Task is Null and am.OrigActivity not in 
    (select origactivity from activitymap am2 
     where am2.origactivity=ua.type and am2.task = ua.taskcode)))
  Left Join BranchMap bm 
   On ua.branch = bm.OrigBrch And  
    (ua.TaskCode = bm.Task Or (bm.Task is Null and bm.OrigBrch not in 
    (select origbrch from branchmap bm2 
     where bm2.origbrch=ua.branch and bm2.task = ua.taskcode)))
  Left Join DeptMap dm 
   On ua.dept = dm.OrigDept And  
    (ua.TaskCode = dm.Task Or (dm.Task is Null and dm.OrigDept not in 
    (select origDept from deptmap dm2 
     where dm2.origDept=ua.Dept and dm2.task = ua.taskcode)))
  WHERE UA.ErrorStatus = 0
    and ((@TranNo = 0) OR (UA.TranNo = @TranNo))
  Order By tranno, at.type desc, b.Code desc, d.Code desc,
           am.Task desc, bm.Task desc, dm.Task desc



  -- import temporary table with TranNos of records
  -- Oper has rights to (branch and dept considered)
  Delete #tmpUnprocActivity where tranno not in
  (
  SELECT distinct UA.TranNo
  FROM #tmpUnprocActivity UA
  JOIN psec.dbo.operrights SR ON
  CASE SR.Scope -- for branch part of join
    when 1024 then isnull(SR.ObjBranch, '') -- skip branch
    when 256 then isnull(UA.Branch , '')
    when 64 then isnull(UA.Branch , '')
    when 4 then isnull(UA.Branch, '')
    else '*N/A*'
  END = isnull(SR.ObjBranch, '')
  AND
  CASE SR.Scope -- for dept part of join
    when 1024 then isnull(SR.ObjDept, '') -- skip dept
    when 256 then isnull(SR.ObjDept, '') -- skip dept
    when 64 then isnull(UA.Dept, '')
    when 4 then isnull(UA.Dept, '')
    else '*N/A*'
  END = isnull(SR.ObjDept, '')
  --AND -- UnprocAct does not have Oper
  --CASE SR.Scope -- for scope part of join
  --    when 1024 then isnull(SR.ObjOper, '') -- skip Oper
  --    when 256 then isnull(SR.ObjOper, '') -- skip Oper
  --    when 64 then isnull(SR.ObjOper, '') -- skip Oper
  --    when 4 then isnull(x.oper, '')
  --    else '*N/A*'
  --END = isnull(SR.ObjOper, '')
  WHERE SR.oper = @oper and SR.[right] = 'LOADBSAUPC'

    and ((@TranNo = 0) OR (UA.TranNo = @TranNo))
 )
  -- make sure there is something to do
  if not exists (select * from #tmpUnprocActivity)
  begin
    set @retCount = 0
	If @trnCnt = 0 BEGIN
   	commit tran BSA_ImpUnprocActivity
	END ELSE BEGIN
		rollback tran BSA_ImpUnprocActivity
	END

    return (0) -- nothing to do
  end
  
  -- insert activity record from unprocessed activity table
  insert into Activity (
	Cust, Account,
	Type, CashTran,
	BaseAmt, RecvPay,
	Curr, FxAmt,
	Ref, ValueDate,
	BookDate, BeneCustId,
	Bene, BeneAcct,
	BeneAddress, BeneCity,
	BeneState, BeneZip,
	BeneCountry, BeneBankCustId,
	BeneBank, BeneBankId,
	BeneBankAcct, BeneBankAddress,
	BeneBankCity, BeneBankState,
	BeneBankZip, BeneBankCountry,
	IntermediaryCustId, Intermediary,
	IntermediaryId, IntermediaryAcct,
	IntermediaryAddress, IntermediaryCity,
	IntermediaryState, IntermediaryZip,
	IntermediaryCountry, 
	IntermediaryCustId2, Intermediary2,
	IntermediaryId2, IntermediaryAcct2,
	IntermediaryAddress2, IntermediaryCity2,
	IntermediaryState2, IntermediaryZip2,
	IntermediaryCountry2, 	
	IntermediaryCustId3, Intermediary3,
	IntermediaryId3, IntermediaryAcct3,
	IntermediaryAddress3, IntermediaryCity3,
	IntermediaryState3, IntermediaryZip3,
	IntermediaryCountry3, 	
	IntermediaryCustId4, Intermediary4,
	IntermediaryId4, IntermediaryAcct4,
	IntermediaryAddress4, IntermediaryCity4,
	IntermediaryState4, IntermediaryZip4,
	IntermediaryCountry4, 
	User1, User2, User3,
	User4, User5,	
	ByOrderCustId,
	ByOrder, ByOrderAcct,
	ByOrderAddress, ByOrderCity,
	ByOrderState, ByOrderZip,
	ByOrderCountry, ByOrderBankCustId,
	ByOrderBank, ByOrderBankId,
	ByOrderBankAcct, ByOrderBankAddress,
	ByOrderBankCity, ByOrderBankState,
	ByOrderBankZip, ByOrderBankCountry,
	Instructions, Notes, Branch,
	Dept,
	ExemptionStatus, PaymtMethod, InstrumentType, InstrumentName, 
	InstrumentQty, InstrumentPrice, RelatedRef, ReferenceType)
  select 
	Cust, Account,
	CAST(t.Type AS INT), CashTran,
	BaseAmt, RecvPay,
	Curr, FxAmt,
	Ref, ValueDate,
	BookDate, BeneCustId,
	Bene, BeneAcct,
	BeneAddress, BeneCity,
	BeneState, BeneZip,
	BeneCountry, BeneBankCustId,
	BeneBank, BeneBankId,
	BeneBankAcct, BeneBankAddress,
	BeneBankCity, BeneBankState,
	BeneBankZip, BeneBankCountry,
	IntermediaryCustId, Intermediary,
	IntermediaryId, IntermediaryAcct,
	IntermediaryAddress, IntermediaryCity,
	IntermediaryState, IntermediaryZip,
	IntermediaryCountry, 
	IntermediaryCustId2, Intermediary2,
	IntermediaryId2, IntermediaryAcct2,
	IntermediaryAddress2, IntermediaryCity2,
	IntermediaryState2, IntermediaryZip2,
	IntermediaryCountry2, 	
	IntermediaryCustId3, Intermediary3,
	IntermediaryId3, IntermediaryAcct3,
	IntermediaryAddress3, IntermediaryCity3,
	IntermediaryState3, IntermediaryZip3,
	IntermediaryCountry3, 	
	IntermediaryCustId4, Intermediary4,
	IntermediaryId4, IntermediaryAcct4,
	IntermediaryAddress4, IntermediaryCity4,
	IntermediaryState4, IntermediaryZip4,
	IntermediaryCountry4, 
	User1, User2, User3,
	User4, User5,	
	ByOrderCustId,
	ByOrder, ByOrderAcct,
	ByOrderAddress, ByOrderCity,
	ByOrderState, ByOrderZip,
	ByOrderCountry, ByOrderBankCustId,
	ByOrderBank, ByOrderBankId,
	ByOrderBankAcct, ByOrderBankAddress,
	ByOrderBankCity, ByOrderBankState,
	ByOrderBankZip, ByOrderBankCountry,
	Instructions, Notes, t.Branch,
	t.Dept,
	ExemptionStatus, PaymtMethod, InstrumentType,
	InstrumentName, CAST (InstrumentQty as float),
	Cast(InstrumentPrice as money), RelatedRef, ReferenceType
  from UnprocActivity UA
    join #tmpUnprocActivity T
      on T.TranNo = UA.TranNo 

  Select @stat = @@error, @insertCount = @@rowcount

  -- Evaluate results of the transaction
  If @stat <> 0 begin
	rollback tran BSA_ImpUnprocActivity
	return @stat
  end

  -- delete the unprocessed activity that was imported
  delete from UnprocActivity
    from UnprocActivity UA, #tmpUnprocActivity UTN
    where UA.TranNo = UTN.TranNo 

  Select @stat = @@error, @delCount = @@rowcount

  -- Evaluate results of the transaction
  If @stat <> 0 begin
	rollback tran BSA_ImpUnprocActivity
	return @stat
  end

  -- get rid of temp table
  drop table #tmpUnprocActivity

  -- make sure insert and delete counts are equal
  if (@insertCount <> @delCount)
  begin
	rollback tran BSA_ImpUnprocActivity
	return -2 -- insert and delete counts not equal
  end

  if (@TranNo = 0)
      select @objectId = convert(varchar, @InsertCount) + ' records'
  else
      select @objectId = convert(varchar, @TranNo)
  Exec BSA_InsEvent @oper, 'Imp', 'UnActivity', @objectId, 
      'Unprocessed Activity record(s) imported into Activity table'

  Select @stat = @@error
  If @stat <> 0 begin
	rollback tran BSA_ImpUnprocActivity
	return @stat
  end

  set @retCount = @insertCount

  If @trnCnt = 0
    commit tran BSA_ImpUnprocActivity

  return @stat
Go

Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Completed Conversion of PBSA Database				  '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go