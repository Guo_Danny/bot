/*
**  File Name:        ConvPsec.sql
**
**  Functional Description:
**
**      This module contains SQL patches
**      for version 9.0.3
**
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2010                                                   ***
*** FIS						                                             ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|---------------------------------------------------- 
11/11/2010      VK		Procedure GetBranchDeptForOperByViewId got dropped.
						Existing function fnGetBranchDeptForOperByViewId used instead.	
						
11/11/2010		VK		Functions SEC_fnListTypesForOperByView, SEC_fnGetSDataRulesForOperByView,
						SEC_fnGetSDataRulesForOperByView got dropped. 
						Functions OFS_fnListTypesForOperByView,OFS_fnGetSDataRulesForOperByView and
						OFS_fnGetCIFRulesForOperByView used instead.
*/

USE PSEC
GO

IF OBJECT_ID (N'dbo.GetBranchDeptForOperByViewId') IS NOT NULL
   DROP PROCEDURE dbo.GetBranchDeptForOperByViewId
GO

IF object_id(N'SEC_fnListTypesForOperByView') IS NOT NULL
      DROP FUNCTION SEC_fnListTypesForOperByView

GO

IF object_id(N'SEC_fnGetSDataRulesForOperByView') IS NOT NULL
      DROP FUNCTION SEC_fnGetSDataRulesForOperByView

GO

IF object_id(N'SEC_fnGetCIFRulesForOperByView') IS NOT NULL
      DROP FUNCTION SEC_fnGetCIFRulesForOperByView

GO

Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print '  Completed Conversion of Psec Database    '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go