/*
**  File Name:        ConvPBSA.sql
**
**  Functional Description:
**
**      This module contains SQL patches
**      for version 9.0.3
**
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2010                                                   ***
*** FIS						                                             ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|---------------------------------------------------- 
 12/07/2010				 Fixed issue with User 1 - User5 fields not being 
						 imported to Activity table from Unprocessed Activity. 
 09/21/2011		CD		 Merge coverage period code changes done for 
						 R100275312 to create new stored procedures.
10/04/2012		SS		Modified the Stored Procedure "BSA_DelRegReport" to include
						Delete Reason in EvtDetails column of Event Table for Compliance Project.
10/09/2012		JP		Modified the stored procedure "BSA_GetCTRConductors" to include Sex and Email
						if case is created for beneficiary.
10/16/2012		JP		Modified the stored procedure "BSA_GetCTRConductors" to include AccNum in Activity table						
10/16/2012		JP		Modified the stored procedure for Filtering Old files for efiling				
12/11/2012		JP		Modified the stored procedure BSA_GetCTRConductors to display Account in new CTR 2012 pdf
02/25/2013		JP		R130380497: Modified the stored procedure BSA_GetCTRConductors for Beneficiary customers in new CTR 2012 pdf
04/03/2013		JP		R130380497: Added new proc BSA_ActivityBranchesForCase for fetching Branch information for a case to display in Part III section of SAR CTR pdf.		
04/22/2013		JP		R130380497: Fetching Branch information for all activities in a case created using Rules.
12/14/2015      SS      Swift 2015 Supported Update - Adding BSA_CountryNameFromIsoCode SP
06/24/2016		AS		I160039680: The branch in the header is being filled with incorrect information. 
						Changed the stored procedure BSA_ActivityBranchesForCase and BSA_ActivityBranchForCase

*/

Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Starting Conversion of PBSA Database				      '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO

use pbsa
go

if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[BSA_ImpUnprocActivity]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure BSA_ImpUnprocActivity
Go

Create Procedure [dbo].[BSA_ImpUnprocActivity] 
    (@TranNo INT,
	@oper VARCHAR(11),
	@retCount INT OUTPUT)
 
 With Encryption 
 As
  declare @insertCount INT,
    @delCount INT
  -- Start standard stored procedure transaction header
  declare @trnCnt int,
      @stat int,
      @objectId ObjectID
  select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_ImpUnprocActivity
  else
    -- Already in a transaction
	save tran BSA_ImpUnprocActivity
  -- End standard stored procedure transaction header

  SET @stat = -1
  -- make sure TranNo is not null and is non-zero
  SET @TranNo = ISNULL(@TranNo, 0)

  -- create temporary table
  if exists (select * from tempdb..sysobjects 
    where [name] like '#tmpUnprocActivity%' and type = 'U')
    drop table #tmpUnprocActivity
  create table #tmpUnprocActivity
    (TranNo INT NOT NULL,
	 Type varchar(20) NULL,
	 Branch varchar(11) NULL,
     Dept varchar(11) NULL)

  INSERT INTO #tmpUnprocActivity
  Select tranno, 
   CASE ua.type 
    WHEN Convert(Varchar, at.Type) THEN ua.type
	WHEN am.OrigActivity THEN Convert(Varchar, am.PrimeCode)
	ELSE NULL -- Error, should not happen
   END,
   CASE ua.branch 
    WHEN b.Code THEN ua.branch
	WHEN bm.OrigBrch THEN bm.PrimeCode
	ELSE NULL -- Error, should not happen
   END,
   CASE ua.dept 
    WHEN d.Code THEN ua.Dept
	WHEN dm.OrigDept THEN dm.PrimeCode
	ELSE NULL -- Error, should not happen
   END
  FROM UnprocActivity ua
  Left Join ActivityType at
   On ua.type = Convert(Varchar, at.type)
  Left Join Psec.dbo.Branch b
   On ua.branch = b.Code
  Left Join psec.dbo.Dept d
   On ua.dept = d.code
  Left Join ActivityMap am 
   On ua.type = am.OrigActivity And 
    (ua.TaskCode = am.Task Or (am.Task is Null and am.OrigActivity not in 
    (select origactivity from activitymap am2 
     where am2.origactivity=ua.type and am2.task = ua.taskcode)))
  Left Join BranchMap bm 
   On ua.branch = bm.OrigBrch And  
    (ua.TaskCode = bm.Task Or (bm.Task is Null and bm.OrigBrch not in 
    (select origbrch from branchmap bm2 
     where bm2.origbrch=ua.branch and bm2.task = ua.taskcode)))
  Left Join DeptMap dm 
   On ua.dept = dm.OrigDept And  
    (ua.TaskCode = dm.Task Or (dm.Task is Null and dm.OrigDept not in 
    (select origDept from deptmap dm2 
     where dm2.origDept=ua.Dept and dm2.task = ua.taskcode)))
  WHERE UA.ErrorStatus = 0
    and ((@TranNo = 0) OR (UA.TranNo = @TranNo))
  Order By tranno, at.type desc, b.Code desc, d.Code desc,
           am.Task desc, bm.Task desc, dm.Task desc



  -- import temporary table with TranNos of records
  -- Oper has rights to (branch and dept considered)
  Delete #tmpUnprocActivity where tranno not in
  (
  SELECT distinct UA.TranNo
  FROM #tmpUnprocActivity UA
  JOIN psec.dbo.operrights SR ON
  CASE SR.Scope -- for branch part of join
    when 1024 then isnull(SR.ObjBranch, '') -- skip branch
    when 256 then isnull(UA.Branch , '')
    when 64 then isnull(UA.Branch , '')
    when 4 then isnull(UA.Branch, '')
    else '*N/A*'
  END = isnull(SR.ObjBranch, '')
  AND
  CASE SR.Scope -- for dept part of join
    when 1024 then isnull(SR.ObjDept, '') -- skip dept
    when 256 then isnull(SR.ObjDept, '') -- skip dept
    when 64 then isnull(UA.Dept, '')
    when 4 then isnull(UA.Dept, '')
    else '*N/A*'
  END = isnull(SR.ObjDept, '')
  --AND -- UnprocAct does not have Oper
  --CASE SR.Scope -- for scope part of join
  --    when 1024 then isnull(SR.ObjOper, '') -- skip Oper
  --    when 256 then isnull(SR.ObjOper, '') -- skip Oper
  --    when 64 then isnull(SR.ObjOper, '') -- skip Oper
  --    when 4 then isnull(x.oper, '')
  --    else '*N/A*'
  --END = isnull(SR.ObjOper, '')
  WHERE SR.oper = @oper and SR.[right] = 'LOADBSAUPC'

    and ((@TranNo = 0) OR (UA.TranNo = @TranNo))
 )
  -- make sure there is something to do
  if not exists (select * from #tmpUnprocActivity)
  begin
    set @retCount = 0
	If @trnCnt = 0 BEGIN
   	commit tran BSA_ImpUnprocActivity
	END ELSE BEGIN
		rollback tran BSA_ImpUnprocActivity
	END

    return (0) -- nothing to do
  end
  
  -- insert activity record from unprocessed activity table
  insert into Activity (
	Cust, Account,
	Type, CashTran,
	BaseAmt, RecvPay,
	Curr, FxAmt,
	Ref, ValueDate,
	BookDate, BeneCustId,
	Bene, BeneAcct,
	BeneAddress, BeneCity,
	BeneState, BeneZip,
	BeneCountry, BeneBankCustId,
	BeneBank, BeneBankId,
	BeneBankAcct, BeneBankAddress,
	BeneBankCity, BeneBankState,
	BeneBankZip, BeneBankCountry,
	IntermediaryCustId, Intermediary,
	IntermediaryId, IntermediaryAcct,
	IntermediaryAddress, IntermediaryCity,
	IntermediaryState, IntermediaryZip,
	IntermediaryCountry, 
	IntermediaryCustId2, Intermediary2,
	IntermediaryId2, IntermediaryAcct2,
	IntermediaryAddress2, IntermediaryCity2,
	IntermediaryState2, IntermediaryZip2,
	IntermediaryCountry2, 	
	IntermediaryCustId3, Intermediary3,
	IntermediaryId3, IntermediaryAcct3,
	IntermediaryAddress3, IntermediaryCity3,
	IntermediaryState3, IntermediaryZip3,
	IntermediaryCountry3, 	
	IntermediaryCustId4, Intermediary4,
	IntermediaryId4, IntermediaryAcct4,
	IntermediaryAddress4, IntermediaryCity4,
	IntermediaryState4, IntermediaryZip4,
	IntermediaryCountry4, 
	User1, User2, User3,
	User4, User5,	
	ByOrderCustId,
	ByOrder, ByOrderAcct,
	ByOrderAddress, ByOrderCity,
	ByOrderState, ByOrderZip,
	ByOrderCountry, ByOrderBankCustId,
	ByOrderBank, ByOrderBankId,
	ByOrderBankAcct, ByOrderBankAddress,
	ByOrderBankCity, ByOrderBankState,
	ByOrderBankZip, ByOrderBankCountry,
	Instructions, Notes, Branch,
	Dept,
	ExemptionStatus, PaymtMethod, InstrumentType, InstrumentName, 
	InstrumentQty, InstrumentPrice, RelatedRef, ReferenceType)
  select 
	Cust, Account,
	CAST(t.Type AS INT), CashTran,
	BaseAmt, RecvPay,
	Curr, FxAmt,
	Ref, ValueDate,
	BookDate, BeneCustId,
	Bene, BeneAcct,
	BeneAddress, BeneCity,
	BeneState, BeneZip,
	BeneCountry, BeneBankCustId,
	BeneBank, BeneBankId,
	BeneBankAcct, BeneBankAddress,
	BeneBankCity, BeneBankState,
	BeneBankZip, BeneBankCountry,
	IntermediaryCustId, Intermediary,
	IntermediaryId, IntermediaryAcct,
	IntermediaryAddress, IntermediaryCity,
	IntermediaryState, IntermediaryZip,
	IntermediaryCountry, 
	IntermediaryCustId2, Intermediary2,
	IntermediaryId2, IntermediaryAcct2,
	IntermediaryAddress2, IntermediaryCity2,
	IntermediaryState2, IntermediaryZip2,
	IntermediaryCountry2, 	
	IntermediaryCustId3, Intermediary3,
	IntermediaryId3, IntermediaryAcct3,
	IntermediaryAddress3, IntermediaryCity3,
	IntermediaryState3, IntermediaryZip3,
	IntermediaryCountry3, 	
	IntermediaryCustId4, Intermediary4,
	IntermediaryId4, IntermediaryAcct4,
	IntermediaryAddress4, IntermediaryCity4,
	IntermediaryState4, IntermediaryZip4,
	IntermediaryCountry4, 
	User1, User2, User3,
	User4, User5,	
	ByOrderCustId,
	ByOrder, ByOrderAcct,
	ByOrderAddress, ByOrderCity,
	ByOrderState, ByOrderZip,
	ByOrderCountry, ByOrderBankCustId,
	ByOrderBank, ByOrderBankId,
	ByOrderBankAcct, ByOrderBankAddress,
	ByOrderBankCity, ByOrderBankState,
	ByOrderBankZip, ByOrderBankCountry,
	Instructions, Notes, t.Branch,
	t.Dept,
	ExemptionStatus, PaymtMethod, InstrumentType,
	InstrumentName, CAST (InstrumentQty as float),
	Cast(InstrumentPrice as money), RelatedRef, ReferenceType
  from UnprocActivity UA
    join #tmpUnprocActivity T
      on T.TranNo = UA.TranNo 

  Select @stat = @@error, @insertCount = @@rowcount

  -- Evaluate results of the transaction
  If @stat <> 0 begin
	rollback tran BSA_ImpUnprocActivity
	return @stat
  end

  -- delete the unprocessed activity that was imported
  delete from UnprocActivity
    from UnprocActivity UA, #tmpUnprocActivity UTN
    where UA.TranNo = UTN.TranNo 

  Select @stat = @@error, @delCount = @@rowcount

  -- Evaluate results of the transaction
  If @stat <> 0 begin
	rollback tran BSA_ImpUnprocActivity
	return @stat
  end

  -- get rid of temp table
  drop table #tmpUnprocActivity

  -- make sure insert and delete counts are equal
  if (@insertCount <> @delCount)
  begin
	rollback tran BSA_ImpUnprocActivity
	return -2 -- insert and delete counts not equal
  end

  if (@TranNo = 0)
      select @objectId = convert(varchar, @InsertCount) + ' records'
  else
      select @objectId = convert(varchar, @TranNo)
  Exec BSA_InsEvent @oper, 'Imp', 'UnActivity', @objectId, 
      'Unprocessed Activity record(s) imported into Activity table'

  Select @stat = @@error
  If @stat <> 0 begin
	rollback tran BSA_ImpUnprocActivity
	return @stat
  end

  set @retCount = @insertCount

  If @trnCnt = 0
    commit tran BSA_ImpUnprocActivity

  return @stat
Go

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'BSA_GetSARCoveragePeriodForEFiling')
    DROP PROCEDURE [DBO].[BSA_GetSARCoveragePeriodForEFiling]
GO
Create Procedure [dbo].[BSA_GetSARCoveragePeriodForEFiling] 
With Encryption As
          
          select Min(BookDate), Max(BookDate)
          from CurrentArchActivity
          where TranNo in
                   (select TranNo
                   from regreport r (nolock) join psec.dbo.branch b on r.OwnerBranch = b.Code 
                   inner join SasActivity on convert(varchar, SaRecNo) = ObjectId and ObjectType = 'SuspAct'  
                   WHERE b.branchnumber is not null AND 
                   [type] in ('SAR', 'SFISAR') AND 1 IN 
                   (select [enabled] from optiontbl where Code = 'SARElect')  AND
                   IsFinalized = 1 and EFileFormat = 0 and RptFiled = 0)
 
Go

------------------


-- Create procedure to get CTR coverage period min and max Book dates

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'BSA_GetCTRCoveragePeriodForEFiling')
    DROP PROCEDURE [DBO].[BSA_GetCTRCoveragePeriodForEFiling]
GO
Create PROCEDURE BSA_GetCTRCoveragePeriodForEFiling 
With Encryption As
 
          select Min(BookDate), Max(BookDate)
          from SuspiciousActivity 
          where RecNo in
                   (select SARecNo
                   from regreport r (nolock) join psec.dbo.branch b on r.OwnerBranch = b.Code 
                   WHERE b.branchnumber is not null AND 
                   [type] = 'CTR' AND 1 IN
                   (select [enabled] from optiontbl where Code = 'CTRElect') AND
                   IsFinalized = 1 and EFileFormat = 0 and RptFiled = 0)
 
Go
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'BSA_DelRegReport')
    DROP PROCEDURE [DBO].[BSA_DelRegReport]
GO

Create Procedure dbo.BSA_DelRegReport ( @repNo SeqNo1, @SaRecNo SeqNo1, 
                                @oper SCode, @ts timestamp, @evtDetail varchar(max) )
 With Encryption As
 
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_DelRegReport
  else
    -- Already in a transaction
	save tran BSA_DelRegReport
  -- End standard stored procedure transaction header
  
  declare	@name 		SCode,
			@stat		int,
			@txt        varchar(50),
			@cnt        int 
  declare   @saTs       timestamp,
            @SuspType   SCode
            
  If Exists ( Select * From RegReport Where RepNo = @repNo ) begin
	Select @name = Type from RegReport Where RepNo = @repNo
    Delete RegReport Where RepNo = @repNo and Ts = @ts
	Select @stat = @@error , @cnt = @@rowcount 
  end else 
	Select @stat = 250006	-- Object does not exist
  If ( @stat = 0 and @cnt = 0 )
		select @stat = 250001 -- concurent update
  If ( @stat <> 0 ) begin 
		rollback tran BSA_DelRegReport
		return @stat
  end 
  
  Select @txt = '''' + @name +''''
  --Exec BSA_InsEvent @oper, 'Del', 'RegRep',@repNo, @name, @evtDetail
  Insert into Event(Oper, Type, ObjectType, ObjectId , LogText, EvtDetail) values(@oper, 'Del', 'RegRep',@repNo, @name, @evtDetail)
  Select @txt = 'Deleted Regulatory Report '+ @name  + '''' + 
		   cast(@repNo as varchar) + ''''  
  Exec BSA_InsEvent @oper, 'Mod', 'SuspAct' , @saRecNo , @txt

  If @trnCnt = 0
		commit tran BSA_DelRegReport
  return @stat
Go

-- Get Customers, Conductors and Transaction Summary
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'BSA_GetCTRConductors')
    DROP PROCEDURE [DBO].[BSA_GetCTRConductors]
GO
Create PROCEDURE dbo.BSA_GetCTRConductors 
(
	@caseId	SeqNo1
)
With Encryption AS
----------------------------------------------------
-- Returns conductor information from activities 
-- related to the case.

-- RecvPay 2 = Pay
--		   1 = Receive
----------------------------------------------------

	DECLARE @activity int
	DECLARE @caseType SCode
    DECLARE @benectr bit
    DECLARE @paytran int

    -- Table to hold activities that created the case
    DECLARE @Activities Table (Cust varchar(35),TranNo int, BaseAmt money,
							  Curr char(3),FxAmt money,RecvPay int, CashAcc varchar(35))
    -- Table to hold transactor id's
    DECLARE @TransActors Table (ord int identity primary key,CustID varchar(35))

    -- Table to hold customer id's
    DECLARE @Customers Table (ord int identity primary key,CustID varchar(35))
	
    --Get Case Record
  If Exists (Select * from SuspiciousActivity where RecNo=@CaseiD )
	SELECT @activity = activity, @caseType = SuspType ,
	@benectr = case when Descr like '%This case was created with beneficiary activity.%' Then 1 Else 0 end,
    @Paytran=CASE When ActInActCnt>0 Then 1 Else 2 End
	FROM SuspiciousActivity 	
	WHERE recno = @caseid
  ELSE 
    SELECT @activity = activity, @caseType = SuspType ,
	@benectr = case when Descr like '%This case was created with beneficiary activity.%' Then 1 Else 0 end,
    @Paytran=CASE When ActInActCnt>0 Then 1 Else 2 End
	FROM SuspiciousActivityHist 	
	WHERE recno = @caseid
	 
    --debug
    --select @benectr
 
        ----------------------------------------------------------------------
        -- These are the only cases we need at this moment
        ----------------------------------------------------------------------
		If @caseType = 'ExcCurrency' or @caseType='Manual' or @caseType='Rule'
			Begin
				-- Get Activities that created the case
              If   @caseType = 'ExcCurrency' 
				Begin

                    If @benectr = 0
                    Begin 
						Insert Into @Activities --For exeed curr cases activities have to be searched.
							Select ah.cust,ah.TranNo,ah.BaseAmt,ah.Curr,ah.FxAmt, ah.RecvPay, ah.Account 
							From SuspiciousActivity sa  Join activityhist ah 
							on ah.Cust = sa.Cust AND ah.BookDate = sa.BookDate 
							and ah.CashTran = 1
							Where sa.recno = @caseId and (@paytran=ah.RecvPay or @paytran=0)
								UNION
							Select a.cust,a.TranNo,a.BaseAmt,a.Curr,a.FxAmt, a.RecvPay, a.Account
							From SuspiciousActivity sa  Join activity a 
							on a.Cust = sa.Cust AND a.BookDate = sa.BookDate 
							AND a.CashTran = 1
							Where sa.recno = @caseId and (@paytran=a.RecvPay or @paytran=0)
                   End
                   Else
                   Begin
                            Insert Into @Activities
							Select Case when ah.BeneCustId is null Then ah.Cust Else ah.BeneCustId End,
							ah.TranNo,ah.BaseAmt,ah.Curr,ah.FxAmt, ah.RecvPay, ah.Account
							From SuspiciousActivity sa  Join activityhist ah 
							on case when ah.BeneCustId is null Then ah.Cust Else ah.BeneCustId End = sa.Cust AND ah.BookDate = sa.BookDate 
							and ah.CashTran = 1
							Where sa.recno = @caseId and (@paytran=ah.RecvPay or @paytran=0)
								UNION
							Select Case when a.BeneCustId is null Then a.Cust Else a.BeneCustId End,
							a.TranNo,a.BaseAmt,a.Curr,a.FxAmt, a.RecvPay, a.Account
							From SuspiciousActivity sa  Join activity a 
							on case when a.BeneCustId is null Then a.Cust Else a.BeneCustId End  = sa.Cust AND a.BookDate = sa.BookDate 
							AND a.CashTran = 1
							Where sa.recno = @caseId  and (@paytran=A.RecvPay or @paytran=0)
                   end

				End
              Else
				Begin
				-- Manual Case
                   Insert Into @Activities 
                   Select ah.cust,ah.TranNo,ah.BaseAmt,ah.Curr,ah.FxAmt, ah.RecvPay, ah.Account
				   From activityhist ah Join dbo.SAsActivity sas
                   on ah.TranNo=sas.TranNo --and ah.CashTran = 1
				   Where sas.ObjectType in ('SuspActHst','SuspAct') and sas.ObjectID=@caseId and sas.Included=1
						UNION
                   Select a.cust,a.TranNo,a.BaseAmt,a.Curr,a.FxAmt, a.RecvPay, a.Account
				   From activity a Join dbo.SAsActivity sas
                   on a.TranNo=sas.TranNo --and a.CashTran = 1
				   Where sas.ObjectType in ('SuspActHst','SuspAct') and sas.ObjectID=@caseId and sas.Included=1
                End

				-- Get Transactors
				Insert Into @TransActors (CustID)
                -- From Activity
				Select distinct ByOrderCustID AS CustID from dbo.Activity where TranNo IN (Select TranNo From @Activities) and ByOrderCustID is not null
				UNION all Select distinct IntermediaryCustId As CustID from dbo.Activity where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId is not null
				UNION all Select distinct IntermediaryCustId3 As CustID from dbo.Activity where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId3 is not null
                -- From Activity History
                UNION all Select distinct ByOrderCustID AS CustID from dbo.ActivityHist where TranNo IN (Select TranNo From @Activities) and ByOrderCustID is not null
				UNION all Select distinct IntermediaryCustId As CustID from dbo.ActivityHist where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId is not null
				UNION all Select distinct IntermediaryCustId3 As CustID from dbo.ActivityHist where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId3 is not null

                --Get Customers
                If @benectr =1 
                Begin
                    
					Insert Into @Customers (CustID)
                    -- From Case
                    Select Cust as CustID from SuspiciousActivity where SuspiciousActivity.recno=@caseID
                    Union Select Cust as CustID from SuspiciousActivityHist where SuspiciousActivityHist.recno=@caseID
                    UNION
					-- From Activity
					Select distinct BeneBankCustId AS CustID from dbo.Activity where TranNo IN (Select TranNo From @Activities) and BeneBankCustId is not null
					UNION all Select distinct IntermediaryCustId2 As CustID from dbo.Activity where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId2 is not null
					UNION all Select distinct IntermediaryCustId4 As CustID from dbo.Activity where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId4 is not null
					-- From Activity History
					UNION all Select distinct BeneBankCustId AS CustID from dbo.ActivityHist where TranNo IN (Select TranNo From @Activities) and BeneBankCustId is not null
					UNION all Select distinct IntermediaryCustId2 As CustID from dbo.ActivityHist where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId2 is not null
					UNION all Select distinct IntermediaryCustId4 As CustID from dbo.ActivityHist where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId4 is not null
                End
 
                  
            End

     -- Return Transactor information

   -- From customer table
   Select top 23 'Transactors' as TableID,Sum.* From (
     Select ID,Name,Address,TIN,City,State,Zip,
     Country,DOB,LicenseNo,LicenseState,PassportNo,DBA,TypeOfBusiness,Telephone,Sex,Email
	 From Customer
     join @TransActors tr on customer.id=tr.custid
     Union all
     -- From activity ByOrderCustID if the id is missing
     Select '0' as ID, IsNull(ByOrder,'') as Name,Isnull(ByOrderAddress,'') as Address,
     '' as TIN, IsNull(ByOrderCity,'') as City,Isnull(ByOrderState,'') as State,
     IsNull(ByOrderZip,'') as Zip, IsNull(ByOrderCountry,'') as Country, Null as DOB,
     '' AS LicenseNo, '' as LicenseState, '' as PaasportNo, '' AS DBA,'' AS TypeOfBusiness,'' AS Telephone,'' AS Sex,'' AS Email
     From CurrentArchActivity a Join @Activities b 
     on a.TranNo=b.TranNo 
     where ByOrderCustID is Null and (IsNull(ByOrder,'')<>''  or IsNull(ByOrderAddress,'')<>''  )
     Union all
     -- From activity IntermediaryCustID if the id is missing
     Select '0' as ID, IsNull(Intermediary,'') as Name,Isnull(IntermediaryAddress,'') as Address,
     '' as TIN, IsNull(IntermediaryCity,'') as City,Isnull(IntermediaryState,'') as State,
     IsNull(IntermediaryZip,'') as Zip, IsNull(IntermediaryCountry,'') as Country, Null as DOB,
     '' AS LicenseNo, '' as LicenseState, '' as PaasportNo, '' AS DBA,'' AS TypeOfBusiness,'' AS Telephone,'' AS Sex,'' AS Email
     From CurrentArchActivity a Join @Activities b 
     on a.TranNo=b.TranNo 
     where IntermediaryCustID is Null and (IsNull(Intermediary,'')<>''  or IsNull(IntermediaryAddress,'')<>''  )
     Union all
     -- From activity IntermediaryCustID3 if the id is missing
     Select '0' as ID, IsNull(Intermediary3,'') as Name,Isnull(IntermediaryAddress3,'') as Address,
     '' as TIN, IsNull(IntermediaryCity3,'') as City,Isnull(IntermediaryState3,'') as State,
     IsNull(IntermediaryZip3,'') as Zip, IsNull(IntermediaryCountry3,'') as Country, Null as DOB,
     '' AS LicenseNo, '' as LicenseState, '' as PaasportNo, '' AS DBA,'' AS TypeOfBusiness,'' AS Telephone,'' AS Sex,'' AS Email
     From CurrentArchActivity a Join @Activities b 
     on a.TranNo=b.TranNo 
     where IntermediaryCustID3 is Null and (IsNull(Intermediary3,'')<>''  or IsNull(IntermediaryAddress3,'')<>''  )
     ) as sum

     -- Return Amounts
	 Select 'Amounts' as TableID, TranNo,BaseAmt,Curr,ISNULL(FxAmt,0) AS FxAmt,RecvPay, CashAcc from @Activities

     -- Return Customers
    iF @benectr=1
    Begin
    Select top 23 'Customers' AS TableID,Summary.* From (
     Select ID,Name,Address,TIN,City,State,Zip,
     Country,DOB,LicenseNo,LicenseState,PassportNo,DBA,TypeOfBusiness,Telephone,Sex,Email
	 From Customer
     join @Customers tr on customer.id=tr.custid
     Union all
     -- From activity BeneBank if the id is missing
     Select '0' as ID, IsNull(BeneBank,'') as Name,Isnull(BeneBankAddress,'') as Address,
     '' as TIN, IsNull(BeneBankCity,'') as City,Isnull(BeneBankState,'') as State,
     IsNull(BeneBankZip,'') as Zip, IsNull(BeneBankCountry,'') as Country, Null as DOB,
     '' AS LicenseNo, '' as LicenseState, '' as PaasportNo, '' AS DBA,'' AS TypeOfBusiness,'' AS Telephone,'' AS Sex,'' AS Email
     From CurrentArchActivity a Join @Activities b 
     on a.TranNo=b.TranNo 
     where BeneBankCustId is Null and (IsNull(BeneBank,'')<>''  or IsNull(BeneBankAddress,'')<>''  )
     Union all
     -- From activity IntermediaryCustID2 if the id is missing
     Select '0' as ID, IsNull(Intermediary2,'') as Name,Isnull(IntermediaryAddress2,'') as Address,
     '' as TIN, IsNull(IntermediaryCity2,'') as City,Isnull(IntermediaryState2,'') as State,
     IsNull(IntermediaryZip2,'') as Zip, IsNull(IntermediaryCountry2,'') as Country, Null as DOB,
     '' AS LicenseNo, '' as LicenseState, '' as PaasportNo, '' AS DBA,'' AS TypeOfBusiness,'' AS Telephone,'' AS Sex,'' AS Email
     From CurrentArchActivity a Join @Activities b 
     on a.TranNo=b.TranNo 
     where IntermediaryCustID2 is Null and (IsNull(Intermediary2,'')<>''  or IsNull(IntermediaryAddress2,'')<>''  )
     Union all
     -- From activity Intermediary4 if the id is missing
     Select '0' as ID, IsNull(Intermediary4,'') as Name,Isnull(IntermediaryAddress4,'') as Address,
     '' as TIN, IsNull(IntermediaryCity4,'') as City,Isnull(IntermediaryState4,'') as State,
     IsNull(IntermediaryZip4,'') as Zip, IsNull(IntermediaryCountry4,'') as Country, Null as DOB,
     '' AS LicenseNo, '' as LicenseState, '' as PaasportNo, '' AS DBA,'' AS TypeOfBusiness,'' AS Telephone,'' AS Sex,'' AS Email
     From CurrentArchActivity a Join @Activities b 
     on a.TranNo=b.TranNo 
     where IntermediaryCustID4 is Null and (IsNull(Intermediary4,'')<>''  or IsNull(IntermediaryAddress4,'')<>''  )
     ) as summary
    end

GO

If exists (Select * From sysobjects 
	Where id = object_id(N'BSA_SelRegReportsForEFiling') and
	OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop procedure BSA_SelRegReportsForEFiling
Go
CREATE Procedure dbo.BSA_SelRegReportsForEFiling
With Encryption As
Begin
select RepNo, SARecNo, Type, Prepared, 
       PreparedBy, Filed, FiledBy , Approved, ApprovedBy , Method, 
       ContentsTyp, RptImageTyp, CreateOper, r.CreateDate, r.LastOper,
       r.LastModify, RptFiled, IsFinalized, FinalizedBy, EFileFormat, 
       Notes, Cust, Account, PdfFileName, OwnerBranch, ownerDept,
       ownerOper, ts, MailReceiptNo, TccCode, AdditionalPersons, Reason, LegacyContents as Contents,LegacyContents, 

RptImage
from regreport r (nolock) join psec.dbo.branch b on r.OwnerBranch = b.Code 
WHERE b.branchnumber is not null AND 
((type in ('SAR', 'SFISAR') AND 1 IN 
(select enabled from optiontbl where Code = 'SARElect')) OR
(type = 'CTR' AND 1 IN
(select enabled from optiontbl where Code = 'CTRElect'))) AND
IsFinalized = 1 and EFileFormat = 0 and RptFiled = 0 and (PdfFileName = 'SAR03292012.pdf' or PdfFileName = 'CTR03292012.pdf')
order by r.type, r.tccCode, b.branchnumber, r.createdate
End
Go

--- Branch data for Bene Transactions
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'BSA_ActivityBranchesForCase')
    DROP PROCEDURE [DBO].[BSA_ActivityBranchesForCase]
GO
Create PROCEDURE dbo.BSA_ActivityBranchesForCase(@ObjectID 	sCode)
With Encryption as

    Declare @CaseType varchar(11)
    Declare @Branch varchar(11)
    Declare @stat int
	Declare @paytran Int

    Select @caseType = SuspType 
	From SuspiciousActivity with (nolock) 	
	Where recno =@ObjectID 

	set @paytran = 0
	If Exists (Select * from SuspiciousActivity with (nolock) where RecNo=@ObjectID )
		SELECT @Paytran=CASE When ActInActCnt>0 Then 1 Else 2 End
		FROM SuspiciousActivity with (nolock)	
		WHERE recno = @ObjectID
	Else 
		SELECT @Paytran=CASE When ActInActCnt>0 Then 1 Else 2 End
		FROM SuspiciousActivityHist with (nolock) 	
		WHERE recno = @ObjectID


    If   @caseType <> 'ExcCurrency'
    Begin 
		If Exists(Select distinct ah.Branch
		From dbo.SAsActivity sas with (nolock)
		Join dbo.ActivityHist ah with (nolock) on sas.TranNo=ah.TranNo
		Where sas.objectID=@ObjectID 
		and sas.Included=1 and upper(sas.objecttype) = 'SUSPACT')

		Select distinct ah.Branch, ah.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		From dbo.SAsActivity sas with (nolock)
		Join dbo.ActivityHist ah with (nolock) on sas.TranNo=ah.TranNo
		Join PSEC.dbo.Branch b with (nolock) on b.Code = ah.Branch
		Where sas.objectID=@ObjectID 
		and sas.Included=1 and upper(sas.objecttype) = 'SUSPACT'
        Order By ah.BookDate

        Else
			Select distinct a.Branch, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			From dbo.SAsActivity sas with (nolock)
			Join dbo.Activity a with (nolock) on sas.TranNo=a.TranNo
			Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch
			Where sas.objectID=@ObjectID 
			and sas.Included=1 and upper(sas.objecttype) = 'SUSPACT'
			Order By a.BookDate

        Select @stat = @@error
        Return @stat
	End

    If (Select Enabled From OptionTbl where code='BeneCTR')=1
    Begin 
		--Exeed currency. Trans not is sasactivity.
		If exists(Select distinct a.Branch
			From SuspiciousActivity sa  with (nolock) Join activityHist a with (nolock)
			on case when a.BeneCustID is NULL Then a.Cust Else a.BeneCustID End = sa.Cust
			Where sa.recno = @objecTID)

			Select distinct a.Branch, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			From SuspiciousActivity sa  with (nolock) Join activityHist a with (nolock)
			on case when a.BeneCustID is NULL Then a.Cust Else a.BeneCustID End = sa.Cust AND a.BookDate = sa.BookDate 
			and (@paytran = a.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch
			and a.CashTran = 1
			Where sa.recno = @objecTID
			Order By a.BookDate

		Else
			Select distinct a.Branch, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		    From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
		    on case when a.BeneCustID is NULL Then a.Cust Else a.BeneCustID End = sa.Cust AND a.BookDate = sa.BookDate 
			and (@paytran = a.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch		    
			and a.CashTran = 1
		    Where sa.recno = @OBJECTID
		    Order By a.BookDate
    End
    Else
    Begin
		--Exeed currency. Trans not is sasactivity.
		If exists(Select distinct a.Branch
			From SuspiciousActivity sa  with (nolock) Join activityHist a with (nolock)
			on a.Cust = sa.Cust
			Where sa.recno = @objecTID)

			Select distinct a.Branch, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			From SuspiciousActivity sa  with (nolock) Join activityHist a with (nolock)
			on a.Cust = sa.Cust AND a.BookDate = sa.BookDate 
			and (@paytran = a.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch		    
			and a.CashTran = 1
			Where sa.recno = @objecTID
			Order By a.BookDate

		Else
			Select distinct a.Branch, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		    From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
		    on a.Cust = sa.Cust AND a.BookDate = sa.BookDate 
			and (@paytran = a.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch		    
			and a.CashTran = 1
		    Where sa.recno = @OBJECTID
		    Order By a.BookDate
    End
							
	 Select @stat = @@error
     Return @stat	 

GO

If Exists(Select * From sysobjects 
          where id = object_id(N'[dbo].[BSA_CountryNameFromIsoCode]') 
              and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
    Drop Procedure dbo.BSA_CountryNameFromIsoCode
Go

Create Procedure dbo.BSA_CountryNameFromIsoCode ( @isoCode CHAR(2), @CountryName VARCHAR(255) output)
 With Encryption As
 
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_CountryNameFromIsoCode
  else
    -- Already in a transaction
	save tran BSA_CountryNameFromIsoCode
  -- End standard stored procedure transaction header
  
  declare	@v_IsoCode char(2),
		@v_Countryname varchar(255),
		@stat int
		
  If Exists ( Select Name From Country with (nolock) Where Code = @isoCode ) 
  begin
    Select @CountryName = Name from Country with (nolock) Where Code = @isoCode 
    Select @stat = @@error  	
  end 
  else     
    Select @stat = 250009	-- Object does not exist
  
  If ( @stat <> 0 ) begin 
       rollback tran BSA_CountryNameFromIsoCode
  	   return @stat
  end 
  If @trnCnt = 0
      commit tran BSA_CountryNameFromIsoCode      
  return @stat
Go

--- Branch data for Bene Transactions
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'BSA_ActivityBranchForCase')
    DROP PROCEDURE [DBO].[BSA_ActivityBranchForCase]
GO

Create PROCEDURE dbo.BSA_ActivityBranchForCase(@ObjectID 	sCode)
With Encryption as

    Declare @CaseType varchar(11)
    Declare @Branch varchar(11)
    Declare @stat int

	Set @Branch=''
    Select @caseType = SuspType 
	From SuspiciousActivity 	
	Where recno =@ObjectID 

    If   @caseType <> 'ExcCurrency'
    Begin 
		Select Top 1 @Branch=a.Branch
		From dbo.SAsActivity sas
		Join dbo.Activity a on sas.TranNo=a.TranNo
		Where sas.objectID=@ObjectID 
		and Included=1 and upper(sas.objecttype) = 'SUSPACT'
        Order By a.BookDate

        If @Branch=''
			Select Top 1 @Branch=ah.Branch
			From dbo.SAsActivity sas
			Join dbo.ActivityHist ah on sas.TranNo=ah.TranNo
			Where sas.objectID=@ObjectID 
			and Included=1 and upper(sas.objecttype) = 'SUSPACT'
			Order By ah.BookDate

        Select @stat = @@error
        Select @Branch AS Branch
        Return @stat
	End

    If (Select Enabled From OptionTbl where code='BeneCTR')=1
    Begin 
		--Exeed currency. Trans not is sasactivity.
			Select Top 1 @Branch=a.Branch
			From SuspiciousActivity sa  Join activityHist a
			on case when a.BeneCustID is NULL Then a.Cust Else a.BeneCustID End = sa.Cust AND a.BookDate = sa.BookDate 
			and a.CashTran = 1
			Where sa.recno = @objecTID
			Order By a.BookDate

		If @Branch = ''
			Select Top 1 @Branch=a.Branch
		    From SuspiciousActivity sa  Join activity a
		    on case when a.BeneCustID is NULL Then a.Cust Else a.BeneCustID End = sa.Cust AND a.BookDate = sa.BookDate 
		    and a.CashTran = 1
		    Where sa.recno = @OBJECTID
		    Order By a.BookDate
    End
    Else
    Begin
		--Exeed currency. Trans not is sasactivity.
			Select Top 1 @Branch=a.Branch
			From SuspiciousActivity sa  Join activityHist a
			on a.Cust = sa.Cust AND a.BookDate = sa.BookDate 
			and a.CashTran = 1
			Where sa.recno = @objecTID
			Order By a.BookDate

		If @Branch = ''
			Select Top 1 @Branch=a.Branch
		    From SuspiciousActivity sa  Join activity a
		    on a.Cust = sa.Cust AND a.BookDate = sa.BookDate 
		    and a.CashTran = 1
		    Where sa.recno = @OBJECTID
		    Order By a.BookDate
    End
							
	 Select @stat = @@error
     Select @Branch as Branch
     Return @stat	 

GO


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Completed Conversion of PBSA Database				  '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go
