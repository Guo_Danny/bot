/*
**  File Name:      ConvOFAC.sql
**
**  Functional Description:
**
**      This module contains SQL conversion procedures for Prime v9.0.3 to 
**  fix the SDN Alternate Name delete functionality in OFS_CompleteLoadDistribution
**
**  Facility        The Compliance Suite's OFAC Database
**  Creation Date:  12/30/2010
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2007-2010                                            ***
*** FIS				                            ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
***************************************************************************
                         Maintenance History

------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification
------------|----------|----------------------------------------------------
12/30/2010		SH		Fixed problem with delete functionality of alternate names, address, keywords 
					to a given SDN party (I100430837)

**/


-------------------------------------------------------------------------------

Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print '  Starting Conversion of OFAC Object - v9.0.3             '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

Use OFAC
Go

Set ANSI_NULLS ON
Go

Set NOCOUNT ON
Go

IF EXISTS (SELECT * FROM sysobjects 
    WHERE ID = OBJECT_ID(N'[dbo].[OFS_CompleteLoadDistribution]') 
    AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE OFS_CompleteLoadDistribution
GO

CREATE PROCEDURE dbo.OFS_CompleteLoadDistribution (
                            @isdiff bit
) With Encryption As

    declare @oper Code
    declare @trnCnt int

    select @oper = 'Prime'
    select @trnCnt = @@trancount  -- Save the current trancount

    if @trnCnt = 0
        -- Transaction has not begun
        begin tran OFS_CompleteLoadDistribution
    else
        -- Already in a transaction
        save tran OFS_CompleteLoadDistribution

    -- Processing starts here:
    declare @stat int

--******** Update Main tables from temp tables ********************************
/* TempLoad SDN tables contains the distribution records
    If ListModifDate is equal then just update the Status with the
    distribution record status else update the whole record except the
    deleted column

    If record is not in main table then insert the record

    Set the Status of all records in main table but not in temp table
    to deleted if not loading a differential file, else set the Status
    to unchanged
*/
--*****************************************************************************

    declare @newstatus int
    If @isdiff = 0
        set @newstatus = 4  --deleted
    Else
        set @newstatus = 1  --unchanged

    Create table #TempUserEntNums (
	EntNum int
    )

    Create table #TempUserAltNums (
	AltNum int
    )

    insert into #TempUserEntNums(EntNum)
    Select  distinct Entnum from SDNTable where UserRec = 1

    insert into #TempUserAltNums(AltNum)
    Select distinct AltNum from SDNAltTable, SDNTable where SDNAltTable.EntNum = SDNTable.Entnum
    and SDNTable.UserRec = 1

   -- For all lists that have been disabled, set the record statuses to deleted
    
	Update SDNTable Set Status = 4, LastModifDate = getDate()
        FROM SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and s.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update SDNAltTable Set Status = 4, LastModifDate = getDate()
        FROM SDNAltTable sa (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = sa.EntNum and sa.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update SDNAddrTable Set Status = 4, LastModifDate = getDate()
        FROM SDNAddrTable sa (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = sa.EntNum and sa.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update DOBs Set Status = 4, LastModify = getDate()
        FROM DOBs d (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = d.EntNum and d.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update Notes Set Status = 4, LastModify = getDate()
        FROM Notes n (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = n.EntNum and n.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update URLs Set Status = 4, LastModify = getDate()
        FROM URLs u (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = u.EntNum and u.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update Relationship Set Status = 4, LastModify = getDate()
        FROM Relationship r (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = r.EntNum and r.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update Keywords Set Status = 4, LastModify = getDate()
        FROM Keywords k (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = k.EntNum and k.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update KeywordsAlt Set Status = 4, LastModify = getDate()
        FROM KeywordsAlt k (nolock), SDNAltTable sa (nolock),
            SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            sa.AltNum = k.AltNum and sa.EntNum = s.EntNum and k.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

   -- Update entire record if distribution listmodifdate != last listmodifdate
   -- or (if status not equal to unchanged and listmodifdates are equal)

If Exists (Select EntNum from TempLoadSDNTable) Begin
   Update SDNtable Set
        [Name] = t.[Name],
        FirstName = NULLIF(t.FirstName,''),
        MiddleName = NULLIF(t.MiddleName,''),
        LastName = NullIf(t.LastName,''),
        Title = t.Title,
        Program = UPPER(t.Program),
        Type = t.Type,
        CallSign = t.CallSign,
        VessType = t.VessType,
        Tonnage = t.Tonnage,
        GRT = t.GRT,
        VessFlag = t.VessFlag,
        VessOwner = t.Vessowner,
        Dob = NullIF(t.DOB,''),
        Remarks = t.Remarks,
        Remarks1 = t.Remarks1,
        Remarks2 = t.Remarks2,
        SDNType = t.SDNType,
        DataType = t.DataType,
        userRec = t.userRec,
        duplicRec = t.duplicRec,
        IgnoreDerived = t.IgnoreDerived,
        ListID = t.ListId,
        ListType = t.ListType,
        Country = t.Country,
        Sex = t.Sex,
        Build = t.Build,
        Height = NullIf(t.Height,0),
        Weight = NullIf(t.Weight,0),
        Race = t.Race,
        Complexion = t.Complexion,
        eyes = t.eyes,
        hair = t.hair,
        PrimeAdded = t.PrimeAdded,
        StandardOrder = t.StandardOrder,
        EffectiveDate = t.EffectiveDate,
        ExpiryDate = t.ExpiryDate,
        Status = t.Status,
        ListCreateDate = t.ListCreatedate,
        ListModifDate = t.ListModifDate,
        LastModifDate = GetDate(),
        LastOper = t.LastOper
    From SDNTable s, TempLoadSDNTable t
        Where s.EntNum = t.EntNum And s.UserRec = 0 And
              ( (IsNull(s.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End
   -- Update status to distribution records status
   -- if distribution listmodifdate = last listmodifdate and status = unchanged
If Exists (Select EntNum from TempLoadSDNTable) Begin
   Update SDNTable Set Status = t.Status
        From SDNTable s, TempLoadSDNTable t
            Where s.EntNum = t.EntNum  and s.UserRec = 0 And
               IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0) And
               t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

-- Update record status of records not in temp table
  If (@newstatus=1)
	Begin
		Update SDNTable Set Status = @newstatus, LastModifDate = getDate()
			FROM SDNTable s (nolock) left outer join TempLoadSDNTable t (nolock)
			on s.EntNum = t.EntNum
			Where t.EntNum is NULL and s.UserRec = 0 and s.Status in (2,3)
	End
  Else
	Begin
		Update SDNTable Set Status = @newstatus, LastModifDate = getDate()
			FROM SDNTable s (nolock) left outer join TempLoadSDNTable t (nolock)
			on s.EntNum = t.EntNum
			Where t.EntNum is NULL and s.UserRec = 0 and s.Status <> 4
	END
   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

-- Insert new records
If Exists (Select EntNum from TempLoadSDNTable) Begin
   Insert into SDNTable (EntNum, [Name], FirstName,
                        MiddleName, LastName,Title,
                        Program, Type, CallSign,
                        VessType, Tonnage, GRT,
                        VessFlag, VessOwner, Dob,
                        Remarks, Remarks1, Remarks2,SDNType,
                        DataType, userRec, deleted,
                        duplicRec, IgnoreDerived,
                        ListID, ListType, Country,
                        Sex, Build, Height, Weight,
                        Race, Complexion, eyes, hair,
                        PrimeAdded, StandardOrder, EffectiveDate,
                        ExpiryDate, Status, ListCreateDate,
                        ListModifDate, CreateDate, LastModifDate,
                        LastOper)
    Select t.EntNum, t.[Name], t.FirstName,
                        t.MiddleName, t.LastName, t.Title,
                        t.Program, t.Type, t.CallSign,
                        t.VessType, t.Tonnage, t.GRT,
                        t.VessFlag, t.VessOwner, t.Dob,
                        t.Remarks, t.Remarks1, t.Remarks2, t.SDNType,
                        t.DataType, t.userRec, t.deleted,
                        t.duplicRec, t.IgnoreDerived,
                        t.ListID, t.ListType, t.Country,
                        t.Sex, t.Build, t.Height, t.Weight,
                        t.Race, t.Complexion, t.eyes, t.hair,
                        t.PrimeAdded, t.StandardOrder, t.EffectiveDate,
                        t.ExpiryDate, t.Status, t.ListCreateDate,
                        t.ListModifDate, GetDate(), GetDate(),
                        t.LastOper
    From TempLoadSDNTable t (nolock) left Outer Join SDNTable s (nolock)
        on t.EntNum = s.EntNum
    where s.Entnum is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select AltNum from TempLoadAltTable) Begin
  Update SDNAlttable Set
        EntNum  = t.EntNum,
        AltType = t.AltType,
        AltName = t.AltName,
        FirstName = t.FirstName,
        MiddleName = t.MiddleName,
        LastName = t.LastName,
        ListId = t.ListId,
        Remarks = t.Remarks,
        status = t.status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModifDate = GetDate(),
        LastOper = t.LastOper
     From SDNAltTable s, TempLoadAltTable t
        Where s.AltNum = t.AltNum And
              ( (IsNull(s.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End
   -- Update status to distribution records status
   -- if distribution listmodifdate = last listmodifdate and status = unchanged
If Exists (Select AltNum from TempLoadAltTable) Begin
   Update SDNAltTable Set Status = t.Status, LastOper = t.LastOper
    From SDNAltTable s, TempLoadAltTable t
            Where s.AltNum = t.AltNum And
               IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End


  -- Update record status of records not in temp table
   If (@newstatus = 1)
	Begin
		Update SDNAltTable Set Status = @newstatus, LastModifDate = getDate(),
			LastOper = @oper
		From SDNAltTable s (nolock) 
                left outer join  TempLoadAltTable t (nolock) on s.AltNum = t.AltNum
                Where t.AltNum is NULL and s.Status in (2,3)
	End
	Else
	Begin
		Update SDNAltTable Set Status = @newstatus, LastModifDate = getDate(),
                LastOper = @oper
                From SDNAltTable s (nolock) 
                left outer join TempLoadAltTable t (nolock) on s.AltNum = t.AltNum
                left outer join #TempUserAltNums ta on s.AltNum = ta.AltNum
		Where t.AltNum is NULL and ta.AltNum is NULL and s.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution



   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select AltNum from TempLoadAltTable) Begin
   Insert into SDNAltTable (EntNum, AltNum, AltType, AltName, FirstName,
      MiddleName, LastName,Remarks, Status, ListCreateDate, ListModifDate,
      CreateDate, LastModifDate, LastOper)
   Select t.EntNum, t.AltNum, t.AltType, t.AltName, t.FirstName,
        t.MiddleName, t.LastName, t.Remarks, t.Status, t.ListCreateDate, t.ListModifDate,
        GetDate(), GetDate(), t.LastOper
   From TempLoadAltTable t (nolock) left Outer Join SDNAltTable s (nolock)
            on t.AltNum = s.AltNum
        where s.AltNum is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select AddrNum from TempLoadAddrTable)  Begin
    -- Update the address records
   Update SDNAddrTable Set
        EntNum = t.EntNum,
        Address = t.Address,
        City = t.City,
        Country = t.Country,
        Remarks = t.Remarks,
        ListId = t.ListId,
        FreeFormat = t.FreeFormat,
        Address2 = t.Address2,
        Address3 = t.Address3,
        Address4 = t.Address4,
        State = t.State,
        PostalCode = t.PostalCode,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModifDate = GetDate(),
        LastOper = t.LastOper
    From SDNAddrTable s, TempLoadAddrTable t
        Where s.AddrNum = t.AddrNum And
              ( (IsNull(s.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select AddrNum from TempLoadAddrTable) Begin
   Update SDNAddrTable Set Status = t.Status, LastOper = t.LastOper
    From SDNAddrTable s, TempLoadAddrTable t
            Where s.AddrNum = t.AddrNum And
               IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
   If (@newstatus = 1)
	Begin
		Update SDNAddrTable Set Status = @newstatus, LastModifDate = getDate(),
			LastOper = @oper
		From SDNAddrTable s (nolock) 
                left outer join  TempLoadAddrTable t (nolock) on s.AddrNum = t.AddrNum
                Where t.AddrNum is NULL and s.Status in (2,3)
	End
	Else
	Begin
		Update SDNAddrTable Set Status = @newstatus, LastModifDate = getDate(),
			LastOper = @oper
		From SDNAddrTable s (nolock) left outer join  TempLoadAddrTable t (nolock) on s.AddrNum = t.AddrNum
                Left outer join #TempUserEntNums ta on s.EntNum = ta.EntNum
		Where t.AddrNum is NULL and ta.EntNum is NULL and s.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select AddrNum from TempLoadAddrTable) Begin
     Insert into SDNAddrTable (EntNum, AddrNum, Address, City, Country, Remarks,
        ListId, FreeFormat, Address2, Address3, Address4,
        State, PostalCode, Status, ListCreateDate, ListModifDate,
        CreateDate, LastModifDate, LastOper)
    Select t.EntNum, t.AddrNum, t.Address, t.City, t.Country, t.Remarks,
        t.ListId, t.FreeFormat, t.Address2, t.Address3, t.Address4,
        t.State, t.PostalCode, t.Status, t.ListCreateDate, t.ListModifDate,
        GetDate(), GetDate(), t.LastOper
    From TempLoadAddrTable t (nolock) left Outer Join SDNAddrTable s (nolock)
        on t.AddrNum = s.AddrNum
    where s.AddrNum is Null


   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsId from TempLoadKeywords)  Begin
    -- Update the address records
   Update Keywords Set
        EntNum = t.EntNum,
        Word = t.Word,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From Keywords k, TempLoadKeywords t
        Where k.keywordsId = t.keywordsId And
              ( (IsNull(k.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsId from TempLoadKeywords) Begin
   Update Keywords Set Status = t.Status, LastOper = t.LastOper
    From Keywords k, TempLoadKeywords t
            Where k.keywordsId = t.keywordsId And
               IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
	If (@newstatus=1)
	Begin
		Update Keywords Set Status = @newstatus, LastModify = getDate(),
			LastOper = @oper
		From Keywords k (nolock) left outer join TempLoadKeywords t (nolock)
        on k.keywordsId = t.keywordsId
        Where t.keywordsId is NULL and k.Status in (2,3)
	End
	Else
	Begin
		Update Keywords Set Status = @newstatus, LastModify = getDate(),
			LastOper = @oper
		From Keywords k (nolock) left outer join TempLoadKeywords t (nolock) on k.keywordsId = t.keywordsId
                left outer join #TempUserEntNums ta on k.EntNum = ta.EntNum
		Where t.keywordsId is NULL and ta.EntNum is NULL and k.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select keywordsId from TempLoadKeywords) Begin
     Insert into keywords (KeywordsId, Entnum, Word, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
     Select t.KeywordsId, t.Entnum, t.Word, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadKeywords t (nolock) left Outer Join keywords k (nolock)
        on t.keywordsId = k.keywordsId
    where k.keywordsId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsAltId from TempLoadKeywordsAlt)  Begin
    -- Update the address records
   Update KeywordsAlt Set
        AltNum = t.AltNum,
        Word = t.Word,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From KeywordsAlt k, TempLoadKeywordsAlt t
        Where k.keywordsAltId = t.keywordsAltId And
              ( (IsNull(k.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsAltId from TempLoadKeywordsAlt) Begin
   Update KeywordsAlt Set Status = t.Status, LastOper = t.LastOper
    From KeywordsAlt k, TempLoadKeywordsAlt t
            Where k.keywordsAltId = t.keywordsAltId And
               IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
	If (@newstatus=1)
	Begin
		Update KeywordsAlt Set Status = @newstatus, LastModify = getDate(),
			LastOper = @oper
		From KeywordsAlt k (nolock) left outer join TempLoadKeywordsAlt t (nolock)
        on k.keywordsAltId = t.keywordsAltId
        Where t.keywordsAltId is NULL and k.Status in (2,3)
	End
	Else
	Begin
		Update KeywordsAlt Set Status = @newstatus, LastModify = getDate(),
			LastOper = @oper
		From KeywordsAlt k (nolock) 
                left outer join TempLoadKeywordsAlt t (nolock) on k.keywordsAltId = t.keywordsAltId
                left outer join #TempUserAltNums ta on k.AltNum = ta.AltNum
		Where t.keywordsAltId is NULL and ta.AltNum is NULL and k.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select keywordsAltId from TempLoadKeywordsAlt) Begin
     Insert into keywordsAlt (KeywordsAltId, Altnum, Word,
        Status, ListCreateDate, ListModifdate,
        CreateOper, CreateDate, LastOper,
            LastModify)
     Select t.KeywordsAltId, t.Altnum, t.Word,
        t.Status, t.ListCreateDate, t.ListModifdate,
        t.CreateOper, GetDate(), t.LastOper,
            GetDate()
    From TempLoadKeywordsAlt t (nolock) left Outer Join KeywordsAlt k (nolock)
        on t.KeywordsAltId = k.KeywordsAltId
    where k.KeywordsAltId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select DOBsId from TempLoadDOBs)  Begin
    -- Update the address records
   Update DOBs Set
        EntNum = t.EntNum,
        DOB = t.DOB,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From DOBs d, TempLoadDOBs t
        Where d.DOBsId = t.DOBsId And
              ( (IsNull(d.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(d.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select DOBsId from TempLoadDOBs) Begin
   Update DOBs Set Status = t.Status, LastOper = t.LastOper
    From DOBs d, TempLoadDOBs t
            Where d.DOBsId = t.DOBsId And
               IsNull(d.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
	If (@newstatus=1)
	Begin
		Update DOBs Set Status = @newstatus, LastModify = getDate(),
			LastOper = @oper
		From DOBs d (nolock) left outer join TempLoadDOBs t (nolock)
        on d.DOBsId = t.DOBsId
        Where t.DOBsId is NULL and d.Status in (2,3) 
	End
	Else
	Begin
		Update DOBs Set Status = @newstatus, LastModify = getDate(),
			LastOper = @oper
		From DOBs d (nolock) 
                left outer join TempLoadDOBs t (nolock) on d.DOBsId = t.DOBsId
                left outer join #TempUserEntNums ta on d.EntNum = ta.EntNum
		Where t.DOBsId is NULL and ta.EntNum is NULL and d.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select DOBsId from TempLoadDOBs) Begin
     Insert into DOBs (DOBsId, Entnum, DOB, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.DOBsId, t.Entnum, t.DOB, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadDOBs t (nolock) left Outer Join DOBs d (nolock)
        on t.DOBsId = d.DOBsId
    where d.DOBsId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select NotesId from TempLoadNotes)  Begin
    -- Update the address records
   Update Notes Set
        EntNum = t.EntNum,
        Note = t.Note,
        NoteType = t.NoteType,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From Notes n, TempLoadNotes t
        Where n.NotesId = t.NotesId And
              ( (IsNull(n.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(n.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select NotesId from TempLoadNotes) Begin
   Update Notes Set Status = t.Status, LastOper = t.LastOper
    From Notes n, TempLoadNotes t
            Where n.NotesId = t.NotesId And
               IsNull(n.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
	If (@newstatus=1)
	Begin
		Update Notes Set Status = @newstatus, LastModify = getDate(),
		LastOper = @oper
		From Notes n (nolock) left outer join TempLoadNotes t (nolock)
        on n.NotesId = t.NotesId
        Where t.NotesId is NULL and n.Status in (2,3)
	End
	Else
	Begin
		Update Notes Set Status = @newstatus, LastModify = getDate(),
		LastOper = @oper
		From Notes n (nolock) 
                left outer join TempLoadNotes t (nolock) on n.NotesId = t.NotesId
                left outer join #TempUserEntNums ta on n.EntNum = ta.EntNum
		Where t.NotesId is NULL and ta.EntNum is NULL and n.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select NotesId from TempLoadNotes) Begin
     Insert into Notes (NotesId, Entnum, Note, NoteType, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.NotesId, t.Entnum, t.Note, t.NoteType, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadNotes t (nolock) left Outer Join Notes n (nolock)
        on t.NotesId = n.NotesId
    where n.NotesId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select URLsId from TempLoadURLs)  Begin
    -- Update the address records
   Update URLs Set
        EntNum = t.EntNum,
        URL = t.URL,
        [Description] = t.[Description],
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From URLs u, TempLoadURLs t
        Where u.URLsId = t.URLsId And
              ( (IsNull(u.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(u.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select URLsId from TempLoadURLs) Begin
   Update URLs Set Status = t.Status, LastOper = t.LastOper
    From URLs u, TempLoadURLs t
            Where u.URLsId = t.URLsId And
               IsNull(u.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
	If (@newstatus=1)
	Begin
		Update URLs Set Status = @newstatus, LastModify = getDate(),
		LastOper = @oper
		From URLs u (nolock) left outer join TempLoadURLs t (nolock)
        on u.URLsId = t.URLsId
        Where t.URLsId is NULL and u.Status in (2,3)
	End
	Else
	Begin
		Update URLs Set Status = @newstatus, LastModify = getDate(),
		LastOper = @oper
		From URLs u (nolock) 
                left outer join TempLoadURLs t (nolock) on u.URLsId = t.URLsId
                left outer join #TempUserEntNums ta on u.EntNum = ta.EntNum
		Where t.URLsId is NULL and ta.EntNum is NULL and u.Status <> 4
	End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select URLsId from TempLoadURLs) Begin
     Insert into URLs (URLsId, Entnum, URL, [Description], Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.URLsId, t.Entnum, t.URL, t.[Description], t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadURLs t (nolock) left Outer Join URLs u (nolock)
        on t.URLsId = u.URLsId
    where u.URLsId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select RelationshipId from TempLoadRelationship)  Begin
    -- Update the address records
   Update Relationship Set
        EntNum = t.EntNum,
        RelatedID = t.RelatedID,
        RelationshipPToR = t.RelationshipPToR,
        RelationshipRToP = t.RelationshipRToP,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From Relationship r, TempLoadRelationship t
        Where r.RelationshipId = t.RelationshipId And
              ( (IsNull(r.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(r.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select RelationshipId from TempLoadRelationship) Begin
   Update Relationship Set Status = t.Status, LastOper = t.LastOper
    From Relationship r, TempLoadRelationship t
            Where r.RelationshipId = t.RelationshipId And
               IsNull(r.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
	If (@newstatus=1)
	Begin
		Update Relationship Set Status = @newstatus, LastModify = getDate(),
		LastOper = @oper
		From Relationship r (nolock) left outer join TempLoadRelationship t (nolock)
        on r.RelationshipId = t.RelationshipId
        Where t.RelationshipId is NULL and r.Status in (2,3)
	End
	Else
	Begin
		Update Relationship Set Status = @newstatus, LastModify = getDate(),
		LastOper = @oper
		From Relationship r (nolock) 
                left outer join TempLoadRelationship t (nolock) on r.RelationshipId = t.RelationshipId
                left outer join #TempUserEntNums ta on r.EntNum = ta.EntNum
		Where t.RelationshipId is NULL and ta.EntNum is NULL and r.Status <> 4
	End
   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select RelationshipId from TempLoadRelationship) Begin
     Insert into Relationship (RelationshipId, Entnum, RelatedID,
        RelationshipPToR, RelationshipRToP, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.RelationshipId, t.Entnum, t.RelatedID,
        t.RelationshipPToR, t.RelationshipRToP, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadRelationship t (nolock) left Outer Join Relationship r (nolock)
        on t.RelationshipId = r.RelationshipId
    where r.RelationshipId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  if @isdiff = 0
  begin
      -- set status of undeleted records to unchanged
      Update SDNTable
        Set Status = 1 Where userRec = 1 and Deleted = 0

      Select @stat = @@error
      If (@stat <> 0)
          Goto AbortCompleteLoadDistribution
  end

--populate dob column in SDNTABLE from DOBs table.  DOB column should be shown as CSV value
update  sdntable set
dob=dt.dob_csv
from (
	select
		t1.entnum,
		stuff((
				select ',' + t.[dob]
				from dobs t
				where t.entnum = t1.entnum and
				t.status in (1,2,3)
				order by t.[dob]
				for xml path('')
				),1,1,'') as dob_csv
	from dobs t1
	group by entnum
) dt
inner join sdntable  s on
dt.entnum=s.entnum
where s.status  in (1,2,3)

/*
	update the status to modified in sdntable if the following condition is met: dobs table contains status of new or modified and status in Sdntable is NOT New.
	The LastModifDate should not be changed if the above conditions are met.
*/
UPDATE sdntable SET
[status]=3
FROM
( 
	SELECT MAX( d.status)AS NewMod, d.entnum 
	FROM dobs as d 
	WHERE d.status in (2,3)
	and 
	not exists
	(select * from sdntable sd where sd.entnum=d.entnum and sd.status=2)
	GROUP BY d.entnum
) AS dt
INNER JOIN sdntable AS st ON
st.entnum=dt.entnum

-- Set Keywords and Keywords Alternate to deleted 
--if the deleted keywords are introduced in the new distribution
Update Keywords 
Set Deleted = 1, 
	LastModify = GetDate() 
where Deleted = 0 and 
	  Word in (select word from Keywords where deleted = 1)

Update KeywordsAlt 
set Deleted = 1, 
	LastModify = GetDate() 
where Deleted = 0 and 
	  Word in (select word from KeywordsAlt where Deleted = 1)

 delete from TempLoadSDNTable
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

 delete from TempLoadAltTable
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadAddrTable
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadKeywords
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

 delete from TempLoadKeywordsAlt
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadDOBs
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadNotes
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadURLs
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadRelationship
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

 If @trnCnt = 0
    commit tran OFS_CompleteLoadDistribution

 return 0  -- Success

AbortCompleteLoadDistribution:
    if (@stat <> 0) Begin
        rollback tran OFS_CompleteLoadDistribution
        return @stat
    End
Go
Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print '  Completed conversion of OFAC object - v9.0.3              '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
