/*
**  File Name:        ConvPBSA.sql
**
**  Functional Description:
**
**      This module contains SQL patches
**      for version 9.0.3
**
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2010                                                   ***
*** FIS						                                             ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|---------------------------------------------------- 
 09/21/2011		CD		 Merge coverage period code changes done for 
						 R100275312 to create new stored procedures.
*/
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO

use pbsa
go

-- Create procedure to get SAR/SFISAR coverage period min and max Book dates 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'BSA_GetSARCoveragePeriodForEFiling')
    DROP PROCEDURE [DBO].[BSA_GetSARCoveragePeriodForEFiling]
GO
Create Procedure [dbo].[BSA_GetSARCoveragePeriodForEFiling] 
With Encryption As
          
          select Min(BookDate), Max(BookDate)
          from CurrentArchActivity
          where TranNo in
                   (select TranNo
                   from regreport r (nolock) join psec.dbo.branch b on r.OwnerBranch = b.Code 
                   inner join SasActivity on convert(varchar, SaRecNo) = ObjectId and ObjectType = 'SuspAct'  
                   WHERE b.branchnumber is not null AND 
                   [type] in ('SAR', 'SFISAR') AND 1 IN 
                   (select [enabled] from optiontbl where Code = 'SARElect')  AND
                   IsFinalized = 1 and EFileFormat = 0 and RptFiled = 0)
 
Go

------------------


-- Create procedure to get CTR coverage period min and max Book dates

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'BSA_GetCTRCoveragePeriodForEFiling')
    DROP PROCEDURE [DBO].[BSA_GetCTRCoveragePeriodForEFiling]
GO
Create PROCEDURE BSA_GetCTRCoveragePeriodForEFiling 
With Encryption As
 
          select Min(BookDate), Max(BookDate)
          from SuspiciousActivity 
          where RecNo in
                   (select SARecNo
                   from regreport r (nolock) join psec.dbo.branch b on r.OwnerBranch = b.Code 
                   WHERE b.branchnumber is not null AND 
                   [type] = 'CTR' AND 1 IN
                   (select [enabled] from optiontbl where Code = 'CTRElect') AND
                   IsFinalized = 1 and EFileFormat = 0 and RptFiled = 0)
 
Go
