USE [Psec]
GO
/****** Object:  StoredProcedure [dbo].[PSEC_ChkMinDaysChgPwd]    Script Date: 06/01/2017 11:31:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PSEC_ChkMinDaysChgPwd]
	@UserName varchar(255), @RemainDays int Output
  As
/*      Procedure to Check if password can be changed based on the parameter 
		for min days to retain password and the last password changed Date
	Output
		Remaining days for password change
	Return values
		0 - OK to Change
		
*/

	declare @optOn int
	declare @now datetime
	declare @PwdMinPer Int
	declare @LastPwdChgDate datetime
	declare @LastPwdChgoper Code
	DECLARE @Code Code

	select @now = GETDATE()

	Set @optOn = 0

	Set @RemainDays = 0

	SET @LastPwdChgDate = NULL
-- 	Check if Non NT Authentication has been enabled
	Select @optOn = Enabled from OptionTbl where OptCode = 'PwdConfig'

	If @optOn = 1 Begin
--		Get the parameter value
		SELECT @PwdMinPer = ParameterValue FROM OptionParameter 
			WHERE OptCode = 'PwdConfig' AND ParameterCode = 'PwdMinPer'
		If IsNull(@PwdMinPer ,0) > 0
		BEGIN
			SELECT @Code = Code, @LastPwdChgDate = LastPwdChgDate,
				@LastPwdChgOper = LastPwdChgOper FROM Operator
				WHERE @UserName = UserName

-- 			If Last Operator is different from oper assume that it was reset by admin
			IF @LastPwdChgDate IS NOT NULL --AND isnull(@LastPwdChgOper,'') = @Code
				IF DateDiff (dd, @LastPwdChgDate,@now) <= @PwdMinPer
					Set @RemainDays = @PwdMinPer - DateDiff (dd, @LastPwdChgDate,@now)
		END
	End

	RETURN 0



