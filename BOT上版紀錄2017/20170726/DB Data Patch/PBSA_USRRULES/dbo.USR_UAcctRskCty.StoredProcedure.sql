/****** Object:  StoredProcedure [dbo].[USR_UAcctRskCty]    Script Date: 07/21/2017 15:26:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[USR_UAcctRskCty]  (@WLCode SCode, @testAlert INT, 
	@NCCTCountry VARCHAR(8000), @UseCreateDate BIT)
AS

/* RULE AND PARAMETER DESCRIPTION
	Detects if a new account that is opened by the account owner whose country of 
	origin belongs to NCCT country list.
	Designed to be run Pre or Post EOD.
	
	@ncctCountry= detects any account with the account owner Country of origin 
	belonging to an ncct country
	@UseCreateDate= Use create date on the account intead of open date
	All accounts whose open/create date is greater than last eval state or the 
	last modify date of the rule will be considered 
*/

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT

DECLARE @lastEvalDate  DATETIME
DECLARE @lastModifyDate    DATETIME
DECLARE @BookDate int
DECLARE @wrkdate datetime

DECLARE	@TT TABLE (
	Account varchar(40),
	AccountOwner varchar(40),
	OpenDate DateTime,
	CountryOforigin varchar(11)
)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_UAcctRskCty 
ELSE
	-- Already in a transaction
	SAVE TRAN USR_UAcctRskCty 
/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @description = [Desc], @WLType = WLType, 
	@lastEvalDate = IsNull(lastEval, convert(datetime, '18991230')),
	@lastModifyDate = IsNUll(LastModify,convert(datetime, '18991230')) 
FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

-- Append "," to @ncctcountry at the end

Select @NCCTCountry = ',' + @NCCTCountry + ','

INSERT INTO @TT(Account, Accountowner, OpenDate, CountryOfOrigin)
SELECT a.Id, b.Cust, CASE IsNull(@UseCreateDate, 0) WHEN 1 THEN a.CreateDate ELSE a.OpenDate END, c.CountryOforigin
	FROM Account a (NOLOCK) 
	inner join AccountOwner b
	on a.Id = b.Account and
		b.Relationship in (	SELECT [Code] FROM RelationshipType WHERE PrimaryRelation = 1)
	 inner join Customer c on
	  b.Cust = c.Id  
	WHERE (CASE IsNull(@UseCreateDate, 0) WHEN 1 THEN a.CreateDate ELSE a.OpenDate END > @LastEvalDate or 
		(@lastEvalDate < @lastModifyDate and 
		 CASE IsNull(@UseCreateDate, 0) WHEN 1 THEN a.CreateDate ELSE a.OpenDate END > @lastModifydate)
		) AND  
	CHARINDEX(',' + c.CountryOforigin + ',', @NCCTCountry) > 0


IF @testAlert = 1 BEGIN
	INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT, IsTest) 
	SELECT @WLCODE, 'Account: ''' + Account + ''' owned by: ''' + Accountowner + 
			''' belongs to NCCT Country: ''' + CountryOfOrigin + 
			'''.' , 0, 
		GETDATE(), NULL, NULL, Accountowner, Account, 1
  	FROM @TT 
		
	SELECT @STAT = @@ERROR	
	IF @STAT <> 0  GOTO ENDOFPROC
END ELSE BEGIN
	IF @WLTYPE = 0 BEGIN
	  INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT) 
	  SELECT @WLCODE, 'Account: ''' + Account + ''' owned by: ''' + Accountowner + 
			''' belongs to NCCT Country: ''' + CountryOfOrigin + 
			'''.', 0, 
		GETDATE(), NULL, NULL, Accountowner, Account 
   	    FROM @TT 
	  SELECT @STAT = @@ERROR	

	  IF @STAT <> 0  GOTO ENDOFPROC
	END ELSE IF @WLTYPE = 1 BEGIN
		Select @wrkdate = getdate()
		Exec @BookDate =  BSA_CvtDateToLong @wrkdate
		INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			WLCODE, WLDESC, CREATETIME )
		SELECT	NULL, @BookDate, Accountowner, Account,
			NULL, 'RULE', NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, 0, 0, 0, 0, 
			NULL, NULL, NULL, NULL, 0, NULL, NULL,
			@WLCODE, 'Account: ''' + Account + ''' owned by: ''' + Accountowner + 
			''' belongs to NCCT Country: ''' + CountryOfOrigin + 
			'''.' , GETDATE() 
		FROM @TT 
	
		SELECT @STAT = @@ERROR	
		IF @STAT <> 0  GOTO ENDOFPROC
	END
		
END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_UAcctRskCty
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_UAcctRskCty
RETURN @stat
GO
