/****** Object:  StoredProcedure [dbo].[USR_AcctXCaY]    Script Date: 07/21/2017 15:20:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[USR_AcctXCaY] (@WLCode SCode, @testAlert INT, 
	@day  int,
	@CTRAmount MONEY)
AS

/* RULE AND PARAMETER DESCRIPTION
	Detects accounts that have cash transactions that exceed a designated
	dollar amount within a specified number of days. Designed to be run Post-EOD.
	@day = No of days activity to be considered


	@CTRAmount the threshold amount 

*/
/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@MINDATE INT
	
DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME
Declare @CurrDate 	GenericDate
Declare @wrkDate 	Datetime
Declare @txt 		VARCHAR(1000)

DECLARE	@TT TABLE (
	Account varchar(35),
	BookDate INT, 
	RecvPay INT
)

DECLARE	@TTSum TABLE (
	Account varchar(35),
	BookDate INT, 
	RecvPay INT,
	TotalSum Money
)

SET NOCOUNT ON
SET @stat = 0

--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_AcctXCaY
ELSE
	-- Already in a transaction
	SAVE TRAN USR_AcctXCaY
/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @description = [Desc], @WLType = WLType  
FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

--a week to to today
SET @minDate = dbo.ConvertSqlDateToInt(
	DATEADD(d, -1 * @day, CONVERT(VARCHAR, GETDATE())))

INSERT INTO @TT (Account, BookDate, RecvPay)
  SELECT Account, BookDate, RecvPay
     FROM ActivityHist (NOLOCK)
       WHERE bookdate > @minDate AND CashTran = 1 AND Account IS NOT NULL
        GROUP BY BookDate, Account, RecvPay

SELECT @STAT = @@ERROR 
IF @stat <> 0 GOTO EndOfProc

INSERT INTO @TTSum (Account, BookDate, RecvPay, TotalSum)
 Select t.Account, t.Bookdate, t.RecvPay, Sum(a.BaseAmt)
   FROM ActivityHist a (NOLOCK), @TT t
	WHERE  t.bookDate >= a.BookDate AND 
		a.BookDate > dbo.ConvertSQLDatetoInt(
		DATEADD(d, -1 * @day, CONVERT(VARCHAR, t.bookdate))) AND
		a.Account= t.Account AND 
		a.RecvPay = t.RecvPay AND
		cashtran = 1
	  Group By t.Account, t.BookDate, t.RecvPay
	  Having SUM(a.BaseAmt) >= @CTRAmount	 

SELECT @STAT = @@ERROR 
IF @stat <> 0 GOTO EndOfProc


IF @testAlert = 1 BEGIN
	SELECT @STARTALRTDATE = GetDate()
	INSERT INTO Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
			LastModify, Cust, Account, IsTest) 
	  Select @WLCode, 'Account:  ''' + t.Account 
		+ ''' had cash transactions of ''' + convert(varchar, t.TotalSum)
		+ ''' total amount on bookdate ''' + CONVERT(VARCHAR, t.Bookdate)  
		+ ''' in ' + Case When t.Recvpay = 1 Then 'deposit' Else 'withdrawal' End 
		+ ''' over a period of ' + CONVERT(VARCHAR, @day) + ' days.', 0, 
		GetDate(), Null, Null, (select top 1 cust from AccountOwner where account = t.Account), t.Account , 1
	  From @TTSum t 
	   
	Select @STAT = @@error	
	Select @ENDALRTDATE = GetDate()

	if @STAT <> 0  GOTO ENDOFPROC

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT distinct 'Alert', AlertNo, TRANNO 
		  FROM ACTIVITYHIST a (NOLOCK) 
		    INNER JOIN Alert On
		     a.Account = Alert.Account And
		     Alert.WLCode = @WLCode and
		      a.RecvPay = Case when Alert.[Desc] like '%deposit%' then 1
			else 2 end and
		    Alert.CreateDate Between @STARTALRTDATE AND @ENDALRTDATE
		Where convert(int,Rtrim(substring(Alert.[Desc], charindex('bookdate', Alert.[Desc], 1)+10 ,8)))
			>= a.Bookdate and a.BookDate >  dbo.ConvertSQLDatetoInt(
			DATEADD(d, -1 * @day, Rtrim(substring(Alert.[Desc], charindex('bookdate', Alert.[Desc], 1)+10 ,8)))) 
		  And a.CashTran = 1
	SELECT @STAT = @@ERROR 
END ELSE BEGIN	
	IF @WLTYPE = 0 BEGIN
	    SELECT @STARTALRTDATE = GetDate()
	
            INSERT INTO Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
			LastModify, Cust, Account) 
	    Select @WLCode, 'Account:  ''' + t.Account 
		+ ''' had cash transactions of ''' + convert(varchar, t.TotalSum)
		+ ''' total amount on bookdate ''' + CONVERT(VARCHAR, t.Bookdate)  
		+ ''' in ' + Case When t.Recvpay = 1 Then 'deposit' Else 'withdrawal' End 
		+ ''' over a period of ' + CONVERT(VARCHAR, @day) + ' days.', 0, 
		GetDate(), Null, Null, (select top 1 cust from AccountOwner where account = t.Account), t.Account 
	     From @TTSum t 
		   
	    Select @STAT = @@error	
	    Select @ENDALRTDATE = GetDate()
	
	    if @STAT <> 0  GOTO ENDOFPROC
	END ELSE IF @WLTYPE = 1 BEGIN
		select @wrkDate = GetDate()
		Exec @CurrDate = BSA_CvtDateToLong @wrkDate
		SELECT @STARTALRTDATE = GetDate()
		insert into SuspiciousActivity (ProfileNo, BookDate, Cust, Account, 
			Activity, SuspType, StartDate, EndDate, RecurType, 
			RecurValue, ActCurrReportAmt, ActInActCnt, ActOutActCnt, 
			ActInActAmt, ActOutActAmt, CurrReportAmt, ExpAvgInActCnt, 
			ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt, InCntTolPerc, 
			OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr, ReviewState, 
			ReviewTime, ReviewOper, App, AppTime, AppOper, 
			WLCode, WLDesc, CreateTime )
		Select	Null, @CurrDate, (select top 1 cust from AccountOwner where account = t.Account), t.Account,
			Null, 'Rule', Null, Null, Null, Null,
			Null, Null, Null, Null, Null, Null, Null, Null,
			Null, Null, 0, 0, 0, 0, 
			Null, Null, Null, Null, 0, Null, Null,
			@WLCode, 'Account:  ''' + t.Account 
		    + ''' had cash transactions of ''' + convert(varchar, t.TotalSum)
		    + ''' total amount on bookdate ''' + CONVERT(VARCHAR, t.Bookdate)  
		    + ''' in ' + Case When t.Recvpay = 1 Then 'deposit' Else 'withdrawal' End 
		    + ''' over a period of ' + CONVERT(VARCHAR, @day) + ' days.', GetDate() 
		From @TTSum t 
		
		Select @STAT = @@error	
		Select @ENDALRTDATE = GetDate()
		if @STAT <> 0  GOTO ENDOFPROC
	END
	IF @WLTYPE = 0 BEGIN
	    INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	      SELECT distinct 'Alert', AlertNo, TRANNO 
		FROM ACTIVITYHIST a (NOLOCK) 
		  INNER JOIN Alert On
		     a.Account = Alert.Account And
		    Alert.WLCode = @WLCode and
		    a.RecvPay = Case when Alert.[Desc] like '%deposit%' then 1
			else 2 end and
		    Alert.CreateDate Between @STARTALRTDATE AND @ENDALRTDATE
		Where  convert(int,Rtrim(substring(Alert.[Desc], charindex('bookdate', Alert.[Desc], 1)+10 ,8)))
			>= a.Bookdate and a.BookDate >  dbo.ConvertSQLDatetoInt(
			DATEADD(d, -1 * @day, Rtrim(substring(Alert.[Desc], charindex('bookdate', Alert.[Desc], 1)+10 ,8)))) 
		  AND a.CashTran = 1

	    SELECT @STAT = @@ERROR 

	END ELSE IF @WLTYPE = 1 BEGIN
	  INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	     SELECT distinct 'SUSPACT', RecNo, TRANNO 
		FROM ACTIVITYHIST a (NOLOCK) 
		  INNER JOIN SuspiciousActivity s On
    		     a.Account = s.Account And
		     s.WLCode = @WLCode and
		     a.RecvPay = Case when s.[WLDesc] like '%deposit%' then 1
			else 2 end and
		     s.CreateTime Between @STARTALRTDATE AND @ENDALRTDATE
		Where convert(int,Rtrim(substring(s.[WLDesc], charindex('bookdate', 
			s.[WLDesc], 1)+10 ,8)))	>= a.Bookdate and 
			a.BookDate >  dbo.ConvertSQLDatetoInt(
			DATEADD(d, -1 * @day, Rtrim(substring(s.[WLDesc], charindex('bookdate', 
				s.[WLDesc], 1)+10 ,8)))) 
 		    AND a.CashTran = 1
	  SELECT @STAT = @@ERROR 
	END
END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_AcctXCaY
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_AcctXCaY

RETURN @stat
GO
