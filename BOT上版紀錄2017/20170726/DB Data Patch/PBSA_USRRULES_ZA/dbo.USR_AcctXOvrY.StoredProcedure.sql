/****** Object:  StoredProcedure [dbo].[USR_AcctXOvrY]    Script Date: 07/21/2017 15:20:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USR_AcctXOvrY] (
	@WLCode 		SCode, 
	@TestAlert 		INT,
	@activityTypeList 	varchar(8000), 
	@day 			INT, 
	@minimumAmount 		MONEY, 
	@AccountList 		VARCHAR(8000), 
	@BranchList 		varchar(8000), 
	@DeptList 		varchar(8000), 
	@CorrespondentBankOnly 	VARCHAR(5)
) AS
/* RULE PARAMETERS -  THESE MUST BE SET ACCORDING TO THE RULE REQUIREMENT*/

/* RULE AND PARAMETER DESCRIPTION
	Detects accounts that had transactions using a specified 
	activity type that exceeded a designated dollar amount over 
	a period of specified days.  Designed to be run Post-EOD. 

	@activityTypeList = Type of activity to be checked   
	@day = No of days activity to be considered
	@minimumAmount= The threshold amount 
	@AccountList = List of Accounts to be checked (Comma Separated or '-ALL-')
        @BranchList  = List of Branches to be checked (Comma Separated or '-ALL-')
        @DeptList    = List of Department to be checked (Comma Separated or '-ALL-')
	@CorrespondentBankOnly = If only Correspondent Banks have to be detected ('Yes' or 'No')
*/

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@MINDATE INT

DECLARE @cur CURSOR
/* Cursor Variables */
DECLARE @account OBJECTID
DECLARE @bookDate INT
DECLARE @recvPay INT
DECLARE @tmpDate INT
DECLARE @totAmt MONEY

-- Temporary table of Activity Types that have not been specified as Exempt
DECLARE @ActType TABLE (
	Type	INT
)
SET NOCOUNT ON
SET @stat = 0

/*****************************************/
	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE,@StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) Then
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 Then
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode
	
	/******************************************/
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_AcctXOvrY
ELSE
	-- Already in a transaction
	SAVE TRAN USR_AcctXOvrY
/* End standard stored procedure transaction header */

--a week to to today
	SET @minDate = dbo.ConvertSqlDateToInt(
	DATEADD(d, -1 * @day, CONVERT(VARCHAR, @startDate)))
	
	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
	SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)
	SELECT @AccountList = dbo.BSA_fnListParams(@AccountList)

	IF LTRIM(RTRIM(@CorrespondentBankOnly)) = 'Yes'
	        SELECT @CorrespondentBankOnly = 1
	ELSE
	        SELECT @CorrespondentBankOnly = NULL

	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

declare @SusCust varchar(35)
	
	SET @CUR = CURSOR FAST_FORWARD FOR 
	SELECT 	ActHist.BookDate, ActHist.Account, ActHist.RecvPay  
	FROM 	ActivityHist ActHist
	INNER	JOIN Customer Cust ON ActHist.Cust = Cust.[Id] 
	INNER	JOIN Account Acct(NOLOCK) ON ActHist.Account = Acct.[Id]
	INNER	JOIN @ActType Act ON Act.Type = ActHist.Type
	WHERE 	ActHist.bookdate >= @MINDATE  AND ActHist.bookdate <= dbo.ConvertSqlDateToInt(@startDate) 
	AND 	ActHist.Account IS NOT NULL
	AND 	(ISNULL(@AccountList, '') = '' OR
	     	CHARINDEX(',' + ltrim(rtrim(ActHist.Account)) + ',', @AccountList ) > 0)
	AND 	(ISNULL(@CorrespondentBankOnly, '') = '' OR
		Cust.CorrBankRelation = @CorrespondentBankOnly)
	AND 	(ISNULL(@BranchList, '') = '' OR
	     	CHARINDEX(',' + ltrim(rtrim(acct.OwnerBranch)) + ',', @BranchList ) > 0)
	AND 	(ISNULL(@DeptList, '') = '' OR 
	     	CHARINDEX(',' + ltrim(rtrim(acct.OwnerDept)) + ',', @DeptList ) > 0)
	GROUP 	BY ActHist.BookDate, ActHist.Account, ActHist.RecvPay

	OPEN @CUR
	FETCH NEXT FROM @CUR INTO @bookDate, @account, @recvPay 
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		SET @tmpDate = dbo.ConvertSQLDatetoInt(
			DATEADD(d, -1 * @day, CONVERT(VARCHAR,@bookdate)))

		Select 	@totAmt = SUM(BaseAmt) 
		FROM 	ActivityHist ActHist
		INNER	JOIN Customer Cust ON ActHist.Cust = Cust.[Id]
		INNER	JOIN Account Acct(NOLOCK) ON ActHist.Account = Acct.[Id]
		INNER	JOIN @ActType Act ON Act.Type = ActHist.Type
		WHERE 	 @bookDate >= ActHist.BookDate 
		AND 	ActHist.BookDate >= @tmpDate  
		AND 	@recvPay = ActHist.recvPay AND @account = ActHist.Account AND
			ActHist.Account IS NOT NULL 
		AND 	(ISNULL(@AccountList, '') = '' OR
			     CHARINDEX(',' + ltrim(rtrim(ActHist.Account)) + ',', @AccountList ) > 0) AND
			(ISNULL(@CorrespondentBankOnly, '') = '' OR
				Cust.CorrBankRelation = @CorrespondentBankOnly) AND
			(ISNULL(@BranchList, '') = '' OR
			     CHARINDEX(',' + ltrim(rtrim(acct.OwnerBranch)) + ',', @BranchList ) > 0)  AND
			(ISNULL(@DeptList, '') = '' OR 
			     CHARINDEX(',' + ltrim(rtrim(acct.OwnerDept)) + ',', @DeptList ) > 0)
		Having 	SUM(ActHist.BaseAmt) >= @minimumAmount

		IF @@ROWCOUNT > 0 
		BEGIN					
			IF @CorrespondentBankOnly = 1
				SET @desc = 'Account:  ''' + @account 
					+ '''(Correspondent Bank Account) had transfers ''' 
					+ CONVERT(VARCHAR, @totAmt)  + ''' on bookdate ''' 
					+ CONVERT(VARCHAR, @bookdate) + ''' in '
			ELSE
				SET @desc = 'Account:  ''' + @account 
					+ ''' had transfers ''' 
					+ CONVERT(VARCHAR, @totAmt)  + ''' on bookdate ''' 
					+ CONVERT(VARCHAR, @bookdate) + ''' in '

			if @recvpay = 1 
				SET  @desc = @desc
					+ 'deposit over a period of ' + CONVERT(VARCHAR, @day) + ' days.'
			else
				SET  @desc = @desc 
					+ 'withdrawal over a period of ' + CONVERT(VARCHAR, @day) + ' days'

			IF @testAlert = 1 
			BEGIN
				EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
					NULL, @account, 1		
				IF @stat <> 0 GOTO EndOfProc
				INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
					SELECT 	'Alert', @ID, ActHist.TRANNO 
					FROM 	ActivityHist ActHist
					INNER	JOIN Customer Cust ON ActHist.Cust = Cust.[Id] 
					INNER	JOIN Account Acct(NOLOCK) ON ActHist.Account = Acct.[Id]
					INNER	JOIN @ActType Act ON Act.Type = ActHist.Type
					WHERE 	@bookDate >= ActHist.BookDate AND 
						ActHist.BookDate >= @tmpDate  AND 
						@recvPay = ActHist.recvPay AND 
						@account = ActHist.Account AND
						ActHist.Account IS NOT NULL
						AND (ISNULL(@AccountList, '') = '' OR 
						     CHARINDEX(',' + ltrim(rtrim(ActHist.Account)) + ',', @AccountList ) > 0) AND
						(ISNULL(@CorrespondentBankOnly, '') = '' OR
							Cust.CorrBankRelation = @CorrespondentBankOnly) AND
						(ISNULL(@BranchList, '') = '' OR
						     CHARINDEX(',' + ltrim(rtrim(acct.OwnerBranch)) + ',', @BranchList ) > 0)  AND
						(ISNULL(@DeptList, '') = '' OR 
						     CHARINDEX(',' + ltrim(rtrim(acct.OwnerDept)) + ',', @DeptList ) > 0)

				SELECT @STAT = @@ERROR 
				IF @stat <> 0 GOTO EndOfProc

			END 
			ELSE 
			BEGIN		    
				IF @WLType = 0 
				BEGIN
					EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
						NULL, @account		
					IF @stat <> 0 GOTO EndOfProc
					INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
						SELECT 	'Alert', @ID, ActHist.TRANNO 
						FROM 	ActivityHist ActHist
						INNER	JOIN Customer Cust ON ActHist.Cust = Cust.[Id]
						INNER	JOIN Account Acct(NOLOCK)ON ActHist.Account = Acct.[Id]
						INNER	JOIN @ActType Act ON Act.Type = ActHist.Type
						WHERE 	@bookDate >= ActHist.BookDate AND 
							ActHist.BookDate >= @tmpDate  AND 
							@recvPay = ActHist.recvPay AND @account = ActHist.Account AND
							ActHist.Account IS NOT NULL
							AND (ISNULL(@AccountList, '') = '' OR
							     CHARINDEX(',' + ltrim(rtrim(ActHist.Account)) + ',', @AccountList ) > 0) AND
							(ISNULL(@CorrespondentBankOnly, '') = '' OR
								Cust.CorrBankRelation = @CorrespondentBankOnly) AND
							(ISNULL(@BranchList, '') = '' OR
							     CHARINDEX(',' + ltrim(rtrim(acct.OwnerBranch)) + ',', @BranchList ) > 0)  AND
							(ISNULL(@DeptList, '') = '' OR 
							     CHARINDEX(',' + ltrim(rtrim(acct.OwnerDept)) + ',', @DeptList ) > 0)

					SELECT @STAT = @@ERROR 
					IF @stat <> 0 GOTO EndOfProc
				END 
				ELSE IF @WLType = 1 
				BEGIN	
			
                    select top 1 @SusCust = cust from accountowner where account = @account;

					EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT, 
						@WLCode, @desc, @bookdate, 
                        @SusCust, 
                        @account
					IF @stat <> 0 GOTO EndOfProc
					INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
						SELECT 	'SUSPACT', @ID, ActHist.TRANNO 
						FROM 	ActivityHist ActHist
						INNER	JOIN Customer Cust ON ActHist.Cust = Cust.[Id]
						INNER	JOIN Account Acct(NOLOCK) ON ActHist.Account = Acct.[Id]
						INNER	JOIN @ActType Act ON Act.Type = ActHist.Type
						WHERE  @bookDate >= ActHist.BookDate AND 
							ActHist.BookDate >= @tmpDate  AND 
							@recvPay = ActHist.recvPay AND @account = ActHist.Account AND
							ActHist.Account IS NOT NULL AND (@ActivityTypeList IS NULL OR 
							CHARINDEX(',' + CONVERT(VARCHAR, acthist.type) + ',',@ActivityTypeList) > 0)
							AND (ISNULL(@AccountList, '') = '' OR
							     CHARINDEX(',' + ltrim(rtrim(ActHist.Account)) + ',', @AccountList ) > 0) AND
							(ISNULL(@CorrespondentBankOnly, '') = '' OR
								Cust.CorrBankRelation = @CorrespondentBankOnly) AND
							(ISNULL(@BranchList, '') = '' OR
							     CHARINDEX(',' + ltrim(rtrim(acct.OwnerBranch)) + ',', @BranchList ) > 0)  AND
							(ISNULL(@DeptList, '') = '' OR 
							     CHARINDEX(',' + ltrim(rtrim(acct.OwnerDept)) + ',', @DeptList ) > 0)
					SELECT @STAT = @@ERROR 
					IF @stat <> 0 GOTO EndOfProc
				END
			END
		END
SkipRec:
	FETCH NEXT FROM @CUR INTO @bookDate, @account, @recvPay
	END

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_AcctXOvrY
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_AcctXOvrY
RETURN @stat
GO
