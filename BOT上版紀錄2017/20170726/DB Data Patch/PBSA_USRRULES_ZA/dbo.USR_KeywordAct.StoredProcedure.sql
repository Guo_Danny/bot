/****** Object:  StoredProcedure [dbo].[USR_KeywordAct]    Script Date: 07/21/2017 15:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[USR_KeywordAct](
				@WLCode SCODE, 
				@testAlert INT, 
				@KeywordList  VARCHAR(8000),
				@ActivityTypeList   VARCHAR(8000),  
				@RecvPay INT,
				@MinTotalAmount MONEY,
				@MinTranCnt INT,
				@MinTranAmt MONEY,				
				@CustomerTypeList Varchar(8000),
				@AccountTypeList Varchar(8000),	
				@LastNoOfDays INT 
	)
AS
BEGIN
	/* Rule and Parameter Description
		Performs a keyword search on Party Names/Notes/Instructions fields of archived activity table.
		An Alert or Case is generated where a match is found in the activity for a given customer/account combination.
		Designed to be run daily, weekly, monthly or quarterly on a Post-EOD Schedule.

	   @keywordList        -- List of keywords to look for. Cannot be null or blank
	   @ActivityTypeList   -- Type of activities to be detected.	 
  	   @RecvPay            -- Deposit/Withdrawal. 1 for Deposit,
                              2 for Withdrawals, 3 for both
       @MinTotalAmount     -- Rule checks if the total sum of all transactions in the specified last number
                          of days is greater than or equal to amount specified
	   @MinTranCnt		   -- Number of transactions identified below this value will not trigger an alert for the customer, anything equal to or above will cause an alert to generate
	   @MinTranAmt		   -- Minimum Amount of each transaction that ticks against the number of transaction count
	   @CustomerTypeList   -- Select customer types to be included
	   @AccountTypeList    -- Select account types to be included
       @LastNoOfDays       -- Rule will consider all activity that occurred within the specified last number of days	
	*/

/*  DECLARATIONS */
	DECLARE	@DESCRIPTION VARCHAR(2000),
		@DESC VARCHAR(2000),
		@ID INT, 
		@WLTYPE INT,
		@Stat INT,
		@TRNCNT INT
	
	DECLARE 	@wrkDate DATETIME  
    DECLARE  	@startAlrtDate DATETIME  
    DECLARE  	@endAlrtDate DATETIME  
    DECLARE  	@CurrDate GenericDate  
    DECLARE  	@MinDate INT  
	DECLARE 	@StartDate DATETIME

	DECLARE	@TT TABLE (
		Cust VARCHAR(40),
		Account VARCHAR(40),
		RecvPay INT,
		TranNO INT,
		BaseAmt MONEY)

	DECLARE	@TT1 TABLE (
		Cust VARCHAR(40),
		Account VARCHAR(40),
		RecvPay INT,
		TranNO INT,
		BaseAmt MONEY)

	DECLARE @Keyword VARCHAR(500)
	DECLARE @pos smallint

	-- Temporary table of Activity Types that have not been specified as Exempt
	DECLARE @ActType TABLE 
	(
	  Type		INT
	)

--- ********************* Begin Rule Procedure **********************************
	SET NOCOUNT ON
	SET @Stat = 0
	SET @LastNoOfDays = ABS(@LastNoOfDays)  

	-- Check for empty list  
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)

	IF(ISNULL(@customerTypeList,'') = '' OR UPPER(ISNULL(@customerTypeList,'-ALL-')) = '-ALL-')
		SELECT @customerTypeList = NULL
	ELSE
   		SELECT @customerTypeList = ',' + REPLACE(LTRIM(RTRIM(@customerTypeList)),CHAR(32),'') + ','
		
	IF(ISNULL(@accountTypeList,'') = '' OR UPPER(ISNULL(@accountTypeList,'-ALL-')) = '-ALL-')
		SELECT @accountTypeList = NULL
	ELSE
   		SELECT @accountTypeList = ',' + REPLACE(LTRIM(RTRIM(@accountTypeList)),CHAR(32),'') + ','
		
	-- Check for empty list
	IF (ISNULL(@KeywordList,'') = '') 
		RETURN @Stat
	ELSE
		SELECT @KeywordList = LTRIM(RTRIM(@KeywordList))+','

	IF (@RecvPay = 3)  
         SET @RecvPay = NULL

	/* START STANDARD STORED PROCEDURE TRANSACTION HEADER */
	SET @TRNCNT = @@TRANCOUNT	-- SAVE THE CURRENT TRANCOUNT
	IF @TRNCNT = 0
		-- TRANSACTION HAS NOT BEGUN
		BEGIN TRAN USR_KeywordAct
	ELSE
		-- ALREADY IN A TRANSACTION
		SAVE TRAN USR_KeywordAct
	/* END STANDARD STORED PROCEDURE TRANSACTION HEADER */


	/*  STANDARD RULES HEADER */
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE, @StartDate = 
		CASE 	
		WHEN UseSysDate in (0,1) THEN
			-- use System date
			GETDATE()
		WHEN UseSysDate = 2 THEN
			-- use business date
			(SELECT BusDate FROM dbo.SysParam)
		ELSE
			GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode


	--Get all the non-exempted activity types
	INSERT INTO @ActType
	SELECT 	Type  FROM vwRuleNonExmActType
	WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	SELECT @MinDate = DBO.CONVERTSQLDATETOINT(  
		DATEADD(D, -1 * @LastNoOfDays, CONVERT(VARCHAR, @StartDate)))  

	-- Keyword list is converted to table form(rows) using intermediate XML.

	SET @pos = 1
	WHILE @pos <  LEN(@KeywordList)

	BEGIN
		SELECT @keyword = LTRIM(RTRIM(SUBSTRING(@KeywordList,@pos,CHARINDEX(',',@KeywordList, @pos)- @pos)))

		IF (LEN(LTRIM(RTRIM(@keyword))) >0)
			INSERT INTO @TT1 ( Cust, Account, TranNo,RecvPay,BaseAmt)
				SELECT Cust, Account, tranno,RecvPay,BaseAmt
					FROM ActivityHist (NOLOCK)
					INNER JOIN @ActType Act ON Act.Type = ActivityHist.Type
					INNER JOIN Customer Cus ON Cus.Id = ActivityHist.Cust
					INNER JOIN Account Acc ON Acc.Id = ActivityHist.Account
					WHERE  	
						(Bene LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						ByOrder LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						BeneBank LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						ByOrderBank LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						Intermediary LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						Intermediary2 LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						Intermediary3 LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						Instructions LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR						
						Intermediary4 LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						ActivityHist.Notes LIKE '%'+LTRIM(RTRIM(@Keyword))+'%')
						AND ((ISNULL(@customerTypeList, '') = '' OR
						CHARINDEX(',' + LTRIM(RTRIM(Cus.Type)) + ',', @customerTypeList ) > 0))
						AND ((ISNULL(@accountTypeList, '') = '' OR
						CHARINDEX(',' + LTRIM(RTRIM(Acc.Type)) + ',', @accountTypeList ) > 0))	
						AND ActivityHist.BaseAmt >= @MinTranAmt
						AND BookDate >= @MinDate AND BookDate < dbo.ConvertSqlDateToInt(@StartDate)  
				  		AND RecvPay = ISNULL(@RecvPay, Recvpay)  
						
		SELECT @pos = CHARINDEX(',',@KeywordList, @pos)+1

	END

	INSERT INTO @TT SELECT DISTINCT * FROM @TT1

	IF (@testAlert = 1  OR @WLType = 0)
	BEGIN

		SELECT @STARTALRTDATE = GETDATE() 

		INSERT INTO Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
				LastModify, Cust, Account, IsTest) 
			SELECT @WLCode, 'Customer ''' + ISNULL(CUST,'') + ''' and Account ''' + ISNULL(Account,'')   
					+ ''' had ' + CONVERT(VARCHAR,Count(TranNo))  
					+ CASE WHEN RecvPay = 1 THEN ' incoming'
							WHEN RecvPay = 2 THEN ' outgoing'
					END
					 + ' transactions totaling $' + CONVERT(VARCHAR,SUM(BaseAmt)) +   
					 ' BETWEEN the period '+ CONVERT(VARCHAR, @MinDate) + 'and '  
					+ CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(@StartDate)) +   
					  ' WHERE specified keywords were matched ' ,   
					0, GETDATE(), Null, Null, Cust, Account, @testAlert  
				FROM @TT   
				WHERE RecvPay = ISNULL(@RecvPay,RecvPay)
				GROUP BY Cust, Account,RecvPay  
				HAVING (@MinTotalAmount = -1 OR SUM(BaseAmt) >= @MinTotalAmount)
				AND COUNT(TranNo) >= @MinTranCnt

		SELECT @Stat = @@ERROR	

		IF @Stat <> 0  GOTO ENDOFPROC

		SELECT @ENDALRTDATE = GETDATE() 

		INSERT INTO SASActivity (ObjectType, ObjectID, TranNo)
			SELECT 'Alert', AlertNo, TranNo   
				FROM @TT t, Alert WHERE  
				IsNull(t.Account,'') = IsNull(Alert.Account,'') AND 
				t.RecvPay = CASE WHEN Alert.[Desc] LIKE '%incoming%' THEN 1
								WHEN Alert.[Desc] LIKE '%outgoing%' THEN 2
							END 
				AND t.cust = Alert.cust AND  
				Alert.WLCode = @WLCode AND  
				Alert.CreateDate BETWEEN @startAlrtDate AND @endAlrtDate 

		SELECT @Stat = @@ERROR
		IF @Stat <> 0  GOTO ENDOFPROC
	END 
	ELSE 
		IF @WLTYPE = 1 
		BEGIN
			SELECT @wrkDate = GETDATE() 
			EXEC @CurrDate = BSA_CvtDateToLong @wrkDate
			SELECT @STARTALRTDATE = GETDATE() 

			declare @key int

			INSERT INTO SuspiciousActivity (ProfileNo, BookDate, Cust, Account, 
				Activity, SuspType, StartDate, EndDate, RecurType, 
				RecurValue, ActCurrReportAmt, ActInActCnt, ActOutActCnt, 
				ActInActAmt, ActOutActAmt, CurrReportAmt, ExpAvgInActCnt, 
				ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt, InCntTolPerc, 
				OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr, ReviewState, 
				ReviewTime, ReviewOper, App, AppTime, AppOper, 
				WLCode, WLDesc, CreateTime )
			SELECT	Null, @CurrDate, cust, Account,
				Null, 'Rule', Null, Null, Null, Null,
				Null, Null, Null, Null, Null, Null, Null, Null,
				Null, Null, 0, 0, 0, 0, 
				Null, Null, Null, Null, 0, Null, Null,
				@WLCode,'Customer ''' + ISNULL(CUST,'') + ''' and Account ''' + ISNULL(Account,'')      
			   + ''' had ' + convert(varchar,Count(TranNo))  
			   + CASE WHEN RecvPay = 1 THEN ' incoming' 
				WHEN RecvPay = 2 THEN 'outgoing'
				END + ' transactions totaling $' + CONVERT(VARCHAR,SUM(BaseAmt)) +   
			   ' BETWEEN the period '+ CONVERT(VARCHAR, @MinDate) + ' and '  
			   + CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(@StartDate)) +   
			   ' where specified keywords were matched ' ,   
			   GetDate()   
		   From @TT   
			WHERE RecvPay  = ISNULL(@RecvPay,RecvPay)
		   GROUP BY Cust, ACCOUNT,RecvPay  
		   HAVING (@MinTotalAmount = -1 OR SUM(BaseAmt) >= @MinTotalAmount  )
		   AND COUNT(TranNo) >= @MinTranCnt
			set @key = @@IDENTITY

			SELECT @ENDALRTDATE = GETDATE()

			INSERT INTO SASActivity (ObjectType, ObjectID, TranNo)
			SELECT 'SUSPACT', s.RecNo, t.TranNo   
			  FROM @TT t, SuspiciousActivity s 
             WHERE s.recno = @key;

	
			SELECT @Stat = @@ERROR	
			
			if @Stat <> 0  GOTO ENDOFPROC
		END
	
	ENDOFPROC:
	IF (@Stat <> 0) BEGIN 
		ROLLBACK TRAN USR_KeywordAct
		RETURN @Stat
	END	

	IF @TRNCNT = 0
		COMMIT TRAN USR_KeywordAct
		RETURN @Stat
END
GO
