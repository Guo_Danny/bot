/****** Object:  StoredProcedure [dbo].[USR_PayMulToBen]    Script Date: 07/21/2017 15:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[USR_PayMulToBen](@WLCode SCode, @testAlert INT, 
	@ActivityTypeList Varchar(8000),
	@CountryList Varchar(8000),
	@AccountTypeList Varchar(8000),
	@minimumAmount money,
	@maximumAmount money,
	@minimumCount int,
	@day INT,
	@recvpay INT,
	@branchList VARCHAR(8000),
	@deptList VARCHAR(8000)
	)
AS
	/* RULE AND PARAMETER DESCRIPTION
	Detects multiple originators sending transactions 
	using specified activity types to a single Beneficiary over a 
	specified time period and the total of these transactions is 
	within a designated dollar amount range. The alert/case will display 
	the account information in the description since the account may not 
	be held at the processing institution.  Filtered by country and account types.
	Designed to be run Post-EOD.
	
		@ActivityTypeList =  Comma separated list of activity types to be considered
		@CountryList =  Comma separated list of country codes to be considered
		@AccountTypeList =  Comma separated list of account types to be considered
		@minimumAmount = Threshold amt of deposits or withdrawals
		@maximumAmount = Maximum amount of deposits or withdrawals
		@minimumCount = Threshold number of deposits or withdrawals
		@day  Last no of days activity to be considered
		@recvpay Deposit/Withdrawal type of transactions to be considered
		@branchList = comma separated list of branches to be considered
		@deptList = comma separated list of departments to be considered
	*/
		
	/*  Declarations */
	DECLARE	@description VARCHAR(2000),
		@desc VARCHAR(2000),
		@Id INT, 
		@WLType INT,
		@stat INT,
		@minDate INT,
		@tranAmt MONEY,
		@tranCnt INT,
		@trnCnt INT
	DECLARE @STARTALRTDATE  DATETIME
	DECLARE @ENDALRTDATE    DATETIME
	
	DECLARE	@TT TABLE (
		tranAmt MONEY,
		tranCnt INT,
		Bene  VARCHAR(40)
	)
	-- Temporary table of Activity Types that have not been specIFied as Exempt
		DECLARE @ActType TABLE (
			Type	INT
		)
	SET NOCOUNT ON
	SET @stat = 0
	/*****************************************/
	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE,@StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) Then
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 Then
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode
	
	/******************************************/
	--- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */
	SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN USR_PayMulToBen
	ELSE
		-- Already in a transaction
		SAVE TRAN USR_PayMulToBen
	/* End standard stored procedure transaction header */
	
	SET @minDate = dbo.ConvertSqlDateToInt(
			DATEADD(d, -1 * @day, @startDate))
		
	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	SELECT @CountryList = dbo.BSA_fnListParams(@CountryList)
	SELECT @AccountTypeList = dbo.BSA_fnListParams(@AccountTypeList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
	SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)

	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	INSERT INTO @tt (tranAmt, tranCnt, Bene)
		SELECT 	DISTINCT SUM(BaseAmt) tranAmt, COUNT(tranNo) tranCnt, a.Bene
		FROM 	ActivityHist a (NOLOCK)
		INNER	JOIN Account ac ON a.Account = ac.Id
		INNER	JOIN @ActType Act ON Act.Type = A.Type
		WHERE  	a.bookdate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate) AND 
			a.recvpay = @recvPay AND 
			a.byOrder IS NOT NULL and 
			a.bene IS NOT NULL AND  
			(@DeptList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, A.Dept) + ',',@DeptList) > 0) AND
			(@BranchList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, A.Branch) + ',',@BranchList) > 0) AND
			(@AccountTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, ac.type) + ',',@AccountTypeList) > 0) AND
			(@CountryList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, a.BeneCountry) + ',',@CountryList) > 0 OR
						 CHARINDEX(',' + CONVERT(VARCHAR, a.ByOrderCountry) + ',',@CountryList) > 0 )
		GROUP 	BY  a.Bene
		HAVING 	(SUM(BaseAmt) >= @minimumAmount AND 
			(SUM(BaseAmt) <= @maximumAmount OR @maximumAmount = -1) AND
			count(tranno) >= @minimumCount) AND 
			COUNT(DISTINCT a.ByOrder) > 1
		
	DECLARE	@cur CURSOR
	DECLARE @bene LONGNAME,
		@bookdate INT
	
	SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())
	
	SET @cur = CURSOR FAST_FORWARD FOR 
	SELECT a.tranAmt, a.tranCnt, a.bene
	FROM @tt a 
	
	OPEN @cur 
	FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @bene
	
	WHILE @@FETCH_STATUS = 0 
	BEGIN
	
		SET @desc = 'Bene:  ''' + @bene + ''' is receiving from '
			+ 'multiple byorder ' 
			+ 'amount: ' + CONVERT(VARCHAR, @tranAmt) + ' over '
			+ CONVERT(VARCHAR, @tranCnt) + ' transactions'
			+ ' over a period of ' + CONVERT(VARCHAR, @day) + ' days'
	
		IF @testAlert = 1 
		BEGIN
			EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
				NULL, NULL, 1
			IF @stat <> 0 GOTO EndOfProc
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	'Alert', @ID, TRANNO 
				FROM 	ACTIVITYHIST A (NOLOCK)
				INNER	JOIN  Account ac ON A.Account = ac.Id
				INNER	JOIN @ActType Act ON Act.Type = A.Type
				WHERE  	a.bookdate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate)  AND 
					a.recvpay = @recvPay AND 
					a.byOrder IS NOT NULL and 
					a.bene =@bene  AND
					(@DeptList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, A.Dept) + ',',@DeptList) > 0) AND
					(@BranchList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, A.Branch) + ',',@BranchList) > 0) AND
					(@AccountTypeList IS NULL OR 
					CHARINDEX(',' + CONVERT(VARCHAR, ac.type) + ',',@AccountTypeList) > 0) AND
					(@CountryList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, a.BeneCountry) + ',',@CountryList) > 0 OR
								 CHARINDEX(',' + CONVERT(VARCHAR, a.ByOrderCountry) + ',',@CountryList) > 0 )
			SELECT @STAT = @@ERROR 
				IF @STAT <> 0 GOTO ENDOFPROC
		END 
		ELSE 
		BEGIN
			IF @WLType = 0 
			BEGIN
				EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
					NULL, NULL, 0
				IF @stat <> 0 GOTO EndOfProc
				INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
					SELECT 	'Alert', @ID, TRANNO 
					FROM 	ACTIVITYHIST A (NOLOCK)
					INNER	JOIN  Account ac ON A.Account = ac.Id
					INNER	JOIN  @ActType Act ON Act.Type = A.Type
					WHERE   a.bookdate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate)  AND 
						a.recvpay = @recvPay AND 
						a.byOrder IS NOT NULL and 
						a.bene =@bene  AND
						(@DeptList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, A.Dept) + ',',@DeptList) > 0) AND
						(@BranchList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, A.Branch) + ',',@BranchList) > 0) AND
						(@AccountTypeList IS NULL 
						OR CHARINDEX(',' + CONVERT(VARCHAR, ac.type) + ',',@AccountTypeList) > 0) AND
						(@CountryList IS NULL 
						OR CHARINDEX(',' + CONVERT(VARCHAR, a.BeneCountry) + ',',@CountryList) > 0 OR
						 CHARINDEX(',' + CONVERT(VARCHAR, a.ByOrderCountry) + ',',@CountryList) > 0 )
				SELECT @STAT = @@ERROR 
				IF @STAT <> 0 GOTO ENDOFPROC
			END 
			ELSE IF @WLType = 1 
			BEGIN	
				EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT, 
					@WLCode, @desc, @bookdate, Null, NULL
				IF @stat <> 0 GOTO EndOfProc   
				INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
					SELECT 	'SUSPACT', @ID, TRANNO 
					FROM 	ACTIVITYHIST A (NOLOCK)
					INNER	JOIN  Account ac ON A.Account = ac.Id
					INNER	JOIN @ActType Act ON Act.Type = A.Type
					WHERE  	a.bookdate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate) AND 
						a.recvpay = @recvPay AND 
						a.byOrder IS NOT NULL and 
						a.bene =@bene  AND
						(@DeptList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, A.Dept) + ',',@DeptList) > 0) AND
						(@BranchList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, A.Branch) + ',',@BranchList) > 0) AND
						(@AccountTypeList IS NULL 
						OR CHARINDEX(',' + CONVERT(VARCHAR, ac.type) + ',',@AccountTypeList) > 0) AND
						(@CountryList IS NULL 
						OR CHARINDEX(',' + CONVERT(VARCHAR, a.BeneCountry) + ',',@CountryList) > 0 OR
						 CHARINDEX(',' + CONVERT(VARCHAR, a.ByOrderCountry) + ',',@CountryList) > 0 )
				SELECT @STAT = @@ERROR 
				IF @STAT <> 0 GOTO ENDOFPROC
			END
		END
		FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @bene
	END
	
	CLOSE @cur
	DEALLOCATE @cur
	
	EndOfProc:
	IF (@stat <> 0) BEGIN 
	  ROLLBACK TRAN USR_PayMulToBen
	  RETURN @stat
	END	
	
	IF @trnCnt = 0
	  COMMIT TRAN USR_PayMulToBen
	RETURN @stat
GO
