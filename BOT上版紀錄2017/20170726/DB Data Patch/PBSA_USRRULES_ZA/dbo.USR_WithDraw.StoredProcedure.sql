/****** Object:  StoredProcedure [dbo].[USR_WithDraw]    Script Date: 07/21/2017 15:21:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[USR_WithDraw] 
(	@WLCode 	  SCODE, 
	@testAlert 	  INT,
	@ActivityTypeList VARCHAR(8000),
	@LastNoOfMonths   INT, 
	@Tolerance 	  Percentage
)
AS

	/* Rule AND Parameter DescriptiON 
	   Detects transactions of a selected activity type after cash 
		deposits, WHERE most of the deposited cash is withdrawn in a specified time 
		period. Designed to be run Post-EOD ON a monthly or quarterly schedule.
	
	   @LastNoOfMonths 	-- Rule will consider all activity that occurred in this specified 
	                       last number of months 
	   @ActivityTypeList -- Comma separated list of activity types to be considered
	   @Tolerance       -- Tolerance percentage of total amount deposited or withdrawn
	   
	*/

	/*  DECLARATIONS */
	DECLARE	@Description VARCHAR(2000),
		@Desc VARCHAR(2000),
		@ID INT, 
		@WlType INT,
		@STAT INT,
		@TrnCnt INT,
		@MinDate INT -- start date , current date is end date
		
	DECLARE @StartAlrtDate  DATETIME
	DECLARE @EndAlrtDate    DATETIME
	DECLARE @CurrDate 	GenericDate
	DECLARE @wrkDate 	DATETIME
	
	DECLARE	@TT TABLE (
		Cust VARCHAR(40),
		DepositedAmt MONEY,
		NoofCashDeposits INT
	)
	
	DECLARE @WithDrawalTT TABLE (
		Cust VARCHAR(40),
		WithDrawalAmt MONEY,
		NoOfWithDrawals INT
	)
	-- Temporary table of Activity Types that have not been specified as Exempt
	DECLARE @ActType TABLE (
		Type	INT
	)
	
	--- ********************* Begin Rule Procedure **********************************
	SET NOCOUNT ON
	SET @STAT = 0
	
	/*****************************************/
	-- Date optiONs
	-- If UseSysDate = 0 or 1 then use current/system date
	-- if UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE,@StartDate = 
		Case 	
			When UseSysDate in (0,1) Then
				-- use System date
				GETDATE()
			When UseSysDate = 2 Then
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			Else
				GETDATE()
		End
	FROM dbo.WatchList
	WHERE WLCode = @WlCode
	
	/******************************************/
	
	SET @LastNoOfMonths = ABS(@LastNoOfMonths)
	
	--if user specifies 0, then it just evaluates the current mONth
-- 	IF @LastNoOfMonths <> 0 BEGIN
-- 		SET @LastNoOfMonths = @LastNoOfMonths - 1
-- 	END

	SELECT @MinDate = DBO.CONVERTSQLDATETOINT(
			DATEADD(M, -1 * @LastNoOfMonths, CONVERT(VARCHAR, @StartDate)))
	
	
	/* START STANDARD STORED PROCEDURE TRANSACTION HEADER */
	SET @TrnCnt = @@TRANCOUNT	-- SAVE THE CURRENT TRANCOUNT
	IF @TrnCnt = 0
		-- TRANSACTION HAS NOT BEGUN
		BEGIN TRAN USR_WithDraw
	ELSE
		-- ALREADY IN A TRANSACTION
		SAVE TRAN USR_WithDraw
	/* END STANDARD STORED PROCEDURE TRANSACTION HEADER */
	
	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)

	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)
	
	INSERT INTO @TT (Cust, DepositedAmt, NoofCashDeposits)
		SELECT 	CUST, SUM(baseAmt), count(TranNo)
		FROM 	ACTIVITYHIST (NOLOCK)
		INNER	JOIN vwRuleNonExmActType ActType ON ActType.Type = ACTIVITYHIST.Type
		WHERE 	BookDate BETWEEN @MinDate AND dbo.ConvertSqlDateToInt(@startDate)
		AND 	CashTran = 1 AND RecvPay = 1
		GROUP 	BY Cust
	
	INSERT INTO @WithDrawalTT (Cust, WithDrawalAmt, NoOfWithDrawals)
		SELECT  T2.cust, SUM(T1.baseamt), COUNT(TranNo) 
		FROM 	ActivityHist T1
		INNER 	JOIN @TT T2 ON T2.Cust = T1.Cust
		INNER	JOIN @ActType ActType ON ActType.Type = T1.Type
		WHERE   T1.RecvPay = 2 
		AND 	T1.BookDate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate)
		AND 	T1.Account IN (
				SELECT 	Account FROM ActivityHist T3 
				INNER	JOIN @ActType ActType ON ActType.Type = T3.Type
				WHERE 	T3.Cust = T2.Cust
				AND  	T3.BookDate BETWEEN @MinDate AND dbo.ConvertSqlDateToInt(@startDate) 
				AND T3.CashTran = 1 AND T3.RecvPay = 1
			)
			GROUP 	BY T2.Cust, T2.DepositedAmt 
			HAVING 	SUM(T1.BaseAmt) >= FLOOR((100 - @Tolerance)* T2.DepositedAmt/100) 

	IF @testAlert = 1 
	BEGIN
		SELECT @StartAlrtDate = GETDATE()
		INSERT INTO Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
				LastModify, Cust, Account, IsTest) 
		  SELECT @WLCode, 'Cust:  ''' + t.CUST 
			+ ''' made ''' + CONVERT(VARCHAR,NoofCashDeposits)
			+ ''' cash deposit transactions of total amount ''' +
			CONVERT(VARCHAR, DepositedAmt)  + 
			''' and a total of ''' + CONVERT(VARCHAR, NoOfWithdrawals) + 
			''' withdrawal transactions totaling amount '''+ 
			CONVERT(VARCHAR,withdrawalamt) + 
			''' have been made against these accounts between the period '''+
			CONVERT(VARCHAR, @MinDate) + ''' AND ''' +
			CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(GETDATE()))+ '''' , 0, 
			GETDATE(), NULL, NULL, T.Cust, NULL, 1
		FROM 	@TT T 
		INNER 	JOIN @withdrawalTT W ON T.Cust = W.Cust

		SELECT @STAT = @@ERROR	
		SELECT @EndAlrtDate = GETDATE()
		if @STAT <> 0  GOTO ENDOFPROC
	
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TranNo)
		SELECT 	'Alert', AlertNo, TranNo 
		FROM 	ActivityHist A 
		INNER 	JOIN Alert 
		INNER 	JOIN @WithDrawalTT W ON W.Cust = Alert.Cust ON  A.Cust = W.Cust
		AND 	Alert.WLCode = @WLCode AND Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
		WHERE 	A.BookDate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate) AND A.CashTran = 1 AND A.RecvPay = 1
		
		UNION
		
		SELECT 'Alert', AlertNo, TranNo FROM Activityhist T1
		INNER	JOIN Alert ON Alert.Cust = T1.Cust
		INNER	JOIN @WithdrawalTT W ON W.Cust = Alert.Cust
		INNER	JOIN @ActType Act ON Act.Type = T1.Type	 
		WHERE   T1.Recvpay = 2 AND T1.Bookdate BETWEEN @MinDate AND dbo.ConvertSqlDateToInt(@startDate)
		AND 	T1.Account IN (
				SELECT 	Account FROM Activityhist T3 
				WHERE 	T3.Cust = W.Cust 
				AND  	T3.BookDate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate) 
				AND 	T3.CashTran = 1 AND T3.RecvPay = 1)
		AND	Alert.WLCode = @WLCode 
		AND 	Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
			
		SELECT @STAT = @@ERROR 
	END 
	ELSE 
	BEGIN
		IF @WlType = 0 
		BEGIN
			SELECT @StartAlrtDate = GETDATE()
			INSERT INTO Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
					LastModify, Cust, Account) 
			  SELECT @WLCode, 'Cust:  ''' + t.CUST 
				+ ''' made ''' + CONVERT(VARCHAR,NoofCashDeposits)
				+ ''' cash deposit transactions of total amount ''' +
				CONVERT(VARCHAR, DepositedAmt)  + 
				''' and a total of ''' + CONVERT(VARCHAR, NoOfWithdrawals) + 
				''' withdrawal transactions totaling amount '''+ 
				CONVERT(VARCHAR,withdrawalamt) + 
				''' have been made against these accounts between the period ''' +
				 CONVERT(VARCHAR, @MinDate) + ''' AND ''' +
				 CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(GETDATE()))+ '''' , 0, 
				GETDATE(), NULL, NULL, t.Cust, NULL 
			FROM @TT T 
			INNER JOIN @withdrawalTT W ON t.Cust = W.Cust
			SELECT @STAT = @@ERROR	
			SELECT @EndAlrtDate = GETDATE()
			IF @STAT <> 0  GOTO ENDOFPROC
		END 
		ELSE IF @WlType = 1 
		BEGIN
		    SELECT @wrkDate = GETDATE()
		    Exec @CurrDate = BSA_CvtDateToLONg @wrkDate
		    SELECT @StartAlrtDate = GETDATE()
		    INSERT INTO SuspiciousActivity (ProfileNo, BookDate, Cust, Account, 
				Activity, SuspType, StartDate, EndDate, RecurType, 
				RecurValue, ActCurrReportAmt, ActInActCnt, ActOutActCnt, 
				ActInActAmt, ActOutActAmt, CurrReportAmt, ExpAvgInActCnt, 
				ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt, InCntTolPerc, 
				OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr, ReviewState, 
				ReviewTime, ReviewOper, App, AppTime, AppOper, 
				WLCode, WLDesc, CreateTime )
			SELECT	NULL, @CurrDate, t.Cust, NULL,
				NULL, 'Rule', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, 'Customer:  ''' + t.CUST 

				+ ''' made ''' + CONVERT(VARCHAR,NoofCashDeposits)
				+ ''' cash deposit transactions of total amount ''' +
				CONVERT(VARCHAR, DepositedAmt)  + 
				''' and a total of ''' + CONVERT(VARCHAR, NoOfWithdrawals) + 
				''' withdrawal transactions totaling amount '''+ 
				CONVERT(VARCHAR,withdrawalamt) + 
				''' have been made against these accounts between the period ''' +
				CONVERT(VARCHAR, @MinDate) + ''' AND ''' +
				CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(GETDATE()))+ '''' , GETDATE() 
			FROM 	@TT T 
			INNER 	JOIN @withdrawalTT W ON T.Cust = W.Cust
			
			SELECT @STAT = @@ERROR	
			SELECT @EndAlrtDate = GETDATE()
			if @STAT <> 0  GOTO ENDOFPROC
		END
		
		IF @WlType = 0 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TranNo)
				SELECT 	'Alert', AlertNo, TranNo 
				FROM 	ActivityHist A
				INNER 	JOIN Alert 
				INNER 	JOIN @WithDrawalTT W ON W.Cust = Alert.Cust ON  A.Cust = W.Cust
				AND 	Alert.WLCode = @WLCode AND Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
				WHERE 	A.BookDate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate) AND A.CashTran = 1 AND A.RecvPay = 1
			UNION
				SELECT 	'Alert', AlertNo, TranNo 
				FROM 	Activityhist T1
				INNER	JOIN Alert ON Alert.Cust = T1.Cust	
				INNER	JOIN @WithdrawalTT W ON W.Cust = Alert.Cust
				INNER	JOIN @ActType Act ON Act.Type = T1.Type
				WHERE   T1.Recvpay = 2 AND T1.Bookdate BETWEEN @MinDate AND dbo.ConvertSqlDateToInt(@startDate)
				AND 	T1.Account IN (
						SELECT 	Account FROM Activityhist T3 
						WHERE 	T3.Cust = W.Cust 
						AND  	T3.BookDate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate)
						AND 	T3.CashTran = 1 AND T3.RecvPay = 1
					)
				AND 	Alert.WLCode = @WLCode 
				AND	Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
				SELECT @STAT = @@ERROR 
		END 
		ELSE IF @WlType = 1 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TranNo)
				SELECT 	'SUSPACT', RecNo, TranNo 
				FROM 	ActivityHist A
				INNER 	JOIN SuspiciousActivity 
				INNER 	JOIN @WithDrawalTT W ON W.Cust = SuspiciousActivity.Cust ON  A.Cust = W.Cust
				AND 	SuspiciousActivity.WLCode = @WLCode 
				AND	SuspiciousActivity.CreateTime BETWEEN @StartAlrtDate AND @EndAlrtDate
				WHERE 	A.BookDate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate) AND A.CashTran = 1 AND A.RecvPay = 1
			UNION
				SELECT 	'SUSPACT', RecNo, TranNo 
				FROM 	Activityhist T1
				INNER	JOIN SuspiciousActivity S
				INNER	JOIN @WithdrawalTT W ON W.Cust = S.Cust ON T1.Cust = W.Cust
				INNER	JOIN @ActType Act ON Act.Type = T1.Type
				WHERE   T1.Recvpay = 2 AND T1.Bookdate BETWEEN @MinDate AND dbo.ConvertSqlDateToInt(@startDate)
				AND 	T1.Account in (
						SELECT 	Account FROM Activityhist T3 
						WHERE 	T3.Cust = W.Cust 
						AND  	T3.BookDate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate)
						AND 	T3.CashTran = 1 AND T3.RecvPay = 1
					)
				AND 	S.Cust = W.Cust 
				AND	S.WLCode = @WLCode 
				AND	S.CreateTime BETWEEN @StartAlrtDate AND @EndAlrtDate
			
			SELECT @STAT = @@ERROR 
		END
	END
	ENDOFPROC:
	IF (@STAT <> 0) BEGIN 
	  ROLLBACK TRAN USR_WithDraw
	  RETURN @STAT
	END	
	
	IF @TrnCnt = 0
	  COMMIT TRAN USR_WithDraw
	RETURN @STAT
GO
