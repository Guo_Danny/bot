USE [OFAC]
GO

/****** Object:  Table [dbo].[TempRelationship]    Script Date: 08/07/2017 14:21:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TempRelationship](
	[RelationshipId] [dbo].[IDType] NOT NULL,
	[EntNum] [dbo].[IDType] NOT NULL,
	[RelatedID] [dbo].[IDType] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_TempRelationship_RelationshipId] PRIMARY KEY NONCLUSTERED 
(
	[RelationshipId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


SET ANSI_PADDING OFF
GO