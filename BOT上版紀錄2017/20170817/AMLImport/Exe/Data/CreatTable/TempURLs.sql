USE [OFAC]
GO

/****** Object:  Table [dbo].[URLs]    Script Date: 08/11/2017 12:37:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TempURLs](
	[URLsID] [dbo].[IDType] NOT NULL,
	[EntNum] [dbo].[IDType] NOT NULL,
	[URL] [dbo].[NoteDataType] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [TempPK_URLs] PRIMARY KEY CLUSTERED 
(
	[URLsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


