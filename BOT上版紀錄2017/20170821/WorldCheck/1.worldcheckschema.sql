USE [OFAC]
GO

/****** Object:  Table [dbo].[WorldCheck]    Script Date: 6/29/2017 2:36:42 PM ******/
--ALTER TABLE [dbo].[WorldCheck] ADD [Entnum] [int] NULL,[UPDDate] [datetime] NULL,[DelFlag] [bit] NULL ;  
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WorldCheck](
	[UID] [int] NOT NULL,
	[LastName] [varchar](max) NULL,
	[FirstName] [varchar](max) NULL,
	[Aliases] [varchar](max) NULL,
	[LowQAliases] [varchar](max) NULL,
	[AlterSpell] [varchar](max) NULL,
	[Category] [varchar](max) NULL,
	[Title] [varchar](max) NULL,
	[SubCategory] [varchar](max) NULL,
	[Position] [varchar](max) NULL,
	[Age] [int] NULL,
	[DOB] [varchar](50) NULL,
	[PlaceOfBirth] [varchar](max) NULL,
	[Deceased] [varchar](50) NULL,
	[Passports] [varchar](max) NULL,
	[SSN] [varchar](max) NULL,
	[Location] [varchar](max) NULL,
	[Country] [varchar](max) NULL,
	[Company] [varchar](max) NULL,
	[Type] [varchar](max) NULL,
	[LinkedTo] [varchar](max) NULL,
	[FurtherInfo] [varchar](max) NULL,
	[Keywords] [varchar](max) NULL,
	[ExSources] [varchar](max) NULL,
	[UpdCategory] [varchar](max) NULL,
	[Entered] [varchar](50) NULL,
	[Updated] [varchar](50) NULL,
	[Editor] [varchar](max) NULL,
	[AgeDate] [varchar](50) NULL,
	--[Entnum] [int] NULL,
	--[UPDDate] [datetime] NULL,
	--[DelFlag] [bit] NULL,
 CONSTRAINT [PK_WorldCheck] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/*
USE [OFAC]
GO

CREATE UNIQUE INDEX [PK_WorldCheckent] ON [dbo].[WorldCheck]
(
	[entnum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
*/
