USE [OFAC]
GO

/****** Object:  Table [dbo].[TempWorldCheck]    Script Date: 6/29/2017 2:36:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TempWorldCheckDel](
	[UID] [int] NOT NULL,
	[Updated] [varchar](50) NULL
)

GO

SET ANSI_PADDING OFF
GO


