USE [OFAC]
GO

/****** Object:  Table [dbo].[TempWorldCheck]    Script Date: 6/29/2017 2:36:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WorldCheckCountry](
	[Code] [varchar](10) NOT NULL,
	[Customer_Country] [varchar](200) NULL,
	[WorldCheck_Country] [varchar](200) NULL,
	[Status] [bit] NULL,
	[LastDate] [datetime] NULL
)

GO

SET ANSI_PADDING OFF
GO



--
insert into [WorldCheckCountry](code,Customer_Country,WorldCheck_Country,Status)
select Code,name,name,'1' from pbsa.dbo.Country
where Name in 
('AUSTRALIA',
'CHINA',
'BAHAMAS',
'BAHRAIN',
'BELGIUM',
'CAYMAN ISLANDS',
'CANADA',
'FRANCE',
'FINLAND',
'GERMANY',
'COLOMBIA',
'DENMARK',
'HONG KONG',
'INDONESIA',
'GREENLAND',
'LUXEMBOURG',
'INDIA',
'ITALY',
'LIECHTENSTEIN',
'NETHERLANDS',
'NEW ZEALAND',
'PHILIPPINES',
'POLAND',
'SINGAPORE',
'JAPAN',
'MALAYSIA',
'MONACO',
'QATAR',
'NORWAY',
'SWITZERLAND',
'TURKEY',
'SOUTH AFRICA',
'THAILAND',
'RUSSIAN FEDERATION',
'UNITED KINGDOM',
'VANUATU',
'UNITED ARAB EMIRATES')

insert into [WorldCheckCountry] (code,Customer_Country,WorldCheck_Country,Status)
values('GG','GUERNSEY.C.I','GUERNSEY','1')

insert into [WorldCheckCountry] (code,Customer_Country,WorldCheck_Country,Status)
values('KR','KOREA.REPUBLIC OF','KOREA, NORTH','1')

insert into [WorldCheckCountry] (code,Customer_Country,WorldCheck_Country,Status)
values('TW','R.O.C.','TAIWAN','1')

insert into [WorldCheckCountry] (code,Customer_Country,WorldCheck_Country,Status)
values('US','UNITED STATES','USA','1')

insert into [WorldCheckCountry] (code,Customer_Country,WorldCheck_Country,Status)
values('VN','VIETNAM','VIET NAM','1')