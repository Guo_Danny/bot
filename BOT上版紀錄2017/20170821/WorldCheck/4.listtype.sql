USE [OFAC]
GO

insert into ListType(code,name,ListDate,IsPrimeList,Enabled,CreateOper,CreateDate,LastOper,LastModify)
VALUES('0114WPEP','0114WPEP',getdate(),0,1,'Prime',getdate(),'Prime',getdate())

insert into ListTypeAssociation(ListType) VALUES ('0114WPEP')

insert into ListType(code,name,ListDate,IsPrimeList,Enabled,CreateOper,CreateDate,LastOper,LastModify)
VALUES('WPEP','WPEP',getdate(),0,1,'Prime',getdate(),'Prime',getdate())

insert into ListTypeAssociation(ListType) VALUES ('WPEP')