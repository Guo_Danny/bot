/*
**  File Name:        ConvOFAC.sql
**
**  Functional Description:
**
**  This module contains SQL conversion procedures for Prime version 10.1 
**
**  Creation Date:    07/27/2016
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2011                                                   ***
*** FIS               			                                 ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|----------------------------------------------------     

07/14/2016      SM      I160340794: Truncate the length of dob column to 150 
                        before inserting into sdntable. While combining all 
                        the dob's for the single entnum the value is 
                        exceeding 150   
         
07/27/2016      AS	updated OFS_AddUpdateDowJonesSDN: set LastModifDate = 
                        @now in the Update statement and changed @program to 
                        Upper(@program) and changed stored proc transaction 
                        name to OFS_AddUpdateDowJonesSDN; Updated table 
                        indexes for performance.

07/25/2016      AS      OFAC Dow Jones GA - Modify and create new
                        Dow Jones Max Date Methods
                        to support additional file formats.
07/27/2016      AS      OFAC Dow Jones GA - Change Dow Jones SDN Range from 
                        9 - 10 million to 7 - 10 million, sharing ids with 
                        World Check. 
                        Dow Jones is now importing over 1 million records 
                        and needs the additional available space.
07/27/2016      AS      Script to update OFS_SetLastSDNUpd 
                        (and add supporting function fn_IsNewestSDNFromDowJones), 
                        to allow support for Incremental Build Images of 
                        Dow Jones Data.
08/25/2016		SS		Defect #6023: Fixed the Invalid character present in the 
						description of the databank option 						
01/18/2017		SS	 	 I160394233 : Modified the following stored procedures for implementing
						 soft delete functionality for Addresses, Alternate Names and Dobs
						 for user defined parties.

						 OFS_AddSDNAlt,OFS_DelAllAddrForEntNum,OFS_DelAllAltNamesForEntNum
						 OFS_DelDOBsEntNum,dbo.OFS_AddSDNAddr,OFS_ImportSDN and OFS_InsDOBs.	
01/18/2017      SM       I160605707:Fixed the issue in the Web method GetSearchResultsStatus 
					     to generate the status for the Archived OFAC Case	
2/21/2017		SS		 Defect 6157: Modified the procedure OFS_AddWorldCheckSDNAlt to add
						 alternate names for Dow Jones Sanctioned Party.						
2/23/2017		SS		 Defect 6162: Modified OFS_InsNotes and OFS_InsUrls procedures to 
						 resolve Primary Key conflict issue for adding Notes and URL for Parties.
2/24/2017		SS		 Defect 6164: Modified OFS_AddWorldCheckSDNAddr to add address details 
						 for Dow Jones Sanctioned Party. Also modified OFS_InsDOBs to update the 
						 date of birth for the user defined sanctioned party.
						 
*/


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Starting Conversion of OFAC Database to version 10.1.0 '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

Use OFAC
GO

Set ANSI_NULLS ON
Go

Set NOCOUNT ON 
Go

If exists (select * from dbo.sysobjects 
          where id = object_id(N'[dbo].[OFS_CompleteLoadDistribution]') and 
                 OBJECTPROPERTY(id, N'IsProcedure') = 1)
                
drop procedure [dbo].[OFS_CompleteLoadDistribution]

Go

CREATE PROCEDURE dbo.OFS_CompleteLoadDistribution (
                            @isdiff bit
) With Encryption As

    declare @oper Code
    declare @trnCnt int

    select @oper = 'Prime'
    select @trnCnt = @@trancount  -- Save the current trancount

    if @trnCnt = 0
        -- Transaction has not begun
        begin tran OFS_CompleteLoadDistribution
    else
        -- Already in a transaction
        save tran OFS_CompleteLoadDistribution

    -- Processing starts here:
    declare @stat int

--******** Update Main tables from temp tables ********************************
/* TempLoad SDN tables contains the distribution records
    If ListModifDate is equal then just update the Status with the
    distribution record status else update the whole record except the
    deleted column

    If record is not in main table then insert the record

    Set the Status of all records in main table but not in temp table
    to deleted if not loading a differential file, else set the Status
    to unchanged
*/
--*****************************************************************************

    declare @newstatus int
    If @isdiff = 0
        set @newstatus = 4  --deleted
    Else
        set @newstatus = 1  --unchanged

    Create table #TempUserEntNums (
                EntNum int
    )

    Create table #TempUserAltNums (
                AltNum int
    )

    insert into #TempUserEntNums(EntNum)
    Select  distinct Entnum from SDNTable where UserRec = 1

    insert into #TempUserAltNums(AltNum)
    Select distinct AltNum from SDNAltTable, SDNTable where 
    SDNAltTable.EntNum = SDNTable.Entnum
    and SDNTable.UserRec = 1

   -- For all lists that have been disabled, set the record statuses to deleted
    
                Update SDNTable Set Status = 4, LastModifDate = getDate()
        FROM SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and s.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update SDNAltTable Set Status = 4, LastModifDate = getDate()
        FROM SDNAltTable sa (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = sa.EntNum and sa.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update SDNAddrTable Set Status = 4, LastModifDate = getDate()
        FROM SDNAddrTable sa (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = sa.EntNum and sa.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update DOBs Set Status = 4, LastModify = getDate()
        FROM DOBs d (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = d.EntNum and d.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update Notes Set Status = 4, LastModify = getDate()
        FROM Notes n (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = n.EntNum and n.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update URLs Set Status = 4, LastModify = getDate()
        FROM URLs u (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = u.EntNum and u.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update Relationship Set Status = 4, LastModify = getDate()
        FROM Relationship r (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = r.EntNum and r.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update Keywords Set Status = 4, LastModify = getDate()
        FROM Keywords k (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = k.EntNum and k.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update KeywordsAlt Set Status = 4, LastModify = getDate()
        FROM KeywordsAlt k (nolock), SDNAltTable sa (nolock),
            SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            sa.AltNum = k.AltNum and sa.EntNum = s.EntNum and k.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

   -- Update entire record if distribution listmodifdate != last listmodifdate
   -- or (if status not equal to unchanged and listmodifdates are equal)

If Exists (Select EntNum from TempLoadSDNTable) Begin
   Update SDNtable Set
        [Name] = t.[Name],
        FirstName = NULLIF(t.FirstName,''),
        MiddleName = NULLIF(t.MiddleName,''),
        LastName = NullIf(t.LastName,''),
        Title = t.Title,
        Program = UPPER(t.Program),
        Type = t.Type,
        CallSign = t.CallSign,
        VessType = t.VessType,
        Tonnage = t.Tonnage,
        GRT = t.GRT,
        VessFlag = t.VessFlag,
        VessOwner = t.Vessowner,
        Dob = NullIF(t.DOB,''),
        Remarks = t.Remarks,
        Remarks1 = t.Remarks1,
        Remarks2 = t.Remarks2,
        SDNType = t.SDNType,
        DataType = t.DataType,
        userRec = t.userRec,
        duplicRec = t.duplicRec,
        IgnoreDerived = t.IgnoreDerived,
        ListID = t.ListId,
        ListType = t.ListType,
        Country = t.Country,
        Sex = t.Sex,
        Build = t.Build,
        Height = NullIf(t.Height,0),
        Weight = NullIf(t.Weight,0),
        Race = t.Race,
        Complexion = t.Complexion,
        eyes = t.eyes,
        hair = t.hair,
        PrimeAdded = t.PrimeAdded,
        StandardOrder = t.StandardOrder,
        EffectiveDate = t.EffectiveDate,
        ExpiryDate = t.ExpiryDate,
        Status = t.Status,
        ListCreateDate = t.ListCreatedate,
        ListModifDate = t.ListModifDate,
        LastModifDate = GetDate(),
        LastOper = t.LastOper
    From SDNTable s, TempLoadSDNTable t
        Where s.EntNum = t.EntNum And s.UserRec = 0 And
              ( (IsNull(s.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End
   -- Update status to distribution records status
   -- if distribution listmodifdate = last listmodifdate and status = unchanged
If Exists (Select EntNum from TempLoadSDNTable) Begin
   Update SDNTable Set Status = t.Status
        From SDNTable s, TempLoadSDNTable t
            Where s.EntNum = t.EntNum  and s.UserRec = 0 And
               IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0) And
               t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

-- Update record status of records not in temp table
  If (@newstatus=1)
                Begin
                     Update SDNTable Set Status = @newstatus 
                     FROM SDNTable s (nolock) left outer join TempLoadSDNTable t (nolock)
                     on s.EntNum = t.EntNum
                     Where t.EntNum is NULL and s.UserRec = 0 and s.Status in (2,3)
                End
  Else
                Begin
                     Update SDNTable Set Status = @newstatus, LastModifDate = getDate()
                     FROM SDNTable s (nolock) left outer join TempLoadSDNTable t (nolock)
                     on s.EntNum = t.EntNum
                     Where t.EntNum is NULL and s.UserRec = 0 and s.Status <> 4
                END
   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

-- Insert new records
If Exists (Select EntNum from TempLoadSDNTable) Begin
   Insert into SDNTable (EntNum, [Name], FirstName,
                        MiddleName, LastName,Title,
                        Program, Type, CallSign,
                        VessType, Tonnage, GRT,
                        VessFlag, VessOwner, Dob,
                        Remarks, Remarks1, Remarks2,SDNType,
                        DataType, userRec, deleted,
                        duplicRec, IgnoreDerived,
                        ListID, ListType, Country,
                        Sex, Build, Height, Weight,
                        Race, Complexion, eyes, hair,
                        PrimeAdded, StandardOrder, EffectiveDate,
                        ExpiryDate, Status, ListCreateDate,
                        ListModifDate, CreateDate, LastModifDate,
                        LastOper)
    Select t.EntNum, t.[Name], t.FirstName,
                        t.MiddleName, t.LastName, t.Title,
                        t.Program, t.Type, t.CallSign,
                        t.VessType, t.Tonnage, t.GRT,
                        t.VessFlag, t.VessOwner, t.Dob,
                        t.Remarks, t.Remarks1, t.Remarks2, t.SDNType,
                        t.DataType, t.userRec, t.deleted,
                        t.duplicRec, t.IgnoreDerived,
                        t.ListID, t.ListType, t.Country,
                        t.Sex, t.Build, t.Height, t.Weight,
                        t.Race, t.Complexion, t.eyes, t.hair,
                        t.PrimeAdded, t.StandardOrder, t.EffectiveDate,
                        t.ExpiryDate, t.Status, t.ListCreateDate,
                        t.ListModifDate, GetDate(), GetDate(),
                        t.LastOper
    From TempLoadSDNTable t (nolock) left Outer Join SDNTable s (nolock)
        on t.EntNum = s.EntNum
    where s.Entnum is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select AltNum from TempLoadAltTable) Begin
  Update SDNAlttable Set
        EntNum  = t.EntNum,
        AltType = t.AltType,
        AltName = t.AltName,
        FirstName = t.FirstName,
        MiddleName = t.MiddleName,
        LastName = t.LastName,
        ListId = t.ListId,
        Remarks = t.Remarks,
        status = t.status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModifDate = GetDate(),
        LastOper = t.LastOper
     From SDNAltTable s, TempLoadAltTable t
        Where s.AltNum = t.AltNum And
              ( (IsNull(s.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End
   -- Update status to distribution records status
   -- if distribution listmodifdate = last listmodifdate and status = unchanged
If Exists (Select AltNum from TempLoadAltTable) Begin
   Update SDNAltTable Set Status = t.Status, LastOper = t.LastOper
    From SDNAltTable s, TempLoadAltTable t
            Where s.AltNum = t.AltNum And
               IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End


  -- Update record status of records not in temp table
   If (@newstatus = 1)
                Begin
                                Update SDNAltTable Set Status = @newstatus,
                                                LastOper = @oper
                                From SDNAltTable s (nolock) 
                left outer join  TempLoadAltTable t (nolock) on s.AltNum = t.AltNum
                Where t.AltNum is NULL and s.Status in (2,3)
                End
                Else
                Begin
                                Update SDNAltTable Set Status = @newstatus, LastModifDate = getDate(),
                LastOper = @oper
                From SDNAltTable s (nolock) 
                left outer join TempLoadAltTable t (nolock) on s.AltNum = t.AltNum
                left outer join #TempUserAltNums ta on s.AltNum = ta.AltNum
                                Where t.AltNum is NULL and ta.AltNum is NULL and s.Status <> 4
                End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution



   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select AltNum from TempLoadAltTable) Begin
   Insert into SDNAltTable (EntNum, AltNum, AltType, AltName, FirstName,
      MiddleName, LastName,Remarks, Status, ListCreateDate, ListModifDate,
      CreateDate, LastModifDate, LastOper)
   Select t.EntNum, t.AltNum, t.AltType, t.AltName, t.FirstName,
        t.MiddleName, t.LastName, t.Remarks, t.Status, t.ListCreateDate, t.ListModifDate,
        GetDate(), GetDate(), t.LastOper
   From TempLoadAltTable t (nolock) left Outer Join SDNAltTable s (nolock)
            on t.AltNum = s.AltNum
        where s.AltNum is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select AddrNum from TempLoadAddrTable)  Begin
    -- Update the address records
   Update SDNAddrTable Set
        EntNum = t.EntNum,
        Address = t.Address,
        City = t.City,
        Country = t.Country,
        Remarks = t.Remarks,
        ListId = t.ListId,
        FreeFormat = t.FreeFormat,
        Address2 = t.Address2,
        Address3 = t.Address3,
        Address4 = t.Address4,
        State = t.State,
        PostalCode = t.PostalCode,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModifDate = GetDate(),
        LastOper = t.LastOper
    From SDNAddrTable s, TempLoadAddrTable t
        Where s.AddrNum = t.AddrNum And
              ( (IsNull(s.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select AddrNum from TempLoadAddrTable) Begin
   Update SDNAddrTable Set Status = t.Status, LastOper = t.LastOper
    From SDNAddrTable s, TempLoadAddrTable t
            Where s.AddrNum = t.AddrNum And
               IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
   If (@newstatus = 1)
                Begin
                                Update SDNAddrTable Set Status = @newstatus, 
                                                LastOper = @oper
                                From SDNAddrTable s (nolock) 
                left outer join  TempLoadAddrTable t (nolock) on s.AddrNum = t.AddrNum
                Where t.AddrNum is NULL and s.Status in (2,3)
                End
                Else
                Begin
                                Update SDNAddrTable Set Status = @newstatus, LastModifDate = getDate(),
                                                LastOper = @oper
                                From SDNAddrTable s (nolock) left outer join  TempLoadAddrTable t (nolock) on s.AddrNum = t.AddrNum
                Left outer join #TempUserEntNums ta on s.EntNum = ta.EntNum
                                Where t.AddrNum is NULL and ta.EntNum is NULL and s.Status <> 4
                End

  Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select AddrNum from TempLoadAddrTable) Begin
     Insert into SDNAddrTable (EntNum, AddrNum, Address, City, Country, Remarks,
        ListId, FreeFormat, Address2, Address3, Address4,
        State, PostalCode, Status, ListCreateDate, ListModifDate,
        CreateDate, LastModifDate, LastOper)
    Select t.EntNum, t.AddrNum, t.Address, t.City, t.Country, t.Remarks,
        t.ListId, t.FreeFormat, t.Address2, t.Address3, t.Address4,
        t.State, t.PostalCode, t.Status, t.ListCreateDate, t.ListModifDate,
        GetDate(), GetDate(), t.LastOper
    From TempLoadAddrTable t (nolock) left Outer Join SDNAddrTable s (nolock)
        on t.AddrNum = s.AddrNum
    where s.AddrNum is Null


   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsId from TempLoadKeywords)  Begin
    -- Update the address records
   Update Keywords Set
        EntNum = t.EntNum,
        Word = t.Word,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From Keywords k, TempLoadKeywords t
        Where k.keywordsId = t.keywordsId And
              ( (IsNull(k.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsId from TempLoadKeywords) Begin
   Update Keywords Set Status = t.Status, LastOper = t.LastOper
    From Keywords k, TempLoadKeywords t
            Where k.keywordsId = t.keywordsId And
               IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
                If (@newstatus=1)
                Begin
                                Update Keywords Set Status = @newstatus, 
                                                LastOper = @oper
                                From Keywords k (nolock) left outer join TempLoadKeywords t (nolock)
        on k.keywordsId = t.keywordsId
        Where t.keywordsId is NULL and k.Status in (2,3)
                End
                Else
                Begin
                                Update Keywords Set Status = @newstatus, LastModify = getDate(),
                                                LastOper = @oper
                                From Keywords k (nolock) left outer join TempLoadKeywords t (nolock) on k.keywordsId = t.keywordsId
                left outer join #TempUserEntNums ta on k.EntNum = ta.EntNum
                                Where t.keywordsId is NULL and ta.EntNum is NULL and k.Status <> 4
                End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select keywordsId from TempLoadKeywords) Begin
     Insert into keywords (KeywordsId, Entnum, Word, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
     Select t.KeywordsId, t.Entnum, t.Word, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadKeywords t (nolock) left Outer Join keywords k (nolock)
        on t.keywordsId = k.keywordsId
    where k.keywordsId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsAltId from TempLoadKeywordsAlt)  Begin
    -- Update the address records
   Update KeywordsAlt Set
        AltNum = t.AltNum,
        Word = t.Word,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From KeywordsAlt k, TempLoadKeywordsAlt t
        Where k.keywordsAltId = t.keywordsAltId And
              ( (IsNull(k.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsAltId from TempLoadKeywordsAlt) Begin
   Update KeywordsAlt Set Status = t.Status, LastOper = t.LastOper
    From KeywordsAlt k, TempLoadKeywordsAlt t
            Where k.keywordsAltId = t.keywordsAltId And
               IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
                If (@newstatus=1)
                Begin
                                Update KeywordsAlt Set Status = @newstatus, 
                                                LastOper = @oper
                                From KeywordsAlt k (nolock) left outer join TempLoadKeywordsAlt t (nolock)
        on k.keywordsAltId = t.keywordsAltId
        Where t.keywordsAltId is NULL and k.Status in (2,3)
                End
                Else
                Begin
                                Update KeywordsAlt Set Status = @newstatus, LastModify = getDate(),
                                                LastOper = @oper
                                From KeywordsAlt k (nolock) 
                left outer join TempLoadKeywordsAlt t (nolock) on k.keywordsAltId = t.keywordsAltId
                left outer join #TempUserAltNums ta on k.AltNum = ta.AltNum
                                Where t.keywordsAltId is NULL and ta.AltNum is NULL and k.Status <> 4
                End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select keywordsAltId from TempLoadKeywordsAlt) Begin
     Insert into keywordsAlt (KeywordsAltId, Altnum, Word,
        Status, ListCreateDate, ListModifdate,
        CreateOper, CreateDate, LastOper,
            LastModify)
     Select t.KeywordsAltId, t.Altnum, t.Word,
        t.Status, t.ListCreateDate, t.ListModifdate,
        t.CreateOper, GetDate(), t.LastOper,
            GetDate()
    From TempLoadKeywordsAlt t (nolock) left Outer Join KeywordsAlt k (nolock)
        on t.KeywordsAltId = k.KeywordsAltId
    where k.KeywordsAltId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select DOBsId from TempLoadDOBs)  Begin
    -- Update the address records
   Update DOBs Set
        EntNum = t.EntNum,
        DOB = t.DOB,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From DOBs d, TempLoadDOBs t
        Where d.DOBsId = t.DOBsId And
              ( (IsNull(d.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(d.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select DOBsId from TempLoadDOBs) Begin
   Update DOBs Set Status = t.Status, LastOper = t.LastOper
    From DOBs d, TempLoadDOBs t
            Where d.DOBsId = t.DOBsId And
               IsNull(d.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
                If (@newstatus=1)
                Begin
                                Update DOBs Set Status = @newstatus,
                                                LastOper = @oper
                                From DOBs d (nolock) left outer join TempLoadDOBs t (nolock)
        on d.DOBsId = t.DOBsId
        Where t.DOBsId is NULL and d.Status in (2,3) 
                End
                Else
                Begin
                                Update DOBs Set Status = @newstatus, LastModify = getDate(),
                                                LastOper = @oper
                                From DOBs d (nolock) 
                left outer join TempLoadDOBs t (nolock) on d.DOBsId = t.DOBsId
                left outer join #TempUserEntNums ta on d.EntNum = ta.EntNum
                                Where t.DOBsId is NULL and ta.EntNum is NULL and d.Status <> 4
                End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select DOBsId from TempLoadDOBs) Begin
     Insert into DOBs (DOBsId, Entnum, DOB, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.DOBsId, t.Entnum, t.DOB, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadDOBs t (nolock) left Outer Join DOBs d (nolock)
        on t.DOBsId = d.DOBsId
    where d.DOBsId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select NotesId from TempLoadNotes)  Begin
    -- Update the address records
   Update Notes Set
        EntNum = t.EntNum,
        Note = t.Note,
        NoteType = t.NoteType,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From Notes n, TempLoadNotes t
        Where n.NotesId = t.NotesId And
              ( (IsNull(n.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(n.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select NotesId from TempLoadNotes) Begin
   Update Notes Set Status = t.Status, LastOper = t.LastOper
    From Notes n, TempLoadNotes t
            Where n.NotesId = t.NotesId And
               IsNull(n.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
                If (@newstatus=1)
                Begin
                                Update Notes Set Status = @newstatus,
                                LastOper = @oper
                                From Notes n (nolock) left outer join TempLoadNotes t (nolock)
        on n.NotesId = t.NotesId
        Where t.NotesId is NULL and n.Status in (2,3)
                End
                Else
                Begin
                                Update Notes Set Status = @newstatus, LastModify = getDate(),
                                LastOper = @oper
                                From Notes n (nolock) 
                left outer join TempLoadNotes t (nolock) on n.NotesId = t.NotesId
                left outer join #TempUserEntNums ta on n.EntNum = ta.EntNum
                                Where t.NotesId is NULL and ta.EntNum is NULL and n.Status <> 4
                End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select NotesId from TempLoadNotes) Begin
     Insert into Notes (NotesId, Entnum, Note, NoteType, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.NotesId, t.Entnum, t.Note, t.NoteType, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadNotes t (nolock) left Outer Join Notes n (nolock)
        on t.NotesId = n.NotesId
    where n.NotesId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select URLsId from TempLoadURLs)  Begin
    -- Update the address records
   Update URLs Set
        EntNum = t.EntNum,
        URL = t.URL,
        [Description] = t.[Description],
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From URLs u, TempLoadURLs t
        Where u.URLsId = t.URLsId And
              ( (IsNull(u.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(u.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select URLsId from TempLoadURLs) Begin
   Update URLs Set Status = t.Status, LastOper = t.LastOper
    From URLs u, TempLoadURLs t
            Where u.URLsId = t.URLsId And
               IsNull(u.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
                If (@newstatus=1)
                Begin
                                Update URLs Set Status = @newstatus,
                                LastOper = @oper
                                From URLs u (nolock) left outer join TempLoadURLs t (nolock)
        on u.URLsId = t.URLsId
        Where t.URLsId is NULL and u.Status in (2,3)
                End
                Else
                Begin
                                Update URLs Set Status = @newstatus, LastModify = getDate(),
                                LastOper = @oper
                                From URLs u (nolock) 
                left outer join TempLoadURLs t (nolock) on u.URLsId = t.URLsId
                left outer join #TempUserEntNums ta on u.EntNum = ta.EntNum
                                Where t.URLsId is NULL and ta.EntNum is NULL and u.Status <> 4
                End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select URLsId from TempLoadURLs) Begin
     Insert into URLs (URLsId, Entnum, URL, [Description], Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.URLsId, t.Entnum, t.URL, t.[Description], t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadURLs t (nolock) left Outer Join URLs u (nolock)
        on t.URLsId = u.URLsId
    where u.URLsId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select RelationshipId from TempLoadRelationship)  Begin
    -- Update the address records
   Update Relationship Set
        EntNum = t.EntNum,
        RelatedID = t.RelatedID,
        RelationshipPToR = t.RelationshipPToR,
        RelationshipRToP = t.RelationshipRToP,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From Relationship r, TempLoadRelationship t
        Where r.RelationshipId = t.RelationshipId And
              ( (IsNull(r.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(r.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select RelationshipId from TempLoadRelationship) Begin
   Update Relationship Set Status = t.Status, LastOper = t.LastOper
    From Relationship r, TempLoadRelationship t
            Where r.RelationshipId = t.RelationshipId And
               IsNull(r.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
                If (@newstatus=1)
                Begin
                                Update Relationship Set Status = @newstatus, 
                                LastOper = @oper
                                From Relationship r (nolock) left outer join TempLoadRelationship t (nolock)
        on r.RelationshipId = t.RelationshipId
        Where t.RelationshipId is NULL and r.Status in (2,3)
                End
                Else
                Begin
                                Update Relationship Set Status = @newstatus, LastModify = getDate(),
                                LastOper = @oper
                                From Relationship r (nolock) 
                left outer join TempLoadRelationship t (nolock) on r.RelationshipId = t.RelationshipId
                left outer join #TempUserEntNums ta on r.EntNum = ta.EntNum
                                Where t.RelationshipId is NULL and ta.EntNum is NULL and r.Status <> 4
                End
   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select RelationshipId from TempLoadRelationship) Begin
     Insert into Relationship (RelationshipId, Entnum, RelatedID,
        RelationshipPToR, RelationshipRToP, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.RelationshipId, t.Entnum, t.RelatedID,
        t.RelationshipPToR, t.RelationshipRToP, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadRelationship t (nolock) left Outer Join Relationship r (nolock)
        on t.RelationshipId = r.RelationshipId
    where r.RelationshipId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  if @isdiff = 0
  begin
      -- set status of undeleted records to unchanged
      Update SDNTable
        Set Status = 1 Where userRec = 1 and Deleted = 0

      Select @stat = @@error
      If (@stat <> 0)
          Goto AbortCompleteLoadDistribution
  end

--populate dob column in SDNTABLE from DOBs table.  DOB column should be shown as CSV value
update  sdntable set
dob=SUBSTRING(dt.dob_csv,1,150)
from (
                select
                                t1.entnum,
                                stuff((
                                                                select ',' + t.[dob]
                                                                from dobs t
                                                                where t.entnum = t1.entnum and
                                                                t.status in (1,2,3)
                                                                order by t.[dob]
                                                                for xml path('')
                                                                ),1,1,'') as dob_csv
                from dobs t1
                group by entnum
) dt
inner join sdntable  s on
dt.entnum=s.entnum
where s.status  in (1,2,3)

/*
                update the status to modified in sdntable if the following condition is met: 
                dobs table contains status of new or modified and status in Sdntable is NOT New.
                The LastModifDate should not be changed if the above conditions are met.
*/
UPDATE sdntable SET
[status]=3
FROM
( 
                SELECT MAX( d.status)AS NewMod, d.entnum 
                FROM dobs as d 
                WHERE d.status in (2,3)
                and 
                not exists
                (select * from sdntable sd where sd.entnum=d.entnum and sd.status=2)
                GROUP BY d.entnum
) AS dt
INNER JOIN sdntable AS st ON
st.entnum=dt.entnum

-- Set Keywords and Keywords Alternate to deleted 
--if the deleted keywords are introduced in the new distribution
Update Keywords 
Set Deleted = 1, 
                LastModify = GetDate() 
where Deleted = 0 and 
                  Word in (select word from Keywords where deleted = 1)

Update KeywordsAlt 
set Deleted = 1, 
                LastModify = GetDate() 
where Deleted = 0 and 
                  Word in (select word from KeywordsAlt where Deleted = 1)


delete from TempLoadSDNTable
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

delete from TempLoadAltTable
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadAddrTable
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadKeywords
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

delete from TempLoadKeywordsAlt
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadDOBs
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadNotes
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadURLs
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadRelationship
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If @trnCnt = 0
    commit tran OFS_CompleteLoadDistribution

return 0  -- Success

AbortCompleteLoadDistribution:
    if (@stat <> 0) Begin
        rollback tran OFS_CompleteLoadDistribution
        return @stat
    End
GO

--------------------------------------------DOW Jones Merge---------------------------------------------
If exists (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_GetDowJonesPFAMaxListDate]') and 
                                                                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                drop procedure [dbo].[OFS_GetDowJonesPFAMaxListDate]
Go

CREATE PROCEDURE OFS_GetDowJonesPFAMaxListDate

WITH ENCRYPTION AS
BEGIN
SELECT  
                MAX(ListcreateDate) 
FROM 
                SDNTable 
 WHERE 
                ListID like 'DJ!_%' ESCAPE '!'
END
GO
-----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_GetDowJonesAMEMaxListDate]') and 
                                                                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                drop procedure [dbo].[OFS_GetDowJonesAMEMaxListDate]
Go

CREATE PROCEDURE OFS_GetDowJonesAMEMaxListDate

WITH ENCRYPTION AS
BEGIN
SELECT  
                MAX(ListcreateDate) 
FROM 
                SDNTable 
 WHERE 
                ListID like 'DJAME!_%' ESCAPE '!'
END
GO
-----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_GetDowJonesACMaxListDate]') and 
                                                                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                drop procedure [dbo].[OFS_GetDowJonesACMaxListDate]
Go

CREATE PROCEDURE OFS_GetDowJonesACMaxListDate

WITH ENCRYPTION AS
BEGIN
SELECT  
                MAX(ListcreateDate) 
FROM 
                SDNTable 
 WHERE 
                ListID like 'DJAC!_%' ESCAPE '!'
END
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_AddUpdateDowJonesSDN]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_AddUpdateDowJonesSDN]
Go

CREATE PROCEDURE dbo.OFS_AddUpdateDowJonesSDN( @entNum int,
                            @name SanctionName,
                            @firstName SanctionName,
                            @middleName SanctionName,
                            @lastName SanctionName,
                            @type char (12),
                            @program char(50),
                            @title varchar(200),
                            @callSign char(8),
                            @vessType char(25),
                            @tonnage char(14),
                            @grt char(8),
                            @vessFlag varchar(40),
                            @vessOwner varchar(150) ,
                            @dob varchar(150) ,
                            @remarks text ,
							@remarks1 varchar(255) ,
							@remarks2 varchar(255) ,
                            @sdnType int,
                            @dataType int,
                            @userRec int ,
                            @sex char,
                            @listID varchar(15),
                            @listType varchar(15),
                            @primeAdded bit,
                            --@standardOrder bit,
                            @effectiveDate datetime,
                            @expiryDate datetime,
                            @status int,
                            @listCreateDate datetime,
                            @listModifDate dateTime,
                            @oper Code,
                            @ignDrvNames BIT, 
							@retEntnum int OUTPUT)
WITH ENCRYPTION 
AS
BEGIN
  declare @event		 varchar(3)
  --Transaction 
  declare @trnCnt        int
  -- Processing starts here:
  declare @now datetime
  declare @pos int
  declare @stat int
  declare @text varchar(255)
  declare @maxCurrentNum int
  declare @minSDNIndex int = 7000000
  declare @maxSDNIndex int = 9999999

  select @now = GETDATE()

  select @trnCnt = @@trancount  -- Save the current trancount

  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_AddUpdateDowJonesSDN
  else
    -- Already in a transaction
    save tran OFS_AddUpdateDowJonesSDN

  --If @type = ''
   -- select @type = 'other'

    Select ISNULL(@ignDrvNames, 1)

If Exists(SELECT EntNum FROM SDNTable WITH(NOLOCK) WHERE ListId=@listid AND Program=@program AND [Type] = @type)
BEGIN
	SELECT @EntNum=EntNum FROM SDNTable WITH(NOLOCK) WHERE ListId=@listid AND Program=@program AND [Type] = @type
	UPDATE SDNTable SET
                            [name]=@name,
                            firstName=@firstName ,
                            middleName=@middleName,
                            lastName=@lastName ,
                            type=@type ,
                            program=UPPER(@program) ,
                            title=@title ,
                            callSign=@callSign ,
                            vessType=@vessType ,
                            tonnage=@tonnage ,
                            grt=@grt ,
                            vessFlag=@vessFlag ,
                            vessOwner=@vessOwner ,
                            dob=@dob ,
                            remarks=@remarks ,
			    remarks1=@remarks1 ,
			    remarks2=@remarks2 ,
                            sdnType=@sdnType ,
                            dataType=@dataType ,
                            userRec=@userRec ,
                            IgnoreDerived = @ignDrvNames ,
                            sex=@sex ,
                            listID=@listID ,
                            listType=@listType ,
                            primeAdded=@primeAdded ,
                            --standardOrder=@standardOrder ,
                            --effectiveDate=@effectiveDate ,
                            --expiryDate=@expiryDate ,
                            status=@status ,
                            --listCreateDate=@listCreateDate ,
                            listModifDate=@listModifDate ,
                            lastoper=@oper ,
                            LastModifDate=@now
		WHERE EntNum = @EntNum

  SELECT @STAT = @@ERROR
  IF @STAT <> 0  GOTO ENDOFPROC


	update Notes
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	update sdnaddrtable
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	update sdnalttable
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	update urls 
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	Update dobs
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	Update relationship
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC


   Select @event = 'MOD'
END
ELSE
BEGIN
		Select @maxCurrentNum = Max(EntNum) From SDNTable WHERE EntNum between @minSDNIndex and @maxSDNIndex
		Select @stat = @@ERROR

		If @stat <> 0 Begin
			return @stat
		End

		If @maxCurrentNum Is NULL Set @maxCurrentNum = 0
		if @maxCurrentNum < @minSDNIndex
		  Set @maxCurrentNum = @minSDNIndex
		else
		  Set @maxCurrentNum = @maxCurrentNum +1
		Set @entNum = @maxCurrentNum

		Insert into SDNTable (EntNum, [Name], FirstName,
				MiddleName, LastName,Title,
				Program,Type, 
				CallSign,
				VessType, Tonnage, GRT,
				VessFlag, VessOwner, Dob,
				Remarks, Remarks1, Remarks2, SDNType,
				DataType, userRec, deleted,
				duplicRec, IgnoreDerived,
				ListID, ListType,
				Sex, 
				PrimeAdded, --StandardOrder, 
				--EffectiveDate,ExpiryDate, 
				[Status], ListCreateDate,
				ListModifDate, CreateDate, LastModifDate,
				LastOper)
		Values
			   (@entNum, @name, NULLIF(@firstName,''),
				NULLIF(@middleName,''), NULLIF(@lastName,''), @title,
				UPPER(@program), @type
				, @callSign,
				@vessType, @tonnage, @grt,
				@vessFlag, @vessOwner, NULLIF(@dob,''),
				@remarks,
				@remarks1,
				@remarks2,				
				@sdnType,
				@dataType, @userRec, 0,
				0, @ignDrvNames,
				@listId, @listType,
				@sex,
				@primeAdded, --@standardOrder,
				 --@effectiveDate,@expiryDate, 
				@status, @listCreateDate,
				@listModifDate, @now, @now, @oper)
  Select @stat = @@ERROR
  IF @STAT <> 0  GOTO ENDOFPROC
		Select @event = 'CRE'
END
  Select @retEntNum = @entNum
    If @stat = 0 Begin
      Exec OFS_InsEvent @oper, @event, 'SanName', @name, Null
      select @stat = @@ERROR
      If @stat = 0 Begin
          Exec OFS_SetFileImageFlag
      End
  End
  
  EndofProc:
  IF @trnCnt = 0 BEGIN
	COMMIT TRAN OFS_AddUpdateDowJonesSDN
  END ELSE IF @stat <> 0 BEGIN
	ROLLBACK TRAN OFS_AddUpdateDowJonesSDN
  END 
  RETURN @stat
END
GO

----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_InsDowJonesRelationship]') and 
                                                                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                drop procedure [dbo].[OFS_InsDowJonesRelationship]
Go

Create PROCEDURE OFS_InsDowJonesRelationship (
            @RelationshipId     IDType,
            @entnum             IDType,
            @relatedid          IDType,
            @relationshipPtoR   varchar(50),
            @relationshipRtoP   varchar(50),
            @status             int,
            @listCreateDate     Datetime,
            @listModifDate      Datetime,
            @oper               SCode
) WITH ENCRYPTION AS
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_InsWorldCheckRelationship
  else -- Already in a transaction
    save tran OFS_InsWorldCheckRelationship
  -- End standard stored procedure transaction header

  declare @now datetime
  declare @stat int

  declare @Existing  int 
  declare @ChildExisting int
  declare @Unchanged int 
  declare @minSDNIndex int = 7000000
  declare @maxSDNIndex int = 9999999

  set @Existing  = 0
  set @Unchanged = 0

  select @now = GETDATE()

  --World Check block. Persist ID, Set status
  if @entNum between @minSDNIndex and @maxSDNIndex
   begin

                                if ( @RelationshipId != 0)
                                begin
                                                --Check by Primary Key
                                                select @Existing = count(RelationshipId)
                                                from Relationship WITH(NOLOCK)
                                                where EntNum = @entNum and 
                                                                  RelationshipId = @RelationshipId and
                                                                  RelatedId = @RelatedId and
                                                                  RelationshipPToR  = @relationshipPtoR and
                                                                  RelationshipRToP = @relationshipRtoP

                                                if @Existing = 0
                                                begin
                                                                --Insert new record
                                                                set  @RelationshipId  = 0
                                                end
                                                else
                                                begin
                                                                -- Existing record unchanged
                                                                update Relationship
                                                                set Status = 1 --unchanged
                                                                where EntNum = @entNum and 
                                                                      RelationshipId = @RelationshipId
                                                end        
                                  end

                                if @Existing = 0
                                begin
                                                --Find record by Relationship ID
                                                select @Existing = count(RelationshipId)
                                                from Relationship WITH(NOLOCK)
                                                where EntNum = @entNum and 
                                                                  Status = 4 and
                                                                  RelatedId = @RelatedId
              
        
                                                if @Existing = 0
                                                                set @status = 2 --added
                                                else
                                                begin
                                                                select @Unchanged = count(RelationshipId)
                                                                from Relationship WITH(NOLOCK)
                                                                where EntNum = @entNum and 
                                                                                  RelatedId = @RelatedId and
                                                                                  RelationshipPToR  = @relationshipPtoR and
                                                                                  IsNull(RelationshipRToP, '') = IsNull(@relationshipRtoP, '')

                                                                if @Existing = 1
                                                                begin
                                                                                --Unique entry exists
                                                                                if @Unchanged  = 0
                                                                                                update Relationship
                                                                                                set  RelationshipPToR  = @relationshipPtoR,
                                                                                                                RelationshipRToP  = @relationshipRtoP,
                                                                                                                ListModifDate = @listModifDate,
                                                                                                                LastModify =  @now, 
                                                                                                                 LastOper = @oper,
                                                                                                                Status = 3 --Modified                                                                        
                                                                                                where EntNum  = @entNum and
                                                                                                                  Status = 4 and 
                                                                                                                  RelatedId = @RelatedId
                                                                                else
                                                                                                update Relationship
                                                                                                set  Status = 1 -- unchanged                                                                            
                                                                                                where EntNum  = @entNum and
                                                                                                                  Status = @status and 
                                                                                                                  RelatedId = @RelatedId
                                                                   
                      
                                                                  end
                                                                else if @Existing > 1 and @Unchanged  > 1
                                                                  begin
                                                                                --Existing entry is not unique but no changes
                                                                                update Relationship
                                                                                set  Status = 1 -- unchanged                                                                            
                                                                                where EntNum = @entNum and 
                                                                                                  Status = @status and
                                                                                                  RelatedId = @RelatedId and 
                                                                                                  RelationshipPToR  = @relationshipPtoR and
                                                                                                  RelationshipRToP = @relationshipRtoP
                                                                  end
                                                                else
                                                                  begin
                                                                                --Set new entry
                                                                                set @Existing = 0
                                                                                set @status = 2 --added
                                                                  end
                                                end
                                end
   end

  if  @Existing = 0
   begin
                                if @RelationshipId = 0
                                begin
                                                --if there are no notes available in the notes table
                                                --then set the notesid to 1
                                                select @RelationshipId = max(RelationshipId) from relationship WITH(NOLOCK)
                                                select @RelationshipId = isnull(@RelationshipId, 10000000)
                                                if @RelationshipId < 10000000
                                                                SET @RelationshipId = 10000000

                                                select @RelationshipId = @RelationshipId + 1
                     end

                                -- Count the number of reverse relationship descriptions in the database
                     select @ChildExisting = count(RelationshipId)
                                                                from Relationship WITH(NOLOCK)
                                                                Where EntNum = @relatedid
                                                                AND RelatedID = @entnum
                     
                     -- if there is only one reverse relationship:
                     --   * get its description and add it to our reverse description variable
                     --   * update this other entry with our current Description as its reverse Description
                     if @ChildExisting = 1
                     Begin
                                                
                                                Select @relationshipRtoP = RelationshipPToR From Relationship WITH(NOLOCK) Where EntNum = @relatedid
                                                AND RelatedID = @entnum
                     
                                                Update Relationship
                                                Set RelationshipRToP = @relationshipPtoR
                                                Where EntNum = @relatedid
                                                AND RelatedID = @entnum
                     End
                     

                                insert into Relationship (RelationshipId, EntNum, RelatedID, RelationshipPToR,
                    RelationshipRToP, Status, ListCreateDate, ListModifDate,
                    CreateOper, CreateDate, LastOper, LastModify)
                                values (@RelationshipId, @entnum, @relatedid, @relationshipPtoR,
                                                    @relationshipRtoP, @status, @listCreateDate,
                                                                @listModifDate, @oper, @now, @oper, @now)
end


  -- Start standard stored procedure transaction footer
  select @stat = @@ERROR
  if @stat = 0
  begin
    if @trnCnt = 0 commit tran OFS_InsWorldCheckRelationship
  end
  else
    rollback tran OFS_InsWorldCheckRelationship
  return @stat
  -- End standard stored procedure transaction footer
GO
----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_GetDowJonesEntNum]') and 
                                                                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                drop procedure [dbo].[OFS_GetDowJonesEntNum]
Go

CREATE PROCEDURE OFS_GetDowJonesEntNum(
@ListId VARCHAR(20)
)
WITH ENCRYPTION AS
BEGIN

select EntNum from SDNTable WITH(NOLOCK) where ListID = @ListId

END
GO
----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_CRYDowJonesImportStats]') and 
                                                                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                drop procedure [dbo].[OFS_CRYDowJonesImportStats]
Go

CREATE procedure [dbo].[OFS_CRYDowJonesImportStats] @AsOfDate datetime
With Encryption
as
Set NOCOUNT ON
begin
                declare  @stats table
                ( 
      StatType varchar(30),
                  InputFile varchar(1000),
                  Total varchar(10),
                  Added varchar(10),
                  Modified varchar(10),
                  Deleted varchar(10),
                  Errors Int,
                  LogDate datetime
                )
                IF(@AsOfDate IS NOT NULL)
                                Set @AsOfDate = DATEADD(SECOND, -1, Cast(DATEADD(DAY, 1, Cast(@AsOfDate As Date)) AS datetime))
                
                insert into @stats
                                select 
                                t.Name, 
                                substring(e.LogText,0 ,patindex('%Total Records%',e.LogText)-2) as InputFile,
                                substring(e.LogText, patindex('%Total Records%',e.LogText)+14 ,patindex('%Added%',e.LogText)- (patindex('%Total Records%',e.LogText)+16)) as Total,
                                substring(e.LogText, patindex('%Added: %',e.LogText)+7 ,patindex('% Modified:%',e.LogText)- (patindex('%Added: %',e.LogText)+8)) as Added,
                                substring(e.LogText, patindex('%Modified: %',e.LogText)+10 ,patindex('% Deleted:%',e.LogText)- (patindex('%Modified: %',e.LogText)+11)) as Modified,
                                substring(e.LogText, patindex('%Deleted:%',e.LogText)+9 ,patindex('% Errors:%',e.LogText)- (patindex('%Deleted:%',e.LogText)+10)) as Deleted,
                                substring(e.LogText, patindex('%Errors:%',e.LogText)+8 ,Len(e.LogText)- (patindex('%Errors:%',e.LogText)+7)) as Errors,
                                e.LogDate
                from [SDNChangeLog] e WITH(NOLOCK)
                                join ObjectType t on e.ObjectType = t.Code
                where ObjectType ='DowJones' and ObjectId='Various'
                AND LogDate <= coalesce(@AsOfDate, GETDATE())
                AND CHARINDEX('Total Records',e.LogText) > 0
                
                select StatType, InputFile,Msg = 
                Convert(Varchar(100), case
                                when Errors=0 
                                                then 'All Records were loaded successfully'
                                when Errors > 0 
                                                then 'All Records were not loaded successfully'
                end),
                Total,
                Added,
                Modified,
                Deleted, 
                Errors,
                LogDate
                from @stats
                Order By LogDate Desc
end

GO
----------------------------------------------------------------------------------------------
If exists (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_CRYDowJonesSancChangeList]') and 
                                                                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                drop procedure [dbo].[OFS_CRYDowJonesSancChangeList]
Go

CREATE PROCEDURE [dbo].[OFS_CRYDowJonesSancChangeList] (@calling_oper AS Code)
With Encryption
As
                Set NOCOUNT ON

                Declare @lastSdnUpdDate datetime

Select @lastSdnUpdDate = IsNull(LastSDNUpdate,0) from SDNDBStatus WITH(NOLOCK)

Select [Name], Program, Type, Status, Deleted, ListType, DataType
    From SDNTable WITH(NOLOCK) WHERE 
                (IsNull(LastmodifDate,0) > @lastSdnUpdDate OR
                                IsNull(CreateDate,0) > @lastSdnUpdDate) 
                AND (Status in (2,3,4))
                AND LISTTYPE IN
                                (
                                                SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 135) 
                                                INTERSECT
                                                SELECT ListType FROM OFAC..OFS_fnListTypesForOperByView(@calling_oper, 100)
                                )
                AND Program like 'DJ-%'  
     Order by ListType, Deleted
Go
----------------------------------------------------------------------------------------------
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 55 )
                INSERT INTO [NoteType] ([Code], [Name],[CreateOper],[LastModify],[LastOper])VALUES
                (55,'Country','Prime',GetDate(),'Prime')
Go

IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 54)
                INSERT INTO [NoteType] ([Code], [Name],[CreateOper],[LastModify],[LastOper])VALUES
                (54,'SanctionReferenceName','Prime',GetDate(),'Prime')
Go

IF Not Exists(SELECT * FROM [ObjectType] WHERE Code = 'DowJones')
INSERT INTO [ObjectType] ([Code], [Name],[CreateOper],[CreateDate],[LastOper])VALUES
                ('DowJones','Dow Jones','Prime',GetDate(),'Prime')
Go
IF exists (SELECT * FROM [NoteType] WHERE Code = 56 AND [Name] = 'CountryType')
Delete from [NoteType] where Code =56 AND [Name] = 'CountryType' 
Go
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 56)
INSERT INTO [NoteType] ([Code],[Name],[CreateOper],[LastModify],[LastOper]) VALUES
    (56,'Date of Registration','Prime',GetDate(),'Prime')
GO
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 57)
INSERT INTO [NoteType] ([Code],[Name],[CreateOper],[LastModify],[LastOper]) VALUES
    (57,'Date of Nationalisation','Prime',GetDate(),'Prime')
GO
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 58)
INSERT INTO [NoteType] ([Code],[Name],[CreateOper],[LastModify],[LastOper]) VALUES
    (58,'Date of Privatisation','Prime',GetDate(),'Prime')
GO
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 59)
INSERT INTO [NoteType] ([Code],[Name],[CreateOper],[LastModify],[LastOper]) VALUES
    (59,'Date of Cessation','Prime',GetDate(),'Prime')
GO
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 60)
INSERT INTO [NoteType] ([Code],[Name],[CreateOper],[LastModify],[LastOper]) VALUES
    (60,'Additional Date','Prime',GetDate(),'Prime')
GO
IF Not Exists(SELECT * FROM [NoteType] WHERE Code = 61)
INSERT INTO [NoteType] ([Code],[Name],[CreateOper],[LastModify],[LastOper]) VALUES
    (61,'Reference Source','Prime',GetDate(),'Prime')
GO
----------------------------------------------------------------------------------------------
IF exists (SELECT * FROM [Report] WHERE Code = 'DJChgRpt' and Name = 'Dow Jones Data Load Difference Report')
Delete from [Report] where Code = 'DJChgRpt' and Name = 'Dow Jones Data Load Difference Report'
Go

IF Not Exists(SELECT * FROM [Report] WHERE Code = 'DJChgRpt' and Name = 'Dow Jones Data Load Difference Report')
insert into Report ( Code, Name, Category, FileName, Enabled,  ReportType,
            ReportLoc, CreateOper,CreateDate, LastOper, LastModify )
  values ( 'DJChgRpt', 'Dow Jones Data Load Difference Report',
           'Audit Reports', 'DJChgList.rpt', 1, 1, 1, 'Prime',
           GetDate(), 'Prime', GetDate() )           
GO    

IF exists (SELECT * FROM [Report] WHERE Code = 'DJDataLoad' and Name = 'Dow Jones Data Load Report')
Delete from [Report] where Code = 'DJDataLoad' and Name = 'Dow Jones Data Load Report'
Go
       
IF Not Exists(SELECT * FROM [Report] WHERE Code = 'DJDataLoad' and Name = 'Dow Jones Data Load Report')
insert into Report ( Code, Name, Category, FileName, Enabled,  ReportType,
            ReportLoc, CreateOper,CreateDate, LastOper, LastModify )
  values ( 'DJDataLoad', 'Dow Jones Data Load Report',
           'Audit Reports', 'DJDataLoad.rpt', 1, 1, 1, 'Prime',
           GetDate(), 'Prime', GetDate() ) 
GO
-------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT *
           FROM   dbo.sysobjects
           WHERE  id = Object_id(N'[dbo].[OFS_AddSDNAlt]')
                  AND Objectproperty(id, N'IsProcedure') = 1)
  DROP PROCEDURE [dbo].[OFS_AddSDNAlt]

GO

CREATE PROCEDURE dbo.Ofs_addsdnalt @entNum         INT,
                                   @altNum         INT,
                                   @altType        CHAR(8),
                                   @altName        SANCTIONNAME,
                                   @firstName      SANCTIONNAME,
                                   @middleNames    SANCTIONNAME,
                                   @lastName       SANCTIONNAME,
                                   @remarks        VARCHAR(200),
                                   @userRec        INT,
                                   @listId         VARCHAR(15),
                                   @status         INT,
                                   @ListCreateDate DATETIME,
                                   @listModifDate  DATETIME,
                                   @oper           CODE
WITH encryption
AS
    DECLARE @trnCnt INT
                DECLARE @minSDNIndex int = 7000000
                DECLARE @maxSDNIndex int = 9999999
    SELECT @trnCnt = @@trancount -- Save the current trancount
    --Do not proceed with null in Alt. Names
    IF @altName IS NULL
       AND @entNum BETWEEN @minSDNIndex AND @maxSDNIndex
      RETURN 0

    IF @trnCnt = 0
      -- Transaction has not begun
      BEGIN TRAN ofs_addsdnalt
    ELSE
      -- Already in a transaction
      SAVE TRAN ofs_addsdnalt

    -- Processing starts here:
    DECLARE @now DATETIME
    DECLARE @stat INT
    DECLARE @text VARCHAR(255)
    DECLARE @Existing INT
    DECLARE @Unchanged INT

    SET @Existing = 0
    SET @Unchanged = 0

    SELECT @now = Getdate()

    IF Len(Rtrim(Ltrim(@altType))) = 0
      SELECT @altType = 'a.k.a.'

    --World Check block. Persist ID, Set status
    IF @entNum >= @minSDNIndex 
      BEGIN
          IF ( @altNum != 0 )
            BEGIN
                --Check by Primary Key
                SELECT @Existing = Count(altnum)
                FROM SDNAltTable with (nolock) 
                WHERE  entnum = @entNum
                       AND altnum = @altnum
                       AND Isnull(altname, '') = Isnull(@altName, '')
                       AND Isnull(alttype, '') = Isnull(@altType, '')
                       AND Isnull(firstname, '') = Isnull(@firstName, '')
                       AND Isnull(middlename, '') = Isnull(@middleNames, '')
                       AND Isnull(lastname, '') = Isnull(@lastName, '')
                       AND Isnull(listid, '') = Isnull(@ListID, '')
                       AND Isnull(remarks, '') = Isnull(@remarks, '')

                IF @Existing = 0
                  BEGIN
						 if @entNum>= 10000000     
                         Begin     
                              
                         select @Existing =  count(AltNum)     
                         from SDNAltTable with (nolock)     
                         where EntNum = @entNum and      
                               AltNum = @altnum and     
                               (IsNull(AltName,    '') <> IsNull(@altName, '') OR     
                               IsNull(AltType,       '') <> IsNull(@alttype,       '') OR     
                               IsNull(FirstName,      '') <> IsNull(@firstname,      '') OR     
                               IsNull(MiddleName, '') <> IsNull(@middleNames, '') OR     
                               IsNull(LastName,    '') <> IsNull(@lastname,    '') OR     
                               IsNull(ListID,   '') <> IsNull(@ListID,   '') OR     
                               IsNull(Remarks,    '') <> IsNull(@remarks,    ''))             
      
                             if @Existing = 0     
                             Begin  

		                      --Insert New record
		                      SET @altNum = 0
     
                             End     
                             Else     
                             Begin     
                                 -- Existing record modified     
                                 update SDNAltTable     
                                 set      
                                 Status  = 3 , --modified     
                                 AltName = @altName,      
                                     AltType = @alttype,            
                                     FirstName = @firstname,     
                                     MiddleName = @middleNames,      
                                     LastName =   @lastname,        
                                     ListID =  @ListID,     
                                 Remarks= @remarks     
                                 where EntNum = @entNum and      
                                       AltNum = @altnum     
                             End     
                                                              
                         end     
                         else     
                         begin     
                             --Insert New record     
                             set @altNum = 0     
                         end  

                  END
                ELSE
                  BEGIN
                      -- Existing record unchanged
                      UPDATE SDNAltTable 
                      SET    status = 1 --unchanged
                      WHERE  entnum = @entNum
                             AND altnum = @altnum
                  END
            END

          IF @Existing = 0
            BEGIN
                --Find record by alt name
                SELECT @Existing = Count(altnum)
                FROM   sdnalttable
                WHERE  entnum = @entNum
                       AND status = 4
                       AND Isnull(altname, '') = Isnull(@altName, '')

                IF @Existing = 0
                  SET @status = 2 --added
                ELSE
                  BEGIN
                      SELECT @Unchanged = Count(altnum)
                      FROM   sdnalttable
                      WHERE  entnum = @entNum
                             AND status = 4
                             AND Isnull(altname, '') = Isnull(@altName, '')
                             AND Isnull(alttype, '') = Isnull(@altType, '')
                             AND Isnull(firstname, '') = Isnull(@firstName, '')
                             AND Isnull(middlename, '') =
                                 Isnull(@middleNames, '')
                             AND Isnull(lastname, '') = Isnull(@lastName, '')
                             AND Isnull(listid, '') = Isnull(@ListID, '')
                             AND Isnull(remarks, '') = Isnull(@remarks, '')

                      IF @Existing = 1
                        BEGIN
                            --Unique entry exists
                            IF @Unchanged = 0
                              UPDATE sdnalttable
                              SET    alttype = @altType,
                                     firstname = @firstName,
                                     middlename = @middleNames,
                                     lastname = @lastName,
                                     listid = @ListID,
                                     remarks = @remarks,
                                     listmodifdate = @listModifDate,
                                     lastmodifdate = @now,
                                     lastoper = @oper,
                                     status = 3 --Modified					 
                              WHERE  entnum = @entNum
                                     AND status = 4
                                     AND Isnull(altname, '') =
                                         Isnull(@altName, '')
                            ELSE
                              UPDATE sdnalttable
                              SET    status = 1 -- unchanged					 
                              WHERE  entnum = @entNum
                                     AND status = 4
                                     AND Isnull(altname, '') =
                                         Isnull(@altName, '')
                        END
                      ELSE IF @Existing > 1
                         AND @Unchanged > 1
                        BEGIN
                            --Existing entry is not unique but no changes
                            UPDATE sdnalttable
                            SET    status = 1 -- unchanged					 
                            WHERE  entnum = @entNum
                                   AND status = 4
                                   AND Isnull(altname, '') =
                                       Isnull(@altName, '')
                                   AND Isnull(alttype, '') =
                                       Isnull(@altType, '')
                                   AND Isnull(firstname, '') =
                                       Isnull(@firstName, '')
                                   AND Isnull(middlename, '') =
                                       Isnull(@middleNames, '')
                                   AND Isnull(lastname, '') =
                                       Isnull(@lastName, '')
                                   AND Isnull(listid, '') = Isnull(@ListID, '')
                                   AND Isnull(remarks, '') =
                                       Isnull(@remarks, '')
                        END
                      ELSE
                        BEGIN
                            --Set new entry
                            SET @Existing = 0
                            SET @status = 2 --added
                        END
                  END
            END
		  if @entNum BETWEEN @minSDNIndex AND @maxSDNIndex       
     	  begin 

          IF EXISTS (SELECT altnum
                     FROM   sdnalttable
                     WHERE  entnum = @entNum
                            AND Isnull(altname, '') = Isnull(@altName, '')
                            AND Isnull(alttype, '') = Isnull(@altType, ''))
            BEGIN
                --Do not insert duplicated Alt Names with different case of letters
                IF Isnull(@status, 0) = 0 -- if unknown status
                  SET @status = 2 -- set new status
                UPDATE sdnalttable
                SET    alttype = @altType,
                       firstname = @firstName,
                       middlename = @middleNames,
                       lastname = @lastName,
                       listid = @ListID,
                       remarks = @remarks,
                       [status] = @status,
                       --listcreatedate = @listCreateDate,
                       listmodifdate = @listModifDate,
                       lastmodifdate = @now,
                       lastoper = @oper
                WHERE  entnum = @entNum
                       AND Isnull(altname, '') = Isnull(@altName, '')
                       AND Isnull(alttype, '') = Isnull(@altType, '')

                SET @Existing = 1
            END
		END
      END

    IF @Existing = 0
      BEGIN
          -- the following if block is added for
          -- user entered alt names
          IF @altNum = 0
            BEGIN
                --if there are no altnames available in the sdnalttable table
                --then set the altnum to 10000000
                SELECT @altNum = Max(altnum)
                FROM   sdnalttable (nolock)

                SELECT @altNum = Isnull(@altNum, 10000000)

                IF @altNum < 10000000
                  SET @altnum = 10000000

                SELECT @altNum = @altNum + 1
            END

          IF Isnull(@status, 0) = 0 -- if unknown status
            SET @status = 2 -- set new status
          INSERT INTO sdnalttable
                      (entnum,
                       altnum,
                       alttype,
                       altname,
                       firstname,
                       middlename,
                       lastname,
                       listid,
                       remarks,
                       status,
                       listcreatedate,
                       listmodifdate,
                       createdate,
                       lastmodifdate,
                       lastoper)
          VALUES      (@entNum,
                       @altNum,
                       @altType,
                       @altName,
                       @firstName,
                       @middleNames,
                       @lastName,
                       @ListID,
                       @remarks,
                       @status,
                       @listCreateDate,
                       @listModifDate,
                       @now,
                       @now,
                       @oper )
      END

    SELECT @stat = @@ERROR

    IF @stat = 0
      BEGIN
          EXEC Ofs_setfileimageflag

          IF @trnCnt = 0
            COMMIT TRANSACTION ofs_addsdnalt
      END
    ELSE
      ROLLBACK TRANSACTION ofs_addsdnalt

    RETURN @stat

go

-- 6.16.2015 Performance indexes added for Dow Jones EMEA list import
USE [OFAC]
GO
/****** Object:  Table [dbo].[SDNTable]    Script Date: 04/27/2015 10:32:43 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SDNTable]') AND name = N'ListId_Program_Type_SDNTable_IDX')
                CREATE NONCLUSTERED INDEX [ListId_Program_Type_SDNTable_IDX] ON [dbo].[SDNTable] 
                (
                                [ListID] ASC,
                                [Program] ASC,
                                [Type] ASC
                )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SDNTable]') AND name = N'ListId_SDNTable_IDX')
                CREATE NONCLUSTERED INDEX [ListId_SDNTable_IDX] ON [dbo].[SDNTable] 
                (
                                [ListID] ASC
                )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                GO

/****** Object:  Table [dbo].[Relationship]    Script Date: 04/27/2015 10:32:43 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Relationship]') AND name = N'EntNum_RelatedId_Relationship_IDX')
                CREATE NONCLUSTERED INDEX [EntNum_RelatedId_Relationship_IDX] ON [dbo].[Relationship] 
                (
                                [EntNum] ASC,
                                [RelatedID] ASC
                )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Relationship]') AND name = N'EntNum_RelatedId_Status_Relationship_IDX')
                CREATE NONCLUSTERED INDEX [EntNum_RelatedId_Status_Relationship_IDX] ON [dbo].[Relationship] 
                (
                                [EntNum] ASC,
                                [RelatedID] ASC,
                                [Status] ASC
                )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                GO

-- 6.16.2015 Performance indexes added for Dow Jones EMEA list import

-- New Function fn_IsNewestSDNFromDowJones

IF EXISTS (SELECT *
           FROM   dbo.sysobjects
           WHERE  id = object_id(N'fn_IsNewestSDNFromDowJones')
                  AND OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
    DROP FUNCTION [dbo].fn_IsNewestSDNFromDowJones;
GO

CREATE FUNCTION dbo.fn_IsNewestSDNFromDowJones
( )
RETURNS BIT
WITH ENCRYPTION
-- Function returns 1 if the last modified record is a Dow Jones
-- Record.  0 if it is not.
AS
BEGIN
    DECLARE @SDNCount AS INT;
    SELECT @SDNCount = (SELECT Count(*)
                        FROM   (SELECT   TOP 1 EntNum,
                                               ListType,
                                               ListID,
                                               Program,
                                               LastModifDate
                                FROM     dbo.SDNTable WITH (NOLOCK)
                                ORDER BY LastModifDate DESC) AS sdn
                        WHERE  ListType = 'USER'
                               AND ListID LIKE 'DJ%');
    RETURN CAST (@SDNCount AS BIT);
END
GO

-- Update to handle Dow Jones Data loads and Incremental Build Images
IF EXISTS (SELECT *
           FROM   dbo.sysobjects
           WHERE  id = object_id(N'[dbo].[OFS_SetLastSDNUpd]')
                  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].OFS_SetLastSDNUpd;


GO
CREATE PROCEDURE OFS_SetLastSDNUpd
@distDate DATETIME
WITH ENCRYPTION
AS
DECLARE @trnCnt AS INT;
SELECT @trnCnt = @@trancount; -- Save the current trancount
IF @trnCnt = 0
    -- Transaction has not begun
    BEGIN TRANSACTION OFS_SetLastSDNUpd;
ELSE
    -- Already in a transaction
    SAVE TRANSACTION OFS_SetLastSDNUpd;
-- Processing starts here:
DECLARE @ctr AS INT;
DECLARE @now AS DATETIME;
DECLARE @stat AS INT;
DECLARE @isDowJonesUpdate AS BIT;

-- Add one second to the Datetime to account for a Precision Loss
-- in the Incremental File Image Builder.
SELECT @now = DATEADD(ss,1,GETDATE());

SELECT *
FROM   SDNdbStatus;

SELECT @stat = @@ERROR,
       @ctr = @@ROWCOUNT;

SELECT @isDowJonesUpdate = dbo.fn_IsNewestSDNFromDowJones();       
       
IF @stat = 0
    BEGIN
        IF @ctr > 0
            -- Update the LastSDNUpdate with the date previous distribution was loaded
            -- and Distributiondate with the current distribution date
            -- when the current distribution in the system is not the same as
            -- one being currently loaded
            BEGIN
                UPDATE  SDNDbStatus
                    SET DistributionDate = @distDate,
                        LastSDNUpdate    = CurrSDNUpdate,
                        CurrSDNUpdate    = @now,
                        SDNUpdateCtr     = SDNUpdateCtr + 1,
                        MemImgBuildReq   = 1
                WHERE   DistributionDate <> @distDate;
                -- Do not Update the LastSDNUpdate with the previous distribution date
                -- and Distributiondate with the current distribution date
                -- when the current distribution in the system is same as
                -- one being currently loaded. This will help retain the incrementals.
                                                                
                -- Check for Dow Jones because the CurrSDNUpdate must always be updated
                -- because this data is loaded over multiple loads.  Incremental Build
                -- Images need this date to equal the last load for Dow Jones.
                IF @isDowJonesUpdate = 1
                    BEGIN
                        UPDATE  SDNDbStatus
                            SET SDNUpdateCtr   = SDNUpdateCtr + 1,
                                MemImgBuildReq = 1,
                  
                                CurrSDNUpdate  = @now  
                        WHERE   DistributionDate = @distDate;
                    END
                ELSE
                    BEGIN
                        UPDATE  SDNDbStatus
                            SET SDNUpdateCtr   = SDNUpdateCtr + 1,
                               MemImgBuildReq = 1
                        WHERE   DistributionDate = @distDate;
                    END
            END
        ELSE
            INSERT  INTO SDNDbStatus (DistributionDate, DateInstalled, CurrSDNUpdate, SDNUpdateCtr, DbModifCtr, MemImgBuildCtr, MemImgBuildReq)
            VALUES                  (@distDate, @now, @now, 1, 0, 0, 1);
        SELECT @stat = @@ERROR;
        IF @stat = 0
            BEGIN
                IF @trnCnt = 0
                    COMMIT TRANSACTION OFS_SetLastSDNUpd;
            END
        ELSE
            ROLLBACK TRANSACTION OFS_SetLastSDNUpd;
    END
ELSE
    ROLLBACK TRANSACTION OFS_SetLastSDNUpd;
RETURN @stat;
GO

-- Deleting the unwanted stored procedure OFS_GetDowJonesMaxListDate from Database--

If exists (select * from dbo.sysobjects 
         where id = object_id(N'[dbo].[OFS_GetDowJonesMaxListDate]') and  
                     OBJECTPROPERTY(id, N'IsProcedure') = 1)    
     drop procedure [dbo].[OFS_GetDowJonesMaxListDate] 
Go    


If exists (select top 1 * from dbo.OptionTbl with (nolock) where Code = 'DATABNKOPT')
	update OptionTbl set Name = 'DataBank Option - Configure system for usage of Prime DataBank Lists/User Defined Lists'
	where Code = 'DATABNKOPT'
go

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'OFS_ImportSDN')
 Begin
	DROP PROCEDURE [dbo].OFS_ImportSDN
 End
GO
CREATE PROCEDURE dbo.OFS_ImportSDN( @entNum int,      
            @name SanctionName,      
            @firstName SanctionName,      
            @middleName SanctionName,      
            @lastName SanctionName,      
            @type char (12),      
            @program char(50),      
            @title varchar(200),      
            @callSign char(8),      
            @vessType char(25),      
            @tonnage char(14),      
            @grt char(8),      
            @vessFlag varchar(40),      
            @vessOwner varchar(150) ,      
            @dob varchar(150) ,      
            @remarks text ,      
            @remarks1 varchar(255) ,      
            @remarks2 varchar(255) ,      
            @sdnType int,      
            @dataType int,      
            @userRec int ,      
            @country varchar(250),      
            @sex char,      
            @build varchar(15),      
            @complexion varchar(15),      
            @race varchar(15),      
            @eyes varchar(15),      
            @hair varchar(15),      
            @height int,      
            @weight int,      
            @listID varchar(15),      
            @listType varchar(15),      
            @primeAdded bit,      
            @standardOrder bit,      
            @effectiveDate datetime,      
            @expiryDate datetime,      
            @status int,      
            @listCreateDate datetime,      
            @listModifDate dateTime,      
			@oper Code,      
            @ignDrvNames BIT,       
  		    @retEntnum int OUTPUT,      
		    @retStatus int OUTPUT)      
WITH ENCRYPTION       
AS      
BEGIN      
  declare @event   varchar(3)      
  --Transaction       
  declare @trnCnt        int      
  -- Processing starts here:      
  declare @now datetime      
  declare @pos int      
  declare @stat int      
  declare @text varchar(255)      
  declare @maxNum int      
  
  select @now = GETDATE()      
  
  select @trnCnt = @@trancount  -- Save the current trancount      
  
  if @trnCnt = 0      
  -- Transaction has not begun      
  begin tran OFS_ImportSDN      
  else      
  -- Already in a transaction      
  save tran OFS_ImportSDN      
  
  Declare @list TABLE(Code VARCHAR(20))      
  insert into @list      
  SELECT code FROM ListType      
  WHERE code in      
  (      
  SELECT listtype from dbo.OFS_GetScopeForListType a       
   INNER JOIN psec.dbo.SEC_fnGetScopeForOperrights(@oper, 'MODOFACLST,DSPOFACLST') b --Rights hardcoded  
      ON a.branchdept = b.branchdept      
  )       
  AND enabled = 1 and isprimelist = 0      
      
	  if NOT Exists (Select Code from @list where code=@listType)      
	  Begin   
			Select @retStatus=201 --No Access Rights Value.  
			Select @retEntNum=0  
			SET @STAT = 1       
			GOTO ENDOFPROC      
	  END  
	  ELSE  
	  BEGIN   
		   If @type = ''      
		   select @type = 'other'      
		   Select ISNULL(@ignDrvNames, 1)      
		   If Exists(SELECT EntNum FROM SDNTable WHERE ListId=@listid AND userrec=@userRec)      
		   BEGIN      
				BEGIN TRY  
				  SELECT @EntNum=EntNum FROM SDNTable WHERE ListId=@listid AND userrec=@userRec      
				  UPDATE SDNTable SET      
				   [name]=@name,      
				   firstName=@firstName ,      
				   middleName=@middleName,      
				   lastName=@lastName ,      
				   type=@type ,      
				   program=UPPER(@program) ,      
				   title=@title ,      
				   callSign=@callSign ,      
				   vessType=@vessType ,      
				   tonnage=@tonnage ,      
				   grt=@grt ,      
				   vessFlag=@vessFlag ,      
				   vessOwner=@vessOwner ,      
				   dob=@dob ,      
				   remarks=@remarks ,      
				   remarks1=@remarks1 ,      
				   remarks2=@remarks2 ,      
				   sdnType=@sdnType ,      
				   dataType=@dataType ,      
				   userRec=@userRec ,      
				   IgnoreDerived = @ignDrvNames ,      
				   country=@country ,      
				   sex=@sex ,      
				   build=@build ,      
				   complexion=@complexion ,      
				   race=@race ,      
				   eyes=@eyes ,      
				   hair=@hair ,      
				   height=@height ,      
				   weight=@weight ,      
				   listID=@listID,      
				   listType=@listType ,      
				   primeAdded=@primeAdded ,      
				   standardOrder=@standardOrder ,      
				   expiryDate=@expiryDate ,      
				   status=3 , --Insert Status Value     
				   listModifDate=@listModifDate , 
				   LastModifDate=@now,   
				   lastoper=@oper      
				  WHERE EntNum = @EntNum      
				  delete from Notes where entnum = @EntNum      
					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

					update sdnaddrtable
					Set Status = 4 --deleted
					where entnum = @EntNum

					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

					update sdnalttable
					Set Status = 4 --deleted
					where entnum = @EntNum

					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

					delete from urls where entnum = @EntNum      

					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

					Update dobs
					Set Status = 4 --deleted
					where entnum = @EntNum

					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

				   delete from relationship where entnum = @EntNum      
				   SELECT @stat = @@ERROR 
					  IF @stat <> 0  GOTO ENDOFPROC
				   delete from relationship where relatedid = @EntNum      
         		   SELECT @stat = @@ERROR 
					  IF @stat <> 0  GOTO ENDOFPROC    
			

				  Select @retStatus=203 --Update Values.     
				  Select @event = 'MOD'      
				END TRY  
				BEGIN CATCH  
				   SELECT ERROR_NUMBER()   
				   Select @stat = @@ERROR      
				   Select @retStatus=204  
				   GOTO ENDOFPROC  
				END CATCH  
		   END      
		   ELSE      
		   BEGIN      
			 BEGIN TRY  
				  Select @maxNum = Max(EntNum) From SDNTable       
				  If @maxNum Is NULL Set @maxNum = 0      
				  if @maxNum < 10000000      
				   Set @maxNum = 10000000      
				  else      
				   Set @maxNum = @maxNum +1      
				  Set @entNum = @maxNum      
			  
				  Insert into SDNTable (EntNum, [Name], FirstName,      
				   MiddleName, LastName,Title,      
				   Program, Type, CallSign,      
				   VessType, Tonnage, GRT,      
				   VessFlag, VessOwner, Dob,      
				   Remarks, Remarks1, Remarks2,SDNType,      
				   DataType, userRec, deleted,      
				   duplicRec, IgnoreDerived,      
				   ListID, ListType, Country,      
				   Sex, Build, Height, Weight,      
				   Race, Complexion, eyes, hair,      
				   PrimeAdded, StandardOrder, EffectiveDate,      
				   ExpiryDate, Status, ListCreateDate,      
				   ListModifDate, CreateDate, LastModifDate,      
				   LastOper)      
				  Values      
				   (@entNum, @name, NULLIF(@firstName,''),      
				   NULLIF(@middleName,''), NULLIF(@lastName,''), @title,      
				   UPPER(@program),  @type,  @callSign,      
				   @vessType, @tonnage, @grt,      
				   @vessFlag, @vessOwner, NULLIF(@dob,''),      
				   @remarks, @remarks1, @remarks2, @sdnType,      
				   @dataType, @userRec, 0,      
				   0, @ignDrvNames,@listId, @listType, @country,      
				   @sex, @build, NullIf(@height,0), NullIf(@weight,0),      
				   @race, @complexion, @eyes, @hair,@primeAdded,  
				   @standardOrder, @effectiveDate,@expiryDate, 2,  
				   @listCreateDate,@listModifDate, @now, @now, @oper )      
				  Select @retStatus=202 --Insert Status Value     
				  Select @event = 'CRE'      
			 END TRY  
			 BEGIN CATCH  
				  SELECT ERROR_NUMBER()   
				  Select @stat = @@ERROR      
				  Select @retStatus=204  
				  GOTO ENDOFPROC  
			 END CATCH  
			END      
	  
		Select @retEntNum = @entNum      
	        
		If @stat = 0 Begin      
			 Exec OFS_InsEvent @oper, @event, 'SanName', @name, Null      
			 select @stat = @@ERROR      
			 If @stat = 0 Begin      
				 Exec OFS_SetFileImageFlag      
			 End      
		End      
    
  End  
  EndofProc:      
  IF @trnCnt = 0 BEGIN      
	   COMMIT TRAN OFS_ImportSDN
	   END ELSE IF @stat <> 0 BEGIN      
	   ROLLBACK TRAN OFS_ImportSDN      
  END     
         
RETURN @stat      
END 
GO

IF EXISTS (SELECT * FROM sysobjects
    WHERE ID = object_id(N'[dbo].[OFS_InsDOBs]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE Dbo.OFS_InsDOBs

go
Create PROCEDURE dbo.OFS_InsDOBs (
            @DOBsId         IDType,
            @EntNum         IDType,
            @dob            char (8),
            @status         int,
            @ListCreateDate Datetime,
            @ListModifDate  Datetime,
            @oper           SCode
) WITH ENCRYPTION AS
 -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_InsDOBs
  else -- Already in a transaction
    save tran OFS_InsDOBs
  -- End standard stored procedure transaction header

  declare @now datetime
  declare @stat int

declare @Existing  int 
  declare @Unchanged int 

  set @Existing  = 0
  set @Unchanged = 0

  select @now = GETDATE()

  --World Check block. Persist ID, Set status
   if @entNum >= 7000000 
   begin

		if ( @DOBsId != 0)
		 begin
			--Check by Primary Key
			select @Existing = count(DOBsId)
			from DOBs with (nolock)
			where EntNum = @entNum and 
				  DOBsId = @DOBsId and
				  DOB = @dob

			if @Existing = 0
			 begin
				If @entNum>= 10000000 
					begin
					select @Existing =  count(DOBsId)
						from DOBs with (nolock)
						where EntNum = @entNum and 
							  DOBsId = @DOBsId and
							  (IsNull(DOB,'') <> IsNull(@dob,''))
					 if @Existing = 0
					 Begin
						--Insert new record
						set  @DOBsId  = 0
					 End
					 Else
					 Begin
						-- Existing record modified
						update DOBs
						set 
						Status  = 3 , --modified
						DOB = @dob
						where EntNum = @entNum and 
						 DOBsId = @DOBsId
					 End
				End
				Else 				  	
				begin	
					--Insert new record
					set  @DOBsId  = 0
				end
			 end
			else
			 begin
				-- Existing record unchanged
				update DOBs
				set Status = 1 --unchanged
				where EntNum = @entNum and 
				       DOBsId = @DOBsId
			 end	
		  end

		if @Existing = 0
		 begin
			--Find record by DOB
			select @Existing = count(DOBsID)
			from DOBs with (nolock)
			where EntNum = @entNum and 
				  Status = 4 and
				DOB = @dob
              
        
			if @Existing = 0
				set @status = 2 --added
			else
		 		--DOB exists
				update DOBs
				set  Status = 1 -- unchanged					 
				where EntNum  = @entNum and
					  Status = 4 and 
					  DOB = @dob			   
	  end
	end

  if  @Existing = 0
   begin

		-- the following if block is added for
		-- user entered dobs
		if @DOBsId = 0
		 begin
			--if there are no dobs available in the DOBs table
			--then set the DOBsId to 10000000
			select @DOBsId = max(DOBsId) from DOBs (nolock)
			select @DOBsId = isnull(@DOBsId, 10000000)
			If @DOBsId < 10000000
				SET @DOBsId = 10000000

				select @DOBsId = @DOBsId + 1
		  end

			insert into DOBs (DOBsId, EntNum, DOB, Status, ListCreateDate,
                ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
			values (@DOBsId, @EntNum, @dob, @status, @listCreateDate,
  
              @ListModifDate, @oper, @now, @oper, @now)

    end

	-- Start standard stored procedure transaction footer
	select @stat = @@ERROR
	if @stat = 0
	  begin
		Exec OFS_SetFileImageFlag
		if @trnCnt = 0 commit tran OFS_InsDOBs
	  end
	else
		rollback tran OFS_InsDOBs

	return @stat
  -- End standard stored procedure transaction footer
GO
IF EXISTS (SELECT * FROM sysobjects
    WHERE ID = object_id(N'[dbo].[OFS_DelDOBsEntNum]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE Dbo.OFS_DelDOBsEntNum

GO

CREATE PROCEDURE DBO.OFS_DelDOBsEntNum (
                    @entnum IDType,
                    @oper    SCode
) WITH ENCRYPTION AS
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_DelDOBsEntNum
  else -- Already in a transaction
    save tran OFS_DelDOBsEntNum
  -- End standard stored procedure transaction header

  declare @stat int
	if @entNum between 7000000 and 9999999
		begin
			delete from DOBs where EntNum=@entnum
		end
	else
		begin		
			--World Check provides soft delete
			update DOBs set status=4 where EntNum=@entnum
		end
  -- Start standard stored procedure transaction footer
  select @stat = @@ERROR
  if @stat = 0
  begin
    if @trnCnt = 0 commit tran OFS_DelDOBsEntNum
  end
  else
    rollback tran OFS_DelDOBsEntNum
  return @stat
  -- End standard stored procedure transaction footer

GO
IF EXISTS (SELECT * FROM sysobjects
    WHERE ID = object_id(N'[dbo].[OFS_DelAllAltNamesForEntNum]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE Dbo.OFS_DelAllAltNamesForEntNum

GO

CREATE PROCEDURE DBO.OFS_DelAllAltNamesForEntNum (@EntNum IDType)
WITH ENCRYPTION AS
BEGIN
  -- Start standard stored procedure transaction header
  declare @stat int
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_DelAllAltNamesForEntNum
  else -- Already in a transaction
    save tran OFS_DelAllAltNamesForEntNum
  -- End standard stored procedure transaction header

if @entnum between 7000000 and 9999999
	begin
		--World check
		update SDNAltTable
		set Deleted = 1
		where EntNum=@entnum and Status <> 4
	end
   else
	begin
		update dbo.SDNAltTable
			set status= 4
			where EntNum=@entnum 
		
    end


  -- Start standard stored procedure transaction footer
  select @stat = @@ERROR
  if @stat = 0
  begin
    if @trnCnt = 0 commit tran OFS_DelAllAltNamesForEntNum
  end
  else
    rollback tran OFS_DelAllAltNamesForEntNum
  return @stat
  -- End standard stored procedure transaction footer
END
GO
 IF EXISTS (SELECT * FROM sysobjects
    WHERE ID = object_id(N'[dbo].[OFS_DelAllAddrForEntNum]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE Dbo.OFS_DelAllAddrForEntNum

GO

CREATE PROCEDURE DBO.OFS_DelAllAddrForEntNum (@EntNum IDType)
WITH ENCRYPTION AS
BEGIN
  -- Start standard stored procedure transaction header
  declare @stat int
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_DelAllAddrForEntNum
  else -- Already in a transaction
    save tran OFS_DelAllAddrForEntNum
  -- End standard stored procedure transaction header

  
  if @entNum between 7000000 and 9999999
   begin
		--World Check provides soft delete
		Update SDNAddrTable
		set Deleted = 1
		WHERE ENTNUM = @EntNum
   end
  else
    begin
		
			update dbo.SDNAddrTable 
			set Status = 4
			WHERE ENTNUM = @EntNum

    end

  -- Start standard stored procedure transaction footer
  select @stat = @@ERROR
  if @stat = 0
  begin
    if @trnCnt = 0 commit tran OFS_DelAllAddrForEntNum
  end
  else
    rollback tran OFS_DelAllAddrForEntNum
  return @stat
  -- End standard stored procedure transaction footer
END
go

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_AddSDNAddr]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_AddSDNAddr]
Go


CREATE PROCEDURE dbo.OFS_AddSDNAddr @entNum int,
        @addrNum int,
        @address varchar(100),
        @city varchar(50),
        @country varchar(250),
        @remarks varchar(200),
        @userRec int,
        @listId varchar(15),
        @freeformat SDNAddress,
        @address2 varchar(50),
        @address3 varchar(50),
        @address4 varchar(50),
        @state varchar(50),
        @postalcode varchar(16),
        @status int,
        @listCreateDate datetime,
        @listModifDate datetime,
        @oper Code
With Encryption As
  declare @trnCnt int

  select @trnCnt = @@trancount  -- Save the current trancount
  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_AddSDNAddr
  else
    -- Already in a transaction
    save tran OFS_AddSDNAddr

  -- Processing starts here:

  declare @now datetime
  declare @stat int
  declare @text varchar(255)

 declare @Existing  int 
  declare @Unchanged int 

  set @Existing  = 0
  set @Unchanged = 0

  select @now = GETDATE()

  --World Check block. Persist ID, Set status
   if @entNum >= 7000000 
   begin

		if (  @addrNum  != 0)
		 begin
			--Check by Primary Key
			select @Existing =  count(AddrNum)
			from SDNAddrTable with (nolock)
			where EntNum = @entNum and 
				  AddrNum =  @addrNum and
				  IsNull(Address,    '') = IsNull(@address,    '') and
				  IsNull(City,       '') = IsNull(@city,       '') and
				  IsNull(State,      '') = IsNull(@State,      '') and
				  IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
				  IsNull(Country,    '') = IsNull(@Country,    '') and
				  IsNull(Address2,   '') = IsNull(@address2,   '') and
				  IsNull(Address3,   '') = IsNull(@address3,   '') and
				  IsNull(Address4,   '') = IsNull(@address4,   '') and
				  IsNull(FreeFormat, '') = IsNull(@freeformat, '') and
				  IsNull(ListID,     '') = IsNull(@listid,     '') and 
				  IsNull(Remarks,    '') = IsNull(@remarks,    '')

			if @Existing = 0
				begin
					if @entNum>= 10000000
						Begin
						--Check by Primary Key
						select @Existing =  count(AddrNum)
						from SDNAddrTable with (nolock)
						where EntNum = @entNum and 
							  AddrNum =  @addrNum and
							  (IsNull(Address,    '') <> IsNull(@address,    '') OR
							  IsNull(City,       '') <> IsNull(@city,       '') OR
							  IsNull(State,      '') <> IsNull(@State,      '') OR
							  IsNull(PostalCode, '') <> IsNull(@PostalCode, '') OR
							  IsNull(Country,    '') <> IsNull(@Country,    '') OR
							  IsNull(Address2,   '') <> IsNull(@address2,   '') OR
							  IsNull(Address3,   '') <> IsNull(@address3,   '') OR
							  IsNull(Address4,   '') <> IsNull(@address4,   '') OR
							  IsNull(FreeFormat, '') <> IsNull(@freeformat, '') OR
							  IsNull(ListID,     '') <> IsNull(@listid,     '') OR 
							  IsNull(Remarks,    '') <> IsNull(@remarks,    ''))		

							if @Existing = 0
							Begin

								--Insert new record
								set  @addrNum = 0

							End
							Else
							Begin
								-- Existing record modified
								update SDNAddrTable
								set 
								Status  = 3 , --modified
								Address =  @address, 
								City     = @city,       
								State    = @State,     
								PostalCode = @PostalCode,
								Country    =   @Country,
								Address2   =  @address2,  
								Address3   = @address3,  
								Address4   = @address4,  
								FreeFormat  = @freeformat,
								ListID     = @listid,     
								Remarks =   @remarks  
								where EntNum = @entNum and 
									  AddrNum =  @addrNum
							End								
						end
					else  -- else for @entNum>= 10000000
			 		begin
						--Insert new record
						set  @addrNum = 0
			 		end
				end	
			else
			 begin
				-- Existing record unchanged
				update SDNAddrTable
				set Status = 1 --unchanged
				where EntNum = @entNum and 
				      AddrNum =  @addrNum  
			 end	
		  end

		if @Existing = 0
		 begin
			--Find record by Address
			select @Existing = count(AddrNum)
			from SDNAddrTable with (nolock)
			where EntNum = @entNum and 
				  Status = 4 and
				IsNull(Address, '') = IsNull(@address, '') and
				IsNull(City, '') = IsNull(@city, '') and
				IsNull(State, '') = IsNull(@State, '') and
				IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
				IsNull(Country, '') = IsNull(@Country, '') 

              
        
			if @Existing = 0
				set @status = 2 --added
			else
			 begin
				select @Unchanged = count(AddrNum)
				from SDNAddrTable
				where EntNum = @entNum and 
				      Status = 4 and
					  IsNull(Address,    '') = IsNull(@address,    '') and
					  IsNull(City,       '') = IsNull(@city,       '') and
					  IsNull(State,      '') = IsNull(@State,      '') and
					  IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
					  IsNull(Country,    '') = IsNull(@Country,    '') and
					  IsNull(Address2,   '') = IsNull(@address2,   '') and
					  IsNull(Address3,   '') = IsNull(@address3,   '') and
					  IsNull(Address4,   '') = IsNull(@address4,   '') and
					  IsNull(FreeFormat, '') = IsNull(@freeformat, '') and
					  IsNull(ListID,     '') = IsNull(@listid,     '') and 
					  IsNull(Remarks,    '') = IsNull(@remarks,    '')
             

				if @Existing = 1
				  begin
					--Unique entry exists
					if @Unchanged  = 0
						update SDNAddrTable
						set  Address2   = @address2,
							 Address3   = @address3,
							 Address4   = @address4,
							 FreeFormat = @freeformat,
							 ListID = @ListID, 
							 Remarks = @remarks,
							 ListModifDate = @listModifDate,
							 LastModifDate =  @now, 
							 LastOper = @oper,
							 Status = 3 --Modified					 
						where EntNum  = @entNum and
							  Status = 4 and 
							  IsNull(Address,    '') = IsNull(@address,    '') and
							  IsNull(City,       '') = IsNull(@city,       '') and
							  IsNull(State,      '') = IsNull(@State,      '') and
							  IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
							  IsNull(Country,    '') = IsNull(@Country,    '') 

					else
						update SDNAddrTable
						set  Status = 1 -- unchanged					 
						where EntNum  = @entNum and
							  Status = 4 and 
							  IsNull(Address,    '') = IsNull(@address,    '') and
							IsNull(City,       '') = IsNull(@city,       '') and
							IsNull(State,      '') = IsNull(@State,      '') and
							IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
							IsNull(Country,    '') = IsNull(@Country,    '') 
                      
				 end
				else if @Existing > 1 and @Unchanged  > 1
				 begin
					--Existing entry is not unique but no changes
					update SDNAddrTable
					set  Status = 1 -- unchanged					 
					where EntNum = @entNum and 
						  Status = 4 and
						  IsNull(Address,    '') = IsNull(@address,    '') and
						  IsNull(City,       '') = IsNull(@city,       '') and
						  IsNull(State,      '') = IsNull(@State,      '') and
						  IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
						  IsNull(Country,    '') = IsNull(@Country,    '') and
						  IsNull(Address2,   '') = IsNull(@address2,   '') and
						  IsNull(Address3,   '') = IsNull(@address3,   '') and
						  IsNull(Address4,   '') = IsNull(@address4,   '') and
						  IsNull(FreeFormat, '') = IsNull(@freeformat, '') and
						  IsNull(ListID,     '') = IsNull(@listid,     '') and 
						  IsNull(Remarks,    '') = IsNull(@remarks,    '')

				 end
				else
				 begin
					--Set new entry
					set @Existing = 0
					set @status = 2 --added
				 end
			end
		end
   end

  if  @Existing = 0
   begin

		-- the following if block is added for
		-- user entered addr
		if @addrNum = 0
		 begin
			--if there are no addr available in the SDNAddrTable
			--then set the addrnum to 10000000
			select @addrNum = max(addrNum) from SDNAddrTable (nolock)
			select @addrNum = isnull(@addrNum, 10000000)
			If @addrNum < 10000000
				SET @addrNum = 10000000

				select @addrNum = @addrNum + 1
		  end

		Insert into SDNAddrTable (EntNum, AddrNum, Address, City, Country, Remarks,
					ListId, FreeFormat, Address2, Address3, Address4,
					State, PostalCode, Status, ListCreateDate, ListModifDate,
					CreateDate, LastModifDate, LastOper)
		Values (@entNum, @addrNum, @address, @city, @country, @remarks,
				@listId, @freeFormat, @address2, @address3, @address4,
				@state, @postalCode, @status, @listCreateDate, @listModifDate,
				@now, @now, @oper )
	end

	Select @stat = @@ERROR

	If @stat = 0
	 Begin
		Exec OFS_SetFileImageFlag
		if @trnCnt = 0
			commit transaction OFS_AddSDNAddr
	 End
	Else
		Rollback Transaction OFS_AddSDNAddr

  return @stat
GO
IF EXISTS (SELECT * FROM sysobjects
    WHERE ID = object_id(N'[dbo].[OFS_InsDOBs]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE Dbo.OFS_InsDOBs

GO

Create PROCEDURE dbo.OFS_InsDOBs (
            @DOBsId         IDType,
            @EntNum         IDType,
            @dob            char (8),
            @status         int,
            @ListCreateDate Datetime,
            @ListModifDate  Datetime,
            @oper           SCode
) WITH ENCRYPTION AS
 -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_InsDOBs
  else -- Already in a transaction
    save tran OFS_InsDOBs
  -- End standard stored procedure transaction header

  declare @now datetime
  declare @stat int

declare @Existing  int 
  declare @Unchanged int 

  set @Existing  = 0
  set @Unchanged = 0

  select @now = GETDATE()

  --World Check block. Persist ID, Set status
   if @entNum >= 7000000 
   begin

		if ( @DOBsId != 0)
		 begin
			--Check by Primary Key
			select @Existing = count(DOBsId)
			from DOBs with (nolock)
			where EntNum = @entNum and 
				  DOBsId = @DOBsId and
				  DOB = @dob

			if @Existing = 0
			 begin
				If @entNum>= 10000000 
					begin
					select @Existing =  count(DOBsId)
						from DOBs with (nolock)
						where EntNum = @entNum and 
							  DOBsId = @DOBsId and
							  (IsNull(DOB,'') <> IsNull(@dob,''))
					 if @Existing = 0
					 Begin
						--Insert new record
						set  @DOBsId  = 0
					 End
					 Else
					 Begin
						-- Existing record modified
						update DOBs
						set 
						Status  = 3 , --modified
						DOB = @dob
						where EntNum = @entNum and 
						 DOBsId = @DOBsId
					 End
				End
				Else 				  	
				begin	
					--Insert new record
					set  @DOBsId  = 0
				end
			 end
			else
			 begin
				-- Existing record unchanged
				update DOBs
				set Status = 1 --unchanged
				where EntNum = @entNum and 
				       DOBsId = @DOBsId
			 end	
		  end

		if @Existing = 0
		 begin
			--Find record by DOB
			select @Existing = count(DOBsID)
			from DOBs with (nolock)
			where EntNum = @entNum and 
				  Status = 4 and
				DOB = @dob
              
        
			if @Existing = 0
				set @status = 2 --added
			else
		 		--DOB exists
				update DOBs
				set  Status = 1 -- unchanged					 
				where EntNum  = @entNum and
					  Status = 4 and 
					  DOB = @dob			   
	  end
	end

  if  @Existing = 0
   begin

		-- the following if block is added for
		-- user entered dobs
		if @DOBsId = 0
		 begin
			--if there are no dobs available in the DOBs table
			--then set the DOBsId to 10000000
			select @DOBsId = max(DOBsId) from DOBs (nolock)
			select @DOBsId = isnull(@DOBsId, 10000000)
			If @DOBsId < 10000000
				SET @DOBsId = 10000000

				select @DOBsId = @DOBsId + 1
		  end

			insert into DOBs (DOBsId, EntNum, DOB, Status, ListCreateDate,
                ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
			values (@DOBsId, @EntNum, @dob, @status, @listCreateDate,
  
              @ListModifDate, @oper, @now, @oper, @now)

    end

	-- Start standard stored procedure transaction footer
	select @stat = @@ERROR
	if @stat = 0
	  begin
		Exec OFS_SetFileImageFlag
		if @trnCnt = 0 commit tran OFS_InsDOBs
	  end
	else
		rollback tran OFS_InsDOBs

	return @stat
  -- End standard stored procedure transaction footer
go

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_AddWorldCheckSDNAlt]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_AddWorldCheckSDNAlt]
Go
CREATE PROCEDURE dbo.OFS_AddWorldCheckSDNAlt @entNum int, @altNum int, @altType char(8),
            @altName SanctionName, @firstName SanctionName,
            @middleNames SanctionName, @lastName SanctionName,
            @remarks varchar(200),  @userRec int, @listId varchar(15),
            @status int, @ListCreateDate Datetime, @listModifDate Datetime,
            @oper Code
With Encryption As
  declare @trnCnt int

   --Do not proceed with null in Alt. Names
   if @altName is null and  @entNum between 7000000 and 9999999
		return 0

  select @trnCnt = @@trancount  -- Save the current trancount
  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_AddWorldCheckSDNAlt
  else
    -- Already in a transaction
    save tran OFS_AddWorldCheckSDNAlt

  -- Processing starts here:

  declare @now datetime
  declare @stat int
  declare @text varchar(255)

  declare @Existing  int 

  set @Existing  = 0


  select @now = GETDATE()

  if len(rtrim(ltrim(@altType))) = 0
       select @altType = 'a.k.a.'


  if @altNum  = 0
	begin	
			--Check if it is User modified
			--Find record by alt name
			select @Existing = count(AltNum)
			from SDNAltTable
			where EntNum = @entNum and 
				  Deleted = 1 and	
				  Status != 4 and	 
				  IsNull(AltName, '') = IsNull(@altName, '')

			if @Existing = 1
			 begin

				update SDNAltTable
				set  AltType = @altType, FirstName = @firstName,
					 MiddleName = @middleNames, LastName = @lastName,
					 ListID = @ListID, Remarks = @remarks,
					 ListModifDate = @listModifDate,
					 LastModifDate =  @now, 
					 LastOper = @oper,
					 Deleted = 0					 
				where EntNum = @entNum and 
					  Deleted = 1 and
					  Status != 4 and
				      IsNull(AltName, '') = IsNull(@altName, '')			
			 end
			else if @Existing > 1
			 begin
				--Handle duplicate names
				select top 1 @AltNum = AltNum
				from SDNAltTable
				where EntNum = @entNum and 
				  Deleted = 1 and
				   Status != 4 and 
				  IsNull(AltName, '') = IsNull(@altName, '')
				  order by AltNum


				update SDNAltTable
				set  AltType = @altType, FirstName = @firstName,
					 MiddleName = @middleNames, LastName = @lastName,
					 ListID = @ListID, Remarks = @remarks,
					 ListModifDate = @listModifDate,
					 LastModifDate =  @now, 
					 LastOper = @oper,
					 Deleted = 0					 
				where EntNum = @entNum and 
					  AltNum = @AltNum
			 end			 
  end
else
  begin
			select @Existing = count(AltNum)
			from SDNAltTable
			where EntNum = @entNum and 				 
				  AltNum = @AltNum

		    if @Existing > 0
			  begin
				update SDNAltTable
				set  AltName = @AltName, AltType = @altType, FirstName = @firstName,
					 MiddleName = @middleNames, LastName = @lastName,
					 ListID = @ListID, Remarks = @remarks,
					 ListModifDate = @listModifDate,
					 LastModifDate =  @now, 
					 LastOper = @oper,
					 Deleted = 0					 
				where EntNum = @entNum and 
					  AltNum = @AltNum
			  end
  end

  if  @Existing = 0
   begin
			if IsNull(@status, 0) = 0 -- if unknown status
			set @status = 2 -- set new status


			-- the following if block is added for
			-- user entered alt names
			if @altNum = 0
			 begin

				if exists (select AltNum from SDNAltTable
							where EntNum = @entNum and
							IsNull(AltName, '') = IsNull(@altName, ''))
					begin
						--Do not insert duplicated Alt Names with different case of letters
					   update   SDNAltTable
					   set		   AltType      = @altType, 
								   FirstName   = @firstName, 
								   MiddleName = @middleNames, 
								   LastName   = @lastName,
								   ListID = @ListID, 
								   Remarks = @remarks, 
								   [Status] = @status, 
								   ListCreateDate = @listCreateDate, 
								   ListModifDate = @listModifDate, 
								   LastModifDate = @now, 
								   LastOper = @oper						
					   where    EntNum = @entNum and
								   IsNull(AltName, '') = IsNull(@altName, '')

					end
			    end
				else
				begin
				
				--if there are no altnames available in the sdnalttable table
				--then set the altnum to 10000000
				select @altNum = max(altNum) from SDNAltTable (nolock)
				select @altNum = isnull(@altNum, 10000000)

				If @altNum < 10000000
					SET @altnum = 10000000
					select @altNum = @altNum + 1

					Insert into SDNAltTable (EntNum, AltNum, AltType, AltName, FirstName,
						  MiddleName, LastName, ListID, Remarks, [Status], ListCreateDate, ListModifDate,
						  CreateDate, LastModifDate, LastOper)
					Values
						(@entNum, @altNum, @altType, @altName, @firstName, @middleNames, @lastName,
						 @ListID, @remarks, @status, @listCreateDate, @listModifDate, @now, @now, @oper )
				end
    
   end

  Select @stat = @@ERROR
  If @stat = 0
    Begin
        Exec OFS_SetFileImageFlag
      if @trnCnt = 0
        commit transaction OFS_AddWorldCheckSDNAlt
    End
  Else
    Rollback Transaction OFS_AddWorldCheckSDNAlt
  return @stat
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_InsNotes]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_InsNotes]
Go

CREATE PROCEDURE dbo.OFS_InsNotes (
    @notesId  IdType,
    @entnum   IdType,
    @NoteType IDType,
    @Note     NoteDataType,
    @Status   INT,
    @ListCreateDate datetime,
    @ListModifDate  datetime,
    @oper     SCode
) WITH ENCRYPTION AS
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_InsNotes
  else -- Already in a transaction
    save tran OFS_InsNotes
  -- End standard stored procedure transaction header

  declare @now datetime
  declare @stat int

  declare @Existing  int 
  declare @Unchanged int 

  set @Existing  = 0
  set @Unchanged = 0

  select @now = GETDATE()

  --World Check block. Persist ID, Set status
  if @entNum between 7000000 and 9999999
   begin
		if ( @notesId  != 0)
		 begin
			--Check by Primary Key
			select @Existing = count(NotesId)
			from Notes
			where EntNum = @entNum and 
				  NotesID = @notesId and
				  Note = @Note and
				  NoteType = @NoteType 

			if @Existing = 0
			 begin
				--Insert new record
				set  @notesId  = 0
			 end
			else
			 begin
				-- Existing record unchanged
				update Notes
				set Status = 1 --unchanged
				where EntNum = @entNum and 
				      NotesId = @notesId  
			 end	
		  end

		if @Existing = 0
		 begin
			--Find record by Note
			select @Existing = count(NotesID)
			from Notes
			where EntNum = @entNum and 
				  Status = 4 and
				  Note = @Note and
				  NoteType = @NoteType
              
        
			if @Existing = 0
				set @status = 2 --added
			else
			 begin
				select @Unchanged = count(NotesID)
				from Notes
				where EntNum = @entNum and 
					  Status = 4 and
					  Note = @Note and
					  NoteType = @NoteType 
			
                  

				if @Existing = 1
				begin
					--Unique entry exists
					if @Unchanged  = 0
						update Notes
						set  NoteType = @NoteType,
							 ListModifDate = @listModifDate,
							LastModify =  @now, 
							LastOper = @oper,
							Status = 3 --Modified					 
						where EntNum  = @entNum and
							  Status = 4 and 
							Note = @Note and
							NoteType = @NoteType
					else
						update Notes
						set  Status = 1 -- unchanged					 
						where EntNum  = @entNum and
							  Status = 4 and 
							  Note = @Note and
							  NoteType = @NoteType		   
                      
			     end

			else if @Existing > 1 and @Unchanged  > 1
			  begin
				--Existing entry is not unique but no changes
				update Notes
				set  Status = 1 -- unchanged					 
				where EntNum = @entNum and 
					  Status = 4 and
					  Note = @Note and
					  NoteType = @NoteType 

			  end
			else
			  begin
				--Set new entry
				set @Existing = 0
				set @status = 2 --added
			  end
		   end
	end
   end

  if  @Existing = 0
   begin
		-- the following if block is added for
		-- user entered notes (attributes)
		if @notesId = 0
		 begin
			--if there are no notes available in the notes table
			--then set the notesid to 1
			select @notesId = max(notesid) from notes (nolock)
			select @notesId = isnull(@notesid, 10000000)
			if @notesid < 10000000
				SET @notesid = 10000000

			select @notesId = @notesId + 1
		 end
		 else
		 begin
			-- check whether notes available in the notes table
			-- if exists then set notesid using 10000000
			
			if exists (select 1 from Notes (nolock) where NotesId = @notesId)
			begin
				select @notesId = max(notesid) from notes (nolock)
				select @notesId = isnull(@notesid, 10000000)
				if @notesid < 10000000
					SET @notesid = 10000000

				select @notesId = @notesId + 1
			end			
		 end
		if IsNull(@status, 0) = 0 -- if unknown status
			set @status = 2 -- set new status

		 insert into Notes (Entnum, NotesId, NoteType, Note, Status, CreateOper, CreateDate, LastOper, LastModify)
		values (@entnum, @notesId, @NoteType, @Note, @Status, @oper, @now, @oper, @now)

 end

  -- Start standard stored procedure transaction footer
  select @stat = @@ERROR
  if @stat = 0
  begin
    Exec OFS_SetFileImageFlag
    if @trnCnt = 0 commit tran OFS_InsNotes
  end
  else
    rollback tran OFS_InsNotes
  return @stat
  -- End standard stored procedure transaction footer
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_InsURLs]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_InsURLs]
Go

CREATE PROCEDURE dbo.OFS_InsURLs (
            @urlsId         IDType,
            @entnum         IDType,
            @url            NoteDataType,
            @description    NoteDataType,
            @status         int,
            @listCreateDate datetime,
            @listModifDate  datetime,
            @oper           SCode
) WITH ENCRYPTION AS
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_InsURLs
  else -- Already in a transaction
    save tran OFS_InsURLs
  -- End standard stored procedure transaction header

  declare @now datetime
  declare @stat int

  declare @Existing  int 
  declare @Unchanged int 

  set @Existing  = 0
  set @Unchanged = 0

  select @now = GETDATE()

  --World Check block. Persist ID, Set status
  if @entNum between 7000000 and 9999999
   begin

		if ( @urlsId  != 0)
		 begin
			--Check by Primary Key
			select @Existing = count(URLsId)
			from URLs
			where EntNum = @entNum and 
				  URLsID = @urlsId and
				  URL = @url and
				  IsNull(Description, '') = IsNull(@description, '')

			if @Existing = 0
			 begin
				--Insert new record
				set  @urlsId  = 0
			 end
			else
			 begin
				-- Existing record unchanged
				update URLs
				set Status = 1 --unchanged
				where EntNum = @entNum and 
				      URLsID = @urlsId  
			 end	
		  end

		if @Existing = 0
		 begin
			--Find record by URL
			select @Existing = count(URLsID)
			from URLs
			where EntNum = @entNum and 
				  Status = 4 and
				  URL = @url
              
        
			if @Existing = 0
				set @status = 2 --added
			else
			  begin
				select @Unchanged = count(URLsID)
				from URLs
				where EntNum = @entNum and 
					  Status = 4 and
					URL = @url and
					IsNull(Description, '') = IsNull(@description, '')

				if @Existing = 1
				  begin
						--Unique entry exists
						if @Unchanged  = 0
							update URLs
							set  Description = @description,
								 ListModifDate = @listModifDate,
								 LastModify =  @now, 
								 LastOper = @oper,
								 Status = 3 --Modified					 
							where EntNum  = @entNum and
								  Status = 4 and 
								  URL = @url
						else
							update URLs
							set  Status = 1 -- unchanged					 
							where EntNum  = @entNum and
								  Status = 4 and 
								  URL = @url
				   
                      
				 end
				else if @Existing > 1 and @Unchanged  > 1
				 begin
						--Existing entry is not unique but no changes
						update URLs
						set  Status = 1 -- unchanged					 
						where EntNum = @entNum and 
							  Status = 4 and
							  URL = @url and
							  IsNull(Description, '') = IsNull(@description, '')

				 end
				else
				  begin
						--Set new entry
						set @Existing = 0
						set @status = 2 --added
				  end
			end
	end
   end

  if  @Existing = 0
   begin

		-- the following if block is added for
		-- user entered urls
		if @urlsId = 0
		 begin
			--if there are no URL available in the urls table
			--then set the urlsid to 1
			select @urlsId = max(urlsId) from urls (nolock)
			select @urlsId = isnull(@urlsId, 10000000)

			if @urlsId < 10000000
				SET @urlsId = 10000000

		   select @urlsId = @urlsId + 1
		  end
		else
		 begin
			-- check whether URL available in the urls table
			-- if exists then set urlsId using 10000000
			
			if exists (select 1 from urls (nolock) where urlsId = @urlsId)
			begin
				select @urlsId = max(urlsId) from urls (nolock)
			select @urlsId = isnull(@urlsId, 10000000)

			if @urlsId < 10000000
				SET @urlsId = 10000000

		   select @urlsId = @urlsId + 1
			end			
		 end
		  insert into URLs (URLsId, EntNum, URL, [Description],
			        Status, ListCreateDate, ListModifDate,
				    CreateOper, CreateDate, LastOper, LastModify)
		  values (@urlsId, @entnum, @url, @description,
			      @Status, @listCreateDate, @listModifDate,
                @oper, @now, @oper, @now)
	 end


  -- Start standard stored procedure transaction footer
  select @stat = @@ERROR
  if @stat = 0
  begin
    Exec OFS_SetFileImageFlag
    if @trnCnt = 0 commit tran OFS_InsURLs
  end
  else
    rollback tran OFS_InsURLs
  return @stat
  -- End standard stored procedure transaction footer
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_AddWorldCheckSDNAddr]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_AddWorldCheckSDNAddr]
Go

CREATE PROCEDURE dbo.OFS_AddWorldCheckSDNAddr @entNum int,
        @addrNum int,
        @address varchar(100),
        @city varchar (50),
        @country varchar(250),
        @remarks varchar(200),
        @userRec int,
        @listId varchar(15),
        @freeformat SDNAddress,
        @address2 varchar(50),
        @address3 varchar(50),
        @address4 varchar(50),
        @state varchar(50),
        @postalcode varchar(16),
        @status int,
        @listCreateDate datetime,
        @listModifDate datetime,
        @oper Code
With Encryption As
  declare @trnCnt int

  select @trnCnt = @@trancount  -- Save the current trancount
  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_AddWorldCheckSDNAddr
  else
    -- Already in a transaction
    save tran OFS_AddWorldCheckSDNAddr

  -- Processing starts here:

  declare @now datetime
  declare @stat int
  declare @text varchar(255)

  declare @Existing  int 
  

  set @Existing  = 0

  select @now = GETDATE()

 


 if (@addrNum = 0)
    begin
		--Find record by Address
		select @Existing = count(AddrNum)
		from SDNAddrTable
		where EntNum = @entNum and 
			  Deleted = 1 and 
			  Status != 4 and
			  IsNull(Address, '') = IsNull(@address, '') and
			  IsNull(City, '') = IsNull(@city, '') and
			  IsNull(State, '') = IsNull(@State, '') and
			  IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
			  IsNull(Country, '') = IsNull(@Country, '')            
        
         
				if @Existing = 1
				  begin
						update SDNAddrTable
						set  Address2   = @address2,
							 Address3   = @address3,
							 Address4   = @address4,
							 FreeFormat = @freeformat,
							 ListID = @ListID, 
							 Remarks = @remarks,
							 ListModifDate = @listModifDate,
							 LastModifDate =  @now, 
							 LastOper = @oper,
							 Deleted = 0					 
						where EntNum  = @entNum and
							  Deleted = 1 and
							  Status != 4 and 
							  IsNull(Address,    '') = IsNull(@address,    '') and
							  IsNull(City,       '') = IsNull(@city,       '') and
							  IsNull(State,      '') = IsNull(@State,      '') and
							  IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
							  IsNull(Country,    '') = IsNull(@Country,    '') 


                      
				 end
				else if @Existing > 1 
				 begin
						--Existing entry is not unique but no changes
						select top 1 @addrNum = AddrNum
						from SDNAddrTable
						where EntNum = @entNum and 
							Deleted = 1 and 
							Status != 4 and
							IsNull(Address, '') = IsNull(@address, '') and
							IsNull(City, '') = IsNull(@city, '') and
							IsNull(State, '') = IsNull(@State, '') and
							IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
							IsNull(Country, '') = IsNull(@Country, '')   
						order by AddrNum

						update SDNAddrTable
						set  Address2   = @address2,
							 Address3   = @address3,
							 Address4   = @address4,
							 FreeFormat = @freeformat,
							 ListID = @ListID, 
							 Remarks = @remarks,
							 ListModifDate = @listModifDate,
							 LastModifDate =  @now, 
							 LastOper = @oper,
							 Deleted = 0					 
						where EntNum  = @entNum and
							  AddrNum = @AddrNum

				 end	
   end
 else
   begin
			select @Existing = count(AddrNum)
			from SDNAddrTable
			where EntNum = @entNum and 				 
				  AddrNum = @AddrNum

		    if @Existing > 0
			  begin
					update SDNAddrTable
					set  Address = @address, 
						 City = @city, 
						 State = @State, 
						 PostalCode = @PostalCode, 
						 Country = @Country, 
					     Address2   = @address2,
						 Address3   = @address3,
						 Address4   = @address4,
						 FreeFormat = @freeformat,
						 ListID = @ListID, 
						 Remarks = @remarks,
						 ListModifDate = @listModifDate,
						 LastModifDate =  @now, 
						 LastOper = @oper,
						 Deleted = 0					 
					where EntNum  = @entNum and
						  AddrNum = @AddrNum
			  end
      end

  if  @Existing = 0
   begin

		-- the following if block is added for
		-- user entered addr	
		--if there are no addr available in the SDNAddrTable
		--then set the addrnum to 10000000
		select @addrNum = max(addrNum) from SDNAddrTable (nolock)
		select @addrNum = isnull(@addrNum, 10000000)
		If @addrNum < 10000000
			SET @addrNum = 10000000

		select @addrNum = @addrNum + 1		 

		Insert into SDNAddrTable (EntNum, AddrNum, Address, City, Country, Remarks,
					ListId, FreeFormat, Address2, Address3, Address4,
					State, PostalCode, Status, ListCreateDate, ListModifDate,
					CreateDate, LastModifDate, LastOper)
		Values (@entNum, @addrNum, @address, @city, @country, @remarks,
				@listId, @freeFormat, @address2, @address3, @address4,
				@state, @postalCode, 2, @listCreateDate, @listModifDate,
				@now, @now, @oper )
	end

	Select @stat = @@ERROR

	If @stat = 0
	 Begin
		Exec OFS_SetFileImageFlag
		if @trnCnt = 0
			commit transaction OFS_AddWorldCheckSDNAddr
	 End
	Else
		Rollback Transaction OFS_AddWorldCheckSDNAddr

  return @stat
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_SelViolByRef]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_SelViolByRef]
Go
create Procedure dbo.OFS_SelViolByRef (@ref as varchar(255),@seqnum as int Output)
With Encryption As

  Declare @stat int
  select @seqnum = SeqNumb from FilterTranTable WITH (NOLOCK) where Ref = @ref
  if @seqnum IS NOT NULL
  begin
		-- If it is a current OFAC Case, then a valid recordset needs to be returned.
         Select *,CASE COALESCE(ConfirmState,0)
             When 0 Then 'Unconfirmed'
             When 1 Then 'Confirmed'
             When 2 Then 'Waived'
             When 3 Then 'Delegated'
             Else ''
             End  as 'ConfirmStatus' from FilterTranTable WITH (NOLOCK) where Ref = @ref
  end
  else 
  BEGIN
		-- If it is an Archived OFAC Case, then a valid recordset needs to be returned.
		-- If the case does not exist, then an empty recordset needs to be returned.
		-- getSearchResults Web Service expects an empty recordset if the case does not exist.
         select @seqnum = SeqNumb from FilterTranHistTable WITH (NOLOCK) where Ref = @ref
             Select *,CASE COALESCE(ConfirmState,0)
             When 0 Then 'Unconfirmed'
             When 1 Then 'Confirmed'
             When 2 Then 'Waived'
             When 3 Then 'Delegated'
             Else ''
             End  as 'ConfirmStatus'from FilterTranHistTable WITH (NOLOCK) where Ref = @ref
  END
           
  select @seqnum
  select @stat = @@ERROR
  return @stat
GO


PRINT ''
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT 'Completed Conversion of OFAC Database to version 10.1.0  '
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT ''
GO

