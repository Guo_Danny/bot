﻿/*
**  File Name:		ConvPbsa.sql
**
**  Functional Description:
**
**  This module contains SQL conversion procedures for Prime version 10.1
**
**  Facility	    The Prime Central Database
**  Creation Date:  07/15/2016
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2009 - 2011 						 ***
*** FIS                                                                  ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in FIS.                                                              ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                   
				       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|----------------------------------------------------

07/11/2016      SM      I160012754: Cases are generated for the excluded 
			            relationship type specified in the CustXOverYDays
		                rule parameters. 
07/12/2016      SM      I150729212 & I150723373: Removed the unrelated 
			            parties activities from Alerts for the rules 
			            CustXOverYDays, USR_CstXOvrYInt, XPercInTypeOutType.  
07/14/2016      SM      I160369848: Cases generated from AtpActvCms based 
			            rules have no txnstied to them
07/14/2016      SM      I150381254: Fixed issue with parsing of ByOrder 
			            details from Alert Details in  USR_SameBeneByOrd 
			            stored proc Consider Alert Details "ByOrder: ‘XYZ’123’"
			            is parsed to XYZ so, the alert did not display the 
			            corresponding Activity record.

07/27/2016	    AS   	I160039680: The branch in the header is being 
						filled with incorrect information. 
						Changed the stored procedure 
						BSA_ActivityBranchesForCase and 
						BSA_ActivityBranchForCase.
12/21/2016	   SM		I160418302: Modified USR_HighRiskCountry rule
						to avoid the error "Invalid Length Parameter 
						passed to Substring function" when UseMonitor 
						value and IncludeAddress field parameters are 
						set to 1.
12/23/2016	   SM		I160534014 : Issue with keyword template rule.
						Previosly the Keyword" template rule was not 
						considering the transactions for the current date.
						Fixed the issue by adding the current day transactions
						by adding "<=" to consider the BookDate condition.
01/24/2016	   SM		I160908392 : Cases generated from USR_KeywordSearch rule
						is not pulling the related activity records.
01/24/2016     SM       Defect 6101: BSA_UpdCaseReportedStatus SP is dropping the
						Consecutive procedure while running BSACaseFeedback Utility
						as the SP is not ended properly.
02/09/2017	   SS	    I160163401: Currently unprocessed accounts are not processed  
	  				    properly if the accountinput file contains NULL value for some 
				        of the fields. To address this issue,required fields are converted 
				        as Empty in the stored procedure BSA_ImpUnprocAccount.

										
*/
					 
USE PBSA
GO


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print '  Starting Conversion of Pbsa Objects To Version 10.1'
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_CustXOverYDays]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.USR_CustXOverYDays
Go


CREATE PROCEDURE dbo.USR_CustXOverYDays (@WLCode SCode, @TestAlert INT,@excludeExemptCustList varchar(8000),
	@activityTypeList varchar(8000), @activityTypeExcludeList VARCHAR(8000),@cashTranIndicator INT,@day INT, 
	@minimumTotalAmount MONEY,@maximumTotalAmount MONEY,@minimumAmount MONEY, @maximumAmount MONEY,
	@minimumCount INT, @customerList VARCHAR(8000), @RiskClassList VARCHAR(8000), @CustomerTypeList VARCHAR(8000),
	@AccountTypeList VARCHAR(8000), @BranchList varchar(8000), 
	@DeptList varchar(8000), @UseRelAcct Bit, @UseRelCust Bit, 
    @ExcludeRelList varchar(8000)
) AS
/* RULE PARAMETERS -  THESE MUST BE SET ACCORDING TO THE RULE REQUIREMENT*/

/* RULE AND PARAMETER DESCRIPTION
	Detects customers that had transactions using a specified 
	activity type that exceeded a designated dollar amount over 
	a period of specified days.  Designed to be run Post-EOD. 
	
	@excludeExemptCustList = Type of Exemptions to exclude customers (Comma seperated or '-ALL-' or '-NONE-')
	@activityTypeList = Type of activity to be checked  (Comma seperated or '-ALL-') 
	@activityTypeExcludeList = Type of activity to be excluded (Comma seperated or '-NONE-') 
	@cashTranIndicator = Cash Tran indicator to include check on activity type 
	@minimumTotalAmount = Minimum Total Transaction Amount
	@maximumTotalAmount = Maximum Total Threshold amount
	@day = No of days activity to be considered
	@minimumAmount= Minimum threshold amount 
	@maximumAmount = Maximum Threshold Amount
	@minimumCount = Minimum Transaction Count
	@CustomerList = List of customer Id's to be checked
	@RiskClassList = List of risk classes of customer to be checked
	@AccountTypeList = List of Accounts to be checked (Comma Separated or '-ALL-')
	@CustomerTypeList = List of Customer Types
    @BranchList  = List of Branches to be checked (Comma Separated or '-ALL-')
    @DeptList    = List of Department to be checked (Comma Separated or '-ALL-')
	@UseRelAcct = Include Related Accounts (1=yes, 0=no) 
	@UseRelCust = Include Related Parties (1=yes, 0=no) 
	@ExcludeRelList = Exclude Relationship Types (comma separated list or '-NONE-')
*/

/*  Declarations */
DECLARE	@description 	VARCHAR(2000),
	@desc 		VARCHAR(2000),
	@Id 		INT, 
	@WLType 	INT,
	@stat 		INT,
	@trnCnt 	INT,
	@MINDATE 	INT,
	@StartDate 	DATETIME,
	@StartAlrtDate  DATETIME,
	@EndAlrtDate	DATETIME
SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_CustXOverYDays
ELSE
	-- Already in a transaction
	SAVE TRAN USR_CustXOverYDays
/* End standard stored procedure transaction header */

/*  standard Rules Header */
-- Date options
-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT  @description = [Desc], @WLType = WLType  ,
	@StartDate = 
	Case 	
		When UseSysDate in (0,1) Then
			-- use System date
			GetDate()
		When UseSysDate = 2 Then
			-- use business date
			(Select BusDate From dbo.SysParam)
		Else
			GetDate()
	End
FROM WatchList WITH (NOLOCK) WHERE WLCode = @WLCode

Declare @BaseCurr char(3)
select @BaseCurr = IsNULL(BaseCurr,'') from SysParam

Create table #TT 
(
	Cust 		VARCHAR(35) ,
	RecvPay		INT,
	TranAmt		MONEY

)

Create table #TT1
(
	Cust	VARCHAR(35) ,
	TranNo	INT,
	BaseAmt MONEY, 
	RecvPay INT)

--a week to to today
	SET @minDate = dbo.ConvertSqlDateToInt(DATEADD(d, -1 * @day, CONVERT(VARCHAR, @StartDate)))

Declare @sDATE INT
SET	@sDATE = dbo.ConvertSqlDateToInt(@StartDate)


	--SELECT @excludeExemptCustList = dbo.BSA_fnListParams(@excludeExemptCustList)
	IF LTRIM(RTRIM(@excludeExemptCustList)) = '-NONE-' OR LTRIM(RTRIM(@excludeExemptCustList)) = ''
		SELECT @excludeExemptCustList = NULL
	ELSE IF LTRIM(RTRIM(@excludeExemptCustList)) = '-ALL-'
			BEGIN
				SELECT @excludeExemptCustList = ''
				SELECT  @excludeExemptCustList = 		
					COALESCE( @excludeExemptCustList + ',','' )  + Code
					FROM ExemptionType 
					 WHERE (SubString(ExemptEntity,1,1) = 1)
					AND (SubString(ExemptFrom,3,1) = 1)
					
				SELECT  @excludeExemptCustList = @excludeExemptCustList + ','
			END
	ELSE                                           
	SELECT @excludeExemptCustList = ',' + 
		REPLACE(LTRIM(RTRIM(@excludeExemptCustList)),' ','')+ ','
		
	--SELECT @activityTypeExcludeList = dbo.BSA_fnListParams(@activityTypeExcludeList)
	IF LTRIM(RTRIM(@activityTypeExcludeList)) = '-NONE-' OR LTRIM(RTRIM(@activityTypeExcludeList)) = ''
		SELECT @activityTypeExcludeList = NULL
	ELSE
	SELECT @activityTypeExcludeList = ',' + 
		REPLACE(LTRIM(RTRIM(@activityTypeExcludeList)),' ','')+ ','
		   
	IF LTRIM(RTRIM(@ExcludeRelList)) = '-NONE-' OR LTRIM(RTRIM(@ExcludeRelList)) = ''
		SELECT @ExcludeRelList = NULL
	ELSE
	SELECT @ExcludeRelList = ',' + 
		REPLACE(LTRIM(RTRIM(@ExcludeRelList)),' ','')+ ','


	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	
	SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)
	SELECT @customerList = dbo.BSA_fnListParams(@customerList)
	SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	SELECT @customerTypeList = dbo.BSA_fnListParams(@customerTypeList)
	SELECT @AccountTypeList = dbo.BSA_fnListParams(@AccountTypeList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
	SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)
	SELECT @ExcludeRelList = dbo.BSA_fnListParams(@ExcludeRelList)
	
		
	INSERT INTO #TT1 
	SELECT distinct	 ActHist.Cust,TranNo, BaseAmt, RecvPay
	FROM 	ActivityHist ActHist WITH (NOLOCK) 
	INNER 	JOIN Customer Cust WITH (NOLOCK) ON ActHist.Cust = Cust.ID
	LEFT	JOIN Account Acct WITH (NOLOCK) ON ActHist.Account = Acct.ID 
	WHERE 	ActHist.bookdate >= @minDate 
    AND	ActHist.bookdate <= @SDate
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Cust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@ActivityTypeExcludeList,'') = '' OR
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeExcludeList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Cust.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Cust.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Cust.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(cust.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(cust.OwnerDept)) + ',', @DeptList) > 0))
	AND (
	(@cashTranIndicator = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR ---include activitytypelist if cash cashTran is 1
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0)))
	OR 
	(@cashTranIndicator = 2 AND (ActHist.CashTran = 1 OR ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
	OR 
	(@cashTranIndicator = 3 AND (ActHist.CashTran = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
    )
	AND	(@minimumAmount = -1 OR BaseAmt >= @minimumAmount) 
	AND (@maximumAmount = -1 OR BaseAmt <= @maximumAmount)
	   


	-- Related Parties
	INSERT INTO #TT1 
	SELECT 	distinct pr.PartyId ,TranNo, BaseAmt, RecvPay
	FROM 	ActivityHist ActHist WITH (NOLOCK) 
	Left	JOIN Account Acct WITH (NOLOCK) ON ActHist.Account = Acct.ID 
	INNER	JOIN PartyRelation pr WITH (NOLOCK) ON ActHist.Cust = pr.RelatedParty
	INNER	JOIN Customer RelCust WITH (NOLOCK) ON pr.PartyId = RelCust.ID
	WHERE 	ActHist.bookdate >= @minDate 
    AND	ActHist.bookdate <= @SDate
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@ActivityTypeExcludeList,'') = '' OR
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeExcludeList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerDept)) + ',', @DeptList) > 0))
	AND (
	(@cashTranIndicator = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR ---include activitytypelist if cash cashTran is 1
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0)))
	OR 
	(@cashTranIndicator = 2 AND (ActHist.CashTran = 1 OR ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
	OR 
	(@cashTranIndicator = 3 AND (ActHist.CashTran = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
    )
	AND	(@minimumAmount = -1 OR BaseAmt >= @minimumAmount) 
	AND (@maximumAmount = -1 OR BaseAmt <= @maximumAmount)
	AND @UseRelCust = 1 AND ActHist.Cust <> pr.PartyID AND pr.Deleted <> 1 
	AND ((ISNULL(@ExcludeRelList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND Exists (select * from #TT1 tt Where tt.Cust = pr.PartyId)
	AND Not Exists (select * from #TT1 tt Where tt.Cust = pr.PartyId AND tt.TranNo = ActHist.tranno)
    

	-- Related Parties (reverse direction)
	INSERT INTO #TT1 
	SELECT 	distinct pr.RelatedParty, TranNo, BaseAmt, RecvPay
	FROM 	ActivityHist ActHist WITH (NOLOCK) 
	Left	JOIN Account Acct WITH (NOLOCK) ON ActHist.Account = Acct.ID 
	INNER	JOIN PartyRelation pr WITH (NOLOCK) ON ActHist.Cust = pr.PartyID
	INNER	JOIN Customer RelCust WITH (NOLOCK) ON pr.RelatedParty = RelCust.ID
	WHERE 	ActHist.bookdate >= @minDate 
    AND	ActHist.bookdate <= @SDate
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@ActivityTypeExcludeList,'') = '' OR
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeExcludeList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerDept)) + ',', @DeptList) > 0))
	AND (
	(@cashTranIndicator = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR ---include activitytypelist if cash cashTran is 1
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0)))
	OR 
	(@cashTranIndicator = 2 AND (ActHist.CashTran = 1 OR ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
	OR 
	(@cashTranIndicator = 3 AND (ActHist.CashTran = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
    )
	AND	(@minimumAmount = -1 OR BaseAmt >= @minimumAmount) 
	AND (@maximumAmount = -1 OR BaseAmt <= @maximumAmount)
	AND @UseRelCust = 1 AND ActHist.Cust <> pr.RelatedParty AND pr.Deleted <> 1 
	AND ((ISNULL(@ExcludeRelList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND Exists (select * from #TT1 tt Where tt.Cust = pr.RelatedParty)
	AND Not Exists (select * from #TT1 tt Where tt.Cust = pr.RelatedParty AND tt.TranNo = ActHist.tranno)
    

	-- Related Accounts
	INSERT INTO #TT1 
	SELECT 	distinct ao.Cust,TranNo, BaseAmt, RecvPay
	FROM 	ActivityHist ActHist WITH (NOLOCK) 
	INNER 	JOIN Customer Cust WITH (NOLOCK) ON ActHist.Cust = Cust.ID
	INNER   JOIN AccountOwner ao WITH (NOLOCK)
	ON ActHist.Account = ao.Account and ActHist.Cust <> ao.cust
	INNER	JOIN Account Acct WITH (NOLOCK) ON ao.Account = Acct.ID 
	INNER	JOIN Customer CustOnAcct WITH (NOLOCK) ON ao.Cust = CustOnAcct.ID
	WHERE 	ActHist.bookdate >= @minDate 
    AND	ActHist.bookdate <= @SDate
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Cust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(CustOnAcct.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@ActivityTypeExcludeList,'') = '' OR
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeExcludeList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(CustOnAcct.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(CustOnAcct.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(CustOnAcct.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(custOnAcct.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(custOnAcct.OwnerDept)) + ',', @DeptList) > 0))
	AND (
	(@cashTranIndicator = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR ---include activitytypelist if cash cashTran is 1
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0)))
	OR 
	(@cashTranIndicator = 2 AND (ActHist.CashTran = 1 OR ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
	OR 
	(@cashTranIndicator = 3 AND (ActHist.CashTran = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
    )
	AND	(@minimumAmount = -1 OR BaseAmt >= @minimumAmount) 
	AND (@maximumAmount = -1 OR BaseAmt <= @maximumAmount)
	AND @UseRelAcct = 1 
	AND ((ISNULL(@ExcludeRelList,'') = ''  OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ao.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND Exists (select * from #TT1 tt Where tt.Cust = ao.Cust)
    AND Not Exists (select * from #TT1 tt Where tt.Cust = ao.Cust AND tt.TranNo = ActHist.tranno)
    

	INSERT INTO #TT
	SELECT 	Cust, RecvPay, SUM(BaseAmt)
	FROM 	#TT1
	GROUP 	BY Cust, RecvPay
	HAVING 	(@minimumTotalAmount = -1 OR SUM(BaseAmt) >= @minimumTotalAmount)
	AND (@maximumTotalAmount = -1 OR SUM(BaseAmt) <= @maximumTotalAmount)
	AND	COUNT(TranNo) >= @minimumCount


	IF @testAlert = 1 
	BEGIN
		SELECT @StartAlrtDate = GETDATE()
		INSERT INTO Alert (WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT, IsTest) 
		  SELECT @WLCode, 
			 CASE WHEN RecvPay = 1 THEN
				'Customer:  ''' + Cust
				+ ''' had ' 
				+ DBO.BSA_InternalizationMoneyToString( TranAmt) + Space(1) + @BaseCurr + 
				+ ' in deposits over a period of ' + CONVERT(VARCHAR, @day) + ' days.'
			 ELSE
				'Customer:  ''' + Cust
				+ ''' had  ' 
				+ DBO.BSA_InternalizationMoneyToString (TranAmt)  + Space(1) + @BaseCurr 
				+ ' in withdrawals over a period of ' + CONVERT(VARCHAR, @day) + ' days.'
			 END,0,
			GETDATE(), NULL, NULL, Cust, NULL , 1
		FROM #TT 
		SELECT @STAT = @@ERROR	
		SELECT @EndAlrtDate = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC

		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	Distinct 'Alert', AlertNo, TT.TRANNO 
		   	FROM 	Alert,#TT1 TT
			WHERE 	Alert.Cust = TT.Cust 
			AND ((Charindex('deposits', Alert.[Desc]) > 0 AND RecvPay = 1) OR
                 (Charindex('withdrawals', Alert.[Desc]) > 0 AND RecvPay = 2))
			AND 	Alert.WLCode = @WLCode 
			AND 	Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate	
			
			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc

	END 
	ELSE 
	BEGIN		    
		IF @WLType = 0 BEGIN
			SELECT @StartAlrtDate = GETDATE()
			INSERT INTO Alert (WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
					LASTMODIFY, CUST, ACCOUNT, IsTest) 
			  SELECT @WLCode, 
				 CASE WHEN RecvPay = 1 THEN
					'Customer:  ''' + Cust + ''' had ' 
					+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +Space(1) + @BaseCurr + 
					+ ' in deposits over a period of ' + CONVERT(VARCHAR, @day) + ' days.'
				 ELSE
					'Customer:  ''' + Cust
					+ ''' had ' 
					+ DBO.BSA_InternalizationMoneyToString(TranAmt) + Space(1) + @BaseCurr  
					+ ' in withdrawals over a period of ' + CONVERT(VARCHAR, @day) + ' days.'
				END,0,
				GETDATE(), NULL, NULL, Cust, NULL , 0
			FROM #TT 
			SELECT @STAT = @@ERROR	
			SELECT @EndAlrtDate = GETDATE()
		
			IF @stat <> 0 GOTO EndOfProc
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	Distinct 'Alert', AlertNo, TT.TRANNO 
				FROM 	Alert,#TT1 TT
			   	WHERE 	Alert.Cust = TT.Cust 
				AND ((Charindex('deposits', Alert.[Desc]) > 0 AND RecvPay = 1) OR
					 (Charindex('withdrawals', Alert.[Desc]) > 0 AND RecvPay = 2))
			   	AND 	Alert.WLCode = @WLCode 
				AND 	Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate	
				
			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc
			END 
		ELSE IF @WLType = 1 
		BEGIN	
		
			SELECT @StartAlrtDate = GETDATE()
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
				ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
				RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
				ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
				EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
				OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
				REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
				WLCode, WLDESC, CREATETIME )
			SELECT	NULL, @minDate, Cust, NULL,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, 
				CASE WHEN RecvPay = 1 THEN
					'Customer:  ''' + Cust + ''' had ' 
					+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +Space(1) + @BaseCurr + 
					+ ' in deposits over a period of ' + CONVERT(VARCHAR, @day) + ' days.'
				 ELSE
					'Customer:  ''' + Cust + ''' had ' 
					+ DBO.BSA_InternalizationMoneyToString(TranAmt) + Space(1) + @BaseCurr  
					+ ' in withdrawals over a period of ' + CONVERT(VARCHAR, @day) + ' days.'
				END , GETDATE() 
			FROM #TT
			SELECT @STAT = @@ERROR	
			SELECT @EndAlrtDate = GETDATE()
			IF @stat <> 0 GOTO EndOfProc
		
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	Distinct 'SUSPACT', RecNo, TT.TRANNO 
				FROM 	SuspiciousActivity SuspAct,#TT1 TT
				WHERE	SuspAct.Cust = TT.Cust
				AND ((Charindex('deposits', WLDesc) > 0 AND RecvPay = 1) OR
					 (Charindex('withdrawals', WLDesc) > 0 AND RecvPay = 2))
				AND	SuspAct.WlCode = @WlCode
				AND	SuspAct.CreateTime BETWEEN @StartAlrtDate AND @EndAlrtDate	
				
			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc
		END
	END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_CustXOverYDays

  drop table #tt
  drop table #tt1

  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_CustXOverYDays


drop table #tt
drop table #tt1

RETURN @stat

Go
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_XPercInTypeOutType]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.USR_XPercInTypeOutType
Go
CREATE  PROCEDURE dbo.USR_XPercInTypeOutType (@WLCode SCode, @testAlert INT,
	@day  int,
	@percent  int,
	@inActivityTypeList varchar(8000),
	@outActivityTypeList varchar(8000),
	@minSumAmt money,
	@riskClassList varchar(8000),
	@customerTypeList varchar(8000),
	@inboundCashType smallint,
	@outboundCashType smallint,
	@UseRelAcct Bit, 
	@UseRelCust Bit, 
    @ExcludeRelList varchar(8000)
	)
AS
/* RULE AND PARAMETER DESCRIPTION
	Detects customers with incoming (receive) transactions of a 
	specified activity type and outgoing (pay) transactions of a specified 
	activity type where the total amounts differ by less than or equal to a 
	specified percentage.  This rule evaluates transactions that are within
	the specified number of days and the aggregate amount of the qualifying
	transactions exceeds the specified minimum amount.  Additional 
	constraints include Cash/Non-Cash for inbound and outbound activity types,
	Customer Type and Risk Class.  Designed to be run Post-EOD
	
	@day = Indicates the number of days that are included in the evaluation of 
		transactions. This includes business and non-business days prior to
		the day the rule is executed.
	@percent = Percentage of the difference in amounts between the deposits 
		(receive) and the withdrawals (pay)
	@inActivityType = A comma separated list of inbound activity types to 
		be evaluated
        @outActivityType = A comma separated list of outbound activity types to 
		be evaluated 
	@minSumAmt = The Minimum aggregated transaction amount that must be exceeded.  
	@riskClass = A comma separated list of Risk Classes to include in the evaluation,
		 use -ALL- for any risk class.
	@customerType = A comma separated list of Customer Types to include in the 
		evaluation, use -ALL- for any customer type.
	@inboundCashType = For the activity types specified in the parameter 
		InActivityType, use 1 to restrict them to only Cash transactions,
		use 0 to restrict them to Non-Cash, 2 for both
	@outboundCashType = For the activity types specified in the parameter 
		OutActivityType, use 1 to restrict them to only 
			    Cash transactions, use 0 to restrict them to Non-Cash, 2 for both
	@UseRelAcct = Include Related Accounts (1=yes, 0=no) 
	@UseRelCust = Include Related Parties (1=yes, 0=no) 
	@ExcludeRelList = Exclude Relationship Types (comma separated list or '-NONE-')
*/

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@MINDATE INT
Declare @cust LONGNAME
Declare @currDateTime datetime 
Declare @CurrBookDate int

DECLARE @SetDate DATETIME

Create table #TT 
(
	Cust VARCHAR(40),
	Recv MONEY,
	Pay MONEY,
	Mindate INT,
	Maxdate INT,
	Descr VARCHAR(2000)
)

Create table #TT1 
(
	Cust	VARCHAR(35),
	TranNo	INT,
	BaseAmt MONEY, 
	RecvPay INT,
	BookDate INT)


SET NOCOUNT ON
SET @stat = 0

IF (ISNULL(@inActivityTypeList,'') = '' 
		OR UPPER(ISNULL(@inActivityTypeList,'-ALL-')) = '-ALL-')
	SELECT @inActivityTypeList = NULL
ELSE
	SELECT @inActivityTypeList = ',' + 
		REPLACE(ltrim(rtrim(@inActivityTypeList)),CHAR(32),'') + ','

IF (ISNULL(@outActivityTypeList,'') = '' 
		OR UPPER(ISNULL(@outActivityTypeList,'-ALL-')) = '-ALL-')
	SELECT @outActivityTypeList = NULL
ELSE
	SELECT @outActivityTypeList = ',' + 
		REPLACE(ltrim(rtrim(@outActivityTypeList)),CHAR(32),'') + ','

IF (ISNULL(@riskClassList,'') = '' 
		OR UPPER(ISNULL(@riskClassList,'-ALL-')) = '-ALL-')
	SELECT @riskClassList = NULL
ELSE
	SELECT @riskClassList = ',' +
		REPLACE(ltrim(rtrim(@riskClassList)),CHAR(32),'') + ','

IF (ISNULL(@customerTypeList,'') = '' 
		OR UPPER(ISNULL(@customerTypeList,'-ALL-')) = '-ALL-')
	SELECT @customerTypeList = NULL
ELSE
	SELECT @customerTypeList = ',' +
		REPLACE(ltrim(rtrim(@customerTypeList)),CHAR(32),'') + ','

IF LTRIM(RTRIM(@ExcludeRelList)) = '-NONE-' OR LTRIM(RTRIM(@ExcludeRelList)) = ''
	SELECT @ExcludeRelList = NULL
ELSE
SELECT @ExcludeRelList = ',' + 
	REPLACE(LTRIM(RTRIM(@ExcludeRelList)),' ','')+ ','

IF (@inboundCashType = 2)
	SET @inboundCashType = NULL

IF (@OutboundCashType = 2)
	SET @OutboundCashType = NULL

--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_XPercInTypeOutType
ELSE
	-- Already in a transaction
	SAVE TRAN USR_XPercInTypeOutType
/* End standard stored procedure transaction header */

/*  standard Rules Header */

-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @description = [Desc], @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList WITH (NOLOCK)
WHERE WLCode = @WLCode

SELECT @day = ABS(@day)

--When the transactions are picked up
SET @minDate = dbo.ConvertSqlDateToInt(
	DATEADD(d, -1 * @day, CONVERT(VARCHAR, @SetDate)))

Insert Into #TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select cust, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist WITH (NOLOCK) 
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
)
AND bookdate >= @minDate  

-- Related Parties
Insert Into #TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select Distinct pr.PartyId, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist ah WITH (NOLOCK) JOIN PartyRelation pr ON ah.Cust = pr.RelatedParty
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
)
AND bookdate >= @minDate 
AND @UseRelCust = 1 AND ah.Cust <> pr.PartyID AND pr.Deleted <> 1  
AND ((ISNULL(@ExcludeRelList,'') = '' OR 
	CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
AND Exists (select * from #tt1 tt Where tt.Cust = pr.PartyId)
AND Not Exists (select * from #tt1 tt Where tt.Cust = pr.PartyId AND tt.TranNo = ah.tranno)

-- Related Parties (reverse direction)
Insert Into #TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select Distinct pr.RelatedParty, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist ah WITH (NOLOCK)  JOIN PartyRelation pr ON ah.Cust = pr.PartyID
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
)
AND bookdate >= @minDate 
AND @UseRelCust = 1 AND ah.Cust <> pr.RelatedParty AND pr.Deleted <> 1
AND ((ISNULL(@ExcludeRelList,'') = '' OR 
	CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
AND Exists (select * from #tt1 tt Where tt.Cust = pr.RelatedParty)
AND Not Exists (select * from #tt1 tt Where tt.Cust = pr.RelatedParty AND tt.TranNo = ah.tranno)

-- Related Accounts
Insert Into #TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select Distinct ao.Cust, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist ah WITH (NOLOCK) JOIN AccountOwner ao 
On ah.Account = ao.Account and ah.Cust <> ao.Cust 
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
)
AND bookdate >= @minDate
AND @UseRelAcct = 1
AND ((ISNULL(@ExcludeRelList,'') = '' OR 
	CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ao.Relationship,''))) + ',', @ExcludeRelList) = 0))
AND Exists (select * from #tt1 tt Where tt.Cust = ao.cust)
AND Not Exists (select * from #tt1 tt Where tt.Cust = ao.Cust AND tt.TranNo = ah.tranno)


INSERT INTO #TT(Cust, recv, pay, mindate, maxdate, Descr)
SELECT cust, SUM(recv) recv, SUM(pay) pay, MIN(mindate) mindate, 
       MAX(maxdate) maxdate, 'Cust:  ''' + cust 
	+ ''' has incoming transfer '''
	+ CONVERT(VARCHAR, SUM(recv))
	+ ''' followed by an outgoing transfer '''
	+ CONVERT(VARCHAR, SUM(pay))
	+ ''' where the percentage '
	+ 'differs by '
        + CASE 
          WHEN SUM(pay) >= SUM(recv) AND SUM(pay) <> 0 THEN 
           CONVERT(VARCHAR, ABS(100 - 100 * SUM(recv)/SUM(pay))) 
          WHEN SUM(pay) < SUM(recv) AND SUM(recv) <> 0 THEN 
           CONVERT(VARCHAR, ABS(100 - 100 * SUM(pay)/SUM(recv)))
          END
	+ ' percent from bookdate ''' 
	+ CONVERT(VARCHAR, MIN(mindate)) + ''' to '''
	+ CONVERT(VARCHAR, MAX(maxdate)) + '''' descr
FROM  (
	SELECT cust, 		
	CASE WHEN recvpay = 1 THEN SUM(baseamt) 
		ELSE 0
		END recv,
	CASE WHEN recvpay = 2 THEN SUM(baseamt) 
		ELSE 0
		END pay,
	MIN(bookdate) AS mindate,
	MAX(bookdate) AS maxdate
	FROM #TT1
	GROUP BY cust, recvpay
) a, 
	Customer WITH (NOLOCK)
	WHERE   a.cust = Customer.Id
	AND ((@riskClassList IS NULL OR 
			CHARINDEX(',' + 
				LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0))
	AND ((@customerTypeList IS NULL OR
			CHARINDEX(',' + 
				LTRIM(RTRIM(Customer.type)) + ',', @customerTypeList ) > 0))
GROUP BY cust
HAVING SUM(recv) <> 0 AND SUM(pay) <> 0 
AND (100 * SUM(recv) between (100 - @percent)*SUM(pay) and (100 + @percent)*SUM(pay)
 OR  100 * SUM(pay) between (100 - @percent)*SUM(recv) and (100 + @percent)*SUM(recv))
AND (SUM(recv) > @MinSumAmt OR SUM(pay) > @MinSumAmt)
Set @currDateTime = GETDATE()
set @CurrBookDate = dbo.ConvertSqlDateToInt(GetDate())

IF @testAlert = 1 BEGIN
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, CUST, IsTest) 
	  SELECT @WLCode, descr, 0, @currDateTime, cust, 1 FROM #TT 

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	  SELECT Distinct 'Alert', AlertNo, TRANNO 
		FROM #tt1 A 
         JOIN #tt T ON a.Cust = t.Cust
         JOIN Alert AL ON a.Cust = al.Cust
        WHERE 
	al.WLCode = @WLCode
	AND al.CreateDate between @currDateTime AND GETDATE()
	SELECT @STAT = @@ERROR 
	IF @stat <> 0 GOTO EndOfProc
END ELSE BEGIN
	IF @WLType = 0 BEGIN
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, CUST) 
	  SELECT @WLCode, descr, 0, @currDateTime, cust FROM #TT 
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	  SELECT Distinct 'Alert', AlertNo, TRANNO 
		FROM #tt1 A 
         JOIN #tt T ON a.Cust = t.Cust
         JOIN Alert AL ON a.Cust = al.Cust
        WHERE 
	al.WLCode = @WLCode
	AND al.CreateDate between @currDateTime AND GETDATE()

	SELECT @STAT = @@ERROR 
	IF @stat <> 0 GOTO EndOfProc
	END ELSE IF @WLType = 1 BEGIN	
                INSERT INTO SUSPICIOUSACTIVITY (BOOKDATE, CUST, SUSPTYPE, 
                        INCNTTOLPERC, OUTCNTTOLPERC, INAMTTOLPERC, 
			OUTAMTTOLPERC, WLCode, WLDESC, CreateTime)
		SELECT	@CurrBookDate, Cust, 'RULE', 0, 0, 0, 0, 
			@WLCode, descr, @currDateTime
		FROM #tt
		IF @stat <> 0 GOTO EndOfProc
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT Distinct 'SUSPACT', RecNo, TRANNO 
			FROM #tt1 A
        	JOIN #tt T ON a.Cust = t.Cust
             	JOIN SuspiciousActivity s ON a.Cust = s.Cust
        	WHERE 
	        s.WLCode = @WLCode
	        AND s.bookdate = @CurrBookDate
			AND s.createTime between @currDateTime AND GETDATE()

		SELECT @STAT = @@ERROR
		IF @stat <> 0 GOTO EndOfProc
	END
END
EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_XPercInTypeOutType

  drop table #tt
  drop table #tt1

  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_XPercInTypeOutType

drop table #tt
drop table #tt1

RETURN @stat

Go

if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_CstXOvrYInt]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.USR_CstXOvrYInt
Go


CREATE PROCEDURE dbo.USR_CstXOvrYInt (@WLCode SCode, 
				@TestAlert INT,
				@excludeExemptCustList varchar(8000),
				@activityTypeList varchar(8000), 
				@activityTypeExcludeList VARCHAR(8000),
				@cashTranIndicator INT,
				@minimumAmount MONEY,
				@maximumAmount MONEY,
				@customerList VARCHAR(8000), 
				@RiskClassList VARCHAR(8000), 
				@CustomerTypeList VARCHAR(8000),
				@AccountTypeList VARCHAR(8000), 
				@BranchList varchar(8000), 
				@DeptList varchar(8000), 
				@UseRelAcct Bit, 
				@UseRelCust Bit, 
				@ExcludeRelList varchar(8000),
				@NumDaysLookback INT, 
				@minTotalAmtForLookBack MONEY, 
				@maxTotalAmtForLookBack MONEY, 
				@minCountForLookback INT,
				@NumDaysInterval INT, 
				@minTotalAmtForInterval MONEY, 
				@maxTotalAmtForInterval MONEY, 
				@minCountForInterval INT
) AS
/* RULE PARAMETERS -  THESE MUST BE SET ACCORDING TO THE RULE REQUIREMENT*/

/* RULE AND PARAMETER DESCRIPTION
	Detects customers that had transactions using a specified 
	activity type that exceeded a designated dollar amount over 
	an interval or specified number of days.  Designed to be run Post-EOD. 
	
	@excludeExemptCustList = Type of Exemptions to exclude customers (Comma separated or '-ALL-' or '-NONE-')
	@activityTypeList = Type of activity to be checked  (Comma separated or '-ALL-') 
	@activityTypeExcludeList = Type of activity to be excluded (Comma separated or '-NONE-') 
	@cashTranIndicator = Cash Tran indicator to include check on activity type 
	@minimumAmount= Minimum threshold amount 
	@maximumAmount = Maximum Threshold Amount
	@CustomerList = List of customer Id's to be checked
	@RiskClassList = List of risk classes of customer to be checked
	@AccountTypeList = List of Accounts to be checked (Comma Separated or '-ALL-')
	@CustomerTypeList = List of Customer Types
   	@BranchList  = List of Branches to be checked (Comma Separated or '-ALL-')
   	@DeptList    = List of Department to be checked (Comma Separated or '-ALL-')
	@UseRelAcct = Include Related Accounts (1=yes, 0=no) 
	@UseRelCust = Include Related Parties (1=yes, 0=no) 
	@ExcludeRelList = Exclude Relationship Types (comma separated list or '-NONE-')
	@NumDaysLookback = Look Back period for the activity
	@minTotalAmtForLookBack = Minimum Total Transaction Amount for the look back
	@maxTotalAmtForLookBack = Maximum Total Threshold amount for the look back
	@minCountForLookback = Minimum Transaction Count for the look back
	@NumDaysInterval = Interval period for the activity
	@minTotalAmtForInterval = Minimum Total Transaction Amount for the interval
	@maxTotalAmtForInterval = Maximum Total Threshold amount for the interval
	@minCountForInterval = Minimum Transaction Count for the interval
*/

/*  Declarations */
	DECLARE	@description 	VARCHAR(2000),
		@Id 				INT, 
		@WLType 			INT,
		@stat 				INT,
		@trnCnt 			INT,
		@Count				INT,
		@BaseCurr			CHAR(3),
		@NoOfIntervals		INT,
		@startDate 			INT,      -- stores start date of the look back period (period 1) as integer
		@endDate			INT,      -- stores end date of the look back period (period 1) as integer
		@LookbackStartDate	DATETIME, -- stores the start date for the interval (period 1)
		@LookbackEndDate 	DATETIME, -- stores the end date for the look back period (period 1)
		@StartAlrtDate		DATETIME, -- stores the start date and time of alert or case creation
		@EndAlrtDate		DATETIME, -- stores the end date and time of alert or case creation
		@IntStartDate		DATETIME, -- stores the start date for the interval (period 2)
		@IntEndDate			DATETIME  -- stores the end date for the interval (period 2)

	SET NOCOUNT ON
	SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
	SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @trnCnt = 0
	-- Transaction has not begun
		BEGIN TRAN USR_CstXOvrYInt
	ELSE
	-- Already in a transaction
		SAVE TRAN USR_CstXOvrYInt
	/* End standard stored procedure transaction header */

/*  standard Rules Header */
-- Date options
-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

/* Temporary Tables declaration */

-- Table to hold transactions that meet the criteria specified
	CREATE TABLE #TempActHist
	(
		TranNo				INT Not Null,
		Cust				VARCHAR(35) Not Null,
		Account				VARCHAR(35) Null,
		Type				INT Not Null,
		CashTran			INT Not Null,
		BaseAmt				MONEY Not Null,
		RecvPay				SMALLINT Not Null,
		Curr				CHAR(3) Not Null,
		Ref					VARCHAR(35) Null,
		BookDate			INT Not Null,
		ExemptionStatus     VARCHAR(11) Null,
		Branch				VARCHAR(11) Not Null,
		Dept				VARCHAR(11) Not Null,
		TranDate			DATETIME Not Null,
		Constraint PK_TempActHist_TranNo Primary Key NonClustered (TranNo)
	)

-- Table to hold transactions that meet the criteria specified
	CREATE TABLE #TT1
	(
		Cust	 VARCHAR(35) ,
		TranNo	 INT,
		BaseAmt  MONEY, 
		RecvPay  INT,
		bookdate INT,
		IntNo    INT)

-- Table to hold customer id and the total of all transactions during the look back period
	CREATE TABLE #TTLookBack 
	(
		Cust 		VARCHAR(35) ,
		RecvPay		INT,
		TranAmt		MONEY
	)

-- Table to hold customer id and the total of all transactions during the interval period
	CREATE TABLE #TTINT 
	(
		IntNo		INT,
		Cust 		VARCHAR(35),
		RecvPay		INT,
		TranAmt		MONEY,
	)

-- Table to hold intervals and their start and end dates
	CREATE TABLE #INT
	(
		IntNo 		INT,
		StartDate	DATETIME,
		EndDate		DATETIME
	)

/* END - Temporary Tables declaration */


	SELECT  @description = [Desc], @WLType = WLType  ,
		@LookbackEndDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GetDate()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GetDate()
		END
	FROM WatchList WITH (NOLOCK) WHERE WLCode = @WLCode

	SELECT @BaseCurr = IsNULL(BaseCurr,'') FROM SysParam
    
	SET @startDate = dbo.ConvertSqlDateToInt(DATEADD(d, -1 * (@NumDaysLookback-1), CONVERT(VARCHAR, @LookbackEndDate)))
	SET	@endDate = dbo.ConvertSqlDateToInt(@LookbackEndDate)


	-- Set Number of days in the interval to 1 to handle divide by 0 error
	IF (@NumDaysInterval=0)
		SELECT @NumDaysInterval = 1

	-- Handle the case where number of days in the interval is greater than number of days in the look back
	IF (@NumDaysInterval>@NumDaysLookback)
		SELECT @NumDaysInterval = @NumDaysLookback

	SELECT @NoOfIntervals = @NumDaysLookback/@NumDaysInterval

	SELECT @Count = 1
	SET @IntEndDate = @LookbackEndDate
	SET @LookbackStartDate = dateadd(day,-1*(@NumDaysLookback-1), @IntEndDate)

	WHILE (@count <= @NoOfIntervals)
	BEGIN
		SET @intstartdate = dateadd(day,-1*(@NumDaysInterval-1), @intendDate)
		IF (@intstartDate < @lookbackstartdate )
		BEGIN
			SET @intstartdate = @lookbackstartdate
		END
		INSERT INTO #INT (intno, startdate, enddate) VALUES (@count, @intstartdate, @intenddate)
		SET @intenddate = dateadd(day, -1,@intstartDate)
		SET @count = @count + 1   
	END
	-- End of the section to identify the interval start and end dates


	IF LTRIM(RTRIM(@excludeExemptCustList)) = '-NONE-' OR LTRIM(RTRIM(@excludeExemptCustList)) = ''
		SELECT @excludeExemptCustList = NULL
	ELSE IF LTRIM(RTRIM(@excludeExemptCustList)) = '-ALL-'
		BEGIN
			SELECT @excludeExemptCustList = ''
			SELECT  @excludeExemptCustList = 		
				COALESCE( @excludeExemptCustList + ',','' )  + Code
			FROM ExemptionType 
			 WHERE (SubString(ExemptEntity,1,1) = 1)
					AND (SubString(ExemptFrom,3,1) = 1)
						
			SELECT  @excludeExemptCustList = @excludeExemptCustList + ','
		END
	ELSE                                           
		SELECT @excludeExemptCustList = ',' + 
			REPLACE(LTRIM(RTRIM(@excludeExemptCustList)),' ','')+ ','
			
	IF LTRIM(RTRIM(@activityTypeExcludeList)) = '-NONE-' OR LTRIM(RTRIM(@activityTypeExcludeList)) = ''
		SELECT @activityTypeExcludeList = NULL
	ELSE
		SELECT @activityTypeExcludeList = ',' + 
			REPLACE(LTRIM(RTRIM(@activityTypeExcludeList)),' ','')+ ','
			   
	IF LTRIM(RTRIM(@ExcludeRelList)) = '-NONE-' OR LTRIM(RTRIM(@ExcludeRelList)) = ''
		SELECT @ExcludeRelList = NULL
	ELSE
		SELECT @ExcludeRelList = ',' + 
			REPLACE(LTRIM(RTRIM(@ExcludeRelList)),' ','')+ ','
	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)
	SELECT @customerList = dbo.BSA_fnListParams(@customerList)
	SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	SELECT @customerTypeList = dbo.BSA_fnListParams(@customerTypeList)
	SELECT @AccountTypeList = dbo.BSA_fnListParams(@AccountTypeList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
	SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)
	SELECT @ExcludeRelList = dbo.BSA_fnListParams (@ExcludeRelList)
	
	INSERT INTO #TempActHist (TranNo, Cust, Account, Type, CashTran,	BaseAmt, RecvPay, Curr,	Ref, BookDate,
			ExemptionStatus, Branch, Dept, TranDate)
	SELECT 	TranNo,	Cust, Account, Type, CashTran,	BaseAmt, RecvPay, Curr,	Ref, BookDate,
			ExemptionStatus, Branch, Dept, TranDate
	FROM   ActivityHist ActHist WITH (NOLOCK)
	WHERE  ActHist.bookdate >= @startDate 
	AND	ActHist.bookdate <= @endDate
	AND ((ISNULL(@ActivityTypeExcludeList,'') = '' OR
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeExcludeList) = 0))
	AND (
	(@cashTranIndicator = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR --include activitytypelist if cash cashTran is 1
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0)))
	OR 
	(@cashTranIndicator = 2 AND (ActHist.CashTran = 1 OR ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
	OR 
	(@cashTranIndicator = 3 AND (ActHist.CashTran = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
	)
	AND	(@minimumAmount = -1 OR BaseAmt >= @minimumAmount) 
	AND (@maximumAmount = -1 OR BaseAmt <= @maximumAmount)

	INSERT INTO #TT1 
	SELECT ActHist.Cust,TranNo, BaseAmt, RecvPay, bookdate, 0
	FROM   #TempActHist ActHist WITH (NOLOCK)
	INNER  JOIN Customer Cust WITH (NOLOCK) ON ActHist.Cust = Cust.ID
	LEFT   JOIN Account Acct WITH (NOLOCK) ON ActHist.Account = Acct.ID 	
	WHERE  
	((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Cust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Cust.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Cust.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Cust.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(cust.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(cust.OwnerDept)) + ',', @DeptList) > 0))
	AND ((ISNULL(@ActivityTypeExcludeList,'') = '' OR
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeExcludeList) = 0))
	   
		-- Related Parties
	INSERT INTO #TT1 
	SELECT 	pr.PartyId ,TranNo, BaseAmt, RecvPay, bookdate, 0
	FROM 	#TempActHist ActHist WITH (NOLOCK) 
	INNER 	JOIN Customer Cust WITH (NOLOCK) ON ActHist.Cust = Cust.ID
	Left	JOIN Account Acct WITH (NOLOCK) ON ActHist.Account = Acct.ID 
	INNER	JOIN PartyRelation pr WITH (NOLOCK) ON ActHist.Cust = pr.RelatedParty
	INNER	JOIN Customer RelCust WITH (NOLOCK) ON pr.RelatedParty = RelCust.ID
	WHERE 	((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Cust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerDept)) + ',', @DeptList) > 0))
	AND @UseRelCust = 1 AND ActHist.Cust <> pr.PartyID AND pr.Deleted <> 1 
	AND ((ISNULL(@ExcludeRelList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND Exists (SELECT * FROM #TT1 tt WHERE tt.Cust = pr.PartyId )
	AND Not Exists (SELECT * FROM #TT1 tt WHERE tt.Cust = pr.PartyId AND tt.TranNo = ActHist.tranno)

	-- Related Parties (reverse direction)
	INSERT INTO #TT1 
	SELECT 	pr.RelatedParty, TranNo, BaseAmt, RecvPay, bookdate, 0
	FROM 	#TempActHist ActHist WITH (NOLOCK) 
	INNER 	JOIN Customer Cust WITH (NOLOCK) ON ActHist.Cust = Cust.ID
	Left	JOIN Account Acct WITH (NOLOCK) ON ActHist.Account = Acct.ID 
	INNER	JOIN PartyRelation pr WITH (NOLOCK) ON ActHist.Cust = pr.PartyID
	INNER	JOIN Customer RelCust WITH (NOLOCK) ON pr.PartyID = RelCust.ID
	WHERE 	((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Cust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerDept)) + ',', @DeptList) > 0))
	AND @UseRelCust = 1 AND ActHist.Cust <> pr.RelatedParty AND pr.Deleted <> 1
	AND ((ISNULL(@ExcludeRelList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND Exists (SELECT * FROM #TT1 tt WHERE tt.Cust = pr.RelatedParty)
	AND Not Exists (SELECT * FROM #TT1 tt WHERE tt.Cust = pr.RelatedParty AND tt.TranNo = ActHist.tranno)

	-- Related Accounts
	INSERT INTO #TT1 
	SELECT 	ao2.Cust,TranNo, BaseAmt, RecvPay, bookdate, 0
	FROM 	#TempActHist ActHist WITH (NOLOCK) 
	INNER 	JOIN Customer Cust WITH (NOLOCK) ON ActHist.Cust = Cust.ID
	INNER   JOIN AccountOwner ao WITH (NOLOCK) ON ActHist.Cust = ao.cust
	INNER	JOIN Account Acct WITH (NOLOCK) ON ao.Account = Acct.ID 
	INNER	JOIN AccountOwner ao2 WITH (NOLOCK) ON ao.Account = ao2.Account And ao.Cust <> ao2.Cust
	INNER	JOIN Customer CustOnAcct ON ao2.Cust = CustOnAcct.ID
	WHERE 	((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Cust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(CustOnAcct.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(CustOnAcct.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(CustOnAcct.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(CustOnAcct.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(custOnAcct.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(custOnAcct.OwnerDept)) + ',', @DeptList) > 0))
	AND @UseRelAcct = 1 AND ActHist.Cust <> ao2.Cust
	AND ((ISNULL(@ExcludeRelList,'') = '' OR ao.Account = ActHist.Account OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ao.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND ((ISNULL(@ExcludeRelList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ao2.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND Exists (SELECT * FROM #TT1 tt WHERE tt.Cust = ao2.Cust)
    	AND Not Exists (SELECT * FROM #TT1 tt WHERE tt.Cust = ao2.Cust AND tt.TranNo = ActHist.tranno)

	INSERT INTO #TTLookBack
	SELECT 	Cust, RecvPay, SUM(BaseAmt)
	FROM 	#TT1
	GROUP 	BY Cust, RecvPay
	HAVING 	(@minTotalAmtForLookBack = -1 OR SUM(BaseAmt) >= @minTotalAmtForLookBack)
	AND (@maxTotalAmtForLookBack = -1 OR SUM(BaseAmt) <= @maxTotalAmtForLookBack)
	AND	COUNT(TranNo) >= @minCountForLookback

    UPDATE #TT1 SET intno = (SELECT intno FROM #INT WHERE bookdate 
							 BETWEEN dbo.ConvertSqlDateToInt(startdate) AND dbo.ConvertSqlDateToInt(enddate))

	INSERT INTO #TTINT 
	SELECT 	#TT1.intno, Cust, RecvPay, SUM(BaseAmt)
	FROM 	#TT1
	GROUP 	BY Cust, RecvPay, intno
	HAVING  (@minTotalAmtForInterval = -1 OR SUM(BaseAmt) >= @minTotalAmtForInterval)
			AND (@maxTotalAmtForInterval = -1 OR SUM(BaseAmt) <= @maxTotalAmtForInterval)
			AND	COUNT(TranNo) >= @minCountForInterval

	--1 means test alert, 0 means actual alert
	IF @testAlert = 1 
	BEGIN
		-- Create test alerts for customers who have exceeded only in the look back period
		SELECT @StartAlrtDate = GETDATE()
		INSERT INTO Alert (WLCode,  STATUS, CreateDate, LASTOPER, 
							LASTMODIFY, CUST, ACCOUNT, IsTest, [DESC]) 
		SELECT @WLCode,0,GETDATE(), NULL, NULL, Cust, NULL , 1,
 			 CASE WHEN RecvPay = 1 THEN
				'Customer ''' + Cust + ''' had deposits of ' 
				+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + 
				+ ' between ' + CONVERT(VARCHAR, @LookbackStartDate,101) + ' and ' + CONVERT(VARCHAR, @LookbackEndDate,101)
			 ELSE
				'Customer  ''' + Cust + ''' had withdrawals of ' 
				+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + 
				+ ' between ' + CONVERT(VARCHAR, @LookbackStartDate,101) + ' and ' + CONVERT(VARCHAR, @LookbackEndDate,101)
			 END
		FROM #TTLookBack WHERE cust NOT IN (SELECT DISTINCT cust FROM #TTINT)
		SELECT @STAT = @@ERROR	
		IF @stat <> 0 GOTO EndOfProc
		SELECT @EndAlrtDate = GETDATE()
	

		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		SELECT 	'Alert', AlertNo, TT.TRANNO 
		FROM 	Alert,#TT1 TT
		WHERE 	Alert.Cust = TT.Cust 
		AND ((Charindex('deposits', Alert.[Desc]) > 0 AND RecvPay = 1) 
		OR	(Charindex('withdrawals', Alert.[Desc]) > 0 AND RecvPay = 2))
		AND Alert.WLCode = @WLCode 
		AND Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
		SELECT @STAT = @@ERROR 
		IF @stat <> 0 GOTO EndOfProc

		-- Create test alerts for customers who have exceeded only in the interval period
		SELECT @StartAlrtDate = GETDATE()
		INSERT INTO Alert (WLCode, STATUS, CreateDate, LASTOPER, 
						LASTMODIFY, CUST, ACCOUNT, IsTest, [DESC]) 
		SELECT @WLCode, 0, GETDATE(), NULL, NULL, Cust, NULL , 1,
			CASE WHEN RecvPay = 1 THEN
				LEFT('Customer ''' + cust + ''' has deposits of ' +
				(SELECT ( DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + ' between ' + 
										CONVERT(VARCHAR(100),startdate, 101) + ' and ' + CONVERT(VARCHAR(100),enddate, 101)+ ', ')
						   FROM #ttint t2, #int 
						   WHERE t1.cust = t2.cust
						   AND t2.recvpay = 1
						   AND t2.intno = #int.intno
						   ORDER BY #int.startdate desc
						   FOR XML PATH( '' )),2000 )
			ELSE
				LEFT('Customer ''' + cust + ''' has withdrawals of ' +
				(SELECT ( DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + ' between ' + 
										CONVERT(VARCHAR(100),startdate, 101) + ' and ' + CONVERT(VARCHAR(100),enddate, 101) + ', ')
						   FROM #ttint t2, #int 
						   WHERE t1.cust = t2.cust
						   AND t2.recvpay = 2
						   AND t2.intno = #int.intno
						   ORDER BY #int.startdate desc
						   FOR XML PATH( '' )),2000 )
			END
		FROM #TTINT t1
		GROUP BY cust, recvpay
		SELECT @STAT = @@ERROR	
		IF @stat <> 0 GOTO EndOfProc
		SELECT @EndAlrtDate = GETDATE()

		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		SELECT 	DISTINCT 'Alert', AlertNo, TT.TRANNO
		FROM 	Alert,#TT1 TT, #INT I1, #TTINT TTI
		WHERE 	Alert.Cust = TT.Cust 
		AND ((CHARINDEX('deposits', Alert.[Desc]) > 0 AND TT.RecvPay = 1) 
		OR   (CHARINDEX('withdrawals', Alert.[Desc]) > 0 AND TT.RecvPay = 2))
		AND Alert.WLCode = @WLCode 
		AND Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
		AND TT.intno = TTI.intno 
		AND TTI.intno = I1.intno
		AND TTI.recvpay = TT.recvpay
		AND TT.bookdate BETWEEN dbo.ConvertSqlDateToInt(I1.startdate) AND dbo.ConvertSqlDateToInt(I1.enddate)
		SELECT @STAT = @@ERROR
	END
	ELSE
	BEGIN
		IF @WLTYPE = 0
		BEGIN
			-- Create alerts for customers who have exceeded only in the look back period
			SELECT @StartAlrtDate = GETDATE()
			INSERT INTO Alert (WLCode,  STATUS, CreateDate, LASTOPER, 
								LASTMODIFY, CUST, ACCOUNT, IsTest, [DESC]) 
			SELECT @WLCode,0,GETDATE(), NULL, NULL, Cust, NULL , 0,
 				 CASE WHEN RecvPay = 1 THEN
					'Customer  ''' + Cust + ''' had deposits of ' 
					+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + 
					+ ' between ' + CONVERT(VARCHAR, @LookbackStartDate,101) + ' and ' + CONVERT(VARCHAR, @LookbackEndDate,101)
				 ELSE
					'Customer  ''' + Cust + ''' had withdrawals of ' 
					+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + 
					+ ' between ' + CONVERT(VARCHAR, @LookbackStartDate,101) + ' and ' + CONVERT(VARCHAR, @LookbackEndDate,101)
				 END
			FROM #TTLookBack WHERE cust NOT IN (SELECT DISTINCT cust FROM #TTINT)
			SELECT @STAT = @@ERROR	
			IF @stat <> 0 GOTO EndOfProc
			SELECT @EndAlrtDate = GETDATE()
		
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	'Alert', AlertNo, TT.TRANNO 
			FROM 	Alert,#TT1 TT
			WHERE 	Alert.Cust = TT.Cust 
			AND ((Charindex('deposits', Alert.[Desc]) > 0 AND RecvPay = 1) 
			OR	(Charindex('withdrawals', Alert.[Desc]) > 0 AND RecvPay = 2))
			AND Alert.WLCode = @WLCode 
			AND Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc

			-- Create alerts for customers who have exceeded only in the interval period
			SELECT @StartAlrtDate = GETDATE()
			INSERT INTO Alert (WLCode, STATUS, CreateDate, LASTOPER, 
							LASTMODIFY, CUST, ACCOUNT, IsTest, [DESC]) 
			SELECT @WLCode, 0, GETDATE(), NULL, NULL, Cust, NULL , 0,
				CASE WHEN RecvPay = 1 THEN
					LEFT('Customer ''' + cust + ''' has deposits of ' +
					(SELECT ( DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + ' between ' + 
											CONVERT(VARCHAR(100),startdate, 101) + ' and ' + CONVERT(VARCHAR(100),enddate, 101)+ ', ')
							   FROM #ttint t2, #int 
							   WHERE t1.cust = t2.cust
							   AND t2.recvpay = 1
							   AND t2.intno = #int.intno
							   ORDER BY #int.startdate desc
							   FOR XML PATH( '' )), 2000 )
				ELSE
					LEFT('Customer ''' + cust + ''' has withdrawals of ' +
					(SELECT ( DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + ' between ' + 
											CONVERT(VARCHAR(100),startdate, 101) + ' and ' + CONVERT(VARCHAR(100),enddate, 101)+ ', ')
							   FROM #ttint t2, #int 
							   WHERE t1.cust = t2.cust
							   AND t2.recvpay = 2
							   AND t2.intno = #int.intno
							   ORDER BY #int.startdate desc
							   FOR XML PATH( '' )), 2000 )
				END
			FROM #TTINT t1
			GROUP BY cust, recvpay
			SELECT @STAT = @@ERROR	
			IF @stat <> 0 GOTO EndOfProc
			SELECT @EndAlrtDate = GETDATE()

			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	DISTINCT 'Alert', AlertNo, TT.TRANNO
			FROM 	Alert,#TT1 TT, #INT I1, #TTINT TTI
			WHERE 	Alert.Cust = TT.Cust 
			AND ((CHARINDEX('deposits', Alert.[Desc]) > 0 AND TT.RecvPay = 1) 
			OR   (CHARINDEX('withdrawals', Alert.[Desc]) > 0 AND TT.RecvPay = 2))
			AND Alert.WLCode = @WLCode 
			AND Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
			AND TT.intno = TTI.intno 
			AND TTI.intno = I1.intno
			AND TTI.recvpay = TT.recvpay
			AND TT.bookdate BETWEEN dbo.ConvertSqlDateToInt(I1.startdate) AND dbo.ConvertSqlDateToInt(I1.enddate)
			SELECT @STAT = @@ERROR
			IF @stat <> 0 GOTO EndOfProc
		END
		ELSE IF @WLTYPE = 1
		BEGIN
			-- Create cases for customers who have exceeded only in the look back period
			SELECT @StartAlrtDate = GETDATE()
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
				ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
				RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
				ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
				EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
				OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
				REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
				WLCode, WLDESC, CREATETIME )
			SELECT	NULL, @startDate, Cust, NULL,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, 
 				 CASE WHEN RecvPay = 1 THEN
					'Customer  ''' + Cust + ''' had deposits of ' 
					+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + 
					+ ' between ' + CONVERT(VARCHAR, @LookbackStartDate,101) + ' and ' + CONVERT(VARCHAR, @LookbackEndDate,101)
				 ELSE
					'Customer  ''' + Cust + ''' had withdrawals of ' 
					+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + 
					+ ' between ' + CONVERT(VARCHAR, @LookbackStartDate,101) + ' and ' + CONVERT(VARCHAR, @LookbackEndDate,101)
				 END , GETDATE() 
			FROM #TTLookBack WHERE cust NOT IN (SELECT DISTINCT cust FROM #TTINT)
			SELECT @STAT = @@ERROR	
			IF @stat <> 0 GOTO EndOfProc
			SELECT @EndAlrtDate = GETDATE()

			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	DISTINCT 'SUSPACT', RecNo, TT.TRANNO 
			FROM 	SuspiciousActivity SuspAct,#TT1 TT
			WHERE 	SuspAct.Cust = TT.Cust 
			AND ((Charindex('deposits', WLDesc) > 0 AND TT.RecvPay = 1) OR
				 (Charindex('withdrawals', WLDesc) > 0 AND TT.RecvPay = 2))
			AND suspAct.WLCode = @WLCode 
			AND	SuspAct.CreateTime BETWEEN @StartAlrtDate AND @EndAlrtDate	
			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc

			-- Create cases for customers who have exceeded only in the interval period
			SELECT @StartAlrtDate = GETDATE()
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
				ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
				RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
				ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
				EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
				OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
				REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
				WLCode, WLDESC, CREATETIME )
			SELECT	NULL, @startDate, Cust, NULL,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, 
				CASE WHEN RecvPay = 1 THEN
					LEFT('Customer ''' + cust + ''' has deposits of ' +
					(SELECT ( DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + ' between ' + 
											CONVERT(VARCHAR(100),startdate, 101) + ' and ' + CONVERT(VARCHAR(100),enddate, 101)+ ', ')
							   FROM #ttint t2, #int 
							   WHERE t1.cust = t2.cust
							   AND t2.recvpay = 1
							   AND t2.intno = #int.intno
							   ORDER BY #int.startdate desc
							   FOR XML PATH( '' )), 2000 )
				ELSE
					LEFT('Customer ''' + cust + ''' has withdrawals of ' +
					(SELECT (DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + ' between ' + 
											CONVERT(VARCHAR(100),startdate, 101) + ' and ' + CONVERT(VARCHAR(100),enddate, 101)+ ', ')
							   FROM #ttint t2, #int 
							   WHERE t1.cust = t2.cust
							   AND t2.recvpay = 2
							   AND t2.intno = #int.intno
							   ORDER BY #int.startdate desc
							   FOR XML PATH( '' )), 2000 )
				END, Getdate()
			FROM #TTINT t1
			GROUP BY cust, recvpay
			SELECT @STAT = @@ERROR	
			IF @stat <> 0 GOTO EndOfProc
			SELECT @EndAlrtDate = GETDATE()

			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	DISTINCT 'SUSPACT', RecNo, TT.TRANNO 
				FROM 	SuspiciousActivity SuspAct,#TT1 TT, #INT I1, #TTINT TTI
				WHERE	SuspAct.Cust = TT.Cust
				AND ((Charindex('deposits', WLDesc) > 0 AND TT.RecvPay = 1) OR
					 (Charindex('withdrawals', WLDesc) > 0 AND TT.RecvPay = 2))
				AND	SuspAct.WlCode = @WlCode
				AND	SuspAct.CreateTime BETWEEN @StartAlrtDate AND @EndAlrtDate	
				AND TT.intno = TTI.intno 
				AND TTI.intno = I1.intno
				AND TTI.recvpay = TT.recvpay
				AND TT.bookdate BETWEEN dbo.ConvertSqlDateToInt(I1.startdate) AND dbo.ConvertSqlDateToInt(I1.enddate)
			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc
		END 
	END
	IF @trnCnt = 0 BEGIN
		COMMIT TRAN USR_CstXOvrYInt
		GOTO Cleanup
	END

EndOfProc:
IF (@stat <> 0) BEGIN 
	ROLLBACK TRAN USR_CstXOvrYInt
	GOTO Cleanup
END	

Cleanup:
	DROP TABLE #TT1
	DROP TABLE #TTINT
	DROP TABLE #TTLookBack
	DROP TABLE #INT
	DROP TABLE #TempActHist

	RETURN @stat
Go
if exists (select * from sysobjects 
	--where id = object_id(N'[dbo].[USR_AntpActvComps]') 
	where id = object_id(N'[dbo].[USR_AntpActvComps]') 	
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	--Drop Procedure [USR_AntpActvComps]
	Drop Procedure USR_AntpActvComps	
Go

CREATE PROCEDURE [dbo].[USR_AntpActvComps] ( 
	@WLCode 		SCode,
	@testAlert		INT,		
	@comparisonType	INT,		-- Example 1, 2, 3, 4 for 'Cash', 'Wire', 'Check', 'Deposit' for (Anticipated Activity Comparison Cash | Wire | Check | Deposit respectively)
	@startDate		VARCHAR(10), -- = convert(Date, GetDate()), -- Convert(DateTime, Convert(VarChar, GetDate(), 101)), -- Use Tdaay's Date for default
	@endDate		VARCHAR(10),
	@minTransCount	INT,
	@minAggTransAmt MONEY,
	@monitorInd		INT,  	 -- 1: Monitor Amount; 2: Monitor Count; 3: Both
	@aggAntpDnFCntAmt   INT, -- set to 0 for handling DOM and FOR WIRE separately, set to 1 for handling them as combined ALL WIREs
	@amtTolerance	INT,			-- = 10,  default to 10 % if necessary
	@cntTolerance	INT,			-- = 10,  default to 10 % if necessary
	@riskClassList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All
	@branchList		VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
	@departmentList VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
	@acctTypesList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All
	@custTypesList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
	@actvTypesListIncl VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.(For Wire rule list Domestic Wire Activity Types to be included.)
	@actvTypesListExcl VARCHAR(8000) = '-NONE-',  --  Use -ALL- for All or -None- for None. (For Wire rule list Foreign Wire Activity Types to be included.)
	@receivedOrPay	INT,			--  Use 1R-2P-3Both 
	@cashType		smallint,		-- Specifies Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both
	@numDayMonitor	INT = 0,		--  Number of Days to Monitor after Account Open Date (Use 0 to Ignore this parameter)
	@ignoreZeroAntVals INT = 1      --  Ignore Zero Anticipated Values (0 for No, 1 for Yes), Default Value = 1 to ignore
)
AS
	
/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@fromDate INT,
 	@toDate INT,
	@strFromDate VARCHAR(10),
	@strToDate VARCHAR(10),
	@strBetweenDates VARCHAR(50),
 	@firstMonitorDate INT, 
 	@strSubject VARCHAR(9),
 	@strExceed1 VARCHAR(26),
 	@strExceed2 VARCHAR(39),
 	@strDueTo VARCHAR(8),
 	@nom INT,					
 	@nod INT,
 	@date2Monitor INT,			
	@STARTALRTDATE DATETIME,
	@ENDALRTDATE DATETIME, 	
	@iZAV bit,
	@tranAmt MONEY,
	@tranCnt INT,
	@trnCnt INT,
	@Cust VarChar(35),
	@OwnerBranch VarChar(11),
	@OwnerDept VarChar(11),
	@nMonthComparison INT,
	@CurrDate GenericDate,
	@wrkDate DATETIME

SET @strSubject = 'Account: '
SET @strExceed1 = ' exceeded the anticipated '
SET @strExceed2 = ' and exceeded the anticipated number of'
SET @strDueTo = ' due to ' 

DECLARE @SetDate DATETIME

DECLARE @DSQA TABLE (
	[TranNo] [int] NULL,
	[Cust] [varchar](35) NULL,
	[Account] [varchar](35) NULL,
	[ActivityType] [int] NULL,
	[ComparisonType] [int] NULL,
	[CashTran] [int] NULL,
	[BaseAmt] [money] NULL,
	[BookDate] [int] NULL,
	[StartMonitorDate] [int] NULL,
	[FirstMonitorDate] [int] NULL,
	[EndMonitorDate] [int] NULL,
	[RecvPay] [smallint] NULL,
	[Curr] [char](3) NULL,
	[Branch] [varchar](11) NULL,
	[Dept] [varchar](11) NULL
)

DECLARE @DSAACTolerated TABLE(
	[Account] [varchar](35) NULL,
-- Cash Tolerated Account Fields	
	[TolTotCashWithDAmtMnth] [money] NULL,
	[TolNoOfCashWithDMnth] [int] NULL,
	[TolTotCashDepositAmtMnth] [money] NULL,
	[TolNoOfCashDepositMnth] [int] NULL,
-- DOM-Wire Tolerated Account Fields	
	[TolTotDomInWireAmtMnth] [money] NULL,
	[TolNoOfDomInWireMnth] [int] NULL,
	[TolTotDomOutWireAmtMnth] [money] NULL,
	[TolNoOfDomOutWireMnth] [int] NULL,
-- FOR-Wire Tolerated Account Fields	
	[TolTotForInWireAmtMnth] [money] NULL,
	[TolNoOfForInWireMnth] [int] NULL,
	[TolTotForOutWireAmtMnth] [money] NULL,
	[TolNoOfForOutWireMnth] [int] NULL,	
-- ALL-Wire Tolerated Account Fields
	[TolTotAllInWireAmtMnth] [money] NULL,
	[TolNoOfAllInWireMnth] [int] NULL,
	[TolTotAllOutWireAmtMnth] [money] NULL,
	[TolNoOfAllOutWireMnth] [int] NULL,
-- Check Tolerated Account Fields	
	[TolTotCheckWAmtMnth] [money] NULL,
	[TolNoOfCheckWMnth] [int] NULL,
-- Deposit Tolerated Account Fields	
	[TolTotDepositAmtMnth] [money] NULL,
	[TolNoOfDepositMnth] [int] NULL	
)

DECLARE @DSTT TABLE (
	[Account] [varchar](35) NULL,
	[ComparisonType] [int] NULL,	
	[Cust] [varchar](35) NULL,
	[AACResultDesc] [varchar](1000) NULL,
	[RecvPay] [int] NULL,
	[TransAmt] [money] NULL,
	[TransCnt] [int] NULL,
	[boundaryAmt] [money] NULL,
	[boundaryCnt] [int] NULL,
	[AACComparisonTypeDetail] [varchar](100) NULL
)

DECLARE @CustomerExemptionsList VARCHAR(8000)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	BEGIN TRAN USR_AntpActvComps
ELSE
	SAVE TRAN USR_AntpActvComps

SELECT @description = [Desc], @WLType = WLType ,
	@SetDate =
	CASE
		WHEN UseSysDate = 2 THEN                       
			(SELECT BusDate FROM dbo.SysParam)	-- use business date
		ELSE
            GetDate()	-- use system date
	END
FROM dbo.WatchList WITH (NOLOCK)
WHERE WLCode = @WLCode

If( ( @startDate is null ) OR ( ltrim(rtrim(@startDate)) = '' )   
	AND ( @endDate is null ) OR ( ltrim(rtrim(@endDate)) = '' ) ) 
	BEGIN
		--If Both Start and End dates are blank then default dates to previous month per the business or system date determined by @SetDate”. 
		DECLARE @nowDate DATETIME = (SELECT DATEADD(M, DATEDIFF(M, 0, @SetDate), 0))
		DECLARE @firstDay DATETIME = (SELECT DATEADD(M, -1, @nowDate))
		DECLARE @lastDay DATETIME = (SELECT DATEADD(D, -1, @nowDate))
		SET @fromDate = dbo.ConvertSqlDateToInt(@firstDay)
		SET @toDate = dbo.ConvertSqlDateToInt(@lastDay)
		SET @strFromDate = CONVERT(varchar(10), CONVERT(date, CONVERT(varchar(8), @fromDate), 112), 101)
		SET @strToDate = CONVERT(varchar(10), CONVERT(date, CONVERT(varchar(8), @toDate), 112), 101)		
	END
ELSE If	( ( @startDate is not null ) AND ( ltrim(rtrim(@startDate)) <> '' ) 
		AND ( @endDate is not null ) AND ( ltrim(rtrim(@endDate)) <> '' ) ) 
	BEGIN
		--If Both dates are populated then use the provided dates	
		SET @fromDate = dbo.ConvertSqlDateToInt(@startDate)
		SET @toDate = dbo.ConvertSqlDateToInt(@endDate)
		SET @strFromDate = CONVERT(varchar(10), CONVERT(date, CONVERT(varchar(8), @fromDate), 112), 101)
		SET @strToDate = CONVERT(varchar(10), CONVERT(date, CONVERT(varchar(8), @toDate), 112), 101)		
	END
ELSE
BEGIN
	--If only one of the Start or End Date is BLANK, then do nothing per Business decision
	GOTO EndOfProc
END

SET @strBetweenDates = ' between the dates of ' + @strFromDate + ' and ' +  @strToDate +'.'

SELECT  @firstMonitorDate = Case WHEN  @numDayMonitor <= 0 THEN 18000101 ELSE DBO.CONVERTSQLDATETOINT( DATEADD(D, -@numDayMonitor, CONVERT(VARCHAR, GETDATE() ) ) ) END
	-- if user-defined @numDayMonitor = 0 entered, we will SET @firstMonitorDate = 18000101 as the default integer value of the date back to Jan 1, 1800.
SELECT @riskClassList     = dbo.BSA_fnListParams(@riskClassList)
SELECT @branchList        = dbo.BSA_fnListParams(@branchList)
SELECT @departmentList    = dbo.BSA_fnListParams(@departmentList)
SELECT @acctTypesList     = dbo.BSA_fnListParams(@acctTypesList)
SELECT @actvTypesListIncl = dbo.BSA_fnListParams(@actvTypesListIncl)
SELECT @actvTypesListExcl = dbo.BSA_fnListParams(@actvTypesListExcl)
SELECT @custTypesList     = dbo.BSA_fnListParams(@custTypesList)
SELECT @comparisonType	  = ISNULL(@comparisonType, 0) 		-- Default to not comparing
SELECT @minTransCount	  = ISNULL(@minTransCount, 0) 		-- Default as ignore this parameter
SELECT @minAggTransAmt 	  = ISNULL(@minAggTransAmt, 0.00)	-- Default as ignore this parameter 		
SELECT @monitorInd		  = ISNULL(@monitorInd, 2) 			-- Default to monitor count
SELECT @aggAntpDnFCntAmt  = ISNULL(@aggAntpDnFCntAmt, 0) 	-- Deafult as handling DOM and FOR WIRE separately
SELECT @amtTolerance	  = ISNULL(@amtTolerance, 0)		-- Default as ignore this parameter 
SELECT @cntTolerance	  = ISNULL(@cntTolerance, 0) 		-- Default as ignore this parameter 
SELECT @iZAV = Case WHEN @ignoreZeroAntVals = 1  THEN 1 WHEN @ignoreZeroAntVals = 0 THEN 0 ELSE 1 END

IF (@cashType = 2)
	SET @cashType = NULL
	
IF (@receivedOrPay = 3)
	SET @receivedOrPay = NULL

IF (ISNULL(@actvTypesListExcl,'') = '' OR UPPER(ISNULL(@actvTypesListExcl,'-NONE-')) = '-NONE-')
	SET @actvTypesListExcl = NULL

If @comparisonType <> 2 -- Cash, Check, or Deposit ActivityType
	BEGIN
		INSERT INTO @DSQA
		SELECT 	 Ach.TranNo
				,Ach.Cust
				,Ach.account
				,Ach.Type AS ActivityType
				,@comparisonType AS ComparisonType
				,Ach.CashTran
				,Ach.baseAmt
				,Ach.bookdate
				,@fromDate AS StartMonitorDate
				,@firstMonitorDate As FirstMonitorDate
				,@toDate AS EndMonitorDate
				,Ach.recvpay 
				,Ach.Curr		
				,Ach.Branch
				,Ach.Dept
				FROM 	ActivityHist Ach WITH (NOLOCK)
				INNER	JOIN Customer WITH (NOLOCK) ON Ach.cust = customer.id 
				INNER   JOIN Account WITH (NOLOCK) ON Ach.account = Account.id		
				WHERE  ( (Account.OpenDate = '') OR (Account.OpenDate Is NULL) OR (dbo.ConvertSqlDateToInt(Account.OpenDate) >= @firstMonitorDate) ) 
					AND (BookDate >= @fromDate) AND (BookDate <= @toDate)
					AND (Ach.RecvPay = ISNULL(@receivedOrPay, Ach.RecvPay))			
					AND (Ach.CASHTRAN = ISNULL(@cashType, Ach.CASHTRAN))	
					AND ((@riskClassList IS NULL OR 
						CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0)) 
					AND ((@branchList IS NULL  OR 
						CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Ach.branch,''))) + ',', @branchList) > 0))		
					AND ((@departmentList IS NULL OR 
						CHARINDEX(',' + CONVERT(VARCHAR,  Ach.dept) + ',',@departmentList) > 0))		
					AND ((@acctTypesList IS NULL OR 
						CHARINDEX(',' + LTRIM(RTRIM(Account.type)) + ',', @acctTypesList ) > 0))
					AND ((@custTypesList IS NULL OR
						CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @custTypesList ) > 0))
					AND (@actvTypesListIncl IS NULL OR 
						CHARINDEX(',' + CONVERT(VARCHAR, Ach.Type) + ',', @actvTypesListIncl ) > 0)
		IF @@ROWCOUNT = 0 GOTO EndOfProc						
	END
ELSE 
IF @comparisonType = 2 -- Wire ActivityType
	BEGIN
		INSERT INTO @DSQA	
		SELECT 	 Ach.TranNo
				,Ach.Cust
				,Ach.account
				,Ach.Type AS ActivityType
				,@comparisonType AS ComparisonType
				,Ach.CashTran
				,Ach.baseAmt
				,Ach.bookdate
				,@fromDate AS StartMonitorDate
				,@firstMonitorDate As FirstMonitorDate				
				,@toDate AS EndMonitorDate
				,Ach.recvpay 
				,Ach.Curr		
				,Ach.Branch
				,Ach.Dept
				FROM 	ActivityHist Ach WITH (NOLOCK)
				INNER	JOIN Customer WITH (NOLOCK) ON Ach.cust = customer.id 
				INNER   JOIN Account WITH (NOLOCK) ON Ach.account = Account.id
				WHERE  ( (Account.OpenDate = '') OR (Account.OpenDate Is NULL) OR (dbo.ConvertSqlDateToInt(Account.OpenDate) >= @firstMonitorDate) ) 
					AND (BookDate >= @fromDate) AND (BookDate <= @toDate)
					AND (Ach.RecvPay = ISNULL(@receivedOrPay, Ach.RecvPay))			
					AND (Ach.CASHTRAN = ISNULL(@cashType, Ach.CASHTRAN))	
					AND ((@riskClassList IS NULL OR 
						CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0)) 
					AND ((@branchList IS NULL  OR 
						CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Ach.branch,''))) + ',', @branchList) > 0))		
					AND ((@departmentList IS NULL OR 
						CHARINDEX(',' + CONVERT(VARCHAR,  Ach.dept) + ',',@departmentList) > 0))		
					AND ((@acctTypesList IS NULL OR 
						CHARINDEX(',' + LTRIM(RTRIM(Account.type)) + ',', @acctTypesList ) > 0))
					AND ((@custTypesList IS NULL OR @actvTypesListExcl IS NULL OR
						CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @custTypesList ) > 0))
					AND (@actvTypesListIncl IS NULL OR  @actvTypesListExcl IS NULL OR
						CHARINDEX(',' + CONVERT(VARCHAR, Ach.Type) + ',', @actvTypesListIncl ) > 0
						OR
						CHARINDEX(',' + CONVERT(VARCHAR, Ach.Type) + ',', @actvTypesListExcl ) > 0)
		IF @@ROWCOUNT = 0 GOTO EndOfProc						

	END

If @comparisonType <> 2
	BEGIN
		If EXISTS ( SELECT * FROM  @DSQA	
			WHERE CHARINDEX(',' + CONVERT(VARCHAR, ActivityType) + ',', @actvTypesListExcl )  > 0
		)
		If (@actvTypesListExcl IS NOT NULL)
			DELETE FROM @DSQA WHERE CHARINDEX(',' + CONVERT(VARCHAR, ActivityType) + ',', @actvTypesListExcl )  > 0
	END
	
	Insert Into @DSAACTolerated
	SELECT DISTINCT 		
			 Q.Account
			,TotCashWithDAmtMnth * (1.0 + @amtTolerance/100.0)  AS TolTotCashWithDAmtMnth
			,CONVERT(INT, NoOfCashWithDMnth * (1.0 + @cntTolerance/100.0) + 0.5 ) AS TolNoOfCashWithDMnth
			,TotCashDepositAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotCashDepositAmtMnth
			,CONVERT(INT, NoOfCashDepositMnth * (1.0 + @cntTolerance/100.0) + 0.5 )AS TolNoOfCashDepositMnth
			,TotDomInWireAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotDomInWireAmtMnth
			,CONVERT(INT, NoOfDomInWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ) AS TolNoOfDomInWireMnth
			,TotDomOutWireAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotDomOutWireAmtMnth
			,CONVERT(INT, NoOfDomOutWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ) AS TolNoOfDomOutWireMnth        
			,TotForInWireAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotForInWireAmtMnth
			,CONVERT(INT, NoOfForInWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ) AS TolNoOfForInWireMnth
			,TotForOutWireAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotForOutWireAmtMnth
			,CONVERT(INT, NoOfForOutWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ) AS TolNoOfForOutWireMnth
			,TolTotAllInWireAmtMnth = Case 
				When ( TotDomInWireAmtMnth Is Not NULL AND TotForInWireAmtMnth Is Not NULL ) Then ( TotDomInWireAmtMnth + TotForInWireAmtMnth ) * (1.0 + @amtTolerance/100.0) 
				When ( TotDomInWireAmtMnth Is NULL AND TotForInWireAmtMnth Is NULL ) Then  NULL
				Else ( IsNULL(TotDomInWireAmtMnth, 0.0) + IsNULL(TotForInWireAmtMnth, 0.0) ) * (1.0 + @amtTolerance/100.0) END
			,TolNoOfAllInWireMnth = Case
				When ( NoOfDomInWireMnth Is Not NULL AND NoOfForInWireMnth Is Not NULL ) Then CONVERT(INT, ( NoOfDomInWireMnth + NoOfForInWireMnth ) * (1.0 + @cntTolerance/100.0) + 0.5 ) 
				When ( NoOfDomInWireMnth Is NULL AND NoOfForInWireMnth Is NULL ) Then NULL
				Else CONVERT(INT, ( IsNULL(NoOfDomInWireMnth,0) + IsNULL(NoOfForInWireMnth,0) ) * (1.0 + @cntTolerance/100.0) + 0.5 ) END
			,TolTotAllOutWireAmtMnth = Case 
				When ( TotDomOutWireAmtMnth Is Not Null AND TotForOutWireAmtMnth Is Not Null) Then ( TotDomOutWireAmtMnth + TotForOutWireAmtMnth ) * (1.0 + @amtTolerance/100.0)
				When ( TotDomOutWireAmtMnth Is Null AND TotForOutWireAmtMnth Is Null) Then NULL
				Else ( IsNULL(TotDomOutWireAmtMnth,0.0) + IsNULL(TotForOutWireAmtMnth, 0.0) ) * (1.0 + @amtTolerance/100.0)	END
			,TolNoOfAllOutWireMnth = Case
				When ( NoOfDomOutWireMnth Is Not Null AND NoOfForOutWireMnth Is Not Null ) Then CONVERT(INT, ( NoOfDomOutWireMnth + NoOfForOutWireMnth ) * (1.0 + @cntTolerance/100.0) + 0.5 )
				When ( NoOfDomOutWireMnth Is Null AND NoOfForOutWireMnth Is Null ) Then Null
				Else CONVERT(INT, ( IsNULL(NoOfDomOutWireMnth,0) + IsNULL(NoOfForOutWireMnth,0) ) * (1.0 + @cntTolerance/100.0) + 0.5 ) END
			,TotCheckWAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotCheckWAmtMnth
			,CONVERT(INT, NoOfCheckWMnth * (1.0 + @cntTolerance/100.0) + 0.5 ) AS TolNoOfCheckWMnth
			,TotDepositAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotDepositAmtMnth
			,CONVERT(INT, NoOfDepositMnth * (1.0 + @cntTolerance/100.0) + 0.5 )  AS TolNoOfDepositMnth		
	FROM	Account A WITH (NOLOCK) INNER JOIN @DSQA Q
	ON		A.Id = Q.Account
               
--
if @comparisonType = 1
	Begin
		Delete From @DSAACTolerated									 
		Where ( TolTotCashDepositAmtMnth Is NULL AND TolNoOfCashDepositMnth Is NULL ) 
		  AND ( TolTotCashWithDAmtMnth Is NULL AND TolNoOfCashWithDMnth Is NULL )
	
		INSERT INTO @DSTT 					   
		SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
		  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
		  + CASE WHEN ( (( T.TolTotCashDepositAmtMnth = 0.00) AND (@iZAV = 0) Or ( T.TolTotCashDepositAmtMnth <> 0.00)) AND ( T.TolTotCashDepositAmtMnth Is Not NULL ) AND (( T.TolNoOfCashDepositMnth = 0 ) AND (@iZAV = 0) Or ( T.TolNoOfCashDepositMnth <> 0 )) AND ( T.TolNoOfCashDepositMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashDepositAmtMnth) AND (COUNT(Q.TranNo) > T.TolNoOfCashDepositMnth) AND (COUNT(Q.TranNo) >= @minTransCount)
					THEN 'cash deposit transactions of $' + CONVERT(varchar, T.TolTotCashDepositAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
				      + @strExceed2 + ' cash deposit transactions of ' + CONVERT(VARCHAR, T.TolNoOfCashDepositMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
				 WHEN ( (( T.TolTotCashDepositAmtMnth = 0.00) AND (@iZAV = 0) Or ( T.TolTotCashDepositAmtMnth <> 0.00)) AND ( T.TolTotCashDepositAmtMnth Is Not NULL ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashDepositAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) ) 
					THEN 'cash deposit transactions of $' + CONVERT(varchar, T.TolTotCashDepositAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
				 WHEN ( (( T.TolNoOfCashDepositMnth = 0 ) AND (@iZAV = 0) Or ( T.TolNoOfCashDepositMnth <> 0 )) AND ( T.TolNoOfCashDepositMnth Is Not NULL ) AND (COUNT(Q.TranNo) >= @minTransCount) AND (COUNT(Q.TranNo) > T.TolNoOfCashDepositMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN 'number of cash deposit transactions of ' + CONVERT(VARCHAR, T.TolNoOfCashDepositMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
			END + @strDueTo 
		  + CASE WHEN ( (( T.TolTotCashDepositAmtMnth = 0.00) AND (@iZAV = 0) Or ( T.TolTotCashDepositAmtMnth <> 0.00)) AND ( T.TolTotCashDepositAmtMnth Is Not NULL ) AND (( T.TolNoOfCashDepositMnth = 0 ) AND (@iZAV = 0) Or ( T.TolNoOfCashDepositMnth <> 0 )) AND ( T.TolNoOfCashDepositMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashDepositAmtMnth) AND (COUNT(Q.TranNo) > T.TolNoOfCashDepositMnth) AND (COUNT(Q.TranNo) >= @minTransCount)
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' cash deposit transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( T.TolTotCashDepositAmtMnth = 0.00) AND (@iZAV = 0) Or ( T.TolTotCashDepositAmtMnth <> 0.00)) AND ( T.TolTotCashDepositAmtMnth Is Not NULL ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashDepositAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'the total cash deposit transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( T.TolNoOfCashDepositMnth = 0 ) AND (@iZAV = 0) Or ( T.TolNoOfCashDepositMnth <> 0 )) AND ( T.TolNoOfCashDepositMnth Is Not NULL ) AND (COUNT(Q.TranNo) >= @minTransCount) AND (COUNT(Q.TranNo) > T.TolNoOfCashDepositMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' cash deposit transactions'
			END + @strBetweenDates
			, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
			, Case WHEN T.TolTotCashDepositAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotCashDepositAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
			, CASE WHEN T.TolNoOfCashDepositMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfCashDepositMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
			, ' cash ' + Case WHEN Q.RecvPay = 1 THEN 'deposit ' ELSE 'withdrawal ' END + 'transactions' AS AACComparisonTypeDetail  			
		FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
		WHERE Q.CashTran = 1 AND (Q.RecvPay = 1) 
		GROUP BY Q.Account, Q.RecvPay, T.TolTotCashDepositAmtMnth, T.TolNoOfCashDepositMnth
		HAVING  
		(
				( (( T.TolTotCashDepositAmtMnth = 0.00) AND (@iZAV = 0) Or ( T.TolTotCashDepositAmtMnth <> 0.00)) AND ( T.TolTotCashDepositAmtMnth Is Not NULL ) AND (( T.TolNoOfCashDepositMnth = 0 ) AND (@iZAV = 0) Or ( T.TolNoOfCashDepositMnth <> 0 )) AND ( T.TolNoOfCashDepositMnth Is Not NULL ) )
				AND ( @monitorInd = 3 ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND ( SUM(Q.BaseAmt) > T.TolTotCashDepositAmtMnth AND COUNT(Q.TranNo) > T.TolNoOfCashDepositMnth AND (COUNT(Q.TranNo) >= @minTransCount) )
			OR
				( (( T.TolTotCashDepositAmtMnth = 0.00) AND (@iZAV = 0) Or ( T.TolTotCashDepositAmtMnth <> 0.00)) AND ( T.TolTotCashDepositAmtMnth Is Not NULL ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashDepositAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) ) 
			OR
				( (( T.TolNoOfCashDepositMnth = 0 ) AND (@iZAV = 0) Or ( T.TolNoOfCashDepositMnth <> 0 )) AND ( T.TolNoOfCashDepositMnth Is Not NULL ) AND (COUNT(Q.TranNo) >= @minTransCount) AND (COUNT(Q.TranNo) > T.TolNoOfCashDepositMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
		)
		UNION
		SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
		  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
		  + CASE WHEN ( (( T.TolTotCashWithDAmtMnth = 0.00) AND (@iZAV = 0) Or ( T.TolTotCashWithDAmtMnth <> 0.00)) AND ( T.TolTotCashWithDAmtMnth Is Not NULL ) AND (( T.TolNoOfCashWithDMnth = 0 ) AND (@iZAV = 0) Or ( T.TolNoOfCashWithDMnth <> 0 )) AND ( T.TolNoOfCashWithDMnth Is Not NULL ) )
						AND ( @monitorInd = 3 ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND ( SUM(Q.BaseAmt) > T.TolTotCashWithDAmtMnth AND (COUNT(Q.TranNo) >= @minTransCount) AND COUNT(Q.TranNo) > T.TolNoOfCashWithDMnth )
					THEN 'cash withdrawal transactions of $' + CONVERT(varchar, T.TolTotCashWithDAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
				      + @strExceed2 + ' cash withdrawal transactions of ' + CONVERT(VARCHAR, T.TolNoOfCashWithDMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
				 WHEN ( (( T.TolTotCashWithDAmtMnth = 0.00) AND (@iZAV = 0) Or ( T.TolTotCashWithDAmtMnth <> 0.00)) AND ( T.TolTotCashWithDAmtMnth Is Not NULL ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashWithDAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'cash withdrawal transactions of $' + CONVERT(varchar, T.TolTotCashWithDAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
				 WHEN ( (( T.TolNoOfCashWithDMnth = 0 ) AND (@iZAV = 0) Or ( T.TolNoOfCashWithDMnth <> 0 )) AND ( T.TolNoOfCashWithDMnth Is Not NULL ) AND (COUNT(Q.TranNo) >= @minTransCount) AND (COUNT(Q.TranNo) > T.TolNoOfCashWithDMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN 'number of cash withdrawal transactions of ' + CONVERT(VARCHAR, T.TolNoOfCashWithDMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
			END + @strDueTo
		  + CASE WHEN ( (( T.TolTotCashWithDAmtMnth = 0.00) AND (@iZAV = 0) Or ( T.TolTotCashWithDAmtMnth <> 0.00)) AND ( T.TolTotCashWithDAmtMnth Is Not NULL ) AND (( T.TolNoOfCashWithDMnth = 0 ) AND (@iZAV = 0) Or ( T.TolNoOfCashWithDMnth <> 0 )) AND ( T.TolNoOfCashWithDMnth Is Not NULL ) )
						AND ( @monitorInd = 3 ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND ( SUM(Q.BaseAmt) > T.TolTotCashWithDAmtMnth AND (COUNT(Q.TranNo) >= @minTransCount) AND COUNT(Q.TranNo) > T.TolNoOfCashWithDMnth )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' cash withdrawal transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( T.TolTotCashWithDAmtMnth = 0.00) AND (@iZAV = 0) Or ( T.TolTotCashWithDAmtMnth <> 0.00)) AND ( T.TolTotCashWithDAmtMnth Is Not NULL ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashWithDAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'the total cash withdrawal transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( T.TolNoOfCashWithDMnth = 0 ) AND (@iZAV = 0) Or ( T.TolNoOfCashWithDMnth <> 0 )) AND ( T.TolNoOfCashWithDMnth Is Not NULL ) AND (COUNT(Q.TranNo) >= @minTransCount) AND (COUNT(Q.TranNo) > T.TolNoOfCashWithDMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' cash withdrawal transactions'
			END + @strBetweenDates			
			, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
			, Case WHEN T.TolTotCashWithDAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotCashWithDAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
			, CASE WHEN T.TolNoOfCashWithDMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfCashWithDMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
			, ' cash ' + Case WHEN Q.RecvPay = 1 THEN 'deposit ' ELSE 'withdrawal ' END + 'transactions' AS AACComparisonTypeDetail  
		FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
		WHERE Q.CashTran = 1 AND (Q.RecvPay = 2)
		GROUP BY Q.Account, Q.RecvPay, T.TolTotCashWithDAmtMnth, T.TolNoOfCashWithDMnth  
		HAVING 
		(
				( (( T.TolTotCashWithDAmtMnth = 0.00) AND (@iZAV = 0) Or ( T.TolTotCashWithDAmtMnth <> 0.00)) AND ( T.TolTotCashWithDAmtMnth Is Not NULL ) AND (( T.TolNoOfCashWithDMnth = 0 ) AND (@iZAV = 0) Or ( T.TolNoOfCashWithDMnth <> 0 )) AND ( T.TolNoOfCashWithDMnth Is Not NULL ) )
				AND ( @monitorInd = 3 ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND ( SUM(Q.BaseAmt) > T.TolTotCashWithDAmtMnth AND (COUNT(Q.TranNo) >= @minTransCount) AND COUNT(Q.TranNo) > T.TolNoOfCashWithDMnth ) 
			OR
				( (( T.TolTotCashWithDAmtMnth = 0.00) AND (@iZAV = 0) Or ( T.TolTotCashWithDAmtMnth <> 0.00)) AND ( T.TolTotCashWithDAmtMnth Is Not NULL ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashWithDAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
			OR
				( (( T.TolNoOfCashWithDMnth = 0 ) AND (@iZAV = 0) Or ( T.TolNoOfCashWithDMnth <> 0 )) AND ( T.TolNoOfCashWithDMnth Is Not NULL ) AND (COUNT(Q.TranNo) >= @minTransCount) AND (COUNT(Q.TranNo) > T.TolNoOfCashWithDMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
		)
	End
ELSE
if @comparisonType =  2
	Begin
		Delete From @DSAACTolerated									 
		Where  ( TolTotDomInWireAmtMnth Is NULL AND TolNoOfDomInWireMnth Is NULL )  
		  AND  ( TolTotDomOutWireAmtMnth Is NULL AND TolNoOfDomOutWireMnth Is NULL )
		  AND  ( TolTotForInWireAmtMnth Is NULL AND TolNoOfForInWireMnth Is NULL ) 
		  AND  ( TolTotForOutWireAmtMnth Is NULL AND TolNoOfForOutWireMnth Is NULL )
		  									 
		if @aggAntpDnFCntAmt = 0
			BEGIN	
				INSERT INTO @DSTT 					   			  
				SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
				  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
				  + CASE WHEN ( (( TolTotDomInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotDomInWireAmtMnth <> 0.00)) AND ( TolTotDomInWireAmtMnth Is Not NULL ) AND (( TolNoOfDomInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDomInWireMnth <> 0 )) AND ( TolNoOfDomInWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDomInWireAmtMnth AND COUNT(TranNo) > TolNoOfDomInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN 'incoming DOM-wire transactions of $' + CONVERT(varchar, T.TolTotDomInWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
							  + @strExceed2 + ' incoming DOM-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfDomInWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
						 WHEN (( TolTotDomInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotDomInWireAmtMnth <> 0.00)) AND (TolTotDomInWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotDomInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'incoming DOM-wire transactions of $' + CONVERT(varchar, T.TolTotDomInWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
						 WHEN (( TolNoOfDomInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDomInWireMnth <> 0 )) AND ( TolNoOfDomInWireMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfDomInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN 'number of incoming DOM-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfDomInWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
					END + @strDueTo
				  + CASE WHEN ( (( TolTotDomInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotDomInWireAmtMnth <> 0.00)) AND ( TolTotDomInWireAmtMnth Is Not NULL ) AND (( TolNoOfDomInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDomInWireMnth <> 0 )) AND ( TolNoOfDomInWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDomInWireAmtMnth AND COUNT(TranNo) > TolNoOfDomInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' incoming DOM-wire transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolTotDomInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotDomInWireAmtMnth <> 0.00)) AND (TolTotDomInWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotDomInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'the total incoming DOM-wire transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolNoOfDomInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDomInWireMnth <> 0 )) AND ( TolNoOfDomInWireMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfDomInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' incoming DOM-wire transactions'
					END + @strBetweenDates
					, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
					, Case WHEN T.TolTotDomInWireAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotDomInWireAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
					, CASE WHEN T.TolNoOfDomInWireMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfDomInWireMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
					, ' DOM-wire ' + Case WHEN Q.RecvPay = 1 THEN 'incoming ' ELSE 'outgoing ' END + 'transactions' AS AACComparisonTypeDetail  			
				FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
				WHERE (RecvPay = 1)
					   AND (@actvTypesListIncl IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, ActivityType) + ',', @actvTypesListIncl ) > 0)
				GROUP BY Q.Account, RecvPay, TolTotDomInWireAmtMnth, TolNoOfDomInWireMnth
				HAVING 
				(
						( (( TolTotDomInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotDomInWireAmtMnth <> 0.00)) AND ( TolTotDomInWireAmtMnth Is Not NULL ) AND (( TolNoOfDomInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDomInWireMnth <> 0 )) AND ( TolNoOfDomInWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDomInWireAmtMnth AND COUNT(TranNo) > TolNoOfDomInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt) 
					OR
						(( TolTotDomInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotDomInWireAmtMnth <> 0.00)) AND (TolTotDomInWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotDomInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
					OR
						(( TolNoOfDomInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDomInWireMnth <> 0 )) AND ( TolNoOfDomInWireMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfDomInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
				)
				UNION 
				SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
				  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
				  + CASE WHEN ( (( TolTotDomOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotDomOutWireAmtMnth <> 0.00)) AND ( TolTotDomOutWireAmtMnth Is Not NULL ) AND (( TolNoOfDomOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDomOutWireMnth <> 0 )) AND ( TolNoOfDomOutWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDomOutWireAmtMnth AND COUNT(TranNo) > TolNoOfDomOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN 'outgoing DOM-wire transactions of $' + CONVERT(varchar, T.TolTotDomOutWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
							  + @strExceed2 + ' outgoing DOM-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfDomOutWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
						 WHEN (( TolTotDomOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotDomOutWireAmtMnth <> 0.00)) AND (TolTotDomOutWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotDomOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'outgoing DOM-wire transactions of $' + CONVERT(varchar, T.TolTotDomOutWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
						 WHEN (( TolNoOfDomOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDomOutWireMnth <> 0 )) AND (TolNoOfDomOutWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfDomOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) 
							THEN 'number of outgoing DOM-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfDomOutWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
					END + @strDueTo
				  + CASE WHEN ( (( TolTotDomOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotDomOutWireAmtMnth <> 0.00)) AND ( TolTotDomOutWireAmtMnth Is Not NULL ) AND (( TolNoOfDomOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDomOutWireMnth <> 0 )) AND ( TolNoOfDomOutWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDomOutWireAmtMnth AND COUNT(TranNo) > TolNoOfDomOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' outgoing DOM-wire transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolTotDomOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotDomOutWireAmtMnth <> 0.00)) AND (TolTotDomOutWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotDomOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'the total outgoing DOM-wire transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolNoOfDomOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDomOutWireMnth <> 0 )) AND (TolNoOfDomOutWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfDomOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) 
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' outgoing DOM-wire transactions'
					END + @strBetweenDates
					, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
					, Case WHEN T.TolTotDomOutWireAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotDomOutWireAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
					, CASE WHEN T.TolNoOfDomOutWireMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfDomOutWireMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
					, ' DOM-wire ' + Case WHEN Q.RecvPay = 1 THEN 'incoming ' ELSE 'outgoing ' END + 'transactions' AS AACComparisonTypeDetail  			
				FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
				WHERE (RecvPay = 2)
					   AND (@actvTypesListIncl IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, ActivityType) + ',', @actvTypesListIncl ) > 0)
				GROUP BY Q.Account, RecvPay, TolTotDomOutWireAmtMnth, TolNoOfDomOutWireMnth
				HAVING 
				(
						( (( TolTotDomOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotDomOutWireAmtMnth <> 0.00)) AND ( TolTotDomOutWireAmtMnth Is Not NULL ) AND (( TolNoOfDomOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDomOutWireMnth <> 0 )) AND ( TolNoOfDomOutWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDomOutWireAmtMnth AND COUNT(TranNo) > TolNoOfDomOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
					OR
						(( TolTotDomOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotDomOutWireAmtMnth <> 0.00)) AND (TolTotDomOutWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotDomOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
					OR
						(( TolNoOfDomOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDomOutWireMnth <> 0 )) AND (TolNoOfDomOutWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfDomOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) 
				)			
				UNION
				SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
				  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
				  + CASE WHEN ( (( TolTotForInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotForInWireAmtMnth <> 0.00)) AND ( TolTotForInWireAmtMnth Is Not NULL ) AND (( TolNoOfForInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfForInWireMnth <> 0 )) AND ( TolNoOfForInWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotForInWireAmtMnth AND COUNT(TranNo) > TolNoOfForInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN 'incoming FOR-wire transactions of $' + CONVERT(varchar, T.TolTotForInWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
							  + @strExceed2 + ' incoming FOR-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfForInWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
						 WHEN (( TolTotForInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotForInWireAmtMnth <> 0.00)) AND (TolTotForInWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotForInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'incoming FOR-wire transactions of $' + CONVERT(varchar, T.TolTotForInWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
						 WHEN (( TolNoOfForInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfForInWireMnth <> 0 )) AND (TolNoOfForInWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfForInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN 'number of incoming FOR-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfForInWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
					END + @strDueTo
				  + CASE WHEN ( (( TolTotForInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotForInWireAmtMnth <> 0.00)) AND ( TolTotForInWireAmtMnth Is Not NULL ) AND (( TolNoOfForInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfForInWireMnth <> 0 )) AND ( TolNoOfForInWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotForInWireAmtMnth AND COUNT(TranNo) > TolNoOfForInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' incoming FOR-wire transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolTotForInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotForInWireAmtMnth <> 0.00)) AND (TolTotForInWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotForInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'the total incoming FOR-wire transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolNoOfForInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfForInWireMnth <> 0 )) AND (TolNoOfForInWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfForInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' incoming FOR-wire transactions'
					END + @strBetweenDates
					, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
					, Case WHEN T.TolTotForInWireAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotForInWireAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
					, CASE WHEN T.TolNoOfForInWireMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfForInWireMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
					, ' FOR-wire ' + Case WHEN Q.RecvPay = 1 THEN 'incoming ' ELSE 'outgoing ' END + 'transactions' AS AACComparisonTypeDetail  			
				FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
				WHERE (RecvPay = 1)
					   AND (@actvTypesListExcl IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, ActivityType) + ',', @actvTypesListExcl ) > 0)
				GROUP BY Q.Account, RecvPay, TolTotForInWireAmtMnth, TolNoOfForInWireMnth
				HAVING 
				(
						( (( TolTotForInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotForInWireAmtMnth <> 0.00)) AND ( TolTotForInWireAmtMnth Is Not NULL ) AND (( TolNoOfForInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfForInWireMnth <> 0 )) AND ( TolNoOfForInWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotForInWireAmtMnth AND COUNT(TranNo) > TolNoOfForInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
					OR
						(( TolTotForInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotForInWireAmtMnth <> 0.00)) AND (TolTotForInWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotForInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
					OR
						(( TolNoOfForInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfForInWireMnth <> 0 )) AND (TolNoOfForInWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfForInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
				) 
				UNION 
				SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
				  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
				  + CASE WHEN ( (( TolTotForOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotForOutWireAmtMnth <> 0.00)) AND ( TolTotForOutWireAmtMnth Is Not NULL ) AND (( TolNoOfForOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfForOutWireMnth <> 0 )) AND ( TolNoOfForOutWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotForOutWireAmtMnth AND COUNT(TranNo) > TolNoOfForOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN 'outgoing FOR-wire transactions of $' + CONVERT(varchar, T.TolTotForOutWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
							  + @strExceed2 + ' outgoing FOR-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfForOutWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
						 WHEN (( TolTotForOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotForOutWireAmtMnth <> 0.00)) AND (TolTotForOutWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotForOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'outgoing FOR-wire transactions of $' + CONVERT(varchar, T.TolTotForOutWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
						 WHEN (( TolNoOfForOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfForOutWireMnth <> 0 )) AND (TolNoOfForOutWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfForOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN 'number of outgoing FOR-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfForOutWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
					END + @strDueTo
				  + CASE WHEN  ( (( TolTotForOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotForOutWireAmtMnth <> 0.00)) AND ( TolTotForOutWireAmtMnth Is Not NULL ) AND (( TolNoOfForOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfForOutWireMnth <> 0 )) AND ( TolNoOfForOutWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotForOutWireAmtMnth AND COUNT(TranNo) > TolNoOfForOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' outgoing FOR-wire transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolTotForOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotForOutWireAmtMnth <> 0.00)) AND (TolTotForOutWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotForOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'the total outgoing FOR-wire transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolNoOfForOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfForOutWireMnth <> 0 )) AND (TolNoOfForOutWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfForOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' outgoing FOR-wire transactions'
					END + @strBetweenDates
					, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
					, Case WHEN T.TolTotForOutWireAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotForOutWireAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
					, CASE WHEN T.TolNoOfForOutWireMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfForOutWireMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
					, ' FOR-wire ' + Case WHEN Q.RecvPay = 1 THEN 'incoming ' ELSE 'outgoing ' END + 'transactions' AS AACComparisonTypeDetail  			
				FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
				WHERE (RecvPay = 2)
					  AND (@actvTypesListExcl IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, ActivityType) + ',', @actvTypesListExcl ) > 0)
				GROUP BY Q.Account, RecvPay, TolTotForOutWireAmtMnth, TolNoOfForOutWireMnth
				HAVING 
				(
						( (( TolTotForOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotForOutWireAmtMnth <> 0.00)) AND ( TolTotForOutWireAmtMnth Is Not NULL ) AND (( TolNoOfForOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfForOutWireMnth <> 0 )) AND ( TolNoOfForOutWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotForOutWireAmtMnth AND COUNT(TranNo) > TolNoOfForOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
					OR
						(( TolTotForOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotForOutWireAmtMnth <> 0.00)) AND (TolTotForOutWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotForOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
					OR
						(( TolNoOfForOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfForOutWireMnth <> 0 )) AND (TolNoOfForOutWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfForOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
				)
			END
		ELSE if @aggAntpDnFCntAmt = 1
			BEGIN
				INSERT INTO @DSTT 	
				SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
				  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
				  + CASE WHEN ( (( TolTotAllInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotAllInWireAmtMnth <> 0.00)) AND ( TolTotAllInWireAmtMnth Is Not NULL ) AND (( TolNoOfAllInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfAllInWireMnth <> 0 )) AND ( TolNoOfAllInWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND ( SUM(BaseAmt) > TolTotAllInWireAmtMnth AND COUNT(TranNo) > TolNoOfAllInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt )
							THEN 'incoming all-wire transactions of $' + CONVERT(varchar, T.TolTotAllInWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, Convert(int, @amtTolerance))) + '% tolerance)'  
							  + @strExceed2 + ' incoming all-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfAllInWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
						 WHEN (( TolTotAllInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotAllInWireAmtMnth <> 0.00)) AND (TolTotAllInWireAmtMnth IS Not NULL) AND (SUM(BaseAmt) > TolTotAllInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) 
							THEN 'incoming all-wire transactions of $' + CONVERT(varchar, T.TolTotAllInWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, Convert(int, @amtTolerance))) + '% tolerance)' 
						 WHEN (( TolNoOfAllInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfAllInWireMnth <> 0 )) AND ( TolNoOfAllInWireMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfAllInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN 'number of incoming all-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfAllInWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
					END + @strDueTo
				  + CASE WHEN ( (( TolTotAllInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotAllInWireAmtMnth <> 0.00)) AND ( TolTotAllInWireAmtMnth Is Not NULL ) AND (( TolNoOfAllInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfAllInWireMnth <> 0 )) AND ( TolNoOfAllInWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND ( SUM(BaseAmt) > TolTotAllInWireAmtMnth AND COUNT(TranNo) > TolNoOfAllInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' incoming all-wire transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolTotAllInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotAllInWireAmtMnth <> 0.00)) AND (TolTotAllInWireAmtMnth IS Not NULL) AND (SUM(BaseAmt) > TolTotAllInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) 
							THEN 'the total incoming all-wire transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolNoOfAllInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfAllInWireMnth <> 0 )) AND ( TolNoOfAllInWireMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfAllInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' incoming all-wire transactions'
					END + @strBetweenDates
					, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
					, Case WHEN T.TolTotAllInWireAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotAllInWireAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
					, CASE WHEN T.TolNoOfAllInWireMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfAllInWireMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
					, ' all-wire ' + Case WHEN Q.RecvPay = 1 THEN 'incoming ' ELSE 'outgoing ' END + 'transactions' AS AACComparisonTypeDetail  			
				FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
				WHERE (RecvPay = 1)
				GROUP BY Q.Account, RecvPay, TolTotAllInWireAmtMnth, TolNoOfAllInWireMnth
				HAVING 
				(
						( (( TolTotAllInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotAllInWireAmtMnth <> 0.00)) AND ( TolTotAllInWireAmtMnth Is Not NULL ) AND (( TolNoOfAllInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfAllInWireMnth <> 0 )) AND ( TolNoOfAllInWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND ( SUM(BaseAmt) > TolTotAllInWireAmtMnth AND COUNT(TranNo) > TolNoOfAllInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt )
					OR
						(( TolTotAllInWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotAllInWireAmtMnth <> 0.00)) AND (TolTotAllInWireAmtMnth IS Not NULL) AND (SUM(BaseAmt) > TolTotAllInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) 
					OR
						(( TolNoOfAllInWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfAllInWireMnth <> 0 )) AND ( TolNoOfAllInWireMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfAllInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
				)
				UNION 
				SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
				  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
				  + CASE WHEN ( (( TolTotAllOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotAllOutWireAmtMnth <> 0.00)) AND ( TolTotAllOutWireAmtMnth Is Not NULL ) AND (( TolNoOfAllOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfAllOutWireMnth <> 0 )) AND ( TolNoOfAllOutWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND ( SUM(BaseAmt) > TolTotAllOutWireAmtMnth AND COUNT(TranNo) > TolNoOfAllOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt )
							THEN 'outgoing all-wire transactions of $' + CONVERT(varchar, T.TolTotAllOutWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
							  + @strExceed2 + ' outgoing all-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfAllOutWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
						 WHEN (( TolTotAllOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotAllOutWireAmtMnth <> 0.00)) AND (TolTotAllOutWireAmtMnth IS Not NULL) AND (SUM(BaseAmt) > TolTotAllOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'outgoing all-wire transactions of $' + CONVERT(varchar, T.TolTotAllOutWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
						 WHEN (( TolNoOfAllOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfAllOutWireMnth <> 0 )) AND (TolNoOfAllOutWireMnth IS Not NULL) AND (COUNT(TranNo) > TolNoOfAllOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN 'number of outgoing all-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfAllOutWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
					END + @strDueTo
				  + CASE WHEN ( (( TolTotAllOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotAllOutWireAmtMnth <> 0.00)) AND ( TolTotAllOutWireAmtMnth Is Not NULL ) AND (( TolNoOfAllOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfAllOutWireMnth <> 0 )) AND ( TolNoOfAllOutWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND ( SUM(BaseAmt) > TolTotAllOutWireAmtMnth AND COUNT(TranNo) > TolNoOfAllOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' outgoing all-wire transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolTotAllOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotAllOutWireAmtMnth <> 0.00)) AND (TolTotAllOutWireAmtMnth IS Not NULL) AND (SUM(BaseAmt) > TolTotAllOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'the total outgoing all-wire transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolNoOfAllOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfAllOutWireMnth <> 0 )) AND (TolNoOfAllOutWireMnth IS Not NULL) AND (COUNT(TranNo) > TolNoOfAllOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' outgoing all-wire transactions'
					END + @strBetweenDates
					, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
					, Case WHEN T.TolTotAllOutWireAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotAllOutWireAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
					, CASE WHEN T.TolNoOfAllOutWireMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfAllOutWireMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
					, ' all-wire ' + Case WHEN Q.RecvPay = 1 THEN 'incoming ' ELSE 'outgoing ' END + 'transactions' AS AACComparisonTypeDetail  			
				FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
				WHERE (RecvPay = 2)
				GROUP BY Q.Account, RecvPay, TolTotAllOutWireAmtMnth, TolNoOfAllOutWireMnth
				HAVING 
				(
						( (( TolTotAllOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotAllOutWireAmtMnth <> 0.00)) AND ( TolTotAllOutWireAmtMnth Is Not NULL ) AND (( TolNoOfAllOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfAllOutWireMnth <> 0 )) AND ( TolNoOfAllOutWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND ( SUM(BaseAmt) > TolTotAllOutWireAmtMnth AND COUNT(TranNo) > TolNoOfAllOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt )
					OR
						(( TolTotAllOutWireAmtMnth = 0.00) AND (@iZAV = 0) Or ( TolTotAllOutWireAmtMnth <> 0.00)) AND (TolTotAllOutWireAmtMnth IS Not NULL) AND (SUM(BaseAmt) > TolTotAllOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
					OR
						(( TolNoOfAllOutWireMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfAllOutWireMnth <> 0 )) AND (TolNoOfAllOutWireMnth IS Not NULL) AND (COUNT(TranNo) > TolNoOfAllOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
				)																			
			END
	END
ELSE
if @comparisonType =  3
	BEGIN
		Delete From @DSAACTolerated									 
		Where ( TolTotCheckWAmtMnth Is NULL AND TolNoOfCheckWMnth Is NULL )
			
		INSERT INTO @DSTT 
		SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
		  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
		  + CASE WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is Not NULL ) AND (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is Not NULL ) 
						AND ( @monitorInd = 3 ) AND ( SUM(BaseAmt) > TolTotCheckWAmtMnth AND COUNT(TranNo) > TolNoOfCheckWMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt ) )
					THEN 'check deposit transactions of $' + CONVERT(varchar, T.TolTotCheckWAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
				      + @strExceed2 + ' check deposit transactions of ' + CONVERT(VARCHAR, T.TolNoOfCheckWMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
				 WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is NOT NULL ) AND (SUM(BaseAmt) >= @minAggTransAmt) AND (SUM(BaseAmt) > TolTotCheckWAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'check deposit transactions of $' + CONVERT(varchar, T.TolTotCheckWAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
				 WHEN ( (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is NOT NULL ) AND (COUNT(TranNo) >= @minTransCount) AND (COUNT(TranNo) > TolNoOfCheckWMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN 'number of check deposit transactions of ' + CONVERT(VARCHAR, T.TolNoOfCheckWMnth) + ' (includes ' + CONVERT(VARCHAR,@cntTolerance) + '% tolerance)'
			END + @strDueTo
		  + CASE WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is Not NULL ) AND (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is Not NULL ) 
						AND ( @monitorInd = 3 ) AND ( SUM(BaseAmt) > TolTotCheckWAmtMnth AND COUNT(TranNo) > TolNoOfCheckWMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' check deposit transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is NOT NULL ) AND (SUM(BaseAmt) >= @minAggTransAmt) AND (SUM(BaseAmt) > TolTotCheckWAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'the total check deposit transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is NOT NULL ) AND (COUNT(TranNo) >= @minTransCount) AND (COUNT(TranNo) > TolNoOfCheckWMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' check deposit transactions'
				 END + @strBetweenDates
			, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
			, Case WHEN T.TolTotCheckWAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotCheckWAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
			, CASE WHEN T.TolNoOfCheckWMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfCheckWMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
			, ' check ' + Case WHEN Q.RecvPay = 1 THEN 'deposit ' ELSE 'withdrawal ' END + 'transactions' AS AACComparisonTypeDetail  			
		FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
		WHERE (RecvPay = 1)
		GROUP BY Q.Account, RecvPay, TolTotCheckWAmtMnth, TolNoOfCheckWMnth
		HAVING
		(
				( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is Not NULL ) AND (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is Not NULL ) 
				AND ( @monitorInd = 3 ) AND ( SUM(BaseAmt) > TolTotCheckWAmtMnth AND COUNT(TranNo) > TolNoOfCheckWMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt ) )
			OR
				( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is NOT NULL ) AND (SUM(BaseAmt) >= @minAggTransAmt) AND (SUM(BaseAmt) > TolTotCheckWAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
			OR
				( (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is NOT NULL ) AND (COUNT(TranNo) >= @minTransCount) AND (COUNT(TranNo) > TolNoOfCheckWMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
		)		
		UNION 
		SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
		  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
		  + CASE WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is Not NULL ) AND (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is Not NULL ) 
						AND ( @monitorInd = 3 ) AND ( SUM(BaseAmt) > TolTotCheckWAmtMnth AND COUNT(TranNo) > TolNoOfCheckWMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt ) )
					THEN 'check withdrawal transactions of $' + CONVERT(varchar, T.TolTotCheckWAmtMnth) + ' (includes ' + CONVERT(VARCHAR,  Convert(int, @amtTolerance)) + '% tolerance)'  
				      + @strExceed2 + ' check withdrawal transactions of ' + CONVERT(VARCHAR, T.TolNoOfCheckWMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
				 WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is NOT NULL ) AND (SUM(BaseAmt) >= @minAggTransAmt) AND (SUM(BaseAmt) > TolTotCheckWAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )			
					THEN 'check withdrawal transactions of $' + CONVERT(varchar, T.TolTotCheckWAmtMnth) + ' (includes ' + CONVERT(VARCHAR,  Convert(int, @amtTolerance)) + '% tolerance)' 
				 WHEN ( (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is NOT NULL ) AND (COUNT(TranNo) >= @minTransCount) AND (COUNT(TranNo) > TolNoOfCheckWMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN 'number of check withdrawal transactions of ' + CONVERT(VARCHAR, T.TolNoOfCheckWMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
			END + @strDueTo
		  + CASE WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is Not NULL ) AND (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is Not NULL ) 
						AND ( @monitorInd = 3 ) AND ( SUM(BaseAmt) > TolTotCheckWAmtMnth AND COUNT(TranNo) > TolNoOfCheckWMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' check withdrawal transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is NOT NULL ) AND (SUM(BaseAmt) >= @minAggTransAmt) AND (SUM(BaseAmt) > TolTotCheckWAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )			
					THEN 'the total check withdrawal transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is NOT NULL ) AND (COUNT(TranNo) >= @minTransCount) AND (COUNT(TranNo) > TolNoOfCheckWMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' check withdrawal transactions'
			END + @strBetweenDates
			, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
			, Case WHEN T.TolTotCheckWAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotCheckWAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
			, CASE WHEN T.TolNoOfCheckWMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfCheckWMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
			, ' check ' + Case WHEN Q.RecvPay = 1 THEN 'deposit ' ELSE 'withdrawal ' END + 'transactions' AS AACComparisonTypeDetail  			
		FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
		WHERE (RecvPay = 2)
		GROUP BY Q.Account, RecvPay, TolTotCheckWAmtMnth, TolNoOfCheckWMnth
		HAVING 
		(
				( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is Not NULL ) AND (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is Not NULL ) 
				AND ( @monitorInd = 3 ) AND ( SUM(BaseAmt) > TolTotCheckWAmtMnth AND COUNT(TranNo) > TolNoOfCheckWMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt ) )
			OR
				( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is NOT NULL ) AND (SUM(BaseAmt) >= @minAggTransAmt) AND (SUM(BaseAmt) > TolTotCheckWAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )			
			OR
				( (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is NOT NULL ) AND (COUNT(TranNo) >= @minTransCount) AND (COUNT(TranNo) > TolNoOfCheckWMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
		)		
	END
ELSE if @comparisonType =  4
	BEGIN
		Delete From @DSAACTolerated									 
		Where ( TolTotDepositAmtMnth Is NULL AND TolNoOfDepositMnth Is NULL )
			
		INSERT INTO @DSTT 
		SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
		  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
		  + CASE WHEN ( (( TolTotDepositAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotDepositAmtMnth <> 0.00 )) AND ( TolTotDepositAmtMnth Is Not NULL ) AND (( TolNoOfDepositMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDepositMnth <> 0 )) AND ( TolNoOfDepositMnth Is Not NULL ) 
						AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDepositAmtMnth AND COUNT(TranNo) > TolNoOfDepositMnth AND (COUNT(TranNo) >= @minTransCount) AND (SUM(BaseAmt) >= @minAggTransAmt) ) )
					THEN 'deposit transactions of $' + CONVERT(varchar, T.TolTotDepositAmtMnth) + ' (includes ' + CONVERT(VARCHAR,  Convert(int, @amtTolerance)) + '% tolerance)'  
				      + @strExceed2 + ' deposit transactions of ' + CONVERT(VARCHAR, T.TolNoOfDepositMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
				 WHEN ( (( TolTotDepositAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotDepositAmtMnth <> 0.00 )) AND ( TolTotDepositAmtMnth Is Not NULL ) AND (SUM(BaseAmt) > TolTotDepositAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'deposit transactions of $' + CONVERT(varchar, T.TolTotDepositAmtMnth) + ' (includes ' + CONVERT(VARCHAR,  Convert(int, @amtTolerance)) + '% tolerance)' 
				 WHEN ( (( TolNoOfDepositMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDepositMnth <> 0 )) AND ( TolNoOfDepositMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfDepositMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN 'number of deposit transactions of ' + CONVERT(VARCHAR, T.TolNoOfDepositMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
			END + @strDueTo
		  + CASE 
				 WHEN ( (( TolTotDepositAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotDepositAmtMnth <> 0.00 )) AND ( TolTotDepositAmtMnth Is Not NULL ) AND (( TolNoOfDepositMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDepositMnth <> 0 )) AND ( TolNoOfDepositMnth Is Not NULL ) 
						AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDepositAmtMnth AND COUNT(TranNo) > TolNoOfDepositMnth AND (COUNT(TranNo) >= @minTransCount) AND (SUM(BaseAmt) >= @minAggTransAmt) ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' deposit transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( TolTotDepositAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotDepositAmtMnth <> 0.00 )) AND ( TolTotDepositAmtMnth Is Not NULL ) AND (SUM(BaseAmt) > TolTotDepositAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'the total deposit transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				WHEN ( (( TolNoOfDepositMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDepositMnth <> 0 )) AND ( TolNoOfDepositMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfDepositMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' deposit transactions'
			END + @strBetweenDates
			, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
			, Case WHEN T.TolTotDepositAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotDepositAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
			, CASE WHEN T.TolNoOfDepositMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfDepositMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
			, ' ' + Case WHEN Q.RecvPay = 1 THEN 'deposit ' ELSE 'withdrawal ' END + 'transactions' AS AACComparisonTypeDetail  			
		FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
		WHERE (RecvPay = 1) 
		GROUP BY Q.Account, RecvPay, TolTotDepositAmtMnth, TolNoOfDepositMnth
		HAVING  
		(
				( (( TolTotDepositAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotDepositAmtMnth <> 0.00 )) AND ( TolTotDepositAmtMnth Is Not NULL ) AND (( TolNoOfDepositMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDepositMnth <> 0 )) AND ( TolNoOfDepositMnth Is Not NULL ) 
				AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDepositAmtMnth AND COUNT(TranNo) > TolNoOfDepositMnth AND (COUNT(TranNo) >= @minTransCount) AND (SUM(BaseAmt) >= @minAggTransAmt) ) )
			OR
				( (( TolTotDepositAmtMnth = 0.00 ) AND (@iZAV = 0) Or ( TolTotDepositAmtMnth <> 0.00 )) AND ( TolTotDepositAmtMnth Is Not NULL ) AND (SUM(BaseAmt) > TolTotDepositAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
			OR
				( (( TolNoOfDepositMnth = 0 ) AND (@iZAV = 0) Or ( TolNoOfDepositMnth <> 0 )) AND ( TolNoOfDepositMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfDepositMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
		)				   
	End

IF ( @testAlert = 1 ) OR ( @testAlert = 0 AND @WLTYPE = 0 )
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		INSERT INTO Alert ( 
				  WLCode
				,[Account]
				,[Cust]
				,[Desc]
				,Status
				,CreateDate
				,LastOper
				,LastModify
				,IsTest
		) 		 
		SELECT @WLCode
				, Account	
				, NULL
				, AACResultDesc 
				, 0
				, GETDATE()
				, Null
				, Null
				, @testAlert 
		FROM @DSTT 
		
		SELECT @STAT = @@ERROR	
		SELECT @ENDALRTDATE = GETDATE()
			
		if @STAT <> 0  GOTO ENDOFPROC
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	DISTINCT 'Alert', AlertNo, Q.TRANNO
			FROM  @DSQA Q INNER JOIN @DSTT T ON Q.Account = T.Account INNER JOIN 
                  Alert A ON T.Account = A.Account
			WHERE	T.Account = A.Account AND A.WLCode = @WLCode 
			AND 	A.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		SELECT @STAT = @@ERROR
	END 
ELSE
IF 	( @testAlert = 0 ) AND ( @WLTYPE = 1 )
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		--SET StartDate and EndDate with @fromDate and @toDate in Date format
		
		INSERT INTo SuspiciousActivity (
				ProfileNo, BookDate, Cust, Account, Activity
				, SuspType, StartDate, EndDate, RecurType, RecurValue
				, ActCurrReportAmt, ActInActCnt, ActOutActCnt, ActInActAmt, ActOutActAmt
				, CurrReportAmt, ExpAvgInActCnt, ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt
				, InCntTolPerc, OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr
				, ReviewState, ReviewTime, ReviewOper, App, AppTime
				, AppOper , WLCode, WLDesc, CreateTime )
		SELECT	Null, dbo.ConvertSqlDateToInt(GETDATE()), Null, Account, Null
			, 'Rule', CONVERT(date, CONVERT(varchar(8), @fromDate), 112), CONVERT(date, CONVERT(varchar(8), @toDate), 112), Null, Null
			, Null, Null, Null, Null, Null
			, Null, Null, Null, Null, Null
			, 0, 0, 0, 0, Null
			, Null, Null, Null, 0, Null
			, Null, @WLCode, AACResultDesc ,GETDATE()
		FROM  @DSTT
		
		SELECT @ENDALRTDATE = GETDATE()
		SELECT @STAT = @@ERROR	

		if @STAT <> 0  GOTO ENDOFPROC
	
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	DISTINCT 'SUSPACT',A.RecNo,  Q.TRANNO
			FROM  @DSQA Q INNER JOIN @DSTT T ON Q.Account = T.Account INNER JOIN 
				  SuspiciousActivity A ON T.Account = A.Account
			WHERE	T.Account = A.Account AND A.WLCode = @WLCode 
			AND 	A.CreateTime BETWEEN @STARTALRTDATE AND @ENDALRTDATE

		SELECT @STAT = @@ERROR		
		IF @STAT <> 0 GOTO ENDOFPROC	
	END	

--------   End of ALERTs or CASEs SQL Script Codes Above -------------------

ENDOFPROC:
IF (@stat <> 0) BEGIN  
  ROLLBACK TRAN USR_AntpActvComps
  RETURN @stat
END	

IF @trnCnt = 0 
  COMMIT TRAN USR_AntpActvComps
  RETURN @stat

Go  

if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_SameBeneByOrd]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure USR_SameBeneByOrd
Go
CREATE PROCEDURE USR_SameBeneByOrd (@WLCode SCode, @testAlert INT, 
        @activityTypes VARCHAR(8000), @branches VARCHAR(8000), 
        @depts VARCHAR(8000), @region SCode, @minimumAmount MONEY, 
        @maximumAmount MONEY, @minimumCount INT, @minimumTotalAmount MONEY, 
        @days INT, @weekends VARCHAR(8000))
AS

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@MINDATE INT
DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME

DECLARE @TransTbl TABLE (
	TranNo INT,
	Bene VARCHAR(40),
	ByOrder VARCHAR(40),
	BookDate INT,
	BaseAmt MONEY,
	Cust VARCHAR(40),
	OwnerBranch VARCHAR(11),
	OwnerDept VARCHAR(11)
)
DECLARE	@TT TABLE (
	Bene VARCHAR(40),
	ByOrder VARCHAR(40),
	tranAmt money,
	tranCnt INT,
	descr VARCHAR(2000),
	CustCount INT,
	BranchCount INT,
	DeptCount INT,
	Cust VARCHAR(40),
	OwnerBranch VARCHAR(11),
	OwnerDept VARCHAR(11)
)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_SameBeneByOrd
ELSE
	-- Already in a transaction
	SAVE TRAN USR_SameBeneByOrd
/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @description = [Desc], @WLType = WLType  
FROM WatchList WITH (NOLOCK) WHERE WLCode = @WLCode

SET @minDate = DBO.CONVERTSQLDATETOINT(DATEADD(day, -1 * dbo.calcWeekendRange(
 @days, ',' + @weekends, 
 (datepart(weekday, getdate()) + @@DATEFIRST - 1) % 7 + 1) + 1, GETDATE()))
INSERT INTO @TransTbl(TranNo, Bene, ByOrder, BookDate, BaseAmt, Cust, OwnerBranch, OwnerDept)
SELECT TranNo, Bene, ByOrder, BookDate, BaseAmt, Cust, Branch, Dept
FROM Activity a WITH (NOLOCK)
WHERE (BaseAmt BETWEEN @minimumAmount AND @maximumAmount OR
       (@maximumAmount = 0 AND BaseAmt >= @minimumAmount)) AND
      (Bene IS NOT NULL OR ByOrder IS NOT NULL) AND
      BookDate >= @minDate AND (IsNull(@activityTypes, '') = '' OR 
      CHARINDEX(',' + CONVERT(VARCHAR, type) + ',', 
       ',' + @activityTypes + ',') > 0) AND
      (IsNull(@branches, '') = '' OR 
      CHARINDEX(',' + CONVERT(VARCHAR, branch) + ',', 
       ',' + @branches + ',') > 0) AND 
      (IsNull(@depts, '') = '' OR 
      CHARINDEX(',' + CONVERT(VARCHAR, dept) + ',', ',' + @depts + ',') > 0)
      AND (IsNull(@region, '') = '' OR EXISTS (
        SELECT * FROM Country WHERE region = @region AND
        (Code = a.BeneCountry OR Code = a.BeneBankCountry OR 
         Code = a.IntermediaryCountry OR Code = a.ByOrderCountry OR
         Code = a.ByOrderBankCountry OR Code = a.IntermediaryCountry2 OR 
         Code = a.IntermediaryCountry3 OR Code = a.IntermediaryCountry4)
      ))
IF @@ROWCOUNT = 0 GOTO EndOfProc
INSERT INTO @TransTbl(TranNo, Bene, ByOrder, BookDate, BaseAmt, Cust, OwnerBranch, OwnerDept)
SELECT TranNo, Bene, ByOrder, BookDate, BaseAmt, Cust, Branch, Dept
FROM ActivityHist a WITH (NOLOCK)
WHERE (BaseAmt BETWEEN @minimumAmount AND @maximumAmount OR
       (@maximumAmount = 0 AND BaseAmt >= @minimumAmount)) AND
      BookDate >= @minDate AND (IsNull(@activityTypes, '') = '' OR 
      CHARINDEX(',' + CONVERT(VARCHAR, type) + ',', 
       ',' + @activityTypes + ',') > 0) AND
      (IsNull(@branches, '') = '' OR 
      CHARINDEX(',' + CONVERT(VARCHAR, branch) + ',', 
       ',' + @branches + ',') > 0) AND 
      (IsNull(@depts, '') = '' OR 
      CHARINDEX(',' + CONVERT(VARCHAR, dept) + ',', ',' + @depts + ',') > 0)
      AND (bene in (SELECT bene FROM @TransTbl) 
       OR ByOrder in (SELECT ByOrder FROM @TransTbl))
      AND (IsNull(@region, '') = '' OR EXISTS (
        SELECT * FROM Country WHERE region = @region AND
        (Code = a.BeneCountry OR Code = a.BeneBankCountry OR 
         Code = a.IntermediaryCountry OR Code = a.ByOrderCountry OR
         Code = a.ByOrderBankCountry OR Code = a.IntermediaryCountry2 OR 
         Code = a.IntermediaryCountry3 OR Code = a.IntermediaryCountry4)
      ))
INSERT INTO @TT(Bene, ByOrder, tranAmt, tranCnt, descr, CustCount, BranchCount, DeptCount)
SELECT Bene, NULL, SUM(BaseAmt) tranAmt, COUNT(TranNo) tranCnt,
'Beneficiary: ''' + Bene + '''  received amount: ''' 
                  + CONVERT(VARCHAR, SUM(BaseAmt)) 
		  + ''' over ''' 
		  + CONVERT(VARCHAR, COUNT(TranNo)) +  ''''
                  + ' transactions in the region ''' + @region
                  + ''' in the past ' + CONVERT(VARCHAR, @days)
                  + ' days.' descr, Count(Distinct Cust), Count(Distinct OwnerBranch), Count(Distinct OwnerDept)
FROM @TransTbl
WHERE Bene IS NOT NULL
GROUP BY Bene
HAVING (SUM(BaseAmt) >= @minimumTotalAmount AND 
	count(tranno) >= @minimumCount)
UNION

SELECT NULL, ByOrder, SUM(BaseAmt) tranAmt, COUNT(TranNo) tranCnt,
'ByOrder: ''' + ByOrder + '''  sent amount: ''' 
                  + CONVERT(VARCHAR, SUM(BaseAmt)) 
		  + ''' over ''' 
		  + CONVERT(VARCHAR, COUNT(TranNo)) +  ''''
                  + ' transactions in the region ''' + @region
                  + ''' in the past ' + CONVERT(VARCHAR, @days)
                  + ' days.' descr, Count(Distinct Cust), Count(Distinct OwnerBranch), Count(Distinct OwnerDept)
FROM @TransTbl
WHERE ByOrder IS NOT NULL
GROUP BY ByOrder
HAVING (SUM(BaseAmt) >= @minimumTotalAmount AND 
	count(tranno) >= @minimumCount)

Update tt Set Cust = trn.Cust
From @tt tt Join @TransTbl trn 
On (tt.Bene = trn.Bene And tt.ByOrder Is Null) 
Or (tt.ByOrder = trn.ByOrder And tt.Bene Is Null) 
Where CustCount = 1

Update tt Set OwnerBranch = trn.OwnerBranch
From @tt tt Join @TransTbl trn 
On (tt.Bene = trn.Bene And tt.ByOrder Is Null) 
Or (tt.ByOrder = trn.ByOrder And tt.Bene Is Null) 
Where CustCount > 1 AND BranchCount = 1

Update tt Set OwnerDept = trn.OwnerDept
From @tt tt Join @TransTbl trn 
On (tt.Bene = trn.Bene And tt.ByOrder Is Null) 
Or (tt.ByOrder = trn.ByOrder And tt.Bene Is Null) 
Where CustCount > 1 AND DeptCount = 1

IF @WLTYPE = 0 OR @testAlert = 1 BEGIN
	SELECT @STARTALRTDATE = GETDATE()
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT, IsTest) 
          SELECT @WLCode, descr, 0, GETDATE(), NULL, NULL, Cust, NULL, 
                 @testAlert
	  FROM @TT 
	SELECT @STAT = @@ERROR	
	SELECT @ENDALRTDATE = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	  SELECT 'Alert', AlertNO, TRANNO FROM @TransTbl a, @TT t, Alert 
          WHERE Alert.[desc] = t.descr
           AND ((ISNULL(a.Bene, '') <> '' AND a.Bene = t.bene and charindex('Beneficiary:', Alert.[desc])>0
                  AND substring(Alert.[desc], 
                        charindex('Beneficiary:', Alert.[desc]) + len('Beneficiary:')+2, 
                        charindex('received amount', Alert.[desc]) - (charindex('Beneficiary:', Alert.[desc]) + len('Beneficiary:')+5)) = a.bene) 
				  OR
               (ISNULL(a.ByOrder, '') <> '' AND a.ByOrder = t.byOrder and charindex('Byorder:', Alert.[desc])>0
                  AND substring(Alert.[desc], 
                        charindex('Byorder:', Alert.[desc]) + len('byorder:')+2, 
                        charindex('sent amount', Alert.[desc]) - (charindex('Byorder:', Alert.[desc]) + len('byorder:')+5)) = a.ByOrder))
           AND Alert.WLCode = @WLCode
           AND Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
	
	SELECT @STAT = @@ERROR 

END ELSE IF @WLTYPE = 1 BEGIN
	SELECT @STARTALRTDATE = GETDATE()
	INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
		ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
		RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
		ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
		EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
		OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
		REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
		WLCode, WLDESC, CREATETIME, OwnerBranch, OwnerDept )
        SELECT  NULL, DBO.CONVERTSQLDATETOINT(GETDATE()), Cust, NULL,
		NULL, 'RULE', NULL, NULL, NULL, NULL,
		NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
		NULL, NULL, 0, 0, 0, 0, 
		NULL, NULL, NULL, NULL, 0, NULL, NULL,
		@WLCode, descr, GETDATE(), OwnerBranch, OwnerDept
	FROM @TT 
	SELECT @STAT = @@ERROR	
	SELECT @ENDALRTDATE = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC


	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	  SELECT 'SUSPACT', RECNO, TRANNO 
          FROM @TransTbl a, @TT t, SUSPICIOUSACTIVITY S 
	  WHERE S.wlDesc = t.descr
          AND ((ISNULL(a.Bene, '') <> '' and charindex('Beneficiary:', S.wldesc)>0
                  AND substring(S.wldesc, 
                        charindex('Beneficiary:', S.wldesc) + len('Beneficiary:')+2, 
                        charindex('received amount', S.wldesc) - (charindex('Beneficiary:', S.wldesc) + len('Beneficiary:')+5)) = a.bene) 
				  OR
               (ISNULL(a.ByOrder, '') <> '' and charindex('Byorder:', S.wldesc)>0
                  AND substring(S.wldesc, 
                        charindex('Byorder:', S.wldesc) + len('byorder:')+2, 
                        charindex('sent amount', S.wldesc) - (charindex('Byorder:', S.wldesc) + len('byorder:')+5)) = a.ByOrder))
           AND S.WLCode = @WLCode
           AND S.CREATETIME BETWEEN @STARTALRTDATE AND @ENDALRTDATE
	SELECT @STAT = @@ERROR 

END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_SameBeneByOrd
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_SameBeneByOrd
RETURN @stat
Go


--- Branch data for Bene Transactions

IF EXISTS (select * from sysobjects 
	where id = object_id(N'[dbo].[BSA_ActivityBranchesForCase]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [DBO].[BSA_ActivityBranchesForCase]
    
GO

Create PROCEDURE dbo.BSA_ActivityBranchesForCase(@ObjectID 	sCode)
With Encryption as

    Declare @CaseType varchar(11)
    Declare @Branch varchar(11)
    Declare @stat int
	Declare @paytran Int

    Select @caseType = SuspType 
	From SuspiciousActivity with (nolock)	
	Where recno =@ObjectID 

	set @paytran = 0
	If Exists (Select * from SuspiciousActivity with (nolock) where RecNo=@ObjectID )
		SELECT @Paytran=CASE When ActInActCnt>0 Then 1 Else 2 End
		FROM SuspiciousActivity with (nolock)
		WHERE recno = @ObjectID
	Else 
		SELECT @Paytran=CASE When ActInActCnt>0 Then 1 Else 2 End
		FROM SuspiciousActivityHist with (nolock)	
		WHERE recno = @ObjectID


    If   @caseType <> 'ExcCurrency'
    Begin 
		If Exists(Select Top 1 a.Branch
		From dbo.SAsActivity sas with (nolock)
		Join dbo.Activity a with (nolock) on sas.TranNo=a.TranNo
		Where sas.objectID=@ObjectID 
		and sas.Included=1 and upper(sas.objecttype) = 'SUSPACT')

		Select distinct a.Branch, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		From dbo.SAsActivity sas with (nolock)
		Join dbo.Activity a with (nolock) on sas.TranNo=a.TranNo
		Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch
		Where sas.objectID=@ObjectID 
		and sas.Included=1 and upper(sas.objecttype) = 'SUSPACT'
        Order By a.BookDate

        Else
			Select distinct ah.Branch, ah.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			From dbo.SAsActivity sas with (nolock)
			Join dbo.ActivityHist ah with (nolock) on sas.TranNo=ah.TranNo
			Join PSEC.dbo.Branch b with (nolock) on b.Code = ah.Branch
			Where sas.objectID=@ObjectID 
			and sas.Included=1 and upper(sas.objecttype) = 'SUSPACT'
			Order By ah.BookDate

        Select @stat = @@error
        Return @stat
	End

    If (Select Enabled From OptionTbl where code='BeneCTR')=1
    Begin 
		--Exceed currency. Trans not is sasactivity.
		If exists(Select Top 1 a.Branch
			From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
			on case when a.BeneCustID is NULL Then a.Cust Else a.BeneCustID End = sa.Cust
			AND a.BookDate = sa.BookDate
			Where sa.recno = @objecTID)

			Select distinct a.Branch, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
			on case when a.BeneCustID is NULL Then a.Cust Else a.BeneCustID End = sa.Cust AND a.BookDate = sa.BookDate 
			and (@paytran = a.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch
			and a.CashTran = 1
			Where sa.recno = @objecTID
			Order By a.BookDate

		Else
			Select distinct ah.Branch, ah.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		    From SuspiciousActivity sa  with (nolock) Join activityhist ah with (nolock)
		    on case when ah.BeneCustID is NULL Then ah.Cust Else ah.BeneCustID End = sa.Cust AND ah.BookDate = sa.BookDate 
			and (@paytran = ah.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = ah.Branch		    
			and ah.CashTran = 1
		    Where sa.recno = @OBJECTID
		    Order By ah.BookDate
    End
    Else
    Begin
		--Exceed currency. Trans not is sasactivity.
		If exists(Select Top 1 a.Branch
			From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
			on a.Cust = sa.Cust AND a.BookDate = sa.BookDate
			Where sa.recno = @objecTID)

			Select distinct a.Branch, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
			on a.Cust = sa.Cust AND a.BookDate = sa.BookDate 
			and (@paytran = a.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch		    
			and a.CashTran = 1
			Where sa.recno = @objecTID
			Order By a.BookDate

		Else
			Select distinct ah.Branch, ah.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		    From SuspiciousActivity sa  with (nolock) Join activityhist ah with (nolock)
		    on ah.Cust = sa.Cust AND ah.BookDate = sa.BookDate 
			and (@paytran = ah.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = ah.Branch		    
			and ah.CashTran = 1
		    Where sa.recno = @OBJECTID
		    Order By ah.BookDate
    End
							
	 Select @stat = @@error
     Return @stat	 

GO

--- Branch data for Bene Transactions
IF EXISTS (select * from sysobjects 
	where id = object_id(N'[dbo].[BSA_ActivityBranchForCase]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [DBO].[BSA_ActivityBranchForCase]
    
    
GO
Create PROCEDURE dbo.BSA_ActivityBranchForCase(@ObjectID 	sCode)
With Encryption as

    Declare @CaseType varchar(11)
    Declare @Branch varchar(11)
    Declare @stat int

	Set @Branch=''
    Select @caseType = SuspType 
	From SuspiciousActivity 	
	Where recno =@ObjectID 

    If   @caseType <> 'ExcCurrency'
    Begin 
		Select Top 1 @Branch=a.Branch
		From dbo.SAsActivity sas
		Join dbo.Activity a on sas.TranNo=a.TranNo
		Where sas.objectID=@ObjectID 
		and Included=1 and upper(sas.objecttype) = 'SUSPACT'
        Order By a.BookDate

        If @Branch=''
			Select Top 1 @Branch=ah.Branch
			From dbo.SAsActivity sas
			Join dbo.ActivityHist ah on sas.TranNo=ah.TranNo
			Where sas.objectID=@ObjectID 
			and Included=1 and upper(sas.objecttype) = 'SUSPACT'
			Order By ah.BookDate

        Select @stat = @@error
        Select @Branch AS Branch
        Return @stat
	End

    If (Select Enabled From OptionTbl where code='BeneCTR')=1
    Begin 
		--Exceed currency. Trans not is sasactivity.
			Select Top 1 @Branch=a.Branch
			From SuspiciousActivity sa  Join activityHist a
			on case when a.BeneCustID is NULL Then a.Cust Else a.BeneCustID End = sa.Cust AND a.BookDate = sa.BookDate 
			and a.CashTran = 1
			Where sa.recno = @objecTID
			Order By a.BookDate

		If @Branch = ''
			Select Top 1 @Branch=a.Branch
		    From SuspiciousActivity sa  Join activity a
		    on case when a.BeneCustID is NULL Then a.Cust Else a.BeneCustID End = sa.Cust AND a.BookDate = sa.BookDate 
		    and a.CashTran = 1
		    Where sa.recno = @OBJECTID
		    Order By a.BookDate
    End
    Else
    Begin
		--Exceed currency. Trans not is sasactivity.
			Select Top 1 @Branch=a.Branch
			From SuspiciousActivity sa  Join activityHist a
			on a.Cust = sa.Cust AND a.BookDate = sa.BookDate 
			and a.CashTran = 1
			Where sa.recno = @objecTID
			Order By a.BookDate

		If @Branch = ''
			Select Top 1 @Branch=a.Branch
		    From SuspiciousActivity sa  Join activity a
		    on a.Cust = sa.Cust AND a.BookDate = sa.BookDate 
		    and a.CashTran = 1
		    Where sa.recno = @OBJECTID
		    Order By a.BookDate
    End
							
	 Select @stat = @@error
     Select @Branch as Branch
     Return @stat	 
GO
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_HighRiskCountry]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.USR_HighRiskCountry
Go
CREATE PROCEDURE dbo.USR_HighRiskCountry(@WLCode SCode, @testAlert INT,
	@period varchar(25),
	@startDate varchar(10),
	@endDate varchar(10),  
    @minIndCount int,
	@minIndAmt money, 
    @maxIndAmt money,
	@minSumAmount money,
	@maxSumAmount money,
	@riskClassList varchar(8000),
	@customerTypeList varchar(8000),
	@accountTypeList varchar(8000),
	@activityTypeList varchar(8000),
    @countryList varchar(8000),
	@UseMonitor smallint,
	@recvpay INT,
	@branchList varchar(8000),
    @deptList varchar(8000),
	@cashType smallint,
	@ExcludeTranbyCustCtryCode smallint,
	@IncludeAddressFields smallint,
	@Useround smallInt,
	@Precision smallInt
	)

AS
/* RULE AND PARAMETER DESCRIPTION
Detects a single Originator sending transactions to one or more 
Beneficiaries. A transaction is included in the evaluation based on its 
qualification with: specified transaction types, a specified time period,
the amount exceeds a certain amount, and the cumulative amount of all the
transactions meeting these constraints is over a specified amount.  
Additional constraints include Country, Customer Type, Risk Class, 
Account Types, Cash/Non-Cash, Branch and Department. The alert/case
will display the account information in the description since the account
may not be held at the processing institution. Designed to be run Post-EOD.

	@period = Specifies the previous period accordingly. It Can be left blank,
		Week, Month, Quarter and Semi Annual. If blank the StartDate and EndDate 
  		must be specified. 
	@startDate = Specifies the Starting Date range of transactions to evaluate,  
    		uses the book date. Relevant only if the Period is not specified.
	@endDate = Specifies the Ending Date range of the transaction's book date.
		Relevant only if the Period is not specified.
        @minIndCount = The Minimum individual transaction count, after applying 
		the Min-Ind-Amt and Max-Ind-Amt parameters
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
   	@maxIndAmt = The Maximum amount the individual transaction cannot exceed 
		to be included in the aggregation.  For no maximum use -1
	@minSumAmount = The Minimum aggregated transaction amount that must be 
		exceeded.  This includes those transactions that are between Min-Ind-Amt
		and Max-Ind-Amt.
	@maxSumAmount = The Maximum aggregated transaction amount that cannot be 
		exceeded. For no maximum use -1
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@customerTypeList = A comma separated list of Customer Types to include 
		in the evaluation, use -ALL- for any customer type.
	@accountTypeList = A comma separated list of Account Types to include 
		in the evaluation, use -ALL- for any account type.
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.
        @countryList = A comma separated list of Countries to include in the 
		evaluation, use -ALL- for any country. Fields evaluated are 
		BeneCountry, BeneBankCountry, ByOrderCountry, ByOrderBankCountry, 
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3 and
		IntermediaryCountry4
	@UseMonitor = Use list of countries from Country table where the "Monitor" flag 
		is set to true instead of country list above (overrides country list).
	@recvpay = Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both
	@branchList = A comma separated list of Branches to include in the evaluation.
	@deptList = A comma separated list of Departments to include in 
		the evaluation.
	@cashType = Specifies Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both
	@ExcludeTranbyCustCtryCode = Exclude all the transactions that produce hit when 
		the country on the transaction is the same as the country of the customer.
		0 for Do not exclude, 
		1 for Exclude if customer country matches, 
		2 for Exclude if any customer country matches" Value="0"/>
	@IncludeAddressFields = Include Address Fields in search to match on country.
					0 - for Do not Include
					1 - for Include
	@UseRound = Indicate if only the high round amounts have to considered.
					0 - Do not round
					1 - Round
	@Precision = Precision for rounding. Specify 3 for 1000
*/
	
/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@fromDate INT,
 	@toDate INT,
	@tranAmt MONEY,
	@tranCnt INT,
	@trnCnt INT
DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME

DECLARE @SetDate DATETIME
DECLARE @Countries varchar(2000)
DECLARE @OrigCountryList varchar(8000)
DECLARE @Accounts varchar(2000)
DECLARE @Country VARCHAR(200)
DECLARE @pos INT
DECLARE @tmp INT

set @tmp = power(10, @precision)

Declare @TT TABLE(
	BaseAmt MONEY,
	tranNo int,
	Cust VARCHAR(40),
	Account VARCHAR(40),
	benecountry VARCHAR(35), 
	beneBankcountry VARCHAR(35), 
	ByOrderCountry VARCHAR(35), 
	ByOrderBankCountry VARCHAR(35),
	IntermediaryCountry VARCHAR(35),
	IntermediaryCountry2 VARCHAR(35),
	IntermediaryCountry3 VARCHAR(35),
	IntermediaryCountry4 VARCHAR(35)	
)

Declare @TempAddrTable TABLE(
	BaseAmt MONEY,
	tranNo int,
	Cust VARCHAR(40),
	Account VARCHAR(40),
	benecountry VARCHAR(35), 
	beneBankcountry VARCHAR(35), 
	ByOrderCountry VARCHAR(35), 
	ByOrderBankCountry VARCHAR(35),
	IntermediaryCountry VARCHAR(35),
	IntermediaryCountry2 VARCHAR(35),
	IntermediaryCountry3 VARCHAR(35),
	IntermediaryCountry4 VARCHAR(35)	,
	BeneAddress	VARCHAR(120),
	BeneBankAddress VARCHAR(120),
	IntermediaryAddress VARCHAR(120),
	ByOrderAddress VARCHAR(120),
	ByOrderBankAddress VARCHAR(120),
	IntermediaryAddress2 VARCHAR(120),
	IntermediaryAddress3 VARCHAR(120),
	IntermediaryAddress4 VARCHAR(120)
)

Declare @TT1 TABLE(
	Cust VARCHAR(40),
	Account VARCHAR(40),
	country VARCHAR(35)	
)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_HighRiskCountry
ELSE
	-- Already in a transaction
	SAVE TRAN USR_HighRiskCountry
/* End standard stored procedure transaction header */

/*  standard Rules Header */
-- Date options

--SET @SetDate = GetDate()

-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @description = [Desc], @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList (NOLOCK)
WHERE WLCode = @WLCode

Declare @BaseCurr char(3)
select @BaseCurr = IsNULL(BaseCurr,'') from SysParam

If(@period is null OR  ltrim(rtrim(@period)) = '') 
BEGIN
	SET @fromDate = dbo.ConvertSqlDateToInt(@startDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@endDate)
END
ELSE
BEGIN
	DECLARE @SQLStartDate datetime, @SQLEndDate datetime
	exec dbo.BSA_GetDateRange @SetDate, @period, 'PREV', 1, 
			@SQLStartDate OUTPUT, @SQLEndDate OUTPUT
	SET @fromDate = dbo.ConvertSqlDateToInt(@SQLStartDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@SQLEndDate)
END

	SELECT @riskClassList = dbo.BSA_fnListParams(@riskClassList)
	SELECT @customerTypeList = dbo.BSA_fnListParams(@customerTypeList)
	SELECT @accountTypeList = dbo.BSA_fnListParams(@accountTypeList)
	SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)
	SELECT @OrigCountryList = @countryList
	SELECT @countryList = dbo.BSA_fnListParams(@countryList)
	IF @UseMonitor = 1
	BEGIN
		SET @countryList = ''
		SELECT @countryList = COALESCE(@countryList + ',', '') 
				+ rtrim(code)
 			FROM Country 
			WHERE Monitor = 1

		IF LEN(LTRIM(RTRIM(@countryList)))> 0
			SELECT @countryList=@countryList+','
		ELSE
			SELECT @countryList = ''
	END

	SELECT @branchList = dbo.BSA_fnListParams(@branchList)
	SELECT @deptList = dbo.BSA_fnListParams(@deptList)

IF (@cashType = 2)
	SET @cashType = NULL

IF (@recvpay = 3)
	SET @recvpay = NULL
-- To include Address Fields in the search criteria


IF (@IncludeAddressFields) = 1
BEGIN
	SET @pos = 1  
	SET @Country = ''
		WHILE (@pos <  LEN(@countryList)   OR  ISNULL(@countrylist,'') = '') 
        BEGIN			
			IF (LEN(ISNULL(@countryList,'')) > 0)
			BEGIN
				SELECT @Country = LTRIM(RTRIM(SUBSTRING(@countryList,@pos,CHARINDEX(',',@countryList, @pos)- @pos)))			
			END 
			IF (LEN(LTRIM(RTRIM(@Country))) >0 Or ISNULL(@countrylist,'') = '')  
				INSERT INTO @TempAddrTable (BaseAmt, tranNo, Cust, Account, benecountry,
					beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
					IntermediaryCountry, IntermediaryCountry2, 
					IntermediaryCountry3, IntermediaryCountry4,BeneAddress,
					BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
					IntermediaryAddress2,IntermediaryAddress3 ,
					IntermediaryAddress4)
				SELECT BaseAmt, tranNo, a.Cust, a.account,
				benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
				IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
				IntermediaryCountry4,BeneAddress,BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
				IntermediaryAddress2,IntermediaryAddress3 ,IntermediaryAddress4
				FROM ActivityHist a (NOLOCK) JOIN Customer (NOLOCK) ON a.cust = Customer.Id
				LEFT JOIN Account ac (NOLOCK) ON a.Account = ac.Id 
				WHERE a.bookdate >= @fromDate AND 
					a.bookdate <= @toDate AND 
					a.recvpay = ISNULL(@recvPay, a.recvpay) AND
					a.BaseAmt >= @minIndAmt AND
					(@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
					(ISNULL(@deptList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.dept,''))) + ',', @deptList) > 0)
					AND (ISNULL(@branchList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.branch,''))) + ',', @branchList) > 0)
					AND (ISNULL(@activityTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.type,''))) + ',', @activityTypeList) > 0)
					AND (ISNULL(@accountTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ac.type,''))) + ',', @accountTypeList) > 0)
					AND (ISNULL(@riskClassList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Customer.RiskClass,''))) + ',', @riskClassList) > 0)
					AND	(ISNULL(@customerTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Customer.type,''))) + ',', @customerTypeList) > 0)
					AND	(CASHTRAN = ISNULL(@cashType, CASHTRAN))
					AND 1 = 
					Case When @UseRound = 1 and (a.baseamt%@tmp = 0) Then 1 
					     When @UseRound = 0 Then 1
					     When @UseRound = 1 and (a.baseamt%@tmp <> 0) Then 0
					Else 1 End
					AND 
					( 
						(ISNULL(@countryList,'') = '') 	
						OR 
 							( CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.BeneAddress) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.BeneBankAddress) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.IntermediaryAddress) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))), a.ByOrderAddress) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.ByOrderBankAddress) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.IntermediaryAddress2) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.IntermediaryAddress3) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) ,a.IntermediaryAddress4 ) > 0 
							)
						OR
							(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.BeneCountry,''))) + ',', @countryList) > 0 OR 
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.BeneBankCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.ByOrderCountry,''))) + ',', @countryList) > 0  OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry4,''))) + ',', @countryList) > 0 
							) 
					)
					
					IF ISNULL(@countrylist,'') = '' 
						BREAK
					ELSE 
						BEGIN
 						-- Get the next country code	
 						SELECT @pos = CHARINDEX(',',@countryList, @pos)+1
						END
        END
END
-- do not include address fields in the search criteria
ELSE
BEGIN
	INSERT INTO @TempAddrTable (BaseAmt, tranNo, Cust, Account, benecountry,
			beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
			IntermediaryCountry, IntermediaryCountry2, 
			IntermediaryCountry3, IntermediaryCountry4,BeneAddress,
			BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
			IntermediaryAddress2,IntermediaryAddress3 ,
			IntermediaryAddress4)	
		SELECT BaseAmt, tranNo, a.Cust, a.account,
		benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
		IntermediaryCountry4,BeneAddress,BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
		IntermediaryAddress2,IntermediaryAddress3 ,IntermediaryAddress4
	FROM ActivityHist a (NOLOCK) JOIN Customer (NOLOCK) on a.cust = Customer.Id
     LEFT JOIN Account ac (NOLOCK) ON a.Account = ac.Id 
      WHERE a.bookdate >= @fromDate AND 
		a.bookdate <= @toDate AND 
		a.recvpay = ISNULL(@recvPay, a.recvpay) AND
		a.BaseAmt >= @minIndAmt AND
	    	(@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
		(ISNULL(@deptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.dept,''))) + ',', @deptList) > 0)
		AND (ISNULL(@branchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.branch,''))) + ',', @branchList) > 0)
		AND (ISNULL(@activityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.type,''))) + ',', @activityTypeList) > 0)
		AND (ISNULL(@accountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ac.type,''))) + ',', @accountTypeList) > 0)
		AND (ISNULL(@riskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Customer.RiskClass,''))) + ',', @riskClassList) > 0)
		AND	(ISNULL(@customerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Customer.type,''))) + ',', @customerTypeList) > 0)
		AND	(CASHTRAN = ISNULL(@cashType, CASHTRAN))
		AND(ISNULL(@countryList,'') = '' OR 
		(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.BeneCountry,''))) + ',', @countryList) > 0 OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.BeneBankCountry,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.ByOrderCountry,''))) + ',', @countryList) > 0  OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry4,''))) + ',', @countryList) > 0 ))
		AND 1 = 
		Case When @UseRound = 1 and (a.baseamt%@tmp = 0) Then 1 
		     When @UseRound = 0 Then 1
		     When @UseRound = 1 and (a.baseamt%@tmp <> 0) Then 0
		Else 1 End
		
END


IF (@ExcludeTranbyCustCtryCode = 0)
BEGIN
	INSERT INTO @TT (BaseAmt, tranNo, Cust, Account, benecountry,
			beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
			IntermediaryCountry, IntermediaryCountry2, 
			IntermediaryCountry3, IntermediaryCountry4)	
		SELECT DISTINCT BaseAmt, tranNo, a.Cust, a.account, 
		benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
		IntermediaryCountry4
FROM @TempAddrTable a INNER JOIN Customer C ON A.Cust = C.ID		
END
ELSE IF @ExcludeTranbyCustCtryCode = 1
BEGIN
	INSERT INTO @TT (BaseAmt, tranNo, Cust, Account, benecountry,
			beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
			IntermediaryCountry, IntermediaryCountry2, 
			IntermediaryCountry3, IntermediaryCountry4)	
		SELECT DISTINCT BaseAmt, tranNo, a.Cust, a.account, 
		benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
		IntermediaryCountry4
	FROM @TempAddrTable A INNER JOIN Customer C ON A.Cust = C.ID
	WHERE (ISNULL(@countryList, '' ) = '' OR 
		(	C.Country is NULL OR a.BeneCountry <> C.Country ) OR
		( C.Country is NULL OR a.BeneBankCountry <> C.Country ) OR
		( C.Country is NULL OR a.ByOrderCountry <> C.Country ) OR
		(C.Country is NULL OR a.ByOrderBankCountry <> C.Country )OR
		( C.Country is NULL OR a.IntermediaryCountry <> C.Country ) OR
		( C.Country is NULL OR a.IntermediaryCountry2 <> C.Country ) OR
		( C.Country is NULL OR a.IntermediaryCountry3 <> C.Country ) OR
		(C.Country is NULL OR a.IntermediaryCountry4 <> C.Country ))
		
END
ELSE IF @ExcludeTranbyCustCtryCode = 2
BEGIN
	INSERT INTO @TT (BaseAmt, tranNo, Cust, Account, benecountry,
			beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
			IntermediaryCountry, IntermediaryCountry2, 
			IntermediaryCountry3, IntermediaryCountry4)	
		SELECT DISTINCT BaseAmt, tranNo, a.Cust, a.account, 
		benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
		IntermediaryCountry4
	FROM @TempAddrTable A INNER JOIN Customer C ON A.Cust = C.ID
	LEFT OUTER JOIN PartyAddress p on a.cust = p.CustomerId
		WHERE (ISNULL(@countryList,'') = '' OR 
		( 		((a.BeneCountry <> c.Country OR c.Country is NULL) 	
				AND (c.CountryOfResidence is NULL OR a.BeneCountry <> c.CountryOfResidence )
				AND (c.CountryOfIncORp is NULL OR a.BeneCountry <> c.CountryOfIncORp )
				AND (c.CountryOfORigin is NULL OR a.BeneCountry <> c.CountryOfORigin  )
				AND (c.CountryOfCitizenship is NULL OR a.BeneCountry <> c.CountryOfCitizenship )
				AND (p.Country is NULL OR a.BeneCountry <> p.Country  ))
		) OR
		( 
			 (( c.Country is NULL OR a.BeneBankCountry <> c.Country)
				AND (c.CountryOfResidence is NULL OR a.BeneBankCountry <> c.CountryOfResidence)
				AND (c.CountryOfIncorp is NULL OR a.BeneBankCountry <> c.CountryOfIncorp)
				AND (c.CountryOfOrigin is NULL OR a.BeneBankCountry <> c.CountryOfORigin)
				AND (c.CountryOfCitizenship is NULL OR a.BeneBankCountry <> c.CountryOfCitizenship)
				AND (p.Country is NULL OR a.BeneBankCountry <> p.Country  ))
		) OR
		( 
				((c.Country is NULL or a.ByOrderCountry <> c.Country)
				AND (c.CountryOfResidence is NULL OR a.ByOrderCountry <> c.CountryOfResidence)
				AND (c.CountryOfIncORp is NULL OR a.ByOrderCountry <> c.CountryOfIncorp )
				AND (c.CountryOfORigin is NULL OR a.ByOrderCountry <> c.CountryOfORigin )
				AND (c.CountryOfCitizenship is NULL OR a.ByOrderCountry <> c.CountryOfCitizenship )
				AND (p.Country is NULL OR a.ByOrderCountry <> p.Country  ))

		) OR
		( 
			 ((a.ByORderBankCountry <> c.Country OR c.Country is NULL)
				AND (c.CountryOfResidence is NULL OR a.ByOrderBankCountry <> c.CountryOfResidence)
				AND (c.CountryOfIncORp is NULL OR a.ByOrderBankCountry <> c.CountryOfIncorp)
				AND (c.CountryOfORigin is NULL OR a.ByOrderBankCountry <> c.CountryOfORigin )
				AND (c.CountryOfCitizenship is NULL OR a.ByOrderBankCountry <> c.CountryOfCitizenship)
				AND (p.Country is NULL OR a.ByOrderBankCountry <> p.Country  ))
		) OR
		( 
			 ((c.Country is NULL OR a.IntermediaryCountry <> c.Country )
				AND (c.CountryOfResidence is NULL OR a.IntermediaryCountry <> c.CountryOfResidence )
				AND (c.CountryOfIncORp is NULL OR a.IntermediaryCountry <> c.CountryOfIncorp )
				AND (c.CountryOfORigin is NULL OR a.IntermediaryCountry <> c.CountryOfORigin )
				AND ( c.CountryOfCitizenship is NULL OR a.IntermediaryCountry <> c.CountryOfCitizenship)
				AND (p.Country is NULL OR a.IntermediaryCountry <> p.Country  ))
		) OR
		( 
			 ((c.Country is NULL OR a.IntermediaryCountry2 <> c.Country)
				AND (c.CountryOfResidence is NULL OR a.IntermediaryCountry2 <> c.CountryOfResidence)
				AND (c.CountryOfIncORp is NULL OR a.IntermediaryCountry2 <> c.CountryOfIncorp)
				AND (c.CountryOfORigin is NULL OR a.IntermediaryCountry2 <> c.CountryOfORigin)
				AND (c.CountryOfCitizenship is NULL OR a.IntermediaryCountry2 <> c.CountryOfCitizenship)
				AND (p.Country is NULL OR a.IntermediaryCountry2 <> p.Country  ))
		) OR
		( 
			 ((c.Country is NULL OR a.IntermediaryCountry3 <> c.Country)
				AND (c.CountryOfResidence is NULL OR a.IntermediaryCountry3 <> c.CountryOfResidence)
				AND (c.CountryOfIncORp is NULL OR a.IntermediaryCountry3 <> c.CountryOfIncorp)
				AND (c.CountryOfORigin is NULL OR a.IntermediaryCountry3 <> c.CountryOfORigin)
				AND (c.CountryOfCitizenship is NULL OR a.IntermediaryCountry3 <> c.CountryOfCitizenship)
				AND (p.Country is NULL OR a.IntermediaryCountry3 <> p.Country  ))
			) OR
		( 
			 ((c.Country is NULL OR a.IntermediaryCountry4 <> c.Country)
				AND ( c.CountryOfResidence is NULL Or a.IntermediaryCountry4 <> c.CountryOfResidence)
				AND (c.CountryOfIncORp is NULL OR a.IntermediaryCountry4 <> c.CountryOfIncorp)
				AND (c.CountryOfORigin is NULL OR a.IntermediaryCountry4 <> c.CountryOfORigin)
				AND (c.CountryOfCitizenship is NULL OR a.IntermediaryCountry4 <> c.CountryOfCitizenship)
				AND (p.Country is NULL OR a.IntermediaryCountry4 <> p.Country  ))
		) )
		
END
--Inserting to @TT1 for building the description
INSERT INTO @TT1(Cust, Account, Country)
	SELECT Cust, Account, benecountry
	FROM @TT 
	WHERE 	
		BeneCountry IS NOT NULL 
	UNION ALL 
	SELECT Cust, Account, beneBankcountry
	FROM @TT 
	WHERE 	
		beneBankcountry IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, IntermediaryCountry
	FROM @TT 
	WHERE 	
		IntermediaryCountry IS NOT NULL 
	UNION ALL 
	SELECT Cust, Account, ByOrderCountry
	FROM @TT 
	WHERE 	
		ByOrderCountry IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, ByOrderBankCountry
	FROM @TT 
	WHERE 	
		ByOrderBankCountry IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, IntermediaryCountry2
	FROM @TT 
	WHERE 	
		IntermediaryCountry2 IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, IntermediaryCountry3
	FROM @TT 
	WHERE 	
		IntermediaryCountry3 IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, IntermediaryCountry4
	FROM @TT 
	WHERE 	
		IntermediaryCountry4 is NOT NULL 
	UNION ALL 
	SELECT Cust, Account, NULL
	FROM @TT 
	WHERE 	
		IsNull(@CountryList, '') = ''


DECLARE	@cur CURSOR
DECLARE @Cust LONGNAME,
	@bookdate INT

SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())

SET @cur = CURSOR FAST_FORWARD FOR 
SELECT DISTINCT SUM(BaseAmt) tranAmt, COUNT(tranNo) tranCnt, a.Cust
FROM @TT a 
GROUP BY  a.Cust
HAVING (SUM(BaseAmt) >= @minSumAmount AND 
	(@maxSumAmount = -1 OR SUM(BaseAmt) <= @maxSumAmount) AND
	count(tranno) >= @minIndCount)

OPEN @cur 
FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @Cust

WHILE @@FETCH_STATUS = 0 BEGIN

	-- Getting all the country field list
	set @Countries = ''
	set @Accounts = ''
	
	--Making the list(in case of -ALL-), Intersection list(in case of given country list)
	select @Countries = COALESCE(@Countries + ', ', '') 
			+ CONVERT(VARCHAR(50), Country)
			From @TT1 where Cust = @Cust and
			(@countryList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Country) + ',',@countryList) > 0)
            and Country is not null
			Group by Country
	select @Accounts = COALESCE(@Accounts + ', ', '')
		+ CONVERT(VARCHAR(50), Account)
		From @TT1 where Cust = @Cust
        and Account is not null
		Group by Account
	--Removing the first comma and space
	If(len(@Countries) > 0)
	Begin
		SET @Countries = substring(@Countries,3,len(@Countries))
	End	
	If(len(@Accounts) > 0)
	Begin
		SET @Accounts = substring(@Accounts,3,len(@Accounts))
	End		
	
	IF (@IncludeAddressFields) = 1
	BEGIN
			 --@OrigCountryList is the original countrylist parameter  without extra commas
			SET @desc = 'Customer ' + @Cust 
			+ ' is doing transactions which pass through the specified countries '
			+ ISNULL(@OrigCountryList,'') 
			+ ' in either the country or the address fields '
			+ CASE WHEN ISNULL(@Accounts,'') = '' THEN '' 
			  ELSE ' using the account(s) ' + ISNULL(@Accounts,'') END
			+ ' and the total amount is: ' + CONVERT(VARCHAR, @tranAmt) + ' over '
			+ CONVERT(VARCHAR, @tranCnt) + ' transactions '
			+ 'for a period from ' 
			+ Cast(dbo.BSA_ConvertIntToSqlDate(@fromDate, 'mm/dd/yyyy') AS Char(11)) 
			+ ' to ' 
			+ Cast(dbo.BSA_ConvertIntToSqlDate(@toDate, 'mm/dd/yyyy') AS Char(11))
			
	END
	ELSE 
	BEGIN
		SET @desc = 'Customer ' + @Cust 
			+ ' is doing transactions which pass through the country(s) ' 
			+ ISNULL(@Countries,'') 
			+ CASE WHEN ISNULL(@Accounts,'') = '' THEN '' 
			  ELSE ' using the account(s) ' + ISNULL(@Accounts,'') END
			+ ' and the total amount is: ' + CONVERT(VARCHAR, @tranAmt) + ' over '
			+ CONVERT(VARCHAR, @tranCnt) + ' transactions '
			+ 'for a period from ' 
			+ Cast(dbo.BSA_ConvertIntToSqlDate(@fromDate, 'mm/dd/yyyy') AS Char(11)) 
			+ ' to ' 
			+ Cast(dbo.BSA_ConvertIntToSqlDate(@toDate, 'mm/dd/yyyy') AS Char(11))
	END 
	IF @testAlert = 1
	BEGIN
		EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
		@Cust, NULL, 1
		IF @stat <> 0 GOTO EndOfProc
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 
		BEGIN
			EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
				@Cust, NULL, 0
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE 
		IF @WLTYPE = 1 
		BEGIN
			EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT, 
				@WLCode, @desc, @bookdate, @Cust, NULL
			IF @stat <> 0 GOTO EndOfProc   	
		END	
	END		
	IF (@WLTYPE = 0) OR (@testAlert = 1)
	BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'Alert', @ID, TRANNO 
				FROM @TT t
				WHERE t.cust = @cust
	
			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC
	END 
	ELSE 
	IF @WLTYPE = 1 
	BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'SUSPACT', @ID, TRANNO 
				FROM @TT t
				WHERE t.cust = @cust

			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC 
	END
	FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @Cust
END

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_HighRiskCountry
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_HighRiskCountry
RETURN @stat

GO


IF EXISTS (SELECT * FROM sysobjects 
		WHERE id = object_id(N'[dbo].[BSA_UpdCaseReportedStatus]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE dbo.BSA_UpdCaseReportedStatus
Go

CREATE PROCEDURE
	dbo.BSA_UpdCaseReportedStatus
(
	@CaseId int,
	@CustomerId varchar(35),
	@CaseStatus varchar(20),
	@CaseStatusChangeDate datetime,
	@IsReportFiled bit,
	@FiledBy varchar(11),
	@FiledDateTime datetime,
	@Notes varchar(3000)
)	
WITH ENCRYPTION
AS

DECLARE @IsDelegationOn bit
--Define Error codes here
DECLARE @RecNotFoundErrorCode INT, @InvalidParameterErrorCode INT, 
		@CustomerIdNotValidErrorCode INT, @CaseAlreadyClosedErrorCode INT,
		@CaseAlreadyArchivedErrorCode INT, @DelegationDisabledErrorCode INT,
		@CaseLockedErrorCode INT
DECLARE @Status INT

SET @InvalidParameterErrorCode = 250010
SET @RecNotFoundErrorCode = 250051
SET @CustomerIdNotValidErrorCode = @RecNotFoundErrorCode + 2
SET @CaseAlreadyClosedErrorCode = @RecNotFoundErrorCode + 1
SET @CaseLockedErrorCode = @RecNotFoundErrorCode + 3
SET @CaseAlreadyArchivedErrorCode = @RecNotFoundErrorCode + 4 
SET @DelegationDisabledErrorCode = 250030  --Invalid State error code

SET @Status = 0

SELECT @IsDelegationOn = (select isnull(o.Enabled,0) from OptionTbl o (nolock) where Code='DlgtOptions')
IF @IsDelegationOn=0
	BEGIN
		SET @Status = @DelegationDisabledErrorCode
		SELECT @Status
		RETURN
	END

--Validate the parameters
IF ISNULL(@CaseID,0) = 0 OR
		@CaseStatus NOT IN ('CONFIRMED', 'WAIVED') OR
		ISNULL(@Notes,'') = ''
	BEGIN
		SET @Status = @InvalidParameterErrorCode
		SELECT @Status
		RETURN
	END


--Set status if case does not exist in SuspiciousActivity
IF NOT EXISTS(SELECT RecNo FROM SuspiciousActivity (nolock) WHERE RecNo = @CaseId)
	SET @Status=@RecNotFoundErrorCode
ELSE
	BEGIN
		DECLARE @ClosedTime datetime, @Cust varchar(35), @LockTime datetime
		SELECT @ClosedTime = ClosedTime, @Cust = Cust, @LockTime = LockTime
			FROM SuspiciousActivity (nolock) WHERE RecNo = @CaseId

		IF @ClosedTime IS NOT NULL
			--Set status if case exists in SuspiciousActivity and is already closed
			SET @Status=@CaseAlreadyClosedErrorCode
		ELSE IF ISNULL(@Cust,'') <> ISNULL(@CustomerId,'')
			--Set status if case exists but cust id does not match
			SET @Status=@CustomerIdNotValidErrorCode
		ELSE IF @LockTime Is NOT Null AND DATEDIFF(minute, @LockTime, GetDate()) <= 10
			--Set status if case is locked and lock has been in place for 10 minutes or less
			SET @Status=@CaseLockedErrorCode
		
	END

--Set status if case does not exist in SuspiciousActivity and it is archived
IF @Status=@RecNotFoundErrorCode AND EXISTS(SELECT RecNo FROM SuspiciousActivityHist (nolock) WHERE RecNo = @CaseId)
	SET @Status = @CaseAlreadyArchivedErrorCode

IF @Status=0
BEGIN

	-- Start standard stored procedure transaction header
	DECLARE @trnCnt INT
	SELECT @trnCnt = @@trancount	-- Save the current trancount
	IF @trnCnt = 0	-- Transaction has not begun
		BEGIN TRAN BSA_UpdCaseReportedStatus
	ELSE
		-- Already in a transaction
		SAVE TRAN BSA_UpdCaseReportedStatus
	-- End standard stored procedure transaction header

	DECLARE @CaseReviewState int, @NotesPrefix VARCHAR(150), @CurrentDateTime DATETIME
	SELECT @CaseReviewState = (SELECT ReviewState FROM SuspiciousActivity (nolock) WHERE RecNo = @CaseId)
	SELECT @CurrentDateTime = GETDATE()
	SET @NotesPrefix = CHAR(13)+CHAR(10)+'CASE FEEDBACK - ' + @FiledBy + ', '+CONVERT(VARCHAR, @CurrentDateTime, 101) + ' ' + 
		CONVERT(VARCHAR,CASE WHEN DATEPART(hh, @CurrentDateTime)>12 THEN DATEPART(hh, @CurrentDateTime)-12 ELSE DATEPART(hh, @CurrentDateTime) END)+':'+
		CASE WHEN DATEPART(mi, @CurrentDateTime)<10 THEN '0'+CONVERT(VARCHAR, DATEPART(mi, @CurrentDateTime)) 
			ELSE CONVERT(VARCHAR, DATEPART(mi, @CurrentDateTime)) 
			END + ':'+
		CASE WHEN DATEPART(ss, @CurrentDateTime)<10 THEN '0'+CONVERT(VARCHAR, DATEPART(ss,@CurrentDateTime))
			ELSE CONVERT(VARCHAR, DATEPART(ss, @CurrentDateTime))
		END + CASE WHEN DATEPART(hh, @CurrentDateTime)>=12 THEN 'PM' ELSE 'AM' END + ': '+CHAR(13)+CHAR(10)

	IF ISNULL(@CaseReviewState,0) = CASE WHEN @CaseStatus = 'CONFIRMED' THEN 3 WHEN @CaseStatus = 'WAIVED' THEN 1 END
		UPDATE SuspiciousActivity
		SET
			ClosedTime=GETDATE(),
			Descr = ISNULL(Descr,'') + @NotesPrefix + @Notes,
			LastOper = @FiledBy, 
			LastModify = GETDATE(),
			LastState = 'Mod'
		WHERE
			RecNo = @CaseId and
			ISNULL(Cust,'') = ISNULL(@CustomerId,'')		
	ELSE
		UPDATE SuspiciousActivity
		SET
			ReviewState=CASE WHEN @CaseStatus = 'CONFIRMED' THEN 3 WHEN @CaseStatus = 'WAIVED' THEN 1 END,
			ReviewTime=GETDATE(),
			ReviewOper=@FiledBy,
			ClosedTime=GETDATE(),
			Descr = ISNULL(Descr,'') + @NotesPrefix + @Notes,
			LastOper = @FiledBy, 
			LastModify = GETDATE(),
			LastState = 'Mod'
		WHERE
			RecNo = @CaseId and
			ISNULL(Cust,'') = ISNULL(@CustomerId,'')
	
	SELECT @Status = @@error
	
	--If a SAR was filed and Probation is enabled and PopPrbtion is enabled and 
	--  the SARFILED probation reason exists then put customer on probation for 90 days
	IF @CaseStatus = 'CONFIRMED' AND @Notes LIKE '%SAR%' AND @IsReportFiled = 1
	BEGIN
		DECLARE @ProbationCode VARCHAR(35)
		SET @ProbationCode='SARFILED'
		IF EXISTS(select * from OptionTbl where Code='probation' and Enabled=1)
			AND EXISTS(select * from OptionTbl where Code='popprbtion' and Enabled = 1)
			AND EXISTS(SELECT * FROM CODETBL WHERE ObjectType='PROBREASON' AND Code=@ProbationCode)
		BEGIN
			UPDATE 
				Customer 
			SET 
				OnProbation = 1,
				ProbationReason = @ProbationCode,
				ProbationStartDate = dbo.ConvertSqlDateToInt(@FiledDateTime),
				ProbationEndDate = dbo.ConvertSqlDateToInt(DATEADD(day,90,@FiledDateTime)),
				LastOper = @FiledBy,
				LastModify = GETDATE()
			FROM 
				Customer INNER JOIN SuspiciousActivity 
				ON
				Customer.Id = SuspiciousActivity.Cust
				WHERE 
				SuspiciousActivity.RecNo = @CaseId
				AND
				(Customer.ProbationEndDate is null OR Customer.ProbationEndDate < dbo.ConvertSqlDateToInt(DATEADD(day,90,@FiledDateTime)))
		END
	END
	
	SELECT @Status = @Status + @@error
	
	IF (@Status <> 0) BEGIN
	  ROLLBACK TRAN BSA_UpdCaseReportedStatus
	END
	IF (@Status=0) BEGIN
		COMMIT TRAN BSA_UpdCaseReportedStatus
	END

END

SELECT @Status

Go
if exists (select * from sysobjects 
	WHERE id = object_id(N'[dbo].[USR_KeywordSearch]') 
	AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure USR_KeywordSearch
Go
CREATE PROCEDURE dbo.USR_KeywordSearch(
				@WLCode SCODE, 
				@testAlert INT, 
				@KeywordList  VARCHAR(8000),
				@ActivityTypeList   VARCHAR(8000),  
				@RecvPay INT,
				@MinTotalAmount MONEY,
				@MinTranCnt INT,
				@MinTranAmt MONEY,				
				@CustomerTypeList Varchar(8000),
				@AccountTypeList Varchar(8000),	
				@LastNoOfDays INT 
	)
AS
BEGIN
	/* Rule and Parameter Description
		Performs a keyword search on Party Names/Notes/Instructions fields of archived activity table.
		An Alert or Case is generated where a match is found in the activity for a given customer/account combination.
		Designed to be run daily, weekly, monthly or quarterly on a Post-EOD Schedule.

	   @keywordList        -- List of keywords to look for. Cannot be null or blank
	   @ActivityTypeList   -- Type of activities to be detected.	 
  	   @RecvPay            -- Deposit/Withdrawal. 1 for Deposit,
                              2 for Withdrawals, 3 for both
       @MinTotalAmount     -- Rule checks if the total sum of all transactions in the specified last number
                          of days is greater than or equal to amount specified
	   @MinTranCnt		   -- Number of transactions identified below this value will not trigger an alert for the customer, anything equal to or above will cause an alert to generate
	   @MinTranAmt		   -- Minimum Amount of each transaction that ticks against the number of transaction count
	   @CustomerTypeList   -- Select customer types to be included
	   @AccountTypeList    -- Select account types to be included
       @LastNoOfDays       -- Rule will consider all activity that occurred within the specified last number of days	
	*/

/*  DECLARATIONS */
	DECLARE	@DESCRIPTION VARCHAR(2000),
		@DESC VARCHAR(2000),
		@ID INT, 
		@WLTYPE INT,
		@Stat INT,
		@TRNCNT INT
	
	DECLARE 	@wrkDate DATETIME  
    DECLARE  	@startAlrtDate DATETIME  
    DECLARE  	@endAlrtDate DATETIME  
    DECLARE  	@CurrDate GenericDate  
    DECLARE  	@MinDate INT  
	DECLARE 	@StartDate DATETIME

	DECLARE	@TT TABLE (
		Cust VARCHAR(40),
		Account VARCHAR(40),
		RecvPay INT,
		TranNO INT,
		BaseAmt MONEY)

	DECLARE	@TT1 TABLE (
		Cust VARCHAR(40),
		Account VARCHAR(40),
		RecvPay INT,
		TranNO INT,
		BaseAmt MONEY)

	DECLARE @Keyword VARCHAR(500)
	DECLARE @pos smallint

	-- Temporary table of Activity Types that have not been specified as Exempt
	DECLARE @ActType TABLE 
	(
	  Type		INT
	)

--- ********************* Begin Rule Procedure **********************************
	SET NOCOUNT ON
	SET @Stat = 0
	SET @LastNoOfDays = ABS(@LastNoOfDays)  

	-- Check for empty list  
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)

	IF(ISNULL(@customerTypeList,'') = '' OR UPPER(ISNULL(@customerTypeList,'-ALL-')) = '-ALL-')
		SELECT @customerTypeList = NULL
	ELSE
   		SELECT @customerTypeList = ',' + REPLACE(LTRIM(RTRIM(@customerTypeList)),CHAR(32),'') + ','
		
	IF(ISNULL(@accountTypeList,'') = '' OR UPPER(ISNULL(@accountTypeList,'-ALL-')) = '-ALL-')
		SELECT @accountTypeList = NULL
	ELSE
   		SELECT @accountTypeList = ',' + REPLACE(LTRIM(RTRIM(@accountTypeList)),CHAR(32),'') + ','
		
	-- Check for empty list
	IF (ISNULL(@KeywordList,'') = '') 
		RETURN @Stat
	ELSE
		SELECT @KeywordList = LTRIM(RTRIM(@KeywordList))+','

	IF (@RecvPay = 3)  
         SET @RecvPay = NULL

	/* START STANDARD STORED PROCEDURE TRANSACTION HEADER */
	SET @TRNCNT = @@TRANCOUNT	-- SAVE THE CURRENT TRANCOUNT
	IF @TRNCNT = 0
		-- TRANSACTION HAS NOT BEGUN
		BEGIN TRAN USR_KeywordSearch
	ELSE
		-- ALREADY IN A TRANSACTION
		SAVE TRAN USR_KeywordSearch
	/* END STANDARD STORED PROCEDURE TRANSACTION HEADER */


	/*  STANDARD RULES HEADER */
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE, @StartDate = 
		CASE 	
		WHEN UseSysDate in (0,1) THEN
			-- use System date
			GETDATE()
		WHEN UseSysDate = 2 THEN
			-- use business date
			(SELECT BusDate FROM dbo.SysParam)
		ELSE
			GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode


	--Get all the non-exempted activity types
	INSERT INTO @ActType
	SELECT 	Type  FROM vwRuleNonExmActType
	WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	SELECT @MinDate = DBO.CONVERTSQLDATETOINT(  
		DATEADD(D, -1 * @LastNoOfDays, CONVERT(VARCHAR, @StartDate)))  

	-- Keyword list is converted to table form(rows) using intermediate XML.

	SET @pos = 1
	WHILE @pos <  LEN(@KeywordList)

	BEGIN
		SELECT @keyword = LTRIM(RTRIM(SUBSTRING(@KeywordList,@pos,CHARINDEX(',',@KeywordList, @pos)- @pos)))

		IF (LEN(LTRIM(RTRIM(@keyword))) >0)
			INSERT INTO @TT1 ( Cust, Account, TranNo,RecvPay,BaseAmt)
				SELECT Cust, Account, tranno,RecvPay,BaseAmt
					FROM ActivityHist (NOLOCK)
					INNER JOIN @ActType Act ON Act.Type = ActivityHist.Type
					INNER JOIN Customer Cus ON Cus.Id = ActivityHist.Cust
					INNER JOIN Account Acc ON Acc.Id = ActivityHist.Account
					WHERE  	
						(Bene LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						ByOrder LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						BeneBank LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						ByOrderBank LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						Intermediary LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						Intermediary2 LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						Intermediary3 LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						Instructions LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR						
						Intermediary4 LIKE '%'+LTRIM(RTRIM(@Keyword))+'%' OR
						ActivityHist.Notes LIKE '%'+LTRIM(RTRIM(@Keyword))+'%')
						AND ((ISNULL(@customerTypeList, '') = '' OR
						CHARINDEX(',' + LTRIM(RTRIM(Cus.Type)) + ',', @customerTypeList ) > 0))
						AND ((ISNULL(@accountTypeList, '') = '' OR
						CHARINDEX(',' + LTRIM(RTRIM(Acc.Type)) + ',', @accountTypeList ) > 0))	
						AND ActivityHist.BaseAmt >= @MinTranAmt
						AND BookDate >= @MinDate AND BookDate <= dbo.ConvertSqlDateToInt(@StartDate)  
				  		AND RecvPay = ISNULL(@RecvPay, Recvpay)  
						
		SELECT @pos = CHARINDEX(',',@KeywordList, @pos)+1

	END

	INSERT INTO @TT SELECT DISTINCT * FROM @TT1

	IF (@testAlert = 1  OR @WLType = 0)
	BEGIN

		SELECT @STARTALRTDATE = GETDATE() 

		INSERT INTO Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
				LastModify, Cust, Account, IsTest) 
			SELECT @WLCode, 'Customer ''' + ISNULL(CUST,'') + ''' and Account ''' + ISNULL(Account,'')   
					+ ''' had ' + CONVERT(VARCHAR,Count(TranNo))  
					+ CASE WHEN RecvPay = 1 THEN ' incoming'
							WHEN RecvPay = 2 THEN ' outgoing'
					END
					 + ' transactions totaling $' + CONVERT(VARCHAR,SUM(BaseAmt)) +   
					 ' BETWEEN the period '+ CONVERT(VARCHAR, @MinDate) + 'and '  
					+ CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(@StartDate)) +   
					  ' WHERE specified keywords were matched ' ,   
					0, GETDATE(), Null, Null, Cust, Account, @testAlert  
				FROM @TT   
				WHERE RecvPay = ISNULL(@RecvPay,RecvPay)
				GROUP BY Cust, Account,RecvPay  
				HAVING (@MinTotalAmount = -1 OR SUM(BaseAmt) >= @MinTotalAmount)
				AND COUNT(TranNo) >= @MinTranCnt

		SELECT @Stat = @@ERROR	

		IF @Stat <> 0  GOTO ENDOFPROC

		SELECT @ENDALRTDATE = GETDATE() 

		INSERT INTO SASActivity (ObjectType, ObjectID, TranNo)
			SELECT 'Alert', AlertNo, TranNo   
				FROM @TT t, Alert WHERE  
				IsNull(t.Account,'') = IsNull(Alert.Account,'') AND 
				t.RecvPay = CASE WHEN Alert.[Desc] LIKE '%incoming%' THEN 1
								WHEN Alert.[Desc] LIKE '%outgoing%' THEN 2
							END 
				AND t.cust = Alert.cust AND  
				Alert.WLCode = @WLCode AND  
				Alert.CreateDate BETWEEN @startAlrtDate AND @endAlrtDate 

		SELECT @Stat = @@ERROR
		IF @Stat <> 0  GOTO ENDOFPROC
	END 
	ELSE 
		IF @WLTYPE = 1 
		BEGIN
			SELECT @wrkDate = GETDATE() 
			EXEC @CurrDate = BSA_CvtDateToLong @wrkDate
			SELECT @STARTALRTDATE = GETDATE() 
			INSERT INTO SuspiciousActivity (ProfileNo, BookDate, Cust, Account, 
				Activity, SuspType, StartDate, EndDate, RecurType, 
				RecurValue, ActCurrReportAmt, ActInActCnt, ActOutActCnt, 
				ActInActAmt, ActOutActAmt, CurrReportAmt, ExpAvgInActCnt, 
				ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt, InCntTolPerc, 
				OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr, ReviewState, 
				ReviewTime, ReviewOper, App, AppTime, AppOper, 
				WLCode, WLDesc, CreateTime )
			SELECT	Null, @CurrDate, cust, Account,
				Null, 'Rule', Null, Null, Null, Null,
				Null, Null, Null, Null, Null, Null, Null, Null,
				Null, Null, 0, 0, 0, 0, 
				Null, Null, Null, Null, 0, Null, Null,
				@WLCode,'Customer ''' + ISNULL(CUST,'') + ''' and Account ''' + ISNULL(Account,'')      
			   + ''' had ' + convert(varchar,Count(TranNo))  
			   + CASE WHEN RecvPay = 1 THEN ' incoming' 
				WHEN RecvPay = 2 THEN 'outgoing'
				END + ' transactions totaling $' + CONVERT(VARCHAR,SUM(BaseAmt)) +   
			   ' BETWEEN the period '+ CONVERT(VARCHAR, @MinDate) + ' and '  
			   + CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(@StartDate)) +   
			   ' where specified keywords were matched ' ,   
			   GetDate()   
		   From @TT   
			WHERE RecvPay  = ISNULL(@RecvPay,RecvPay)
		   GROUP BY Cust, ACCOUNT,RecvPay  
		   HAVING (@MinTotalAmount = -1 OR SUM(BaseAmt) >= @MinTotalAmount  )
		   AND COUNT(TranNo) >= @MinTranCnt

           SELECT @Stat = @@ERROR        
         
           IF @Stat <> 0  GOTO ENDOFPROC        
           
           SELECT @ENDALRTDATE = GETDATE()    

		   
			INSERT INTO SASActivity (ObjectType, ObjectID, TranNo)
			SELECT 'SUSPACT', SA.RecNo, TranNo   
				FROM @TT t, SuspiciousActivity SA WHERE  
				IsNull(t.Account,'') = IsNull(SA.Account,'') AND 
				t.RecvPay = CASE WHEN SA.[WLDesc] LIKE '%incoming%' THEN 1
								WHEN SA.[WLDesc] LIKE '%outgoing%' THEN 2
							END 
				AND t.cust = SA.cust AND  
				SA.WLCode = @WLCode AND  
				SA.CreateTime BETWEEN @startAlrtDate AND @endAlrtDate 

	
			SELECT @Stat = @@ERROR	
			
			if @Stat <> 0  GOTO ENDOFPROC
		END
	
	ENDOFPROC:
	IF (@Stat <> 0) BEGIN 
		ROLLBACK TRAN USR_KeywordSearch
		RETURN @Stat
	END	

	IF @TRNCNT = 0
		COMMIT TRAN USR_KeywordSearch
		RETURN @Stat
END
Go

if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[BSA_ImpUnprocAccount]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure [dbo].[BSA_ImpUnprocAccount]
Go

Create Procedure dbo.BSA_ImpUnprocAccount
    (@pCode VarChar(35),
	@oper VARCHAR(11),
	@retCount INT OUTPUT)
 
 With Encryption As

  declare @insertCount INT,
    @delCount INT
  -- Start standard stored procedure transaction header
  declare @trnCnt int,
      @stat int,
      @objectId ObjectID
  select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_ImpUnprocAccount
  else
    -- Already in a transaction
	save tran BSA_ImpUnprocAccount
  -- End standard stored procedure transaction header

  SET @stat = -1
  -- make sure Code is not null and is non-zero
  SET @pCode = ISNULL(@pCode, '')

  -- create temporary table
  if exists (select * from tempdb..sysobjects 
    where [name] like '#tmpUnprocAccount%' and type = 'U')
    drop table #tmpUnprocAccount
  create table #tmpUnprocAccount (
    [Id] VarChar(35) NULL,
	[Name] [varchar](40) NULL,
	[Type] [varchar](11) NULL,
	[Monitor] [varchar](10) NULL,
	[ExemptionStatus] [varchar](11) NULL,
	[Notes] [varchar](500) NULL,
	[Branch] [varchar](11) NULL,
	[OpenDate] [varchar](25) NULL,
	[RiskFactor] tinyint NULL,
	[CreateOper] [varchar](11) NULL,
	[CreateDate] [varchar](25) NULL,
	[LastOper] [varchar](11) NULL,
	[LastModify] [varchar](25) NULL,
	[OwnerBranch] [varchar](11) NULL,
	[OwnerDept] [varchar](11) NULL,
	[OwnerOper] [varchar](11) NULL,
	[Closed] [varchar](10) NULL,
	[CloseDate] [varchar](25) NULL,
	[CloseReason] [varchar](255) NULL,
	[Reference] [varchar](100) NULL,
	[FreeLookEndDate] [varchar](25) NULL,
	[Request] [varchar](10) NULL,
	[CashValue] [varchar](50) NULL,
	[ExpiryDate] [varchar](25) NULL,
	[SinglePremium] [varchar](10) NULL,
	[PolicyAmount] [varchar](50) NULL,
	[User1] [varchar](40) NULL,
	[User2] [varchar](40) NULL,
	[User3] [varchar](40) NULL,
	[User4] [varchar](40) NULL,
	[User5] [varchar](40) NULL,
	[AccountOfficer] [varchar](40) NULL,
	[BalanceAmt] [varchar](50) NULL,
	[SourceofPayments] [varchar](250) NULL,
	[AcStatus] VarChar(35) NULL,
	[InitialDeposit] [varchar](50) NULL,
	[TypeOfFunds] [varchar](25) NULL,
	[ReqWireTrans] [varchar](10) NULL,
	[AccACHPay] [varchar](10) NULL,
	[OrgACHPay] [varchar](10) NULL,
	[NoOfDepositMnth] [varchar](25) NULL,
	[TotDepositAmtMnth] [varchar](50) NULL,
	[NoOfCashDepositMnth] [varchar](25) NULL,
	[TotCashDepositAmtMnth] [varchar](50) NULL,
	[NoOfCashWithDMnth] [varchar](25) NULL,
	[TotCashWithDAmtMnth] [varchar](50) NULL,
	[NoOfCheckWMnth] [varchar](25) NULL,
	[TotCheckWAmtMnth] [varchar](50) NULL,
	[NoOfDomInWireMnth] [varchar](25) NULL,
	[TotDomInWireAmtMnth] [varchar](50) NULL,
	[NoOfDomOutWireMnth] [varchar](25) NULL,
	[TotDomOutWireAmtMnth] [varchar](50) NULL,
	[ReqForInWireTrans] [varchar](10) NULL,
	[NoOfForInWireMnth] [varchar](25) NULL,
	[TotForInWireAmtMnth] [varchar](50) NULL,
	[OrigCntryForInWires] [varchar](1000) NULL,
	[ReqForOutWireTrans] [varchar](10) NULL,
	[NoOfForOutWireMnth] [varchar](25) NULL,
	[TotForOutWireAmtMnth] [varchar](50) NULL,
	[DestCntryForOutWires] [varchar](1000) NULL,
	[IsProspect] [varchar](10) NULL,
	[ErrMsg] [varchar](max) NULL,
	[ErrorStatus] [bit] NULL,
	-- Add 4 new fields to Account Start Here below --
	[DescOfAcctPurpose] [varchar](255) NULL,				-- Description of Account Purpose
	[PowerOfAttorney] [varchar](10) NULL,					-- Power Of Attorney bit NULL 
	[RemoteDepositCapture] [varchar](10) NULL,				-- Remote Deposit Capture bit NULL 
	[RemoteDepositCaptureRiskRating] [varchar](40) NULL		-- Remote Deposit Capture Risk Rating
	-- Add 4 new fields to Account End Here  --        	
)

  INSERT INTO #tmpUnprocAccount
  SELECT [Id]
      ,UA.[Name]
      ,[Type]
      ,[Monitor]
      ,UA.[ExemptionStatus]
      ,[Notes]
      ,CASE ua.branch 
		WHEN b.Code THEN ua.branch
		WHEN bm.OrigBrch THEN bm.PrimeCode
		ELSE NULL -- Error, should not happen
       END
      ,[OpenDate]
      ,UA.[RiskFactor]
      ,UA.[CreateOper]
      ,UA.[CreateDate]
      ,UA.[LastOper]
      ,UA.[LastModify]
      ,CASE ua.OwnerBranch 
		WHEN ob.Code THEN ua.OwnerBranch
		WHEN bm2.OrigBrch THEN bm2.PrimeCode
		ELSE NULL -- Error, should not happen
       END
      ,CASE ua.OwnerDept 
		WHEN d.Code THEN ua.OwnerDept
		WHEN dm.OrigDept THEN dm.PrimeCode
		ELSE NULL -- Error, should not happen
	   END
      ,[OwnerOper]
      ,[Closed]
      ,[CloseDate]
      ,[CloseReason]
      ,[Reference]
      ,[FreeLookEndDate]
      ,[Request]
      ,[CashValue]
      ,[ExpiryDate]
      ,[SinglePremium]
      ,[PolicyAmount]
      ,[User1]
      ,[User2]
      ,[User3]
      ,[User4]
      ,[User5]
      ,[AccountOfficer]
      ,[BalanceAmt]
      ,[SourceofPayments]
      ,[AcStatus]
      ,[InitialDeposit]
      ,[TypeOfFunds]
      ,[ReqWireTrans]
      ,[AccACHPay]
      ,[OrgACHPay]
      ,[NoOfDepositMnth]
      ,[TotDepositAmtMnth]
      ,[NoOfCashDepositMnth]
      ,[TotCashDepositAmtMnth]
      ,[NoOfCashWithDMnth]
      ,[TotCashWithDAmtMnth]
      ,[NoOfCheckWMnth]
      ,[TotCheckWAmtMnth]
      ,[NoOfDomInWireMnth]
      ,[TotDomInWireAmtMnth]
      ,[NoOfDomOutWireMnth]
      ,[TotDomOutWireAmtMnth]
      ,[ReqForInWireTrans]
      ,[NoOfForInWireMnth]
      ,[TotForInWireAmtMnth]
      ,[OrigCntryForInWires]
      ,[ReqForOutWireTrans]
      ,[NoOfForOutWireMnth]
      ,[TotForOutWireAmtMnth]
      ,[DestCntryForOutWires]
      ,[IsProspect]
      ,[ErrMsg]
      ,[ErrorStatus]
      ,[DescOfAcctPurpose]
      ,[PowerOfAttorney]
      ,[RemoteDepositCapture]
      ,[RemoteDepositCaptureRiskRating]
  FROM UnprocAccount ua
  Left Join AccountType at
   On ua.type = Convert(Varchar, at.Code)
  Left Join Psec.dbo.Branch b
   On ua.branch = b.Code
  Left Join Psec.dbo.Branch ob
   On ua.OwnerBranch = ob.Code
  Left Join psec.dbo.Dept d
   On ua.OwnerDept = d.code
  Left Join BranchMap bm 
   On ua.branch = bm.OrigBrch And  
    bm.Task is Null
  Left Join BranchMap bm2
   On ua.OwnerBranch = bm2.OrigBrch And  
    bm2.Task is Null
  Left Join DeptMap dm 
   On ua.OwnerDept = dm.OrigDept And  
    dm.Task is Null
  WHERE UA.ErrorStatus = 0
    and ((@pCode = '') OR (UA.Id = @pCode))
  Order By Id, at.Code desc, b.Code desc, ob.Code, d.Code desc



  -- import temporary table with Codes of records
  -- Oper has rights to (branch and dept considered)
  Delete #tmpUnprocAccount where Id not in
  (
  SELECT distinct UA.Id
  FROM #tmpUnprocAccount UA
  JOIN psec.dbo.operrights SR ON
  CASE SR.Scope -- for branch part of join
    when 1024 then isnull(SR.ObjBranch, '') -- skip branch
    when 256 then isnull(UA.Branch , '')
    when 64 then isnull(UA.Branch , '')
    when 4 then isnull(UA.Branch, '')
    else '*N/A*'
  END = isnull(SR.ObjBranch, '')
  AND
  CASE SR.Scope -- for dept part of join
    when 1024 then isnull(SR.ObjDept, '') -- skip dept
    when 256 then isnull(SR.ObjDept, '') -- skip dept
    when 64 then isnull(UA.OwnerDept, '')
    when 4 then isnull(UA.OwnerDept, '')
    else '*N/A*'
  END = isnull(SR.ObjDept, '')
  --AND -- UnprocAct does not have Oper
  --CASE SR.Scope -- for scope part of join
  --    when 1024 then isnull(SR.ObjOper, '') -- skip Oper
  --    when 256 then isnull(SR.ObjOper, '') -- skip Oper
  --    when 64 then isnull(SR.ObjOper, '') -- skip Oper
  --    when 4 then isnull(x.oper, '')
  --    else '*N/A*'
  --END = isnull(SR.ObjOper, '')
  WHERE SR.oper = @oper and SR.[right] = 'LOADBSAUPC' -- !!!!!!!!!!!! LOADBSAUPA

    and ((@pCode = '') OR (UA.Id = @pCode))
 )
  -- make sure there is something to do
  if not exists (select * from #tmpUnprocAccount)
  begin
    set @retCount = 0
	If @trnCnt = 0 BEGIN
   	commit tran BSA_ImpUnprocAccount
	END ELSE BEGIN
		rollback tran BSA_ImpUnprocAccount
	END

    return (0) -- nothing to do
  end
  
  -- Update if exists
  Update Ac Set Name = NullIf(RTrim(T.name), ''), Monitor = T.monitor,
    ExemptionStatus = NullIf(RTrim(T.exemptionStatus), ''), 
	Notes = NullIf(RTrim(T.notes), ''), Branch = RTrim(T.branch), 
	OpenDate = dbo.BSA_ShortDateToInternationalizationDate(T.openDate), 
	LastOper = RTrim( IsNull (T.LastOper, @oper)), 
	OwnerBranch = NullIf(RTrim(T.ownerBranch), ''), 
	OwnerDept = NullIf(RTrim(T.ownerDept), ''), 
	OwnerOper = NullIf(RTrim(T.ownerOper), ''),
	RiskFactor = T.riskFactor,
	closed = T.closed,
    closeDate = dbo.BSA_ShortDateToInternationalizationDate(T.CloseDate),
    expiryDate = dbo.BSA_ShortDateToInternationalizationDate(T.expiryDate),
    freelookenddate = dbo.BSA_ShortDateToInternationalizationDate(T.freelookenddate),
    Reference = NullIf(RTrim(T.Reference), ''),
    closeReason = NullIf(RTrim(T.CloseReason),'') ,
    Type = NullIf(RTrim(T.type), ''),
    policyAmount = dbo.BSA_StringToInternationalizationMoney(T.PolicyAmount),
    CashValue = dbo.BSA_StringToInternationalizationMoney(T.CashValue),
    Request = T.Request,
    SinglePremium = T.SinglePremium,
	Accountofficer = NULLIF(RTrim(T.Accountofficer), ''),
	BalanceAmt = NullIf(dbo.BSA_StringToInternationalizationMoney(T.BalanceAmt),0),
	SourceofPayments = NullIf(RTrim(T.SourceofPayments),''),
	User1 = NullIf(T.User1,''), 
	User2 = NullIf(T.User2,''), 
	User3 = NullIf(T.User3,''), 
	User4 = NullIf(T.User4,''),
	User5 = NullIf(T.User5,''),
	Acstatus = NULLIF(RTrim(T.acstatus), ''),
	InitialDeposit = dbo.BSA_StringToInternationalizationMoney(T.InitialDeposit),
	TypeOfFunds = NullIf(T.TypeOfFunds,''),
	ReqWireTrans = T.ReqWireTrans ,
	AccACHPay = T.AccACHPay ,
	OrgACHPay = T.OrgACHPay,
	NoOfDepositMnth = T.NoOfDepositMnth ,
	TotDepositAmtMnth = dbo.BSA_StringToInternationalizationMoney(T.TotDepositAmtMnth), 
	NoOfCashDepositMnth = T.NoOfCashDepositMnth,
	TotCashDepositAmtMnth = dbo.BSA_StringToInternationalizationMoney(T.TotCashDepositAmtMnth), 
	NoOfCashWithDMnth = T.NoOfCashWithDMnth,
	TotCashWithDAmtMnth= dbo.BSA_StringToInternationalizationMoney(T.TotCashWithDAmtMnth), 
	NoOfCheckWMnth= T.NoOfCheckWMnth,
	TotCheckWAmtMnth= dbo.BSA_StringToInternationalizationMoney(T.TotCheckWAmtMnth), 
	NoOfDomInWireMnth = T.NoOfDomInWireMnth,
	TotDomInWireAmtMnth = dbo.BSA_StringToInternationalizationMoney(T.TotDomInWireAmtMnth), 
	NoOfDomOutWireMnth = T.NoOfDomOutWireMnth,
	TotDomOutWireAmtMnth = dbo.BSA_StringToInternationalizationMoney(T.TotDomOutWireAmtMnth) , 
	ReqForInWireTrans = T.ReqForInWireTrans,
	NoOfForInWireMnth = T.NoOfForInWireMnth,
	TotForInWireAmtMnth = dbo.BSA_StringToInternationalizationMoney(T.TotForInWireAmtMnth),
	OrigCntryForInWires = NullIf(T.OrigCntryForInWires, ''),
	ReqForOutWireTrans = T.ReqForOutWireTrans,
	NoOfForOutWireMnth = T.NoOfForOutWireMnth,
	TotForOutWireAmtMnth = dbo.BSA_StringToInternationalizationMoney(T.TotForOutWireAmtMnth),
	DestCntryForOutWires = NullIf(T.DestCntryForOutWires,''),
	IsProspect= T.IsProspect,
    DescOfAcctPurpose = NullIf(RTrim(T.DescOfAcctPurpose), ''),
    PowerOfAttorney = T.PowerOfAttorney,
    RemoteDepositCapture = T.RemoteDepositCapture,
    RemoteDepositCaptureRiskRating = NullIf(RTrim(T.RemoteDepositCaptureRiskRating), '')	
  From #tmpUnprocAccount T Join Account Ac On T.Id = Ac.Id

  Select @stat = @@error, @insertCount = @@rowcount

  -- Evaluate results of the transaction
  If @stat <> 0 begin
	rollback tran BSA_ImpUnprocAccount
	return @stat
  end


  -- insert Account record from unprocessed Account table
  insert into Account ( Id, Name, Monitor, 
			  ExemptionStatus, Notes, 
			  Branch, OpenDate, CreateOper, CreateDate, OwnerBranch, OwnerDept, 
			  OwnerOper, RiskFactor, Closed,
              CloseDate , closeReason ,
              Reference , Type ,
              Request , FreeLookEndDate ,
              ExpiryDate , PolicyAmount ,
              CashValue , SinglePremium, AccountOfficer,
        	  BalanceAmt, SourceofPayments, User1 , User2, User3, User4, User5,
			  AcStatus,InitialDeposit,TypeOfFunds ,ReqWireTrans ,AccACHPay ,
			  OrgACHPay ,NoOfDepositMnth  ,TotDepositAmtMnth  ,NoOfCashDepositMnth  ,
			  TotCashDepositAmtMnth ,NoOfCashWithDMnth  , TotCashWithDAmtMnth  ,
			  NoOfCheckWMnth  ,TotCheckWAmtMnth ,NoOfDomInWireMnth ,TotDomInWireAmtMnth  ,
			  NoOfDomOutWireMnth ,TotDomOutWireAmtMnth ,ReqForInWireTrans ,NoOfForInWireMnth  ,
			  TotForInWireAmtMnth ,OrigCntryForInWires ,ReqForOutWireTrans ,NoOfForOutWireMnth  ,
			  TotForOutWireAmtMnth  ,DestCntryForOutWires,IsProspect,
			  DescOfAcctPurpose, PowerOfAttorney, RemoteDepositCapture, RemoteDepositCaptureRiskRating) 
  select 
			Id, Name, Case When Monitor In ('1', 'true') Then 1 else 0 end, 
			NullIf(RTrim(ExemptionStatus), ''), NullIf(RTrim(Notes), ''), 
			Branch, dbo.BSA_ShortDateToInternationalizationDate(OpenDate),
			CreateOper, CreateDate, NullIf(RTrim(OwnerBranch), ''), NullIf(RTrim(OwnerDept), ''), 
			NullIf(RTrim(OwnerOper), ''), Riskfactor, 
            Case When Closed in ('1', 'True') Then 1 Else 0 end,
            dbo.BSA_ShortDateToInternationalizationDate(CloseDate) , closeReason ,
            NullIf(RTrim(Reference), '') , NullIf(RTrim([Type]), ''), 
            Case When Request in ('1', 'True') Then 1 Else 0 End, 
			  dbo.BSA_ShortDateToInternationalizationDate(FreeLookEndDate) ,
			  dbo.BSA_ShortDateToInternationalizationDate(ExpiryDate) , 
			  dbo.BSA_StringToInternationalizationMoney(PolicyAmount) ,
			  dbo.BSA_StringToInternationalizationMoney(CashValue) , 
			Case When SinglePremium In ('1', 'True') Then 1 Else 0 End, 
            NULLIF(RTRIM(AccountOfficer), ''),
	              NullIf(dbo.BSA_StringToInternationalizationMoney(BalanceAmt),0), 
                  NULLIF(RTRIM(SourceofPayments), ''), NullIf(User1,'') , NullIf(User2,''), NullIf(User3,''), NullIf(User4,''), NullIf(User5,''),
				  NULLIF(RTRIM(AcStatus), ''), NullIf(dbo.BSA_StringToInternationalizationMoney(InitialDeposit),0),
                  NullIf(TypeOfFunds,'') ,
            Case When ReqWireTrans In ('1', 'True') Then 1 Else 0 End, 
            Case When AccACHPay In ('1', 'True') Then 1 Else 0 End,
		    Case When OrgACHPay In ('1', 'True') Then 1 Else 0 End,
                  NullIf(NoOfDepositMnth,0)  ,
				  NullIf(dbo.BSA_StringToInternationalizationMoney(TotDepositAmtMnth),0)  ,
				  NullIf(NoOfCashDepositMnth,0)  ,
				  NullIf(dbo.BSA_StringToInternationalizationMoney(TotCashDepositAmtMnth),0) ,
                  NullIf(NoOfCashWithDMnth,0)  , 
				  NullIf(dbo.BSA_StringToInternationalizationMoney(TotCashWithDAmtMnth),0)  ,
				  NullIf(NoOfCheckWMnth,0)  ,
                  NullIf(dbo.BSA_StringToInternationalizationMoney(TotCheckWAmtMnth),0)  ,
                  NullIf(NoOfDomInWireMnth,0) ,
				  NullIf(dbo.BSA_StringToInternationalizationMoney(TotDomInWireAmtMnth),0)  ,
				  NullIf(NoOfDomOutWireMnth,0) ,
				  NullIf(dbo.BSA_StringToInternationalizationMoney(TotDomOutWireAmtMnth),0) ,
            Case When ReqForInWireTrans In ('1', 'True') Then 1 Else 0 End,
                  NullIf(NoOfForInWireMnth,0)  ,
				  NullIf(dbo.BSA_StringToInternationalizationMoney(TotForInWireAmtMnth),0) ,
                  NullIf(OrigCntryForInWires,'') ,
            Case When ReqForOutWireTrans In ('1', 'True') Then 1 Else 0 End, 
                  NullIf(NoOfForOutWireMnth,0)  ,
				  NullIf(dbo.BSA_StringToInternationalizationMoney(TotForOutWireAmtMnth),0)  ,
                  NullIf(DestCntryForOutWires,''),
			Case When IsProspect In ('1', 'True') Then 1 Else 0 End,
				  NullIf(RTrim([DescOfAcctPurpose]), ''), 
			Case When PowerOfAttorney In ('1', 'True') Then 1 Else 0 End, 
			Case When RemoteDepositCapture In ('1', 'True') Then 1 Else 0 End, 
			      NullIf(RTrim([RemoteDepositCaptureRiskRating]), '')
  from #tmpUnprocAccount UA
      Where Not Exists (Select * from Account ac Where AC.Id = UA.Id)

  Select @stat = @@error, @insertCount = @insertCount + @@rowcount

  -- Evaluate results of the transaction
  If @stat <> 0 begin
	rollback tran BSA_ImpUnprocAccount
	return @stat
  end

  -- delete the unprocessed Account that was imported
  delete from UnprocAccount
    from UnprocAccount UA, #tmpUnprocAccount UTN
    where UA.Id = UTN.Id 

  Select @stat = @@error, @delCount = @@rowcount

  -- Evaluate results of the transaction
  If @stat <> 0 begin
	rollback tran BSA_ImpUnprocAccount
	return @stat
  end

  -- Insert account owners
  Insert Into AccountOwner (Account, Cust, Relationship, CreateOper, 
                            CreateDate, LastOper, LastModify)
  Select Account, Cust, Relationship, CreateOper, CreateDate, LastOper, 
         LastModify
  From UnprocAccountOwner uao
  Where ((@pCode = '') OR (uao.account = @pCode)) And Not exists
  (Select * from AccountOwner ao where ao.account = uao.account
   And ao.Cust = uao.Cust And ao.Relationship = uao.Relationship)
  -- Evaluate results of the transaction
  Select @stat = @@error
  If @stat <> 0 begin
	rollback tran BSA_ImpUnprocAccount
	return @stat
  end

  -- delete account owners even if they did not gety imported. i.e. they exist
  Delete uao 
  From UnprocAccountOwner uao
  Where ((@pCode = '') OR (account = @pCode)) And Not exists
  (Select * from UnprocAccount ua where ua.Id = uao.account)
  -- Evaluate results of the transaction
  Select @stat = @@error
  If @stat <> 0 begin
	rollback tran BSA_ImpUnprocAccount
	return @stat
  end

  -- get rid of temp table
  drop table #tmpUnprocAccount

  -- make sure insert and delete counts are equal
  if (@insertCount <> @delCount)
  begin
	rollback tran BSA_ImpUnprocAccount
	return -2 -- insert and delete counts not equal
  end

  if (@pCode = '')
      select @objectId = convert(varchar, @InsertCount) + ' records'
  else
      select @objectId = convert(varchar, @pCode)
  Exec BSA_InsEvent @oper, 'Imp', 'UnAccount', @objectId, 
      'Unprocessed Account record(s) imported into Account table'

  Select @stat = @@error
  If @stat <> 0 begin
	rollback tran BSA_ImpUnprocAccount
	return @stat
  end

  set @retCount = @insertCount

  If @trnCnt = 0
    commit tran BSA_ImpUnprocAccount

  return IsNull(@stat, 0)
Go


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Completed conversion of Pbsa objects To Version 10.1'
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

