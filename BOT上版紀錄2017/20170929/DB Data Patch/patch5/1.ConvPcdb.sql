/*
**  File Name:        ConvPCDB.sql
**
**  Functional Description:
**
**  This module contains SQL conversion procedures for Prime version 10.1 
**
**  Creation Date:    08/25/2016
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2011                                                   ***
*** FIS               			                                 		 ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in FIS.                                            					 ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by FIS.					 ***
***                                                                      ***
****************************************************************************
                       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|----------------------------------------------------     

08/25/2016		SS		Defect #6023: Fixed the Invalid character present in the 
						description of the databank option 						
12/21/2016      SM      I160837162: updated the table name in Join condition
						to display the all unproc accounts when Exemption status
						is selected from More Columns link.
12/21/2016      SM      I160713492: Updated the table name in ColumnExpression 
						to display 'All customer' when Exempt from selected from
						More columns link.
12/22/2016      SM      I160411626: Changed the OFAC Purge Button tooltip to display the appropriate message.
05/16/2017      SS      I170238733: Created to support Attachment in PCDB Options. 
                       
*/


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Starting Conversion of PCDB Database to version 10.1.0 '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

Use PCDB
GO

Set ANSI_NULLS ON
Go

Set NOCOUNT ON 
Go

If exists (SELECT top 1 * FROM dbo.CustomSettings with (nolock) WHERE Code='OFACOptions' and Product='OFAC' and SettingCode='DATABNKOPT')
	update CustomSettings set [Key]='DataBank Option - Configure system for usage of Prime DataBank Lists/User Defined Lists'
	where Code='OFACOptions' and Product='OFAC' and SettingCode='DATABNKOPT'

go

--UnProcessed accounts error
IF EXISTS(SELECT * FROM ListViewColumns WHERE  TableId = 850 and ColumnName = 'ExemptFrom' and Position = 37)
update Pcdb.dbo.ListViewColumns set joinCondition = 'LEFT OUTER Join ExemptionType ON UnProcAccount.ExemptionStatus = ExemptionType.Code' where  tableID = 850
and Columnname = 'ExemptFrom' and Position = 37

go

--All Customer error
If Exists (Select * from Pcdb.dbo.ListViewColumns where Tableid  = 23 And 
		TableName = 'Customer' and [Position]= 91)
update Pcdb.dbo.ListViewColumns set ColumnExpression = 'Customer.Type' where  Tableid  = 23 And 
		TableName = 'Customer' and [Position]= 91
		
go
-- OFAC Purge Button tooltip change
If Exists (Select * from Pcdb.dbo.ViewTasks where PageId = 5 and ViewId = 136)
update Pcdb.dbo.ViewTasks set TaskId = 1146 where PageId = 5 and ViewId = 136

go

--All ReqPatriotActCert	
If Exists (Select * from ListViewColumns where Tableid  = 814 And 
	TableName = 'vwReqPatriotActCert' and [Position]= 91)
update Pcdb.dbo.ListViewColumns set ColumnExpression = 'vwReqPatriotActCert.Type' where  Tableid  = 814 And 
	TableName = 'vwReqPatriotActCert' and [Position]= 91
go

-- Creating a new PCDBOptions page to set Number of Attachments for Common Attachment Option 
IF NOT EXISTS(SELECT * FROM PageTbl WHERE PageId=2021)
insert into PageTbl values (2021, 'PCDBOptions.aspx', NULL, NULL, 1) 
GO
 
--Add record to ControlTbl for the created Page ID
IF NOT EXISTS(SELECT * FROM ControlTbl WHERE ControlId=2021)
 INSERT INTO ControlTbl VALUES (2021, NULL, NULL,4, 2021, 4, NULL, NULL) 
 GO
-- Update the controlid of PCDBOptions.aspx for the vwPCDBOptions page.
IF EXISTS(SELECT ViewId from ViewColumns with(nolock) where viewid=452 and columnid=0 and tableid=452 )
update ViewColumns set controlid=2021 where viewid=452 and columnid=0 and tableid=452 
GO

--Add Attachment option in Custom Objects
IF NOT EXISTS(SELECT * FROM CustomObjects WHERE Code='AttachOpt')
 INSERT INTO CustomObjects
(Code, Description, Enabled, CreateDate, CreateOper, LastModifDate, LastOper)
 values
 ('AttachOpt', 'Number of Attachments', 1, GETDATE(), 'PRIME', GETDATE(),'PRIME') 

 GO

--Add Attachment option in Custom Settings
IF NOT EXISTS(SELECT * FROM CustomSettings WHERE Code='AttachOpt' and Product='CDB' and SettingCode='MaxNoAttch')
 insert into CustomSettings
 (Code, Product, RecordType, OperCode, [Key], Value, IsUserEditable, SettingCode, CreateDate, CreateOper, LastModifDate, LastOper)
 Values
 ('AttachOpt','CDB','SYSTEM','','Maximum Number of Attachments to be added to a single document', 5, 1, 'MaxNoAttch', GETDATE(), 'PRIME', GETDATE(), 'PRIME') 
 GO
 
--Added Attachment under PCDB Options
 if not exists (select * from OptionTbl where Code='Attachment')
 INSERT INTO
 OptionTbl
 (Code, Name, Enabled, ModEnable, CreateOper, CreateDate, LastOper, LastModify)
 VALUES
 ('Attachment', 'Attachment', 0, 1, 'Prime', getdate(), null, null)
GO

--Update the ViewParameters to display the all the Common Options in vwPCDBOptions page
IF EXISTS(SELECT ViewId from ViewParameters with(nolock) where ViewId = 452)
update ViewParameters set SQLOperationId = 11, ParameterValue ='DashBoard, Attachment' where ViewId = 452

go

PRINT ''
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT 'Completed Conversion of PCDB Database to version 10.1.0  '
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT ''
GO



