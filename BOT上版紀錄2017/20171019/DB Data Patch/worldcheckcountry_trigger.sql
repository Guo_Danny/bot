USE [OFAC]
GO

/****** Object:  Trigger [dbo].[WorldCheckCountryModif]    Script Date: 10/25/2017 11:27:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[WorldCheckCountryModif] ON [dbo].[WorldCheckCountry]
FOR UPDATE
AS

  update [dbo].[WorldCheckCountry] set LastDate = convert(nchar(10),GETDATE(), 120)
  from inserted u , WorldCheckCountry w where u.Code = w.Code


GO
