USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[USR_U1MnthBetXY]    Script Date: 10/6/2017 11:03:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[USR_U1MnthBetXY] (
	@WLCode 		SCode, 
	@TestAlert 		INT,
	@activityTypeList 	VARCHAR(8000), 
	@minimumAmount 		MONEY, 
	@maximumAmount 		MONEY,
	@CustTypeList		Varchar(8000),
	@RiskClassList 		Varchar(8000),
	@BranchList 		Varchar(8000)

)
AS

	/* RULE AND PARAMETER DESCRIPTION
	    Detects accounts that had transactions using a specified 
		activity type and the total of these transactions were between a designated 
		dollar amount range within 1 calender month.  Designed to be run Post-EOD.
				@activityTypeList	- List of activity types to be detected.
				@minimumAmount	- Minimum amount
				@maximumAmount	- Maximum amount
				@CustTypeList - List of Customer Types that will be considered. 
						   -ALL-, blank or NULL for all Customer Types
				@RiskClassList	- List of Risk Classes that will be considered. 
						   -ALL-, blank or NULL for all Risk Classes
				@BranchList	- List of Branches that will be considered. 
						   -ALL-, blank or NULL for all Branches
				[Return] INT = Return the error status.  0 for success.
		
	*/

	/*  Declarations */
	DECLARE	@description 	VARCHAR(2000),
	    	@desc 		VARCHAR(2000),
	    	@Id 		INT, 
	    	@WLType 	INT,
	    	@stat 		INT,
	   		@trnCnt 	INT,
	    	@minDate 	INT,
	    	@maxDate 	INT
	DECLARE @STARTALRTDATE  DATETIME
	DECLARE @ENDALRTDATE    DATETIME
	
	If @maximumAmount = -1
	Begin
		set @maximumAmount = 99999999999999;
	End
	
	SET NOCOUNT ON
	SET @stat = 0
	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE,@StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode

	SET @minDate = dbo.FirstDayOfMonth(DATEADD(MONTH, -1,@StartDate))
	SET @maxDate = dbo.FirstDayOfMonth(@StartDate)
	
	--- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */
	SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
	    BEGIN TRAN USR_U1MnthBetXY
	ELSE
	    -- Already in a transaction
	    SAVE TRAN USR_U1MnthBetXY
	/* End standard stored procedure transaction header */
	
	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)
	SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)

	DECLARE	@TT TABLE 
	(
	   Account 	VARCHAR(40),
	   Recvpay 	INT,
	   BaseAmt 	MONEY,
	   TranNo 	INT,
           SortValue 	INT
	)
	
	DECLARE	@TTMerge TABLE 
	(
	   Account 	VARCHAR(40),
	   Recvpay 	INT,
	   TotalAmt 	MONEY,
	   SortValue 	INT
	)
	-- Temporary table of Activity Types that have not been specified as Exempt
	DECLARE @ActType TABLE 
	(
	  Type		INT
	)	

	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	INSERT INTO @TT ( Account, SortValue, BaseAmt, TranNo, RecvPay)
		SELECT 	Account, FLOOR(bookDate / 100) * 100 + 1 SortValue, BaseAmt, TranNo, RecvPay 
		FROM 	ActivityHist A 
		INNER	JOIN Customer ON Customer.ID = A.Cust
		INNER	JOIN Account ON Account.ID = A.Account
		INNER	JOIN @ActType Act ON Act.Type = A.Type
		WHERE   BookDate >= @minDate and BookDate < @maxdate 
		AND	((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) 
		AND	((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) 
		AND
			((ISNULL(@BranchList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Account.Branch)) + ',', @BranchList ) > 0)) 
		
	INSERT INTO @TTMerge (Account, RecvPay, TotalAmt, SortValue)
		SELECT 	Account, RecvPay, SUM(BaseAmt), SortValue
		FROM 	@TT
		WHERE 	Account IS NOT NULL
		GROUP 	BY account, sortValue, recvpay
		HAVING 	@minimumAmount <= SUM(baseamt) AND SUM(baseamt) <= @maximumAmount	
	
	--1 means test, 0 means no
	IF @testAlert = 1 
	BEGIN
		  SELECT @STARTALRTDATE = GETDATE()
		   INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT, IsTest) 
		   SELECT @WLCode, CASE WHEN Recvpay =1 THEN 'Account: ''' + Account + ''' had transactions totaling ''' 
		    	+ CONVERT(VARCHAR, TotalAmt) + 
		        ''' for the MONTH of ''' + CONVERT(VARCHAR, SortValue) +
		        ''' of incoming types specified '  ELSE
		        'Account: ''' + Account + ''' had transactions totaling ''' 
		        + CONVERT(VARCHAR, TotalAmt) + 
		        ''' for the MONTH of ''' + CONVERT(VARCHAR, SortValue) +
		        ''' of outgoing types specified '  END , 0, 
		        GETDATE(), NULL, NULL, 
				(select top 1 cust from AccountOwner where Account = m.Account), Account , 1
		   FROM @TTMerge as m
	
	
		SELECT @STAT = @@ERROR	
		SELECT @ENDALRTDATE = GETDATE()
	
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	'Alert', AlertNo, TranNo
		    	FROM 	@TTMerge A 
			INNER 	JOIN @TT B ON A.SortValue = B.SortValue 
			AND	A.Account = B.Account 
			AND	A.RecvPay = B.RecvPay 
			INNER 	JOIN Alert ON A.Account = Alert.Account 
			AND	Alert.WLCode = @WLCode 
			AND	Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
			WHERE 	A.RecvPay = CASE WHEN Alert.[desc] LIKE '%of outgoing type%' THEN 2
				ELSE 1 END	
		
		SELECT @STAT = @@ERROR 
	
		
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 
		BEGIN
			SELECT @STARTALRTDATE = GETDATE()
		   	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT) 
		   	SELECT 	@WLCode, CASE WHEN Recvpay =1 THEN 'Account: ''' + Account + ''' had transactions totaling ''' 
			    	+ CONVERT(VARCHAR, TotalAmt) + 
			        ''' for the MONTH of ''' + CONVERT(VARCHAR, SortValue) +
			        ''' of incoming types specified '  ELSE
			        'Account: ''' + Account + ''' had transactions totaling''' 
			        + CONVERT(VARCHAR, TotalAmt) + 
			        ''' for the MONTH of ''' + CONVERT(VARCHAR, SortValue) +
			        ''' of outgoing types specified '  END , 0, 
			        GETDATE(), NULL, NULL, 
					(select top 1 cust from AccountOwner where Account = m.Account), Account 
		   	FROM 	@TTMerge as m
		
			SELECT @STAT = @@ERROR	
			SELECT @ENDALRTDATE = GETDATE()
			IF @STAT <> 0  GOTO ENDOFPROC
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
		    SELECT @STARTALRTDATE = GETDATE()
		    	INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			        ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			        RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			        ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			        EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			        OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			        REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			        WLCode, WLDESC, CREATETIME )
		    	SELECT	NULL, dbo.ConvertSqlDateToInt(@StartDate), 
				    (select top 1 cust from AccountOwner where Account = m.Account), Account,
			        NULL, 'RULE', NULL, NULL, NULL, NULL,
			        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			        NULL, NULL, 0, 0, 0, 0, 
			        NULL, NULL, NULL, NULL, 0, NULL, NULL,
			        @WLCode, CASE WHEN Recvpay =1 THEN 'Account: ''' + Account + ''' had transactions totaling ''' 
			        + CONVERT(VARCHAR, TotalAmt) + ''' of incoming types specified ' + 
			        ''' for the MONTH of' + CONVERT(VARCHAR, SortValue) ELSE
			        'Account: ''' + Account + ''' had transactions totaling''' 
			        + CONVERT(VARCHAR, TotalAmt) + 
			        ''' for the MONTH of ''' + CONVERT(VARCHAR, sortvalue) +
			        ''' of outgoing types specified ' + 
			        ''' on ' + CONVERT(VARCHAR, sortvalue) END , GETDATE() 
		   	FROM @TTMerge as m
	
			SELECT @STAT = @@ERROR	
			SELECT @ENDALRTDATE = GETDATE()
			IF @STAT <> 0  GOTO ENDOFPROC
		END
		
		IF @WLType = 0 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   		SELECT 	'Alert', AlertNo, TranNo
		    		FROM 	@TTMerge A 
				INNER 	JOIN @TT B ON A.SortValue = B.SortValue 
				AND	A.Account = B.Account 
				AND	A.RecvPay = B.RecvPay 
				INNER 	JOIN Alert ON A.Account = Alert.Account 
				AND	Alert.WLCode = @WLCode 
				AND	 Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
				WHERE 	A.recvpay = CASE WHEN Alert.[desc] LIKE '%of outgoing type%' THEN 2
					ELSE 1 END	
		
			SELECT @STAT = @@ERROR 
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		    		SELECT 	'SUSPACT', RecNo, TRANNO 
		    		FROM 	@TTMerge A 
				INNER 	JOIN @TT B ON A.SortValue = B.SortValue 
				AND	A.Account = B.Account 
				AND	A.RecvPay = B.RecvPay 
				INNER 	JOIN SuspiciousActivity S ON A.Account = S.Account 
				AND	S.WLCode = @WLCode 
				AND	S.CREATETIME BETWEEN @STARTALRTDATE AND @ENDALRTDATE
				WHERE 	A.recvpay = CASE WHEN S.wldesc LIKE '%of outgoing type%' THEN 2
					ELSE 1 END
			SELECT @STAT = @@ERROR 
		END
	END
	
	EndOfProc:
	IF (@stat <> 0) BEGIN 
	    ROLLBACK TRAN USR_U1MnthBetXY
	    RETURN @stat
	END	
	
	IF @trnCnt = 0
	    COMMIT TRAN USR_U1MnthBetXY
	RETURN @stat

GO
