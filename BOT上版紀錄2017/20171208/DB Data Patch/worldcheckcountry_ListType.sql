USE [OFAC]
GO

insert into ListType (code,name,Description,IsPrimeList,Enabled,CreateOper,CreateDate,LastOper,LastModify)
VALUES
('0282CIF1','0282 PEP for CIF','0282 PEP for CIF',0,1,'Prime',getdate(),'Prime',getdate())

insert into ListTypeAssociation(ListType)
VALUES
('0282CIF1')

insert into ListType (code,name,Description,IsPrimeList,Enabled,CreateOper,CreateDate,LastOper,LastModify)
VALUES
('0282CIF2','0282 PEP for CIF','0282 PEP for CIF',0,1,'Prime',getdate(),'Prime',getdate())

insert into ListTypeAssociation(ListType)
VALUES
('0282CIF2')

insert into ListType (code,name,Description,IsPrimeList,Enabled,CreateOper,CreateDate,LastOper,LastModify)
VALUES
('0282CIF3','0282 PEP for CIF','0282 PEP for CIF',0,1,'Prime',getdate(),'Prime',getdate())

insert into ListTypeAssociation(ListType)
VALUES
('0282CIF3')
