USE [OFAC]
GO

ALTER TABLE WorldCheckCountry
  ADD ListType VARCHAR(50)

/****** CIF3 ******/
select * from WorldCheckCountry
where WorldCheck_Country in (
'USA',
'MEXICO',
'BRAZIL',
'CHINA',
'ARGENTINA',
'RUSSIAN FEDERATION'
)




/****** CIF2 ******/
select * from WorldCheckCountry
where WorldCheck_Country in (
'PERU',
'INDIA',
'COLOMBIA',
'CHILE',
'ITALY',
'UNITED KINGDOM',
'INDONESIA',
'CANADA',
'GERMANY',
'SPAIN',
'UKRAINE',
'FRANCE',
'SWITZERLAND',
'PHILIPPINES',
'ECUADOR',
'AUSTRALIA',
'POLAND',
'VENEZUELA',
'ROMANIA',
'THAILAND',
'VIET NAM',
'JAPAN',
'NIGERIA',
'HUNGARY',
'BOLIVIA'
)

/****** CIF1 ******/
select * from WorldCheckCountry
where listtype is NULL
