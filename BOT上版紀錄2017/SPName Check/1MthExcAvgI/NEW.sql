USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[USR_1MthExcAvgI]    Script Date: 11/24/2016 2:17:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USR_1MthExcAvgI] (@WLCode SCode, @TestAlert INT,
												@activityTypeList varchar(8000), 
												@CustTypeList  Varchar(8000),
												@RiskClassList Varchar(8000),
												@TypeOfBusinessList Varchar(8000),
												@BranchList Varchar(8000),
												@RecvPay Int,
												@NumMonths int,
												@ExcAmtThreshold Money,
												@InclLastMth tinyint

)
AS
/* RULE AND PARAMETER Description
    Detects Accounts with wire transfers between specified amount range
    AND for over a period of a month.
	@ActivityTypeList	- Type of activity to be detected.
	@CustTypeList - List of Customer Types that will be considered.
					-ALL-, blank or NULL for all Customer Types
	@RiskClassList	- List of Risk Classes that will be considered. 
					-ALL-, blank or NULL for all Risk Classes
	@TypeOfBusinessList	-- List of type of businesses that will be considered. 
			   			-ALL-, blank or NULL for all types
   	@BranchList	- List of Branches that will be considered. 
					-ALL-, blank or NULL for all Branches
	@NumMonths	- Number of Months to be evaluated
	@ExcAmtThreshold - Amount by which Avereage Amount should exceed aggregate for the month
	@InclLastMth	- Include Last Month in Average

*/
/*  Declarations */
	DECLARE	@Description VARCHAR(2000),
		    @Desc VARCHAR(2000),
		    @Id INT, 
		    @WLType INT,
		    @Stat INT,
		    @TrnCnt INT,
		    @MinDate INT,
		    @MaxDate INT,
		    @MinDateAvg INT,
		    @MaxDateAvg INT,
		    @ActivityType  INT,
	            @Pos	  INT

	DECLARE @StartAlrtDate  DATETIME
	DECLARE @EndAlrtDate    DATETIME

	SET NOCOUNT ON
	SET @Stat = 0

	--- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */
	SET @TrnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @TrnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN USR_1MthExcAvgI
	ELSE
		-- Already in a transaction
		SAVE TRAN USR_1MthExcAvgI
	/* End stANDard stored procedure transaction header */

	/*****************************************/
	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	SET 	@StartDate = GETDATE()
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE, @StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode
	/******************************************/

	SET @MinDate = dbo.FirstDayOfMonth(DATEADD(MONTH, -1,@StartDate))
	SET @MaxDate = dbo.FirstDayOfMonth(@StartDate)

	IF @InclLastmth = 1
	BEGIN
		SET @MaxDateAvg = @MaxDate
		SET @MinDateAvg = dbo.ConvertSqlDateToInt(DATEADD(MM, @NumMonths * -1, 
				dbo.BSA_ConvertintToSqlDate(@MaxDate,'MM/DD/YYYY')))
	END
	ELSE
	BEGIN
		SET @MaxDateAvg = @MinDate
		SET @MinDateAvg = dbo.ConvertSqlDateToInt(DATEADD(MM, @NumMonths * -1, 
				dbo.BSA_ConvertintToSqlDate(@MinDate,'MM/DD/YYYY')))
	END

	-- Call BSA_fnListParams for each of the Paramters that support comma separated values

	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)
	SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
	SELECT @TypeofBusinessList = dbo.BSA_fnListParams(@TypeofBusinessList)	
	
	Create table #TT (
		   Account varchar(40),
		   Recvpay INT,
		   BaseAmt MONEY,
		   TranNo INT,
		   SortValue INT
	)
	DECLARE	@TT1 TABLE (
		   Account varchar(40),
			Recvpay INT,
		   SumBaseAmt MONEY,
		   SortValue INT
	)
	DECLARE	@TTMerge TABLE (
		   Account varchar(40),
		   Recvpay INT,

		   TotalAmt MONEY,
		   SortValue INT
	)
	DECLARE	@TTMerge1 TABLE (
		   Account varchar(40),
		   Recvpay INT,
		   AvgTotalAmt MONEY
	)
	DECLARE @ActType TABLE
	(
		Type INT
	)

		--If RecvPay = 3 Consider both Deposits ans WithDrawals
		IF (@RecvPay = 3)
		SET @RecvPay = NULL

	-- Get all the Activity Types that have not been exempted.
	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	-- Get the transactions for the last month that match the input criteria specified
	INSERT INTO #TT ( Account, SortValue, BaseAmt, TranNo, RecvPay)
		SELECT 	AH.Account, FLOOR(BookDate / 100) * 100 + 1 SortValue, BaseAmt, TranNo, RecvPay 
		FROM 	ActivityHist AH (NOLOCK)
			INNER JOIN Account Ac ON Ac.[Id] = AH.Account
			INNER JOIN @ActType Act ON AH.Type = Act.Type
			INNER JOIN Customer C ON AH.Cust = C.[Id]
		WHERE	BookDate >= @MinDate AND BookDate < @MaxDate
			AND	((ISNULL(@CustTypeList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(C.Type)) + ',', @CustTypeList ) > 0)) 		
			AND	((ISNULL(@RiskClassList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(C.RiskClass)) + ',', @RiskClassList ) > 0)) 
			AND	((ISNULL(@TypeOfBusinessList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(C.TypeOfBusiness)) + ',', @TypeOfBusinessList ) > 0)) 
			AND	((ISNULL(@BranchList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(Ac.Branch)) + ',', @BranchList ) > 0))
		
	-- Get the aggregate amounts for the transactions in the last month
	INSERT INTO @TTMerge (Account, RecvPay, TotalAmt, SortValue)
	SELECT 	Account, RecvPay, SUM(BaseAmt), SortValue
	FROM 	#TT
	WHERE 	Account IS NOT NULL
	GROUP 	BY Account, sortValue, RecvPay

	-- Get the average monthly total for all the accounts for the period specified; 
	INSERT INTO @TTMerge1 (Account, RecvPay, AvgTotalAmt)
		SELECT Account, RecvPay, AVG(SumBaseAmt) AvgTotalAmt
		FROM 	(
			SELECT 	AH.Account,  Sum(AH.BaseAmt) SumBaseAmt,AH.RecvPay
			FROM 	ActivityHist AH (NOLOCK)
			INNER JOIN @ActType Act ON AH.Type = Act.Type
			INNER JOIN Customer C ON AH.Cust = C.[Id]
			WHERE  	AH.BookDate >= @MinDateAvg AND AH.BookDate < @MaxDateAvg
			AND		EXISTS (SELECT NULL FROM #TT T WHERE T.Account = AH.Account)
			AND	((ISNULL(@CustTypeList, '') = '' OR
						CHARINDEX(',' + LTRIM(RTRIM(C.Type)) + ',', @CustTypeList ) > 0)) 		
			AND	((ISNULL(@RiskClassList, '') = '' OR
						CHARINDEX(',' + LTRIM(RTRIM(C.RiskClass)) + ',', @RiskClassList ) > 0)) 
			AND	((ISNULL(@TypeOfBusinessList, '') = '' OR
						CHARINDEX(',' + LTRIM(RTRIM(C.TypeOfBusiness)) + ',', @TypeOfBusinessList ) > 0)) 
			GROUP 	BY AH.Account,  FLOOR(BookDate / 100) * 100 + 1,AH.RecvPay
				) TT1
		GROUP 	BY Account, RecvPay

--1 means test, 0 means no
	IF @testAlert = 1 Or @WLType = 0
	BEGIN
		SELECT @StartAlrtDate = GETDATE()
		INSERT INTO Alert ( WLCode, [DESC], Status, CreateDate, LastOper, 
			LastModify, Cust, Account, IsTest) 
		   SELECT @WLCode, 'Account: ''' + T1.Account + ''' had transactions totalling ''' 
				+ CONVERT(VARCHAR, T1.TotalAmt) + 
				''' for the month of ''' + CONVERT(VARCHAR, SortValue) + ''' of ' + 
				CASE WHEN T1.RecvPay = 1 THEN 'incoming' 
					WHEN T1.RecvPay = 2 THEN 'outgoing' 
				END + ' types specified where the average monthly total was exceeded by over '
			+ CONVERT(VARCHAR, @ExcAmtThreshold) , 0, GETDATE(), NULL, NULL, NULL, T1.Account , @testAlert
		   FROM @TTMerge T1, @TTMerge1 T2
		   WHERE T1.Account = T2.Account
			AND T1.RecvPay = T2.RecvPay AND T1.RecvPay = ISNULL(@RecvPay,T1.RecvPay)
			AND (T1.TotalAmt - T2.AvgTotalAmt) > @ExcAmtThreshold

		SELECT @STAT = @@ERROR	
		SELECT @EndAlrtDate = GETDATE()

		INSERT INTO SASActivity (ObjectType, ObjectID, TranNo)
		SELECT 	'Alert', AlertNo, TranNo
		FROM 	@TTMerge a 
			INNER JOIN #TT b ON a.SortValue = b.SortValue 
				AND a.Account = b.Account 
				AND a.RecvPay = b.RecvPay 
			INNER JOIN Alert ON a.Account = Alert.Account 
		WHERE Alert.WLCode = @WLCode 
				AND Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
				AND a.RecvPay = CASE WHEN Alert.[DESC] like '%incoming%' THEN 1
									 WHEN Alert.[DESC] like '%outgoing%' THEN 2
								END					
		SELECT @STAT = @@ERROR 
	END
	ELSE IF @WLTYPE = 1 
	BEGIN
		SELECT @StartAlrtDate = GETDATE()
		INSERT INTO SuspiciousActivity (ProfileNo, BookDate, Cust, Account, 
			Activity, SuspType, StartDate, EndDate, RecurType, 
			RecurValue, ActCurrReportAmt, ActInActCnt, ActOutActCnt, 
			ActInActAmt, ActOutActAmt, CurrReportAmt, ExpAvgInActCnt, 
			ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt, InCntTolPerc, 
			OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr, ReviewState, 
			ReviewTime, ReviewOper, App, AppTime, AppOper, 
			WLCode, WLDesc, CreateTime )
			SELECT	NULL, SortValue, 
			    (select cust from accountowner where account = T1.Account), 
			    T1.Account,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, 'Account: ''' + T1.Account + ''' had transactions totalling ''' 
				+ CONVERT(VARCHAR, T1.TotalAmt) + 
				''' for the month of ''' + CONVERT(VARCHAR, SortValue) +
				''' of ' + CASE WHEN T1.RecvPay = 1 THEN 'incoming' 
								WHEN T1.RecvPay = 2 THEN 'outgoing' 
							END 
				+ ' types specified where the average monthly total was exceeded by over '
				+ CONVERT(VARCHAR, @ExcAmtThreshold)   , GETDATE() 
			FROM 	@TTMerge T1, @TTMerge1 T2
			WHERE 	T1.Account = T2.Account
				AND 	T1.RecvPay = T2.RecvPay AND T1.RecvPay = ISNULL(@RecvPay,T1.RecvPay)
				AND 	(T1.TotalAmt - T2.AvgTotalAmt) > @ExcAmtThreshold

		SELECT @STAT = @@ERROR	
		SELECT @EndAlrtDate = GETDATE()
		IF @STAT <> 0  GOTO EndOfProc

		INSERT INTO SASActivity (ObjectType, ObjectID, TranNo)
			SELECT 	'SUSPACT', RecNo, TranNo 
			FROM 	@TTMerge a INNER JOIN #TT b ON a.SortValue = b.SortValue AND
				 a.Account = b.Account AND
				 a.RecvPay = b.RecvPay  
				INNER 	JOIN SuspiciousActivity s ON a.Account = s.Account 
			WHERE 	S.WLCode = @WLCode 
				AND S.CreateTime BETWEEN @StartAlrtDate AND @EndAlrtDate
				AND a.RecvPay = CASE WHEN s.[WLDesc] like '%incoming%' THEN 1
									 WHEN s.[WLDesc] like '%outgoing%' THEN 2
								END	

		SELECT @STAT = @@ERROR 		
	END

EndOfProc:
IF (@Stat <> 0) BEGIN 
    ROLLBACK TRAN USR_1MthExcAvgI
    RETURN @Stat
END	

IF @TrnCnt = 0
    COMMIT TRAN USR_1MthExcAvgI

drop table #tt

RETURN @Stat




 


GO

