USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[USR_IMultiBene]    Script Date: 11/17/2016 18:31:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[USR_IMultiBene](
	@WLCode SCode, 
	@testAlert INT,
	@period varchar(25),
	@startDate varchar(10),
	@endDate varchar(10),  
    @minIndCount int,
	@minIndAmt money, 
    @maxIndAmt money,
	@minSumAmount money,
	@maxSumAmount money,
	@riskClassList varchar(8000),
	@customerTypeList varchar(8000),
	@accountTypeList varchar(8000),
	@activityTypeList varchar(8000),
    @countryList varchar(8000),
	@recvpay INT,
	@branchList varchar(8000),
    @deptList varchar(8000),
	@cashType smallint,
	@NumBene smallint
	)

AS


/* RULE AND PARAMETER DESCRIPTION
Detects a single Originator sending transactions to one or more 
Beneficiaries. A transaction is included in the evaluation based on its 
qualification with: specified transaction types, a specified time period,
the amount exceeds a certain amount, and the cumulative amount of all the
transactions meeting these constraints is over a specified amount.  
Additional constraints include Country, Customer Type, Risk Class, 
Account Types, Cash/Non-Cash, Branch and Department. The alert/case
will display the account information in the description since the account
may not be held at the processing institution. Designed to be run Post-EOD.

	@period = Specifies the previous period accordingly. It Can be left blank,
		Week, Month, Quarter and Semi Annual. If blank the StartDate and EndDate 
  		must be specified. 
	@startDate = Specifies the Starting Date range of transactions to evaluate,  
    		uses the book date. Relevant only if the Period is not specified.
	@endDate = Specifies the Ending Date range of the transactionAs book date.
		Relevant only if the Period is not specified.
    	@minIndCount = The Minimum individual transaction count, after applying 
		the Min-Ind-Amt and Max-Ind-Amt parameters
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
   	@maxIndAmt = The Maximum amount the individual transaction cannot exceed 
		to be included in the aggregation.  For no maximum use -1
	@minSumAmount = The Minimum aggregated transaction amount that must be 
		exceeded.  This includes those transactions that are between Min-Ind-Amt
		and Max-Ind-Amt.
	@maxSumAmount = The Maximum aggregated transaction amount that cannot be 
		exceeded. For no maximum use -1
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@customerTypeList = A comma separated list of Customer Types to include 
		in the evaluation, use -ALL- for any customer type.
	@accountTypeList = A comma separated list of Account Types to include 
		in the evaluation, use -ALL- for any account type.
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.
    	@countryList = A comma separated list of Countries to include in the 
		evaluation, use -ALL- for any country. Fields evaluated are 
		BeneCountry and ByOrderCountry.
	@recvpay = Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both
	@branchList = A comma separated list of Branches to include in the evaluation.
	@deptList = A comma separated list of Departments to include in 
		the evaluation.
	@cashType = Specifies Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both
	@NumBene - Number of Beneficiaries. Alert is generated only if payments are sent to specified number or more Beneficiaries.
*/
	
/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@fromDate INT,
 	@toDate INT,
	@tranAmt MONEY,
	@tranCnt INT,
	@trnCnt INT,
	@Cust VarChar(35),
	@OwnerBranch VarChar(11),
	@OwnerDept VarChar(11)
DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME

DECLARE @SetDate DATETIME

DECLARE @TransTbl TABLE (
	TranNo INT,
	Bene VARCHAR(40),
	ByOrder VARCHAR(40),
	ByOrderCustId VARCHAR(40),
	BookDate INT,
	BaseAmt MONEY,
	Cust VARCHAR(40),
	OwnerBranch VARCHAR(11),
	OwnerDept VARCHAR(11)
)

DECLARE	@TT TABLE (
	tranAmt MONEY,
	tranCnt INT,
	byOrder  VARCHAR(40),
	ByOrderCustId VARCHAR(40),
	CustCount INT,
	BranchCount INT,
	DeptCount INT,
	Cust VARCHAR(40),
	OwnerBranch VARCHAR(11),
	OwnerDept VARCHAR(11)
)

DECLARE @CustomerExemptionsList VARCHAR(8000)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_IMultiBene
ELSE
	-- Already in a transaction
	SAVE TRAN USR_IMultiBene
/* End standard stored procedure transaction header */

/*  standard Rules Header */
-- Date options

-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @description = [Desc], @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList (NOLOCK)
WHERE WLCode = @WLCode

Declare @BaseCurr char(3)
select @BaseCurr = IsNULL(BaseCurr,'') from SysParam

If(@period is null OR  ltrim(rtrim(@period)) = '') 
BEGIN
	SET @fromDate = dbo.ConvertSqlDateToInt(@startDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@endDate)
END
ELSE
BEGIN
	DECLARE @SQLStartDate datetime, @SQLEndDate datetime
	exec dbo.BSA_GetDateRange @SetDate, @period, 'PREV', 1, 
			@SQLStartDate OUTPUT, @SQLEndDate OUTPUT
	SET @fromDate = dbo.ConvertSqlDateToInt(@SQLStartDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@SQLEndDate)
END

SELECT @riskClassList = dbo.BSA_fnListParams(@riskClassList)
SELECT @customerTypeList = dbo.BSA_fnListParams(@customerTypeList)
SELECT @accountTypeList = dbo.BSA_fnListParams(@accountTypeList)
SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)
SELECT @countryList = dbo.BSA_fnListParams(@countryList)
SELECT @branchList = dbo.BSA_fnListParams(@branchList)
SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)

SELECT @CustomerExemptionsList = dbo.BSA_GetExemptionCodes('Customer','Rule')
IF ltrim(rtrim(@CustomerExemptionsList)) = ''
	SET @CustomerExemptionsList = NULL

IF (@cashType = 2)
	SET @cashType = NULL

IF (@recvpay = 3)
	SET @recvpay = NULL

Insert Into @TransTbl(TranNo, Bene, ByOrder, BookDate, BaseAmt, Cust, OwnerBranch, OwnerDept, ByOrderCustId)
Select TranNo, Bene, ByOrder, BookDate, BaseAmt, Cust, a.Branch, Dept, ByOrderCustId
FROM ActivityHist a (NOLOCK)
Join Customer (NOLOCK) ON 	a.cust = Customer.Id
Left Join Account Ac (NOLOCK) ON a.Account = ac.Id
WHERE  	a.bookdate >= @fromDate AND 
	a.bookdate <= @toDate AND 
	a.recvpay = ISNULL(@recvPay, a.recvpay) AND
	a.BaseAmt >= @minIndAmt AND
    (@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
	a.byOrder IS NOT NULL and 
	a.bene IS NOT NULL AND  
	(@deptList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR,  a.dept) + ',',@deptList) > 0) AND
	(@branchList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR,  a.branch) + ',', @branchList) > 0) AND 
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
	(@accountTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, ac.type) + ',',@accountTypeList) > 0) AND
	(@countryList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, a.BeneCountry) + ',',@countryList) > 0 OR
			CHARINDEX(',' + 
				CONVERT(VARCHAR, a.ByOrderCountry) + ',',@countryList) > 0 ) AND
	(@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0) AND
	(@customerTypeList IS NULL OR
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.type)) + ',', @customerTypeList ) > 0) AND
	(Customer.Exemptionstatus IS NULL OR @CustomerExemptionsList IS NULL OR
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.Exemptionstatus)) + ',', @CustomerExemptionsList ) = 0) AND
	(CASHTRAN = ISNULL(@cashType, CASHTRAN))


INSERT INTO @tt (tranAmt, tranCnt, byOrder, CustCount, BranchCount, DeptCount, ByOrderCustId)
SELECT DISTINCT SUM(BaseAmt) tranAmt, COUNT(tranNo) tranCnt, a.byOrder, 
       Count(Distinct Cust), Count(Distinct OwnerBranch), Count(Distinct OwnerDept),ByOrderCustId
FROM @TransTbl a 
GROUP BY  a.byOrder, a.ByOrderCustId

HAVING (
SUM(BaseAmt) >= @minSumAmount 
AND (@maxSumAmount = -1 OR SUM(BaseAmt) <= @maxSumAmount) 
AND count(tranno) >= @minIndCount
) 
AND COUNT(DISTINCT a.Bene) >= @NumBene


Update tt Set Cust = trn.Cust
From @tt tt Join @TransTbl trn 
On tt.ByOrder = trn.ByOrder
Where CustCount = 1

Update tt Set OwnerBranch = trn.OwnerBranch
From @tt tt Join @TransTbl trn 
On tt.ByOrder = trn.ByOrder
Where CustCount > 1 AND BranchCount = 1

Update tt Set OwnerDept = trn.OwnerDept
From @tt tt Join @TransTbl trn 
On tt.ByOrder = trn.ByOrder
Where CustCount > 1 AND DeptCount = 1

DECLARE	@cur CURSOR
DECLARE @byOrder LONGNAME,@bookdate INT, @ByOrderCustId varchar(40)

SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())

SET @cur = CURSOR FAST_FORWARD FOR 
SELECT a.tranAmt, a.tranCnt, a.byOrder, a.Cust, a.OwnerBranch, a.OwnerDept, a.ByOrderCustId
FROM @tt a 

OPEN @cur 
FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @byOrder, @cust, @OwnerBranch, @Ownerdept, @ByOrderCustId

WHILE @@FETCH_STATUS = 0 BEGIN

	SET @desc = @byOrder + '(ByOrder) is sending to '
		+ 'multiple bene ' 
		+ 'amount: ' + DBO.BSA_InternalizationMoneyToString(@tranAmt) + Space(1) + @BaseCurr + ' over '
		+ CONVERT(VARCHAR, @tranCnt) + ' transactions'
		+ ' over a period from ' + Cast(dbo.BSA_InternalizationIntToShortDate(@fromDate) AS Char(11)) + ' to ' 
		+ Cast(dbo.BSA_InternalizationIntToShortDate(@toDate) AS Char(11))

	IF @testAlert = 1
	BEGIN
		EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
		@Cust, NULL, 1
		IF @stat <> 0 GOTO EndOfProc
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 
		BEGIN
			EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
				@Cust, NULL, 0
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE 
		IF @WLTYPE = 1 
		BEGIN
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
				ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
				RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
				ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
				EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
				OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
				REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
				WLCode, WLDESC, CREATETIME, OwnerBranch, OwnerDept )
				SELECT  NULL, DBO.CONVERTSQLDATETOINT(GETDATE()), @Cust, NULL,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, @desc, GETDATE(), @OwnerBranch, @OwnerDept
			SELECT @STAT = @@ERROR, @ID = @@IDENTITY
			IF @stat <> 0 GOTO EndOfProc   	
		END	
	END		
	IF (@WLTYPE = 0) OR (@testAlert = 1)
	BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'Alert', @ID, TRANNO 
			FROM ActivityHist a (NOLOCK)
			Join Customer (NOLOCK) ON 	a.cust = Customer.Id
			Left Join Account Ac (NOLOCK) ON a.Account = ac.Id
			WHERE  	a.bookdate >= @fromDate AND 
				a.bookdate <= @toDate AND 
				a.recvpay = ISNULL(@recvPay, a.recvpay) AND
				a.BaseAmt >= @minIndAmt AND
			    (@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
				(a.byOrder = @byOrder or a.byOrderCustId=@ByOrderCustId) and 
				a.bene IS NOT NULL AND  
				(@deptList IS NULL OR 
						CHARINDEX(',' + 
							CONVERT(VARCHAR,  a.dept) + ',',@deptList) > 0) AND
				(@branchList IS NULL OR 
						CHARINDEX(',' + 
							CONVERT(VARCHAR,  a.branch) + ',', @branchList) > 0) AND 
				(@activityTypeList IS NULL OR 
					CHARINDEX(',' + 
						CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
				(@accountTypeList IS NULL OR 
					CHARINDEX(',' + 
						CONVERT(VARCHAR, ac.type) + ',',@accountTypeList) > 0) AND
				(@countryList IS NULL OR 
					CHARINDEX(',' + 
						CONVERT(VARCHAR, a.BeneCountry) + ',',@countryList) > 0 OR
							 CHARINDEX(',' + 
								CONVERT(VARCHAR, a.ByOrderCountry) 
									+ ',',@countryList) > 0 ) AND
				((@riskClassList IS NULL OR 
						CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) 
							+ ',', @riskClassList ) > 0)) AND
				((@customerTypeList IS NULL OR
						CHARINDEX(',' + LTRIM(RTRIM(Customer.type)) 
							+ ',', @customerTypeList ) > 0)) AND
				(Customer.Exemptionstatus IS NULL OR @CustomerExemptionsList IS NULL OR
					CHARINDEX(',' + LTRIM(RTRIM(Customer.Exemptionstatus)) + ',', 
					@CustomerExemptionsList ) = 0) AND
				(CASHTRAN = ISNULL(@cashType, CASHTRAN))
	
			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC
	END 
	ELSE 
	IF @WLTYPE = 1 
	BEGIN

		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'SUSPACT', @ID, TRANNO 
				FROM ActivityHist a (NOLOCK)
				Join Customer (NOLOCK) ON 	a.cust = Customer.Id
				Left Join Account Ac (NOLOCK) ON a.Account = ac.Id
			WHERE  	a.bookdate >= @fromDate AND 
				a.bookdate <= @toDate AND 
				a.recvpay = ISNULL(@recvPay, a.recvpay) AND
				a.BaseAmt >= @minIndAmt AND
				(@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
			   	(a.byOrder = @byOrder or a.byOrderCustId=@ByOrderCustId) and  
				a.bene IS NOT NULL AND  
				(@deptList IS NULL OR 
						CHARINDEX(',' + 
							CONVERT(VARCHAR,  a.dept) + ',',@deptList) > 0) AND
				(@branchList IS NULL OR 
						CHARINDEX(',' + 
							CONVERT(VARCHAR,  a.branch) + ',', @branchList) > 0) AND 
				(@activityTypeList IS NULL OR 
					CHARINDEX(',' + 
						CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
				(@accountTypeList IS NULL OR 
					CHARINDEX(',' + 
						CONVERT(VARCHAR, ac.type) + ',',@accountTypeList) > 0) AND
				(@countryList IS NULL OR 
					CHARINDEX(',' + 
						CONVERT(VARCHAR, a.BeneCountry) + ',',@countryList) > 0 OR
							 CHARINDEX(',' + 
								CONVERT(VARCHAR, a.ByOrderCountry) 
									+ ',',@countryList) > 0 ) AND
				((@riskClassList IS NULL OR 
						CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) 
							+ ',', @riskClassList ) > 0)) AND
				((@customerTypeList IS NULL OR
						CHARINDEX(',' + LTRIM(RTRIM(Customer.type)) 
							+ ',', @customerTypeList ) > 0)) AND
				(Customer.Exemptionstatus IS NULL OR @CustomerExemptionsList IS NULL OR
					CHARINDEX(',' + LTRIM(RTRIM(Customer.Exemptionstatus)) + ',', 
					@CustomerExemptionsList ) = 0) AND
				(CASHTRAN = ISNULL(@cashType, CASHTRAN))

			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC 
	END
	FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @byOrder, @cust, @OwnerBranch, @Ownerdept, @ByOrderCustId
END

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_IMultiBene
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_IMultiBene
RETURN @stat

