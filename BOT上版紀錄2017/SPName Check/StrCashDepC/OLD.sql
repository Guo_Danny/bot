USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[USR_StructuredCTRDeposits]    Script Date: 11/17/2016 18:21:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[USR_StructuredCTRDeposits](@WLCode SCODE, @testAlert INT,
	@CTRAmount MONEY,     
	@LastNoOfMonths INT,  
	@MinTotalCTRAmount MONEY,
	@Tolerance Percentage )
AS

/* Rule and Parameter Description
   Detects structuring of cash deposits to an account in order to avoid CTR 
	reporting. An Alert or SA case is created for each account that has cash 
	deposits being structured and the total amount deposited exceeds a specified 
	maximum  dollar amount over a certain time period.  Designed to be run 
	Post-EOD on a monthly or quarterly schedule.
 
   @CTRAmount	           -- Rule will take into account any cash transaction greater than 
                          or equal to (@CTRAmount - Tolerance percent of @CTRAmount)
   @LastNoOfMonths     -- Rule will consider all activity that occurred in this 
                          specified last number of months 
   @MinTotalCTRAmount     -- Rule checks if the total sum of all cash transactions in 
                          the specified last number of months is greater than or 
                          equal to (@MinTotalCTRAmount - Tolerance% of @MinTotalCTRAmount)
   @Tolerance  	       -- Tolerance as given above
*/

/*  DECLARATIONS */
DECLARE	@CUR CURSOR,
	@DESCRIPTION VARCHAR(2000),
	@DESC VARCHAR(2000),
	@ID INT, 
	@WLTYPE INT,
	@STAT INT,
	@TRNCNT INT,
	@MINDATE INT -- start date , current date is end date

	
Declare @startAlrtDate datetime
Declare @endAlrtDate Datetime
Declare @POSSCTRAMT Money
Declare @CurrDate GenericDate
Declare @wrkDate Datetime

DECLARE	@TT TABLE (
	Account VARCHAR(40),
	TranNO INT,
	BaseAmt MONEY,
	BookDate Int
)
--- ********************* Begin Rule Procedure **********************************
SET NOCOUNT ON
SET @STAT = 0


/*****************************************/
-- Date options
-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam
Declare @StartDate Datetime

Select @StartDate = 
	Case 	
		When UseSysDate in (0,1) Then
			-- use System date
			GetDate()
		When UseSysDate = 2 Then
			-- use business date
			(Select BusDate From dbo.SysParam)
		Else
			GetDate()
	End
From dbo.WatchList
Where WLCode = @WlCode

/******************************************/

SET @LastNoOfMonths = ABS(@LastNoOfMonths)



SELECT @MINDATE = DBO.CONVERTSQLDATETOINT(
		DATEADD(M, -1 * @LastNoOfMonths, CONVERT(VARCHAR, @StartDate)))

/* START STANDARD STORED PROCEDURE TRANSACTION HEADER */
SET @TRNCNT = @@TRANCOUNT	-- SAVE THE CURRENT TRANCOUNT
IF @TRNCNT = 0
	-- TRANSACTION HAS NOT BEGUN
	BEGIN TRAN USR_StructuredCTRDeposits
ELSE
	-- ALREADY IN A TRANSACTION
	SAVE TRAN USR_StructuredCTRDeposits
/* END STANDARD STORED PROCEDURE TRANSACTION HEADER */


/*  STANDARD RULES HEADER */
SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE  
FROM WATCHLIST (NOLOCK) WHERE WLCode = @WLCode

SET @POSSCTRAMT = Floor((100 - @Tolerance)* @CTRAmount/100)
--should we consider maxtotamount
SET @MinTotalCTRAmount = Floor((100 - @Tolerance)* @MinTotalCTRAmount/100) 

INSERT INTO @TT ( Account, TranNo, BaseAmt, Bookdate)
SELECT Account,tranno, baseAmt, bookdate
	FROM ACTIVITYHIST (NOLOCK)
	WHERE BOOKDATE >= @MINDATE AND CASHTRAN = 1 AND RECVPAY = 1
	      AND BASEAMT between @POSSCTRAMT and @CTRAmount 	

IF @testAlert = 1 BEGIN
	SELECT @STARTALRTDATE = GetDate()
	Insert into Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
			LastModify, Cust, Account, IsTest) 
	  Select @WLCode, 'Account: ''' + ACCOUNT 
		+ ''' had ''' + convert(varchar,Count(TranNo))
		+ ''' cash transactions of total 
	amount ''' + CONVERT(VARCHAR, Sum(BaseAmt))  + ''' between the 
	period ''' + CONVERT(VARCHAR, @MINDATE) + ''' and ''' +
		 CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(getdate()))+ '''. 
	The cash deposited in each transaction was close to CTR amount and so these
	transactions could be suspicious' , 0, 
		GetDate(), Null, Null, NULL, ACCOUNT, 1
	  From @TT 
	GROUP BY ACCOUNT
	    HAVING SUM(Baseamt) >= @MinTotalCTRAmount
	Select @STAT = @@error	
	Select @ENDALRTDATE = GetDate()
	if @STAT <> 0  GOTO ENDOFPROC

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'Alert', AlertNo, TRANNO 
		FROM @TT t, Alert where
		t.Account = Alert.Account And
		Alert.WLCode = @WLCode and
		Alert.CreateDate between @startAlrtDate and @endAlrtDate
	SELECT @STAT = @@ERROR
END ELSE BEGIN
	IF @WLTYPE = 0 BEGIN
	SELECT @STARTALRTDATE = GetDate()
	Insert into Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
			LastModify, Cust, Account) 
	  Select @WLCode, 'Account: ''' + ACCOUNT 
		+ ''' had ''' + convert(varchar,Count(TranNo))
		+ ''' cash transactions of total 
	amount ''' + CONVERT(VARCHAR, Sum(BaseAmt))  + ''' between the 
	period ''' + CONVERT(VARCHAR, @MINDATE) + ''' and ''' +
		 CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(getdate()))+ '''. 
	The cash deposited in each transaction was close to CTR amount and so these
	transactions could be suspicious' , 0, 
		GetDate(), Null, Null, NULL, ACCOUNT 
	  From @TT 
	GROUP BY ACCOUNT
	    HAVING SUM(Baseamt) >= @MinTotalCTRAmount
	Select @STAT = @@error	
	Select @ENDALRTDATE = GetDate()
	if @STAT <> 0  GOTO ENDOFPROC
	END ELSE IF @WLTYPE = 1 BEGIN
		select @wrkDate = GetDate()
		Exec @CurrDate = BSA_CvtDateToLong @wrkDate
		SELECT @STARTALRTDATE = GetDate()
		insert into SuspiciousActivity (ProfileNo, BookDate, Cust, Account, 
			Activity, SuspType, StartDate, EndDate, RecurType, 
			RecurValue, ActCurrReportAmt, ActInActCnt, ActOutActCnt, 
			ActInActAmt, ActOutActAmt, CurrReportAmt, ExpAvgInActCnt, 
			ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt, InCntTolPerc, 
			OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr, ReviewState, 
			ReviewTime, ReviewOper, App, AppTime, AppOper, 
			WLCode, WLDesc, CreateTime )
		Select	Null, @CurrDate, Null, Account,
			Null, 'Rule', Null, Null, Null, Null,
			Null, Null, Null, Null, Null, Null, Null, Null,
			Null, Null, 0, 0, 0, 0, 
			Null, Null, Null, Null, 0, Null, Null,
			@WLCode,'ACCOUNT: ''' + ACCOUNT 
		+ ''' HAD ''' + convert(varchar,Count(TranNo))
		+ ''' CASH TRANSACTIONS OF TOTAL 
	AMOUNT ''' + CONVERT(VARCHAR, Sum(BaseAmt))  + ''' BETWEEN THE 
	PERIOD ''' + CONVERT(VARCHAR, @MINDATE) + ''' AND ''' +
		 CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(getdate()))+ '''. 
	The cash deposited in each transaction was close to CTR amount and so these
	transactions could be suspicious', GetDate() 
	From @TT 
	GROUP BY ACCOUNT
	    HAVING SUM(Baseamt) >= @MinTotalCTRAmount
	
	Select @STAT = @@error	
	Select @ENDALRTDATE = GetDate()
	if @STAT <> 0  GOTO ENDOFPROC
	END
	
	IF @WLTYPE = 0 BEGIN
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'Alert', AlertNo, TRANNO 
		FROM @TT t, Alert where
		t.Account = Alert.Account And
		Alert.WLCode = @WLCode and
		Alert.CreateDate between @startAlrtDate and @endAlrtDate
	SELECT @STAT = @@ERROR
	END ELSE IF @WLTYPE = 1 BEGIN 
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'SUSPACT', RecNo, TRANNO 
		FROM @TT t, SuspiciousActivity s where
		t.Account = s.Account And
		s.WLCode = @WLCode and
		s.CreateTime between @startAlrtDate and @endAlrtDate
	SELECT @STAT = @@ERROR 
	END
END

ENDOFPROC:
IF (@STAT <> 0) BEGIN 
  ROLLBACK TRAN USR_StructuredCTRDeposits
  RETURN @STAT
END	

IF @TRNCNT = 0
  COMMIT TRAN USR_StructuredCTRDeposits
RETURN @STAT
