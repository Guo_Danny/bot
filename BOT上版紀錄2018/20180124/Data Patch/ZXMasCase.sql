--use ofac
--go
--
/*選取所有FATFAMLD及MAS的案件(顯示customer name)*/
/*unarchived cases */
select  CONVERT(CHAR(2), '1') as Flag, CONVERT(CHAR(15),sdn.program) as program, CONVERT(CHAR(200),rtrim(sdn.name)) as sdnname, b.type, b.ref,b.reqtime, CONVERT(CHAR(40),rtrim(b.name)) customername, 
CONVERT(CHAR(80),rtrim(b.matchname)) matchname,rtrim(b.matchtext) matchtext,b.currency,b.amount,sdn.listtype, b.seqnumb, b.source from OFAC..sdntable sdn
inner join (select  substring(ref,15,3) as type,ft.ref, ft.reqtime,c.name,matchname,matchtext,currency,amount,entnum, ft.seqnumb,ft.source
  from OFAC..matchtable ma
  inner join OFAC..filtertrantable ft on ma.seqnumb=ft.seqnumb 
  left outer join PBSA.dbo.customer c on c.id=ft.ref 
   --where ft.reqtime between '2015-10-01 00:00:00.000' and '2016-01-01 00:00:00.000'
   --where year(ft.reqtime)= year(getdate()) and (month(ft.reqtime) between month(getdate())-4 and month(getdate())-1))
   where ft.ReqTime BETWEEN dateadd(MONTH,-4,getdate()) and dateadd(MONTH,-1,getdate()))
 
 b on sdn.entnum=b.entnum  where (sdn.listtype = 'MAS' OR sdn.LISTTYPE='FATFAMLD')  and (source = 'swift' or source = 'cif filter' or source = 'cif')

union all

/* archived cases */
select  CONVERT(CHAR(2), '1') as Flag, CONVERT(CHAR(15),sdn.program) as program, CONVERT(CHAR(200),rtrim(sdn.name)) as sdnname, b.type, b.ref,b.reqtime, CONVERT(CHAR(40),rtrim(b.name)) customername, 
CONVERT(CHAR(80),rtrim(b.matchname)) matchname,rtrim(b.matchtext) matchtext,b.currency,b.amount,sdn.listtype, b.seqnumb, b.source from OFAC..sdntable sdn
inner join (select  substring(ref,15,3) as type,ft.ref, ft.reqtime,c.name,matchname,matchtext,currency,amount,entnum, ft.seqnumb,ft.source
  from OFAC..matchhisttable ma
  inner join  OFAC..filtertranhisttable ft  on ma.seqnumb=ft.seqnumb 
 left outer join PBSA.dbo.customer c on c.id=ft.ref
 --where ft.reqtime between '2015-10-01 00:00:00.000' and '2016-01-01 00:00:00.000' )  
--WHERE year(ft.reqtime)= year(getdate()) and (month(ft.reqtime) between month(getdate())-4 and month(getdate())-1))
where ft.ReqTime BETWEEN dateadd(MONTH,-4,getdate()) and dateadd(MONTH,-1,getdate()))
 b on sdn.entnum=b.entnum where (sdn.listtype = 'MAS' OR sdn.LISTTYPE='FATFAMLD') and (source = 'swift' or source = 'cif filter' or source = 'cif')

union all

select  CONVERT(CHAR(2), '1') as Flag, CONVERT(CHAR(15),sdn.program) as program, CONVERT(CHAR(200),rtrim(sdn.name)) as sdnname, b.type, b.ref,b.reqtime, CONVERT(CHAR(40),rtrim(b.name)) customername, 
CONVERT(CHAR(80),rtrim(b.matchname)) matchname,rtrim(b.matchtext) matchtext,b.currency,b.amount,sdn.listtype, b.seqnumb, b.source from OFAC..sdntable sdn
inner join (select  substring(ref,1,3) as type,ft.ref, ft.reqtime,c.name,matchname,matchtext,currency,amount,entnum, ft.seqnumb,ft.source
  from OFAC..matchtable ma
  inner join OFAC..filtertrantable ft on ma.seqnumb=ft.seqnumb 
  left outer join PBSA.dbo.customer c on c.id=ft.ref 
   --where ft.reqtime between '2015-10-01 00:00:00.000' and '2016-01-01 00:00:00.000'
   --where year(ft.reqtime)= year(getdate()) and (month(ft.reqtime) between month(getdate())-4 and month(getdate())-1))
   where ft.ReqTime BETWEEN dateadd(MONTH,-4,getdate()) and dateadd(MONTH,-1,getdate()))
 b on sdn.entnum=b.entnum  where (sdn.listtype = 'MAS' OR sdn.LISTTYPE='FATFAMLD')  and source = 'inquiry'

union all

/* archived cases */
select  CONVERT(CHAR(2), '1') as Flag, CONVERT(CHAR(15),sdn.program) as program, CONVERT(CHAR(200),rtrim(sdn.name)) as sdnname, b.type, b.ref,b.reqtime, CONVERT(CHAR(40),rtrim(b.name)) customername, 
CONVERT(CHAR(80),rtrim(b.matchname)) matchname,rtrim(b.matchtext) matchtext,b.currency,b.amount,sdn.listtype, b.seqnumb, b.source from OFAC..sdntable sdn
inner join (select  substring(ref,1,3) as type,ft.ref, ft.reqtime,c.name,matchname,matchtext,currency,amount,entnum, ft.seqnumb,ft.source
  from OFAC..matchhisttable ma
  inner join  OFAC..filtertranhisttable ft  on ma.seqnumb=ft.seqnumb 
 left outer join PBSA.dbo.customer c on c.id=ft.ref
 --where ft.reqtime between '2015-10-01 00:00:00.000' and '2016-01-01 00:00:00.000' )  
--WHERE year(ft.reqtime)= year(getdate()) and (month(ft.reqtime) between month(getdate())-4 and month(getdate())-1))
where ft.ReqTime BETWEEN dateadd(MONTH,-4,getdate()) and dateadd(MONTH,-1,getdate()))
 b on sdn.entnum=b.entnum where (sdn.listtype = 'MAS' OR sdn.LISTTYPE='FATFAMLD') and source = 'inquiry'

order by program,sdnname,ref

 


/*WHERE子句加入SUBSTRING篩選電文種類為4或7開頭,及source為inquiry之案件*/
/*unarchived cases :電文種類為4或7開頭*/

select   CONVERT(CHAR(2), '2') as Flag, CONVERT(CHAR(15),sdn.program) as program, CONVERT(CHAR(200),rtrim(sdn.name)) as sdnname, b.type, b.ref,b.reqtime, 
CONVERT(CHAR(80),rtrim(b.matchname)) matchname,rtrim(b.matchtext) matchtext,b.currency,b.amount,sdn.listtype, b.seqnumb, b.source from OFAC..sdntable sdn
inner join (select substring(ref,15,3) as type,ft.ref,ft.reqtime,ft.seqnumb,matchtext,matchname,
currency,amount, entnum,ft.source
  from OFAC..matchtable ma, OFAC..filtertrantable ft 
  where ma.seqnumb=ft.seqnumb
  --and ft.reqtime between '2015-10-01 00:00:00.000' and '2016-01-01 00:00:00.000')
  --and year(ft.reqtime)= year(getdate()) and (month(ft.reqtime) between month(getdate())-4 and month(getdate())-1))
  and ft.ReqTime BETWEEN dateadd(MONTH,-4,getdate()) and dateadd(MONTH,-1,getdate()))
  b on sdn.entnum=b.entnum
where (sdn.listtype = 'MAS' OR sdn.LISTTYPE='FATFAMLD') 
 and (source='swift' and (substring(ref,15,1)='7' or substring(ref,15,1)='4')) 

UNION ALL

/* archived cases:電文種類為4或7開頭 */
select  CONVERT(CHAR(2), '2') as Flag, CONVERT(CHAR(15),sdn.program) as program, CONVERT(CHAR(200),rtrim(sdn.name)) as sdnname, b.type, b.ref,b.reqtime, 
CONVERT(CHAR(80),rtrim(b.matchname)) matchname,rtrim(b.matchtext) matchtext,b.currency,b.amount,sdn.listtype, b.seqnumb, b.source from OFAC..sdntable sdn
inner join (select substring(ref,15,3) as type,ft.ref,ft.reqtime,ft.seqnumb,matchtext,matchname,
currency,amount, entnum,ft.source
  from  OFAC..matchhisttable ma, OFAC..filtertranhisttable ft 
  where ma.seqnumb=ft.seqnumb
  -- and ft.reqtime between '2015-10-01 00:00:00.000' and '2016-01-01 00:00:00.000')
  --and year(ft.reqtime)= year(getdate()) and (month(ft.reqtime) between month(getdate())-4 and month(getdate())-1))
  and ft.ReqTime BETWEEN dateadd(MONTH,-4,getdate()) and dateadd(MONTH,-1,getdate()))
  b on sdn.entnum=b.entnum
where (sdn.listtype = 'MAS' OR sdn.LISTTYPE='FATFAMLD') 
and (source='swift' and (substring(ref,15,1)='7' or substring(ref,15,1)='4'))


union all

/* unarchived cases:source為inquiry */
select  CONVERT(CHAR(2), '2') as Flag, CONVERT(CHAR(15),sdn.program) as program, CONVERT(CHAR(200),rtrim(sdn.name)) as sdnname, b.type, b.ref,b.reqtime, 
CONVERT(CHAR(80),rtrim(b.matchname)) matchname,rtrim(b.matchtext) matchtext,b.currency,b.amount,sdn.listtype, b.seqnumb, b.source from OFAC..sdntable sdn 
inner join (select substring(ref,1,3) as type,ft.ref,ft.reqtime,ft.seqnumb,matchtext,matchname,
currency,amount, entnum,ft.source
  from OFAC..matchtable ma, OFAC..filtertrantable ft where ma.seqnumb=ft.seqnumb
  -- and ft.reqtime between '2015-10-01 00:00:00.000' and '2016-01-01 00:00:00.000')
   --and year(ft.reqtime)= year(getdate()) and (month(ft.reqtime) between month(getdate())-4 and month(getdate())-1))
   and ft.ReqTime BETWEEN dateadd(MONTH,-4,getdate()) and dateadd(MONTH,-1,getdate()))
   b on sdn.entnum=b.entnum
where (sdn.listtype = 'MAS' OR sdn.LISTTYPE='FATFAMLD') and  source = 'inquiry'


UNION ALL

/* archived cases:source為inquiry */
select  CONVERT(CHAR(2), '2') as Flag, CONVERT(CHAR(15),sdn.program) as program, CONVERT(CHAR(200),rtrim(sdn.name)) as sdnname, b.type, b.ref,b.reqtime, 
CONVERT(CHAR(80),rtrim(b.matchname)) matchname,rtrim(b.matchtext) matchtext,b.currency,b.amount,sdn.listtype, b.seqnumb, b.source from OFAC..sdntable sdn 
inner join (select substring(ref,1,3) as type,ft.ref,ft.reqtime,ft.seqnumb,matchtext,matchname,
currency,amount, entnum,ft.source
  from  OFAC..matchhisttable ma, OFAC..filtertranhisttable ft where ma.seqnumb=ft.seqnumb
  -- and ft.reqtime between '2015-10-01 00:00:00.000' and '2016-01-01 00:00:00.000')
   --and year(ft.reqtime)= year(getdate()) and (month(ft.reqtime) between month(getdate())-4 and month(getdate())-1))
  and ft.ReqTime BETWEEN dateadd(MONTH,-4,getdate()) and dateadd(MONTH,-1,getdate()))
  b on sdn.entnum=b.entnum
where (sdn.listtype = 'MAS' OR sdn.LISTTYPE='FATFAMLD') and  source = 'inquiry'

 order by program,sdnname,type,ref
