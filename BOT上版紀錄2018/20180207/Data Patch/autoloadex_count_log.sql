﻿/* Polly 版 */

declare @StrDate datetime ,@_txt varchar(500),@_txt2 varchar(500),@listtype varchar(50),@modidate char(10), @count varchar(10);
select @StrDate=max(listmodifdate) from OFAC.dbo.SDNTable(nolock) 
--select @StrDate as StrDate  
set @_txt2 = '';
declare cur CURSOR FOR
select ListType, convert(char(16),listmodifdate,111) listmodifdate, count(*)  from OFAC.dbo.SDNTable(nolock) where 
                            (ListModifDate >=@StrDate )and  
                            (ListModifDate <=getdate() or LastModifDate <=getdate()) and 
                                                        listtype not like '%WPEP' and ListType not in ('PEP') 

group by ListType,listmodifdate 
order by listtype,listmodifdate

open cur;
fetch next from cur into @listtype,@modidate,@count;
WHILE @@fetch_status = 0 
begin
  set @_txt2 = @_txt2 + @listtype + ',' + @modidate + ',' + @count + char(13);
  fetch next from cur into @listtype,@modidate,@count;
end
close cur;
DEALLOCATE cur;

select 'Start Date:' + convert(char(16),@StrDate,111) + char(13) + 'ListType,ModifiDate,Count' + char(13) + @_txt2

insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
values (getdate(),'Prime','Starting load of the distribution dated ' + convert(char(16),@StrDate,111) + char(13) + 'ListType,ModifiDate,Count' + char(13) + @_txt2,'Ann','Event','Autoload','Starting load of the distribution dated ' + convert(char(16),@StrDate,111) + char(13) + 'ListType,ModifiDate,Count' + char(13) + @_txt2)
