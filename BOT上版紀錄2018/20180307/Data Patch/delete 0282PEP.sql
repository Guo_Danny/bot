select * from OptionTbl

update OptionTbl set Enabled = 0 where Code = 'PEP'

select * from ListType

select * from ListTypeAssociation

delete ListTypeAssociation where ListType in ('0282','0282PEP')

delete ListType where Code in ('0282','0282PEP')

--SDNAltTable
select top 100 * from SDNAltTable(nolock)
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

delete SDNAltTable
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

--URLS
select top 100 * from URLs(nolock)
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

delete URLs
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

--Notes
select top 100 * from [dbo].[Notes](nolock)
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

delete [dbo].[Notes]
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

--Keywords
select top 100 * from [dbo].[Keywords](nolock)
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

delete [dbo].[Keywords]
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

--KeywordsAlt
select top 100 * from [dbo].[KeywordsAlt](nolock)
where AltNum in (
select b.KeywordsId from SDNTable(nolock) a
join Keywords(nolock) b on a.EntNum = b.EntNum
where a.ListType = '0282'
)

delete [dbo].[KeywordsAlt]
where AltNum in (
select b.KeywordsId from SDNTable(nolock) a
join Keywords(nolock) b on a.EntNum = b.EntNum
where a.ListType = '0282'
)

--DOBS
select top 100 * from [dbo].[DOBs](nolock)
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

delete [dbo].[DOBs]
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

--DOBS
select top 100 * from [dbo].[SDNAddrTable](nolock)
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

delete [dbo].[SDNAddrTable]
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

--Relationship
select top 100 * from [dbo].[Relationship](nolock)
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

delete [dbo].[Relationship]
where EntNum in (
select EntNum from SDNTable(nolock)
where ListType = '0282'
)

--SDNTable
select * from SDNTable(nolock)
where ListType = '0282'

delete SDNTable where ListType = '0282'