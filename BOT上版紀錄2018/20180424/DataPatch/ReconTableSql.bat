REM --- TEST ---
SQLCMD -S TOSBAMLV3 -U sa -P Passw0rd -i"D:\PRIME\BOTOSB\ReconTablesql.sql" -o"D:\PRIME\BOTOSB\ZXRECON.TXT"

REM --- NY:0083 ---
REM SQLCMD -S OSBAMLNYV -U primeuser -P Passw0rd "-i"D:\PRIME\BOTOSB\ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"
REM --- LA:0094 ---
REM SQLCMD -S OSBAMLLAV -U primeuser -P Passw0rd "-i"D:\PRIME\BOTOSB\ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"
REM --- HK:0114 ---
REM SQLCMD -S OSBAMLHKV -U primeuser -P Passw0rd "-i"D:\PRIME\BOTOSB\ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"
REM --- SG:0138 ---
REM SQLCMD -S OSBAMLSGV -U primeuser -P Passw0rd "-i"D:\PRIME\BOTOSB\ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"
REM --- TK:0117 ---
REM SQLCMD -S OSBAMLTKV -U primeuser -P Passw0rd "-i"D:\PRIME\BOTOSB\ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"
REM --- ZA:0161 ---
REM SQLCMD -S OSBAMLZAV -U primeuser -P Passw0rd "-i"D:\PRIME\BOTOSB\ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"
REM --- LN:0210 ---
REM SQLCMD -S OSBAMLLNV -U primeuser -P Passw0rd "-i"D:\PRIME\BOTOSB\ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"

REM forfiles /p D:\ftproot\ReconReport /s /m zxrecon.txt /c "cmd /c if @fsize GTR 62000 echo @file"
SET sNoFile=Empty
SET cmdcopy=A
forfiles /p D:\ftproot\ReconReport /s /m zxrecon.txt /c "cmd /c if @fsize LSS 62000 SET cmdcopy=%sNoFile%"

echo %cmdcopy% %sNoFile%

if %cmdcopy%==%sNoFile% goto EndProc

set cmdcopy=copy /Y D:\PRIME\BOTOSB\ZXRECON.TXT D:\ftproot\ReconReport\ZXRECON.TXT
%cmdcopy% 

REM --- BACKUP ZXRECON.TXT ---
REM SET sDate = %date:~4,2%%date:~7,2%%date:~10,4%
REM SET sDATE=%date:~0,2%%date:~3,2%
REM SET sDATE=%date:~10,4%%date:~4,2%%date:~7,2%
SET sDATE=%date:~7,2%
ECHO %SDATE%

REM set sbkfile=D:\ftproot\ReconReport\backup\ZXRECON.TXT.%%date:~4,2%%date:~7,2%%date:~10,4%
REM set cmdcopy =copy /Y D:\ftproot\ReconReport\ZXRECON.TXT D:\ftproot\ReconReport\backup\ZXRECON.TXT.%date:~4,2%%date:~7,2%%date:~10,4%

set cmdcopy=copy /Y D:\ftproot\ReconReport\ZXRECON.TXT D:\ftproot\ReconReport\backup\ZXRECON.TXT.%sdate%
%cmdcopy% 

:EndProc

