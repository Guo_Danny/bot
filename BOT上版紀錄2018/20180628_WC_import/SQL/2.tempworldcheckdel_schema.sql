USE [OFAC]
GO

/****** Object:  Table [dbo].[TempWorldCheckDel]    Script Date: 8/7/2017 3:46:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TempWorldCheckDel](
	[UID] [int] NOT NULL,
	[Updated] [varchar](50) NULL
) 

GO

SET ANSI_PADDING OFF
GO
