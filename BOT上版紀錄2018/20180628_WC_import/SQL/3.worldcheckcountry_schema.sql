USE [OFAC]
GO

/****** Object:  Table [dbo].[TempWorldCheckDel]    Script Date: 8/7/2017 3:46:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WorldCheckCountry](
	[Code] [varchar](10) NOT NULL,
	[Customer_Country] [varchar](200) NULL,
	[WorldCheck_Country] [varchar](200) NULL,
	[Status] [bit] NULL,
	[ListType] [varchar](50) NULL
) 

GO

SET ANSI_PADDING OFF
GO

/*****Add trriger****/
CREATE TRIGGER [dbo].[WorldCheckCountryModif] ON [dbo].[WorldCheckCountry]
FOR UPDATE
AS

  update [dbo].[WorldCheckCountry] set LastDate = convert(nchar(10),GETDATE(), 120)
  from inserted u , WorldCheckCountry w where u.Code = w.Code


GO

ALTER TABLE [dbo].[WorldCheckCountry] ENABLE TRIGGER [WorldCheckCountryModif]
GO


/*
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('AI','ANGUILLA','ANGUILLA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('AU','AUSTRALIA','AUSTRALIA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('AT','AUSTRIA','AUSTRIA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('BE','BELGIUM','BELGIUM','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('BZ','BELIZE','BELIZE','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('BM','BERMUDA','BERMUDA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('BN','BRUNEI DARUSSALAM','BRUNEI','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('CA','CANADA','CANADA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('KY','CAYMAN ISLANDS','CAYMAN ISLANDS','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('CN','CHINA','CHINA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('DK','DENMARK','DENMARK','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('FR','FRANCE','FRANCE','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('DE','GERMANY','GERMANY','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('HK','HONG KONG','HONG KONG','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('IN','INDIA','INDIA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('ID','INDONESIA','INDONESIA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('IL','ISRAEL','ISRAEL','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('IT','ITALY','ITALY','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('JP','JAPAN','JAPAN','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('KR','KOREA.REPUBLIC OF','KOREA, NORTH','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('MO','MACAU','MACAU','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('MY','MALAYSIA','MALAYSIA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('MH','MARSHALL ISLANDS','MARSHALL ISLANDS','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('MU','MAURITIUS','MAURITIUS','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('MX','MEXICO','MEXICO','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('AN','NETH.ANTILLES','NETHERLANDS ANTILLES','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('NL','NETHERLANDS','NETHERLANDS','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('NZ','NEW ZEALAND','NEW ZEALAND','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('PA','PANAMA','PANAMA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('PH','PHILIPPINES','PHILIPPINES','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('PT','PORTUGAL','PORTUGAL','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('TW','R.O.C.','TAIWAN','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('RU','RUSSIAN FEDERATION','RUSSIAN FEDERATION','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('WS','SAMOA','SAMOA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('SA','SAUDI ARABIA','SAUDI ARABIA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('SC','SEYCHELLES','SEYCHELLES','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('SG','SINGAPORE','SINGAPORE','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('ZA','SOUTH AFRICA','SOUTH AFRICA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('ES','SPAIN','SPAIN','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('CH','SWITZERLAND','SWITZERLAND','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('TH','THAILAND','THAILAND','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('TR','TURKEY','TURKEY','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('AE','UNITED ARAB EMIRATES','UNITED ARAB EMIRATES','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('GB','UNITED KINGDOM','UNITED KINGDOM','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('US','UNITED STATES','USA','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('VN','VIETNAM','VIET NAM','1')
inser into worldcheckcountry(code,customer_country,worldcheck_country,status)values ('VG','VIRGIN ISLANDS.BRITISH','VIRGIN ISLANDS (BRITISH)','1')
*/