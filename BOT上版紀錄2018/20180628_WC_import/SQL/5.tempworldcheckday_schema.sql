USE [OFAC]
GO

/****** Object:  Table [dbo].[TempWorldCheckDay]    Script Date: 8/7/2017 3:46:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TempWorldCheckDay](
	[UID] [int] NOT NULL,
	[LastName] [varchar](max) NULL,
	[FirstName] [varchar](max) NULL,
	[Aliases] [varchar](max) NULL,
	[LowQAliases] [varchar](max) NULL,
	[AlterSpell] [varchar](max) NULL,
	[Category] [varchar](max) NULL,
	[Title] [varchar](max) NULL,
	[SubCategory] [varchar](max) NULL,
	[Position] [varchar](max) NULL,
	[Age] [varchar](10) NULL,
	[DOB] [varchar](50) NULL,
	[PlaceOfBirth] [varchar](max) NULL,
	[Deceased] [varchar](50) NULL,
	[Passports] [varchar](max) NULL,
	[SSN] [varchar](max) NULL,
	[IDENTIFICATION_NUMBERS] [varchar](max) NULL,
	[Location] [varchar](max) NULL,
	[Country] [varchar](max) NULL,
	[Company] [varchar](max) NULL,
	[Type] [varchar](max) NULL,
	[LinkedTo] [varchar](max) NULL,
	[FurtherInfo] [varchar](max) NULL,
	[Keywords] [varchar](max) NULL,
	[ExSources] [varchar](max) NULL,
	[UpdCategory] [varchar](max) NULL,
	[Entered] [varchar](50) NULL,
	[Updated] [varchar](50) NULL,
	[Editor] [varchar](max) NULL,
	[AgeDate] [varchar](50) NULL,
 CONSTRAINT [PK_TempWorldCheckDay_UID] PRIMARY KEY NONCLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
