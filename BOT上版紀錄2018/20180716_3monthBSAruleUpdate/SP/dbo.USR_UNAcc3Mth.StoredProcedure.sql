USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_UNAcc3Mth]    Script Date: 7/16/2018 4:56:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[USR_UNAcc3Mth] (@WLCode SCode, @TestAlert INT,@excludeExemptCustList varchar(8000),
	@activityTypeList varchar(8000), @activityTypeExcludeList VARCHAR(8000),@cashTranIndicator INT,@day INT, 
	@minimumTotalAmount MONEY,@maximumTotalAmount MONEY,@minimumAmount MONEY, @maximumAmount MONEY,
	@minimumCount INT, @customerList VARCHAR(8000), @RiskClassList VARCHAR(8000), @CustomerTypeList VARCHAR(8000),
	@AccountTypeList VARCHAR(8000), @BranchList varchar(8000), 
	@DeptList varchar(8000), @UseRelAcct Bit, @UseRelCust Bit, 
    @ExcludeRelList varchar(8000)
) AS
/* RULE PARAMETERS -  THESE MUST BE SET ACCORDING TO THE RULE REQUIREMENT*/

/* RULE AND PARAMETER DESCRIPTION
	Detects customers that had transactions using a specified 
	activity type that exceeded a designated dollar amount over 
	a period of specified days.  Designed to be run Post-EOD. 
	
	@excludeExemptCustList = Type of Exemptions to exclude customers (Comma seperated or '-ALL-' or '-NONE-')
	@activityTypeList = Type of activity to be checked  (Comma seperated or '-ALL-') 
	@activityTypeExcludeList = Type of activity to be excluded (Comma seperated or '-NONE-') 
	@cashTranIndicator = Cash Tran indicator to include check on activity type 
	@minimumTotalAmount = Minimum Total Transaction Amount
	@maximumTotalAmount = Maximum Total Threshold amount
	@day = No of days activity to be considered
	@minimumAmount= Minimum threshold amount 
	@maximumAmount = Maximum Threshold Amount
	@minimumCount = Minimum Transaction Count
	@CustomerList = List of customer Id's to be checked
	@RiskClassList = List of risk classes of customer to be checked
	@AccountTypeList = List of Accounts to be checked (Comma Separated or '-ALL-')
	@CustomerTypeList = List of Customer Types
    @BranchList  = List of Branches to be checked (Comma Separated or '-ALL-')
    @DeptList    = List of Department to be checked (Comma Separated or '-ALL-')
	@UseRelAcct = Include Related Accounts (1=yes, 0=no) 
	@UseRelCust = Include Related Parties (1=yes, 0=no) 
	@ExcludeRelList = Exclude Relationship Types (comma separated list or '-NONE-')
*/

/*  Declarations */
DECLARE	@description 	VARCHAR(2000),
	@desc 		VARCHAR(2000),
	@Id 		INT, 
	@WLType 	INT,
	@stat 		INT,
	@trnCnt 	INT,
	@MINDATE 	INT,
	@StartDate 	DATETIME,
	@StartAlrtDate  DATETIME,
	@EndAlrtDate	DATETIME
SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_UNAcc3Mth
ELSE
	-- Already in a transaction
	SAVE TRAN USR_UNAcc3Mth
/* End standard stored procedure transaction header */

/*  standard Rules Header */
-- Date options
-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT  @description = [Desc], @WLType = WLType  ,
	@StartDate = 
	Case 	
		When UseSysDate in (0,1) Then
			-- use System date
			GetDate()
		When UseSysDate = 2 Then
			-- use business date
			(Select BusDate From dbo.SysParam)
		Else
			GetDate()
	End
FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

Declare @BaseCurr char(3)
select @BaseCurr = IsNULL(BaseCurr,'') from SysParam

Create table #TT 
(
	Cust 		VARCHAR(35) ,
	RecvPay		INT,
	TranAmt		MONEY

)

Create table #TT1
(
	Cust	VARCHAR(35) ,
	TranNo	INT,
	BaseAmt MONEY, 
	RecvPay INT,
	bookdate int)

/* modify by danny 2017.05.30 get fromdate 90 days means 3 month */
--a week to to today
--	SET @minDate = dbo.ConvertSqlDateToInt(DATEADD(d, -1 * @day, CONVERT(VARCHAR, @StartDate)))
DECLARE @SQLStartDate datetime, @SQLEndDate datetime, @maxDate int
exec dbo.BSA_GetDateRange @StartDate, 'Month', 'PREV', 3, 
	@SQLStartDate OUTPUT, @SQLEndDate OUTPUT
SET @minDate = dbo.ConvertSqlDateToInt(@SQLStartDate)
set @maxDate = dbo.ConvertSqlDateToInt(@SQLEndDate)

Declare @sDATE INT
SET	@sDATE = dbo.ConvertSqlDateToInt(@StartDate)

--Modify 2017.10 activity must contain last month activity
DECLARE @SQLCurrStartDate datetime, @SQLCurrEndDate datetime, @currMinDate int
exec dbo.BSA_GetDateRange @StartDate, 'Month', 'PREV', 1, 
	@SQLCurrStartDate OUTPUT, @SQLCurrEndDate OUTPUT
SET @currMinDate = dbo.ConvertSqlDateToInt(@SQLCurrStartDate)


	--SELECT @excludeExemptCustList = dbo.BSA_fnListParams(@excludeExemptCustList)
	IF LTRIM(RTRIM(@excludeExemptCustList)) = '-NONE-' OR LTRIM(RTRIM(@excludeExemptCustList)) = ''
		SELECT @excludeExemptCustList = NULL
	ELSE IF LTRIM(RTRIM(@excludeExemptCustList)) = '-ALL-'
			BEGIN
				SELECT @excludeExemptCustList = ''
				SELECT  @excludeExemptCustList = 		
					COALESCE( @excludeExemptCustList + ',','' )  + Code
					FROM ExemptionType 
					 WHERE (SubString(ExemptEntity,1,1) = 1)
					AND (SubString(ExemptFrom,3,1) = 1)
					
				SELECT  @excludeExemptCustList = @excludeExemptCustList + ','
			END
	ELSE                                           
	SELECT @excludeExemptCustList = ',' + 
		REPLACE(LTRIM(RTRIM(@excludeExemptCustList)),' ','')+ ','
		
	--SELECT @activityTypeExcludeList = dbo.BSA_fnListParams(@activityTypeExcludeList)
	IF LTRIM(RTRIM(@activityTypeExcludeList)) = '-NONE-' OR LTRIM(RTRIM(@activityTypeExcludeList)) = ''
		SELECT @activityTypeExcludeList = NULL
	ELSE
	SELECT @activityTypeExcludeList = ',' + 
		REPLACE(LTRIM(RTRIM(@activityTypeExcludeList)),' ','')+ ','
		   
	IF LTRIM(RTRIM(@ExcludeRelList)) = '-NONE-' OR LTRIM(RTRIM(@ExcludeRelList)) = ''
		SELECT @ExcludeRelList = NULL
	ELSE
	SELECT @ExcludeRelList = ',' + 
		REPLACE(LTRIM(RTRIM(@ExcludeRelList)),' ','')+ ','


	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	
	SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)
	SELECT @customerList = dbo.BSA_fnListParams(@customerList)
	SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	SELECT @customerTypeList = dbo.BSA_fnListParams(@customerTypeList)
	SELECT @AccountTypeList = dbo.BSA_fnListParams(@AccountTypeList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
	SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)
	SELECT @ExcludeRelList = dbo.BSA_fnListParams(@ExcludeRelList)
	
		
	INSERT INTO #TT1 
	SELECT distinct	 ActHist.Cust,TranNo, BaseAmt, RecvPay, bookdate
	FROM 	ActivityHist ActHist WITH (NOLOCK) 
	INNER 	JOIN Customer Cust WITH (NOLOCK) ON ActHist.Cust = Cust.ID
	LEFT	JOIN Account Acct WITH (NOLOCK) ON ActHist.Account = Acct.ID 
	WHERE 	ActHist.bookdate >= @minDate 
    AND	ActHist.bookdate <= @maxDate
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Cust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@ActivityTypeExcludeList,'') = '' OR
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeExcludeList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Cust.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Cust.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Cust.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(cust.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(cust.OwnerDept)) + ',', @DeptList) > 0))
	AND (
	(@cashTranIndicator = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR ---include activitytypelist if cash cashTran is 1
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0)))
	OR 
	(@cashTranIndicator = 2 AND (ActHist.CashTran = 1 OR ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
	OR 
	(@cashTranIndicator = 3 AND (ActHist.CashTran = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
    )
	AND	(@minimumAmount = -1 OR BaseAmt >= @minimumAmount) 
	AND (@maximumAmount = -1 OR BaseAmt <= @maximumAmount)
    


	-- Related Parties
	INSERT INTO #TT1 
	SELECT 	distinct pr.PartyId ,TranNo, BaseAmt, RecvPay, bookdate
	FROM 	ActivityHist ActHist WITH (NOLOCK) 
	Left	JOIN Account Acct WITH (NOLOCK) ON ActHist.Account = Acct.ID 
	INNER	JOIN PartyRelation pr WITH (NOLOCK) ON ActHist.Cust = pr.RelatedParty
	INNER	JOIN Customer RelCust WITH (NOLOCK) ON pr.PartyId = RelCust.ID
	WHERE 	ActHist.bookdate >= @minDate 
    AND	ActHist.bookdate <= @maxDate
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@ActivityTypeExcludeList,'') = '' OR
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeExcludeList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerDept)) + ',', @DeptList) > 0))
	AND (
	(@cashTranIndicator = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR ---include activitytypelist if cash cashTran is 1
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0)))
	OR 
	(@cashTranIndicator = 2 AND (ActHist.CashTran = 1 OR ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
	OR 
	(@cashTranIndicator = 3 AND (ActHist.CashTran = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
    )
	AND	(@minimumAmount = -1 OR BaseAmt >= @minimumAmount) 
	AND (@maximumAmount = -1 OR BaseAmt <= @maximumAmount)
	AND @UseRelCust = 1 AND ActHist.Cust <> pr.PartyID AND pr.Deleted <> 1 
	AND ((ISNULL(@ExcludeRelList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND Exists (select * from #TT1 tt Where tt.Cust = pr.PartyId)
	AND Not Exists (select * from #TT1 tt Where tt.Cust = pr.PartyId AND tt.TranNo = ActHist.tranno)
    

	-- Related Parties (reverse direction)
	INSERT INTO #TT1 
	SELECT 	distinct pr.RelatedParty, TranNo, BaseAmt, RecvPay, bookdate
	FROM 	ActivityHist ActHist WITH (NOLOCK) 
	Left	JOIN Account Acct WITH (NOLOCK) ON ActHist.Account = Acct.ID 
	INNER	JOIN PartyRelation pr WITH (NOLOCK) ON ActHist.Cust = pr.PartyID
	INNER	JOIN Customer RelCust WITH (NOLOCK) ON pr.RelatedParty = RelCust.ID
	WHERE 	ActHist.bookdate >= @minDate 
    AND	ActHist.bookdate <= @maxDate
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@ActivityTypeExcludeList,'') = '' OR
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeExcludeList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerDept)) + ',', @DeptList) > 0))
	AND (
	(@cashTranIndicator = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR ---include activitytypelist if cash cashTran is 1
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0)))
	OR 
	(@cashTranIndicator = 2 AND (ActHist.CashTran = 1 OR ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
	OR 
	(@cashTranIndicator = 3 AND (ActHist.CashTran = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
    )
	AND	(@minimumAmount = -1 OR BaseAmt >= @minimumAmount) 
	AND (@maximumAmount = -1 OR BaseAmt <= @maximumAmount)
	AND @UseRelCust = 1 AND ActHist.Cust <> pr.RelatedParty AND pr.Deleted <> 1 
	AND ((ISNULL(@ExcludeRelList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND Exists (select * from #TT1 tt Where tt.Cust = pr.RelatedParty)
	AND Not Exists (select * from #TT1 tt Where tt.Cust = pr.RelatedParty AND tt.TranNo = ActHist.tranno)
    

	-- Related Accounts
	INSERT INTO #TT1 
	SELECT 	distinct ao.Cust,TranNo, BaseAmt, RecvPay, bookdate
	FROM 	ActivityHist ActHist WITH (NOLOCK) 
	INNER 	JOIN Customer Cust WITH (NOLOCK) ON ActHist.Cust = Cust.ID
	INNER   JOIN AccountOwner ao WITH (NOLOCK)
	ON ActHist.Account = ao.Account and ActHist.Cust <> ao.cust
	INNER	JOIN Account Acct WITH (NOLOCK) ON ao.Account = Acct.ID 
	INNER	JOIN Customer CustOnAcct WITH (NOLOCK) ON ao.Cust = CustOnAcct.ID
	WHERE 	ActHist.bookdate >= @minDate 
    AND	ActHist.bookdate <= @maxDate
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Cust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(CustOnAcct.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@ActivityTypeExcludeList,'') = '' OR
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeExcludeList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(CustOnAcct.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(CustOnAcct.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(CustOnAcct.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(custOnAcct.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(custOnAcct.OwnerDept)) + ',', @DeptList) > 0))
	AND (
	(@cashTranIndicator = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR ---include activitytypelist if cash cashTran is 1
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0)))
	OR 
	(@cashTranIndicator = 2 AND (ActHist.CashTran = 1 OR ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
	OR 
	(@cashTranIndicator = 3 AND (ActHist.CashTran = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
    )
	AND	(@minimumAmount = -1 OR BaseAmt >= @minimumAmount) 
	AND (@maximumAmount = -1 OR BaseAmt <= @maximumAmount)
	AND @UseRelAcct = 1 
	AND ((ISNULL(@ExcludeRelList,'') = '' OR ao.Account = ActHist.Account OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ao.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND Exists (select * from #TT1 tt Where tt.Cust = ao.Cust)
    AND Not Exists (select * from #TT1 tt Where tt.Cust = ao.Cust AND tt.TranNo = ActHist.tranno)
    

	INSERT INTO #TT
	SELECT 	Cust, RecvPay, SUM(BaseAmt)
	FROM 	#TT1 o
	GROUP 	BY Cust, RecvPay
	HAVING 	(@minimumTotalAmount = -1 OR SUM(BaseAmt) >= @minimumTotalAmount)
	AND (@maximumTotalAmount = -1 OR SUM(BaseAmt) <= @maximumTotalAmount)
	AND	COUNT(TranNo) >= @minimumCount
	--Modify 2017.10 activity must contain last month activity
	and (select count(*) from #TT1 d where bookdate >= @currMinDate and o.cust= d.cust and o.recvpay = d.recvpay) > 0


	IF @testAlert = 1 
	BEGIN
		SELECT @StartAlrtDate = GETDATE()
		INSERT INTO Alert (WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT, IsTest) 
		  SELECT @WLCode, 
			 CASE WHEN RecvPay = 1 THEN
				'Customer:  ''' + Cust
				+ ''' had ' 
				+ DBO.BSA_InternalizationMoneyToString( TranAmt) + Space(1) + @BaseCurr + 
				+ ' in deposits over a period of 3 Months.'
			 ELSE
				'Customer:  ''' + Cust
				+ ''' had  ' 
				+ DBO.BSA_InternalizationMoneyToString (TranAmt)  + Space(1) + @BaseCurr 
				+ ' in withdrawals over a period of 3 Months.'
			 END,0,
			GETDATE(), NULL, NULL, Cust, NULL , 1
		FROM #TT 
		SELECT @STAT = @@ERROR	
		SELECT @EndAlrtDate = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC

		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	Distinct 'Alert', AlertNo, TT.TRANNO 
		   	FROM 	Alert,#TT1 TT
			WHERE 	Alert.Cust = TT.Cust 
			AND ((Charindex('deposits', Alert.[Desc]) > 0 AND RecvPay = 1) OR
                 (Charindex('withdrawals', Alert.[Desc]) > 0 AND RecvPay = 2))
			AND 	Alert.WLCode = @WLCode 
			AND 	Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate	
			
			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc

	END 
	ELSE 
	BEGIN		    
		IF @WLType = 0 BEGIN
			SELECT @StartAlrtDate = GETDATE()
			INSERT INTO Alert (WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
					LASTMODIFY, CUST, ACCOUNT, IsTest) 
			  SELECT @WLCode, 
				 CASE WHEN RecvPay = 1 THEN
					'Customer:  ''' + Cust + ''' had ' 
					+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +Space(1) + @BaseCurr + 
					+ ' in deposits over a period of 3 Months.'
				 ELSE
					'Customer:  ''' + Cust
					+ ''' had ' 
					+ DBO.BSA_InternalizationMoneyToString(TranAmt) + Space(1) + @BaseCurr  
					+ ' in withdrawals over a period of 3 Months.'
				END,0,
				GETDATE(), NULL, NULL, Cust, NULL , 0
			FROM #TT 
			SELECT @STAT = @@ERROR	
			SELECT @EndAlrtDate = GETDATE()
		
			IF @stat <> 0 GOTO EndOfProc
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	Distinct 'Alert', AlertNo, TT.TRANNO 
				FROM 	Alert,#TT1 TT
			   	WHERE 	Alert.Cust = TT.Cust 
				AND ((Charindex('deposits', Alert.[Desc]) > 0 AND RecvPay = 1) OR
					 (Charindex('withdrawals', Alert.[Desc]) > 0 AND RecvPay = 2))
			   	AND 	Alert.WLCode = @WLCode 
				AND 	Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate	
				
			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc
			END 
		ELSE IF @WLType = 1 
		BEGIN	
		
			SELECT @StartAlrtDate = GETDATE()
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
				ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
				RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
				ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
				EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
				OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
				REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
				WLCode, WLDESC, CREATETIME )
			SELECT	NULL, @minDate, Cust, NULL,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, 
				CASE WHEN RecvPay = 1 THEN
					'Customer:  ''' + Cust + ''' had ' 
					+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +Space(1) + @BaseCurr + 
					+ ' in deposits over a period of 3 Months.'
				 ELSE
					'Customer:  ''' + Cust + ''' had ' 
					+ DBO.BSA_InternalizationMoneyToString(TranAmt) + Space(1) + @BaseCurr  
					+ ' in withdrawals over a period of 3 Months.'
				END , GETDATE() 
			FROM #TT
			SELECT @STAT = @@ERROR	
			SELECT @EndAlrtDate = GETDATE()
			IF @stat <> 0 GOTO EndOfProc
		
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	Distinct 'SUSPACT', RecNo, TT.TRANNO 
				FROM 	SuspiciousActivity SuspAct,#TT1 TT
				WHERE	SuspAct.Cust = TT.Cust
				AND ((Charindex('deposits', WLDesc) > 0 AND RecvPay = 1) OR
					 (Charindex('withdrawals', WLDesc) > 0 AND RecvPay = 2))
				AND	SuspAct.WlCode = @WlCode
				AND	SuspAct.CreateTime BETWEEN @StartAlrtDate AND @EndAlrtDate	
				
			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc
		END
	END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_UNAcc3Mth

  drop table #tt
  drop table #tt1

  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_UNAcc3Mth


drop table #tt
drop table #tt1

RETURN @stat

GO


