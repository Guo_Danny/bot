DECLARE @strSysDate char(100), @TempStr VARCHAR(5000);
select @strSysDate=rtrim(replace(CONVERT(CHAR(10),GETDATE()-1,111),'/','') ) ;
--SELECT @strsysdate='2018-03-21';
select @strsysdate, len(@strsysdate);

DECLARE @STRTEMP1 VARCHAR(200), @STRTEMP2 VARCHAR(200), @STRTEMP3 VARCHAR(200), @STRTEMP3_1 VARCHAR(200), @STRTEMP4 VARCHAR(200);
DECLARE @STRTEMP5 VARCHAR(200), @STRTEMP6 VARCHAR(200), @STRTEMP7 VARCHAR(200), @STRTEMP8 VARCHAR(200);
DECLARE @STRTEMP9 VARCHAR(200), @STRTEMP10 VARCHAR(200), @STRTEMP11 VARCHAR(200), @STRTEMP12 VARCHAR(200);
SELECT @STRTEMP1='insert into ofac..ReconTable (Source,MsgRef,TranSearchType,SancPartyCount,FileImage,Branch,Department,CaseReference,UserMessageReference,SearchOper,SearchDate,CreateDate) ';
SELECT @STRTEMP2=' select a.Source,a.ref,'+''''+'1'+''''+',a.MatchCount,a.FileImage,a.Branch,a.Dept,a.Ref,a.UserMessageReference,'+''''+'Prime'+''''+',a.TranTime,GETDATE() from ofac..FilterTranHistTable a  where 1=1';
SELECT @STRTEMP3=' AND a.trantime>=CONVERT(CHAR(10),GETDATE()-15,101)' + '+'''+' 00:00:00'+'''' + ' AND a.trantime<=CONVERT(CHAR(10),GETDATE(),101)' + '+'''+' 00:00:00'+''''; 
SELECT @STRTEMP3_1=' AND a.ref LIKE '+ ' '''+ '%' + rtrim(@strsysdate) + '%' + '''' ;
SELECT @STRTEMP4=' AND Upper(a.Source)='+''''+'SWIFT'+''''+' and not exists (select * from ofac..ReconTable b where Upper(b.source)='+''''+'SWIFT'+'''';
SELECT @STRTEMP5=' AND b.searchDate>=CONVERT(CHAR(10),GETDATE()-15,101)' + '+'''+' 00:00:00'+'''' + ' AND b.searchDate<=CONVERT(CHAR(10),GETDATE(),101)' + '+'''+' 00:00:00'+'''';
SELECT @STRTEMP6=' AND CaseReference LIKE '+ ' '''+ '%' + rtrim(@strsysdate) + '%' + '''' +' and a.Ref = b.CaseReference )';

SELECT @STRTEMP7='union select a.Source,a.ref,'+''''+'1'+''''+',a.MatchCount,a.FileImage,a.Branch,a.Dept,a.Ref,a.UserMessageReference,'+''''+'Prime'+''''+',a.TranTime,GETDATE() from ofac..FilterTranTable a  where 1=1 and a.Ref not like '+ ' '''+ '%CIF%' + '''' ;
--SELECT @STRTEMP2
--SELECT @STRTEMP3
--SELECT @STRTEMP4
--SELECT @STRTEMP5
--SELECT @STRTEMP6

SELECT @TEMPSTR=RTRIM(@STRTEMP1)+RTRIM(@STRTEMP2)+RTRIM(@STRTEMP3)+@STRTEMP3_1+RTRIM(@STRTEMP4)+RTRIM(@STRTEMP5)+RTRIM(@STRTEMP6)+RTRIM(@STRTEMP7)+RTRIM(@STRTEMP3)+RTRIM(@STRTEMP3_1)+RTRIM(@STRTEMP4)+RTRIM(@STRTEMP5)+RTRIM(@STRTEMP6);
--SELECT @TEMPSTR=RTRIM(@STRTEMP2)+RTRIM(@STRTEMP3)+RTRIM(@STRTEMP4)+RTRIM(@STRTEMP5)+RTRIM(@STRTEMP6);
SELECT @TEMPSTR,LEN(@TEMPSTR);
EXEC (@TEMPSTR)

select @STRTEMP8 = convert(char(10),count(*)) from ReconTable where SearchOper='Prime' and rtrim(replace(CONVERT(CHAR(10),SearchDate,111),'/','') )=@strsysdate

insert into OFAC..SDNChangeLog (logdate, oper, logtext, type, objecttype, objectid) 
values(getdate(), 'Prime', 'Missing records total inserted is :' + @STRTEMP8 + ' records', 'Ann', 'Event', 'Reconfix')