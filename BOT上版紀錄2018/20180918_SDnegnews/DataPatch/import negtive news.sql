--select count(*) 
update SDNTable set ListType = 'WORLD-CHECK' , Program = 'Negative news'
from SDNTable(nolock) a
join WorldCheck(nolock) b on a.EntNum = b.Entnum
where isnull(b.DelFlag,0) = 0
and 
((Category = 'BANK') or (Category = 'CRIME - OTHER' and FurtherInfo like '%Arms %')or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Bribery %' or FurtherInfo like '%Corruption %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Fraud %' or FurtherInfo like '%Embezzlement %' or FurtherInfo like '%Theft %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Tax %'))or (Category = 'CRIME - OTHER' and (FurtherInfo like '%Human Trafficking %' or FurtherInfo like '%Human Rights %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Insider Trading %' or FurtherInfo like '%Market Manipulation %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Money Laundering %'))or (Category = 'CRIME - NARCOTICS' and (FurtherInfo like '%Narcotics %'))or (Category = 'CRIME - ORGANIZED')or (Category = 'CRIME - OTHER' and (FurtherInfo like '%Pharmaceutical %' or FurtherInfo like '%Illegal distribution %' or FurtherInfo like '%Illegal production %' or FurtherInfo like '%Banned %' or FurtherInfo like '%Fake medicines %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Securities %'))or (Category = 'TERRORISM')or (Category = 'CRIME - WAR') )

--insert
declare @date datetime; set @date = getdate();
begin tran
insert into SDNTable(EntNum,
    Name,
    FirstName,LastName,
    ListType,
    Program,
    Title,
    [type],Dob,PrimeAdded,
    Country,
    Remarks,
    Remarks1,
    Remarks2,
    SDNType,DataType,
    UserRec,Deleted,DuplicRec,IgnoreDerived,Status,ListCreateDate,ListModifDate,CreateDate,LastModifDate,LastOper)
select Entnum, 
    ltrim(case when isnull(FirstName,'') = '-' then '' else isnull(FirstName,'') end + ' ' +isnull(LastName,'')) name, 
    ltrim(case when isnull(FirstName,'') = '-' then '' else isnull(FirstName,'') end FirstName, isnull(LastName,'') LastName, 
    'WORLD-CHECK' listtype,
    'Negative news' program,
    SUBSTRING(isnull(Position,''),0,200) Position,
    [type],dob,'0' primeadded,
    Country,
    FurtherInfo + ',Keywords:' + isnull(Keywords,'')+',isnull(ExSources,''):'+ExSources,
    SUBSTRING(isnull(Keywords,''),0,255) Keywords,
    SUBSTRING(isnull(Location,''),0,255) Location,
    '1' sdntype, '1' datatype, '1' userrec,'0' deleted,'0' duplicRec,
    '0' ignorederived,'1' [status],Entered,Updated,@date createdate,@date lastmodifdate,'Prime' lastoper
from [dbo].[WorldCheck] wc
where isnull(DelFlag,'0') = '0'
and ((Category = 'BANK') or (Category = 'CRIME - OTHER' and FurtherInfo like '%Arms %')or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Bribery %' or FurtherInfo like '%Corruption %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Fraud %' or FurtherInfo like '%Embezzlement %' or FurtherInfo like '%Theft %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Tax %'))or (Category = 'CRIME - OTHER' and (FurtherInfo like '%Human Trafficking %' or FurtherInfo like '%Human Rights %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Insider Trading %' or FurtherInfo like '%Market Manipulation %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Money Laundering %'))or (Category = 'CRIME - NARCOTICS' and (FurtherInfo like '%Narcotics %'))or (Category = 'CRIME - ORGANIZED')or (Category = 'CRIME - OTHER' and (FurtherInfo like '%Pharmaceutical %' or FurtherInfo like '%Illegal distribution %' or FurtherInfo like '%Illegal production %' or FurtherInfo like '%Banned %' or FurtherInfo like '%Fake medicines %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Securities %'))or (Category = 'TERRORISM')or (Category = 'CRIME - WAR') )
and wc.Entnum not in (select Entnum from SDNTable(nolock) where EntNum = wc.Entnum)
