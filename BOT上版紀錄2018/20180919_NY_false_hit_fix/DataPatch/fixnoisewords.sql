-- delete noisewords
select * from noisewords
where word in (
'MIN',
'DE',
'DU',
'NA',
'Y',
'I',
'LO',
'GEN',
'THE',
'INC',
'AU',
'PARA',
'LE',
'A',
'DO'
)

delete noisewords
where word in (
'MIN',
'DE',
'DU',
'NA',
'Y',
'I',
'LO',
'GEN',
'THE',
'INC',
'AU',
'PARA',
'LE',
'A',
'DO'
)


--add noise word

insert into NoiseWords (word,CreateDate,LastOper) values ('ID',getdate(),'Prime')

--delete altname
/*23903067-KAO
23069994-Ri
21306652-HUA,HUGI,HUJI
20820644-GLOBAL
21999518-GLOBAL
200376831-DAVE
20451064-'TEK',TEK
*/
select * from SDNAltTable where 
EntNum in (
23903067,
23069994,
21306652,
20820644,
21999518,
200376831,
20451064
) and AltName in ('KAO','Ri','HUA','HUGI','HUJI','GLOBAL','DAVE','TEK','''TEK''')

update SDNAltTable set Status = 4, Deleted = 1 where 
EntNum in (
23903067,
23069994,
21306652,
20820644,
21999518,
200376831,
20451064
) and AltName in ('KAO','Ri','HUA','HUGI','HUJI','GLOBAL','DAVE','TEK','''TEK''')
