begin tran
insert into Customer(id,Parent,name,Address,State,Zip,Country,LastActReset,CreateOper,CreateDate,OwnerBranch,RiskClass,Closed,BearerShares,NomeDePlume,Type,Resident,PEP,FEP,MSB,CorrBankRelation,
ShellBank,HighRiskRespBank,OffshoreBank,PayThroughAC,RegulatedAffiliate,RFCalculate,Status,CTRAmt,Embassy,ForeignGovt,CharityOrg,DoCIP,
Prospect,IndOrBusType,OnProbation,KYCStatus)
values('WFBIUS6S','WFBIUS6S','WELLS FARGO BANK, N.A.','420 MINTGOMERY STREET SAN','FRANCISCO,CA','94104','US','1899-12-30 00:00:00.000','PRIMEADMIN',getdate(),'0083','DefClass',0,0,0,'B',0,0,0,0,0,
0,0,0,0,0,0,1,0,0,0,0,0,
0,'B',0,0)

insert into Customer(id,Parent,name,Address,State,Zip,Country,LastActReset,CreateOper,CreateDate,OwnerBranch,RiskClass,Closed,BearerShares,NomeDePlume,Type,Resident,PEP,FEP,MSB,CorrBankRelation,
ShellBank,HighRiskRespBank,OffshoreBank,PayThroughAC,RegulatedAffiliate,RFCalculate,Status,CTRAmt,Embassy,ForeignGovt,CharityOrg,DoCIP,
Prospect,IndOrBusType,OnProbation,KYCStatus)
values('BKTWTWTP283','BKTWTWTP283','BKTWTWTP283','','','','TW','1899-12-30 00:00:00.000','PRIMEADMIN',getdate(),'0083','DefClass',0,0,0,'B',0,0,0,0,0,
0,0,0,0,0,0,1,0,0,0,0,0,
0,'B',0,0)

insert into Account(id,name,type,Monitor,Branch,OpenDate,RiskFactor,CreateOper,CreateDate,
OwnerBranch,Closed,Request,CashValue,SinglePremium,PolicyAmount,
ReqWireTrans,AccACHPay,OrgACHPay,ReqForInWireTrans,IsProspect,PowerOfAttorney,RemoteDepositCapture)
values
('50168500070','WELLS FARGO BANK, N.A.','Q',0,'0083',getdate(),0,'PRIMEADMIN',getdate(),
'0083',0,0,0,0,0,
0,0,0,0,0,0,0)


insert into Account(id,name,type,Monitor,Branch,OpenDate,RiskFactor,CreateOper,CreateDate,
OwnerBranch,Closed,Request,CashValue,SinglePremium,PolicyAmount,
ReqWireTrans,AccACHPay,OrgACHPay,ReqForInWireTrans,IsProspect,PowerOfAttorney,RemoteDepositCapture)
values
('083035000036','BKTWTWTP283','Q',0,'0083',getdate(),0,'PRIMEADMIN',getdate(),
'0083',0,0,0,0,0,
0,0,0,0,0,0,0)

insert into AccountOwner(Account,Cust,Relationship,CreateOper,CreateDate)
values
('50168500070','WFBIUS6S','OWNER','PRIMEADMIN',getdate())

insert into AccountOwner(Account,Cust,Relationship,CreateOper,CreateDate)
values
('083035000036','BKTWTWTP283','OWNER','PRIMEADMIN',getdate())

commit