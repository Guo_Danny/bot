update Watchlist
set Params = '<Params><Param Name="@minIndAmt" Alias="Minimum individual transaction amount" Value="50000" DataType="Money" /><Param Name="@maxIndAmt" Alias="Maximum individual transaction amount. For no maximum, use -1" Value="-1" DataType="Money" /><Param Name="@minCount" Alias="Minimum transaction count" Value="1" DataType="Int" /><Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. Use -ALL- for All." Value="1013,1023,1033,1043" DataType="String" /><Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@minSumAmount" Alias="Minimum aggregated transaction amount." Value="50000" /></Params>'
,LastOper = 'PRIMEADMIN', IsPreEOD = 0 where wlcode = 'SameBeneOrg'

update Watchlist set Desc = 'Detects accounts that had transactions using a specified activity type  and the total of these transactions were between a designated dollar  amount range within 1 calender month.  Designed to be run Post-EOD.  20181019 modify by SD branch, change from 1 month to 1 day.'
,LastOper = 'PRIMEADMIN' where WLCode = 'U1MnthBetXY'


begin tran
select * from Watchlist
where IsUserDefined = 1
--and WLCode like '282%'


update Watchlist set UseSysDate = 0
where IsUserDefined = 1
--and WLCode like '282%'

update Watchlist set Schedule = 2
where IsUserDefined = 1
and WLCode = 'U1MnthBetXY'

update Watchlist set Schedule = 0
where IsUserDefined = 1
and WLCode in ('IHRCtry1Mth', 'IHRCtry3Mth', 'OHRCtry1Mth', 'OHRCtry3Mth', 'LgBankHRCty', 'UAcctRskCty', 'UBneEQByOrd')



commit