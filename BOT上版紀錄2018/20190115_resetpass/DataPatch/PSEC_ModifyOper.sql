USE [Psec]
GO

/****** Object:  StoredProcedure [dbo].[PSEC_ModifyOper]    Script Date: 1/15/2019 3:18:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[PSEC_ModifyOper] @code Code,
                                 @userName varchar (255),
                                 @branch Code,
                                 @dept Code,
                                 @enable bit,
                                 @deleteF bit,
                                 @security int,
                                 @cdb int,
                                 @message int,
                                 @msg2 int,
                                 @testkey int,
                                 @ofac int,
                                 @reimb int,
                                 @name char (35),
                                 @eMail varchar (100),
                                 @oper Code,
                                 @secToken int,
                                 @pwd  varchar(100),
								 @forcePwdChg Bit,
                                 @hintQ varchar(255),
                                 @hintAns varchar(255),
								 @Approved bit
  As

	-- Start standard stored procedure transaction header
	DECLARE @trnCnt int
	SELECT @trnCnt = @@trancount	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN PSEC_ModifyOper
	ELSE
		-- Already in a transaction
 		SAVE TRAN PSEC_ModifyOper
	-- End standard stored procedure transaction header

	declare @app      bit
	declare @appOp    Code
	declare @appDate  datetime
  	declare @ModifiedByOper Code
	declare @optOn    int
  	declare @stat     int
	declare @now      datetime
	declare @ExpirationDate datetime, 
		@LastPwdChgDate datetime, 
		@LastPwdChgOper Code,
		@ExpPeriod int,
		@NumPwdHist int
	declare @Oldpwd varchar(100), @OldsecToken int

	--2018.12 fix the issue that over try to login and operator lock.
	declare @enab_o bit

	select @now = GETDATE()
	Set @optOn = 1

	SELECT @Oldpwd = Password, @OldsecToken = SecurityToken from Operator where UserName = @Username
	Set @ModifiedByOper =  ISNULL(RTRIM(@oper),'PRIME')

	--2018.12 fix the issue that over try to login and operator lock.
	Select @app = Approved, @enab_o = [Enable] from Operator where Code = RTRIM(@code)

	Select @optOn = Enabled from OptionTbl where OptCode = 'PwdConfig'

	Set @LastPwdChgDate = NULL
	Set @LastPwdChgOper = NULL
	Set @ExpirationDate = NULL

	IF isnull(@pwd,'') <> isnull(@Oldpwd,'')
	BEGIN
		Set @LastPwdChgDate = @now
		Set @LastPwdChgOper = @oper
		If @optOn = 1 Begin
			SELECT @ExpPeriod = ParameterValue FROM OptionParameter 
				WHERE OptCode = 'PwdConfig' AND ParameterCode = 'PwdExpPer'
			If IsNull(@ExpPeriod ,0) > 0
				Set @ExpirationDate = DateAdd(dd, @ExpPeriod, getdate())
		END
	END
	
	  /* This stored proc is specifically used for modifying operator and update
	     Last Modify Time and Last Modify By. Last Approved Time and Last Approved By 
	     are updated using different stored procedure PSEC_ApproveOper. */

	--2018.12 fix the issue that over try to login and operator lock.
	if isnull(@enab_o,'') <> isnull(@enable,'') and @enable = 1
		begin

			UPDATE Operator
				SET Branch = @branch, UserName = RTRIM(@userName), Dept = @dept,
				Enable = @enable, DeleteF = @deleteF, SecurityPrMsk = @security,
				CDBPrMsk = @cdb, MessagePrMsk = @message, Message2PrMsk = @msg2,
				TestkeyPrMsk = @testkey, OFACPrMsk = @ofac, ReimbPrMsk = @reimb,
				Name = RTRIM(@name), EMail = NULLIF(RTRIM(@eMail), ''), LastModify = @now,
				LastOper = @ModifiedByOper,  SecurityToken = @secToken,
				Password = NullIF(@pwd, ''), HintQuestion = NullIF(@hintQ, ''), Approved = @Approved,
				HintAnswer = NullIF(@hintAns, ''), ForcePasswordChange = @forcePwdChg, 
				LastPwdChgDate=ISNULL(@LastPwdChgDate,LastPwdChgDate),
				LastPwdChgOper=isnull(@LastPwdChgOper,LastPwdChgOper), 
				ExpirationDate=isnull(@ExpirationDate,ExpirationDate),
				MaxAttempts = 0
			WHERE Code = RTRIM(@code)
		end
	else
		begin
			UPDATE Operator
				SET Branch = @branch, UserName = RTRIM(@userName), Dept = @dept,
				Enable = @enable, DeleteF = @deleteF, SecurityPrMsk = @security,
				CDBPrMsk = @cdb, MessagePrMsk = @message, Message2PrMsk = @msg2,
				TestkeyPrMsk = @testkey, OFACPrMsk = @ofac, ReimbPrMsk = @reimb,
				Name = RTRIM(@name), EMail = NULLIF(RTRIM(@eMail), ''), LastModify = @now,
				LastOper = @ModifiedByOper,  SecurityToken = @secToken,
				Password = NullIF(@pwd, ''), HintQuestion = NullIF(@hintQ, ''), Approved = @Approved,
				HintAnswer = NullIF(@hintAns, ''), ForcePasswordChange = @forcePwdChg, 
				LastPwdChgDate=ISNULL(@LastPwdChgDate,LastPwdChgDate),
				LastPwdChgOper=isnull(@LastPwdChgOper,LastPwdChgOper), 
				ExpirationDate=isnull(@ExpirationDate,ExpirationDate)
			WHERE Code = RTRIM(@code)
		end

	select @stat = @@ERROR
	if @stat <> 0
	begin
		ROLLBACK TRAN PSEC_ModifyOper
		RETURN @stat
	end

--	Insert password history records
	If @optOn = 1 AND (isnull(@pwd,'') <> isnull(@Oldpwd,''))
	BEGIN
--		Get the parameter for number of history records to be maintained
		SELECT @NumPwdHist = ParameterValue FROM OptionParameter 
			WHERE OptCode = 'PwdConfig' AND ParameterCode = 'NumPwdHist'
		If IsNull(@NumPwdHist ,0) > 0
		BEGIN
--			Increment SeqNo for all history
			UPDATE PasswordHist SET SeqNo = SeqNo+1
				WHERE Code = @Code
			SELECT @stat = @@error
			IF ( @stat <> 0 )
			BEGIN
				ROLLBACK TRAN PSEC_ModifyOper
				RETURN @stat
			END
--			Insert the new history record
			EXEC @stat = PSEC_InsPasswordHist @Code, 1, @OldPwd, @Oper
			IF ( @stat <> 0 )
			BEGIN
				ROLLBACK TRAN PSEC_ModifyOper
				RETURN @stat
			END

--			Delete remaining history records
			DELETE FROM PasswordHist WHERE Code = @Code And SeqNo > @NumPwdHist
			IF ( @stat <> 0 )
			BEGIN
				ROLLBACK TRAN PSEC_ModifyOper
				RETURN @stat
			END

		END
	END
	IF @trnCnt = 0
		COMMIT TRAN PSEC_ModifyOper
	
	return @stat

GO


