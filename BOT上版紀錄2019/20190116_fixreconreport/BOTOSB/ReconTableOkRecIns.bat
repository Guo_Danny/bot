REM --- SELECT OFAC..FilterTranHistTable (CASE TABLE) NOT IN OFAC..ReconTable INTO OFAC..ReconTable ---
copy /Y D:\Prime\Temp\DIR-OK-INS.OUT \\tosbamldbv4\prime\temp\DIR-OK-INS.OUT

SQLCMD -E -S TOSBAMLDBV4 -i"D:\PRIME\BOTOSB\ReconTableOkRecIns.sql" -o"D:\PRIME\BOTOSB\ZXRECON-INS-OK.OUT"

REM del \\tosbamldbv4\prime\temp\DIR-OK-INS.OUT

goto EndProc

REM --- BACKUP ZXRECON.TXT ---
REM SET sDate = %date:~4,2%%date:~7,2%%date:~10,4%
REM SET sDATE=%date:~0,2%%date:~3,2%
REM SET sDATE=%date:~10,4%%date:~4,2%%date:~7,2%
SET sDATE=%date:~7,2%
ECHO %SDATE%

REM set sbkfile=D:\ftproot\ReconReport\backup\ZXRECON.TXT.%%date:~4,2%%date:~7,2%%date:~10,4%
REM set cmdcopy =copy /Y D:\ftproot\ReconReport\ZXRECON.TXT D:\ftproot\ReconReport\backup\ZXRECON.TXT.%date:~4,2%%date:~7,2%%date:~10,4%
set cmdcopy=copy /Y D:\ftproot\ReconReport\ZXRECON.TXT D:\ftproot\ReconReport\backup\ZXRECON.TXT.%sdate%
%cmdcopy% 

:EndPrpc
