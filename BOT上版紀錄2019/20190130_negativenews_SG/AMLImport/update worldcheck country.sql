update WorldCheckCountry set Status = 0
where code in ('AT','BH','CY','BR')

update WorldCheckCountry set Status = 1
where code in ('BE','EG','KW','KY','TR','JO')

--先把新增的轉進去
select sdn.EntNum from SDNTable(nolock) sdn
join WorldCheckCountry wcc on sdn.Country = wcc.WorldCheck_Country
where wcc.Status = 1
and sdn.Status != 4
and sdn.ListType = 'WPEP'

update SDNTable set ListType = '0138WPEP' where EntNum in (
select sdn.EntNum from SDNTable(nolock) sdn
join WorldCheckCountry wcc on sdn.Country = wcc.WorldCheck_Country
where wcc.Status = 1
and sdn.Status != 4
and sdn.ListType = 'WPEP'
)

--再把不要的移除
select sdn.EntNum from SDNTable(nolock) sdn
join WorldCheckCountry wcc on sdn.Country = wcc.WorldCheck_Country
where wcc.Status = 0
and sdn.Status != 4
and sdn.ListType = '0138WPEP'

update SDNTable set ListType = 'WPEP' where EntNum in (
select sdn.EntNum from SDNTable(nolock) sdn
join WorldCheckCountry wcc on sdn.Country = wcc.WorldCheck_Country
where wcc.Status = 0
and sdn.Status != 4
and sdn.ListType = '0138WPEP'
)

--12/28 更新 控制type來決定要不要轉入SDN
select * from WorldCheckCountry

--begin tran
--ALTER TABLE WorldCheckCountry ADD [Type] char(100);
--commit

--建index
USE [OFAC]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [UID_Word_WCKeywords_IDX]    Script Date: 1/31/2019 11:44:35 AM ******/
CREATE NONCLUSTERED INDEX [UID_Word_WCKeywords_IDX] ON [dbo].[WCKeywords]
(
	[UID] ASC,
	[Word] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


--根據WCK 來重新轉入負面消息與其他黑名單 for SG and HK
select distinct SDNPROGRAM from WCKeywordslistTable

update SDNTable set Program = 'other' where Program = 'Adverse News'
and ListType = 'world-check'

update SDNTable set ListType = 'WORLD-CHECK',Program = wckl.SDNPROGRAM,LastModifDate = getdate()
from SDNTable a join WorldCheck wc on a.EntNum = wc.Entnum
left join WCKeywords wck on wc.UID = wck.uid
left join WCKeywordslistTable wckl on wck.Word = wckl.Abbreviation
where isnull(wc.DelFlag,'0') = '0'
--and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
and ((wckl.Used = 1 and wckl.SDNPROGRAM = 'Adverse News')
or wc.Category like '%CRIME%' 
)


select Program,count(*) from SDNTable(nolock)
where ListType = 'world-check'
group by Program


-- for SD
update SDNTable set ListType = 'WORLD-CHECK', Program = 'Negative News'
from SDNTable a join WorldCheck wc
on a.EntNum = wc.Entnum
where isnull(wc.DelFlag,'0') = '0' 
and ((Category = 'BANK') or (Category = 'CRIME - OTHER' and FurtherInfo like '%Arms %')or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Bribery %' or FurtherInfo like '%Corruption %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Fraud %' or FurtherInfo like '%Embezzlement %' or FurtherInfo like '%Theft %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Tax %'))or (Category = 'CRIME - OTHER' and (FurtherInfo like '%Human Trafficking %' or FurtherInfo like '%Human Rights %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Insider Trading %' or FurtherInfo like '%Market Manipulation %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Money Laundering %'))or (Category = 'CRIME - NARCOTICS' and (FurtherInfo like '%Narcotics %'))or (Category = 'CRIME - ORGANIZED')or (Category = 'CRIME - OTHER' and (FurtherInfo like '%Pharmaceutical %' or FurtherInfo like '%Illegal distribution %' or FurtherInfo like '%Illegal production %' or FurtherInfo like '%Banned %' or FurtherInfo like '%Fake medicines %'))or (Category = 'CRIME - FINANCIAL' and (FurtherInfo like '%Securities %'))or (Category = 'TERRORISM')or (Category = 'CRIME - WAR')
) 
