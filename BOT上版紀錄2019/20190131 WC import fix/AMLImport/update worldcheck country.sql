update WorldCheckCountry set Status = 0
where code in ('AT','BH','CY','BR')

update WorldCheckCountry set Status = 1
where code in ('BE','EG','KW','KY','TR','JO')

--先把新增的轉進去
select sdn.EntNum from SDNTable(nolock) sdn
join WorldCheckCountry wcc on sdn.Country = wcc.WorldCheck_Country
where wcc.Status = 1
and sdn.Status != 4
and sdn.ListType = 'WPEP'

update SDNTable set ListType = '0138WPEP' where EntNum in (
select sdn.EntNum from SDNTable(nolock) sdn
join WorldCheckCountry wcc on sdn.Country = wcc.WorldCheck_Country
where wcc.Status = 1
and sdn.Status != 4
and sdn.ListType = 'WPEP'
)

--再把不要的移除
select sdn.EntNum from SDNTable(nolock) sdn
join WorldCheckCountry wcc on sdn.Country = wcc.WorldCheck_Country
where wcc.Status = 0
and sdn.Status != 4
and sdn.ListType = '0138WPEP'

update SDNTable set ListType = 'WPEP' where EntNum in (
select sdn.EntNum from SDNTable(nolock) sdn
join WorldCheckCountry wcc on sdn.Country = wcc.WorldCheck_Country
where wcc.Status = 0
and sdn.Status != 4
and sdn.ListType = '0138WPEP'
)

--12/28 更新 控制type來決定要不要轉入SDN
select * from WorldCheckCountry

begin tran
ALTER TABLE WorldCheckCountry ADD [Type] char(100);
commit

update WorldCheckCountry set type = 'other' where WorldCheck_Country = 'BRAZIL'

--建index
USE [OFAC]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [UID_Word_WCKeywords_IDX]    Script Date: 1/31/2019 11:44:35 AM ******/
CREATE NONCLUSTERED INDEX [UID_Word_WCKeywords_IDX] ON [dbo].[WCKeywords]
(
	[UID] ASC,
	[Word] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


