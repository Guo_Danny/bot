USE [OFAC]
GO

/****** Object:  StoredProcedure [dbo].[OFS_AddFilterMsg]    Script Date: 3/5/2019 10:07:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER PROCEDURE [dbo].[OFS_AddFilterMsg] @seqNumb int, @msgType GenericIdType, @msgLen int,
            @msg text
  As
  declare @stat int

  -- Not using begin / commit tran since this routine is used with other
  -- table updates in ofacsrvr.cpp
  insert into MsgTable
      (SeqNumb, MsgType, MsgLen, Msg)
    values
      ( @seqNumb, @msgType, @msgLen, @msg )

  --20190212 fix v10.1.3 patch8 user msg reference NULL issue.
  
  declare @UserMessageReference varchar(200),@CaseReference varchar(200),@pos_s int, @pos_e int, @str_tag20 varchar(200)

  select @UserMessageReference = UserMessageReference, @CaseReference = Ref from FilterTranTable where SeqNumb = @seqNumb

  if @UserMessageReference is null and CHARINDEX(':20:',@msg) > 0
	select @pos_e = CHARINDEX(char(13),@msg,CHARINDEX(':20:',@msg)),
		   @pos_s = CHARINDEX(':20:',@msg)
	select @str_tag20 = SUBSTRING(@msg,CHARINDEX(':20:',@msg)+4,@pos_e-@pos_s-4)
	
	if CHARINDEX('FS',@CaseReference) > 0 or CHARINDEX('FR',@CaseReference) > 0
		update FilterTranTable set UserMessageReference = @CaseReference where SeqNumb = @seqNumb;
	else
		update FilterTranTable set UserMessageReference = @str_tag20 where SeqNumb = @seqNumb;
	
  --20190212 modify end line.

  select @stat = @@ERROR
  return @stat

GO


