USE [OFAC]
GO

/****** Object:  StoredProcedure [dbo].[OFS_SelReconTableByDate]    Script Date: 5/9/2019 4:07:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE Proc [dbo].[OFS_SelReconTableByDate] (@searchStartDate DateTime,
                   @searchEndDate DateTime)
 
As

  Declare @trnCnt int
  Declare @stat   int
  Declare @sql    nvarchar(4000)

  Select @trnCnt = @@trancount  -- Save the current trancount

  If @trnCnt = 0
    -- Transaction has not begun
    Begin tran OFS_SelReconTableByDate
  Else
    -- Already in a transaction
    Save tran OFS_SelReconTableByDate
    -- End standard stored procedure transaction header

    Select Source,replace(CaseReference,'-1.pri','') as CaseReference,SearchDate,'1' 'Total Msgs',  
	case when SancPartyCount > 0 then 1 else 0 end 'Bad', 
	case when SancPartyCount > 0 then 0 else 1 end 'Good', 
	replace(UserMessageReference,'-1.pri','') 'ref'
	from ReconTable where SearchDate Between @searchStartDate And  @searchEndDate
	and Source = 'SWIFT'
  Select @stat = @@ERROR

  -- Evaluate results of the transaction
  If (@stat = 0)
      If @trnCnt = 0
        Commit tran OFS_SelReconTableByDate
  Else
    Rollback tran OFS_SelReconTableByDate

  return @stat

GO


