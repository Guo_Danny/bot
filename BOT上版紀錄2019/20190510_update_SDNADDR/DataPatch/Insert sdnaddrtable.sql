
use OFAC
truncate table TmpAddrTable;

--procedure 1
begin tran
declare @ListCreateDate datetime, @ListModifDate datetime,@CreateDate datetime, @LastModifDate datetime, 
@entnum varchar(15),@country varchar(30), @maxaddrnum varchar(15), @location varchar(5000), @country_new varchar(30),
@addr varchar(200), @City varchar(200), @State varchar(200), @location_tmp varchar(5000), @location_1 varchar(1000),@location_2 varchar(3000) ,@location_3 varchar(3000)
set @ListCreateDate = NULL;
set @ListModifDate = NULL;
set @CreateDate = NULL;
set @LastModifDate = NULL;
select @maxaddrnum = max(AddrNum)+1 from SDNAddrTable(nolock)

declare cur CURSOR FOR
select sdn.entnum, wcc.Customer_Country, wc.location, sdn.ListCreatedate,sdn.ListModifDate, sdn.CreateDate, sdn.LastModifDate
from SDNTable(nolock) sdn
join worldcheckcountry (nolock) wcc on sdn.Country = wcc.WorldCheck_Country
join worldcheck (nolock) wc on sdn.entnum = wc.entnum
where sdn.EntNum > 20000000 and sdn.Status != 4 
and sdn.ListType != 'WPEP'
--and (sdn.ListType like '0%WPEP' or sdn.ListType = 'WORLD-CHECK')
--and sdn.EntNum < 20001000
open cur;
fetch next from cur into @entnum,@country,@location,@ListCreateDate,@ListModifDate,@CreateDate,@LastModifDate;
WHILE @@fetch_status = 0 
begin
	set @location_tmp = @location;
	
	WHILE CHARINDEX(';',@location_tmp) > 0
		begin
			-- part1
			set @location_1 = SUBSTRING(@location_tmp, 1, CHARINDEX(';', @location_tmp) - 1);
			set @location_tmp = SUBSTRING(@location_tmp, CHARINDEX(';', @location_tmp) + 1, LEN(@location_tmp) - CHARINDEX(';', @location_tmp));
			
			-- part2 for addr
			set @addr = SUBSTRING(@location_1, 1, CHARINDEX('~', @location_1) - 1);
			set @location_2 = SUBSTRING(@location_1, CHARINDEX('~', @location_1) + 1, LEN(@location_1) - CHARINDEX('~', @location_1));

			set @City = SUBSTRING(@location_2, 1, CHARINDEX(',', @location_2) - 1);
			
			--part for state
			set @location_3 = SUBSTRING(@location_2, CHARINDEX(',', @location_2) + 1, LEN(@location_2) - CHARINDEX(',', @location_2));
			set @State = SUBSTRING(@location_3, 1, CHARINDEX('~', @location_3) - 1);
			set @country_new = SUBSTRING(@location_3, CHARINDEX('~', @location_3) + 1, LEN(@location_3) - CHARINDEX('~', @location_3));
			--update worldcheck country as customer country
			select @country_new = Customer_Country from WorldCheckCountry where WorldCheck_Country = LTRIM(RTRIM(@country_new));
			
			--fit for len
			set @addr = LTRIM(RTRIM(@addr));
			if len(@addr) > 100
			begin
				set @addr = SUBSTRING(@addr,0,100);
			end
			set @City = LTRIM(RTRIM(@City));
			if len(@City) > 50
			begin
				set @City = SUBSTRING(@City,0,50);
			end
			set @State = LTRIM(RTRIM(@State));
			if len(@State) > 50
			begin
				set @State = SUBSTRING(@State,0,50);
			end
			set @country_new = LTRIM(RTRIM(@country_new));

			--insert to table
			if (@location_1 != '~,~')
			begin 
			insert into TmpAddrTable(EntNum,AddrNum,Address,City,State,Country,Deleted,Status,ListCreatedate,ListModifDate,CreateDate,LastModifDate,LastOper)
			values (@entnum, @maxaddrnum,@addr,@City,@State, @country_new, '0', 1,@ListCreateDate, @ListModifDate,@CreateDate,@LastModifDate, 'PRIMEADMIN')
			set @maxaddrnum = @maxaddrnum + 1;
			end
		end
	if @location_tmp = '~,~'
		begin
			set @addr = '';
			set @City = '';
			set @State = '';
		end
	--print @location_tmp;
	if len(@location_tmp) > 4
		begin
			set @addr = SUBSTRING(@location_tmp, 1, CHARINDEX('~', @location_tmp) - 1);
			set @location_2 = SUBSTRING(@location_tmp, CHARINDEX('~', @location_tmp) + 1, LEN(@location_tmp) - CHARINDEX('~', @location_tmp));
			set @City = SUBSTRING(@location_2, 1, CHARINDEX(',', @location_2) - 1);
			--print @City;
			--part for state
			set @location_3 = SUBSTRING(@location_2, CHARINDEX(',', @location_2) + 1, LEN(@location_2) - CHARINDEX(',', @location_2));
			set @State = SUBSTRING(@location_3, 1, CHARINDEX('~', @location_3) - 1);
			set @country_new = SUBSTRING(@location_3, CHARINDEX('~', @location_3) + 1, LEN(@location_3) - CHARINDEX('~', @location_3));
			--update worldcheck country as customer country
			select @country_new = Customer_Country from WorldCheckCountry where WorldCheck_Country = LTRIM(RTRIM(@country_new));

			--fit for len
			set @addr = LTRIM(RTRIM(@addr));
			if len(@addr) > 100
			begin
				set @addr = SUBSTRING(@addr,0,100);
			end
			set @City = LTRIM(RTRIM(@City));
			if len(@City) > 50
			begin
				set @City = SUBSTRING(@City,0,50);
			end
			set @State = LTRIM(RTRIM(@State));
			if len(@State) > 50
			begin
				set @State = SUBSTRING(@State,0,50);
			end
			set @country_new = LTRIM(RTRIM(@country_new));

			--insert to table
			insert into TmpAddrTable(EntNum,AddrNum,Address,City,State,Country,Deleted,Status,ListCreatedate,ListModifDate,CreateDate,LastModifDate,LastOper)
			values (@entnum, @maxaddrnum,@addr,@City,@State, @country_new, '0', 1,@ListCreateDate, @ListModifDate,@CreateDate,@LastModifDate, 'PRIMEADMIN')
			set @maxaddrnum = @maxaddrnum + 1;
		end
  fetch next from cur into @entnum,@country,@location,@ListCreateDate,@ListModifDate,@CreateDate,@LastModifDate;
end
close cur;
DEALLOCATE cur;

--commit
--rollback

--procedure 2
--begin tran

--insert into SDNAddrTable(EntNum,AddrNum,Address,City,State,Country,Deleted,Status,ListCreateDate,ListModifDate,CreateDate,LastModifDate,LastOper)
--select EntNum,AddrNum,Address,City,State,Country,0,Status,ListCreatedate,ListModifDate,CreateDate,LastModifDate,LastOper from TmpAddrTable

--commit
--rollback

--select * from SDNAddrTable where EntNum > 20000000

--select count(*) from OFAC..TmpAddrTable

--select * from OFAC..TmpAddrTable

