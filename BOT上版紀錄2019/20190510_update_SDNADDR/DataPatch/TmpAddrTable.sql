USE [OFAC]
GO

/****** Object:  Table [dbo].[TmpAddrTable]    Script Date: 6/24/2019 5:42:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TmpAddrTable](
	[EntNum] [int] NULL,
	[AddrNum] [int] NULL,
	[Address] [varchar](100) NULL,
	[City] [varchar](50) NULL,
	[Country] [varchar](250) NULL,
	[Remarks] [varchar](200) NULL,
	[Deleted] [bit] NOT NULL,
	[ListID] [varchar](15) NULL,
	[FreeFormat] [dbo].[SDNAddress] NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[PostalCode] [varchar](16) NULL,
	[Status] [int] NOT NULL,
	[ListCreatedate] [datetime] NULL,
	[ListModifDate] [datetime] NULL,
	[CreateDate] [datetime] NULL,
	[LastModifDate] [datetime] NULL,
	[LastOper] [dbo].[Code] NOT NULL
) ON [PRIMARY]
GO

