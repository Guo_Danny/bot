<?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <configSections>
    <section name="infragistics.web" type="System.Configuration.SingleTagSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <sectionGroup name="system.web.extensions" type="System.Web.Configuration.SystemWebExtensionsSectionGroup, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
      <sectionGroup name="scripting" type="System.Web.Configuration.ScriptingSectionGroup, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
        <section name="scriptResourceHandler" type="System.Web.Configuration.ScriptingScriptResourceHandlerSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication" />
        <sectionGroup name="webServices" type="System.Web.Configuration.ScriptingWebServicesSectionGroup, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
          <section name="jsonSerialization" type="System.Web.Configuration.ScriptingJsonSerializationSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="Everywhere" />
          <section name="profileService" type="System.Web.Configuration.ScriptingProfileServiceSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication" />
          <section name="authenticationService" type="System.Web.Configuration.ScriptingAuthenticationServiceSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication" />
          <section name="roleService" type="System.Web.Configuration.ScriptingRoleServiceSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication" />
        </sectionGroup>
      </sectionGroup>
    </sectionGroup>
    <!-- Define the custom configuration sections in the <configSections> element... -->
    <section name="tenantGroup" type="CMBrowser.tenantsSection" allowDefinition="Everywhere" allowExeDefinition="MachineToApplication" restartOnExternalChanges="true" />
  </configSections>
  <!-- application specific settings -->
  <appSettings>
    <add key="DefaultSupportEmailAddressKey" value="Email:" />
    <add key="DefaultSupportEmailAddress" value="fcs.support@fisglobal.com" />
    <add key="DefaultSupportTelephoneNumberKey" value="Tel:" />
    <add key="DefaultSupportTelephoneNumber" value="1.866.275.6868 (Option 2-5)" />
    <add key="CrystalImageCleaner-AutoStart" value="true" />
    <add key="CrystalImageCleaner-Sleep" value="60000" />
    <add key="CrystalImageCleaner-Age" value="120000" />
    <add key="aspnet:MaxHttpCollectionKeys" value="12000" />
    <!-- HideErrorDetails
     When set to 'true', PCS error details will only be available in the event viewer log.  When
     set to 'false', the error details will be available on the PCS error page as well as the event viewer log. -->
    <add key="HideErrorDetails" value="false" />
    <!-- EnableSessionTimeoutHandling
     When set to 'true', open PCS windows will either close or redirect to the login page automatically
     when a session expires due to inactivity.  When set to 'false', open PCS windows will remain open
     when the session expires and will redirect to the login page when the user tries to perform any
     action in the window. -->
    <add key="EnableSessionTimeoutHandling" value="true" />
    <!-- ChildWindowSessionTimeoutAction
     When set to 'close' and EnableSessionTimeoutHandling is set to 'true', open PCS windows will close automatically
     when a session expires due to inactivity.  When set to 'redirect' and EnableSessionTimeoutHandling is set to 'true',
     open PCS windows will redirect to the login page automatically when a session expires due to inactivity. -->
    <add key="ChildWindowSessionTimeoutAction" value="close" />
    <!--EncryptConsoleConfiguration
      It is used to set the location of the EncryptConsoleConfig.xml file and is needed by the EncryptConsoleBusinessLogic application to access voltage API.
      Also, the drive in the value field will vary based on where the prime is installed-->
    <add key="EncryptConsoleConfiguration" value="C:\Prime\EncryptConsoleBusinessLogic\EncryptConsoleConfig.xml" />
    <!-- CrystalReportsProvider  
         When set to 'SQLNCLI11', Crystal reports connects with SQL Server Using this   
         DB provider Microsoft SQL Server Native Client 11.0-->
    <add key="CrystalReportsProvider" value="SQLNCLI11" />
    <!--LastLoginConfiguration
    Used to display Last Login in PCS and value contains the comma separated list of Organisation for those Last login will be displayed.
    <add key="LastLoginConfiguration" value="OrgCode1,OrgCode2, OrgCode3"/>
    If value is set to '*', Last Login configuration will be enabled for all tenants on the server-->
    <add key="LastLoginConfiguration" value="*" />
    <!--ServerNameConfiguration
    Used to display Server Name on PCS Login page, [SERVERNAME] is the place holder and will be replaced by actual server name in UI
    <add key="ServerNameConfiguration" value="Production- [SERVERNAME]"/>
    <add key="ServerNameConfiguration" value="Ready123"/>-->
    <add key="ServerNameConfiguration" value="[SERVERNAME]" />
    <!--PDFHtmlDisplayProperty
      Used to set the "target = '_self'" in pdf tag to resolve SAR/CTR issue with updated version of Adobe Acrobat Reader DC 19.010.20064 -->
    <add key="PDFHtmlDisplayProperty" value="target = '_self'" />
  </appSettings>
  <tenantGroup lockAllElementsExcept="tenants">
    <tenants>
      <!-- notesRefreshInterval should be in minutes-->
      <tenant name="prime" notesRefreshInterval="1" />
      <tenant name="idola" notesRefreshInterval="2" />
    </tenants>
  </tenantGroup>
  <system.web>
    <!--  DYNAMIC DEBUG COMPILATION
          Set compilation debug="true" to insert debugging symbols (.pdb information)
          into the compiled page. Because this creates a larger file that executes
          more slowly, you should set this value to true only when debugging and to
          false at all other times. For more information, refer to the documentation about
          debugging ASP.NET files.
    -->
    <compilation defaultLanguage="vb" debug="false">
      <assemblies>
        <add assembly="CrystalDecisions.CrystalReports.Engine, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" />
        <add assembly="CrystalDecisions.ReportSource, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" />
        <add assembly="CrystalDecisions.Shared, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" />
        <add assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" />
        <!-- Crystal Reports 2013 Changes
                                The following 3 CrystalDecisions assembly definitions have been commented and are now excluded.
                                If there are any issues related to the following components while launching 
                                Prime or running Crystal Reports, uncomment the appropriate lines and ensure the version is updated with the 
                                appropriate DLL version on your server. -->
        <!--<add assembly="CrystalDecisions.ReportAppServer.ClientDoc, Version=12.0.1100.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"/>-->
        <!--<add assembly="CrystalDecisionss.Enterprise.Framework, Version=12.0.1100.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"/>-->
        <!--<add assembly="CrystalDecisions.Enterprise.InfoStore, Version=12.0.1100.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"/>-->
        <add assembly="System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089" />
        <add assembly="System.DirectoryServices, Version=2.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A" />
        <add assembly="System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A" />
        <add assembly="Infragistics2.WebUI.UltraWebTab.v8.3, Version=8.3.20083.2039, Culture=neutral, PublicKeyToken=7DD5C3163F2CD0CB" />
        <add assembly="Infragistics2.WebUI.UltraWebChart.v8.3, Version=8.3.20083.2039, Culture=neutral, PublicKeyToken=7DD5C3163F2CD0CB" />
        <add assembly="Infragistics2.WebUI.Shared.v8.3, Version=8.3.20083.2039, Culture=neutral, PublicKeyToken=7DD5C3163F2CD0CB" />
        <add assembly="Infragistics2.WebUI.Misc.v8.3, Version=8.3.20083.2039, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" />
        <add assembly="Infragistics2.WebUI.UltraWebNavigator.v8.3, Version=8.3.20083.2039, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" />
        <add assembly="Infragistics2.WebUI.WebHtmlEditor.v8.3, Version=8.3.20083.2039, Culture=neutral, PublicKeyToken=7DD5C3163F2CD0CB" />
        <add assembly="Infragistics2.WebUI.UltraWebGrid.v8.3, Version=8.3.20083.2039, Culture=neutral, PublicKeyToken=7DD5C3163F2CD0CB" />
        <add assembly="Infragistics2.WebUI.WebSpellChecker.v8.3, Version=8.3.20083.2039, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" />
        <add assembly="AntiXssLibrary, Version=4.3.0.0, Culture=neutral, PublicKeyToken=d127efab8a9c114f, processorArchitecture=MSIL" />
        <add assembly="HtmlSanitizationLibrary, Version=4.3.0.0, Culture=neutral, PublicKeyToken=d127efab8a9c114f, processorArchitecture=MSIL" />
        <add assembly="System.Core, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089" />
        <add assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Xml.Linq, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089" />
        <add assembly="System.Data.DataSetExtensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089" />
      </assemblies>
    </compilation>
    <pages validateRequest="false">
      <tagMapping>
        <add tagType="System.Web.UI.WebControls.WebParts.WebPartManager" mappedTagType="Metavante.Prime.Common.Web.UI.WebControls.WebParts.WebPartManager,Metavante.Prime.Common.Web.UI" />
      </tagMapping>
      <controls>
        <add tagPrefix="asp" namespace="System.Web.UI" assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add tagPrefix="asp" namespace="System.Web.UI.WebControls" assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
      </controls>
    </pages>
    <!--  ASP.NET Header Disclosure
          To remove X-AspNet-Version disclosure in the header, in the web.config find/create <system.web> and add:
          <system.web>
           <httpRuntime enableVersionHeader="false" />

          To remove X-Powered-By, in the web.config find/create <system.webServer> and add:
          <system.webServer>
           <httpProtocol>
             <customHeaders>
               <remove name="X-Powered-By" />
             </customHeaders>
           </httpProtocol>
    -->
    <!--  CUSTOM ERROR MESSAGES
          Set customErrors mode="On" or "RemoteOnly" to enable custom error messages, "Off" to disable. 
          Add <error> tags for each of the errors you want to handle.

          "On" Always display custom (friendly) messages.
          "Off" Always display detailed ASP.NET error information.
          "RemoteOnly" Display custom (friendly) messages only to users not running 
           on the local Web server. This setting is recommended for security purposes, so 
           that you do not display application detail information to remote clients.
    -->
    <customErrors mode="RemoteOnly" defaultRedirect="CMError.aspx"/>
    <!--  AUTHENTICATION 
          This section sets the authentication policies of the application. Possible modes are "Windows", 
          "Forms", "Passport" and "None"

          "None" No authentication is performed. 
          "Windows" IIS performs authentication (Basic, Digest, or Integrated Windows) according to 
           its settings for the application. Anonymous access must be disabled in IIS. 
          "Forms" You provide a custom form (Web page) for users to enter their credentials, and then 
           you authenticate them in your application. A user credential token is stored in a cookie.
          "Passport" Authentication is performed via a centralized authentication service provided
           by Microsoft that offers a single logon and core profile services for member sites.
    -->
    <authentication mode="Windows" />
    <!--  AUTHORIZATION 
          This section sets the authorization policies of the application. You can allow or deny access
          to application resources by user or role. Wildcards: "*" mean everyone, "?" means anonymous 
          (unauthenticated) users.
    -->
    <authorization>
      <allow users="*" />
      <!-- Allow all users -->
      <!--  <allow     users="[comma separated list of users]"
                             roles="[comma separated list of roles]"/>
                  <deny      users="[comma separated list of users]"
                             roles="[comma separated list of roles]"/>
            -->
    </authorization>
    <!--  APPLICATION-LEVEL TRACE LOGGING
          Application-level tracing enables trace log output for every page within an application. 
          Set trace enabled="true" to enable application trace logging.  If pageOutput="true", the
          trace information will be displayed at the bottom of each page.  Otherwise, you can view the 
          application trace log by browsing the "trace.axd" page from your web application
          root. 
    -->
    <trace enabled="false" requestLimit="10" pageOutput="false" traceMode="SortByTime" localOnly="false" />
    <!--  SESSION STATE SETTINGS
          By default ASP.NET uses cookies to identify which requests belong to a particular session. 
          If cookies are not available, a session can be tracked by adding a session identifier to the URL. 
          To disable cookies, set sessionState cookieless="true".
    -->
    <sessionState mode="InProc" stateConnectionString="tcpip=127.0.0.1:42424" sqlConnectionString="data source=127.0.0.1;Trusted_Connection=yes" cookieless="false" timeout="120" />
    <httpHandlers>
      <remove verb="*" path="*.asmx" />
      <add verb="*" path="*.asmx" validate="false" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
      <add verb="*" path="*_AppService.axd" validate="false" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
      <add verb="GET,HEAD" path="ScriptResource.axd" type="System.Web.Handlers.ScriptResourceHandler, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" validate="false" />
      <add verb="GET" path="CrystalImageHandler.aspx" type="CrystalDecisions.Web.CrystalImageHandler, CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" />
    </httpHandlers>
    <webParts>
      <personalization defaultProvider="PrimeSqlPersonalizationProvider">
        <providers>
          <clear />
          <add name="PrimeSqlPersonalizationProvider" type=" Metavante.Prime.Common.Web.UI.WebControls.WebParts.SQLPersonalizationProvider" applicationName="CMBrowser" />
        </providers>
        <authorization>
          <deny users="*" verbs="enterSharedScope" />
          <allow users="*" verbs="modifyState" />
        </authorization>
      </personalization>
    </webParts>
    <membership defaultProvider="PrimeActiveDirectoryMembershipProvider">
      <providers>
        <clear />
        <add name="PrimeActiveDirectoryMembershipProvider" type="Metavante.Prime.Common.ActiveDirectoryMembershipProvider,Metavante.Prime.Common" applicationName="ADSyncUtil" description="Prime Active Directory Membership Provider" connectionProtection="Secure" searchScope="SUBTREE" enableSearchMethods="True" attributeMapUsername="sAMAccountName" attributeMapEmail="mail" attributeMapOperCode="opercode" attributeMapName="displayname" attributeMapBranch="branch" attributeMapDepartment="department" groupMapCDDUserOnly="" groupMapMILogUserOnly="" />
      </providers>
    </membership>
    <httpModules>
      <add name="ScriptModule" type="System.Web.Handlers.ScriptModule, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
    </httpModules>
    <!--  GLOBALIZATION
          This section sets the globalization settings of the application. 
    -->
    <globalization requestEncoding="utf-8" responseEncoding="utf-8" culture="en-US" uiCulture="en-US" />
    <identity impersonate="false" />
    <!-- The requireSSl property of the httpCookies element below should be set to true when Prime is configured to use https protocol. 
         This prevents cookies from being transmitted over non-encrypted connections.
    -->
    <httpCookies requireSSL="false" />
  </system.web>
  <system.codedom>
    <compilers>
      <compiler language="vb;vbs;visualbasic;vbscript" extension=".vb" compilerOptions="/platform:x86" type="Microsoft.VisualBasic.VBCodeProvider, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" warningLevel="4">
          <providerOption name="CompilerVersion" value="v3.5" />
          <providerOption name="OptionInfer" value="true" />
          <providerOption name="WarnAsError" value="false" />
      </compiler>
    </compilers>
  </system.codedom>
  <!-- To help prevent Clickjacking Attacks the following is recommended:
         Configure IIS to send the X-Frame-Options header by adding the customHeaders setting below to your site's Web.config file:
         <system.webServer>
           <httpProtocol>
             <customHeaders>
               <add name="X-Frame-Options" value="SAMEORIGIN" />
             </customHeaders>
           </httpProtocol>        
         </system.webServer>
    -->
  <system.webServer>
    <handlers>
      <remove name="WebServiceHandlerFactory-Integrated" />
      <remove name="ScriptHandlerFactory" />
      <remove name="ScriptHandlerFactoryAppServices" />
      <remove name="ScriptResource" />
      <add name="CrystalImageHandler.aspx_GET" verb="GET" path="CrystalImageHandler.aspx" type="CrystalDecisions.Web.CrystalImageHandler, CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" preCondition="integratedMode" />
      <add name="ScriptHandlerFactory" verb="*" path="*.asmx" preCondition="integratedMode" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
      <add name="ScriptHandlerFactoryAppServices" verb="*" path="*_AppService.axd" preCondition="integratedMode" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
      <add name="ScriptResource" verb="GET,HEAD" path="ScriptResource.axd" preCondition="integratedMode" type="System.Web.Handlers.ScriptResourceHandler, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
    </handlers>
    <validation validateIntegratedModeConfiguration="false" />
    <modules>
      <remove name="ScriptModule" />
      <add name="ScriptModule" preCondition="managedHandler" type="System.Web.Handlers.ScriptModule, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
    </modules>
    <httpErrors errorMode="DetailedLocalOnly" existingResponse="Replace" defaultResponseMode="ExecuteURL">
        <remove statusCode="404" subStatusCode="-1" />
        <remove statusCode="500" subStatusCode="-1" />
        <error statusCode="404" path="Compliance Suite Error.html" responseMode="File" />
        <error statusCode="500" path="Compliance Suite Error.html" responseMode="File" />
    </httpErrors>
  </system.webServer>
  <runtime>
    <assemblyBinding appliesTo="v2.0.50727" xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Extensions" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-1.1.0.0" newVersion="3.5.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Extensions.Design" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-1.1.0.0" newVersion="3.5.0.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
</configuration>