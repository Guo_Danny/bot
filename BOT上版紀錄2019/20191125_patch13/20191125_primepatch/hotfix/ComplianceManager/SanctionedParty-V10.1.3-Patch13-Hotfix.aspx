<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SanctionedParty.aspx.vb" Inherits="CMBrowser.SanctionedParty" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Sanctioned Party</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="Styles.css" type="text/css" rel="stylesheet"/>
		<style type="text/css">.tgridheader {
	BORDER-RIGHT: white outset; PADDING-RIGHT: 2px; BORDER-TOP: white outset; PADDING-LEFT: 2px; FONT-WEIGHT: bold; FONT-SIZE: x-small; PADDING-BOTTOM: 2px; BORDER-LEFT: white outset; COLOR: white; PADDING-TOP: 2px; BORDER-BOTTOM: white outset; BACKGROUND-COLOR: #990000; TEXT-ALIGN: left
}
.lblgridcell {
	FONT-SIZE: 12px; TEXT-ALIGN: left; Font-Names: Verdana, Arial, Helvetica, sans-serif
}
.BoldRed {
	FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: red
}
.BoldBlack {
	FONT-WEIGHT: bold; FONT-SIZE: small; COLOR: black
}
.NormalRed {
	FONT-WEIGHT: normal; FONT-SIZE: x-small; COLOR: red
}
.NormalBlack {
	FONT-WEIGHT: normal; FONT-SIZE: 11px; VERTICAL-ALIGN: middle; COLOR: black; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.NormalLink {
	FONT-WEIGHT: normal; FONT-SIZE: 11px
}
.BoldBlue {
	FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: blue
}
.GridEditableRegularCol {
	FONT-SIZE: 11px
}
.GridEditableRegularCol INPUT {
	WIDTH: 100%
}
		</style>
		<script language="JavaScript" src="js/nav.js"></script>
		<script type="text/jscript" language="JavaScript">
		
		<!--
		function hidectr()
		{
			try
			{	
				//Hide			
				document.getElementById("tblSanctionedParties").style.display	 = "none";
			}
			catch(ex){}
		}
		function showctr()
		{
			try
			{
				//Show
				document.getElementById("tblSanctionedParties").style.display	 = "";
			}
			catch(ex){}
		}
		
		//-->
		</script>

        <link href="Styles.css" rel="stylesheet" type="text/css" />
        <link href="Styles.css" rel="stylesheet" type="text/css" />
        <link href="Styles.css" rel="stylesheet" type="text/css" />
	</head>
	<body onload="hidectr();AddWindowToPCSWindowCookie(window.name);" onafterprint="hidectr();" onbeforeprint="showctr();" class="formbody" onUnload="RemoveWindowFromPCSWindowCookie(window.name);">
		<form id="Form1" method="post" runat="server">
			<table id="PageLayout" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr>
								<td width="600"><asp:imagebutton id="imgPrime" runat="server" width="808px" ImageUrl="images/compliance-manager.gif"
										AlternateText="Home"></asp:imagebutton></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordMessage" runat="server" width="808px" cssClass="RecordMessage">Record Message</asp:label></td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" Height="24px" Font-Size="X-Small" Width="616px"></asp:validationsummary></td>
				</tr>
				<tr>
					<td>
						<table id="Table2" style="WIDTH: 816px; HEIGHT: 40px" cellspacing="0" cellpadding="0" width="816"
							border="0">
							<tr align="center">
								<td align="right"><asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder></td>
								<td align="left"><uc1:ucrecordscrollbar id="RecordScrollBar" runat="server"></uc1:ucrecordscrollbar></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><uc1:uctabcontrol id="MainTabControl" runat="server"></uc1:uctabcontrol></td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordHeader" runat="server" width="804px" CSSClass="paneltitle"></asp:label></td>
				</tr>
			</table>
            <table id="tblSanctionedParties" runat="server" border="0" cellpadding="0" cellspacing="0"
                width="250">
                <tr>
                    <td>
                        <asp:Label ID="Label31" runat="server" CssClass="LABEL" Width="96px">Date:</asp:Label>
                    </td>
                    <td style="width: 161px">
                        <asp:TextBox ID="txtDate" runat="server" CssClass="TEXTBOX" Height="24px" ReadOnly="True"
                            Text="" Width="160px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label33" runat="server" CssClass="LABEL" Width="96px">Processed By:</asp:Label>
                    </td>
                    <td style="width: 161px">
                        <asp:TextBox ID="txtProcessedBy" runat="server" CssClass="TEXTBOX" Height="24px"
                            ReadOnly="True" Text="" Width="160px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label34" runat="server" CssClass="LABEL" Width="96px">Verified By:</asp:Label>
                    </td>
                    <td style="width: 161px">
                        <asp:TextBox ID="txtVerifiedBy" runat="server" CssClass="TEXTBOX" Height="24px" Text=""
                            Width="160px"></asp:TextBox>
                    </td>
                </tr>
            </table>
			<br/>
			<asp:panel id="Panel1" runat="server" Height="176px" Width="744px" BorderStyle="None" BorderWidth="1px"
				Cssclass="Panel">
				<table id="Table1" cellspacing="0" cellpadding="0" width="300" border="0">
					<tr>
						<td colspan="6">
							<table cellspacing="0" cellpadding="0" width="300" border="0">
								<tr>
									<td>
										<asp:label id="Label2" runat="server" CssClass="LABEL">Party ID:</asp:label></td>
									<td>
										<asp:textbox id="txtCode" runat="server" Width="80px" CssClass="TEXTBOX"></asp:textbox></td>
									<td style="WIDTH: 105px">
										<asp:label id="lblSource" runat="server" Width="96px" CssClass="LABEL">Source:</asp:label></td>
									<td>
										<asp:textbox id="txtSource" runat="server" Width="185px" CssClass="TEXTBOX"></asp:textbox></td>
									<td>
										<asp:Label id="lblStatus" runat="server" Width="92px" CssClass="LABEL">List Status:</asp:Label></td>
									<td>
										<asp:textbox id="txtStatus" runat="server" Width="112px" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
							</table>
						</td>
						</tr>
					<tr>
						<td style="HEIGHT: 25px">
							<asp:label id="Label5" runat="server" CssClass="LABEL">Name:</asp:label></td>
						<td style="HEIGHT: 25px" colspan="3">
							<asp:textbox id="txtName" runat="server" Width="565px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label3" runat="server" CssClass="LABEL">Title:</asp:label></td>
						<td colspan="3">
							<asp:textbox id="txtTitle" runat="server" Height="48px" Width="565px" CssClass="TEXTBOX" TextMode="MultiLine"></asp:textbox></td>
					</tr>
					<tr>
						<td style="HEIGHT: 21px">
							<asp:label id="Label15" runat="server" CssClass="LABEL">Program:</asp:label></td>
						<td style="HEIGHT: 21px">
							<asp:textbox id="txtProgram" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="HEIGHT: 21px">
							<asp:label id="Label17" runat="server" CssClass="LABEL">Party Type:</asp:label></td>
						<td style="HEIGHT: 21px">
							<asp:DropDownList id="ddlPartyType" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td style="height: 48px">
							<asp:label id="Label4" runat="server" CssClass="LABEL">List Type:</asp:label></td>
						<td style="height: 48px">
                            <asp:DropDownList id="ddlListType" runat="server" CssClass="DropDownList">
                            </asp:DropDownList>
							<asp:textbox id="txtList" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="height: 48px">
							<asp:label id="Label18" runat="server" CssClass="LABEL">Entity Type:</asp:label></td>
						<td style="height: 48px">
							<asp:DropDownList id="ddlentityType" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<asp:CheckBox id="chkSkipDervNames" runat="server" CssClass="CheckBox" Text=""></asp:CheckBox>
							<asp:Label id="Label1" runat="server" CssClass="LABEL">Skip Name Derivation</asp:Label></td>
						<td>
							<asp:label id="lblDeleted" runat="server" CssClass="LABEL">Deleted by User:</asp:label></td>
						<td>
							<asp:textbox id="txtDeleted" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td valign="top"></td>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td valign="top"></td>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td valign="top" align="right"></td>
						<td colspan="3"></td>
					</tr>
				</table>
				<table id="Table3" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td></td>
					</tr>
					<tr>
						<td>
							<asp:Panel id="pnlPredefList" runat="server">
								<table id="Table5" cellspacing="0" cellpadding="1" width="100%" border="0">
									<tr>
										<td>
											<asp:label id="lblAddresses" runat="server" CssClass="LABEL">Addresses:</asp:label></td>
										<td>
											<asp:DataGrid id="dgAddresses" runat="server" Width="568px" BorderStyle="Solid" BorderWidth="1px"
												AutoGenerateColumns="False" ShowHeader="True" ForeColor="Black" GridLines="Vertical" CellPadding="0"
												BorderColor="#999999" BackColor="White">
												<FooterStyle BackColor="#CCCCCC"></FooterStyle>
												<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
												<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
												<HeaderStyle CssClass="gridheader"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="1px" Font-Size="11px"></HeaderStyle>
														<HeaderTemplate>
															Del
														</HeaderTemplate>
														<ItemTemplate>
															<asp:CheckBox id="chkDeleteAddress" Text="" TextAlign="Left" Width="1px" runat="server" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container.DataItem, "Deleted").ToString()) %>'>
															</asp:CheckBox>
															<input id="hdfldAddrNum"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.AddrNum").ToString())%>' name="hdfldAddrNum"/>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="Address" HeaderText="Address" HeaderStyle-HorizontalAlign="Left">
														<ItemStyle Font-Size="11px"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="City" HeaderText="City" HeaderStyle-HorizontalAlign="Left">
														<ItemStyle Font-Size="11px"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="State" HeaderText="State" HeaderStyle-HorizontalAlign="Left">
														<ItemStyle Font-Size="11px"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Country" HeaderText="Country" HeaderStyle-HorizontalAlign="Left">
														<ItemStyle Font-Size="11px"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Postal Code" HeaderText="Postal Code" HeaderStyle-HorizontalAlign="Left">
														<ItemStyle Font-Size="11px"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Status" HeaderText="List Status" HeaderStyle-HorizontalAlign="Left">
														<ItemStyle Font-Size="11px"></ItemStyle>
													</asp:BoundColumn>
												</Columns>
												<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
											</asp:DataGrid></td>
									</tr>
									<tr>
										<td>
											<asp:label id="lblAKAs" runat="server" CssClass="LABEL">Alternate Names:</asp:label></td>
										<td>
											<asp:DataGrid id="dgAlternateNames" runat="server" Width="568px" BorderStyle="Solid" BorderWidth="1px"
												AutoGenerateColumns="False" ForeColor="Black" GridLines="Vertical" CellPadding="0" BorderColor="#999999"
												BackColor="White">
												<FooterStyle BackColor="#CCCCCC"></FooterStyle>
												<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
												<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
												<HeaderStyle CssClass="gridheader"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="1px" Font-Size="11px"></HeaderStyle>
														<HeaderTemplate>
															Del
														</HeaderTemplate>
														<ItemTemplate>
															<asp:CheckBox id="chkDeleteAka" Text="" TextAlign="Left" Width="1px" runat="server" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container.DataItem, "Deleted").ToString()) %>'>
															</asp:CheckBox>
															<input id="hdfldAkaNum"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Altnum").ToString())%>' name="hdlflAkaNum"/>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="AltName" HeaderText="Alternate Name" HeaderStyle-HorizontalAlign="Left">
														<ItemStyle Font-Size="11px"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="AltType" HeaderText="Type" HeaderStyle-HorizontalAlign="Left">
														<ItemStyle Font-Size="11px"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Status" HeaderText="List Status" HeaderStyle-HorizontalAlign="Left">
														<ItemStyle Font-Size="11px"></ItemStyle>
													</asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Category" HeaderText="Category" HeaderStyle-HorizontalAlign="Left">
														<ItemStyle Font-Size="11px"></ItemStyle>
													</asp:BoundColumn>
												</Columns>
												<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
											</asp:DataGrid></td>
									</tr>
									<tr>
										<td>
											<asp:label id="lblDOBs" style="PADDING-RIGHT: 2px" runat="server" Font-Size="11px" CssClass="LABEL"
												Font-Bold="True">Date of Birth(s): </asp:label></td>
										<td>
											<asp:DataGrid id="dgDateofBirths" runat="server" Width="568px" BorderStyle="Solid" BorderWidth="1px"
												AutoGenerateColumns="False" ShowHeader="False" ForeColor="Black" GridLines="Vertical" CellPadding="3"
												BorderColor="#999999" BackColor="White">
												<FooterStyle BackColor="#CCCCCC"></FooterStyle>
												<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
												<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
												<HeaderStyle CssClass="gridheader"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="dob">
														<ItemStyle Font-Size="11px"></ItemStyle>
													</asp:BoundColumn>
												</Columns>
												<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
											</asp:DataGrid></td>
									</tr>
									<tr>
										<td colspan="2">
											<asp:DataGrid id="dgAttributes" runat="server" Width="713px" BorderStyle="solid" BorderWidth="0px"
												AutoGenerateColumns="False" ShowHeader="False" ForeColor="Black" GridLines="Vertical" CellPadding="3"
												BorderColor="#999999" BackColor="White">
												<FooterStyle BackColor="#CCCCCC"></FooterStyle>
												<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
												<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
												<HeaderStyle CssClass="gridheader"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="Name">
														<ItemStyle Font-Size="11px" Font-Bold="True" HorizontalAlign="Right" Width="138px" BackColor="#cccce5"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Value">
														<ItemStyle Font-Size="11px" BorderWidth="1px" BorderStyle="Solid"></ItemStyle>
													</asp:BoundColumn>
												</Columns>
												<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
											</asp:DataGrid></td>
									</tr>
								</table>
							</asp:Panel></td>
					</tr>
					<tr>
						<td>
							<asp:Panel id="pnlUserdefList" runat="server">
								<table id="gridmain">
									<tr>
										<td>&nbsp;</td>
										<td>
											<asp:label id="lblError" runat="server" Font-Size="8pt" ForeColor="Red" Font-Bold="True"></asp:label></td>
									</tr>
									<tr>
										<td>
											<asp:label id="Label19" runat="server" CssClass="LABEL">Addresses:</asp:label></td>
										<td>
											<asp:datagrid id="grdAddress" runat="server" Width="568px" BorderStyle="Solid" AutoGenerateColumns="False"
												BorderColor="#999999" BackColor="White" ToolTip="List of addresses">
												<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
												<HeaderStyle CssClass="gridheader"></HeaderStyle>
												<Columns>
                                                    <asp:TemplateColumn>
														<HeaderStyle Width="1px" Font-Size="11px"></HeaderStyle>
														<HeaderTemplate>
															Del
														</HeaderTemplate>
														<ItemTemplate>
															<asp:CheckBox id="chkDeleteAddressUSR" Text="" TextAlign=left Width="1px" Runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Deleted") %>'>
															</asp:CheckBox>
														</ItemTemplate>
													</asp:TemplateColumn>
													
													<asp:BoundColumn DataField="AddrNum" ReadOnly="True" HeaderText="#">
														<HeaderStyle Width="25px"></HeaderStyle>
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Address" HeaderText="Address">
														<HeaderStyle Width="150px"></HeaderStyle>
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="City" HeaderText="City">
														<HeaderStyle Width="80px"></HeaderStyle>
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="State" HeaderText="State">
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Country" HeaderText="Country">
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Postal Code" HeaderText="Postal Code">
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit">
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:EditCommandColumn>
													<asp:BoundColumn DataField="AddrNumDb" HeaderText="AddrNumDb" Visible="False">
													</asp:BoundColumn>
												</Columns>
											</asp:datagrid>
											<asp:linkbutton id="lnkAddAddress" runat="server" Font-Size="11px">Add Address</asp:linkbutton></td>
									</tr>
									<tr>
										<td>
											<asp:label id="Label20" runat="server" CssClass="LABEL">Alternate Names:</asp:label></td>
										<td>
											<asp:datagrid id="grdAltNames" runat="server" BorderStyle="Solid" AutoGenerateColumns="False"
												BorderColor="#999999" BackColor="White">
												<HeaderStyle CssClass="gridheader"></HeaderStyle>
												<Columns>
												<asp:TemplateColumn>
														<HeaderStyle Width="1px" Font-Size="11px"></HeaderStyle>
														<HeaderTemplate>
															Del
														</HeaderTemplate>
														<ItemTemplate>
															<asp:CheckBox id="chkDeleteAkaUSR" Text="" TextAlign=left Width="1px" Runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Deleted") %>'>
															</asp:CheckBox>
														</ItemTemplate>
													</asp:TemplateColumn>

													<asp:BoundColumn DataField="AltNum" SortExpression="AltNum" ReadOnly="True" HeaderText="#">
														<HeaderStyle Width="25px"></HeaderStyle>
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="AltName" ReadOnly="True" HeaderText="Alternate Name">
														<HeaderStyle Width="150px"></HeaderStyle>
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="First Name" HeaderText="First Name">
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Middle Name" HeaderText="Middle Name">
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Last Name" HeaderText="Last Name">
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit">
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:EditCommandColumn>
													<asp:BoundColumn DataField="AltNumDb" HeaderText="AltNumDb" Visible="False">
													</asp:BoundColumn>
												</Columns>
											</asp:datagrid>
											<asp:linkbutton id="lnkAddAltName" runat="server" Font-Size="11px">Add Alternate Name</asp:linkbutton></td>
									</tr>
									<tr>
										<td width="100">
											<asp:label id="Label21" runat="server" CssClass="LABEL">Date Of Birth(s):</asp:label></td>
										<td>
											<asp:datagrid id="grdDOBs" runat="server" BorderStyle="Solid" AutoGenerateColumns="False" BorderColor="#999999"
												BackColor="White">
												<HeaderStyle CssClass="gridheader"></HeaderStyle>
												<Columns>
     											 <asp:TemplateColumn>
														<HeaderStyle Width="1px" Font-Size="11px"></HeaderStyle>
														<HeaderTemplate>
															Del
														</HeaderTemplate>
														<ItemTemplate>
															<asp:CheckBox id="chkDeleteDOBUSR" Text="" TextAlign=left Width="1px" Runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Deleted") %>'>															</asp:CheckBox>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="DOBNum" ReadOnly="True" HeaderText="#" ItemStyle-CssClass="GridEditableRegularCol">
														<HeaderStyle Width="25px"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="DOB" HeaderText="DOB (&lt;i&gt;mmddyyyy&lt;/i&gt;)" ItemStyle-CssClass="GridEditableRegularCol">
														<HeaderStyle Width="150px"></HeaderStyle>
													</asp:BoundColumn>
													<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"
														ItemStyle-CssClass="GridEditableRegularCol"></asp:EditCommandColumn>
													<asp:BoundColumn DataField="DOBNumDb" HeaderText="DOBNum" Visible="False"></asp:BoundColumn>
												</Columns>
											</asp:datagrid>
											<asp:linkbutton id="lnkAddDOB" runat="server" Font-Size="11px">Add DOB</asp:linkbutton></td>
									</tr>
									<tr>
										<td width="100">
											<asp:label id="Label26" runat="server" CssClass="LABEL">Attributes:</asp:label></td>
										<td>
											<asp:DropDownList id="ddlNoteType" runat="server" DataValueField="Code" DataTextField="Name"></asp:DropDownList>
											<asp:TextBox id="txtNote" runat="server" MaxLength="500"></asp:TextBox>
											<asp:linkbutton id="lnkAddAttribute" runat="server" Font-Size="11px">Add Attribute</asp:linkbutton>
											<asp:datagrid id="grdNotes" runat="server" BorderStyle="Solid" AutoGenerateColumns="False" BorderColor="#999999"
												BackColor="White">
												<HeaderStyle CssClass="gridheader"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="NoteNum" ReadOnly="True" HeaderText="#">
														<HeaderStyle Width="25px"></HeaderStyle>
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="NoteType" ReadOnly="True">
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:TemplateColumn HeaderText="Attribute Type">
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
														<ItemTemplate>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="Note" HeaderText="Value">
														<HeaderStyle Width="150px"></HeaderStyle>
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:BoundColumn>
													<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit">
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:EditCommandColumn>
													<asp:ButtonColumn Text="Delete" CommandName="Delete">
														<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
													</asp:ButtonColumn>
												</Columns>
											</asp:datagrid></td>
									</tr>
								</table>
							</asp:Panel></td>
					</tr>
				</table>
				<table id="Table4" style="WIDTH: 712px; HEIGHT: 14px" cellspacing="0" cellpadding="0" width="712"
					border="0">
					<tr>
						<td valign="top" width="100">
							<asp:label id="Remarks" runat="server" CssClass="LABEL">Remarks:</asp:label></td>
						<td valign="top" align="left" colspan="1">
							<asp:Literal id="htmlRemarks" runat="server" Text="" Visible="False"></asp:Literal>
							<asp:TextBox id="txtRemarks" runat="server" Height="510px" Width="565px" TextMode="MultiLine"></asp:TextBox></td>
					</tr>
				</table>
			</asp:panel><asp:panel id="Panel2" runat="server" Height="15px" Width="712px" cssclass="Panel">
				<table id="Table11" cellspacing="0" cellpadding="0" width="300" border="0">
					<asp:Panel id="pnlNonVesselInfo" runat="server" Visible="true">
					<tr>
						<td>
							<asp:label id="Label10" runat="server" CssClass="LABEL">Gender:</asp:label></td>
						<td>
							<asp:textbox id="txtSex" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label11" runat="server" CssClass="LABEL">Height (inches):</asp:label></td>
						<td colspan="3">
							<asp:textbox id="txtHeight" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label12" runat="server" CssClass="LABEL">Build:</asp:label></td>
						<td>
							<asp:textbox id="txtBuild" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label14" runat="server" CssClass="LABEL">Weight(lbs):</asp:label></td>
						<td colspan="3">
							<asp:textbox id="txtWeight" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label23" runat="server" CssClass="LABEL">Hair:</asp:label></td>
						<td>
							<asp:textbox id="txtHair" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label22" runat="server" CssClass="LABEL">Eyes:</asp:label></td>
						<td colspan="3">
							<asp:textbox id="txtEyes" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label25" runat="server" CssClass="LABEL">Race:</asp:label></td>
						<td>
							<asp:textbox id="txtRace" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label24" runat="server" CssClass="LABEL">Complexion:</asp:label></td>
						<td colspan="3">
							<asp:textbox id="txtComplexion" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label8" runat="server" CssClass="LABEL">Country:</asp:label></td>
						<td>
							<asp:textbox id="txtCountry" runat="server" CssClass="TEXTBOX" BackColor="LightGray" ReadOnly="True"></asp:textbox></td>
						<td>
						</td>
						<td>
						</td>
					</tr>
					</asp:Panel>
					<asp:Panel id="pnlVesselInfo" runat="server" Visible="true">			 
					<tr>
					    
						<td>
							<asp:label id="Label29" runat="server" CssClass="LABEL">Vessel Owner:</asp:label></td>
						<td>
							<asp:textbox id="txtVessOwner" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label30" runat="server" CssClass="LABEL">Tonnage:</asp:label></td>
						<td colspan="3">
							<asp:textbox id="txtTonnage" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
					    <td>
							<asp:label id="Label37" runat="server" CssClass="LABEL">Vessel Type:</asp:label></td>
						<td>
							<asp:textbox id="txtVessType" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label32" runat="server" CssClass="LABEL">Call Sign:</asp:label></td>
						<td>
							<asp:textbox id="txtCallSign" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						
					</tr>
					<tr>
						<td>
							<asp:label id="Label35" runat="server" CssClass="LABEL">GRT:</asp:label></td>
						<td>
							<asp:textbox id="txtGRT" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						
						<td>
							<asp:label id="Label36" runat="server" CssClass="LABEL">Vessel Flag:</asp:label></td>
						<td>
							<asp:textbox id="txtVessFlag" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						
					</tr>
					</asp:Panel>
					<tr>
						<td>
							<asp:label id="Label6" runat="server" CssClass="LABEL">Created On:</asp:label></td>
						<td>
							<asp:textbox id="txtCreated" runat="server" CssClass="TEXTBOX" BackColor="LightGray" ReadOnly="True"></asp:textbox></td>
						<td>
							<asp:label id="Label7" runat="server" CssClass="LABEL">Last Modified By:</asp:label></td>
						<td>
							<asp:textbox id="txtModifyOper" runat="server" CssClass="TEXTBOX" BackColor="LightGray" ReadOnly="True"></asp:textbox></td>
					</tr>
					
					<tr>
						<td>
							<asp:label id="Label9" runat="server" CssClass="LABEL">Last Modified On:</asp:label></td>
						<td>
							<asp:textbox id="txtLastModified" runat="server" CssClass="TEXTBOX" BackColor="LightGray" ReadOnly="True"></asp:textbox></td>
					    <td></td>
					    <td></td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="WIDTH: 137px" valign="top">
							<asp:label id="Label16" runat="server" Font-Size="11px" CssClass="Label" Font-Bold="True">Sources:</asp:label></td>
						<td style="WIDTH: 376px" colspan="3">
							<asp:DataGrid id="dgUrls" runat="server" Width="560px" BorderStyle="Solid" BorderWidth="1px" AutoGenerateColumns="False"
								ShowHeader="False" ForeColor="Black" GridLines="Vertical" CellPadding="3" BorderColor="#999999"
								BackColor="White">
								<FooterStyle BackColor="#CCCCCC"></FooterStyle>
								<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
								<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
								<HeaderStyle CssClass="gridheader"></HeaderStyle>
								<Columns>
									<asp:HyperLinkColumn Target="_blank" DataNavigateUrlField="url" DataTextField="url">
										<ItemStyle Font-Size="11px"></ItemStyle>
									</asp:HyperLinkColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
							</asp:DataGrid>
							<asp:Panel id="pnlURL" runat="server">
								<asp:datagrid id="grdURLs" runat="server" BorderStyle="Solid" AutoGenerateColumns="False" BorderColor="#999999"
									BackColor="White">
									<HeaderStyle CssClass="gridheader"></HeaderStyle>
									<Columns>
										<asp:BoundColumn DataField="URLNum" ReadOnly="True" HeaderText="#">
											<HeaderStyle Width="25px"></HeaderStyle>
											<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="URL" HeaderText="URL">
											<HeaderStyle Width="150px"></HeaderStyle>
											<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
										</asp:BoundColumn>
										<asp:HyperLinkColumn Text="go" Target="_blank" DataNavigateUrlField="URL">
											<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
										</asp:HyperLinkColumn>
										<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit">
											<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
										</asp:EditCommandColumn>
										<asp:ButtonColumn Text="Delete" CommandName="Delete">
											<ItemStyle CssClass="GridEditableRegularCol"></ItemStyle>
										</asp:ButtonColumn>
									</Columns>
								</asp:datagrid>
								<asp:linkbutton id="lnkAddURL" runat="server" Font-Size="11px">Add URL</asp:linkbutton>
							</asp:Panel></td>
					</tr>
				</table>
				<asp:label id="lblDerivedNames" runat="server" width="804px" CSSClass="paneltitle">Derived Names</asp:label>
				<table id="Table12" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td>
							<asp:DataGrid id="dgDerivedNames" runat="server" Width="713px" BorderStyle="Solid" BorderWidth="1px"
								AutoGenerateColumns="False" ShowHeader="True" ForeColor="Black" GridLines="Vertical" CellPadding="3"
								BorderColor="#999999" BackColor="White">
								<FooterStyle BackColor="#CCCCCC"></FooterStyle>
								<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
								<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
								<HeaderStyle CssClass="gridheader"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn>
										<HeaderStyle Width="1px" Font-Size="11px"></HeaderStyle>
										<HeaderTemplate>
											Deleted
										</HeaderTemplate>
										<ItemTemplate>
											<asp:CheckBox id="chkDeleteDerivedName" Text="" TextAlign=left Width="1px" runat="server" Visible='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container.DataItem, "ShowCheckBox").ToString()) %>' Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container.DataItem, "Deleted").ToString()) %>'>
											</asp:CheckBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn HeaderText="Derived Name" DataField="DerivedName">
										<ItemStyle Font-Size="11px" Font-Bold="True" Width="138px"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn HeaderText="Derived From" DataField="PartyName">
										<ItemStyle Font-Size="11px"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
							</asp:DataGrid>
							<asp:Label id="lblNoDerivedNames" runat="server" Font-Size="11px" Font-Bold="True">No Records Found</asp:Label></td>
					</tr>
				</table>
				<br />
				<asp:label id="lblKeywords" runat="server" width="804px" CSSClass="paneltitle">Keywords</asp:label>
				<TABLE id="Table13" cellSpacing="0" cellPadding="0" border="0">
					<TR>
						<TD>
							<asp:DataGrid id="dgKeywords" runat="server" Width="713px" BorderStyle="Solid" BorderWidth="1px"
								AutoGenerateColumns="False" ShowHeader="True" ForeColor="Black" GridLines="Vertical" CellPadding="3"
								BorderColor="#999999" BackColor="White">
								<FooterStyle BackColor="#CCCCCC"></FooterStyle>
								<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#000099"></SelectedItemStyle>
								<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
								<HeaderStyle CssClass="gridheader"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn>
										<HeaderStyle Width="1px" Font-Size="11px"></HeaderStyle>
										<HeaderTemplate>
											Deleted
										</HeaderTemplate>
										<ItemTemplate>
											<asp:CheckBox id="chkDeleteKeyword" Text="" TextAlign=left Width="1px" Runat="server" Visible='<%# DataBinder.Eval(Container.DataItem, "ShowCheckBox") %>' Checked='<%# DataBinder.Eval(Container.DataItem, "Deleted") %>'>
											</asp:CheckBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn HeaderText="Keyword" DataField="Keyword">
										<ItemStyle Font-Size="11px" Font-Bold="True" Width="138px"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn HeaderText="Derived From" DataField="PartyName">
										<ItemStyle Font-Size="11px"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
							</asp:DataGrid>
							<asp:Label id="lblNoKeywords" runat="server" Font-Size="11px" Font-Bold="True">No Records Found</asp:Label></TD>
					</TR>
				</TABLE>
				<br />
				<asp:label id="lblNameParts" runat="server" width="804px" CSSClass="paneltitle">Name Parts</asp:label>
				<table id="tblNameParts">
					<tr>
						<td>
							<asp:label id="Label13" runat="server" CssClass="LABEL">First Name:</asp:label></td>
						<td colspan="3">
							<asp:textbox id="txtFirstName" runat="server" Width="565px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label27" runat="server" CssClass="LABEL">Middle Name:</asp:label></td>
						<td colspan="3">
							<asp:textbox id="txtMiddleName" runat="server" Width="565px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label28" runat="server" CssClass="LABEL">Last Name:</asp:label></td>
						<td colspan="3">
							<asp:textbox id="txtLastName" runat="server" Width="565px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
				</table>
			</asp:panel><asp:panel id="Panel3" runat="server" Height="15px" Width="712px" cssclass="Panel">
				<asp:DataGrid id="dgRelationships" runat="server" Width="713px" BorderStyle="None" BorderWidth="0px"
					AutoGenerateColumns="False" ShowHeader="False" ForeColor="Black" GridLines="Vertical" CellPadding="3"
					BorderColor="#999999">
					<FooterStyle BackColor="#CCCCCC"></FooterStyle>
					<HeaderStyle CssClass="gridheader"></HeaderStyle>
					<Columns>
						<asp:BoundColumn DataField="Relation">
							<ItemStyle Font-Size="11px" HorizontalAlign="Right" Width="138px" BackColor="#cccce5"></ItemStyle>
						</asp:BoundColumn>
						<asp:TemplateColumn>
							<ItemStyle Font-Size="11px"></ItemStyle>
							<HeaderTemplate>
								Related Party
							</HeaderTemplate>
							<ItemTemplate>
								<a href='javascript:<%# 
											GetPopupScript("SanctionedParty.aspx" & _
											"?RecordID=" & Microsoft.Security.Application.Encoder.UrlEncode(DataBinder.Eval(Container.DataItem, "PartyId").ToString()) & _
											"&ActionID=2&RowNum=0&NoScroll=1", "scrollbars=no,resizable=yes,width=880,height=600") 
											%>'>
									<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.PartyName").ToString())%>
								</a>
							</ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
					<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
				</asp:DataGrid>
			</asp:panel><asp:panel id="cPanelValidators" runat="server" Height="46px" Width="864px"></asp:panel>
			<input id="hdfldSdnType" runat="server" type="hidden" value="" name="hdfldSdnType" />
			</form>
	</body>
</html>
