USE [OFAC]
GO

/****** Object:  StoredProcedure [dbo].[OFS_ImportSDN]    Script Date: 6/5/2018 5:45:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[OFS_ImportSDN]( @entNum int,      
            @name SanctionName,      
            @firstName SanctionName,      
            @middleName SanctionName,      
            @lastName SanctionName,      
            @type char (12),      
            @program char(200),      
            @title varchar(200),      
            @callSign char(8),      
            @vessType char(25),      
            @tonnage char(14),      
            @grt char(8),      
            @vessFlag varchar(40),      
            @vessOwner varchar(150) ,      
            @dob varchar(150) ,      
            @remarks text ,      
            @remarks1 varchar(255) ,      
            @remarks2 varchar(255) ,      
            @sdnType int,      
            @dataType int,      
            @userRec int ,      
            @country varchar(250),      
            @sex char,      
            @build varchar(15),      
            @complexion varchar(15),      
            @race varchar(15),      
            @eyes varchar(15),      
            @hair varchar(15),      
            @height int,      
            @weight int,      
            @listID varchar(15),      
            @listType varchar(15),      
            @primeAdded bit,      
            @standardOrder bit,      
            @effectiveDate datetime,      
            @expiryDate datetime,      
            @status int,      
            @listCreateDate datetime,      
            @listModifDate dateTime,      
			@oper Code,      
            @ignDrvNames BIT,       
  		    @retEntnum int OUTPUT,      
		    @retStatus int OUTPUT)             
AS      
BEGIN      
  declare @event   varchar(3)      
  --Transaction       
  declare @trnCnt        int      
  -- Processing starts here:      
  declare @now datetime      
  declare @pos int      
  declare @stat int      
  declare @text varchar(255)      
  declare @maxNum int      
  declare @usermaxNum int

  select @now = GETDATE()      
  
  select @trnCnt = @@trancount  -- Save the current trancount      
  
  if @trnCnt = 0      
  -- Transaction has not begun      
  begin tran OFS_ImportSDN      
  else      
  -- Already in a transaction      
  save tran OFS_ImportSDN      
  
  Declare @list TABLE(Code VARCHAR(20))      
  insert into @list      
  SELECT code FROM ListType      
  WHERE code in      
  (      
  SELECT listtype from dbo.OFS_GetScopeForListType a       
   INNER JOIN psec.dbo.SEC_fnGetScopeForOperrights(@oper, 'MODOFACLST,DSPOFACLST') b --Rights hardcoded  
      ON a.branchdept = b.branchdept      
  )       
  AND enabled = 1 and isprimelist = 0      
      
	  if NOT Exists (Select Code from @list where code=@listType)      
	  Begin   
			Select @retStatus=201 --No Access Rights Value.  
			Select @retEntNum=0  
			SET @STAT = 1       
			GOTO ENDOFPROC      
	  END  
	  ELSE  
	  BEGIN   
		   If @type = ''      
		   select @type = 'other'      
		   Select ISNULL(@ignDrvNames, 1)      
		   If Exists(SELECT EntNum FROM SDNTable WHERE ListId=@listid AND userrec=@userRec)      
		   BEGIN      
				BEGIN TRY  
				  SELECT @EntNum=EntNum FROM SDNTable WHERE ListId=@listid AND userrec=@userRec      
				  UPDATE SDNTable SET      
				   [name]=@name,      
				   firstName=@firstName ,      
				   middleName=@middleName,      
				   lastName=@lastName ,      
				   type=@type ,      
				   program=UPPER(@program) ,      
				   title=@title ,      
				   callSign=@callSign ,      
				   vessType=@vessType ,      
				   tonnage=@tonnage ,      
				   grt=@grt ,      
				   vessFlag=@vessFlag ,      
				   vessOwner=@vessOwner ,      
				   dob=@dob ,      
				   remarks=@remarks ,      
				   remarks1=@remarks1 ,      
				   remarks2=@remarks2 ,      
				   sdnType=@sdnType ,      
				   dataType=@dataType ,      
				   userRec=@userRec ,      
				   IgnoreDerived = @ignDrvNames ,      
				   country=@country ,      
				   sex=@sex ,      
				   build=@build ,      
				   complexion=@complexion ,      
				   race=@race ,      
				   eyes=@eyes ,      
				   hair=@hair ,      
				   height=@height ,      
				   weight=@weight ,      
				   listID=@listID,      
				   listType=@listType ,      
				   primeAdded=@primeAdded ,      
				   standardOrder=@standardOrder ,      
				   expiryDate=@expiryDate ,      
				   status=3 , --Insert Status Value     
				   listModifDate=@listModifDate , 
				   LastModifDate=@now,   
				   lastoper=@oper      
				  WHERE EntNum = @EntNum      
				  delete from Notes where entnum = @EntNum      
					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

					update sdnaddrtable
					Set Status = 4 --deleted
					where entnum = @EntNum

					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

					update sdnalttable
					Set Status = 4 --deleted
					where entnum = @EntNum

					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

					delete from urls where entnum = @EntNum      

					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

					Update dobs
					Set Status = 4 --deleted
					where entnum = @EntNum

					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

				   delete from relationship where entnum = @EntNum      
				   SELECT @stat = @@ERROR 
					  IF @stat <> 0  GOTO ENDOFPROC
				   delete from relationship where relatedid = @EntNum      
         		   SELECT @stat = @@ERROR 
					  IF @stat <> 0  GOTO ENDOFPROC    
			

				  Select @retStatus=203 --Update Values.     
				  Select @event = 'MOD'      
				END TRY  
				BEGIN CATCH  
				   SELECT ERROR_NUMBER()   
				   Select @stat = @@ERROR      
				   Select @retStatus=204  
				   GOTO ENDOFPROC  
				END CATCH  
		   END      
		   ELSE      
		   BEGIN      
			 BEGIN TRY  
				  Select @maxNum = Max(EntNum) From SDNTable       
				  If @maxNum Is NULL Set @maxNum = 0      
				  if @maxNum < 10000000      
				   Set @maxNum = 10000000      
				  else      
				   --modify 2017.12 for World-Check One
				   Select @usermaxNum = Max(EntNum) From SDNTable where EntNum >= 10000000 and EntNum <= 20000000
				   If @usermaxNum Is NULL Set @usermaxNum = 10000000

				   Set @maxNum = @usermaxNum +1      
				  Set @entNum = @maxNum      
			  
				  Insert into SDNTable (EntNum, [Name], FirstName,      
				   MiddleName, LastName,Title,      
				   Program, Type, CallSign,      
				   VessType, Tonnage, GRT,      
				   VessFlag, VessOwner, Dob,      
				   Remarks, Remarks1, Remarks2,SDNType,      
				   DataType, userRec, deleted,      
				   duplicRec, IgnoreDerived,      
				   ListID, ListType, Country,      
				   Sex, Build, Height, Weight,      
				   Race, Complexion, eyes, hair,      
				   PrimeAdded, StandardOrder, EffectiveDate,      
				   ExpiryDate, Status, ListCreateDate,      
				   ListModifDate, CreateDate, LastModifDate,      
				   LastOper)      
				  Values      
				   (@entNum, @name, NULLIF(@firstName,''),      
				   NULLIF(@middleName,''), NULLIF(@lastName,''), @title,      
				   UPPER(@program),  @type,  @callSign,      
				   @vessType, @tonnage, @grt,      
				   @vessFlag, @vessOwner, NULLIF(@dob,''),      
				   @remarks, @remarks1, @remarks2, @sdnType,      
				   @dataType, @userRec, 0,      
				   0, @ignDrvNames,@listId, @listType, @country,      
				   @sex, @build, NullIf(@height,0), NullIf(@weight,0),      
				   @race, @complexion, @eyes, @hair,@primeAdded,  
				   @standardOrder, @effectiveDate,@expiryDate, 2,  
				   @listCreateDate,@listModifDate, @now, @now, @oper )      
				  Select @retStatus=202 --Insert Status Value     
				  Select @event = 'CRE'      
			 END TRY  
			 BEGIN CATCH  
				  SELECT ERROR_NUMBER()   
				  Select @stat = @@ERROR      
				  Select @retStatus=204  
				  GOTO ENDOFPROC  
			 END CATCH  
			END      
	  
		Select @retEntNum = @entNum      
	        
		If @stat = 0 Begin      
			 Exec OFS_InsEvent @oper, @event, 'SanName', @name, Null      
			 select @stat = @@ERROR      
			 If @stat = 0 Begin      
				 Exec OFS_SetFileImageFlag      
			 End      
		End      
    
  End  
  EndofProc:      
  IF @trnCnt = 0 BEGIN      
	   COMMIT TRAN OFS_ImportSDN
	   END ELSE IF @stat <> 0 BEGIN      
	   ROLLBACK TRAN OFS_ImportSDN      
  END     
         
RETURN @stat      
END 
GO


