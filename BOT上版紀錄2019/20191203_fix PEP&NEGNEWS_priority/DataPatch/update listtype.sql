select distinct EntNum,ListType,Program,Country from SDNTable where EntNum in (
select Entnum from WorldCheck
where (SubCategory != '' and SubCategory is not null)
and Entnum in (
select Entnum from SDNTable(nolock)
where ListType = 'world-check'
and Program = 'negative news'
)
)

begin tran
update SDNTable set listtype = (select case when wcc.[status] = '1' and CHARINDEX(rtrim(sdn.Type),rtrim(wcc.Type)) > 0 then '0282WPEP' 
                                when wcc.[status] = '1' and wcc.Type is null then '0282WPEP' 
                                else 'WPEP' end), Program = wc.SubCategory
from SDNTable sdn
join WorldCheckCountry wcc on wcc.WorldCheck_Country = sdn.Country
join WorldCheck wc on sdn.EntNum = wc.Entnum
where sdn.EntNum in (
select EntNum from WorldCheck
where (SubCategory != '' and SubCategory is not null)
and Entnum in (
select Entnum from SDNTable(nolock)
where ListType = 'world-check'
and Program = 'negative news'
)
)


commit

rollback

select distinct a.EntNum,a.ListType,a.Program,wckl.SDNPROGRAM,wc.Category,a.Status,wcc.Status
from SDNTable a join WorldCheck wc on a.EntNum = wc.Entnum
left join WCKeywords wck on wc.UID = wck.uid
left join WCKeywordslistTable wckl on wck.Word = wckl.Abbreviation
left join WorldCheckCountry wcc on wc.Country = wcc.WorldCheck_Country
where isnull(wc.DelFlag,'0') = '0'
--and wc.UPDDate >= '2019-12-10'
and ((wckl.Used = 1 and wckl.SDNPROGRAM = 'negative news' and (SubCategory is null or SubCategory = '' or a.listtype != '0083WPEP'))
or (wc.Category like '%CRIME%' and (SubCategory is null or SubCategory = '' or a.listtype != '0083WPEP') )
)
and a.ListType != 'WORLD-CHECK' and a.Program != 'Negative news'


begin tran
update SDNTable set ListType = 'WORLD-CHECK', Program = 'Negative News'
where EntNum in (
select a.EntNum
from SDNTable a join WorldCheck wc on a.EntNum = wc.Entnum
left join WCKeywords wck on wc.UID = wck.uid
left join WCKeywordslistTable wckl on wck.Word = wckl.Abbreviation
where isnull(wc.DelFlag,'0') = '0'
--and wc.UPDDate >= '2019-12-10'
and ((wckl.Used = 1 and wckl.SDNPROGRAM = 'negative news' and (SubCategory is null or SubCategory = '' or a.listtype != '0083WPEP'))
or (wc.Category like '%CRIME%' and (SubCategory is null or SubCategory = '' or a.listtype != '0083WPEP') )
)
and a.ListType != 'WORLD-CHECK' and a.Program != 'Negative news'
)

commit
rollback

