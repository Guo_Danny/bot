declare @date_num int;
set @date_num = -2557;

select @date_num = RetentionPeriod from SystemParams;

set @date_num = @date_num * -1;

--select @date_num

/*
select * from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())

select * from MatchHistTable
where SeqNumb in (
select SeqNumb from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())
)

select * from MatchTextHistTable
where SeqNumb in (
select SeqNumb from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())
)

select * from MsgHistTable
where SeqNumb in (
select SeqNumb from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())
)

select * from SDNChangeLog
where logdate < dateadd(day,@date_num,getdate())
*/

--/*

begin tran

delete SDNChangeLog
where logdate < dateadd(day,@date_num,getdate())


delete MatchHistTable
where SeqNumb in (
select SeqNumb from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())
)

delete MatchTextHistTable
where SeqNumb in (
select SeqNumb from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())
)

delete MsgHistTable
where SeqNumb in (
select SeqNumb from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())
)

delete FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())

--*/
--commit


