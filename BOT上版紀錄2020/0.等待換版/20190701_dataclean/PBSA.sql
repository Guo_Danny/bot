declare @date_num int;
set @date_num = -2557;

select @date_num = SusRetention from SysParam;

set @date_num = @date_num * -1;

/*
select * from ActivityHist
where BookDate < dbo.ConvertSqlDateToInt(dateadd(day,@date_num,getdate()))

select * from AlertHist
where AppTime < dateadd(day,@date_num,getdate())

select * from SuspiciousActivity where RecNo = '1130'

select * from SAsActivity 
where ObjectType = 'alert' 
and objectid in (
select AlertNo from AlertHist
where AppTime < dateadd(day,@date_num,getdate())
)

select * from SAsActivity 
where ObjectType = 'SUSPACT'
and objectid in (
select RecNo from SuspiciousActivityHist
where BookDate < dbo.ConvertSqlDateToInt(dateadd(day,@date_num,getdate()))
)

select * from SuspiciousActivityHist
where BookDate < dbo.ConvertSqlDateToInt(dateadd(day,@date_num,getdate()))

*/

--/*
begin tran

delete ActivityHist
where BookDate < dbo.ConvertSqlDateToInt(dateadd(day,@date_num,getdate()))

delete SAsActivity 
where ObjectType = 'alert' 
and objectid in (
select AlertNo from AlertHist
where AppTime < dateadd(day,@date_num,getdate())
)

delete SAsActivity 
where ObjectType = 'SUSPACT'
and objectid in (
select RecNo from SuspiciousActivityHist
where BookDate < dbo.ConvertSqlDateToInt(dateadd(day,@date_num,getdate()))
)

delete AlertHist
where AppTime < dateadd(day,@date_num,getdate())

delete SuspiciousActivityHist
where BookDate < dbo.ConvertSqlDateToInt(dateadd(day,@date_num,getdate()))

delete Event 
where TrnTime < dateadd(day,@date_num,getdate())

--*/

--commit