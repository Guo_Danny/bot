declare @date_num int;
set @date_num = -2557;

/*
select * from AccessLog
where OrigDateTime < dateadd(day,@date_num,getdate())

select * from [Event]
where TrnTime < dateadd(day,@date_num,getdate())

*/

--/*
begin tran

delete AccessLog
where OrigDateTime < dateadd(day,@date_num,getdate())

delete [Event]
where TrnTime < dateadd(day,@date_num,getdate())

--*/
--commit
