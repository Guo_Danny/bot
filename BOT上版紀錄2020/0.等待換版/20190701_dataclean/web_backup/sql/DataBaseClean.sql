USE OFAC
GO

declare @date_num int, @count varchar(12);
set @date_num = -2557;
set @count = 0;

select @date_num = RetentionPeriod from SystemParams;

set @date_num = @date_num * -1;

--select @date_num

/*
select * from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())

select * from MatchHistTable
where SeqNumb in (
select SeqNumb from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())
)

select * from MatchTextHistTable
where SeqNumb in (
select SeqNumb from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())
)

select * from MsgHistTable
where SeqNumb in (
select SeqNumb from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())
)

select * from SDNChangeLog
where logdate < dateadd(day,@date_num,getdate())
*/

--/*
select @count = count(*) from SDNChangeLog 
where logdate < dateadd(day,@date_num,getdate())

insert into SDNChangeLog (LogDate,Oper,LogText, Type,ObjectType, ObjectId,EvtDetail)
values(getdate(), 'Primeadmin', 'Clean '+ @count +' records log for 7 years ago','Ann','Event','','Clean '+ @count +' records log for 7 years ago')

delete SDNChangeLog
where logdate < dateadd(day,@date_num,getdate())

/*
delete MatchHistTable
where SeqNumb in (
select SeqNumb from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())
)

delete MatchTextHistTable
where SeqNumb in (
select SeqNumb from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())
)

delete MsgHistTable
where SeqNumb in (
select SeqNumb from FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())
)

delete FilterTranHistTable
where AppTime < dateadd(day,@date_num,getdate())
*/

--*/
--commit

/*
select * from ActivityHist
where trandate < dbo.ConvertSqlDateToInt(dateadd(day,@date_num,getdate()))

select * from AlertHist
where AppTime < dateadd(day,@date_num,getdate())

select * from SuspiciousActivity where RecNo = '1130'

select * from SAsActivity 
where ObjectType = 'alert' 
and objectid in (
select AlertNo from AlertHist
where AppTime < dateadd(day,@date_num,getdate())
)

select * from SAsActivity 
where ObjectType = 'SUSPACT'
and objectid in (
select RecNo from SuspiciousActivityHist
where closedate < dbo.ConvertSqlDateToInt(dateadd(day,@date_num,getdate()))
)

select * from SuspiciousActivityHist
where closedate < dbo.ConvertSqlDateToInt(dateadd(day,@date_num,getdate()))

select * from event

*/

select @count = count(*) from PBSA..Event 
where TrnTime < dateadd(day,@date_num,getdate())

insert into PBSA..Event (TrnTime,Oper,LogText, Type,ObjectType, ObjectId,EvtDetail)
values(getdate(), 'Primeadmin', 'Clean '+ @count +' records log for 7 years ago','Ann','Event','','Clean '+ @count +' records log for 7 years ago')

delete PBSA..Event 
where TrnTime < dateadd(day,@date_num,getdate())


--select * from Event
--select * from accesslog

select @count = count(*) from PSEC..Event 
where TrnTime < dateadd(day,@date_num,getdate())

insert into PSEC..Event (TrnTime,Oper,LogText, Type,ObjectType, ObjectId,EvtDetail)
values(getdate(), 'Primeadmin', 'Clean '+ @count +' records log for 7 years ago','Tru','Event','','Clean '+ @count +' records log for 7 years ago')

delete PSEC..[Event]
where TrnTime < dateadd(day,@date_num,getdate())

select @count = count(*) from PSEC..AccessLog 
where OrigDateTime < dateadd(day,@date_num,getdate())

insert into PSEC..AccessLog (OrigDateTime,CurrDateTime,Code,UserName,Node,Image,Status,ErrorText)
values(getdate(),getdate(), 'Unknown','Primeadmin', 'OSBAML','TRUNCATE',0,'Clean '+ @count +' records log for 7 years ago')

delete PSEC..AccessLog
where OrigDateTime < dateadd(day,@date_num,getdate())


select @count = count(*) from PTRNG..Event 
where TrnTime < dateadd(day,@date_num,getdate())

insert into PTRNG..Event (TrnTime,Oper,LogText, Type,ObjectType, ObjectId)
values(getdate(), 'Primeadmin', 'Clean '+ @count +' records log for 7 years ago','Tru','Event','')

delete PTRNG..event where TrnTime < dateadd(day,@date_num,getdate())


select @count = count(*) from REIMB..Event 
where TrnTime < dateadd(day,@date_num,getdate())

insert into REIMB..Event (TrnTime,Oper,LogText, Type,ObjectType, ObjectId)
values(getdate(), 'Primeadmin', 'Clean '+ @count +' records log for 7 years ago','Tru','Event','')

delete REIMB..event where TrnTime < dateadd(day,@date_num,getdate())
