USE [Psec]
GO

/****** Object:  View [dbo].[GetRoleRight_Oper]    Script Date: 12/2/2019 1:52:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO





------------------------------------------------------
--        Views                                                                                                                                                      
------------------------------------------------------                                                                                                               


CREATE View [dbo].[GetRoleRight_Oper]
As
select distinct rr.[role],rr.[Right],rm.[Oper], OPRT.ObjBranch,OPRT.ObjDept,OPRT.Scope
from RoleRights rr
join OperRights OPRT on rr.[Right] = OPRT.[Right]
join RoleMembers rm on rm.Role = rr.[Role]

GO


