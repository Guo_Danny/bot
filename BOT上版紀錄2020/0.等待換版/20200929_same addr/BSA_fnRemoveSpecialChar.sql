USE [PBSA]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


---------------------------------------------------------------------------------
CREATE Function [dbo].[BSA_fnRemoveSpecialChar] (@Inputs Varchar(120) )  
	Returns Varchar(120)  
  As  
/*
This function remove special characters as ''
*/
BEGIN
   DECLARE @retVal varchar(120)
   set @retVal = replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
				replace(
						@Inputs
							,char(32),'') -- space
							,char(33),'') -- !
							,char(34),'') -- "         
							,char(35),'') -- #         
							,char(36),'') -- $       
							,char(37),'') -- $          
							,char(38),'') -- &          
							,char(39),'') -- '          
							,char(40),'') -- (          
							,char(41),'') -- )          
							,char(42),'') -- *          
							,char(43),'') -- +          
							,char(44),'') -- ,          
							,char(45),'') -- -          
							,char(46),'') -- .          
							,char(47),'') -- /                 
							,char(58),'') --           
							,char(59),'') -- ;          
							,char(60),'') -- <          
							,char(61),'') -- =          
							,char(62),'') -- >          
							,char(63),'') -- ?          
							,char(64),'') -- @          
							,char(91),'') -- [ 
							,char(92),'') -- \
							,char(93),'') -- ]
							,char(94),'') -- ^
							,char(95),'') -- _
							,char(96),'') -- `
							,char(123),'') -- }    
							,char(124),'') -- |         
							,char(125),'') -- }          
							,char(126),'') -- ~         
							,'（','')                      
							,'）','')                      
							,'。','')                      
							,'，','')                      
							,'、','')  ;
   RETURN(@retVal)
END

GO


