
use OFAC

truncate table TempLoadAltTable;

--procedure 1

declare @ListCreateDate datetime, @ListModifDate datetime,@CreateDate datetime, @LastModifDate datetime, 
@entnum varchar(15),@SDNName varchar(3000),@Aliases varchar(3000), @maxaltnum varchar(15), @location varchar(5000), @sdntype varchar(30),
@AltType varchar(200), @Category varchar(200), @SDNName_tmp varchar(200), @Aliases_tmp varchar(5000), @location_1 varchar(1000),@location_1_1 varchar(1000),@location_2 varchar(1000) ,@location_2_1 varchar(1000)
set @ListCreateDate = NULL;
set @ListModifDate = NULL;
set @CreateDate = NULL;
set @LastModifDate = NULL;
set @location_1_1 = '';
select @maxaltnum = max(AltNum)+1 from SDNAltTable(nolock)

declare cur CURSOR FOR
select distinct sdna.EntNum,sdn.Name,wc.Aliases,sdna.AltType,sdn.[Type],sdna.Category,sdn.ListCreatedate,sdn.ListModifDate
from OFAC..SDNTable sdn
join OFAC..SDNAltTable sdna on sdn.EntNum = sdna.EntNum
join OFAC..WorldCheck wc on wc.Entnum = sdn.EntNum
where len(sdna.AltName) <= 4
--and sdna.EntNum in ('21708985','20000702')
--and sdn.EntNum < 20001000
open cur;
fetch next from cur into @entnum,@SDNName,@Aliases,@AltType,@sdntype,@Category,@ListCreateDate,@ListModifDate;
WHILE @@fetch_status = 0 
begin
	set @location_1_1 = '';
	set @SDNName_tmp = @SDNName;
	-- get sdn name parts
	WHILE CHARINDEX(' ',@SDNName_tmp) > 0
	begin
		set @location_1 = SUBSTRING(@SDNName_tmp, 1, 1);
		set @SDNName_tmp = SUBSTRING(@SDNName_tmp, CHARINDEX(' ', @SDNName_tmp) + 1, LEN(@SDNName_tmp) - CHARINDEX(' ', @SDNName_tmp));
		set @location_1_1 = @location_1_1 + @location_1
	end
	if len(@SDNName_tmp) > 0
	begin 
		set @location_1 = SUBSTRING(@SDNName_tmp,1,1);
		set @location_1_1 = ltrim(rtrim(@location_1_1)) + ltrim(rtrim(@location_1));
	end
	--print 'abbreviation=' + @location_1_1
	
	-- get aliases
	set @Aliases_tmp = @Aliases;
	WHILE CHARINDEX(';',@Aliases_tmp) > 0
		begin
			-- part1
			set @location_2 = SUBSTRING(@Aliases_tmp, 1, CHARINDEX(';', @Aliases_tmp) - 1);
			set @Aliases_tmp = SUBSTRING(@Aliases_tmp, CHARINDEX(';', @Aliases_tmp) + 1, LEN(@Aliases_tmp) - CHARINDEX(';', @Aliases_tmp));

			--print @location_2

			/*--insert to table*/
			if (@location_1_1 = ltrim(rtrim(@location_2)))
			begin 
				if (@sdntype = 'individual')
				begin
					insert into TempLoadAltTable(EntNum,AltNum,AltType,AltName,LastName,ListID,Remarks,Deleted,Status,ListCreatedate,ListModifDate,LastOper,Category)
					values (@entnum, @maxaltnum,'a.k.a.',@location_2,@location_2, NULL, 'Manual add', 1, 4,@ListCreateDate, @ListModifDate, 'PRIMEADMIN', NULL)
					set @maxaltnum = @maxaltnum + 1;
				end
				else
				begin
					insert into TempLoadAltTable(EntNum,AltNum,AltType,AltName,LastName,ListID,Remarks,Deleted,Status,ListCreatedate,ListModifDate,LastOper,Category)
					values (@entnum, @maxaltnum,'a.k.a.',@location_2,NULL, NULL, 'Manual add', 1, 4,@ListCreateDate, @ListModifDate, 'PRIMEADMIN', NULL)
					set @maxaltnum = @maxaltnum + 1;
				end
			end
			else
			begin
				if (@sdntype = 'individual')
				begin
					insert into TempLoadAltTable(EntNum,AltNum,AltType,AltName,LastName,ListID,Remarks,Deleted,Status,ListCreatedate,ListModifDate,LastOper,Category)
					values (@entnum, @maxaltnum,'a.k.a.',@location_2,@location_2, NULL, 'Manual add', 0, 1,@ListCreateDate, @ListModifDate, 'PRIMEADMIN', NULL)
					set @maxaltnum = @maxaltnum + 1;
				end
				else
				begin
					insert into TempLoadAltTable(EntNum,AltNum,AltType,AltName,LastName,ListID,Remarks,Deleted,Status,ListCreatedate,ListModifDate,LastOper,Category)
					values (@entnum, @maxaltnum,'a.k.a.',@location_2,NULL, NULL, 'Manual add', 0, 1,@ListCreateDate, @ListModifDate, 'PRIMEADMIN', NULL)
					set @maxaltnum = @maxaltnum + 1;
				end
			end
		end
		--check for the tail string
		if len(@Aliases_tmp) > 0
		begin
			set @location_2 = @Aliases_tmp;
			if (@location_1_1 = ltrim(rtrim(@location_2)))
			begin 
				if (@sdntype = 'individual')
				begin
					insert into TempLoadAltTable(EntNum,AltNum,AltType,AltName,LastName,ListID,Remarks,Deleted,Status,ListCreatedate,ListModifDate,LastOper,Category)
					values (@entnum, @maxaltnum,'a.k.a.',@location_2,@location_2, NULL, 'Manual add', 1, 4,@ListCreateDate, @ListModifDate, 'PRIMEADMIN', NULL)
					set @maxaltnum = @maxaltnum + 1;
				end
				else
				begin
					insert into TempLoadAltTable(EntNum,AltNum,AltType,AltName,LastName,ListID,Remarks,Deleted,Status,ListCreatedate,ListModifDate,LastOper,Category)
					values (@entnum, @maxaltnum,'a.k.a.',@location_2,NULL, NULL, 'Manual add', 1, 4,@ListCreateDate, @ListModifDate, 'PRIMEADMIN', NULL)
					set @maxaltnum = @maxaltnum + 1;
				end
			end
			else
			begin
				if (@sdntype = 'individual')
				begin
					insert into TempLoadAltTable(EntNum,AltNum,AltType,AltName,LastName,ListID,Remarks,Deleted,Status,ListCreatedate,ListModifDate,LastOper,Category)
					values (@entnum, @maxaltnum,'a.k.a.',@location_2,@location_2, NULL, 'Manual add', 0, 1,@ListCreateDate, @ListModifDate, 'PRIMEADMIN', NULL)
					set @maxaltnum = @maxaltnum + 1;
				end
				else
				begin
					insert into TempLoadAltTable(EntNum,AltNum,AltType,AltName,LastName,ListID,Remarks,Deleted,Status,ListCreatedate,ListModifDate,LastOper,Category)
					values (@entnum, @maxaltnum,'a.k.a.',@location_2,NULL, NULL, 'Manual add', 0, 1,@ListCreateDate, @ListModifDate, 'PRIMEADMIN', NULL)
					set @maxaltnum = @maxaltnum + 1;
				end
			end
		end
		--print @location_2
  fetch next from cur into @entnum,@SDNName,@Aliases,@AltType,@sdntype,@Category,@ListCreateDate,@ListModifDate;
end
close cur;
DEALLOCATE cur;

/*
insert into SDNAltTable([EntNum],[AltNum],[AltType],[AltName],[LastName],[ListID],[Remarks],[Deleted],[Status],[ListCreateDate],[ListModifDate],[CreateDate],[LastOper],[Category])
select [EntNum],[AltNum],[AltType],[AltName],[LastName],[ListID],[Remarks],[Deleted],[Status],[ListCreateDate],[ListModifDate],getdate(),[LastOper],[Category]
from TempLoadAltTable
*/
