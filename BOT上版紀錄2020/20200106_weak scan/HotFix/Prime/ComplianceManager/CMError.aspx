<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CMError.aspx.vb" Inherits="CMBrowser.CMError"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Compliance Suite Error</title>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="Styles.css" type="text/css" rel="stylesheet"/>
		<script language="JavaScript" src="js/nav.js"></script>
		<script language="javascript" id="clientEventHandlersJS">
<!--

function lnkMoreDetails_onclick() {

	if (document.getElementById ("lblErrorDetails").style.display == "")
	{
		document.getElementById ("lblErrorDetails").style.display = "none";
		document.getElementById ("lnkMoreDetails").innerText = "More Technical Details...";
	}
	else
	{
		document.getElementById ("lblErrorDetails").style.display = "";
		document.getElementById ("lnkMoreDetails").innerText = "Hide Technical Details...";
	}
}

//-->
		</script>
	</head>
	<body  onload="AddWindowToPCSWindowCookie(window.name);" onUnload="RemoveWindowFromPCSWindowCookie(window.name);">
		<form id="Form1" method="post" runat="server">
			<asp:imagebutton id="imgPrime" runat="server" ImageUrl="images/compliance-manager.gif" AlternateText="Home"></asp:imagebutton>
			<table width="100%">
				<tr>
						<!--<asp:label id="lblMessage" runat="server" CssClass="systemerror"></asp:label><BR>
						<asp:panel id="pnlErrorDetails" Runat="server">
						</asp:panel>-->
				</tr>
				<tr>
					<td style="HEIGHT: 55px"></td>
					<td style="HEIGHT: 55px"><asp:label id="lblUserDetailedMsg" runat="server" CssClass="systemerror"></asp:label><br>
						<hr/>
					</td>
					<td style="HEIGHT: 55px"></td>
				</tr>
				<tr>
					<td style="HEIGHT: 37px"></td>
					<td style="HEIGHT: 37px"><a language="javascript" class="lnktask" id="lnkMoreDetails" onclick="return lnkMoreDetails_onclick()">More 
							Technical Details...</a>
						<br/>
						<br/>
						<asp:label id="lblErrorDetails" style="DISPLAY: none" runat="server" CssClass="systemerrordetails"></asp:label></td>
					<td style="HEIGHT: 37px"></td>
				</tr>
				<tr>
					<td></td>
					<td align="center"><br/>
						<input onclick="history.back()" type="button" value="Back"/> <input onclick="window.close();" type="button" value="Close"/>
					</td>
					<td></td>
				</tr>
			</table>
		</form>
	</body>
</html>
