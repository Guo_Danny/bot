
use OFAC
GO

update ListType set IsPrimeList = 1
where Code = 'WORLD-CHECK' and IsPrimeList != 1;

truncate table TmpSDNAltTable;

--procedure 1
--begin tran
declare @ListCreateDate datetime, @ListModifDate datetime,@CreateDate datetime, @LastModifDate datetime, 
@entnum varchar(15),@altname varchar(max), @altname_tmp varchar(max), @altname_tmp1 varchar(max), @maxaltnum varchar(15),@LastOper varchar(15), @Category varchar(15)
set @ListCreateDate = NULL;
set @ListModifDate = NULL;
set @CreateDate = NULL;
set @LastModifDate = NULL;
select @maxaltnum = max(AltNum)+1 from SDNAltTable (nolock);
set @LastOper = 'PrimeAdmin'
set @Category = 'weak'

declare cur CURSOR FOR
select sdn.entnum, wc.LowQAliases
from SDNTable(nolock) sdn
join worldcheck (nolock) wc on sdn.entnum = wc.entnum
where sdn.EntNum > 20000000 and sdn.Status != 4 
and sdn.ListType = 'World-Check'
and isnull(LowQAliases,'') != ''
and sdn.Program = 'OFAC'
and wc.UPDDate > getdate() - 0.042
--and (sdn.ListType like '0%WPEP' or sdn.ListType = 'WORLD-CHECK')
--and sdn.EntNum < 20001000
open cur;
fetch next from cur into @entnum,@altname;
WHILE @@fetch_status = 0 
begin
	set @altname_tmp = @altname;
	
	WHILE CHARINDEX(';',@altname_tmp) > 0
		begin
			-- part1
			set @altname_tmp1 = SUBSTRING(@altname_tmp, 1, CHARINDEX(';', @altname_tmp) - 1);
			set @altname_tmp = SUBSTRING(@altname_tmp, CHARINDEX(';', @altname_tmp) + 1, LEN(@altname_tmp) - CHARINDEX(';', @altname_tmp));
			
			--insert to table
			if (@altname_tmp1 != '')
			begin 
			insert into TmpSDNAltTable(EntNum,AltNum,AltName,Status)
			values (@entnum, @maxaltnum,@altname_tmp1,'1')
			set @maxaltnum = @maxaltnum + 1;
			end
		end
	--print @location_tmp;
	if len(@altname_tmp) > 0
		begin

			--insert to table
			insert into TmpSDNAltTable(EntNum,AltNum,AltName,Status)
			values (@entnum, @maxaltnum,@altname_tmp,'1')
			set @maxaltnum = @maxaltnum + 1;
		end
  fetch next from cur into @entnum,@altname;
end
close cur;
DEALLOCATE cur;

--commit
--rollback

--procedure 2
--begin tran

insert into SDNAltTable (EntNum,AltNum,AltType,AltName,Deleted,Status,ListCreateDate,ListModifDate,CreateDate,LastModifDate,LastOper,Category)
select tsdna.EntNum,AltNum,'a.k.a.',AltName,0,2, sdn.ListCreatedate,sdn.ListModifDate,sdn.CreateDate,getdate(),'PrimeAdmin','weak'
from TmpSDNAltTable tsdna
join SDNTable sdn on sdn.EntNum = tsdna.EntNum

--commit
--rollback

--select * from SDNAltTable where EntNum = '20624354'

--select count(*) from OFAC..TmpAddrTable

--select * from OFAC..TmpSDNAltTable
--select * from SDNAltTable where AltNum = '11525190'

--added 2020.01
update SDNAltTable set Status = 1 
from SDNAltTable sdna 
join SDNTable sdn on sdn.EntNum = sdna.EntNum
where sdn.Status != 4 and sdna.Status = 4

update SDNAltTable set AltType = 'a.k.a.' where Category = 'weak'
and EntNum > 20000000
