USE PBSA
GO

declare @RFCODE varchar(20),@country_code varchar(50), @score int, @riskfactor varchar(20); 
set @score = 0;
set @riskfactor = '0';
--set @RFCODE = 'oiTxCountry';
--set @RFCODE = 'OiNational';
--set @RFCODE = 'OeNational';
--set @RFCODE = 'OeResidence';
set @RFCODE = 'oeTxCountry';



delete RiskFactorValue
where RFICode = @RFCODE

if @RFCODE in ('oiTxCountry','OiNational','OeNational','OeResidence','oeTxCountry')
begin
  insert into RiskFactorValue (RFICode,[Value],DataType,Score,CreateOper,CreateDate)
  values(@RFCODE,'AF',0,25,'PRIMEADMIN',getdate())
  insert into RiskFactorValue (RFICode,[Value],DataType,Score,CreateOper,CreateDate)
  values(@RFCODE,'ZA',0,4,'PRIMEADMIN',getdate())
  insert into RiskFactorValue (RFICode,[Value],DataType,Score,CreateOper,CreateDate)
  values(@RFCODE,'TW',0,6,'PRIMEADMIN',getdate())
  insert into RiskFactorValue (RFICode,[Value],DataType,Score,CreateOper,CreateDate)
  values(@RFCODE,'CN',0,9,'PRIMEADMIN',getdate())
end
else
begin
  insert into RiskFactorValue (RFICode,[Value],DataType,Score,CreateOper,CreateDate)
  values(@RFCODE,'AF',0,25,'PRIMEADMIN',getdate())
  insert into RiskFactorValue (RFICode,[Value],DataType,Score,CreateOper,CreateDate)
  values(@RFCODE,'ZA',0,8,'PRIMEADMIN',getdate())
  insert into RiskFactorValue (RFICode,[Value],DataType,Score,CreateOper,CreateDate)
  values(@RFCODE,'TW',0,12,'PRIMEADMIN',getdate())
  insert into RiskFactorValue (RFICode,[Value],DataType,Score,CreateOper,CreateDate)
  values(@RFCODE,'CN',0,18,'PRIMEADMIN',getdate())
end


declare cur CURSOR FOR
select Code from Country where code not in ('90','AF','ZA','TW','CN')

open cur;
fetch next from cur into @country_code;
WHILE @@fetch_status = 0 
begin
  
  select @riskfactor = RiskFactor from [Country_ZARisk] where code = @country_code

  set @score = 25;
  if @RFCODE in ('oiTxCountry','OiNational','OeNational','OeResidence','oeTxCountry')
  begin 
	  if @riskfactor = '1'
		 set @score = 4;
	  if @riskfactor = '2'
		 set @score = 6;
	  if @riskfactor = '3'
		 set @score = 12;
	  if @riskfactor = '4'
		 set @score = 25;
	  if @country_code = 'CN'
		 set @score = 9;
	  if @country_code = 'AL'
		 set @score = 9;
	  if @country_code = 'BD'
		 set @score = 9;
	  if @country_code = 'IN'
		 set @score = 9;
	  if @country_code = 'KW'
		 set @score = 9;
	  if @country_code = 'MW'
		 set @score = 9;
	  if @country_code = 'MY'
		 set @score = 9;
	  if @country_code = 'PH'
		 set @score = 9;
	  if @country_code = 'LC'
		 set @score = 9;
  end
  else 
  begin
	  if @riskfactor = '1'
		 set @score = 8;
	  if @riskfactor = '2'
		 set @score = 12;
	  if @riskfactor = '3'
		 set @score = 24;
	  if @riskfactor = '4'
		 set @score = 25;
	  if @country_code = 'CN'
		 set @score = 18;
	  if @country_code = 'AL'
		 set @score = 18;
	  if @country_code = 'AU'
		 set @score = 18;
	  if @country_code = 'BA'
		 set @score = 16;
	  if @country_code = 'BD'
		 set @score = 18;
	  if @country_code = 'HK'
		 set @score = 18;
	  if @country_code = 'IN'
		 set @score = 18;
	  if @country_code = 'KW'
		 set @score = 18;
	  if @country_code = 'MW'
		 set @score = 18;
	  if @country_code = 'MY'
		 set @score = 18;
	  if @country_code = 'PH'
		 set @score = 18;
	  if @country_code = 'LC'
		 set @score = 18;
	  if @country_code = 'TH'
		 set @score = 18;
  end
  insert into RiskFactorValue (RFICode,[Value],DataType,Score,CreateOper,CreateDate)
  values(@RFCODE,@country_code,0,@score,'PRIMEADMIN',getdate())

  fetch next from cur into @country_code;
end
close cur;
DEALLOCATE cur;
