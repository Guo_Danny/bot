--Risk calculate item for SP
--20210310 no need use
USE [PBSA]
GO
SET IDENTITY_INSERT [dbo].[RFCalculatedItem] ON 

INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (1, N'Fatf Country', NULL, 1, N'RFC_FatFCountry', NULL, 0, N'Prime', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (2, N'Years of Opening', NULL, 1, N'RFC_YearsOpening', NULL, 0, N'Prime', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (3, N'No Address', NULL, 1, N'RFC_NoAddress', NULL, 0, N'Prime', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (4, N'No Tel', NULL, 1, N'RFC_NoTel', NULL, 0, N'Prime', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (5, N'None Parent', NULL, 1, N'RFC_NonParent', NULL, 0, N'prime', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (6, N'OnBoard Transaction Channel', NULL, 1, N'RFC_OnBoardTxChannel', NULL, 0, N'Prime', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (7, N'OnBoard Transaction Channel', NULL, 1, N'RFC_OnBoardTxChannel', NULL, 0, N'Prime', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (8, N'OnBoard Transaction Channel', NULL, 1, N'RFC_OnBdTxChannel', NULL, 0, N'Prime', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (9, N'OnBoard Transaction Channel', NULL, 1, N'RFC_OnBdTxChannel', NULL, 0, N'Prime', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (10, N'OnBoard Transaction Channel', NULL, 1, N'RFC_OnBdTxChannel', NULL, 0, N'Prime', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (21, N'YearsOpening', NULL, 1, N'RFC_YearsOpening', NULL, 0, N'Prime', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (22, N'YearsOpening', NULL, 1, N'RFC_YearsOpening', NULL, 0, N'Prime', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (23, N'New Individual Customer', NULL, 1, N'RFC_NewCustI', NULL, 0, N'PrimeAdmin', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (24, N'New Entity Customer', NULL, 1, N'RFC_NewCustE', NULL, 0, N'PrimeAdmin', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (25, N'Individual Customer', NULL, 1, N'RFC_CustI', NULL, 0, N'PrimeAdmin', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (26, N'Entity Customer', NULL, 1, N'RFC_CustE', NULL, 0, N'PrimeAdmin', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (27, N'RiskChannel', NULL, 1, N'RFC_RiskChannel', NULL, 0, N'PRIMEADMIN', getdate(), NULL, NULL)
INSERT [dbo].[RFCalculatedItem] ([ID], [Name], [Description], [Type], [ProcName], [Params], [Status], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (27, N'ChannelRisk', NULL, 1, N'RFC_ChannelRisk', NULL, 0, N'PRIMEADMIN', getdate(), NULL, NULL)

SET IDENTITY_INSERT [dbo].[RFCalculatedItem] OFF
