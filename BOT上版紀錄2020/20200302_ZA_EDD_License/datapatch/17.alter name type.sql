USE [PBSA]
GO

/****** Object:  UserDefinedDataType [dbo].[NoteDataType1]    Script Date: 8/26/2020 5:19:38 PM ******/
CREATE TYPE [dbo].[LongName1] FROM [nvarchar](100) NULL
GO

DROP INDEX [IdxCustomer_Name] ON [dbo].[Customer] WITH ( ONLINE = OFF )
GO
ALTER TABLE customer ALTER COLUMN [name] [LongName1] NULL;

CREATE CLUSTERED INDEX [IdxCustomer_Name] ON [dbo].[Customer]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE customerhist ALTER COLUMN [name] [LongName1] NULL;

ALTER TABLE Account ALTER COLUMN [name] [LongName1] NULL;
ALTER TABLE CustomerOwner ALTER COLUMN [name] [LongName1] NULL;
ALTER TABLE CustomerReference ALTER COLUMN [refname] [LongName1] NULL;
ALTER TABLE KYCCNames ALTER COLUMN [Custname] [LongName1] NULL;

/****** Object:  Index [IdxKYCCNames_KYCCName]    Script Date: 9/9/2020 4:43:38 PM ******/
DROP INDEX [IdxKYCCNames_KYCCName] ON [dbo].[KYCCNames] WITH ( ONLINE = OFF )
GO

ALTER TABLE KYCCNames ALTER COLUMN [KYCCname] [LongName1] NOT NULL;

SET ANSI_PADDING ON
GO

/****** Object:  Index [IdxKYCCNames_KYCCName]    Script Date: 9/9/2020 4:43:38 PM ******/
CREATE CLUSTERED INDEX [IdxKYCCNames_KYCCName] ON [dbo].[KYCCNames]
(
	[KYCCName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE kycdata ALTER COLUMN [CDDUSER40] [varchar](3000) NULL;


update Pcdb..ListViewColumns set displayname='Currency' where TableName='Account' and ColumnName='user1' select * from pcdb..ListViewColumns where TableName='Account' and ColumnName='user1'

--2020.11 add
alter table customer alter column notes varchar(4000)

