USE [PBSA]
GO

/****** Object:  Table [dbo].[EddFormItem]    Script Date: 11/25/2020 3:55:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EddFormItem](
	[ItemSno] [int] IDENTITY(1,1) NOT NULL,
	[FormType] [varchar](35) NOT NULL,
	[FormID] [varchar](50) NOT NULL,
	[ItemID] [varchar](50) NOT NULL,
	[ItemName] [varchar](300) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastModifyDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EddFormItem] PRIMARY KEY CLUSTERED 
(
	[ItemSno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

---
USE [PBSA]
GO

/****** Object:  Table [dbo].[EddFormID]    Script Date: 11/25/2020 3:55:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [dbo].[EddFormID](
	[FormSno] [int] IDENTITY(1,1) NOT NULL,
	[FormType] [varchar](35) NOT NULL,
	[FormVersion] [varchar](100) NOT NULL,
	[FormName] [varchar](300) NOT NULL,
	[FormID] [varchar](50) NOT NULL,
	[FormIDName] [varchar](300) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastModifyDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EddFormID] PRIMARY KEY CLUSTERED 
(
	[FormSno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[CRY_EDD_FormCheckLists]    Script Date: 11/27/2020 6:39:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
Create PROCEDURE [dbo].[CRY_EDD_FormCheckLists]  
(
	@Cust 		  VARCHAR(35),
	@FormType	  VARCHAR(35)
)

  AS

	Declare @iNewCust int, @CustName varchar(200), @PoposeRisk varchar(50)
	Declare @AMLCO varchar(50), @CCO varchar(50), @GM varchar(50)
	Declare @AMLCOdate varchar(50),@CCOdate varchar(50), @GMdate varchar(50)
	Declare @txt varchar(4000), @strTxt varchar(4000),@cDate datetime

/*	
	-- Cust
	IF(ISNULL(@Cust, '') = '' OR UPPER(ISNULL(@Cust, '-ALL-')) = '-ALL-')
	    SELECT @Cust = NULL
	ELSE
    	SELECT @Cust = ',' + Replace(IsNull(LTRIM(RTRIM(@Cust)), ''),', ',',') + ','
 
	--FormType
	IF(ISNULL(@FormType, '') = '' OR UPPER(ISNULL(@FormType, '-ALL-')) = '-ALL-')
	    SELECT @FormType = NULL
	ELSE
    	SELECT @FormType = ',' + Replace(IsNull(LTRIM(RTRIM(@FormType)), ''),', ',',') + ','
 */
    --get newcust
	SELECT @iNewCust=0,@CustName='',@txt='', @strTxt=''
    --select @iNewCust=1,@CustName=Name from Customer(nolock) 
	--where id=@Cust and ProbationStartDate is null 
	--AND   datediff(day,KYCDataCreateDate,getdate()) < 14
    select @txt=nullif(ProbationStartDate,''),@CustName=Name,@cDate=KYCDataCreateDate from Customer(nolock) 
	where id=@Cust
	if (@txt='' or @txt is null) and datediff(day,@cDate,getdate()) < 30
	   select @iNewCust=1

	--get popose risk
	select @PoposeRisk=ProposedRiskClass from CustProposedRiskClass 
	where cust=@Cust

	select @AMLCO='', @AMLCOdate='',@CCO='',@CCOdate='', @GM='', @GMdate=''
	if @iNewCust = 1 and (@PoposeRisk= 'HighRisk' or @PoposeRisk='DeRisk')
       begin
          select @AMLCO='AMLCO : ',@AMLCOdate='DATE : ',@CCO='C.C.O. :',@CCOdate='DATE : ',@GM='G.M. : ', @GMdate='DATE : '
       end
    else if  (@iNewCust=1 AND  @PoposeRisk= 'MedHiRisk' ) or (@iNewCust=0 AND @PoposeRisk= 'MedHiRisk' or @PoposeRisk= 'HighRisk' or @PoposeRisk='DeRisk')
       begin
	      select @AMLCO='AMLCO : ',@AMLCOdate='DATE : '
       end

	   --Get Form Check List
	   select FID.*, FITEM.*,@iNewCust AS iNewCust,@PoposeRisk AS PoposeRisk ,@Cust AS Cust,@CustName AS CustName ,@AMLCO AS AMLCO,@AMLCOdate AS AMLCOdate,@CCO AS CCO,@CCOdate AS CCOdate,@GM AS GM,@GMdate AS GMdate 
	   FROM eddformid FID, eddformitem FITEM
       WHERE FID.FORMTYPE = @FormType
	   AND FID.FORMTYPE=FITEM.FORMTYPE AND FID.FORMID=FITEM.FORMID
	   ORDER BY FID.FORMID, FITEM.ITEMID

	Return


-----OP-INDIVIDUAL
insert into eddformid values ('OP-INDIVIDUAL','FORM 2.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- INDIVIDUALS',1,'FOR THE ACCOUNT HOLDER AND ALL SIGNATORIES:',getdate(),getdate())
insert into eddformid values ('OP-INDIVIDUAL','FORM 2.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- INDIVIDUALS',2,'IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('OP-INDIVIDUAL','FORM 2.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- INDIVIDUALS',3,'OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('OP-INDIVIDUAL','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2)',getdate(),getdate())
insert into eddformitem values  ('OP-INDIVIDUAL','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE CUSTOMER OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('OP-INDIVIDUAL','1','3','POWER OF ATTORNEY (FORM 14) – ONLY WHERE THERE ARE SIGNATORIES',getdate(),getdate())

insert into eddformitem values  ('OP-INDIVIDUAL','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 15.0)',getdate(),getdate())
insert into eddformitem values  ('OP-INDIVIDUAL','1','5','FACSIMILE INDEMNITY (FORM 17.0)',getdate(),getdate())
insert into eddformitem values  ('OP-INDIVIDUAL','1','6','FORM A',getdate(),getdate())
insert into eddformitem values  ('OP-INDIVIDUAL','1','7','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('OP-INDIVIDUAL','1','8','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('OP-INDIVIDUAL','1','9','PROOF OF ADDRESS',getdate(),getdate())
insert into eddformitem values  ('OP-INDIVIDUAL','1','10','SOURCE OF INCOME (FORM 30.1)',getdate(),getdate())
insert into eddformitem values  ('OP-INDIVIDUAL','1','11','PROOF OF SOURCE OF INCOME',getdate(),getdate())
insert into eddformitem values  ('OP-INDIVIDUAL','1','12','FATCA IDENTIFICATION FORM – INDIVIDUALS AND SOLE PROPRIETORS',getdate(),getdate())

insert into eddformitem values  ('OP-INDIVIDUAL','2','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('OP-INDIVIDUAL','2','2','IDENTITY DOCUMENT / PASSPORT ',getdate(),getdate())
insert into eddformitem values  ('OP-INDIVIDUAL','2','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('OP-INDIVIDUAL','3','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('OP-INDIVIDUAL','3','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem

-----OP-CC
insert into eddformid values ('OP-CC','FORM 3.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- CLOSE CORPORATIONS','1','FOR THE ACCOUNT HOLDER:',getdate(),getdate())
insert into eddformid values ('OP-CC','FORM 3.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- CLOSE CORPORATIONS','2','FOR EVERY MEMBER AND SIGNATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('OP-CC','FORM 3.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- CLOSE CORPORATIONS','3','IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('OP-CC','FORM 3.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- CLOSE CORPORATIONS','4','OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('OP-CC','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2)',getdate(),getdate())
insert into eddformitem values  ('OP-CC','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE A MEMBER OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('OP-CC','1','3','POWER OF ATTORNEY (FORM 14) / RESOLUTION (FORM 20.0)',getdate(),getdate())
insert into eddformitem values  ('OP-CC','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 16.0)',getdate(),getdate())
insert into eddformitem values  ('OP-CC','1','5','FACSIMILE INDEMNITY (FORM 17.0)',getdate(),getdate())
insert into eddformitem values  ('OP-CC','1','6','FORM B',getdate(),getdate())
insert into eddformitem values  ('OP-CC','1','7','PROOF OF BUSINESS ADDRESS',getdate(),getdate())
insert into eddformitem values  ('OP-CC','1','8','FOUNDING STATEMENT AND CERTIFICATE OF INCORPORATION (FORM CK1)',getdate(),getdate())
insert into eddformitem values  ('OP-CC','1','9','AMENDED FOUNDING STATEMENT (FORM CK2) – ONLY IF APPLICABLE',getdate(),getdate())
insert into eddformitem values  ('OP-CC','1','10','CIPC-COMPANIES & CLOSE CORPORATIONS-DISCLOSURE CERTIFICATE or CERTIFICATE of CONFIRMATION',getdate(),getdate())
insert into eddformitem values  ('OP-CC','1','11','SOURCE OF INCOME (FORM 30.1)',getdate(),getdate())
insert into eddformitem values  ('OP-CC','1','12','PROOF OF SOURCE OF INCOME',getdate(),getdate())
insert into eddformitem values  ('OP-CC','1','13','FATCA IDENTIFICATION FORM – CLOSE CORPORATIONS AND MEMBERS ',getdate(),getdate())

insert into eddformitem values  ('OP-CC','2','1','FORM A OR THE APPLICABLE FORM',getdate(),getdate())
insert into eddformitem values  ('OP-CC','2','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('OP-CC','2','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('OP-CC','2','4','PROOF OF ADDRESS',getdate(),getdate())


insert into eddformitem values  ('OP-CC','3','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('OP-CC','3','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('OP-CC','3','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('OP-CC','4','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('OP-CC','4','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem

-----OP-FOREIGN COM
insert into eddformid values ('OP-FOREIGN COM','FORM 4.1A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- FOREIGN COMPANIES','1','FOR THE ACCOUNT HOLDER:',getdate(),getdate())
insert into eddformid values ('OP-FOREIGN COM','FORM 4.1A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- FOREIGN COMPANIES','2','FOR EVERY SHAREHOLDER, DIRECTOR AND SIGNATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('OP-FOREIGN COM','FORM 4.1A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- FOREIGN COMPANIES','3','IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('OP-FOREIGN COM','FORM 4.1A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- FOREIGN COMPANIES','4','OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('OP-FOREIGN COM','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2)',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE A SHAREHOLDER OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','1','3','POWER OF ATTORNEY (FORM 14) / RESOLUTION (FORM 21.0)',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 16.0)',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','1','5','FACSIMILE INDEMNITY (FORM 17.0)',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','1','6','FORM B',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','1','7','PROOF OF BUSINESS ADDRESS IN SOUTH AFRICA',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','1','8','VOTING RIGHTS',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','1','9','OFFICIAL DOCUMENTATION ISSUED BY AN AUTHORITY FOR RECORDING THE INCORPORATION OF COMPANIES OF THE COUNTRY OF INCORPORATION OF THE FOREIGN COMPANY',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','1','10','PROOF OF BUSINESS NAME AND ADDRESS IN THE COUNTRY OF INCORPORATION',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','1','11','SOURCE OF INCOME (FORM 30.1)',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','1','12','PROOF OF SOURCE OF INCOME',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','1','13','FATCA IDENTIFICATION FORM – COMPANIES, DIRECTORS AND SHAREHOLDERS',getdate(),getdate())

insert into eddformitem values  ('OP-FOREIGN COM','2','1','FORM A – ADDENDUM 1 (FORM 31.1) OR THE APPLICABLE FORM',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','2','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','2','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','2','4','PROOF OF ADDRESS',getdate(),getdate())


insert into eddformitem values  ('OP-FOREIGN COM','3','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','3','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','3','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('OP-FOREIGN COM','4','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('OP-FOREIGN COM','4','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem

-----OP-PTY
insert into eddformid values ('OP-PTY','FORM 4.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- SOUTH AFRICAN COMPANIES','1','FOR THE ACCOUNT HOLDER:',getdate(),getdate())
insert into eddformid values ('OP-PTY','FORM 4.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- SOUTH AFRICAN COMPANIES','2','FOR EVERY SHAREHOLDER, DIRECTOR AND SIGNATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('OP-PTY','FORM 4.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- SOUTH AFRICAN COMPANIES','3','IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('OP-PTY','FORM 4.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- SOUTH AFRICAN COMPANIES','4','OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('OP-PTY','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2)',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE A SHAREHOLDER OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','3','POWER OF ATTORNEY (FORM 14) / RESOLUTION (FORM 21.0)',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 16.0)',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','5','FACSIMILE INDEMNITY (FORM 17.0)',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','6','FORM B',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','7','PROOF OF BUSINESS ADDRESS',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','8','VOTING RIGHTS',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','9','CERTIFICATE OF INCOPRORATION (FORM CM1) / REGISTRATION CERTIFICATE (FORM COR14.3)',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','10','NOTICE OF REGISTERED OFFICE AND POSTAL ADDRESS (FORM CM22) / NOTICE OF CHANGE OF REGISTERED ADDRESS (FORM COR21.1) – ONLY IF APPLICABLE',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','11','NOTICE OF CHANGE CONCERNING A DIRECTOR (FORM COR39) – ONLY IF APPLICABLE ',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','12','CIPC-COMPANIES & CLOSE CORPORATIONS- DISCLOSURE CERTIFICATE or CERTIFICATE of CONFIRMATION',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','13','SOURCE OF INCOME (FORM 30.1)',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','14','PROOF OF SOURCE OF INCOME',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','1','15','FATCA IDENTIFICATION FORM – COMPANIES, DIRECTORS AND SHAREHOLDERS',getdate(),getdate())

insert into eddformitem values  ('OP-PTY','2','1','FORM A OR THE APPLICABLE FORM',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','2','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','2','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','2','4','PROOF OF ADDRESS',getdate(),getdate())


insert into eddformitem values  ('OP-PTY','3','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','3','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','3','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('OP-PTY','4','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('OP-PTY','4','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem

-----OP-OTHER
insert into eddformid values ('OP-OTHER','FORM 5.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- OTHER LEGAL ENTITIES (E.G. ASSOCIATIONS)','1','FOR THE ACCOUNT HOLDER:',getdate(),getdate())
insert into eddformid values ('OP-OTHER','FORM 5.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- OTHER LEGAL ENTITIES (E.G. ASSOCIATIONS)','2','FOR EACH SIGNATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('OP-OTHER','FORM 5.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- OTHER LEGAL ENTITIES (E.G. ASSOCIATIONS)','3','IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('OP-OTHER','FORM 5.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- OTHER LEGAL ENTITIES (E.G. ASSOCIATIONS)','4','OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('OP-OTHER','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2)',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE A MEMBER OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','1','3','POWER OF ATTORNEY (FORM 14) / RESOLUTION (FORM 23.0)',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 16)',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','1','5','FACSIMILE INDEMNITY (FORM 17.0)',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','1','6','FORM E',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','1','7','PROOF OF BUSINESS ADDRESS',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','1','8','CONSTITUTION OR OTHER FOUNDING DOCUMENTATION',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','1','9','SOURCE OF INCOME (FORM 30.1)',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','1','10','PROOF OF SOURCE OF INCOME',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','1','11','FATCA IDENTIFICATION FORM – PARTNERSHIPS, PARTNERS AND OTHER LEGAL ENTITIES',getdate(),getdate())

insert into eddformitem values  ('OP-OTHER','2','1','FORM A',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','2','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','2','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','2','4','PROOF OF ADDRESS',getdate(),getdate())


insert into eddformitem values  ('OP-OTHER','3','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','3','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','3','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('OP-OTHER','4','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('OP-OTHER','4','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem
 
-----OP-PARTNER
insert into eddformid values ('OP-PARTNER','FORM 6.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- PARTNERSHIPS','1','FOR THE ACCOUNT HOLDER:',getdate(),getdate())
insert into eddformid values ('OP-PARTNER','FORM 6.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- PARTNERSHIPS','2','FOR EVERY SHAREHOLDER, DIRECTOR AND SIGNATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('OP-PARTNER','FORM 6.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- PARTNERSHIPS','3','IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('OP-PARTNER','FORM 6.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- PARTNERSHIPS','4','OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('OP-PARTNER','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2)',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE A SHAREHOLDER OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','1','3','POWER OF ATTORNEY (FORM 14) / RESOLUTION (FORM 23.0)',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 16.0)',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','1','5','FACSIMILE INDEMNITY (FORM 17.0)',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','1','6','FORM D',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','1','7','PROOF OF BUSINESS ADDRESS',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','1','8','PARTNERSHIP AGREEMENT / DECLARATION SIGNED BY ALL THE PARTNERS',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','1','9','SOURCE OF INCOME (FORM 30.1)',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','1','10','PROOF OF SOURCE OF INCOME',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','1','11','FATCA IDENTIFICATION FORM – COMPANIES, DIRECTORS AND SHAREHOLDERS',getdate(),getdate())

insert into eddformitem values  ('OP-PARTNER','2','1','FORM A',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','2','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','2','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','2','4','PROOF OF ADDRESS',getdate(),getdate())


insert into eddformitem values  ('OP-PARTNER','3','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','3','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','3','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('OP-PARTNER','4','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('OP-PARTNER','4','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem
 
-----OP-TRUST
insert into eddformid values ('OP-TRUST','FORM 7.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- TRUSTS','1','FOR THE ACCOUNT HOLDER:',getdate(),getdate())
insert into eddformid values ('OP-TRUST','FORM 7.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- TRUSTS','2','FOR THE FOUNDER, EVERY TRUSTEE, EVERY BENEFICIARY AND SIGNATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('OP-TRUST','FORM 7.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- TRUSTS','3','IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('OP-TRUST','FORM 7.0A - NOVEMBER 2015','ACCOUNT OPENING CHECKLIST- TRUSTS','4','OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('OP-TRUST','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2)',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE A SHAREHOLDER OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','1','3','POWER OF ATTORNEY (FORM 14) / RESOLUTION (FORM 22.0)',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 16)',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','1','5','FACSIMILE INDEMNITY (FORM 17.0)',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','1','6','FORM C',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','1','7','PROOF OF BUSINESS ADDRESS',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','1','8','TRUST DEED OR OTHER FOUNDING DOCUMENT',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','1','9','MAGTIGINGBRIEF LETTER OF AUTHORITY',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','1','10','SOURCE OF INCOME (FORM 30.1)',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','1','11','PROOF OF SOURCE OF INCOME',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','1','12','FATCA IDENTIFICATION FORM – TRUSTS, TRUSTEES AND BENEFICIARIES',getdate(),getdate())

insert into eddformitem values  ('OP-TRUST','2','1','FORM A OR THE APPLICABLE FORM',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','2','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','2','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','2','4','PROOF OF ADDRESS',getdate(),getdate())


insert into eddformitem values  ('OP-TRUST','3','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','3','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','3','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('OP-TRUST','4','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('OP-TRUST','4','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem
 
-----RE-INDIVIDUAL
insert into eddformid values ('RE-INDIVIDUAL','FORM 34.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- INDIVIDUALS','1','FOR THE ACCOUNT HOLDER AND ALL SIGNATORIES:',getdate(),getdate())
insert into eddformid values ('RE-INDIVIDUAL','FORM 34.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- INDIVIDUALS','2','FOR SIGNATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-INDIVIDUAL','FORM 34.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- INDIVIDUALS','3','IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-INDIVIDUAL','FORM 34.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- INDIVIDUALS','4','OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('RE-INDIVIDUAL','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2) AND BOTSA STATEMENT',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE CUSTOMER OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','1','3','POWER OF ATTORNEY (FORM 14) – ONLY WHERE THERE ARE SIGNATORIES   (  ) NEW     (  ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 15.0)                                                      (  ) NEW     (  ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','1','5','FACSIMILE INDEMNITY (FORM 17.0)                                                                                     (  ) NEW     (  ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','1','6','CSD FORM 8.A.0 FORM A: INDIVIDUAL REVERIFICATION FORM AND ANNEXURE',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','1','7','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','1','8','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','1','9','PROOF OF ADDRESS ',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','1','10','PROOF OF SOURCE OF INCOME',getdate(),getdate())

insert into eddformitem values  ('RE-INDIVIDUAL','2','1','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','2','2','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','2','3','PROOF OF ADDRESS',getdate(),getdate())


insert into eddformitem values  ('RE-INDIVIDUAL','3','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','3','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','3','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('RE-INDIVIDUAL','4','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('RE-INDIVIDUAL','4','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem
 
-----RE-CC
insert into eddformid values ('RE-CC','FORM 35.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- CLOSE CORPORATIONS','1','FOR THE ACCOUNT HOLDER:',getdate(),getdate())
insert into eddformid values ('RE-CC','FORM 35.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- CLOSE CORPORATIONS','2','FOR EVERY MEMBER AND SIGNATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-CC','FORM 35.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- CLOSE CORPORATIONS','3','IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-CC','FORM 35.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- CLOSE CORPORATIONS','4','OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('RE-CC','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2) AND BOTSA STATEMENT',getdate(),getdate())
insert into eddformitem values  ('RE-CC','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE CUSTOMER OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('RE-CC','1','3','POWER OF ATTORNEY (FORM 14) / RESOLUTION (FORM 20.0)              (  ) NEW     (  ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-CC','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 16.0)                                    (  ) NEW     (  ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-CC','1','5','FACSIMILE INDEMNITY (FORM 17.0)                                                                    (  ) NEW     (  ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-CC','1','6','CSD FORM 9.B.1 FORM B: CLOSE CORPORATION DATA AND ANNEXURE',getdate(),getdate())
insert into eddformitem values  ('RE-CC','1','7','PROOF OF BUSINESS ADDRESS',getdate(),getdate())
insert into eddformitem values  ('RE-CC','1','8','FOUNDING STATEMENT AND CERTIFICATE OF INCORPORATION (FORM CK1)',getdate(),getdate())
insert into eddformitem values  ('RE-CC','1','9','AMENDED FOUNDING STATEMENT (FORM CK2) – ONLY IF APPLICABLE',getdate(),getdate())
insert into eddformitem values  ('RE-CC','1','10','CIPC-COMPANIES & CLOSE CORPORATIONS- DISCLOSURE CERTIFICATE or CERTIFICATE of CONFIRMATION',getdate(),getdate())
insert into eddformitem values  ('RE-CC','1','11','PROOF OF SOURCE OF INCOME',getdate(),getdate())

insert into eddformitem values  ('RE-CC','2','1','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-CC','2','2','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('RE-CC','2','3','PROOF OF ADDRESS',getdate(),getdate())


insert into eddformitem values  ('RE-CC','3','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('RE-CC','3','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-CC','3','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('RE-CC','4','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('RE-CC','4','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem
 

 -----RE-PTY
insert into eddformid values ('RE-PTY','FORM 36.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- SOUTH AFRICAN COMPANIES','1','FOR THE ACCOUNT HOLDER:',getdate(),getdate())
insert into eddformid values ('RE-PTY','FORM 36.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- SOUTH AFRICAN COMPANIES','2','FOR EVERY SHAREHOLDER, DIRECTOR AND SIGNATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-PTY','FORM 36.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- SOUTH AFRICAN COMPANIES','3','IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-PTY','FORM 36.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- SOUTH AFRICAN COMPANIES','4','OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('RE-PTY','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2) AND BOTSA STATEMENT',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE A SHAREHOLDER OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','1','3','POWER OF ATTORNEY (FORM 14) / RESOLUTION (FORM 21.0)              (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 16.0)                                    (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','1','5','FACSIMILE INDEMNITY (FORM 17.0)                                                                    (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','1','6','CSD FORM 9.B.0 FORM B: COMPANY DATA AND ANNEXURE',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','1','7','PROOF OF BUSINESS ADDRESS',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','1','8','VOTING RIGHTS',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','1','9','CERTIFICATE OF INCOPRORATION (FORM CM1) / REGISTRATION CERTIFICATE (FORM COR14.3)',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','1','10','NOTICE OF REGISTERED OFFICE AND POSTAL ADDRESS (FORM CM22) / NOTICE OF CHANGE OF REGISTERED ADDRESS (FORM COR21.1) – ONLY IF APPLICABLE',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','1','11','NOTICE OF CHANGE CONCERNING A DIRECTOR (FORM COR39) – ONLY IF APPLICABLE ',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','1','12','CIPC-COMPANIES & CLOSE CORPORATIONS- DISCLOSURE CERTIFICATE or CERTIFICATE of CONFIRMATION',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','1','13','PROOF OF SOURCE OF INCOME',getdate(),getdate())

insert into eddformitem values  ('RE-PTY','2','1','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','2','2','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','2','3','PROOF OF ADDRESS',getdate(),getdate())


insert into eddformitem values  ('RE-PTY','3','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','3','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','3','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('RE-PTY','4','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('RE-PTY','4','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem
 
-----RE-FORGEIGN COM
insert into eddformid values ('RE-FOREIGN COM','FORM 37.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- FOREIGN COMPANIES','1','FOR THE ACCOUNT HOLDER:',getdate(),getdate())
insert into eddformid values ('RE-FOREIGN COM','FORM 37.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- FOREIGN COMPANIES','2','FOR EVERY SHAREHOLDER, DIRECTOR AND SIGNATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-FOREIGN COM','FORM 37.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- FOREIGN COMPANIES','3','IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-FOREIGN COM','FORM 37.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- FOREIGN COMPANIES','4','OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('RE-FOREIGN COM','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2) AND BOTSA STATEMENT',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE A SHAREHOLDER OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','1','3','POWER OF ATTORNEY (FORM 14) / RESOLUTION (FORM 21.0)               (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 16.0)                                     (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','1','5','FACSIMILE INDEMNITY (FORM 17.0)                                                                     (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','1','6','CSD FORM 9.B.0 FORM B: COMPANY DATA AND ANNEXURE',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','1','7','PROOF OF BUSINESS ADDRESS IN SOUTH AFRICA',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','1','8','VOTING RIGHTS',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','1','9','OFFICIAL DOCUMENTATION ISSUED BY AN AUTHORITY FOR RECORDING THE INCORPORATION OF COMPANIES OF THE COUNTRY OF INCORPORATION OF THE FOREIGN COMPANY',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','1','10','PROOF OF BUSINESS NAME AND ADDRESS IN THE COUNTRY OF INCORPORATION',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','1','11','PROOF OF SOURCE OF INCOME',getdate(),getdate())

insert into eddformitem values  ('RE-FOREIGN COM','2','1','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','2','2','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','2','3','PROOF OF ADDRESS',getdate(),getdate())


insert into eddformitem values  ('RE-FOREIGN COM','3','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','3','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','3','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('RE-FOREIGN COM','4','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('RE-FOREIGN COM','4','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem
 
-----RE-OTHER
insert into eddformid values ('RE-OTHER','FORM 38.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- OTHER LEGAL ENTITIES (E.G. ASSOCIATIONS)','1','FOR THE ACCOUNT HOLDER:',getdate(),getdate())
insert into eddformid values ('RE-OTHER','FORM 38.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- OTHER LEGAL ENTITIES (E.G. ASSOCIATIONS)','2','FOR EACH SIGNATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-OTHER','FORM 38.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- OTHER LEGAL ENTITIES (E.G. ASSOCIATIONS)','3','IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-OTHER','FORM 38.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- OTHER LEGAL ENTITIES (E.G. ASSOCIATIONS)','4','OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('RE-OTHER','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2) AND BOTSA STATEMENT',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE A SHAREHOLDER OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','1','3','POWER OF ATTORNEY (FORM 14) / RESOLUTION (FORM 23.0)                   (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 16.0)                                          (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','1','5','FACSIMILE INDEMNITY (FORM 17.0)                                                                         (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','1','6','CSD FORM 12.E.0 FORM E: OTHER LEGAL ENTITIES AND ANNEXURE',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','1','7','PROOF OF BUSINESS ADDRESS',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','1','8','CONSTITUTION OR OTHER FOUNDING DOCUMENTATION',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','1','9','PROOF OF SOURCE OF INCOME',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','1','10','PROOF OF BUSINESS NAME AND ADDRESS IN THE COUNTRY OF INCORPORATION',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','1','11','PROOF OF SOURCE OF INCOME',getdate(),getdate())

insert into eddformitem values  ('RE-OTHER','2','1','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','2','2','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','2','3','PROOF OF ADDRESS',getdate(),getdate())


insert into eddformitem values  ('RE-OTHER','3','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','3','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','3','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('RE-OTHER','4','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('RE-OTHER','4','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem
 

-----RE-PARTNER
insert into eddformid values ('RE-PARTNER','FORM 39.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- PARTNERSHIPS','1','FOR THE ACCOUNT HOLDER:',getdate(),getdate())
insert into eddformid values ('RE-PARTNER','FORM 39.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- PARTNERSHIPS','2','FOR EVERY SHAREHOLDER, DIRECTOR AND SIGNATORY::',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-PARTNER','FORM 39.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- PARTNERSHIPS','3','IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-PARTNER','FORM 39.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- OTHER LEGAL ENTITIES (E.G. ASSOCIATIONS)','4','OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('RE-PARTNER','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2) AND BOTSA STATEMENT',getdate(),getdate())
insert into eddformitem values  ('RE-PARTNER','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE A SHAREHOLDER OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('RE-PARTNER','1','3','POWER OF ATTORNEY (FORM 14) / RESOLUTION (FORM 23.0)                   (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-PARTNER','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 16.0)                                          (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-PARTNER','1','5','FACSIMILE INDEMNITY (FORM 17.0)                                                                         (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-PARTNER','1','6','CSD FORM 11.D.0 FORM D: PARTNERSHIPS AND ANNEXURE',getdate(),getdate())
insert into eddformitem values  ('RE-PARTNER','1','7','PROOF OF BUSINESS ADDRESS',getdate(),getdate())
insert into eddformitem values  ('RE-PARTNER','1','8','PARTNERSHIP AGREEMENT / DECLARATION SIGNED BY ALL THE PARTNERS',getdate(),getdate())
insert into eddformitem values  ('RE-PARTNER','1','9','PROOF OF SOURCE OF INCOME',getdate(),getdate())


insert into eddformitem values  ('RE-PARTNER','2','1','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-PARTNER','2','2','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('RE-PARTNER','2','3','PROOF OF ADDRESS',getdate(),getdate())


insert into eddformitem values  ('RE-PARTNER','3','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('RE-PARTNER','3','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-PARTNER','3','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('RE-PARTNER','4','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('RE-PARTNER','4','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem
 


-----RE-TRUST
insert into eddformid values ('RE-TRUST','FORM 40.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- TRUSTS','1','FOR THE ACCOUNT HOLDER:',getdate(),getdate())
insert into eddformid values ('RE-TRUST','FORM 40.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- TRUSTS','2','FOR THE FOUNDER, EVERY TRUSTEE, EVERY BENEFICIARY AND SIGNATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-TRUST','FORM 40.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- TRUSTS','3','IF THERE IS A DECLARATION OF RESIDENCE THEN FOR THE DECLARATORY:',getdate(),getdate())
INSERT INTO EDDFORMID VALUES ('RE-TRUST','FORM 40.1A - NOVEMBER 2015','ACCOUNT RE-VERIFICATION CHECKLIST- TRUSTS','4','OTHER',getdate(),getdate())
select * from eddformid

insert into eddformitem values ('RE-TRUST','1','1','CUSTOMER RISK ASSESSMENT (FORM 1.2) AND BOTSA STATEMENT',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','1','2','RISK ASSESSMENT PEPS (FORM 29.0) – ONLY WHERE A FOUNDER, TRUSTEE, BENEFICIARY OR SIGNATORY IS A PEP',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','1','3','POWER OF ATTORNEY (FORM 14) / RESOLUTION (FORM 22.0)                     (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','1','4','CERTIFICATE OF SIGNING AUTHORITY (FORM 16.0)                                           (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','1','5','FACSIMILE INDEMNITY (FORM 17.0)                                                                           (   ) NEW     (   ) EXISTING',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','1','6','CSD FORM 10.C.0 FORM C: TRUSTS AND ANNEXURE',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','1','7','PROOF OF BUSINESS ADDRESS',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','1','8','TRUST DEED OR OTHER FOUNDING DOCUMENT',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','1','9','MAGTIGINGBRIEF LETTER OF AUTHORITY',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','1','10','PROOF OF SOURCE OF INCOME',getdate(),getdate())

insert into eddformitem values  ('RE-TRUST','2','1','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','2','2','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','2','3','PROOF OF ADDRESS',getdate(),getdate())


insert into eddformitem values  ('RE-TRUST','3','1','DECLARATION OF RESIDENCE (FORM 18.0)',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','3','2','IDENTITY DOCUMENT / PASSPORT',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','3','3','IF THERE IS A PASSPORT THEN OBTAIN A VALID COPY OF THE VISA',getdate(),getdate())

insert into eddformitem values  ('RE-TRUST','4','1','SCREENING (PRIME COMPLIANCE AND THOMSON REUTERS)',getdate(),getdate())
insert into eddformitem values  ('RE-TRUST','4','2','(  ) F0306     (  ) F0300     (  ) F0104     (  ) F3013     (  ) F3014',getdate(),getdate())

select * from eddformitem
 
