USE PBSA
GO

delete CodeTbl where code = 'Closed'

delete CodeTbl where code = 'Close'
 
insert into CodeTbl(Code,name,ObjectType,CreateOper,CreateDate)
values('Close','Close','AcStatus','PRIMEADMIN',getdate())

insert into CodeTbl(Code,name,ObjectType,CreateOper,CreateDate)
values('Earmark','Earmark','AcStatus','PRIMEADMIN',getdate())

insert into CodeTbl(Code,name,ObjectType,CreateOper,CreateDate)
values('MoveOut','Move Out','AcStatus','PRIMEADMIN',getdate())

insert into CodeTbl(Code,name,ObjectType,CreateOper,CreateDate)
values('Rejected','Rejected','AcStatus','PRIMEADMIN',getdate())

insert into CodeTbl(Code,name,ObjectType,CreateOper,CreateDate)
values('SetMortgage','Set Mortgage','AcStatus','PRIMEADMIN',getdate())

insert into CodeTbl(Code,name,ObjectType,CreateOper,CreateDate)
values('Suspended1','Temporary Suspended1','AcStatus','PRIMEADMIN',getdate())