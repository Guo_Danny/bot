USE [PBSA]
GO
INSERT [dbo].[RiskFactorType] ([Code], [Name], [Description], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (N'BANK', N'BANK OF RISK MODEL', N'BANK OF RISK MODEL', N'ADMIN', getdate(), NULL, NULL)
INSERT [dbo].[RiskFactorType] ([Code], [Name], [Description], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (N'ENTITY', N'ENTITY OF RISK MODEL', N'ENTITY OF RISK MODEL', N'ADMIN', getdate(), N'Primeadmin', getdate())
INSERT [dbo].[RiskFactorType] ([Code], [Name], [Description], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (N'INDIVIDUAL', N'INDIVIDUAL OF RISK MODEL', N'INDIVIDUAL OF RISK MODEL', N'ADMIN', getdate(), N'Primeadmin', getdate())
--INSERT [dbo].[RiskFactorType] ([Code], [Name], [Description], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (N'NEWENTITY', N'NEW ENTITY OF RISK MODEL', N'NEW ENTITY OF RISK MODEL', N'Primeadmin', getdate(), NULL, NULL)
--INSERT [dbo].[RiskFactorType] ([Code], [Name], [Description], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES (N'NEWIND', N'NEW INDIVIDUAL OF RISK MODEL', N'NEW INDIVIDUAL OF RISK MODEL', N'Primeadmin', getdate(), NULL, NULL)
