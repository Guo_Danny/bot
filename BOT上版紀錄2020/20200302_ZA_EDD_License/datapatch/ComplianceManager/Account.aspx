<%@ Register TagPrefix="uc1" TagName="ucSupportingInfo" Src="ucSupportingInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucActivityList" Src="ucActivityList.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Account.aspx.vb" Inherits="CMBrowser.Account" ValidateRequest="false"%>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNote" Src="ucNote.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Account</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="Styles.css" type="text/css" rel="stylesheet"/>
		<script language="JavaScript" src="js/nav.js"></script>
		<style>.InVisibleCOlumn { VISIBILITY: hidden }
		</style>
		<script language="JavaScript" type="text/javascript">
		function tmpSource_onchange(ddl_id, txt_id)
		{
			var tsource = ddl_id;//document.getElementById(ddl_id);
			var txtdource = txt_id;//document.getElementById(txt_id);
			txtdource.value = tsource.value;
		}
		
		function ddlUD1_Load()
		{
			var tsource = document.getElementById('ddlUD1');
			var txtdource = document.getElementById('txtUserDefined1');
			tsource.value = txtdource.value;
		}
		
		function ddlSourceofFound_Load()
		{
			var tsource = document.getElementById('ddlSourceoffound');
			var txtdource = document.getElementById('txtSourceOfFunds');
			tsource.value = txtdource.value;
		}
		</script>
        <link href="Styles.css" rel="stylesheet" type="text/css" />
	</head>
	<body class="formbody" MS_POSITIONING="FlowLayout" onload="AddWindowToPCSWindowCookie(window.name);ddlSourceofFound_Load();" onunload="RemoveWindowFromPCSWindowCookie(window.name);">
		<form id="Form1" method="post" runat="server">
            <asp:ScriptManager id="smnAccount" runat="server" EnableScriptGlobalization="True"></asp:ScriptManager>
			<table id="PageLayout" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr>
								<td width="600"><asp:imagebutton id="imgPrime" runat="server" AlternateText="Home" ImageUrl="images/compliance-manager.gif"></asp:imagebutton></td>
								<td>&nbsp;</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordMessage" runat="server" width="808px" cssClass="RecordMessage">Record Message</asp:label></td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" Height="24px" Font-Size="X-Small" Width="616px"></asp:validationsummary></td>
				</tr>
				<tr>
					<td><asp:label id="lblAccountOwnerError" runat="server" Font-Size="X-Small" Width="336px" ForeColor="Red"
							Visible="False" EnableViewState="False">Error Message</asp:label></td>
				</tr>
				<tr>
					<td>
						<table id="Table2" style="WIDTH: 816px; HEIGHT: 40px" cellspacing="0" cellpadding="0" width="816"
							border="0">
							<tr align="center">
								<td align="right"><asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder></td>
								<td align="left"><uc1:ucrecordscrollbar id="RecordScrollBar" runat="server"></uc1:ucrecordscrollbar></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><uc1:uctabcontrol id="MainTabControl" runat="server"></uc1:uctabcontrol></td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordHeader" runat="server" width="804px" CSSClass="paneltitle"></asp:label></td>
				</tr>
			</table>
			<asp:panel id="Panel1" runat="server" Height="200px" Width="750px" Cssclass="Panel">
				<br />
				<table id="Table1" cellspacing="0" cellpadding="0" width="300" border="0">
					<tr>
						<td>
							<asp:label id="Label1" runat="server" CssClass="LABEL">Account No:</asp:label></td>
						<td>
							<asp:textbox id="txtCode" runat="server" CssClass="TEXTBOX" ToolTip="Account Number"></asp:textbox></td>
						<td>
							<asp:label id="Label2" runat="server" CssClass="LABEL">Account Name:</asp:label></td>
						<td>
							<asp:textbox id="txtName" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label3" runat="server" CssClass="LABEL">Branch:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlBranch" runat="server" Cssclass="DROPDOWNLIST"></asp:DropDownList></td>
						<td>
							<asp:label id="Label18" runat="server" CssClass="LABEL">Account Officer:</asp:label></td>
						<td>
							<asp:textbox id="txtAccountOfficer" runat="server" CssClass="TEXTBOX" ToolTip=""></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label4" runat="server" cssClass="LABEL">Type:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlAccountType" runat="server" Cssclass="DROPDOWNLIST"></asp:DropDownList></td>
						<td>
							<asp:label id="Label17" runat="server" CssClass="LABEL">Reference:</asp:label></td>
						<td>
							<asp:textbox id="txtReference" runat="server" CssClass="TEXTBOX" ToolTip=""></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label12" runat="server" CssClass="LABEL">Open/Closed:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOpenClosed" runat="server" Cssclass="DROPDOWNLIST"></asp:DropDownList></td>
						<td>
							<asp:label id="Label9" runat="server" CssClass="LABEL">Source of Founds:</asp:label><asp:textbox id="txtSourceOfFunds" runat="server" Width="0px"></asp:textbox></td>
						<td>
							<asp:DropDownList id="ddlSourceoffound" runat="server" CssClass="DropDownList" onchange="tmpSource_onchange(ddlSourceoffound,txtSourceOfFunds);">
								<asp:ListItem Text="" Value=""></asp:ListItem>
								<asp:ListItem Text="Maintenance" Value="Maintenance"></asp:ListItem>
								<asp:ListItem Text="Bursary" Value="Bursary"></asp:ListItem>
								<asp:ListItem Text="Allowance" Value="Allowance"></asp:ListItem>
								<asp:ListItem Text="Income from employment" Value="Income from employment"></asp:ListItem>
								<asp:ListItem Text="Company profits" Value="Company profits"></asp:ListItem>
								<asp:ListItem Text="Company sale or sale of interest in company" Value="Company sale or sale of interest in company" Selected="True"></asp:ListItem>
								<asp:ListItem Text="Court Order" Value="Court Order"></asp:ListItem>
								<asp:ListItem Text="Gift/donation" Value="Gift/donation"></asp:ListItem>
								<asp:ListItem Text="Loan" Value="Loan"></asp:ListItem>
								<asp:ListItem Text="Maturing investments" Value="Maturing investments"></asp:ListItem>
								<asp:ListItem Text="Pension" Value="Pension"></asp:ListItem>
								<asp:ListItem Text="Sale of shares" Value="Sale of shares"></asp:ListItem>
								<asp:ListItem Text="Divorce settlement" Value="Divorce settlement"></asp:ListItem>
								<asp:ListItem Text="Lottery/gambling winnings" Value="Lottery/gambling winnings"></asp:ListItem>
								<asp:ListItem Text="Dividends from investments" Value="Dividends from investments"></asp:ListItem>
								<asp:ListItem Text="Encashment claim" Value="Encashment claim"></asp:ListItem>
								<asp:ListItem Text="Inheritance" Value="Inheritance"></asp:ListItem>
								<asp:ListItem Text="Sale of property" Value="Sale of property"></asp:ListItem>
								<asp:ListItem Text="Deceased estates" Value="Deceased estates"></asp:ListItem>
								<asp:ListItem Text="Cryptocurrency" Value="Cryptocurrency"></asp:ListItem>
								<asp:ListItem Text="Rental income" Value="Rental income"></asp:ListItem>
								<asp:ListItem Text="Trust Distributions" Value="Trust Distributions"></asp:ListItem>
								<asp:ListItem Text="Other" Value="Other"></asp:ListItem>
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label15" runat="server" CssClass="LABEL">Open Date:</asp:label></td>
						<td>
							<asp:textbox id="txtOpenDate" runat="server" CssClass="TEXTBOX" ToolTip=""></asp:textbox></td>
						<td>
							<asp:label id="Label11" runat="server" Width="144px" CssClass="LABEL" ForeColor="Red">Account Balance:</asp:label></td>
						<td>
							<asp:textbox id="txtAccountBalance" runat="server" CssClass="TEXTBOX" ToolTip=""></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label16" runat="server" CssClass="LABEL">Close Date:</asp:label></td>
						<td>
							<asp:textbox id="txtCloseDate" runat="server" CssClass="TEXTBOX" ToolTip=""></asp:textbox></td>
						<td>
							<asp:label id="Label5" runat="server" CssClass="LABEL">Closed Reason:</asp:label></td>
						<td>
							<asp:textbox id="txtCloseReason" runat="server" CssClass="TEXTBOX" ToolTip=""></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label10" runat="server" CssClass="LABEL">Expiry Date:</asp:label></td>
						<td>
							<asp:textbox id="txtExpiryDate" runat="server" CssClass="TEXTBOX" ToolTip=""></asp:textbox></td>
						<td>
							<asp:label id="Label19" runat="server" CssClass="LABEL">Exemption Status:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlExemptionStatus" runat="server" Cssclass="DROPDOWNLIST"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label8" runat="server" CssClass="LABEL">Free Look End Date:</asp:label></td>
						<td>
							<asp:textbox id="txtFreeLookEndDate" runat="server" CssClass="TEXTBOX" ToolTip=""></asp:textbox></td>
						<td>
							<asp:label id="Label14" runat="server" CssClass="LABEL">Policy Value:</asp:label></td>
						<td>
							<asp:textbox id="txtPolicyValue" runat="server" CssClass="TEXTBOX" ToolTip=""></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label13" runat="server" CssClass="LABEL">Single Premium:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlSinglePremium" runat="server" Cssclass="DROPDOWNLIST"></asp:DropDownList></td>
						<td>
							<asp:label id="Label20" runat="server" CssClass="LABEL">Cash Value:</asp:label></td>
						<td>
							<asp:textbox id="txtCashValue" runat="server" CssClass="TEXTBOX" ToolTip=""></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label7" runat="server" CssClass="LABEL" ForeColor="Red">Account Status:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlAcctStatus" runat="server" Cssclass="DROPDOWNLIST"></asp:DropDownList></td>
						<td>
							<asp:label id="Label50" runat="server" CssClass="LABEL">Open Request:</asp:label></td>
						<td align="left">
							<asp:CheckBox id="chkRequest" runat="server" Width="80px" CssClass="CheckBox"></asp:CheckBox></td>
					</tr>
				</table>
				<br />
				<uc1:uctabcontrol id="RelationshipTab" runat="server"></uc1:uctabcontrol>
				<asp:panel id="Panel11" runat="server" Width="750px" Height="200px" Cssclass="Panel">
					<table id="tblUpdateOwners" runat="server">
						<tr>
							<td>
								<asp:label id="lblPartyId" runat="server" Width="90px" CssClass="LABEL">Party Id:</asp:label></td>
							<td>
								<asp:textbox id="txtPartyId" runat="server" Width="232px" CssClass="TEXTBOX" ToolTip=""></asp:textbox></td>
							<td>
								<asp:Button id="cmdAddOwner" runat="server" Text="Add"></asp:Button>&nbsp;
							</td>
						</tr>
						<tr>
							<td>
								<asp:label id="lblPriRelationship" runat="server" Width="80px" CssClass="LABEL">Relationship:</asp:label></td>
							<td>
								<asp:DropDownList id="ddlPriRelationship" runat="server" Width="232px" CssClass="DropDownList"></asp:DropDownList></td>
							<td></td>
						</tr>
					</table>
					<asp:DataGrid id="dgAccountOwners" runat="server" Width="808px" BackColor="White" cellpadding="4"
						AutoGenerateColumns="False" BorderColor="#CC9966" BorderWidth="1px" BorderStyle="None">
						<FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
						<SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
						<ItemStyle Font-Size="11px" ForeColor="#330099" BackColor="White"></ItemStyle>
						<HeaderStyle Font-Size="11px" Font-Bold="True" ForeColor="#FFFFCC" BackColor="#3366cc"></HeaderStyle>
						<Columns>
							<asp:TemplateColumn HeaderText="Id">
								<ItemTemplate>
									<a href='javascript:<%# 
								GetCustomerPopupScript(DataBinder.Eval(Container.DataItem, "PartyId"), _
								"Customer",  "scrollbars=yes,resizable=yes,width=880,height=600") %>'>
										<%#Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container.DataItem, "PartyId").ToString())%>
									</a>
									<input id="hdfldPartyId" runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.PartyId").ToString()) %>' name="hdfldPartyId"/>
									<input id="hdfldStatus" runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Status").ToString()) %>' name="hdfldStatus"/>
									<input id="hdfldRelCode" runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.RelationshipCode").ToString()) %>' name="hdfldRelCode"/>
									</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Name">
								<ItemTemplate>
        							<asp:label id="Name" runat="server" text='<%#Microsoft.Security.Application.Encoder.HtmlEncode(Databinder.Eval(Container, "DataItem.PartyName").ToString())%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Relationship">
								<ItemTemplate>
        							<asp:label id="Relationship" runat="server" text='<%#Microsoft.Security.Application.Encoder.HtmlEncode(Databinder.Eval(Container, "DataItem.Relationship").ToString())%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Risk Class">
								<ItemTemplate>
        							<asp:label id="RiskClass" runat="server" text='<%#Microsoft.Security.Application.Encoder.HtmlEncode(Databinder.Eval(Container, "DataItem.RiskClass").ToString())%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Type Of Business">
								<ItemTemplate>
        							<asp:label id="TypeofBusiness" runat="server" text='<%#Microsoft.Security.Application.Encoder.HtmlEncode(Databinder.Eval(Container, "DataItem.TypeofBusiness").ToString())%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:ButtonColumn HeaderText="Delete" Text="Del" CommandName="Delete"></asp:ButtonColumn>
						</Columns>
						<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
					</asp:DataGrid>
				</asp:panel>
				<asp:panel id="Panel12" runat="server" Width="750px" Height="200px" Cssclass="Panel">
					<table id="tblUpdateRelationships" runat="server">
						<tr>
							<td>
								<asp:label id="lblPartyId2" runat="server" Width="90px" CssClass="LABEL">Party Id:</asp:label></td>
							<td>
								<asp:textbox id="txtPartyId2" runat="server" Width="232px" CssClass="TEXTBOX" ToolTip=""></asp:textbox></td>
							<td>
								<asp:Button id="cmdAddRelationship" runat="server" Text="Add"></asp:Button>&nbsp;
							</td>
						</tr>
						<tr>
							<td>
								<asp:label id="lblRelationship" runat="server" Width="80px" CssClass="LABEL">Relationship:</asp:label></td>
							<td>
								<asp:DropDownList id="ddlRelationship" runat="server" Width="232px" CssClass="DropDownList"></asp:DropDownList></td>
							<td></td>
						</tr>
					</table>
					<asp:DataGrid id="dgAccountRelationships" runat="server" Width="808px" BackColor="White" cellpadding="4"
						AutoGenerateColumns="False" BorderColor="#CC9966" BorderWidth="1px" BorderStyle="None">
						<FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
						<SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
						<ItemStyle Font-Size="11px" ForeColor="#330099" BackColor="White"></ItemStyle>
						<HeaderStyle Font-Size="11px" Font-Bold="True" ForeColor="#FFFFCC" BackColor="#3366cc"></HeaderStyle>
						<Columns>
							<asp:TemplateColumn HeaderText="Id">
								<ItemTemplate>
									<a href='javascript:<%# 
								GetCustomerPopupScript(DataBinder.Eval(Container.DataItem, "PartyId"), _
								"Customer",  "scrollbars=yes,resizable=yes,width=880,height=600") %>'>
										<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container.DataItem, "PartyId").ToString())%>
									</a>
									<input id="Hidden2" runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.PartyId").ToString())%>' name="hdfldCustomerId"/>
									<input id="Hidden1" runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Status").ToString())%>' name="hdfldStatus"/>
									<input id="Hidden3" runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.RelationshipCode").ToString())%>' name="hdfldRelCode"/>
									</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Name">
								<ItemTemplate>
        							<asp:label ID="PartyName" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.PartyName").ToString())%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Relationship">
								<ItemTemplate>
        							<asp:label ID="Relationship" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Relationship").ToString())%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Risk Class">
								<ItemTemplate>
        							<asp:label ID="RiskClass" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.RiskClass").ToString())%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Type Of Business">
								<ItemTemplate>
        							<asp:label ID="TypeofBusiness" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.TypeofBusiness").ToString())%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:ButtonColumn HeaderText="Delete" Text="Del" CommandName="Delete"></asp:ButtonColumn>
						</Columns>
						<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
					</asp:DataGrid>
				</asp:panel>
			</asp:panel>
			<asp:panel id="Panel2" runat="server" Height="46px" Width="816px" cssclass="Panel">
				<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td>
							<asp:label id="Label41" runat="server" CssClass="Label" ForeColor="Red">Currency:</asp:label></td>
						<td>
							<asp:textbox id="txtUserDefined1" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label51" runat="server" CssClass="LABEL">Owner Branch:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerBranch" runat="server" Width="208px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label42" runat="server" CssClass="Label">User Defined 2:</asp:label></td>
						<td>
							<asp:textbox id="txtUserDefined2" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label52" runat="server" CssClass="LABEL">Owner Department:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerDept" runat="server" Width="208px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label43" runat="server" CssClass="Label">User Defined 3:</asp:label></td>
						<td style="height: 26px">
							<asp:textbox id="txtUserDefined3" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label53" runat="server" CssClass="LABEL">Owner Operator:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerOper" runat="server" Width="208px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label44" runat="server" CssClass="Label">User Defined 4:</asp:label></td>
						<td>
							<asp:textbox id="txtUserDefined4" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
                            <asp:label id="lblAuthorizedForRDC" runat="server" CssClass="LABEL">Authorized For RDC:</asp:label>                           
                         </td>
						<td>
                            <asp:CheckBox ID="chkRDC" Text="" CssClass="CHECKBOX" runat="server" />&nbsp;&nbsp;&nbsp;
                            <asp:label id="lblPowerOfAttorney" runat="server" CssClass="LABEL">Power of Attorney:</asp:label>
                            <asp:CheckBox ID="chkPOA" Text="" CssClass="CHECKBOXR" runat="server" />
                       </td>					
                    </tr>
					<tr>
						<td >
							<asp:label id="Label45" runat="server" CssClass="Label">User Defined 5:</asp:label></td>
						<td >
							<asp:textbox id="txtUserDefined5" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
					    <td >
							<asp:label ID="lblRDCRiskRating" runat="server" CssClass="LABEL">RDC Risk Rating:</asp:label></td>
						<td >
							<asp:textbox id="txtRDCRiskRating" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
				</table>

                <table>
        			<tr>
                        <td style="width: 130px; height: 41px" ><asp:label id="lblAccountPurpose" runat="server" CssClass="LABELML" Height="55px" Width="137px" ForeColor="Red" >Account Purpose:</asp:label></td>
                        <td style="width: 568px; height: 41px" align="left" ><asp:textbox id="txtAccountPurpose" runat="server" TextMode="MultiLine" Height="55px" Width="562px" ></asp:textbox></td>	
					</tr>
				</table>
			</asp:panel>
			<asp:panel id="Panel3" runat="server" Height="46px" Width="816px" cssclass="Panel">&nbsp; 
<uc1:ucActivityList id="ActivityList" runat="server"></uc1:ucActivityList></asp:panel><asp:panel id="Panel4" runat="server" Height="15px" Width="816px" cssclass="Panel">
				<uc1:ucSupportingInfo id="supportingInfo" runat="server"></uc1:ucSupportingInfo>
			</asp:panel><asp:panel id="Panel5" runat="server" Height="15px" Width="816px" cssclass="Panel">
				<uc1:ucNote id="AccountNotes" runat="server"></uc1:ucNote>
			</asp:panel>
			<asp:panel id="Panel6" Width="816" CssClass="Panel" runat="server">
				<table id="tblKYC" cellspacing="1" cellpadding="1" width="380" border="0">
					<tr>
						<td width="20%">
							<asp:label id="lblInitialDeposit" runat="server" CssClass="KYCLABEL">Initial Deposit:</asp:label></td>
						<td width="20%">
							<asp:TextBox id="txtInitialDeposit" CssClass="textbox" runat="server"></asp:TextBox></td>
						<td width="10%">&nbsp;</td>
						<td width="20%">
							<asp:label id="lblTypeOfFunds" runat="server" CssClass="KYCLABEL">Type Of Funds:</asp:label></td>
						<td width="20%">
							<asp:TextBox id="txtTypeOfFunds" CssClass="textbox" runat="server"></asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblAccACHPay" runat="server" CssClass="KYCLABEL">Accept ACH Payments:</asp:label></td>
						<td>
							<asp:CheckBox id="chkAccACHPay" CssClass="checkbox" runat="server"></asp:CheckBox></td>
						<td width="10%">&nbsp;</td>
						<td>
							<asp:label id="lblOrgACHPay" runat="server" CssClass="KYCLABEL">Originate ACH Payments:</asp:label></td>
						<td>
							<asp:CheckBox id="chkOrgACHPay" CssClass="checkbox" runat="server"></asp:CheckBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblNoOfDepositMnth" runat="server" CssClass="KYCLABEL">The anticipated number of deposits per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtNoOfDepositMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
						<td width="10%">&nbsp;</td>
						<td>
							<asp:label id="lblTotDepositAmtMnth" runat="server" CssClass="KYCLABEL">The anticipated total amount of deposits per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtTotDepositAmtMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblNoOfCashDepositMnth" runat="server" CssClass="KYCLABEL">The anticipated number of cash deposits per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtNoOfCashDepositMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
						<td width="10%">&nbsp;</td>
						<td>
							<asp:label id="lblTotCashDepositAmtMnth" runat="server" CssClass="KYCLABEL">The anticipated amount of cash deposits per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtTotCashDepositAmtMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblNoOfCashWithDMnth" runat="server" CssClass="KYCLABEL">The anticipated number of cash withdrawals per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtNoOfCashWithDMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
						<td width="10%">&nbsp;</td>
						<td>
							<asp:label id="lblTotCashWithDAmtMnth" runat="server" CssClass="KYCLABEL">The anticipated amount of cash withdrawals per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtTotCashWithDAmtMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblNoOfCheckWMnth" runat="server" CssClass="KYCLABEL">The anticipated number of checks written per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtNoOfCheckWMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
						<td width="10%">&nbsp;</td>
						<td>
							<asp:label id="lblTotCheckWAmtMnth" runat="server" CssClass="KYCLABEL">The anticipated amount of checks written per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtTotCheckWAmtMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblIsProspect" runat="server" CssClass="KYCLABEL">Prospect:</asp:label></td>
						<td>
							<asp:CheckBox id="chkIsProspect" CssClass="checkbox" runat="server"></asp:CheckBox></td>
						<td width="10%">&nbsp;</td>
						<td>
							<asp:label id="lblReqWireTrans" runat="server" CssClass="KYCLABEL">Require Wire Transfers:</asp:label></td>
						<td>
							<asp:CheckBox id="chkReqWireTrans" CssClass="checkbox" runat="server"></asp:CheckBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblNoOfDomInWireMnth" runat="server" CssClass="KYCLABEL">The anticipated number of domestic incoming wires per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtNoOfDomInWireMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
						<td width="10%">&nbsp;</td>
						<td>
							<asp:label id="lblTotDomInWireAmtMnth" runat="server" CssClass="KYCLABEL">The anticipated amount of domestic incoming wires per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtTotDomInWireAmtMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblNoOfDomOutWireMnth" runat="server" CssClass="KYCLABEL">The anticipated number of domestic outgoing wires per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtNoOfDomOutWireMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
						<td width="10%">&nbsp;</td>
						<td>
							<asp:label id="lblTotDomOutWireAmtMnth" runat="server" CssClass="KYCLABEL">The anticipated amount of domestic outgoing wires per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtTotDomOutWireAmtMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblReqForInWireTrans" runat="server" CssClass="KYCLABEL">Require Foreign Incoming Wire Transfers:</asp:label></td>
						<td>
							<asp:CheckBox id="chkReqForInWireTrans" CssClass="checkbox" runat="server"></asp:CheckBox></td>
						<td width="10%">&nbsp;</td>
						<td>
							<asp:label id="lblReqForOutWireTrans" runat="server" CssClass="KYCLABEL">Require Foreign Outgoing Wire Transfers:</asp:label></td>
						<td>
							<asp:CheckBox id="chkReqForOutWireTrans" CssClass="checkbox" runat="server"></asp:CheckBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblNoOfForInWireMnth" runat="server" CssClass="KYCLABEL">The anticipated number of foreign incoming wires per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtNoOfForInWireMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
						<td width="10%">&nbsp;</td>
						<td>
							<asp:label id="lblNoOfForOutWireMnth" runat="server" CssClass="KYCLABEL">The anticipated number of foreign outgoing wires per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtNoOfForOutWireMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblTotForInWireAmtMnth" runat="server" CssClass="KYCLABEL">The anticipated amount of foreign incoming wires per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtTotForInWireAmtMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
						<td width="10%">&nbsp;</td>
						<td>
							<asp:label id="lblTotForOutWireAmtMnth" runat="server" CssClass="KYCLABEL">The anticipated amount of foreign outgoing wires per month:</asp:label></td>
						<td>
							<asp:TextBox id="txtTotForOutWireAmtMnth" CssClass="textbox" runat="server"></asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblOrigCntryForInWires" runat="server" CssClass="KYCLABEL">The originating country or countries of incoming wires:</asp:label></td>
						<td>
							<asp:listbox id="lstOrigCntryForInWires" Width="208px" runat="server" SelectionMode="Multiple"
								Rows="3"></asp:listbox></td>
						<td width="10%">&nbsp;</td>
						<td>
							<asp:label id="lblDestCntryForOutWires" runat="server" CssClass="KYCLABEL">The destination country or countries of outgoing wires:</asp:label></td>
						<td>
							<asp:listbox id="lstDestCntryForOutWires" Width="208px" runat="server" SelectionMode="Multiple"
								Rows="3"></asp:listbox></td>
					</tr>
				</table>
			</asp:panel>
			<asp:panel id="cPanelValidators" runat="server" Height="46px" Width="864px"></asp:panel></form>
	</body>
</html>
