<%@ Register TagPrefix="uc1" TagName="ucComboBox" Src="ucComboBox.ascx" %>
<%@ Page Language="vb" validateRequest="false" AutoEventWireup="false" Codebehind="Activity.aspx.vb" Inherits="CMBrowser.Activity" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNote" Src="ucNote.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Activity</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="js/nav.js"></script>
	</HEAD>
	<body class="formbody"  onload="AddWindowToPCSWindowCookie(window.name);" onUnload="RemoveWindowFromPCSWindowCookie(window.name);">
		<form id="Form1" method="post" runat="server">
			<table id="PageLayout" cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td>
						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="600"><asp:imagebutton id="imgPrime" runat="server" ImageUrl="images/compliance-manager.gif" AlternateText="Home"
										width="808px"></asp:imagebutton></TD>
								<TD>&nbsp;</TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordMessage" runat="server" width="808px" cssClass="RecordMessage">Record Message</asp:label></td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" Width="616px" Font-Size="X-Small" Height="24px"></asp:validationsummary></td>
				</tr>
				<tr>
					<td>
						<TABLE id="Table2" style="WIDTH: 816px; HEIGHT: 40px" cellSpacing="0" cellPadding="0" width="816"
							border="0">
							<TR align="center">
								<TD align="right"><asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder></TD>
								<td align="left"><uc1:ucrecordscrollbar id="RecordScrollBar" runat="server"></uc1:ucrecordscrollbar></td>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td>
						<asp:panel id="pnlDeleteReason" runat="server" Width="850px" Cssclass="Panel" Height="40px">
<asp:label id="lblDeleteReason" runat="server" Width="110px" CssClass="LABEL">Enter reason:</asp:label>
<asp:textbox id="txtDeleteReason" runat="server" Width="464px" CssClass="TEXTBOX"></asp:textbox>&nbsp; 
<asp:Button id="cmdDeleteActivity" runat="server" Width="100px" Text="Delete"></asp:Button>&nbsp; 
<asp:Button id="cmdCancelDelete" runat="server" Width="100px" Text="Do Not Delete"></asp:Button>
						</asp:panel>
					</td>
				</tr>
				<tr>
					<td><uc1:uctabcontrol id="MainTabControl" runat="server"></uc1:uctabcontrol></td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordHeader" runat="server" width="804px" CSSClass="paneltitle"></asp:label></td>
				</tr>
			</table>
			<br>
			<asp:panel id="Panel1" runat="server" Width="750px" Height="200px" Cssclass="Panel">
				<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="300" border="0">
					<TR>
						<TD style="WIDTH: 88px">
							<asp:label id="Label20" runat="server" Width="110px" CssClass="LABEL">Transaction No:</asp:label></TD>
						<TD>
							<asp:textbox id="txtCode" runat="server" Width="190px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label1" runat="server" Width="110px" CssClass="LABEL">Branch:</asp:label></TD>
						<TD>
							<asp:DropDownList id="ddlBranch" runat="server" Width="140px" CssClass="DropDownList"></asp:DropDownList></TD>
						<TD>
							<asp:label id="Label2" runat="server" Width="110px" CssClass="LABEL">Department:</asp:label></TD>
						<TD>
							<asp:DropDownList id="ddlDepartment" runat="server" Width="140px" CssClass="DropDownList"></asp:DropDownList></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 88px">
							<asp:LinkButton id="lnkCustomerID" CssClass="Label" Width="110px" 
							Runat="server" CausesValidation="False">Customer Id:</asp:LinkButton><asp:label id="lblCustomerID" 
							runat="server" Width="110px" CssClass="LABEL" Visible="False">Customer Id:</asp:label></TD>
						<TD>
							<uc1:ucComboBox id="ddlCustomerId" runat="server"></uc1:ucComboBox></TD>
						<TD>
							<asp:label id="Label18" runat="server" Width="110px" CssClass="LABEL">Cust Name:</asp:label></TD>
						<TD colSpan="3">
							<asp:textbox id="txtCustomerName" runat="server" Width="392px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 88px">
							<asp:LinkButton id="lnkAccountId" CssClass="Label" Width="110px" 
							Runat="server" CausesValidation="False">Account Id:</asp:LinkButton><asp:label id="lblAccountId" 
							runat="server" Width="110px" CssClass="LABEL" Visible="False">Account Id:</asp:label></TD>
						<TD>
							<uc1:ucComboBox id="ddlAccountId" runat="server"></uc1:ucComboBox></TD>
						<TD>
							<asp:label id="Label5" runat="server" Width="110px" CssClass="LABEL">Type:</asp:label></TD>
						<TD>
							<asp:DropDownList id="ddlActivityType" runat="server" Width="140px" CssClass="DropDownList"></asp:DropDownList></TD>
						<TD>
							<asp:label id="Label6" runat="server" Width="110px" CssClass="LABEL">Recv/Pay:</asp:label></TD>
						<TD>
							<asp:DropDownList id="ddlDirection" runat="server" Width="140px" CssClass="DropDownList"></asp:DropDownList></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 88px; HEIGHT: 17px">
							<asp:label id="Label15" runat="server" Width="110px" CssClass="LABEL">Paymt Method:</asp:label></TD>
						<TD style="HEIGHT: 17px">
							<asp:DropDownList id="ddlPaymentMethod" runat="server" Width="190px" CssClass="DropDownList"></asp:DropDownList></TD>
						<TD style="HEIGHT: 17px">
							<asp:label id="Label7" runat="server" Width="110px" CssClass="LABEL">Book Date:</asp:label></TD>
						<TD style="HEIGHT: 17px">
							<asp:textbox id="txtBookDate" runat="server" Width="140px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD style="HEIGHT: 17px">
							<asp:label id="Label10" runat="server" Width="110px" CssClass="LABEL">Base Amount:</asp:label></TD>
						<TD style="HEIGHT: 17px">
							<asp:textbox id="txtBaseAmount" runat="server" Width="140px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 88px">
							<asp:label id="Label16" runat="server" Width="110px" CssClass="LABEL">Cash Tran:</asp:label></TD>
						<TD>
							<asp:DropDownList id="ddlCashTransaction" runat="server" Width="190px" CssClass="DropDownList"></asp:DropDownList></TD>
						<TD>
							<asp:label id="Label8" runat="server" Width="110px" CssClass="LABEL">Value Date:</asp:label></TD>
						<TD>
							<asp:textbox id="txtValueDate" runat="server" Width="140px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label11" runat="server" Width="110px" CssClass="LABEL">Currency:</asp:label></TD>
						<TD>
							<asp:textbox id="txtCurrency" runat="server" Width="140px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 88px">
							<asp:label id="Label13" runat="server" Width="110px" CssClass="LABEL">Tran Ref:</asp:label></TD>
						<TD>
							<asp:textbox id="txtTranRef" runat="server" Width="190px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label9" runat="server" Width="110px" CssClass="LABEL">Tran Date:</asp:label></TD>
						<TD>
							<asp:textbox id="txtTranDate" runat="server" Width="140px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label12" runat="server" Width="110px" CssClass="LABEL">FX Amount:</asp:label></TD>
						<TD>
							<asp:textbox id="txtFXAmount" runat="server" Width="140px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 88px" vAlign="top">
							<asp:label id="Label14" runat="server" Width="110px" CssClass="LABEL">Related Ref:</asp:label></TD>
						<TD vAlign="top">
							<asp:textbox id="txtRelatedRef" runat="server" Width="190px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD vAlign="top">
							<asp:label id="Label17" runat="server" Width="110px" CssClass="LABEL">Instructions:</asp:label></TD>
						<TD vAlign="top" colSpan="3">
							<asp:textbox id="txtInstructions" runat="server" Height="44px" Width="392px" CssClass="TEXTBOX"
								TextMode="MultiLine"></asp:textbox></TD>
					</TR>
				</TABLE>
				<BR>
				<TABLE>
					<TR>
						<TD style="WIDTH: 87px; HEIGHT: 14px" colSpan="4">
							<asp:label id="Label47" runat="server" width="804px" CSSClass="paneltitle">
						Party Information</asp:label></TD>
						</TR>
				</TABLE>
				<BR>
				<TABLE id="Table41" cellSpacing="0" cellPadding="0" width="300" border="0">
					<TR>
						<TD colSpan="2">
							<asp:label id="Label38" runat="server" Height="20px" Width="110px" CssClass="LABEL" Font-Underline="True">Originator</asp:label></TD>
						<TD colSpan="2">
							<asp:label id="Label39" runat="server" Height="20px" Width="110px" CssClass="LABEL" Font-Underline="True">Beneficiary</asp:label></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label29" runat="server" Width="110px" CssClass="LABEL">Name:</asp:label></TD>
						<TD>
							<asp:textbox id="txtOriginatorName" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label31" runat="server" Width="110px" CssClass="LABEL">Name:</asp:label></TD>
						<TD>
							<asp:textbox id="txtBeneName" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label32" runat="server" Width="110px" CssClass="LABEL">Account:</asp:label></TD>
						<TD>
							<asp:textbox id="txtOriginatorAccount" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label35" runat="server" Width="110px" CssClass="LABEL">Account:</asp:label></TD>
						<TD>
							<asp:textbox id="txtBeneAccount" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label33" runat="server" Width="110px" CssClass="LABEL">Customer Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtOriginatorCustomerId" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label36" runat="server" Width="110px" CssClass="LABEL">Customer Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtBeneCustomerId" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label34" runat="server" Width="110px" CssClass="LABEL">Address:</asp:label></TD>
						<TD>
							<asp:textbox id="txtOriginatorAddress" runat="server" Height="60px" Font-Size="12px" Width="291px"
								CssClass="TEXTBOX" TextMode="MultiLine"></asp:textbox></TD>
						<TD>
							<asp:label id="Label37" runat="server" Width="110px" CssClass="LABEL">Address:</asp:label></TD>
						<TD>
							<asp:textbox id="txtBeneAddress" runat="server" Height="60px" Font-Size="12px" Width="292px"
								CssClass="TEXTBOX" TextMode="MultiLine"></asp:textbox></TD>
					</TR>
					<TR>
						<TD colSpan="2">
							<asp:panel id="AddressPanel1" runat="server" cssclass="Panel">
								<TABLE id="Table05" cellSpacing="0" cellPadding="0" width="402" border="0">
									<TR>
										<TD>
											<asp:label id="Label30" runat="server" Width="110px" CssClass="LABEL">City:</asp:label></TD>
										<TD colSpan="3">
											<asp:textbox id="txtOriginatorCity" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label40" runat="server" Width="110px" CssClass="LABEL">State:</asp:label></TD>
										<TD>
											<asp:textbox id="txtOriginatorState" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
										<TD>
											<asp:label id="Label42" runat="server" Width="68px" CssClass="LABEL">Zip:</asp:label></TD>
										<TD>
											<asp:textbox id="txtOriginatorZip" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label41" runat="server" Width="110px" CssClass="LABEL">Country:</asp:label></TD>
										<TD colSpan="3">
											<asp:DropDownList id="ddlOriginatorCountry" runat="server" Width="292px" CssClass="DropDownList"></asp:DropDownList></TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
						<TD colSpan="2">
							<asp:panel id="AddressPanel2" runat="server" cssclass="Panel">
								<TABLE id="Table15" cellSpacing="0" cellPadding="0" width="402" border="0">
									<TR>
										<TD>
											<asp:label id="Label43" runat="server" Width="110px" CssClass="LABEL">City:</asp:label></TD>
										<TD colSpan="3">
											<asp:textbox id="txtBeneCity" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label44" runat="server" Width="110px" CssClass="LABEL">State:</asp:label></TD>
										<TD>
											<asp:textbox id="txtBeneState" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
										<TD>
											<asp:label id="Label46" runat="server" Width="68px" CssClass="LABEL">Zip:</asp:label></TD>
										<TD>
											<asp:textbox id="txtBeneZip" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label45" runat="server" Width="110px" CssClass="LABEL">Country:</asp:label></TD>
										<TD colSpan="3">
											<asp:DropDownList id="ddlBeneCountry" runat="server" Width="292px" CssClass="DropDownList"></asp:DropDownList></TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
					</TR>
				</TABLE>
				<BR>
				<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="300" border="0">
					<TR>
						<TD colSpan="2">
							<asp:label id="Label48" runat="server" Height="20px" Width="110px" CssClass="LABEL" Font-Underline="True">Originator Bank</asp:label></TD>
						<TD colSpan="2">
							<asp:label id="Label49" runat="server" Height="20px" Width="110px" CssClass="LABEL" Font-Underline="True">Beneficiary Bank</asp:label></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label66" runat="server" Width="110px" CssClass="LABEL">Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtOriginatorBankId" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label67" runat="server" Width="110px" CssClass="LABEL">Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtBeneBankId" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label50" runat="server" Width="110px" CssClass="LABEL">Name:</asp:label></TD>
						<TD>
							<asp:textbox id="txtOriginatorBankName" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label51" runat="server" Width="110px" CssClass="LABEL">Name:</asp:label></TD>
						<TD>
							<asp:textbox id="txtBeneBankName" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label52" runat="server" Width="110px" CssClass="LABEL">Account:</asp:label></TD>
						<TD>
							<asp:textbox id="txtOriginatorBankAccount" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label53" runat="server" Width="110px" CssClass="LABEL">Account:</asp:label></TD>
						<TD>
							<asp:textbox id="txtBeneBankAccount" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label54" runat="server" Width="110px" CssClass="LABEL">Customer Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtOriginatorBankCustomerId" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label55" runat="server" Width="110px" CssClass="LABEL">Customer Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtBeneBankCustomerId" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label56" runat="server" Width="110px" CssClass="LABEL">Address:</asp:label></TD>
						<TD>
							<asp:textbox id="txtOriginatorBankAddress" runat="server" Height="60px" Font-Size="12px" Width="291px"
								CssClass="TEXTBOX" TextMode="MultiLine"></asp:textbox></TD>
						<TD>
							<asp:label id="Label57" runat="server" Width="110px" CssClass="LABEL">Address:</asp:label></TD>
						<TD>
							<asp:textbox id="txtBeneBankAddress" runat="server" Height="60px" Width="292px" CssClass="TEXTBOX"
								TextMode="MultiLine"></asp:textbox></TD>
					</TR>
					<TR>
						<TD colSpan="2">
							<asp:panel id="AddressPanel3" runat="server" cssclass="Panel">
								<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="300" border="0">
									<TR>
										<TD>
											<asp:label id="Label58" runat="server" Width="110px" CssClass="LABEL">City:</asp:label></TD>
										<TD colSpan="3">
											<asp:textbox id="txtOriginatorBankCity" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label59" runat="server" Width="110px" CssClass="LABEL">State:</asp:label></TD>
										<TD>
											<asp:textbox id="txtOriginatorBankState" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
										<TD>
											<asp:label id="Label61" runat="server" Width="68px" CssClass="LABEL">Zip:</asp:label></TD>
										<TD>
											<asp:textbox id="txtOriginatorBankZip" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label60" runat="server" Width="110px" CssClass="LABEL">Country:</asp:label></TD>
										<TD colSpan="3">
											<asp:DropDownList id="ddlOriginatorBankCountry" runat="server" Width="292px" CssClass="DropDownList"></asp:DropDownList></TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
						<TD colSpan="2">
							<asp:panel id="AddressPanel4" runat="server" cssclass="Panel">
								<TABLE id="Table151" cellSpacing="0" cellPadding="0" width="402" border="0">
									<TR>
										<TD>
											<asp:label id="Label62" runat="server" Width="110px" CssClass="LABEL">City:</asp:label></TD>
										<TD colSpan="3">
											<asp:textbox id="txtBeneBankCity" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label63" runat="server" Width="110px" CssClass="LABEL">State:</asp:label></TD>
										<TD>
											<asp:textbox id="txtBeneBankState" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
										<TD>
											<asp:label id="Label65" runat="server" Width="68px" CssClass="LABEL">Zip:</asp:label></TD>
										<TD>
											<asp:textbox id="txtBeneBankZip" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label64" runat="server" Width="110px" CssClass="LABEL">Country:</asp:label></TD>
										<TD colSpan="3">
											<asp:DropDownList id="ddlBeneBankCountry" runat="server" Width="292px" CssClass="DropDownList"></asp:DropDownList></TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
					</TR>
				</TABLE>
				<BR>
				<TABLE id="Table145" cellSpacing="0" cellPadding="0" width="300" border="0">
					<TR>
						<TD colSpan="2">
							<asp:label id="Label68" runat="server" Height="20px" Width="110px" CssClass="LABEL" Font-Underline="True">Sender's Correspondent</asp:label></TD>
						<TD colSpan="2">
							<asp:label id="Label69" runat="server" Height="20px" Width="110px" CssClass="LABEL" Font-Underline="True">Receiver's Correspondent</asp:label></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label70" runat="server" Width="110px" CssClass="LABEL">Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediaryId" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label71" runat="server" Width="110px" CssClass="LABEL">Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary2Id" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label72" runat="server" Width="110px" CssClass="LABEL">Name:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediaryName" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label73" runat="server" Width="110px" CssClass="LABEL">Name:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary2Name" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label74" runat="server" Width="110px" CssClass="LABEL">Account:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediaryAccount" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label75" runat="server" Width="110px" CssClass="LABEL">Account:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary2Account" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label76" runat="server" Width="110px" CssClass="LABEL">Customer Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediaryCustomerId" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label77" runat="server" Width="110px" CssClass="LABEL">Customer Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary2CustomerId" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label78" runat="server" Width="110px" CssClass="LABEL">Address:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediaryAddress" runat="server" Height="60px" Width="291px" CssClass="TEXTBOX"
								TextMode="MultiLine"></asp:textbox></TD>
						<TD>
							<asp:label id="Label79" runat="server" Width="110px" CssClass="LABEL">Address:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary2Address" runat="server" Height="60px" Width="292px" CssClass="TEXTBOX"
								TextMode="MultiLine"></asp:textbox></TD>
					</TR>
					<TR>
						<TD colSpan="2">
							<asp:panel id="AddressPanel5" runat="server" cssclass="Panel">
								<TABLE id="Table8" cellSpacing="0" cellPadding="0" width="402" border="0">
									<TR>
										<TD>
											<asp:label id="Label80" runat="server" Width="110px" CssClass="LABEL">City:</asp:label></TD>
										<TD colSpan="3">
											<asp:textbox id="txtIntermediaryCity" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label81" runat="server" Width="110px" CssClass="LABEL">State:</asp:label></TD>
										<TD>
											<asp:textbox id="txtIntermediaryState" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
										<TD>
											<asp:label id="Label83" runat="server" Width="68px" CssClass="LABEL">Zip:</asp:label></TD>
										<TD>
											<asp:textbox id="txtIntermediaryZip" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label82" runat="server" Width="110px" CssClass="LABEL">Country:</asp:label></TD>
										<TD colSpan="3">
											<asp:DropDownList id="ddlIntermediaryCountry" runat="server" Width="292px" CssClass="DropDownList"></asp:DropDownList></TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
						<TD colSpan="2">
							<asp:panel id="AddressPanel6" runat="server" cssclass="Panel">
								<TABLE id="Table152" cellSpacing="0" cellPadding="0" width="402" border="0">
									<TR>
										<TD>
											<asp:label id="Label84" runat="server" Width="110px" CssClass="LABEL">City:</asp:label></TD>
										<TD colSpan="3">
											<asp:textbox id="txtIntermediary2City" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label85" runat="server" Width="110px" CssClass="LABEL">State:</asp:label></TD>
										<TD>
											<asp:textbox id="txtIntermediary2State" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
										<TD>
											<asp:label id="Label87" runat="server" Width="68px" CssClass="LABEL">Zip:</asp:label></TD>
										<TD>
											<asp:textbox id="txtIntermediary2Zip" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label86" runat="server" Width="110px" CssClass="LABEL">Country:</asp:label></TD>
										<TD colSpan="3">
											<asp:DropDownList id="ddlIntermediary2Country" runat="server" Width="292px" CssClass="DropDownList"></asp:DropDownList></TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
					</TR>
				</TABLE>
				<BR>
				<TABLE id="Table143" cellSpacing="0" cellPadding="0" width="300" border="0">
					<TR>
						<TD colSpan="2">
							<asp:label id="Label88" runat="server" Height="20px" Width="110px" CssClass="LABEL" Font-Underline="True">Intermediary Bank</asp:label></TD>
						<TD colSpan="2">
							<asp:label id="Label89" runat="server" Height="20px" Width="110px" CssClass="LABEL" Font-Underline="True">Account With Institution</asp:label></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label90" runat="server" Width="110px" CssClass="LABEL">Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary3Id" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label91" runat="server" Width="110px" CssClass="LABEL">Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary4Id" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label92" runat="server" Width="110px" CssClass="LABEL">Name:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary3Name" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label93" runat="server" Width="110px" CssClass="LABEL">Name:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary4Name" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label94" runat="server" Width="110px" CssClass="LABEL">Account:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary3Account" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label95" runat="server" Width="110px" CssClass="LABEL">Account:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary4Account" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label96" runat="server" Width="110px" CssClass="LABEL">Customer Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary3CustomerId" runat="server" Width="291px" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD>
							<asp:label id="Label97" runat="server" Width="110px" CssClass="LABEL">Customer Id:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary4CustomerId" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label98" runat="server" Width="110px" CssClass="LABEL">Address:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary3Address" runat="server" Height="60px" Width="291px" CssClass="TEXTBOX"
								TextMode="MultiLine"></asp:textbox></TD>
						<TD>
							<asp:label id="Label99" runat="server" Width="110px" CssClass="LABEL">Address:</asp:label></TD>
						<TD>
							<asp:textbox id="txtIntermediary4Address" runat="server" Height="60px" Width="292px" CssClass="TEXTBOX"
								TextMode="MultiLine"></asp:textbox></TD>
					</TR>
					<TR>
						<TD colSpan="2">
							<asp:panel id="AddressPanel7" runat="server" cssclass="Panel">
								<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="402" border="0">
									<TR>
										<TD>
											<asp:label id="Label100" runat="server" Width="110px" CssClass="LABEL">City:</asp:label></TD>
										<TD colSpan="3">
											<asp:textbox id="txtIntermediary3City" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label101" runat="server" Width="110px" CssClass="LABEL">State:</asp:label></TD>
										<TD>
											<asp:textbox id="txtIntermediary3State" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
										<TD>
											<asp:label id="Label103" runat="server" Width="68px" CssClass="LABEL">Zip:</asp:label></TD>
										<TD>
											<asp:textbox id="txtIntermediary3Zip" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label102" runat="server" Width="110px" CssClass="LABEL">Country:</asp:label></TD>
										<TD colSpan="3">
											<asp:DropDownList id="ddlIntermediary3Country" runat="server" Width="292px" CssClass="DropDownList"></asp:DropDownList></TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
						<TD colSpan="2">
							<asp:panel id="AddressPanel8" runat="server" cssclass="Panel">
								<TABLE id="Table150" cellSpacing="0" cellPadding="0" width="402" border="0">
									<TR>
										<TD>
											<asp:label id="Label104" runat="server" Width="110px" CssClass="LABEL">City:</asp:label></TD>
										<TD colSpan="3">
											<asp:textbox id="txtIntermediary4City" runat="server" Width="292px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label105" runat="server" Width="110px" CssClass="LABEL">State:</asp:label></TD>
										<TD>
											<asp:textbox id="txtIntermediary4State" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
										<TD>
											<asp:label id="Label107" runat="server" Width="68px" CssClass="LABEL">Zip:</asp:label></TD>
										<TD>
											<asp:textbox id="txtIntermediary4Zip" runat="server" Width="110px" CssClass="TEXTBOX"></asp:textbox></TD>
									</TR>
									<TR>
										<TD>
											<asp:label id="Label106" runat="server" Width="110px" CssClass="LABEL">Country:</asp:label></TD>
										<TD colSpan="3">
											<asp:DropDownList id="ddlIntermediary4Country" runat="server" Width="292px" CssClass="DropDownList"></asp:DropDownList></TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
					</TR>
				</TABLE>
				<INPUT id="hdfldOriginatorAddress" type="hidden" name="hdfldOriginatorAddress" Runat="server">
				<INPUT id="hdfldOriginatorBankAddress" type="hidden" name="hdfldOriginatorBankAddress"
					Runat="server"> <INPUT id="hdfldBeneAddress" type="hidden" name="hdfldBeneAddress" Runat="server">
				<INPUT id="hdfldBeneBankAddress" type="hidden" name="hdfldBeneBankAddress" Runat="server">
				<INPUT id="hdfldIntermediaryAddress" type="hidden" name="hdfldIntermediaryAddress" Runat="server">
				<INPUT id="hdfldIntermediary2Address" type="hidden" name="hdfldIntermediary2Address" Runat="server">
				<INPUT id="hdfldIntermediary3Address" type="hidden" name="hdfldIntermediary3Address" Runat="server">
				<INPUT id="hdfldIntermediary4Address" type="hidden" name="hdfldIntermediary4Address" Runat="server">
			</asp:panel><asp:panel id="Panel2" runat="server" Width="712px" Height="15px" cssclass="Panel">
				<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="300" border="0">
					<TR>
						<TD  align="right">
							<asp:label id="Label19" runat="server" CssClass="LABEL">SwiftNo-103(UserDefined1):</asp:label></TD>
						<TD>
							<asp:textbox id="txtUserDefined1" runat="server" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD  align="right">
							<asp:label id="Label21" runat="server" Width="180px" CssClass="LABEL" ForeColor="Red">Close Reason:</asp:label></TD>
						<TD>
							<asp:textbox id="txtInstrumentName" runat="server" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD align="right">
							<asp:label id="Label22" runat="server" CssClass="LABEL">SwiftNo-202(UserDefined2):</asp:label></TD>
						<TD>
							<asp:textbox id="txtUserDefined2" runat="server" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD align="right">
							<asp:label id="Label23" runat="server" Width="180px" CssClass="LABEL" ForeColor="Red">Due Date(Instrument Name):</asp:label></TD>
						<TD>
							<asp:textbox id="txtInstrumentQty" runat="server" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD align="right">
							<asp:label id="Label24" runat="server" CssClass="LABEL">SwiftNo-202C(UserDefined3):</asp:label></TD>
						<TD>
							<asp:textbox id="txtUserDefined3" runat="server" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD align="right">
							<asp:label id="Label25" runat="server" Width="180px" CssClass="LABEL">Instrument Type:</asp:label></TD>
						<TD>
							<asp:DropDownList id="ddlInstrumentType" runat="server" CssClass="DropDownList"></asp:DropDownList></TD>
					</TR>
					<TR>
						<TD align="right">
							<asp:label id="Label26" runat="server" CssClass="LABEL" ForeColor="Red">Bop Category(UserDefined4):</asp:label></TD>
						<TD>
							<asp:textbox id="txtUserDefined4" runat="server" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD align="right">
							<asp:label id="Label27" runat="server" Width="180px" CssClass="LABEL" ForeColor="Red">Tx.Balance(InstrumentPrice):</asp:label></TD>
						<TD>
							<asp:textbox id="txtInstrumentPrice" runat="server" CssClass="TEXTBOX"></asp:textbox></TD>
					</TR>
					<TR>
						<TD align="right">
							<asp:label id="Label28" runat="server" CssClass="LABEL" ForeColor="Red">SUBCAT(UserDefined5):</asp:label></TD>
						<TD>
							<asp:textbox id="txtUserDefined5" runat="server" CssClass="TEXTBOX"></asp:textbox></TD>
						<TD align="right">
							<asp:label id="Label108" runat="server" Width="180px" CssClass="LABEL">Exemption Status:</asp:label></TD>
						<TD>
							<asp:DropDownList id="ddlExemptionStatus" runat="server" CssClass="DropDownList"></asp:DropDownList></TD>
					</TR>
				</TABLE>
			</asp:panel><asp:panel id="Panel3" runat="server" Width="712px" Height="15px" cssclass="Panel">
			
			</asp:panel>
			<asp:Panel id="Panel4" runat="server" Width="812px" Height="15px" cssclass="Panel">
			<table id="audit" width="100%" cellSpacing="0" cellPadding="0" border="0">
			<tr>
			<td colspan="4" align="right"><asp:Button id="cmdAuditUpdate" runat="server" Width="100px" Text="Update"></asp:Button></td>
			</tr>
			<tr height="15px">
			<td colspan="4" align="right">&nbsp;</td>
			</tr>
			<TR>
				<TD>
					<asp:label id="Label4" runat="server" Width="180px" CssClass="LABEL">Audit Status:</asp:label></TD>
				<TD>
					<asp:DropDownList id="ddlaudstatus" runat="server" CssClass="DropDownList"></asp:DropDownList></TD>
				<TD>
					<asp:label id="Label3" runat="server" CssClass="LABEL">Audit Date:</asp:label></TD>
				<TD>
					<asp:textbox id="txtauditdate" runat="server" CssClass="TEXTBOX"></asp:textbox></TD>
			</TR>
			<TR>
				<TD>
					<asp:label id="Label109" runat="server" Width="180px" CssClass="LABEL">Completion Date:</asp:label></TD>
				<TD>
					<asp:textbox id="txtcompldate" runat="server" CssClass="TEXTBOX"></asp:textbox></TD>
				<TD>
					<asp:label id="Label110" runat="server" CssClass="LABEL">Operator:</asp:label></TD>
				<TD>
					<asp:textbox id="txtaudoper" runat="server" CssClass="TEXTBOX"></asp:textbox></TD>
			</TR>
			<tr height="15px">
			<td colspan="4" align="right">&nbsp;</td>
			</tr>
			<tr>
			<td colspan="4" align="right"><uc1:ucNote id="ucAuditNotes" runat="server"></uc1:ucNote></td>
			</tr>
			</table>
			<asp:panel id="pnlAuditValidators" Runat="server">								
								</asp:panel>
			</asp:Panel>
			<asp:panel id="Panel5" runat="server" Width="712px" Height="15px" cssclass="Panel"><uc1:ucNote id="ActivityNotes" runat="server"></uc1:ucNote></asp:panel><asp:panel id="cPanelValidators" runat="server" Width="864px" Height="46px">
				<asp:CustomValidator id="CVCCustomerCheck" runat="server" ErrorMessage="The customer id does not exist in the database"
					display="None" ControlToValidate=""></asp:CustomValidator>
				<asp:CustomValidator id="CVCIsActivityCurrent" runat="server" ErrorMessage="This activity has been archived and cannot be modified/deleted."
					display="None" ControlToValidate="txtCode" OnServerValidate="IsActivityCurrent"></asp:CustomValidator>
			</asp:panel></form>
	</body>
</HTML>
