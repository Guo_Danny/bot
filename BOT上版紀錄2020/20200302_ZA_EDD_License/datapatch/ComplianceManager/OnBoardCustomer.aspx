<%@ Register TagPrefix="uc1" TagName="ucSupportingInfo" Src="ucSupportingInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="OnBoardCustomer.aspx.vb" Inherits="CMBrowser.OnBoardCustomer" EnableViewState="True" ValidateRequest="False"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head id="Head1" runat="server">
    <title>OnBoard Customer</title>    
    <link href="Styles.css" type="text/css" rel="Stylesheet" />    
    <style type="text/css">
        .showstate { MARGIN-TOP: 2px; FLOAT: right; CURSOR: pointer; MARGIN-RIGHT: 3px }
	    .headers { }
	    .switchcontent { }
	    .hdrtable { BORDER-RIGHT: #6c0000 1px; BORDER-TOP: #6c0000 1px; BORDER-LEFT: #6c0000 1px; BORDER-BOTTOM: #6c0000 1px; BORDER-COLLAPSE: collapse }
	    .itmtable { BORDER-COLLAPSE: collapse }
	    .itemrow { BORDER-COLLAPSE: collapse }
	    .headerfont { FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: white; BACKGROUND-COLOR: #6c0000; TEXT-ALIGN: left }
	    .itemfont { FONT-SIZE: x-small }	 
	    .KYCDISABLEDLABEL
        {
	        padding-right: 6px;
	        font-weight: bold;
	        color: #808080;
	        font-size: 11px;
	        vertical-align: middle;
	        text-transform: capitalize;
	        width: 180px;
	        font-family: Verdana, Arial, Helvetica, sans-serif;
	        height: 8px;
	        text-align: right;
	        white-space:normal
        }          
        .KYCDROPDOWNLIST
        {	        
	        height: 24px;
	        text-align: left;
        }
	    .style1
        {
            height: 5px;
        }
		a.mybutton { 
			font-size: 0px; 
		} 
	</style>
	
	<script language="JavaScript" type="text/javascript" src="js/nav.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/jquery-3_5_1.js"></script>
	<script language="JavaScript" type="text/javascript">

    /***********************************************
    * Switch Content script II- �   (www.dynamicdrive.com)
    * This notice must stay intact for legal use. Last updated April 2nd, 2005.
    * Visit http://www.dynamicdrive.com/ for full source code
    ***********************************************/

        var enablepersist="off" //Enable saving state of content structure using session cookies? (on/off)
        var memoryduration="7" //persistence in # of days

        var contractsymbol='images/minus.gif' //Path to image to represent contract state.
        var expandsymbol='images/plus.gif' //Path to image to represent expand state.

        /////No need to edit beyond here //////////////////////////

        function getElementbyClass(rootobj, classname){
        var temparray=new Array()
        var inc=0
        var rootlength=rootobj.length
        for (i=0; i<rootlength; i++){
        if (rootobj[i].className==classname)
        temparray[inc++]=rootobj[i]
        }
        return temparray
        }

        function sweeptoggle(ec){
        var inc=0
        while (ccollect[inc]){
        ccollect[inc].style.display=(ec=="contract")? "none" : ""
        inc++
        }
        revivestatus()
        }

        function expandcontent(curobj, cid){
        if (ccollect.length>0){
        document.getElementById(cid).style.display=(document.getElementById(cid).style.display!="none")? "none" : ""
        curobj.src=(document.getElementById(cid).style.display=="none")? expandsymbol : contractsymbol
        }
        }

        function revivecontent(){
        selectedItem=getselectedItem()
        selectedComponents=selectedItem.split("|")
        for (i=0; i<selectedComponents.length-1; i++)
        document.getElementById(selectedComponents[i]).style.display="none"
        }

        function revivestatus(){
        var inc=0
        while (statecollect[inc]){
		
        if (ccollect[inc].style.display=="none")
        statecollect[inc].src=expandsymbol
        else
        statecollect[inc].src=contractsymbol
        inc++
        }
        }

        function get_cookie(Name) {
        var search = Name + "="
        var returnvalue = "";
        if (document.cookie.length > 0) {
        offset = document.cookie.indexOf(search)
        if (offset != -1) {
        offset += search.length
        end = document.cookie.indexOf(";", offset);
        if (end == -1) end = document.cookie.length;
        returnvalue=unescape(document.cookie.substring(offset, end))
        }
        }
        return returnvalue;
        }

        function getselectedItem(){
        if (get_cookie(window.location.pathname) != ""){
        selectedItem=get_cookie(window.location.pathname)
        return selectedItem
        }
        else
        return ""
        }

        function saveswitchstate(){
        var inc=0, selectedItem=""
        while (ccollect[inc]){
        if (ccollect[inc].style.display=="none")
        selectedItem+=ccollect[inc].id+"|"
        inc++
        }
        if (get_cookie(window.location.pathname)!=selectedItem){ //only update cookie if current states differ from cookie's
        var expireDate = new Date()
        expireDate.setDate(expireDate.getDate()+parseInt(memoryduration))
        document.cookie = window.location.pathname+"="+selectedItem+";path=/;expires=" + expireDate.toGMTString()
        }
        }

        function do_onload(){
        uniqueidn=window.location.pathname+"firsttimeload"
        var alltags=document.all? document.all : document.getElementsByTagName("*")
        ccollect=getElementbyClass(alltags, "switchcontent")
        statecollect=getElementbyClass(alltags, "showstate")
        if (enablepersist=="on" && get_cookie(window.location.pathname)!="" && ccollect.length>0)
        revivecontent()
        if (ccollect.length>0 && statecollect.length>0)
        revivestatus()
        }
		
        if (window.addEventListener)
        window.addEventListener("load", do_onload, false)
        else if (window.attachEvent)
        window.attachEvent("onload", do_onload)
        else if (document.getElementById)
        window.onload=do_onload

        if (enablepersist=="on" && document.getElementById)
        window.onunload=saveswitchstate

    </script>
	
	<script language="JavaScript" type="text/javascript">
		function document_onkeypress() 
		{
			try
			{
				if (event.keyCode == 13)	
				{									
					//Cancel the postback event from the Enter key
					window.event.returnValue=false;
				}				
			}
			catch(ex){}	
		}
		
	    function ch2txt(curobj){
		    newid = new String(curobj.id);
		    newid = newid.replace("chk","hdn");
		    document.getElementById(newid).value=curobj.checked;
		    }
		    function onIdChange(){
		    document.getElementById("o_hdn_id_1").value=document.getElementById("o_txt_id_1").value;
		}
		
		// Function to disable Next button for Pre Risk Form
		function DisableStepNext()
		{
		    var btnNext1 = document.getElementById("btnStepNext1");
		    var btnNext2 = document.getElementById("btnStepNext2");
		    var btnSave1 = document.getElementById("btnSave");
		    var btnSave2 = document.getElementById("btnSave1");
		    var txt = document.getElementById("txtPreScoreEntry");
		    var hdn = document.getElementById("hdnPreScoreEntry");
		    
		    btnNext1.disabled = true;
		    btnNext2.disabled = true;
		    btnSave1.disabled = true;
		    btnSave2.disabled = true;			  
		    
		    txt.value = "";
		    hdn.value = "";  
		}
		
		// Function to validate Customer Type DropDown
		function ValidateCustType()
		{
		    try
		    {
		        var ddlCustType = document.getElementById("ddlCustomerType");
		        var lblCustType = document.getElementById("lblCustTypeMsg");
		        
		        if (ddlCustType != null)
		        {
		            if (ddlCustType.disabled == false)
		            {
		                if (ddlCustType.value == '')
		                {
		                    lblCustType.style.display = "inline";
		                    ddlCustType.style.backgroundColor = "yellow";
		                    return false;
		                }    
		                else
		                {
		                    lblCustType.style.display = "none";
		                    ddlCustType.style.backgroundColor = "white";
		                    return true;
		                }    
		            }
		            else
		                return false;
		        }
		        else
		            return false;
		    }
		    catch (err)
		    {
		        return false;
		    }    
		    
		}
		
		// Function to handle Risk Class drop down selection change
		function RiskClassChange()
		{		
		    var ddl = document.getElementById("o_ddl_RiskClass");
		    var hdn = document.getElementById("o_txt_RiskClass");		    
		    hdn.value = ddl.value;		    		    
		}
		
		// 2020 06 ZA EDD Function check the Operator is not empty.
		function CheckSubmit()
		{
			
			var ddl = document.getElementById("ddlOperator");
			var txtnotes = document.getElementById("txtWFComments");
			if (ddl != null && ddl.value == '' && ddl.disabled == false)
			{
				alert('NOTE!! Operator can not be None, Please Check it again!!')
				return false;
			}
			else if (txtnotes != null && txtnotes.value == '')
			{
				alert('NOTE!! Remarks can not be Empty, Please Check it again!!')
				return false;
			}
			else
			{
				return true;
			}
		}
		function OpenBsaCase() {
			var qstring;
			var custid;
			custid = document.getElementById("o_txt_id_1").value;
			qstring = "https://10.1.10.48/showBsacase.asp?id=" + custid;
			window.open(qstring, '', 'scrollbars=yes,resizable=yes,width=880,height=600'); return false;
		}

		function ReviewRisk() {
			var qstring;
			var custid;
			custid = document.getElementById("o_txt_id_1").value;
			qstring = "RiskScoreReview.aspx?RecordID=" + custid + "&ActionID=2&ViewID=73&QPM=0&RowNum=0&ViewName=vwRiskScores&ArchiveMode=0&RecordsTotal=1";
			window.open(qstring, '', 'scrollbars=yes,resizable=yes,width=880,height=600'); return false;
		}
		
		function CustCheckList() {
			var qstring;
			var custid;
			var vListType;
			custid = document.getElementById("o_txt_id_1").value;
			
			if (document.getElementById("o_ddl_Comments6_1") === null)
			{
				vListType = document.getElementById("txt_o_ddl_Comments6_1").value;
			}
			else
			{
				vListType = document.getElementById("o_ddl_Comments6_1").value;
			}
			qstring = "CrystalReport.aspx?RecordID=CustChkList&ActionID=2&ViewID=87&ReportParam1=" + custid + "&ReportParam2=" + vListType + "";
			window.open(qstring, '', 'scrollbars=yes,resizable=yes,width=880,height=600'); return false;
		}
		
		function ChangeCommentStyle()
		{		
			var element = document.getElementById("o_txt_COMMENTS1_1");
			var parent = element.parentNode;
			parent.removeChild( element );
			parent.innerHTML = element.type == 'text' ? '<textarea id="' + element.id + '" cols=50 rows=5>' + element.value +'</textarea>' : '<input type="text" id="' + element.id + '"  value="' + element.value + '" />';
			

		}
		function sleep(delay) {
			var start = new Date().getTime();
			while (new Date().getTime() < start + delay);
		}
		function ReloadData()
		{
			document.getElementById("btnLnkRiskScore").click();
			alert('Please Re-Query this Customer Data!');
			sleep(1000);
			document.getElementById("cmdDone").click();
			
		}
		
	</script>
	
	<script language="JavaScript" type="text/javascript" event="onkeypress" for="document">
	<!--
		return document_onkeypress()
	//-->
	</script>
</head>
<body class="formbody" onload="AddWindowToPCSWindowCookie(window.name);" onUnload="RemoveWindowFromPCSWindowCookie(window.name);">
    <form id="form1" runat="server">
    <div>
        <table id="PageLayout" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td id="TaskBarLayout" valign="top">&nbsp;</td>
                <td id="FormLayout" valign="top">
                    <table id="OnBoardCustomer" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td style="width: 100%; background: images/bkg.jpg" colspan="2">
                                            <!-- Logo -->
                                            <asp:imagebutton id="imgPrime" runat="server" AlternateText="Home" ImageUrl="images/compliance-manager.gif" 
													width="804px">
											</asp:imagebutton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <!-- Status Message -->
                                            <asp:label id="clblRecordMessage" runat="server" width="100%" Visible="false" cssClass="RecordMessage">Record Message</asp:label>                                            
                                        </td>
                                    </tr>
                                    <tr>
										<td colspan="2">
										    <!-- Validation Summary -->
										    <asp:validationsummary id="cValidationSummary1" runat="server" Width="616px" Font-Size="X-Small" Height="24px"></asp:validationsummary>
										    
										    <!-- Client-Side Error Message -->						                    
						                    <asp:label id="lblCustTypeMsg" runat="server" Width="616px" Font-Size="X-Small" Height="24px" EnableViewState="false" ForeColor="Red" Text="<ul><li>Customer type is mandatory</li></ul>"></asp:label>
										</td>
									</tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
						   <td align="center" style="width: 100%">
						        <!-- Special Message -->
						        <asp:label id="lblMessage" runat="server"></asp:label><asp:label id="lblResult" 
                                    runat="server" EnableViewState="False"></asp:label>
						        <br />
							</td>
						</tr>
						<tr>
						   <td>
						      <!-- Tab -->
						      <uc1:uctabcontrol id="MainTabControl" runat="server"></uc1:uctabcontrol>
						   </td>
						</tr>
						<tr>
						    <td align="center" style="width: 100%">
						        <!-- Heading Panel -->
						        <asp:panel id="TabControlPad" runat="server" Width="100%" Height="10px" HorizontalAlign="Right"
										CSSClass="tabpad" BackColor="#3366CC">
									<asp:Label id="Label1" runat="server" CSSClass="paneltitle">OnBoard Customer</asp:Label>
								</asp:panel>
								
								<!-- Search Customer Panel -->
								<asp:panel id="Panel1" runat="server">
								    <table cellspacing="0" cellpadding="0" width="100%" border="0">
								        <tr class="TabSelected">
								            <td class="TabSelected" colspan="3">
								                <asp:Label id="lblTitlePanel1" runat="server" CSSClass="paneltitle">Specify Search Parameters</asp:Label>
								            </td>
								        </tr>
								        <tr>
										   <td align="center" style="height: 20px">&nbsp;</td>
										</tr>
										<tr>
										   <td class="Sectionheading" align="center">OnBoard New Customer</td>
										</tr>
										<tr>
										    <td align="left">
										        <table id="OnBoardNewCust" cellspacing="0" cellpadding="0" width="50%" border="0">
										            <tr>
													   <td colspan="2">&nbsp;</td>
													</tr>
													<tr>
													   <td align="right">
													   <asp:Label id="lblCustomerType" Width="144px" Runat="server" CssClass="Label"><span class="MandatoryStar">*</span>&nbsp;Customer Type</asp:Label>
													   </td>
													   <td>
													      <!-- Customer Type DropDown -->  
														  <asp:DropDownList id="ddlCustomerType" runat="server" CssClass="KYCDROPDOWNLIST" AppendDataBoundItems="True">
                                                              <asp:ListItem></asp:ListItem>
                                                          </asp:DropDownList>                                                          
													   </td>
													</tr>
													<tr>
													   <td colspan="2" style="height: 20px">&nbsp;</td>
													</tr>
													<tr>
													   <td align="center" colspan="2">
													       <!-- Next Button -->
														   <asp:Button id="btnNext" Runat="server" CssClass="cmd" Text="Next" CausesValidation="false" ToolTip="Next" OnClientClick="return ValidateCustType()"></asp:Button>
													   </td>
													</tr>
										        </table>
										    </td>
										</tr>
										<tr>
										   <!-- Seperator --> 
										   <td align="center"><hr /></td>
										</tr>
										<tr>
										   <td align="center" style="height: 10px">&nbsp;</td>
										</tr>
										<tr>
										   <td class="Sectionheading" align="center">OnBoard Existing Customer</td>
										</tr>
										<tr>
										   <td align="center" style="height: 20px">&nbsp;</td>
										</tr>
										<tr>
										    <td>
										        <asp:Label id="lblActivityMsg" Visible="False" Runat="server"></asp:Label>
												<asp:Label id="lblQuickSearchMsg" runat="server" CssClass="usererrorsummary"></asp:Label>
												<table>
												    <!-- Search Fields -->
												    <tr>
												        <td valign="top">
												            <asp:Table id="tblQuickSearch" runat="server"></asp:Table>
															<asp:Table id="tblAdditionalParameters" style="DISPLAY: none" runat="server"></asp:Table>
															<br />
															<asp:HyperLink id="lnkAdditionalParameters" runat="server" Visible="False" CssClass="lnktask">More parameters...</asp:HyperLink>												            
												        </td>
												        <td valign="top">
												            <asp:panel id="pnlListBy" runat="server" Visible="False" Width="150" BorderStyle="Outset">
																<asp:Table id="grdListBy" runat="server" Visible="True"></asp:Table>
															</asp:panel>
												        </td>												        
												    </tr>
												    <tr>
													   <td colspan="2" style="height: 20px">&nbsp;</td>
													</tr>
													<tr>
													   <td align="center" colspan="2">
													       <!-- Search Button -->
														   <asp:Button id="cmdQuickSearch" runat="server" CssClass="cmd" Text="Search" CausesValidation="False" ToolTip="Search"></asp:Button>
														   <input id="hfldDelView" type="hidden" name="hfldDelView" runat="server" />
														   <br />
													   </td>
													</tr>
												</table>
										    </td>
										</tr>
										<tr>
										   <td align="left">&nbsp;</td>
										</tr>
										<tr>
										   <!-- Seperator -->
										   <td align="center"><hr /></td>
										</tr>
								    </table>
								</asp:panel>
								
								<!-- Select Customer Panel -->
								<asp:panel id="Panel2" runat="server">
								    <table cellspacing="0" cellpadding="0" width="100%" border="0">
										<tr class="TabSelected">
											<td class="TabSelected"></td>
										</tr>
										<tr>
										   <td align="center">
											   <asp:Label id="lblListTitle" runat="server" CssClass="lblfld"></asp:Label>
											   <br />
											   <uc1:ucView id="ViewGrid" runat="server"></uc1:ucView>
											   <br />
											</td>
										</tr>
									</table>
								</asp:panel>
								
								<!--OnBoard Customer Panel -->
								<asp:panel id="Panel3" runat="server">
								
                                    <!-- Expand Contract Panel -->								
								    <asp:panel id="pnlExpandContract" Visible="False" Width="100%" Runat="server" BorderStyle="None"
											BorderWidth="1" cssclass="Panel">
										<table id="Tblsave" style="width: 100%; height: 40px" cellspacing="0" cellpadding="0" width="100%" border="0">
											<tr align="center">
											    <td style="width: 30%"></td>
												<td align="left" style="width: 70%">
												    <asp:button id="btnStepBack1" runat="server" CssClass="cmd" Text="&lt; Back" CausesValidation="false" CommandName="Back" OnCommand="btnStep_Command" ToolTip="Previous Step"></asp:button>&nbsp;
												    <asp:button id="btnStepNext1" runat="server" CssClass="cmd" Text="Next &gt;" CausesValidation="false" CommandName="Next" OnCommand="btnStep_Command" ToolTip="Next Step"></asp:button>&nbsp;
													<asp:button id="btnSave" runat="server" CssClass="cmd" Text="Save" CausesValidation="false" ToolTip="Save"></asp:button>&nbsp;
													<asp:button id="btnOnBoard" runat="server" CssClass="cmd" Text="OnBoard" CausesValidation="false" ToolTip="OnBoard"></asp:button>&nbsp;
    												<asp:button id="btnCancel" runat="server" CssClass="cmd" Text="Cancel" CausesValidation="False" ToolTip="Cancel"></asp:button>
										            <asp:button id="btnPrint" runat="server" CssClass="cmd" Text="Print" CausesValidation="false" ToolTip="Opens Print Preview Window" ></asp:button>&nbsp;
												</td>
											</tr>
										</table>	
										
										<!-- Expand Contract All Panel -->
										<asp:panel id="pnlExpConAll" Visible="False" Runat="server">
											<p style="text-align: right">
											    <a class="smalllabel" href="javascript:sweeptoggle('contract')">Contract 
														All Sections</a> | <a class="smalllabel" href="javascript:sweeptoggle('expand')">
														Expand All Sections</a> &nbsp;&nbsp;&nbsp;
											</p>
										</asp:panel>
										
										<br />
										
										<!-- Pre Risk Calculation (Step 1) -->
										
										<!-- Customer ID Fields (Step 2) -->
                                        <asp:Panel ID="pnlStep2" runat="server">
                                        <div id="header41" class="headers">
										    <table id="hdrTable41" class="hdrtable" border="0" style="border-width:1px;border-style:solid;width:100%;">
								                <tr style="background-color:Maroon;width:100%;">
									                <td align="left" style="width:50%;"><span id="lblHdrCustomerIdSec" class="headerfont">Customer ID Section</span></td><td align="right" style="width:45%;"></td><td align="right" style="width:5%;"><img id="imgMinCustIdSec" class="showstate" onclick="expandcontent(this,'sc41')" src="images/minus.gif" alt="ExpandContract" style="border: 0" /></td>
								                </tr>
							                </table>
							            </div>
										<div id="sc41" class="switchcontent">
							                <table id="swctable41" class="hdrtable" border="0" style="border-width:1px;border-style:solid;width:100%;">
											<tr>
											   <td align="right" style="width: 30%">											       
												   <asp:label id="lblCustomerID" runat="server" Width="75px" CssClass="LABEL"><span class="MandatoryStar">*&nbsp;</span>Id</asp:label>
											   </td>
											   <td style="width: 70%">
												   <asp:textbox id="o_txt_id_1" runat="server" CssClass="TEXTBOX"></asp:textbox><input id="o_hdn_id_1" type="hidden" name="o_hdn_id_1" runat="server" />
												   <asp:PlaceHolder id="phCustIdGen" runat="server"></asp:PlaceHolder>&nbsp; 
												   <input id="o_hdn_custts_1" type="hidden" name="o_hdn_custts_1" runat="server" />
											   </td>
											</tr>
											<tr>
											   <td align="right" style="width: 30%">											       
												   <asp:label id="Label2" runat="server" Width="75px" CssClass="LABEL"><span class="MandatoryStar">*&nbsp;</span>Customer Type</asp:label>
											  </td>
											  <td style="width: 70%">
												  <asp:DropDownList id="o_ddl_CustomerType" runat="server" CssClass="KYCDROPDOWNLIST"></asp:DropDownList><input id="o_txt_CustomerType" type="hidden" name="o_txtCustomerType" runat="server" />
											  </td>
											</tr>
											<tr>
											   <td align="right" style="width: 30%; height: 22px;">											       
												   <asp:label id="Label3" runat="server" Width="75px" CssClass="LABEL"><span class="MandatoryStar">*&nbsp;</span>Risk Class</asp:label>
											   </td>
											   <td style="width: 70%; height: 22px;">
											   
												     <asp:DropDownList id="o_ddl_RiskClass" runat="server" CssClass="KYCDROPDOWNLIST"></asp:DropDownList><input id="o_txt_RiskClass" type="hidden" name="o_txtRiskClass" runat="server" /> 
												   
											   </td>
											</tr>
											<tr>
											   <td align="right" style="width: 30%">
												   <asp:label id="lblComments" runat="server" Width="75px" CssClass="LABEL">Remarks</asp:label>
											   </td>
											   <td style="width: 70%">
												   <asp:textbox id="o_txt_Notes" runat="server" width="700px" Height="320px" 
                                                       TextMode="MultiLine" Wrap="True" ></asp:textbox>
											   </td>
											</tr>
										</table>
										</div>
										</asp:Panel>
											
									</asp:panel>
									
									<br />
									
									<!-- KYC Fields Panel -->
									<asp:panel id="pnlKYCFields" Visible="False" Width="100%" Runat="server" CssClass="panel" BorderStyle="none"
											BorderWidth="1">
											
										<!-- Item Section Panel -->	
										<asp:panel id="phrItemSection" runat="server" Visible="False">
										<!-- Other Steps Dynamically Loaded Here -->
										</asp:panel>
																				
										<!-- Workflow Panel -->
										<asp:panel ID="pnlWorkFlow" runat="server" Visible="False">
										    <div id="header1" class="headers">
										    <table id="hdrTable1" class="hdrtable" border="0" style="border-width:1px;border-style:solid;width:100%;">
								                <tr style="background-color:Maroon;width:100%;">
									                <td align="left" style="width:50%;"><span id="lblHdrOperator" class="headerfont">Approvals Section</span></td>
                                                    <td align="right" style="width:45%;" rowspan="1"></td><td align="right" style="width:5%;"><img id="imgMinOperator" class="showstate" onclick="expandcontent(this,'sc1')" src="images/minus.gif" alt="ExpandContract" style="border: 0" /></td>
								                </tr>
							                </table>
							                </div>
							                <div id="sc1" class="switchcontent">
							                <table id="swctable1" class="hdrtable" border="0" style="border-width:1px;border-style:solid;width:100%;">
											   <tr><td colspan="2" class="style1"></td></tr>
											   <tr class="itemfont">
											   <td align="right" style="width: 30%; height: 22px;">											       
												   <asp:label id="lblOperator" runat="server" Width="75px" CssClass="LABEL">Operator</asp:label>
											   </td>
											   <td style="width: 70%; height: 22px;">
												   <asp:DropDownList id="ddlOperator" runat="server" CssClass="KYCDROPDOWNLIST"></asp:DropDownList>
												   <a class="lnktask" OnClick="ReviewRisk();">*Mgr.Override</a> <a class="lnktask" OnClick="CustCheckList();">*CustomerCheckList</a>
											   </td>
											   </tr>
											   <tr class="itemfont">
											   <td align="right" style="width: 30%; height: 22px;">											       
												   <asp:label id="lblWFComments" runat="server" Width="75px" CssClass="LABEL" Text="Remarks"></asp:label>
											  </td>
											  <td style="width: 70%; height: 22px;">
												  <asp:textbox id="txtWFComments" runat="server" width="700px" Height="85px" 
                                                      TextMode="MultiLine" Wrap="True" ></asp:textbox>
											  </td>
											  </tr>	
											   <tr><td colspan="2" style="height: 5px"></td></tr>
											   <tr><td style="width:30%"></td><td  align="left" style="height:24px;width:70%" >
											            <asp:button id="btnApprove" runat="server" CssClass="cmd" Text="Approve" CausesValidation="false" ToolTip="Approve" OnClientClick="return CheckSubmit();"></asp:button>&nbsp;
											            <asp:button id="btnReject" runat="server" CssClass="cmd" Text="Reject" CausesValidation="false" ToolTip="Reject"></asp:button>&nbsp;
											            <asp:button id="btnWFOnBoard" runat="server" CssClass="cmd" Text="OnBoard" CausesValidation="false" Visible="False" ToolTip="OnBoard"></asp:button>
											       </td>
											   </tr>
											</table>
											</div>
											</asp:panel>
											
											<table id="Tblsave1" style="width: 100%; height: 40px" cellspacing="0" cellpadding="0"
												width="100%" border="0" >
												<tr align="center">
												    <td style="width:30%"></td>
													<td align="left" style="width: 70%; height: 40px;">
														<asp:button id="btnStepBack2" runat="server" CssClass="cmd" Text="&lt; Back" CausesValidation="false" CommandName="Back" OnCommand="btnStep_Command" ToolTip="Previous Step"></asp:button>&nbsp;
												        <asp:button id="btnStepNext2" runat="server" CssClass="cmd" Text="Next &gt;" CausesValidation="false" CommandName="Next" OnCommand="btnStep_Command" ToolTip="Next Step"></asp:button>&nbsp;
														<asp:button id="btnSave1" runat="server" CssClass="cmd" Text="Save" CausesValidation="false" ToolTip="Save"></asp:button>&nbsp;
														<asp:button id="btnOnBoard1" runat="server" CssClass="cmd" Text="OnBoard" CausesValidation="false" ToolTip="OnBoard"></asp:button>&nbsp;
														<asp:button id="btnCancel1" runat="server" CssClass="cmd" Text="Cancel" CausesValidation="False" ToolTip="Cancel"></asp:button>
											            <asp:button id="btnPrint1" runat="server" CssClass="cmd" Text="Print" CausesValidation="false" ToolTip="Opens Print Preview Window" ></asp:button>&nbsp;
													</td>
												</tr>
											</table>
											
									</asp:panel>
									
								</asp:panel>
								
								<!-- Wrap-up Panel -->
								<asp:panel id="Panel4" Runat="server">
								
								    <asp:Label id="lblMsg" Visible="False" Runat="server" CssClass="label"></asp:Label>
									<br />
									<br />
									
									<!-- KYC Services Panel -->
									<asp:panel id="pnlKYCServices" Visible="False" Runat="server">
									
									    <table id="Tbldone" style="width: 100%; height: 40px" cellspacing="0" cellpadding="0" width="100%"
												border="0">
												<tr align="center">
													<td align="center" style="width: 100%">
														<asp:button id="cmdDone" runat="server" CssClass="cmd" Text="Close" CausesValidation="false" ToolTip="Close"></asp:button>
														<input id="txtCode" type="hidden" name="txtCode" runat="server" />
													</td>
												</tr>
										</table>
										
										<table id="tblkycactions" style="width: 100%" cellspacing="0" cellpadding="0" width="100%"
												border="0" runat="server">
												<tr>
													<td class="Sectionheading" style="width: 100%">OnBoard Actions</td>
												</tr>
										</table>
									
									</asp:panel>
									
									<br />
									
									<!-- Risk Score Panel -->
									<asp:panel id="pnlRiskScore" Visible="False" Runat="server">
										<table class="itmtable" id="tblriskscore" style="width: 100%" border="0">
											<tr class="itemfont">
												<td class="Label" style="width: 30%">
												    <span class="MandatoryStar">*&nbsp;</span> Risk 
														Score
												</td>
												<td style="width: 70%">
													<asp:LinkButton id="btnLnkRiskScore" cssclass="mybutton" onclick="btnRiskScore_Click" Runat="server" CausesValidation="False" Width="0px"></asp:LinkButton>
													<a class="lnktask" OnClick="ReloadData();">Generate Risk Score</a>
												</td>
											</tr>
										</table>
									</asp:panel>
									
									<!-- Results Section - IDV, Risk and OFAC Case -->
									<table id="tblwrapup" style="width: 100%" cellspacing="0" cellpadding="0" width="100%"
											border="0" runat="server">
										<tr>
										   <td style="width: 100%; height: 20px">
											   <hr />
										   </td>
										</tr>										
										<tr>
										   <td class="Sectionheading" style="width: 100%">IDV Request History</td>
										</tr>
										<tr>
										   <td style="width: 100%; height: 20px">&nbsp;</td>
										</tr>
										<tr>
										   <td style="width: 100%">
											   <uc1:ucView id="vwRequestResults" runat="server" LinkBtnCauseValidation="false"></uc1:ucView>
										   </td>
										</tr>
										<tr>
										   <td style="width: 100%; height: 20px">
											   <hr />
										   </td>
										</tr>
										<tr>
										   <td class="Sectionheading" style="width: 100%">Risk Score</td>
										</tr>
										<tr>
										   <td style="width: 100%; height: 20px">&nbsp;</td>
										</tr>
										<tr>
										   <td style="width: 100%">
											   <uc1:ucView id="vwRiskScores" runat="server" LinkBtnCauseValidation="false"></uc1:ucView>
										   </td>
										</tr>
										<tr>
										   <td style="width: 100%; height: 20px">
											   <hr />
										   </td>
										</tr>
										<tr>
										   <td class="Sectionheading" style="width: 100%">OFAC Cases&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="lnktask" OnClick="OpenBsaCase();">*BSA Cases</a></td>
										</tr>
										<tr>
										   <td style="width: 100%; height: 20px"></td>
										</tr>
										<tr>
										   <td style="width: 100%">
											   <uc1:ucView id="vwOfacCases" runat="server" LinkBtnCauseValidation="false"></uc1:ucView>
										   </td>
										</tr>
									</table>
								
								</asp:panel>
								
								<!-- Supporting Info Panel -->
								<asp:panel id="Panel5" runat="server" Width="712px" Height="15px" cssclass="Panel">
										<uc1:ucSupportingInfo id="supportingInfo" runat="server" visible="true"></uc1:ucSupportingInfo>
								</asp:panel>
								
								<!-- Validator Panels -->
								<asp:panel id="cPanelValidators" runat="server" Width="100%" Height="46px">
								    <input id="checkedRows" type="hidden" name="checkedRows" runat="server" />
								</asp:panel>								
								<asp:panel id="pnlReqFieldValidators" Runat="server">								
								</asp:panel>								
						    </td>
						</tr>
                    </table><!-- OnBoard Customer Table End -->
                </td>
            </tr>
        </table><!-- Page Layout Table End -->
    </div>              
    </form>
</body>
</html>
