﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OverrideRiskScore.aspx.vb" Inherits="CMBrowser.OverrideRiskScore" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
		<title>Override Risk Class</title>
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="Styles.css" type="text/css" rel="stylesheet" />
        <base target="_self"/>
		<script language="JavaScript" src="js/nav.js"></script>
		<script language="JavaScript">
		    function ConfirmReject() {
		        var confirmed;
		        
				var reason = document.getElementById("txtReason");
		        if(reason.value == '')
		        {
		            alert('Please write your Reason!');
		            return false;
		        }
				confirmed = confirm("Are you sure you want to override risk class?");
				if(confirmed && reason.value != '')
				{
					closepopup();
					return true;
				}
				
		        return false;
		    }
		    function closepopup()
		    {
		       window.opener = self;
		        window.close();
		    }
		</script>
</head>
<body class="FORMBODY" MS_POSITIONING="FlowLayout" onload="AddWindowToPCSWindowCookie(window.name);" onUnload="RemoveWindowFromPCSWindowCookie(window.name);">
    <form id="form1" runat="server">
<table id="PageLayout" cellSpacing="0" cellPadding="0" border="0">

				<tr>
					<td>
						<table id="Table2" style="WIDTH: 600px; HEIGHT: 40px" cellSpacing="0" cellPadding="0" border="0">
							<tr align="center">
								<td align="center">
									<asp:button id="cmdSubmit" runat="server" Width="100px" Text="Submit" OnClientClick="return ConfirmReject();"></asp:button>&nbsp;
									<asp:button id="cmdClose" runat="server" Width="100px" Text="Close" OnClientClick="closepopup();return false"></asp:button>
								</td>								
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordHeader" runat="server" width="808px" CSSClass="paneltitle" Font-Size="Small"></asp:label></td>
				</tr>
			</table>
        				<table id="Table1" style="margin-top:10px" cellSpacing="5" cellPadding="5" border="0">
					<tr>
						<td style="HEIGHT: 16px">
							<asp:Label id="lblOption" runat="server" CssClass="LABEL">Risk Class:</asp:Label></td>
						<td style="HEIGHT: 16px">
							<asp:DropDownList id="ddlOption" runat="server" Width="264px" CssClass="DropDownList">
							</asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:Label id="lblRiskClass" runat="server" CssClass="LABEL">Reason:</asp:Label></td>
						<td>
							<asp:TextBox ID="txtReason" runat="server" Width="260px" Height="50px" TextMode="MultiLine" onkeypress="return this.value.length<=200"></asp:TextBox></td>
					</tr>
                            </table>
    </form>
</body>
</html>
