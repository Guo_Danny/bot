<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RiskClass.aspx.vb" Inherits="CMBrowser.RiskClass" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>RiskClass</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="js/nav.js"></script>

        <link href="Styles.css" rel="stylesheet" type="text/css" />
        <link href="Styles.css" rel="stylesheet" type="text/css" />
        
        <script language="JavaScript"  type="text/javascript" >
            function PromptDisableProfiling(chkEnableProfiling) 
            {
				try
				{
				    if (chkEnableProfiling.checked == false)
				    {
				        if (confirm("WARNING:  Activity will not be evaluated or \n" +
				        "accumulated for a profile while it is disabled. \n \n" +
				        "Do you wish to continue?") == false)
				            chkEnableProfiling.checked = true;
				            
				    }
				}
				catch(ex) {}	
		    }
		    
		    function LockPrompt(chkLock)
		    {
		        try
		        {
		            var vLockAction;
		            var vIsLocked;
		            var vRiskClass;
		            
		            if (document.getElementById("hfldLocked").value == "1")
		                vIsLocked = true;
		            else
		                vIsLocked = false;
		                
		           vRiskClass = document.getElementById("txtCode").value;		           
		           vLockAction= chkLock.parentNode.title;
		                
		            if (chkLock.checked)
		            {
		                if (vIsLocked == false)
		                {                  
		                    
		                     if (confirm(vLockAction + "\n\n" +
		                        "Do you want to lock '" + 
		                        vRiskClass + "' risk class?") == false)
		                    {
		                        chkLock.checked = false;   
		                    }
		                }
		                
		            }
		            else
		            {
		                if (vIsLocked)
		                {		                            
		                    if (confirm(vLockAction + "\n\n" + 
		                        "Do you want to unlock '" + 
		                        vRiskClass + "' risk class?") == false)
		                    {
		                        chkLock.checked = true;   
		                    }
		                }
		                
		            }
		        }
		        catch(ex) {}
		    }
			function DisplayTitle()
			{
				var code = document.getElementById("txtCode").value;
				document.getElementById("risklevel").innerHTML = "Risk Level : " + code;
			}
			
			function SourceLoad()
			{
				var txt1 = document.getElementById("txtAutoAcceptAmt");
				var ddl1 = document.getElementById("ddlAutoAcceptAmt");
				var txt2 = document.getElementById("txtAutoAcceptCnt");
				var ddl2 = document.getElementById("ddlAutoAcceptCnt");
				
				ddl1.value = txt1.value;
				ddl2.value = txt2.value;
			}
			
			function tmpSource_onchange(ddl_id, txt_id)
			{
				var tsource = ddl_id;
				var txtdource = txt_id;
				txtdource.value = tsource.value;
			}
        </script>
        
	</HEAD>
	<body class="formbody" onload="AddWindowToPCSWindowCookie(window.name);DisplayTitle();SourceLoad();" onUnload="RemoveWindowFromPCSWindowCookie(window.name);">
		<form id="Form1" method="post" runat="server">
			<table id="PageLayout" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr>
								<td><asp:imagebutton id="imgPrime" runat="server" width="808px" AlternateText="Home" ImageUrl="images/compliance-manager.gif"></asp:imagebutton></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordMessage" runat="server" width="808px" cssClass="RecordMessage">Record Message</asp:label></td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" Height="24px" Font-Size="X-Small" Width="616px"></asp:validationsummary></td>
				</tr>
				<tr>
					<td>
						<table id="Table2" style="WIDTH: 816px; HEIGHT: 40px" cellspacing="0" cellpadding="0" width="816"
							border="0">
							<tr align="center">
								<td align="right"><asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder></td>
								<td align="left"><uc1:ucrecordscrollbar id="RecordScrollBar" runat="server"></uc1:ucrecordscrollbar></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:panel id="pnlPrompt" Height="40px" Width="850px" Visible="False" Runat="server" CssClass="Panel">
					    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        <asp:Label id="lblPromptMsg" runat="server" Height="20px" Font-Bold="True">Label</asp:Label>&nbsp; 
                        <asp:Button id="cmdYes" runat="server" Width="48px" Text="YES"></asp:Button>&nbsp; 
                        <asp:Button id="cmdNo" runat="server" Width="48px" Text="NO"></asp:Button></asp:panel>
                     </td>
				</tr>
				<tr>
					<td><uc1:uctabcontrol id="MainTabControl" runat="server"></uc1:uctabcontrol></td>
				</tr>
				<tr>
					<td>
						<asp:label id="clblRecordHeader" runat="server" width="804px" CSSClass="paneltitle" Visible="False"></asp:label>
						<span id="Label_RiskLevel" class="paneltitle" style="display:inline-block;width:804px;"><div id="risklevel">RiskLevel</div></span>
					</td>
				</tr>
			</table>
			<asp:panel id="Panel1" runat="server" Height="200px" Width="804px" BorderStyle="None" BorderWidth="1px"
				BorderColor="Silver" Cssclass="Panel">
				<table id="Table1" cellspacing="0" cellpadding="0" width="804"	border="0">
					<tr style="HEIGHT: 8px">
						<td colspan="4"></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label1" Width="170px" runat="server" CssClass="LABEL">Code:</asp:label>
						</td>
						<td>
							<asp:textbox id="txtCode" Width="161px" runat="server" CssClass="TEXTBOX"></asp:textbox>
						</td>
						<td>
							<asp:label id="Label14" Width="170px" runat="server" CssClass="LABEL">Name:</asp:label></td>
						<td>
							<asp:textbox id="txtName" Width="293px" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label3" Width="170px" runat="server" CssClass="LABEL" ForeColor="Red">Review Freq.(Months)<br/> New Cust:</asp:label>
						</td>
						<td>
							<asp:textbox id="txtRiskFactor" Width="160px" runat="server" CssClass="TEXTBOX"></asp:textbox>
						</td>
						<td></td>
						<td>
							<asp:CheckBox id="chkEnableProfiling" Width="293px" runat="server" CssClass="CheckBox" Text="Enable Profiling"></asp:CheckBox>
						</td>
					</tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:CheckBox ID="chkLock" runat="server" CssClass="CheckBox" Text="Lock Risk Class"
                             OnClick="LockPrompt(this);"   Width="160px" />
                            <input id="hfldLocked" name="hfldLocked" value="" type="hidden" runat="server" />                             
                            <input id="hfldLockAction" name="hfldLockAction" value="" type="hidden" runat="server" />
                            <input id="hfldLockEnabled" name="hfldLockEnabled" value="0" type="hidden" runat="server" />                     
                        </td>
                        <td></td>
                        <td>
                            <asp:CheckBox ID="chkAutoProfGen" runat="server" CssClass="CheckBox" Text="Allow Profile Generation"
                                Width="293px" />
                        </td>
                    </tr>
					<tr>
						<td style="HEIGHT: 5px" colspan="4"></td>
					</tr>
					<tr>
						<td colspan="4">
							<asp:Label id="Label2" runat="server" width="804px" CSSClass="paneltitle">Risk Parameters</asp:Label></td>
					</tr>
					<tr style="HEIGHT: 8px">
						<td colspan="4"></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label6" Width="170px" runat="server" CssClass="LABEL">CTR Amount:</asp:label>
						</td>
						<td>
							<asp:textbox id="txtCTRAmount" Width="160px" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="WIDTH: 191px; HEIGHT: 14px"></td>
						<td style="HEIGHT: 14px">
						<asp:CheckBox id="chkRiskByAccount" runat="server" Width="293px" ForeColor="Red" Text="MLCO Approved" cssclass="CheckBox"></asp:CheckBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label7" Width="170px" runat="server" CssClass="LABEL">Recurrence Type:</asp:label>
						</td>
						<td>
							<asp:DropDownList id="ddlRecurrenceType" Width="161px" runat="server" cssclass="DropDownList" AutoPostBack="True"></asp:DropDownList>
						</td>
						<td></td>
						<td>
							<asp:CheckBox id="chkRiskByType" runat="server" Width="293px" Text="By Activity Type" cssclass="CheckBox"></asp:CheckBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label8" Width="170px" runat="server" CssClass="LABEL">Case Score:</asp:label>
						</td>
						<td>
							<asp:textbox id="txtCaseScore" Width="160px" runat="server" CssClass="TEXTBOX"></asp:textbox>
						</td>
						<td></td>
						<td>
							<asp:CheckBox id="chkGenCases" runat="server" Width="294px" Text="Generate Cases at End of Recurrence period"
								cssclass="CheckBox"></asp:CheckBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label16" Width="170px" runat="server" CssClass="LABEL" ForeColor="Red">Review Freq.(Months)<br/> Existing Cust:</asp:label>
						</td>
						<td>
							<asp:textbox id="txtProbationDays" Width="160px" runat="server" CssClass="TEXTBOX" MaxLength="3"></asp:textbox>
						</td>
						<td></td>
						<td>
							<asp:CheckBox id="chkAutoExpireProbation" runat="server" Width="293px" Text="Remove Probation Automatically"
								cssclass="CheckBox"></asp:CheckBox>
					    </td>
					</tr>
					<tr>	
					    <td></td>					
						<td>
							<asp:label id="Label19" Width="170px" runat="server" Font-Size="13px" Font-Bold="True" Font-Underline="True">Count:</asp:label>
						</td>
						<td></td>
						<td>
							<asp:label id="Label20" runat="server" Font-Size="13px" Font-Bold="True" Font-Underline="True">Amount:</asp:label>
						</td>						
					</tr>
					<tr>
						<td>
							<asp:label id="Label4" Width="170px" runat="server" CssClass="Label"> Tolerance (%):</asp:label></td>
						<td>
							<asp:textbox id="txtCntTolPerc" Width="160px" runat="server" CssClass="TEXTBOX"></asp:textbox>
						</td>
						<td>
							
						</td>
						<td colspan="2">
							
						</td>
						<td></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label12" Width="170px" runat="server" CssClass="Label">Retrigger Tolerance (%):</asp:label>
						</td>
						<td>
							<asp:textbox id="txtCntRetriggerTolPerc" Width="160px" runat="server" CssClass="TEXTBOX"></asp:textbox>
						</td>
						<td>
							<asp:label id="Label13" Width="170px" runat="server" CssClass="Label">Retrigger Tolerance (%):</asp:label>
						</td>
						<td>
							<asp:textbox id="txtAmtRetriggerTolPerc" runat="server" Width="293px" CssClass="TEXTBOX"></asp:textbox>
						</td>						
					</tr>
					<tr>
						<td>
							<asp:label id="Label21" Width="170px" runat="server" CssClass="Label">Minimum Variance:</asp:label>
						</td>
						<td>
							<asp:textbox id="txtCntMinVariance" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox>
						</td>
						<td>
							<asp:label id="Label15" Width="170px" runat="server" CssClass="Label">Minimum Variance:</asp:label>
						</td>
						<td>
							<asp:textbox id="txtAmtMinVariance" runat="server" Width="293px" CssClass="TEXTBOX"></asp:textbox>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label17" Width="170px" runat="server" CssClass="Label">Generate</asp:label></td>
						<td>
							<asp:DropDownList id="ddlProfileType" runat="server" Width="161px" cssclass="DropDownList" AutoPostBack="True"></asp:DropDownList>
						</td>
						<td align="center">
					        <asp:label id="Label18" Width="170px" runat="server" Font-Size="11px" Font-Bold="True">using calculation type</asp:label>
						</td>
						<td>
							<asp:DropDownList id="ddlCalculationType" runat="server" Width="294px" cssclass="DropDownList"></asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td style="HEIGHT: 14px" colspan="4"></td>
					</tr>
				</table>
				<table width="804" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td colspan="4">
							<asp:Label id="Label22" runat="server" width="804px" CSSClass="paneltitle">Automated Profile Configuration</asp:Label></td>
					</tr>
					<tr>
						<td colspan="4"></td>
					</tr>
					<tr>						
						<td align="right">
							<asp:label width="160px" id="Label29" runat="server" Font-Size="11px" 
							Font-Bold="True">Generate Profiles on a </asp:label>&nbsp;</td>
						<td>
							<asp:DropDownList width="161px" 
							id="ddlAutoAcceptRecurrence" runat="server" cssclass="DropDownList"></asp:DropDownList>
						</td>
						<td>
						    &nbsp;<asp:label id="Label25" 
							width="170px" runat="server" Font-Size="11px" Font-Bold="True">basis</asp:label>
						</td>
						<td></td>
						
					</tr>
					<tr>
						<td align="right">
							<asp:label id="Label26" Width="160px" runat="server" Font-Size="11px" Font-Bold="True">Process</asp:label>&nbsp;</td>
						<td colspan="3" align="left">
							<asp:DropDownList id="ddlProcessActivityType" runat="server" Width="161px" cssclass="DropDownList"
								AutoPostBack="True"></asp:DropDownList>&nbsp;<asp:label id="lblLastPeriods" 
								Width="80px" runat="server" Font-Size="11px" 
								Font-Bold="True">with the last</asp:label>&nbsp;<asp:textbox id="txtAutoAcceptPeriod" 
								runat="server" Width="80px" CssClass="TEXTBOX"></asp:textbox>&nbsp;<asp:label id="lblProcessActivityPeriod"
								Width="250px" runat="server" Font-Size="11px" Font-Bold="True">monthly recurrence periods</asp:label>
						</td>
					</tr>
					<tr>
					    <td></td>
						<td colspan="3">
							<asp:CheckBox id="chkAutoAccept" runat="server" Width="200px" Text="Automatically accept profiles"
								cssclass="CheckBox"></asp:CheckBox></td>
					</tr>
				    <tr>
						<td>
							<asp:label id="Label23" width="170px" runat="server" CssClass="LABEL">Auto Accept Amount (%):</asp:label>
						</td>
						<td>
							<asp:textbox id="txtAutoAcceptAmt" width="160px" runat="server" CssClass="TEXTBOX"></asp:textbox>
						</td>
						<td>
							<asp:label id="Label24" width="170px" runat="server" CssClass="LABEL">Auto Accept Count (%):</asp:label>
						</td>
						<td>
							<asp:textbox id="txtAutoAcceptCnt" runat="server" Width="296px" CssClass="TEXTBOX"></asp:textbox>
						</td>
						
					</tr>
				</table>
				<table id="table3" style="HEIGHT: 64px" cellspacing="0" cellpadding="0" width="804"
					border="0">
					<tr>
						<td colspan="4">
							<asp:Label id="Label9" runat="server" width="804px" CSSClass="paneltitle">Risk Score Range</asp:Label></TD>
					</tr>
					<tr>						
						<td>
							<asp:label id="Label10" width="170px" runat="server" CssClass="LABEL">Individual Low:</asp:label>
						</td>
						<td>
							<asp:textbox id="txtRiskScoreLow"  width="160px" runat="server" CssClass="TEXTBOX"></asp:textbox>
						</td>
						<td>
							<asp:label id="Label11" runat="server" width="170px" CssClass="LABEL" ForeColor="Red">Individual High:</asp:label>
						</td>
						<td>
							<asp:textbox id="txtRiskScoreHigh" runat="server" Width="296px" CssClass="TEXTBOX"></asp:textbox>
						</td>								
					</tr>
					<tr>
						<td>
							</td>
						<td>
							
						</td>
						<td>
							<asp:label id="Label5" Width="170px" runat="server" CssClass="Label" ForeColor="Red">Entity High:</asp:label>
						</td>
						<td>
							<asp:textbox id="txtAmtTolPerc" runat="server" Width="293px" CssClass="TEXTBOX"></asp:textbox>
						</td>
					</tr>
				</table>
			</asp:panel>
			                <asp:Panel ID="Panel2" runat="server" CssClass="Panel" Height="250px" Width="784px">
                    <table id="Table8" border="0" cellpadding="0" cellspacing="0" style="width: 560px;
                        height: 132px" width="560">
                        <tr>
                            <td>
                                <p>
                                    <asp:CheckBox ID="chkOvrCountAndAmount" runat="server" CssClass="CheckBox" Text="Update count and amount from activity during dynamic profile generation" /></p>
                                <p>
                                    <asp:Label ID="Label31" runat="server" Font-Names="Verdana, Arial, Helvetica, sans-serif"
                                        Font-Size="12px">
									Update profile values from risk class during dynamic profile generation for:</asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrGenCasesEndOfRecurrance" runat="server" CssClass="CheckBox"
                                    Text="Generate cases at the end of recurrence period" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrTolerances" runat="server" CssClass="CheckBox" Text="Tolerances percentage" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrMinAmtandCountVariance" runat="server" CssClass="CheckBox"
                                    Text="Minimum amount and count variance" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrRetriggerTolerances" runat="server" CssClass="CheckBox" Text="Retrigger tolerances percentage" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrProfileType" runat="server" CssClass="CheckBox" Text="ProfileType" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrAutoAccept" runat="server" CssClass="CheckBox" Text="Auto Accept" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrCalculationType" runat="server" CssClass="CheckBox" Text="Calculation Type" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrAutoAcceptThresholds" runat="server" CssClass="CheckBox"
                                    Text="Auto Accept Amount/Count Thresholds" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrRecurType" runat="server" CssClass="CheckBox" Text="Recurrence Type/Value" /></td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 20px">
                                <asp:CheckBox ID="chkOvrAutoGenFreq" runat="server" CssClass="CheckBox" Text="Auto Generation Frequency" /></td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 20px">
                                <asp:CheckBox ID="chkOvrProcessActivity" runat="server" CssClass="CheckBox" Text="Activity to consider for Auto Generation" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrEnableProfiling" runat="server" CssClass="CheckBox" Text="Enable Profiling" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrGenOptions" runat="server" CssClass="CheckBox" Text="Generation Options" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Button ID="cmdPropagate" runat="server" Text="Apply to Profiles now" /></td>
                        </tr>
                    </table>
                </asp:Panel>
			<asp:panel id="Panel3" runat="server" Height="46px" Width="712px" cssclass="Panel">&nbsp;</asp:panel>
			<asp:panel id="Panel4" runat="server" Height="15px" Width="712px" cssclass="Panel"></asp:panel><asp:panel id="cPanelValidators" runat="server" Height="46px" Width="864px"></asp:panel></form>
	</body>
</HTML>
