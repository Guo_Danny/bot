<%@ Register TagPrefix="uc1" Tagname="ucPageheader" Src="ucPageheader.ascx" %>
<%@ Register TagPrefix="uc1" Tagname="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" Tagname="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RiskScoreReview.aspx.vb" Inherits="CMBrowser.RiskScoreReview" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd html 4.0 transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Risk Score</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="Styles.css" type="text/css" rel="stylesheet" />
		<style type="text/css">.tgridheader { BORDER-RIGHT: white outset; PADDING-RIGHT: 2px; BORDER-TOP: white outset; PADDING-LEFT: 2px; FONT-WEIGHT: bold; FONT-SIZE: x-small; PADDING-BOTTOM: 2px; BORDER-LEFT: white outset; COLOR: white; PADDING-TOP: 2px; BORDER-BOTTOM: white outset; BACKGROUND-COLOR: #990000; TEXT-ALIGN: left }
	.lblgridcell { FONT-SIZE: 12px; TEXT-ALIGN: right; Font-names: Verdana, Arial, Helvetica, sans-serif }
		</style>
		<script language="JavaScript" src="js/nav.js" type="text/javascript"  ></script>
		<script language="JavaScript" type="text/javascript">
			function ChangeName()
			{
				
				document.getElementById("cmdDelete").value = 'Reject';
				document.getElementById("cmdDelete").onclick = function()
				{
					//alert('Do you want to Reject this Risk Score?');
					if(confirm('Are you sure you want to Reject this Record?'))
					{
					}
					else
					{
						return false;
					}
				}
			}
		</script>
	</head>
	<body class="formbody" onload="AddWindowToPCSWindowCookie(window.name);ChangeName();" onUnload="RemoveWindowFromPCSWindowCookie(window.name);" >
		<form id="Form1" method="post" runat="server">
			<table id="PageLayout" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr>
								<td  style="width:600px" ><asp:imagebutton id="imgPrime" runat="server" width="808px" ImageUrl="images/compliance-manager.gif"
										AlternateText="Home"></asp:imagebutton></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordMessage" runat="server" width="808px" cssClass="RecordMessage">Record Message</asp:label></td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" Height="24px" Font-Size="X-Small" Width="616px"></asp:validationsummary></td>
				</tr>
				<tr>
					<td>
						<table id="table2" style="WIDTH: 808px; HEIGHT: 40px" cellspacing="0" cellpadding="0" width="808"
							border="0">
							<tr align="center">
								<td align="right"><asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder></td>
								<td align="left"><uc1:ucrecordscrollbar id="RecordScrollBar" runat="server"></uc1:ucrecordscrollbar></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><uc1:uctabcontrol id="MainTabControl" runat="server"></uc1:uctabcontrol></td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordheader" runat="server" width="808px" CSSClass="paneltitle"></asp:label></td>
				</tr>
			</table>
			<br />
			<asp:panel id="Panel1" runat="server" Height="176px" Width="808px" BorderStyle="None" BorderWidth="1px"
				Cssclass="Panel">
				<table id="table41" cellspacing="0" cellpadding="0" width="808px" border="0">
					<tr>
						<td>
							<asp:linkButton id="lnkCustomerId" width="140px" runat="server" CausesValidation="False" CssClass="Label">Customer Id:</asp:linkButton></td>
						<td>
							<asp:textbox id="txtCode" width="244px" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label1" width="160px" runat="server" CssClass="LABEL">Customer name:</asp:label></td>
						<td>
							<asp:textbox id="txtCustomername" width="264px" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:linkButton id="lnkRiskModel" width="140px" runat="server" CausesValidation="False" CssClass="Label">Risk Model:</asp:linkButton></td>
						<td>
							<asp:textbox id="txtriskModel" width="244px" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:linkButton id="lnkCurrentriskClass" width="160px" runat="server" CausesValidation="False" CssClass="Label">Current Risk Class:</asp:linkButton></td>
						<td>
							<asp:textbox id="txtCurrentriskClass"  width="264px" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
				</table>
				<br />
				<asp:DataGrid id="dgScoreDetails" runat="server" Width="808px" BorderWidth="1px" BorderStyle="None"
					BackColor="White" cellpadding="4" AutoGenerateColumns="False" BorderColor="#CC9966">
					<FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
					<SelectedItemStyle Font-Bold="true" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
					<ItemStyle Font-Size="11px" ForeColor="#330099" BackColor="White"></ItemStyle>
					<headerStyle Font-Size="11px" Font-Bold="true" ForeColor="White" BackColor="#990000"></headerStyle>
					<Columns>
						<asp:BoundColumn DataField="RiskItem" headerText="Item">
							<headerStyle Width="150px"></headerStyle>
						</asp:BoundColumn>
						<asp:TemplateColumn headerText="Req">
							<headerStyle Width="25px"></headerStyle>
							<ItemTemplate>
								<asp:CheckBox ID ="chkRequired" runat="server" Enabled="false" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Required").ToString()) %>'>
								</asp:CheckBox>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn DataField="Prompt" headerText="Prompt">
							<headerStyle Width="190px"></headerStyle>
						</asp:BoundColumn>
						<asp:TemplateColumn headerText="Value">
							<headerStyle Width="120px"></headerStyle>
							<ItemTemplate>
								<asp:TextBox Width="120px" id="txtValue" cssclass="TextBox" 
								    ontextchanged="ValueChanged" autopostback="true"
								    runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Value") %>'>
								</asp:TextBox>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn DataField="Weight" headerText="Weight(%)">
							<headerStyle HorizontalAlign="Right" Width="110px"></headerStyle>
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:TemplateColumn headerText="Def. Score">
							<headerStyle Width="115px"></headerStyle>
							<ItemTemplate>
								<asp:CheckBox ID ="chkDefaultScore" runat="server" Enabled="false" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.DefaultScore").ToString()) %>'>
								</asp:CheckBox>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn headerText="Score">
							<headerStyle HorizontalAlign="Right" Width="50px"></headerStyle>
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
							<ItemTemplate>
								<asp:TextBox id="txtScore" Width="55px" style="text-align:right;" cssclass="TextBoxWithNoBorder" ontextchanged="ScoreChanged" autopostback="true" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Score").ToString())%>'>
								</asp:TextBox><input id="hdfldScoreType"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ScoreType").ToString())%>' name="hdfldScoreType" /><input id="hdfldRiskItemId" runat="server" type="hidden" value='<%# DataBinder.Eval(Container, "DataItem.RiskItemId")%>' name="hdfldRiskItemId" /><input id="hdfldOldScore" runat="server" type="hidden" value='<%# DataBinder.Eval(Container, "DataItem.OldScore")%>' name="hdfldOldScore" />
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn headerText="Weighted Score">
							<headerStyle HorizontalAlign="Right" Width="170px"></headerStyle>
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
							<ItemTemplate>
								<asp:TextBox id="txtWeightedScore" style="text-align:right;" cssclass="TextBoxWithNoBorder" 
								 runat="server" Width="160px" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.WeightedScore").ToString())%>'></asp:TextBox>							
							</ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
				</asp:DataGrid>
				<table style="WIDTH: 808px" cellspacing="0" cellpadding="0" border="0">
					<tr>
					    <td colspan="3" style="WIDTH: 150px">&nbsp;</td>
					    <td style="WIDTH: 30px">&nbsp;</td>
						<td align="right" colspan="2">
						    <asp:label id="Label7" runat="server" Width="210px" CssClass="LABEL"
						    Font-Size="11px" Font-Bold="True" >Total Weight:</asp:label>
						</td>
						<td align="left" valign="top">
						    <asp:textbox id="txtTotalWeight" runat="server" Width="90px" CssClass="gridcell"></asp:textbox>
						</td>					
						<td align="right">
							<asp:label id="Label3" runat="server" Width="150px" CssClass="LABEL">Proposed Risk Score:</asp:label>
						</td>
						<td>
							<asp:textbox id="txtFinalScore" runat="server" Width="175px" CssClass="gridcell"></asp:textbox>
						</td>
					</tr>
					<tr>	
					    <td style="WIDTH: 5px">&nbsp;</td>
						<td align="right"><asp:label id="Label5" runat="server" Width="140px" Font-Size="11px" Font-Bold="True" 
							Text="Weighted Score"></asp:label></td>
						<td style="WIDTH: 5px">&nbsp;</td>
						<td align="left" valign="top" colspan="3"><asp:label id="Label4" runat="server" Width="240px" Font-Size="11px"
							Text="= (Weight x Score)/Total(Weight)"></asp:label>
						</td>	
						<td style="WIDTH: 90px">&nbsp;</td>				    			    				
						<td align="right">
							<asp:linkButton id="lnkNewRiskClass" runat="server" Width="150px" CausesValidation="False" CssClass="Label">Proposed Risk Class:</asp:linkButton>
						</td>
						<td>
							<asp:textbox id="txtNewRiskClass" runat="server" Width="175px" CssClass="gridcell"></asp:textbox>
						</td>
					</tr>
				</table>
				<table>
					<tr>					    
					    <td align="right"><asp:label id="Label2" runat="server" Width="145px" Font-Size="11px" Font-Bold="True" 
							Text="Proposed Risk Score"></asp:label></td>						
						<td align="left"><asp:label id="Label6" runat="server" Width="160px" Font-Size="11px"
							Text="= Total(Weighted Score) ="></asp:label></td>	
						<td align="left"><asp:label id="lblFinalScore"  
							runat="server" Font-Size="11px" Width="80px" Text=""></asp:label>
						</td>							
					</tr>
				</table>
			</asp:panel>
			<asp:panel id="Panel2" runat="server" Height="15px" Width="808px" cssclass="Panel">
				<table>
					<tr>
						<td>
							<asp:label id="Label51" runat="server" CssClass="LABEL">Owner Branch:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerBranch" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label52" runat="server" CssClass="LABEL">Owner Department:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerDept" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label53" runat="server" CssClass="LABEL">Owner Operator:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerOper" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					</table>
			</asp:panel>
			<asp:panel id="Panel3" runat="server" Height="15px" Width="712px" cssclass="Panel">					
					<uc1:ucView id="vwRiskScoreHist" runat="server"></uc1:ucView>				
			</asp:panel>
			<asp:panel id="Panel4" runat="server" Height="15px" Width="712px" cssclass="Panel"></asp:panel>
			<asp:panel id="cPanelValidators" runat="server" Width="864px" Height="46px">
				<asp:CustomValidator id="CVCScoreCheck" runat="server" ErrorMessage="A risk item must have a value and its score must be numeric and in the range (0,100)."
					display="None" OnServerValidate="ValidateScores" ControlToValidate="txtdummy"></asp:CustomValidator>
				<asp:textbox id="txtdummy" runat="server" Width="140px" CssClass="TEXTBOX" Visible="False">Dummy</asp:textbox>
			</asp:panel></form>
	</body>
</html>