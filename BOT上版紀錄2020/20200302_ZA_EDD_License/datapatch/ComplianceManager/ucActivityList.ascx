<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ucActivityList.ascx.vb" Inherits="CMBrowser.ucActivityList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<script type="text/javascript"  language="javascript">

    function IsFirstDayofMonth(pDate)
    {
        var vDate = new Date(pDate.getFullYear(), pDate.getMonth(), 1);
              
        if (vDate.toLocaleDateString() == pDate.toLocaleDateString())
            return true;
        else
            return false;
       
    }
    function IsFirstDayofQuarter(pDate)
    {
        if (IsFirstDayofMonth(pDate))
        {
           
            switch (pDate.getMonth())
            {
                case 0:
                    return true;
                    break;
                    
                case 3:
                    return true;
                    break;
                    
                case 6:
                    return true;
                    break;
                    
                case 9:
                    return true;
                    break;
                    
                default:
                    return false;
                    break;             
            }
        }
        else
        {
            return false;
        }
    }
    
	function ConfirmMessage()
	{
		try
		{
		    
		    if (window.document.forms(0).ActivityList_optByPeriodActivity.checked)
		    {   
		    
		        var vStartDate = new Date(window.document.forms(0).ActivityList_txtStDate.value);		        
		       		    
		        var MsgOK = "Do you want to reset the Start Date" + "\n" +
		                    "to the first day of the ";
		                
		        var MsgCancel = "\n\n" + "Click Cancel to proceed without resetting." + "\n\n";
		        
		        if (window.document.forms(0).ActivityList_optByWeek.checked)
		        {
		            if (vStartDate.getDay()>0)
		            {		            
				        if(confirm(MsgOK + "Week?" + MsgCancel))
				            window.document.forms(0).ActivityList_hfldCalcStDate.value="Yes";
				    
				    }
		        }

		        if (window.document.forms(0).ActivityList_optByMonth.checked)
		        {
		            if (IsFirstDayofMonth(vStartDate)==false)	
		            {
				        if(confirm(MsgOK + "Month?" + MsgCancel))
				            window.document.forms(0).ActivityList_hfldCalcStDate.value="Yes";
				    }
		        }
		
		        if (window.document.forms(0).ActivityList_optByQuarter.checked)
		        {
		    
		            if (IsFirstDayofQuarter(vStartDate)==false)
		            {
				        if(confirm(MsgOK + "Quarter?" + MsgCancel))	
				            window.document.forms(0).ActivityList_hfldCalcStDate.value="Yes";		    
				
		            }
		         }
		    }
		    return true;
        }
        catch(ex) {}
		
	}
	
	function OpenActivityCash() {
	var qstring;
	var custid;
	var date_s;
	var date_e;
	
	var url = location.href;
		if(url.indexOf('?')!=-1)
		{
			custid = "";
			var ary = url.split('?')[1].split('&');
			
			for(i=0;i<=ary.length-1;i++)
			{

				if(ary[i].split('=')[0] == 'RecordID')
					custid = ary[i].split('=')[1];
			}
			
		}
		
	date_s = window.document.forms(0).ActivityList_txtStDate.value;
	date_e = window.document.forms(0).ActivityList_txtEdDate.value;
	
	qstring = "https://10.1.10.48/showActivityCash.asp?id=" + custid + "&sdate=" + date_s + "&edate=" + date_e;
	window.open(qstring, '', 'scrollbars=yes,resizable=yes,width=880,height=600'); return false;

	}


	function OpenRelatedActivityWindow(pRecordID, pViewID, pObjectName, 
	                pStartDateID, pEndDateID, pRelAccIDs, pRelPartyIDs)
	{
	    try
	    {
	        var vStartDate = "";
	        var vEndDate   = "";
	        var vLargePopup = 5;
	        var vQueryString = ""
	        
	        var txtStartDate = window.document.getElementById(pStartDateID);
	        if (txtStartDate != null)
	            vStartDate= txtStartDate.value;
	            
	        var txtEndDate = window.document.getElementById(pEndDateID);
	         if (txtEndDate != null)
	            vEndDate= txtEndDate.value;
	        
	        
	       vQueryString =  "ViewRelatedActivity.aspx?" + 
	                       "RecordId="    + pRecordID +
	                       "&ViewId="     + pViewID +
	                       "&ObjectName=" + pObjectName +	                       
	                       "&StartDate="  + encodeURIComponent(pStartDateID) +
	                       "&EndDate="    + encodeURIComponent(pEndDateID) +
	                       "&RelAcc="     + encodeURIComponent(pRelAccIDs) +
	                       "&RelParty="   + encodeURIComponent(pRelPartyIDs);
	                       
	       var nw=window.open(vQueryString, "ViewRelatedActivity", 
	                                    getwindowparam(vLargePopup));
		
	        if (nw != null)
		        nw.focus();	
	        
	        
	    }
	    catch(ex){}
	}
	
</script>

<asp:ScriptManagerProxy ID="smpUcActivityList" runat="server">
</asp:ScriptManagerProxy>
<asp:table id="Table1" runat="server" CellSpacing="2" CellPadding="2" >
    
	<asp:TableRow ID="trActivityOptions" Visible="False">
	    <asp:TableCell Width="5px">&nbsp;</asp:TableCell>
		<asp:TableCell Wrap="false">
			<asp:RadioButton Checked="True" ID="optByPeriod" GroupName="ListBasis" Runat="server" CssClass="lblfld"
				Text="Data by Period"></asp:RadioButton>
		</asp:TableCell>
		<asp:TableCell Wrap="false">
			<asp:RadioButton ID="optByProfile" GroupName="ListBasis" Runat="server" CssClass="lblfld" Text="Data by Profile"></asp:RadioButton>
		</asp:TableCell>
		<asp:TableCell ColumnSpan="4">
			<asp:label ID="lblComment" cssclass="LabelCaption" Runat="server" Text="Note: Select 'Data By Period' if the activities have to be listed based on the date range selected. 
			Select 'Data By Profile', if the activities should be listed based on the profile selected."></asp:label>
		</asp:TableCell>
	</asp:TableRow>
	<asp:TableRow>
	    <asp:TableCell Width="5px">&nbsp;</asp:TableCell>
		<asp:TableCell HorizontalAlign="Right">
			<asp:Label id="Label1" Width="135px" runat="server" CssClass="lblfld">Start Date:</asp:Label>			
		</asp:TableCell>
		<asp:TableCell>
			<asp:TextBox id="txtStDate" Width="135px" runat="server"></asp:TextBox>
			<input id="hfldCalcStDate" name="hfldCalcStDate" 
			        runat="server" value="" type="hidden" />
		</asp:TableCell>
		<asp:TableCell HorizontalAlign="Right">
			<asp:Label id="Label2" Width="80px" Font-Bold="True" Font-Size="12px" runat="server">List By</asp:Label>
		</asp:TableCell>
		<asp:TableCell>			
			<asp:RadioButton id="optByPeriodActivity" AutoPostBack="true" Checked="true"
			    Width="80px" Font-Size="12px" runat="server" 
			    GroupName="optListBy" Text="Period"></asp:RadioButton>
		</asp:TableCell>
		<asp:TableCell VerticalAlign="Bottom">
			<asp:RadioButton id="optByRelatedActivity" AutoPostBack="true" 
			        Font-Size="12px" Width="150px" runat="server" 
			        GroupName="optListBy" Text="Related Activity"></asp:RadioButton>
		</asp:TableCell>
		<asp:TableCell HorizontalAlign="Center">
			<asp:Button id="cmdListActivity" width="92px" Enabled="False" OnClientClick="return ConfirmMessage();" runat="server" CausesValidation="False" Text="List Activity" 
            ></asp:Button>
   		</asp:TableCell>
	</asp:TableRow>
	<asp:TableRow>
	    <asp:TableCell Width="5px">&nbsp;</asp:TableCell>
		<asp:TableCell HorizontalAlign="Right">
			<asp:Label id="Label3" Width="135px" runat="server" CssClass="lblfld">End Date:</asp:Label>
		</asp:TableCell>
		<asp:TableCell>
			<asp:TextBox id="txtEdDate" Width="135px" runat="server"></asp:TextBox>
		</asp:TableCell>
		<asp:TableCell></asp:TableCell>
		<asp:TableCell>
		       <asp:Panel ID="pnlPeriodOptions" runat="server" 
                BorderStyle="Solid" BorderColor="DarkGray" BorderWidth="1px">   
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <asp:RadioButton id="optByDay" Width="80px" Font-Size="12px" 
                                GroupName="optPeriod" runat="server" Text="Day"></asp:RadioButton>
                        </td>
                        <td>
                            <asp:RadioButton id="optByWeek" Width="80px" Font-Size="12px" runat="server" GroupName="optPeriod" Text="Week"></asp:RadioButton>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>  
			                <asp:RadioButton id="optByMonth" Font-Size="12px" GroupName="optPeriod" runat="server" Checked="True"
				            Text="Month"></asp:RadioButton>
				        </td>
				        <td>				        
			                <asp:RadioButton id="optByQuarter" Font-Size="12px" GroupName="optPeriod" runat="server" Text="Quarter"></asp:RadioButton>
			            </td>
				    </tr>
				 </table>
            </asp:Panel>
		</asp:TableCell>
		<asp:TableCell>		   		    
            <asp:Panel ID="pnlRelatedOptions" Visible="False" runat="server" 
                BorderStyle="Solid" BorderColor="DarkGray" BorderWidth="1px">   
                <table>
                    <tr>
                        <td></td>
                        <td>     
                            <asp:CheckBox ID="chkRelatedAccounts"  Checked="false" Visible="false"
                            Font-Size="12px"  runat="server" Text="Accounts" />
                         </td>
                     </tr>
                     <tr>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td>                            
                            <asp:CheckBox ID="chkRelatedParties" Checked="false" 
                                Font-Size="12px" runat="server" Text="Parties" />            
                         </td>
                    </tr>
                </table>    
             </asp:Panel>
		</asp:TableCell>
		<asp:TableCell HorizontalAlign="Center">
		    <asp:Button id="cmdHideDetails" width="92px" Enabled="False" runat="server" CausesValidation="False" Text="Hide Details"></asp:Button>
            <a class="lnktask" Onclick="OpenActivityCash();">*CashActivity</a>
		</asp:TableCell>
	</asp:TableRow>
	<asp:TableRow ID="trDateRollup">
	    <asp:TableCell Width="5px">&nbsp;</asp:TableCell>
		<asp:TableCell ColumnSpan="7" HorizontalAlign="Center">
			<asp:label id="lblListTitle" runat="server" CssClass="lblfld" Visible="False"></asp:label>
			<asp:Label id="lblActivityMsg" runat="server" Visible="False"></asp:Label>
			<uc1:ucView id="ViewGrid" runat="server"></uc1:ucView>
		</asp:TableCell>
	</asp:TableRow>	
	<asp:TableRow ID="trRelatedAccounts" >	    
		<asp:TableCell    ColumnSpan="2" HorizontalAlign="Right" VerticalAlign="Top">
			<asp:label id="lblRelatedAccounts" runat="server" 
			Width="140px" CssClass="lblfld" Text="Related Accounts:"></asp:label>
		</asp:TableCell>
		<asp:TableCell ColumnSpan="4" HorizontalAlign="Left" VerticalAlign="Top">
            <asp:TextBox ID="txtRelatedAccounts" Width="530px" Height="45px" TextMode="MultiLine" runat="server"></asp:TextBox>
		</asp:TableCell>
		<asp:TableCell VerticalAlign="Top" HorizontalAlign="Center">
            <input id="cmdRelatedAccounts" name="cmdRelatedAccounts" 
             style="WIDTH: 92px" runat="server" type="button" value="Summarize" />
         </asp:TableCell>
	</asp:TableRow>	
	<asp:TableRow ID="trRelatedParties">	    
		<asp:TableCell ColumnSpan="2" HorizontalAlign="Right" VerticalAlign="Top">
			<asp:label id="lblRelatedParties" runat="server" 
			Width="140px" CssClass="lblfld" Text="Related Parties:"></asp:label>
		</asp:TableCell>
		<asp:TableCell ColumnSpan="4" HorizontalAlign="Left" VerticalAlign="Top">
            <asp:TextBox ID="txtRelatedParties" Width="530px" Height="45px"  TextMode="MultiLine" runat="server"></asp:TextBox>
		</asp:TableCell>
		<asp:TableCell VerticalAlign="Top" HorizontalAlign="Center">
           <input id="cmdRelatedParties" name="cmdRelatedParties" 
            style="WIDTH: 92px" runat="server" type="button" value="Summarize" /> 
        </asp:TableCell>
	</asp:TableRow>	
	<asp:TableRow>
	    <asp:TableCell Width="5px">&nbsp;</asp:TableCell>
		<asp:TableCell ColumnSpan="7" HorizontalAlign="Center">
			<asp:label id="lblDetailsTitle" runat="server" CssClass="lblfld" Visible="False"></asp:label>
			<uc1:ucView id="ViewDetailsGrid" runat="server"></uc1:ucView>
		</asp:TableCell>
	</asp:TableRow>
</asp:table>
