Industry Ind 超過長度40之選項

4.Business dealing in second hand engines, car and body and spare parts – employee
5.Dealing in clothing – owner/director/senior management position.
6.Fire arms/Ammunition/Explosives-Business owner/person in senior manage position.
7.Fishing/hunting – owner/director/senior manag. position.
8.Gov./State owned enterprise position – Director General/CEO/Mayor.
9.Gov./State owned enterprise /Municipal employee.
10.Import and/or exports business – owner/director/senior manage position.
12.Organisations dependant on financial support from public or government – persons in senior management positions.
13.Precious metals/stones – Owner/senior manage position.
15.Scrap metal – Owner/director/senior manage position.
16.Second hand engines, car body and spare parts – Owner/director/senior manager.
17.Senior gov./State owned enterprise/municipal employee (Manager/Director).
19.Textile products – owner/director/senior manag position.
20.Tobacco products – owner/director/senior manage position.
25.Fire arms/ammunition/explosives – employee in non-managerial position.
26.Fishing/hunting – employee not in managerial position.
29.Precious metals/stones – Employee in non-managerial position.
30.Property investment company – owner/director/senior manager.
32.Retail business – owner/director /senior management position.
33.Wholesale business – owner/director/ senior management position.
43.Building materials and garden supplies
51.Freight services not related to International trade.
56.Mining not related to Precious metals or stones
57.Miscellaneous Manufacturing Industries
63.Textile/clothing – employee in non-managerial position.
65.Investment customer earning interest only

Industry Ent 超過長度40之選項

7.Freight services – small to medium sized companies
10.Organisations dependant on financial support from public or government
13.Respondent bank (Correspondent bank relationship).
14.Second hand engines, car body and spare parts.
19.Government/State owned enterprise/Municipality
21.Mining not related to precious metals or stones
27.Retail (general) not specifically mentioned
31.Wholesale (general) not specifically mentioned
33.Freight services – big well known international freight companies i.e. MSC.
41.Electrical/electronic suppliers and services
57.Miscellaneous Manufacturing Industries