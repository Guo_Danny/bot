USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_InsKYCData]    Script Date: 11/11/2020 10:31:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








ALTER Procedure [dbo].[BSA_InsKYCData] ( @CustomerId ObjectID, @NamePrefix varchar(25), @FirstName varchar(50),
		@MiddleName varchar(50), @LastName varchar(50),      @NameSuffix varchar(25),
		@Alias varchar(50),      @PlaceofBirth varchar(50),  @MothersMaidenName varchar(50), @Introducer varchar(50),
		@YrsAtCurrAddress int,   @NoOfDependants int,        @Employername varchar(50),
		@JobTitle varchar(50),   @JobTime varchar(50),       @LengthOfEmp varchar(50),
		@IsAlien bit,            @CountryofDualCitizenship SCODE, @SecondaryEmployer varchar(50),
		@OriginOfFunds varchar(50), @IsPersonalRelationship bit,
		@DateOfInception datetime,@LegalEntityType varchar(50),
		@NoofEmployees int,      @TotalAnnualSales money,    @ProductsSold varchar(255),
		@ServicesProvided varchar(255), @GeographicalAreasServed varchar(255), @IsPubliclyTraded bit,
		@TickerSymbol varchar(10), @Exchange varchar(50),    @IsPensionFund bit,
		@IsEndowmentFund bit,    @IsGovernmentSponsored bit, @IsSubsidiary bit,
		@IsForeignCorrBnk bit,   @RequireCashTrans bit,      @RequireForeignTrans bit,
		@RequireWireTrans bit,   @HasForeignBranches bit,    @ForeignBranchCountries varchar(255),
		@TypeofMonInstruments varchar(255), @URL varchar(255), @HasPatriotActCertification bit,
		@PatriotActCertExpiryDate datetime, @MoneyOrders bit,  @MORegistration varchar(10),
		@MORegConfirmationNo varchar(50),   @MOActingasAgentfor varchar(20), @MOMaxDailyAmtPerPerson money,
		@MOMonthlyAmt money,                @MOPercentIncome varchar(5),     @TravelersCheck bit,
		@TCRegistration varchar(10),        @TCRegConfirmationNo varchar(50),@TCActingasAgentfor varchar(20),
		@TCMaxDailyAmtPerPerson money,      @TCMonthlyAmt money,             @TCPercentIncome varchar(5),
		@MoneyTransmission bit,             @MTRegistration varchar(10),     @MTRegConfirmationNo varchar(50),
		@MTActingasAgentfor varchar(20),    @MTMaxDailyAmtPerPerson money,   @MTMonthlyAmt money,
		@MTPercentIncome varchar(5),        @CheckCashing bit,               @CCRegistration varchar(10),
		@CCRegConfirmationNo varchar(50),   @CCActingasAgentfor varchar(20), @CCMaxDailyAmtPerPerson money,
		@CCMonthlyAmt money,                @CCPercentIncome varchar(5),     @CurrencyExchange bit,
		@CERegistration varchar(10),        @CERegConfirmationNo varchar(50),@CEActingasAgentfor varchar(20),
		@CEMaxDailyAmtPerPerson money,      @CEMonthlyAmt money,             @CEPercentIncome varchar(5),
		@CurrencyDealing bit,               @CDRegistration varchar(10),     @CDRegConfirmationNo varchar(50),
		@CDActingasAgentfor varchar(20),    @CDMaxDailyAmtPerPerson money,   @CDMonthlyAmt money,
		@CDPercentIncome varchar(5),        @StoredValue bit,                @SVRegistration varchar(10),
		@SVRegConfirmationNo varchar(50),   @SVActingasAgentfor varchar(20), @SVMaxDailyAmtPerPerson money,
		@SVMonthlyAmt money,                @SVPercentIncome varchar(5),     @CDDUser1 varchar(40),         	
		@CDDUser2 varchar(40),		    @CDDUser3 varchar(40),           @CDDUser4 varchar(40),		
		@CDDUser5 varchar(40),         	    @CDDUser6 varchar(40),           @CDDUser7 varchar(40),		
		@CDDUser8 varchar(40),         	    @CDDUser9 varchar(40),           @CDDUser10 varchar(40),		
		@CDDUser11 varchar(40),             @CDDUser12 varchar(40),          @CDDUser13 varchar(40),		
		@CDDUser14 varchar(40),             @CDDUser15 varchar(40),          @CDDUser16 varchar(40),		
		@CDDUser17 varchar(40),             @CDDUser18 varchar(40),          @CDDUser19 varchar(40),
		@CDDUser20 varchar(40),             @CDDUser21 varchar(40),          @CDDUser22 varchar(40),		
		@CDDUser23 varchar(40),             @CDDUser24 varchar(40),          @CDDUser25 varchar(40),		
		@CDDUser26 varchar(40),             @CDDUser27 varchar(40),          @CDDUser28 varchar(40),		
		@CDDUser29 varchar(40),             @CDDUser30 varchar(40),          @CDDUser31 varchar(1000),		
		@CDDUser32 varchar(1000),            @CDDUser33 varchar(1000),         @CDDUser34 varchar(1000),		
		@CDDUser35 varchar(1000),            @CDDUser36 varchar(1000),         @CDDUser37 varchar(1000),		
		@CDDUser38 varchar(1000),            @CDDUser39 varchar(1000),         @CDDUser40 varchar(3000),		
		@createOper SCode,
		--	Begin: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw
		@RelationshipWithBankAffl bit ,					-- Relationship with Bank Affiliate
		@PrivInvestmentCompany bit ,					-- Private Investiment Company
		@ResultsOfNegativePressSearch varchar(255)      -- Results of Negative Press Search	    
		--	End: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw	
)
  As

	-- Start standard stored procedure transaction header
	declare @trnCnt int
	select @trnCnt = @@trancount	-- Save the current trancount
	If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_InsKYCData
	else
	-- Already in a transaction
	save tran BSA_InsKYCData
	-- End standard stored procedure transaction header

       --2019.04 update for CDD Begin
   declare @item_Channel varchar(50), @item_TypeProd varchar(50), @item_Country varchar(50), 
   @item1 varchar(50), @item2 varchar(50), @item3 varchar(50), @item4 varchar(50), @item5 varchar(50), 
   @type varchar(50), 
   @RFICODE_Channel varchar(50), @RFICODE_TypeProd varchar(50), @RFICODE_Country varchar(50);


   select @type = [type] from Customer where id = @CustomerId

       if isnull(RTrim(@type),'') = 'I' or isnull(RTrim(@type),'') = ''
       begin
              set @RFICODE_Channel = 'oiTxChannel';
              set @RFICODE_TypeProd = 'oiTypeProd';
              set @RFICODE_Country = 'oiTxCountry';
    end

       if isnull(RTrim(@type),'') = 'E'
       begin
              set @RFICODE_Channel = 'oeTxChannel';
              set @RFICODE_TypeProd = 'oeTypeProd';
              set @RFICODE_Country = 'oeTxCountry';
    end

       if isnull(RTrim(@type),'') = 'B'
       begin
              set @RFICODE_Channel = 'oeTxChannel';
              set @RFICODE_TypeProd = 'oeTypeProd';
              set @RFICODE_Country = 'oeTxCountry';
    end
       
       --Channel
       set @item1=NullIf(RTrim(@CDDUser14) ,'')
	   set @item2=NullIf(RTrim(@CDDUser15) ,'')
	   set @item3=NullIf(RTrim(@CDDUser16) ,'')

	   select @item1 
       select @item2
       select @item3

       select @item_Channel=value FROM  RiskFactorValue WHERE RFICODE=@RFICODE_Channel                      --需要改成不同的Code: oiTxChannel , OiTypeProd, oiTxCountry (第2碼i是CUST TYPE (FIS), if e: 代表Entity)
       and value in (@item1, @item2, @item3) and score in ( select max(score) FROM  RiskFactorValue 
       WHERE RFICODE=@RFICODE_Channel                    --需要改成不同的Code: oiTxChannel , OiTypeProd, oiTxCountry (第2碼i是CUST TYPE (FIS), if e: 代表Entity)
       and value in (@item1, @item2, @item3) 
       )


       --TypeProd
       set @item1=NullIf(RTrim(@CDDUser2) ,'')
	   set @item2=NullIf(RTrim(@CDDUser3) ,'')
	   set @item3=NullIf(RTrim(@CDDUser4) ,'')
	   set @item4=NullIf(RTrim(@CDDUser5) ,'')
	   set @item5=NullIf(RTrim(@CDDUser6) ,'')

	   select @item1 
       select @item2
       select @item3
	   select @item4
	   select @item5

       select @item_TypeProd=value FROM  RiskFactorValue WHERE RFICODE=@RFICODE_TypeProd                      --需要改成不同的Code: oiTxChannel , OiTypeProd, oiTxCountry (第2碼i是CUST TYPE (FIS), if e: 代表Entity)
       and value in (@item1, @item2, @item3, @item4, @item5) and score in ( select max(score) FROM  RiskFactorValue 
       WHERE RFICODE=@RFICODE_TypeProd                     --需要改成不同的Code: oiTxChannel , OiTypeProd, oiTxCountry (第2碼i是CUST TYPE (FIS), if e: 代表Entity)
       and value in (@item1, @item2, @item3, @item4, @item5) 
       )

       --Country
       set @item1=NullIf(RTrim(@CDDUser8) ,'')
	   set @item2=NullIf(RTrim(@CDDUser9) ,'')
	   set @item3=NullIf(RTrim(@CDDUser10) ,'')
	   set @item4=NullIf(RTrim(@CDDUser11) ,'')
	   set @item5=NullIf(RTrim(@CDDUser12) ,'')

	   select @item1 
       select @item2
       select @item3
	   select @item4
	   select @item5

       select @item_Country=value FROM  RiskFactorValue WHERE RFICODE=@RFICODE_Country                      --需要改成不同的Code: oiTxChannel , OiTypeProd, oiTxCountry (第2碼i是CUST TYPE (FIS), if e: 代表Entity)
       and value in (@item1, @item2, @item3, @item4, @item5) and score in ( select max(score) FROM  RiskFactorValue 
       WHERE RFICODE=@RFICODE_Country                     --需要改成不同的Code: oiTxChannel , OiTypeProd, oiTxCountry (第2碼i是CUST TYPE (FIS), if e: 代表Entity)
       and value in (@item1, @item2, @item3, @item4, @item5) 
       )

       --2019.04 update for CDD End

	declare @stat int
	Insert Into KYCData (	CustomerId, 	NamePrefix ,		FirstName ,
			MiddleName ,		LastName ,		NameSuffix ,
			Alias ,			PlaceofBirth ,		MothersMaidenName ,
			Introducer,		YrsAtCurrAddress,	NoOfDependants ,
			EmployerName,		JobTitle ,		JobTime , 
			LengthOfEmp,		IsAlien ,		CountryofDualCitizenship,
			SecondaryEmployer ,	OriginOfFunds ,		IsPersonalRelationship ,
			DateOfInception,	LegalEntityType,	NoofEmployees ,
			TotalAnnualSales ,	ProductsSold ,		ServicesProvided ,
			GeographicalAreasServed,IsPubliclyTraded,	TickerSymbol ,
			Exchange ,		IsPensionFund ,		IsEndowmentFund ,
			IsGovernmentSponsored ,	IsSubsidiary ,		IsForeignCorrBnk ,
			RequireCashTrans ,	RequireForeignTrans , 	RequireWireTrans ,
			HasForeignBranches ,	ForeignBranchCountries ,TypeofMonInstruments ,
			URL ,			HasPatriotActCertification ,PatriotActCertExpiryDate ,
			MoneyOrders ,		MORegistration ,	MORegConfirmationNo ,
			MOActingasAgentfor ,	MOMaxDailyAmtPerPerson ,MOMonthlyAmt ,
			MOPercentIncome ,	TravelersCheck ,	TCRegistration ,
			TCRegConfirmationNo ,	TCActingasAgentfor ,	TCMaxDailyAmtPerPerson ,
			TCMonthlyAmt ,		TCPercentIncome ,	MoneyTransmission ,
			MTRegistration ,	MTRegConfirmationNo,	MTActingasAgentfor ,
			MTMaxDailyAmtPerPerson ,MTMonthlyAmt ,		MTPercentIncome ,
			CheckCashing ,		CCRegistration ,	CCRegConfirmationNo ,
			CCActingasAgentfor ,	CCMaxDailyAmtPerPerson ,CCMonthlyAmt ,
			CCPercentIncome ,	CurrencyExchange ,	CERegistration ,
			CERegConfirmationNo ,	CEActingasAgentfor ,	CEMaxDailyAmtPerPerson ,
			CEMonthlyAmt ,		CEPercentIncome ,	CurrencyDealing ,
			CDRegistration ,	CDRegConfirmationNo ,	CDActingasAgentfor ,
			CDMaxDailyAmtPerPerson ,CDMonthlyAmt ,		CDPercentIncome ,
			StoredValue ,		SVRegistration ,	SVRegConfirmationNo ,
			SVActingasAgentfor ,	SVMaxDailyAmtPerPerson ,SVMonthlyAmt,
			SVPercentIncome ,	CDDUser1,		CDDUser2,
			CDDUser3,		CDDUser4,		CDDUser5,
			CDDUser6,		CDDUser7,		CDDUser8,
			CDDUser9,		CDDUser10,		CDDUser11,
			CDDUser12,		CDDUser13,		CDDUser14,
			CDDUser15,		CDDUser16,		CDDUser17,
			CDDUser18,		CDDUser19,		CDDUser20,
			CDDUser21,		CDDUser22,		CDDUser23,
			CDDUser24,		CDDUser25,		CDDUser26,
			CDDUser27,		CDDUser28,		CDDUser29,
			CDDUser30,		CDDUser31,		CDDUser32,
			CDDUser33,		CDDUser34,		CDDUser35,
			CDDUser36,		CDDUser37,		CDDUser38,
			CDDUser39,		CDDUser40,		CreateOper,
			CreateDate,
			--	Begin: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw
			RelationshipWithBankAffl,		-- Relationship with Bank Affiliate
			PrivInvestmentCompany,			-- Private Investiment Company
			ResultsOfNegativePressSearch    -- Results of Negative Press Search	    
			--	End: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw			
	) 
	values (RTrim(@customerid),		NullIf(RTrim(@nameprefix),''), 	NullIf(RTrim(@firstname),''), 
			NullIf(RTrim(@middlename),''),	NullIf(RTrim(@lastname),''),	NullIf(RTrim(@namesuffix),''),
			NullIf(RTrim(@alias),''),	NullIf(RTrim(@placeofbirth),''),	NullIf(RTrim(@mothersmaidenname),''),
			NullIf(RTrim(@introducer),''),	NullIf(@yrsatcurraddress,0),	NullIf(@noofdependants,0),
			NullIf(RTrim(@EmployerName),''),NullIf(RTrim(@jobtitle),''),	NullIf(RTrim(@jobtime),''),
			NullIF(RTRIM(@LengthOfEmp),''),	@isalien,			NullIf(RTrim(@countryofdualcitizenship),''),
			NullIf(RTrim(@secondaryemployer),''),NullIf(RTrim(@OriginOfFunds),''),@ispersonalrelationship,
			@dateofinception,		NullIf(RTrim(@legalentitytype),''),@noofemployees,
			@totalannualsales,		NullIf(RTrim(@productssold), ''),NullIf(RTrim(@servicesprovided), ''),
			NullIf(RTrim(@geographicalareasserved), ''),@IsPubliclyTraded,	NullIf(RTrim(@tickersymbol), ''),
			NullIf(RTrim(@exchange), ''),	@ispensionfund,			@isendowmentfund,
			@isgovernmentsponsored,		@issubsidiary,			@isforeigncorrbnk,
			@requirecashtrans,		@requireforeigntrans,		@requirewiretrans,
			@hasforeignbranches,		NullIf(RTrim(@ForeignBranchCountries), ''), NullIf(RTrim(@TypeofMonInstruments), ''), 
			NullIf(RTrim(@URL), ''),	@HasPatriotActCertification,	@PatriotActCertExpiryDate,
			@moneyorders,			NullIf(RTrim(@MORegistration), ''), NullIf(RTrim(@MORegConfirmationNo), ''), 
			NullIf(RTrim(@MOActingasAgentfor), ''),@MOMaxDailyAmtPerPerson,	@MOMonthlyAmt,
			NullIf(RTrim(@MOPercentIncome), ''),@TravelersCheck ,		NullIf(RTrim(@TCRegistration), '') ,
			NullIf(RTrim(@TCRegConfirmationNo), '') ,NullIf(RTrim(@TCActingasAgentfor), '') ,@TCMaxDailyAmtPerPerson ,
			@TCMonthlyAmt,			NullIf(RTrim(@TCPercentIncome), ''),@MoneyTransmission,
			NullIf(RTrim(@MTRegistration), '') ,NullIf(RTrim(@MTRegConfirmationNo), '') ,NullIf(RTrim(@MTActingasAgentfor), '') ,
			@MTMaxDailyAmtPerPerson ,	@MTMonthlyAmt ,			NullIf(RTrim(@MTPercentIncome), '') ,
			@CheckCashing ,			NullIf(RTrim(@CCRegistration), '') ,NullIf(RTrim(@CCRegConfirmationNo), '') ,
			NullIf(RTrim(@CCActingasAgentfor), ''), @CCMaxDailyAmtPerPerson ,@CCMonthlyAmt ,
			NullIf(RTrim(@CCPercentIncome), '') ,@CurrencyExchange ,	NullIf(RTrim(@CERegistration), '') ,
			NullIf(RTrim(@CERegConfirmationNo), ''),NullIf(RTrim(@CEActingasAgentfor), '') ,@CEMaxDailyAmtPerPerson ,
			@CEMonthlyAmt ,			NullIf(RTrim(@CEPercentIncome), ''),@CurrencyDealing ,
			NullIf(RTrim(@CDRegistration), '') ,NullIf(RTrim(@CDRegConfirmationNo), '') ,NullIf(RTrim(@CDActingasAgentfor), ''),
			@CDMaxDailyAmtPerPerson ,	@CDMonthlyAmt,			NullIf(RTrim(@CDPercentIncome), '') ,
			@StoredValue ,			NullIf(RTrim(@SVRegistration), '') ,NullIf(RTrim(@SVRegConfirmationNo), '') ,
			NullIf(RTrim(@SVActingasAgentfor), ''), @SVMaxDailyAmtPerPerson , @SVMonthlyAmt,
			NullIf(RTrim(@SVPercentIncome), '') , @item_TypeProd, NullIf(RTrim(@CDDUser2) ,''),
			NullIf(RTrim(@CDDUser3) ,''),	NullIf(RTrim(@CDDUser4) ,''),	NullIf(RTrim(@CDDUser5) ,''),         
			NullIf(RTrim(@CDDUser6) ,''),    @item_Country,	NullIf(RTrim(@CDDUser8) ,''),         
			NullIf(RTrim(@CDDUser9) ,''),    NullIf(RTrim(@CDDUser10) ,''),	NullIf(RTrim(@CDDUser11) ,''),         
			NullIf(RTrim(@CDDUser12) ,''),   @item_Channel,	NullIf(RTrim(@CDDUser14) ,''),         
			NullIf(RTrim(@CDDUser15) ,''),   NullIf(RTrim(@CDDUser16) ,''),	NullIf(RTrim(@CDDUser17) ,''),         
			NullIf(RTrim(@CDDUser18) ,''),   NullIf(RTrim(@CDDUser19) ,''),	NullIf(RTrim(@CDDUser20) ,''),         
			NullIf(RTrim(@CDDUser21) ,''),   NullIf(RTrim(@CDDUser22) ,''),	NullIf(RTrim(@CDDUser23) ,''),         
			NullIf(RTrim(@CDDUser24) ,''),   NullIf(RTrim(@CDDUser25) ,''),	NullIf(RTrim(@CDDUser26) ,''),         
			NullIf(RTrim(@CDDUser27) ,''),   NullIf(RTrim(@CDDUser28) ,''),	NullIf(RTrim(@CDDUser29) ,''),         
			NullIf(RTrim(@CDDUser30) ,''),   NullIf(RTrim(@CDDUser31) ,''),	NullIf(RTrim(@CDDUser32) ,''),         
			NullIf(RTrim(@CDDUser33) ,''),   NullIf(RTrim(@CDDUser34) ,''),	NullIf(RTrim(@CDDUser35) ,''),         
			NullIf(RTrim(@CDDUser36) ,''),   NullIf(RTrim(@CDDUser37) ,''),	NullIf(RTrim(@CDDUser38) ,''),         
			NullIf(RTrim(@CDDUser39) ,''),   NullIf(RTrim(@CDDUser40) ,''),	RTrim(@CreateOper),
			GetDate(),
			--	Begin: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw
			@RelationshipWithBankAffl,							-- Relationship with Bank Affiliate
			@PrivInvestmentCompany,								-- Private Investiment Company
			NullIf(RTrim(@ResultsOfNegativePressSearch) ,'')	-- Results of Negative Press Search	    
			--	End: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw				
	)

	Select @stat = @@error

	-- Evaluate results of the transaction
	If @stat <> 0 begin
	rollback tran BSA_InsKYCData
	return @stat
	end
	If @trnCnt = 0
	commit tran BSA_InsKYCData
	return @stat
GO


