USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_Populate_RealtimeCustomer]    Script Date: 11/11/2020 10:46:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--Populating Data to RealTimeRiskRating tables
ALTER PROCEDURE [dbo].[BSA_Populate_RealtimeCustomer]
@XmlDoc xml,@userName varchar(255),@CustType varchar(11),@riskModel varchar(11) = NULL,
--@ErrText varchar(8000) output
@RtrIDOutput Int output
  As
declare @RTRCust int
declare @index int
declare @p2 xml
declare @CustIdXML ObjectId
declare @CustIdDb ObjectId
declare @RTRCustId ObjectId
declare @AccntId ObjectId
declare @PartyInfo varchar (100)
declare @AccountInfo varchar (100)
declare @operCode varchar(10)
declare @isClosed bit
declare @OnriskModel varchar(11)
declare @CustTypefromCustomer varchar(11)
set @index = 0
set @isClosed = 0
set @p2=convert(xml,@XmlDoc)
set @PartyInfo = 'GETPARTYRISKSCOREREQUEST/INPUT/PartyData/PartyInfo'
set @AccountInfo = 'GETPARTYRISKSCOREREQUEST/INPUT/PartyData/Account'
exec  sp_xml_preparedocument @index OUTPUT, @p2

select @CustIdXML=CustId   from openxml(@index,@PartyInfo,2)with (CustId ObjectId )
select @CustIdDb=Id from pbsa.dbo.Customer where Id=@CustIdXML
select @isClosed = closed from pbsa.dbo.customer with (nolock) where  Id=@CustIdXML
select @RTRCustId = Id from pbsa.dbo.RTRCustomer where id=@CustIdXML
create table #RTRAccount(RTRID INT IDENTITY(1000,1), Id varchar(35))
insert into #RTRAccount(Id)
select Id   from openxml(@index,@AccountInfo,2)with (Id  varchar(35)) 
	select @operCode=code from psec.dbo.operator where  UserName=@userName
		if @CustIdDb is not NULL  -- Existing Customer : 1) pull data from Customer,KYCData then 2) merge with new values from XML
		begin
					insert into pbsa.dbo.RTRCustomer (Id,Name,Parent,DBA,SecCode,[Address],City,[State],
					Zip,Country,Telephone,Email,TIN,LicenseNo,LicenseState,PassportNo,DOB,TypeOfBusiness,SourceOfFunds,AccountOfficer,
					AcctOffTel,AcctOffEmail,CompOfficer,CompOffTel,	CompOffEmail,IdList,Notes,ExemptionStatus,LastActReset,
					LastReview,LastReviewOper,CreateOper,CreateDate,LastOper,LastModify,OwnerBranch,OwnerDept,OwnerOper,RiskClass,Closed,
					User1,User2,User3,User4,User5,CountryOfOrigin,CountryOfIncorp,OffshoreCorp,BearerShares,NomeDePlume,Type,Resident,
					BusRelationNature,PrevRelations,PEP,PoliticalPos,FEP,MSB,CorrBankRelation,Sex,ShellBank,HighRiskRespBank,
					OffshoreBank,PayThroughAC,RegulatedAffiliate,USPerson,RFCalculate,[Status],ReviewDate,CTRAmt,AmtRange,SwiftTID,
					Embassy,ForeignGovt,CharityOrg,DoCIP,Prospect,AssetSize,Income,IndOrBusType,OnProbation,ProbationReason,ProbationStartDate,
					ProbationEndDate,ClosedDate,ClosedReason,OpenDate,CountryOfResidence,CountryOfCitizenship,KYCDataCreateDate,KYCDataModifyDate,KYCOperator,KYCStatus)
					
					select  Id,Name,Parent,DBA,	SecCode,[Address],City,[State],	Zip,Country,Telephone,Email,TIN,LicenseNo,LicenseState,PassportNo,DOB,
					TypeOfBusiness,SourceOfFunds,AccountOfficer,AcctOffTel,AcctOffEmail,CompOfficer,CompOffTel,
					CompOffEmail,IdList,Notes,ExemptionStatus,LastActReset,	LastReview,LastReviewOper,CreateOper,CreateDate,LastOper,
					LastModify,OwnerBranch,OwnerDept,OwnerOper,isnull(RiskClass,'DefClass'),isnull(Closed,0),User1,User2,User3,User4,User5,CountryOfOrigin,CountryOfIncorp,
					OffshoreCorp,BearerShares,NomeDePlume,Type,Resident,BusRelationNature,PrevRelations,PEP,PoliticalPos,FEP,
					MSB,CorrBankRelation,Sex,ShellBank,HighRiskRespBank,OffshoreBank,PayThroughAC,RegulatedAffiliate,USPerson,
					RFCalculate,[Status],ReviewDate,CTRAmt,AmtRange,SwiftTID,Embassy,ForeignGovt,CharityOrg,DoCIP,Prospect,AssetSize,
					Income,IndOrBusType,OnProbation,ProbationReason,ProbationStartDate,	ProbationEndDate,ClosedDate,ClosedReason,OpenDate,CountryOfResidence,
					CountryOfCitizenship,KYCDataCreateDate,KYCDataModifyDate,KYCOperator,KYCStatus from pbsa.dbo.Customer where Id=@CustIdXML and Closed <> 1
					--print ' Inserted to the RTRCustomer from Customer Table'
				
			
					INSERT INTO pbsa.dbo.RTRKYCData   -- New fields has to be added here
					(CustomerId,NamePrefix,FirstName,MiddleName,LastName,NameSuffix,Alias,PlaceofBirth,MothersMaidenName,Introducer,YrsAtCurrAddress,
					NoOfDependants,EmployerName,JobTitle,JobTime,LengthOfEmp,IsAlien,CountryofDualCitizenship,SecondaryEmployer,OriginOfFunds,IsPersonalRelationship,DateOfInception,
					LegalEntityType,NoOfEmployees,TotalAnnualSales,ProductsSold,ServicesProvided,GeographicalAreasServed,IsPubliclyTraded,TickerSymbol,Exchange,IsPensionFund,
					IsEndowmentFund,IsGovernmentSponsored,IsSubsidiary,IsForeignCorrBnk,RequireCashTrans,RequireForeignTrans,RequireWireTrans,HasForeignBranches,ForeignBranchCountries,
					TypeofMonInstruments,url,HasPatriotActCertification,PatriotActCertExpiryDate,MoneyOrders,MORegistration,MORegConfirmationNo,MOActingasAgentfor,MOMaxDailyAmtPerPerson,MOMonthlyAmt,
					MOPercentIncome,TravelersCheck,TCRegistration,TCRegConfirmationNo,TCActingasAgentfor,TCMaxDailyAmtPerPerson,TCMonthlyAmt,TCPercentIncome,MoneyTransmission,
					MTRegistration,MTRegConfirmationNo,MTActingasAgentfor,MTMaxDailyAmtPerPerson,MTMonthlyAmt,MTPercentIncome,CheckCashing,CCRegistration,CCRegConfirmationNo,
					CCActingasAgentfor,CCMaxDailyAmtPerPerson,CCMonthlyAmt,CCPercentIncome,CurrencyExchange,CERegistration,CERegConfirmationNo,CEActingasAgentfor,CEMaxDailyAmtPerPerson,
					CEMonthlyAmt,CEPercentIncome,CurrencyDealing,CDRegistration,CDRegConfirmationNo,CDActingasAgentfor,CDMaxDailyAmtPerPerson,CDMonthlyAmt,CDPercentIncome,StoredValue,
					SVRegistration,SVRegConfirmationNo,SVActingasAgentfor,SVMaxDailyAmtPerPerson,SVMonthlyAmt,SVPercentIncome,CDDUser1,CDDUser2,CDDUser3,CDDUser4,CDDUser5,CDDUser6,
					CDDUser7,CDDUser8,CDDUser9,CDDUser10,CDDUser11,CDDUser12,CDDUser13,CDDUser14,CDDUser15,CDDUser16,CDDUser17,CDDUser18,CDDUser19,CDDUser20,CDDUser21,CDDUser22,
					CDDUser23,CDDUser24,CDDUser25,CDDUser26,CDDUser27,CDDUser28,CDDUser29,CDDUser30,CDDUser31,CDDUser32,CDDUser33,CDDUser34,CDDUser35,CDDUser36,CDDUser37,CDDUser38,
					CDDUser39,CDDUser40,
					-- new RTRKycdata  fields below
					RelationshipWithBankAffl,PrivInvestmentCompany,ResultsOfNegativePressSearch,
					CreateOper,CreateDate,LastOper,LastModify)
					
					select CustomerId,NamePrefix,FirstName,MiddleName,LastName,NameSuffix,Alias,PlaceofBirth,MothersMaidenName,Introducer,YrsAtCurrAddress,
					NoOfDependants,EmployerName,JobTitle,JobTime,LengthOfEmp,IsAlien,CountryofDualCitizenship,SecondaryEmployer,OriginOfFunds,IsPersonalRelationship,DateOfInception,
					LegalEntityType,NoOfEmployees,TotalAnnualSales,ProductsSold,ServicesProvided,GeographicalAreasServed,IsPubliclyTraded,TickerSymbol,Exchange,IsPensionFund,
					IsEndowmentFund,IsGovernmentSponsored,IsSubsidiary,IsForeignCorrBnk,RequireCashTrans,RequireForeignTrans,RequireWireTrans,HasForeignBranches,ForeignBranchCountries,
					TypeofMonInstruments,url,HasPatriotActCertification,PatriotActCertExpiryDate,MoneyOrders,MORegistration,MORegConfirmationNo,MOActingasAgentfor,MOMaxDailyAmtPerPerson,MOMonthlyAmt,
					MOPercentIncome,TravelersCheck,TCRegistration,TCRegConfirmationNo,TCActingasAgentfor,TCMaxDailyAmtPerPerson,TCMonthlyAmt,TCPercentIncome,MoneyTransmission,
					MTRegistration,MTRegConfirmationNo,MTActingasAgentfor,MTMaxDailyAmtPerPerson,MTMonthlyAmt,MTPercentIncome,CheckCashing,CCRegistration,CCRegConfirmationNo,
					CCActingasAgentfor,CCMaxDailyAmtPerPerson,CCMonthlyAmt,CCPercentIncome,CurrencyExchange,CERegistration,CERegConfirmationNo,CEActingasAgentfor,CEMaxDailyAmtPerPerson,
					CEMonthlyAmt,CEPercentIncome,CurrencyDealing,CDRegistration,CDRegConfirmationNo,CDActingasAgentfor,CDMaxDailyAmtPerPerson,CDMonthlyAmt,CDPercentIncome,StoredValue,
					SVRegistration,SVRegConfirmationNo,SVActingasAgentfor,SVMaxDailyAmtPerPerson,SVMonthlyAmt,SVPercentIncome,CDDUser1,CDDUser2,CDDUser3,CDDUser4,CDDUser5,CDDUser6,
					CDDUser7,CDDUser8,CDDUser9,CDDUser10,CDDUser11,CDDUser12,CDDUser13,CDDUser14,CDDUser15,CDDUser16,CDDUser17,CDDUser18,CDDUser19,CDDUser20,CDDUser21,CDDUser22,
					CDDUser23,CDDUser24,CDDUser25,CDDUser26,CDDUser27,CDDUser28,CDDUser29,CDDUser30,CDDUser31,CDDUser32,CDDUser33,CDDUser34,CDDUser35,CDDUser36,CDDUser37,CDDUser38,
					CDDUser39,CDDUser40,
					-- new KYCDATA fields below
					RelationshipWithBankAffl,PrivInvestmentCompany,ResultsOfNegativePressSearch,
					CreateOper,CreateDate,LastOper,LastModify from pbsa.dbo.KYCData with (nolock) where CustomerId=@CustIdXML and @isClosed <> 1
					--print 'RTRKycdata data inserted from existing Kycdata  table'
					--#Region RTRCustomer Update begin			
										
					update pbsa.dbo.RTRCustomer
					--  RTRCustomer.RTRID=ISNULL(NULLIF(XCust.RTRID,''),RTRCustomer.RTRID),
					-- RTRCustomer.Id=ISNULL(NULLIF(XCust.Id,''),RTRCustomer.Id),
						  SET RTRCustomer.Parent=ISNULL(NULLIF(XCust.Parent,''),RTRCustomer.Parent),
						  RTRCustomer.Name=ISNULL(NULLIF(XCust.Name,''),RTRCustomer.Name),
						  RTRCustomer.DBA=ISNULL(NULLIF(XCust.DBA,''),RTRCustomer.DBA),
						  RTRCustomer.SecCode=ISNULL(NULLIF(XCust.SecCode,''),RTRCustomer.SecCode),
						  RTRCustomer.Address=ISNULL(NULLIF(XCust.Address,''),RTRCustomer.Address),
						  RTRCustomer.City=ISNULL(NULLIF(XCust.City,''),RTRCustomer.City),
						  RTRCustomer.State=ISNULL(NULLIF(XCust.State,''),RTRCustomer.State),
						  RTRCustomer.Zip=ISNULL(NULLIF(XCust.Zip,''),RTRCustomer.Zip),
						  RTRCustomer.Country=ISNULL(NULLIF(XCust.Country,''),RTRCustomer.Country),
						  RTRCustomer.Telephone=ISNULL(NULLIF(XCust.Telephone,''),RTRCustomer.Telephone),
						  RTRCustomer.Email=ISNULL(NULLIF(XCust.Email,''),RTRCustomer.Email),
						  RTRCustomer.TIN=ISNULL(NULLIF(XCust.TIN,''),RTRCustomer.TIN),
						  RTRCustomer.LicenseNo=ISNULL(NULLIF(XCust.LicenseNo,''),RTRCustomer.LicenseNo),
						  RTRCustomer.LicenseState=ISNULL(NULLIF(XCust.LicenseState,''),RTRCustomer.LicenseState),
						  RTRCustomer.PassportNo=ISNULL(NULLIF(XCust.PassportNo,''),RTRCustomer.PassportNo),
						  RTRCustomer.DOB=ISNULL(NULLIF(XCust.DOB,''),RTRCustomer.DOB),
						  RTRCustomer.TypeOfBusiness=ISNULL(NULLIF(XCust.TypeOfBusiness,''),RTRCustomer.TypeOfBusiness),
						  RTRCustomer.SourceOfFunds=ISNULL(NULLIF(XCust.SourceOfFunds,''),RTRCustomer.SourceOfFunds),
						  RTRCustomer.AccountOfficer=ISNULL(NULLIF(XCust.AccountOfficer,''),RTRCustomer.AccountOfficer),
						  RTRCustomer.AcctOffTel=ISNULL(NULLIF(XCust.AcctOffTel,''),RTRCustomer.AcctOffTel),
						  RTRCustomer.AcctOffEmail=ISNULL(NULLIF(XCust.AcctOffEmail,''),RTRCustomer.AcctOffEmail),
						  RTRCustomer.CompOfficer=ISNULL(NULLIF(XCust.CompOfficer,''),RTRCustomer.CompOfficer),
						  RTRCustomer.CompOffTel=ISNULL(NULLIF(XCust.CompOffTel,''),RTRCustomer.CompOffTel),
						  RTRCustomer.CompOffEmail=ISNULL(NULLIF(XCust.CompOffEmail,''),RTRCustomer.CompOffEmail),
						  RTRCustomer.IdList=ISNULL(NULLIF(XCust.IdList,''),RTRCustomer.IdList),
						  RTRCustomer.Notes=ISNULL(NULLIF(XCust.Notes,''),RTRCustomer.Notes),
						  RTRCustomer.ExemptionStatus=ISNULL(NULLIF(XCust.ExemptionStatus,''),RTRCustomer.ExemptionStatus),
						  RTRCustomer.LastActReset=ISNULL(NULLIF(XCust.LastActReset,''),RTRCustomer.LastActReset),
						  RTRCustomer.LastReview=ISNULL(NULLIF(XCust.LastReview,''),RTRCustomer.LastReview),
						  RTRCustomer.LastReviewOper=ISNULL(NULLIF(XCust.LastReviewOper,''),RTRCustomer.LastReviewOper),
						  RTRCustomer.CreateOper=ISNULL(NULLIF(XCust.CreateOper,''),RTRCustomer.CreateOper),
						  RTRCustomer.CreateDate=ISNULL(NULLIF(XCust.CreateDate,''),RTRCustomer.CreateDate),
						  RTRCustomer.LastOper=ISNULL(NULLIF(XCust.LastOper,''),RTRCustomer.LastOper),
						  RTRCustomer.LastModify=ISNULL(NULLIF(XCust.LastModify,''),RTRCustomer.LastModify),
						  RTRCustomer.OwnerBranch=ISNULL(NULLIF(XCust.OwnerBranch,''),RTRCustomer.OwnerBranch),
						  RTRCustomer.OwnerDept=ISNULL(NULLIF(XCust.OwnerDept,''),RTRCustomer.OwnerDept),
						  RTRCustomer.OwnerOper=ISNULL(NULLIF(XCust.OwnerOper,''),RTRCustomer.OwnerOper),
						  RTRCustomer.RiskClass=ISNULL(NULLIF(XCust.RiskClass,''),RTRCustomer.RiskClass),
						  RTRCustomer.Closed=ISNULL(NULLIF(XCust.Closed,''),RTRCustomer.Closed),
						  RTRCustomer.User1=ISNULL(NULLIF(XCust.User1,''),RTRCustomer.User1),
						  RTRCustomer.User2=ISNULL(NULLIF(XCust.User2,''),RTRCustomer.User2),
						  RTRCustomer.User3=ISNULL(NULLIF(XCust.User3,''),RTRCustomer.User3),
						  RTRCustomer.User4=ISNULL(NULLIF(XCust.User4,''),RTRCustomer.User4),
						  RTRCustomer.User5=ISNULL(NULLIF(XCust.User5,''),RTRCustomer.User5),
						  --RTRCustomer.Ts=ISNULL(NULLIF(XCust.Ts,''),RTRCustomer.Ts),
						  RTRCustomer.CountryOfOrigin=ISNULL(NULLIF(XCust.CountryOfOrigin,''),RTRCustomer.CountryOfOrigin),
						  RTRCustomer.CountryOfIncorp=ISNULL(NULLIF(XCust.CountryOfIncorp,''),RTRCustomer.CountryOfIncorp),
						  RTRCustomer.OffshoreCorp=ISNULL(NULLIF(XCust.OffshoreCorp,''),RTRCustomer.OffshoreCorp),
						  RTRCustomer.BearerShares=ISNULL(NULLIF(XCust.BearerShares,''),RTRCustomer.BearerShares),
						  RTRCustomer.NomeDePlume=ISNULL(NULLIF(XCust.NomeDePlume,''),RTRCustomer.NomeDePlume),
						  RTRCustomer.Type=ISNULL(NULLIF(XCust.CustomerType,''),RTRCustomer.Type),
						  RTRCustomer.Resident=ISNULL(NULLIF(XCust.Resident,''),RTRCustomer.Resident),
						  RTRCustomer.BusRelationNature=ISNULL(NULLIF(XCust.BusRelationNature,''),RTRCustomer.BusRelationNature),
						  RTRCustomer.PrevRelations=ISNULL(NULLIF(XCust.PrevRelations,''),RTRCustomer.PrevRelations),
						  RTRCustomer.PEP=ISNULL(NULLIF(XCust.PEP,''),RTRCustomer.PEP),
						  RTRCustomer.PoliticalPos=ISNULL(NULLIF(XCust.PoliticalPos,''),RTRCustomer.PoliticalPos),
						  RTRCustomer.FEP=ISNULL(NULLIF(XCust.FEP,''),RTRCustomer.FEP),
						  RTRCustomer.MSB=ISNULL(NULLIF(XCust.MSB,''),RTRCustomer.MSB),
						  RTRCustomer.CorrBankRelation=ISNULL(NULLIF(XCust.CorrBankRelation,''),RTRCustomer.CorrBankRelation),
						  RTRCustomer.Sex=ISNULL(NULLIF(XCust.Sex,''),RTRCustomer.Sex),
						  RTRCustomer.ShellBank=ISNULL(NULLIF(XCust.ShellBank,''),RTRCustomer.ShellBank),
						  RTRCustomer.HighRiskRespBank=ISNULL(NULLIF(XCust.HighRiskRespBank,''),RTRCustomer.HighRiskRespBank),
						  RTRCustomer.OffshoreBank=ISNULL(NULLIF(XCust.OffshoreBank,''),RTRCustomer.OffshoreBank),
						  RTRCustomer.PayThroughAC=ISNULL(NULLIF(XCust.PayThroughAC,''),RTRCustomer.PayThroughAC),
						  RTRCustomer.RegulatedAffiliate=ISNULL(NULLIF(XCust.RegulatedAffiliate,''),RTRCustomer.RegulatedAffiliate),
						  RTRCustomer.USPerson=ISNULL(NULLIF(XCust.USPerson,''),RTRCustomer.USPerson),
						  RTRCustomer.RFCalculate=ISNULL(NULLIF(XCust.RFCalculate,''),RTRCustomer.RFCalculate),
						  RTRCustomer.Status=ISNULL(NULLIF(XCust.Status,''),RTRCustomer.Status),
						  RTRCustomer.ReviewDate=ISNULL(NULLIF(XCust.ReviewDate,''),RTRCustomer.ReviewDate),
						  RTRCustomer.CTRAmt=ISNULL(NULLIF(XCust.CTRAmt,''),RTRCustomer.CTRAmt),
						  RTRCustomer.AmtRange=ISNULL(NULLIF(XCust.AmtRange,''),RTRCustomer.AmtRange),
						  RTRCustomer.SwiftTID=ISNULL(NULLIF(XCust.SwiftTID,''),RTRCustomer.SwiftTID),
						  RTRCustomer.Embassy=ISNULL(NULLIF(XCust.Embassy,''),RTRCustomer.Embassy),
						  RTRCustomer.ForeignGovt=ISNULL(NULLIF(XCust.ForeignGovt,''),RTRCustomer.ForeignGovt),
						  RTRCustomer.CharityOrg=ISNULL(NULLIF(XCust.CharityOrg,''),RTRCustomer.CharityOrg),
						  RTRCustomer.DoCIP=ISNULL(NULLIF(XCust.DoCIP,''),RTRCustomer.DoCIP),
						  RTRCustomer.Prospect=ISNULL(NULLIF(XCust.Prospect,''),RTRCustomer.Prospect),
						  RTRCustomer.AssetSize=ISNULL(NULLIF(XCust.AssetSize,''),RTRCustomer.AssetSize),
						  RTRCustomer.Income=ISNULL(NULLIF(XCust.Income,''),RTRCustomer.Income),
						  RTRCustomer.IndOrBusType=ISNULL(NULLIF(XCust.IndOrBusType,''),RTRCustomer.IndOrBusType),
						  RTRCustomer.OnProbation=ISNULL(NULLIF(XCust.OnProbation,''),RTRCustomer.OnProbation),
						  RTRCustomer.ProbationReason=ISNULL(NULLIF(XCust.ProbationReason,''),RTRCustomer.ProbationReason),
						  RTRCustomer.ProbationStartDate=ISNULL(NULLIF(XCust.ProbationStartDate,''),RTRCustomer.ProbationStartDate),
						  RTRCustomer.ProbationEndDate=ISNULL(NULLIF(XCust.ProbationEndDate,''),RTRCustomer.ProbationEndDate),
						  RTRCustomer.ClosedDate=ISNULL(NULLIF(XCust.ClosedDate,''),RTRCustomer.ClosedDate),
						  RTRCustomer.ClosedReason=ISNULL(NULLIF(XCust.ClosedReason,''),RTRCustomer.ClosedReason),
						  RTRCustomer.OpenDate=ISNULL(NULLIF(XCust.OpenDate,''),RTRCustomer.OpenDate),
						  RTRCustomer.CountryOfResidence=ISNULL(NULLIF(XCust.CountryOfResidence,''),RTRCustomer.CountryOfResidence),
						  RTRCustomer.CountryOfCitizenship=ISNULL(NULLIF(XCust.CountryOfCitizenship,''),RTRCustomer.CountryOfCitizenship),
						  RTRCustomer.KYCDataCreateDate=ISNULL(NULLIF(XCust.KYCDataCreateDate,''),RTRCustomer.KYCDataCreateDate),
						  RTRCustomer.KYCDataModifyDate=ISNULL(NULLIF(XCust.KYCDataModifyDate,''),RTRCustomer.KYCDataModifyDate),
						  RTRCustomer.KYCOperator=ISNULL(NULLIF(XCust.KYCOperator,''),RTRCustomer.KYCOperator),
						  RTRCustomer.KYCStatus=ISNULL(NULLIF(XCust.KYCStatus,''),RTRCustomer.KYCStatus)

					  from OpenXml(@index,@PartyInfo,2)
											with (CustId dbo.ObjectId ,Parent dbo.ObjectId ,Name dbo.LongName ,
						DBA dbo.LongName ,SecCode varchar(10) ,Address dbo.Address ,City dbo.City ,State dbo.State ,
						Zip dbo.Zip ,Country dbo.SCode ,Telephone varchar(20) ,Email varchar(60) ,TIN varchar(20) ,
						LicenseNo varchar(35) ,LicenseState dbo.State ,PassportNo varchar(20) ,DOB dbo.GenericDate ,TypeOfBusiness varchar(80) ,
						SourceOfFunds varchar(80) ,AccountOfficer dbo.LongName ,AcctOffTel varchar(20) ,AcctOffEmail varchar(60) ,
						CompOfficer dbo.LongName ,CompOffTel varchar(20) ,CompOffEmail varchar(60) ,IdList varchar(255) ,Notes varchar(2000) ,
						ExemptionStatus dbo.SCode ,LastActReset dbo.GenericTime ,LastReview dbo.GenericTime ,LastReviewOper dbo.SCode ,CreateOper dbo.SCode ,
						CreateDate dbo.CreateTime ,LastOper dbo.SCode ,LastModify dbo.GenericTime ,OwnerBranch dbo.SCode ,OwnerDept dbo.SCode ,OwnerOper dbo.SCode ,
						RiskClass dbo.SCode ,Closed bit ,User1 varchar(40) ,User2 varchar(40) ,User3 varchar(40) ,User4 varchar(40) ,User5 varchar(40) ,--Ts timestamp ,
						CountryOfOrigin dbo.SCode ,CountryOfIncorp dbo.SCode ,OffshoreCorp int ,BearerShares bit ,NomeDePlume bit ,CustomerType dbo.SCode ,Resident bit ,
						BusRelationNature dbo.LongName ,PrevRelations dbo.LongName ,PEP bit ,PoliticalPos dbo.LongName ,FEP bit ,MSB bit ,CorrBankRelation bit ,Sex char(1) ,
						ShellBank bit ,HighRiskRespBank bit ,OffshoreBank bit ,PayThroughAC bit ,RegulatedAffiliate bit ,USPerson dbo.LongName ,RFCalculate bit ,
						Status int ,ReviewDate datetime ,CTRAmt money ,AmtRange varchar(50) ,SwiftTID varchar(11) ,Embassy bit ,ForeignGovt bit ,CharityOrg bit ,DoCIP bit ,
						Prospect bit ,AssetSize money ,Income money ,IndOrBusType char(1) ,OnProbation bit ,ProbationReason dbo.ObjectId ,ProbationStartDate dbo.GenericDate ,
						ProbationEndDate dbo.GenericDate ,ClosedDate dbo.GenericDate ,ClosedReason dbo.ObjectId ,OpenDate dbo.GenericDate ,CountryOfResidence dbo.SCode ,
						CountryOfCitizenship dbo.SCode ,KYCDataCreateDate datetime ,KYCDataModifyDate datetime ,KYCOperator varchar(20) ,KYCStatus int 
						) XCust
							where RTRCustomer.ID = @CustIdDb
							-- if nothing is inserted , return 0
							select @RtrIDOutput = ISNULL(SCOPE_IDENTITY (),0)
						
	
					
						
						-- Fetching the Customer Type of the existing customer from customer table.If it is available then update the RTRCustomer Table with Ongoing risk model value
						-- which is corresponding to the customer type.
						-- Also for the existing customer, Fetching the risk model which is configured for the given customer type.						
						select @CustTypefromCustomer= c.Type from Pbsa.dbo.customer c with (nolock) where c.Id = @CustIdDb
						select @OnriskModel= ct.RFTCode from pbsa.dbo.CustomerType ct with (nolock) where ct.code=@CustTypefromCustomer
						
						
						-- if passing a new real time customer type , than the exisitng one, then use the new customer type passed. Else use the existing configuration.
						if @CustTypefromCustomer is not NULL
							if  @CustTypefromCustomer<>@CustType
								update pbsa.dbo.rtrCustomer set RFTCode=@riskModel,Type=@CustType where Id=@CustIdDb and RFTCode is null 
							else
								update pbsa.dbo.rtrCustomer set RFTCode=@OnriskModel,Type=@CustTypefromCustomer where Id=@CustIdDb and RFTCode is null
							
						-- if the exisiting customer type is null or empty, update it with new customer type passed via web service
						else
							update pbsa.dbo.rtrCustomer set RFTCode=@riskModel,Type=@CustType where Id=@CustIdDb and RFTCode is null
						

						--#Region RTRCustomer updation end
										
						-- update RTRKYCDATA here    -- new KYC fields has to be updated here
							update pbsa.dbo.RTRKYCData 
							 SET RTRKYCData.NamePrefix=ISNULL(NULLIF(Kyc1.NamePrefix,''),RTRKYCData.NamePrefix),
						  RTRKYCData.FirstName=ISNULL(NULLIF(Kyc1.FirstName,''),RTRKYCData.FirstName),
						  RTRKYCData.MiddleName=ISNULL(NULLIF(Kyc1.MiddleName,''),RTRKYCData.MiddleName),
						  RTRKYCData.LastName=ISNULL(NULLIF(Kyc1.LastName,''),RTRKYCData.LastName),
						  RTRKYCData.NameSuffix=ISNULL(NULLIF(Kyc1.NameSuffix,''),RTRKYCData.NameSuffix),
						  RTRKYCData.Alias=ISNULL(NULLIF(Kyc1.Alias,''),RTRKYCData.Alias),
						  RTRKYCData.PlaceofBirth=ISNULL(NULLIF(Kyc1.PlaceofBirth,''),RTRKYCData.PlaceofBirth),
						  RTRKYCData.MothersMaidenName=ISNULL(NULLIF(Kyc1.MothersMaidenName,''),RTRKYCData.MothersMaidenName),
						  RTRKYCData.Introducer=ISNULL(NULLIF(Kyc1.Introducer,''),RTRKYCData.Introducer),
						  RTRKYCData.YrsAtCurrAddress=ISNULL(NULLIF(Kyc1.YrsAtCurrAddress,''),RTRKYCData.YrsAtCurrAddress),
						  RTRKYCData.NoOfDependants=ISNULL(NULLIF(Kyc1.NoOfDependants,''),RTRKYCData.NoOfDependants),
						  RTRKYCData.EmployerName=ISNULL(NULLIF(Kyc1.EmployerName,''),RTRKYCData.EmployerName),
						  RTRKYCData.JobTitle=ISNULL(NULLIF(Kyc1.JobTitle,''),RTRKYCData.JobTitle),
						  RTRKYCData.JobTime=ISNULL(NULLIF(Kyc1.JobTime,''),RTRKYCData.JobTime),
						  RTRKYCData.LengthOfEmp=ISNULL(NULLIF(Kyc1.LengthOfEmp,''),RTRKYCData.LengthOfEmp),
						  RTRKYCData.IsAlien=ISNULL(NULLIF(Kyc1.IsAlien,''),RTRKYCData.IsAlien),
						  RTRKYCData.CountryofDualCitizenship=ISNULL(NULLIF(Kyc1.CountryofDualCitizenship,''),RTRKYCData.CountryofDualCitizenship),
						  RTRKYCData.SecondaryEmployer=ISNULL(NULLIF(Kyc1.SecondaryEmployer,''),RTRKYCData.SecondaryEmployer),
						  RTRKYCData.OriginOfFunds=ISNULL(NULLIF(Kyc1.OriginOfFunds,''),RTRKYCData.OriginOfFunds),
						  RTRKYCData.IsPersonalRelationship=ISNULL(NULLIF(Kyc1.IsPersonalRelationship,''),RTRKYCData.IsPersonalRelationship),
						  RTRKYCData.DateOfInception=ISNULL(NULLIF(Kyc1.DateOfInception,''),RTRKYCData.DateOfInception),
						  RTRKYCData.LegalEntityType=ISNULL(NULLIF(Kyc1.LegalEntityType,''),RTRKYCData.LegalEntityType),
						  RTRKYCData.NoOfEmployees=ISNULL(NULLIF(Kyc1.NoOfEmployees,''),RTRKYCData.NoOfEmployees),
						  RTRKYCData.TotalAnnualSales=ISNULL(NULLIF(Kyc1.TotalAnnualSales,''),RTRKYCData.TotalAnnualSales),
						  RTRKYCData.ProductsSold=ISNULL(NULLIF(Kyc1.ProductsSold,''),RTRKYCData.ProductsSold),
						  RTRKYCData.ServicesProvided=ISNULL(NULLIF(Kyc1.ServicesProvided,''),RTRKYCData.ServicesProvided),
						  RTRKYCData.GeographicalAreasServed=ISNULL(NULLIF(Kyc1.GeographicalAreasServed,''),RTRKYCData.GeographicalAreasServed),
						  RTRKYCData.IsPubliclyTraded=ISNULL(NULLIF(Kyc1.IsPubliclyTraded,''),RTRKYCData.IsPubliclyTraded),
						  RTRKYCData.TickerSymbol=ISNULL(NULLIF(Kyc1.TickerSymbol,''),RTRKYCData.TickerSymbol),
						  RTRKYCData.Exchange=ISNULL(NULLIF(Kyc1.Exchange,''),RTRKYCData.Exchange),
						  RTRKYCData.IsPensionFund=ISNULL(NULLIF(Kyc1.IsPensionFund,''),RTRKYCData.IsPensionFund),
						  RTRKYCData.IsEndowmentFund=ISNULL(NULLIF(Kyc1.IsEndowmentFund,''),RTRKYCData.IsEndowmentFund),
						  RTRKYCData.IsGovernmentSponsored=ISNULL(NULLIF(Kyc1.IsGovernmentSponsored,''),RTRKYCData.IsGovernmentSponsored),
						  RTRKYCData.IsSubsidiary=ISNULL(NULLIF(Kyc1.IsSubsidiary,''),RTRKYCData.IsSubsidiary),
						  RTRKYCData.IsForeignCorrBnk=ISNULL(NULLIF(Kyc1.IsForeignCorrBnk,''),RTRKYCData.IsForeignCorrBnk),
						  RTRKYCData.RequireCashTrans=ISNULL(NULLIF(Kyc1.RequireCashTrans,''),RTRKYCData.RequireCashTrans),
						  RTRKYCData.RequireForeignTrans=ISNULL(NULLIF(Kyc1.RequireForeignTrans,''),RTRKYCData.RequireForeignTrans),
						  RTRKYCData.RequireWireTrans=ISNULL(NULLIF(Kyc1.RequireWireTrans,''),RTRKYCData.RequireWireTrans),
						  RTRKYCData.HasForeignBranches=ISNULL(NULLIF(Kyc1.HasForeignBranches,''),RTRKYCData.HasForeignBranches),
						  RTRKYCData.ForeignBranchCountries=ISNULL(NULLIF(Kyc1.ForeignBranchCountries,''),RTRKYCData.ForeignBranchCountries),
						  RTRKYCData.TypeofMonInstruments=ISNULL(NULLIF(Kyc1.TypeofMonInstruments,''),RTRKYCData.TypeofMonInstruments),
						  RTRKYCData.url=ISNULL(NULLIF(Kyc1.url,''),RTRKYCData.url),
						  RTRKYCData.HasPatriotActCertification=ISNULL(NULLIF(Kyc1.HasPatriotActCertification,''),RTRKYCData.HasPatriotActCertification),
						  RTRKYCData.PatriotActCertExpiryDate=ISNULL(NULLIF(Kyc1.PatriotActCertExpiryDate,''),RTRKYCData.PatriotActCertExpiryDate),
						  RTRKYCData.MoneyOrders=ISNULL(NULLIF(Kyc1.MoneyOrders,''),RTRKYCData.MoneyOrders),
						  RTRKYCData.MORegistration=ISNULL(NULLIF(Kyc1.MORegistration,''),RTRKYCData.MORegistration),
						  RTRKYCData.MORegConfirmationNo=ISNULL(NULLIF(Kyc1.MORegConfirmationNo,''),RTRKYCData.MORegConfirmationNo),
						  RTRKYCData.MOActingasAgentfor=ISNULL(NULLIF(Kyc1.MOActingasAgentfor,''),RTRKYCData.MOActingasAgentfor),
						  RTRKYCData.MOMaxDailyAmtPerPerson=ISNULL(NULLIF(Kyc1.MOMaxDailyAmtPerPerson,''),RTRKYCData.MOMaxDailyAmtPerPerson),
						  RTRKYCData.MOMonthlyAmt=ISNULL(NULLIF(Kyc1.MOMonthlyAmt,''),RTRKYCData.MOMonthlyAmt),
						  RTRKYCData.MOPercentIncome=ISNULL(NULLIF(Kyc1.MOPercentIncome,''),RTRKYCData.MOPercentIncome),
						  RTRKYCData.TravelersCheck=ISNULL(NULLIF(Kyc1.TravelersCheck,''),RTRKYCData.TravelersCheck),
						  RTRKYCData.TCRegistration=ISNULL(NULLIF(Kyc1.TCRegistration,''),RTRKYCData.TCRegistration),
						  RTRKYCData.TCRegConfirmationNo=ISNULL(NULLIF(Kyc1.TCRegConfirmationNo,''),RTRKYCData.TCRegConfirmationNo),
						  RTRKYCData.TCActingasAgentfor=ISNULL(NULLIF(Kyc1.TCActingasAgentfor,''),RTRKYCData.TCActingasAgentfor),
						  RTRKYCData.TCMaxDailyAmtPerPerson=ISNULL(NULLIF(Kyc1.TCMaxDailyAmtPerPerson,''),RTRKYCData.TCMaxDailyAmtPerPerson),
						  RTRKYCData.TCMonthlyAmt=ISNULL(NULLIF(Kyc1.TCMonthlyAmt,''),RTRKYCData.TCMonthlyAmt),
						  RTRKYCData.TCPercentIncome=ISNULL(NULLIF(Kyc1.TCPercentIncome,''),RTRKYCData.TCPercentIncome),
						  RTRKYCData.MoneyTransmission=ISNULL(NULLIF(Kyc1.MoneyTransmission,''),RTRKYCData.MoneyTransmission),
						  RTRKYCData.MTRegistration=ISNULL(NULLIF(Kyc1.MTRegistration,''),RTRKYCData.MTRegistration),
						  RTRKYCData.MTRegConfirmationNo=ISNULL(NULLIF(Kyc1.MTRegConfirmationNo,''),RTRKYCData.MTRegConfirmationNo),
						  RTRKYCData.MTActingasAgentfor=ISNULL(NULLIF(Kyc1.MTActingasAgentfor,''),RTRKYCData.MTActingasAgentfor),
						  RTRKYCData.MTMaxDailyAmtPerPerson=ISNULL(NULLIF(Kyc1.MTMaxDailyAmtPerPerson,''),RTRKYCData.MTMaxDailyAmtPerPerson),
						  RTRKYCData.MTMonthlyAmt=ISNULL(NULLIF(Kyc1.MTMonthlyAmt,''),RTRKYCData.MTMonthlyAmt),
						  RTRKYCData.MTPercentIncome=ISNULL(NULLIF(Kyc1.MTPercentIncome,''),RTRKYCData.MTPercentIncome),
						  RTRKYCData.CheckCashing=ISNULL(NULLIF(Kyc1.CheckCashing,''),RTRKYCData.CheckCashing),
						  RTRKYCData.CCRegistration=ISNULL(NULLIF(Kyc1.CCRegistration,''),RTRKYCData.CCRegistration),
						  RTRKYCData.CCRegConfirmationNo=ISNULL(NULLIF(Kyc1.CCRegConfirmationNo,''),RTRKYCData.CCRegConfirmationNo),
						  RTRKYCData.CCActingasAgentfor=ISNULL(NULLIF(Kyc1.CCActingasAgentfor,''),RTRKYCData.CCActingasAgentfor),
						  RTRKYCData.CCMaxDailyAmtPerPerson=ISNULL(NULLIF(Kyc1.CCMaxDailyAmtPerPerson,''),RTRKYCData.CCMaxDailyAmtPerPerson),
						  RTRKYCData.CCMonthlyAmt=ISNULL(NULLIF(Kyc1.CCMonthlyAmt,''),RTRKYCData.CCMonthlyAmt),
						  RTRKYCData.CCPercentIncome=ISNULL(NULLIF(Kyc1.CCPercentIncome,''),RTRKYCData.CCPercentIncome),
						  RTRKYCData.CurrencyExchange=ISNULL(NULLIF(Kyc1.CurrencyExchange,''),RTRKYCData.CurrencyExchange),
						  RTRKYCData.CERegistration=ISNULL(NULLIF(Kyc1.CERegistration,''),RTRKYCData.CERegistration),
						  RTRKYCData.CERegConfirmationNo=ISNULL(NULLIF(Kyc1.CERegConfirmationNo,''),RTRKYCData.CERegConfirmationNo),
						  RTRKYCData.CEActingasAgentfor=ISNULL(NULLIF(Kyc1.CEActingasAgentfor,''),RTRKYCData.CEActingasAgentfor),
						  RTRKYCData.CEMaxDailyAmtPerPerson=ISNULL(NULLIF(Kyc1.CEMaxDailyAmtPerPerson,''),RTRKYCData.CEMaxDailyAmtPerPerson),
						  RTRKYCData.CEMonthlyAmt=ISNULL(NULLIF(Kyc1.CEMonthlyAmt,''),RTRKYCData.CEMonthlyAmt),
						  RTRKYCData.CEPercentIncome=ISNULL(NULLIF(Kyc1.CEPercentIncome,''),RTRKYCData.CEPercentIncome),
						  RTRKYCData.CurrencyDealing=ISNULL(NULLIF(Kyc1.CurrencyDealing,''),RTRKYCData.CurrencyDealing),
						  RTRKYCData.CDRegistration=ISNULL(NULLIF(Kyc1.CDRegistration,''),RTRKYCData.CDRegistration),
						  RTRKYCData.CDRegConfirmationNo=ISNULL(NULLIF(Kyc1.CDRegConfirmationNo,''),RTRKYCData.CDRegConfirmationNo),
						  RTRKYCData.CDActingasAgentfor=ISNULL(NULLIF(Kyc1.CDActingasAgentfor,''),RTRKYCData.CDActingasAgentfor),
						  RTRKYCData.CDMaxDailyAmtPerPerson=ISNULL(NULLIF(Kyc1.CDMaxDailyAmtPerPerson,''),RTRKYCData.CDMaxDailyAmtPerPerson),
						  RTRKYCData.CDMonthlyAmt=ISNULL(NULLIF(Kyc1.CDMonthlyAmt,''),RTRKYCData.CDMonthlyAmt),
						  RTRKYCData.CDPercentIncome=ISNULL(NULLIF(Kyc1.CDPercentIncome,''),RTRKYCData.CDPercentIncome),
						  RTRKYCData.StoredValue=ISNULL(NULLIF(Kyc1.StoredValue,''),RTRKYCData.StoredValue),
						  RTRKYCData.SVRegistration=ISNULL(NULLIF(Kyc1.SVRegistration,''),RTRKYCData.SVRegistration),
						  RTRKYCData.SVRegConfirmationNo=ISNULL(NULLIF(Kyc1.SVRegConfirmationNo,''),RTRKYCData.SVRegConfirmationNo),
						  RTRKYCData.SVActingasAgentfor=ISNULL(NULLIF(Kyc1.SVActingasAgentfor,''),RTRKYCData.SVActingasAgentfor),
						  RTRKYCData.SVMaxDailyAmtPerPerson=ISNULL(NULLIF(Kyc1.SVMaxDailyAmtPerPerson,''),RTRKYCData.SVMaxDailyAmtPerPerson),
						  RTRKYCData.SVMonthlyAmt=ISNULL(NULLIF(Kyc1.SVMonthlyAmt,''),RTRKYCData.SVMonthlyAmt),
						  RTRKYCData.SVPercentIncome=ISNULL(NULLIF(Kyc1.SVPercentIncome,''),RTRKYCData.SVPercentIncome),
						  RTRKYCData.CDDUser1=ISNULL(NULLIF(Kyc1.CDDUser1,''),RTRKYCData.CDDUser1),
						  RTRKYCData.CDDUser2=ISNULL(NULLIF(Kyc1.CDDUser2,''),RTRKYCData.CDDUser2),
						  RTRKYCData.CDDUser3=ISNULL(NULLIF(Kyc1.CDDUser3,''),RTRKYCData.CDDUser3),
						  RTRKYCData.CDDUser4=ISNULL(NULLIF(Kyc1.CDDUser4,''),RTRKYCData.CDDUser4),
						  RTRKYCData.CDDUser5=ISNULL(NULLIF(Kyc1.CDDUser5,''),RTRKYCData.CDDUser5),
						  RTRKYCData.CDDUser6=ISNULL(NULLIF(Kyc1.CDDUser6,''),RTRKYCData.CDDUser6),
						  RTRKYCData.CDDUser7=ISNULL(NULLIF(Kyc1.CDDUser7,''),RTRKYCData.CDDUser7),
						  RTRKYCData.CDDUser8=ISNULL(NULLIF(Kyc1.CDDUser8,''),RTRKYCData.CDDUser8),
						  RTRKYCData.CDDUser9=ISNULL(NULLIF(Kyc1.CDDUser9,''),RTRKYCData.CDDUser9),
						  RTRKYCData.CDDUser10=ISNULL(NULLIF(Kyc1.CDDUser10,''),RTRKYCData.CDDUser10),
						  RTRKYCData.CDDUser11=ISNULL(NULLIF(Kyc1.CDDUser11,''),RTRKYCData.CDDUser11),
						  RTRKYCData.CDDUser12=ISNULL(NULLIF(Kyc1.CDDUser12,''),RTRKYCData.CDDUser12),
						  RTRKYCData.CDDUser13=ISNULL(NULLIF(Kyc1.CDDUser13,''),RTRKYCData.CDDUser13),
						  RTRKYCData.CDDUser14=ISNULL(NULLIF(Kyc1.CDDUser14,''),RTRKYCData.CDDUser14),
						  RTRKYCData.CDDUser15=ISNULL(NULLIF(Kyc1.CDDUser15,''),RTRKYCData.CDDUser15),
						  RTRKYCData.CDDUser16=ISNULL(NULLIF(Kyc1.CDDUser16,''),RTRKYCData.CDDUser16),
						  RTRKYCData.CDDUser17=ISNULL(NULLIF(Kyc1.CDDUser17,''),RTRKYCData.CDDUser17),
						  RTRKYCData.CDDUser18=ISNULL(NULLIF(Kyc1.CDDUser18,''),RTRKYCData.CDDUser18),
						  RTRKYCData.CDDUser19=ISNULL(NULLIF(Kyc1.CDDUser19,''),RTRKYCData.CDDUser19),
						  RTRKYCData.CDDUser20=ISNULL(NULLIF(Kyc1.CDDUser20,''),RTRKYCData.CDDUser20),
						  RTRKYCData.CDDUser21=ISNULL(NULLIF(Kyc1.CDDUser21,''),RTRKYCData.CDDUser21),
						  RTRKYCData.CDDUser22=ISNULL(NULLIF(Kyc1.CDDUser22,''),RTRKYCData.CDDUser22),
						  RTRKYCData.CDDUser23=ISNULL(NULLIF(Kyc1.CDDUser23,''),RTRKYCData.CDDUser23),
						  RTRKYCData.CDDUser24=ISNULL(NULLIF(Kyc1.CDDUser24,''),RTRKYCData.CDDUser24),
						  RTRKYCData.CDDUser25=ISNULL(NULLIF(Kyc1.CDDUser25,''),RTRKYCData.CDDUser25),
						  RTRKYCData.CDDUser26=ISNULL(NULLIF(Kyc1.CDDUser26,''),RTRKYCData.CDDUser26),
						  RTRKYCData.CDDUser27=ISNULL(NULLIF(Kyc1.CDDUser27,''),RTRKYCData.CDDUser27),
						  RTRKYCData.CDDUser28=ISNULL(NULLIF(Kyc1.CDDUser28,''),RTRKYCData.CDDUser28),
						  RTRKYCData.CDDUser29=ISNULL(NULLIF(Kyc1.CDDUser29,''),RTRKYCData.CDDUser29),
						  RTRKYCData.CDDUser30=ISNULL(NULLIF(Kyc1.CDDUser30,''),RTRKYCData.CDDUser30),
						  RTRKYCData.CDDUser31=ISNULL(NULLIF(Kyc1.CDDUser31,''),RTRKYCData.CDDUser31),
						  RTRKYCData.CDDUser32=ISNULL(NULLIF(Kyc1.CDDUser32,''),RTRKYCData.CDDUser32),
						  RTRKYCData.CDDUser33=ISNULL(NULLIF(Kyc1.CDDUser33,''),RTRKYCData.CDDUser33),
						  RTRKYCData.CDDUser34=ISNULL(NULLIF(Kyc1.CDDUser34,''),RTRKYCData.CDDUser34),
						  RTRKYCData.CDDUser35=ISNULL(NULLIF(Kyc1.CDDUser35,''),RTRKYCData.CDDUser35),
						  RTRKYCData.CDDUser36=ISNULL(NULLIF(Kyc1.CDDUser36,''),RTRKYCData.CDDUser36),
						  RTRKYCData.CDDUser37=ISNULL(NULLIF(Kyc1.CDDUser37,''),RTRKYCData.CDDUser37),
						  RTRKYCData.CDDUser38=ISNULL(NULLIF(Kyc1.CDDUser38,''),RTRKYCData.CDDUser38),
						  RTRKYCData.CDDUser39=ISNULL(NULLIF(Kyc1.CDDUser39,''),RTRKYCData.CDDUser39),
						  RTRKYCData.CDDUser40=ISNULL(NULLIF(Kyc1.CDDUser40,''),RTRKYCData.CDDUser40),
						  -- new fields added below
						  RTRKYCData.RelationshipWithBankAffl=ISNULL(NULLIF(Kyc1.RelWithBankAffiliate,''),RTRKYCData.RelationshipWithBankAffl),
						  RTRKYCData.PrivInvestmentCompany=ISNULL(NULLIF(Kyc1.PrivInvCompany,''),RTRKYCData.PrivInvestmentCompany),
						  RTRKYCData.ResultsOfNegativePressSearch=ISNULL(NULLIF(Kyc1.NegPressSearchResults,''),RTRKYCData.ResultsOfNegativePressSearch),
						  -- new fields end
						  RTRKYCData.CreateOper=ISNULL(NULLIF(Kyc1.CreateOper,''),RTRKYCData.CreateOper),
						  RTRKYCData.CreateDate=ISNULL(NULLIF(Kyc1.CreateDate,''),RTRKYCData.CreateDate),
						  RTRKYCData.LastOper=ISNULL(NULLIF(Kyc1.LastOper,''),RTRKYCData.LastOper),
						  RTRKYCData.LastModify=ISNULL(NULLIF(Kyc1.LastModify,''),RTRKYCData.LastModify)

						  
						from OpenXml(@index,@PartyInfo,2)
						with (CustId dbo.ObjectId ,
						NamePrefix varchar(25) ,FirstName varchar(50) ,MiddleName varchar(50) ,
						LastName varchar(50) ,NameSuffix varchar(25) ,Alias varchar(50) ,
						PlaceofBirth varchar(50) ,MothersMaidenName varchar(50) ,Introducer varchar(50) ,
						YrsAtCurrAddress int ,NoOfDependants int ,EmployerName varchar(50) ,JobTitle varchar(50) ,
						JobTime varchar(50) ,LengthOfEmp varchar(25) ,IsAlien bit ,CountryofDualCitizenship dbo.SCode ,
						SecondaryEmployer varchar(50) ,OriginOfFunds varchar(50) ,IsPersonalRelationship bit ,
						DateOfInception datetime ,LegalEntityType varchar(50) ,NoOfEmployees int ,
						TotalAnnualSales money ,ProductsSold varchar(255) ,ServicesProvided varchar(255) ,
						GeographicalAreasServed varchar(255) ,IsPubliclyTraded bit ,TickerSymbol varchar(10) ,
						Exchange varchar(50) ,IsPensionFund bit ,IsEndowmentFund bit ,IsGovernmentSponsored bit ,
						IsSubsidiary bit ,IsForeignCorrBnk bit ,RequireCashTrans bit ,RequireForeignTrans bit ,
						RequireWireTrans bit ,HasForeignBranches bit ,ForeignBranchCountries varchar(255) ,TypeofMonInstruments varchar(255) ,
						url varchar(255) ,HasPatriotActCertification bit ,PatriotActCertExpiryDate datetime ,MoneyOrders bit ,
						MORegistration varchar(10) ,MORegConfirmationNo varchar(50) ,MOActingasAgentfor varchar(20) ,MOMaxDailyAmtPerPerson money ,
						MOMonthlyAmt money ,MOPercentIncome varchar(5) ,TravelersCheck bit ,TCRegistration varchar(10) ,TCRegConfirmationNo varchar(50) ,
						TCActingasAgentfor varchar(20) ,TCMaxDailyAmtPerPerson money ,TCMonthlyAmt money ,TCPercentIncome varchar(5) ,MoneyTransmission bit ,
						MTRegistration varchar(10) ,MTRegConfirmationNo varchar(50) ,MTActingasAgentfor varchar(20) ,MTMaxDailyAmtPerPerson money ,
						MTMonthlyAmt money ,MTPercentIncome varchar(5) ,CheckCashing bit ,CCRegistration varchar(10) ,CCRegConfirmationNo varchar(50) ,
						CCActingasAgentfor varchar(20) ,CCMaxDailyAmtPerPerson money ,CCMonthlyAmt money ,CCPercentIncome varchar(5) ,CurrencyExchange bit ,
						CERegistration varchar(10) ,CERegConfirmationNo varchar(50) ,CEActingasAgentfor varchar(20) ,CEMaxDailyAmtPerPerson money ,CEMonthlyAmt money ,
						CEPercentIncome varchar(5) ,CurrencyDealing bit ,CDRegistration varchar(10) ,CDRegConfirmationNo varchar(50) ,CDActingasAgentfor varchar(20) ,
						CDMaxDailyAmtPerPerson money ,CDMonthlyAmt money ,CDPercentIncome varchar(5) ,StoredValue bit ,SVRegistration varchar(10) ,
						SVRegConfirmationNo varchar(50) ,SVActingasAgentfor varchar(20) ,SVMaxDailyAmtPerPerson money ,SVMonthlyAmt money ,SVPercentIncome varchar(5) ,CDDUser1 varchar(40) ,
						CDDUser2 varchar(40) ,CDDUser3 varchar(40) ,CDDUser4 varchar(40) ,CDDUser5 varchar(40) ,CDDUser6 varchar(40) ,
						CDDUser7 varchar(40) ,CDDUser8 varchar(40) ,CDDUser9 varchar(40) ,CDDUser10 varchar(40) ,
						CDDUser11 varchar(40) ,CDDUser12 varchar(40) ,CDDUser13 varchar(40) ,CDDUser14 varchar(40) ,
						CDDUser15 varchar(40) ,CDDUser16 varchar(40) ,CDDUser17 varchar(40) ,CDDUser18 varchar(40) ,
						CDDUser19 varchar(40) ,CDDUser20 varchar(40) ,CDDUser21 varchar(40) ,CDDUser22 varchar(40) ,CDDUser23 varchar(40) ,CDDUser24 varchar(40) ,
						CDDUser25 varchar(40) ,CDDUser26 varchar(40) ,CDDUser27 varchar(40) ,CDDUser28 varchar(40) ,CDDUser29 varchar(40) ,
						CDDUser30 varchar(40) ,CDDUser31 varchar(1000) ,CDDUser32 varchar(1000) ,CDDUser33 varchar(1000) ,CDDUser34 varchar(1000) ,CDDUser35 varchar(1000) ,
						CDDUser36 varchar(1000) ,CDDUser37 varchar(1000) ,CDDUser38 varchar(1000) ,CDDUser39 varchar(1000) ,CDDUser40 varchar(3000), 
						-- new RTRKYCDATA fields
						RelWithBankAffiliate bit ,PrivInvCompany bit ,NegPressSearchResults varchar(255),
						-- new RTRKYCDATA fields end
						CreateOper dbo.SCode ,
						CreateDate dbo.CreateTime ,LastOper dbo.SCode ,LastModify dbo.GenericTime ) Kyc1
						where RTRKYCData.CustomerId = @CustIdDb
					    -- Updation of RTRKYCDATA End		
	
				
		end	else --# NEW Customer : if custId is not  existing Customer table, then insert real time data from XML 
		begin 
	
					insert into pbsa.dbo.RTRCustomer (Id,Name,Parent,DBA,
					SecCode,[Address],City,[State],	Zip,Country,Telephone,Email,TIN,
					LicenseNo,LicenseState,PassportNo,DOB,TypeOfBusiness,SourceOfFunds,AccountOfficer,
					AcctOffTel,AcctOffEmail,CompOfficer,CompOffTel,	CompOffEmail,IdList,Notes,ExemptionStatus,LastActReset,
					LastReview,LastReviewOper,CreateOper,CreateDate,LastOper,LastModify,OwnerBranch,OwnerDept,OwnerOper,RiskClass,Closed,
					User1,User2,User3,User4,User5,CountryOfOrigin,CountryOfIncorp,	OffshoreCorp,BearerShares,NomeDePlume,Type,Resident,
					BusRelationNature,PrevRelations,PEP,PoliticalPos,FEP,MSB,CorrBankRelation,Sex,ShellBank,HighRiskRespBank,
					OffshoreBank,PayThroughAC,RegulatedAffiliate,USPerson,RFCalculate,[Status],ReviewDate,CTRAmt,AmtRange,SwiftTID,
					Embassy,ForeignGovt,CharityOrg,DoCIP,Prospect,AssetSize,Income,IndOrBusType,OnProbation,ProbationReason,ProbationStartDate,
					ProbationEndDate,ClosedDate,ClosedReason,OpenDate,CountryOfResidence,CountryOfCitizenship,KYCDataCreateDate,KYCDataModifyDate,KYCOperator,KYCStatus)
					select  CustId,Name,Parent,DBA,	SecCode,[Address],City,[State],	Zip,Country,Telephone,Email,TIN,
					LicenseNo,LicenseState,PassportNo,DOB,	TypeOfBusiness,SourceOfFunds,AccountOfficer,
					AcctOffTel,AcctOffEmail,CompOfficer,CompOffTel,	CompOffEmail,IdList,Notes,ExemptionStatus,LastActReset,
					LastReview,LastReviewOper,CreateOper,CreateDate,LastOper,LastModify,OwnerBranch,OwnerDept,OwnerOper,isnull(RiskClass,'DefClass'),isnull(Closed,0),
					User1,User2,User3,User4,User5,CountryOfOrigin,CountryOfIncorp,OffshoreCorp,BearerShares,NomeDePlume,CustomerType,Resident,
					BusRelationNature,PrevRelations,PEP,PoliticalPos,FEP,MSB,CorrBankRelation,Sex,ShellBank,HighRiskRespBank,
					OffshoreBank,PayThroughAC,RegulatedAffiliate,USPerson,RFCalculate,[Status],ReviewDate,CTRAmt,AmtRange,SwiftTID,
					Embassy,ForeignGovt,CharityOrg,DoCIP,Prospect,AssetSize,Income,IndOrBusType,OnProbation,ProbationReason,ProbationStartDate,
					ProbationEndDate,ClosedDate,ClosedReason,OpenDate,CountryOfResidence,CountryOfCitizenship,KYCDataCreateDate,KYCDataModifyDate,KYCOperator,KYCStatus
					from openxml(@index,@PartyInfo,2)
					with
					(	CustId dbo.ObjectId,Parent dbo.ObjectId ,Name dbo.LongName ,
						DBA dbo.LongName ,SecCode varchar(10) ,Address dbo.Address ,City dbo.City ,
						State dbo.State ,Zip dbo.Zip ,Country dbo.SCode ,Telephone varchar(20) ,Email varchar(60) ,
						TIN varchar(20) ,LicenseNo varchar(35) ,LicenseState dbo.State ,PassportNo varchar(20) ,DOB dbo.GenericDate ,
						TypeOfBusiness varchar(80) ,SourceOfFunds varchar(80) ,AccountOfficer dbo.LongName ,AcctOffTel varchar(20) ,
						AcctOffEmail varchar(60) ,CompOfficer dbo.LongName ,CompOffTel varchar(20) ,CompOffEmail varchar(60) ,
						IdList varchar(255) ,Notes varchar(2000) ,ExemptionStatus dbo.SCode ,LastActReset dbo.GenericTime ,
						LastReview dbo.GenericTime ,LastReviewOper dbo.SCode ,CreateOper dbo.SCode ,CreateDate dbo.CreateTime ,
						LastOper dbo.SCode ,LastModify dbo.GenericTime ,OwnerBranch dbo.SCode ,OwnerDept dbo.SCode ,
						OwnerOper dbo.SCode ,RiskClass dbo.SCode ,Closed bit ,
						User1 varchar(40) ,User2 varchar(40) ,User3 varchar(40) ,
						User4 varchar(40) ,User5 varchar(40) ,CountryOfOrigin dbo.SCode ,CountryOfIncorp dbo.SCode ,
						OffshoreCorp int ,BearerShares bit ,NomeDePlume bit ,CustomerType dbo.SCode ,Resident bit ,BusRelationNature dbo.LongName ,
						PrevRelations dbo.LongName ,PEP bit ,PoliticalPos dbo.LongName ,FEP bit ,MSB bit ,CorrBankRelation bit ,Sex char(1) ,
						ShellBank bit ,HighRiskRespBank bit ,OffshoreBank bit ,PayThroughAC bit ,RegulatedAffiliate bit ,USPerson dbo.LongName ,
						RFCalculate bit ,Status int ,ReviewDate datetime ,CTRAmt money ,AmtRange varchar(50) ,SwiftTID varchar(11) ,
						Embassy bit ,ForeignGovt bit ,CharityOrg bit ,DoCIP bit ,Prospect bit ,AssetSize money ,Income money ,IndOrBusType char(1) ,
						OnProbation bit ,ProbationReason dbo.ObjectId ,ProbationStartDate dbo.GenericDate ,ProbationEndDate dbo.GenericDate ,
						ClosedDate dbo.GenericDate ,ClosedReason dbo.ObjectId ,OpenDate dbo.GenericDate ,CountryOfResidence dbo.SCode ,
						CountryOfCitizenship dbo.SCode ,KYCDataCreateDate datetime ,KYCDataModifyDate datetime ,KYCOperator varchar(20) ,KYCStatus int )
						
						select @RtrIDOutput = SCOPE_IDENTITY ()
						
						-- Updating the RiskModel for new customers.This value is coming from the webservice web config file
						update pbsa.dbo.rtrCustomer set RFTCode=@riskModel where Type=@CustType and RFTCode is null
						
						-- insert new values to RTRKYCData	-- added new KYC fields here
						--print 'Given CuromerId is not exist in database, so Inserting New Data to RTRKYCData from XML'
						INSERT INTO pbsa.dbo.RTRKYCData 
						(CustomerId,NamePrefix,FirstName,MiddleName,LastName,NameSuffix,Alias,PlaceofBirth,MothersMaidenName,Introducer,YrsAtCurrAddress,
						NoOfDependants,EmployerName,JobTitle,JobTime,LengthOfEmp,IsAlien,CountryofDualCitizenship,SecondaryEmployer,OriginOfFunds,IsPersonalRelationship,DateOfInception,
						LegalEntityType,NoOfEmployees,TotalAnnualSales,ProductsSold,ServicesProvided,GeographicalAreasServed,IsPubliclyTraded,TickerSymbol,Exchange,IsPensionFund,
						IsEndowmentFund,IsGovernmentSponsored,IsSubsidiary,IsForeignCorrBnk,RequireCashTrans,RequireForeignTrans,RequireWireTrans,HasForeignBranches,ForeignBranchCountries,
						TypeofMonInstruments,url,HasPatriotActCertification,PatriotActCertExpiryDate,MoneyOrders,MORegistration,MORegConfirmationNo,MOActingasAgentfor,MOMaxDailyAmtPerPerson,MOMonthlyAmt,
						MOPercentIncome,TravelersCheck,TCRegistration,TCRegConfirmationNo,TCActingasAgentfor,TCMaxDailyAmtPerPerson,TCMonthlyAmt,TCPercentIncome,MoneyTransmission,
						MTRegistration,MTRegConfirmationNo,MTActingasAgentfor,MTMaxDailyAmtPerPerson,MTMonthlyAmt,MTPercentIncome,CheckCashing,CCRegistration,CCRegConfirmationNo,
						CCActingasAgentfor,CCMaxDailyAmtPerPerson,CCMonthlyAmt,CCPercentIncome,CurrencyExchange,CERegistration,CERegConfirmationNo,CEActingasAgentfor,CEMaxDailyAmtPerPerson,
						CEMonthlyAmt,CEPercentIncome,CurrencyDealing,CDRegistration,CDRegConfirmationNo,CDActingasAgentfor,CDMaxDailyAmtPerPerson,CDMonthlyAmt,CDPercentIncome,StoredValue,
						SVRegistration,SVRegConfirmationNo,SVActingasAgentfor,SVMaxDailyAmtPerPerson,SVMonthlyAmt,SVPercentIncome,CDDUser1,CDDUser2,CDDUser3,CDDUser4,CDDUser5,CDDUser6,
						CDDUser7,CDDUser8,CDDUser9,CDDUser10,CDDUser11,CDDUser12,CDDUser13,CDDUser14,CDDUser15,CDDUser16,CDDUser17,CDDUser18,CDDUser19,CDDUser20,CDDUser21,CDDUser22,
						CDDUser23,CDDUser24,CDDUser25,CDDUser26,CDDUser27,CDDUser28,CDDUser29,CDDUser30,CDDUser31,CDDUser32,CDDUser33,CDDUser34,CDDUser35,CDDUser36,CDDUser37,CDDUser38,
						CDDUser39,CDDUser40,RelationshipWithBankAffl,PrivInvestmentCompany,ResultsOfNegativePressSearch,CreateOper,CreateDate,LastOper,LastModify)
						
						SELECT CustId,NamePrefix,FirstName,MiddleName,LastName,NameSuffix,Alias,PlaceofBirth,MothersMaidenName,Introducer,YrsAtCurrAddress,
						NoOfDependants,EmployerName,JobTitle,JobTime,LengthOfEmp,IsAlien,CountryofDualCitizenship,SecondaryEmployer,OriginOfFunds,IsPersonalRelationship,DateOfInception,
						LegalEntityType,NoOfEmployees,TotalAnnualSales,ProductsSold,ServicesProvided,GeographicalAreasServed,IsPubliclyTraded,TickerSymbol,Exchange,IsPensionFund,
						IsEndowmentFund,IsGovernmentSponsored,IsSubsidiary,IsForeignCorrBnk,RequireCashTrans,RequireForeignTrans,RequireWireTrans,HasForeignBranches,ForeignBranchCountries,
						TypeofMonInstruments,url,HasPatriotActCertification,PatriotActCertExpiryDate,MoneyOrders,MORegistration,MORegConfirmationNo,MOActingasAgentfor,MOMaxDailyAmtPerPerson,MOMonthlyAmt,
						MOPercentIncome,TravelersCheck,TCRegistration,TCRegConfirmationNo,TCActingasAgentfor,TCMaxDailyAmtPerPerson,TCMonthlyAmt,TCPercentIncome,MoneyTransmission,
						MTRegistration,MTRegConfirmationNo,MTActingasAgentfor,MTMaxDailyAmtPerPerson,MTMonthlyAmt,MTPercentIncome,CheckCashing,CCRegistration,CCRegConfirmationNo,
						CCActingasAgentfor,CCMaxDailyAmtPerPerson,CCMonthlyAmt,CCPercentIncome,CurrencyExchange,CERegistration,CERegConfirmationNo,CEActingasAgentfor,CEMaxDailyAmtPerPerson,
						CEMonthlyAmt,CEPercentIncome,CurrencyDealing,CDRegistration,CDRegConfirmationNo,CDActingasAgentfor,CDMaxDailyAmtPerPerson,CDMonthlyAmt,CDPercentIncome,StoredValue,
						SVRegistration,SVRegConfirmationNo,SVActingasAgentfor,SVMaxDailyAmtPerPerson,SVMonthlyAmt,SVPercentIncome,CDDUser1,CDDUser2,CDDUser3,CDDUser4,CDDUser5,CDDUser6,
						CDDUser7,CDDUser8,CDDUser9,CDDUser10,CDDUser11,CDDUser12,CDDUser13,CDDUser14,CDDUser15,CDDUser16,CDDUser17,CDDUser18,CDDUser19,CDDUser20,CDDUser21,CDDUser22,
						CDDUser23,CDDUser24,CDDUser25,CDDUser26,CDDUser27,CDDUser28,CDDUser29,CDDUser30,CDDUser31,CDDUser32,CDDUser33,CDDUser34,CDDUser35,CDDUser36,CDDUser37,CDDUser38,
						CDDUser39,CDDUser40,RelWithBankAffiliate,PrivInvCompany,NegPressSearchResults,CreateOper,CreateDate,LastOper,LastModify
						
						from openxml(@index,@PartyInfo,2)
								with (CustId dbo.ObjectId ,
						NamePrefix varchar(25) ,FirstName varchar(50) ,MiddleName varchar(50) ,
						LastName varchar(50) ,NameSuffix varchar(25) ,Alias varchar(50) ,
						PlaceofBirth varchar(50) ,MothersMaidenName varchar(50) ,Introducer varchar(50) ,
						YrsAtCurrAddress int ,NoOfDependants int ,EmployerName varchar(50) ,JobTitle varchar(50) ,
						JobTime varchar(50) ,LengthOfEmp varchar(25) ,IsAlien bit ,CountryofDualCitizenship dbo.SCode ,
						SecondaryEmployer varchar(50) ,OriginOfFunds varchar(50) ,IsPersonalRelationship bit ,
						DateOfInception datetime ,LegalEntityType varchar(50) ,NoOfEmployees int ,
						TotalAnnualSales money ,ProductsSold varchar(255) ,ServicesProvided varchar(255) ,
						GeographicalAreasServed varchar(255) ,IsPubliclyTraded bit ,TickerSymbol varchar(10) ,
						Exchange varchar(50) ,IsPensionFund bit ,IsEndowmentFund bit ,IsGovernmentSponsored bit ,
						IsSubsidiary bit ,IsForeignCorrBnk bit ,RequireCashTrans bit ,RequireForeignTrans bit ,
						RequireWireTrans bit ,HasForeignBranches bit ,ForeignBranchCountries varchar(255) ,TypeofMonInstruments varchar(255) ,
						url varchar(255) ,HasPatriotActCertification bit ,PatriotActCertExpiryDate datetime ,MoneyOrders bit ,
						MORegistration varchar(10) ,MORegConfirmationNo varchar(50) ,MOActingasAgentfor varchar(20) ,MOMaxDailyAmtPerPerson money ,
						MOMonthlyAmt money ,MOPercentIncome varchar(5) ,TravelersCheck bit ,TCRegistration varchar(10) ,TCRegConfirmationNo varchar(50) ,
						TCActingasAgentfor varchar(20) ,TCMaxDailyAmtPerPerson money ,TCMonthlyAmt money ,TCPercentIncome varchar(5) ,MoneyTransmission bit ,
						MTRegistration varchar(10) ,MTRegConfirmationNo varchar(50) ,MTActingasAgentfor varchar(20) ,MTMaxDailyAmtPerPerson money ,
						MTMonthlyAmt money ,MTPercentIncome varchar(5) ,CheckCashing bit ,CCRegistration varchar(10) ,CCRegConfirmationNo varchar(50) ,
						CCActingasAgentfor varchar(20) ,CCMaxDailyAmtPerPerson money ,CCMonthlyAmt money ,CCPercentIncome varchar(5) ,CurrencyExchange bit ,
						CERegistration varchar(10) ,CERegConfirmationNo varchar(50) ,CEActingasAgentfor varchar(20) ,CEMaxDailyAmtPerPerson money ,CEMonthlyAmt money ,
						CEPercentIncome varchar(5) ,CurrencyDealing bit ,CDRegistration varchar(10) ,CDRegConfirmationNo varchar(50) ,CDActingasAgentfor varchar(20) ,
						CDMaxDailyAmtPerPerson money ,CDMonthlyAmt money ,CDPercentIncome varchar(5) ,StoredValue bit ,SVRegistration varchar(10) ,
						SVRegConfirmationNo varchar(50) ,SVActingasAgentfor varchar(20) ,SVMaxDailyAmtPerPerson money ,SVMonthlyAmt money ,SVPercentIncome varchar(5) ,CDDUser1 varchar(40) ,
						CDDUser2 varchar(40) ,CDDUser3 varchar(40) ,CDDUser4 varchar(40) ,CDDUser5 varchar(40) ,CDDUser6 varchar(40) ,
						CDDUser7 varchar(40) ,CDDUser8 varchar(40) ,CDDUser9 varchar(40) ,CDDUser10 varchar(40) ,
						CDDUser11 varchar(40) ,CDDUser12 varchar(40) ,CDDUser13 varchar(40) ,CDDUser14 varchar(40) ,
						CDDUser15 varchar(40) ,CDDUser16 varchar(40) ,CDDUser17 varchar(40) ,CDDUser18 varchar(40) ,
						CDDUser19 varchar(40) ,CDDUser20 varchar(40) ,CDDUser21 varchar(40) ,CDDUser22 varchar(40) ,CDDUser23 varchar(40) ,CDDUser24 varchar(40) ,
						CDDUser25 varchar(40) ,CDDUser26 varchar(40) ,CDDUser27 varchar(40) ,CDDUser28 varchar(40) ,CDDUser29 varchar(40) ,
						CDDUser30 varchar(40) ,CDDUser31 varchar(1000) ,CDDUser32 varchar(1000) ,CDDUser33 varchar(1000) ,CDDUser34 varchar(1000) ,CDDUser35 varchar(1000) ,
						CDDUser36 varchar(1000) ,CDDUser37 varchar(1000) ,CDDUser38 varchar(1000) ,CDDUser39 varchar(1000) ,CDDUser40 varchar(3000) ,
						RelWithBankAffiliate bit ,	PrivInvCompany bit ,	NegPressSearchResults varchar(255) ,
						CreateOper dbo.SCode ,CreateDate dbo.CreateTime ,LastOper dbo.SCode ,LastModify dbo.GenericTime )
						
		end
		
					-- For Existing Accounts
					INSERT INTO pbsa.dbo.RTRAccount(Id,Monitor,CloseDate,
						CloseReason,Type,Reference,	Request,PolicyAmount,ExpiryDate,
						FreeLookEndDate,CashValue,SinglePremium,BalanceAmt,	SourceofPayments,
						AcStatus,InitialDeposit,TypeOfFunds,ReqWireTrans,
						AccACHPay,OrgACHPay,NoOfDepositMnth,TotDepositAmtMnth,
						NoOfCashDepositMnth,TotCashDepositAmtMnth,
						NoOfCashWithDMnth,TotCashWithDAmtMnth,
						NoOfCheckWMnth,	TotCheckWAmtMnth,NoOfDomInWireMnth,
						TotDomInWireAmtMnth,NoOfDomOutWireMnth,
						TotDomOutWireAmtMnth,ReqForInWireTrans,
						NoOfForInWireMnth,TotForInWireAmtMnth,
						OrigCntryForInWires,ReqForOutWireTrans,
						NoOfForOutWireMnth,	TotForOutWireAmtMnth,
						DestCntryForOutWires,IsProspect,AccountRelationship,
						User1,User2,User3,User4,User5,OwnerBranch,OwnerDept,OwnerOper,ExemptionStatus,
						Notes,Branch,Closed,AccountOfficer,RiskFactor,
						-- new RTRAccount fields added below
						DescOfAcctPurpose,PowerOfAttorney,RemoteDepositCapture,RemoteDepositCaptureRiskRating
						)
						select Id,Monitor,CloseDate,
						CloseReason,Type,Reference,	Request,PolicyAmount,ExpiryDate,
						FreeLookEndDate,CashValue,SinglePremium,BalanceAmt,	SourceofPayments,
						AcStatus,InitialDeposit,TypeOfFunds,ReqWireTrans,
						AccACHPay,OrgACHPay,NoOfDepositMnth,TotDepositAmtMnth,
						NoOfCashDepositMnth,TotCashDepositAmtMnth,
						NoOfCashWithDMnth,TotCashWithDAmtMnth,
						NoOfCheckWMnth,	TotCheckWAmtMnth,NoOfDomInWireMnth,
						TotDomInWireAmtMnth,NoOfDomOutWireMnth,
						TotDomOutWireAmtMnth,ReqForInWireTrans,
						NoOfForInWireMnth,TotForInWireAmtMnth,
						OrigCntryForInWires,ReqForOutWireTrans,
						NoOfForOutWireMnth,	TotForOutWireAmtMnth,
						DestCntryForOutWires,IsProspect,AccountOwner.Relationship,
						User1,User2,User3,User4,User5,OwnerBranch,OwnerDept,OwnerOper,ExemptionStatus,
						Notes,Branch,Closed,AccountOfficer,RiskFactor,
						-- new Account fields added below
						DescOfAcctPurpose,PowerOfAttorney,RemoteDepositCapture,RemoteDepositCaptureRiskRating
						 from pbsa.dbo.Account  with (nolock)inner join pbsa.dbo.AccountOwner   with (nolock) 
						on Account.Id=AccountOwner.Account	where Account.Id in (select Id from  #RTRAccount WITH (NOLOCK)) and @isClosed <> 1
					-- Inserting to Account End
					-- Update RtrAccount Begin
						UPDATE  RTRAccount  SET 				
						RTRAccount.Monitor=ISNULL(NULLIF(XAcct.Monitor,''),RTRAccount.Monitor),
						RTRAccount.CloseDate=ISNULL(NULLIF(XAcct.CloseDate,''),RTRAccount.CloseDate),
						RTRAccount.CloseReason=ISNULL(NULLIF(XAcct.CloseReason,''),RTRAccount.CloseReason),
						RTRAccount.Type=ISNULL(NULLIF(XAcct.Type,''),RTRAccount.Type),
						RTRAccount.Reference=ISNULL(NULLIF(XAcct.Reference,''),RTRAccount.Reference),
						RTRAccount.Request=ISNULL(NULLIF(XAcct.Request,''),RTRAccount.Request),
						RTRAccount.PolicyAmount=ISNULL(NULLIF(XAcct.PolicyAmount,''),RTRAccount.PolicyAmount),
						RTRAccount.ExpiryDate=ISNULL(NULLIF(XAcct.ExpiryDate,''),RTRAccount.ExpiryDate),
						RTRAccount.FreeLookEndDate=ISNULL(NULLIF(XAcct.FreeLookEndDate,''),RTRAccount.FreeLookEndDate),
						RTRAccount.CashValue=ISNULL(NULLIF(XAcct.CashValue,''),RTRAccount.CashValue),
						RTRAccount.SinglePremium=ISNULL(NULLIF(XAcct.SinglePremium,''),RTRAccount.SinglePremium),
						RTRAccount.BalanceAmt=ISNULL(NULLIF(XAcct.BalanceAmt,''),RTRAccount.BalanceAmt),
						RTRAccount.SourceofPayments=ISNULL(NULLIF(XAcct.SourceofPayments,''),RTRAccount.SourceofPayments),
						RTRAccount.AcStatus=ISNULL(NULLIF(XAcct.AcStatus,''),RTRAccount.AcStatus),
						RTRAccount.InitialDeposit=ISNULL(NULLIF(XAcct.InitialDeposit,''),RTRAccount.InitialDeposit),
						RTRAccount.TypeOfFunds=ISNULL(NULLIF(XAcct.TypeOfFunds,''),RTRAccount.TypeOfFunds),
						RTRAccount.ReqWireTrans=ISNULL(NULLIF(XAcct.ReqWireTrans,''),RTRAccount.ReqWireTrans),
						RTRAccount.AccACHPay=ISNULL(NULLIF(XAcct.AccACHPay,''),RTRAccount.AccACHPay),
						RTRAccount.OrgACHPay=ISNULL(NULLIF(XAcct.OrgACHPay,''),RTRAccount.OrgACHPay),
						RTRAccount.NoOfDepositMnth=ISNULL(NULLIF(XAcct.NoOfDepositMnth,''),RTRAccount.NoOfDepositMnth),
						RTRAccount.TotDepositAmtMnth=ISNULL(NULLIF(XAcct.TotDepositAmtMnth,''),RTRAccount.TotDepositAmtMnth),
						RTRAccount.NoOfCashDepositMnth=ISNULL(NULLIF(XAcct.NoOfCashDepositMnth,''),RTRAccount.NoOfCashDepositMnth),
						RTRAccount.TotCashDepositAmtMnth=ISNULL(NULLIF(XAcct.TotCashDepositAmtMnth,''),RTRAccount.TotCashDepositAmtMnth),
						RTRAccount.NoOfCashWithDMnth=ISNULL(NULLIF(XAcct.NoOfCashWithDMnth,''),RTRAccount.NoOfCashWithDMnth),
						RTRAccount.TotCashWithDAmtMnth=ISNULL(NULLIF(XAcct.TotCashWithDAmtMnth,''),RTRAccount.TotCashWithDAmtMnth),
						RTRAccount.NoOfCheckWMnth=ISNULL(NULLIF(XAcct.NoOfCheckWMnth,''),RTRAccount.NoOfCheckWMnth),
						RTRAccount.TotCheckWAmtMnth=ISNULL(NULLIF(XAcct.TotCheckWAmtMnth,''),RTRAccount.TotCheckWAmtMnth),
						RTRAccount.NoOfDomInWireMnth=ISNULL(NULLIF(XAcct.NoOfDomInWireMnth,''),RTRAccount.NoOfDomInWireMnth),
						RTRAccount.TotDomInWireAmtMnth=ISNULL(NULLIF(XAcct.TotDomInWireAmtMnth,''),RTRAccount.TotDomInWireAmtMnth),
						RTRAccount.NoOfDomOutWireMnth=ISNULL(NULLIF(XAcct.NoOfDomOutWireMnth,''),RTRAccount.NoOfDomOutWireMnth),
						RTRAccount.TotDomOutWireAmtMnth=ISNULL(NULLIF(XAcct.TotDomOutWireAmtMnth,''),RTRAccount.TotDomOutWireAmtMnth),
						RTRAccount.ReqForInWireTrans=ISNULL(NULLIF(XAcct.ReqForInWireTrans,''),RTRAccount.ReqForInWireTrans),
						RTRAccount.NoOfForInWireMnth=ISNULL(NULLIF(XAcct.NoOfForInWireMnth,''),RTRAccount.NoOfForInWireMnth),
						RTRAccount.TotForInWireAmtMnth=ISNULL(NULLIF(XAcct.TotForInWireAmtMnth,''),RTRAccount.TotForInWireAmtMnth),
						RTRAccount.OrigCntryForInWires=ISNULL(NULLIF(XAcct.OrigCntryForInWires,''),RTRAccount.OrigCntryForInWires),
						RTRAccount.ReqForOutWireTrans=ISNULL(NULLIF(XAcct.ReqForOutWireTrans,''),RTRAccount.ReqForOutWireTrans),
						RTRAccount.NoOfForOutWireMnth=ISNULL(NULLIF(XAcct.NoOfForOutWireMnth,''),RTRAccount.NoOfForOutWireMnth),
						RTRAccount.TotForOutWireAmtMnth=ISNULL(NULLIF(XAcct.TotForOutWireAmtMnth,''),RTRAccount.TotForOutWireAmtMnth),
						RTRAccount.DestCntryForOutWires=ISNULL(NULLIF(XAcct.DestCntryForOutWires,''),RTRAccount.DestCntryForOutWires),
						RTRAccount.IsProspect=ISNULL(NULLIF(XAcct.IsProspect,''),RTRAccount.IsProspect),
						RTRAccount.AccountRelationship=ISNULL(NULLIF(XAcct.AccountRelationship,''),RTRAccount.AccountRelationship),
						RTRAccount.User1=ISNULL(NULLIF(XAcct.User1,''),RTRAccount.User1),
						RTRAccount.User2=ISNULL(NULLIF(XAcct.User2,''),RTRAccount.User2),
						RTRAccount.User3=ISNULL(NULLIF(XAcct.User3,''),RTRAccount.User3),
						RTRAccount.User4=ISNULL(NULLIF(XAcct.User4,''),RTRAccount.User4),
						RTRAccount.User5=ISNULL(NULLIF(XAcct.User5,''),RTRAccount.User5),
						RTRAccount.OwnerBranch=ISNULL(NULLIF(XAcct.OwnerBranch,''),RTRAccount.OwnerBranch),
						RTRAccount.OwnerDept=ISNULL(NULLIF(XAcct.OwnerDept,''),RTRAccount.OwnerDept),
						RTRAccount.OwnerOper=ISNULL(NULLIF(XAcct.OwnerOper,''),RTRAccount.OwnerOper),
						RTRAccount.ExemptionStatus=ISNULL(NULLIF(XAcct.ExemptionStatus,''),RTRAccount.ExemptionStatus),
						RTRAccount.Notes=ISNULL(NULLIF(XAcct.Notes,''),RTRAccount.Notes),
						RTRAccount.Branch=ISNULL(NULLIF(XAcct.Branch,''),RTRAccount.Branch),
						RTRAccount.Closed=ISNULL(NULLIF(XAcct.Closed,''),RTRAccount.Closed),
						RTRAccount.AccountOfficer=ISNULL(NULLIF(XAcct.AccountOfficer,''),RTRAccount.AccountOfficer),

						-- New RTRAccount fields added below
						RTRAccount.DescOfAcctPurpose=ISNULL(NULLIF(XAcct.AccountPurpose,''),RTRAccount.DescOfAcctPurpose),
						RTRAccount.PowerOfAttorney=ISNULL(NULLIF(XAcct.POA,''),RTRAccount.PowerOfAttorney),
						RTRAccount.RemoteDepositCapture=ISNULL(NULLIF(XAcct.RemoteDepCapture,''),RTRAccount.RemoteDepositCapture),
						RTRAccount.RemoteDepositCaptureRiskRating=ISNULL(NULLIF(XAcct.RemoteDepCaptureRiskRating,''),RTRAccount.RemoteDepositCaptureRiskRating)

						from OpenXml(@index,@AccountInfo,2)
						with (Id varchar(35),
								Monitor bit,
								CloseDate datetime  ,
								CloseReason varchar(255),
								Type varchar(11),
								Reference varchar(100),
								Request bit,
								PolicyAmount money,
								ExpiryDate datetime  ,
								FreeLookEndDate datetime  ,
								CashValue money,
								SinglePremium bit,
								BalanceAmt money,
								SourceofPayments varchar(250),
								AcStatus varchar(35),
								InitialDeposit money,
								TypeOfFunds varchar(25),
								ReqWireTrans bit,
								AccACHPay bit,
								OrgACHPay bit,
								NoOfDepositMnth int,
								TotDepositAmtMnth money,
								NoOfCashDepositMnth int,
								TotCashDepositAmtMnth money,
								NoOfCashWithDMnth int,
								TotCashWithDAmtMnth money,
								NoOfCheckWMnth int,
								TotCheckWAmtMnth money,
								NoOfDomInWireMnth int,
								TotDomInWireAmtMnth money,
								NoOfDomOutWireMnth int,
								TotDomOutWireAmtMnth money,
								ReqForInWireTrans bit,
								NoOfForInWireMnth int,
								TotForInWireAmtMnth money,
								OrigCntryForInWires varchar(1000),
								ReqForOutWireTrans bit,
								NoOfForOutWireMnth int,
								TotForOutWireAmtMnth money,
								DestCntryForOutWires varchar(1000),
								IsProspect bit ,AccountRelationship varchar(11),	
								User1  varchar(40),
								User2  varchar(40),
								User3  varchar(40),
								User4  varchar(40),
								User5  varchar(40),
								OwnerBranch  varchar(11),
								OwnerDept  varchar(11),
								OwnerOper  varchar(11),
								ExemptionStatus  varchar(11),
								Notes  varchar(500),
								Branch  varchar(11),
								Closed  bit,
								AccountOfficer  varchar(40),
								RiskFactor  int,									        
								AccountPurpose varchar(255),
								POA bit,
								RemoteDepCapture bit,
								RemoteDepCaptureRiskRating varchar(40)
								)XAcct
								where XAcct.id=RTRAccount.id
					
					
					-- Update RtrAccount End
					
					-- For New Accounts
						INSERT INTO pbsa.dbo.RTRAccount(Id,Monitor,CloseDate,
						CloseReason,Type,Reference,	Request,PolicyAmount,ExpiryDate,
						FreeLookEndDate,CashValue,SinglePremium,BalanceAmt,	SourceofPayments,
						AcStatus,InitialDeposit,TypeOfFunds,ReqWireTrans,
						AccACHPay,OrgACHPay,NoOfDepositMnth,TotDepositAmtMnth,
						NoOfCashDepositMnth,TotCashDepositAmtMnth,
						NoOfCashWithDMnth,TotCashWithDAmtMnth,
						NoOfCheckWMnth,	TotCheckWAmtMnth,NoOfDomInWireMnth,
						TotDomInWireAmtMnth,NoOfDomOutWireMnth,
						TotDomOutWireAmtMnth,ReqForInWireTrans,
						NoOfForInWireMnth,TotForInWireAmtMnth,
						OrigCntryForInWires,ReqForOutWireTrans,
						NoOfForOutWireMnth,	TotForOutWireAmtMnth,
						DestCntryForOutWires,IsProspect,
						User1,User2,User3,User4,User5,OwnerBranch,OwnerDept,OwnerOper,ExemptionStatus,
						Notes,Branch,Closed,AccountOfficer,RiskFactor,
						AccountRelationship,						
						-- new RTRAccount Fields added below
						DescOfAcctPurpose,PowerOfAttorney,RemoteDepositCapture,RemoteDepositCaptureRiskRating
						)
						SELECT Id,Monitor,CloseDate,
						CloseReason,Type,Reference,	Request,PolicyAmount,ExpiryDate,
						FreeLookEndDate,CashValue,SinglePremium,BalanceAmt,	SourceofPayments,
						AcStatus,InitialDeposit,TypeOfFunds,ReqWireTrans,
						AccACHPay,OrgACHPay,NoOfDepositMnth,TotDepositAmtMnth,
						NoOfCashDepositMnth,TotCashDepositAmtMnth,
						NoOfCashWithDMnth,TotCashWithDAmtMnth,
						NoOfCheckWMnth,	TotCheckWAmtMnth,NoOfDomInWireMnth,
						TotDomInWireAmtMnth,NoOfDomOutWireMnth,
						TotDomOutWireAmtMnth,ReqForInWireTrans,
						NoOfForInWireMnth,TotForInWireAmtMnth,
						OrigCntryForInWires,ReqForOutWireTrans,
						NoOfForOutWireMnth,	TotForOutWireAmtMnth,
						DestCntryForOutWires,IsProspect,
						User1,User2,User3,User4,User5,OwnerBranch,OwnerDept,OwnerOper,ExemptionStatus,
						Notes,Branch,isnull(Closed,0),AccountOfficer,RiskFactor,
						AccountRelationship,AccountPurpose,POA,RemoteDepCapture,RemoteDepCaptureRiskRating from OpenXml(@index,@AccountInfo,2)
						WITH (Id varchar(35),
								Monitor bit,
								CloseDate datetime   ,
								CloseReason varchar(255),
								Type varchar(11),
								Reference varchar(100),
								Request bit,
								PolicyAmount money,
								ExpiryDate datetime   ,
								FreeLookEndDate datetime   ,
								CashValue money,
								SinglePremium bit,
								BalanceAmt money,
								SourceofPayments varchar(250),
								AcStatus varchar(35),
								InitialDeposit money,
								TypeOfFunds varchar(25),
								ReqWireTrans bit,
								AccACHPay bit,
								OrgACHPay bit,
								NoOfDepositMnth int,
								TotDepositAmtMnth money,
								NoOfCashDepositMnth int,
								TotCashDepositAmtMnth money,
								NoOfCashWithDMnth int,
								TotCashWithDAmtMnth money,
								NoOfCheckWMnth int,
								TotCheckWAmtMnth money,
								NoOfDomInWireMnth int,
								TotDomInWireAmtMnth money,
								NoOfDomOutWireMnth int,
								TotDomOutWireAmtMnth money,
								ReqForInWireTrans bit,
								NoOfForInWireMnth int,
								TotForInWireAmtMnth money,
								OrigCntryForInWires varchar(1000),
								ReqForOutWireTrans bit,
								NoOfForOutWireMnth int,
								TotForOutWireAmtMnth money,
								DestCntryForOutWires varchar(1000),
								IsProspect bit,
								AccountRelationship varchar(11),
								User1  varchar(40),
								User2  varchar(40),
								User3  varchar(40),
								User4  varchar(40),
								User5  varchar(40),
								OwnerBranch  varchar(11),
								OwnerDept  varchar(11),
								OwnerOper  varchar(11),
								ExemptionStatus  varchar(11),
								Notes  varchar(500),
								Branch  varchar(11),
								Closed  bit,
								AccountOfficer  varchar(40),
								RiskFactor  int,
								-- new RTRAccount Fields added below
								AccountPurpose varchar(255),
								POA bit,
								RemoteDepCapture bit,
								RemoteDepCaptureRiskRating varchar(40))XacctNew
								-- new RTRAccount fields end
 						        where  Id  is NULL or Id NOT IN (select Id from pbsa.dbo.Account with(nolock))
 				
					
		
		select @RTRCust= RTRID from pbsa.dbo.RTRCustomer with (nolock)
	
		
	    if exists( select RTRCustId from pbsa.dbo.RTRAccount where RTRCustId is null)
			update pbsa.dbo.RTRAccount set RTRCustId=@RTRCust
				
		update pbsa.dbo.RTRAccount set Id=NULL where id =''
	
		INSERT INTO PBSA.DBO.RTRAccountOwner(Account,Cust,Relationship) select RTRID,RTRCustId,AccountRelationship from pbsa.dbo.RTRAccount with (nolock)				
		WHERE RTRCustId=@RTRCust


		drop table #RTRAccount
GO


