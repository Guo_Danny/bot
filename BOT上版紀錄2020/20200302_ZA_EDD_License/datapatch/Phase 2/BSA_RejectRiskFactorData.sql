USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_RejectRiskFactorData]    Script Date: 7/7/2020 6:18:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[BSA_RejectRiskFactorData]( 
				@Cust OBJECTID,
				@RiskClass SCODE, 
				@Reason VARCHAR(200),
				@Oper SCODE
				)
  AS

	-- Start standard stored procedure transaction header

	DECLARE @trnCnt int
	SELECT @trnCnt = @@trancount	-- Save the current trancount

	--20200513 add, begin
	declare @Mint int 
	--20200513 add, end
	
	IF @trnCnt = 0
		BEGIN TRAN BSA_RejectRiskFactorData  -- Transaction has not begun
	ELSE	
		SAVE TRAN BSA_RejectRiskFactorData    -- Already in a transaction

	-- End standard stored procedure transaction header
  
	DECLARE	@stat 	INT,
			@cnt 	INT,
			@dt		DateTime,
			@txt	VARCHAR(6000),
			@logtxt VARCHAR(6000),
			@oldRiskClass SCode,
			@oldLastModify SCode,
			@LastReview	DateTime

	IF EXISTS (SELECT 1 FROM RiskFactorData with (nolock) WHERE cust = @Cust AND Status = 0)
		BEGIN 
			
			set @dt = GetDate()

			--Set Last Review time after customer's LastModify time
			set @LastReview = DateAdd(s, 10, @dt)

			If dbo.BSA_CountMissingScores(@Cust) > 0 Begin
			Select @stat = 250010   -- DB_INV_PARAM
			GoTo EndOfProc
			End

			/*
				 Delete Old accepted scores other than the ones
				 recently accepted, for this customer
			*/
			Delete RiskFactorData
					Where Cust = @Cust And Status = 1

			SELECT @oldRiskClass = RiskClass FROM RiskScoreHist with (nolock)
					WHERE CustomerId = @Cust AND DateAccepted IS NULL
			
			--update proposed record from the Risk Score History	
			--update 2020.07 update for ZA override policy start
			UPDATE RiskScoreHist SET RiskClass = @RiskClass ,
										RiskScore = NULL, 
										AcceptOper = @Oper,
										--DateAccepted = @LastReview
										DateAccepted =NULL
										WHERE CustomerId = @Cust 
										AND DateAccepted IS NULL

			--Update RiskFactorData SET  Status = 1,		-- Reviewed/Accepted
			--							LastOper = @Oper,
			--							LastReview = @LastReview,
			--							LastModify = @LastReview
			--							WHERE Cust = @Cust AND Status = 0
			
			--2020.07 ZA asked to trigger the risk rating code value, if they override the risk class.
			update KYCData set CDDUser25 = '10' where CustomerId = @Cust
			
			
			--update 2020.07 update for ZA override policy End

			SELECT @stat = @@error, @cnt = @@rowcount
					IF @stat <> 0
						BEGIN
							GOTO EndOfProc
						END
					ELSE IF @cnt = 0 
						BEGIN
							SELECT @stat = 250006	-- DB_UNKNOWN
							GOTO EndOfProc
					END

			/**************************************************
				Assign the RiskClass to the Customer
				after modifying RiskFactorData to avoid
				conflicts with locked risk class actions
			**************************************************/
		    --20200513 add get riskclass of Probation days,begin
			/*
		    select @Mint=probationdays from RiskClass where code=@RiskClass
		    --20200513 add get riskclass of Probation days,end
			
			--20200514 add for customer update lastreview, reviewdate
			UPDATE Customer SET RiskClass = @RiskClass, 
								LastOper = @Oper, 
								LastModify = @dt, LastReview=@dt, ReviewDate=dateadd(M,@Mint,@dt)								
								WHERE [ID] = @Cust
			
			--Check if customer was updated
			SELECT @stat = @@error, @cnt = @@rowcount
					IF @stat <> 0
						BEGIN
							GOTO EndOfProc
						END
					ELSE IF @cnt = 0 
						BEGIN
							SELECT @stat = 250006    -- DB_UNKNOWN
							GOTO EndOfProc --exit
						End
			*/

		END
	ELSE
		BEGIN
			SELECT @stat = 250006	-- Object does not exist -- DB_UNKNOWN
		END

EndOfProc:

	IF ( @stat = 0 and @cnt = 0 )
		SELECT @stat = 250001 -- concurent update
	IF ( @stat <> 0 ) 
		BEGIN 
			ROLLBACK TRAN BSA_RejectRiskFactorData
			RETURN @stat
		END	
	SELECT @logtxt = 'Risk Class Overridden'
	SELECT @txt	= 'RiskClass Proposed: ' + CONVERT(VARCHAR,ISNULL(@oldRiskClass,'NULL')) + '| RiskClass Overridden: ' + CONVERT(VARCHAR,ISNULL(@RiskClass,'NULL'))+
				  ' | Reason: ' + @Reason 

	EXEC BSA_InsEvent @Oper, 'Mod', 'Cust', @Cust,@logtxt, @txt

	IF @trnCnt = 0
		COMMIT TRAN BSA_RejectRiskFactorData
	RETURN @stat
GO


