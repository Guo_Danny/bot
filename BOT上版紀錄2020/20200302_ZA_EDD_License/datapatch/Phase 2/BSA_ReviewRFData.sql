USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_ReviewRFData]    Script Date: 7/14/2020 2:56:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER Procedure [dbo].[BSA_ReviewRFData] ( @Cust ObjectID, @oper SCode)
    As
 
	-- Start standard stored procedure transaction header
	declare @trnCnt int
	select @trnCnt = @@trancount	-- Save the current trancount
	--20200513 add
	declare @Mint int 
	--20200513 add

	If @trnCnt = 0
		-- Transaction has not begun
		begin tran BSA_ReviewRFData
	else
		-- Already in a transaction
		save tran BSA_ReviewRFData
	-- End standard stored procedure transaction header
	
	Declare @cnt 		int,
			@dt			DateTime,
			@Score 		int,
			@stat		int,
			@RiskClass	SCode,
			@txt		varchar(200)

	If Exists (Select * From RiskFactorData
			    Where Cust = @Cust And Status = 0) Begin

		-- Exit if any records found with non-initialized Score
		If dbo.BSA_CountMissingScores(@Cust) > 0 Begin
			Select @stat = 250010   -- DB_INV_PARAM
			GoTo EndOfProc
		End

		-- Find out score and RiskClass
		Select @Score = RiskScore,
		       @RiskClass = RiskClass
		from RiskScoreHist
		where CustomerId = @Cust and DateAccepted is null
		
		If @Score Is Null Or @RiskClass Is Null Begin
			Select @stat = 250010   -- DB_INV_PARAM
			Goto EndOfProc
		End
	
		-- Delete Old accepted scores other than the ones
		--   recently accepted, for this customer
		Delete RiskFactorData
		 Where Cust = @Cust And Status = 1

		--Update Risk Score History
		update RiskScoreHist
		set DateAccepted= GetDate(),
			AcceptOper = @oper 
		where CustomerId = @Cust and DateAccepted is null
		
		-- Update Review Fields
		Select @dt = GetDate()
		Update RiskFactorData
		   Set Status = 1,			-- Reviewed/Accepted
			   LastReview = @dt,
			   LastReviewOper = @oper
		 Where Cust = @Cust And Status = 0
		Select @stat = @@error, @cnt = @@rowcount
		If @stat <> 0
			Goto EndOfProc
		Else If @cnt = 0 Begin
			Select @stat = 250006	-- DB_UNKNOWN
			Goto EndOfProc
		End

		--20200513 add get riskclass of Probation days
		select @Mint=probationdays from RiskClass where code=@riskclass

		-- Assign the RiskClass to the Customer
		-- after modifying RiskFactorData
		-- to avoid conflicts with locked risk class actions
		-- 20200513 polly add assign lastreview, reviewdate
		Update Customer
		   Set RiskClass = @RiskClass, 
			   LastOper = @oper, LastModify = GetDate(), LastReview=GetDate(),ReviewDate=dateadd(M,@Mint,GETDATE())
		 Where [ID] = @Cust

		--Check if customer was updated
		Select @stat = @@error, 
			   @cnt = @@rowcount
		If @stat <> 0
		  Begin
			Goto EndOfProc
	      End
		Else If @cnt = 0 
		  Begin
			Select @stat = 250006 -- Not found
			Goto EndOfProc --exit
		  End

	End Else 
		Select @stat = 250006	-- DB_OBJECT_NOT_EXIST

EndOfProc: 
	-- Evaluate results of the transaction
	if (@stat = 0) begin
		If @trnCnt = 0
			commit tran BSA_ReviewRFData

		Select @txt = 'Reviewed Risk Data of ' + @Cust
		Exec BSA_InsEvent @oper, 'Rev', 'RFData', @Cust, @txt
	end else 
		rollback tran BSA_ReviewRFData
	
	return @stat
GO


