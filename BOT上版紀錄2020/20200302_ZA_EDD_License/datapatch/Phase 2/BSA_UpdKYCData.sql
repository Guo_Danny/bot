USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_UpdKYCData]    Script Date: 7/1/2020 5:06:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER Procedure [dbo].[BSA_UpdKYCData] (@CustomerId ObjectID, @NamePrefix varchar(25),   @FirstName varchar(50),
              @MiddleName varchar(50),   @LastName varchar(50),            @NameSuffix varchar(25),
              @Alias varchar(50),        @PlaceofBirth varchar(50), @MothersMaidenName varchar(50),
              @Introducer varchar(50),   @YrsAtCurrAddress int,            @NoOfDependants int,
              @EmployerName varchar(50), @JobTitle varchar(50),            @JobTime varchar(50),
              @LengthOfEmp varchar(50),  @IsAlien bit,              @CountryofDualCitizenship SCODE,
              @SecondaryEmployer varchar(50),   @OriginOfFunds varchar(50), @IsPersonalRelationship bit,
              @DateOfInception datetime, @LegalEntityType varchar(50),     @NoofEmployees int,
              @TotalAnnualSales money,   @ProductsSold varchar(255), @ServicesProvided varchar(255),
              @GeographicalAreasServed varchar(255),@IsPubliclyTraded bit,  @TickerSymbol varchar(10),
              @Exchange varchar(50),            @IsPensionFund bit,        @IsEndowmentFund bit,
              @IsGovernmentSponsored bit, @IsSubsidiary bit,         @IsForeignCorrBnk bit,
              @RequireCashTrans bit,            @RequireForeignTrans bit,  @RequireWireTrans bit,
              @HasForeignBranches bit,   @ForeignBranchCountries varchar(255),@TypeofMonInstruments varchar(255),
              @URL varchar(255),         @HasPatriotActCertification bit,@PatriotActCertExpiryDate datetime,
              @MoneyOrders bit,          @MORegistration varchar(10),      @MORegConfirmationNo varchar(50),
              @MOActingasAgentfor varchar(20),@MOMaxDailyAmtPerPerson money,       @MOMonthlyAmt money,
              @MOPercentIncome varchar(5),      @TravelersCheck bit,       @TCRegistration varchar(10),
              @TCRegConfirmationNo varchar(50),@TCActingasAgentfor varchar(20),@TCMaxDailyAmtPerPerson money,
              @TCMonthlyAmt money,       @TCPercentIncome varchar(5),      @MoneyTransmission bit,
              @MTRegistration varchar(10),      @MTRegConfirmationNo varchar(50),@MTActingasAgentfor varchar(20),
              @MTMaxDailyAmtPerPerson money,    @MTMonthlyAmt money,       @MTPercentIncome varchar(5),
              @CheckCashing bit,         @CCRegistration varchar(10),      @CCRegConfirmationNo varchar(50),
              @CCActingasAgentfor varchar(20),@CCMaxDailyAmtPerPerson money,       @CCMonthlyAmt money,
              @CCPercentIncome varchar(5),      @CurrencyExchange bit,            @CERegistration varchar(10),
              @CERegConfirmationNo varchar(50),@CEActingasAgentfor varchar(20),@CEMaxDailyAmtPerPerson money,
              @CEMonthlyAmt money,       @CEPercentIncome varchar(5),      @CurrencyDealing bit,
              @CDRegistration varchar(10),      @CDRegConfirmationNo varchar(50),@CDActingasAgentfor varchar(20),
              @CDMaxDailyAmtPerPerson money,    @CDMonthlyAmt money,       @CDPercentIncome varchar(5),
              @StoredValue bit,          @SVRegistration varchar(10),      @SVRegConfirmationNo varchar(50),
              @SVActingasAgentfor varchar(20),@SVMaxDailyAmtPerPerson money,       @SVMonthlyAmt money,
              @SVPercentIncome varchar(5),   @CDDUser1 varchar(40),                @CDDUser2 varchar(40),
              @CDDUser3 varchar(40),         @CDDUser4 varchar(40),         @CDDUser5 varchar(40),         
              @CDDUser6 varchar(40),         @CDDUser7 varchar(40),         @CDDUser8 varchar(40),         
              @CDDUser9 varchar(40),         @CDDUser10 varchar(40),        @CDDUser11 varchar(40),         
              @CDDUser12 varchar(40),        @CDDUser13 varchar(40),        @CDDUser14 varchar(40),         
              @CDDUser15 varchar(40),        @CDDUser16 varchar(40),        @CDDUser17 varchar(40),         
              @CDDUser18 varchar(40),        @CDDUser19 varchar(40),        @CDDUser20 varchar(40),         
              @CDDUser21 varchar(40),        @CDDUser22 varchar(40),        @CDDUser23 varchar(40),         
              @CDDUser24 varchar(40),        @CDDUser25 varchar(40),        @CDDUser26 varchar(40),         
              @CDDUser27 varchar(40),        @CDDUser28 varchar(40),        @CDDUser29 varchar(40),         
              @CDDUser30 varchar(40),        @CDDUser31 varchar(1000),              @CDDUser32 varchar(1000),         
              @CDDUser33 varchar(1000),       @CDDUser34 varchar(1000),              @CDDUser35 varchar(1000),         
              @CDDUser36 varchar(1000),       @CDDUser37 varchar(1000),              @CDDUser38 varchar(1000),         
              @CDDUser39 varchar(1000),       @CDDUser40 varchar(3000),              @LastOper Scode, 
              @ts timestamp,
              --     Begin: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --      yw
              @RelationshipWithBankAffl bit ,                               -- Relationship with Bank Affiliate
              @PrivInvestmentCompany bit ,                                  -- Private Investiment Company
              @ResultsOfNegativePressSearch varchar(255)      -- Results of Negative Press Search           
              --     End: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --      yw     
)
  As
       -- Start standard stored procedure transaction header
       declare @trnCnt int
       select @trnCnt = @@trancount  -- Save the current trancount
       If @trnCnt = 0
              -- Transaction has not begun
              begin tran BSA_UpdKYCData
       else
       -- Already in a transaction
              save tran BSA_UpdKYCData
       -- End standard stored procedure transaction header

       declare     @stat int,
                           @cnt int
       --2019.04 update for CDD Begin
   declare @item_Channel varchar(50), @item_TypeProd varchar(50), @item_Country varchar(50), 
   @item1 varchar(50), @item2 varchar(50), @item3 varchar(50), @item4 varchar(50), @item5 varchar(50), 
   @type varchar(50), 
   @RFICODE_Channel varchar(50), @RFICODE_TypeProd varchar(50), @RFICODE_Country varchar(50);


   select @type = [type] from Customer where id = @CustomerId

       if isnull(RTrim(@type),'') = 'I' or isnull(RTrim(@type),'') = ''
       begin
              set @RFICODE_Channel = 'oiTxChannel';
              set @RFICODE_TypeProd = 'oiTypeProd';
              set @RFICODE_Country = 'oiTxCountry';
    end

       if isnull(RTrim(@type),'') = 'E'
       begin
              set @RFICODE_Channel = 'oeTxChannel';
              set @RFICODE_TypeProd = 'oeTypeProd';
              set @RFICODE_Country = 'oeTxCountry';
    end

       if isnull(RTrim(@type),'') = 'B'
       begin
              set @RFICODE_Channel = 'oeTxChannel';
              set @RFICODE_TypeProd = 'oeTypeProd';
              set @RFICODE_Country = 'oeTxCountry';
    end
       
       --Channel
       set @item1=NullIf(RTrim(@CDDUser14) ,'')
	   set @item2=NullIf(RTrim(@CDDUser15) ,'')
	   set @item3=NullIf(RTrim(@CDDUser16) ,'')

	   select @item1 
       select @item2
       select @item3

       select @item_Channel=value FROM  RiskFactorValue WHERE RFICODE=@RFICODE_Channel                      --需要改成不同的Code: oiTxChannel , OiTypeProd, oiTxCountry (第2碼i是CUST TYPE (FIS), if e: 代表Entity)
       and value in (@item1, @item2, @item3) and score in ( select max(score) FROM  RiskFactorValue 
       WHERE RFICODE=@RFICODE_Channel                    --需要改成不同的Code: oiTxChannel , OiTypeProd, oiTxCountry (第2碼i是CUST TYPE (FIS), if e: 代表Entity)
       and value in (@item1, @item2, @item3) 
       )

       --TypeProd
       set @item1=NullIf(RTrim(@CDDUser2) ,'')
	   set @item2=NullIf(RTrim(@CDDUser3) ,'')
	   set @item3=NullIf(RTrim(@CDDUser4) ,'')
	   set @item4=NullIf(RTrim(@CDDUser5) ,'')
	   set @item5=NullIf(RTrim(@CDDUser6) ,'')

	   select @item1 
       select @item2
       select @item3
	   select @item4
	   select @item5

       select @item_TypeProd=value FROM  RiskFactorValue WHERE RFICODE=@RFICODE_TypeProd                      --需要改成不同的Code: oiTxChannel , OiTypeProd, oiTxCountry (第2碼i是CUST TYPE (FIS), if e: 代表Entity)
       and value in (@item1, @item2, @item3, @item4, @item5) and score in ( select max(score) FROM  RiskFactorValue 
       WHERE RFICODE=@RFICODE_TypeProd                     --需要改成不同的Code: oiTxChannel , OiTypeProd, oiTxCountry (第2碼i是CUST TYPE (FIS), if e: 代表Entity)
       and value in (@item1, @item2, @item3, @item4, @item5) 
       )

       --Country
       set @item1=NullIf(RTrim(@CDDUser8) ,'')
	   set @item2=NullIf(RTrim(@CDDUser9) ,'')
	   set @item3=NullIf(RTrim(@CDDUser10) ,'')
	   set @item4=NullIf(RTrim(@CDDUser11) ,'')
	   set @item5=NullIf(RTrim(@CDDUser12) ,'')

	   select @item1 
       select @item2
       select @item3
	   select @item4
	   select @item5

       select @item_Country=value FROM  RiskFactorValue WHERE RFICODE=@RFICODE_Country                      --需要改成不同的Code: oiTxChannel , OiTypeProd, oiTxCountry (第2碼i是CUST TYPE (FIS), if e: 代表Entity)
       and value in (@item1, @item2, @item3, @item4, @item5) and score in ( select max(score) FROM  RiskFactorValue 
       WHERE RFICODE=@RFICODE_Country                     --需要改成不同的Code: oiTxChannel , OiTypeProd, oiTxCountry (第2碼i是CUST TYPE (FIS), if e: 代表Entity)
       and value in (@item1, @item2, @item3, @item4, @item5) 
       )

       --2019.04 update for CDD End

	   --2020.07 modify for mlco status

	   if @CDDUser25 = '10'
	   begin
		set @ispersonalrelationship = 1
	   end

	   --2020.07 modify for mlco status End

    Update KYCData Set NamePrefix = NullIf(RTrim(@nameprefix),''),
                     FirstName=NullIf(RTrim(@firstname),'') ,
                     MiddleName=NullIf(RTrim(@middlename),'') ,
                     LastName=NullIf(RTrim(@lastname),'') ,
                     NameSuffix= NullIf(RTrim(@namesuffix),''),
                     Alias=NullIf(RTrim(@alias),'') ,
                     PlaceofBirth=NullIf(RTrim(@placeofbirth),'') ,
                     MothersMaidenName=NullIf(RTrim(@mothersmaidenname),'') ,
                     Introducer = NullIf(RTrim(@Introducer),''),
                     YrsAtCurrAddress=NullIf(@yrsatcurraddress,0),
                     NoOfDependants=NullIf(@noofdependants,0) ,
                     EmployerName = NullIf(RTrim(@employername),''),
                     JobTitle=NullIf(RTrim(@jobtitle),'') ,
                     JobTime=NullIf(RTrim(@jobtime),'') ,
                     LengthOfEmp= NullIf(RTrim(@LengthOfEmp),''),                  
                     IsAlien=@isalien,
                     CountryofDualCitizenship=NullIf(RTrim(@countryofdualcitizenship),''),
                     SecondaryEmployer=NullIf(RTrim(@secondaryemployer),'') ,
                     OriginOfFunds=NullIf(RTrim(@OriginOfFunds),'') ,
                     IsPersonalRelationship =@ispersonalrelationship,
                     DateOfInception=@dateofinception,
                     LegalEntityType=NullIf(RTrim(@legalentitytype),'') ,
                     NoofEmployees=@noofemployees,
                     TotalAnnualSales= @totalannualsales,
                     ProductsSold=NullIf(RTrim(@productssold), '') ,
                     ServicesProvided=NullIf(RTrim(@servicesprovided), '') ,
                     GeographicalAreasServed=NullIf(RTrim(@geographicalareasserved), '') ,
                     IsPubliclyTraded=@IsPubliclyTraded ,
                     TickerSymbol=NullIf(RTrim(@tickersymbol), '') ,
                     Exchange=NullIf(RTrim(@exchange), '') ,
                     IsPensionFund=@ispensionfund ,
                     IsEndowmentFund=@isendowmentfund ,
                     IsGovernmentSponsored=@isgovernmentsponsored,
                     IsSubsidiary=@issubsidiary,
                     IsForeignCorrBnk=@isforeigncorrbnk ,
                     RequireCashTrans=@requirecashtrans ,
                     RequireForeignTrans=@requireforeigntrans ,
                     RequireWireTrans=@requirewiretrans ,
                     HasForeignBranches=@hasforeignbranches,
                     ForeignBranchCountries=NullIf(RTrim(@ForeignBranchCountries), '') ,
                     TypeofMonInstruments=NullIf(RTrim(@TypeofMonInstruments), ''),
                     URL=NullIf(RTrim(@URL), ''),
                     HasPatriotActCertification=@HasPatriotActCertification ,
                     PatriotActCertExpiryDate=@PatriotActCertExpiryDate ,
                     MoneyOrders=@moneyorders ,
                     MORegistration=NullIf(RTrim(@MORegistration), '') ,
                     MORegConfirmationNo=NullIf(RTrim(@MORegConfirmationNo), '') ,
                     MOActingasAgentfor=NullIf(RTrim(@MOActingasAgentfor), ''),
                     MOMaxDailyAmtPerPerson=@MOMaxDailyAmtPerPerson ,
                     MOMonthlyAmt=@MOMonthlyAmt ,
                     MOPercentIncome=NullIf(RTrim(@MOPercentIncome), ''),
                     TravelersCheck=@TravelersCheck,
                     TCRegistration=NullIf(RTrim(@TCRegistration), ''),
                     TCRegConfirmationNo=NullIf(RTrim(@TCRegConfirmationNo), '') ,
                     TCActingasAgentfor=NullIf(RTrim(@TCActingasAgentfor), '') ,
                     TCMaxDailyAmtPerPerson=@TCMaxDailyAmtPerPerson ,
                     TCMonthlyAmt=@TCMonthlyAmt ,
                     TCPercentIncome=NullIf(RTrim(@TCPercentIncome), ''),
                     MoneyTransmission=@MoneyTransmission ,
                     MTRegistration=NullIf(RTrim(@MTRegistration), ''),
                     MTRegConfirmationNo=NullIf(RTrim(@MTRegConfirmationNo), '') ,
                     MTActingasAgentfor =NullIf(RTrim(@MTActingasAgentfor), ''),
                     MTMaxDailyAmtPerPerson=@MTMaxDailyAmtPerPerson ,
                     MTMonthlyAmt=@MTMonthlyAmt,
                     MTPercentIncome=NullIf(RTrim(@MTPercentIncome), '') ,
                     CheckCashing =@CheckCashing,
                     CCRegistration= NullIf(RTrim(@CCRegistration), ''),
                     CCRegConfirmationNo= NullIf(RTrim(@CCRegConfirmationNo), ''),
                     CCActingasAgentfor=NullIf(RTrim(@CCActingasAgentfor), '') ,
                     CCMaxDailyAmtPerPerson=@CCMaxDailyAmtPerPerson ,
                     CCMonthlyAmt=@CCMonthlyAmt ,
                     CCPercentIncome=NullIf(RTrim(@CCPercentIncome), ''),
                     CurrencyExchange=@CurrencyExchange ,
                     CERegistration=NullIf(RTrim(@CERegistration), '') ,
                     CERegConfirmationNo=NullIf(RTrim(@CERegConfirmationNo), '') ,
                     CEActingasAgentfor=NullIf(RTrim(@CEActingasAgentfor), '') , 
                     CEMaxDailyAmtPerPerson=@CEMaxDailyAmtPerPerson ,
                     CEMonthlyAmt=@CEMonthlyAmt,
                     CEPercentIncome=NullIf(RTrim(@CEPercentIncome), '') ,
                     CurrencyDealing=@CurrencyDealing,
                     CDRegistration=NullIf(RTrim(@CDRegistration), '') , 
                     CDRegConfirmationNo= NullIf(RTrim(@CDRegConfirmationNo), ''),
                     CDActingasAgentfor =NullIf(RTrim(@CDActingasAgentfor), ''),
                     CDMaxDailyAmtPerPerson=@CDMaxDailyAmtPerPerson ,
                     CDMonthlyAmt=@CDMonthlyAmt ,
                     CDPercentIncome=NullIf(RTrim(@CDPercentIncome), '') , 
                     StoredValue=@StoredValue ,
                     SVRegistration=NullIf(RTrim(@SVRegistration), '') ,
                     SVRegConfirmationNo=NullIf(RTrim(@SVRegConfirmationNo), '') ,
                     SVActingasAgentfor=NullIf(RTrim(@SVActingasAgentfor), '') ,
                     SVMaxDailyAmtPerPerson=@SVMaxDailyAmtPerPerson ,
                     SVMonthlyAmt=@SVMonthlyAmt,
                     SVPercentIncome=NullIf(RTrim(@SVPercentIncome),''),
                     CDDUser1 =  @item_TypeProd,         
                     CDDUser2 = NullIf(RTrim(@CDDUser2) ,''),
                     CDDUser3 = NullIf(RTrim(@CDDUser3) ,''),         
                     CDDUser4 = NullIf(RTrim(@CDDUser4) ,''),        
                     CDDUser5 = NullIf(RTrim(@CDDUser5) ,''),
                     CDDUser6 = NullIf(RTrim(@CDDUser6) ,''),         
                     CDDUser7 = @item_Country,       
                     CDDUser8 = NullIf(RTrim(@CDDUser8) ,''),         
                     CDDUser9 = NullIf(RTrim(@CDDUser9) ,''), 
                     CDDUser10 = NullIf(RTrim(@CDDUser10) ,''),      
                     CDDUser11 = NullIf(RTrim(@CDDUser11) ,''),         
                     CDDUser12 = NullIf(RTrim(@CDDUser12) ,''),        
                     CDDUser13 = @item_Channel,      
                     CDDUser14 = NullIf(RTrim(@CDDUser14) ,''),         
                     CDDUser15 = NullIf(RTrim(@CDDUser15) ,''),        
                     CDDUser16 = NullIf(RTrim(@CDDUser16) ,''),      
                     CDDUser17 = NullIf(RTrim(@CDDUser17) ,''),         
                     CDDUser18 = NullIf(RTrim(@CDDUser18) ,''),        
                     CDDUser19 = NullIf(RTrim(@CDDUser19) ,''),      
                     CDDUser20 = NullIf(RTrim(@CDDUser20) ,''),         
                     CDDUser21 = NullIf(RTrim(@CDDUser21) ,''),         
                     CDDUser22 = NullIf(RTrim(@CDDUser22) ,''),             
                     CDDUser23 = NullIf(RTrim(@CDDUser23) ,''),         
                     CDDUser24 = NullIf(RTrim(@CDDUser24) ,''),         
                     CDDUser25 = NullIf(RTrim(@CDDUser25) ,''),             
                     CDDUser26 = NullIf(RTrim(@CDDUser26) ,''),         
                     CDDUser27 = NullIf(RTrim(@CDDUser27) ,''),         
                     CDDUser28 = NullIf(RTrim(@CDDUser28) ,''),             
                     CDDUser29 = NullIf(RTrim(@CDDUser29) ,''),         
                     CDDUser30 = NullIf(RTrim(@CDDUser30) ,''),         
                     CDDUser31 = NullIf(RTrim(@CDDUser31) ,''),             
                     CDDUser32 = NullIf(RTrim(@CDDUser32) ,''),         
                     CDDUser33 = NullIf(RTrim(@CDDUser33) ,''),         
                     CDDUser34 = NullIf(RTrim(@CDDUser34) ,''),             
                     CDDUser35 = NullIf(RTrim(@CDDUser35) ,''),         
                     CDDUser36 = NullIf(RTrim(@CDDUser36) ,''),         
                     CDDUser37 = NullIf(RTrim(@CDDUser37) ,''),             
                     CDDUser38 = NullIf(RTrim(@CDDUser38) ,''),         
                     CDDUser39 = NullIf(RTrim(@CDDUser39) ,''),         
                     CDDUser40 = NullIf(RTrim(@CDDUser40) ,''),
            LastOper =NullIf(RTrim(@lastoper),''),
                     --     Begin: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --     yw
                     RelationshipWithBankAffl = @RelationshipWithBankAffl,                                           -- Relationship with Bank Affiliate
                     PrivInvestmentCompany = @PrivInvestmentCompany,                                                       -- Private Investiment Company
                     ResultsOfNegativePressSearch = NullIf(RTrim(@ResultsOfNegativePressSearch) ,'')       -- Results of Negative Press Search          
                     --     End: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --  yw            
    Where CustomerId = @customerid and Ts = @ts
    Select @stat = @@error, @cnt = @@rowcount
    
    If ( @stat = 0 and @cnt = 0 )
        Select @stat = 250001 -- Concurrent Update
    If ( @stat <> 0 ) begin
        rollback tran BSA_UpdKYCData
        return @stat
    end
     
    If @trnCnt = 0
              commit tran BSA_UpdKYCData
    return @stat
GO


