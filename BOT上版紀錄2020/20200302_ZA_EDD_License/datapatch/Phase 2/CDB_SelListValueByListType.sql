USE [Pcdb]
GO

/****** Object:  StoredProcedure [dbo].[CDB_SelListValueByListType]    Script Date: 12/8/2020 5:36:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/* Create Procedure to list rows by ListType from ListValue table */
ALTER PROCEDURE [dbo].[CDB_SelListValueByListType] ( @listtype varchar(50) )
  AS
	BEGIN
		declare @oper varchar(100), @showflag varchar(10), @custid varchar(20), @risk varchar(50)
		set @showflag = ''
		set @oper = ''
		set @custid = ''
		set @risk = ''
		if LTRIM(RTRIM(@listtype)) = 'RiskRateCode'
		begin
			select @oper = OperCode from pcdb..PageStateHistoryTbl where ViewId = '803' and PageId = '801' and ObjectId = '304'
			and Created = (select max(Created) from pcdb..PageStateHistoryTbl where ViewId = '803' and PageId = '801' and ObjectId = '304')
			/*
			20201208 ZA ask chk risk class
			*/
			select @custid = ParameterValue from pcdb..ViewParameters where ViewId = '73' and opercode = @oper

			if @custid != ''
			begin
				select @risk = isnull(CDDUser39,'') from pbsa..KYCData where CustomerId = @custid
			end

			if charindex(rtrim(@risk),'DeRisk,HighRisk,MedHiRisk') > 0
			begin
				select top 1 @showflag = Oper from psec..OperRights
				where [right] = 'RJCTEDDRFD'
				and Oper = @oper

				if isnull(@showflag,'') != ''
				begin
					SELECT * FROM ListValue WHERE ListTypeCode = LTRIM(RTRIM(@listtype))
				end
				else
				begin
					SELECT * FROM ListValue WHERE ListTypeCode = LTRIM(RTRIM('RiskRateCodeOP'))
				end
			end
			else
			begin
				SELECT * FROM ListValue WHERE ListTypeCode = LTRIM(RTRIM('RiskRateCodeOP')) and Code = 0
			end
		end
		else
		begin
			SELECT * FROM ListValue WHERE ListTypeCode = LTRIM(RTRIM(@listtype))
		end
	END	
GO


