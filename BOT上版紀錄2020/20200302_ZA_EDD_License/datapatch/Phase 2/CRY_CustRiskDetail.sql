USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[CRY_CustRiskDetail]    Script Date: 2/26/2021 10:22:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



 
ALTER Procedure [dbo].[CRY_CustRiskDetail] ( @Cust ObjectId, @Accepted int )
    As


if @Accepted = 0
	Select rfd.id, rfd.Cust, rfd.RFTCode, rfd.RFICode, 
		isnull(lv.Description, Value) Value, rfd.Score, rfd.ScoreType,
		rfd.LastReview, rfd.LastReviewOper,
		CustName=c.Name, RFIName=rfi.Name, 
		RFIRequired=rfi.Required, Weight,
		RFScore = dbo.BSA_CustRFScore(c.ID),
		RCCode = dbo.BSA_RiskClassByScore(dbo.BSA_CustRFScore(c.ID))
	  From RiskFactorData rfd 
	  join RiskFactorItem rfi on rfi.Code = rfd.RFICode
	  join Customer c on c.ID = rfd.Cust
	  left join pcdb..ListValue lv on charindex(lv.ListTypeCode,rfd.rficode) > 0 and lv.Code = rfd.Value 
	 Where c.RFCalculate = 1 And rfd.Status = @Accepted
	   And (Upper(@Cust) = 'ALL' OR c.ID = @Cust)
	 Order By c.Name
else
	Select rfd.id, rfd.Cust, rfd.RFTCode, rfd.RFICode, 
		isnull(lv.Description, Value) Value, rfd.Score, rfd.ScoreType,
		rfd.LastReview, rfd.LastReviewOper,
		CustName=c.Name, RFIName=rfi.Name, 
		RFIRequired=rfi.Required, Weight,
		RFScore = FinalScore,
		RCCode = c.RiskClass
	  From RiskFactorData rfd
	  join RiskFactorItem rfi on rfi.Code = rfd.RFICode
	  join Customer c on c.ID = rfd.Cust
	  join (Select Cust,
				Case When Sum(Weight) = 0 
					Then 0 -- if Weights=0 then score=0 (to avoid divide by 0)
				Else round(
				 Sum( Weight * (
					Case When Score = 0 And ScoreType = 0 Then -- Missing Score 
						-- Use default score for missing score items
						-- see where clause below
						Cast(DefScore as Decimal)
					Else -- Score is present
						Cast(Score as Decimal)
					End
				    )) / Sum(Weight)
			     ,0) 
				End As FinalScore 
		  From RiskFactorData rfd1, RiskFactorItem rfi1
		 Where  rfi1.Code = rfd1.RFICode
		   And Status = 1
		  group by Cust) As FinalScores on c.ID = FinalScores.Cust   
      left join pcdb..ListValue lv on charindex(lv.ListTypeCode,rfd.rficode) > 0 and lv.Code = rfd.Value
	 Where c.RFCalculate = 1 And rfd.Status = @Accepted    
	   And (Upper(@Cust) = 'ALL' OR c.ID = @Cust)
	 Order By c.Name


GO


