USE [PBSA]
GO

/****** Object:  View [dbo].[CustProposedRiskClass]    Script Date: 7/13/2020 2:38:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO





ALTER View [dbo].[CustProposedRiskClass] as 

/* modify by 2020.07 for the Entity Risk Class
Select s.Cust,  s.Score,  Case when s.Score is Null Then Null 
		Else
			(Select top 1 r.Code from Riskclass r Where 
			   s.Score <= ( Case When  r.RFScoreHi  = 
                                (Select max(RFScoreHi) as MaxScore from RiskClass ) Then 100 
				  Else r.RFScoreHi 
				  End
				)
			   And s.Score > (
				Select IsNull(max(RFScoreHi),-1) 
				  From RiskClass rc2
				 Where rc2.RFScoreHi <  r.RFScoreHi 
				)
			)
		End as ProposedRiskClass
from  CustRFScores s (nolock) 
GO
*/

Select s.Cust, c.Type,  s.Score,  Case when s.Score is Null Then Null 
		Else
			case when c.Type = 'I' then (Select top 1 r.Code from Riskclass r Where 
			   s.Score <= ( Case When  r.RFScoreHi  = 
                                (Select max(RFScoreHi) as MaxScore from RiskClass ) Then 100 
				  Else r.RFScoreHi 
				  End
				)
			   And s.Score > (
				Select IsNull(max(RFScoreHi),-1) 
				  From RiskClass rc2
				 Where rc2.RFScoreHi <  r.RFScoreHi 
				)
			) 
		   when c.Type = 'E' then (Select top 1 r.Code from RiskClass r Where 
			   s.Score <= ( Case When  r.AmtTolPerc  = 
                                (Select max(AmtTolPerc) as MaxScore from RiskClass ) Then 100 
				  Else r.AmtTolPerc 
				  End
				)
			   And s.Score > (
				Select IsNull(max(AmtTolPerc),-1) 
				  From RiskClass rc2
				 Where rc2.AmtTolPerc <  r.AmtTolPerc 
				)
			) End
		End as ProposedRiskClass
from  CustRFScores s (nolock) 
join customer c on s.Cust = c.Id
GO


