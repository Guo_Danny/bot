USE [PBSA]
GO

/****** Object:  View [dbo].[CustRFScores]    Script Date: 7/9/2020 4:15:26 PM ******/
--modify by 2020.07 change the result score from Round to Ceiling
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


ALTER View [dbo].[CustRFScores] as 
Select s.Cust, Case when IsNull(m.CntMissingScore,0) = 0 Then s.RFScore Else Null End as Score from
(
Select r.Cust, 	Case When Sum(Weight) = 0 Then 0 -- if Weights=0 then score=0 (to avoid divide by 0)
		Else convert(int, CEILING( Sum( Weight * (
			Case When Score = 0 And ScoreType = 0 Then 
				-- Missing Score 
				-- Use default score for missing score items
				-- see where clause below
				Cast(rfi.DefScore as Decimal)
			Else -- Score is present
				Cast(Score as Decimal)
			End
			)) / Sum(Weight)
			))
		End
	as RFScore
from  RiskFactorData r (nolock) 
inner join RiskFactorItem rfi (nolock)
 on  rfi.Code = r.RFICode
    And Status = 0
Group by r.Cust
) s left outer join CntMissingScoresView  m (nolock)
on s.cust = m.cust

GO


