USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_UpdRiskFactorList]    Script Date: 6/5/2020 12:06:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER Procedure [dbo].[BSA_UpdRiskFactorList] (
		@RFICode SCode, @RFTCode SCode,
		@LastOper SCode, @ts timestamp, @OverRideItem BIT)
	  As

	-- Start standard stored procedure transaction header
	declare @trnCnt int
	select @trnCnt = @@trancount	-- Save the current trancount
	If @trnCnt = 0
		-- Transaction has not begun
		begin tran BSA_UpdRiskFactorList
	else
		-- Already in a transaction
		save tran BSA_UpdRiskFactorList
	-- End standard stored procedure transaction header

	declare @stat int, @cnt int

	if ((@RFICode != 'oMlcoHigh' or @RFICode != 'OSancLists') and @overRideItem != 0)
	begin
	Update RiskFactorList
	   Set RFICode = @RFICode,
			RFTCode = @RFTCode,
			LastOper = NullIf(@LastOper,''),
			LastModify = GetDate(),
			overRideItem = @overRideItem
	 Where RFICode = @RFICode 
	   and RFTCode = @RFTCode 
	   and Ts = @ts

    Select @stat = @@error, @cnt = @@rowcount
	end

	-- Evaluate results of the transaction
	If ( @stat = 0 and @cnt = 0 ) 
		Select @stat = 250001	-- Concurrent Update

	If ( @stat <> 0 ) begin
		rollback tran BSA_UpdRiskFactorList
		return @stat
	end
	If @trnCnt = 0
		commit tran BSA_UpdRiskFactorList
	return @stat
GO


