USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[RFC_CustE]    Script Date: 3/3/2020 3:57:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
-- calculate the new coming entity customer
-- return score 100 for exists entity customer, and 0 for others.
-- create date <= getdate() - 1 means exists customer.

 
Create PROC [dbo].[RFC_CustE](@cust ObjectID, @val VARCHAR(50) OUTPUT, @score int OUTPUT)
AS
 
DECLARE @stat INT,@trnCnt INT
 
SET NOCOUNT ON
SET @stat = 0

set @score = 0
set @val = Null

select @val = convert(varchar(8),cust.CreateDate,112), @score = 100
from Customer cust 
where cust.CreateDate is not null
and convert(varchar(10),cust.CreateDate,111) <= convert(varchar(10),getdate()-1,111)
and cust.Type = 'E'
and cust.Id =@cust

 
RETURN @stat
GO


