
INSERT INTO
CDDCalculatedItem(ItemName,Description,ProcName,AcceptanceItemParams, AcceptanceSectionParams, CreateOper,CreateDate)
VALUES('RiskScore','Generate Risk Score on demand','CDD_RiskScore','CustNo','','Prime','04/13/20')



CREATE PROC CDD_RiskScore(@CustNo varchar(50))
AS
BEGIN
DECLARE @RiskScore INT 
SET @RiskScore = 0

declare @p5 int
set @p5=1
declare @p6 int 
set @p6=1
declare @p7 int
set @p7=0
exec BSA_GenerateRFData 2,@CustNo,0,'PrimeAdmin',@p5 output,@p6 output,@p7 output

select @RiskScore = RiskScore from RiskScoreHist
where CustomerId = @CustNo
and CreateDate = (select max(CreateDate) from RiskScoreHist where CustomerId = @CustNo)

SELECT @RiskScore

END