USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_UpdAccount]    Script Date: 10/19/2020 6:17:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 



--------------------------------------------------------------------------------
ALTER Procedure [dbo].[BSA_UpdAccount] ( @id ObjectId, @name LongName1, @monitor bit,
              @exemptionStatus SCode, @notes varchar(500),
			  @branch SCode, @openDate datetime, 
			  @ownerBranch SCode, @ownerDept SCode, 
			  @ownerOper SCode, @riskFactor RiskFactor, 
			  @closed bit, @oper SCode, 
              @CloseDate datetime, @closeReason Varchar(255),
              @Reference Varchar(100), @Type Scode,
              @Request bit, @FreeLookEndDate datetime,
              @ExpiryDate datetime, @PolicyAmount money,
              @CashValue money, @SinglePremium bit , 
              @acctOfficer LongName, 
			  @BalanceAmt Money, @SourceofPayments varchar(250),
			  @User1 varchar(40), @User2 varchar(40),
			  @User3 varchar(40), @User4 varchar(40), 
			  @User5 varchar(40), @AcStatus SCode,
			  @ts timestamp, @InitialDeposit money ,
 			  @TypeOfFunds varchar(25) ,@ReqWireTrans Bit ,
 			  @AccACHPay Bit ,@OrgACHPay Bit ,@NoOfDepositMnth int ,
 			  @TotDepositAmtMnth money , @NoOfCashDepositMnth int ,
 			  @TotCashDepositAmtMnth money , @NoOfCashWithDMnth int ,
 			  @TotCashWithDAmtMnth money , @NoOfCheckWMnth int ,
 			  @TotCheckWAmtMnth money , @NoOfDomInWireMnth int ,
 			  @TotDomInWireAmtMnth money , @NoOfDomOutWireMnth int ,
 			  @TotDomOutWireAmtMnth money , @ReqForInWireTrans Bit ,
 			  @NoOfForInWireMnth int ,@TotForInWireAmtMnth money ,
 			  @OrigCntryForInWires varchar(1000) ,@ReqForOutWireTrans Bit ,
 			  @NoOfForOutWireMnth int ,@TotForOutWireAmtMnth money ,
 			  @DestCntryForOutWires varchar(1000),@IsProspect bit,
			  @DescOfAcctPurpose [varchar](255) ,				-- Description of Account Purpose
			  @PowerOfAttorney bit ,							-- Power Of Attorney
			  @RemoteDepositCapture bit ,						-- Remote Deposit Capture 
			  @RemoteDepositCaptureRiskRating [varchar](40) 	-- Remote Deposit Capture Risk Rating
			  -- Add 4 new fields to Account End Here  --      			  
 		)
   As

  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_UpdAccount
  else
    -- Already in a transaction
	save tran BSA_UpdAccount
  -- End standard stored procedure transaction header

  declare	@stat int, 
			@cnt int

  Update Account Set Name = NullIf(RTrim(@name), ''), Monitor = @monitor,
		ExemptionStatus = NullIf(RTrim(@exemptionStatus), ''), 
		Notes = NullIf(RTrim(@notes), ''), Branch = RTrim(@branch), 
		OpenDate = @openDate, LastOper = RTrim(@oper), 
		OwnerBranch = NullIf(RTrim(@ownerBranch), ''), 
		OwnerDept = NullIf(RTrim(@ownerDept), ''), 
		OwnerOper = NullIf(RTrim(@ownerOper), ''),
		RiskFactor = @riskFactor,
		closed = @closed,
		closeDate =@CloseDate,
		expiryDate = @expiryDate,
		freelookenddate = @freelookenddate,
		Reference = NullIf(Rtrim(@Reference), ''),
		closeReason = NullIf(Rtrim(@CloseReason),'') ,
		Type = NullIf(Rtrim(@type), ''),
		policyAmount = @PolicyAmount,
		CashValue = @CashValue,
		Request =@Request,
		SinglePremium =@SinglePremium,
		Accountofficer = NULLIF(RTRIM(@AcctOfficer), ''),
		BalanceAmt = NullIf(@BalanceAmt,0),
		SourceofPayments = NullIf(RTRIM(@SourceofPayments),''),
		User1 = NullIf(@User1,''), 
		User2 = NullIf(@User2,''), 
		User3 = NullIf(@User3,''), 
		User4 = NullIf(@User4,''),
		User5 = NullIf(@User5,''),
		Acstatus = NULLIF(RTRIM(@acstatus), ''),
		InitialDeposit = @InitialDeposit,
		TypeOfFunds = NullIf(@TypeOfFunds,''),
		ReqWireTrans = @ReqWireTrans ,
		AccACHPay = @AccACHPay ,
		OrgACHPay = @OrgACHPay,
		NoOfDepositMnth = @NoOfDepositMnth ,
		TotDepositAmtMnth = @TotDepositAmtMnth, 
		NoOfCashDepositMnth = @NoOfCashDepositMnth,
		TotCashDepositAmtMnth = @TotCashDepositAmtMnth, 
		NoOfCashWithDMnth = @NoOfCashWithDMnth,
		TotCashWithDAmtMnth= @TotCashWithDAmtMnth, 
		NoOfCheckWMnth= @NoOfCheckWMnth,
		TotCheckWAmtMnth= @TotCheckWAmtMnth, 
		NoOfDomInWireMnth = @NoOfDomInWireMnth,
		TotDomInWireAmtMnth = @TotDomInWireAmtMnth, 
		NoOfDomOutWireMnth = @NoOfDomOutWireMnth,
		TotDomOutWireAmtMnth = @TotDomOutWireAmtMnth , 
		ReqForInWireTrans = @ReqForInWireTrans,
		NoOfForInWireMnth = @NoOfForInWireMnth,
		TotForInWireAmtMnth = @TotForInWireAmtMnth,
		OrigCntryForInWires = NullIf(@OrigCntryForInWires, ''),
		ReqForOutWireTrans = @ReqForOutWireTrans,
		NoOfForOutWireMnth = @NoOfForOutWireMnth,
		TotForOutWireAmtMnth = @TotForOutWireAmtMnth,
		DestCntryForOutWires = NullIf(@DestCntryForOutWires,''),
		IsProspect= @IsProspect,
		-- Add 4 new fields to Account Start Here below -- yw
		DescOfAcctPurpose = NullIF(RTrim(@DescOfAcctPurpose), ''),							-- Description of Account Purpose
		PowerOfAttorney = IsNull(@PowerOfAttorney,0),										-- Power Of Attorney
		RemoteDepositCapture = IsNull(@RemoteDepositCapture,0),								-- Remote Deposit Capture 
		RemoteDepositCaptureRiskRating = NullIF(RTrim(@RemoteDepositCaptureRiskRating), '')	-- Remote Deposit Capture Risk Rating
		-- Add 4 new fields to Account End Here  -- yw     		  	
  Where Id = @id and Ts = @ts
  Select @stat = @@error, @cnt = @@rowcount
  -- Evaluate results of the transaction
  If ( @stat = 0 and @cnt = 0 ) 
	  Select @stat = 250001	-- Concurrent Update

  If ( @stat <> 0 ) begin
	rollback tran BSA_UpdAccount
	return @stat
  end
  If @trnCnt = 0
     commit tran BSA_UpdAccount  
  return @stat
GO


