USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_UpdCustomer]    Script Date: 10/19/2020 6:18:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER Procedure [dbo].[BSA_UpdCustomer] ( @id as ObjectId, @parent as ObjectId, 
					@name as LongName1, @dBA as LongName, 
					@secCode as varchar(10), @address as Address,@city as City, 
					@state as State, @zip as Zip, @country as SCode, 
					@telephone as varchar(20), @email as varchar(60),
					@tIN as varchar(20), @LicenseNo As Varchar(35), @LicenseState As State,
                                        @passportNo as varchar(20), 
					@dOB as GenericDate, @typeOfBusiness as varchar(80),
					@sourceOfFunds as varchar(80), @accountOfficer as LongName,
					@acctOffTel as varchar(20), @acctOffEmail as varchar(60),
					@compOfficer as LongName, @compOffTel as varchar(20),
					@compOffEmail as varchar(60), @idList as varchar(255),
					@notes as varchar(2000), @exemptionStatus as SCode,
					@lastActReset as GenericTime,
					@lastReview as GenericTime, @lastReviewOper as SCode,
					@ownerBranch SCode, @ownerDept SCode, @ownerOper SCode, 
					@riskClass SCode, @oper SCode, @ts timestamp,
					@countryOfOrigin SCode, @countryOfIncorp SCode, 
					@OffshoreCorp int, @BearerShares bit, @NomeDePlume bit,
					@Type SCode,
					@Resident Bit, @BusRelationNature VarChar(40),
					@PrevRelations VarChar(40),	
					@PEP Bit, @PoliticalPos VarChar(40), @FEP Bit, @MSB Bit,
					@CorrBankRelation bit, 
					@Sex Char(1), @ShellBank Bit, @HighRiskRespBank Bit,
					@OffshoreBank Bit, @PayThroughAC Bit, 
					@RegulatedAffiliate Bit, @USPerson VarChar(40),
					@RFCalculate Bit, @Status Int, @ReviewDate DateTime,
					@CTRAmt money, @User1 varchar(40), @User2 varchar(40),
					@User3 varchar(40), @User4 varchar(40), 
					@User5 varchar(40),
					@SwiftTID varchar(11), @embassy bit, 
					@ForeignGovt bit, @charityOrg bit, @DoCIP bit,
					@Prospect bit, @AssetSize Money, @Income Money,
					@IndOrBusType char(1),
					@profiledCountries Varchar(4000), @OnProbation Bit, 
					@ProbationReason SCode, @ProbationStartDate as GenericDate,
					@ProbationEndDate as GenericDate,
					@Closed Bit, @ClosedDate as GenericDate ,@ClosedReason ObjectId , 
					@OpenDate as GenericDate ,
					@CountryofResidence SCode,@CountryofCitizenship SCode,
					@KYCStatus int, @KYCOperator varchar(20),
					@KYCDataCreateDate datetime,@KYCDataModifyDate datetime
					)
  As

  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_UpdCustomer
  else
    -- Already in a transaction
	save tran BSA_UpdCustomer
  -- End standard stored procedure transaction header

  declare	@stat int, 
			@cnt int


  if @parent = '' Select @parent = @id

  Update Customer Set 
	Id = NullIf(RTrim(@id), ''), Parent = NullIf(RTrim(@parent), ''), 
	Name = NullIf(RTrim(@name), ''), DBA = NullIf(RTrim(@dBA), ''), 
	SecCode = NullIf(RTrim(@secCode), ''), 
	Address = NullIf(RTrim(@address), ''), City = NullIf(RTrim(@city), ''), 
	State = NullIf(RTrim(@state), ''), Zip = NullIf(RTrim(@zip), ''), 
            Country = NullIf(RTrim(@country), ''), 
	Telephone = NullIf(RTrim(@telephone), ''), 
	Email = NullIf(RTrim(@email), ''), TIN = NullIf(RTrim(@tIN), ''), 
        LicenseNo = NullIf(RTrim(@LicenseNo), ''), 
        LicenseState = NullIf(RTrim(@LicenseState), ''),
	PassportNo = NullIf(RTrim(@passportNo), ''), DOB = NullIf(@dOB, 0), 
	TypeOfBusiness = NullIf(RTrim(@typeOfBusiness), ''), 
	SourceOfFunds = NullIf(RTrim(@sourceOfFunds), ''), 
	AccountOfficer = NullIf(RTrim(@accountOfficer), ''), 
	AcctOffTel = NullIf(RTrim(@acctOffTel), ''), 
	AcctOffEmail = NullIf(RTrim(@acctOffEmail), ''), 
	CompOfficer = NullIf(RTrim(@compOfficer), ''), 
	CompOffTel = NullIf(RTrim(@compOffTel), ''), 
	CompOffEmail = NullIf(RTrim(@compOffEmail), ''), 
	IdList = NullIf(RTrim(@idList), ''), Notes = NullIf(RTrim(@notes), ''), 
	ExemptionStatus = NullIf(RTrim(@exemptionStatus), ''),
	LastActReset = @lastActReset, 
	LastReview = NullIf(@lastReview, 0), 
	LastReviewOper = NullIf(RTrim(@lastReviewOper), ''), 
	LastOper = RTrim(@oper),
	OwnerBranch = NullIf(RTrim(@ownerBranch), ''), 
	OwnerDept = NullIf(RTrim(@ownerDept), ''), 
	OwnerOper = NullIf(RTrim(@ownerOper), ''),
	RiskClass = @riskClass,
	CountryOfOrigin = NullIf(RTrim(@countryOfOrigin), ''),
	CountryOfIncorp= NullIf(RTrim(@countryOfIncorp), ''), 
	OffshoreCorp = NullIf(@OffshoreCorp, 0),
	BearerShares = @BearerShares,
    	NomeDePlume = @NomeDePlume,
	Type = NullIf(@Type, ''),
	Resident = @Resident, BusRelationNature = NullIf(@BusRelationNature,''),
	PrevRelations = NullIf(@PrevRelations,''), 
	PEP = @PEP, PoliticalPos = NullIf(@PoliticalPos,''), FEP = @FEP, MSB = @MSB,
	CorrBankRelation = @CorrBankRelation,
	Sex = NullIf(@Sex,''), ShellBank = @ShellBank,
	HighRiskRespBank = @HighRiskRespBank, OffshoreBank = @OffshoreBank,
	PayThroughAC = @PayThroughAC, RegulatedAffiliate = @RegulatedAffiliate,
	USPerson = NullIf(@USPerson,''), RFCalculate = @RFCalculate,
	Status = @Status, ReviewDate = @ReviewDate,
	CTRAmt = @CTRAmt,
	User1 = NullIf(@User1,''), 
	User2 = NullIf(@User2,''), 
	User3 = NullIf(@User3,''), 
	User4 = NullIf(@User4,''),
	User5 = NullIf(@User5,''),
	SwiftTID = NUllIf(RTrim(@SwiftTID),''), 
	Embassy = @embassy, 
	ForeignGovt = @ForeignGovt, 
	CharityOrg = @CharityOrg, 
	DoCIP = @DoCIP,
	Prospect = @Prospect,
	AssetSize = NullIf(@AssetSize,0),
	Income = NullIf(@Income,0),
	IndOrBusType = NullIf(RTrim(@IndOrBusType),''),
	OnProbation = @OnProbation,
	ProbationReason = NullIf(@ProbationReason, ''),
	ProbationStartDate = NullIf(@ProbationStartDate, 0),
	ProbationEndDate = NullIf(@ProbationEndDate, 0),
	Closed = @Closed, 
	ClosedDate = NullIf(@ClosedDate, 0),
	ClosedReason = NullIf(@ClosedReason, ''), 
	OpenDate = NullIf(@OpenDate, 0), 
	CountryofResidence = NullIf(RTrim(@CountryofResidence), ''), 
	CountryofCitizenship = NullIf(RTrim(@CountryofCitizenship), '') ,
	KYCDataCreateDate = @KYCDataCreateDate,
	KYCDataModifyDate = @KYCDataModifyDate,
	KYCStatus = @KYCStatus,
	KYCOperator = NullIf(@KYCOperator, '')
	
   Where Id = @id and Ts = @ts
  Select @stat = @@error, @cnt = @@rowcount
  -- Evaluate results of the transaction
  If ( @stat = 0 and @cnt = 0 ) 
	  Select @stat = 250001	-- Concurrent Update
  else
	 if @stat = 0 begin
		-- Insert Profile Countries (Comma delimited with a comma at the end)
		if len(@profiledCountries) >= 0 begin
			exec BSA_InsCountryOwners @profiledCountries, @id, @oper
			select stat = @@error
		end
	  end


  If ( @stat <> 0 ) begin
	rollback tran BSA_UpdCustomer
	return @stat
  end
  If @trnCnt = 0
     commit tran BSA_UpdCustomer
  return @stat
GO


