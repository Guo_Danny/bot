BC,Buy cash     、T/C,買入現鈔/旅行支票,
SC,Sell cash     、T/C,賣出現鈔/旅行支票,
MT,Outward transfer-MT,匯出信匯MT
DD,Outward transfer-DD,匯出票匯/MT,匯出信匯
RC,RC,光票託收,
CB,CB,光票買入,
TT,Telegraphic transfer-TT,匯出電匯,
IO,Outward transfer-IO,匯出電匯
CE,Export commission,出口佣金
IT,Inward transfer-TT,匯入電匯
IM,Inward transfer-MT,匯入信匯
ID,Inward transfer-DD,匯入票匯
IG,Inward transfer-RTGS,匯入電匯
OA,O/A,
BT,Financial Transfer,MT202
CO,Cash order payment,現鈔訂購付款
TC,T/C clearing,旅支清算