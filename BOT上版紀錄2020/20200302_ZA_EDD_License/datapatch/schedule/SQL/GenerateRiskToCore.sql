
SET NOCOUNT ON;
select distinct convert(char(35),vrsh.CustomerId) + '~' + convert(char(11),isnull(rs.Code,'')) + '~' + convert(char(8),isnull(c.ReviewDate,''),112) + '~' + convert(char(2),isnull(k.CDDUser25,'00')) 
 from vwRiskScoreHist vrsh
join RiskClass rs on vrsh.RiskClass = rs.name 
join Customer c on c.Id = vrsh.CustomerId
join KYCData k on k.CustomerId = c.Id
where id not like 'Noncust%'
and (DateAccepted >= convert(varchar(10),getdate()-10,111))
and KYCStatus = 2
and isnull(c.ProbationStartDate,'') != isnull(convert(varchar(8),c.ReviewDate,112),'')
