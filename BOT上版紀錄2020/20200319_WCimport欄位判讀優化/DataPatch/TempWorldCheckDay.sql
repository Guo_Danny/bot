USE [OFAC]
GO

/****** Object:  Table [dbo].[TempWorldCheckDay]    Script Date: 3/23/2020 11:43:48 AM ******/
DROP TABLE [dbo].[TempWorldCheckDay]
GO

/****** Object:  Table [dbo].[TempWorldCheckDay]    Script Date: 3/23/2020 11:43:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TempWorldCheckDay](
	[UID] [int] NOT NULL,
	[LASTNAME] [varchar](max) NULL,
	[FIRSTNAME] [varchar](max) NULL,
	[ALIASES] [varchar](max) NULL,
	[LOWQALIASES] [varchar](max) NULL,
	[ALTERSPELL] [varchar](max) NULL,
	[CATEGORY] [varchar](max) NULL,
	[TITLE] [varchar](max) NULL,
	[SUBCATEGORY] [varchar](max) NULL,
	[POSITION] [varchar](max) NULL,
	[AGE] [varchar](10) NULL,
	[DOB] [varchar](50) NULL,
	[DOBS] [varchar](max) NULL,
	[PLACEOFBIRTH] [varchar](max) NULL,
	[DECEASED] [varchar](50) NULL,
	[PASSPORTS] [varchar](max) NULL,
	[SSN] [varchar](max) NULL,
	[IDENTIFICATION_NUMBERS] [varchar](max) NULL,
	[LOCATION] [varchar](max) NULL,
	[COUNTRY] [varchar](max) NULL,
	[CITIZENSHIP] [varchar](max) NULL,
	[COMPANY] [varchar](max) NULL,
	[TYPE] [varchar](max) NULL,
	[LINKEDTO] [varchar](max) NULL,
	[FURTHERINFO] [varchar](max) NULL,
	[KEYWORDS] [varchar](max) NULL,
	[EXSOURCES] [varchar](max) NULL,
	[UPDCATEGORY] [varchar](max) NULL,
	[ENTERED] [varchar](50) NULL,
	[UPDATED] [varchar](50) NULL,
	[EDITOR] [varchar](max) NULL,
	[AGEDATE] [varchar](50) NULL,
	[PEPROLES] [varchar](max) NULL,
	[PEPSTATUS] [varchar](50) NULL,
 CONSTRAINT [PK_TempWorldCheckday] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


