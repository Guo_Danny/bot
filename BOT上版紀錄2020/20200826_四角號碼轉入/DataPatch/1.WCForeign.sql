USE [OFAC]
GO

/****** Object:  Table [dbo].[WCForeign]    Script Date: 8/26/2020 5:05:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WCForeign](
	[AliasesID] [varchar](10) NOT NULL,
	[RecordId] [varchar](10) NOT NULL
) ON [PRIMARY]
GO


USE [OFAC]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [PK_WCForeignAliases]    Script Date: 8/26/2020 5:06:19 PM ******/
CREATE NONCLUSTERED INDEX [PK_WCForeignAliases] ON [dbo].[WCForeign]
(
	[RecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


