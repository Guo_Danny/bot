USE [OFAC]
GO

/****** Object:  Table [dbo].[WCForeignAliases]    Script Date: 8/26/2020 5:08:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WCForeignAliases](
	[Language] [varchar](10) NOT NULL,
	[AliasesText] [nvarchar](1500) NOT NULL,
	[AliasesID] [varchar](10) NOT NULL
) ON [PRIMARY]
GO



USE [OFAC]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [PK_WCForeignAlias]    Script Date: 8/26/2020 5:08:41 PM ******/
CREATE NONCLUSTERED INDEX [PK_WCForeignAlias] ON [dbo].[WCForeignAliases]
(
	[AliasesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


