USE [OFAC]
GO

/****** Object:  Table [dbo].[WCForeignRecords]    Script Date: 8/26/2020 5:09:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WCForeignRecords](
	[RecordId] [varchar](10) NOT NULL,
	[UID] [varchar](10) NOT NULL
) ON [PRIMARY]
GO



USE [OFAC]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [PK_Records]    Script Date: 8/26/2020 5:09:19 PM ******/
CREATE NONCLUSTERED INDEX [PK_Records] ON [dbo].[WCForeignRecords]
(
	[RecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO




USE [OFAC]
GO

/****** Object:  View [dbo].[WCNativeVIEW]    Script Date: 8/26/2020 5:10:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


Create View [dbo].[WCNativeVIEW]
 As
select wcr.UID,wcr.UID + 20000000 as 'Entnum',wcfa.LANGUAGE,wcfa.AliasesText from ofac..WCForeignRecords wcr
join ofac..WCForeign wcfas on wcr.RecordId = wcfas.RecordId
join ofac..WCForeignAliases wcfa on wcfa.AliasesID = wcfas.AliasesID

GO


