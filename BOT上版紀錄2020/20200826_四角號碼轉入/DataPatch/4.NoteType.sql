insert into NoteType(Code,name,CreateOper,CreateDate,LastOper,LastModify)
values ('100','WCSubcategory','PrimeAdmin', getdate(),'PrimeAdmin',getdate())
insert into NoteType(Code,name,CreateOper,CreateDate,LastOper,LastModify)
values ('101','WCKeywords','PrimeAdmin', getdate(),'PrimeAdmin',getdate())
insert into NoteType(Code,name,CreateOper,CreateDate,LastOper,LastModify)
values ('102','WCNative','PrimeAdmin', getdate(),'PrimeAdmin',getdate())
insert into NoteType(Code,name,CreateOper,CreateDate,LastOper,LastModify)
values ('103','WCTitle','PrimeAdmin', getdate(),'PrimeAdmin',getdate())
insert into NoteType(Code,name,CreateOper,CreateDate,LastOper,LastModify)
values ('104','WCPosition','PrimeAdmin', getdate(),'PrimeAdmin',getdate())
insert into NoteType(Code,name,CreateOper,CreateDate,LastOper,LastModify)
values ('105','WCUPDCategory','PrimeAdmin', getdate(),'PrimeAdmin',getdate())

insert into NoteType(Code,name,CreateOper,CreateDate,LastOper,LastModify)
values ('106','WCBIC','PrimeAdmin', getdate(),'PrimeAdmin',getdate())
insert into NoteType(Code,name,CreateOper,CreateDate,LastOper,LastModify)
values ('107','WCIMO','PrimeAdmin', getdate(),'PrimeAdmin',getdate())
insert into NoteType(Code,name,CreateOper,CreateDate,LastOper,LastModify)
values ('108','WCARN','PrimeAdmin', getdate(),'PrimeAdmin',getdate())
insert into NoteType(Code,name,CreateOper,CreateDate,LastOper,LastModify)
values ('109','WCMSN','PrimeAdmin', getdate(),'PrimeAdmin',getdate())


USE [OFAC]
GO

/****** Object:  UserDefinedDataType [dbo].[NoteDataType1]    Script Date: 8/26/2020 5:19:38 PM ******/
CREATE TYPE [dbo].[NoteDataType1] FROM [nvarchar](1000) NULL
GO



ALTER TABLE Notes ALTER COLUMN [Note] [NoteDataType1] NULL;

ALTER TABLE WCKeywordslistTable ALTER COLUMN "explanation" varchar(2000);