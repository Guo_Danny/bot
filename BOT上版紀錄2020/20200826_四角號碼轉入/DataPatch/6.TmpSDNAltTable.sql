USE [OFAC]
GO

/****** Object:  Table [dbo].[TmpSDNAltTable]    Script Date: 8/27/2020 6:32:17 PM ******/
DROP TABLE [dbo].[TmpSDNAltTable]
GO

/****** Object:  Table [dbo].[TmpSDNAltTable]    Script Date: 8/27/2020 6:32:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TmpSDNAltTable](
	[EntNum] [int] NULL,
	[AltNum] [int] NOT NULL,
	[AltName] [dbo].[SanctionName] NULL,
	[FirstName] [dbo].[SanctionName] NULL,
	[LastName] [dbo].[SanctionName] NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_TmpSDNAltTable_AltNum] PRIMARY KEY NONCLUSTERED 
(
	[AltNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


