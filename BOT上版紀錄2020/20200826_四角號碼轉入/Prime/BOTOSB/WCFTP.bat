"C:\Program Files (x86)\WinSCP\WinSCP.exe" /script="D:\Prime\BOTOSB\WCFTP.txt"

set hour=%time:~0,2%
if "%hour:~0,1%" == " " set hour=0%hour:~1,1%

d:
cd\worldcheck

7z e -y d:\worldcheck\world-check-native-character-names.xml.gz

COPY d:\worldcheck\premium-world-check.csv  d:\worldcheck\backup\premium-world-check.csv.%DATE:~7,2%
Rem REN  d:\worldcheck\premium-world-check.csv premium-world-check.csv.%DATE:~7,2%
COPY d:\worldcheck\premium-world-check-day.csv  d:\worldcheck\backup\premium-world-check-day.csv.%DATE:~7,2%.%hour%
Rem REN  d:\worldcheck\premium-world-check-day.csv premium-world-check-day.csv.%DATE:~7,2%
COPY d:\worldcheck\world-check-deleted-day-v2.csv  d:\worldcheck\backup\world-check-deleted-day-v2.csv.%DATE:~7,2%.%hour%
Rem REN  d:\worldcheck\world-check-deleted-day-v2.csv world-check-deleted-day-v2.csv.%DATE:~7,2%
COPY d:\worldcheck\world-check-native-character-names.xml.gz  d:\worldcheck\backup\world-check-native-character-names.xml.gz.%DATE:~7,2%.%hour%



exit