/*
update Watchlist set [Desc] = 'Detects customers that had transactions matching the specified criteria within the last X months. Can be used to monitor for structured cash transactions or specific types of transactions such as ATM withdrawals.  Designed to be run Post-EOD (i.e. This rule excludes Current activity)   Individual transactions must be greater than Minimum Transaction Amount AND less than Maximum Transaction Amount and must also match values specified for Customer, Customer Type, Risk Class, Account Type, Branch and Department.   When using the Cash CashTran Indicator parameter,      1 = Ignore the Cash Tran indicator,      2 = include any transaction where the Cash Tran indicator is Yes OR that matches a specified Activity Type,      3 = include any transaction where the Cash Tran indicator is Yes AND that matches a specified Activity Type.   Specify Number of months as 0(zero) for current date.  When using the Exclude Exempt parameter, customers will be excluded from evaluation if their Exemption Type is included in the comma separated list or if -ALL- is entered AND if the Exemption Type is configured as exempt from rules.   The grand total amount of selected transactions must be greater than the Minimum Total Transaction Amount AND less than the Maximum Total Transaction Amount.'
,Title = 'Detects customers that had transactions matching the specified criteria within the last X months.'
where IsUserDefined = 1
and WLCode in ('ILgTrans90','OLgTrans90') 
*/
update Watchlist set Params = '<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="" /><Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'
where IsUserDefined = 1
and WLCode in ('ILgTrans90') 

update Watchlist set Params = '<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="" /><Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'
where IsUserDefined = 1
and WLCode in ('OLgTrans90') 


update Watchlist set Params = '<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="" /><Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'
where IsUserDefined = 1
and WLCode in ('IMultiBene') 

update Watchlist set Params = '<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="" /><Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'
where IsUserDefined = 1
and WLCode in ('OMultiBene') 