USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_CstXOvrYInt]    Script Date: 9/21/2020 3:57:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO



ALTER PROCEDURE [dbo].[USR_CstXOvrYInt] (@WLCode SCode, 
								@TestAlert INT,
								@excludeExemptCustList varchar(8000),
								@activityTypeList varchar(8000), 
								@activityTypeExcludeList VARCHAR(8000),
								@cashTranIndicator INT,
								@minimumAmount MONEY,
								@maximumAmount MONEY,
								@customerList VARCHAR(8000), 
								@RiskClassList VARCHAR(8000), 
								@CustomerTypeList VARCHAR(8000),
								@AccountTypeList VARCHAR(8000), 
								@BranchList varchar(8000), 
								@DeptList varchar(8000), 
								@UseRelAcct Bit, 
								@UseRelCust Bit, 
								@ExcludeRelList varchar(8000),
								@NumDaysLookback INT, 
								@minTotalAmtForLookBack MONEY, 
								@maxTotalAmtForLookBack MONEY, 
								@minCountForLookback INT,
								@NumDaysInterval INT, 
								@minTotalAmtForInterval MONEY, 
								@maxTotalAmtForInterval MONEY, 
								@minCountForInterval INT
) AS
/* RULE PARAMETERS -  THESE MUST BE SET ACCORDING TO THE RULE REQUIREMENT*/

/* RULE AND PARAMETER DESCRIPTION
	Detects customers that had transactions using a specified 
	activity type that exceeded a designated dollar amount over 
	an interval or specified number of days.  Designed to be run Post-EOD. 
	
	@excludeExemptCustList = Type of Exemptions to exclude customers (Comma separated or '-ALL-' or '-NONE-')
	@activityTypeList = Type of activity to be checked  (Comma separated or '-ALL-') 
	@activityTypeExcludeList = Type of activity to be excluded (Comma separated or '-NONE-') 
	@cashTranIndicator = Cash Tran indicator to include check on activity type 
	@minimumAmount= Minimum threshold amount 
	@maximumAmount = Maximum Threshold Amount
	@CustomerList = List of customer Id's to be checked
	@RiskClassList = List of risk classes of customer to be checked
	@AccountTypeList = List of Accounts to be checked (Comma Separated or '-ALL-')
	@CustomerTypeList = List of Customer Types
   	@BranchList  = List of Branches to be checked (Comma Separated or '-ALL-')
   	@DeptList    = List of Department to be checked (Comma Separated or '-ALL-')
	@UseRelAcct = Include Related Accounts (1=yes, 0=no) 
	@UseRelCust = Include Related Parties (1=yes, 0=no) 
	@ExcludeRelList = Exclude Relationship Types (comma separated list or '-NONE-')
	@NumDaysLookback = Look Back period for the activity
	@minTotalAmtForLookBack = Minimum Total Transaction Amount for the look back
	@maxTotalAmtForLookBack = Maximum Total Threshold amount for the look back
	@minCountForLookback = Minimum Transaction Count for the look back
	@NumDaysInterval = Interval period for the activity
	@minTotalAmtForInterval = Minimum Total Transaction Amount for the interval
	@maxTotalAmtForInterval = Maximum Total Threshold amount for the interval
	@minCountForInterval = Minimum Transaction Count for the interval
*/

/*  Declarations */
	DECLARE	@description 	VARCHAR(2000),
		@Id 				INT, 
		@WLType 			INT,
		@stat 				INT,
		@trnCnt 			INT,
		@Count				INT,
		@BaseCurr			CHAR(3),
		@NoOfIntervals		INT,
		@startDate 			INT,      -- stores start date of the look back period (period 1) as integer
		@endDate			INT,      -- stores end date of the look back period (period 1) as integer
		@LookbackStartDate	DATETIME, -- stores the start date for the interval (period 1)
		@LookbackEndDate 	DATETIME, -- stores the end date for the look back period (period 1)
		@StartAlrtDate		DATETIME, -- stores the start date and time of alert or case creation
		@EndAlrtDate		DATETIME, -- stores the end date and time of alert or case creation
		@IntStartDate		DATETIME, -- stores the start date for the interval (period 2)
		@IntEndDate			DATETIME  -- stores the end date for the interval (period 2)

	SET NOCOUNT ON
	SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
	SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @trnCnt = 0
	-- Transaction has not begun
		BEGIN TRAN USR_CstXOvrYInt
	ELSE
	-- Already in a transaction
		SAVE TRAN USR_CstXOvrYInt
	/* End standard stored procedure transaction header */

/*  standard Rules Header */
-- Date options
-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

/* Temporary Tables declaration */

-- Table to hold transactions that meet the criteria specified
	CREATE TABLE #TempActHist
	(
		TranNo				INT Not Null,
		Cust				VARCHAR(35) Not Null,
		Account				VARCHAR(35) Null,
		Type				INT Not Null,
		CashTran			INT Not Null,
		BaseAmt				MONEY Not Null,
		RecvPay				SMALLINT Not Null,
		Curr				CHAR(3) Not Null,
		Ref					VARCHAR(35) Null,
		BookDate			INT Not Null,
		ExemptionStatus     VARCHAR(11) Null,
		Branch				VARCHAR(11) Not Null,
		Dept				VARCHAR(11) Not Null,
		TranDate			DATETIME Not Null,
		Constraint PK_TempActHist_TranNo Primary Key NonClustered (TranNo)
	)

-- Table to hold transactions that meet the criteria specified
	CREATE TABLE #TT1
	(
		Cust	 VARCHAR(35) ,
		TranNo	 INT,
		BaseAmt  MONEY, 
		RecvPay  INT,
		bookdate INT,
		IntNo    INT)

-- Table to hold customer id and the total of all transactions during the look back period
	CREATE TABLE #TTLookBack 
	(
		Cust 		VARCHAR(35) ,
		RecvPay		INT,
		TranAmt		MONEY
	)

-- Table to hold customer id and the total of all transactions during the interval period
	CREATE TABLE #TTINT 
	(
		IntNo		INT,
		Cust 		VARCHAR(35),
		RecvPay		INT,
		TranAmt		MONEY,
	)

-- Table to hold intervals and their start and end dates
	CREATE TABLE #INT
	(
		IntNo 		INT,
		StartDate	DATETIME,
		EndDate		DATETIME
	)

/* END - Temporary Tables declaration */


	SELECT  @description = [Desc], @WLType = WLType  ,
		@LookbackEndDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GetDate()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GetDate()
		END
	FROM WatchList WITH (NOLOCK) WHERE WLCode = @WLCode

	SELECT @BaseCurr = IsNULL(BaseCurr,'') FROM SysParam
    
	SET @startDate = dbo.ConvertSqlDateToInt(DATEADD(d, -1 * (@NumDaysLookback-1), CONVERT(VARCHAR, @LookbackEndDate)))
	SET	@endDate = dbo.ConvertSqlDateToInt(@LookbackEndDate)


	-- Set Number of days in the interval to 1 to handle divide by 0 error
	IF (@NumDaysInterval=0)
		SELECT @NumDaysInterval = 1

	-- Handle the case where number of days in the interval is greater than number of days in the look back
	IF (@NumDaysInterval>@NumDaysLookback)
		SELECT @NumDaysInterval = @NumDaysLookback

	SELECT @NoOfIntervals = @NumDaysLookback/@NumDaysInterval

	SELECT @Count = 1
	SET @IntEndDate = @LookbackEndDate
	SET @LookbackStartDate = dateadd(day,-1*(@NumDaysLookback-1), @IntEndDate)

	WHILE (@count <= @NoOfIntervals)
	BEGIN
		SET @intstartdate = dateadd(day,-1*(@NumDaysInterval-1), @intendDate)
		IF (@intstartDate < @lookbackstartdate )
		BEGIN
			SET @intstartdate = @lookbackstartdate
		END
		INSERT INTO #INT (intno, startdate, enddate) VALUES (@count, @intstartdate, @intenddate)
		SET @intenddate = dateadd(day, -1,@intstartDate)
		SET @count = @count + 1   
	END
	-- End of the section to identify the interval start and end dates


	IF LTRIM(RTRIM(@excludeExemptCustList)) = '-NONE-' OR LTRIM(RTRIM(@excludeExemptCustList)) = ''
		SELECT @excludeExemptCustList = NULL
	ELSE IF LTRIM(RTRIM(@excludeExemptCustList)) = '-ALL-'
		BEGIN
			SELECT @excludeExemptCustList = ''
			SELECT  @excludeExemptCustList = 		
				COALESCE( @excludeExemptCustList + ',','' )  + Code
			FROM ExemptionType 
			 WHERE (SubString(ExemptEntity,1,1) = 1)
					AND (SubString(ExemptFrom,3,1) = 1)
						
			SELECT  @excludeExemptCustList = @excludeExemptCustList + ','
		END
	ELSE                                           
		SELECT @excludeExemptCustList = ',' + 
			REPLACE(LTRIM(RTRIM(@excludeExemptCustList)),' ','')+ ','
			
	IF LTRIM(RTRIM(@activityTypeExcludeList)) = '-NONE-' OR LTRIM(RTRIM(@activityTypeExcludeList)) = ''
		SELECT @activityTypeExcludeList = NULL
	ELSE
		SELECT @activityTypeExcludeList = ',' + 
			REPLACE(LTRIM(RTRIM(@activityTypeExcludeList)),' ','')+ ','
			   
	IF LTRIM(RTRIM(@ExcludeRelList)) = '-NONE-' OR LTRIM(RTRIM(@ExcludeRelList)) = ''
		SELECT @ExcludeRelList = NULL
	ELSE
		SELECT @ExcludeRelList = ',' + 
			REPLACE(LTRIM(RTRIM(@ExcludeRelList)),' ','')+ ','
	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)
	SELECT @customerList = dbo.BSA_fnListParams(@customerList)
	SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	SELECT @customerTypeList = dbo.BSA_fnListParams(@customerTypeList)
	SELECT @AccountTypeList = dbo.BSA_fnListParams(@AccountTypeList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
	SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)
	SELECT @ExcludeRelList = dbo.BSA_fnListParams (@ExcludeRelList)
	
	INSERT INTO #TempActHist (TranNo, Cust, Account, Type, CashTran,	BaseAmt, RecvPay, Curr,	Ref, BookDate,
			ExemptionStatus, Branch, Dept, TranDate)
	SELECT 	TranNo,	Cust, Account, Type, CashTran,	BaseAmt, RecvPay, Curr,	Ref, BookDate,
			ExemptionStatus, Branch, Dept, TranDate
	FROM   ActivityHist ActHist WITH (NOLOCK)
	WHERE  ActHist.bookdate >= @startDate 
	AND	ActHist.bookdate <= @endDate
	AND ((ISNULL(@ActivityTypeExcludeList,'') = '' OR
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeExcludeList) = 0))
	AND (
	(@cashTranIndicator = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR --include activitytypelist if cash cashTran is 1
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0)))
	OR 
	(@cashTranIndicator = 2 AND (ActHist.CashTran = 1 OR ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
	OR 
	(@cashTranIndicator = 3 AND (ActHist.CashTran = 1 AND ((ISNULL(@ActivityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeList) > 0))))
	)
	AND	(@minimumAmount = -1 OR BaseAmt >= @minimumAmount) 
	AND (@maximumAmount = -1 OR BaseAmt <= @maximumAmount)

	INSERT INTO #TT1 
	SELECT ActHist.Cust,TranNo, BaseAmt, RecvPay, bookdate, 0
	FROM   #TempActHist ActHist WITH (NOLOCK)
	INNER  JOIN Customer Cust WITH (NOLOCK) ON ActHist.Cust = Cust.ID
	LEFT   JOIN Account Acct WITH (NOLOCK) ON ActHist.Account = Acct.ID 	
	WHERE  
	((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Cust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Cust.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Cust.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Cust.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(cust.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(cust.OwnerDept)) + ',', @DeptList) > 0))
	AND ((ISNULL(@ActivityTypeExcludeList,'') = '' OR
		CHARINDEX(',' + LTRIM(RTRIM(acthist.type)) + ',', @ActivityTypeExcludeList) = 0))
	   
		-- Related Parties
		/* 2020.09 remove duplicate transaction
	INSERT INTO #TT1 
	SELECT 	pr.PartyId ,TranNo, BaseAmt, RecvPay, bookdate, 0
	FROM 	#TempActHist ActHist WITH (NOLOCK) 
	INNER 	JOIN Customer Cust WITH (NOLOCK) ON ActHist.Cust = Cust.ID
	Left	JOIN Account Acct WITH (NOLOCK) ON ActHist.Account = Acct.ID 
	INNER	JOIN PartyRelation pr WITH (NOLOCK) ON ActHist.Cust = pr.RelatedParty
	INNER	JOIN Customer RelCust WITH (NOLOCK) ON pr.RelatedParty = RelCust.ID
	WHERE 	((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Cust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerDept)) + ',', @DeptList) > 0))
	AND @UseRelCust = 1 AND ActHist.Cust <> pr.PartyID AND pr.Deleted <> 1 
	AND ((ISNULL(@ExcludeRelList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND Exists (SELECT * FROM #TT1 tt WHERE tt.Cust = pr.PartyId )
	AND Not Exists (SELECT * FROM #TT1 tt WHERE tt.Cust = pr.PartyId AND tt.TranNo = ActHist.tranno)
	*/
	-- Related Parties (reverse direction)
	INSERT INTO #TT1 
	SELECT 	pr.RelatedParty, TranNo, BaseAmt, RecvPay, bookdate, 0
	FROM 	#TempActHist ActHist WITH (NOLOCK) 
	INNER 	JOIN Customer Cust WITH (NOLOCK) ON ActHist.Cust = Cust.ID
	Left	JOIN Account Acct WITH (NOLOCK) ON ActHist.Account = Acct.ID 
	INNER	JOIN PartyRelation pr WITH (NOLOCK) ON ActHist.Cust = pr.PartyID
	INNER	JOIN Customer RelCust WITH (NOLOCK) ON pr.RelatedParty = RelCust.ID
	WHERE 	((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Cust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(RelCust.OwnerDept)) + ',', @DeptList) > 0))
	AND @UseRelCust = 1 AND ActHist.Cust <> pr.RelatedParty AND pr.Deleted <> 1
	AND ((ISNULL(@ExcludeRelList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND Exists (SELECT * FROM #TT1 tt WHERE tt.Cust = pr.RelatedParty)
	AND Not Exists (SELECT * FROM #TT1 tt WHERE tt.Cust = pr.RelatedParty AND tt.TranNo = ActHist.tranno)

	-- Related Accounts
	INSERT INTO #TT1 
	SELECT 	ao2.Cust,TranNo, BaseAmt, RecvPay, bookdate, 0
	FROM 	#TempActHist ActHist WITH (NOLOCK) 
	INNER 	JOIN Customer Cust WITH (NOLOCK) ON ActHist.Cust = Cust.ID
	INNER   JOIN AccountOwner ao WITH (NOLOCK) ON ActHist.Cust = ao.cust
	INNER	JOIN Account Acct WITH (NOLOCK) ON ao.Account = Acct.ID 
	INNER	JOIN AccountOwner ao2 WITH (NOLOCK) ON ao.Account = ao2.Account And ao.Cust <> ao2.Cust
	INNER	JOIN Customer CustOnAcct ON ao2.Cust = CustOnAcct.ID
	WHERE 	((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Cust.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@excludeExemptCustList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(CustOnAcct.ExemptionStatus,''))) + ',', @excludeExemptCustList) = 0))
	AND ((ISNULL(@customerList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(CustOnAcct.Id)) + ',', @customerList) > 0))
	AND ((ISNULL(@RiskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(CustOnAcct.RiskClass)) + ',',@RiskClassList) > 0))
	AND ((ISNULL(@CustomerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(CustOnAcct.Type)) + ',', @CustomerTypeList) > 0))
	AND ((ISNULL(@AccountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(Acct.Type)) + ',', @AccountTypeList) > 0))
	AND ((ISNULL(@BranchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(custOnAcct.OwnerBranch)) + ',', @BranchList) > 0))
	AND ((ISNULL(@DeptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(custOnAcct.OwnerDept)) + ',', @DeptList) > 0))
	AND @UseRelAcct = 1 AND ActHist.Cust <> ao2.Cust
	AND ((ISNULL(@ExcludeRelList,'') = '' OR ao.Account = ActHist.Account OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ao.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND ((ISNULL(@ExcludeRelList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ao2.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND Exists (SELECT * FROM #TT1 tt WHERE tt.Cust = ao2.Cust)
    	AND Not Exists (SELECT * FROM #TT1 tt WHERE tt.Cust = ao2.Cust AND tt.TranNo = ActHist.tranno)

	INSERT INTO #TTLookBack
	SELECT 	Cust, RecvPay, SUM(BaseAmt)
	FROM 	#TT1
	GROUP 	BY Cust, RecvPay
	HAVING 	(@minTotalAmtForLookBack = -1 OR SUM(BaseAmt) >= @minTotalAmtForLookBack)
	AND (@maxTotalAmtForLookBack = -1 OR SUM(BaseAmt) <= @maxTotalAmtForLookBack)
	AND	COUNT(TranNo) >= @minCountForLookback

    UPDATE #TT1 SET intno = (SELECT intno FROM #INT WHERE bookdate 
							 BETWEEN dbo.ConvertSqlDateToInt(startdate) AND dbo.ConvertSqlDateToInt(enddate))

	INSERT INTO #TTINT 
	SELECT 	#TT1.intno, Cust, RecvPay, SUM(BaseAmt)
	FROM 	#TT1
	GROUP 	BY Cust, RecvPay, intno
	HAVING  (@minTotalAmtForInterval = -1 OR SUM(BaseAmt) >= @minTotalAmtForInterval)
			AND (@maxTotalAmtForInterval = -1 OR SUM(BaseAmt) <= @maxTotalAmtForInterval)
			AND	COUNT(TranNo) >= @minCountForInterval

	--1 means test alert, 0 means actual alert
	IF @testAlert = 1 
	BEGIN
		-- Create test alerts for customers who have exceeded only in the look back period
		SELECT @StartAlrtDate = GETDATE()
		INSERT INTO Alert (WLCode,  STATUS, CreateDate, LASTOPER, 
							LASTMODIFY, CUST, ACCOUNT, IsTest, [DESC]) 
		SELECT @WLCode,0,GETDATE(), NULL, NULL, Cust, NULL , 1,
 			 CASE WHEN RecvPay = 1 THEN
				'Customer ''' + Cust + ''' had deposits of ' 
				+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + 
				+ ' between ' + CONVERT(VARCHAR, @LookbackStartDate,101) + ' and ' + CONVERT(VARCHAR, @LookbackEndDate,101)
			 ELSE
				'Customer  ''' + Cust + ''' had withdrawals of ' 
				+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + 
				+ ' between ' + CONVERT(VARCHAR, @LookbackStartDate,101) + ' and ' + CONVERT(VARCHAR, @LookbackEndDate,101)
			 END
		FROM #TTLookBack WHERE cust NOT IN (SELECT DISTINCT cust FROM #TTINT)
		SELECT @STAT = @@ERROR	
		IF @stat <> 0 GOTO EndOfProc
		SELECT @EndAlrtDate = GETDATE()
	

		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		SELECT 	'Alert', AlertNo, TT.TRANNO 
		FROM 	Alert,#TT1 TT
		WHERE 	Alert.Cust = TT.Cust 
		AND ((Charindex('deposits', Alert.[Desc]) > 0 AND RecvPay = 1) 
		OR	(Charindex('withdrawals', Alert.[Desc]) > 0 AND RecvPay = 2))
		AND Alert.WLCode = @WLCode 
		AND Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
		SELECT @STAT = @@ERROR 
		IF @stat <> 0 GOTO EndOfProc

		-- Create test alerts for customers who have exceeded only in the interval period
		SELECT @StartAlrtDate = GETDATE()
		INSERT INTO Alert (WLCode, STATUS, CreateDate, LASTOPER, 
						LASTMODIFY, CUST, ACCOUNT, IsTest, [DESC]) 
		SELECT @WLCode, 0, GETDATE(), NULL, NULL, Cust, NULL , 1,
			CASE WHEN RecvPay = 1 THEN
				LEFT('Customer ''' + cust + ''' has deposits of ' +
				(SELECT ( DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + ' between ' + 
										CONVERT(VARCHAR(100),startdate, 101) + ' and ' + CONVERT(VARCHAR(100),enddate, 101)+ ', ')
						   FROM #ttint t2, #int 
						   WHERE t1.cust = t2.cust
						   AND t2.recvpay = 1
						   AND t2.intno = #int.intno
						   ORDER BY #int.startdate desc
						   FOR XML PATH( '' )),2000 )
			ELSE
				LEFT('Customer ''' + cust + ''' has withdrawals of ' +
				(SELECT ( DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + ' between ' + 
										CONVERT(VARCHAR(100),startdate, 101) + ' and ' + CONVERT(VARCHAR(100),enddate, 101) + ', ')
						   FROM #ttint t2, #int 
						   WHERE t1.cust = t2.cust
						   AND t2.recvpay = 2
						   AND t2.intno = #int.intno
						   ORDER BY #int.startdate desc
						   FOR XML PATH( '' )),2000 )
			END
		FROM #TTINT t1
		GROUP BY cust, recvpay
		SELECT @STAT = @@ERROR	
		IF @stat <> 0 GOTO EndOfProc
		SELECT @EndAlrtDate = GETDATE()

		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		SELECT 	DISTINCT 'Alert', AlertNo, TT.TRANNO
		FROM 	Alert,#TT1 TT, #INT I1, #TTINT TTI
		WHERE 	Alert.Cust = TT.Cust 
		AND ((CHARINDEX('deposits', Alert.[Desc]) > 0 AND TT.RecvPay = 1) 
		OR   (CHARINDEX('withdrawals', Alert.[Desc]) > 0 AND TT.RecvPay = 2))
		AND Alert.WLCode = @WLCode 
		AND Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
		AND TT.intno = TTI.intno 
		AND TTI.intno = I1.intno
		AND TTI.recvpay = TT.recvpay
		AND TT.bookdate BETWEEN dbo.ConvertSqlDateToInt(I1.startdate) AND dbo.ConvertSqlDateToInt(I1.enddate)
		SELECT @STAT = @@ERROR
	END
	ELSE
	BEGIN
		IF @WLTYPE = 0
		BEGIN
			-- Create alerts for customers who have exceeded only in the look back period
			SELECT @StartAlrtDate = GETDATE()
			INSERT INTO Alert (WLCode,  STATUS, CreateDate, LASTOPER, 
								LASTMODIFY, CUST, ACCOUNT, IsTest, [DESC]) 
			SELECT @WLCode,0,GETDATE(), NULL, NULL, Cust, NULL , 0,
 				 CASE WHEN RecvPay = 1 THEN
					'Customer  ''' + Cust + ''' had deposits of ' 
					+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + 
					+ ' between ' + CONVERT(VARCHAR, @LookbackStartDate,101) + ' and ' + CONVERT(VARCHAR, @LookbackEndDate,101)
				 ELSE
					'Customer  ''' + Cust + ''' had withdrawals of ' 
					+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + 
					+ ' between ' + CONVERT(VARCHAR, @LookbackStartDate,101) + ' and ' + CONVERT(VARCHAR, @LookbackEndDate,101)
				 END
			FROM #TTLookBack WHERE cust NOT IN (SELECT DISTINCT cust FROM #TTINT)
			SELECT @STAT = @@ERROR	
			IF @stat <> 0 GOTO EndOfProc
			SELECT @EndAlrtDate = GETDATE()
		
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	'Alert', AlertNo, TT.TRANNO 
			FROM 	Alert,#TT1 TT
			WHERE 	Alert.Cust = TT.Cust 
			AND ((Charindex('deposits', Alert.[Desc]) > 0 AND RecvPay = 1) 
			OR	(Charindex('withdrawals', Alert.[Desc]) > 0 AND RecvPay = 2))
			AND Alert.WLCode = @WLCode 
			AND Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc

			-- Create alerts for customers who have exceeded only in the interval period
			SELECT @StartAlrtDate = GETDATE()
			INSERT INTO Alert (WLCode, STATUS, CreateDate, LASTOPER, 
							LASTMODIFY, CUST, ACCOUNT, IsTest, [DESC]) 
			SELECT @WLCode, 0, GETDATE(), NULL, NULL, Cust, NULL , 0,
				CASE WHEN RecvPay = 1 THEN
					LEFT('Customer ''' + cust + ''' has deposits of ' +
					(SELECT ( DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + ' between ' + 
											CONVERT(VARCHAR(100),startdate, 101) + ' and ' + CONVERT(VARCHAR(100),enddate, 101)+ ', ')
							   FROM #ttint t2, #int 
							   WHERE t1.cust = t2.cust
							   AND t2.recvpay = 1
							   AND t2.intno = #int.intno
							   ORDER BY #int.startdate desc
							   FOR XML PATH( '' )), 2000 )
				ELSE
					LEFT('Customer ''' + cust + ''' has withdrawals of ' +
					(SELECT ( DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + ' between ' + 
											CONVERT(VARCHAR(100),startdate, 101) + ' and ' + CONVERT(VARCHAR(100),enddate, 101)+ ', ')
							   FROM #ttint t2, #int 
							   WHERE t1.cust = t2.cust
							   AND t2.recvpay = 2
							   AND t2.intno = #int.intno
							   ORDER BY #int.startdate desc
							   FOR XML PATH( '' )), 2000 )
				END
			FROM #TTINT t1
			GROUP BY cust, recvpay
			SELECT @STAT = @@ERROR	
			IF @stat <> 0 GOTO EndOfProc
			SELECT @EndAlrtDate = GETDATE()

			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	DISTINCT 'Alert', AlertNo, TT.TRANNO
			FROM 	Alert,#TT1 TT, #INT I1, #TTINT TTI
			WHERE 	Alert.Cust = TT.Cust 
			AND ((CHARINDEX('deposits', Alert.[Desc]) > 0 AND TT.RecvPay = 1) 
			OR   (CHARINDEX('withdrawals', Alert.[Desc]) > 0 AND TT.RecvPay = 2))
			AND Alert.WLCode = @WLCode 
			AND Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
			AND TT.intno = TTI.intno 
			AND TTI.intno = I1.intno
			AND TTI.recvpay = TT.recvpay
			AND TT.bookdate BETWEEN dbo.ConvertSqlDateToInt(I1.startdate) AND dbo.ConvertSqlDateToInt(I1.enddate)
			SELECT @STAT = @@ERROR
			IF @stat <> 0 GOTO EndOfProc
		END
		ELSE IF @WLTYPE = 1
		BEGIN
			-- Create cases for customers who have exceeded only in the look back period
			SELECT @StartAlrtDate = GETDATE()
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
				ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
				RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
				ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
				EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
				OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
				REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
				WLCode, WLDESC, CREATETIME )
			SELECT	NULL, @startDate, Cust, NULL,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, 
 				 CASE WHEN RecvPay = 1 THEN
					'Customer  ''' + Cust + ''' had deposits of ' 
					+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + 
					+ ' between ' + CONVERT(VARCHAR, @LookbackStartDate,101) + ' and ' + CONVERT(VARCHAR, @LookbackEndDate,101)
				 ELSE
					'Customer  ''' + Cust + ''' had withdrawals of ' 
					+ DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + 
					+ ' between ' + CONVERT(VARCHAR, @LookbackStartDate,101) + ' and ' + CONVERT(VARCHAR, @LookbackEndDate,101)
				 END , GETDATE() 
			FROM #TTLookBack WHERE cust NOT IN (SELECT DISTINCT cust FROM #TTINT)
			SELECT @STAT = @@ERROR	
			IF @stat <> 0 GOTO EndOfProc
			SELECT @EndAlrtDate = GETDATE()

			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	DISTINCT 'SUSPACT', RecNo, TT.TRANNO 
			FROM 	SuspiciousActivity SuspAct,#TT1 TT
			WHERE 	SuspAct.Cust = TT.Cust 
			AND ((Charindex('deposits', WLDesc) > 0 AND TT.RecvPay = 1) OR
				 (Charindex('withdrawals', WLDesc) > 0 AND TT.RecvPay = 2))
			AND suspAct.WLCode = @WLCode 
			AND	SuspAct.CreateTime BETWEEN @StartAlrtDate AND @EndAlrtDate	
			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc

			-- Create cases for customers who have exceeded only in the interval period
			SELECT @StartAlrtDate = GETDATE()
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
				ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
				RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
				ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
				EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
				OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
				REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
				WLCode, WLDESC, CREATETIME )
			SELECT	NULL, @startDate, Cust, NULL,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, 
				CASE WHEN RecvPay = 1 THEN
					LEFT('Customer ''' + cust + ''' has deposits of ' +
					(SELECT ( DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + ' between ' + 
											CONVERT(VARCHAR(100),startdate, 101) + ' and ' + CONVERT(VARCHAR(100),enddate, 101)+ ', ')
							   FROM #ttint t2, #int 
							   WHERE t1.cust = t2.cust
							   AND t2.recvpay = 1
							   AND t2.intno = #int.intno
							   ORDER BY #int.startdate desc
							   FOR XML PATH( '' )), 2000 )
				ELSE
					LEFT('Customer ''' + cust + ''' has withdrawals of ' +
					(SELECT (DBO.BSA_InternalizationMoneyToString( TranAmt)  +SPACE(1) + @BaseCurr + ' between ' + 
											CONVERT(VARCHAR(100),startdate, 101) + ' and ' + CONVERT(VARCHAR(100),enddate, 101)+ ', ')
							   FROM #ttint t2, #int 
							   WHERE t1.cust = t2.cust
							   AND t2.recvpay = 2
							   AND t2.intno = #int.intno
							   ORDER BY #int.startdate desc
							   FOR XML PATH( '' )), 2000 )
				END, Getdate()
			FROM #TTINT t1
			GROUP BY cust, recvpay
			SELECT @STAT = @@ERROR	
			IF @stat <> 0 GOTO EndOfProc
			SELECT @EndAlrtDate = GETDATE()

			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	DISTINCT 'SUSPACT', RecNo, TT.TRANNO 
				FROM 	SuspiciousActivity SuspAct,#TT1 TT, #INT I1, #TTINT TTI
				WHERE	SuspAct.Cust = TT.Cust
				AND ((Charindex('deposits', WLDesc) > 0 AND TT.RecvPay = 1) OR
					 (Charindex('withdrawals', WLDesc) > 0 AND TT.RecvPay = 2))
				AND	SuspAct.WlCode = @WlCode
				AND	SuspAct.CreateTime BETWEEN @StartAlrtDate AND @EndAlrtDate	
				AND TT.intno = TTI.intno 
				AND TTI.intno = I1.intno
				AND TTI.recvpay = TT.recvpay
				AND TT.bookdate BETWEEN dbo.ConvertSqlDateToInt(I1.startdate) AND dbo.ConvertSqlDateToInt(I1.enddate)
			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc
		END 
	END
	IF @trnCnt = 0 BEGIN
		COMMIT TRAN USR_CstXOvrYInt
		GOTO Cleanup
	END

EndOfProc:
IF (@stat <> 0) BEGIN 
	ROLLBACK TRAN USR_CstXOvrYInt
	GOTO Cleanup
END	

Cleanup:
	DROP TABLE #TT1
	DROP TABLE #TTINT
	DROP TABLE #TTLookBack
	DROP TABLE #INT
	DROP TABLE #TempActHist

	RETURN @stat
GO


