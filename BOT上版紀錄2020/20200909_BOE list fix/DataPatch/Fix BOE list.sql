use OFAC
go

select * from ofac..SDNTable(nolock) where listtype = 'BOE' 
and ([name] like '%i??%' or [FirstName] like '%i??%' or [MiddleName] like '%i??%'
or [LastName] like '%i??%' or [Title] like '%i??%')

  
begin tran
update ofac..SDNTable set [name] = replace([name],'i??','''') where listtype = 'BOE' and [name] like '%i??%'

update ofac..SDNTable set [FirstName] = replace([FirstName],'i??','''') where listtype = 'BOE' and [FirstName] like '%i??%'

update ofac..SDNTable set [MiddleName] = replace([MiddleName],'i??','''') where listtype = 'BOE' and [MiddleName] like '%i??%'

update ofac..SDNTable set [LastName] = replace([LastName],'i??','''') where listtype = 'BOE' and [LastName] like '%i??%'

update ofac..SDNTable set [Title] = replace([Title],'i??','''') where listtype = 'BOE' and [Title] like '%i??%'


commit