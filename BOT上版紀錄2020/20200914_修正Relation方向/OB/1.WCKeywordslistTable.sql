ALTER TABLE WCKeywordslistTable Add [LastModify] datetime Null;

Create TRIGGER [dbo].[WCKeywordslistTableModif] ON [dbo].[WCKeywordslistTable]
FOR UPDATE
AS

  update [dbo].[WCKeywordslistTable] set LastModify = GETDATE()
  from inserted u , WCKeywordslistTable w where u.Abbreviation = w.Abbreviation


GO
