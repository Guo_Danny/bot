USE [OFAC]
GO

/****** Object:  Index [EntNumIdx_Notes]    Script Date: 7/31/2020 2:31:42 PM ******/
CREATE NONCLUSTERED INDEX [NoteTypeIdx_Notes] ON [dbo].[Notes]
(
	[NoteType],
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

alter table Notes add ListID varchar(15) NULL;