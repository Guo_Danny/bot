--Enable the Sanctions keywords.

select * from WCKeywordslistTable 
--where Abbreviation like 'DFAT%' 
--where Abbreviation like 'FATF%' 
--where Abbreviation like 'HKMA%' and kwtype='sanctions' 
--where Abbreviation like 'MAS%' and KWType='SANCTIONS' 
--where Abbreviation like 'NS-PLC%' and KWType='SANCTIONS' 
--where Abbreviation like 'OFAC%' and KWType='SANCTIONS' 
--where Abbreviation like 'TW%' and KWType='SANCTIONS' 
--where CountryAuthority='UNITED KINGDOM' and KWType='SANCTIONS' 
--Where Abbreviation like 'un%' and KWType='SANCTIONS' 
--Where Abbreviation like '%311%' and KWType='SANCTIONS' 
--Where Abbreviation like '%BXA%'
--where Abbreviation like 'EU%'
--where Abbreviation like '%WMD%' and CountryAuthority = 'USA'
--where SOURCENAME like '%CAATSA%' and KWType = 'SANCTIONS'

--FOR LN
--where KWType = 'SANCTIONS' and CountryAuthority in ('CANADA', 'JAPAN')

begin tran
update WCKeywordslistTable set used = 1, SDNPROGRAM = 'SANCTIONS' 
--where Abbreviation like 'DFAT%' 
--where Abbreviation like 'FATF%' 
--where Abbreviation like 'HKMA%' and kwtype='sanctions' 
--where Abbreviation like 'MAS%' and KWType='SANCTIONS' 
--where Abbreviation like 'NS-PLC%' and KWType='SANCTIONS' 
--where Abbreviation like 'OFAC%' and KWType='SANCTIONS' 
--where Abbreviation like 'TW%' and KWType='SANCTIONS' 
--where CountryAuthority='UNITED KINGDOM' and KWType='SANCTIONS' 
--Where Abbreviation like 'un%' and KWType='SANCTIONS' 
--Where Abbreviation like '%311%' and KWType='SANCTIONS' 
--Where Abbreviation like '%BXA%'
--where Abbreviation like 'EU%'
--where Abbreviation like '%WMD%' and CountryAuthority = 'USA'
--where SOURCENAME like '%CAATSA%' and KWType = 'SANCTIONS'

--FOR LN
--where KWType = 'SANCTIONS' and CountryAuthority in ('CANADA', 'JAPAN')


begin tran
update WorldCheck set UPDDate = getdate()
from WCKeywordslistTable wckl
join WCKeywords wck on wckl.Abbreviation = wck.Word
join WorldCheck wc on wck.UID = wc.UID
where wckl.Used = 1
and SDNPROGRAM = 'SANCTIONS'