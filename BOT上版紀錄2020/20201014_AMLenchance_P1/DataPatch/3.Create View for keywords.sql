--*INQ-WC-BOE
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and Program = 'SANCTIONS' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where KWType = 'SANCTIONS' and CountryAuthority = 'UNITED KINGDOM'))

--*INQ-WC-FATF
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like '%FATF%'))

--*INQ-WC-WMD
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like '%WMD%' and CountryAuthority = 'USA'))

--*INQ-WC-OFAC
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like 'OFAC%'))

--*INQ-WC-EU
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like 'EU%'))

--*INQ-WC-NSPLC
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like 'NS-PLC%'))

--*INQ-WC-CAATSA
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where SOURCENAME like '%CAATSA%'))

--*INQ-WC-TWMOJ
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation = 'TWMOJ'))

--*INQ-WC-TWMPB
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like '%TWMPB%'))

--*INQ-WC-DPL
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Explanation like '%denied person%'))

--*INQ-WC-FINCEN311
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Explanation like '%FINCEN%'))

--*INQ-WC-DFAT
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like 'DFAT%'))

--*INQ-WC-HKMA
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like 'HKMA%' and CountryAuthority = 'HONG KONG'))

--*INQ-WC-MAS
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like 'MAS-%' or Abbreviation = 'MAS'))

--*INQ-WC-UN
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like 'UN%' and CountryAuthority = 'UNKNOWN'))

--*INQ-WC-JPCA (only for LN)
--select * from sdntable
 where (SDNTable.Status != '4' or SDNTable.Status is null) and ListType = 'World-Check' and EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where KWType = 'SANCTIONS' and CountryAuthority in ('CANADA', 'JAPAN')))
