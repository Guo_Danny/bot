use PCDB
GO

--*INQ-WC-SANCTIONS
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' and program = ''SANCTIONS'' ',@ParameterRangeValue=''

exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-SANCTIONS',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1


--*INQ-WC-BOE
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where KWType = ''SANCTIONS'' and CountryAuthority = ''UNITED KINGDOM'')
)',@ParameterRangeValue=''

exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-BOE',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-FATF
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like ''%FATF%'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-FATF',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-WMD
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like ''%WMD%'' and CountryAuthority = ''USA'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-WMD',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-OFAC
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like ''%OFAC%'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-OFAC',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-EU
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like ''%EU%'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-EU',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-NSPLC
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like ''NS-PLC%'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-NSPLC',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-CAATSA
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like ''%CAATSA%'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-CAATSA',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-TWMOJ
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation = ''TWMOJ'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-TWMOJ',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-TWMPB
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like ''%TWMPB%'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-TWMPB',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-DPL
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Explanation like ''%denied person%'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-DPL',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-FINCEN311
--202011.30 modify
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
--(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Explanation like ''%FINCEN%'')
(select EntNum from WCKeywords where Word in (''USTREAS.311'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-FINCEN311',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-DFAT
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like ''%DFAT%'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-DFAT',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-HKMA
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like ''HKMA%'' and CountryAuthority = ''HONG KONG'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-HKMA',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-MAS
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like ''MAS-%'' or Abbreviation = ''MAS'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-MAS',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-UN
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where Abbreviation like ''UN%'' and CountryAuthority = ''UNKNOWN'')
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-UN',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-JPCA
exec WCDB_SaveQueryPlusParameter @ObjectId=5,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where (SDNTable.Status != ''4'' or SDNTable.Status is null) 
 and SDNTable.ListType = ''World-Check'' 
 and SDNTable.EntNum in 
(select EntNum from WCKeywords where Word in (select Abbreviation from WCKeywordslistTable where KWType = ''SANCTIONS'' and CountryAuthority in (''CANADA'', ''JAPAN''))
)',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=5,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-JPCA',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-Keywords
exec WCDB_SaveQueryPlusParameter @ObjectId=92,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where logtext like ''%WorldCheck Keywords List :%'' ',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=92,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-Keywords',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*INQ-WC-imp-comp
exec WCDB_SaveQueryPlusParameter @ObjectId=92,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where logtext like ''%Complete WorldCheck Job %'' ',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=92,@OperCode='PRIMEADMIN',@ViewName='*INQ-WC-imp-comp',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

