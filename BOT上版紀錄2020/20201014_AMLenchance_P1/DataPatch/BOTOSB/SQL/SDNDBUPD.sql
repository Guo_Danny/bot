declare @distdate date,@date date, @builddate datetime, @disdatetime datetime;
 set @date = getdate()
select @date = min(a.LogDate) from OFAC..SDNChangeLog a where a.LogDate > convert(varchar(10),getdate(),111) 
and objectid = 'WORLDCHECK'
and LogText like '%Complete WorldCheck Job%' 

select @disdatetime = max(a.LogDate) from OFAC..SDNChangeLog a where a.LogDate > convert(varchar(10),getdate(),111) 
and objectid = 'WORLDCHECK'
and LogText like '%Complete WorldCheck Job%' 

select @builddate = max(LogDate) from OFAC..SDNChangeLog where LogDate > convert(varchar(10),getdate(),111) 
and logtext like '%Completed all Sanction Data Rules.%'

select @distdate = DistributionDate from OFAC..SDNDbStatus
select @distdate, @disdatetime, @date, @builddate 

if @date > @distdate
update OFAC..SDNDbStatus set DistributionDate = @date, LastSDNUpdate = CurrSDNUpdate ,CurrSDNUpdate = @disdatetime
