﻿use OFAC
go

declare @SDate varchar(20), @logtext varchar(255), @oper varchar(255),@cont int;
select @Sdate=convert( varchar(10), getdate() -1,112);
set @logtext = 'WorldCheck Import Job Incomplete, Please contact Taipei center PRIME support team to fix import failed.'
--select @SDate;

    select @cont = count(*) from SDNChangeLog
	where logdate >= @Sdate
	and logtext like '%Complete WorldCheck Job%'

declare cur_oper CURSOR FOR
select Oper from PSEC..RoleMembers rm where [role] like '%caseap%'
open cur_oper;
fetch next from cur_oper into @oper;
while @@fetch_status = 0 
begin

    --select @logtext,@oper;
    if @cont < 3
    --select @logtext,@oper;
    insert into PBSA..Notification (Subsystem,ObjectType, ObjectId,NotifyTime,SnoozeTime,NotifyOper,NotifyDescr,NotificationType,RecurType,RecurExpireDate,NextRecurDate)values
    ('WorldCheck Import Fail','Task','Task',GETDATE(), getdate(),@oper,@logtext,'Task',0,null,null);
    
    
    insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
    values (getdate(),'Prime',@logtext,'Ann','Event','Notifications',@oper);

  fetch next from cur_oper into @oper;
end
close cur_oper;
DEALLOCATE cur_oper;
