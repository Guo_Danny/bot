USE [OFAC]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [ListType_SDNTable_IDX]    Script Date: 9/5/2019 5:47:14 PM ******/
CREATE NONCLUSTERED INDEX [ListType_SDNTable_IDX] ON [dbo].[SDNTable]
(
	[ListType] ASC,
	[EntNum] ASC,
	[Program] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


