USE [OFAC]
GO

/****** Object:  StoredProcedure [dbo].[OFS_AddFilterMsg]    Script Date: 10/22/2020 9:58:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[OFS_AddFilterMsg] @seqNumb int, @msgType GenericIdType, @msgLen int,
            @msg text
  As
  declare @stat int

  -- Not using begin / commit tran since this routine is used with other
  -- table updates in ofacsrvr.cpp
  insert into MsgTable
      (SeqNumb, MsgType, MsgLen, Msg)
    values
      ( @seqNumb, @msgType, @msgLen, @msg )

  --20190212 fix v10.1.3 patch8 user msg reference NULL issue.
  
  declare @UserMessageReference varchar(200),@pos_s int, @pos_e int, @str_tag varchar(200), @amt varchar(12)

  select @UserMessageReference = Ref from FilterTranTable where SeqNumb = @seqNumb

  if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,4)) in ('103','202','202C','205','205C') or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '4') and CHARINDEX(':32A:',@msg) > 0
	begin
	--select @pos_e = CHARINDEX(char(13),@msg,CHARINDEX(':20:',@msg)),
		   --@pos_s = CHARINDEX(':20:',@msg)
	--select @str_tag20 = SUBSTRING(@msg,CHARINDEX(':20:',@msg)+4,@pos_e-@pos_s-4)
	--select @pos_s = CHARINDEX(':32A:',@msg)
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':32A:', @Msg) + 11 , 3))
	select @amt = replace(replace(replace(replace(SUBSTRING(substring(@Msg,CHARINDEX(':32A:', @Msg) + 14, 30),1,CHARINDEX(':',substring(@Msg,CHARINDEX(':32A:', @Msg) + 14, 30))),':',''),',','.'), char(10),''),char(13),'')
	update FilterTranTable set Currency = @str_tag, Amount = @amt where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and CHARINDEX(':32B:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':32B:', @Msg) + 5 , 3))
	select @amt = replace(replace(replace(replace(SUBSTRING(substring(@Msg,CHARINDEX(':32A:', @Msg) + 8, 30),1,CHARINDEX(':',substring(@Msg,CHARINDEX(':32A:', @Msg) + 8, 30))),':',''),',','.'), char(10),''),char(13),'')
	update FilterTranTable set Currency = @str_tag, Amount = @amt where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,4)) in ('190','290')) and CHARINDEX(':32D:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':32D:', @Msg) + 11 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '3' or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':33B:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':33B:', @Msg) + 5 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '3' or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':32C:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':32C:', @Msg) + 5 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '3' or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':32H:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':32H:', @Msg) + 5 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '3' or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':32M:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':32M:', @Msg) + 5 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '3' or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':33E:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':33E:', @Msg) + 5 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '3' or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':33H:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':33H:', @Msg) + 5 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '3' or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':33T:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':33T:', @Msg) + 5 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '3' or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':33S:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':33S:', @Msg) + 5 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '3' or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':33V:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':33V:', @Msg) + 5 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '3' or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':34B:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':34B:', @Msg) + 5 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '3' or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':34H:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':34H:', @Msg) + 5 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '3' or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':34G:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':34G:', @Msg) + 5 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '3' or RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':71F:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':71F:', @Msg) + 5 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':34A:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':34A:', @Msg) + 11 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '4') and CHARINDEX(':33C:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':33C:', @Msg) + 11 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end
	
  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '4') and CHARINDEX(':33D:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':33D:', @Msg) + 11 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':90B:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':90B:', @Msg) + 17 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':19A:',@msg) > 0
	begin
	if ISNUMERIC (SUBSTRING(@Msg,15,1)) = 0
		begin
		select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':19A:', @Msg) + 13 , 3))
		end
	else
		begin
		select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':19A:', @Msg) + 12 , 3))
		end
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':11A:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':11A:', @Msg) + 12 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':92F:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':92F:', @Msg) + 12 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '5') and CHARINDEX(':92H:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':92H:', @Msg) + 12 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end
 
	--20201015 add MT7xx currency tag, begin Line
  else if RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '7' and CHARINDEX(':32',@msg) =0 and (CHARINDEX(':33',@msg) > 0 or CHARINDEX(':34',@msg) > 0 )
	begin
	  if CHARINDEX(':33B:',@msg) > 0
	     select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':33B:', @Msg) + 5 , 3))
         
       if CHARINDEX(':34B:',@msg) > 0
	      select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':34B:', @Msg) + 5 , 3))
    
	   if CHARINDEX(':33A:',@msg) > 0
	     select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':33A:', @Msg) + 11 , 3))
         
       if CHARINDEX(':34A:',@msg) > 0
	      select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':34A:', @Msg) + 11 , 3))   

	   update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end
  --20201015 add MT7xx currency tag, end line
  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '9') and CHARINDEX(':60M:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':60M:', @Msg) + 11 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '9') and CHARINDEX(':62M:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':62M:', @Msg) + 11 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '9') and CHARINDEX(':60F:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':60F:', @Msg) + 11 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '9') and CHARINDEX(':64:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':64:', @Msg) + 10 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '9') and CHARINDEX(':65:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':65:', @Msg) + 10 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '9') and CHARINDEX(':90D:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':90D:', @Msg) + 10 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end

  else if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,1)) = '9') and CHARINDEX(':90C:',@msg) > 0
	begin
	select @str_tag = RTRIM(SUBSTRING(@Msg,CHARINDEX(':90C:', @Msg) + 10 , 3))
	update FilterTranTable set Currency = @str_tag where SeqNumb = @seqNumb;
	end
	
  --20190212 modify end line.

  select @stat = @@ERROR
  return @stat

GO


