USE [Pcdb]
GO

/****** Object:  StoredProcedure [dbo].[WCDB_GetParameterSelections]    Script Date: 10/22/2020 10:57:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[WCDB_GetParameterSelections]
	(
	@TableId   int,
	@CodeId int,
	@NameId int

	)
  As

Declare @SelectClause 		nvarchar(512)
Declare @FromClause    		nvarchar(512)
Declare @TableName		nvarchar(512)
Declare @DatabaseName		nvarchar(512)
Declare @OrderBy		nvarchar(512)
Declare @CboQuery		nvarchar(2048)

--Select Clause
set @SelectClause ='select Distinct  '


	select @SelectClause=@SelectClause + ListViewColumns.ColumnName + ', '
	from ListViewColumns 
	where TableId=@TableId and Position=@CodeId

if @CodeId = @NameId
  begin
	--Use Column Expression to create descriptive Name when name and code are the same

	--select @SelectClause=@SelectClause + ListViewColumns.ColumnExpression + ', '
	--from ListViewColumns 
	--where TableId=@TableId and [Position]=@CodeId

	select @SelectClause=@SelectClause + ListViewColumns.ColumnExpression
	from ListViewColumns 
	where TableId=@TableId and [Position]=@NameId

	/* -- Correct Column Expression by removing TableName.--*/
	select @TableName=TableName	
	from ListViewColumns  
	where TableId=@TableId and [Position]=@NameId

	set @SelectClause=Replace(@SelectClause, @TableName + '.', '')

  end

else
  begin

	--select @SelectClause=@SelectClause + ListViewColumns.ColumnName + ', '
	--from ListViewColumns 
	--where TableId=@TableId and Position=@CodeId


	select @SelectClause=@SelectClause + ListViewColumns.ColumnName
	from ListViewColumns 
	where TableId=@TableId and [Position]=@NameId
  end




--From Clause
select @FromClause=' from ' + DatabaseName + '.dbo.' + TableName, @DatabaseName=DatabaseName
from ListViewTables 
where TableId=@TableId


--Order by clause

set @OrderBy = ' order by 2 '

if @TableId = 136 and @CodeId = 6
	set @OrderBy = ' where status != 4 order by 2 '

--Build View Query
set @CboQuery = @SelectClause + @FromClause + @OrderBy

exec WCDB_ExecQuery @CboQuery, @DatabaseName


GO


