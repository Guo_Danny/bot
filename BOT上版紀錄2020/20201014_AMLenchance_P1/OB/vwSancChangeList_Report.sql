USE [OFAC]
GO

/****** Object:  View [dbo].[vwSancChangeList_Report]    Script Date: 11/9/2020 2:53:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO







CREATE View [dbo].[vwSancChangeList_Report]
AS
	SELECT title,Entnum,ListType,Program,Attribute,[status],Deleted,SDNName,DIFF  FROM
(
	SELECT '|' as title,Entnum , [Name] AS 'SDNName', [Name] AS 'Diff', Program, [Status], Deleted, ListType, 'SDN Entity' AS  'Attribute'
	FROM SDNTable WHERE (ISNULL(LastModifDate,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus) or
		ISNULL(CreateDate,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus)) and (
		[Status] = 2 or [Status] = 3 or [Status] = 4)
	UNION
	SELECT '|' as title,alt.Entnum,sdn.[Name] AS 'SDNName', altname AS 'Diff', Program, alt.[Status], ALT.Deleted, ListType ,'Alternate' AS 'Attribute'
	FROM SDNTable AS sdn
		INNER JOIN sdnalttable AS alt ON
		sdn.Entnum = alt.entnum
	Where (ISNULL(alt.LastModifDate,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus) or
		ISNULL(alt.CreateDate,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus)) and (
		alt.[Status] = 2 or alt.[Status] = 3 or alt.[Status] = 4)
	UNION
	SELECT '|' as title,addr.Entnum,sdn.[name] AS 'SDNName',
		 LTRIM(RTRIM(ISNULL(addr.Freeformat, '')))+ ',' +  LTRIM(RTRIM(ISNULL(addr.Address, ''))) + ',' + LTRIM(RTRIM(ISNULL(addr.Address2, '')))  + ',' + LTRIM(RTRIM(ISNULL(addr.Address3, ''))) + ',' +  LTRIM(RTRIM(ISNULL(addr.Address4 ,''))) + ',' + LTRIM(RTRIM(ISNULL(addr.City, '')))
		+ ',' + LTRIM(RTRIM(ISNULL(addr.State, ''))) + ',' + LTRIM(RTRIM(ISNULL(addr.Country, ''))) + ',' + LTRIM(RTRIM(ISNULL(addr.PostalCode, '') )) AS 'Diff',
		Program, addr.[Status], addr.Deleted, ListType ,'Address' AS 'Attribute' 
	FROM SDNTable AS sdn
		INNER JOIN sdnaddrtable AS addr ON
		sdn.Entnum = addr.Entnum
	Where (ISNULL(addr.LastModifDate,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus) or
		ISNULL(addr.CreateDate,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus)) and (
		addr.[Status] = 2 or addr.[Status] = 3 or addr.[Status] = 4)
	UNION
	SELECT '|' as title,dobs.Entnum,sdn.[Name] AS 'SDNName', dobs.Dob AS 'Diff', Program, dobs.[Status], 0, ListType ,'DOB' AS 'Attribute' 
	FROM SDNTable AS sdn
		INNER JOIN dobs AS dobs ON
		sdn.Entnum = dobs.Entnum
	Where (ISNULL(dobs.Lastmodify,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus) or
		ISNULL(dobs.CreateDate,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus)) and (
		dobs.[Status] = 2 or dobs.[Status] = 3 or dobs.[Status] = 4)
	--201909 add keywords and notes for match to the count of SDN alert email
	UNION
	SELECT '|' as title,ky.Entnum,sdn.[Name] AS 'SDNName', ky.Word AS 'Diff', Program, ky.[Status], 0, ListType ,'Keywords' AS 'Attribute' 
	FROM SDNTable AS sdn
		INNER JOIN Keywords AS ky ON
		sdn.Entnum = ky.Entnum
	Where (ISNULL(ky.Lastmodify,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus) or
		ISNULL(ky.CreateDate,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus)) and (
		ky.[Status] = 2 or ky.[Status] = 3 or ky.[Status] = 4)
	UNION
	SELECT '|' as title,sdnalt.Entnum,sdn.[Name] AS 'SDNName', kya.Word AS 'Diff', Program, kya.[Status], 0, ListType ,'KeywordsAlt' AS 'Attribute' 
	FROM SDNTable AS sdn
		INNER JOIN SDNAltTable AS sdnalt ON sdn.Entnum = sdnalt.Entnum
		INNER JOIN KeywordsAlt AS kya ON kya.AltNum = sdnalt.AltNum
	Where (ISNULL(kya.Lastmodify,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus) or
		ISNULL(kya.CreateDate,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus)) and (
	kya.[Status] = 2 or kya.[Status] = 3 or kya.[Status] = 4)
	UNION
	SELECT '|' as title,nts.Entnum,sdn.[Name] AS 'SDNName', nts.Note AS 'Diff', Program, nts.[Status], 0, ListType ,'Notes' AS 'Attribute' 
	FROM SDNTable AS sdn
		INNER JOIN Notes AS nts ON
		sdn.Entnum = nts.Entnum
	Where (ISNULL(nts.Lastmodify,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus) or
		ISNULL(nts.CreateDate,0) > (select top 1 IsNull(LastSDNUpdate,0) from SDNDBStatus)) and (
		nts.[Status] = 2 or nts.[Status] = 3 or nts.[Status] = 4) and isnull(nts.ListID,'') = ''
		) DT
GO


