"C:\Program Files (x86)\WinSCP\WinSCP.exe" /script=D:\Prime\BOTOSB\WCFTP_all.txt

d:
cd\worldcheck

7z e -y d:\worldcheck\premium-world-check.csv.gz

COPY /Y d:\worldcheck\premium-world-check.csv \\10.7.16.149\worldcheck\premium-world-check.csv
COPY /Y d:\worldcheck\world-check-deleted-v2.csv \\10.7.16.149\worldcheck\world-check-deleted-v2.csv

set hour=%time:~0,2%
if "%hour:~0,1%" == " " set hour=0%hour:~1,1%

COPY d:\worldcheck\premium-world-check.csv.gz  d:\worldcheck\backup\premium-world-check.csv.gz.%DATE:~7,2%

COPY d:\worldcheck\world-check-deleted-v2.csv  d:\worldcheck\backup\world-check-deleted-v2.csv.%DATE:~7,2%.%hour%

del /F d:\worldcheck\premium-world-check.csv

del /F d:\worldcheck\premium-world-check.csv.gz

del /F d:\worldcheck\world-check-deleted-v2.csv

exit