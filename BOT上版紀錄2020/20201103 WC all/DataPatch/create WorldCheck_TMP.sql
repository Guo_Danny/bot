USE [OFAC]
GO

/****** Object:  Table [dbo].[WorldCheck_TMP]    Script Date: 11/2/2020 10:26:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WorldCheck_TMP](
	[UID] [int] NOT NULL,
	[LastName] [varchar](max) NULL,
	[FirstName] [varchar](max) NULL,
	[Aliases] [varchar](max) NULL,
	[LowQAliases] [varchar](max) NULL,
	[AlterSpell] [varchar](max) NULL,
	[Category] [varchar](max) NULL,
	[Title] [varchar](max) NULL,
	[SubCategory] [varchar](max) NULL,
	[Position] [varchar](max) NULL,
	[Age] [int] NULL,
	[DOB] [varchar](50) NULL,
	[DOBS] [varchar](164) NULL,
	[PlaceOfBirth] [varchar](max) NULL,
	[Deceased] [varchar](50) NULL,
	[Passports] [varchar](max) NULL,
	[SSN] [varchar](max) NULL,
	[IDENTIFICATION_NUMBERS] [varchar](max) NULL,
	[Location] [varchar](max) NULL,
	[Country] [varchar](max) NULL,
	[CITIZENSHIP] [varchar](max) NULL,
	[Company] [varchar](max) NULL,
	[Type] [varchar](max) NULL,
	[LinkedTo] [varchar](max) NULL,
	[FurtherInfo] [varchar](max) NULL,
	[Keywords] [varchar](max) NULL,
	[ExSources] [varchar](max) NULL,
	[UpdCategory] [varchar](max) NULL,
	[Entered] [varchar](50) NULL,
	[Updated] [varchar](50) NULL,
	[Editor] [varchar](max) NULL,
	[AgeDate] [varchar](50) NULL,
 CONSTRAINT [PK_WorldCheck_TMP] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


USE [OFAC]
GO

/****** Object:  Index [PK_TempWorldCheckUPD]    Script Date: 1/17/2019 4:35:46 PM ******/
CREATE NONCLUSTERED INDEX [PK_TempWorldCheckUPD] ON [dbo].[WorldCheck_TMP]
(
	[UPDDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


USE [OFAC]
GO

/****** Object:  Index [PK_WorldCheckEnt]    Script Date: 1/17/2019 4:36:00 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [PK_WorldCheckEnt] ON [dbo].[WorldCheck_TMP]
(
	[Entnum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


