
--disable sdntable

begin tran
update ofac..SDNTable set Status = 4, LastModifDate = getdate()
where ListType in 
(
select Code from ofac..OptionTbl
where enabled = 1
)
commit


--disable listtype

begin tran
update ofac..ListType set Enabled = 0
where Code in (select Code from ofac..OptionTbl
where enabled = 1
and code in (select distinct ListType from ofac..SDNTable(nolock) where EntNum < 10000000))
commit
--disable OptionTbl

begin tran
update ofac..OptionTbl set Enabled = 0
where enabled = 1
and code in (select distinct ListType from ofac..SDNTable(nolock) where EntNum < 10000000)
commit