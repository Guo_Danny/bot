/*
select sdn.EntNum, sdn.ListType,sdn.Program, sdn.Status, wc.UPDDate from WCKeywordslistTable wckl
join WCKeywords wck on wckl.Abbreviation = wck.Word
join WorldCheck wc on wc.UID = wck.UID
join SDNTable sdn on sdn.EntNum = wc.Entnum
where wckl.SDNPROGRAM = 'SANCTIONS'
and wckl.KWType != 'SANCTIONS'
and wckl.Abbreviation not in ('BXA','CAATSA228-WC','FATF','FATF-SD')
*/

begin tran
update WCKeywordslistTable set Used = 0 where SDNPROGRAM = 'SANCTIONS'
and KWType != 'SANCTIONS'
and Abbreviation not in ('BXA','CAATSA228-WC','FATF','FATF-SD')

commit

begin tran
update WorldCheck set UPDDate = getdate()
from WCKeywordslistTable wckl
join WCKeywords wck on wckl.Abbreviation = wck.Word
join WorldCheck wc on wc.UID = wck.UID
join SDNTable sdn on sdn.EntNum = wc.Entnum
where wckl.SDNPROGRAM = 'SANCTIONS'
and wckl.KWType != 'SANCTIONS'
and wckl.Abbreviation not in ('BXA','CAATSA228-WC','FATF','FATF-SD')
commit



/*
select * from WCKeywordslistTable 
where SDNPROGRAM = 'SANCTIONS'
and KWType != 'SANCTIONS'
and Abbreviation not in ('BXA','CAATSA228-WC','FATF','FATF-SD')
*/
