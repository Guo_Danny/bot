begin tran
update ofac..SDNTable set FirstName = null, LastName = null
where EntNum > 20000000
and [type] != 'individual'
and (isnull(FirstName, '') != '' or isnull(LastName, '') != '')

commit