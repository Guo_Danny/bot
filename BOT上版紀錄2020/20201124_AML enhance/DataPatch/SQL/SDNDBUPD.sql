declare @distdate date,@date date, @builddate datetime, @disdatetime datetime;
 set @date = getdate()

--取得最近一次world check 轉入成功的日期
select @date = min(a.LogDate) from OFAC..SDNChangeLog a where a.LogDate > convert(varchar(10),getdate(),111) 
and objectid = 'WORLDCHECK'
and LogText like '%Complete WorldCheck Job%' 

--取得最近一次world check 轉入成功並且build file image的日期時間點
select @disdatetime = max(a.LogDate) from OFAC..SDNChangeLog a where a.LogDate > convert(varchar(10),getdate(),111) 
and objectid = 'WORLDCHECK'
and LogText like '%Complete WorldCheck Job%' 

select @builddate = max(LogDate) from OFAC..SDNChangeLog where LogDate > convert(varchar(10),getdate(),111) 
and logtext like '%Completed all Sanction Data Rules.%'

select @distdate = DistributionDate from OFAC..SDNDbStatus
select @distdate, @disdatetime, @date, @builddate 

--比較前一次DistributionDate與最近一次world check 轉入成功的日期
--若前一次DistributionDate較小，則更新DistributionDate為最近一次world check 轉入成功的日期
if @date > @distdate

update SDNDbStatus set DistributionDate = @date, 
LastSDNUpdate = CurrSDNUpdate ,
CurrSDNUpdate = @disdatetime



