--Query
select vp.ViewId,vt.ViewName, vp.ParameterValue from pcdb..ViewTbl vt
join pcdb..ViewParameters vp on vt.ViewId = vp.ViewId
where ViewName = '*INQ-WC-FINCEN311'
--

begin tran

update pcdb..ViewParameters set ParameterValue = ' where (SDNTable.Status != ''4'' or SDNTable.Status is null)    and SDNTable.ListType = ''World-Check''    and SDNTable.EntNum in   (select EntNum from WCKeywords where Word in (''USTREAS.311'')  )'
where viewid = (select vp.ViewId from pcdb..ViewTbl vt
join pcdb..ViewParameters vp on vt.ViewId = vp.ViewId
where ViewName = '*INQ-WC-FINCEN311')

commit
