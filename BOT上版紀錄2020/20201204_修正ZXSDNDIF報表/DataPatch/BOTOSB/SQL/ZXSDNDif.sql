select substring(UPPER(DATENAME(Weekday, GETDATE())),1,3) + '*','Current Distribute Date: ' + convert(char(10), DistributionDate, 111) as 'UPDATE - ALERT CHANGE' from OFAC.dbo.SDNDbStatus(nolock)

/*
select ListType, count(distinct Entnum) as 'Count' from OFAC.dbo.vwSancChangeList  
where (ListType in (select code from OFAC.dbo.OptionTbl) and ListType not in ('PEP', 'USER'))
group by ListType
order by ListType
*/

print ''
/*
select substring(UPPER(DATENAME(Weekday, GETDATE())),1,3) + '*','***************************************************************************'
select substring(UPPER(DATENAME(Weekday, GETDATE())),1,3) + '*','We had modified the following alternate names by branch''s requirement '
select substring(UPPER(DATENAME(Weekday, GETDATE())),1,3) + '*','ACT, AKA, APR, ACCT, ADD, ACC, BIC, BBK, BNF, CASE, CRED, DISTRICT, FEE, FAC,'
select substring(UPPER(DATENAME(Weekday, GETDATE())),1,3) + '*','GU, HKMA, INS, INC, IND, MAC, MIO, MEI, PAC, SAC, SSI, USD, UPD, VIA, YORK'
select substring(UPPER(DATENAME(Weekday, GETDATE())),1,3) + '*','***************************************************************************'
*/
declare @Altname varchar(600), @altlist varchar(3000), @count int; 
set @Altname = '';
set @altlist = '';
set @count = 0;
declare C_cur CURSOR FOR
select distinct AltName from OFAC..SDNAltTable where Status = 4 and Deleted = 1 order by AltName
open C_cur;
fetch next from C_cur into @Altname;
WHILE @@FETCH_STATUS = 0

begin
	
	set @Altname = rtrim(@Altname) + ',';
	set @altlist = @altlist + @Altname;
	set @count = @count + 1;
	fetch next from C_cur into @Altname;
	
	if @count > 9
	begin 
		set @altlist = @altlist + char(13) + char(10) + substring(UPPER(DATENAME(Weekday, GETDATE())),1,3) + '* ';
		set @count = 0;
	end	

end

close C_cur;
deallocate C_cur;

set @altlist = substring(rtrim(@altlist),1,len(rtrim(@altlist))-1);

select substring(UPPER(DATENAME(Weekday, GETDATE())),1,3) + '*','***************************************************************************'

select substring(UPPER(DATENAME(Weekday, GETDATE())),1,3) + '*',@altlist

select substring(UPPER(DATENAME(Weekday, GETDATE())),1,3) + '*','***************************************************************************'

print ''

select '*',rtrim(convert(char(15),ListType)) + ' by Entnum Total: ' as 'List' ,count(distinct Entnum) as 'Count' from OFAC.dbo.vwSancChangeList_Report  
--where  listtype='World-Check' and program in ('DPL','UN','EU','TWMPB','SANCTIONS')
--where (ListType='World-Check' and program in (select sdnprogram from OFAC.dbo.WCKeywordslistTable where used = 1 and sdnprogram not like '%news%')
--or 
--ListType = 'HQ Sharing List')
group by ListType
order by ListType

print ''

/*select Entnum,substring(SDNName,1,50) SDNName,substring(DIFF,1,60) DIFF, ListType, substring(Program,1,30)Program,Attribute, 
case when status = 2 then 'New' when status = 3 then 'Modify' when status = 4 then 'Delete' else 'Other' end as 'Status'
from OFAC.dbo.vwSancChangeList  (nolock)
where (ListType in (select code from OFAC.dbo.OptionTbl) and ListType not in ('PEP', 'USER'))
union */
select substring(UPPER(DATENAME(Weekday, GETDATE())),1,3) + title as flag,Entnum,ListType,substring(Program,1,30)Program,Attribute,
case when status = 2 then 'New' when status = 3 then 'Modify' when status = 4 then 'Delete' else 'Other' end as 'Status',
substring(SDNName,1,50) SDNName,substring(DIFF,1,60) DIFF   

from OFAC.dbo.vwSancChangeList_Report  (nolock)
--where ListType='World-Check' and program in ('DPL','UN','EU','TWMPB','SANCTIONS')
--where (ListType='World-Check' and program in (select sdnprogram from OFAC.dbo.WCKeywordslistTable where used = 1 and sdnprogram not like '%news%')
--or 
--ListType = 'HQ Sharing List')
order by ListType,program, Entnum

print ''

print 'WORLD-CHECK SANCTION OF KEYWORDS LISTS'

select substring(UPPER(DATENAME(Weekday, GETDATE())),1,3) + '!' as flag ,substring(Abbreviation,1,50) Abbreviation,substring(replace(replace(CountryAuthority,char(10),' '),char(13),' '),1,50) CountryAuthority,substring(replace(replace(KWType,char(10),''),char(13),''),1,50) KWType,substring(replace(replace(SDNProgram,char(10),''),char(13),''),1,50) Program,substring(replace(replace(SOURCENAME,char(10),''),char(13),''),1,500) SOURCENAME from ofac..WCKeywordslistTable
where Used = 1
order by SDNProgram desc, Abbreviation asc



