USE PBSA
GO

update PBSA..SysParam set BusDate = convert(char(10),dateadd(m, datediff(m,0,getdate()),0),110)

DECLARE @strTmp varchar(5000), @strTmp1 varchar(5000),@strTmp2 varchar(5000), @strTmp3 varchar(5000)
DECLARE @SQLStartDate datetime, @SQLEndDate datetime, @BusDate varchar(10)
set @BusDate = convert(char(10),dateadd(m, datediff(m,0,getdate()),0),110)

exec dbo.BSA_GetDateRange @BusDate, 'Month', 'PREV', 3, 
@SQLStartDate OUTPUT, @SQLEndDate OUTPUT

select @strTmp='', @strTmp1='',@strTmp2='',@strTmp3=''

/*
====================IHRCtry3Mth
*/

select @strTmp = Params from PBSA..Watchlist  where WLCode like 'IHRCtry3Mth%'
--set @strTmp = '<Params><Param Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Quarter" /><Param Name="@StartDate" Alias="Start Date for Lookback MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="12/01/2020" /><Param Name="@EndDate" Alias="End Date for Lookback MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="12/31/2020" /><Param Name="@minIndCount" Alias="Minimum transaction count" Value="10" /><Param Name="@minIndAmt" Alias="Minimum individual transaction amount" Value="90000" /><Param Name="@maxIndAmt" Alias="Maximum individual transaction amount. For no maximum use -1" Value="-1" /><Param Name="@minSumAmount" Alias="Minimum aggregated transaction amount." Value="1500000" /><Param Name="@maxSumAmount" Alias="Maximum aggregated transaction amount. For no maximum use -1" Value="-1" /><Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All." Value="I" /><Param Name="@accountTypeList" Alias="List of Account Types separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. Use -ALL- for All." Value="1015,1025,1035,1045" /><Param Name="@countryList" Alias="List of Countries separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@UseMonitor" Alias="Use Monitor Field in Country Table. 1 for Yes, 0 for No" Value="1" /><Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="3" /><Param Name="@branchList" Alias="List of Branches separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@cashType" Alias="Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both" Value="2" /><Param Name="@ExcludeTranbyCustCtryCode" Alias="Exclude Transactions on Customer Country.&#xD;&#xA;          0 - Do not exclude,1 - By customer country,&#xD;&#xA;          2 - By Any customer country" Value="0" /><Param Name="@IncludeAddressFields" Alias="Include Address Fields to match on country.&#xD;&#xA;          0 - Do not Include,1 - Include" Value="0" /><Param Name="@UseRound" Alias="Indicate if only the high round amounts have to considered.&#xD;&#xA;          0 - Do not round,1 - Round" Value="0" /><Param Name="@Precision" Alias="Precision for rounding. Specify 3 for 1000, 2 for 100" Value="0" /></Params>'

select @strtmp1 = substring(@strTmp,1,CHARINDEX('@StartDate',@strTmp)+136)
select @strtmp2 = substring(@strTmp,CHARINDEX('@StartDate',@strTmp)+147,len(@strTmp) - CHARINDEX('@StartDate',@strTmp)+146)

set @strTmp3 = @strtmp1 + convert(varchar(10),@SQLStartDate,101) +  @strtmp2

update PBSA..Watchlist set Params = @strTmp3 where WLCode like 'IHRCtry3Mth%'

select @strTmp = Params from PBSA..Watchlist  where WLCode like 'IHRCtry3Mth%'
select @strtmp1 = substring(@strTmp,1,CHARINDEX('@EndDate',@strTmp)+132)
select @strtmp2 = substring(@strTmp,CHARINDEX('@EndDate',@strTmp)+143,len(@strTmp) - CHARINDEX('@StartDate',@strTmp)+142)

set @strTmp3 = @strtmp1 + convert(varchar(10),@SQLEndDate,101) + @strtmp2

update PBSA..Watchlist set Params = @strTmp3 where WLCode like 'IHRCtry3Mth%'


/*
====================OHRCtry3Mth
*/

select @strTmp = Params from PBSA..Watchlist  where WLCode like 'OHRCtry3Mth%'
--set @strTmp = '<Params><Param Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Quarter" /><Param Name="@StartDate" Alias="Start Date for Lookback MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="12/01/2020" /><Param Name="@EndDate" Alias="End Date for Lookback MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="12/31/2020" /><Param Name="@minIndCount" Alias="Minimum transaction count" Value="10" /><Param Name="@minIndAmt" Alias="Minimum individual transaction amount" Value="90000" /><Param Name="@maxIndAmt" Alias="Maximum individual transaction amount. For no maximum use -1" Value="-1" /><Param Name="@minSumAmount" Alias="Minimum aggregated transaction amount." Value="1500000" /><Param Name="@maxSumAmount" Alias="Maximum aggregated transaction amount. For no maximum use -1" Value="-1" /><Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All." Value="I" /><Param Name="@accountTypeList" Alias="List of Account Types separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. Use -ALL- for All." Value="1015,1025,1035,1045" /><Param Name="@countryList" Alias="List of Countries separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@UseMonitor" Alias="Use Monitor Field in Country Table. 1 for Yes, 0 for No" Value="1" /><Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="3" /><Param Name="@branchList" Alias="List of Branches separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" /><Param Name="@cashType" Alias="Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both" Value="2" /><Param Name="@ExcludeTranbyCustCtryCode" Alias="Exclude Transactions on Customer Country.&#xD;&#xA;          0 - Do not exclude,1 - By customer country,&#xD;&#xA;          2 - By Any customer country" Value="0" /><Param Name="@IncludeAddressFields" Alias="Include Address Fields to match on country.&#xD;&#xA;          0 - Do not Include,1 - Include" Value="0" /><Param Name="@UseRound" Alias="Indicate if only the high round amounts have to considered.&#xD;&#xA;          0 - Do not round,1 - Round" Value="0" /><Param Name="@Precision" Alias="Precision for rounding. Specify 3 for 1000, 2 for 100" Value="0" /></Params>'

select @strtmp1 = substring(@strTmp,1,CHARINDEX('@StartDate',@strTmp)+136)
select @strtmp2 = substring(@strTmp,CHARINDEX('@StartDate',@strTmp)+147,len(@strTmp) - CHARINDEX('@StartDate',@strTmp)+146)

set @strTmp3 = @strtmp1 + convert(varchar(10),@SQLStartDate,101) +  @strtmp2

update PBSA..Watchlist set Params = @strTmp3 where WLCode like 'OHRCtry3Mth%'

select @strTmp = Params from PBSA..Watchlist  where WLCode like 'OHRCtry3Mth%'
select @strtmp1 = substring(@strTmp,1,CHARINDEX('@EndDate',@strTmp)+132)
select @strtmp2 = substring(@strTmp,CHARINDEX('@EndDate',@strTmp)+143,len(@strTmp) - CHARINDEX('@StartDate',@strTmp)+142)

set @strTmp3 = @strtmp1 + convert(varchar(10),@SQLEndDate,101) + @strtmp2

update PBSA..Watchlist set Params = @strTmp3 where WLCode like 'OHRCtry3Mth%'
