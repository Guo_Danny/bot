1.
select WLCode,[Desc] from Watchlist where WLCode in ('IHRCtry3Mth','OHRCtry3Mth')

2.
update Watchlist set Params = replace(Params,'Param Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month"',
'Param Name="@Period" Alias="Activity Period. Last No Of Months, if Blank-Use Date Parameters" Value="3"') 
where WLCode in ('IHRCtry3Mth','OHRCtry3Mth')

3.
update Watchlist set Params = replace(Params, '<Param Name="@StartDate" Alias="Start Date for Lookback MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="2020/11/01" DataType="" />',
'<Param Name="@StartDate" Alias="Start Date for Lookback MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" DataType="" />')
where WLCode in ('IHRCtry3Mth','OHRCtry3Mth')

4.
update Watchlist set Params = replace(Params, '<Param Name="@EndDate" Alias="End Date for Lookback MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="2020/11/30" DataType="" />',
'<Param Name="@EndDate" Alias="End Date for Lookback MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" DataType="" />')
where WLCode in ('IHRCtry3Mth','OHRCtry3Mth')