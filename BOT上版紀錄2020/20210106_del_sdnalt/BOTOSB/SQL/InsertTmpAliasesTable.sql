/****** Script for SelectTopNRows command from SSMS  ******/
use ofac
go
declare @ListCreateDate datetime, @ListModifDate datetime,@CreateDate datetime, @LastModifDate datetime, 
@entnum varchar(15),@SDNName varchar(3000),@Aliases varchar(max), @maxalianum varchar(15), @location varchar(max), @sdntype varchar(30),
@AltType varchar(200), @Category varchar(200), @SDNName_tmp varchar(200), @Aliases_tmp varchar(max), @location_1 varchar(1000),@location_1_1 varchar(1000),@location_2 varchar(1000) ,@location_2_1 varchar(1000)
set @ListCreateDate = NULL;
set @ListModifDate = NULL;
set @CreateDate = NULL;
set @LastModifDate = NULL;
set @location_1_1 = '';
--select @maxalianum = case when alianum is null then 1 else alianum +1  end
-- from tmpaliasestable
select @maxalianum = max(alianum)+1 from TmpAliasesTable(nolock)

if @maxalianum is null
begin
set  @maxalianum =1
end

--begin tran
--delete  from TmpAliasesTable where entnum in 
--(   select entnum from TmpAliasesTable
--    group by entnum,altname    having count(1)  >1)




delete  from TmpAliasesTable where Entnum in (
select distinct t1.entnum from 
( select count(1) as countt1, entnum from (select distinct entnum,altname from [OFAC].[dbo].[TmpAliasesTable]) dt group by entnum ) T1  
inner join 
( select count(1) as countt2, entnum from (select entnum,altname from SDNAltTable where EntNum >= 20000000 and status !=4 
and isnull(listid,'') in ('alt','') and isnull(AltType,'') = 'a.k.a.' and entnum in 
(select entnum from sdntable where program != 'other' and status !=4) ) dt2 group by entnum ) T2 on T1.entnum = T2.EntNum and t1.countt1 != t2.countt2 
inner join worldcheck wc on t1.entnum= wc.entnum
)



--rollback 

declare cur CURSOR FOR
--select distinct sdn.EntNum,wc.Aliases
--from OFAC..SDNTable(nolock) sdn
--join OFAC..WorldCheck(nolock) wc on wc.Entnum = sdn.EntNum
--join ofac..SDNAltTable(nolock) sdna on sdna.EntNum = sdn.EntNum
--where isnull(wc.Aliases,'') != '' 
--and isnull(sdna.ListID,'') in ('ALT','')
--and isnull(sdna.AltType,'') = 'a.k.a.'
--and sdn.Status != 4
--and sdn.EntNum  >= 20000000
--and sdn.program != 'other'
--and sdn.entnum not in (select entnum from TmpAliasesTable)
--order by sdn.EntNum

	
	select entnum,aliases from worldcheck where entnum not in 
	(select entnum from TmpAliasesTable ) and isnull(DelFlag,'') != 1
	and entnum in (select entnum from sdntable where status !=4 and program !='other')
	 and isnull(aliases,'') != '' and rtrim(aliases) !=''
	order by entnum 



open cur;
fetch next from cur into @entnum,@Aliases;
WHILE @@fetch_status = 0 
begin

	-- get aliases
	set @Aliases_tmp = @Aliases;
	WHILE CHARINDEX(';',@Aliases_tmp) > 0
		begin
			-- part1
			set @location_2 = SUBSTRING(@Aliases_tmp, 1, CHARINDEX(';', @Aliases_tmp) - 1);
			set @Aliases_tmp = SUBSTRING(@Aliases_tmp, CHARINDEX(';', @Aliases_tmp) + 1, LEN(@Aliases_tmp) - CHARINDEX(';', @Aliases_tmp));

			--print @location_2

			/*--insert to table*/
			insert into TmpAliasesTable(EntNum,alianum,AltName,aliatype)
			values (@entnum, @maxalianum,@location_2,'aliases')
			set @maxalianum = @maxalianum + 1;
		end
		--check for the tail string
		if len(@Aliases_tmp) > 0
		begin
			set @location_2 = @Aliases_tmp;
			insert into TmpAliasesTable(EntNum,alianum,AltName,aliatype)
			values (@entnum, @maxalianum,@location_2,'aliases')
			set @maxalianum = @maxalianum + 1;
		end
		--print @location_2
  fetch next from cur into @entnum,@Aliases;
end
close cur;
DEALLOCATE cur;

--begin tran
update SDNAltTable set Status = 4, Remarks = 'Expired AKAs', LastModifDate = getdate()
from SDNAltTable sdna 
left join TmpaliasesTable(nolock) tmpsdna on sdna.EntNum = tmpsdna.EntNum and sdna.AltName = tmpsdna.AltName
where sdna.EntNum >= 20000000
and sdna.status = 1
and tmpsdna.AltName is null
and isnull(sdna.ListID,'') in ('ALT','')
and sdna.entnum  in (select entnum from sdntable where status != 4 and program !='other')

--rollback

--commit

