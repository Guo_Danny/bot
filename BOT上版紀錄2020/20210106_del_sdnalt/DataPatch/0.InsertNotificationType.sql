insert into pbsa..NotificationType(code, [name], CreateOper, CreateDate)
values('SDNALT','SDNALT discrepancy','Primeadmin',getdate())

insert into pbsa..NotificationType(code, [name], CreateOper, CreateDate)
values('FILEIMG','Build file image fail','Primeadmin',getdate())

insert into pbsa..NotificationType(code, [name], CreateOper, CreateDate)
values('WCIMPORT','WorldCheck import fail','Primeadmin',getdate())
