USE [OFAC]
GO

/****** Object:  Table [dbo].[TmpAliasesTable]    Script Date: 2/25/2021 4:38:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TmpAliasesTable](
	[EntNum] [int] NOT NULL,
	[AltName] [dbo].[SanctionName] NOT NULL,
	[FirstName] [dbo].[SanctionName] NULL,
	[LastName] [dbo].[SanctionName] NULL,
	[AliaType] [varchar](20) NOT NULL,
	[AliaNum] [int] NOT NULL,
 CONSTRAINT [pk_tmpaliasestable] PRIMARY KEY CLUSTERED 
(
	[EntNum] ASC,
	[AliaNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


