
use OFAC

truncate table TmpAliasesTable;

--procedure 1 

declare @ListCreateDate datetime, @ListModifDate datetime,@CreateDate datetime, @LastModifDate datetime, 
@entnum varchar(15),@SDNName varchar(3000),@Aliases varchar(max), @maxaltnum varchar(15), @location varchar(max), @sdntype varchar(30),
@AltType varchar(200), @Category varchar(200), @SDNName_tmp varchar(200), @Aliases_tmp varchar(max), @location_1 varchar(1000),@location_1_1 varchar(1000),@location_2 varchar(1000) ,@location_2_1 varchar(1000)
set @ListCreateDate = NULL;
set @ListModifDate = NULL;
set @CreateDate = NULL;
set @LastModifDate = NULL;
set @location_1_1 = '';


declare cur CURSOR FOR
select distinct sdn.EntNum,wc.Aliases
from OFAC..SDNTable(nolock) sdn
join OFAC..WorldCheck(nolock) wc on wc.Entnum = sdn.EntNum
join ofac..SDNAltTable(nolock) sdna on sdna.EntNum = sdn.EntNum
where isnull(wc.Aliases,'') != '' 
and isnull(sdna.ListID,'') in ('ALT','')
and isnull(sdna.AltType,'') = 'a.k.a.'
and sdn.Status != 4
and sdn.EntNum >= 20000000 and sdn.EntNum < 23000000
--and sdn.EntNum >= 23000000
--and sdn.EntNum in ('21776872')
order by sdn.EntNum

open cur;
fetch next from cur into @entnum,@Aliases;
WHILE @@fetch_status = 0 
begin

	-- get aliases
	set @Aliases_tmp = @Aliases;
	set @maxaltnum = '1';
	WHILE CHARINDEX(';',@Aliases_tmp) > 0
		begin
			
			-- part1
			set @location_2 = SUBSTRING(@Aliases_tmp, 1, CHARINDEX(';', @Aliases_tmp) - 1);
			set @Aliases_tmp = SUBSTRING(@Aliases_tmp, CHARINDEX(';', @Aliases_tmp) + 1, LEN(@Aliases_tmp) - CHARINDEX(';', @Aliases_tmp));

			--print @location_2

			/*--insert to table*/
			insert into TmpAliasesTable(EntNum,AliaNum,AltName,AliaType)
			values (@entnum, @maxaltnum,@location_2, 'ALT')
			set @maxaltnum = @maxaltnum + 1;
		end
		--check for the tail string
		if len(@Aliases_tmp) > 0
		begin
			set @location_2 = @Aliases_tmp;
			insert into TmpAliasesTable(EntNum,AliaNum,AltName,AliaType)
			values (@entnum, @maxaltnum,@location_2, 'ALT')
			set @maxaltnum = @maxaltnum + 1;
		end
		--print @location_2
  fetch next from cur into @entnum,@Aliases;
end
close cur;
DEALLOCATE cur;

/*
begin tran
update SDNAltTable set Status = 4, Remarks = 'Expired WC AKAs', LastModifDate = getdate()
from SDNAltTable sdna 
left join TmpAliasesTable tmpsdna on sdna.EntNum = tmpsdna.EntNum and sdna.AltName = tmpsdna.AltName
join SDNTable sdn on sdna.EntNum = sdn.EntNum
where sdna.EntNum > 20000000
and sdna.status = 1
and sdn.Status != 4
and sdna.EntNum >= 20000000 and sdna.EntNum < 23000000
--and sdna.EntNum >= 23000000
and tmpsdna.AltName is null
and isnull(sdna.ListID,'') in ('ALT','')
and isnull(sdna.AltType,'') = 'a.k.a.'


commit

Confirm

select distinct sdna.EntNum,sdna.AltName,sdna.ListID,sdna.AltType,sdna.Category, tmpsdna.EntNum,tmpsdna.AltName, sdna.Remarks
from SDNAltTable(nolock) sdna 
left join TmpAliasesTable(nolock) tmpsdna on sdna.EntNum = tmpsdna.EntNum and sdna.AltName = tmpsdna.AltName
join SDNTable sdn on sdna.EntNum = sdn.EntNum
where sdna.EntNum > 20000000
and sdna.status = 1
and sdn.Status != 4
and sdna.EntNum >= 20000000 and sdna.EntNum < 23000000
--and sdna.EntNum >= 23000000
and tmpsdna.AltName is null
and isnull(sdna.ListID,'') in ('ALT','')
and isnull(sdna.AltType,'') = 'a.k.a.'
order by sdna.entnum

*/
