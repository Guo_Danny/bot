select distinct t1.entnum,t1.countt1, t2.countt2, wc.UPDDate from 
( select count(1) as countt1, entnum from (select distinct entnum,altname from [OFAC].[dbo].[TmpAliasesTable]) dt group by entnum ) T1  
inner join 
( select count(1) as countt2, entnum from (select entnum,altname from [OFAC].[dbo].SDNAltTable where EntNum > 20000000 and status !=4 and isnull(listid,'') in ('alt','') and isnull(AltType,'') = 'a.k.a.' and entnum in (select entnum from [OFAC].[dbo].sdntable where program != 'other' and status !=4) ) dt2 group by entnum ) T2 on T1.entnum = T2.EntNum and t1.countt1 != t2.countt2 
inner join [OFAC].[dbo].worldcheck wc on t1.entnum= wc.entnum
