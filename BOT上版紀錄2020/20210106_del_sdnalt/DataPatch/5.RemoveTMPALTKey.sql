USE [OFAC]
GO

/****** Object:  Index [PK_TmpSDNAltTable_AltNum]    Script Date: 3/5/2021 11:01:29 AM ******/
ALTER TABLE [dbo].[TmpSDNAltTable] DROP CONSTRAINT [PK_TmpSDNAltTable_AltNum]
GO

alter table TmpSDNAltTable alter column altnum int null

