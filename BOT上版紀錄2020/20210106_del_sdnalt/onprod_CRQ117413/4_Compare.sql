USE OFAC
GO

--check listid is null
--select * from SDNAltTable where EntNum > 20000000 and Status != 4 and listid is null

--update SDNAltTable set listid = 'ALT' where EntNum > 20000000 and Status != 4 and listid is null

declare @now datetime;
set @now = getdate();

declare @ListCreateDate datetime, @ListModifDate datetime,@CreateDate datetime, @LastModifDate datetime, 
@entnum varchar(15),@SDNName varchar(3000),@Aliases varchar(max), @maxaltnum varchar(15), @location varchar(max), @sdntype varchar(30),
@AltType varchar(200), @Category varchar(200), @SDNName_tmp varchar(200), @Aliases_tmp varchar(max), @location_1 varchar(1000),@location_1_1 varchar(1000),@location_2 varchar(1000) ,@location_2_1 varchar(1000),
@oper varchar(50);
set @ListCreateDate = NULL;
set @ListModifDate = NULL;
set @CreateDate = NULL;
set @LastModifDate = NULL;
set @location_1_1 = '';

--check again and write to notifications

	set @location_1_1 = 1;
	set @Aliases_tmp = '';


declare curnoti CURSOR FOR

select distinct t1.entnum from
--select t1.*,t2.* from 
( select count(1) as countt1, entnum from (select distinct entnum,altname from [OFAC].[dbo].[TmpAliasesTable]) dt group by entnum ) T1  
inner join 
( select count(1) as countt2, entnum from (select distinct entnum,altname from SDNAltTable where EntNum > 20000000 and status !=4 and isnull(listid,'') in ('alt','') and isnull(AltType,'') = 'a.k.a.' and entnum in (select entnum from sdntable where program != 'other' and status !=4) ) dt2 group by entnum ) T2 on T1.entnum = T2.EntNum and t1.countt1 != t2.countt2 
inner join worldcheck wc on t1.entnum= wc.entnum
order by t1.Entnum

open curnoti;
fetch next from curnoti into @entnum;
WHILE @@fetch_status = 0 
begin

	if @location_1_1 <= 5 begin
	set @Aliases_tmp = @Aliases_tmp + ',' + @entnum;
	
	
	set @location_1_1 = @location_1_1 + 1;
	end
	fetch next from curnoti into @entnum;
end
close curnoti;
DEALLOCATE curnoti;

if len(@Aliases_tmp) > 1 begin

	declare cur_oper CURSOR FOR
	select Oper from PSEC..OperRights
	where [Right] = 'CTRLCLIEN'
	and Oper not in ('ADMIN','PRIMEADMIN')
	open cur_oper;
	fetch next from cur_oper into @oper;
	while @@fetch_status = 0 
	begin
		
		insert into PBSA..Notification (Subsystem,ObjectType, ObjectId,NotifyTime,SnoozeTime,NotifyOper,NotifyDescr,NotificationType,RecurType,RecurExpireDate,NextRecurDate)values
		('SDNALTTable DISCREPANCY','SDNAlt','SDNAlt',GETDATE(), getdate(),@oper,'Altname discrepancy occurs, entnum : ' + @Aliases_tmp + ', etc, please contact support team for more details.','SdnAlt',0,null,null);
		
		fetch next from cur_oper into @oper;
	end
	close cur_oper;
	DEALLOCATE cur_oper;
end


