use ofac

go


select  count(1),entnum,altname from sdnalttable where status !=4 
and entnum in 

(select distinct t1.entnum from
-- (select t1.*,t2.*,wc.aliases from
 ( select count(1) as counttmpalia, entnum from
  (select distinct entnum,altname from [OFAC].[dbo].[TmpAliasesTable]) dt group by entnum ) T1
   inner join ( select count(1) as countsdnalt, entnum from 
   (select entnum,altname from SDNAltTable where EntNum > 20000000 
   and (status !=4 or Deleted = 1) and isnull(listid,'') in ('alt','') and isnull(AltType,'') = 'a.k.a.' 
   and entnum in (select entnum from sdntable where program != 'other' and status !=4) ) dt2 
   group by entnum ) T2 on T1.entnum = T2.EntNum and t1.counttmpalia != t2.countsdnalt 
   inner join worldcheck wc on t1.entnum= wc.entnum 
  -- order by t1.Entnum
  )  

  --and entnum in (select entnum from sdntable where program = 'sanctions')
   group by entnum,altname 
   having count(*) > 1 
   
   
 
 truncate table TempSDNAltTable;

insert into TempSDNAltTable (EntNum, AltNum, AltName, Deleted, CreateDate, LastOper)
select sdnb.EntNum,sdnb.AltNum, sdnb.AltName, 0, getdate(), 'PrimeAdmin' from SDNAltTable sdnb  
join 
(
select sdna.EntNum, sdna.AltName from SDNAltTable sdna where EntNum in (
select t1.entnum from 
( select count(1) as countt1, entnum from (select distinct entnum,altname from [OFAC].[dbo].[TmpAliasesTable]) dt group by entnum ) T1  
inner join 
( select count(1) as countt2, entnum from (select entnum,altname from SDNAltTable where EntNum >= 20000000 and (status !=4 or Deleted = 1) 
and isnull(listid,'') in ('alt','') and isnull(AltType,'') = 'a.k.a.' and entnum in 
(select entnum from sdntable where program != 'other' and status !=4) ) dt2 group by entnum ) T2 on T1.entnum = T2.EntNum and t1.countt1 != t2.countt2 
inner join worldcheck wc on t1.entnum= wc.entnum
)
group by sdna.EntNum, sdna.AltName
having count(sdna.AltNum) > 1
) X on X.entnum = sdnb.entnum and X.altname = sdnb.altname

begin tran
update SDNAltTable set Remarks = 'reserved alt'
--select sdna.EntNum, sdna.AltNum 
from SDNAltTable sdna 
join 
(
select a.EntNum,a.AltName, max(a.AltNum) maxaltnum from TempSDNAltTable a
group by a.EntNum, a.AltName
) T on T.entnum = sdna.entnum and T.maxaltnum = sdna.AltNum

commit

--select * from TempSDNAltTable a where EntNum = '20000001'

--select * from SDNAltTable where EntNum = '23098832' and AltName = 'JAMAT-UL-AHRAR'


 
 
   
 
 
 begin tran 
 delete SDNAltTable where altnum in 
 ( select sdna.AltNum from SDNAltTable sdna join 
 ( select a.EntNum,a.AltName, max(a.AltNum) maxaltnum 
 from TempSDNAltTable a group by a.EntNum, a.AltName ) T 
 on T.entnum = sdna.entnum and T.AltName = sdna.AltName 
 and isnull(sdna.Remarks,'') != 'reserved alt')
