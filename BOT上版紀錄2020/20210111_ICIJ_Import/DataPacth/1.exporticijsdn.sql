USE [OFAC]
GO

/****** Object:  Table [dbo].[exporticijsdn]    Script Date: 3/10/2021 10:16:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[exporticijsdn](
	[EntNum] [varchar](50) NULL,
	[Name] [varchar](max) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[ListID] [varchar](50) NULL,
	[ListType] [varchar](50) NULL,
	[Title] [varchar](50) NULL,
	[Program] [varchar](50) NULL,
	[Type] [varchar](50) NULL,
	[CallSign] [varchar](50) NULL,
	[VessType] [varchar](50) NULL,
	[Tonnage] [varchar](50) NULL,
	[GRT] [varchar](50) NULL,
	[VessFlag] [varchar](50) NULL,
	[VessOwner] [varchar](50) NULL,
	[Dob] [varchar](50) NULL,
	[PrimeAdded] [varchar](50) NULL,
	[Sex] [varchar](50) NULL,
	[Height] [varchar](50) NULL,
	[Weight] [varchar](50) NULL,
	[Build] [varchar](50) NULL,
	[Eyes] [varchar](50) NULL,
	[Hair] [varchar](50) NULL,
	[Complexion] [varchar](50) NULL,
	[Race] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Remarks] [varchar](max) NULL,
	[Remarks1] [varchar](50) NULL,
	[Remarks2] [varchar](50) NULL,
	[EffectiveDate] [varchar](50) NULL,
	[ExpiryDate] [varchar](50) NULL,
	[StandardOrder] [varchar](50) NULL,
	[SDNType] [varchar](50) NULL,
	[DataType] [varchar](50) NULL,
	[UserRec] [varchar](50) NULL,
	[Deleted] [varchar](50) NULL,
	[DuplicRec] [varchar](50) NULL,
	[IgnoreDerived] [varchar](50) NULL,
	[Status] [varchar](50) NULL,
	[ListCreatedate] [varchar](50) NULL,
	[ListModifDate] [varchar](50) NULL,
	[CreateDate] [varchar](50) NULL,
	[LastModifDate] [varchar](50) NULL,
	[LastOper] [varchar](50) NULL,
	[ts] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


