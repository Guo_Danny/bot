USE [OFAC]
GO

/****** Object:  Table [dbo].[exporticijrelationship]    Script Date: 3/10/2021 10:15:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[exporticijrelationship](
	[RelationshipId] [varchar](50) NULL,
	[EntNum] [varchar](50) NULL,
	[RelatedID] [varchar](50) NULL,
	[RelationshipPToR] [varchar](50) NULL,
	[RelationshipRToP] [varchar](50) NULL,
	[Status] [varchar](50) NULL,
	[ListCreateDate] [varchar](50) NULL,
	[ListModifDate] [varchar](50) NULL,
	[CreateOper] [varchar](50) NULL,
	[CreateDate] [varchar](50) NULL,
	[LastOper] [varchar](50) NULL,
	[LastModify] [varchar](50) NULL,
	[Ts] [varchar](50) NULL
) ON [PRIMARY]
GO


