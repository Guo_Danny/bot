
use OFAC
go

truncate table TempLoadRelationship;

--procedure 1 

declare @ListCreateDate datetime, @ListModifDate datetime,@CreateDate datetime, @CreateOper varchar(11), 
@entnum varchar(15),@RelationshipId int, @RelatedID varchar(15), @RelationshipPToR varchar(50),
@RelationshipRToP varchar(50), @Status int; 
set @ListCreateDate = NULL;
set @ListModifDate = NULL;
set @CreateDate = NULL;

select @RelationshipId = max(RelationshipId)+1 from Relationship(nolock)

declare cur CURSOR FOR
select [EntNum],[RelatedID],[RelationshipPToR],[RelationshipRToP],[Status],convert(datetime,substring(ListCreatedate,1,19),21),convert(datetime,substring(ListModifDate,1,19),21),[CreateOper],convert(datetime,substring(CreateDate,1,19),21)
from OFAC..exporticijrelationship(nolock)
order by entnum

open cur;
fetch next from cur into @entnum,@RelatedID,@RelationshipPToR,@RelationshipRToP,@Status,@ListCreateDate,@ListModifDate,@CreateOper,@CreateDate;
WHILE @@fetch_status = 0 
begin
	insert into TempLoadRelationship([RelationshipId],[EntNum],[RelatedID],[RelationshipPToR],[RelationshipRToP],[Status],[ListCreateDate],[ListModifDate],[CreateOper],[CreateDate],LastOper,LastModify)
	values(@RelationshipId,@entnum,@RelatedID,@RelationshipPToR,@RelationshipRToP,@Status,@ListCreateDate,@ListModifDate,@CreateOper,@CreateDate,@CreateOper,getdate());
	
	set @RelationshipId = @RelationshipId + 1;
	fetch next from cur into @entnum,@RelatedID,@RelationshipPToR,@RelationshipRToP,@Status,@ListCreateDate,@ListModifDate,@CreateOper,@CreateDate;
end
close cur;
DEALLOCATE cur;


insert into Relationship ([RelationshipId]
      ,[EntNum]
      ,[RelatedID]
      ,[RelationshipPToR]
      ,[RelationshipRToP]
      ,[Status]
      ,[ListCreateDate]
      ,[ListModifDate]
      ,[CreateOper]
      ,[CreateDate]
      ,[LastOper]
      ,[LastModify])
select [RelationshipId]
      ,[EntNum]
      ,[RelatedID]
      ,[RelationshipPToR]
      ,[RelationshipRToP]
      ,[Status]
      ,getdate()
      ,getdate()
      ,[CreateOper]
      ,getdate()
      ,[LastOper]
      ,getdate() from TempLoadRelationship

