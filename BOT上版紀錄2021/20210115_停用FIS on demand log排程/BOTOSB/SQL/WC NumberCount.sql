﻿declare @StrDate datetime ,@_txt varchar(500),@_txt2 varchar(500),@listtype varchar(50),@modidate char(10), @count varchar(10);

-- update listtype to WPEP where WC.subcatory is blank.
--201808 mark-- update OFAC.dbo.SDNTable set ListType = 'WPEP' where ListType = '0138WPEP' and Program = 'PEP';

--count for 0138wpep
set @_txt2 = '';

declare cur_count CURSOR FOR
select listtype,count(1)  from OFAC.dbo.sdntable(nolock)
where Status != 4 and listtype like '0%WPEP' group by Listtype;
open cur_count;
fetch next from cur_count into @listtype,@_txt2;
while @@fetch_status = 0
begin 
  insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
  values (getdate(),'Prime','Total Active '+ @listtype +' records :' + @_txt2,'Ann','Event','World-Check Count','Total Active '+ @listtype +' records :' + @_txt2);

  fetch next from cur_count into @listtype,@_txt2;
end
close cur_count;
deallocate cur_count;

-- negative news count
declare cur_negcount CURSOR FOR
select listtype,count(1)  from OFAC.dbo.sdntable(nolock)
where Status != 4 and listtype = 'WORLD-CHECK' and Program = 'negative news' group by Listtype;
open cur_negcount;
fetch next from cur_negcount into @listtype,@_txt2;
while @@fetch_status = 0
begin 
  insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
  values (getdate(),'Prime','Total Active Negative News records :' + @_txt2,'Ann','Event','World-Check Count','Total Active Negative News records :' + @_txt2);

  fetch next from cur_negcount into @listtype,@_txt2;
end
close cur_negcount;
deallocate cur_negcount;

