﻿use OFAC
go

declare @SDate datetime, @logtext varchar(255), @oper varchar(255),@subsystem varchar(25);
select @Sdate=convert(datetime, getdate())-1;
--select @SDate;
set @subsystem = 'OFAC server Stopped';
set @logtext = 'BOTOSB service stopped,please GO RESOURCES-UTILITIES-Process Manager for restarting';
declare cur_oper CURSOR FOR
select Oper from PSEC..OperRights
where [Right] = 'CTRLCLIEN'
and Oper not in ('ADMIN','PRIMEADMIN')
open cur_oper;
fetch next from cur_oper into @oper;
while @@fetch_status = 0 
begin
  
    select @oper;
    if @@ROWCOUNT > 0 
    begin 
    insert into PBSA..Notification (Subsystem,ObjectType, ObjectId,NotifyTime,SnoozeTime,NotifyOper,NotifyDescr,NotificationType,RecurType,RecurExpireDate,NextRecurDate)values
    (@subsystem,'Task','Task',GETDATE(), getdate(),@oper,@logtext,'Task',0,null,null);
    

    insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
    values (getdate(),'Prime',@logtext,'Ann','Event',@oper,'Notifications');
    end

  fetch next from cur_oper into @oper;
end
close cur_oper;
DEALLOCATE cur_oper;
