﻿use OFAC
go

declare @SDate datetime, @logtext varchar(255), @oper varchar(255);
select @Sdate=convert(datetime, getdate())-1;
--select @SDate;

declare cur_oper CURSOR FOR
select Oper from PSEC..OperRights
where [Right] = 'CTRLCLIEN'
open cur_oper;
fetch next from cur_oper into @oper;
while @@fetch_status = 0 
begin
  declare cur CURSOR FOR
    SELECT CONVERT(VARCHAR(16),LOGDATE,20) + ' ' + LogText + ', Please contact Taipei center PRIME support team to fix build file image failed.' FROM [OFAC].[dbo].[SDNChangeLog] where logtext like '%unable to build file image%' 
    and oper='0' and LogDate > @SDate;
  open cur;
  fetch next from cur into @logtext;
  WHILE @@fetch_status = 0 
  begin
    select @logtext,@oper;
    if @@ROWCOUNT > 0 
    --select @logtext,@oper;
    insert into PBSA..Notification (Subsystem,ObjectType, ObjectId,NotifyTime,SnoozeTime,NotifyOper,NotifyDescr,NotificationType,RecurType,RecurExpireDate,NextRecurDate)values
    ('OFAC BUILD IMAGE FAIL','Task','Task',GETDATE(), getdate(),@oper,@logtext,'Task',0,null,null);
    
    
    insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
    values (getdate(),'Prime',@logtext,'Ann','Event','Notifications',@oper);

    fetch next from cur into @logtext;
  end
  close cur;
  DEALLOCATE cur;

  fetch next from cur_oper into @oper;
end
close cur_oper;
DEALLOCATE cur_oper;
