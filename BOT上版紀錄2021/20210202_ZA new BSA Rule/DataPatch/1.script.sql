use PBSA
GO

declare @para_h varchar(500), @para_t varchar(5000), @para varchar(5000), @para_new varchar(5000);

select @para = Params from Watchlist where WLCode = 'DailyTrn'

select @para_t = SUBSTRING(@para,9,LEN(@para)-7)

set @para_h = '<Params><Param Name="@CustTypeList" Alias="Customer Types,Comma Separated(Or -ALL-)" Value="-ALL-" />'

set @para_t = REPLACE(@para_t,'Maximum Amount', 'Maximum Amount(-1 if no maximum)')

set @para_new = @para_h + @para_t

begin tran
update Watchlist set Params = @para_new, LastOper = 'primeadmin' where WLCode = 'DailyTrn'

rollback
--commit

--update title and desc from account to customer
update Watchlist set Title = replace(Title, 'accounts', 'customers'), [Desc] = REPLACE([desc], 'accounts', 'customers') where  WLCode = 'DailyTrn'


--insert new rule
INSERT INTO [PBSA].[WatchList]([WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], [CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], [ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], [OverrideExemption], [TemplateWLCode])
  VALUES('ICDailyTrn', 'Detect Customers with transactions between designated dollar amount within 1 day', 'Detects Customers that had transactions using a specified activity type 
and the total of these transactions were between a designated dollar 
amount range within 1 day.  Designed to be run Pre-EOD.', 1, 'USR_IC1DBetXY', 'Rule', 1, 1, NULL, NULL, NULL, 'PRIMEADMIN', GetDate(), NULL, GetDate(), NULL, NULL, 1, '', 0, 15, 0, 1, 1, '<Params><Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@minimumAmount" Alias="Minimum Amount" Value="20000" DataType="Money" /><Param Name="@maximumAmount" Alias="Maximum Amount" Value="-1" DataType="Money" /><Param Name="@RiskClassList" Alias="List of Risk Classes separated by comma. -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@BranchList" Alias="List of Branches separated by comma. -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@CustTypeList" Alias="List of Customer Types separated by Comma. -ALL- for All." Value="I" DataType="" /></Params>', 0, 1, 'User Defined', 1, 0, 'C1DayBetXY')

INSERT INTO [PBSA].[WatchList]([WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], [CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [Ts], [RuleType], [RuleText], [RuleFormat], [ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], [OverrideExemption], [TemplateWLCode])
  VALUES('OCDailyTrn', 'Detect Customers with transactions between designated dollar amount within 1 day', 'Detects Customers that had transactions using a specified activity type 
and the total of these transactions were between a designated dollar 
amount range within 1 day.  Designed to be run Pre-EOD.', 1, 'USR_OC1DBetXY', 'Rule', 1, 1, NULL, NULL, NULL, 'PRIMEADMIN', GetDate(), NULL, GetDate(), NULL, NULL, 1, '', 0, 15, 0, 1, 1, '<Params><Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@minimumAmount" Alias="Minimum Amount" Value="200000" DataType="Money" /><Param Name="@maximumAmount" Alias="Maximum Amount" Value="-1" DataType="Money" /><Param Name="@RiskClassList" Alias="List of Risk Classes separated by comma. -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@BranchList" Alias="List of Branches separated by comma. -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@CustTypeList" Alias="List of Customer Types separated by Comma. -ALL- for All." Value="E" DataType="" /></Params>', 0, 1, 'User Defined', 1, 0, 'C1DayBetXY')

--set as preEOD
update Watchlist set IsPreEOD = 1 where WLCode in ('IBetXY1M','OBetXY1M')