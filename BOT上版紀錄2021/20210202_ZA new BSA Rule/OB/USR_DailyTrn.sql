USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_DailyTrn]    Script Date: 2/1/2021 3:40:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[USR_DailyTrn] ( 
	@WLCode 	SCode,
	@TestAlert 	INT,
	@CustTypeList   VARCHAR(8000),
	@activityTypeList VARCHAR(8000), 
	@minimumAmount 	MONEY, 
	@maximumAmount 	MONEY,
	@RiskClassList 	VARCHAR(8000),
	@BranchList 	VARCHAR(8000)
)
AS
	/* RULE AND PARAMETER DESCRIPTION
	   Detects accounts with type between specified amount range
	   Designed to be ran Pre-EOD.
		@activityTypeList	List of Activity Types that will be considered. 
				   -ALL-, blank or NULL for all Types
		@minimumAmount	- Minimum amount
		@maximumAmount	- Maximum amount
		@RiskClassList	- List of Risk Classes that will be considered. 
				   -ALL-, blank or NULL for all Risk Classes
		@BranchList	- List of Branches that will be considered. 
				   -ALL-, blank or NULL for all Branches
		@CustTypeList - List of Customer Types that will be considered. 
				   -ALL-, blank or NULL for all Customer Types

	*/
		
	/*  Declarations */
	DECLARE	@description 	VARCHAR(2000),
		@desc 		VARCHAR(2000),
		@Id 		INT, 
		@WLType 	INT,
		@stat 		INT,
		@trnCnt 	INT
	DECLARE @STARTALRTDATE  DATETIME
	DECLARE @ENDALRTDATE    DATETIME
	
	DECLARE	@TT TABLE (
		Cust        VARCHAR(40),
		--Account 	VARCHAR(40),
		Recvpay 	INT,
		TotalAmt 	MONEY,
		BOOKDATE 	INT
	)
	-- Temporary table of Activity Types that have not been specIFied as Exempt
	DECLARE @ActType TABLE (
		Type	INT
	)
	
	SET NOCOUNT ON
	SET @stat = 0

	--20210107 add
	If @maximumAmount = -1
	Begin
		set @maximumAmount = 99999999999999;
	End	
	
	--- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */
	SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN USR_DailyTrn
	ELSE
		-- Already in a transaction
		SAVE TRAN USR_DailyTrn
	/* End standard stored procedure transaction header */
	
	/*  standard Rules Header */
	SELECT @description = [Desc], @WLType = WLType  
	FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
	SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)

	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	--INSERT INTO @TT(Account, RecvPay, TotalAmt, BookDate)
	INSERT INTO @TT(Cust, RecvPay, TotalAmt, BookDate)
		--SELECT 	account, recvpay, SUM(baseAmt), bookdate  --MASK 2021-01-28
		SELECT 	Cust, recvpay, SUM(baseAmt), bookdate
		FROM 	activity (NOLOCK)
		INNER	JOIN Customer ON activity.cust = customer.id 
		INNER	JOIN Account ON Activity.account = Account.id
		INNER	JOIN @ActType Act ON Act.Type = Activity.Type
		WHERE 	((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) AND
		    	((ISNULL(@BranchList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Account.Branch)) + ',', @BranchList ) > 0))  AND	
				((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) 
		--GROUP 	BY account, bookdate, recvpay   --MASK 2021-01-28
		AND ISNUMERIC(Cust)>0
		GROUP BY Cust, BookDate, RecvPay
		HAVING 	@minimumAmount <= SUM(baseamt) AND SUM(baseamt) <= @maximumAmount
	
	--1 means test, 0 means no
	IF @testAlert = 1 
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT, IsTest) 
			--SELECT @WLCode, case when Recvpay =1 Then 'Account: ''' + Account + ''' had ''$' 
			SELECT @WLCode, case when Recvpay =1 Then 'Cust: ''' + Cust + ''' had ''$' 
				+ CONVERT(VARCHAR, TotalAmt) + ''' of incoming types specified '  +
				'on ' + CONVERT(VARCHAR, bookdate) else
				--'Account: ''' + Account + ''' had ''$' 
				'Cust: ''' + Cust + ''' had ''$' 
				+ CONVERT(VARCHAR, TotalAmt) + ''' of outgoing types specified ' +
				'on ' + CONVERT(VARCHAR, bookdate) end , 0, 
				--GETDATE(), NULL, NULL, NULL, Account, 1 
				GETDATE(), NULL, NULL, Cust, NULL, 1 
		  	FROM 	@TT 
		SELECT @STAT = @@ERROR	
		SELECT @ENDALRTDATE = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
		
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	'Alert', AlertNO, TRANNO 
			FROM 	Activity a
			--INNER	JOIN @TT T ON a.account = t.account --Mask 2021-0128
			INNER	JOIN @TT T ON a.Cust = t.Cust
			AND 	a.bookdate = t.bookdate 
			AND	a.recvpay = t.recvpay
			--INNER	JOIN Alert ON A.Account = Alert.Account
			INNER	JOIN Alert ON a.Cust = Alert.Cust
			INNER	JOIN customer ON a.cust = customer.id
			--INNER	JOIN account ON a.account = account.id
			INNER	JOIN @ActType Act ON Act.Type = A.Type
			WHERE	((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) AND
			    ((ISNULL(@BranchList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(customer.OwnerBranch)) + ',', @BranchList ) > 0))  AND	
				((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) AND
				a.recvpay = case when Alert.[Desc] like '%of outgoing type%' then 2
				else 1 end and
	        		 CONVERT(VARCHAR,a.Bookdate) = substring(Alert.[DESC], charindex('types specified '  +
				'on ',Alert.[DESC],1)+ len('types specified '  +
				'on ')+1,8) and
				Alert.WLCode = @WLCode AND
				Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		
		SELECT @STAT = @@ERROR 
		IF @STAT <> 0  GOTO ENDOFPROC
	END 
	ELSE 
	BEGIN 
		IF @WLTYPE = 0 
		BEGIN
			SELECT @STARTALRTDATE = GETDATE()
			INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT) 
		 		--SELECT @WLCode, case when Recvpay =1 Then 'Account: ''' + Account + ''' had ''$'
				SELECT @WLCode, case when Recvpay =1 Then 'Cust: ''' + Cust + ''' had ''$' 
					+ CONVERT(VARCHAR, TotalAmt) + ''' of incoming types specified ' +
					'on ' + CONVERT(VARCHAR, bookdate) else
					--'Account: ''' + Account + ''' had ''$'
					'Cust: ''' + Cust + ''' had ''$' 
					+ CONVERT(VARCHAR, TotalAmt) + ''' of outgoing types specified ' + 
					'on ' + CONVERT(VARCHAR, bookdate) end , 0, 
				--GETDATE(), NULL, NULL, NULL, Account
				GETDATE(), NULL, NULL, Cust, NULL 
		  	FROM @TT 
			SELECT @STAT = @@ERROR
			SELECT @ENDALRTDATE = GETDATE()
			IF @STAT <> 0  GOTO ENDOFPROC
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
			SELECT @STARTALRTDATE = GETDATE()
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
				ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
				RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
				ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
				EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
				OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
				REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
				WLCode, WLDESC, CREATETIME )
			--SELECT	NULL, BookDate, Null, Account,
			SELECT	NULL, BookDate,  Cust, NULL, 
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				--@WLCode, case when Recvpay =1 Then 'Account: ''' + Account + ''' had ''$' 
				@WLCode, case when Recvpay =1 Then 'Cust: ''' + Cust + ''' had ''$'
				+ CONVERT(VARCHAR, TotalAmt) + ''' of incoming types specified '  +
				'on ' + CONVERT(VARCHAR, bookdate) else
				--'Account: ''' + Account + ''' had ''$' 
				'Cust: ''' + Cust + ''' had ''$'
				+ CONVERT(VARCHAR, TotalAmt) + ''' of outgoing types specified '  +
				'on ' + CONVERT(VARCHAR, bookdate) end , GETDATE() 
			FROM 	@TT 
			SELECT @STAT = @@ERROR	
			SELECT @ENDALRTDATE = GETDATE()
			IF @STAT <> 0  GOTO ENDOFPROC
		END
		
		IF @WLTYPE = 0 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   		SELECT 	'Alert', AlertNO, TRANNO 
				FROM 	Activity A
				--INNER	JOIN @TT T ON a.account = t.account 
				INNER	JOIN @TT T ON a.Cust = t.Cust
				AND 	a.bookdate = t.bookdate 
				AND	a.recvpay = t.recvpay
				--INNER	JOIN Alert ON a.Account = Alert.Account
				INNER	JOIN Alert ON a.Cust = Alert.Cust
				INNER	JOIN customer ON a.cust = customer.id
				--INNER	JOIN account ON a.account = account.id
				INNER	JOIN @ActType Act ON Act.Type = A.Type				
				WHERE	((ISNULL(@RiskClassList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) AND
				    ((ISNULL(@BranchList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(Customer.OwnerBranch)) + ',', @BranchList ) > 0)) AND
				    ((ISNULL(@CustTypeList, '') = '' OR
				    CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) AND
				 	a.recvpay = case when Alert.[Desc] like '%of outgoing type%' then 2
					else 1 end and
			        	CONVERT(VARCHAR,a.Bookdate) = substring(Alert.[DESC], 
					charindex('types specified '  +
					'on ',Alert.[DESC],1)+ len('types specified ' +	'on ')+1,8) and
					Alert.WLCode = @WLCode AND
					Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		
		SELECT @STAT = @@ERROR 
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   	SELECT 	'SUSPACT', RECNO, TRANNO 
			FROM 	Activity A 
			--INNER	JOIN @TT T ON A.Account = T.Account AND A.BookDate = T.BookDate 
			INNER	JOIN @TT T ON A.Cust = T.Cust AND A.BookDate = T.BookDate 
			AND	A.RecvPay = T.RecvPay
			--INNER	JOIN SUSPICIOUSACTIVITY S ON A.Account = S.Account
			INNER	JOIN SUSPICIOUSACTIVITY S ON A.Cust = S.Cust
			INNER	JOIN Customer ON A.Cust= Customer.ID
			--INNER	JOIN Account ON A.Account = Account.id 
			INNER	JOIN @ActType Act ON Act.Type = A.Type
			WHERE	((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) AND
				((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) AND
				a.recvpay = case when s.wldesc like '%of outgoing type%' then 2
				else 1 end and
	        		 CONVERT(VARCHAR,a.Bookdate) = substring(S.wldesc, charindex('types specified '  +
				'on ',S.wldesc,1)+ len('types specified '  +
				'on ')+1,8) and
				S.WLCode = @WLCode AND
				S.CREATETIME BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		SELECT @STAT = @@ERROR 
		END
	
	END 
	
	EndOfProc:
	IF (@stat <> 0) BEGIN 
	  ROLLBACK TRAN USR_DailyTrn
	  RETURN @stat
	END	
	
	IF @trnCnt = 0
	  COMMIT TRAN USR_DailyTrn
	RETURN @stat
	
GO


