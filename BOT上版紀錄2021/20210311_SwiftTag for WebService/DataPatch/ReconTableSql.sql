DECLARE @strSysDate char(100), @TempStr VARCHAR(5000);
select @strSysDate=rtrim(replace(CONVERT(CHAR(10),GETDATE()-1,111),'/','') ) ;
--SELECT @strsysdate='20150102'
select @strsysdate, len(@strsysdate);

DECLARE @STRTEMP1 VARCHAR(200), @STRTEMP2 VARCHAR(200), @STRTEMP3 VARCHAR(200), @STRTEMP4 VARCHAR(200);
DECLARE @STRTEMP5 VARCHAR(200), @STRTEMP6 VARCHAR(200), @STRTEMP7 VARCHAR(200), @STRTEMP8 VARCHAR(200);
DECLARE @STRTEMP9 VARCHAR(200), @STRTEMP10 VARCHAR(200), @STRTEMP11 VARCHAR(200), @STRTEMP12 VARCHAR(200);

SELECT @STRTEMP1='select CONVERT(CHAR(2),' +''''+ '1'+ ''''+') as '+ ''''+ 'Flag'+ ''',' + 'CONVERT(CHAR(40),replace(REPLACE(CASEREFERENCE,'+ ''''+ '.pri'+ ''',' + '''' + '''' + '),' + ''''+ '-1'+ ''','+''''+''''+')) as ' + ''''+ 'Message(Batch)Name'+''''+',' 
SELECT @STRTEMP2=' CONVERT(CHAR(10),SearchDate,101)+'+ '''' + ' ' + '''' + '+CONVERT(char(8),SearchDate,114)' + ' as '+''''+'Time-In'+''''+',' 
SELECT @STRTEMP3=' CONVERT(CHAR(10),SearchDate,101)+'+ '''' + ' ' + '''' + '+CONVERT(char(8),SearchDate,114)' + ' as '+''''+'Time-Processed'+''''+','
SELECT @STRTEMP4=' 1 as '+''''+'Total Msgs'+''''+',' 
--SELECT @STRTEMP1
--SELECT @STRTEMP2
--SELECT @STRTEMP3
--SELECT @STRTEMP4
SELECT @STRTEMP5=' (select 
    case 
       when sancpartycount>0  then 1
       when sancpartycount=0  then 0
    end
) as '+ ''''+'Bad'+''''+',' 

SELECT @STRTEMP6=' (select 
    case 
       when sancpartycount=0  then 1
       when sancpartycount>0  then 0
    end
)  as '+''''+'Good'+''''+',' 

SELECT @STRTEMP7=' CONVERT(CHAR(40),replace(replace(Usermessagereference,'+''''+'.pri'+''''+','+''''+''''+'),'+''''+'-1'+''''+','+''''+''''+')) as '+''''+'Reference'+''''
SELECT @STRTEMP8=' from ofac..ReconTable where Upper(source) in (''SWIFT'',''WS_SWIFT'')' 
SELECT @STRTEMP9=' AND searchDate>=CONVERT(CHAR(10),GETDATE()-15,101)' + '+'''+' 00:00:00'+'''' + ' AND searchDate<=CONVERT(CHAR(10),GETDATE(),101)' + '+'''+' 00:00:00'+''''
SELECT @STRTEMP10=' AND CaseReference LIKE'+ ' '''+ '%' + rtrim(@strsysdate) + '%' + ''' order by SearchDate ' 
SELECT @TEMPSTR=RTRIM(@STRTEMP1)+RTRIM(@STRTEMP2)+RTRIM(@STRTEMP3)+RTRIM(@STRTEMP4)+RTRIM(@STRTEMP5)+RTRIM(@STRTEMP6)+RTRIM(@STRTEMP7)+RTRIM(@STRTEMP8)+RTRIM(@STRTEMP9)+RTRIM(@STRTEMP10)
SELECT @TEMPSTR,LEN(@TEMPSTR)
EXEC (@TEMPSTR)

SELECT @STRTEMP1='select CONVERT(CHAR(2),' +''''+ '1'+ ''''+') ,'+ 'CONVERT(CHAR(18),'+''''+'Total Messages  =' + '''' + '),'  + 'CONVERT(char(12),count(*))'
SELECT @STRTEMP2=' from ofac..ReconTable where Upper(source) in (''SWIFT'',''WS_SWIFT'')'
SELECT @STRTEMP3=' AND searchDate>=CONVERT(CHAR(10),GETDATE()-15,101)' + '+'''+' 00:00:00'+'''' + ' AND searchDate<=CONVERT(CHAR(10),GETDATE(),101)' + '+'''+' 00:00:00'+''''
SELECT @STRTEMP4=' AND CaseReference LIKE'+ ' '''+ '%' + rtrim(@strsysdate) + '%' + '''' 
SELECT @TEMPSTR=RTRIM(@STRTEMP1)+RTRIM(@STRTEMP2)+RTRIM(@STRTEMP3)+RTRIM(@STRTEMP4)
SELECT @TEMPSTR,LEN(@TEMPSTR)
EXEC (@TEMPSTR)

---select CONVERT(CHAR(2),'1') ,
---CONVERT(CHAR(18),'Total Messages  ='), 
---convert(char(12),count(*)) from ofac..ReconTable where upper(source)='SWIFT'
---and searchDate>='2014-06-18 00:00:00' and searchDate<='2014-06-19 00:00:00' 
---AND searchDate>=CONVERT(CHAR(10),GETDATE()-1,101) + ' 00:00:00' 
---AND searchDate<=CONVERT(CHAR(10),GETDATE(),101) + ' 00:00:00'

SELECT @STRTEMP1='select CONVERT(CHAR(2),' +''''+ '1'+ ''''+') ,'+ 'CONVERT(CHAR(18),'+''''+ 'Violations  =' + '''' + '),'  + 'CONVERT(char(12),count(*))'
SELECT @STRTEMP2=' from ofac..ReconTable where Upper(source) in (''SWIFT'',''WS_SWIFT'')'
SELECT @STRTEMP3=' AND searchDate>=CONVERT(CHAR(10),GETDATE()-15,101)' + '+'''+' 00:00:00'+'''' + ' AND searchDate<=CONVERT(CHAR(10),GETDATE(),101)' + '+'''+' 00:00:00'+''''
SELECT @STRTEMP4=' AND CaseReference LIKE'+ ' '''+ '%' + rtrim(@strsysdate) + '%' + '''' 
SELECT @STRTEMP5=' AND sancpartycount>0'
SELECT @TEMPSTR=RTRIM(@STRTEMP1)+RTRIM(@STRTEMP2)+RTRIM(@STRTEMP3)+RTRIM(@STRTEMP4)+RTRIM(@STRTEMP5)
SELECT @TEMPSTR,LEN(@TEMPSTR)
EXEC (@TEMPSTR)

---select CONVERT(CHAR(2),'1') ,
---CONVERT(CHAR(18),'Violations  =' ), 
---convert(char(12),count(*)) from ofac..ReconTable where upper(source)='SWIFT'
---and searchDate>='2014-06-18 00:00:00' and searchDate<='2014-06-19 00:00:00' and sancpartycount>0
---AND searchDate>=CONVERT(CHAR(10),GETDATE()-1,101) + ' 00:00:00' 
---AND searchDate<=CONVERT(CHAR(10),GETDATE(),101) + ' 00:00:00'
---and sancpartycount>0


SELECT @STRTEMP1='select CONVERT(CHAR(2),' +''''+ '1'+ ''''+') ,'+ 'CONVERT(CHAR(18),'+''''+ 'Non-Violations =' + '''' + '),' + 'CONVERT(char(12),count(*))'
SELECT @STRTEMP2=' from ofac..ReconTable where Upper(source) in (''SWIFT'',''WS_SWIFT'')'
SELECT @STRTEMP3=' AND searchDate>=CONVERT(CHAR(10),GETDATE()-15,101)' + '+'''+' 00:00:00'+'''' + ' AND searchDate<=CONVERT(CHAR(10),GETDATE(),101)' + '+'''+' 00:00:00'+''''
SELECT @STRTEMP4=' AND CaseReference LIKE'+ ' '''+ '%' + rtrim(@strsysdate) + '%' + '''' 
SELECT @STRTEMP5=' AND sancpartycount=0'
SELECT @TEMPSTR=RTRIM(@STRTEMP1)+RTRIM(@STRTEMP2)+RTRIM(@STRTEMP3)+RTRIM(@STRTEMP4)+RTRIM(@STRTEMP5)
SELECT @TEMPSTR,LEN(@TEMPSTR)
EXEC (@TEMPSTR)


---select CONVERT(CHAR(2),'1') ,
---CONVERT(CHAR(18),'Non-Violations ='), 
---convert(char(12),count(*)) from ofac..ReconTable where upper(source)='SWIFT'
--and searchDate>='2014-06-18 00:00:00' and searchDate<='2014-06-19 00:00:00' and sancpartycount=0
---AND searchDate>=CONVERT(CHAR(10),GETDATE()-1,101) + ' 00:00:00' 
---AND searchDate<=CONVERT(CHAR(10),GETDATE(),101) + ' 00:00:00'
---and sancpartycount=0