USE [OFAC]
GO

/****** Object:  Table [dbo].[SwiftTag]    Script Date: 4/21/2021 5:18:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SwiftTag](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MT] [varchar](10) NULL,
	[Tag] [varchar](10) NOT NULL,
	[FieldName] [varchar](100) NULL,
	[Condition] [varchar](100) NULL,
	[Format] [varchar](100) NULL,
	[Remark] [varchar](2000) NULL,
	[Enable] [bit] NOT NULL,
	[LastModify] [datetime] NOT NULL
) ON [PRIMARY]
GO


