USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[CRY_SACaseReport_ActivityDetail]    Script Date: 10/27/2020 2:59:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE Procedure [dbo].[CRY_SACaseReport_ActivityDetail] ( @caseNoAtv SeqNo1)
 
  
  As

declare	@intBook int, 
        @bookDate Datetime,
	    @startDate Datetime,
        @endDate Datetime,
        @intStartDate int,
        @intEndDate int

    select act.TranNo, convert(date,convert(char(10),act.BookDate),111) BookDate,
    case when act.RecvPay = 1 then 'Receive' when act.RecvPay = 2 then 'Pay' end RecvPay ,
    act.Curr,act.BaseAmt TotAmt, act.Ref,act.Type, act.BeneCustId
    from pbsa..SuspiciousActivity sus
    join pbsa..SAsActivity sas on sus.RecNo = sas.ObjectID
    join pbsa..Activityhist act on act.TranNo = sas.TranNo
    where sus.RecNo = @caseNoAtv
    and sas.Included = 1
    Order by act.BookDate


	
GO


