<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UpdateSelectedCases.aspx.vb" Inherits="CMBrowser.UpdateSelectedCases" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Update selected cases</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="litMeta" runat="server"></asp:literal><LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="js/nav.js"></script>
		<script language="javascript" FOR="window" EVENT="onload">	
			 AddWindowToPCSWindowCookie(window.name);		
			 var checkedrows = getcheckedrows();
			 document.getElementById("hdfldSelectedCases").value = checkedrows;		
		</script>
		<style type="text/css">.BoldRed { FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: red }
	.BoldBlack { FONT-WEIGHT: bold; FONT-SIZE: small; COLOR: black }
	.NormalRed { FONT-WEIGHT: normal; FONT-SIZE: x-small; COLOR: red }
	.NormalBlack { FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: black }
	.BoldBlue { FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: blue }
		</style>
	</HEAD>
	<body class="formbody" MS_POSITIONING="FlowLayout" onUnload="RemoveWindowFromPCSWindowCookie(window.name);">
		<form id="Form1" method="post" runat="server">
			<table id="PageLayout" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td><asp:label id="clblRecordMessage" runat="server" width="808px" cssClass="RecordMessage">Record Message</asp:label></td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" Height="24px" Font-Size="X-Small" Width="616px"></asp:validationsummary></td>
			    </tr>
				<tr>
					<td>
						<table id="Table2" style="WIDTH: 816px; HEIGHT: 40px" cellspacing="0" cellpadding="0" width="816"
							border="0">
							<tr align="center">
								<td align="center"><asp:button id="cmdUpdateCases" runat="server" Width="166px" Text="Waive Selected Cases"></asp:button>&nbsp;&nbsp;&nbsp;
									<asp:button id="cmdClose" runat="server" Width="99px" Text="Close"></asp:button></td>
								<td align="left"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordHeader" runat="server" width="804px" CSSClass="paneltitle"></asp:label></td>
				</tr>
			</table>
			<asp:panel id="Panel1" runat="server" Height="176px" Width="744px" BorderStyle="None" BorderWidth="1px"
				Cssclass="Panel">&nbsp; 
                    <table id="Table1" cellspacing="0" cellpadding="0" width="300" border="0">
					    <tr>
						    <td valign="top">
							    <asp:Label id="lblReason" runat="server" CssClass="Label">Enter reason for waiving:</asp:Label></td>
						    <td>
							    <asp:TextBox id="txtReason" runat="server" Width="535px" Height="272px" 
							    MaxLength="500" TextMode="MultiLine"></asp:TextBox></td>
					    </tr>
				</table><input id="hdfldSelectedCases" type="hidden" name="hdfldSelectedCases" runat="server" />
			</asp:panel><asp:panel id="Panel2" runat="server" Height="15px" Width="712px" cssclass="Panel"></asp:panel><asp:panel id="cPanelValidators" runat="server" Height="46px" Width="864px">
				<uc1:ucrecordscrollbar id="RecordScrollBar" runat="server"></uc1:ucrecordscrollbar>
				<asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder>
			</asp:panel></form>
	</body>
</HTML>
