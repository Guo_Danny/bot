use PBSA
GO

declare @para_h varchar(500), @para_t varchar(5000), @para varchar(5000), @para_new varchar(5000);

select @para = Params from Watchlist where WLCode in ('idailytrn')

set @para_new = replace(REPLACE(@para,'minimum Amount', 'Minimum individual transaction amount'),'maximum Amount','Maximum individual transaction amount. For no maximum use -1')

update Watchlist set Params = @para_new, LastOper = 'primeadmin' where WLCode = 'idailytrn'

select @para = Params from Watchlist where WLCode in ('odailytrn')

set @para_new = replace(REPLACE(@para,'minimum Amount', 'Minimum individual transaction amount'),'maximum Amount','Maximum individual transaction amount. For no maximum use -1')

update Watchlist set Params = @para_new, LastOper = 'primeadmin' where WLCode = 'odailytrn'

