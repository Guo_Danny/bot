USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_ODailyTrn]    Script Date: 4/9/2021 6:15:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[USR_ODailyTrn] ( 
	@WLCode 	SCode,
	@TestAlert 	INT,
	@activityTypeList VARCHAR(8000), 
	@minimumAmount 	MONEY, 
	@maximumAmount 	MONEY,
	@RiskClassList 	VARCHAR(8000),
	@BranchList 	VARCHAR(8000),
	@deptList 	VARCHAR(8000),
	@CustTypeList   VARCHAR(8000)

)
AS
	/* RULE AND PARAMETER DESCRIPTION
	   Detects Customers with type between specified amount range
	   Designed to be ran Pre-EOD.
		@activityTypeList	List of Activity Types that will be considered. 
				   -ALL-, blank or NULL for all Types
		@minimumAmount	- Minimum amount
		@maximumAmount	- Maximum amount
		@RiskClassList	- List of Risk Classes that will be considered. 
				   -ALL-, blank or NULL for all Risk Classes
		@BranchList	- List of Branches that will be considered. 
				   -ALL-, blank or NULL for all Branches
		@deptList	- List of Departments that will be considered. 
				   -ALL-, blank or NULL for all Departments
		@CustTypeList - List of Customer Types that will be considered. 
				   -ALL-, blank or NULL for all Customer Types

	*/
		
	/*  Declarations */
	DECLARE	@description 	VARCHAR(2000),
		@desc 		VARCHAR(2000),
		@Id 		INT, 
		@WLType 	INT,
		@stat 		INT,
		@trnCnt 	INT
	DECLARE @STARTALRTDATE  DATETIME
	DECLARE @ENDALRTDATE    DATETIME
	
	DECLARE	@TT TABLE (
		Cust 	VARCHAR(40),
		Recvpay 	INT,
		TotalAmt 	MONEY,
		BOOKDATE 	INT
	)

	DECLARE	@CTT TABLE (
		Cust 	VARCHAR(40),
		TotalRecAmt	MONEY,
		TotalPayAmt	MONEY,
		TotalAmt 	MONEY,
		BOOKDATE 	INT
	)
	-- Temporary table of Activity Types that have not been specIFied as Exempt
	DECLARE @ActType TABLE (
		Type	INT
	)
	
	SET NOCOUNT ON
	SET @stat = 0

	--20210107 add
	If @maximumAmount = -1
	Begin
		set @maximumAmount = 99999999999999;
	End	
	
	--- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */
	SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN USR_ODailyTrn
	ELSE
		-- Already in a transaction
		SAVE TRAN USR_ODailyTrn
	/* End standard stored procedure transaction header */
	
	/*  standard Rules Header */
	SELECT @description = [Desc], @WLType = WLType  
	FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
	SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)
    SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)

	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	INSERT INTO @TT(Cust, RecvPay, TotalAmt, BookDate)
		SELECT 	Cust, RecvPay, SUM(baseAmt), bookdate
		FROM 	activity (NOLOCK)
		INNER	JOIN Customer ON activity.cust = customer.id 
		INNER	JOIN @ActType Act ON Act.Type = Activity.Type
		WHERE 	((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) AND
			    ((ISNULL(@BranchList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.OwnerBranch)) + ',', @BranchList ) > 0)) AND
			    ((ISNULL(@DeptList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.OwnerDept)) + ',', @DeptList ) > 0))   AND	
				((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) 
        AND ISNUMERIC(Cust)>0
		and baseAmt >= @minimumAmount and baseAmt <= @maximumAmount
		GROUP 	BY Cust, bookdate, RecvPay
	
	Insert into @CTT(cust, TotalAmt, TotalRecAmt, TotalPayAmt, BOOKDATE)
	select cust, sum(TotalAmt), sum(recamt), sum(payamt), BOOKDATE from (
	select cust, TotalAmt, 
	case when Recvpay = 1 then TotalAmt else 0 end recamt,
	case when Recvpay = 2 then TotalAmt else 0 end payamt, 
	BOOKDATE
	from @TT) T group by cust, BOOKDATE
	select * from @CTT

	--1 means test, 0 means no
	IF @testAlert = 1 
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT, IsTest) 
			SELECT @WLCode, case when len(cust)>0 Then 'Customer: ''' + Cust + ''' had ''$' 
				+ CONVERT(VARCHAR, TotalRecAmt) + ''' of incoming types specified, and ''$' 
				+ CONVERT(VARCHAR, TotalPayAmt) + ''' of outgoing types specified '  +
				'on ' + CONVERT(VARCHAR, bookdate) end, 0, 
				GETDATE(), NULL, NULL, Cust, NULL, 1 
		  	FROM 	@CTT 
		SELECT @STAT = @@ERROR	
		SELECT @ENDALRTDATE = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
		
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	'Alert', AlertNO, TRANNO 
			FROM 	Activity a
			INNER	JOIN @CTT T ON a.Cust = t.Cust 
			AND 	a.bookdate = t.bookdate 
			INNER	JOIN Alert ON A.Cust = Alert.Cust
			INNER	JOIN customer ON a.cust = customer.id
			INNER	JOIN @ActType Act ON Act.Type = A.Type
			WHERE	((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) AND
			    ((ISNULL(@BranchList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.OwnerBranch)) + ',', @BranchList ) > 0)) AND
			    ((ISNULL(@DeptList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.OwnerDept)) + ',', @DeptList ) > 0)) AND	
				((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) AND
				Alert.WLCode = @WLCode AND
				baseAmt >= @minimumAmount and baseAmt <= @maximumAmount and
				Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		
		SELECT @STAT = @@ERROR 
		IF @STAT <> 0  GOTO ENDOFPROC
	END 
	ELSE 
	BEGIN 
		IF @WLTYPE = 0 
		BEGIN
			SELECT @STARTALRTDATE = GETDATE()
			INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT) 
		 		SELECT @WLCode, case when len(cust)>0 Then 'Customer: ''' + Cust + ''' had ''$' 
				+ CONVERT(VARCHAR, TotalRecAmt) + ''' of incoming types specified, and ''$' 
				+ CONVERT(VARCHAR, TotalPayAmt) + ''' of outgoing types specified '  +
				'on ' + CONVERT(VARCHAR, bookdate) end, 0, 
				GETDATE(), NULL, NULL, Cust, NULL 
		  	FROM @CTT 
			SELECT @STAT = @@ERROR
			SELECT @ENDALRTDATE = GETDATE()
			IF @STAT <> 0  GOTO ENDOFPROC
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
			SELECT @STARTALRTDATE = GETDATE()
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
				ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
				RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
				ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
				EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
				OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
				REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
				WLCode, WLDESC, CREATETIME )
			SELECT	NULL, BookDate, Cust, NULL,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, case when len(cust)>0 Then 'Customer: ''' + Cust + ''' had ''$' 
				+ CONVERT(VARCHAR, TotalRecAmt) + ''' of incoming types specified, and ''$' 
				+ CONVERT(VARCHAR, TotalPayAmt) + ''' of outgoing types specified '  +
				'on ' + CONVERT(VARCHAR, bookdate) end, GETDATE() 
			FROM 	@CTT 
			SELECT @STAT = @@ERROR	
			SELECT @ENDALRTDATE = GETDATE()
			IF @STAT <> 0  GOTO ENDOFPROC
		END
		
		IF @WLTYPE = 0 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   		SELECT 	'Alert', AlertNO, TRANNO 
				FROM 	Activity A
				INNER	JOIN @CTT T ON a.Cust = t.Cust 
				AND 	a.bookdate = t.bookdate 
				INNER	JOIN Alert ON a.Cust = Alert.Cust
				INNER	JOIN customer ON a.cust = customer.id
				INNER	JOIN @ActType Act ON Act.Type = A.Type				
				WHERE	((ISNULL(@RiskClassList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) AND
				    ((ISNULL(@BranchList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(Customer.OwnerBranch)) + ',', @BranchList ) > 0)) AND
				    ((ISNULL(@DeptList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(Customer.OwnerDept)) + ',', @DeptList ) > 0)) AND	
			    	((ISNULL(@CustTypeList, '') = '' OR
				    CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) AND
					Alert.WLCode = @WLCode AND
					baseAmt >= @minimumAmount and baseAmt <= @maximumAmount and
					Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		
		SELECT @STAT = @@ERROR 
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   	SELECT 	'SUSPACT', RECNO, TRANNO 
			FROM 	Activity A 
			INNER	JOIN @CTT T ON A.Cust = T.Cust AND A.BookDate = T.BookDate 
			INNER	JOIN SUSPICIOUSACTIVITY S ON A.Cust = S.Cust
			INNER	JOIN Customer ON A.Cust= Customer.ID
			INNER	JOIN @ActType Act ON Act.Type = A.Type
			WHERE	((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) AND
				((ISNULL(@BranchList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.OwnerBranch)) + ',', @BranchList ) > 0)) AND
				((ISNULL(@DeptList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.OwnerDept)) + ',', @DeptList ) > 0))  AND	
				((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) AND
				S.WLCode = @WLCode AND
				baseAmt >= @minimumAmount and baseAmt <= @maximumAmount and
				S.CREATETIME BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		SELECT @STAT = @@ERROR 
		END
	
	END 
	
	EndOfProc:
	IF (@stat <> 0) BEGIN 
	  ROLLBACK TRAN USR_ODailyTrn
	  RETURN @stat
	END	
	
	IF @trnCnt = 0
	  COMMIT TRAN USR_ODailyTrn
	RETURN @stat
	
GO


