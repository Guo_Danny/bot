<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
	<meta content="JavaScript" name="vs_defaultClientScript">
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	<link href="https://10.7.16.165/primecompliancesuite/Styles.css" type="text/css" rel="stylesheet">
	<script language="JavaScript" src="../Compliance Suite Error_files/nav.js.下載"></script>
	<link href="Styles.css" type="text/css" rel="stylesheet"/>
    <title>Cash Transactions</title>
	<script type="text/javascript"  language="javascript">
	
	function OpenActivityDetail(seqno) {
		var qstring;
		qstring = "https://10.7.16.165/primecompliancesuite/Activity.aspx?RecordID=" + seqno + "&ActionID=2&ViewID=94&QPM=0&RowNum=0&ViewName=vwListByCase&ArchiveMode=0&RecordsTotal=15&CheckCount=15&IsRecChecked=1";
		
		window.open(qstring, '', 'scrollbars=yes,resizable=yes,width=880,height=600'); 
		}
		
	function ShowDetail(div_id) {
		var div_id_all = document.getElementById('div_detail').value;
		
		var ary = div_id_all.split(',')
		for(i=0;i<=ary.length-1;i++)
		{
			if(ary[i].length > 0)
			{
				document.getElementById('divDetail_' + ary[i]).style.display="none";
			}
		}
		document.getElementById(div_id).style.display="block";
		}
	</script>
  </head>
  <BODY bgcolor="#CCCCE5">
    <form name="Form1" id="Form1">
	<div>
      
		
    </div>
	<table width="100%">
	<input type="image" name="imgPrime1" id="imgPrime" src="https://10.7.16.165/primecompliancesuite/images/compliance-manager.gif">
	    <br/>
		<hr />
		<table>
			<tr>
				<td class="Sectionheading" style="width: 100%">Activity List</td>
			</tr>
			<tr>
				<td class="Sectionheading"><%Response.write("<br/>Ref ID: "&request.querystring("id")&"")%></td>
			</tr>
			<tr>
				<td class="Sectionheading">
<%
set cnn2 = Server.CreateObject("ADODB.Connection") 
cnn2.Open "driver={ODBC Driver 13 for SQL Server};database=PBSA;server=10.9.1.132;uid=primeuser;pwd=Passw0rd"
Response.write("<br/><br/>")
set ID = request.querystring("id")
set sdate = request.querystring("sdate")
set edate = request.querystring("edate")

sqlGroup = "select substring(convert(char(8),BookDate),1,6) 'BookDate',"
sqlGroup = sqlGroup + "sum(case when RecvPay = 1 then 1 when RecvPay = 2 then 0 end) 'In Count',"
sqlGroup = sqlGroup + "ROUND(sum(case when RecvPay = 1 then BaseAmt when RecvPay = 2 then 0 end),2) 'In Total ($)',"
sqlGroup = sqlGroup + "sum(case when RecvPay = 1 then 0 when RecvPay = 2 then 1 end) 'Out Count',"
sqlGroup = sqlGroup + "ROUND(sum(case when RecvPay = 1 then 0 when RecvPay = 2 then BaseAmt end),2) 'Out Total ($)' "
sqlGroup = sqlGroup + "from CurrentArchActivity "
sqlGroup = sqlGroup + "where Cust = '"& ID &"' "
sqlGroup = sqlGroup + "and CashTran = 1 "
sqlGroup = sqlGroup + "and bookdate >= convert(varchar(8),convert(date,'" & sdate & "'),112) and bookdate <= convert(varchar(8),convert(date,'" & edate & "'),112) "
sqlGroup = sqlGroup + "group by substring(convert(char(8),BookDate),1,6) "

sqlSum = "select 'Total',sum(InCount),sum(InTotal$), sum(OutCount),sum(OutTotal$) from ( "
sqlSum = sqlSum + "select substring(convert(char(8),BookDate),1,6) 'bookdate', "
sqlSum = sqlSum + "sum(case when RecvPay = 1 then 1 when RecvPay = 2 then 0 end) 'InCount', "
sqlSum = sqlSum + "sum(case when RecvPay = 1 then BaseAmt when RecvPay = 2 then 0 end) 'InTotal$', "
sqlSum = sqlSum + "sum(case when RecvPay = 1 then 0 when RecvPay = 2 then 1 end) 'OutCount', "
sqlSum = sqlSum + "sum(case when RecvPay = 1 then 0 when RecvPay = 2 then BaseAmt end) 'OutTotal$' "
sqlSum = sqlSum + "from CurrentArchActivity "
sqlSum = sqlSum + "where Cust = '"& ID &"' "
sqlSum = sqlSum + "and CashTran = 1 "
sqlSum = sqlSum + "and bookdate >= convert(varchar(8),convert(date,'" & sdate & "'),112) and bookdate <= convert(varchar(8),convert(date,'" & edate & "'),112) "
sqlSum = sqlSum + "group by substring(convert(char(8),BookDate),1,6) "
sqlSum = sqlSum + ") X"

sqlAvg = "select 'Average', '',ROUND(AVG(InTotal$),2),'', Round(AVG(OutTotal$),2) from ( "
sqlAvg = sqlAvg + "select substring(convert(char(8),BookDate),1,6) 'bookdate', "
sqlAvg = sqlAvg + "sum(case when RecvPay = 1 then 1 when RecvPay = 2 then 0 end) 'InCount', "
sqlAvg = sqlAvg + "sum(case when RecvPay = 1 then BaseAmt when RecvPay = 2 then 0 end) 'InTotal$', "
sqlAvg = sqlAvg + "sum(case when RecvPay = 1 then 0 when RecvPay = 2 then 1 end) 'OutCount', "
sqlAvg = sqlAvg + "sum(case when RecvPay = 1 then 0 when RecvPay = 2 then BaseAmt end) 'OutTotal$' "
sqlAvg = sqlAvg + "from CurrentArchActivity "
sqlAvg = sqlAvg + "where Cust = '"& ID &"' "
sqlAvg = sqlAvg + "and CashTran = 1 "
sqlAvg = sqlAvg + "and bookdate >= convert(varchar(8),convert(date,'" & sdate & "'),112) and bookdate <= convert(varchar(8),convert(date,'" & edate & "'),112) "
sqlAvg = sqlAvg + "group by substring(convert(char(8),BookDate),1,6) "
sqlAvg = sqlAvg + ") X"

sqlMax = "select 'Maximum', '', Max(InTotal$), '', Max(OutTotal$) from ( "
sqlMax = sqlMax + "select substring(convert(char(8),BookDate),1,6) 'bookdate', "
sqlMax = sqlMax + "sum(case when RecvPay = 1 then 1 when RecvPay = 2 then 0 end) 'InCount', "
sqlMax = sqlMax + "sum(case when RecvPay = 1 then BaseAmt when RecvPay = 2 then 0 end) 'InTotal$', "
sqlMax = sqlMax + "sum(case when RecvPay = 1 then 0 when RecvPay = 2 then 1 end) 'OutCount', "
sqlMax = sqlMax + "sum(case when RecvPay = 1 then 0 when RecvPay = 2 then BaseAmt end) 'OutTotal$' "
sqlMax = sqlMax + "from CurrentArchActivity "
sqlMax = sqlMax + "where Cust = '"& ID &"' "
sqlMax = sqlMax + "and CashTran = 1 "
sqlMax = sqlMax + "and bookdate >= convert(varchar(8),convert(date,'" & sdate & "'),112) and bookdate <= convert(varchar(8),convert(date,'" & edate & "'),112) "
sqlMax = sqlMax + "group by substring(convert(char(8),BookDate),1,6) "
sqlMax = sqlMax + ") X"

<!--Response.write("Record Counts: "&rs.recordcount&"<br/><br/>")-->
%>
				</td>
			</tr>
		</table>
	</table>
		<table cellspacing="0" cellpadding="0" rules="all" border="1" style="width:800px;border-collapse:collapse;" >
		<tr>
<%
set rsGroup= Server.CreateObject("ADODB.RecordSet")
rsGroup.Open sqlGroup,cnn2 ,1,1
string_table = ""
string_divall = ""

if rsGroup.recordcount > 0 then
	for i = 0 to rsGroup.Fields.Count - 1
	Response.write("<td style=""color: white;background-color: #990000;text-align: center;FONT-SIZE: 14px;height:20px;font-weight:bold;"">"&rsGroup(i).Name&"</td>")
	next
	Response.write("</tr>")

	rsGroup.MoveFirst
	While Not rsGroup.EOF
	Response.write("<tr>")
	For i=0 to rsGroup.Fields.Count - 1
		if i = 0 then
			Response.write("<td class=""viewgridcell"" cellspacing=""0"" height=""25px""><a href=""javascript:ShowDetail('divDetail_"& rsGroup(i) &"');"">"&rsGroup(i)&"</a></td>")
	
	string_divall = string_divall + rsGroup(i) + ","
	string_table = string_table + "<div id=""divDetail_"& rsGroup(i) &""" style=""display:none;"">"
	string_table = string_table + "<table cellspacing=""0"" cellpadding=""0"" rules=""all"" border=""1"" style=""width:800px;border-collapse:collapse;"" >"
	string_table = string_table + "<tr>"
	
sqlDetail = "select TranNo,Cust,Account,Type,case when CashTran = 0 then 'No' when CashTran = 1 then 'Yes' end 'Cash',BaseAmt,case when RecvPay = 1 then 'Receive' when RecvPay = 2 then 'Pay' end 'Recv/Pay', FxAmt,BookDate from CurrentArchActivity where CashTran = 1 and cust = '" & ID & "' and bookdate >= '"&rsGroup(i)&"' * 100 + 1 and bookdate < ('"&rsGroup(i)&"' + 1) * 100 + 1"
set rs= Server.CreateObject("ADODB.RecordSet")
rs.Open sqlDetail,cnn2 ,1,1

if rs.recordcount > 0 then
	for j = 0 to rs.Fields.Count - 1
	string_table = string_table + "<td style=""color: white;background-color: #990000;text-align: center;FONT-SIZE: 14px;height:20px;font-weight:bold;"">"&rs(j).Name&"</td>"
	next
	string_table = string_table + "</tr>"

	rs.MoveFirst
	While Not rs.EOF
	string_table = string_table + "<tr>"
	For j=0 to rs.Fields.Count - 1
		if j = 0 then
			string_table = string_table + "<td class=""viewgridcell"" cellspacing=""0"" height=""25px""><a href=""javascript:OpenActivityDetail('"&rs(j)&"');"">"&rs(j)&"</a></td>"
		else
			string_table = string_table + "<td class=""viewgridcell"" bgcolor=""white"" cellspacing=""0"" height=""25px"" align=""right"">"&rs(j)&"</td>"
		end if
	Next
	string_table = string_table + "</tr>"
	rs.MoveNext
	Wend
	rs.close
end if
	string_table = string_table + "</table>"
	string_table = string_table + "</div>"
			
		else
			Response.write("<td class=""viewgridcell"" bgcolor=""white"" cellspacing=""0"" height=""25px"" align=""right"">"&rsGroup(i)&"</td>")
		end if
	
	

	
	Next
	Response.write("</tr>")
	rsGroup.MoveNext
	Wend
	rsGroup.close
end if

set rsSum= Server.CreateObject("ADODB.RecordSet")
rsSum.Open sqlSum,cnn2 ,1,1

str_sum = ""
str_sum_tmp = ""
if rsSum.recordcount > 0 then
	rsSum.MoveFirst
	While Not rsSum.EOF
	str_sum = str_sum + "<tr>"
	For i=0 to rsSum.Fields.Count - 1
		str_sum = str_sum + "<td class=""viewgridcell"" bgcolor=""white"" cellspacing=""0"" height=""25px"" align=""right""><I><b>"&rsSum(i)&"</b></I></td>"
		if Len(rsSum(2)) > 0 then
			str_sum_tmp = Len(rsSum(2))
		end if
	Next

	rsSum.MoveNext
	Wend
	str_sum = str_sum + "</tr>"
	
	if str_sum_tmp <>  "" then
		Response.write(str_sum)
	end if
	rsSum.close
end if

set rsAvg= Server.CreateObject("ADODB.RecordSet")
rsAvg.Open sqlAvg,cnn2 ,1,1

str_avg = ""
str_avg_tmp = ""
if rsAvg.recordcount > 0 then
	rsAvg.MoveFirst
	While Not rsAvg.EOF
	str_avg = str_avg + "<tr>"
	For i=0 to rsAvg.Fields.Count - 1
		str_avg = str_avg + "<td class=""viewgridcell"" bgcolor=""white"" cellspacing=""0"" height=""25px"" align=""right""><I><b>"&rsAvg(i)&"</b></I></td>"
		if Len(rsAvg(2)) > 0 then
			str_avg_tmp = Len(rsAvg(2))
		end if
	Next
	rsAvg.MoveNext
	Wend
	str_avg = str_avg + "</tr>"
	
	if str_avg_tmp <>  "" then
		Response.write(str_avg)
	end if
	rsAvg.close
end if

set rsMax= Server.CreateObject("ADODB.RecordSet")
rsMax.Open sqlMax,cnn2 ,1,1

str_Max = ""
str_Max_tmp = ""
if rsMax.recordcount > 0 then
	rsMax.MoveFirst
	While Not rsMax.EOF
	str_Max = str_Max + "<tr>"
	For i=0 to rsMax.Fields.Count - 1
		str_Max = str_Max + "<td class=""viewgridcell"" bgcolor=""white"" cellspacing=""0"" height=""25px"" align=""right""><I><b>"&rsMax(i)&"</b></I></td>"
		if Len(rsMax(2)) > 0 then
			str_Max_tmp = Len(rsMax(2))
		end if
	Next
	
	rsMax.MoveNext
	Wend
	str_Max = str_Max + "</tr>"
	if str_Max_tmp <>  "" then
		Response.write(str_Max)
	end if
	rsMax.close
end if

%>
	</table>
	<br/>
	<br/>
<% 
Response.write(string_table)
Response.write("<input id=""div_detail"" style=""visibility:hidden"" value="""& string_divall &"""/>")
%>
  </form>
  </BODY> 
</HTML> 