USE [PBSA]
GO

--Acceptance Models
--CDD Models

INSERT INTO [dbo].[CheckListType]([Code], [Name], [Description], [ReviewPeriod], [RPAbsolute], [CDD], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES('KycEntity', 'KYC-Entity', 'Customer type is Entity', 0, 1, 1, 'PRIMEADMIN', getdate(), 'PRIMEADMIN', NULL)

--須確認是否存在
--INSERT INTO [dbo].[CheckListType]([Code], [Name], [Description], [ReviewPeriod], [RPAbsolute], [CDD], [CreateOper], [CreateDate], [LastOper], [LastModify]) VALUES('KYCIndiv', 'KYC-Individual', 'KYC Model for Individual', 0, 1, 1, 'Prime', '20180520 20:32:56.103', 'PRIMEADMIN', getdate())
