

--cdd user define 1= type products
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('TypeProd', 'Type of Products',1,getdate(),'Prime',getdate(),'Prime')

insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('TypeProd','CDD/EDD',getdate(),'Prime',getdate(),'Prime')


--cdd user define 2.Traded Country
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('TxCountry', 'Traded Country',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('TxCountry','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 3.Transaction Channels
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('TxChannel', 'Trn. Channels',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('TxChannel','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 4.PEP Status
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('PEPStat', 'PEP Status',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('PEPStat','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 5.Nationality of Country
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('National', 'Nationality of Country',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('National','CDD/EDD',getdate(),'Prime',getdate(),'Prime')


--cdd user define 6.Country of Residence
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('Residence', 'Country of Residence',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('Residence','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 7.Mgr. Override
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('MgrOverride', 'Mgr. Override',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('MgrOverride','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 8.Reverification Channel
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('RevChannel', 'Reverification Channel',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('RevChannel','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 9.Sanctions Listing
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('SancLists', 'Sanctions Listing',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('SancLists','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 10.COMMENTS
--insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
--values ('Comments', 'COMMENTS',1,getdate(),'Prime',getdate(),'Prime')


--insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
--values ('Comments','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 11.Customer Type Ind
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('ICustType', 'Customer Type Individual',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('ICustType','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 12.Customer Type Ent
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('ECustType', 'Customer Type Entity',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('ECustType','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 13.Customer Status Ind
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('ICustStat', 'Customer Status Individual',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('ICustStat','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 14.Customer Status Ent
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('ECustStat', 'Customer Status Entity',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('ECustStat','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 15.Industry Ind
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('IIndustry', 'Industry Individual',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('IIndustry','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 16.Industry Ent
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('EIndustry', 'Industry Entity',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('EIndustry','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 17.Shareholder Listing
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('ESharehold', 'Shareholder Listing',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('ESharehold','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 18.Country of Birth (Country of Origin)
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('CounOfBirth', 'Country of Birth (Country of Origin)',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('CounOfBirth','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 19.Risk Rating Code
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('RiskRateCode', 'Risk Rating Code',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('RiskRateCode','CDD/EDD',getdate(),'Prime',getdate(),'Prime')


--cdd user define 20.Customer Checking List
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('CustCheckList', 'Customer Checking List',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('CustCheckList','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

--cdd user define 21.Risk Rating Code
insert into PCDB..ListType (code,Description,Enabled,CreateDate,CreateOper,LastModifDate,LastOper)
values ('RiskRateCodeOP', 'Risk Rating Code OP',1,getdate(),'Prime',getdate(),'Prime')


insert into pcdb..listtypeproduct (listtypecode,productcode,createdate,createoper,lastmodifdate,lastoper) 
values ('RiskRateCodeOP','CDD/EDD',getdate(),'Prime',getdate(),'Prime')

