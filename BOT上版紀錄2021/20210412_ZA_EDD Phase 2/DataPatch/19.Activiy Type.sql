USE PBSA
GO
/*
Activity Type and Map
*/
insert into ActivityType(type,name,RiskFactor,CreateOper,CreateDate)
values(2854,'0161 Q3000 Foreign currency deposit- dem',0,'ADMIN',getdate())

insert into ActivityMap(OrigActivity,PrimeCode,CreateOper,CreateDate)
values('0161Q3000',2854,'ADMIN',getdate())

insert into ActivityType(type,name,RiskFactor,CreateOper,CreateDate)
values(3064,'0161 Q1300',0,'ADMIN',getdate())

insert into ActivityMap(OrigActivity,PrimeCode,CreateOper,CreateDate)
values('0161Q1300',3064,'ADMIN',getdate())

insert into ActivityType(type,name,RiskFactor,CreateOper,CreateDate)
values(3074,'0161 Q2300',0,'ADMIN',getdate())

insert into ActivityMap(OrigActivity,PrimeCode,CreateOper,CreateDate)
values('0161Q2300',3074,'ADMIN',getdate())

insert into ActivityType(type,name,RiskFactor,CreateOper,CreateDate)
values(3084,'0161 Q4700',0,'ADMIN',getdate())

insert into ActivityMap(OrigActivity,PrimeCode,CreateOper,CreateDate)
values('0161Q4700',3084,'ADMIN',getdate())

insert into ActivityType(type,name,RiskFactor,CreateOper,CreateDate)
values(3094,'0161 Q1100',0,'ADMIN',getdate())

insert into ActivityMap(OrigActivity,PrimeCode,CreateOper,CreateDate)
values('0161Q1100',3094,'ADMIN',getdate())

insert into ActivityType(type,name,RiskFactor,CreateOper,CreateDate)
values(2604,'0161 Q7000',0,'ADMIN',getdate())

insert into ActivityMap(OrigActivity,PrimeCode,CreateOper,CreateDate)
values('0161Q7000',2604,'ADMIN',getdate())

insert into ActivityType(type,name,RiskFactor,CreateOper,CreateDate)
values(3104,'0161 Q7900',0,'ADMIN',getdate())

insert into ActivityMap(OrigActivity,PrimeCode,CreateOper,CreateDate)
values('0161Q7900',3104,'ADMIN',getdate())

/*
PaymentMethod
*/

insert into PaymentMethods(code,name,CreateOper,CreateDate) values('BC','Buy cash','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('SC','Sell cash','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('MT','Outward transfer-MT','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('DD','Outward transfer-DD','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('RC','RC','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('CB','CB','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('TT','Telegraphic transfer-TT','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('IO','Outward transfer-IO','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('CE','Export commission','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('IT','Inward transfer-TT','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('IM','Inward transfer-MT','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('ID','Inward transfer-DD','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('IG','Inward transfer-RTGS','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('OA','O/A','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('BT','Financial Transfer','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('CO','Cash order payment','PRIMEADMIN',getdate())
insert into PaymentMethods(code,name,CreateOper,CreateDate) values('TC','T/C clearing','PRIMEADMIN',getdate())
