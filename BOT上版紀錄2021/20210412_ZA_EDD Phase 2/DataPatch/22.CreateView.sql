--*RiskScore-MLCO
exec WCDB_SaveQueryPlusParameter @ObjectId=40,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where RiskFactorDataView.Cust IN (SELECT id FROM CUSTOMER WHERE KYCSTATUS=2) or RiskFactorDataView.Cust in (select customerid from RiskScoreHist where DateAccepted is null and riskscore is null and isnull(AcceptOper,'''')<>'''') ',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=40,@OperCode='PRIMEADMIN',@ViewName='*RiskScore-MLCO',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*OverrideRiskCust
exec WCDB_SaveQueryPlusParameter @ObjectId=1,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where id in (select objectid from event  where Event.ObjectType = ''Cust'' AND logtext like ''%Risk Class Overridden%'') AND OWNERBRANCH=''0161'' ',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=1,@OperCode='PRIMEADMIN',@ViewName='*OverrideRiskCust',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1


--*MgrOverCust
exec WCDB_SaveQueryPlusParameter @ObjectId=1,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where Customer.Id like ''%0161%'' and customer.id in (select customerid from kycdata where  IsPersonalRelationship=1) ',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=1,@OperCode='PRIMEADMIN',@ViewName='*MgrOverCust',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1


--*UploadCustRisk10D
exec WCDB_SaveQueryPlusParameter @ObjectId=1,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue='  where id in (select CustomerId from vwRiskScoreHist where DateAccepted >= convert(varchar(10),getdate()-10,111)) and kycstatus=2 ',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=1,@OperCode='PRIMEADMIN',@ViewName='*UploadCustRisk10D',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1


--*NeedReviewCust2m
exec WCDB_SaveQueryPlusParameter @ObjectId=1,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where isnull( ProbationStartDate,'''' )<>'''' and convert(date,convert(char(30),probationstartdate,101),101) >= convert(date,dateadd(month,+1, CONVERT(date,getdate(),101)),101) and convert(date,convert(char(30),probationstartdate,101),101) <=convert(date,dateadd(month,+2,CONVERT(date,getdate(),101)),101) and Customer.OwnerBranch = ''0161'' ',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=1,@OperCode='PRIMEADMIN',@ViewName='*NeedReviewCust2m',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1


--*ReviewRejScoreLog
exec WCDB_SaveQueryPlusParameter @ObjectId=24,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where Event.ObjectType = ''Cust'' AND evtdetail  like ''%Rejected by Approver 1%''  ',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=24,@OperCode='PRIMEADMIN',@ViewName='*ReviewRejScoreLog',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1


--*ReviewAppScoreLog
exec WCDB_SaveQueryPlusParameter @ObjectId=24,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where Event.ObjectType = ''Cust'' AND evtdetail  like ''%Approved by Approver 1%''  ',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=24,@OperCode='PRIMEADMIN',@ViewName='*ReviewAppScoreLog',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1

--*OverrideLog
exec WCDB_SaveQueryPlusParameter @ObjectId=24,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where Event.ObjectType = ''Cust'' AND logtext like ''%Risk Class Overridden%'' ',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=24,@OperCode='PRIMEADMIN',@ViewName='*OverrideLog',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1


--*DelRejScoreLog
exec WCDB_SaveQueryPlusParameter @ObjectId=24,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where Event.Type = ''Del'' and Event.ObjectType = ''RFData'' ',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=24,@OperCode='PRIMEADMIN',@ViewName='*DelRejScoreLog',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1


--*AcceptScoreLog
exec WCDB_SaveQueryPlusParameter @ObjectId=24,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where Event.ObjectType = ''Cust'' AND evtdetail  like ''%Reviewed Risk Score Acceptance done%'' ',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=24,@OperCode='PRIMEADMIN',@ViewName='*AcceptScoreLog',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1


--*RiskRatingHighCust
exec WCDB_SaveQueryPlusParameter @ObjectId=1,@ParameterId=0,@OperCode='PRIMEADMIN',@ParameterOperation=0,
@ParameterValue=' where Customer.Id like ''%0161%'' and customer.id in (select customerid from kycdata where isnull(cdduser25,'''')=''10'') ',@ParameterRangeValue=''


exec WCDB_SaveView @ObjectId=1,@OperCode='PRIMEADMIN',@ViewName='*RiskRatingHighCust',@QueryMode=2,@QueryTimeout=15,@ArchiveMode=0,@Public=1
