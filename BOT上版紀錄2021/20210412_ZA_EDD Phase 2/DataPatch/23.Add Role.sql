--Operator
exec Psec.dbo.PSEC_AddRole 'CDDOP161','CDD operator right for South Africa','CDD Operator role right ',1,0,'primeadmin'

exec Psec.dbo.PSEC_AddRoleRight 'CDDOP161','','','',1024,'REVBSACUST','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDOP161','','','',1024,'DSPEDDRFD','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDOP161','','','',1024,'GENEDDRFD','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDOP161','','','',1024,'ONBRDCUST','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDOP161','','','',1024,'DSPONBRD','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDOP161','','','',1024,'DSPBSACUST','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDOP161','','','',1024,'MODBSATBL','primeadmin',0

--exec Psec.dbo.PSEC_AddRoleMember 'CDDOP161','992509a','primeadmin',0,0
--exec Psec.dbo.PSEC_AddRoleMember 'CDDOP161','900395a','primeadmin',0,0
exec Psec.dbo.PSEC_AssignRoleRightsToMembers 'CDDOP161','primeadmin',''

--Manager
exec Psec.dbo.PSEC_AddRole 'CDDMGR161','CDD Manager right for South Africa','CDD Manager role right ',1,0,'primeadmin'

exec Psec.dbo.PSEC_AddRoleRight 'CDDMGR161','','','',1024,'RJCTEDDRFD','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMGR161','','','',1024,'CDD1STAPP','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMGR161','','','',1024,'DSPBSACUST','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMGR161','','','',1024,'DSPEDDRFD','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMGR161','','','',1024,'ONBRDCUST','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMGR161','','','',1024,'DSPONBRD','primeadmin',0

--exec Psec.dbo.PSEC_AddRoleMember 'CDDMGR161','061850a','primeadmin',0,0
--exec Psec.dbo.PSEC_AddRoleMember 'CDDMGR161','118282a','primeadmin',0,0
exec Psec.dbo.PSEC_AssignRoleRightsToMembers 'CDDMGR161','primeadmin',''

--MLCO
exec Psec.dbo.PSEC_AddRole 'CDDMLCO161','CDD MLCO right for South Africa','CDD MLCO role right ',1,0,'primeadmin'

exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'CDDOPERCHG','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'ACPTEDDRFD','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'MODEDDRFD','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'DSPBSACUST','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'MODEDDRF','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'REVEDDRFD','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'DSPEDDRFD','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'ONBRDCUST','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'DSPONBRD','primeadmin',0

--2021-05-26 add admin
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'ADDEDDCL','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'ADDEDDSEC','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'ADDEDDSECI','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'GENEDDCLD','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'GENEDDRFD','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'MODCUSTAT','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'MODEDDCL','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'MODEDDCLD','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'MODEDDIDRQ','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'MODEDDIDRS','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'MODEDDPROV','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'MODEDDSEC','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'MODEDDSECI','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'MODEDDSVC','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'MODPWDINFO','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'REVEDDCLAL','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'REVEDDCLD','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'REVEDDIDRQ','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'STRSTPIDVS','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'STRSTPPLOG','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'EDDCLD2nd','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'EDDCLDApp','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'EDDCLDComp','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'DSPORG','primeadmin',0
exec Psec.dbo.PSEC_AddRoleRight 'CDDMLCO161','','','',1024,'EXPLSTVW','primeadmin',0
--exec Psec.dbo.PSEC_AddRoleMember 'CDDMLCO161','993548a','primeadmin',0,0
--exec Psec.dbo.PSEC_AddRoleMember 'CDDMLCO161','994326a','primeadmin',0,0
--exec Psec.dbo.PSEC_AddRoleMember 'CDDMLCO161','993377a','primeadmin',0,0
exec Psec.dbo.PSEC_AssignRoleRightsToMembers 'CDDMLCO161','primeadmin',''




