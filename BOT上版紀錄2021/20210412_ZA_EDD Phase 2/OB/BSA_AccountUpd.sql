USE [PBSA]
GO

/****** Object:  Trigger [dbo].[BSA_AccountUpd]    Script Date: 4/13/2021 11:33:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-------------------------------------------------------------------------------

ALTER TRIGGER [dbo].[BSA_AccountUpd] on [dbo].[Account]   For Update As
	
	Declare @LastOper SCode
	Declare @AccExemptStatus varchar(255)

If @@rowcount > 0 begin

	select @LastOper = i.LastOper
	from Inserted i

	/* Profile Enable / Disable Changes - Start
			This code block has been added for Enable / Disable the Profile based on Customer / Account Exemption.
			1. If the Account is exempted, then disable the profile associated with the Account 
			2. If the Account is not exempted and the customer is exempted, then disable the profile associated with the Account for that Customer
			3. If the Account is not exempted and the customer is not exempted, then enable the profile associated with the Account
			*/
			
			--Update profiles for Account if the exemption status has changed

					-- Case 1 : If the Account is exempted, then disable the profile associated with the Account
					UPDATE P
					SET Enabled = 0,
					LastOper = @LastOper
					FROM Profile P INNER JOIN Inserted I ON P.Account = I.Id
					INNER JOIN Deleted D ON D.Id = I.Id
					INNER JOIN ExemptionType et WITH (NOLOCK) ON i.ExemptionStatus = et.Code
					WHERE SUBSTRING(et.ExemptEntity,2,1) = 1 and SUBSTRING(et.ExemptFrom,1,1) = 1
					AND P.Enabled <> 0 AND IsNull(d.ExemptionStatus,0) <> IsNull(i.ExemptionStatus,0)

					-- Case 2 : If the customer on the profile is exempted, 
					-- then disable the profile associated with the Customer and Account
					UPDATE P
					SET Enabled = 0,
					LastOper = @LastOper
					FROM Profile P INNER JOIN Inserted I ON P.Account = I.Id
					INNER JOIN Deleted D ON D.Id = I.Id
					inner join Customer c WITH (NOLOCK) on p.Cust = c.Id
					inner join ExemptionType et WITH (NOLOCK) on c.ExemptionStatus = et.Code
					where SUBSTRING(et.ExemptEntity,1,1) = 1 and SUBSTRING(et.ExemptFrom,1,1) = 1
					and P.Enabled <> 0 and IsNull(d.ExemptionStatus,0) <> IsNull(i.ExemptionStatus,0)

					-- Case 3 : If the Account is not exempted and the customer is not exempted, then enable the profile associated with the Account

					UPDATE P
					SET Enabled = 1,
					LastOper = @LastOper
					FROM Profile P INNER JOIN Inserted I ON P.Account = I.Id
					INNER JOIN Deleted D ON D.Id = I.Id
					INNER JOIN Customer c WITH (NOLOCK) on P.Cust = c.Id
					LEFT JOIN ExemptionType et WITH (NOLOCK) on c.ExemptionStatus = et.Code
					LEFT JOIN ExemptionType e WITH (NOLOCK) on I.ExemptionStatus = e.Code
					where (SUBSTRING(et.ExemptEntity,1,1) = 0 OR SUBSTRING(et.ExemptFrom,1,1) = 0 OR c.ExemptionStatus Is Null)
						AND   
						(SUBSTRING(e.ExemptEntity,2,1) = 0 OR SUBSTRING(e.ExemptFrom,1,1) = 0 OR i.ExemptionStatus Is Null)
					AND P.Enabled <> 1 AND IsNull(d.ExemptionStatus,0) <> IsNull(i.ExemptionStatus,0)

			-- Profile Enable / Disable Changes - End


  Update Account Set LastModify = getDate() 
	From Inserted I, Account d Where  i.Id = d.Id



if exists (select i.Id from Inserted i inner join  Deleted d 
			on d.id= i.id
			where IsNull(d.Closed,0) = 0 and IsNull(i.Closed,0) = 1)
  begin
	


	--For all related customers update Last Oper
	--to inforce Risk Scoring
	update Customer
	set LastOper = @LastOper
	where id in 
	(select aow.cust from AccountOwner aow 
	 inner join inserted i on aow.Account = i.id
	 inner join deleted d on i.id = d.id
	 where IsNull(d.Closed,0) = 0 and IsNull(i.Closed,0) = 1)

	--Record Customer Event on Closed Account	
	 Insert Event (TrnTime, Oper, Type, ObjectType, ObjectId , LogText) 
	 Select getDate(), i.LastOper, 'Ann', 'Cust',  aow.Cust, 
	 'Closed account ' + i.id + ' with relation as ' + aow.relationship 
	 from AccountOwner aow 
	 inner join inserted i on aow.Account = i.id
	 inner join deleted d on i.id = d.id
	 where IsNull(d.Closed,0) = 0 and IsNull(i.Closed,0) = 1

	If dbo.BSA_fnGetEDDOption('CCAAC') = '1'
	   begin

		
			Declare @ClosedReason	ObjectId
			Declare @ClosedDate		GenericDate


			--Close Customers when last related account is closed
			select @ClosedReason = dbo.BSA_fnGetEDDOption('CRAAC')

			set @ClosedDate = dbo.ConvertSqlDateToInt(GETDATE())

			update Customer
			set LastOper = @LastOper,
				Closed = 1,
				ClosedDate = @ClosedDate,
				ClosedReason= @ClosedReason
			where id in 
			(select aow.cust from AccountOwner aow 
				inner join inserted i on aow.Account = i.id
				inner join deleted d on i.id = d.id
				where IsNull(d.Closed,0) = 0 and IsNull(i.Closed,0) = 1 and 
				not exists (select a.id from Account a 
						inner join AccountOwner aow1  	
						on a.id= aow1.Account
						where aow1.cust = aow.cust and 
							  aow1.Account <> aow.account and 
							  IsNull(a.Closed,0) = 0))

			--Record Customer Event on Last Closed Account
			Insert Event (TrnTime, Oper, Type, ObjectType, ObjectId , LogText) 
			Select getDate(), i.LastOper, 'Ann', 'Cust',  aow.Cust, 
			'Closed Customer ' + aow.Cust + 
			' because closed last related account ' + i.id + 
			' with relation as ' + aow.relationship 
			from AccountOwner aow 
				inner join inserted i on aow.Account = i.id
				inner join deleted d on i.id = d.id
				where IsNull(d.Closed,0) = 0 and IsNull(i.Closed,0) = 1 and 
				not exists (select a.id from Account a 
						inner join AccountOwner aow1  	
						on a.id= aow1.Account
						where aow1.cust = aow.cust and 
							  aow1.Account <> aow.account and 
							  IsNull(a.Closed,0) = 0)

	   end
  end



  Insert Event (TrnTime, Oper, Type, ObjectType, ObjectId , LogText, EvtDetail) 
   Select getDate(), i.LastOper, 'Mod', 'Acct', convert(varchar, i.Id), 	
 Case when d.Closed = 0 and i.closed = 1 then 'Closed Account' else Null End, 
Case 
	WHEN ((i.[Id] Is Null And d.[Id] Is Not Null ) Or 
	(i.[Id] Is Not Null And d.[Id] Is Null ) Or 
	(i.[Id] != d.[Id])) 
	THEN '| Id OLD: ' + Convert(varchar, IsNull(d.[Id],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Id],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Name] Is Null And d.[Name] Is Not Null ) Or 
	(i.[Name] Is Not Null And d.[Name] Is Null ) Or 
	(i.[Name] != d.[Name])) 
	THEN '| Name OLD: ' + Convert(varchar, IsNull(d.[Name],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Name],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Type] Is Null And d.[Type] Is Not Null ) Or 
	(i.[Type] Is Not Null And d.[Type] Is Null ) Or 
	(i.[Type] != d.[Type])) 
	THEN '| Type OLD: ' + Convert(varchar, IsNull(d.[Type],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Type],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Monitor] Is Null And d.[Monitor] Is Not Null ) Or 
	(i.[Monitor] Is Not Null And d.[Monitor] Is Null ) Or 
	(i.[Monitor] != d.[Monitor])) 
	THEN '| Monitor OLD: ' + case when IsNull(d.[Monitor],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[Monitor],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[ExemptionStatus] Is Null And d.[ExemptionStatus] Is Not Null ) Or 
	(i.[ExemptionStatus] Is Not Null And d.[ExemptionStatus] Is Null ) Or 
	(i.[ExemptionStatus] != d.[ExemptionStatus])) 
	THEN '| ExemptionStatus OLD: ' + Convert(varchar, IsNull(d.[ExemptionStatus],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ExemptionStatus],'Null'))
	ELSE '' End +
Case 
	WHEN (((i.[Notes] Is Null And d.[Notes] Is Not Null ) Or 
	(i.[Notes] Is Not Null And d.[Notes] Is Null ) Or 
	(i.[Notes] != d.[Notes])) and
	len(Convert(varchar(500), IsNull(i.[Notes],'Null'))) > len(Convert(varchar(500), IsNull(d.[Notes],'Null'))))
	THEN '| Notes ADDED: ' + right(Convert(varchar(500), IsNull(i.[Notes],'Null')), 
        len(Convert(varchar(500), IsNull(i.[Notes],'Null')))-len(Convert(varchar(500), IsNull(d.[Notes],'Null'))))
	ELSE '' End +
Case 
	WHEN ((i.[Branch] Is Null And d.[Branch] Is Not Null ) Or 
	(i.[Branch] Is Not Null And d.[Branch] Is Null ) Or 
	(i.[Branch] != d.[Branch])) 
	THEN '| Branch OLD: ' + Convert(varchar, IsNull(d.[Branch],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Branch],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[OpenDate] Is Null And d.[OpenDate] Is Not Null ) Or 
	(i.[OpenDate] Is Not Null And d.[OpenDate] Is Null ) Or 
	(i.[OpenDate] != d.[OpenDate])) 
	THEN '| OpenDate OLD: ' + case when d.[OpenDate] is Null then 'Null' else 
	Convert(varchar, d.[OpenDate]) End + 
	' NEW: ' + case when i.[OpenDate] is Null then 'Null' else 
	Convert(varchar, i.[OpenDate]) End
	ELSE '' End +
Case 
	WHEN ((i.[RiskFactor] Is Null And d.[RiskFactor] Is Not Null ) Or 
	(i.[RiskFactor] Is Not Null And d.[RiskFactor] Is Null ) Or 
	(i.[RiskFactor] != d.[RiskFactor])) 
	THEN '| RiskFactor OLD: ' + Convert(varchar, IsNull(d.[RiskFactor],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[RiskFactor],0))
	ELSE '' End +
Case 
	WHEN ((i.[CreateOper] Is Null And d.[CreateOper] Is Not Null ) Or 
	(i.[CreateOper] Is Not Null And d.[CreateOper] Is Null ) Or 
	(i.[CreateOper] != d.[CreateOper])) 
	THEN '| CreateOper OLD: ' + Convert(varchar, IsNull(d.[CreateOper],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[CreateOper],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[CreateDate] Is Null And d.[CreateDate] Is Not Null ) Or 
	(i.[CreateDate] Is Not Null And d.[CreateDate] Is Null ) Or 
	(i.[CreateDate] != d.[CreateDate])) 
	THEN '| CreateDate OLD: ' + case when d.[CreateDate] is Null then 'Null' else 
	Convert(varchar, d.[CreateDate]) End + 
	' NEW: ' + case when i.[CreateDate] is Null then 'Null' else 
	Convert(varchar, i.[CreateDate]) End
	ELSE '' End +
Case 
	WHEN ((i.[LastOper] Is Null And d.[LastOper] Is Not Null ) Or 
	(i.[LastOper] Is Not Null And d.[LastOper] Is Null ) Or 
	(i.[LastOper] != d.[LastOper])) 
	THEN '| LastOper OLD: ' + Convert(varchar, IsNull(d.[LastOper],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[LastOper],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[LastModify] Is Null And d.[LastModify] Is Not Null ) Or 
	(i.[LastModify] Is Not Null And d.[LastModify] Is Null ) Or 
	(i.[LastModify] != d.[LastModify])) 
	THEN '| LastModify OLD: ' + case when d.[LastModify] is Null then 'Null' else 
	Convert(varchar, d.[LastModify]) End + 
	' NEW: ' + case when i.[LastModify] is Null then 'Null' else 
	Convert(varchar, i.[LastModify]) End
	ELSE '' End +
Case 
	WHEN ((i.[OwnerBranch] Is Null And d.[OwnerBranch] Is Not Null ) Or 
	(i.[OwnerBranch] Is Not Null And d.[OwnerBranch] Is Null ) Or 
	(i.[OwnerBranch] != d.[OwnerBranch])) 
	THEN '| OwnerBranch OLD: ' + Convert(varchar, IsNull(d.[OwnerBranch],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[OwnerBranch],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[OwnerDept] Is Null And d.[OwnerDept] Is Not Null ) Or 
	(i.[OwnerDept] Is Not Null And d.[OwnerDept] Is Null ) Or 
	(i.[OwnerDept] != d.[OwnerDept])) 
	THEN '| OwnerDept OLD: ' + Convert(varchar, IsNull(d.[OwnerDept],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[OwnerDept],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[OwnerOper] Is Null And d.[OwnerOper] Is Not Null ) Or 
	(i.[OwnerOper] Is Not Null And d.[OwnerOper] Is Null ) Or 
	(i.[OwnerOper] != d.[OwnerOper])) 
	THEN '| OwnerOper OLD: ' + Convert(varchar, IsNull(d.[OwnerOper],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[OwnerOper],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Closed] Is Null And d.[Closed] Is Not Null ) Or 
	(i.[Closed] Is Not Null And d.[Closed] Is Null ) Or 
	(i.[Closed] != d.[Closed])) 
	THEN '| Closed OLD: ' + case when IsNull(d.[Closed],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[Closed],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[CloseDate] Is Null And d.[CloseDate] Is Not Null ) Or 
	(i.[CloseDate] Is Not Null And d.[CloseDate] Is Null ) Or 
	(i.[CloseDate] != d.[CloseDate])) 
	THEN '| CloseDate OLD: ' + case when d.[CloseDate] is Null then 'Null' else 
	Convert(varchar, d.[CloseDate]) End + 
	' NEW: ' + case when i.[CloseDate] is Null then 'Null' else 
	Convert(varchar, i.[CloseDate]) End
	ELSE '' End +
Case 
	WHEN ((i.[CloseReason] Is Null And d.[CloseReason] Is Not Null ) Or 
	(i.[CloseReason] Is Not Null And d.[CloseReason] Is Null ) Or 
	(i.[CloseReason] != d.[CloseReason])) 
	THEN '| CloseReason OLD: ' + Convert(varchar, IsNull(d.[CloseReason],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[CloseReason],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Reference] Is Null And d.[Reference] Is Not Null ) Or 
	(i.[Reference] Is Not Null And d.[Reference] Is Null ) Or 
	(i.[Reference] != d.[Reference])) 
	THEN '| Reference OLD: ' + Convert(varchar, IsNull(d.[Reference],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Reference],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[FreeLookEndDate] Is Null And d.[FreeLookEndDate] Is Not Null ) Or 
	(i.[FreeLookEndDate] Is Not Null And d.[FreeLookEndDate] Is Null ) Or 
	(i.[FreeLookEndDate] != d.[FreeLookEndDate])) 
	THEN '| FreeLookEndDate OLD: ' + case when d.[FreeLookEndDate] is Null then 'Null' else 
	Convert(varchar, d.[FreeLookEndDate]) End + 
	' NEW: ' + case when i.[FreeLookEndDate] is Null then 'Null' else 
	Convert(varchar, i.[FreeLookEndDate]) End
	ELSE '' End +
Case 
	WHEN ((i.[Request] Is Null And d.[Request] Is Not Null ) Or 
	(i.[Request] Is Not Null And d.[Request] Is Null ) Or 
	(i.[Request] != d.[Request])) 
	THEN '| Request OLD: ' + case when IsNull(d.[Request],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[Request],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[CashValue] Is Null And d.[CashValue] Is Not Null ) Or 
	(i.[CashValue] Is Not Null And d.[CashValue] Is Null ) Or 
	(i.[CashValue] != d.[CashValue])) 
	THEN '| CashValue OLD: ' + Convert(varchar, IsNull(d.[CashValue],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[CashValue],0))
	ELSE '' End +
Case 
	WHEN ((i.[ExpiryDate] Is Null And d.[ExpiryDate] Is Not Null ) Or 
	(i.[ExpiryDate] Is Not Null And d.[ExpiryDate] Is Null ) Or 
	(i.[ExpiryDate] != d.[ExpiryDate])) 
	THEN '| ExpiryDate OLD: ' + case when d.[ExpiryDate] is Null then 'Null' else 
	Convert(varchar, d.[ExpiryDate]) End + 
	' NEW: ' + case when i.[ExpiryDate] is Null then 'Null' else 
	Convert(varchar, i.[ExpiryDate]) End
	ELSE '' End +
Case 
	WHEN ((i.[SinglePremium] Is Null And d.[SinglePremium] Is Not Null ) Or 
	(i.[SinglePremium] Is Not Null And d.[SinglePremium] Is Null ) Or 
	(i.[SinglePremium] != d.[SinglePremium])) 
	THEN '| SinglePremium OLD: ' + case when IsNull(d.[SinglePremium],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[SinglePremium],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[PolicyAmount] Is Null And d.[PolicyAmount] Is Not Null ) Or 
	(i.[PolicyAmount] Is Not Null And d.[PolicyAmount] Is Null ) Or 
	(i.[PolicyAmount] != d.[PolicyAmount])) 
	THEN '| PolicyAmount OLD: ' + Convert(varchar, IsNull(d.[PolicyAmount],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[PolicyAmount],0))
	ELSE '' End +
Case 
	WHEN ((i.[User1] Is Null And d.[User1] Is Not Null ) Or 
	(i.[User1] Is Not Null And d.[User1] Is Null ) Or 
	(i.[User1] != d.[User1])) 
	THEN '| Currency(User1) OLD: ' + Convert(varchar, IsNull(d.[User1],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User1],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[User2] Is Null And d.[User2] Is Not Null ) Or 
	(i.[User2] Is Not Null And d.[User2] Is Null ) Or 
	(i.[User2] != d.[User2])) 
	THEN '| User2 OLD: ' + Convert(varchar, IsNull(d.[User2],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User2],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[User3] Is Null And d.[User3] Is Not Null ) Or 
	(i.[User3] Is Not Null And d.[User3] Is Null ) Or 
	(i.[User3] != d.[User3])) 
	THEN '| User3 OLD: ' + Convert(varchar, IsNull(d.[User3],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User3],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[User4] Is Null And d.[User4] Is Not Null ) Or 
	(i.[User4] Is Not Null And d.[User4] Is Null ) Or 
	(i.[User4] != d.[User4])) 
	THEN '| User4 OLD: ' + Convert(varchar, IsNull(d.[User4],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User4],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[User5] Is Null And d.[User5] Is Not Null ) Or 
	(i.[User5] Is Not Null And d.[User5] Is Null ) Or 
	(i.[User5] != d.[User5])) 
	THEN '| User5 OLD: ' + Convert(varchar, IsNull(d.[User5],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User5],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[DescOfAcctPurpose] Is Null And d.[DescOfAcctPurpose] Is Not Null ) Or 
	(i.[DescOfAcctPurpose] Is Not Null And d.[DescOfAcctPurpose] Is Null ) Or 
	(i.[DescOfAcctPurpose] != d.[DescOfAcctPurpose])) 
	THEN '| Account Purpose(DescOfAcctPurpose) OLD: ' + Convert(varchar, IsNull(d.[DescOfAcctPurpose],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[DescOfAcctPurpose],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[RemoteDepositCaptureRiskRating] Is Null And d.[RemoteDepositCaptureRiskRating] Is Not Null ) Or 
	(i.[RemoteDepositCaptureRiskRating] Is Not Null And d.[RemoteDepositCaptureRiskRating] Is Null ) Or 
	(i.[RemoteDepositCaptureRiskRating] != d.[RemoteDepositCaptureRiskRating])) 
	THEN '| RemoteDepositCaptureRiskRating OLD: ' + Convert(varchar, IsNull(d.[RemoteDepositCaptureRiskRating],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[RemoteDepositCaptureRiskRating],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[PowerOfAttorney] Is Null And d.[PowerOfAttorney] Is Not Null ) Or 
	(i.[PowerOfAttorney] Is Not Null And d.[PowerOfAttorney] Is Null ) Or 
	(i.[PowerOfAttorney] != d.[PowerOfAttorney])) 
	THEN '| PowerOfAttorney OLD: ' + case when IsNull(d.[PowerOfAttorney],0) = 0 then 'False' else 'True' End + ' NEW: ' + case when IsNull(i.[PowerOfAttorney],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[RemoteDepositCapture] Is Null And d.[RemoteDepositCapture] Is Not Null ) Or 
	(i.[RemoteDepositCapture] Is Not Null And d.[RemoteDepositCapture] Is Null ) Or 
	(i.[RemoteDepositCapture] != d.[RemoteDepositCapture])) 
	THEN '| [RemoteDepositCapture] OLD: ' + case when IsNull(d.[RemoteDepositCapture],0) = 0 then 'False' else 'True' End + ' NEW: ' + case when IsNull(i.[RemoteDepositCapture],0) = 0 then 'False' else 'True' End
	ELSE '' End +	
Case 
	WHEN ((i.[AccountOfficer] Is Null And d.[AccountOfficer] Is Not Null ) Or 
	(i.[AccountOfficer] Is Not Null And d.[AccountOfficer] Is Null ) Or 
	(i.[AccountOfficer] != d.[AccountOfficer])) 
	THEN '| AccountOfficer OLD: ' + Convert(varchar, IsNull(d.[AccountOfficer],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[AccountOfficer],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BalanceAmt] Is Null And d.[BalanceAmt] Is Not Null ) Or 
	(i.[BalanceAmt] Is Not Null And d.[BalanceAmt] Is Null ) Or 
	(i.[BalanceAmt] != d.[BalanceAmt])) 
	THEN '| Account Balance(BalanceAmt) OLD: ' + Convert(varchar, IsNull(d.[BalanceAmt],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BalanceAmt],0))
	ELSE '' End +
Case 
	WHEN ((i.[SourceofPayments] Is Null And d.[SourceofPayments] Is Not Null ) Or 
	(i.[SourceofPayments] Is Not Null And d.[SourceofPayments] Is Null ) Or 
	(i.[SourceofPayments] != d.[SourceofPayments])) 
	THEN '| SourceofPayments OLD: ' + Convert(varchar, IsNull(d.[SourceofPayments],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[SourceofPayments],'Null'))
	ELSE '' End  +
Case 
	WHEN ((i.[AcStatus] Is Null And d.[AcStatus] Is Not Null ) Or 
	(i.[AcStatus] Is Not Null And d.[AcStatus] Is Null ) Or 
	(i.[AcStatus] != d.[AcStatus])) 
	THEN '| Account Status(AcStatus) OLD: ' + Convert(varchar, IsNull(d.[AcStatus],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[AcStatus],'Null'))
	ELSE '' End 
	From Inserted i, deleted d where  i.Id = d.Id
End
GO


