USE [PBSA]
GO

/****** Object:  Trigger [dbo].[BSA_ActivityUpd]    Script Date: 4/13/2021 11:34:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER TRIGGER [dbo].[BSA_ActivityUpd] on [dbo].[Activity]   For Update As

  Update Activity Set LastModify = getDate() 
	From Inserted I, Activity d Where  i.TranNo = d.TranNo

  Insert Event (TrnTime, Oper, Type, ObjectType, ObjectId , LogText, EvtDetail) 
   Select getDate(), i.LastOper, 'Mod', 'Activity', convert(varchar, i.TranNo),  '', 
Case 
	WHEN ((i.[TranNo] Is Null And d.[TranNo] Is Not Null ) Or 
	(i.[TranNo] Is Not Null And d.[TranNo] Is Null ) Or 
	(i.[TranNo] != d.[TranNo])) 
	THEN '| TranNo OLD: ' + Convert(varchar, IsNull(d.[TranNo],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[TranNo],0))
	ELSE '' End +
Case 
	WHEN ((i.[Cust] Is Null And d.[Cust] Is Not Null ) Or 
	(i.[Cust] Is Not Null And d.[Cust] Is Null ) Or 
	(i.[Cust] != d.[Cust])) 
	THEN '| Cust OLD: ' + Convert(varchar, IsNull(d.[Cust],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Cust],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Account] Is Null And d.[Account] Is Not Null ) Or 
	(i.[Account] Is Not Null And d.[Account] Is Null ) Or 
	(i.[Account] != d.[Account])) 
	THEN '| Account OLD: ' + Convert(varchar, IsNull(d.[Account],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Account],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Type] Is Null And d.[Type] Is Not Null ) Or 
	(i.[Type] Is Not Null And d.[Type] Is Null ) Or 
	(i.[Type] != d.[Type])) 
	THEN '| Type OLD: ' + Convert(varchar, IsNull(d.[Type],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Type],0))
	ELSE '' End +
Case 
	WHEN ((i.[CashTran] Is Null And d.[CashTran] Is Not Null ) Or 
	(i.[CashTran] Is Not Null And d.[CashTran] Is Null ) Or 
	(i.[CashTran] != d.[CashTran])) 
	THEN '| CashTran OLD: ' + Convert(varchar, IsNull(d.[CashTran],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[CashTran],0))
	ELSE '' End +
Case 
	WHEN ((i.[BaseAmt] Is Null And d.[BaseAmt] Is Not Null ) Or 
	(i.[BaseAmt] Is Not Null And d.[BaseAmt] Is Null ) Or 
	(i.[BaseAmt] != d.[BaseAmt])) 
	THEN '| BaseAmt OLD: ' + Convert(varchar, IsNull(d.[BaseAmt],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BaseAmt],0))
	ELSE '' End +
Case 
	WHEN ((i.[RecvPay] Is Null And d.[RecvPay] Is Not Null ) Or 
	(i.[RecvPay] Is Not Null And d.[RecvPay] Is Null ) Or 
	(i.[RecvPay] != d.[RecvPay])) 
	THEN '| RecvPay OLD: ' + Convert(varchar, IsNull(d.[RecvPay],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[RecvPay],0))
	ELSE '' End +
Case 
	WHEN ((i.[Curr] Is Null And d.[Curr] Is Not Null ) Or 
	(i.[Curr] Is Not Null And d.[Curr] Is Null ) Or 
	(i.[Curr] != d.[Curr])) 
	THEN '| Curr OLD: ' + Convert(varchar, IsNull(d.[Curr],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Curr],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[FxAmt] Is Null And d.[FxAmt] Is Not Null ) Or 
	(i.[FxAmt] Is Not Null And d.[FxAmt] Is Null ) Or 
	(i.[FxAmt] != d.[FxAmt])) 
	THEN '| FxAmt OLD: ' + Convert(varchar, IsNull(d.[FxAmt],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[FxAmt],0))
	ELSE '' End +
Case 
	WHEN ((i.[Ref] Is Null And d.[Ref] Is Not Null ) Or 
	(i.[Ref] Is Not Null And d.[Ref] Is Null ) Or 
	(i.[Ref] != d.[Ref])) 
	THEN '| Ref OLD: ' + Convert(varchar, IsNull(d.[Ref],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Ref],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ValueDate] Is Null And d.[ValueDate] Is Not Null ) Or 
	(i.[ValueDate] Is Not Null And d.[ValueDate] Is Null ) Or 
	(i.[ValueDate] != d.[ValueDate])) 
	THEN '| ValueDate OLD: ' + Convert(varchar, IsNull(d.[ValueDate],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ValueDate],0))
	ELSE '' End +
Case 
	WHEN ((i.[BookDate] Is Null And d.[BookDate] Is Not Null ) Or 
	(i.[BookDate] Is Not Null And d.[BookDate] Is Null ) Or 
	(i.[BookDate] != d.[BookDate])) 
	THEN '| BookDate OLD: ' + Convert(varchar, IsNull(d.[BookDate],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BookDate],0))
	ELSE '' End +
Case 
	WHEN ((i.[ExemptionStatus] Is Null And d.[ExemptionStatus] Is Not Null ) Or 
	(i.[ExemptionStatus] Is Not Null And d.[ExemptionStatus] Is Null ) Or 
	(i.[ExemptionStatus] != d.[ExemptionStatus])) 
	THEN '| ExemptionStatus OLD: ' + Convert(varchar, IsNull(d.[ExemptionStatus],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ExemptionStatus],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[PaymtMethod] Is Null And d.[PaymtMethod] Is Not Null ) Or 
	(i.[PaymtMethod] Is Not Null And d.[PaymtMethod] Is Null ) Or 
	(i.[PaymtMethod] != d.[PaymtMethod])) 
	THEN '| PaymtMethod OLD: ' + Convert(varchar, IsNull(d.[PaymtMethod],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[PaymtMethod],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[InstrumentType] Is Null And d.[InstrumentType] Is Not Null ) Or 
	(i.[InstrumentType] Is Not Null And d.[InstrumentType] Is Null ) Or 
	(i.[InstrumentType] != d.[InstrumentType])) 
	THEN '| InstrumentType OLD: ' + Convert(varchar, IsNull(d.[InstrumentType],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[InstrumentType],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[InstrumentName] Is Null And d.[InstrumentName] Is Not Null ) Or 
	(i.[InstrumentName] Is Not Null And d.[InstrumentName] Is Null ) Or 
	(i.[InstrumentName] != d.[InstrumentName])) 
	THEN '| DueDate(InstrumentName) OLD: ' + Convert(varchar, IsNull(d.[InstrumentName],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[InstrumentName],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[InstrumentQty] Is Null And d.[InstrumentQty] Is Not Null ) Or 
	(i.[InstrumentQty] Is Not Null And d.[InstrumentQty] Is Null ) Or 
	(i.[InstrumentQty] != d.[InstrumentQty])) 
	THEN '| InstrumentQty OLD: ' + Convert(varchar, IsNull(d.[InstrumentQty],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[InstrumentQty],0))
	ELSE '' End +
Case 
	WHEN ((i.[InstrumentPrice] Is Null And d.[InstrumentPrice] Is Not Null ) Or 
	(i.[InstrumentPrice] Is Not Null And d.[InstrumentPrice] Is Null ) Or 
	(i.[InstrumentPrice] != d.[InstrumentPrice])) 
	THEN '| Tx.Balance(InstrumentPrice) OLD: ' + Convert(varchar, IsNull(d.[InstrumentPrice],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[InstrumentPrice],0))
	ELSE '' End +
Case 
	WHEN ((i.[RelatedRef] Is Null And d.[RelatedRef] Is Not Null ) Or 
	(i.[RelatedRef] Is Not Null And d.[RelatedRef] Is Null ) Or 
	(i.[RelatedRef] != d.[RelatedRef])) 
	THEN '| RelatedRef OLD: ' + Convert(varchar, IsNull(d.[RelatedRef],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[RelatedRef],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ReferenceType] Is Null And d.[ReferenceType] Is Not Null ) Or 
	(i.[ReferenceType] Is Not Null And d.[ReferenceType] Is Null ) Or 
	(i.[ReferenceType] != d.[ReferenceType])) 
	THEN '| ReferenceType OLD: ' + Convert(varchar, IsNull(d.[ReferenceType],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ReferenceType],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneCustId] Is Null And d.[BeneCustId] Is Not Null ) Or 
	(i.[BeneCustId] Is Not Null And d.[BeneCustId] Is Null ) Or 
	(i.[BeneCustId] != d.[BeneCustId])) 
	THEN '| BeneCustId OLD: ' + Convert(varchar, IsNull(d.[BeneCustId],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneCustId],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Bene] Is Null And d.[Bene] Is Not Null ) Or 
	(i.[Bene] Is Not Null And d.[Bene] Is Null ) Or 
	(i.[Bene] != d.[Bene])) 
	THEN '| Bene OLD: ' + Convert(varchar, IsNull(d.[Bene],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Bene],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneAcct] Is Null And d.[BeneAcct] Is Not Null ) Or 
	(i.[BeneAcct] Is Not Null And d.[BeneAcct] Is Null ) Or 
	(i.[BeneAcct] != d.[BeneAcct])) 
	THEN '| BeneAcct OLD: ' + Convert(varchar, IsNull(d.[BeneAcct],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneAcct],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneAddress] Is Null And d.[BeneAddress] Is Not Null ) Or 
	(i.[BeneAddress] Is Not Null And d.[BeneAddress] Is Null ) Or 
	(i.[BeneAddress] != d.[BeneAddress])) 
	THEN '| BeneAddress OLD: ' + Convert(varchar, IsNull(d.[BeneAddress],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneAddress],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneCity] Is Null And d.[BeneCity] Is Not Null ) Or 
	(i.[BeneCity] Is Not Null And d.[BeneCity] Is Null ) Or 
	(i.[BeneCity] != d.[BeneCity])) 
	THEN '| BeneCity OLD: ' + Convert(varchar, IsNull(d.[BeneCity],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneCity],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneState] Is Null And d.[BeneState] Is Not Null ) Or 
	(i.[BeneState] Is Not Null And d.[BeneState] Is Null ) Or 
	(i.[BeneState] != d.[BeneState])) 
	THEN '| BeneState OLD: ' + Convert(varchar, IsNull(d.[BeneState],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneState],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneZip] Is Null And d.[BeneZip] Is Not Null ) Or 
	(i.[BeneZip] Is Not Null And d.[BeneZip] Is Null ) Or 
	(i.[BeneZip] != d.[BeneZip])) 
	THEN '| BeneZip OLD: ' + Convert(varchar, IsNull(d.[BeneZip],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneZip],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneCountry] Is Null And d.[BeneCountry] Is Not Null ) Or 
	(i.[BeneCountry] Is Not Null And d.[BeneCountry] Is Null ) Or 
	(i.[BeneCountry] != d.[BeneCountry])) 
	THEN '| BeneCountry OLD: ' + Convert(varchar, IsNull(d.[BeneCountry],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneCountry],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneBankCustId] Is Null And d.[BeneBankCustId] Is Not Null ) Or 
	(i.[BeneBankCustId] Is Not Null And d.[BeneBankCustId] Is Null ) Or 
	(i.[BeneBankCustId] != d.[BeneBankCustId])) 
	THEN '| BeneBankCustId OLD: ' + Convert(varchar, IsNull(d.[BeneBankCustId],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneBankCustId],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneBank] Is Null And d.[BeneBank] Is Not Null ) Or 
	(i.[BeneBank] Is Not Null And d.[BeneBank] Is Null ) Or 
	(i.[BeneBank] != d.[BeneBank])) 
	THEN '| BeneBank OLD: ' + Convert(varchar, IsNull(d.[BeneBank],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneBank],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneBankId] Is Null And d.[BeneBankId] Is Not Null ) Or 
	(i.[BeneBankId] Is Not Null And d.[BeneBankId] Is Null ) Or 
	(i.[BeneBankId] != d.[BeneBankId])) 
	THEN '| BeneBankId OLD: ' + Convert(varchar, IsNull(d.[BeneBankId],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneBankId],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneBankAcct] Is Null And d.[BeneBankAcct] Is Not Null ) Or 
	(i.[BeneBankAcct] Is Not Null And d.[BeneBankAcct] Is Null ) Or 
	(i.[BeneBankAcct] != d.[BeneBankAcct])) 
	THEN '| BeneBankAcct OLD: ' + Convert(varchar, IsNull(d.[BeneBankAcct],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneBankAcct],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneBankAddress] Is Null And d.[BeneBankAddress] Is Not Null ) Or 
	(i.[BeneBankAddress] Is Not Null And d.[BeneBankAddress] Is Null ) Or 
	(i.[BeneBankAddress] != d.[BeneBankAddress])) 
	THEN '| BeneBankAddress OLD: ' + Convert(varchar, IsNull(d.[BeneBankAddress],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneBankAddress],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneBankCity] Is Null And d.[BeneBankCity] Is Not Null ) Or 
	(i.[BeneBankCity] Is Not Null And d.[BeneBankCity] Is Null ) Or 
	(i.[BeneBankCity] != d.[BeneBankCity])) 
	THEN '| BeneBankCity OLD: ' + Convert(varchar, IsNull(d.[BeneBankCity],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneBankCity],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneBankState] Is Null And d.[BeneBankState] Is Not Null ) Or 
	(i.[BeneBankState] Is Not Null And d.[BeneBankState] Is Null ) Or 
	(i.[BeneBankState] != d.[BeneBankState])) 
	THEN '| BeneBankState OLD: ' + Convert(varchar, IsNull(d.[BeneBankState],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneBankState],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneBankZip] Is Null And d.[BeneBankZip] Is Not Null ) Or 
	(i.[BeneBankZip] Is Not Null And d.[BeneBankZip] Is Null ) Or 
	(i.[BeneBankZip] != d.[BeneBankZip])) 
	THEN '| BeneBankZip OLD: ' + Convert(varchar, IsNull(d.[BeneBankZip],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneBankZip],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneBankCountry] Is Null And d.[BeneBankCountry] Is Not Null ) Or 
	(i.[BeneBankCountry] Is Not Null And d.[BeneBankCountry] Is Null ) Or 
	(i.[BeneBankCountry] != d.[BeneBankCountry])) 
	THEN '| BeneBankCountry OLD: ' + Convert(varchar, IsNull(d.[BeneBankCountry],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneBankCountry],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryCustId] Is Null And d.[IntermediaryCustId] Is Not Null ) Or 
	(i.[IntermediaryCustId] Is Not Null And d.[IntermediaryCustId] Is Null ) Or 
	(i.[IntermediaryCustId] != d.[IntermediaryCustId])) 
	THEN '| IntermediaryCustId OLD: ' + Convert(varchar, IsNull(d.[IntermediaryCustId],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryCustId],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Intermediary] Is Null And d.[Intermediary] Is Not Null ) Or 
	(i.[Intermediary] Is Not Null And d.[Intermediary] Is Null ) Or 
	(i.[Intermediary] != d.[Intermediary])) 
	THEN '| Intermediary OLD: ' + Convert(varchar, IsNull(d.[Intermediary],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Intermediary],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryId] Is Null And d.[IntermediaryId] Is Not Null ) Or 
	(i.[IntermediaryId] Is Not Null And d.[IntermediaryId] Is Null ) Or 
	(i.[IntermediaryId] != d.[IntermediaryId])) 
	THEN '| IntermediaryId OLD: ' + Convert(varchar, IsNull(d.[IntermediaryId],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryId],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryAcct] Is Null And d.[IntermediaryAcct] Is Not Null ) Or 
	(i.[IntermediaryAcct] Is Not Null And d.[IntermediaryAcct] Is Null ) Or 
	(i.[IntermediaryAcct] != d.[IntermediaryAcct])) 
	THEN '| IntermediaryAcct OLD: ' + Convert(varchar, IsNull(d.[IntermediaryAcct],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryAcct],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryAddress] Is Null And d.[IntermediaryAddress] Is Not Null ) Or 
	(i.[IntermediaryAddress] Is Not Null And d.[IntermediaryAddress] Is Null ) Or 
	(i.[IntermediaryAddress] != d.[IntermediaryAddress])) 
	THEN '| IntermediaryAddress OLD: ' + Convert(varchar, IsNull(d.[IntermediaryAddress],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryAddress],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryCity] Is Null And d.[IntermediaryCity] Is Not Null ) Or 
	(i.[IntermediaryCity] Is Not Null And d.[IntermediaryCity] Is Null ) Or 
	(i.[IntermediaryCity] != d.[IntermediaryCity])) 
	THEN '| IntermediaryCity OLD: ' + Convert(varchar, IsNull(d.[IntermediaryCity],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryCity],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryState] Is Null And d.[IntermediaryState] Is Not Null ) Or 
	(i.[IntermediaryState] Is Not Null And d.[IntermediaryState] Is Null ) Or 
	(i.[IntermediaryState] != d.[IntermediaryState])) 
	THEN '| IntermediaryState OLD: ' + Convert(varchar, IsNull(d.[IntermediaryState],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryState],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryZip] Is Null And d.[IntermediaryZip] Is Not Null ) Or 
	(i.[IntermediaryZip] Is Not Null And d.[IntermediaryZip] Is Null ) Or 
	(i.[IntermediaryZip] != d.[IntermediaryZip])) 
	THEN '| IntermediaryZip OLD: ' + Convert(varchar, IsNull(d.[IntermediaryZip],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryZip],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryCountry] Is Null And d.[IntermediaryCountry] Is Not Null ) Or 
	(i.[IntermediaryCountry] Is Not Null And d.[IntermediaryCountry] Is Null ) Or 
	(i.[IntermediaryCountry] != d.[IntermediaryCountry])) 
	THEN '| IntermediaryCountry OLD: ' + Convert(varchar, IsNull(d.[IntermediaryCountry],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryCountry],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderCustId] Is Null And d.[ByOrderCustId] Is Not Null ) Or 
	(i.[ByOrderCustId] Is Not Null And d.[ByOrderCustId] Is Null ) Or 
	(i.[ByOrderCustId] != d.[ByOrderCustId])) 
	THEN '| ByOrderCustId OLD: ' + Convert(varchar, IsNull(d.[ByOrderCustId],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderCustId],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrder] Is Null And d.[ByOrder] Is Not Null ) Or 
	(i.[ByOrder] Is Not Null And d.[ByOrder] Is Null ) Or 
	(i.[ByOrder] != d.[ByOrder])) 
	THEN '| ByOrder OLD: ' + Convert(varchar, IsNull(d.[ByOrder],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrder],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderAcct] Is Null And d.[ByOrderAcct] Is Not Null ) Or 
	(i.[ByOrderAcct] Is Not Null And d.[ByOrderAcct] Is Null ) Or 
	(i.[ByOrderAcct] != d.[ByOrderAcct])) 
	THEN '| ByOrderAcct OLD: ' + Convert(varchar, IsNull(d.[ByOrderAcct],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderAcct],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderAddress] Is Null And d.[ByOrderAddress] Is Not Null ) Or 
	(i.[ByOrderAddress] Is Not Null And d.[ByOrderAddress] Is Null ) Or 
	(i.[ByOrderAddress] != d.[ByOrderAddress])) 
	THEN '| ByOrderAddress OLD: ' + Convert(varchar, IsNull(d.[ByOrderAddress],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderAddress],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderCity] Is Null And d.[ByOrderCity] Is Not Null ) Or 
	(i.[ByOrderCity] Is Not Null And d.[ByOrderCity] Is Null ) Or 
	(i.[ByOrderCity] != d.[ByOrderCity])) 
	THEN '| ByOrderCity OLD: ' + Convert(varchar, IsNull(d.[ByOrderCity],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderCity],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderState] Is Null And d.[ByOrderState] Is Not Null ) Or 
	(i.[ByOrderState] Is Not Null And d.[ByOrderState] Is Null ) Or 
	(i.[ByOrderState] != d.[ByOrderState])) 
	THEN '| ByOrderState OLD: ' + Convert(varchar, IsNull(d.[ByOrderState],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderState],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderZip] Is Null And d.[ByOrderZip] Is Not Null ) Or 
	(i.[ByOrderZip] Is Not Null And d.[ByOrderZip] Is Null ) Or 
	(i.[ByOrderZip] != d.[ByOrderZip])) 
	THEN '| ByOrderZip OLD: ' + Convert(varchar, IsNull(d.[ByOrderZip],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderZip],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderCountry] Is Null And d.[ByOrderCountry] Is Not Null ) Or 
	(i.[ByOrderCountry] Is Not Null And d.[ByOrderCountry] Is Null ) Or 
	(i.[ByOrderCountry] != d.[ByOrderCountry])) 
	THEN '| ByOrderCountry OLD: ' + Convert(varchar, IsNull(d.[ByOrderCountry],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderCountry],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderBankCustId] Is Null And d.[ByOrderBankCustId] Is Not Null ) Or 
	(i.[ByOrderBankCustId] Is Not Null And d.[ByOrderBankCustId] Is Null ) Or 
	(i.[ByOrderBankCustId] != d.[ByOrderBankCustId])) 
	THEN '| ByOrderBankCustId OLD: ' + Convert(varchar, IsNull(d.[ByOrderBankCustId],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderBankCustId],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderBank] Is Null And d.[ByOrderBank] Is Not Null ) Or 
	(i.[ByOrderBank] Is Not Null And d.[ByOrderBank] Is Null ) Or 
	(i.[ByOrderBank] != d.[ByOrderBank])) 
	THEN '| ByOrderBank OLD: ' + Convert(varchar, IsNull(d.[ByOrderBank],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderBank],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderBankId] Is Null And d.[ByOrderBankId] Is Not Null ) Or 
	(i.[ByOrderBankId] Is Not Null And d.[ByOrderBankId] Is Null ) Or 
	(i.[ByOrderBankId] != d.[ByOrderBankId])) 
	THEN '| ByOrderBankId OLD: ' + Convert(varchar, IsNull(d.[ByOrderBankId],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderBankId],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderBankAcct] Is Null And d.[ByOrderBankAcct] Is Not Null ) Or 
	(i.[ByOrderBankAcct] Is Not Null And d.[ByOrderBankAcct] Is Null ) Or 
	(i.[ByOrderBankAcct] != d.[ByOrderBankAcct])) 
	THEN '| ByOrderBankAcct OLD: ' + Convert(varchar, IsNull(d.[ByOrderBankAcct],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderBankAcct],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderBankAddress] Is Null And d.[ByOrderBankAddress] Is Not Null ) Or 
	(i.[ByOrderBankAddress] Is Not Null And d.[ByOrderBankAddress] Is Null ) Or 
	(i.[ByOrderBankAddress] != d.[ByOrderBankAddress])) 
	THEN '| ByOrderBankAddress OLD: ' + Convert(varchar, IsNull(d.[ByOrderBankAddress],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderBankAddress],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderBankCity] Is Null And d.[ByOrderBankCity] Is Not Null ) Or 
	(i.[ByOrderBankCity] Is Not Null And d.[ByOrderBankCity] Is Null ) Or 
	(i.[ByOrderBankCity] != d.[ByOrderBankCity])) 
	THEN '| ByOrderBankCity OLD: ' + Convert(varchar, IsNull(d.[ByOrderBankCity],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderBankCity],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderBankState] Is Null And d.[ByOrderBankState] Is Not Null ) Or 
	(i.[ByOrderBankState] Is Not Null And d.[ByOrderBankState] Is Null ) Or 
	(i.[ByOrderBankState] != d.[ByOrderBankState])) 
	THEN '| ByOrderBankState OLD: ' + Convert(varchar, IsNull(d.[ByOrderBankState],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderBankState],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderBankZip] Is Null And d.[ByOrderBankZip] Is Not Null ) Or 
	(i.[ByOrderBankZip] Is Not Null And d.[ByOrderBankZip] Is Null ) Or 
	(i.[ByOrderBankZip] != d.[ByOrderBankZip])) 
	THEN '| ByOrderBankZip OLD: ' + Convert(varchar, IsNull(d.[ByOrderBankZip],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderBankZip],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderBankCountry] Is Null And d.[ByOrderBankCountry] Is Not Null ) Or 
	(i.[ByOrderBankCountry] Is Not Null And d.[ByOrderBankCountry] Is Null ) Or 
	(i.[ByOrderBankCountry] != d.[ByOrderBankCountry])) 
	THEN '| ByOrderBankCountry OLD: ' + Convert(varchar, IsNull(d.[ByOrderBankCountry],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderBankCountry],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Instructions] Is Null And d.[Instructions] Is Not Null ) Or 
	(i.[Instructions] Is Not Null And d.[Instructions] Is Null ) Or 
	(i.[Instructions] != d.[Instructions])) 
	THEN '| Instructions OLD: ' + Convert(varchar, IsNull(d.[Instructions],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Instructions],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Branch] Is Null And d.[Branch] Is Not Null ) Or 
	(i.[Branch] Is Not Null And d.[Branch] Is Null ) Or 
	(i.[Branch] != d.[Branch])) 
	THEN '| Branch OLD: ' + Convert(varchar, IsNull(d.[Branch],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Branch],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Dept] Is Null And d.[Dept] Is Not Null ) Or 
	(i.[Dept] Is Not Null And d.[Dept] Is Null ) Or 
	(i.[Dept] != d.[Dept])) 
	THEN '| Dept OLD: ' + Convert(varchar, IsNull(d.[Dept],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Dept],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[TranDate] Is Null And d.[TranDate] Is Not Null ) Or 
	(i.[TranDate] Is Not Null And d.[TranDate] Is Null ) Or 
	(i.[TranDate] != d.[TranDate])) 
	THEN '| TranDate OLD: ' + case when d.[TranDate] is Null then 'Null' else 
	Convert(varchar, d.[TranDate]) End + 
	' NEW: ' + case when i.[TranDate] is Null then 'Null' else 
	Convert(varchar, i.[TranDate]) End
	ELSE '' End +
Case 
	WHEN ((i.[CostCenter] Is Null And d.[CostCenter] Is Not Null ) Or 
	(i.[CostCenter] Is Not Null And d.[CostCenter] Is Null ) Or 
	(i.[CostCenter] != d.[CostCenter])) 
	THEN '| CostCenter OLD: ' + Convert(varchar, IsNull(d.[CostCenter],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[CostCenter],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryCustId2] Is Null And d.[IntermediaryCustId2] Is Not Null ) Or 
	(i.[IntermediaryCustId2] Is Not Null And d.[IntermediaryCustId2] Is Null ) Or 
	(i.[IntermediaryCustId2] != d.[IntermediaryCustId2])) 
	THEN '| IntermediaryCustId2 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryCustId2],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryCustId2],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Intermediary2] Is Null And d.[Intermediary2] Is Not Null ) Or 
	(i.[Intermediary2] Is Not Null And d.[Intermediary2] Is Null ) Or 
	(i.[Intermediary2] != d.[Intermediary2])) 
	THEN '| Intermediary2 OLD: ' + Convert(varchar, IsNull(d.[Intermediary2],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Intermediary2],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryId2] Is Null And d.[IntermediaryId2] Is Not Null ) Or 
	(i.[IntermediaryId2] Is Not Null And d.[IntermediaryId2] Is Null ) Or 
	(i.[IntermediaryId2] != d.[IntermediaryId2])) 
	THEN '| IntermediaryId2 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryId2],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryId2],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryAcct2] Is Null And d.[IntermediaryAcct2] Is Not Null ) Or 
	(i.[IntermediaryAcct2] Is Not Null And d.[IntermediaryAcct2] Is Null ) Or 
	(i.[IntermediaryAcct2] != d.[IntermediaryAcct2])) 
	THEN '| IntermediaryAcct2 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryAcct2],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryAcct2],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryAddress2] Is Null And d.[IntermediaryAddress2] Is Not Null ) Or 
	(i.[IntermediaryAddress2] Is Not Null And d.[IntermediaryAddress2] Is Null ) Or 
	(i.[IntermediaryAddress2] != d.[IntermediaryAddress2])) 
	THEN '| IntermediaryAddress2 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryAddress2],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryAddress2],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryCity2] Is Null And d.[IntermediaryCity2] Is Not Null ) Or 
	(i.[IntermediaryCity2] Is Not Null And d.[IntermediaryCity2] Is Null ) Or 
	(i.[IntermediaryCity2] != d.[IntermediaryCity2])) 
	THEN '| IntermediaryCity2 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryCity2],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryCity2],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryState2] Is Null And d.[IntermediaryState2] Is Not Null ) Or 
	(i.[IntermediaryState2] Is Not Null And d.[IntermediaryState2] Is Null ) Or 
	(i.[IntermediaryState2] != d.[IntermediaryState2])) 
	THEN '| IntermediaryState2 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryState2],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryState2],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryZip2] Is Null And d.[IntermediaryZip2] Is Not Null ) Or 
	(i.[IntermediaryZip2] Is Not Null And d.[IntermediaryZip2] Is Null ) Or 
	(i.[IntermediaryZip2] != d.[IntermediaryZip2])) 
	THEN '| IntermediaryZip2 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryZip2],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryZip2],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryCountry2] Is Null And d.[IntermediaryCountry2] Is Not Null ) Or 
	(i.[IntermediaryCountry2] Is Not Null And d.[IntermediaryCountry2] Is Null ) Or 
	(i.[IntermediaryCountry2] != d.[IntermediaryCountry2])) 
	THEN '| IntermediaryCountry2 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryCountry2],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryCountry2],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryCustId3] Is Null And d.[IntermediaryCustId3] Is Not Null ) Or 
	(i.[IntermediaryCustId3] Is Not Null And d.[IntermediaryCustId3] Is Null ) Or 
	(i.[IntermediaryCustId3] != d.[IntermediaryCustId3])) 
	THEN '| IntermediaryCustId3 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryCustId3],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryCustId3],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Intermediary3] Is Null And d.[Intermediary3] Is Not Null ) Or 
	(i.[Intermediary3] Is Not Null And d.[Intermediary3] Is Null ) Or 
	(i.[Intermediary3] != d.[Intermediary3])) 
	THEN '| Intermediary3 OLD: ' + Convert(varchar, IsNull(d.[Intermediary3],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Intermediary3],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryId3] Is Null And d.[IntermediaryId3] Is Not Null ) Or 
	(i.[IntermediaryId3] Is Not Null And d.[IntermediaryId3] Is Null ) Or 
	(i.[IntermediaryId3] != d.[IntermediaryId3])) 
	THEN '| IntermediaryId3 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryId3],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryId3],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryAcct3] Is Null And d.[IntermediaryAcct3] Is Not Null ) Or 
	(i.[IntermediaryAcct3] Is Not Null And d.[IntermediaryAcct3] Is Null ) Or 
	(i.[IntermediaryAcct3] != d.[IntermediaryAcct3])) 
	THEN '| IntermediaryAcct3 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryAcct3],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryAcct3],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryAddress3] Is Null And d.[IntermediaryAddress3] Is Not Null ) Or 
	(i.[IntermediaryAddress3] Is Not Null And d.[IntermediaryAddress3] Is Null ) Or 
	(i.[IntermediaryAddress3] != d.[IntermediaryAddress3])) 
	THEN '| IntermediaryAddress3 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryAddress3],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryAddress3],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryCity3] Is Null And d.[IntermediaryCity3] Is Not Null ) Or 
	(i.[IntermediaryCity3] Is Not Null And d.[IntermediaryCity3] Is Null ) Or 
	(i.[IntermediaryCity3] != d.[IntermediaryCity3])) 
	THEN '| IntermediaryCity3 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryCity3],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryCity3],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryState3] Is Null And d.[IntermediaryState3] Is Not Null ) Or 
	(i.[IntermediaryState3] Is Not Null And d.[IntermediaryState3] Is Null ) Or 
	(i.[IntermediaryState3] != d.[IntermediaryState3])) 
	THEN '| IntermediaryState3 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryState3],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryState3],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryZip3] Is Null And d.[IntermediaryZip3] Is Not Null ) Or 
	(i.[IntermediaryZip3] Is Not Null And d.[IntermediaryZip3] Is Null ) Or 
	(i.[IntermediaryZip3] != d.[IntermediaryZip3])) 
	THEN '| IntermediaryZip3 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryZip3],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryZip3],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryCountry3] Is Null And d.[IntermediaryCountry3] Is Not Null ) Or 
	(i.[IntermediaryCountry3] Is Not Null And d.[IntermediaryCountry3] Is Null ) Or 
	(i.[IntermediaryCountry3] != d.[IntermediaryCountry3])) 
	THEN '| IntermediaryCountry3 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryCountry3],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryCountry3],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryCustId4] Is Null And d.[IntermediaryCustId4] Is Not Null ) Or 
	(i.[IntermediaryCustId4] Is Not Null And d.[IntermediaryCustId4] Is Null ) Or 
	(i.[IntermediaryCustId4] != d.[IntermediaryCustId4])) 
	THEN '| IntermediaryCustId4 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryCustId4],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryCustId4],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Intermediary4] Is Null And d.[Intermediary4] Is Not Null ) Or 
	(i.[Intermediary4] Is Not Null And d.[Intermediary4] Is Null ) Or 
	(i.[Intermediary4] != d.[Intermediary4])) 
	THEN '| Intermediary4 OLD: ' + Convert(varchar, IsNull(d.[Intermediary4],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Intermediary4],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryId4] Is Null And d.[IntermediaryId4] Is Not Null ) Or 
	(i.[IntermediaryId4] Is Not Null And d.[IntermediaryId4] Is Null ) Or 
	(i.[IntermediaryId4] != d.[IntermediaryId4])) 
	THEN '| IntermediaryId4 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryId4],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryId4],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryAcct4] Is Null And d.[IntermediaryAcct4] Is Not Null ) Or 
	(i.[IntermediaryAcct4] Is Not Null And d.[IntermediaryAcct4] Is Null ) Or 
	(i.[IntermediaryAcct4] != d.[IntermediaryAcct4])) 
	THEN '| IntermediaryAcct4 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryAcct4],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryAcct4],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryAddress4] Is Null And d.[IntermediaryAddress4] Is Not Null ) Or 
	(i.[IntermediaryAddress4] Is Not Null And d.[IntermediaryAddress4] Is Null ) Or 
	(i.[IntermediaryAddress4] != d.[IntermediaryAddress4])) 
	THEN '| IntermediaryAddress4 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryAddress4],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryAddress4],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryCity4] Is Null And d.[IntermediaryCity4] Is Not Null ) Or 
	(i.[IntermediaryCity4] Is Not Null And d.[IntermediaryCity4] Is Null ) Or 
	(i.[IntermediaryCity4] != d.[IntermediaryCity4])) 
	THEN '| IntermediaryCity4 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryCity4],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryCity4],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryState4] Is Null And d.[IntermediaryState4] Is Not Null ) Or 
	(i.[IntermediaryState4] Is Not Null And d.[IntermediaryState4] Is Null ) Or 
	(i.[IntermediaryState4] != d.[IntermediaryState4])) 
	THEN '| IntermediaryState4 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryState4],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryState4],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryZip4] Is Null And d.[IntermediaryZip4] Is Not Null ) Or 
	(i.[IntermediaryZip4] Is Not Null And d.[IntermediaryZip4] Is Null ) Or 
	(i.[IntermediaryZip4] != d.[IntermediaryZip4])) 
	THEN '| IntermediaryZip4 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryZip4],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryZip4],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryCountry4] Is Null And d.[IntermediaryCountry4] Is Not Null ) Or 
	(i.[IntermediaryCountry4] Is Not Null And d.[IntermediaryCountry4] Is Null ) Or 
	(i.[IntermediaryCountry4] != d.[IntermediaryCountry4])) 
	THEN '| IntermediaryCountry4 OLD: ' + Convert(varchar, IsNull(d.[IntermediaryCountry4],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryCountry4],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[User1] Is Null And d.[User1] Is Not Null ) Or 
	(i.[User1] Is Not Null And d.[User1] Is Null ) Or 
	(i.[User1] != d.[User1])) 
	THEN '| User1 OLD: ' + Convert(varchar, IsNull(d.[User1],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User1],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[User2] Is Null And d.[User2] Is Not Null ) Or 
	(i.[User2] Is Not Null And d.[User2] Is Null ) Or 
	(i.[User2] != d.[User2])) 
	THEN '| User2 OLD: ' + Convert(varchar, IsNull(d.[User2],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User2],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[User3] Is Null And d.[User3] Is Not Null ) Or 
	(i.[User3] Is Not Null And d.[User3] Is Null ) Or 
	(i.[User3] != d.[User3])) 
	THEN '| User3 OLD: ' + Convert(varchar, IsNull(d.[User3],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User3],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[User4] Is Null And d.[User4] Is Not Null ) Or 
	(i.[User4] Is Not Null And d.[User4] Is Null ) Or 
	(i.[User4] != d.[User4])) 
	THEN '| Bop Category(User4) OLD: ' + Convert(varchar, IsNull(d.[User4],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User4],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[User5] Is Null And d.[User5] Is Not Null ) Or 
	(i.[User5] Is Not Null And d.[User5] Is Null ) Or 
	(i.[User5] != d.[User5])) 
	THEN '| SUBCAT(User5) OLD: ' + Convert(varchar, IsNull(d.[User5],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User5],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneEncode] Is Null And d.[BeneEncode] Is Not Null ) Or 
	(i.[BeneEncode] Is Not Null And d.[BeneEncode] Is Null ) Or 
	(i.[BeneEncode] != d.[BeneEncode])) 
	THEN '| BeneEncode OLD: ' + Convert(varchar, IsNull(d.[BeneEncode],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneEncode],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[BeneBankEncode] Is Null And d.[BeneBankEncode] Is Not Null ) Or 
	(i.[BeneBankEncode] Is Not Null And d.[BeneBankEncode] Is Null ) Or 
	(i.[BeneBankEncode] != d.[BeneBankEncode])) 
	THEN '| BeneBankEncode OLD: ' + Convert(varchar, IsNull(d.[BeneBankEncode],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BeneBankEncode],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IntermediaryEncode] Is Null And d.[IntermediaryEncode] Is Not Null ) Or 
	(i.[IntermediaryEncode] Is Not Null And d.[IntermediaryEncode] Is Null ) Or 
	(i.[IntermediaryEncode] != d.[IntermediaryEncode])) 
	THEN '| IntermediaryEncode OLD: ' + Convert(varchar, IsNull(d.[IntermediaryEncode],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IntermediaryEncode],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderEncode] Is Null And d.[ByOrderEncode] Is Not Null ) Or 
	(i.[ByOrderEncode] Is Not Null And d.[ByOrderEncode] Is Null ) Or 
	(i.[ByOrderEncode] != d.[ByOrderEncode])) 
	THEN '| ByOrderEncode OLD: ' + Convert(varchar, IsNull(d.[ByOrderEncode],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderEncode],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ByOrderBankEncode] Is Null And d.[ByOrderBankEncode] Is Not Null ) Or 
	(i.[ByOrderBankEncode] Is Not Null And d.[ByOrderBankEncode] Is Null ) Or 
	(i.[ByOrderBankEncode] != d.[ByOrderBankEncode])) 
	THEN '| ByOrderBankEncode OLD: ' + Convert(varchar, IsNull(d.[ByOrderBankEncode],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ByOrderBankEncode],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Intermediary2Encode] Is Null And d.[Intermediary2Encode] Is Not Null ) Or 
	(i.[Intermediary2Encode] Is Not Null And d.[Intermediary2Encode] Is Null ) Or 
	(i.[Intermediary2Encode] != d.[Intermediary2Encode])) 
	THEN '| Intermediary2Encode OLD: ' + Convert(varchar, IsNull(d.[Intermediary2Encode],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Intermediary2Encode],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Intermediary3Encode] Is Null And d.[Intermediary3Encode] Is Not Null ) Or 
	(i.[Intermediary3Encode] Is Not Null And d.[Intermediary3Encode] Is Null ) Or 
	(i.[Intermediary3Encode] != d.[Intermediary3Encode])) 
	THEN '| Intermediary3Encode OLD: ' + Convert(varchar, IsNull(d.[Intermediary3Encode],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Intermediary3Encode],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Intermediary4Encode] Is Null And d.[Intermediary4Encode] Is Not Null ) Or 
	(i.[Intermediary4Encode] Is Not Null And d.[Intermediary4Encode] Is Null ) Or 
	(i.[Intermediary4Encode] != d.[Intermediary4Encode])) 
	THEN '| Intermediary4Encode OLD: ' + Convert(varchar, IsNull(d.[Intermediary4Encode],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Intermediary4Encode],'Null'))
	ELSE '' End +
Case 
	WHEN (((i.[Notes] Is Null And d.[Notes] Is Not Null ) Or 
	(i.[Notes] Is Not Null And d.[Notes] Is Null ) Or 
	(i.[Notes] != d.[Notes])) and
	 len(Convert(varchar(255), IsNull(i.[Notes],'Null'))) > len(Convert(varchar(255), IsNull(d.[Notes],'Null'))))
	THEN '| Notes ADDED: ' + right(Convert(varchar(255), IsNull(i.[Notes],'Null')), 
        len(Convert(varchar(255), IsNull(i.[Notes],'Null')))-len(Convert(varchar(255), IsNull(d.[Notes],'Null'))))
	ELSE '' End +
Case 
	WHEN ((i.[LastOper] Is Null And d.[LastOper] Is Not Null ) Or 
	(i.[LastOper] Is Not Null And d.[LastOper] Is Null ) Or 
	(i.[LastOper] != d.[LastOper])) 
	THEN '| LastOper OLD: ' + Convert(varchar, IsNull(d.[LastOper],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[LastOper],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[LastModify] Is Null And d.[LastModify] Is Not Null ) Or 
	(i.[LastModify] Is Not Null And d.[LastModify] Is Null ) Or 
	(i.[LastModify] != d.[LastModify])) 
	THEN '| LastModify OLD: ' + case when d.[LastModify] is Null then 'Null' else 
	Convert(varchar, d.[LastModify]) End + 
	' NEW: ' + case when i.[LastModify] is Null then 'Null' else 
	Convert(varchar, i.[LastModify]) End
	ELSE '' End +
Case 
	WHEN ((i.[Purge] Is Null And d.[Purge] Is Not Null ) Or 
	(i.[Purge] Is Not Null And d.[Purge] Is Null ) Or 
	(i.[Purge] != d.[Purge])) 
	THEN '| Purge OLD: ' + case when IsNull(d.[Purge],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[Purge],0) = 0 then 'False' else 'True' End
	ELSE '' End 
	From Inserted i, deleted d where  i.TranNo = d.TranNo
GO


