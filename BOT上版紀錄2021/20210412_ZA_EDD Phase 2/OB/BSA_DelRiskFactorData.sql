USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_DelRiskFactorData]    Script Date: 4/19/2021 6:28:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER Procedure [dbo].[BSA_DelRiskFactorData] ( 
					@cust ObjectID, @oper SCode, @ts timestamp )

	  As
 
	-- Start standard stored procedure transaction header
	declare @trnCnt int
	declare @strNotes varchar(6000), @tmpNotes varchar(6000)   --20200828 ZA request onboard remarks add to customer Notes

	select @trnCnt = @@trancount	-- Save the current trancount
	If @trnCnt = 0
		-- Transaction has not begun
		begin tran BSA_DelRiskFactorData
	else
		-- Already in a transaction
		save tran BSA_DelRiskFactorData
	-- End standard stored procedure transaction header
  
	declare	@stat 	int,
			@cnt 	int,
			@txt	varchar(200)

	If Exists ( Select * From RiskFactorData 
				 Where cust = @cust And Status = 0) begin
        --update 2020.07 update for ZA override policy start
		--Delete RiskFactorData 
		 --Where cust = @cust And Status = 0
		 -- Not using timestamp (ts) here because deleting 
		 -- multiple records
			
		--Select @stat = @@error , @cnt = @@rowcount 

		--20200828 ZA request Onboard remarks add to Customer Notes,start
		Select @tmpNotes='', @strNotes=''
		Select @tmpNotes = Notes from customer where id=@cust
		Select @strNotes = @tmpNotes + char(13) + @oper + ', ' + convert(varchar(20),getdate(),111) + ' ' + left(convert(varchar(20),getdate(),114),8) + ' , ' + 'Deleted RiskScores & Reject to Operator.' 
		--20200828 ZA request Onboard remarks add to Customer Notes,end

		--202007xx ZA Delete change to Reject and reback to operator
		--202007xx so cancel delete riskscorehist data
		--Delete proposed record from the Risk Score History
		--delete from RiskScoreHist
		--where CustomerId = @cust and DateAccepted is null

		update customer set KYCOperator = (select CreateOper from RiskScoreHist where DateAccepted is null and customerid = @cust), KYCStatus = 6, Notes = @strNotes
		where id = @cust

		update RiskScoreHist set RiskScore = 0, CreateDate = getdate(), AcceptOper = @oper
		where CustomerId = @cust and DateAccepted is null

		
		--update 2020.07 update for ZA override policy End

	end else 
		Select @stat = 250006	-- Object does not exist
	If ( @stat = 0 and @cnt = 0 )
		select @stat = 250001 -- concurent update
	If ( @stat <> 0 ) begin 
		rollback tran BSA_DelRiskFactorData
		return @stat
	end	
	Select @txt = 'Deleted Data/Scores for Customer ' + @cust
	--2020/3/18 mask and change --Exec BSA_InsEvent @oper, 'Del', 'RFData', 0, @txt
	--2020/3/18 change put @cust to objectid
	Exec BSA_InsEvent @oper, 'Del', 'RFData', @cust, @txt

	If @trnCnt = 0
		commit tran BSA_DelRiskFactorData
	return @stat
GO


