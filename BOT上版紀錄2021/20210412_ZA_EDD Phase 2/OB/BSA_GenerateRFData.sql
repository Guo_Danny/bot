USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_GenerateRFData]    Script Date: 12/21/2020 10:05:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO



-- Do not add encryption to this stored procedure. There is a big performance 
-- hit
ALTER Procedure [dbo].[BSA_GenerateRFData]( @filter Int, @filterData varchar(50), 
					@aggType Int, @Oper Scode, 
					@custs Int Output, @recs BigInt Output, @errs Int Output)
As
Begin
	Set Nocount On
	Declare			
		@sSql nVarChar(4000),
		@stat int,
		@txt varchar(300),
		@RFICode Varchar(35),
		@MinRFICode varchar(35),
		@CalcItemID Int,
		@Cust Varchar(35), @iNewCust int,
		@MinCust Varchar(35),
		@Score Int, @ProposedRisk varchar(35),
		@sp Varchar(50),
		@Val Varchar(1000),
		@optAutoAcceptSame bit,
		@custsAccepted Int,
		@custsSkipped INT, 
		@errsAccepted Int

	-- to perform in batches of 100,000 records (Cust + RFTCode + RFICode Combination
	Declare @LastCust varchar(35)
	Declare @LastRFTCode Varchar(11)
	Declare @LastRFICode Varchar(11) 
	Declare @RecExists Bit 
	Declare @RowsSel Int
	Declare @MinCalcItemID Int

	-- to store the list of customers to be processed
	Create Table #CustList
	(
		Cust varchar(35), 
		RFTCode VARCHAR(11), 
		RFICode VARCHAR(11), 
		Required Bit,
		Weight Int,
		DefScore Int,
		Source Int,
		SourceTable Varchar(50),
		SourceField Varchar(50),
		CalcItemID Int,
		OverRideITem Bit,
		CalculationType Char(3),
		Value Varchar(1000) Null,
		ScoreType Int Null,
		Score Decimal(18,2) Null
	)
	
	-- 2008 START  Recommended change
	Create Nonclustered Index IdxCustList on #CustList (cust, RFiCOde)
	-- 2008 END  Recommended change

	-- To store the values for each customer 
	Create Table #CustVal
	(
		Cust Varchar(35),
		RFICode Varchar(11),
		Value Varchar(1000),
		CalculationType Char(3) Null,
		AccountID Varchar(35)
	)

	Set @LastCust = ''
	Set @LastRFTCode = ''
	Set @LastRFICode = '' 
	Set @Errs = 0
	Set @custs = 0
	Set @recs = 0
	Set @recExists = 1
	Set @RowsSel = 0
	--Filter Types
	-- 0 ALL
	-- 1 For Customer Type
	-- 2 For Given Customer
	-- 3 For All New Customers or Recently modified customers
	-- 4 For All Customers created within a date range
	-- 5 For a Given customer via BSA RISK Score Webservice 

	select @txt = 'Risk scores generation started on ' + CONVERT(VARCHAR, GETDATE())
	--With filter 3, If the review date is less than the create date, 
	--that means that items have been generated after model has been accepted.
	--if modify date is greater than the latest create date, that means a customer has
	--been modified after creation of items, so generate new items
	--If the lastreview is greater than the create, that means that no items
	--have been generated after the acceptance of the last risk.  If the lastmodify
	-- date is greater than the last review, that means the customer has been
	-- modified, so generate new items.
	--The first section of the query retrieves the modified customers and the second section
	--retrieves the new customers.

	-- for testing
	Select @txt = 'Start Risk Scoring with @Filter = ' + cast(@Filter as varchar) + ' And @Filterdata = ' + @filterdata
	Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', @txt

	While @RecExists = 1
	Begin
		If @filter <> 3 
			BEGIN
			-- To retrieve all the customer records satisfying the 
			-- selected criterion
			If @filter = 5  Begin
			--  'BSA RIsk Score webservice'
			Insert #CustList
                        (Cust, RFTCode, RFICode, Required, Weight,
                        DefScore, Source, SourceTable, SourceField,
                        CalcItemID, OverRideITem, CalculationType, scoreType)
								Select @filterData, rfl.RFTCode,
                                rfi.Code, rfi.Required, rfi.Weight, 
                                rfi.DefScore, rfi.Source, 
                                rfi.SourceTable, rfi.SourceField, 
                                rfi.CalcItemId, rfl.OverRideItem,
                                rfi.CalculationType, 1 ScoreType
                            From 
                                (
                                Select    
                                                [RTRID], Type, CreateDate,RFTCode
                                From
                                                dbo.RTRCustomer c with (NOLOCK)
                                Where
                                                [RTRID]= @filterData and Closed <> 1
                                ) c
                            Inner Join
                                            dbo.CustomerType ct with(NOLOCK)
                            On
                                            ct.Code = c.Type
                            Inner Join
                                            dbo.RiskFactorList rfl with (NOLOCK)
                            On
                                            rfl.RFTCode = c.rftcode
                            Inner Join
                                            dbo.RiskFactorItem rfi with(NOLOCK)
                            On
                                            rfi.Code = rfl.RFICode
                        Where 
							(c.[RTRID] > @LastCust 
							Or
							(c.[RTRID] = @LastCust And rfl.RFTCode> @LastRFTCode 
							And rfi.Code >= @LastRFICode)
							)
							Order By rfl.RFTCode, rfi.Code
                                                               
                                 
                                     
                Select @RowsSel = @@RowCount
                                        
                Select @recExists = Case When @RowsSel = 0 then 0 Else 1 End
            end 
			
			If @filter = 2  Begin
				Insert #CustList
				(Cust, RFTCode, RFICode, Required, Weight,
				DefScore, Source, SourceTable, SourceField,
				CalcItemID, OverRideITem, CalculationType, scoreType)
				Select @filterData, rfl.RFTCode,
					rfi.Code, rfi.Required, rfi.Weight, 
					rfi.DefScore, rfi.Source, 
					rfi.SourceTable, rfi.SourceField, 
					rfi.CalcItemId, rfl.OverRideItem,
					rfi.CalculationType, 1 ScoreType
				From 
					(
					Select	
						[id], Type, CreateDate
					From
						dbo.Customer c WITH(NOLOCK)
					Where
						[Id]= @filterData and Closed <> 1
					) c
				Inner Join
					dbo.CustomerType ct WITH(NOLOCK)
				On
					ct.Code = c.Type
				Inner Join
					dbo.RiskFactorList rfl WITH(NOLOCK)
				On
					rfl.RFTCode = ct.RFTCode
				Inner Join
					dbo.RiskFactorItem rfi WITH(NOLOCK)
				On
					rfi.Code = rfl.RFICode
				Where 
					(c.[id] > @LastCust 
					Or
					(c.[id] = @LastCust And rfl.RFTCode> @LastRFTCode 
					And rfi.Code >= @LastRFICode)
					)
				Order By rfl.RFTCode, rfi.Code
				Select @RowsSel = @@RowCount
				Select @recExists = Case When @RowsSel = 0 then 0 Else 1 End
		
			End 
			Else
			Begin
				if  @filter<>5
				begin  
				
						Insert #CustList
						(Cust, RFTCode, RFICode, Required, Weight,
						DefScore, Source, SourceTable, SourceField,
						CalcItemID, OverRideITem, CalculationType, scoreType)
						Select Top 5000000
							c.[ID], rfl.RFTCode,
							rfi.Code, rfi.Required, rfi.Weight, 
							rfi.DefScore, rfi.Source, 
							rfi.SourceTable, rfi.SourceField, 
							rfi.CalcItemId, rfl.OverRideItem,
							rfi.CalculationType, 1 ScoreType
						From 
							(
							Select	
								[id], Type, CreateDate
							From
								dbo.Customer c with(NOLOCK)
							Where
								RFCalculate = 1 and Closed <> 1
							) c
						Inner Join
							dbo.CustomerType ct with(NOLOCK)
						On
							ct.Code = c.Type
						Inner Join
							dbo.RiskFactorList rfl with(NOLOCK)
						On
							rfl.RFTCode = ct.RFTCode
						Inner Join
							dbo.RiskFactorItem rfi with (NOLOCK)
						On
							rfi.Code = rfl.RFICode
						Where 
							(c.[id] > @LastCust 
							Or
							(c.[id] = @LastCust And rfl.RFTCode> @LastRFTCode 
							And rfi.Code >= @LastRFICode)
							)
							And
							1 = Case 
							When @filter = 0 Then 1	-- All customers						
							When @filter = 1 And c.Type = @filterData Then 1 -- For Customer Type
							When @filter = 2 And c.[ID] = @filterData Then 1 -- For One Customer	
							When @filter = 4 And
								c.CreateDate >= LEFT(@filterData, CharIndex( '~', @filterData)-1)  And
								c.CreateDate <= RIGHT(@filterData, Len(@filterData) - CharIndex( '~', @filterData))  
							Then 1	-- For date/time range
							Else 0
							End 
						Order By c.ID, rfl.RFTCode, rfi.Code
						
						Select @RowsSel = @@RowCount
						Select @recExists = Case When @RowsSel = 0 then 0 Else 1 End
				End
			End
		End
		Else
			Begin
			-- select the new and recently modified customers
		
			Insert #CustList
			(Cust, RFTCode, RFICode, Required, 
			Weight,DefScore, Source, 
			SourceTable, SourceField, CalcItemID, 
			OverRideItem, CalculationType,ScoreType)
			Select Top 5000000 * 
			From
			(
			Select 
				c.[ID], rfl.RFTCode,rfi.Code, rfi.Required, 
				rfi.Weight, rfi.DefScore, rfi.Source, 
				rfi.SourceTable, rfi.SourceField, rfi.CalcItemId, 
				rfl.OverRideItem, rfi.CalculationType, 1 ScoreType
			From 
				(
				Select
					[id],
					Type,
					LastModify
				From
					dbo.Customer  (noLock)
				Where
					RFCalculate = 1 and Closed <> 1
				) C
			Inner Join
				dbo.CustomerType ct (noLock)
			On
				ct.Code = c.Type
			Inner Join
				RiskFactorList rfl (noLock)
			On
				rfl.RFTCode = ct.RFTCode	
			Inner Join
				RiskFactorItem rfi (noLock)
			On
				rfi.Code = rfl.RFICode
			Inner Join
				(
				Select 
					Cust,
					Max(CreateDate) CreateDate,
					Max(Coalesce(LastReview, 0)) LastReview
				From
					dbo.RiskFactorData  (nolock)
				Group By Cust
				) r
			On
				r.Cust = c.[id]
			Where
			(
				(r.LastReview < r.CreateDate And r.CreateDate < c.LastModify)
			Or
				(r.LastReview > r.CreateDate And r.LastReview < c.LastModify)
			)
			
			Union All
			
			Select 
				c.[ID], rfl.RFTCode,
				rfi.Code, rfi.Required, rfi.Weight, 
				rfi.DefScore, rfi.Source, 
				rfi.SourceTable, rfi.SourceField, 
				rfi.CalcItemId, rfl.OverRideItem,
				rfi.CalculationType, 1
			From 
				(
				Select
					[id],
					Type
				From
					dbo.Customer  (noLock)
				Where
					RFCalculate = 1 and Closed <> 1
				And
					[id] Not In
					(Select cust From RiskFactorData (nolock))
				) C
			Inner Join
				dbo.CustomerType ct (noLock)
			On
				ct.Code = c.Type
			Inner Join
				dbo.RiskFactorList rfl (noLock)
			On
				rfl.RFTCode = ct.RFTCode
			Inner Join
				dbo.RiskFactorItem rfi (noLock)
			On
				rfi.Code = rfl.RFICode
			) CustList
			Where
			(	CustList.[id] > @LastCust
			Or
				(CustList.[id] = @lastCust And CustList.RFTCode >= @LastRFTCode
				And CustList.Code > @LastRFICode)
			)
			Order by CustList.[id], CustList.RFTCode, CustList.Code

			Select @RowsSel = @@RowCount
			Select @recExists = Case When @RowsSel = 0 then 0 Else 1 End

		End

		If @recExists = 0 
			Goto CompleteScoring

		Select @Lastcust = Max(Cust), @LastRFTCode = Max(RFTCode), @LastRFICode = Max(RFICode)
		From #custList
	
		-- for testing
		Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', 'Extracted the Customer list for Processing'
	
		-- to find the value for the customers for Source = 0
		Select @RFICode=MIN(RFICode) FROM #CustList Where Source = 0
	
		Set @MinRFICode=@RFICode
		While @MinRFICode IS NOT NULL
		Begin
			Set @ssql = ''
			
			---------------------------------------  code for setting up the value based in RTR tables
			if @filter=5 
			begin
		
				Select @ssql =  
			Case When SourceTable = 'Customer' Then
						
				'Update #CustList
				Set Value = Cast(RTRCustomer.' + CustList.SourceField + ' as varchar(1000)), Score = RiskFactorValue.Score
				From Pbsa.dbo.RTRCustomer with (nolock)
				Inner Join #CustList CustList (nolock) On RTRCustomer.[RTRId] = CustList.Cust
				Left Outer Join Pbsa.dbo.RiskFactorValue (nolock) On RiskFactorValue.RFICode = CustList.RFICode 
				And 1 = 
					Case when RiskFactorValue.Score Is Not Null and 
							RiskFactorValue.Value = Cast(RTRCustomer.' + CustList.SourceField + ' As Varchar(2000)) Then  1
					Else
						Case When RiskFactorValue.Value in (''0'',''1'',''False'',''True'') Then
							Case When RiskFactorValue.Value = Cast(RTRCustomer.' +  CustList.SourceField + ' as varchar(2000)) Then 1
							When Convert(VarChar, RTRCustomer.' +  CustList.SourceField + ') = ''0'' And RiskFactorValue.Value = ''False'' Then 1			
							When Convert(VarChar, RTRCustomer.' +  CustList.SourceField + ') = ''1'' And RiskFactorValue.Value = ''True'' Then 1
							Else 0
							End 
						End 
					End 
				Where Source = 0 And CustList.SourceTable = ''Customer''
				 And  CustList.RFICode = ''' + @MinRFICode + '''' 
			When SourceTable = 'KYCData' Then
				'Update #CustList
				Set Value = Cast(RTRKYCData.' + CustList.SourceField + ' as varchar(1000)), Score = RiskFactorValue.Score
				From Pbsa.dbo.RTRKYCData with(nolock)
				Inner Join #CustList CustList (nolock) On RTRKYCData.RTRId = CustList.Cust
				Left Outer Join Pbsa.dbo.RiskFactorValue (nolock) On RiskFactorValue.RFICode = CustList.RFICode 
				And 1 = 
					Case when RiskFactorValue.Score Is Not Null and 
							RiskFactorValue.Value = Cast(RTRKYCData.' + CustList.SourceField + ' As Varchar(2000)) Then  1
					Else
						Case When RiskFactorValue.Value in (''0'',''1'',''False'',''True'') Then
							Case When RiskFactorValue.Value = Cast(RTRKYCData.' +  CustList.SourceField + ' as varchar(2000)) Then 1
							When Convert(VarChar, RTRKYCData.' +  CustList.SourceField + ') = ''0'' And RiskFactorValue.Value = ''False'' Then 1			
							When Convert(VarChar, RTRKYCData.' +  CustList.SourceField + ') = ''1'' And RiskFactorValue.Value = ''True'' Then 1
							Else 0
							End 
						End 
					End 
				Where Source = 0 And CustList.SourceTable = ''KYCData''
				 And  CustList.RFICode = ''' + @MinRFICode + '''' 				
			When SourceTable = 'AccountType' Then
				'Insert #CustVal
				Select RTRAccountOwner.Cust, CustList.RFICode, AccountType.' + CustList.SourceField + ', CustList.CalculationType, NULL From
						Pbsa.dbo.RTRAccountOwner (nolock) Inner Join Pbsa.dbo.RTRAccount (nolock) On RTRAccount.RtrId = RTRAccountOwner.Account  and ISNULL(RTRAccount.Closed,0) <> 1
				Inner Join Pbsa.dbo.AccountType(nolock) On AccountType.Code = RTRAccount.Type
				Inner Join Pbsa.dbo.RelationshipType(nolock) on RelationshipType.Code = RTRAccountOwner.Relationship
				Inner Join #CustList CustList (nolock) On RTRAccountOwner.Cust = CustList.Cust
				Where CustList.Source = 0 And RelationshipType.IncludeInRisk = 1 And CustList.SourceTable = ''AccountType''
				And CustList.RFICode = ''' + @MinRFICode + ''''
				
	
				
			When SourceTable = 'Account' Then
				'Insert #CustVal
				Select RTRAccountOwner.Cust, CustList.RFICode, RTRAccount.' + CustList.SourceField + ', CustList.CalculationType, NULL From
					Pbsa.dbo.RTRAccountOwner (nolock) Inner Join Pbsa.dbo.RTRAccount (nolock) On RTRAccount.RtrId = RTRAccountOwner.Account  and ISNULL(RTRAccount.Closed,0) <> 1
				Inner Join Pbsa.dbo.RelationshipType(nolock) on RelationshipType.Code = RTRAccountOwner.Relationship
				Inner Join #CustList CustList (nolock) On RTRAccountOwner.Cust = CustList.Cust
				Where CustList.Source = 0 And RelationshipType.IncludeInRisk = 1 And CustList.SourceTable = ''Account''
				And CustList.RFICode = ''' + @MinRFICode + ''''
			End
			
			from 
				#CustList CustList (nolock)
			Where 
				Source = 0 
			And
				RFICode = @MinRFICode

			end
			
			---web service update end
			else 
			begin
			Select @ssql =  
			Case When SourceTable = 'Customer' Then
						
				'Update #CustList
				Set Value = Cast(Customer.' + CustList.SourceField + ' as varchar(1000)), Score = RiskFactorValue.Score
				From Pbsa.dbo.Customer (nolock)
				Inner Join #CustList CustList (nolock) On Customer.[id] = CustList.Cust
				Left Outer Join Pbsa.dbo.RiskFactorValue (nolock) On RiskFactorValue.RFICode = CustList.RFICode 
				And 1 = 
					Case when RiskFactorValue.Score Is Not Null and 
							RiskFactorValue.Value = Cast(Customer.' + CustList.SourceField + ' As Varchar(2000)) Then  1
					Else
						Case When RiskFactorValue.Value in (''0'',''1'',''False'',''True'') Then
							Case When RiskFactorValue.Value = Cast(Customer.' +  CustList.SourceField + ' as varchar(2000)) Then 1
							When Convert(VarChar, Customer.' +  CustList.SourceField + ') = ''0'' And RiskFactorValue.Value = ''False'' Then 1			
							When Convert(VarChar, Customer.' +  CustList.SourceField + ') = ''1'' And RiskFactorValue.Value = ''True'' Then 1
							Else 0
							End 
						End 
					End 
				Where Source = 0 And CustList.SourceTable = ''Customer''
				 And  CustList.RFICode = ''' + @MinRFICode + '''' 
			When SourceTable = 'KYCData' Then
				'Update #CustList
				Set Value = Cast(KYCData.' + CustList.SourceField + ' as varchar(1000)), Score = RiskFactorValue.Score
				From Pbsa.dbo.KYCData (nolock)
				Inner Join #CustList CustList (nolock) On KYCData.customerid = CustList.Cust
				Left Outer Join Pbsa.dbo.RiskFactorValue (nolock) On RiskFactorValue.RFICode = CustList.RFICode 
				And 1 = 
					Case when RiskFactorValue.Score Is Not Null and 
							RiskFactorValue.Value = Cast(KYCData.' + CustList.SourceField + ' As Varchar(2000)) Then  1
					Else
						Case When RiskFactorValue.Value in (''0'',''1'',''False'',''True'') Then
							Case When RiskFactorValue.Value = Cast(KYCData.' +  CustList.SourceField + ' as varchar(2000)) Then 1
							When Convert(VarChar, KYCData.' +  CustList.SourceField + ') = ''0'' And RiskFactorValue.Value = ''False'' Then 1			
							When Convert(VarChar, KYCData.' +  CustList.SourceField + ') = ''1'' And RiskFactorValue.Value = ''True'' Then 1
							Else 0
							End 
						End 
					End 
				Where Source = 0 And CustList.SourceTable = ''KYCData''
				 And  CustList.RFICode = ''' + @MinRFICode + '''' 				
			When SourceTable = 'AccountType' Then
				'Insert #CustVal
				Select AccountOwner.Cust, CustList.RFICode, AccountType.' + CustList.SourceField + ', CustList.CalculationType, NULL From
					Pbsa.dbo.AccountOwner (nolock) Inner Join Pbsa.dbo.Account (nolock) On Account.ID = AccountOwner.Account  and Account.Closed <> 1
				Inner Join Pbsa.dbo.AccountType(nolock) On AccountType.Code = Account.Type
				Inner Join Pbsa.dbo.RelationshipType(nolock) on RelationshipType.Code = AccountOwner.Relationship
				Inner Join #CustList CustList (nolock) On AccountOwner.Cust = CustList.Cust
				Where CustList.Source = 0 And RelationshipType.IncludeInRisk = 1 And CustList.SourceTable = ''AccountType''
				And CustList.RFICode = ''' + @MinRFICode + ''''
			--CD08/25/2014 Add NULL as last item in AccountType statement above. Add entire block below.
			When SourceTable = 'Account' Then
				'Insert #CustVal
				Select AccountOwner.Cust, CustList.RFICode, Account.' + CustList.SourceField + ', CustList.CalculationType, Account.Id From
					Pbsa.dbo.AccountOwner (nolock) Inner Join Pbsa.dbo.Account (nolock) On Account.ID = AccountOwner.Account  and Account.Closed <> 1
				Inner Join Pbsa.dbo.RelationshipType(nolock) on RelationshipType.Code = AccountOwner.Relationship
				Inner Join #CustList CustList (nolock) On AccountOwner.Cust = CustList.Cust
				Where CustList.Source = 0 And RelationshipType.IncludeInRisk = 1 And CustList.SourceTable = ''Account''
				And CustList.RFICode = ''' + @MinRFICode + ''''
			End
			from 
				#CustList CustList (nolock)
			Where 
				Source = 0 
			And
				RFICode = @MinRFICode
		End
			Exec sp_Executesql @ssql
	
			Set @stat = @@Error
			If @stat <> 0 Goto ErrHandler
	
			goto GetNext
	
	ErrHandler:
			Select @errs = @errs + 1
			Select @txt = 'Error_GenerateRFData: #' + cast(@stat as varchar) + 
				' while Generating RF Data record for ' + 
				'Customer: ' + @cust + 
				', RFItem: ' + @MinRFICode 
			Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', @txt
	
	GetNext:
		-- for testing
		Set @txt = 'Processed RFICode ' + @MinRFICode 
		Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', @txt
			Select @RFICode=MIN(RFICode) FROM #CustList (nolock) WHERE 
				 Source = 0 And RFICode > @MinRFICode
			Set @MinRFICode = @RFICode
		END
	
		-- for testing
		Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', 'Populated CustVal'
	
		-- update the scores for the customers (source - 0 for aaccounttype risk items)
		Update #CustList
		Set Value = CustVal.Value, 
			Score =  CustVal.Score		
		From 
			#CustList CustList (nolock)
		Inner Join
			(
			Select Val.Cust, Val.RFICode,
				Case 
				When Val.CalculationType = 'Avg' Then
					Avg(RiskFactorValue.Score)
				When Val.CalculationType = 'Min' Then
					Min(RiskFactorValue.Score)
				When Val.CalculationType = 'Max' Then
					Max(RiskFactorValue.Score)
				When Val.CalculationType = 'Sum' Then
					Sum(RiskFactorValue.Score)
				End Score,
				Case When count(*) > 1 Then
					'Multivalued'
				Else
					--For Account fields include the AccountId in parentheses. Format="{Value} (AcctId:{AccountId})" 
					Min(RiskFactorValue.Value) + ISNULL(' (AcctId:'+MIN(Val.AccountID) + ')', '')
				End Value
			From	
				#CustVal Val (nolock)
			Inner Join
				dbo.RiskFactorValue (nolock)
			On
				RiskFactorValue.RFICode = Val.RFICode
			And 
				RiskFactorValue.Value = Val.Value
			Group By Val.Cust, Val.RFICode, Val.CalculationType
			) CustVal
		On
			CustList.Cust = CustVal.Cust 
		And
			CustList.RFICode = CustVal.RFICode
		Where
			CustList.Source = 0
		And
			--CD08/25/2014 Add Account table to this condition
			--CustList.SourceTable = 'AccountType'
			CustList.SourceTable IN ('AccountType', 'Account')

		--For Account risk items if CalculationType is Min or Max and Value field shows Multivalued at this point
		--Update the Value to show the actual value and the Account Id in parentheses that was used to pull the Value from
		IF EXISTS (SELECT TOP 1 * FROM #CustList WHERE SourceTable='Account' and CalculationType in ('Max', 'Min') and [Value]='Multivalued' and [Source]=0)
		BEGIN
			UPDATE cl
				SET Value = cv.Value + ISNULL(' (AcctId:'+cv.AccountID + ')', '')
			FROM 
				#custlist cl
			INNER JOIN
				#custval cv (NOLOCK)
			ON
				cl.Cust = cv.Cust
				And cl.RFICode = cv.RFICode
			INNER JOIN
				RiskFactorValue (NOLOCK)
			ON
				RiskFactorValue.RFICode = cv.RFICode
				And RiskFactorValue.Value = cv.Value
				And RiskFactorValue.Score = cl.Score
			WHERE
				cl.Source = 0
				and 
				cl.SourceTable='Account'
				and
				cl.CalculationType in ('Max', 'Min')
				and
				cl.Value='Multivalued'
		END
	
		-- for testing
		Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', 'Filled up values and scores for Source = 0'
			
	            -- loop through calculated items
		    Declare CalcItemCursor Cursor 
		    FAST_FORWARD
		    For 
		      Select distinct RFICOde, CalcItemId  From #CustList (nolock) where source =1
	
		    Declare @spParamCnt int
	
		    Open CalcItemCursor
		    Fetch Next From CalcItemCursor into @RFICode, @calcItemId
		    While (@@FETCH_STATUS <> -1) Begin
			Select 
				@sp = ProcName
			From 
				RFCalculatedItem
			Where 
				Id = @calcItemId
			
			Select @spParamCnt = 0
			select @spParamCnt = count(*)  from sysobjects o, syscolumns c
				where o.xtype='P' and o.id = c.id 
					and o.name = @sp
			
			if @spParamCnt = 1 Begin
				-- for each item call exec
				Execute @sp @RFICode
			End Else 
			Begin
				-- For existing calculated items with 3 parameters
				-- call the stored procedure for each customer
				Select @Cust=MIN(Cust) FROM #CustList (nolock) Where Source = 1
					and RFICode = @RFICode
				Set @MinCust = @Cust
				While @MinCust IS NOT NULL
				Begin
					Execute @sp @Cust, @val Output, @Score Output
					Select @stat = @@Error
					If @stat <> 0 Goto HandleErr
	
					goto GetNextCust
HandleErr:
					Select @errs = @errs + 1
					Select @txt = 'Error_GenerateRFData: #' + cast(@stat as varchar) + 
						' while Generating RF Data record for ' + 
						'Customer: ' + @Mincust + 
						', Calculated Item ID: ' + cast(@calcItemId as varchar)
					Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', @txt
	
GetNextCust:
					-- update the Score and value for source = 1
					Update #CustList
						Set Score = @Score, Value = @Val
					Where Cust = @MinCust
						And  CalcItemID = @calcItemId
					And Source = 1 and RFICode = @RFICode
	
					Select @Cust=MIN(Cust) FROM #CustList WHERE 
					 Source = 1 And (Cust > @MinCust) and RFICode = @RFICode
					Set @MinCust = @Cust
				END
			End
		
			Fetch Next From CalcItemCursor into @RFICode, @calcItemId	
		    End
		    Close CalcItemCursor
		    Deallocate CalcItemCursor


		-- in calc item, update custlist with value and score based on calcitemid
		
	
		-- for testing
		Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', 'Processed calculated items'
	
		-- set the score and scoretype for Source = 2
		Update #CustList
		Set 
			Score = IsNull(DefScore,0),
			ScoreType = Case When DefScore Is Null Then 0 Else 2 End,
			Value = Null
		Where
			Source = 2
		
		--  If Score is not present, check if the default score exists. 
		-- If default score exists, set the Score to default score
		Update #CustList
		Set 
			Score = IsNull(DefScore,0),
			ScoreType = Case When DefScore Is Null Then 0 Else 2 End
		Where
			Score Is Null
	
	
		-- delete those records with OverrideItem = 1 and without a score
		Delete #CustList
		Where OverrideItem = 1 And Score <= 0
	
		-- Records with Overrideitem overrides the other records
		-- for a customer. So delete those records with Override = 0
		-- if the customer has any other record with override = 1
	
		Delete  #CustList
		From
			#CustList (nolock)
		Inner Join
			#CustList OverRideRisk (nolock)
		On
			#CustList.Cust =  OverRideRisk.Cust
		And
			#CustList.RFTCode = OverRideRisk.RFTCode
		Where 
			#CustList.OverRideItem = 0
		And 
			OverRideRisk.OverrideItem = 1
	
		-- for testing
		Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', 'completed the modifications to data in temp table'
		
		-- Delete existing records from RiskFactorData for the 
		-- Customers under consideration
		DELETE dbo.RiskFactorData 
		From 
			dbo.RiskFactorData (nolock)
		Inner join
			#CustList custList
		On
			RiskFactorData.Cust = CustList.Cust
		WHERE (RiskFactorData.Status is Null 
		Or RiskFactorData.Status = 0)
		
	--for risk score web service 
	if @filter=5 begin
	DELETE dbo.RTRRiskFactorData 
		From 
			dbo.RTRRiskFactorData (nolock)
		Inner join
			#CustList custList
		On
			RTRRiskFactorData.Cust = CustList.Cust
		WHERE (RTRRiskFactorData.Status is Null 
		Or RTRRiskFactorData.Status = 0)
	
			INSERT INTO dbo.RTRRiskFactorData (Cust, RFTCode, RFICode, 
			[Value], SCore, ScoreType, CreateOper, CreateDate)
		SELECT Cust, RFTCode, RFICode, [Value], SCore, ScoreType, 
			@Oper, GetDate()
		FROM 
			#CustList (nolock)
			Select @recs = @@ROWCOUNT + @recs
			
			
		DECLARE @riskitem VARCHAR(200)  
		DECLARE @riskModel varchar(11)
		SELECT @riskitem = COALESCE(@riskitem + ', ', '') + rficode FROM RTRRiskFactorData where cust=@filterData
		SELECT @riskitem

		DECLARE @indivScore varchar(100)
		SELECT @indivScore = COALESCE( @indivScore  + ', ', ''  )+ cast(Score as varchar) FROM RTRRiskFactorData where cust=@filterData
		SELECT @indivScore
		
		-- getting the correct risk model value based on the customer(existing or new customer) passed through request, to display in the PCS BSA events.
		Select  @riskModel= RFTCode FROM dbo.RTRRiskFactorData with (nolock) where cust=@filterData

		-- set @txt= 'The  risk item(s) ' + @riskitem +  ' and its(their) corresponding risk score value(s) ' + @indivScore + ' are used to calculate the Risk Score.'
		set @txt= 'The Risk Model ' + @riskModel + ' and risk item(s) ' + @riskitem +  ' and its(their) corresponding risk score value(s) ' + @indivScore + ' are used to calculate the Risk Score.'
		Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', @txt
	end 
	else  -- moved the code here , because if @filter=5 then no need to insert into RiskFactorData
	begin
		-- insert the values to RiskFactorData
		INSERT INTO dbo.RiskFactorData (Cust, RFTCode, RFICode, 
			[Value], SCore, ScoreType, CreateOper, CreateDate)
		SELECT Cust, RFTCode, RFICode, [Value], SCore, ScoreType, 
			@Oper, GetDate()
		FROM 
			#CustList (nolock)
		
		Select @recs = @@ROWCOUNT + @recs
	
	end
	
        ------------------------------------------------------
		--Add proposed risk scores to the
		-- Risk Score History table
		------------------------------------------------------
		
		--Clear pending previously proposed scores
		delete from RiskScoreHist
		where DateAccepted is null and
			  CustomerId in (select distinct cust
							 From #CustList (nolock))


		--Get EDD Option, Maximum number of
		-- Risk Score History records per customer
		select * from pcdb..CustomSettings where  product='EDD'
		Declare @MaxHistRecords int
		select @MaxHistRecords = Convert(int, dbo.BSA_fnGetEDDOption('MaxRSHist'))



		--Clear old history scores, for customers where
		--MaxRSHist will be exceeded
		delete from [RiskScoreHist]
		where ScoreId in 
		(Select ScoreID  FROM [RiskScoreHist] AllHist
		inner join
			(SELECT [CustomerId],      
			min([CreateDate]) OldestCreateDate   
			FROM [PBSA].[dbo].[RiskScoreHist]
			where CustomerId in (select distinct cust
							 From #CustList (nolock))
			Group by CustomerId
			having count(*) >= @MaxHistRecords) OldHist
		on AllHist.CustomerId = OldHist.CustomerId and
		AllHist.CreateDate = OldHist.OldestCreateDate )

		
		--Insert new proposed scores
		insert into RiskScoreHist
			(CustomerID, RiskScore, RiskClass, 
			  CreateOper, CreateDate, AcceptOper, DateAccepted)
		SELECT [Cust], [Score], [ProposedRiskClass],	
				@Oper, GetDate(), NULL, NULL					
		FROM [CustProposedRiskClass]
		where Cust in (select distinct cust
							 From #CustList (nolock))

		Select @Custs = @custs + Count(Distinct Cust) From #CustList

		select @txt = 'Successfully completed Risk scores generation on ' + 
					CONVERT(VARCHAR, GETDATE()) + '.' + 
					  char(13) + char(10) +
					  '      Total Records: ' + Cast(@recs as varchar) +
					  char(13) + char(10) +
					  '  Total Customers: ' + Cast(@custs as varchar) +
					  char(13) + char(10) +
					  '         Total Errors: ' + Cast(@errs as varchar)
		Exec BSA_InsEvent 'Prime', 'Cre', 'RFData', '', @txt
		truncate table #CustList
		
		-- Autoaccept if riskclasses are unchanged
		Select @optAutoAcceptSame = enabled from OptionTbl where Code = 'RFAcptSame'
		If IsNull(@optAutoAcceptSame, 0) = 1
		 exec BSA_AcceptAllRFScores @oper, 1, @custsAccepted output, @custsSkipped output, @errsAccepted output
		
	End	

	

CompleteScoring:
		
		--2020-11-23 add for South Africa Request, Start
		--1. new Customer and new risk in (HighRisk, DeRisk) --> select code from riskclass where AutoAcceptAmountThreshold in (2)
		--2. New Customer of next review date = Accept Date + 12 Months (from riskclass) riskfactor
		----declare @ProposeRisk varchar(35), @iNewCust int, @filterData varchar(35)
		select @ProposedRisk='', @iNewCust=0   --, @filterData='0161503666'
		select @ProposedRisk=ProposedRiskClass from CustProposedRiskClass where cust=@filterData
		
		--check new customer
		select @iNewCust=1 from Customer where (ProbationStartDate is null or ProbationStartDate ='19000101' or isnull(ProbationStartDate,'')='' ) 
		       and datediff(day,KYCDataCreateDate,getdate()) <10 and id=@filterData

/* 20201208 mask for South Africa request change: provide check lists form so did not update CDDuser40, Start		
		--20201208 mask for South Africa request change: provide check lists form so did not update CDDuser40, Start
		--if new customer and proposedrisk in High risk or derisk update EDD fields D.G.M, C.C.O, G.M.
		if @iNewcust=1 and (@ProposedRisk= 'HighRisk' or @ProposedRisk='DeRisk')
		begin
			update KYCData SET cdduser39=@ProposedRisk,CDDUser40='Yes' where CustomerId=@filterData   
			update KYCData SET cdduser39=@ProposedRisk where CustomerId=@filterData
		end
		----20201208 mask for South Africa request change: provide check lists form so did not update CDDuser40, End
*/
		----select @ProposedRisk, @iNewcust, @filterData
		----select CDDuser32, cdduser33,cdduser38, cdduser39,cdduser40  from KYCData where customerid=@filterData

		--2020-11-23 add for South Africa Request, End	

		----20201127 polly add for onboarding add new field of NextReviewDate, Start
		declare @Mint int, @RvDt datetime, @IssueCty varchar(100)
		
		----same withe Store procedure: BSA_ReviewRFData, BSA_RejectRiskFactorData
		--add get riskclass of Probation days
		----add for South Africa Request: new cust lastreview must be 12 months,start			

		if @iNewCust=1
		   select @Mint=RiskFactor from RiskClass where code=@ProposedRisk
		else 
		   select @Mint=probationdays from RiskClass where code=@ProposedRisk

		--add for partyid exist and expirdate is closed, start
		select @RvDt='', @IssueCty=''
		select @RvDt=convert(date,min(ExpiryDate),111) from PartyID 
		       where CustomerId=@filterData
			   and ExpiryDate>=convert(date,GETDATE(),111)
			   and ExpiryDate<convert(date,dateadd(MONTH,@Mint,GETDATE()),111)
 		
		----if issue contry= ZA, TW then expirydate=29991231
		--select @IssueCty=IssueCountry from PartyID 
		--       where CustomerId=@filterData and IssueCountry in ('TW','ZA')
		--	   group by IssueCountry
		
		--if ( @RvDt ='' or @RvDt is null ) or (@IssueCty='ZA' or @IssueCty='TW' or @IssueCty='' or @IssueCty is null)   
		if ( @RvDt ='' OR ISNULL(@RvDt,'')=''  ) 
		   select @RvDt=convert(date,convert(char(10),dateadd(MONTH,@Mint,GETDATE()),101),101)
  		--add for partyid exist and expirdate is closed, end	

		update KYCData SET CDDUser38=convert(varchar(20),@RvDt,101), CDDUser39=@ProposedRisk, LastOper = @Oper where CustomerId=@filterData
		update Customer SET ReviewDate=convert(datetime,@RvDt,111), LastOper = @Oper where Id=@filterData
		----20201127 polly add for onboarding add new field of LastReviewDate, End

	-- for testing
	Exec BSA_InsEvent 'Prime', 'Ann', 'RFData', '', 'Completed Risk Score generation'
	Drop table #CustList
	Drop Table #CustVal

	Return 0
End
GO

