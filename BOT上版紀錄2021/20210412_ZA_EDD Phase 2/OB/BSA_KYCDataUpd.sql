USE [PBSA]
GO

/****** Object:  Trigger [dbo].[BSA_KYCDataUpd]    Script Date: 8/24/2021 6:13:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[BSA_KYCDataUpd] on [dbo].[KYCData]   For Update 
As

  Declare	@customerid ObjectId, 
		@oper SCode
		
  If @@rowcount > 0 begin
	Update KYCData set LastModify = getdate()
	  From Inserted I, KYCData P Where I.CustomerId = P.CustomerId 
  	
  Insert Event (TrnTime, Oper, Type, ObjectType, ObjectId , EvtDetail) 
   Select getDate(), i.LastOper, 'Mod', 'Cust', convert(varchar, i.CustomerId), 
	Case 
		WHEN ((i.[CustomerId] Is Null And d.[Customerid] Is Not Null ) Or 
		(i.[Customerid] Is Not Null And d.[Customerid] Is Null ) Or 
		(i.[Customerid] != d.[Customerid])) 
		THEN '| Customerid OLD: ' + Convert(varchar, IsNull(d.[Customerid],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[Customerid],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[NamePrefix] Is Null And d.[NamePrefix] Is Not Null ) Or 
		(i.[NamePrefix] Is Not Null And d.[NamePrefix] Is Null ) Or 
		(i.[NamePrefix] != d.[NamePrefix])) 
		THEN '| NamePrefix OLD: ' + Convert(varchar, IsNull(d.[NamePrefix],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[NamePrefix],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[FirstName] Is Null And d.[FirstName] Is Not Null ) Or 
		(i.[FirstName] Is Not Null And d.[FirstName] Is Null ) Or 
		(i.[FirstName] != d.[FirstName])) 
		THEN '| FirstName OLD: ' + Convert(varchar, IsNull(d.[FirstName],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[FirstName],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[MiddleName] Is Null And d.[MiddleName] Is Not Null ) Or 
		(i.[MiddleName] Is Not Null And d.[MiddleName] Is Null ) Or 
		(i.[MiddleName] != d.[MiddleName])) 
		THEN '| MiddleName OLD: ' + Convert(varchar, IsNull(d.[MiddleName],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MiddleName],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[LastName] Is Null And d.[LastName] Is Not Null ) Or 
		(i.[LastName] Is Not Null And d.[LastName] Is Null ) Or 
		(i.[LastName] != d.[LastName])) 
		THEN '| LastName OLD: ' + Convert(varchar, IsNull(d.[LastName],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[LastName],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[NameSuffix] Is Null And d.[NameSuffix] Is Not Null ) Or 
		(i.[NameSuffix] Is Not Null And d.[NameSuffix] Is Null ) Or 
		(i.[NameSuffix] != d.[NameSuffix])) 
		THEN '| NameSuffix OLD: ' + Convert(varchar, IsNull(d.[NameSuffix],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[NameSuffix],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[Alias] Is Null And d.[Alias] Is Not Null ) Or 
		(i.[Alias] Is Not Null And d.[Alias] Is Null ) Or 
		(i.[Alias] != d.[Alias])) 
		THEN '| Alias OLD: ' + Convert(varchar, IsNull(d.[Alias],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[Alias],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[PlaceofBirth] Is Null And d.[PlaceofBirth] Is Not Null ) Or 
		(i.[PlaceofBirth] Is Not Null And d.[PlaceofBirth] Is Null ) Or 
		(i.[PlaceofBirth] != d.[PlaceofBirth])) 
		THEN '| PlaceofBirth OLD: ' + Convert(varchar, IsNull(d.[PlaceofBirth],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[PlaceofBirth],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[MothersMaidenName] Is Null And d.[MothersMaidenName] Is Not Null ) Or 
		(i.[MothersMaidenName] Is Not Null And d.[MothersMaidenName] Is Null ) Or 
		(i.[MothersMaidenName] != d.[MothersMaidenName])) 
		THEN '| MothersMaidenName OLD: ' + Convert(varchar, IsNull(d.[MothersMaidenName],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MothersMaidenName],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[Introducer] Is Null And d.[Introducer] Is Not Null ) Or 
		(i.[Introducer] Is Not Null And d.[Introducer] Is Null ) Or 
		(i.[Introducer] != d.[Introducer])) 
		THEN '| Introducer OLD: ' + Convert(varchar, IsNull(d.[Introducer],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[Introducer],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[YrsAtCurrAddress] Is Null And d.[YrsAtCurrAddress] Is Not Null ) Or 
		(i.[YrsAtCurrAddress] Is Not Null And d.[YrsAtCurrAddress] Is Null ) Or 
		(i.[YrsAtCurrAddress] != d.[YrsAtCurrAddress])) 
		THEN '| YrsAtCurrAddress OLD: ' + Convert(varchar, IsNull(d.[YrsAtCurrAddress],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[YrsAtCurrAddress],0))
		ELSE '' End +
	Case 
		WHEN ((i.[NoOfDependants] Is Null And d.[NoOfDependants] Is Not Null ) Or 
		(i.[NoOfDependants] Is Not Null And d.[NoOfDependants] Is Null ) Or 
		(i.[NoOfDependants] != d.[NoOfDependants])) 
		THEN '| NoOfDependants OLD: ' + Convert(varchar, IsNull(d.[NoOfDependants],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[NoOfDependants],0))
		ELSE '' End +
	Case 
		WHEN ((i.[EmployerName] Is Null And d.[EmployerName] Is Not Null ) Or 
		(i.[EmployerName] Is Not Null And d.[EmployerName] Is Null ) Or 
		(i.[EmployerName] != d.[EmployerName])) 
		THEN '| EmployerName OLD: ' + Convert(varchar, IsNull(d.[EmployerName],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[EmployerName],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[JobTitle] Is Null And d.[JobTitle] Is Not Null ) Or 
		(i.[JobTitle] Is Not Null And d.[JobTitle] Is Null ) Or 
		(i.[JobTitle] != d.[JobTitle])) 
		THEN '| JobTitle OLD: ' + Convert(varchar, IsNull(d.[JobTitle],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[JobTitle],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[JobTime] Is Null And d.[JobTime] Is Not Null ) Or 
		(i.[JobTime] Is Not Null And d.[JobTime] Is Null ) Or 
		(i.[JobTime] != d.[JobTime])) 
		THEN '| JobTime OLD: ' + Convert(varchar, IsNull(d.[JobTime],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[JobTime],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[LengthOfEmp] Is Null And d.[LengthOfEmp] Is Not Null ) Or 
		(i.[LengthOfEmp] Is Not Null And d.[LengthOfEmp] Is Null ) Or 
		(i.[LengthOfEmp] != d.[LengthOfEmp])) 
		THEN '| LengthOfEmp OLD: ' + Convert(varchar, IsNull(d.[LengthOfEmp],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[LengthOfEmp],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[IsAlien] Is Null And d.[IsAlien] Is Not Null ) Or 
		(i.[IsAlien] Is Not Null And d.[IsAlien] Is Null ) Or 
		(i.[IsAlien] != d.[IsAlien])) 
		THEN '| IsAlien OLD: ' + case when IsNull(d.[IsAlien],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[IsAlien],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[CountryofDualCitizenship] Is Null And d.[CountryofDualCitizenship] Is Not Null ) Or 
		(i.[CountryofDualCitizenship] Is Not Null And d.[CountryofDualCitizenship] Is Null ) Or 
		(i.[CountryofDualCitizenship] != d.[CountryofDualCitizenship])) 
		THEN '| CountryofDualCitizenship OLD: ' + Convert(varchar, IsNull(d.[CountryofDualCitizenship],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CountryofDualCitizenship],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[SecondaryEmployer] Is Null And d.[SecondaryEmployer] Is Not Null ) Or 
		(i.[SecondaryEmployer] Is Not Null And d.[SecondaryEmployer] Is Null ) Or 
		(i.[SecondaryEmployer] != d.[SecondaryEmployer])) 
		THEN '| SecondaryEmployer OLD: ' + Convert(varchar, IsNull(d.[SecondaryEmployer],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[SecondaryEmployer],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[OriginOfFunds] Is Null And d.[OriginOfFunds] Is Not Null ) Or 
		(i.[OriginOfFunds] Is Not Null And d.[OriginOfFunds] Is Null ) Or 
		(i.[OriginOfFunds] != d.[OriginOfFunds])) 
		THEN '| OriginOfFunds OLD: ' + Convert(varchar, IsNull(d.[OriginOfFunds],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[OriginOfFunds],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[IsPersonalRelationship] Is Null And d.[IsPersonalRelationship] Is Not Null ) Or 
		(i.[IsPersonalRelationship] Is Not Null And d.[IsPersonalRelationship] Is Null ) Or 
		(i.[IsPersonalRelationship] != d.[IsPersonalRelationship])) 
		THEN '| Mgr.Override(IsPersonalRelationship) OLD: ' + case when IsNull(d.[IsPersonalRelationship],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[IsPersonalRelationship],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[DateOfInception] Is Null And d.[DateOfInception] Is Not Null ) Or 
		(i.[DateOfInception] Is Not Null And d.[DateOfInception] Is Null ) Or 
		(i.[DateOfInception] != d.[DateOfInception])) 
		THEN '| DateOfInception OLD: ' + case when d.[DateOfInception] is Null then 'Null' else 
		Convert(varchar, d.[DateOfInception]) End + ' NEW: ' + case when i.[DateOfInception] is Null then 'Null' else 
		Convert(varchar, i.[DateOfInception]) End
		ELSE '' End +
	Case 
		WHEN ((i.[LegalEntityType] Is Null And d.[LegalEntityType] Is Not Null ) Or 
		(i.[LegalEntityType] Is Not Null And d.[LegalEntityType] Is Null ) Or 
		(i.[LegalEntityType] != d.[LegalEntityType])) 
		THEN '| LegalEntityType OLD: ' + Convert(varchar, IsNull(d.[LegalEntityType],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[LegalEntityType],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[NoOfEmployees] Is Null And d.[NoOfEmployees] Is Not Null ) Or 
		(i.[NoOfEmployees] Is Not Null And d.[NoOfEmployees] Is Null ) Or 
		(i.[NoOfEmployees] != d.[NoOfEmployees])) 
		THEN '| NoOfEmployees OLD: ' + Convert(varchar, IsNull(d.[NoOfEmployees],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[NoOfEmployees],0))
		ELSE '' End +
	Case 
		WHEN ((i.[TotalAnnualSales] Is Null And d.[TotalAnnualSales] Is Not Null ) Or 
		(i.[TotalAnnualSales] Is Not Null And d.[TotalAnnualSales] Is Null ) Or 
		(i.[TotalAnnualSales] != d.[TotalAnnualSales])) 
		THEN '| TotalAnnualSales OLD: ' + Convert(varchar, IsNull(d.[TotalAnnualSales],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[TotalAnnualSales],0))
		ELSE '' End +
	Case 
		WHEN ((i.[ProductsSold] Is Null And d.[ProductsSold] Is Not Null ) Or 
		(i.[ProductsSold] Is Not Null And d.[ProductsSold] Is Null ) Or 
		(i.[ProductsSold] != d.[ProductsSold])) 
		THEN '| ProductsSold OLD: ' + Convert(varchar, IsNull(d.[ProductsSold],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[ProductsSold],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[ServicesProvided] Is Null And d.[ServicesProvided] Is Not Null ) Or 
		(i.[ServicesProvided] Is Not Null And d.[ServicesProvided] Is Null ) Or 
		(i.[ServicesProvided] != d.[ServicesProvided])) 
		THEN '| ServicesProvided OLD: ' + Convert(varchar, IsNull(d.[ServicesProvided],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[ServicesProvided],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[GeographicalAreasServed] Is Null And d.[GeographicalAreasServed] Is Not Null ) Or 
		(i.[GeographicalAreasServed] Is Not Null And d.[GeographicalAreasServed] Is Null ) Or 
		(i.[GeographicalAreasServed] != d.[GeographicalAreasServed])) 
		THEN '| GeographicalAreasServed OLD: ' + Convert(varchar, IsNull(d.[GeographicalAreasServed],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[GeographicalAreasServed],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[IsPubliclyTraded] Is Null And d.[IsPubliclyTraded] Is Not Null ) Or 
		(i.[IsPubliclyTraded] Is Not Null And d.[IsPubliclyTraded] Is Null ) Or 
		(i.[IsPubliclyTraded] != d.[IsPubliclyTraded])) 
		THEN '| STR(IsPubliclyTraded) OLD: ' + case when IsNull(d.[IsPubliclyTraded],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[IsPubliclyTraded],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[TickerSymbol] Is Null And d.[TickerSymbol] Is Not Null ) Or 
		(i.[TickerSymbol] Is Not Null And d.[TickerSymbol] Is Null ) Or 
		(i.[TickerSymbol] != d.[TickerSymbol])) 
		THEN '| TickerSymbol OLD: ' + Convert(varchar, IsNull(d.[TickerSymbol],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[TickerSymbol],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[Exchange] Is Null And d.[Exchange] Is Not Null ) Or 
		(i.[Exchange] Is Not Null And d.[Exchange] Is Null ) Or 
		(i.[Exchange] != d.[Exchange])) 
		THEN '| Exchange OLD: ' + Convert(varchar, IsNull(d.[Exchange],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[Exchange],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[IsPensionFund] Is Null And d.[IsPensionFund] Is Not Null ) Or 
		(i.[IsPensionFund] Is Not Null And d.[IsPensionFund] Is Null ) Or 
		(i.[IsPensionFund] != d.[IsPensionFund])) 
		THEN '| SANCTION(IsPensionFund) OLD: ' + case when IsNull(d.[IsPensionFund],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[IsPensionFund],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[IsEndowmentFund] Is Null And d.[IsEndowmentFund] Is Not Null ) Or 
		(i.[IsEndowmentFund] Is Not Null And d.[IsEndowmentFund] Is Null ) Or 
		(i.[IsEndowmentFund] != d.[IsEndowmentFund])) 
		THEN '| NegativeNews(IsEndowmentFund) OLD: '+ case when IsNull(d.[IsEndowmentFund],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[IsEndowmentFund],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[IsGovernmentSponsored] Is Null And d.[IsGovernmentSponsored] Is Not Null ) Or 
		(i.[IsGovernmentSponsored] Is Not Null And d.[IsGovernmentSponsored] Is Null ) Or 
		(i.[IsGovernmentSponsored] != d.[IsGovernmentSponsored])) 
		THEN '| Complex Holding Stru.(IsGovernmentSponsored) OLD: ' + case when IsNull(d.[IsGovernmentSponsored],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[IsGovernmentSponsored],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[IsSubsidiary] Is Null And d.[IsSubsidiary] Is Not Null ) Or 
		(i.[IsSubsidiary] Is Not Null And d.[IsSubsidiary] Is Null ) Or 
		(i.[IsSubsidiary] != d.[IsSubsidiary])) 
		THEN '| IsSubsidiary OLD: ' + case when IsNull(d.[IsSubsidiary],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[IsSubsidiary],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[IsForeignCorrBnk] Is Null And d.[IsForeignCorrBnk] Is Not Null ) Or 
		(i.[IsForeignCorrBnk] Is Not Null And d.[IsForeignCorrBnk] Is Null ) Or 
		(i.[IsForeignCorrBnk] != d.[IsForeignCorrBnk])) 
		THEN '| IsForeignCorrBnk OLD: ' + case when IsNull(d.[IsForeignCorrBnk],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[IsForeignCorrBnk],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[RequireCashTrans] Is Null And d.[RequireCashTrans] Is Not Null ) Or 
		(i.[RequireCashTrans] Is Not Null And d.[RequireCashTrans] Is Null ) Or 
		(i.[RequireCashTrans] != d.[RequireCashTrans])) 
		THEN '| RequireCashTrans OLD: ' + case when IsNull(d.[RequireCashTrans],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[RequireCashTrans],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[RequireForeignTrans] Is Null And d.[RequireForeignTrans] Is Not Null ) Or 
		(i.[RequireForeignTrans] Is Not Null And d.[RequireForeignTrans] Is Null ) Or 
		(i.[RequireForeignTrans] != d.[RequireForeignTrans])) 
		THEN '| RequireForeignTrans OLD: ' + case when IsNull(d.[RequireForeignTrans],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[RequireForeignTrans],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[RequireWireTrans] Is Null And d.[RequireWireTrans] Is Not Null ) Or 
		(i.[RequireWireTrans] Is Not Null And d.[RequireWireTrans] Is Null ) Or 
		(i.[RequireWireTrans] != d.[RequireWireTrans])) 
		THEN '| RequireWireTrans OLD: ' + case when IsNull(d.[RequireWireTrans],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[RequireWireTrans],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[HasForeignBranches] Is Null And d.[HasForeignBranches] Is Not Null ) Or 
		(i.[HasForeignBranches] Is Not Null And d.[HasForeignBranches] Is Null ) Or 
		(i.[HasForeignBranches] != d.[HasForeignBranches])) 
		THEN '| HasForeignBranches OLD: ' + case when IsNull(d.[HasForeignBranches],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[HasForeignBranches],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[ForeignBranchCountries] Is Null And d.[ForeignBranchCountries] Is Not Null ) Or 
		(i.[ForeignBranchCountries] Is Not Null And d.[ForeignBranchCountries] Is Null ) Or 
		(i.[ForeignBranchCountries] != d.[ForeignBranchCountries])) 
		THEN '| ForeignBranchCountries OLD: ' + Convert(varchar, IsNull(d.[ForeignBranchCountries],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[ForeignBranchCountries],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[TypeofMonInstruments] Is Null And d.[TypeofMonInstruments] Is Not Null ) Or 
		(i.[TypeofMonInstruments] Is Not Null And d.[TypeofMonInstruments] Is Null ) Or 
		(i.[TypeofMonInstruments] != d.[TypeofMonInstruments])) 
		THEN '| TypeofMonInstruments OLD: ' + Convert(varchar, IsNull(d.[TypeofMonInstruments],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[TypeofMonInstruments],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[url] Is Null And d.[url] Is Not Null ) Or 
		(i.[url] Is Not Null And d.[url] Is Null ) Or 
		(i.[url] != d.[url])) 
		THEN '| url OLD: ' + Convert(varchar, IsNull(d.[url],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[url],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[HasPatriotActCertification] Is Null And d.[HasPatriotActCertification] Is Not Null ) Or 
		(i.[HasPatriotActCertification] Is Not Null And d.[HasPatriotActCertification] Is Null ) Or 
		(i.[HasPatriotActCertification] != d.[HasPatriotActCertification])) 
		THEN '| HasPatriotActCertification OLD: ' + case when IsNull(d.[HasPatriotActCertification],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[HasPatriotActCertification],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[PatriotActCertExpiryDate] Is Null And d.[PatriotActCertExpiryDate] Is Not Null ) Or 
		(i.[PatriotActCertExpiryDate] Is Not Null And d.[PatriotActCertExpiryDate] Is Null ) Or 
		(i.[PatriotActCertExpiryDate] != d.[PatriotActCertExpiryDate])) 
		THEN '| PatriotActCertExpiryDate OLD: ' + case when d.[PatriotActCertExpiryDate] is Null then 'Null' else 
		Convert(varchar, d.[PatriotActCertExpiryDate]) End + ' NEW: ' + case when i.[PatriotActCertExpiryDate] is Null then 'Null' else 
		Convert(varchar, i.[PatriotActCertExpiryDate]) End
		ELSE '' End +
	Case 
		WHEN ((i.[MoneyOrders] Is Null And d.[MoneyOrders] Is Not Null ) Or 
		(i.[MoneyOrders] Is Not Null And d.[MoneyOrders] Is Null ) Or 
		(i.[MoneyOrders] != d.[MoneyOrders])) 
		THEN '| MoneyOrders OLD: ' + case when IsNull(d.[MoneyOrders],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[MoneyOrders],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[MORegistration] Is Null And d.[MORegistration] Is Not Null ) Or 
		(i.[MORegistration] Is Not Null And d.[MORegistration] Is Null ) Or 
		(i.[MORegistration] != d.[MORegistration])) 
		THEN '| MORegistration OLD: ' + Convert(varchar, IsNull(d.[MORegistration],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MORegistration],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[MORegConfirmationNo] Is Null And d.[MORegConfirmationNo] Is Not Null ) Or 
		(i.[MORegConfirmationNo] Is Not Null And d.[MORegConfirmationNo] Is Null ) Or 
		(i.[MORegConfirmationNo] != d.[MORegConfirmationNo])) 
		THEN '| MORegConfirmationNo OLD: ' + Convert(varchar, IsNull(d.[MORegConfirmationNo],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MORegConfirmationNo],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[MOActingasAgentfor] Is Null And d.[MOActingasAgentfor] Is Not Null ) Or 
		(i.[MOActingasAgentfor] Is Not Null And d.[MOActingasAgentfor] Is Null ) Or 
		(i.[MOActingasAgentfor] != d.[MOActingasAgentfor])) 
		THEN '| MOActingasAgentfor OLD: ' + Convert(varchar, IsNull(d.[MOActingasAgentfor],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MOActingasAgentfor],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[MOMaxDailyAmtPerPerson] Is Null And d.[MOMaxDailyAmtPerPerson] Is Not Null ) Or 
		(i.[MOMaxDailyAmtPerPerson] Is Not Null And d.[MOMaxDailyAmtPerPerson] Is Null ) Or 
		(i.[MOMaxDailyAmtPerPerson] != d.[MOMaxDailyAmtPerPerson])) 
		THEN '| MOMaxDailyAmtPerPerson OLD: ' + Convert(varchar, IsNull(d.[MOMaxDailyAmtPerPerson],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MOMaxDailyAmtPerPerson],0))
		ELSE '' End +
	Case 
		WHEN ((i.[MOMonthlyAmt] Is Null And d.[MOMonthlyAmt] Is Not Null ) Or 
		(i.[MOMonthlyAmt] Is Not Null And d.[MOMonthlyAmt] Is Null ) Or 
		(i.[MOMonthlyAmt] != d.[MOMonthlyAmt])) 
		THEN '| MOMonthlyAmt OLD: ' + Convert(varchar, IsNull(d.[MOMonthlyAmt],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MOMonthlyAmt],0))
		ELSE '' End +
	Case 
		WHEN ((i.[MOPercentIncome] Is Null And d.[MOPercentIncome] Is Not Null ) Or 
		(i.[MOPercentIncome] Is Not Null And d.[MOPercentIncome] Is Null ) Or 
		(i.[MOPercentIncome] != d.[MOPercentIncome])) 
		THEN '| MOPercentIncome OLD: ' + Convert(varchar, IsNull(d.[MOPercentIncome],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MOPercentIncome],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[TravelersCheck] Is Null And d.[TravelersCheck] Is Not Null ) Or 
		(i.[TravelersCheck] Is Not Null And d.[TravelersCheck] Is Null ) Or 
		(i.[TravelersCheck] != d.[TravelersCheck])) 
		THEN '| TravelersCheck OLD: ' + case when IsNull(d.[TravelersCheck],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[TravelersCheck],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[TCRegistration] Is Null And d.[TCRegistration] Is Not Null ) Or 
		(i.[TCRegistration] Is Not Null And d.[TCRegistration] Is Null ) Or 
		(i.[TCRegistration] != d.[TCRegistration])) 
		THEN '| TCRegistration OLD: ' + Convert(varchar, IsNull(d.[TCRegistration],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[TCRegistration],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[TCRegConfirmationNo] Is Null And d.[TCRegConfirmationNo] Is Not Null ) Or 
		(i.[TCRegConfirmationNo] Is Not Null And d.[TCRegConfirmationNo] Is Null ) Or 
		(i.[TCRegConfirmationNo] != d.[TCRegConfirmationNo])) 
		THEN '| TCRegConfirmationNo OLD: ' + Convert(varchar, IsNull(d.[TCRegConfirmationNo],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[TCRegConfirmationNo],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[TCActingasAgentfor] Is Null And d.[TCActingasAgentfor] Is Not Null ) Or 
		(i.[TCActingasAgentfor] Is Not Null And d.[TCActingasAgentfor] Is Null ) Or 
		(i.[TCActingasAgentfor] != d.[TCActingasAgentfor])) 
		THEN '| TCActingasAgentfor OLD: ' + Convert(varchar, IsNull(d.[TCActingasAgentfor],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[TCActingasAgentfor],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[TCMaxDailyAmtPerPerson] Is Null And d.[TCMaxDailyAmtPerPerson] Is Not Null ) Or 
		(i.[TCMaxDailyAmtPerPerson] Is Not Null And d.[TCMaxDailyAmtPerPerson] Is Null ) Or 
		(i.[TCMaxDailyAmtPerPerson] != d.[TCMaxDailyAmtPerPerson])) 
		THEN '| TCMaxDailyAmtPerPerson OLD: ' + Convert(varchar, IsNull(d.[TCMaxDailyAmtPerPerson],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[TCMaxDailyAmtPerPerson],0))
		ELSE '' End +
	Case 
		WHEN ((i.[TCMonthlyAmt] Is Null And d.[TCMonthlyAmt] Is Not Null ) Or 
		(i.[TCMonthlyAmt] Is Not Null And d.[TCMonthlyAmt] Is Null ) Or 
		(i.[TCMonthlyAmt] != d.[TCMonthlyAmt])) 
		THEN '| TCMonthlyAmt OLD: ' + Convert(varchar, IsNull(d.[TCMonthlyAmt],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[TCMonthlyAmt],0))
		ELSE '' End +
	Case 
		WHEN ((i.[MoneyTransmission] Is Null And d.[MoneyTransmission] Is Not Null ) Or 
		(i.[MoneyTransmission] Is Not Null And d.[MoneyTransmission] Is Null ) Or 
		(i.[MoneyTransmission] != d.[MoneyTransmission])) 
		THEN '| MoneyTransmission OLD: ' + case when IsNull(d.[MoneyTransmission],0) = 0 then 'False' else 'True' End + 
		' NEW: ' + case when IsNull(i.[MoneyTransmission],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[MTRegistration] Is Null And d.[MTRegistration] Is Not Null ) Or 
		(i.[MTRegistration] Is Not Null And d.[MTRegistration] Is Null ) Or 
		(i.[MTRegistration] != d.[MTRegistration])) 
		THEN '| MTRegistration OLD: ' + Convert(varchar, IsNull(d.[MTRegistration],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MTRegistration],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[MTRegConfirmationNo] Is Null And d.[MTRegConfirmationNo] Is Not Null ) Or 
		(i.[MTRegConfirmationNo] Is Not Null And d.[MTRegConfirmationNo] Is Null ) Or 
		(i.[MTRegConfirmationNo] != d.[MTRegConfirmationNo])) 
		THEN '| MTRegConfirmationNo OLD: ' + Convert(varchar, IsNull(d.[MTRegConfirmationNo],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MTRegConfirmationNo],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[MTActingasAgentfor] Is Null And d.[MTActingasAgentfor] Is Not Null ) Or 
		(i.[MTActingasAgentfor] Is Not Null And d.[MTActingasAgentfor] Is Null ) Or 
		(i.[MTActingasAgentfor] != d.[MTActingasAgentfor])) 
		THEN '| MTActingasAgentfor OLD: ' + Convert(varchar, IsNull(d.[MTActingasAgentfor],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MTActingasAgentfor],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[MTMaxDailyAmtPerPerson] Is Null And d.[MTMaxDailyAmtPerPerson] Is Not Null ) Or 
		(i.[MTMaxDailyAmtPerPerson] Is Not Null And d.[MTMaxDailyAmtPerPerson] Is Null ) Or 
		(i.[MTMaxDailyAmtPerPerson] != d.[MTMaxDailyAmtPerPerson])) 
		THEN '| MTMaxDailyAmtPerPerson OLD: ' + Convert(varchar, IsNull(d.[MTMaxDailyAmtPerPerson],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MTMaxDailyAmtPerPerson],0))
		ELSE '' End +
	Case 
		WHEN ((i.[MTMonthlyAmt] Is Null And d.[MTMonthlyAmt] Is Not Null ) Or 
		(i.[MTMonthlyAmt] Is Not Null And d.[MTMonthlyAmt] Is Null ) Or 
		(i.[MTMonthlyAmt] != d.[MTMonthlyAmt])) 
		THEN '| MTMonthlyAmt OLD: ' + Convert(varchar, IsNull(d.[MTMonthlyAmt],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MTMonthlyAmt],0))
		ELSE '' End +
	Case 
		WHEN ((i.[MTPercentIncome] Is Null And d.[MTPercentIncome] Is Not Null ) Or 
		(i.[MTPercentIncome] Is Not Null And d.[MTPercentIncome] Is Null ) Or 
		(i.[MTPercentIncome] != d.[MTPercentIncome])) 
		THEN '| MTPercentIncome OLD: ' + Convert(varchar, IsNull(d.[MTPercentIncome],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[MTPercentIncome],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CheckCashing] Is Null And d.[CheckCashing] Is Not Null ) Or 
		(i.[CheckCashing] Is Not Null And d.[CheckCashing] Is Null ) Or 
		(i.[CheckCashing] != d.[CheckCashing])) 
		THEN '| CheckCashing OLD: ' + case when IsNull(d.[CheckCashing],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[CheckCashing],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[CCRegistration] Is Null And d.[CCRegistration] Is Not Null ) Or 
		(i.[CCRegistration] Is Not Null And d.[CCRegistration] Is Null ) Or 
		(i.[CCRegistration] != d.[CCRegistration])) 
		THEN '| CCRegistration OLD: ' + Convert(varchar, IsNull(d.[CCRegistration],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CCRegistration],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CCRegConfirmationNo] Is Null And d.[CCRegConfirmationNo] Is Not Null ) Or 
		(i.[CCRegConfirmationNo] Is Not Null And d.[CCRegConfirmationNo] Is Null ) Or 
		(i.[CCRegConfirmationNo] != d.[CCRegConfirmationNo])) 
		THEN '| CCRegConfirmationNo OLD: ' + Convert(varchar, IsNull(d.[CCRegConfirmationNo],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CCRegConfirmationNo],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CCActingasAgentfor] Is Null And d.[CCActingasAgentfor] Is Not Null ) Or 
		(i.[CCActingasAgentfor] Is Not Null And d.[CCActingasAgentfor] Is Null ) Or 
		(i.[CCActingasAgentfor] != d.[CCActingasAgentfor])) 
		THEN '| CCActingasAgentfor OLD: ' + Convert(varchar, IsNull(d.[CCActingasAgentfor],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CCActingasAgentfor],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CCMaxDailyAmtPerPerson] Is Null And d.[CCMaxDailyAmtPerPerson] Is Not Null ) Or 
		(i.[CCMaxDailyAmtPerPerson] Is Not Null And d.[CCMaxDailyAmtPerPerson] Is Null ) Or 
		(i.[CCMaxDailyAmtPerPerson] != d.[CCMaxDailyAmtPerPerson])) 
		THEN '| CCMaxDailyAmtPerPerson OLD: ' + Convert(varchar, IsNull(d.[CCMaxDailyAmtPerPerson],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CCMaxDailyAmtPerPerson],0))
		ELSE '' End +
	Case 
		WHEN ((i.[CCMonthlyAmt] Is Null And d.[CCMonthlyAmt] Is Not Null ) Or 
		(i.[CCMonthlyAmt] Is Not Null And d.[CCMonthlyAmt] Is Null ) Or 
		(i.[CCMonthlyAmt] != d.[CCMonthlyAmt])) 
		THEN '| CCMonthlyAmt OLD: ' + Convert(varchar, IsNull(d.[CCMonthlyAmt],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CCMonthlyAmt],0))
		ELSE '' End +
	Case 
		WHEN ((i.[CCPercentIncome] Is Null And d.[CCPercentIncome] Is Not Null ) Or 
		(i.[CCPercentIncome] Is Not Null And d.[CCPercentIncome] Is Null ) Or 
		(i.[CCPercentIncome] != d.[CCPercentIncome])) 
		THEN '| CCPercentIncome OLD: ' + Convert(varchar, IsNull(d.[CCPercentIncome],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CCPercentIncome],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CurrencyExchange] Is Null And d.[CurrencyExchange] Is Not Null ) Or 
		(i.[CurrencyExchange] Is Not Null And d.[CurrencyExchange] Is Null ) Or 
		(i.[CurrencyExchange] != d.[CurrencyExchange])) 
		THEN '| CurrencyExchange OLD: ' + case when IsNull(d.[CurrencyExchange],0) = 0 then 'False' else 'True' End + 
		' NEW: ' + case when IsNull(i.[CurrencyExchange],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[CERegistration] Is Null And d.[CERegistration] Is Not Null ) Or 
		(i.[CERegistration] Is Not Null And d.[CERegistration] Is Null ) Or 
		(i.[CERegistration] != d.[CERegistration])) 
		THEN '| CERegistration OLD: ' + Convert(varchar, IsNull(d.[CERegistration],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CERegistration],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CERegConfirmationNo] Is Null And d.[CERegConfirmationNo] Is Not Null ) Or 
		(i.[CERegConfirmationNo] Is Not Null And d.[CERegConfirmationNo] Is Null ) Or 
		(i.[CERegConfirmationNo] != d.[CERegConfirmationNo])) 
		THEN '| CERegConfirmationNo OLD: ' + Convert(varchar, IsNull(d.[CERegConfirmationNo],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CERegConfirmationNo],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CEActingasAgentfor] Is Null And d.[CEActingasAgentfor] Is Not Null ) Or 
		(i.[CEActingasAgentfor] Is Not Null And d.[CEActingasAgentfor] Is Null ) Or 
		(i.[CEActingasAgentfor] != d.[CEActingasAgentfor])) 
		THEN '| CEActingasAgentfor OLD: ' + Convert(varchar, IsNull(d.[CEActingasAgentfor],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CEActingasAgentfor],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CEMaxDailyAmtPerPerson] Is Null And d.[CEMaxDailyAmtPerPerson] Is Not Null ) Or 
		(i.[CEMaxDailyAmtPerPerson] Is Not Null And d.[CEMaxDailyAmtPerPerson] Is Null ) Or 
		(i.[CEMaxDailyAmtPerPerson] != d.[CEMaxDailyAmtPerPerson])) 
		THEN '| CEMaxDailyAmtPerPerson OLD: ' + Convert(varchar, IsNull(d.[CEMaxDailyAmtPerPerson],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CEMaxDailyAmtPerPerson],0))
		ELSE '' End +
	Case 
		WHEN ((i.[CEMonthlyAmt] Is Null And d.[CEMonthlyAmt] Is Not Null ) Or 
		(i.[CEMonthlyAmt] Is Not Null And d.[CEMonthlyAmt] Is Null ) Or 
		(i.[CEMonthlyAmt] != d.[CEMonthlyAmt])) 
		THEN '| CEMonthlyAmt OLD: ' + Convert(varchar, IsNull(d.[CEMonthlyAmt],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CEMonthlyAmt],0))
		ELSE '' End +
	Case 
		WHEN ((i.[CEPercentIncome] Is Null And d.[CEPercentIncome] Is Not Null ) Or 
		(i.[CEPercentIncome] Is Not Null And d.[CEPercentIncome] Is Null ) Or 
		(i.[CEPercentIncome] != d.[CEPercentIncome])) 
		THEN '| CEPercentIncome OLD: ' + Convert(varchar, IsNull(d.[CEPercentIncome],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CEPercentIncome],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CurrencyDealing] Is Null And d.[CurrencyDealing] Is Not Null ) Or 
		(i.[CurrencyDealing] Is Not Null And d.[CurrencyDealing] Is Null ) Or 
		(i.[CurrencyDealing] != d.[CurrencyDealing])) 
		THEN '| CurrencyDealing OLD: ' + case when IsNull(d.[CurrencyDealing],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[CurrencyDealing],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[CDRegistration] Is Null And d.[CDRegistration] Is Not Null ) Or 
		(i.[CDRegistration] Is Not Null And d.[CDRegistration] Is Null ) Or 
		(i.[CDRegistration] != d.[CDRegistration])) 
		THEN '| CDRegistration OLD: ' + Convert(varchar, IsNull(d.[CDRegistration],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDRegistration],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDRegConfirmationNo] Is Null And d.[CDRegConfirmationNo] Is Not Null ) Or 
		(i.[CDRegConfirmationNo] Is Not Null And d.[CDRegConfirmationNo] Is Null ) Or 
		(i.[CDRegConfirmationNo] != d.[CDRegConfirmationNo])) 
		THEN '| CDRegConfirmationNo OLD: ' + Convert(varchar, IsNull(d.[CDRegConfirmationNo],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDRegConfirmationNo],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDActingasAgentfor] Is Null And d.[CDActingasAgentfor] Is Not Null ) Or 
		(i.[CDActingasAgentfor] Is Not Null And d.[CDActingasAgentfor] Is Null ) Or 
		(i.[CDActingasAgentfor] != d.[CDActingasAgentfor])) 
		THEN '| CDActingasAgentfor OLD: ' + Convert(varchar, IsNull(d.[CDActingasAgentfor],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDActingasAgentfor],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDMaxDailyAmtPerPerson] Is Null And d.[CDMaxDailyAmtPerPerson] Is Not Null ) Or 
		(i.[CDMaxDailyAmtPerPerson] Is Not Null And d.[CDMaxDailyAmtPerPerson] Is Null ) Or 
		(i.[CDMaxDailyAmtPerPerson] != d.[CDMaxDailyAmtPerPerson])) 
		THEN '| CDMaxDailyAmtPerPerson OLD: ' + Convert(varchar, IsNull(d.[CDMaxDailyAmtPerPerson],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDMaxDailyAmtPerPerson],0))
		ELSE '' End +
	Case 
		WHEN ((i.[CDMonthlyAmt] Is Null And d.[CDMonthlyAmt] Is Not Null ) Or 
		(i.[CDMonthlyAmt] Is Not Null And d.[CDMonthlyAmt] Is Null ) Or 
		(i.[CDMonthlyAmt] != d.[CDMonthlyAmt])) 
		THEN '| CDMonthlyAmt OLD: ' + Convert(varchar, IsNull(d.[CDMonthlyAmt],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDMonthlyAmt],0))
		ELSE '' End +
	Case 
		WHEN ((i.[CDPercentIncome] Is Null And d.[CDPercentIncome] Is Not Null ) Or 
		(i.[CDPercentIncome] Is Not Null And d.[CDPercentIncome] Is Null ) Or 
		(i.[CDPercentIncome] != d.[CDPercentIncome])) 
		THEN '| CDPercentIncome OLD: ' + Convert(varchar, IsNull(d.[CDPercentIncome],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDPercentIncome],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[StoredValue] Is Null And d.[StoredValue] Is Not Null ) Or 
		(i.[StoredValue] Is Not Null And d.[StoredValue] Is Null ) Or 
		(i.[StoredValue] != d.[StoredValue])) 
		THEN '| StoredValue OLD: ' + case when IsNull(d.[StoredValue],0) = 0 then 'False' else 'True' End + 
			' NEW: ' + case when IsNull(i.[StoredValue],0) = 0 then 'False' else 'True' End
		ELSE '' End +
	Case 
		WHEN ((i.[SVRegistration] Is Null And d.[SVRegistration] Is Not Null ) Or 
		(i.[SVRegistration] Is Not Null And d.[SVRegistration] Is Null ) Or 
		(i.[SVRegistration] != d.[SVRegistration])) 
		THEN '| SVRegistration OLD: ' + Convert(varchar, IsNull(d.[SVRegistration],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[SVRegistration],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[SVRegConfirmationNo] Is Null And d.[SVRegConfirmationNo] Is Not Null ) Or 
		(i.[SVRegConfirmationNo] Is Not Null And d.[SVRegConfirmationNo] Is Null ) Or 
		(i.[SVRegConfirmationNo] != d.[SVRegConfirmationNo])) 
		THEN '| SVRegConfirmationNo OLD: ' + Convert(varchar, IsNull(d.[SVRegConfirmationNo],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[SVRegConfirmationNo],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[SVActingasAgentfor] Is Null And d.[SVActingasAgentfor] Is Not Null ) Or 
		(i.[SVActingasAgentfor] Is Not Null And d.[SVActingasAgentfor] Is Null ) Or 
		(i.[SVActingasAgentfor] != d.[SVActingasAgentfor])) 
		THEN '| SVActingasAgentfor OLD: ' + Convert(varchar, IsNull(d.[SVActingasAgentfor],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[SVActingasAgentfor],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[SVMaxDailyAmtPerPerson] Is Null And d.[SVMaxDailyAmtPerPerson] Is Not Null ) Or 
		(i.[SVMaxDailyAmtPerPerson] Is Not Null And d.[SVMaxDailyAmtPerPerson] Is Null ) Or 
		(i.[SVMaxDailyAmtPerPerson] != d.[SVMaxDailyAmtPerPerson])) 
		THEN '| SVMaxDailyAmtPerPerson OLD: ' + Convert(varchar, IsNull(d.[SVMaxDailyAmtPerPerson],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[SVMaxDailyAmtPerPerson],0))
		ELSE '' End +
	Case 
		WHEN ((i.[SVMonthlyAmt] Is Null And d.[SVMonthlyAmt] Is Not Null ) Or 
		(i.[SVMonthlyAmt] Is Not Null And d.[SVMonthlyAmt] Is Null ) Or 
		(i.[SVMonthlyAmt] != d.[SVMonthlyAmt])) 
		THEN '| SVMonthlyAmt OLD: ' + Convert(varchar, IsNull(d.[SVMonthlyAmt],0)) + 
		' NEW: ' + Convert(varchar, IsNull(i.[SVMonthlyAmt],0))
		ELSE '' End +
	Case 
		WHEN ((i.[SVPercentIncome] Is Null And d.[SVPercentIncome] Is Not Null ) Or 
		(i.[SVPercentIncome] Is Not Null And d.[SVPercentIncome] Is Null ) Or 
		(i.[SVPercentIncome] != d.[SVPercentIncome])) 
		THEN '| [SVPercentIncome] OLD: ' + Convert(varchar, IsNull(d.[SVPercentIncome],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[SVPercentIncome],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser1] Is Null And d.[CDDUser1] Is Not Null ) Or 
		(i.[CDDUser1] Is Not Null And d.[CDDUser1] Is Null ) Or 
		(i.[CDDUser1] != d.[CDDUser1])) 
		THEN '| TypeofProducts(CDDUser1) OLD: ' + Convert(varchar, IsNull(d.[CDDUser1],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser1],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser2] Is Null And d.[CDDUser2] Is Not Null ) Or 
		(i.[CDDUser2] Is Not Null And d.[CDDUser2] Is Null ) Or 
		(i.[CDDUser2] != d.[CDDUser2])) 
		THEN '| TypeofProducts1(CDDUser2) OLD: ' + Convert(varchar, IsNull(d.[CDDUser2],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser2],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser3] Is Null And d.[CDDUser3] Is Not Null ) Or 
		(i.[CDDUser3] Is Not Null And d.[CDDUser3] Is Null ) Or 
		(i.[CDDUser3] != d.[CDDUser3])) 
		THEN '| TypeofProducts2(CDDUser3) OLD: ' + Convert(varchar, IsNull(d.[CDDUser3],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser3],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser4] Is Null And d.[CDDUser4] Is Not Null ) Or 
		(i.[CDDUser4] Is Not Null And d.[CDDUser4] Is Null ) Or 
		(i.[CDDUser4] != d.[CDDUser4])) 
		THEN '| TypeofProducts3(CDDUser4) OLD: ' + Convert(varchar, IsNull(d.[CDDUser4],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser4],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser5] Is Null And d.[CDDUser5] Is Not Null ) Or 
		(i.[CDDUser5] Is Not Null And d.[CDDUser5] Is Null ) Or 
		(i.[CDDUser5] != d.[CDDUser5])) 
		THEN '| TypeofProducts4(CDDUser5) OLD: ' + Convert(varchar, IsNull(d.[CDDUser5],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser5],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser6] Is Null And d.[CDDUser6] Is Not Null ) Or 
		(i.[CDDUser6] Is Not Null And d.[CDDUser6] Is Null ) Or 
		(i.[CDDUser6] != d.[CDDUser6])) 
		THEN '| TypeofProducts5(CDDUser6) OLD: ' + Convert(varchar, IsNull(d.[CDDUser6],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser6],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser7] Is Null And d.[CDDUser7] Is Not Null ) Or 
		(i.[CDDUser7] Is Not Null And d.[CDDUser7] Is Null ) Or 
		(i.[CDDUser7] != d.[CDDUser7])) 
		THEN '| TradeCountry(CDDUser7) OLD: ' + Convert(varchar, IsNull(d.[CDDUser7],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser7],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser8] Is Null And d.[CDDUser8] Is Not Null ) Or 
		(i.[CDDUser8] Is Not Null And d.[CDDUser8] Is Null ) Or 
		(i.[CDDUser8] != d.[CDDUser8])) 
		THEN '| TradeCountry1(CDDUser8) OLD: ' + Convert(varchar, IsNull(d.[CDDUser8],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser8],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser9] Is Null And d.[CDDUser9] Is Not Null ) Or 
		(i.[CDDUser9] Is Not Null And d.[CDDUser9] Is Null ) Or 
		(i.[CDDUser9] != d.[CDDUser9])) 
		THEN '| TradeCountry2(CDDUser9) OLD: ' + Convert(varchar, IsNull(d.[CDDUser9],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser9],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser10] Is Null And d.[CDDUser10] Is Not Null ) Or 
		(i.[CDDUser10] Is Not Null And d.[CDDUser10] Is Null ) Or 
		(i.[CDDUser10] != d.[CDDUser10])) 
		THEN '| TradeCountry3(CDDUser10) OLD: ' + Convert(varchar, IsNull(d.[CDDUser10],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser10],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser11] Is Null And d.[CDDUser11] Is Not Null ) Or 
		(i.[CDDUser11] Is Not Null And d.[CDDUser11] Is Null ) Or 
		(i.[CDDUser11] != d.[CDDUser11])) 
		THEN '| TradeCountry4(CDDUser11) OLD: ' + Convert(varchar, IsNull(d.[CDDUser11],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser11],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser12] Is Null And d.[CDDUser12] Is Not Null ) Or 
		(i.[CDDUser12] Is Not Null And d.[CDDUser12] Is Null ) Or 
		(i.[CDDUser12] != d.[CDDUser12])) 
		THEN '| TradeCountry5(CDDUser12) OLD: ' + Convert(varchar, IsNull(d.[CDDUser12],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser12],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser13] Is Null And d.[CDDUser13] Is Not Null ) Or 
		(i.[CDDUser13] Is Not Null And d.[CDDUser13] Is Null ) Or 
		(i.[CDDUser13] != d.[CDDUser13])) 
		THEN '| TransactionChannels(CDDUser13) OLD: ' + Convert(varchar, IsNull(d.[CDDUser13],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser13],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser14] Is Null And d.[CDDUser14] Is Not Null ) Or 
		(i.[CDDUser14] Is Not Null And d.[CDDUser14] Is Null ) Or 
		(i.[CDDUser14] != d.[CDDUser14])) 
		THEN '| TransactionChannels1(CDDUser14) OLD: ' + Convert(varchar, IsNull(d.[CDDUser14],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser14],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser15] Is Null And d.[CDDUser15] Is Not Null ) Or 
		(i.[CDDUser15] Is Not Null And d.[CDDUser15] Is Null ) Or 
		(i.[CDDUser15] != d.[CDDUser15])) 
		THEN '| TransactionChannels2(CDDUser15) OLD: ' + Convert(varchar, IsNull(d.[CDDUser15],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser15],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser16] Is Null And d.[CDDUser16] Is Not Null ) Or 
		(i.[CDDUser16] Is Not Null And d.[CDDUser16] Is Null ) Or 
		(i.[CDDUser16] != d.[CDDUser16])) 
		THEN '| TransactionChannels3(CDDUser16) OLD: ' + Convert(varchar, IsNull(d.[CDDUser16],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser16],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser17] Is Null And d.[CDDUser17] Is Not Null ) Or 
		(i.[CDDUser17] Is Not Null And d.[CDDUser17] Is Null ) Or 
		(i.[CDDUser17] != d.[CDDUser17])) 
		THEN '| CustomerType(CDDUser17) OLD: ' + Convert(varchar, IsNull(d.[CDDUser17],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser17],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser18] Is Null And d.[CDDUser18] Is Not Null ) Or 
		(i.[CDDUser18] Is Not Null And d.[CDDUser18] Is Null ) Or 
		(i.[CDDUser18] != d.[CDDUser18])) 
		THEN '| CustomerStatus(CDDUser18) OLD: ' + Convert(varchar, IsNull(d.[CDDUser18],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser18],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser19] Is Null And d.[CDDUser19] Is Not Null ) Or 
		(i.[CDDUser19] Is Not Null And d.[CDDUser19] Is Null ) Or 
		(i.[CDDUser19] != d.[CDDUser19])) 
		THEN '| PEP Status(CDDUser19) OLD: ' + Convert(varchar, IsNull(d.[CDDUser19],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser19],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser20] Is Null And d.[CDDUser20] Is Not Null ) Or 
		(i.[CDDUser20] Is Not Null And d.[CDDUser20] Is Null ) Or 
		(i.[CDDUser20] != d.[CDDUser20])) 
		THEN '| ReverificationChannel(CDDUser20) OLD: ' + Convert(varchar, IsNull(d.[CDDUser20],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser20],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser21] Is Null And d.[CDDUser21] Is Not Null ) Or 
		(i.[CDDUser21] Is Not Null And d.[CDDUser21] Is Null ) Or 
		(i.[CDDUser21] != d.[CDDUser21])) 
		THEN '| SanctionList(CDDUser21) OLD: ' + Convert(varchar, IsNull(d.[CDDUser21],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser21],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser22] Is Null And d.[CDDUser22] Is Not Null ) Or 
		(i.[CDDUser22] Is Not Null And d.[CDDUser22] Is Null ) Or 
		(i.[CDDUser22] != d.[CDDUser22])) 
		THEN '| ShareHolderListing(CDDUser22) OLD: ' + Convert(varchar, IsNull(d.[CDDUser22],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser22],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser23] Is Null And d.[CDDUser23] Is Not Null ) Or 
		(i.[CDDUser23] Is Not Null And d.[CDDUser23] Is Null ) Or 
		(i.[CDDUser23] != d.[CDDUser23])) 
		THEN '| Mgr.OverrideScore(CDDUser23) OLD: ' + Convert(varchar, IsNull(d.[CDDUser23],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser23],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser24] Is Null And d.[CDDUser24] Is Not Null ) Or 
		(i.[CDDUser24] Is Not Null And d.[CDDUser24] Is Null ) Or 
		(i.[CDDUser24] != d.[CDDUser24])) 
		THEN '| Industry(CDDUser24) OLD: ' + Convert(varchar, IsNull(d.[CDDUser24],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser24],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser25] Is Null And d.[CDDUser25] Is Not Null ) Or 
		(i.[CDDUser25] Is Not Null And d.[CDDUser25] Is Null ) Or 
		(i.[CDDUser25] != d.[CDDUser25])) 
		THEN '| RiskRatingCode(CDDUser25) OLD: ' + Convert(varchar, IsNull(d.[CDDUser25],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser25],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser26] Is Null And d.[CDDUser26] Is Not Null ) Or 
		(i.[CDDUser26] Is Not Null And d.[CDDUser26] Is Null ) Or 
		(i.[CDDUser26] != d.[CDDUser26])) 
		THEN '| SourceFunds1(CDDUser26) OLD: ' + Convert(varchar, IsNull(d.[CDDUser26],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser26],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser27] Is Null And d.[CDDUser27] Is Not Null ) Or 
		(i.[CDDUser27] Is Not Null And d.[CDDUser27] Is Null ) Or 
		(i.[CDDUser27] != d.[CDDUser27])) 
		THEN '| SourceFunds1AMT(CDDUser27) OLD: ' + Convert(varchar, IsNull(d.[CDDUser27],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser27],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser28] Is Null And d.[CDDUser28] Is Not Null ) Or 
		(i.[CDDUser28] Is Not Null And d.[CDDUser28] Is Null ) Or 
		(i.[CDDUser28] != d.[CDDUser28])) 
		THEN '| SourceFunds2(CDDUser28) OLD: ' + Convert(varchar, IsNull(d.[CDDUser28],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser28],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser29] Is Null And d.[CDDUser29] Is Not Null ) Or 
		(i.[CDDUser29] Is Not Null And d.[CDDUser29] Is Null ) Or 
		(i.[CDDUser29] != d.[CDDUser29])) 
		THEN '| SourceFunds2AMT(CDDUser29) OLD: ' + Convert(varchar, IsNull(d.[CDDUser29],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[CDDUser29],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser30] Is Null And d.[CDDUser30] Is Not Null ) Or 
		(i.[cdduser30] Is Not Null And d.[cdduser30] Is Null ) Or 
		(i.[cdduser30] != d.[cdduser30])) 
		THEN '| SourceFunds3(CDDUser30) OLD: ' + Convert(varchar, IsNull(d.[cdduser30],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[cdduser30],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[cdduser31] Is Null And d.[cdduser31] Is Not Null ) Or 
		(i.[cdduser31] Is Not Null And d.[cdduser31] Is Null ) Or 
		(i.[cdduser31] != d.[cdduser31])) 
		THEN '| SourceFunds3AMT(CDDUser31) OLD: ' + Convert(varchar(1000), IsNull(d.[cdduser31],'Null')) + 
		' NEW: ' + Convert(varchar(1000), IsNull(i.[cdduser31],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[cdduser32] Is Null And d.[cdduser32] Is Not Null ) Or 
		(i.[cdduser32] Is Not Null And d.[cdduser32] Is Null ) Or 
		(i.[cdduser32] != d.[CDDUser32])) 
		THEN '| COMMENTS1(CDDUser32) OLD: ' + Convert(varchar(1000), IsNull(d.[cdduser32],'Null')) + 
		' NEW: ' + Convert(varchar(1000), IsNull(i.[cdduser32],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser33] Is Null And d.[CDDUser33] Is Not Null ) Or 
		(i.[CDDUser33] Is Not Null And d.[CDDUser33] Is Null ) Or 
		(i.[CDDUser33] != d.[CDDUser33])) 
		THEN '| COMMENTS2(CDDUser33) OLD: ' + Convert(varchar(1000), IsNull(d.[CDDUser33],'Null')) + 
		' NEW: ' + Convert(varchar(1000), IsNull(i.[CDDUser33],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser34] Is Null And d.[CDDUser34] Is Not Null ) Or 
		(i.[CDDUser34] Is Not Null And d.[CDDUser34] Is Null ) Or 
		(i.[CDDUser34] != d.[CDDUser34])) 
		THEN '| COMMENTS3(CDDUser34) OLD: ' + Convert(varchar(1000), IsNull(d.[CDDUser34],'Null')) + 
		' NEW: ' + Convert(varchar(1000), IsNull(i.[CDDUser34],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser35] Is Null And d.[CDDUser35] Is Not Null ) Or 
		(i.[CDDUser35] Is Not Null And d.[CDDUser35] Is Null ) Or 
		(i.[CDDUser35] != d.[CDDUser35])) 
		THEN '| COMMENTS4(CDDUser35) OLD: ' + Convert(varchar(1000), IsNull(d.[CDDUser35],'Null')) + 
		' NEW: ' + Convert(varchar(1000), IsNull(i.[CDDUser35],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser36] Is Null And d.[CDDUser36] Is Not Null ) Or 
		(i.[CDDUser36] Is Not Null And d.[CDDUser36] Is Null ) Or 
		(i.[CDDUser36] != d.[CDDUser36])) 
		THEN '| COMMENTS5(CDDUser36) OLD: ' + Convert(varchar(1000), IsNull(d.[CDDUser36],'Null')) + 
		' NEW: ' + Convert(varchar(1000), IsNull(i.[CDDUser36],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser37] Is Null And d.[CDDUser37] Is Not Null ) Or 
		(i.[CDDUser37] Is Not Null And d.[CDDUser37] Is Null ) Or 
		(i.[CDDUser37] != d.[CDDUser37])) 
		THEN '| CheckLists-FormType(CDDUser37) OLD: ' + Convert(varchar(1000), IsNull(d.[CDDUser37],'Null')) + 
		' NEW: ' + Convert(varchar(1000), IsNull(i.[CDDUser37],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser38] Is Null And d.[CDDUser38] Is Not Null ) Or 
		(i.[CDDUser38] Is Not Null And d.[CDDUser38] Is Null ) Or 
		(i.[CDDUser38] != d.[CDDUser38])) 
		THEN '| RE-VERIFICATION DATE(CDDUser38) OLD: ' + Convert(varchar(1000), IsNull(d.[CDDUser38],'Null')) + 
		' NEW: ' + Convert(varchar(1000), IsNull(i.[CDDUser38],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[CDDUser39] Is Null And d.[CDDUser39] Is Not Null ) Or 
		(i.[CDDUser39] Is Not Null And d.[CDDUser39] Is Null ) Or 
		(i.[CDDUser39] != d.[CDDUser39])) 
		THEN '| ProposedRisk(CDDUser39) OLD: ' + Convert(varchar(1000), IsNull(d.[CDDUser39],'Null')) + 
		' NEW: ' + Convert(varchar(1000), IsNull(i.[CDDUser39],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[cdduser40] Is Null And d.[cdduser40] Is Not Null ) Or 
		(i.[cdduser40] Is Not Null And d.[cdduser40] Is Null ) Or 
		(i.[cdduser40] != d.[cdduser40])) 
		THEN '| Comments(CDDUser40) OLD: ' + Convert(varchar(1000), IsNull(d.[cdduser40],'Null')) + 
		' NEW: ' + Convert(varchar(1000), IsNull(i.[cdduser40],'Null'))
		ELSE '' End +
	Case 
		WHEN ((i.[RelationshipWithBankAffl] Is Null And d.[RelationshipWithBankAffl] Is Not Null ) Or 
		(i.[RelationshipWithBankAffl] Is Not Null And d.[RelationshipWithBankAffl] Is Null ) Or 
		(i.[RelationshipWithBankAffl] != d.[RelationshipWithBankAffl])) 
		THEN '| HQ SharingList(RelationshipWithBankAffl) OLD: ' + case when IsNull(d.[RelationshipWithBankAffl],0) = 0 then 'False' else 'True' End + ' NEW: ' + case when IsNull(i.[RelationshipWithBankAffl],0) = 0 then 'False' else 'True' End
		ELSE '' End +		
	Case 
		WHEN ((i.[PrivInvestmentCompany] Is Null And d.[PrivInvestmentCompany] Is Not Null ) Or 
		(i.[PrivInvestmentCompany] Is Not Null And d.[PrivInvestmentCompany] Is Null ) Or 
		(i.[PrivInvestmentCompany] != d.[PrivInvestmentCompany])) 
		THEN '| PrivInvestmentCompany OLD: ' + case when IsNull(d.[PrivInvestmentCompany],0) = 0 then 'False' else 'True' End + ' NEW: ' + case when IsNull(i.[PrivInvestmentCompany],0) = 0 then 'False' else 'True' End
		ELSE '' End +		
	Case 
		WHEN ((i.[ResultsOfNegativePressSearch] Is Null And d.[ResultsOfNegativePressSearch] Is Not Null ) Or 
		(i.[ResultsOfNegativePressSearch] Is Not Null And d.[ResultsOfNegativePressSearch] Is Null ) Or 
		(i.[ResultsOfNegativePressSearch] != d.[ResultsOfNegativePressSearch])) 
		THEN '| ResultsOfNegativePressSearch OLD: ' + Convert(varchar, IsNull(d.[ResultsOfNegativePressSearch],'Null')) + 
		' NEW: ' + Convert(varchar, IsNull(i.[ResultsOfNegativePressSearch],'Null'))
		ELSE '' End 		

	From Inserted i, deleted d where  i.[CustomerId] = d.[CustomerId]
  End
GO


