USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_ReviewRFData]    Script Date: 4/27/2021 5:27:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER Procedure [dbo].[BSA_ReviewRFData] ( @Cust ObjectID, @oper SCode)
    As
 
	-- Start standard stored procedure transaction header
	declare @trnCnt int
	select @trnCnt = @@trancount	-- Save the current trancount
	--20200513 add
	declare @Mint int 
	--20200513 add

	If @trnCnt = 0
		-- Transaction has not begun
		begin tran BSA_ReviewRFData
	else
		-- Already in a transaction
		save tran BSA_ReviewRFData
	-- End standard stored procedure transaction header
	
	Declare @cnt 		int,
			@dt			DateTime, 
			@Score 		int, 
			@stat		int,
			@RiskClass	SCode,
			@txt		varchar(200),
			@IssueCty   varchar(100),
			@RvDt		Datetime

    --20200828 ZA if MgrOverRide must approve by MLCO,start
    Declare @isMgrOverRide int, @RatingCode varchar(100), @iNewCust int
	Declare @strNotes varchar(6000),@tmpNotes varchar(6000)
	Select @isMgrOverRide=0, @RatingCode='00'
	--20200828 ZA if MgrOverRide must approve by MLCO,End

	If Exists (Select * From RiskFactorData
			    Where Cust = @Cust And Status = 0) Begin

		-- Exit if any records found with non-initialized Score
		If dbo.BSA_CountMissingScores(@Cust) > 0 Begin
			Select @stat = 250010   -- DB_INV_PARAM
			GoTo EndOfProc
		End

		-- Find out score and RiskClass
		Select @Score = RiskScore,
		       @RiskClass = RiskClass
		from RiskScoreHist
		where CustomerId = @Cust and DateAccepted is null
		
	    --20200828 ZA if MgrOverRide must approve by MLCO,start
		select @isMgrOverRide = IsPersonalRelationship, @RatingCode = CDDUser25 from KYCData(nolock) where CustomerId=@Cust
		if @isMgrOverRide <> 1 and @RatingCode <> '10'
		
		If @Score Is Null Or @RiskClass Is Null Begin
			Select @stat = 250010   -- DB_INV_PARAM
			Goto EndOfProc
		End
		--20200828 ZA if MgrOverRide must approve by MLCO,end
		
		-- Delete Old accepted scores other than the ones
		--   recently accepted, for this customer
		Delete RiskFactorData
		 Where Cust = @Cust And Status = 1

		--Update Risk Score History
		update RiskScoreHist
		set DateAccepted= GetDate(),
			AcceptOper = @oper 
		where CustomerId = @Cust and DateAccepted is null
		
		-- Update Review Fields
		Select @dt = GetDate()
		Update RiskFactorData
		   Set Status = 1,			-- Reviewed/Accepted
			   LastReview = @dt,
			   LastReviewOper = @oper
		 Where Cust = @Cust And Status = 0
/*
		Select @stat = @@error, @cnt = @@rowcount
		If @stat <> 0
			Goto EndOfProc
		Else If @cnt = 0 Begin
			Select @stat = 250006	-- DB_UNKNOWN
			Goto EndOfProc
		End
*/
        ----20201127 add onboard input field of last reveiw date(cdduser38), Start
		--reference Store procedure : BSA_GenerateRFData, so mask under
/* mask 
		--20200513 add get riskclass of Probation days
		select @Mint=probationdays from RiskClass where code=@riskclass
		
		--20201126 polly add for South Africa Request: new cust lastreview must be 12 months,start			
		select @iNewCust=0
		select @iNewCust=1 from Customer where id=@Cust and ProbationStartDate is null and datediff(day,KYCDataCreateDate,getdate()) <10 
		if @iNewcust=1 
		   select @Mint=12
		--20201126 polly add for South Africa Request: new cust lastreview must be 12 months,end
		
		--20200729 add for partyid exist and expirdate is closed, start
		select @RvDt='', @IssueCty=''
		select @RvDt=convert(datetime,min(ExpiryDate),111) from PartyID 
		       where CustomerId=@Cust 
			   and ExpiryDate>=convert(datetime,GETDATE(),111)
			   and ExpiryDate<convert(datetime,dateadd(MONTH,@Mint,GETDATE()),111)
		
		----20201127 if issue contry= ZA, TW then expirydate=29991231
		----select @IssueCty=IssueCountry from PartyID 
		----       where CustomerId=@Cust and IssueCountry in ('TW','ZA')
		----	   group by IssueCountry
		----if ( @RvDt ='' or @RvDt is null ) or (@IssueCty='ZA' or @IssueCty='TW' or @IssueCty='' or @IssueCty is null)   
		
		if ( @RvDt ='' or @RvDt is null ) 
		   select @RvDt=convert(datetime,convert(char(10),dateadd(MONTH,@Mint,GETDATE()),111),111)
  		--20200729 add for partyid exist and expirdate is closed, end			
*/

       select @RvDt=convert(datetime,CDDUser38,101) from KYCData(nolock) where customerid=@Cust

       ----20201127 add onboard input field of last reveiw date(cdduser38), End

		-- Assign the RiskClass to the Customer
		-- after modifying RiskFactorData
		-- to avoid conflicts with locked risk class actions

		--20201126 polly add for Notes input over size bug fix , start
		--get notes size from system table
         Declare @iNotesLen int  
      	 --select @iNotesLen=2000
		 select @iNotesLen=b.max_length 
			from sys.tables a join sys.all_columns b on a.object_id = b.object_id 
			where a.name = 'CUSTOMER' and b.name = 'Notes'

	   --if notes size is not closed max length, just append data
		Select @tmpNotes='', @strNotes=''
		Select @tmpNotes = Notes from Customer where id=@Cust
		if len(@tmpNotes) < (@iNotesLen - 100)
		    Select @strNotes=@tmpNotes + char(13) + @oper + ', ' + convert(varchar(20),getdate(),111) + ' ' + left(convert(varchar(20),getdate(),114),8) + ' ' + ', Reviewed Risk Score Acceptance done.' 
		else
		   --if notes size is closed max length, insert to Event log , and get last time newline of data + this time data
		   begin
		     --insert into event log
		     Select @txt = 'Reviewed Risk Data of ' + @Cust + ', Notes PREVIOUS: ' + @tmpNotes
		     Exec BSA_InsEvent @oper, 'Rev', 'RFData', @Cust, @txt
			 
			 --get last time newline data + this time info
			 select @txt=''
			 Select @txt=reverse(substring(reverse(@tmpNotes),1,charindex(';01#&;31#&',reverse(@tmpNotes))-1 ))
			 Select @strNotes=@txt + char(13) + @oper + ', ' + convert(varchar(20),getdate(),111) + ' ' + left(convert(varchar(20),getdate(),114),8) + ' ' + ', Reviewed Risk Score Acceptance done.' 
		   end
		--20201126 polly add for Notes input over size bug fix , end

		--20200513 polly add assign lastreview, reviewdate	       
		Update Customer
		   Set RiskClass = @RiskClass, 
			   LastOper = @oper, LastModify = GetDate(), LastReview=GetDate(),ReviewDate=@RvDt, Notes = @strNotes  --dateadd(Month,@Mint,GETDATE())
		 Where [ID] = @Cust

		--Check if customer was updated
		Select @stat = @@error, 
			   @cnt = @@rowcount
		If @stat <> 0
		  Begin
			Goto EndOfProc
	      End
		Else If @cnt = 0 
		  Begin
			Select @stat = 250006 -- Not found
			Goto EndOfProc --exit
		  End

	End Else 
		Select @stat = 250006	-- DB_OBJECT_NOT_EXIST

EndOfProc: 
	-- Evaluate results of the transaction
	if (@stat = 0) begin
		If @trnCnt = 0
			commit tran BSA_ReviewRFData

		Select @txt = 'Reviewed Risk Data of ' + @Cust
		Exec BSA_InsEvent @oper, 'Rev', 'RFData', @Cust, @txt
	end else 
		rollback tran BSA_ReviewRFData
	
	return @stat
GO


