USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_UpdParty]    Script Date: 4/15/2021 2:07:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER Procedure [dbo].[BSA_UpdParty] ( @id as ObjectId, @parent as ObjectId, 
					@name as LongName1, @dBA as LongName, 
					@secCode as varchar(10), @address as Address,@city as City, 
					@state as State, @zip as Zip, @country as SCode, 
					@telephone as varchar(20), @email as varchar(60),
					@tIN as varchar(20), @LicenseNo As Varchar(35), @LicenseState As State,
                    @passportNo as varchar(20), 
					@dOB as GenericDate, @typeOfBusiness as varchar(80),
					@sourceOfFunds as varchar(80), @accountOfficer as LongName,
					@acctOffTel as varchar(20), @acctOffEmail as varchar(60),
					@compOfficer as LongName, @compOffTel as varchar(20),
					@compOffEmail as varchar(60), @idList as varchar(255),
					@notes as varchar(2000), @exemptionStatus as SCode,
					@lastActReset as GenericTime,
					@lastReview as GenericTime, @lastReviewOper as SCode,
					@ownerBranch SCode, @ownerDept SCode, @ownerOper SCode, 
					@riskClass SCode, @oper SCode, @ts timestamp,
					@countryOfOrigin SCode, @countryOfIncorp SCode, 
					@OffshoreCorp int, @BearerShares bit, @NomeDePlume bit,
					@Type SCode,
					@Resident Bit, @BusRelationNature VarChar(40),
					@PrevRelations VarChar(40),	
					@PEP Bit, @PoliticalPos VarChar(40), @FEP Bit, @MSB Bit,
					@CorrBankRelation bit, 
					@Sex Char(1), @ShellBank Bit, @HighRiskRespBank Bit,
					@OffshoreBank Bit, @PayThroughAC Bit, 
					@RegulatedAffiliate Bit, @USPerson VarChar(40),
					@RFCalculate Bit, @Status Int, @ReviewDate DateTime,
					@CTRAmt money, @User1 varchar(40), @User2 varchar(40),
					@User3 varchar(40), @User4 varchar(40), 
					@User5 varchar(40),
					@SwiftTID varchar(11), @embassy bit, 
					@ForeignGovt bit, @charityOrg bit, @DoCIP bit,
					@Prospect bit, @AssetSize Money, @Income Money,
					@IndOrBusType char(1),
					@profiledCountries Varchar(4000), @OnProbation Bit, 
					@ProbationReason SCode, @ProbationStartDate as GenericDate,
					@ProbationEndDate as GenericDate,
					@Closed Bit, @ClosedDate as GenericDate ,@ClosedReason ObjectId , 
					@OpenDate as GenericDate ,
					@CountryofResidence SCode,@CountryofCitizenship SCode,
					@KYCStatus int,
					@KYCDataCreateDate datetime,@KYCDataModifyDate datetime,
					@NamePrefix varchar(25),	@FirstName varchar(50),
					@MiddleName varchar(50),	@LastName varchar(50),		@NameSuffix varchar(25),
					@Alias varchar(50),		@PlaceofBirth varchar(50),	@MothersMaidenName varchar(50),
					@Introducer varchar(50),	@YrsAtCurrAddress int,		@NoOfDependants int,
					@EmployerName varchar(50),	@JobTitle varchar(50),		@JobTime varchar(50),
					@LengthOfEmp varchar(50),	@IsAlien bit,			@CountryofDualCitizenship SCODE,
					@SecondaryEmployer varchar(50),	@OriginOfFunds varchar(50),	@IsPersonalRelationship bit,
					@DateOfInception datetime,	@LegalEntityType varchar(50),	@NoofEmployees int,
					@TotalAnnualSales money,	@ProductsSold varchar(255),	@ServicesProvided varchar(255),
					@GeographicalAreasServed varchar(255),@IsPubliclyTraded bit,	@TickerSymbol varchar(10),
					@Exchange varchar(50),		@IsPensionFund bit,		@IsEndowmentFund bit,
					@IsGovernmentSponsored bit,	@IsSubsidiary bit,		@IsForeignCorrBnk bit,
					@RequireCashTrans bit,		@RequireForeignTrans bit,	@RequireWireTrans bit,
					@HasForeignBranches bit,	@ForeignBranchCountries varchar(255),@TypeofMonInstruments varchar(255),
					@URL varchar(255),		@HasPatriotActCertification bit,@PatriotActCertExpiryDate datetime,
					@MoneyOrders bit,		@MORegistration varchar(10),	@MORegConfirmationNo varchar(50),
					@MOActingasAgentfor varchar(20),@MOMaxDailyAmtPerPerson money,	@MOMonthlyAmt money,
					@MOPercentIncome varchar(5),	@TravelersCheck bit,		@TCRegistration varchar(10),
					@TCRegConfirmationNo varchar(50),@TCActingasAgentfor varchar(20),@TCMaxDailyAmtPerPerson money,
					@TCMonthlyAmt money,		@TCPercentIncome varchar(5),	@MoneyTransmission bit,
					@MTRegistration varchar(10),	@MTRegConfirmationNo varchar(50),@MTActingasAgentfor varchar(20),
					@MTMaxDailyAmtPerPerson money,	@MTMonthlyAmt money,		@MTPercentIncome varchar(5),
					@CheckCashing bit,		@CCRegistration varchar(10),	@CCRegConfirmationNo varchar(50),
					@CCActingasAgentfor varchar(20),@CCMaxDailyAmtPerPerson money,	@CCMonthlyAmt money,
					@CCPercentIncome varchar(5),	@CurrencyExchange bit,		@CERegistration varchar(10),
					@CERegConfirmationNo varchar(50),@CEActingasAgentfor varchar(20),@CEMaxDailyAmtPerPerson money,
					@CEMonthlyAmt money,		@CEPercentIncome varchar(5),	@CurrencyDealing bit,
					@CDRegistration varchar(10),	@CDRegConfirmationNo varchar(50),@CDActingasAgentfor varchar(20),
					@CDMaxDailyAmtPerPerson money,	@CDMonthlyAmt money,		@CDPercentIncome varchar(5),
					@StoredValue bit,		@SVRegistration varchar(10),	@SVRegConfirmationNo varchar(50),
					@SVActingasAgentfor varchar(20),@SVMaxDailyAmtPerPerson money,	@SVMonthlyAmt money,
					@SVPercentIncome varchar(5),   @CDDUser1 varchar(40),         	@CDDUser2 varchar(40),
					@CDDUser3 varchar(40),         @CDDUser4 varchar(40),		@CDDUser5 varchar(40),         
					@CDDUser6 varchar(40),         @CDDUser7 varchar(40),		@CDDUser8 varchar(40),         
					@CDDUser9 varchar(40),         @CDDUser10 varchar(40),		@CDDUser11 varchar(40),         
					@CDDUser12 varchar(40),        @CDDUser13 varchar(40),		@CDDUser14 varchar(40),         
					@CDDUser15 varchar(40),        @CDDUser16 varchar(40),		@CDDUser17 varchar(40),         
					@CDDUser18 varchar(40),        @CDDUser19 varchar(40),		@CDDUser20 varchar(40),         
					@CDDUser21 varchar(40),        @CDDUser22 varchar(40),		@CDDUser23 varchar(40),         
					@CDDUser24 varchar(40),        @CDDUser25 varchar(40),		@CDDUser26 varchar(40),         
					@CDDUser27 varchar(40),        @CDDUser28 varchar(40),		@CDDUser29 varchar(40),         
					@CDDUser30 varchar(40),        @CDDUser31 varchar(1000),		@CDDUser32 varchar(1000),         
					@CDDUser33 varchar(1000),       @CDDUser34 varchar(1000),		@CDDUser35 varchar(1000),         
					@CDDUser36 varchar(1000),       @CDDUser37 varchar(1000),		@CDDUser38 varchar(1000),         
					@CDDUser39 varchar(1000),       @CDDUser40 varchar(3000),		@kdts timestamp,@AreKYCFieldsPresent bit,
					--		Begin: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw
					@RelationshipWithBankAffl bit ,					-- Relationship with Bank Affiliate
					@PrivInvestmentCompany bit ,					-- Private Investiment Company
					@ResultsOfNegativePressSearch varchar(255)      -- Results of Negative Press Search	    
					--		End: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw	    					
)
  As

  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_UpdParty
  else
    -- Already in a transaction
	save tran BSA_UpdParty
  -- End standard stored procedure transaction header

  declare	@stat int, 
			@cnt int


  if @parent = '' Select @parent = @id

  Update Customer Set 
		Id = NullIf(RTrim(@id), ''), Parent = NullIf(RTrim(@parent), ''), 
		Name = NullIf(RTrim(@name), ''), DBA = NullIf(RTrim(@dBA), ''), 
		SecCode = NullIf(RTrim(@secCode), ''), 
		Address = NullIf(RTrim(@address), ''), City = NullIf(RTrim(@city), ''), 
		State = NullIf(RTrim(@state), ''), Zip = NullIf(RTrim(@zip), ''), 
		Country = NullIf(RTrim(@country), ''), 
		Telephone = NullIf(RTrim(@telephone), ''), 
		Email = NullIf(RTrim(@email), ''), TIN = NullIf(RTrim(@tIN), ''), 
		LicenseNo = NullIf(RTrim(@LicenseNo), ''), 
		LicenseState = NullIf(RTrim(@LicenseState), ''),
		PassportNo = NullIf(RTrim(@passportNo), ''), DOB = NullIf(@dOB, 0), 
		TypeOfBusiness = NullIf(RTrim(@typeOfBusiness), ''), 
		SourceOfFunds = NullIf(RTrim(@sourceOfFunds), ''), 
		AccountOfficer = NullIf(RTrim(@accountOfficer), ''), 
		AcctOffTel = NullIf(RTrim(@acctOffTel), ''), 
		AcctOffEmail = NullIf(RTrim(@acctOffEmail), ''), 
		CompOfficer = NullIf(RTrim(@compOfficer), ''), 
		CompOffTel = NullIf(RTrim(@compOffTel), ''), 
		CompOffEmail = NullIf(RTrim(@compOffEmail), ''), 
		IdList = NullIf(RTrim(@idList), ''), Notes = NullIf(RTrim(@notes), ''), 
		ExemptionStatus = NullIf(RTrim(@exemptionStatus), ''),
		LastActReset = NullIf(@lastActReset, 0), 
		LastReview = NullIf(@lastReview, 0), 
		LastReviewOper = NullIf(RTrim(@lastReviewOper), ''), 
		LastOper = RTrim(@oper),
		OwnerBranch = NullIf(RTrim(@ownerBranch), ''), 
		OwnerDept = NullIf(RTrim(@ownerDept), ''), 
		OwnerOper = NullIf(RTrim(@ownerOper), ''),
		RiskClass = @riskClass,
		CountryOfOrigin = NullIf(RTrim(@countryOfOrigin), ''),
		CountryOfIncorp= NullIf(RTrim(@countryOfIncorp), ''), 
		OffshoreCorp = NullIf(@OffshoreCorp, 0),
		BearerShares = @BearerShares,
		NomeDePlume = @NomeDePlume,
		Type = NullIf(@Type, ''),
		Resident = @Resident, BusRelationNature = NullIf(@BusRelationNature,''),
		PrevRelations = NullIf(@PrevRelations,''), 
		PEP = @PEP, PoliticalPos = NullIf(@PoliticalPos,''), FEP = @FEP, MSB = @MSB,
		CorrBankRelation = @CorrBankRelation,
		Sex = NullIf(@Sex,''), ShellBank = @ShellBank,
		HighRiskRespBank = @HighRiskRespBank, OffshoreBank = @OffshoreBank,
		PayThroughAC = @PayThroughAC, RegulatedAffiliate = @RegulatedAffiliate,
		USPerson = NullIf(@USPerson,''), RFCalculate = @RFCalculate,
		Status = @Status, ReviewDate = @ReviewDate,
		CTRAmt = @CTRAmt,
		User1 = NullIf(@User1,''), 
		User2 = NullIf(@User2,''), 
		User3 = NullIf(@User3,''), 
		User4 = NullIf(@User4,''),
		User5 = NullIf(@User5,''),
		SwiftTID = NUllIf(RTrim(@SwiftTID),''), 
		Embassy = @embassy, 
		ForeignGovt = @ForeignGovt, 
		CharityOrg = @CharityOrg, 
		DoCIP = @DoCIP,
		Prospect = @Prospect,
		AssetSize = NullIf(@AssetSize,0),
		Income = NullIf(@Income,0),
		IndOrBusType = NullIf(RTrim(@IndOrBusType),''),
		OnProbation = @OnProbation,
		ProbationReason = NullIf(@ProbationReason, ''),
		ProbationStartDate = NullIf(@ProbationStartDate, 0),
		ProbationEndDate = NullIf(@ProbationEndDate, 0),
		Closed = @Closed, 
		ClosedDate = NullIf(@ClosedDate, 0),
		ClosedReason = NullIf(@ClosedReason, ''), 
		OpenDate = NullIf(@OpenDate, 0), 
		CountryofResidence = NullIf(RTrim(@CountryofResidence), ''), 
		CountryofCitizenship = NullIf(RTrim(@CountryofCitizenship), '') ,
		KYCDataCreateDate = @KYCDataCreateDate,
		KYCDataModifyDate = @KYCDataModifyDate,
		KYCStatus = @KYCStatus		 		
  Where Id = @id and Ts = @ts
  Select @stat = @@error, @cnt = @@rowcount
  -- Evaluate results of the transaction
  If ( @stat = 0 and @cnt = 0 ) 
	  Select @stat = 250001	-- Concurrent Update
  else
	 if @stat = 0 begin
		-- Insert Profile Countries (Comma delimited with a comma at the end)
		if len(@profiledCountries) > 0 begin
			exec BSA_InsCountryOwners @profiledCountries, @id, @oper
			select stat = @@error
		end
	  end


  If ( @stat <> 0 ) begin
	rollback tran BSA_UpdParty
	return @stat
  end
  
  
  If @AreKYCFieldsPresent = 1 begin
  declare @custcount int
  select @custcount= Count(*) from kycdata where customerid=@id
  -- custcount is zero then add kycdata details as not all customers will have kycdata.
  if @custcount = 0 begin
		Insert Into KYCData (	CustomerId, 	NamePrefix ,		FirstName ,
	     		MiddleName ,		LastName ,		NameSuffix ,
	     		Alias ,			PlaceofBirth ,		MothersMaidenName ,
	     		Introducer,		YrsAtCurrAddress,	NoOfDependants ,
	     		EmployerName,		JobTitle ,		JobTime , 
	     		LengthOfEmp,		IsAlien ,		CountryofDualCitizenship,
	     		SecondaryEmployer ,	OriginOfFunds ,		IsPersonalRelationship ,
	     		DateOfInception,	LegalEntityType,	NoofEmployees ,
	     		TotalAnnualSales ,	ProductsSold ,		ServicesProvided ,
	     		GeographicalAreasServed,IsPubliclyTraded,	TickerSymbol ,
	     		Exchange ,		IsPensionFund ,		IsEndowmentFund ,
	     		IsGovernmentSponsored ,	IsSubsidiary ,		IsForeignCorrBnk ,
	     		RequireCashTrans ,	RequireForeignTrans , 	RequireWireTrans ,
	     		HasForeignBranches ,	ForeignBranchCountries ,TypeofMonInstruments ,
	     		URL ,			HasPatriotActCertification ,PatriotActCertExpiryDate ,
	     		MoneyOrders ,		MORegistration ,	MORegConfirmationNo ,
	     		MOActingasAgentfor ,	MOMaxDailyAmtPerPerson ,MOMonthlyAmt ,
	     		MOPercentIncome ,	TravelersCheck ,	TCRegistration ,
	     		TCRegConfirmationNo ,	TCActingasAgentfor ,	TCMaxDailyAmtPerPerson ,
	     		TCMonthlyAmt ,		TCPercentIncome ,	MoneyTransmission ,
	     		MTRegistration ,	MTRegConfirmationNo,	MTActingasAgentfor ,
	     		MTMaxDailyAmtPerPerson ,MTMonthlyAmt ,		MTPercentIncome ,
	     		CheckCashing ,		CCRegistration ,	CCRegConfirmationNo ,
	     		CCActingasAgentfor ,	CCMaxDailyAmtPerPerson ,CCMonthlyAmt ,
	     		CCPercentIncome ,	CurrencyExchange ,	CERegistration ,
	     		CERegConfirmationNo ,	CEActingasAgentfor ,	CEMaxDailyAmtPerPerson ,
	     		CEMonthlyAmt ,		CEPercentIncome ,	CurrencyDealing ,
	     		CDRegistration ,	CDRegConfirmationNo ,	CDActingasAgentfor ,
	     		CDMaxDailyAmtPerPerson ,CDMonthlyAmt ,		CDPercentIncome ,
	     		StoredValue ,		SVRegistration ,	SVRegConfirmationNo ,
	     		SVActingasAgentfor ,	SVMaxDailyAmtPerPerson ,SVMonthlyAmt,
	     		SVPercentIncome ,	CDDUser1,		CDDUser2,
	     		CDDUser3,		CDDUser4,		CDDUser5,
	     		CDDUser6,		CDDUser7,		CDDUser8,
	     		CDDUser9,		CDDUser10,		CDDUser11,
	     		CDDUser12,		CDDUser13,		CDDUser14,
	     		CDDUser15,		CDDUser16,		CDDUser17,
	     		CDDUser18,		CDDUser19,		CDDUser20,
	     		CDDUser21,		CDDUser22,		CDDUser23,
	     		CDDUser24,		CDDUser25,		CDDUser26,
	     		CDDUser27,		CDDUser28,		CDDUser29,
	     		CDDUser30,		CDDUser31,		CDDUser32,
	     		CDDUser33,		CDDUser34,		CDDUser35,
	     		CDDUser36,		CDDUser37,		CDDUser38,
	     		CDDUser39,		CDDUser40,		CreateOper,
	     		CreateDate,
				--		Begin: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw
				RelationshipWithBankAffl,		-- Relationship with Bank Affiliate
				PrivInvestmentCompany,			-- Private Investiment Company
				ResultsOfNegativePressSearch    -- Results of Negative Press Search	    
				--		End: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw	    					  			     		
	    ) 
	    values (RTrim(@id),NullIf(RTrim(@nameprefix),''), NullIf(RTrim(@firstname),''), 
	     		NullIf(RTrim(@middlename),''),	NullIf(RTrim(@lastname),''),	NullIf(RTrim(@namesuffix),''),
	     		NullIf(RTrim(@alias),''),	NullIf(RTrim(@placeofbirth),''),NullIf(RTrim(@mothersmaidenname),''),
	     		NullIf(RTrim(@introducer),''),	NullIf(@yrsatcurraddress,0),	NullIf(@noofdependants,0),
	     		NullIf(RTrim(@EmployerName),''),NullIf(RTrim(@jobtitle),''),	NullIf(RTrim(@jobtime),''),
	     		NullIF(RTRIM(@LengthOfEmp),''),	@isalien,			NullIf(RTrim(@countryofdualcitizenship),''),
	     		NullIf(RTrim(@secondaryemployer),''),NullIf(RTrim(@OriginOfFunds),''),@ispersonalrelationship,
	     		@dateofinception,		NullIf(RTrim(@legalentitytype),''),@noofemployees,
	     		@totalannualsales,		NullIf(RTrim(@productssold), ''),NullIf(RTrim(@servicesprovided), ''),
	     		NullIf(RTrim(@geographicalareasserved), ''),@IsPubliclyTraded,	NullIf(RTrim(@tickersymbol), ''),
	     		NullIf(RTrim(@exchange), ''),	@ispensionfund,			@isendowmentfund,
	     		@isgovernmentsponsored,		@issubsidiary,			@isforeigncorrbnk,
	     		@requirecashtrans,		@requireforeigntrans,		@requirewiretrans,
	     		@hasforeignbranches,		NullIf(RTrim(@ForeignBranchCountries), ''), NullIf(RTrim(@TypeofMonInstruments), ''), 
	     		NullIf(RTrim(@URL), ''),	@HasPatriotActCertification,	@PatriotActCertExpiryDate,
	     		@moneyorders,			NullIf(RTrim(@MORegistration), ''), NullIf(RTrim(@MORegConfirmationNo), ''), 
	     		NullIf(RTrim(@MOActingasAgentfor), ''),@MOMaxDailyAmtPerPerson,	@MOMonthlyAmt,
	     		NullIf(RTrim(@MOPercentIncome), ''),@TravelersCheck ,		NullIf(RTrim(@TCRegistration), '') ,
	     		NullIf(RTrim(@TCRegConfirmationNo), '') ,NullIf(RTrim(@TCActingasAgentfor), '') ,@TCMaxDailyAmtPerPerson ,
	     		@TCMonthlyAmt,			NullIf(RTrim(@TCPercentIncome), ''),@MoneyTransmission,
	     		NullIf(RTrim(@MTRegistration), '') ,NullIf(RTrim(@MTRegConfirmationNo), '') ,NullIf(RTrim(@MTActingasAgentfor), '') ,
	     		@MTMaxDailyAmtPerPerson ,	@MTMonthlyAmt ,			NullIf(RTrim(@MTPercentIncome), '') ,
	     		@CheckCashing ,			NullIf(RTrim(@CCRegistration), '') ,NullIf(RTrim(@CCRegConfirmationNo), '') ,
	     		NullIf(RTrim(@CCActingasAgentfor), ''), @CCMaxDailyAmtPerPerson ,@CCMonthlyAmt ,
	     		NullIf(RTrim(@CCPercentIncome), '') ,@CurrencyExchange ,	NullIf(RTrim(@CERegistration), '') ,
	     		NullIf(RTrim(@CERegConfirmationNo), ''),NullIf(RTrim(@CEActingasAgentfor), '') ,@CEMaxDailyAmtPerPerson ,
	     		@CEMonthlyAmt ,			NullIf(RTrim(@CEPercentIncome), ''),@CurrencyDealing ,
	     		NullIf(RTrim(@CDRegistration), '') ,NullIf(RTrim(@CDRegConfirmationNo), '') ,NullIf(RTrim(@CDActingasAgentfor), ''),
	     		@CDMaxDailyAmtPerPerson ,	@CDMonthlyAmt,			NullIf(RTrim(@CDPercentIncome), '') ,
	     		@StoredValue ,			NullIf(RTrim(@SVRegistration), '') ,NullIf(RTrim(@SVRegConfirmationNo), '') ,
	     		NullIf(RTrim(@SVActingasAgentfor), ''), @SVMaxDailyAmtPerPerson , @SVMonthlyAmt,
	     		NullIf(RTrim(@SVPercentIncome), '') , NullIf(RTrim(@CDDUser1) ,''), NullIf(RTrim(@CDDUser2) ,''),
	     		NullIf(RTrim(@CDDUser3) ,''),	NullIf(RTrim(@CDDUser4) ,''),	NullIf(RTrim(@CDDUser5) ,''),         
	     		NullIf(RTrim(@CDDUser6) ,''),    NullIf(RTrim(@CDDUser7) ,''),	NullIf(RTrim(@CDDUser8) ,''),         
	     		NullIf(RTrim(@CDDUser9) ,''),    NullIf(RTrim(@CDDUser10) ,''),	NullIf(RTrim(@CDDUser11) ,''),         
	     		NullIf(RTrim(@CDDUser12) ,''),   NullIf(RTrim(@CDDUser13) ,''),	NullIf(RTrim(@CDDUser14) ,''),         
	     		NullIf(RTrim(@CDDUser15) ,''),   NullIf(RTrim(@CDDUser16) ,''),	NullIf(RTrim(@CDDUser17) ,''),         
	     		NullIf(RTrim(@CDDUser18) ,''),   NullIf(RTrim(@CDDUser19) ,''),	NullIf(RTrim(@CDDUser20) ,''),         
	     		NullIf(RTrim(@CDDUser21) ,''),   NullIf(RTrim(@CDDUser22) ,''),	NullIf(RTrim(@CDDUser23) ,''),         
	     		NullIf(RTrim(@CDDUser24) ,''),   NullIf(RTrim(@CDDUser25) ,''),	NullIf(RTrim(@CDDUser26) ,''),         
	     		NullIf(RTrim(@CDDUser27) ,''),   NullIf(RTrim(@CDDUser28) ,''),	NullIf(RTrim(@CDDUser29) ,''),         
	     		NullIf(RTrim(@CDDUser30) ,''),   NullIf(RTrim(@CDDUser31) ,''),	NullIf(RTrim(@CDDUser32) ,''),         
	     		NullIf(RTrim(@CDDUser33) ,''),   NullIf(RTrim(@CDDUser34) ,''),	NullIf(RTrim(@CDDUser35) ,''),         
	     		NullIf(RTrim(@CDDUser36) ,''),   NullIf(RTrim(@CDDUser37) ,''),	NullIf(RTrim(@CDDUser38) ,''),         
	     		NullIf(RTrim(@CDDUser39) ,''),   NullIf(RTrim(@CDDUser40) ,''),	RTrim(@Oper),
	   			GetDate(),
				--		Begin: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw
				@RelationshipWithBankAffl,							-- Relationship with Bank Affiliate
				@PrivInvestmentCompany,								-- Private Investiment Company
				NullIf(RTrim(@ResultsOfNegativePressSearch) ,'')	-- Results of Negative Press Search	    
				--		End: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw	    					  				
	   	)  
   		
   		Select @stat = @@error
   		
   		 -- Evaluate results of the transaction
   		If @stat <> 0 begin
   			 rollback tran BSA_UpdParty
   			 return @stat
  		end
  	
  	end 
  	else 	
	    Update KYCData Set NamePrefix = NullIf(RTrim(@nameprefix),''),
				FirstName=NullIf(RTrim(@firstname),'') ,
				MiddleName=NullIf(RTrim(@middlename),'') ,
				LastName=NullIf(RTrim(@lastname),'') ,
				NameSuffix= NullIf(RTrim(@namesuffix),''),
				Alias=NullIf(RTrim(@alias),'') ,
				PlaceofBirth=NullIf(RTrim(@placeofbirth),'') ,
				MothersMaidenName=NullIf(RTrim(@mothersmaidenname),'') ,
				Introducer = NullIf(RTrim(@Introducer),''),
				YrsAtCurrAddress=NullIf(@yrsatcurraddress,0),
				NoOfDependants=NullIf(@noofdependants,0) ,
				EmployerName = NullIf(RTrim(@employername),''),
				JobTitle=NullIf(RTrim(@jobtitle),'') ,
				JobTime=NullIf(RTrim(@jobtime),'') ,
				LengthOfEmp= NullIf(RTrim(@LengthOfEmp),''),			
				IsAlien=@isalien,
				CountryofDualCitizenship=NullIf(RTrim(@countryofdualcitizenship),''),
				SecondaryEmployer=NullIf(RTrim(@secondaryemployer),'') ,
				OriginOfFunds=NullIf(RTrim(@OriginOfFunds),'') ,
				IsPersonalRelationship =@ispersonalrelationship,
				DateOfInception=@dateofinception,
				LegalEntityType=NullIf(RTrim(@legalentitytype),'') ,
				NoofEmployees=@noofemployees,
				TotalAnnualSales= @totalannualsales,
				ProductsSold=NullIf(RTrim(@productssold), '') ,
				ServicesProvided=NullIf(RTrim(@servicesprovided), '') ,
				GeographicalAreasServed=NullIf(RTrim(@geographicalareasserved), '') ,
				IsPubliclyTraded=@IsPubliclyTraded ,
				TickerSymbol=NullIf(RTrim(@tickersymbol), '') ,
				Exchange=NullIf(RTrim(@exchange), '') ,
				IsPensionFund=@ispensionfund ,
				IsEndowmentFund=@isendowmentfund ,
				IsGovernmentSponsored=@isgovernmentsponsored,
				IsSubsidiary=@issubsidiary,
				IsForeignCorrBnk=@isforeigncorrbnk ,
				RequireCashTrans=@requirecashtrans ,
				RequireForeignTrans=@requireforeigntrans ,
				RequireWireTrans=@requirewiretrans ,
				HasForeignBranches=@hasforeignbranches,
				ForeignBranchCountries=NullIf(RTrim(@ForeignBranchCountries), '') ,
				TypeofMonInstruments=NullIf(RTrim(@TypeofMonInstruments), ''),
				URL=NullIf(RTrim(@URL), ''),
				HasPatriotActCertification=@HasPatriotActCertification ,
				PatriotActCertExpiryDate=@PatriotActCertExpiryDate ,
				MoneyOrders=@moneyorders ,
				MORegistration=NullIf(RTrim(@MORegistration), '') ,
				MORegConfirmationNo=NullIf(RTrim(@MORegConfirmationNo), '') ,
				MOActingasAgentfor=NullIf(RTrim(@MOActingasAgentfor), ''),
				MOMaxDailyAmtPerPerson=@MOMaxDailyAmtPerPerson ,
				MOMonthlyAmt=@MOMonthlyAmt ,
				MOPercentIncome=NullIf(RTrim(@MOPercentIncome), ''),
				TravelersCheck=@TravelersCheck,
				TCRegistration=NullIf(RTrim(@TCRegistration), ''),
				TCRegConfirmationNo=NullIf(RTrim(@TCRegConfirmationNo), '') ,
				TCActingasAgentfor=NullIf(RTrim(@TCActingasAgentfor), '') ,
				TCMaxDailyAmtPerPerson=@TCMaxDailyAmtPerPerson ,
				TCMonthlyAmt=@TCMonthlyAmt ,
				TCPercentIncome=NullIf(RTrim(@TCPercentIncome), ''),
				MoneyTransmission=@MoneyTransmission ,
				MTRegistration=NullIf(RTrim(@MTRegistration), ''),
				MTRegConfirmationNo=NullIf(RTrim(@MTRegConfirmationNo), '') ,
				MTActingasAgentfor =NullIf(RTrim(@MTActingasAgentfor), ''),
				MTMaxDailyAmtPerPerson=@MTMaxDailyAmtPerPerson ,
				MTMonthlyAmt=@MTMonthlyAmt,
				MTPercentIncome=NullIf(RTrim(@MTPercentIncome), '') ,
				CheckCashing =@CheckCashing,
				CCRegistration= NullIf(RTrim(@CCRegistration), ''),
				CCRegConfirmationNo= NullIf(RTrim(@CCRegConfirmationNo), ''),
				CCActingasAgentfor=NullIf(RTrim(@CCActingasAgentfor), '') ,
				CCMaxDailyAmtPerPerson=@CCMaxDailyAmtPerPerson ,
				CCMonthlyAmt=@CCMonthlyAmt ,
				CCPercentIncome=NullIf(RTrim(@CCPercentIncome), ''),
				CurrencyExchange=@CurrencyExchange ,
				CERegistration=NullIf(RTrim(@CERegistration), '') ,
				CERegConfirmationNo=NullIf(RTrim(@CERegConfirmationNo), '') ,
				CEActingasAgentfor=NullIf(RTrim(@CEActingasAgentfor), '') , 
				CEMaxDailyAmtPerPerson=@CEMaxDailyAmtPerPerson ,
				CEMonthlyAmt=@CEMonthlyAmt,
				CEPercentIncome=NullIf(RTrim(@CEPercentIncome), '') ,
				CurrencyDealing=@CurrencyDealing,
				CDRegistration=NullIf(RTrim(@CDRegistration), '') , 
				CDRegConfirmationNo= NullIf(RTrim(@CDRegConfirmationNo), ''),
				CDActingasAgentfor =NullIf(RTrim(@CDActingasAgentfor), ''),
				CDMaxDailyAmtPerPerson=@CDMaxDailyAmtPerPerson ,
				CDMonthlyAmt=@CDMonthlyAmt ,
				CDPercentIncome=NullIf(RTrim(@CDPercentIncome), '') , 
				StoredValue=@StoredValue ,
				SVRegistration=NullIf(RTrim(@SVRegistration), '') ,
				SVRegConfirmationNo=NullIf(RTrim(@SVRegConfirmationNo), '') ,
				SVActingasAgentfor=NullIf(RTrim(@SVActingasAgentfor), '') ,
				SVMaxDailyAmtPerPerson=@SVMaxDailyAmtPerPerson ,
				SVMonthlyAmt=@SVMonthlyAmt,
				SVPercentIncome=NullIf(RTrim(@SVPercentIncome),''),
				CDDUser1 = NullIf(RTrim(@CDDUser1) ,''),         
				CDDUser2 = NullIf(RTrim(@CDDUser2) ,''),
				CDDUser3 = NullIf(RTrim(@CDDUser3) ,''),         
				CDDUser4 = NullIf(RTrim(@CDDUser4) ,''),		
				CDDUser5 = NullIf(RTrim(@CDDUser5) ,''),         
				CDDUser6 = NullIf(RTrim(@CDDUser6) ,''),         
				CDDUser7 = NullIf(RTrim(@CDDUser7) ,''),		
				CDDUser8 = NullIf(RTrim(@CDDUser8) ,''),         
				CDDUser9 = NullIf(RTrim(@CDDUser9) ,''),         
				CDDUser10 = NullIf(RTrim(@CDDUser10) ,''),	
				CDDUser11 = NullIf(RTrim(@CDDUser11) ,''),         
				CDDUser12 = NullIf(RTrim(@CDDUser12) ,''),        
				CDDUser13 = NullIf(RTrim(@CDDUser13) ,''),	
				CDDUser14 = NullIf(RTrim(@CDDUser14) ,''),         
				CDDUser15 = NullIf(RTrim(@CDDUser15) ,''),        
				CDDUser16 = NullIf(RTrim(@CDDUser16) ,''),	
				CDDUser17 = NullIf(RTrim(@CDDUser17) ,''),         
				CDDUser18 = NullIf(RTrim(@CDDUser18) ,''),        
				CDDUser19 = NullIf(RTrim(@CDDUser19) ,''),	
				CDDUser20 = NullIf(RTrim(@CDDUser20) ,''),         
				CDDUser21 = NullIf(RTrim(@CDDUser21) ,''),         
				CDDUser22 = NullIf(RTrim(@CDDUser22) ,''),		
				CDDUser23 = NullIf(RTrim(@CDDUser23) ,''),         
				CDDUser24 = NullIf(RTrim(@CDDUser24) ,''),         
				CDDUser25 = NullIf(RTrim(@CDDUser25) ,''),		
				CDDUser26 = NullIf(RTrim(@CDDUser26) ,''),         
				CDDUser27 = NullIf(RTrim(@CDDUser27) ,''),         
				CDDUser28 = NullIf(RTrim(@CDDUser28) ,''),		
				CDDUser29 = NullIf(RTrim(@CDDUser29) ,''),         
				CDDUser30 = NullIf(RTrim(@CDDUser30) ,''),         
				CDDUser31 = NullIf(RTrim(@CDDUser31) ,''),		
				CDDUser32 = NullIf(RTrim(@CDDUser32) ,''),         
				CDDUser33 = NullIf(RTrim(@CDDUser33) ,''),         
				CDDUser34 = NullIf(RTrim(@CDDUser34) ,''),		
				CDDUser35 = NullIf(RTrim(@CDDUser35) ,''),         
				CDDUser36 = NullIf(RTrim(@CDDUser36) ,''),         
				CDDUser37 = NullIf(RTrim(@CDDUser37) ,''),		
				CDDUser38 = NullIf(RTrim(@CDDUser38) ,''),         
				CDDUser39 = NullIf(RTrim(@CDDUser39) ,''),         
				CDDUser40 = NullIf(RTrim(@CDDUser40) ,''),
	            LastOper =NullIf(RTrim(@oper),''),
				--		Begin: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw
				RelationshipWithBankAffl = @RelationshipWithBankAffl,							-- Relationship with Bank Affiliate
				PrivInvestmentCompany = @PrivInvestmentCompany,									-- Private Investiment Company
				ResultsOfNegativePressSearch = NullIf(RTrim(@ResultsOfNegativePressSearch) ,'')	-- Results of Negative Press Search	    
				--		End: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw	    					  					            
       Where CustomerId = @id and Ts = @kdts
	   Select @stat = @@error, @cnt = @@rowcount
       If ( @stat = 0 and @cnt = 0 )
           Select @stat = 250001 -- Concurrent Update
       If ( @stat <> 0 ) begin
           rollback tran BSA_UpdParty
           return @stat
	   end  
  End 
  
  
  If @trnCnt = 0
 	  commit tran BSA_UpdParty
  return @stat
GO


