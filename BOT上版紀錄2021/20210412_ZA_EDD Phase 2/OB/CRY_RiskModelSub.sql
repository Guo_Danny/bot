USE [PBSA]
GO

ALTER PROCEDURE [dbo].[CRY_RiskModelSub] ( @RFICode SCode)
    As

	Select rfv.ID, rfv.RFICode, isnull(lv.Description, rfv.Value) Value, rfv.Score
	  From RiskFactorValue rfv
	  left join pcdb..ListValue lv on charindex(lv.ListTypeCode,rfv.rficode) > 0 and lv.Code = rfv.Value
	 Where rfv.RFICode = @RFICode
	 Order By rfv.ID