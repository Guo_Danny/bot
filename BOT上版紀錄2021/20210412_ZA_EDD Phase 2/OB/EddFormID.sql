USE [PBSA]
GO

/****** Object:  Table [dbo].[EddFormID]    Script Date: 11/25/2020 3:55:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [dbo].[EddFormID](
	[FormSno] [int] IDENTITY(1,1) NOT NULL,
	[FormType] [varchar](35) NOT NULL,
	[FormVersion] [varchar](100) NOT NULL,
	[FormName] [varchar](300) NOT NULL,
	[FormID] [int] NOT NULL,
	[FormIDName] [varchar](300) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastModifyDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EddFormID] PRIMARY KEY CLUSTERED 
(
	[FormSno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO