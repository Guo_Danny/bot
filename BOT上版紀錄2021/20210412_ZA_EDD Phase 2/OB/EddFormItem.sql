USE [PBSA]
GO

/****** Object:  Table [dbo].[EddFormItem]    Script Date: 11/25/2020 3:55:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EddFormItem](
	[ItemSno] [int] IDENTITY(1,1) NOT NULL,
	[FormType] [varchar](35) NOT NULL,
	[FormID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[ItemName] [varchar](300) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastModifyDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EddFormItem] PRIMARY KEY CLUSTERED 
(
	[ItemSno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO