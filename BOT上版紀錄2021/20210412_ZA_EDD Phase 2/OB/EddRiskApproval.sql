USE [PBSA]
GO

/****** Object:  Table [dbo].[EddRiskApproval]    Script Date: 4/13/2021 11:37:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EddRiskApproval](
	[KeyID] [int] NOT NULL,
	[PreviousRisk] [varchar](50) NOT NULL,
	[CurrentRisk] [varchar](50) NOT NULL,
	[NewCustomer] [int] NOT NULL,
	[Manager] [int] NULL,
	[AMLCO] [int] NULL,
	[CCO] [int] NULL,
	[GM] [int] NULL,
	[LastModifyDate] [datetime] NULL,
 CONSTRAINT [PK_EddRiskApproval] PRIMARY KEY CLUSTERED 
(
	[PreviousRisk] ASC,
	[CurrentRisk] ASC,
	[NewCustomer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


