USE [PBSA]
GO

/****** Object:  Trigger [dbo].[EddRiskApprovalModif]    Script Date: 4/13/2021 11:38:03 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[EddRiskApprovalModif] ON [dbo].[EddRiskApproval]
FOR UPDATE
AS

  update [dbo].[EddRiskApproval] set LastModifyDate = GETDATE()
  from inserted u , EddRiskApproval w where u.KeyID = w.KeyID


GO

ALTER TABLE [dbo].[EddRiskApproval] ENABLE TRIGGER [EddRiskApprovalModif]
GO


