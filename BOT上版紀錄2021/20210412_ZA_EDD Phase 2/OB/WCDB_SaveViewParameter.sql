USE [Pcdb]
GO

/****** Object:  StoredProcedure [dbo].[WCDB_SaveViewParameter]    Script Date: 6/5/2020 5:09:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[WCDB_SaveViewParameter]

 (
	@ViewId 		 int,
	@OperCode 		 Code,
	@TableName 		varchar(255),
	@ColumnName		varchar(255),
	@ParameterOperation      int,
	@ParameterValue 	 varchar(2000),
	@ParameterRangeValue	 varchar(255)
	
 
)


  As


  declare @trnCnt int


  select @trnCnt = @@trancount	-- Save the current trancount

  if @trnCnt = 0
	-- Transaction has not begun
	begin tran WCDB_SaveViewParameter
  else
    -- Already in a transaction
	save tran WCDB_SaveViewParameter


	Declare @TableId         int
	Declare @ColumnId      int
	Declare @ParameterId int


	select @TableId =TableId from ListViewTables
	where TableName=@TableName

	select @ColumnId = [Position]
	from ListViewColumns
	where TableId=@TableId and ColumnName=@ColumnName



	--Get Unique parameter ID 		
	select  @ParameterId = max(ParameterId) +1
	from ViewParameters
	where 	ViewId = @ViewId and 
		(OperCode = @OperCode  or
		(OperCode = 'Prime' and CategoryId in (0,1) ) )




	if @ParameterId is null
		set @ParameterId=1


	
	--20200603 modify for customer ->  Supporting Info -> Event search all objectType
	if @ViewId = 11 and @ColumnName = 'ObjectType' and @ParameterValue = 'Cust' begin
		select 1

	end
	else begin

	insert into ViewParameters
	values(@ViewId, @ParameterId,  @OperCode,
	@ColumnId, @TableId,@ParameterOperation,
	@ParameterValue, 0, 1,2) 

			
	insert into ViewParameterSelections
	values	(@ViewId,  @ParameterId, @OperCode, 1, @ParameterRangeValue, 'to')

	end



	  if @@error <> 0
		  rollback tran WCDB_SaveViewParameter
	  else	 
	    begin		 
		  if @trnCnt = 0
			commit tran WCDB_SaveViewParameter
	    end

GO


