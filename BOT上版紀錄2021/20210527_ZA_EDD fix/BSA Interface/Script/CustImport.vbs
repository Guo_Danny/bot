'No support provided for this file.

Dim Name2Search 'As String
Dim Path2Search 'As String
Dim a, e 'As Variant
Dim objApp 'As Wscript.Shell
Dim outFile, outputFile 'As String
Dim fs 'As FileSystemObject
Dim FileName 'As String
Dim ErrorFile 'As String
Dim PrimeExePath 'As String
Dim TenantCode 'As String
Dim BackUpPath 'As String


'********************************************************************************************************************************
'	Modify the follow for the specific server environment

   TenantCode = "BOTOSB"

'Note: All path statements must end with \

   Path2Search = "d:\Prime\BOTOSB\Import\Customer\" 'Path of customer, account and activity source files

   Name2Search = "????Customer.*.txt" 'files to import, uses same wildcard convention as DOS DIR command
   
   outputFile = "d:\Prime\BOTOSB\LOGS\PBSA\FileLoadCustomer.txt" 'Path and Name of file for log file.

   PrimeExePath = "d:\Prime\Exe\" 'Path to Prime\Exe folder

   BackUpPath = "d:\Prime\BOTOSB\Backup\Pbsa\" 'Path to tenant backup folder for Pbsa

'********************************************************************************************************************************

   Set fs = CreateObject("Scripting.FileSystemObject")
   Set outFile = fs.OpenTextFile(outputFile, 8, True)  'Open the file to be created
   outFile.WriteBlankLines(1)

   outFile.WriteLine (Now & " Started Customer File Import")

   Set objApp = CreateObject("WScript.Shell") 

   outFile.WriteLine(Now & " Searching for " & Path2Search & Name2Search)
   a = ListDir(Path2Search & Name2Search)

   If UBound(a) = -1 then
      outFile.WriteLine (Now & " No Files Found.")
      outFile.WriteLine ("------------------------------------------------------------------------------------------")
      outFile.WriteBlankLines(1)
      outFile.Close
      WScript.Quit 1
   End If

   On Error Resume Next
   For Each FileName In a

'Customer
	
      'Write to Log File
      outFile.WriteLine (Now & " Found " & FileName)

      'Write to Log File
      outFile.WriteLine (Now & " Copying " & FileName & " to " & BackUpPath)

      'Execute Statement
      fs.CopyFile FileName, BackUpPath
      If Err.Number <> 0 Then ErrHdlr Err, outFile
      
      'Write to Log File
      outFile.WriteLine(Now & " Executing " & PrimeExePath & "CustImport.exe /o=" & TenantCode & " /a /r /f=" & FileName)
      
      'Execute Statement
      Return = objApp.Run(PrimeExePath & "CustImport.exe /o=" & TenantCode & " /a /r /f=" & FileName, 1, True)
      If Err.Number <> 0 Then ErrHdlr Err, outFile
      
      'Write to Log File
      outFile.WriteLine (Now & " Deleting " & FileName)

      'Execute Statement
      fs.DeleteFile FileName
      If Err.Number <> 0 Then ErrHdlr Err, outFile

   Next
   
   outFile.WriteLine (Now & " Completed")
   outFile.WriteLine ("------------------------------------------------------------------------------------------")
   outFile.WriteBlankLines(1)
   outFile.Close
   
   
'******************************************************************************
Private Function ErrHdlr(ByRef objErr, ByRef LogFile)
    outFile.WriteBlankLines(1)
	LogFile.WriteLine (Now & " ERROR: " & CStr(objErr.Number) & " " & objErr.Description)
    outFile.WriteLine ("------------------------------------------------------------------------------------------")
    outFile.WriteBlankLines(1)
	LogFile.Close
	WScript.Quit 1
End Function

   
'******************************************************************************
' Returns an array with the file names that match Path.
' The Path string may contain the wildcard characters "*"
' and "?" in the file name component. The same rules apply
' as with the MSDOS DIR command.
' If Path is a directory, the contents of this directory is listed.
' If Path is empty, the current directory is listed.
Public Function ListDir (ByVal Path)
   Dim fso: Set fso = CreateObject("Scripting.FileSystemObject")
   If Path = "" then Path = "*.*"
   Dim Parent, Filter
   if fso.FolderExists(Path) then     ' Path is a directory
      Parent = Path
      Filter = "*"
     Else
      Parent = fso.GetParentFolderName(Path)
      If Parent = "" Then If Right(Path,1) = ":" Then Parent = Path: Else Parent = "."
      Filter = fso.GetFileName(Path)
      If Filter = "" Then Filter = "*"
      End If
   ReDim a(10)
   Dim n: n = 0
   Dim Folder: Set Folder = fso.GetFolder(Parent)
   Dim Files: Set Files = Folder.Files
   Dim File
   For Each File In Files
      If CompareFileName(File.Name,Filter) Then
         If n > UBound(a) Then ReDim Preserve a(n*2)
         a(n) = File.Path
         n = n + 1
         End If
      Next
   ReDim Preserve a(n-1)
   ListDir = a
End Function

Private Function CompareFileName (ByVal Name, ByVal Filter) ' (recursive)
   CompareFileName = False
   Dim np, fp: np = 1: fp = 1
   Do
      If fp > Len(Filter) Then CompareFileName = np > len(name): Exit Function
      If Mid(Filter,fp) = ".*" Then       ' special case: ".*" at end of filter
         If np > Len(Name) Then CompareFileName = True: Exit Function
         End If
      Dim fc: fc = Mid(Filter,fp,1): fp = fp + 1
      Select Case fc
         Case "*"
            CompareFileName = CompareFileName2(name,np,filter,fp)
            Exit Function
         Case "?"
            If np <= Len(Name) And Mid(Name,np,1) <> "." Then np = np + 1
         Case Else
            If np > Len(Name) Then Exit Function
            Dim nc: nc = Mid(Name,np,1): np = np + 1
            If Strcomp(fc,nc,vbTextCompare)<>0 Then Exit Function
         End Select
      Loop
End Function

Private Function CompareFileName2 (ByVal Name, ByVal np0, ByVal Filter, ByVal fp0)
   Dim fp: fp = fp0
   Dim fc2
   Do
      If fp > Len(Filter) Then CompareFileName2 = True: Exit Function
      If Mid(Filter,fp) = ".*" Then    ' special case: ".*" at end of filter
         CompareFileName2 = True: Exit Function
         End If
      fc2 = Mid(Filter,fp,1): fp = fp + 1
      If fc2 <> "*" And fc2 <> "?" Then Exit Do
      Loop
   Dim np
   For np = np0 To Len(Name)
      Dim nc: nc = Mid(Name,np,1)
      If StrComp(fc2,nc,vbTextCompare)=0 Then
         If CompareFileName(Mid(Name,np+1),Mid(Filter,fp)) Then
            CompareFileName2 = True: Exit Function
            End If
         End If
      Next
   CompareFileName2 = False
End Function