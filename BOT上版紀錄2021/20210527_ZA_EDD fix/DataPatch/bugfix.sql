USE [PBSA]
GO
--刪除多餘custcheck list item
delete from eddformitem where formtype='RE-OTHER' and Formid=1 and itemid in (10,11)

--更新CDD Onboard欄位名稱 LicenseNo 為 ID
update CheckListItem set Name = 'ID', LastOper = 'Primeadmin' where Code = 'LicenseNo'

--因UPD_Customer.sql有問題，昨天晚上有請瑋欣幫忙修正，但還是導致有一筆CDD資料目前卡在ALLEN身上但是無法往下一步驟執行。
--請您協助執行script(讓該筆狀態由send to approve 1 修正為 temp saved 這樣才能夠讓流程繼續往下走)
update pbsa..Customer set KYCStatus = 1 where id = '0161503671'
