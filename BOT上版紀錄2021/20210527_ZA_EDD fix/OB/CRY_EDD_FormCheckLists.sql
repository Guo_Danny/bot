USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[CRY_EDD_FormCheckLists]    Script Date: 5/27/2021 11:02:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




 
ALTER PROCEDURE [dbo].[CRY_EDD_FormCheckLists]  
(
	@Cust 		  VARCHAR(35),
	@FormType	  VARCHAR(35)
)

  AS

  	--Declare @Cust  VARCHAR(35),	@FormType VARCHAR(35)
	--SELECT @Cust='0161000013', @FormType='RE-TRUST'	

	Declare @iNewCust int, @CustName varchar(200), @ProposedRisk varchar(50),@CurrRisk varchar(50)
	Declare @AMLCO varchar(50), @CCO varchar(50), @GM varchar(50)
	Declare @AMLCOdate varchar(50),@CCOdate varchar(50), @GMdate varchar(50)
	Declare @txt varchar(4000), @strTxt varchar(4000),@cDate datetime, @RvDate varchar(50)
    Declare @sData varchar(600), @amlcoList varchar(1000), @ccoList varchar(1000), @gmList varchar(1000), @count int, @AllList varchar(3000)

    --get newcust
	SELECT @iNewCust=0,@CustName='',@txt='', @strTxt='', @RvDate='',@CurrRisk=''
    --select @iNewCust=1,@CustName=Name from Customer(nolock) 
	--where id=@Cust and ProbationStartDate is null 
	--AND   datediff(day,KYCDataCreateDate,getdate()) < 14
    select @txt=nullif(ProbationStartDate,''),@CustName=Name,@cDate=KYCDataCreateDate from Customer(nolock) 
	where id=@Cust
	if (@txt='' or @txt is null or @txt='19000101') and datediff(day,@cDate,getdate()) < 10
	   select @iNewCust=1

	--select @iNewCust, @cDate, @txt

    --20201214, get approve by GM RISK 
	IF @iNewCust=0
	   BEGIN
	           --get risk class for approved by MLCO
			   SELECT @sData = '', @amlcoList = '', @ccoList='', @gmList='', @count = 0;   
			  
			   SELECT @amlcoList=E.AMLCO, @ccoList=E.CCO, @gmList=E.GM
			   FROM EddFormApproval(NOLOCK) E, RiskFactorDataView(NOLOCK) R, RiskClass(NOLOCK) RC 
               where E.PreviousRisk=R.RiskClass and R.ProposedRiskClass=RC.Code
               and E.CurrentRisk=R.ProposedRiskClass and E.PreviousRisk=R.RiskClass
               and R.Cust=@Cust
		               
			   --select @amlcoList, @ccoList, @gmList

		END
   ELSE IF @iNewCust=1
	    BEGIN    
		 	   --get risk class for approved by MLCO
			   SELECT @sData = '', @amlcoList = '', @ccoList='', @gmList='', @count = 0;   
			  
			   SELECT @amlcoList=E.AMLCO, @ccoList=E.CCO, @gmList=E.GM
			   FROM EddFormApproval(NOLOCK) E, RiskFactorDataView(NOLOCK) R, RiskClass(NOLOCK) RC 
			   WHERE R.ProposedRiskClass=RC.Code and E.NewCustomer=1
               AND E.CurrentRisk=R.ProposedRiskClass and ISNULL(E.PreviousRisk,'')=''
               and R.Cust=@Cust

			  --select @amlcoList, @ccoList, @gmList
		END


	--get proposed risk
	select @ProposedRisk=ProposedRiskClass from PBSA..RiskFactorDataView(nolock) 
	where cust=@Cust

	--get onboard INPUT ITEM: RE-VERIFICATION DATE
	--get riskclass of current risk
	select @RvDate=A.cdduser38, @CurrRisk=RiskClass 
	from PBSA..KYCData A(nolock), PBSA..Customer B(nolock) 
	where CustomerId=@Cust and A.CustomerId=B.Id

	select @AMLCO='', @AMLCOdate='',@CCO='',@CCOdate='', @GM='', @GMdate=''

	--20201214 change get risk approve control from riskclass, Start
	--if @iNewCust = 1 and (@ProposedRisk= 'HighRisk' or @ProposedRisk='DeRisk')
	--if @iNewCust=1 and CHARINDEX(@ProposedRisk,@AllList) > 0  --20201225 MASK
	if @iNewCust=1 and @amlcoList='1' and @ccoList='1' and @gmList='1'
          select @AMLCO='AMLCO : ',@AMLCOdate='DATE : ',@CCO='C.C.O. :',@CCOdate='DATE : ',@GM='G.M. : ', @GMdate='DATE : '
    --else if @iNewCust=1 and CHARINDEX(@ProposedRisk,@CCOList) > 0  --20201225 MASK
	else if @iNewCust=1 and @amlcoList='1' and @ccoList='1'  and @gmList='0'
          select @AMLCO='AMLCO : ',@AMLCOdate='DATE : ',@CCO='C.C.O. :',@CCOdate='DATE : '
	--else if @iNewCust=1 and CHARINDEX(@ProposedRisk,@GmList) > 0   --20201225 MASK
	else if @iNewCust=1 and @amlcoList='1' and @gmList='1'  and @ccoList='0'
	      select @AMLCO='AMLCO : ',@AMLCOdate='DATE : ',@CCO='G.M. :',@CCOdate='DATE : '
    else if @iNewCust=1 and @ccoList='1' and @gmList='1' and @amlcoList='0'
	      select @CCO='C.C.O. :',@CCOdate='DATE : ',@GM='G.M. : ', @GMdate='DATE : '

	--else if  (@iNewCust=1 AND  @ProposedRisk= 'MedHiRisk' ) or (@iNewCust=0 AND (@ProposedRisk= 'MedHiRisk' or @ProposedRisk= 'HighRisk' or @ProposedRisk='DeRisk'))
    --else if  (@iNewCust=1 AND  CHARINDEX(@ProposedRisk, @MlcoList)>0 )	or ( @iNewCust=0 AND  CHARINDEX(@ProposedRisk,@MlcoList) > 0 )   --20201225 MASK
	  else if  ( @iNewCust=1 AND  @amlcoList='1' and @ccoList='0' and @gmList='0' ) or ( @iNewCust=0 and @amlcoList='1' and @ccoList='0' and @gmList='0' )
	    begin
		    --select @ProposedRisk as ProposedRisk, @MlcoList as Mlco
	        select @AMLCO='AMLCO : ',@AMLCOdate='DATE : '
         end

/*    --20201225 MASK 
	--else if  (@iNewCust=0 AND (@ProposedRisk= 'MediumRisk' or @ProposedRisk= 'LowRisk') )
    else if  @iNewCust=0 AND   CHARINDEX(@ProposedRisk, @MlcoList)=0  
        begin
		  --select @CurrRisk as CurrRisk, @MlcoList as Mlco
		  --if @iNewCust=0 AND (@CurrRisk='MedHiRisk' or @CurrRisk='HighRisk' or @CurrRisk='DeRisk')
		  if @iNewCust=0 AND CHARINDEX(@CurrRisk, @MlcoList)>0 
		     select @AMLCO='AMLCO : ',@AMLCOdate='DATE : '
		end
*/

	   --Get Form Check List
	   select FID.*, FITEM.*,@iNewCust AS iNewCust,@ProposedRisk AS ProposedRisk ,@Cust AS Cust,@CustName AS CustName ,@AMLCO AS AMLCO,@AMLCOdate AS AMLCOdate,@CCO AS CCO,@CCOdate AS CCOdate,@GM AS GM,@GMdate AS GMdate, @RvDate as ReVerificationDate, @CurrRisk as CurrRisk
	   FROM eddformid(nolock) FID, eddformitem(nolock) FITEM
       WHERE FID.FORMTYPE = @FormType
	   AND FID.FORMTYPE=FITEM.FORMTYPE AND FID.FORMID=FITEM.FORMID
	   ORDER BY FID.FORMID, FITEM.ITEMID

	Return



