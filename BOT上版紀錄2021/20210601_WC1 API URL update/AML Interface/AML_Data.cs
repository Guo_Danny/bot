﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using log4net;
using System.Net;
using System.Security.Cryptography;
using System.IO;
using Newtonsoft.Json;
using System.Collections;
using System.Xml;
using System.Reflection;

namespace BlackListService
{
    public class AML_Data
    {
        private Database database;

        private static readonly ILog log = LogManager.GetLogger("App.Logging");

        private string gatewayhost = String.Empty;
        private string gatewayurl = String.Empty;
        private string apikey = String.Empty;
        private string apiurl = String.Empty;
        private string apisecret = String.Empty;
        private string secondaryFields = String.Empty;
        private string entityType = String.Empty;
        private string customFields = String.Empty;
        private string typeid = String.Empty;
        private string groupId = String.Empty;
        private string providerTypes = String.Empty;
        private int basenum = 0;
        private string strProxy = String.Empty;
        private string str_1000 = String.Empty;
        private string TR1 = String.Empty;
        private string TR2 = String.Empty;
        private string TR3 = String.Empty;
        private string str_exclude = String.Empty;
        private int apiretry = 5;
        private int retrySec = 10;
        private string OFACWS_Enable = "N";
        private string OFACWS_DB = "N";
        private string OFACWS_Currency_Tag = "";
        private string OFACWS_SSL = "";

        public AML_Data()
        {
            database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

            XmlDocument doc = new XmlDocument();
            Fortify fortifyService = new Fortify();

            string str_path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + @"\Setting.xml";
            doc.XmlResolver = null;
            doc.Load(fortifyService.PathManipulation(str_path));
            XmlNodeList settingnodes = doc.SelectNodes("//Setting");
            foreach (XmlNode node in settingnodes)
            {
                gatewayhost = node["gatewayhost"].InnerText.Replace("\r\n", "").Trim();
                gatewayurl = node["gatewayurl"].InnerText.Replace("\r\n", "").Trim();
                apikey = node["apikey"].InnerText.Replace("\r\n", "").Trim();
                apiurl = node["apiurl"].InnerText.Replace("\r\n", "").Trim();
                apisecret = node["apisecret"].InnerText.Replace("\r\n", "").Trim();
                secondaryFields = node["secondaryFields"].InnerText.Replace("\r\n", "").Trim();
                entityType = node["entityType"].InnerText.Replace("\r\n", "").Trim();
                customFields = node["customFields"].InnerText.Replace("\r\n", "").Trim();
                typeid = node["typeID"].InnerText.Replace("\r\n", "").Trim();
                groupId = node["groupId"].InnerText.Replace("\r\n", "").Trim();
                providerTypes = node["providerTypes"].InnerText.Replace("\r\n", "").Trim();
                basenum = int.Parse(node["basenum"].InnerText.Replace("\r\n", "").Trim());
                strProxy = node["goproxy"].InnerText.Replace("\r\n", "").Trim();
                str_1000 = node["type1000"].InnerText.Replace("\r\n", "").Trim();
                TR1 = node["TradeMark"].InnerText.Replace("\r\n", "").Trim();
                TR2 = node["TradeMark2"].InnerText.Replace("\r\n", "").Trim();
                TR3 = node["TradeMark3"].InnerText.Replace("\r\n", "").Trim();
                str_exclude = node["excludenames"].InnerText.Replace("\r\n", "").Trim();
                apiretry = int.Parse(node["apiretry"].InnerText.Replace("\r\n", "").Trim());
                retrySec = int.Parse(node["apiretrySec"].InnerText.Replace("\r\n", "").Trim());

                OFACWS_SSL = node["OFACWS_SSL"].InnerText.Replace("\r\n", "").Trim();
            }
        }

        public void Updatematchcount(int imatchcount, string strRef)
        {
            Fortify fortifyService = new Fortify();

            string sql = "update FilterTranTable set MatchCount = @match ";
            sql += "where Ref = @ref ;";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@match", fortifyService.PathManipulation(imatchcount.ToString())));
                cmd.Parameters.Add(new SqlParameter("@ref", fortifyService.PathManipulation(strRef)));

                ExecuteSQL(cmd);
            }
        }

        /// <summary>
        /// CheckSdn for World-Check
        /// </summary>
        /// <param name="strEntnum"></param>
        /// <param name="strName"></param>
        /// <param name="strBranch"></param>
        /// <param name="strprogram"></param>
        /// <param name="strreferenceId"></param>
        /// <param name="strmatchedNameType"></param>
        /// <param name="strsources"></param>
        /// <param name="strcategories"></param>
        /// <returns></returns>
        public string CheckSdn(string strEntnum,string strName,string strBranch,string strprogram,string strreferenceId, string strmatchedNameType, string[] strsources,string[] strcategories)
        {
            string sql = "";
            string rEntnum = "";
            Fortify fortifyService = new Fortify();
            string sour = "";
            string cate = "";
            foreach (string s in strsources)
            {
                sour = sour + s + ",";
            }
            sour = sour.TrimEnd(',');

            foreach (string s in strcategories)
            {
                cate = cate + s + ",";
            }
            cate = cate.TrimEnd(',');

            strEntnum = (int.Parse(strEntnum) + basenum).ToString();

            sql = @"select entnum from sdntable (nolock) 
                      where entnum = @ent;";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.CommandText = sql;

                database.AddInParameter(cmd, "@ent", DbType.Int32, strEntnum);

                IDataReader reader = database.ExecuteReader(cmd);
                try
                {
                    while (reader.Read())
                    {
                        rEntnum = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }

                if (rEntnum == "")
                {
                    string remark = "";
                    remark += TR1 + " \r\n";
                    remark += TR2 + " \r\n";
                    remark += TR3 + " \r\n";
                    remark += "Added by World-Check one API. \r\n";
                    remark += "referenceId:" + strreferenceId + " \r\n";
                    remark += "matchedNameType:" + strmatchedNameType + " \r\n";
                    remark += "sources:" + sour + " \r\n";
                    remark += "categories:" + cate + " \r\n";

                    //insert sdntable
                    sql = "insert into OFAC..sdntable(entnum,name,listtype,program,type,primeadded,remarks,sdntype,datatype,userrec,deleted,duplicrec,ignorederived,status,";
                    sql += "Listcreatedate,listmodifdate,createdate,lastmodifdate,lastoper)";
                    sql += "select @entnum,@name,@listtype,@program,'other',0,@remark,1,1,1,0,0,0,1,";
                    sql += "getdate(),getdate(),getdate(),getdate(),'PrimeAdmin';";

                    cmd.CommandText = sql;

                    cmd.Parameters.Add(new SqlParameter("@entnum", fortifyService.PathManipulation(strEntnum)));
                    cmd.Parameters.Add(new SqlParameter("@name", fortifyService.PathManipulation(strName)));
                    cmd.Parameters.Add(new SqlParameter("@listtype", "WC1"));
                    cmd.Parameters.Add(new SqlParameter("@program", fortifyService.PathManipulation(strprogram)));
                    cmd.Parameters.Add(new SqlParameter("@remark", fortifyService.PathManipulation(remark)));

                    LogAdd("insert sdn ent:" + fortifyService.PathManipulation(strEntnum) + ",name:" + fortifyService.PathManipulation(strName));

                    ExecuteSQL(cmd);
                }
            }
            return rEntnum;
        }

        /// <summary>
        /// CheckSdn for OFAC_WS
        /// </summary>
        /// <param name="strEntnum"></param>
        /// <param name="strName"></param>
        /// <param name="strBranch"></param>
        /// <param name="strprogram"></param>
        /// <returns></returns>
        public string CheckSdn(string strEntnum, string strName, string strBranch, string strProgram)
        {
            string sql = "";
            string rEntnum = "";
            string sqllisttype = "";
            string strBlocklisttype = "";
            Fortify fortifyService = new Fortify();

            LogAdd("name=" + strName);

            sqllisttype = @"select BlockListTypeList from OFAC..SanctionDataRule
                              where code like @branch ";

            using (DbCommand cmd = database.GetSqlStringCommand(sqllisttype))
            {
                cmd.CommandText = sqllisttype;

                database.AddInParameter(cmd, "@branch", DbType.String, strBranch + "SWIFT%");

                IDataReader reader = database.ExecuteReader(cmd);
                try
                {
                    while (reader.Read())
                    {
                        strBlocklisttype = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            LogAdd("BlockListType=" + strBlocklisttype);

            sql = @"select entnum from OFAC..sdntable (nolock) 
                      where name = @name and program = @program and status != 4 
                      and CHARINDEX(rtrim(ListType+','),@blocklisttype) = 0;";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.CommandText = sql;

                database.AddInParameter(cmd, "@name", DbType.String, strName);
                database.AddInParameter(cmd, "@program", DbType.String, strProgram);
                database.AddInParameter(cmd, "@blocklisttype", DbType.String, strBlocklisttype);
                IDataReader reader = database.ExecuteReader(cmd);
                try
                {
                    while (reader.Read())
                    {
                        rEntnum = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }

            LogAdd("entnum=" + rEntnum);
            return rEntnum;
        }

        /// <summary>
        /// Inser Recon table reports
        /// </summary>
        /// <param name="strSource"></param>
        /// <param name="strTranSearchType"></param>
        /// <param name="strSancPartyCount"></param>
        /// <param name="strbranch"></param>
        /// <param name="strdepartment"></param>
        /// <param name="strCaseRef"></param>
        /// <param name="strUserMsgRef"></param>
        /// <param name="strSearchOper"></param>
        /// <param name="strCreater"></param>
        /// <returns></returns>
        public Boolean InsertRecon(string strSource, string strTranSearchType, string strSancPartyCount, string strbranch, string strdepartment, string strCaseRef, string strUserMsgRef, string strSearchOper, string strCreater)
        {
            Boolean bol = false;
            Fortify fortifyService = new Fortify();
            try
            {
                string sql = @"
insert into OFAC..ReconTable(Source,MsgRef,TranSearchType,SancPartyCount,Branch,Department,CaseReference,UserMessageReference,FileImage,SearchOper,
SearchDate,CreateDate)
select top 1 @Source,
@Source + '-' + replace(CONVERT(VARCHAR(10),GETDATE(),110),'-','') + '-' + CAST ((select count(Source)+1 from ReconTable) as VARCHAR(20)),
@TranSearchType,@SancPartyCount,@branch,@department,@CaseRef,
@UserMsgRef,@fileimg,@SearchOper,getdate(),getdate() from OFAC..recontable where 1 = 1
";
                using (DbCommand cmd = database.GetSqlStringCommand(sql))
                {
                    cmd.Parameters.Add(new SqlParameter("@Source", fortifyService.PathManipulation(strSource)));
                    cmd.Parameters.Add(new SqlParameter("@TranSearchType", fortifyService.PathManipulation(strTranSearchType)));
                    cmd.Parameters.Add(new SqlParameter("@SancPartyCount", fortifyService.PathManipulation(strSancPartyCount)));
                    cmd.Parameters.Add(new SqlParameter("@branch", fortifyService.PathManipulation(strbranch)));
                    cmd.Parameters.Add(new SqlParameter("@department", fortifyService.PathManipulation(strdepartment)));
                    cmd.Parameters.Add(new SqlParameter("@CaseRef", fortifyService.PathManipulation(strCaseRef)));
                    cmd.Parameters.Add(new SqlParameter("@UserMsgRef", fortifyService.PathManipulation(strUserMsgRef)));
                    cmd.Parameters.Add(new SqlParameter("@fileimg", fortifyService.PathManipulation(string.Format("{0}Swift", strbranch))));
                    cmd.Parameters.Add(new SqlParameter("@SearchOper", fortifyService.PathManipulation(strSearchOper)));

                    ExecuteSQL(cmd);
                    bol = true;
                }
                return bol;
            }
            catch(Exception ex)
            {
                LogAdd("ex:" + ex.ToString());
                return bol;
            }
        }

        /// <summary>
        /// Insert Case
        /// </summary>
        /// <param name="strRef"></param>
        /// <param name="strbranch"></param>
        /// <returns></returns>
        public Boolean InsertFilterTran(string strRef,string strbranch, string strmatchcount)
        {
            try
            {
                bool bol_r = false;
                Fortify fortifyService = new Fortify();
                string SQL = "";

                SQL = "insert into OFAC..FilterTranTable (Source,Ref,Branch,Dept,ReqTime,ClientId,TranTime,Result,MatchCount,MsgType,Currency,KeyField1,KeyField2,CIFFieldsDelimiter,LengthKF1,LengthKF2,TransactionType,SystemRef,FileImage,App,IsIncremental) ";
                SQL += "select top 1 @Source,@Ref,@Branch,@Dept,getdate(),@ClientId,getdate(),@Result,@MatchCount,@MsgType,@Currency,@KeyField1,@KeyField2,@CIFFieldsDelimiter,";
                SQL += "@LengthKF1,@LengthKF2,@TransactionType,@SystemRef,@FileImage,@App,@IsIncremental ";

                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.Parameters.Add(new SqlParameter("@Source", "Thomson Reuters World-Check One"));
                    cmd.Parameters.Add(new SqlParameter("@Ref", fortifyService.PathManipulation(strRef)));
                    cmd.Parameters.Add(new SqlParameter("@branch", fortifyService.PathManipulation(strbranch)));
                    cmd.Parameters.Add(new SqlParameter("@Dept", "AML"));
                    cmd.Parameters.Add(new SqlParameter("@ClientId", "8888"));
                    cmd.Parameters.Add(new SqlParameter("@Result", "1"));
                    cmd.Parameters.Add(new SqlParameter("@MatchCount", fortifyService.PathManipulation(strmatchcount)));
                    cmd.Parameters.Add(new SqlParameter("@MsgType", "1"));
                    cmd.Parameters.Add(new SqlParameter("@Currency", "USD"));
                    cmd.Parameters.Add(new SqlParameter("@KeyField1", "0"));
                    cmd.Parameters.Add(new SqlParameter("@KeyField2", "1"));
                    cmd.Parameters.Add(new SqlParameter("@CIFFieldsDelimiter", "|"));
                    cmd.Parameters.Add(new SqlParameter("@LengthKF1", "0"));
                    cmd.Parameters.Add(new SqlParameter("@LengthKF2", "0"));
                    cmd.Parameters.Add(new SqlParameter("@TransactionType", "1"));
                    cmd.Parameters.Add(new SqlParameter("@SystemRef", string.Format("WC1-{0}-{1}-0",DateTime.Now.ToString("MMddyyyy"), DateTime.Now.ToString("Hmmss"))));
                    cmd.Parameters.Add(new SqlParameter("@FileImage", "WC1"));
                    cmd.Parameters.Add(new SqlParameter("@App", "0"));
                    cmd.Parameters.Add(new SqlParameter("@IsIncremental", "0"));

                    ExecuteSQL(cmd);
                    bol_r = true;
                }
                LogAdd("InsertFilterTran:"+ bol_r);
                return bol_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// InsertFilterTran for OFACWS
        /// </summary>
        /// <param name="strRef"></param>
        /// <param name="strbranch"></param>
        /// <param name="strmatchcount"></param>
        /// <returns></returns>
        public Boolean InsertFilterTran_OFACWS(string strRef, string strbranch, string strmatchcount, string str_currency, out int iseqnumb)
        {
            try
            {
                bool bol_r = false;
                Fortify fortifyService = new Fortify();
                string SQL = "";

                SQL = "insert into OFAC..FilterTranTable (Source,Ref,Branch,Dept,ReqTime,ClientId,TranTime,Result,MatchCount,MsgType,Currency,KeyField1,KeyField2,CIFFieldsDelimiter,LengthKF1,LengthKF2,TransactionType,SystemRef,FileImage,App,IsIncremental) ";
                SQL += "select top 1 @Source,@Ref,@Branch,@Dept,getdate(),@ClientId,getdate(),@Result,@MatchCount,@MsgType,@Currency,@KeyField1,@KeyField2,@CIFFieldsDelimiter,";
                SQL += "@LengthKF1,@LengthKF2,@TransactionType,@SystemRef,@FileImage,@App,@IsIncremental ";

                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.Parameters.Add(new SqlParameter("@Source", "OFAC_Webservice"));
                    cmd.Parameters.Add(new SqlParameter("@Ref", fortifyService.PathManipulation(strRef)));
                    cmd.Parameters.Add(new SqlParameter("@branch", fortifyService.PathManipulation(strbranch)));
                    cmd.Parameters.Add(new SqlParameter("@Dept", "AML"));
                    cmd.Parameters.Add(new SqlParameter("@ClientId", "8888"));
                    cmd.Parameters.Add(new SqlParameter("@Result", "1"));
                    cmd.Parameters.Add(new SqlParameter("@MatchCount", fortifyService.PathManipulation(strmatchcount)));
                    cmd.Parameters.Add(new SqlParameter("@MsgType", "1"));
                    cmd.Parameters.Add(new SqlParameter("@Currency", fortifyService.PathManipulation(str_currency)));
                    cmd.Parameters.Add(new SqlParameter("@KeyField1", "0"));
                    cmd.Parameters.Add(new SqlParameter("@KeyField2", "1"));
                    cmd.Parameters.Add(new SqlParameter("@CIFFieldsDelimiter", "|"));
                    cmd.Parameters.Add(new SqlParameter("@LengthKF1", "0"));
                    cmd.Parameters.Add(new SqlParameter("@LengthKF2", "0"));
                    cmd.Parameters.Add(new SqlParameter("@TransactionType", "1"));
                    cmd.Parameters.Add(new SqlParameter("@SystemRef", string.Format("WS-{0}-{1}-0", DateTime.Now.ToString("MMddyyyy"), DateTime.Now.ToString("Hmmss"))));
                    cmd.Parameters.Add(new SqlParameter("@FileImage", fortifyService.PathManipulation(strbranch) + "swift"));
                    cmd.Parameters.Add(new SqlParameter("@App", "0"));
                    cmd.Parameters.Add(new SqlParameter("@IsIncremental", "0"));

                    ExecuteSQL(cmd);
                    bol_r = true;
                }

                // get seqnumb after insert sql executed
                iseqnumb = 0;
                SQL = "SELECT IDENT_CURRENT ('OFAC..FilterTranTable') AS Current_Identity;";
                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.CommandText = SQL;

                    IDataReader reader = database.ExecuteReader(cmd);
                    try
                    {
                        while (reader.Read())
                        {
                            iseqnumb = int.Parse(reader[0].ToString());
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }

                LogAdd("InsertFilterTran:" + bol_r);
                return bol_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// InsertMatchTable for World-Check
        /// </summary>
        /// <param name="strRef"></param>
        /// <param name="strentNum"></param>
        /// <param name="strEngName"></param>
        /// <param name="strOrgSDN"></param>
        /// <param name="Strength"></param>
        /// <returns></returns>
        public Boolean InsertMatchTable(string strRef,string strentNum,string strEngName, string strOrgSDN,string Strength)
        {
            try
            {
                Fortify fortifyService = new Fortify();

                strentNum = (int.Parse(strentNum) + basenum).ToString();

                bool bol_r = false;

                string SQL = "";
                SQL = "insert into OFAC..MatchTable (SeqNumb,MatchName,OriginalSDNName,MatchText,MatchCountry,MatchType,Score,entNum) ";
                SQL += "select top 1 SeqNumb, @MatchName,@OriginalSDNName,@MatchText,@MatchCountry,@MatchType,@Score,@entNum ";
                SQL += "from OFAC..FilterTranTable where Ref = @Ref order by ReqTime DESC ";

                int iscore = 0;
                if (Strength.ToUpper() == "STRONG")
                {
                    iscore = 95;
                }
                else if (Strength.ToUpper() == "MEDIUM")
                {
                    iscore = 85;
                }
                else if (Strength.ToUpper() == "WEAK")
                {
                    iscore = 75;
                }
                else if (Strength.ToUpper() == "EXACT")
                {
                    iscore = 100;
                }

                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.Parameters.Add(new SqlParameter("@Ref", fortifyService.PathManipulation(strRef)));
                    cmd.Parameters.Add(new SqlParameter("@MatchName", fortifyService.PathManipulation(strEngName)));
                    cmd.Parameters.Add(new SqlParameter("@OriginalSDNName", fortifyService.PathManipulation(strOrgSDN)));
                    cmd.Parameters.Add(new SqlParameter("@MatchText", fortifyService.PathManipulation(strEngName)));
                    cmd.Parameters.Add(new SqlParameter("@MatchCountry", "WC1"));
                    cmd.Parameters.Add(new SqlParameter("@MatchType", "other"));
                    cmd.Parameters.Add(new SqlParameter("@Score", fortifyService.PathManipulation(iscore.ToString())));
                    cmd.Parameters.Add(new SqlParameter("@entNum", fortifyService.PathManipulation(strentNum)));

                    ExecuteSQL(cmd);
                    bol_r = true;
                }
                LogAdd(string.Format("InsertMatchTable:{0},strRef={1},strentNum={2}", fortifyService.PathManipulation(bol_r.ToString()), fortifyService.PathManipulation(strRef), fortifyService.PathManipulation(strentNum)));
                return bol_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// InsertMatchTable for OFAC_WS
        /// </summary>
        /// <param name="strRef"></param>
        /// <param name="strentNum"></param>
        /// <param name="strEngName"></param>
        /// <param name="strOrgSDN"></param>
        /// <param name="iscore"></param>
        /// <param name="strMatchtext"></param>
        /// <returns></returns>
        public Boolean InsertMatchTable(string strRef, string strentNum, string strEngName, string strOrgSDN, string iscore, string strMatchtext)
        {
            try
            {
                Fortify fortifyService = new Fortify();

                //strentNum = (int.Parse(strentNum) + basenum).ToString();

                bool bol_r = false;

                LogAdd(string.Format("InsertMatchTable:{0},strRef={1},strentNum={2}", fortifyService.PathManipulation(bol_r.ToString()), fortifyService.PathManipulation(strRef), fortifyService.PathManipulation(strentNum)));

                string SQL = "";
                SQL = "insert into OFAC..MatchTable (SeqNumb,MatchName,OriginalSDNName,MatchText,MatchCountry,MatchType,Score,entNum) ";
                SQL += "select top 1 SeqNumb, @MatchName,@OriginalSDNName,@MatchText,@MatchCountry,@MatchType,@Score,@entNum ";
                SQL += "from OFAC..FilterTranTable where Ref = @Ref order by ReqTime DESC ";

                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.Parameters.Add(new SqlParameter("@Ref", fortifyService.PathManipulation(strRef)));
                    cmd.Parameters.Add(new SqlParameter("@MatchName", fortifyService.PathManipulation(strEngName)));
                    cmd.Parameters.Add(new SqlParameter("@OriginalSDNName", fortifyService.PathManipulation(strOrgSDN)));
                    cmd.Parameters.Add(new SqlParameter("@MatchText", fortifyService.PathManipulation(strMatchtext)));
                    cmd.Parameters.Add(new SqlParameter("@MatchCountry", "OFACWS"));
                    cmd.Parameters.Add(new SqlParameter("@MatchType", "other"));
                    cmd.Parameters.Add(new SqlParameter("@Score", fortifyService.PathManipulation(iscore.ToString())));
                    cmd.Parameters.Add(new SqlParameter("@entNum", fortifyService.PathManipulation(strentNum)));

                    ExecuteSQL(cmd);
                    bol_r = true;
                }
                
                return bol_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// InsertMsgTable
        /// </summary>
        /// <param name="strRef"></param>
        /// <param name="strcif_file"></param>
        /// <returns></returns>
        public Boolean InsertMsgTable(string strRef,string strcif_file)
        {
            try
            {
                Fortify fortifyService = new Fortify();
                bool bol_r = false;
                string strMsg = "";
                System.IO.StreamReader file = new System.IO.StreamReader(fortifyService.PathManipulation(strcif_file));
                string line = "";
                StringWriter strWriter = new StringWriter();

                while ((line = file.ReadLine()) != null)
                {
                    strMsg += line;
                    strWriter.WriteLine(line);
                }

                string SQL = "";
                SQL = "insert into OFAC..MsgTable(SeqNumb,MsgType,MsgLen,Msg) ";
                SQL += "select top 1 SeqNumb , @MsgType,@MsgLen,@Msg ";
                SQL += "from OFAC..FilterTranTable where Ref = @Ref order by ReqTime DESC";

                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.Parameters.Add(new SqlParameter("@Ref", fortifyService.PathManipulation(strRef)));
                    cmd.Parameters.Add(new SqlParameter("@MsgType", "1"));
                    cmd.Parameters.Add(new SqlParameter("@MsgLen", fortifyService.PathManipulation(strMsg).Length));
                    cmd.Parameters.Add(new SqlParameter("@Msg", fortifyService.PathManipulation(strWriter.ToString())));
                    
                    ExecuteSQL(cmd);
                    bol_r = true;
                }

                file.Close();
                return bol_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update match count for OFAC_WS more than 1 name scaned
        /// </summary>
        /// <param name="iCount"></param>
        /// <param name="strseqNum"></param>
        /// <returns></returns>
        public Boolean UpdateMatchCount(int iCount, int iseqNum)
        {
            try
            {
                Fortify fortifyService = new Fortify();
                bool bol_r = false;

                LogAdd(string.Format("Update match count:{0},strseqNum={1}", fortifyService.PathManipulation(iCount.ToString()), fortifyService.PathManipulation(iseqNum.ToString())));

                string SQL = "";
                SQL = "update OFAC..FilterTranTable  set MatchCount = @count where SeqNumb = @strseqNum ";

                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.Parameters.Add(new SqlParameter("@count", fortifyService.PathManipulation(iCount.ToString())));
                    cmd.Parameters.Add(new SqlParameter("@strseqNum", fortifyService.PathManipulation(iseqNum.ToString())));

                    ExecuteSQL(cmd);
                    bol_r = true;
                }

                return bol_r;

            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get the Currency from swift
        /// </summary>
        /// <param name="str_Ref"></param>
        /// <param name="str_file"></param>
        /// <param name="str_tag"></param>
        /// <returns></returns>
        public String GetCurrency(string str_Ref, string str_file, string str_tag)
        {
            try
            {
                Fortify fortifyService = new Fortify();
                string str_r = "USD";

                System.IO.StreamReader file = new System.IO.StreamReader(fortifyService.PathManipulation(str_file));
                string line = "";
                //StringWriter strWriter = new StringWriter();

                string str_refMT = str_Ref.Substring(14, 4).Trim();

                while ((line = file.ReadLine()) != null)
                {
                    foreach(string stag in str_tag.Split(','))
                    {
                        if (line.Contains(stag))
                        {
                            if (stag == ":32A:" && (str_refMT == "103" || str_refMT == "202" || str_refMT == "202C" || str_refMT == "205" || str_refMT == "205C" || str_refMT.Substring(0,1) == "4"))
                            {
                                str_r = line.Substring(11, 3);
                            }
                            else if (stag == ":32B:" && (str_refMT == "203" || str_refMT == "210"  || str_refMT.Substring(0, 1) == "3" || str_refMT.Substring(0, 1) == "4" || str_refMT.Substring(0, 1) == "7"))
                            {
                                str_r = line.Substring(5, 3);
                            }
                            else if (stag == ":32D:" && (str_refMT == "190" || str_refMT == "290"))
                            {
                                str_r = line.Substring(11, 3);
                            }
                            else if ((stag == ":33B:" || stag == ":32C:" || stag == ":32H:" || stag == ":32M:" || stag == ":33E:" || stag == ":33H:" || stag == ":33T:" || stag == ":33S:" || stag == ":33V:" || stag == ":34B:" || stag == ":34H:" || stag == ":34G:" || stag == ":71F:") && (str_refMT.Substring(0, 1) == "3" || str_refMT.Substring(0, 1) == "5"))
                            {
                                str_r = line.Substring(5, 3);
                            }
                            else if (stag == ":34A:" && (str_refMT.Substring(0, 1) == "5"))
                            {
                                str_r = line.Substring(11, 3);
                            }
                            else if ((stag == ":33C:" || stag == ":32D:") && (str_refMT.Substring(0, 1) == "4"))
                            {
                                str_r = line.Substring(11, 3);
                            }
                            else if (stag == ":90B:" && (str_refMT.Substring(0, 1) == "5"))
                            {
                                str_r = line.Substring(17, 3);
                            }
                            else if ((stag == ":11A:" || stag == ":92F:" || stag == ":92H:") && (str_refMT.Substring(0, 1) == "5"))
                            {
                                str_r = line.Substring(12, 3);
                            }
                            else if ((stag == ":60M:" || stag == ":62M:" || stag == ":60F:" || stag == ":62F:") && (str_refMT.Substring(0, 1) == "9"))
                            {
                                str_r = line.Substring(11, 3);
                            }
                            else if ((stag == ":64:" || stag == ":65:") && (str_refMT.Substring(0, 1) == "9"))
                            {
                                str_r = line.Substring(10, 3);
                            }
                            else if ((stag == ":90C:" || stag == ":90D:") && (str_refMT.Substring(0, 1) == "9"))
                            {
                                str_r = line.Substring(10, 3);
                            }
                        }
                    }
                    //strWriter.WriteLine(line);
                }
                file.Close();
                file.Dispose();
                LogAdd("currency=" + str_r);
                return str_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Check World-Check API
        /// </summary>
        /// <param name="strcif_file"></param>
        /// <param name="str_filename"></param>
        /// <param name="strReferenceNumber"></param>
        /// <param name="strBankNo"></param>
        /// <param name="strBranchNo"></param>
        /// <param name="intretry"></param>
        /// <param name="intdelay"></param>
        /// <param name="str_nation_col"></param>
        /// <param name="str_engname_col"></param>
        /// <param name="strRef"></param>
        /// <param name="strEngName"></param>
        /// <param name="iScore"></param>
        /// <param name="str_score"></param>
        /// <param name="str_dob_col"></param>
        /// <returns></returns>
        public Boolean checkPEPS(string strcif_file, string str_filename, string strReferenceNumber, string strBankNo, string strBranchNo, int intretry, int intdelay, string str_nation_col, string str_engname_col, ref string strRef, ref string strEngName, ref int iScore, string str_score, string str_dob_col, string str_cif_type)
        {
            bool quit = false;
            bool bol_r = false;
            int intloop = 0;
            Fortify fortifyService = new Fortify();

            while (!quit && intloop < intretry)
            {
                try
                {
                    intloop = intloop + 1;

                    string strNation = "";
                    string streName = "";
                    string strDob = "";

                    int iDOB = 0;

                    string[] str_ciftp = str_cif_type.Split('|');

                    string[] str_nationtp = str_nation_col.Split('|');
                    string[] str_engnametp = str_engname_col.Split('|');

                    //string[] str_nation = str_nation_col.Split(',');
                    //string[] str_engname = str_engname_col.Split(',');

                    System.IO.StreamReader file = new System.IO.StreamReader(fortifyService.PathManipulation(strcif_file));
                    string line = "";
                    string line_steam = "";
                    string[] arry_list;

                    LogAdd(string.Format("Try:{0}", intloop));

                    //2017/04/24 by danny 修改 取代換行 
                    while ((line = file.ReadLine()) != null)
                    {
                        if (str_filename.Substring(4, 2).ToUpper() == "CI")
                        {
                            line_steam += line;
                        }
                        else if (str_filename.Substring(4, 1).ToUpper() == "S" && str_filename.Substring(14, 4).ToString().Trim() != "1000")
                        {
                            if ((line.LastIndexOf(":59") > -1 || line.LastIndexOf(":50") > -1) && line.LastIndexOf(":50A") == -1)
                            {
                                
                                if (line.LastIndexOf(":59:") >= 0 || line.LastIndexOf(":50K:") >= 0)
                                {
                                    if (line.Substring(line.LastIndexOf(":59:")+4,1) == "/" || line.Substring(line.LastIndexOf(":50K:")+5, 1) == "/")
                                    {
                                        line = file.ReadLine();
                                        line_steam += line + ";";
                                    }
                                    else
                                    {
                                        line_steam += line.LastIndexOf(":59") >= 0 ? parstr(line, ":59") : parstr(line, ":50") + ";";
                                    }
                                }
                                else if (line.LastIndexOf(":59F:") >= 0 || line.LastIndexOf(":50F:") >= 0)
                                {
                                    if (line.Substring(line.LastIndexOf(":59F") + 4, 2) == "1/")
                                    {
                                        line_steam += line.LastIndexOf(":59") >= 0 ? parstr(line, ":59F") : parstr(line, ":50") + ";";
                                    }
                                    else
                                    {
                                        line = file.ReadLine();
                                        line_steam += line.LastIndexOf(":59") >= 0 ? parstr(line, ":59F") : parstr(line, ":50F") + ";";
                                    }
                                }
                            }
                        }
                        else if (str_filename.Substring(4, 1).ToUpper() == "F" && str_filename.Substring(14, 4).ToString().Trim() == "1000")
                        {
                            for (int i = 0; i < str_1000.Split(',').Count(); i++)
                            {
                                if (line.LastIndexOf(str_1000.Split(',')[i]) > -1)
                                {
                                    line_steam += parstr(line, str_1000.Split(',')[i]) + ";";
                                }
                            }
                        }
                    }
                    file.Close();

                    if (str_filename.Substring(4, 2).ToUpper() == "CI")
                    {
                        for(int i = 0; i < str_ciftp.Count(); i ++)
                        {
                            if (str_filename.Substring(14, 4).ToUpper().Trim() == str_ciftp[i])
                            {
                                arry_list = line_steam.Split('|');
                                LogAdd(string.Format("sanction text col count : {0}", arry_list.Length));

                                strDob = int.TryParse(str_dob_col, out iDOB) == true ? arry_list[int.Parse(str_dob_col)] : "";
                                LogAdd(string.Format("DOB: {0}", fortifyService.PathManipulation(strDob)));

                                string[] str_nation = str_nationtp[i].Split(',');

                                foreach (string nationcol in str_nation)
                                {
                                    if (int.Parse(nationcol) >= arry_list.Length)
                                    {
                                        break;
                                    }
                                    strNation += Checkstr(string.IsNullOrEmpty(arry_list[int.Parse(nationcol)]) == false ? arry_list[int.Parse(nationcol)] : "", "nation") + ';';
                                }

                                string[] str_engname = str_engnametp[i].Split(',');

                                foreach (string engnamecol in str_engname)
                                {
                                    if (int.Parse(engnamecol) >= arry_list.Length)
                                    {
                                        break;
                                    }
                                    streName += Checkstr(string.IsNullOrEmpty(arry_list[int.Parse(engnamecol)]) == false ? arry_list[int.Parse(engnamecol)].Replace('*', ' ') : "", "engname") + ';';
                                }
                            }
                        }

                        line_steam = streName.TrimEnd(';');
                    }
                    else
                    {
                        line_steam = line_steam.TrimEnd(';');
                        LogAdd(string.Format("EnglishName:{0}", fortifyService.PathManipulation(line_steam)));
                    }

                    ArrayList wcresult = new ArrayList();

                    strRef = str_filename.Substring(0, 25) + DateTime.Now.ToString("HHmmss");

                    int imaincase = 0;
                    int iallmatchcount = 0;
                    //20180912 call WCAPI
                    LogAdd("OFACWS=" + OFACWS_Enable);
                    if (OFACWS_Enable == "N")
                    {
                        foreach (string s in line_steam.Split(';'))
                        {
                            /* Wait
                            if (strDob != "" && strNation.TrimEnd(';') != "" && line_steam != "")
                            {

                            }
                            else
                            */
                            if (line_steam != "")
                            {
                                if (s == "")
                                {
                                    //prepare for pass string || !string.IsNullOrEmpty(str_exclude) && str_exclude.Split('|').ToList().Any(exclude => s.ToUpper().Contains(exclude))
                                    //LogAdd("pass to send wc1 :" + s + ".");
                                    continue;
                                }

                                LogAdd("----Create Case Start----");
                                string casesystemid = post_scansanctiondata("", "Q", s, strRef);
                                LogAdd("----Create Case End----");

                                System.Threading.Thread.Sleep(1000);

                                LogAdd("----Screen Text Start----");
                                var result = post_scansanctiondata(casesystemid, "S", s, strRef);
                                LogAdd("----Screen Text End----");

                                System.Threading.Thread.Sleep(1000);

                                LogAdd("----Get Result Start----");
                                wcresult = wc_getresult(casesystemid, "");
                                LogAdd("----Get Result End----");

                                if (wcresult != null && wcresult.Count > 0)
                                {
                                    LogAdd("text " + fortifyService.PathManipulation(s) + ", hit count:" + wcresult.Count);

                                    iallmatchcount = iallmatchcount + wcresult.Count;
                                    //create case
                                    LogAdd("check is first case:" + imaincase);
                                    if (imaincase == 0)
                                    {
                                        LogAdd("---case create start---");
                                        InsertFilterTran(strRef, strBankNo, wcresult.Count.ToString());
                                        LogAdd("---case create end---");
                                    }

                                    for (int i = 0; i < wcresult.Count; i++)
                                    {
                                        ResultDetail resultdetail = JsonConvert.DeserializeObject<ResultDetail>(fortifyService.PathManipulation(wcresult[i].ToString()));

                                        string strentnum = resultdetail.referenceId.Substring(resultdetail.referenceId.LastIndexOf('_') + 1);
                                        string strprogram = resultdetail.sources[0].Substring(resultdetail.sources[0].LastIndexOf('_') + 1);

                                        if (Checkstr(resultdetail.matchedTerm, "engname") != "")
                                        {
                                            //check sdn data
                                            CheckSdn(strentnum, resultdetail.matchedTerm, strBankNo, strprogram, resultdetail.referenceId, resultdetail.matchedNameType, resultdetail.sources, resultdetail.categories);

                                            //insert MatchTable
                                            InsertMatchTable(strRef, strentnum, resultdetail.submittedTerm, resultdetail.matchedTerm, resultdetail.matchStrength);
                                        }
                                        else
                                        {
                                            //check language non english word
                                            //iallmatchcount = iallmatchcount - 1;
                                            ArrayList wcnames = wc_getresult(casesystemid, resultdetail.referenceId);
                                            for (int j = 0; j < wcnames.Count; j++)
                                            {
                                                string wcnametype = ((BlackListService.AML_Data.Names)new System.Collections.ArrayList(wcnames)[j]).type;

                                                string wcfullname = ((BlackListService.AML_Data.Names)new System.Collections.ArrayList(wcnames)[j]).fullName;

                                                if (wcnametype.ToUpper() == "PRIMARY")
                                                {
                                                    //check sdn data
                                                    LogAdd("---check sdn non eng---");
                                                    CheckSdn(strentnum, wcfullname, strBankNo, strprogram, resultdetail.referenceId, resultdetail.matchedNameType, resultdetail.sources, resultdetail.categories);

                                                    //insert MatchTable
                                                    LogAdd("---insert match non eng---");
                                                    InsertMatchTable(strRef, strentnum, resultdetail.submittedTerm, wcfullname, resultdetail.matchStrength);
                                                }
                                            }
                                        }
                                    }

                                    // insert MsgTable
                                    if (imaincase == 0)
                                        InsertMsgTable(strRef, strcif_file);
                                }
                                else
                                {
                                    imaincase = imaincase - 1;
                                }
                            }
                            imaincase = imaincase + 1;
                            //update the case total match count
                            Updatematchcount(iallmatchcount, strRef);
                        }
                    }

                    quit = true;

                }
                catch (Exception ex)
                {
                    Logerr(ex.ToString());

                    if (ex.ToString().IndexOf("timed out") > 0)
                    {
                        quit = false;
                        System.Threading.Thread.Sleep(intdelay);
                    }
                    else
                    {
                        //client.Close();
                        quit = true;
                        throw ex;
                    }
                }
            }
            return bol_r;
        }

        private string post_scansanctiondata(string caseSystemId, string str_Mode, string strsanctiondata, string refstr)
        {
            Fortify fortifyService = new Fortify();
            DateTime dateValue = DateTime.UtcNow; // get the datetime NOW GMT

            string date = dateValue.ToString("R"); // WC1 header requires GMT datetime stamp

            string requestendpoint = apiurl;
            if (str_Mode == "S")
            {
                requestendpoint = requestendpoint + "/" + caseSystemId + "/screeningRequest";
            }

            string strref = refstr;

            customFields = "[{\"typeId\":\""+ typeid + "\",\"value\":\"" + strref + "\"}]";

            //customFields = "[]";

            string postData = "{\"secondaryFields\":" + secondaryFields + ",\"entityType\":\"" + entityType + "\",\"customFields\":" + customFields + ",\"groupId\":\"" + groupId + "\",\"providerTypes\":" + providerTypes + ",\"name\":\"" + strsanctiondata + "\"}";

            string msg = postData;

            UTF8Encoding encoding = new UTF8Encoding();
            byte[] byte1 = encoding.GetBytes(fortifyService.PathManipulation(postData));

            // Assemble the POST request - NOTE every character including spaces have to be EXACT 
            // for the API server to decode the authorization signature       
            string strUrl = requestendpoint.Replace("https://" + gatewayhost, "");

            string dataToSign = "(request-target): post " + strUrl + "\n" +
                "host: " + gatewayhost + "\n" +   // no https only the host name
                "date: " + date + "\n" +          // GMT date as a string
                "content-type: " + "application/json" + "\n" +
                "content-length: " + byte1.Length + "\n" +
                 msg;

            LogAdd("---api secret---");
            LogAdd(fortifyService.PathManipulation(apisecret));
            LogAdd("---dataToSign---");
            LogAdd(fortifyService.PathManipulation(dataToSign));
            LogAdd("string hmac = generateAuthHeader(dataToSign, apisecret);");
            // The Request and API secret are now combined and encrypted
            string hmac = generateAuthHeader(dataToSign, apisecret);

            // Assemble the authorization string - This needs to match the dataToSign elements 
            // i.e. requires  host date content-type content-length
            //- NOTE every character including spaces have to be EXACT else decryption will fail with 401 Unauthorized
            string authorisation = "Signature keyId=\"" + apikey + "\",algorithm=\"hmac-sha256\",headers=\"(request-target) host date content-type content-length\",signature=\"" + hmac + "\"";

            LogAdd("---Hmac---");
            LogAdd(hmac);

            // Send the Request to the API server
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(requestendpoint);

            // Set the Headers
            WebReq.Method = "POST";
            WebReq.Headers.Add("Authorization", fortifyService.PathManipulation(authorisation));
            WebReq.Headers.Add("Cache-Control", "no-cache");
            WebReq.ContentLength = msg.Length;

            // Set the content type of the data being posted.
            Type type = WebReq.Headers.GetType();
            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            MethodInfo methodInfo = type.GetMethod("AddWithoutValidate", flags);
            object[] myPara = new object[2];
            myPara[0] = "Date";
            myPara[1] = dateValue.ToString("R");
            methodInfo.Invoke(WebReq.Headers, myPara);

            WebReq.ContentType = "application/json";
            WebReq.ContentLength = byte1.Length;

            //proxy
            if (strProxy!= String.Empty)
            {
                string[] strByPass = new string[] { @"192.\.168\..*", @"172\.17\..*" };
                WebReq.Proxy = new WebProxy(new Uri(strProxy), true, strByPass);
            }

            //SSL TLS1.2
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return CheckCert();
            };

            //for SSL
            if (OFACWS_SSL == "Y")
            {
                System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;
            }

            Stream newStream = WebReq.GetRequestStream();
            newStream.Write(byte1, 0, byte1.Length);

            bool bol_success = false;

            //2018.03 add api retry function
            for (int i = 0; i < apiretry; i++)
            {
                if (bol_success == true)
                {
                    break;
                }
                else
                {
                    System.Threading.Thread.Sleep(1000 * retrySec);
                    LogAdd("Try times:" + i);
                }
                try
                {
                    // Get the Response - Status OK
                    HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                    // Status information about the request

                    // Get the Response data
                    Stream Answer = WebResp.GetResponseStream();
                    StreamReader _Answer = new StreamReader(Answer);
                    string jsontxt = _Answer.ReadToEnd();

                    LogAdd("return value:" + jsontxt);

                    // convert json text to a pretty printout
                    var obj = JsonConvert.DeserializeObject(fortifyService.PathManipulation(jsontxt));
                    var f = JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented);

                    if (str_Mode == "Q")
                    {
                        wcapi wcapi = JsonConvert.DeserializeObject<wcapi>(fortifyService.PathManipulation(f));
                        caseSystemId = wcapi.caseSystemId;
                        LogAdd("caseSysID:" + fortifyService.PathManipulation(caseSystemId));
                    }
                    if (str_Mode == "S")
                    {
                        caseSystemId = f;
                    }
                    bol_success = true;
                }
                catch (WebException ex)
                {
                    Logerr(ex.ToString());
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        Logerr("WebException code:" + ((int)response.StatusCode).ToString());
                    }
                }
            }
            return caseSystemId;
        }

        private ArrayList wc_getresult(string caseSystemId, string StrEntID)
        {
            Fortify fortifyService = new Fortify();
            DateTime dateValue = DateTime.UtcNow; // get the datetime NOW GMT

            string date = dateValue.ToString("R"); // WC1 header requires GMT datetime stamp

            //set host and credentials to the WC1 API Pilot server WC1SampleClientAPI account

            string requestendpoint = apiurl + "/" + caseSystemId + "/results";
            
            if (StrEntID != "")
            {
                requestendpoint = apiurl.Replace("cases", "reference/profile/") + StrEntID;
            }

            // Assemble the GET request - NOTE every character including spaces have to be EXACT 
            // for the API server to decode the authorization signature       
            string strUrl = requestendpoint.Replace("https://" + gatewayhost, "");
            if (strUrl.IndexOf('?') > 0)
                strUrl = strUrl.Replace(strUrl.Substring(strUrl.IndexOf('?')), "");
            string dataToSign = "(request-target): get " + strUrl + "\n" +
                "host: " + gatewayhost + "\n" +   // no https only the host name
                "date: " + date;                  // GMT date as a string
            // The Request and API secret are now combined and encrypted
            string hmac = generateAuthHeader(dataToSign, apisecret);

            // Assemble the authorization string - This needs to match the dataToSign elements 
            // i.e. requires ONLY host date (no content body for a GET request)
            //- NOTE every character including spaces have to be EXACT else decryption will fail with 401 Unauthorized
            string authorisation = "Signature keyId=\"" + apikey + "\",algorithm=\"hmac-sha256\",headers=\"(request-target) host date\",signature=\"" + hmac + "\"";

            // Send the Request to the API server
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(requestendpoint);
            // Set the Headers
            WebReq.Method = "GET";
            WebReq.Headers.Add("Authorization", fortifyService.PathManipulation(authorisation));
            WebReq.Headers.Add("Cache-Control", "no-cache");
            Type type = WebReq.Headers.GetType();
            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            MethodInfo methodInfo = type.GetMethod("AddWithoutValidate", flags);
            object[] myPara = new object[2];
            myPara[0] = "Date";
            myPara[1] = dateValue.ToString("R");
            methodInfo.Invoke(WebReq.Headers, myPara);
            LogAdd("---Get Ready---");

            //proxy
            if (strProxy != String.Empty)
            {
                string[] strByPass = new string[] { @"192.\.168\..*", @"172\.17\..*" };
                WebReq.Proxy = new WebProxy(new Uri(strProxy), true, strByPass);
            }

            //SSL TLS1.2
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return CheckCert();
            };

            //for SSL
            if (OFACWS_SSL == "Y")
            {
                System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;
            }

            try
            {
                // Get the Response - Status OK
                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                // Status information about the request

                // Get the Response data
                Stream Answer = WebResp.GetResponseStream();
                StreamReader _Answer = new StreamReader(Answer);
                string jsontxt = _Answer.ReadToEnd();

                // convert json text to a pretty printout
                var obj = JsonConvert.DeserializeObject(fortifyService.PathManipulation(jsontxt));
                var f = JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented);

                ArrayList result = new ArrayList();

                if (StrEntID == "")
                {
                    result = JsonConvert.DeserializeObject<ArrayList>(fortifyService.PathManipulation(f));
                }
                else
                {
                    Profile prof = JsonConvert.DeserializeObject<Profile>(f);
                    
                    for (int i = 0; i < prof.names.Count(); i++)
                    {
                        result.Add(prof.names[i]);
                    }
                }

                LogAdd("---Get Finish---");
                return result;
            }
            catch (WebException ex)
            {
                Logerr(ex.ToString());
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    Logerr("WebException code:" + ((int)response.StatusCode).ToString());
                }
                return null;
            }
        }

        public class wcapi
        {
            public string caseId { get; set; }
            public string name { get; set; }

            public string[] providerTypes { get; set; }
            public custfields[] customFields { get; set; }
            public string[] secondaryFields { get; set; }
            public string groupId { get; set; }
            public string entityType { get; set; }
            public string caseSystemId { get; set; }
            public string caseScreeningState { get; set; }
            public string lifecycleState { get; set; }
            public creator creator { get; set; }
            public modifier modifier { get; set; }
            public string creationDate { get; set; }
            public string modificationDate { get; set; }
            public string outstandingActions { get; set; }
        }

        public class custfields
        {
            public string typeId { get; set; }
            public string value { get; set; }
            public string dateTimeValue { get; set; }
        }

        public class creator
        {
            public string userId { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string email { get; set; }
            public string status { get; set; }
        }

        public class modifier
        {
            public string userId { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string email { get; set; }
            public string status { get; set; }

        }

        public class ResultDetail
        {
            public string resultId { get; set; }
            public string referenceId { get; set; }
            public string matchStrength { get; set; }
            public string matchedTerm { get; set; }
            public string submittedTerm { get; set; }
            public string matchedNameType { get; set; }
            public string[] secondaryFieldResults { get; set; }
            public string[] sources { get; set; }
            public string[] categories { get; set; }
            public string creationDate { get; set; }
            public string modificationDate { get; set; }
            public string resolution { get; set; }
            public ResultReview resultReview { get; set; }
        }

        public class ResultReview
        {
            public string reviewRequired { get; set; }
            public string reviewRequiredDate { get; set; }
            public string reviewRemark { get; set; }
            public string reviewDate { get; set; }
        }

        public class Profile
        {
            public string entityId { get; set; }
            public Names[] names { get; set; }
        }

        public class Names
        {
            public string fullName { get; set; }
            public string givenName { get; set; }
            public LanguageCode languageCode { get; set; }
            public string lastName { get; set; }
            public string originalScript { get; set; }
            public string prefix { get; set; }
            public string suffix { get; set; }
            public string type { get; set; }
        }

        public class LanguageCode
        {
            public string code { get; set; }
            public string name { get; set; }
        }

        private static string generateAuthHeader(string dataToSign, string apisecret)
        {
            byte[] secretKey = Encoding.UTF8.GetBytes(apisecret);
            HMACSHA256 hmac = new HMACSHA256(secretKey);
            hmac.Initialize();

            byte[] bytes = Encoding.UTF8.GetBytes(dataToSign);
            byte[] rawHmac = hmac.ComputeHash(bytes);
            hmac.Clear();
            return (Convert.ToBase64String(rawHmac));

        }

        private string Checkstr(string str_check,string str_flag)
        {
            string str_return = "";

            bool bolR = true;

            if (System.Text.Encoding.Default.GetBytes(str_check).Length > 1)
            {
                for (int i = 0; i < str_check.Length; i++)
                {
                    int AsciiCodeO = (int)System.Convert.ToChar(str_check[i]);

                    if (AsciiCodeO < 32 || AsciiCodeO > 126)
                    {
                        bolR = false;
                    }
                }
            }
            else
            {
                bolR = false;
            }

            if (bolR)
            {
                if (str_flag == "nation")
                {
                    str_return = str_check.Substring(0, 2);
                }
                else
                {
                    str_return = str_check.Replace('*',' ');
                }
            }
            else
            {
                str_return = "";
            }

            return str_return.Trim();
        }

        private string parstr(string str_line, string str_code)
        {
            string str_return = "";
            int istart = 0;
            int ifirst = 0;
            int ilast = 0;

            if (str_code == ":50" || str_code == ":59")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == ":")
                    {
                        str_return = str_line.Substring(i + 1);
                        break;
                    }
                }
            }
            else if (str_code == ":59F" || str_code == ":50F")
            {
                istart = 0;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "/")
                    {
                        str_return = str_line.Substring(i + 1);
                        break;
                    }
                }
            }
            else if (str_code == "{3100}" || str_code == "{3400}")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length + 9;

                
                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "*")
                    {
                        str_return = str_line.Substring(istart, i - istart);
                        break;
                    }
                    else if (str_line[i].ToString() == "{" || str_line[i + 1].ToString() == "{")
                    {
                        //第一個*後面有可能接下一個tag,那就跳過不抓
                        str_return = "";
                        break;
                    }

                    if (i- istart > 18)//maxlength 18
                    {
                        str_return = str_line.Substring(istart, 18);
                        break;
                    }
                }

            }
            else if (str_code == "{4000}" || str_code == "{4100}" || str_code == "{4200}" || str_code == "{4400}" || str_code == "{5000}" || str_code == "{5100}" || str_code == "{5200}")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "*")
                    {
                        if (ifirst == 0)
                        {
                            ifirst = i;
                        }
                        else
                        {
                            ilast = i;
                            str_return = str_line.Substring(ifirst + 1, ilast - ifirst - 1);
                            break;
                        }
                    }

                    if(ifirst != 0 && str_line[ifirst + 1].ToString() == "{")
                    {
                        //第一個*後面有可能接下一個tag,那就跳過不抓
                        str_return = "";
                        break;
                    }

                    if (ifirst != 0 && i - ifirst > 35)//maxlength 35
                    {
                        str_return = str_line.Substring(ifirst + 1, 35);
                        break;
                    }
                }
            }
            else if (str_code == "{5010}")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "*")
                    {
                        if (ifirst == 0 && str_line[i+1].ToString() == "1" && str_line[i+2].ToString()=="/")
                        {
                            ifirst = i;
                        }
                        else if (ifirst != 0)
                        {
                            ilast = i;
                            str_return = str_line.Substring(ifirst + 3, ilast - ifirst - 3);
                            break;
                        }
                    }

                    if (ifirst != 0 && str_line[ifirst + 1].ToString() == "{")
                    {
                        //第一個*後面有可能接下一個tag,那就跳過不抓
                        str_return = "";
                        break;
                    }

                    if (ifirst != 0 && i - ifirst > 35)//maxlength 35
                    {
                        str_return = str_line.Substring(ifirst + 1, 35);
                        break;
                    }
                }
            }
            else if (str_code == "{7050}" || str_code == "{7059}")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "*")
                    {
                        if (ifirst == 0)
                        {
                            ifirst = 1;
                        }
                        else if (ifirst == 1)
                        {
                            ifirst = i;
                        }
                        else
                        {
                            ilast = i;
                            str_return = str_line.Substring(ifirst + 1, ilast - ifirst - 1);
                            break;
                        }
                    }

                    if (ifirst != 0 && str_line[ifirst + 1].ToString() == "{")
                    {
                        //第二個*後面有可能接下一個tag,那就跳過不抓
                        str_return = "";
                        break;
                    }

                    if (ifirst > 1 && i - ifirst > 35)//maxlength 35
                    {
                        str_return = str_line.Substring(ifirst + 1, 35);
                        break;
                    }
                }
            }

            return str_return.Trim();
        }

        private string InClauseSql(string name, List<string> param)
        {
            StringBuilder sql = new StringBuilder();

            for (int i = 0; i < param.Count(); i++)
                sql.Append(string.Format("@{ 0}{1},", name, i.ToString()));

            return sql.ToString().TrimEnd(',');
        }

        private int ExecuteSQL(DbCommand cmd)
        {
            using (DbConnection conn = database.CreateConnection())
            {
                conn.Open();
                DbTransaction trans = conn.BeginTransaction();

                try
                {
                    int rows = database.ExecuteNonQuery(cmd, trans);
                    trans.Commit();
                    conn.Close();
                    return rows;
                }
                catch
                {
                    trans.Rollback();
                    conn.Close();
                    throw;
                }
            }
        }

        private Boolean CheckCert()
        {
            Boolean bol_r = false;

            if ("a" == "a")
            {
                bol_r = true;
            }

            return bol_r;
        }

        private void LogAdd(string logstring)
        {
            Fortify fortifyService = new Fortify();
            log.Info(fortifyService.PathManipulation(logstring));
        }

        private void Logerr(string logstring)
        {
            Fortify fortifyService = new Fortify();
            log.Error(fortifyService.PathManipulation(logstring));
        }

        private void Logerr(string logstring, Exception ex)
        {
            Fortify fortifyService = new Fortify();
            log.Error(fortifyService.PathManipulation(logstring), ex);
        }
    }
}
