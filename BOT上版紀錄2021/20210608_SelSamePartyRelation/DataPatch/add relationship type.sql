USE PBSA
GO

insert into RelationshipType(code,name, CreateOper,CreateDate,PrimaryRelation, Score, EnabledForAutoDiscovery, Source, IncludeInRisk)
values ('SameAE', 'Same Authorize Entities', 'Primeadmin', getdate(), 0,0,0,1,1)
insert into RelationshipType(code,name, CreateOper,CreateDate,PrimaryRelation, Score, EnabledForAutoDiscovery, Source, IncludeInRisk)
values ('SameAI', 'Same Authorize Individuals', 'Primeadmin', getdate(), 0,0,0,1,1)
insert into RelationshipType(code,name, CreateOper,CreateDate,PrimaryRelation, Score, EnabledForAutoDiscovery, Source, IncludeInRisk)
values ('SameAS', 'Same Authorize Signatories', 'Primeadmin', getdate(), 0,0,0,1,1)
insert into RelationshipType(code,name, CreateOper,CreateDate,PrimaryRelation, Score, EnabledForAutoDiscovery, Source, IncludeInRisk)
values ('SameBC', 'Same Bene Owner', 'Primeadmin', getdate(), 0,0,0,1,1)

insert into RelationshipType(code,name, CreateOper,CreateDate,PrimaryRelation, Score, EnabledForAutoDiscovery, Source, IncludeInRisk)
values ('SameSH', 'Same Share holder', 'Primeadmin', getdate(), 0,0,0,1,1)

insert into RelationshipType(code,name, CreateOper,CreateDate,PrimaryRelation, Score, EnabledForAutoDiscovery, Source, IncludeInRisk)
values ('SameDIR', 'Same Director', 'Primeadmin', getdate(), 0,0,0,1,1)

insert into RelationshipType(code,name, CreateOper,CreateDate,PrimaryRelation, Score, EnabledForAutoDiscovery, Source, IncludeInRisk)
values ('SameTS', 'Same Trust Settlor', 'Primeadmin', getdate(), 0,0,0,1,1)

insert into RelationshipType(code,name, CreateOper,CreateDate,PrimaryRelation, Score, EnabledForAutoDiscovery, Source, IncludeInRisk)
values ('SamePT', 'Partner ship', 'Primeadmin', getdate(), 0,0,0,1,1)
