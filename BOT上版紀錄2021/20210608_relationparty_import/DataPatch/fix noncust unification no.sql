--check noncust
select name, count(id) cnt from customer where id like '%noncust%'
group by name
order by 2 desc

select id, name, tin from customer where id like '%noncust%'
and name in (select name from customer where id like '%noncust%'
group by name having count(id) > 1)

--update unification no
begin tran
update customer set tin = T.tin
--select c1.tin, T.tin, c1.Name 
from customer c1 
 join (
select name, tin from customer where name in (
select name from customer where id like '%noncust%'
group by name
having count(id) > 1) and tin is not null
) T on c1. name = T.name
and c1.tin is  null 

commit

--此步驟執行後在commit之前，需要先檢查是否有被誤刪的資料
begin tran
delete from customer where id like '%noncust%'
and name in (select name from customer where id like '%noncust%'
group by name having count(name) > 1)
and CreateDate = (select max(d.CreateDate) from Customer d where d.Name = customer.Name )

--記得做commit

