USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_XInTOutT]    Script Date: 7/15/2021 4:31:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USR_XInTOutT] (@WLCode SCode, @testAlert INT,
	@day  int,
	@percent  int,
	@inActivityTypeList varchar(8000),
	@outActivityTypeList varchar(8000),
	@minSumAmt money,
	@riskClassList varchar(8000),
	@customerTypeList varchar(8000),
	@inboundCashType smallint,
	@outboundCashType smallint,
	@UseRelAcct Bit, 
	@UseRelCust Bit, 
    @ExcludeRelList varchar(8000)
	)
AS
/* RULE AND PARAMETER DESCRIPTION
	Detects customers with incoming (receive) transactions of a 
	specified activity type and outgoing (pay) transactions of a specified 
	activity type where the total amounts differ by less than or equal to a 
	specified percentage.  This rule evaluates transactions that are within
	the specified number of days and the aggregate amount of the qualifying
	transactions exceeds the specified minimum amount.  Additional 
	constraints include Cash/Non-Cash for inbound and outbound activity types,
	Customer Type and Risk Class.  Designed to be run Post-EOD
	
	@day = Indicates the number of days that are included in the evaluation of 
		transactions. This includes business and non-business days prior to
		the day the rule is executed.
	@percent = Percentage of the difference in amounts between the deposits 
		(receive) and the withdrawals (pay)
	@inActivityType = A comma separated list of inbound activity types to 
		be evaluated
        @outActivityType = A comma separated list of outbound activity types to 
		be evaluated 
	@minSumAmt = The Minimum aggregated transaction amount that must be exceeded.  
	@riskClass = A comma separated list of Risk Classes to include in the evaluation,
		 use -ALL- for any risk class.
	@customerType = A comma separated list of Customer Types to include in the 
		evaluation, use -ALL- for any customer type.
	@inboundCashType = For the activity types specified in the parameter 
		InActivityType, use 1 to restrict them to only Cash transactions,
		use 0 to restrict them to Non-Cash, 2 for both
	@outboundCashType = For the activity types specified in the parameter 
		OutActivityType, use 1 to restrict them to only 
			    Cash transactions, use 0 to restrict them to Non-Cash, 2 for both
	@UseRelAcct = Include Related Accounts (1=yes, 0=no) 
	@UseRelCust = Include Related Parties (1=yes, 0=no) 
	@ExcludeRelList = Exclude Relationship Types (comma separated list or '-NONE-')
*/

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@MINDATE INT
Declare @cust LONGNAME
Declare @currDateTime datetime 
Declare @CurrBookDate int

DECLARE @SetDate DATETIME

DECLARE @TT table
(
	Cust VARCHAR(40),
	Recv MONEY,
	Pay MONEY,
	Mindate INT,
	Maxdate INT,
	Descr VARCHAR(2000)
)

DECLARE @TT1 table
(
	Cust	VARCHAR(35),
	TranNo	INT,
	BaseAmt MONEY, 
	RecvPay INT,
	BookDate INT)

--JUL-15-2021 add current acivity for checking duplicate
DECLARE @CrrTT table
(
	Cust	VARCHAR(35),
	TranNo	INT,
	BaseAmt MONEY, 
	RecvPay INT,
	BookDate INT)

SET NOCOUNT ON
SET @stat = 0

IF (ISNULL(@inActivityTypeList,'') = '' 
		OR UPPER(ISNULL(@inActivityTypeList,'-ALL-')) = '-ALL-')
	SELECT @inActivityTypeList = NULL
ELSE
	SELECT @inActivityTypeList = ',' + 
		REPLACE(ltrim(rtrim(@inActivityTypeList)),CHAR(32),'') + ','

IF (ISNULL(@outActivityTypeList,'') = '' 
		OR UPPER(ISNULL(@outActivityTypeList,'-ALL-')) = '-ALL-')
	SELECT @outActivityTypeList = NULL
ELSE
	SELECT @outActivityTypeList = ',' + 
		REPLACE(ltrim(rtrim(@outActivityTypeList)),CHAR(32),'') + ','

IF (ISNULL(@riskClassList,'') = '' 
		OR UPPER(ISNULL(@riskClassList,'-ALL-')) = '-ALL-')
	SELECT @riskClassList = NULL
ELSE
	SELECT @riskClassList = ',' +
		REPLACE(ltrim(rtrim(@riskClassList)),CHAR(32),'') + ','

IF (ISNULL(@customerTypeList,'') = '' 
		OR UPPER(ISNULL(@customerTypeList,'-ALL-')) = '-ALL-')
	SELECT @customerTypeList = NULL
ELSE
	SELECT @customerTypeList = ',' +
		REPLACE(ltrim(rtrim(@customerTypeList)),CHAR(32),'') + ','

IF LTRIM(RTRIM(@ExcludeRelList)) = '-NONE-' OR LTRIM(RTRIM(@ExcludeRelList)) = ''
	SELECT @ExcludeRelList = NULL
ELSE
SELECT @ExcludeRelList = ',' + 
	REPLACE(LTRIM(RTRIM(@ExcludeRelList)),' ','')+ ','

IF (@inboundCashType = 2)
	SET @inboundCashType = NULL

IF (@OutboundCashType = 2)
	SET @OutboundCashType = NULL

--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_XInTOutT
ELSE
	-- Already in a transaction
	SAVE TRAN USR_XInTOutT
/* End standard stored procedure transaction header */

/*  standard Rules Header */

-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @description = [Desc], @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList WITH (NOLOCK)
WHERE WLCode = @WLCode

SELECT @day = ABS(@day)

--When the transactions are picked up
SET @minDate = dbo.ConvertSqlDateToInt(
	DATEADD(d, -1 * @day, CONVERT(VARCHAR, @SetDate)))

Insert Into @TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select cust, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist WITH (NOLOCK) 
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
)
AND bookdate >= @minDate  

--JUL-15-2021 add current acivity for checking duplicate
Insert Into @CrrTT (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select cust, Tranno, BaseAmt, RecvPay, BookDate 
From Activity WITH (NOLOCK) 
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
)

-- Related Parties
Insert Into @TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select Distinct pr.PartyId, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist ah WITH (NOLOCK) JOIN PartyRelation pr ON ah.Cust = pr.RelatedParty
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
)
AND bookdate >= @minDate 
AND @UseRelCust = 1 AND ah.Cust <> pr.PartyID AND pr.Deleted <> 1  
AND ((ISNULL(@ExcludeRelList,'') = '' OR 
	CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
AND Exists (select * from @TT1 tt Where tt.Cust = pr.PartyId)
AND Not Exists (select * from @TT1 tt Where tt.Cust = pr.PartyId AND tt.TranNo = ah.tranno)

-- Related Parties (reverse direction)
Insert Into @TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select Distinct pr.RelatedParty, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist ah WITH (NOLOCK)  JOIN PartyRelation pr ON ah.Cust = pr.PartyID
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
)
AND bookdate >= @minDate 
AND @UseRelCust = 1 AND ah.Cust <> pr.RelatedParty AND pr.Deleted <> 1
AND ((ISNULL(@ExcludeRelList,'') = '' OR 
	CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
AND Exists (select * from @TT1 tt Where tt.Cust = pr.RelatedParty)
AND Not Exists (select * from @TT1 tt Where tt.Cust = pr.RelatedParty AND tt.TranNo = ah.tranno)

-- Related Accounts
Insert Into @TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select Distinct ao.Cust, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist ah WITH (NOLOCK) JOIN AccountOwner ao 
On ah.Account = ao.Account and ah.Cust <> ao.Cust 
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
)
AND bookdate >= @minDate
AND @UseRelAcct = 1
AND ((ISNULL(@ExcludeRelList,'') = '' OR 
	CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ao.Relationship,''))) + ',', @ExcludeRelList) = 0))
AND Exists (select * from @TT1 tt Where tt.Cust = ao.cust)
AND Not Exists (select * from @TT1 tt Where tt.Cust = ao.Cust AND tt.TranNo = ah.tranno)

--JUL-1-2021 delete TT1 where not exists in current activity
DELETE a FROM @TT1 a WHERE NOT EXISTS (SELECT 1 FROM @CrrTT b WHERE a.Cust=b.Cust)

--JUN-15-2021 add current activity
Insert Into @TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select distinct cust, Tranno, BaseAmt, RecvPay, BookDate 
from @CrrTT 

INSERT INTO @TT(Cust, recv, pay, mindate, maxdate, Descr)
SELECT cust, SUM(recv) recv, SUM(pay) pay, MIN(mindate) mindate, 
       MAX(maxdate) maxdate, 'Cust:  ''' + cust 
	+ ''' has incoming transfer '''
	+ CONVERT(VARCHAR, SUM(recv))
	+ ''' followed by an outgoing transfer '''
	+ CONVERT(VARCHAR, SUM(pay))
	+ ''' where the percentage '
	+ 'differs by '
        + CASE 
          WHEN SUM(pay) >= SUM(recv) AND SUM(pay) <> 0 THEN 
           CONVERT(VARCHAR, ABS(100 - 100 * SUM(recv)/SUM(pay))) 
          WHEN SUM(pay) < SUM(recv) AND SUM(recv) <> 0 THEN 
           CONVERT(VARCHAR, ABS(100 - 100 * SUM(pay)/SUM(recv)))
          END
	+ ' percent from bookdate ''' 
	+ CONVERT(VARCHAR, MIN(mindate)) + ''' to '''
	+ CONVERT(VARCHAR, MAX(maxdate)) + '''' descr
FROM  (
	SELECT cust, 		
	CASE WHEN recvpay = 1 THEN SUM(baseamt) 
		ELSE 0
		END recv,
	CASE WHEN recvpay = 2 THEN SUM(baseamt) 
		ELSE 0
		END pay,
	MIN(bookdate) AS mindate,
	MAX(bookdate) AS maxdate
	FROM @TT1
	GROUP BY cust, recvpay
) a, 
	Customer WITH (NOLOCK)
	WHERE   a.cust = Customer.Id
	AND ((@riskClassList IS NULL OR 
			CHARINDEX(',' + 
				LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0))
	AND ((@customerTypeList IS NULL OR
			CHARINDEX(',' + 
				LTRIM(RTRIM(Customer.type)) + ',', @customerTypeList ) > 0))
GROUP BY cust
HAVING SUM(recv) <> 0 AND SUM(pay) <> 0 
AND (100 * SUM(recv) between (100 - @percent)*SUM(pay) and (100 + @percent)*SUM(pay)
 OR  100 * SUM(pay) between (100 - @percent)*SUM(recv) and (100 + @percent)*SUM(recv))
AND (SUM(recv) > @MinSumAmt OR SUM(pay) > @MinSumAmt)
Set @currDateTime = GETDATE()
set @CurrBookDate = dbo.ConvertSqlDateToInt(GetDate())

IF @testAlert = 1 BEGIN
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, CUST, IsTest) 
	  SELECT @WLCode, descr, 0, dbo.ConvertSqlDateToInt(getdate()), cust, 1 FROM @TT 

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	  SELECT Distinct 'Alert', AlertNo, TRANNO 
		FROM @TT1 A 
         JOIN @TT T ON a.Cust = t.Cust
         JOIN Alert AL ON a.Cust = al.Cust
        WHERE 
	al.WLCode = @WLCode
	AND al.CreateDate between @currDateTime AND GETDATE()
	SELECT @STAT = @@ERROR 
	IF @stat <> 0 GOTO EndOfProc
END ELSE BEGIN
	IF @WLType = 0 BEGIN
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, CUST) 
	  SELECT @WLCode, descr, 0, dbo.ConvertSqlDateToInt(getdate()), cust FROM @TT 
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	  SELECT Distinct 'Alert', AlertNo, TRANNO 
		FROM @TT1 A 
         JOIN @TT T ON a.Cust = t.Cust
         JOIN Alert AL ON a.Cust = al.Cust
        WHERE 
	al.WLCode = @WLCode
	AND al.CreateDate between @currDateTime AND GETDATE()

	SELECT @STAT = @@ERROR 
	IF @stat <> 0 GOTO EndOfProc
	END ELSE IF @WLType = 1 BEGIN	
                INSERT INTO SUSPICIOUSACTIVITY (BOOKDATE, CUST, SUSPTYPE, 
                        INCNTTOLPERC, OUTCNTTOLPERC, INAMTTOLPERC, 
			OUTAMTTOLPERC, WLCode, WLDESC, CreateTime)
		SELECT	dbo.ConvertSqlDateToInt(getdate()), Cust, 'RULE', 0, 0, 0, 0, 
			@WLCode, descr, @currDateTime
		FROM @TT
		IF @stat <> 0 GOTO EndOfProc
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT Distinct 'SUSPACT', RecNo, TRANNO 
			FROM @TT1 A
        	JOIN @TT T ON a.Cust = t.Cust
             	JOIN SuspiciousActivity s ON a.Cust = s.Cust
        	WHERE 
	        s.WLCode = @WLCode
	        AND s.bookdate = @CurrBookDate
			AND s.createTime between @currDateTime AND GETDATE()

		SELECT @STAT = @@ERROR
		IF @stat <> 0 GOTO EndOfProc
	END
END
EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_XInTOutT

  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_XInTOutT

RETURN @stat


GO


