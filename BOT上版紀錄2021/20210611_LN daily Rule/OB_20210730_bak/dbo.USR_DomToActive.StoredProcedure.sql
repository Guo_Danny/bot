USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_DomToActive]    Script Date: 12/3/2020 9:29:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[USR_DomToActive] (
 	@WLCode SCode, 
 	@testAlert INT,
 	@day INT, 
 	@BranchList VARCHAR(8000), 
 	@DeptList VARCHAR(8000), 
 	@ThresholdAmt INT,
 	@ActvTypeList VARCHAR(8000), 
  	@ExcludeActvTypeList VARCHAR(8000)
)
AS

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@maxDate INT,
	@openDate DATETIME,
	@bookdate INT
DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME

/*SET THE PARAMETER VALUES */
SET @day = ABS(@day)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN USR_DomToActive
	ELSE
		-- Already in a transaction
		SAVE TRAN USR_DomToActive
	/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @description = [Desc], @WLType = WLType  
FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

DECLARE @ActType TABLE (
	Type	INT
)

	
--Setting up the Parameter values
IF LTRIM(RTRIM(@ExcludeActvTypeList)) = '-NONE-' OR LTRIM(RTRIM(@ExcludeActvTypeList)) = ''
		SELECT @ExcludeActvTypeList = NULL
	ELSE
	SELECT @ExcludeActvTypeList = ',' + 
		REPLACE(LTRIM(RTRIM(@ExcludeActvTypeList)),' ','')+ ','

-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
SELECT @ActvTypeList = dbo.BSA_fnListParams(@ActvTypeList)
SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)




INSERT INTO @ActType
	SELECT 	Type  FROM vwRuleNonExmActType
	WHERE	(@ActvTypeList IS NULL 
		OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActvTypeList) > 0)	



DECLARE @tt TABLE (
	account VARCHAR(40),
	bookdate INT
)

	INSERT INTO @tt (account, bookdate)

	SELECT 
		ActivityHist.account,
		MAX(ActivityHist.bookdate) 
	FROM ActivityHist (NOLOCK)
	WHERE 
		ActivityHist.account IS NOT NULL
		AND ActivityHist.Account IN 
			(SELECT 
				Activity.Account 
			 FROM 
				Activity, 
				Account (NOLOCK) 
				
			WHERE Activity.account = account.[id] 
				AND  (ISNULL(@BranchList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(Account.OwnerBranch)) + ',',   @BranchList ) > 0)
				AND  (ISNULL(@DeptList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(Account.OwnerDept)) + ',',   @DeptList ) > 0)
				AND ((ISNULL(@ExcludeActvTypeList,'') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(Activity.Type)) + ',', @ExcludeActvTypeList) = 0))
				AND ((ISNULL(@ActvTypeList,'') = '' OR ---include activitytypelist
 					CHARINDEX(',' + LTRIM(RTRIM(Activity.Type)) + ',', @ActvTypeList) > 0))
			GROUP BY Activity.account 
			HAVING SUM(Activity.BaseAmt) >= @ThresholdAmt)
		AND ((ISNULL(@ExcludeActvTypeList,'') = '' OR
			CHARINDEX(',' + LTRIM(RTRIM(ActivityHist.Type)) + ',', @ExcludeActvTypeList) = 0))
	GROUP BY ActivityHist.account  

IF EXISTS(
		SELECT distinct A.account Account
		FROM dbo.Activity A (NOLOCK)
		INNER JOIN @tt t
		ON A.account = t.account 
		WHERE t.Bookdate < 
			dbo.ConvertSqlDateToInt(DATEADD(d, -@day, dbo.BSA_ConvertintToSqlDate(A.BookDate, 'mm/dd/yyyy')))
			AND ((ISNULL(@ActvTypeList,'') = '' OR ---include activitytypelist
 					CHARINDEX(',' + LTRIM(RTRIM(A.Type)) + ',', @ActvTypeList) > 0))
		GROUP BY A.Account
	)
	BEGIN
		IF @testAlert = 1 
			BEGIN	
		 	   SELECT @STARTALRTDATE = GETDATE()
		 	   INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
		 			LASTMODIFY, CUST, ACCOUNT, IsTest) 
		 	   SELECT @WLCode, 'Account:  ''' + A.Account + ''' became active' +
		 		' after being dormant for more than ' + CONVERT(VARCHAR, @day) +
		 		' days.' , 0, 
		 		GETDATE(), NULL, NULL, (select top 1 cust from AccountOwner where account = A.Account), A.Account, 1
		   	   FROM dbo.Activity A (NOLOCK)
		 	   INNER JOIN @tt t
		 	   ON A.account = t.account 
		 	   WHERE t.Bookdate < 
		 		dbo.ConvertSqlDateToInt(DATEADD(d, -@day, CONVERT(VARCHAR, A.BookDate)))
				AND ((ISNULL(@ActvTypeList,'') = '' OR ---include activitytypelist
 					CHARINDEX(',' + LTRIM(RTRIM(A.Type)) + ',', @ActvTypeList) > 0))
		 	   GROUP BY A.Account
		   	
				SELECT @STAT = @@ERROR	
		   		SELECT @ENDALRTDATE = GETDATE()
		   		IF @STAT <> 0  GOTO ENDOFPROC
	
	
			  INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
 			  SELECT 'Alert', AlertNO, TRANNO 
			  FROM Activity a, @TT t, Alert, @ActType act
			  where
			  a.account = t.account AND a.Type = act.Type and 
			  t.Bookdate < dbo.ConvertSqlDateToInt(DATEADD(d, -@day, CONVERT(VARCHAR, A.BookDate)))
			  and a.Account = Alert.Account AND
			  Alert.WLCode = @WLCode 
			  AND ((ISNULL(@ExcludeActvTypeList,'') = '' OR
			  CHARINDEX(',' + LTRIM(RTRIM(a.type)) + ',', @ExcludeActvTypeList) = 0))
			  AND ((ISNULL(@ActvTypeList,'') = '' OR ---include activitytypelist
			  CHARINDEX(',' + LTRIM(RTRIM(a.type)) + ',', @ActvTypeList) > 0)) AND
			  Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE

	    	          SELECT @STAT = @@ERROR
 
			END 
		ELSE 
		BEGIN
			IF @WLTYPE = 0 
				BEGIN
			           SELECT @STARTALRTDATE = GETDATE()
				   INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
					LASTMODIFY, CUST, ACCOUNT) 
				   SELECT @WLCode, 'Account:  ''' + A.Account + ''' became active' +
					' after being dormant for more than ' + CONVERT(VARCHAR, @day) +
					' days.' , 0, 
					GETDATE(), NULL, NULL, (select top 1 cust from AccountOwner where account = A.Account), A.Account
			  		FROM dbo.Activity A (NOLOCK)
					INNER JOIN @tt t
					ON A.account = t.account 
					WHERE t.Bookdate < 
					dbo.ConvertSqlDateToInt(DATEADD(d, -@day, CONVERT(VARCHAR, A.BookDate)))
					AND ((ISNULL(@ActvTypeList,'') = '' OR ---include activitytypelist
 					CHARINDEX(',' + LTRIM(RTRIM(A.Type)) + ',', @ActvTypeList) > 0))
					GROUP BY A.Account

				   SELECT @STAT = @@ERROR	
				   SELECT @ENDALRTDATE = GETDATE()
				   IF @STAT <> 0  GOTO ENDOFPROC
				END 

			ELSE IF @WLTYPE = 1 
				BEGIN
					SELECT @STARTALRTDATE = GETDATE()
				   	INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
						ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
						RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
						ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
						EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
						OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
						REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
						WLCode, WLDESC, CREATETIME )
				    	SELECT NULL, convert(varchar, getdate(), 112), (select top 1 cust from AccountOwner where account = A.Account), A.Account,
						NULL, 'RULE', NULL, NULL, NULL, NULL,
						NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
						NULL, NULL, 0, 0, 0, 0, 
						NULL, NULL, NULL, NULL, 0, NULL, NULL,
						@WLCode,  'Account:  ''' + A.Account + ''' became active' +
						' after being dormant for more than ' + CONVERT(VARCHAR, @day) +
						' days.' , GETDATE() 
				   	FROM dbo.Activity A (NOLOCK)
					INNER JOIN @tt t
					ON A.account = t.account 
					WHERE t.Bookdate < 
					dbo.ConvertSqlDateToInt(DATEADD(d, -@day, CONVERT(VARCHAR, A.BookDate)))
					AND ((ISNULL(@ActvTypeList,'') = '' OR ---include activitytypelist
 					CHARINDEX(',' + LTRIM(RTRIM(A.Type)) + ',', @ActvTypeList) > 0))
					GROUP BY A.Account
				    
					SELECT @STAT = @@ERROR	
				    	SELECT @ENDALRTDATE = GETDATE()
				    	IF @STAT <> 0  GOTO ENDOFPROC
				END

		IF @WLTYPE = 0 
			BEGIN
				INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				   SELECT 'Alert', AlertNO, TRANNO 
					FROM Activity a, @TT t, Alert, @ActType act where
						a.account = t.account AND a.Type = act.Type and 
						t.Bookdate < dbo.ConvertSqlDateToInt(DATEADD(d, -@day, CONVERT(VARCHAR, A.BookDate)))
						and a.Account = Alert.Account AND ((ISNULL(@ExcludeActvTypeList,'') = '' OR
						CHARINDEX(',' + LTRIM(RTRIM(a.type)) + ',', @ExcludeActvTypeList) = 0))
						AND ((ISNULL(@ActvTypeList,'') = '' OR ---include activitytypelist
						CHARINDEX(',' + LTRIM(RTRIM(a.type)) + ',', @ActvTypeList) > 0)) AND
						Alert.WLCode = @WLCode AND
						Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE

				SELECT @STAT = @@ERROR 
			END 

		ELSE IF @WLTYPE = 1 
			BEGIN
				INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 'SUSPACT', RECNO, TRANNO 
				FROM Activity a, @TT t, SUSPICIOUSACTIVITY S, @ActType act 
				where a.account = t.account AND a.Type = act.Type AND ((ISNULL(@ExcludeActvTypeList,'') = '' OR
				      	CHARINDEX(',' + LTRIM(RTRIM(a.type)) + ',', @ExcludeActvTypeList) = 0))
				      	AND ((ISNULL(@ActvTypeList,'') = '' OR ---include activitytypelist
				      	CHARINDEX(',' + LTRIM(RTRIM(a.type)) + ',', @ActvTypeList) > 0)) AND
				      	t.Bookdate < dbo.ConvertSqlDateToInt(DATEADD(d, -@day, CONVERT(VARCHAR, A.BookDate)))
				      	AND ((ISNULL(@ActvTypeList,'') = '' OR ---include activitytypelist
 						CHARINDEX(',' + LTRIM(RTRIM(A.Type)) + ',', @ActvTypeList) > 0))
						and a.Account = S.Account AND
				      	S.WLCode = @WLCode AND
				      	S.CREATETIME BETWEEN @STARTALRTDATE AND @ENDALRTDATE
				
				SELECT @STAT = @@ERROR 
			END
	END
END
EndOfProc:
IF (@stat <> 0) 
	BEGIN 
  		ROLLBACK TRAN USR_DomToActive
  		RETURN @stat
	END	

IF @trnCnt = 0
  	COMMIT TRAN USR_DomToActive
RETURN @stat

GO


