USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_GetDateRange]    Script Date: 6/15/2021 10:24:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


ALTER Procedure [dbo].[BSA_GetDateRange]
(
	@CurrentDate datetime, @Period Varchar(25),
	@type varchar(4),
	@Cycles int,
	@SQLStartDate datetime output,
	@SQLEndDate datetime output
)
AS

/*Parameter Description
  	@CurrentDate = Date which has to be used as the base for calculation of period
	@Period = Indicates Week/Month/Quarter/Semi-Annual
	@type varchar = Indicates whether we need to go current(CURR) or previous (PREV)
	@Cycles = Indicates howmany cycles (Cycles should be always > 0)
	@SQLStartDate = Period start date
	@SQLEndDate = Period end date
*/
--JUN-11-2021 add for day
IF @Period = 'DAY' 
BEGIN
	--Start of the Day
	SELECT @SQLStartDate = DATEADD(day, -1 * @cycles, @currentDate)
	IF @Type = 'PREV'
		Begin		
			--Last date of previous Week
			SELECT @SQLEndDate = DATEADD(day, -1, @currentDate)
		End
		
END

IF @Period = 'WEEK' 
BEGIN
	IF @Type = 'CURR'
		Begin
			--Last date of Current Week
			SELECT @SQLEndDate = DATEADD(wk, DATEDIFF(wk, 5, @CurrentDate), 5)
			SET @cycles = @cycles -1
		End
	--Start of the Week
	SELECT @SQLStartDate = DATEADD(wk, DATEDIFF(wk, 6, @CurrentDate) - @cycles, 6)
	IF @Type = 'PREV'
		Begin		
			--Last date of previous Week
			SELECT @SQLEndDate = DATEADD(wk, DATEDIFF(wk, 5, @CurrentDate) - 1, 5)
		End
		
END

IF @Period = 'MONTH' 
BEGIN
	
	IF @Type = 'CURR'
		Begin
			--Last date of Current month
			Select @SQLEndDate = dateadd(m, 1, @CurrentDate) - day(dateadd(m, 1, @CurrentDate))
			SET @cycles = @cycles -1
		End
	--Start of Month
	SELECT @SQLStartDate = DATEADD(mm, DATEDIFF(mm, 0, @CurrentDate) - @Cycles, 0)

	IF @Type = 'PREV'
		Begin		
			--Last date of previous Month
			SELECT @CurrentDate = DateAdd(month, -1, @CurrentDate)
			SELECT @SQLEndDate = dateadd(m, 1, @CurrentDate) - day(dateadd(m, 1, @CurrentDate))
		End
	
END

IF @Period = 'QUARTER' 
BEGIN
	IF @Type = 'CURR'
	Begin
		--Last date of Current Quarter
		Select @SQLEndDate = DATEADD(qq, DATEDIFF(qq, -1, @CurrentDate), -1)
		SET @cycles = @cycles -1
	End
	--Start of Quarter
	SELECT @SQLStartDate = DATEADD(qq, DATEDIFF(qq, 0, @CurrentDate) - @Cycles, 0)
	IF @Type = 'PREV'		
	Begin
		--Last date of Previous Quarter	
		SELECT @SQLEndDate = DATEADD(qq, DATEDIFF(qq, -1, @CurrentDate)-1 ,-1)
	End
END 

IF @Period = 'SEMI-ANNUAL' 
BEGIN
	IF @Type = 'CURR'
	Begin
		--Last date of Current Semi-Annual
		Select @SQLEndDate = DATEADD(qq, DATEDIFF(qq, -1, @CurrentDate), -1)
		If((datepart(mm, @SQLEndDate) % 6) <> 0) 
		BEGIN
			SELECT @SQLEndDate = DATEADD(qq, 1, @SQLEndDate+1)-1
		END	
		SELECT @SQLStartDate = DATEADD(qq, -2*@Cycles, @SQLEndDate+1)
	End
	IF @Type = 'PREV'		
	Begin
		SELECT @SQLStartDate = DATEADD(yy, DATEDIFF(yy, 0, @CurrentDate), 0)
		If(datepart(mm, @CurrentDate) > 6) 
		BEGIN
			SELECT @SQLStartDate = DATEADD(qq, 2, @SQLStartDate)
		END
		SELECT @SQLStartDate = DATEADD(qq, -2*@Cycles, @SQLStartDate)
		SELECT @SQLEndDate = DATEADD(dd,-1,DATEADD(qq, 2*@Cycles, @SQLStartDate))
	End
		
END 

GO


