use pbsa
go 

delete watchlist where wlcode = 'F4100'

declare @par varchar(3000)

select @par = params from Watchlist where WLCode = 'L4200'

select @par = replace(@par,'<Param Name="@Tday" Alias="Number of days that termination before due date" Value="30"','<Param Name="@Tmon" Alias="Number of months that termination before due date" Value="1"')

update Watchlist set Params = @par where wlcode = 'L4200'

