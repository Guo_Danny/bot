USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_F4100]    Script Date: 1/29/2021 11:18:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER PROCEDURE [dbo].[USR_F4100] (@WLCode SCode, @testAlert INT, 
	@activityTypeList VARCHAR(8000),
	@riskClasses VARCHAR(8000),
	@CustTypeList VARCHAR(8000),
	@productTypes VARCHAR(8000),
	@minimumAmount MONEY, 
	@minimumCount INT, 
	@day INT, 
	@cashorNonCash INT,
    @receivePay SMALLINT,
	@Tday int)

AS

/* RULE AND PARAMETER DESCRIPTION
	Detects repetitive transactions of the specified activity types 
	exceeding a designated amount within a selected time frame.
	Designed to be run Pre-EOD.

	@activityType = valid activity types delimited by comma ex: 101,102
	@riskClasses = Comma separated list of risk classes ex: High,Medium
	@CustTypeList = Comma seperated list of cusotmer types 
	@productTypes = Comma separated list of account types
	@minimumAmount = minimum amount threshold
	@minimumCount = minimum count threshold
	@day = period threshold, measured in days
	@cashOrNonCashorAll = specify whether to consider cash transactions or 
			      non cash transactions or all transactions of the 
			      specified activity types
	@receivePay = specify whether its a pay or receieve transaction
*/

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@minDate INT,
	@CustomerExemptionsList VARCHAR(8000),
	@AccountExemptionsList VARCHAR(8000)
DECLARE @StartAlrtDate  DATETIME
DECLARE @EndAlrtDate    DATETIME


DECLARE	@TT TABLE (
	Cust varchar(40),
	BookDate INT,
	RecvPay SMALLINT,
	TranNo	INT
)

-- Temporary table of Activity Types that have not been specified as Exempt
DECLARE @ActType TABLE (
	Type	INT
)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_F4100 
ELSE
	-- Already in a transaction
	SAVE TRAN USR_F4100 
/* End standard stored procedure transaction header */

/*  standard Rules Header */
	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @RuleStartDate DATETIME
	
	SELECT @description = [Desc], @WLTYPE = WLTYPE, @RuleStartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList (NOLOCK)
	WHERE WLCode = @WlCode

Declare @BaseCurr char(3)
select @BaseCurr = IsNULL(BaseCurr,'') from SysParam

SET @minDate = dbo.ConvertSqlDateToInt(
	DATEADD(d, -1 * ABS(@day), CONVERT(VARCHAR, @RuleStartDate)))

SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)
SELECT @riskClasses = dbo.BSA_fnListParams(@riskClasses)
SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)
SELECT @productTypes = dbo.BSA_fnListParams(@productTypes)

-- Get the list of exemptions that are exempted on customer from rules
SELECT @CustomerExemptionsList = dbo.BSA_GetExemptionCodes('Customer','Rule')
IF ltrim(rtrim(@CustomerExemptionsList)) = ''
	SET @CustomerExemptionsList = NULL

-- Get the list of exemptions that are exempted on account from rules
SELECT @AccountExemptionsList = dbo.BSA_GetExemptionCodes('Account','Rule')
IF ltrim(rtrim(@AccountExemptionsList)) = ''
	SET @AccountExemptionsList = NULL


IF @cashorNonCash < 0 or @cashorNonCash >1
	SET @cashorNonCash = NULL

IF @receivePay < 1 or @receivePay >2
	SET @receivePay = NULL
	
INSERT INTO @ActType
	SELECT 	Type  FROM vwRuleNonExmActType
	WHERE	(@ActivityTypeList IS NULL 
		OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

INSERT INTO @TT(Cust, BookDate, RecvPay, TranNo)
SELECT a.Cust, a.BookDate, a.RecvPay, a.TranNo
	FROM CurrentArchActivity a (NOLOCK) INNER JOIN 
	Customer c ON a.cust = c.id  INNER JOIN 
	Account ac ON a.account = ac.id
	INNER JOIN @ActType act ON A.Type = act.Type
	WHERE ((@productTypes IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, ac.Type) + ',',@productTypes) > 0) )
	AND ((@riskClasses IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, c.RiskClass) + ',',@riskClasses) > 0) )
	AND ((@CustTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, c.Type) + ',',@CustTypeList) > 0) )
	AND	(c.Exemptionstatus IS NULL OR @CustomerExemptionsList IS NULL OR
		CHARINDEX(',' + LTRIM(RTRIM(c.Exemptionstatus)) + ',', @CustomerExemptionsList ) = 0)
	/*AND	(ac.Exemptionstatus IS NULL OR @AccountExemptionsList IS NULL OR
		CHARINDEX(',' + LTRIM(RTRIM(ac.Exemptionstatus)) + ',', @AccountExemptionsList ) = 0)
	JUL-02-2021 mark for replace account by cust	*/ 
	AND a.BaseAmt >= @minimumAmount
	AND a.recvPay = ISNULL(@receivePay,a.recvPay) AND a.bookdate >= @minDate
	AND a.CashTran = ISNULL(@cashorNonCash, a.CashTran)
	And isnull(a.InstrumentQty,0) != 0 AND DATEDIFF(DAY,convert(datetime, convert(char(8),a.BookDate)),convert(date,cast(cast(InstrumentQty as int) as varchar),112)) > isnull(@Tday,30) 
	--and ac.AcStatus = 'Close'  --JUN-30-2021 remove this statement
	AND a.Cust IS NOT NULL
	AND EXISTS (
		SELECT Cust From Activity b 
			INNER JOIN @ActType act ON b.Type = act.Type
			WHERE 
				b.recvpay = ISNULL(@receivePay,b.recvPay) AND
				b.CashTran = ISNULL(@cashorNonCash, b.CashTran) AND
				b.Cust = a.Cust AND
				b.BaseAmt>=@minimumAmount 	
				AND b.bookdate >= @minDate			
			GROUP BY Cust			
			HAVING Count(b.TranNo) >= @minimumCount
			)
	GROUP BY a.CUst, a.BookDate, a.RecvPay, a.TranNo


IF @testAlert = 1 BEGIN
	SELECT @StartAlrtDate = GETDATE()
	INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT, IsTest) 
	SELECT @WLCODE, 'Customer:  ''' + T.Cust + ''' has ''' + convert(varchar,count(T.TranNo)) + ''' transactions totaling amount is ' + 
		convert(varchar,sum(act.BaseAmt)) + ' in ' + CONVERT(VARCHAR, @day) + ' days. ' , 0, 
		GETDATE(), NULL, NULL, T.Cust, (select top 1 Account from AccountOwner where AccountOwner.Cust = T.Cust), 1
	FROM @TT T
	join CurrentArchActivity act on T.TranNo = act.TranNo
	GROUP BY T.Cust 

	SELECT @STAT = @@ERROR	
	SELECT @EndAlrtDate = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'ALERT', ALERTNO, TRANNO 
		FROM @TT t, Alert 
		WHERE
		t.Cust = Alert.Cust AND
		Alert.WLCode = @WLCODE AND
		Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
	
	SELECT @STAT = @@ERROR 
	IF @STAT <> 0  GOTO ENDOFPROC
END ELSE BEGIN
	IF @WLTYPE = 0 BEGIN
	SELECT @StartAlrtDate = GETDATE()
	INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT) 
	SELECT @WLCODE, 'Customer:  ''' + T.Cust + ''' has ''' + convert(varchar,count(T.TranNo)) + ''' transactions totaling amount is ' + 
		convert(varchar,sum(act.BaseAmt)) + ' in ' + CONVERT(VARCHAR, @day) + ' days. ' , 0, 
		GETDATE(), NULL, NULL, T.Cust, (select top 1 Account from AccountOwner where AccountOwner.Cust = T.Cust) 
	FROM @TT T 
	join CurrentArchActivity act on T.TranNo = act.TranNo
	GROUP BY T.Cust 

	SELECT @STAT = @@ERROR	
	SELECT @EndAlrtDate = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC
	END ELSE IF @WLTYPE = 1 BEGIN
		SELECT @StartAlrtDate = GETDATE()
		INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			WLCODE, WLDESC, CREATETIME )
		SELECT	NULL, MIN(T.BookDate) , T.Cust, (select top 1 Account from AccountOwner where AccountOwner.Cust = T.Cust),
			NULL, 'RULE', NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, 0, 0, 0, 0, 
			NULL, NULL, NULL, NULL, 0, NULL, NULL,
			@WLCODE, 'Customer:  ''' + T.Cust + ''' has ''' + convert(varchar,count(T.TranNo)) + ''' transactions totaling amount is ' + 
		convert(varchar,sum(act.BaseAmt)) +' in ' + CONVERT(VARCHAR, @day) + ' days. ' , GETDATE() 
	FROM @TT T
	join CurrentArchActivity act on T.TranNo = act.TranNo
	GROUP BY T.Cust

	SELECT @STAT = @@ERROR	
	SELECT @EndAlrtDate = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC
	END
	
	IF @WLTYPE = 0 BEGIN
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'ALERT', ALERTNO, TRANNO 
		FROM @TT t, Alert 
		WHERE
		t.Cust = Alert.Cust AND
		Alert.WLCode = @WLCODE AND
		Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
	
	SELECT @STAT = @@ERROR 
	END ELSE IF @WLTYPE = 1 BEGIN
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'SUSPACT', RECNO, TRANNO 
		FROM @TT t, SUSPICIOUSACTIVITY S 
		WHERE
		t.Cust = S.Cust AND 
		S.WLCODE = @WLCODE AND
		S.CREATETIME BETWEEN @StartAlrtDate AND @EndAlrtDate
	SELECT @STAT = @@ERROR 
	END
END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_F4100
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_F4100
RETURN @stat
GO


