USE [OFAC]
GO

/****** Object:  StoredProcedure [dbo].[OFS_AddFilterMsg]    Script Date: 7/8/2021 4:49:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[OFS_AddFilterMsg] @seqNumb int, @msgType GenericIdType, @msgLen int,
            @msg text
  As
  declare @stat int

  -- Not using begin / commit tran since this routine is used with other
  -- table updates in ofacsrvr.cpp
  insert into MsgTable
      (SeqNumb, MsgType, MsgLen, Msg)
    values
      ( @seqNumb, @msgType, @msgLen, @msg )
  
  declare @server varchar(40)
  set @server = ''
  select @server = [server] from SystemParams where [server] = 'OSBAMLTKAPV'
  --JUL-08-2021 check for certain server
  if isnull(@server,'') != ''
  begin
	  declare @UserMessageReference varchar(200),@pos_s int, @pos_e int, @str_tag varchar(200), @amt varchar(20), @str_curr varchar(20)

	  select @UserMessageReference = Ref from FilterTranTable where SeqNumb = @seqNumb
  
	  select @str_tag='', @str_curr='', @amt='0.00'
	  
	  if @UserMessageReference is not null and (RTRIM(SUBSTRING(@UserMessageReference,15,4)) in ('103','202','202C'))
	  begin
		--JUL-08-2021 fix UserMessageReference
		declare @sTmp VARCHAR(40), @sTag20 varchar(40)
		select @sTmp=SUBSTRING(@Msg,CHARINDEX(':20:',@Msg)+4,35)
		if len(isnull(@sTmp,'')) > 1
		begin
			select @sTag20=SUBSTRING(@sTmp,1,CHARINDEX(':',@sTmp)-1)
			--select @sTag20 as Tag20
			if len(isnull(@sTag20,'')) > 1
			begin
				update FilterTranTable set UserMessageReference = @sTag20 where SeqNumb = @seqNumb
			end else 
			begin
				update FilterTranTable set UserMessageReference = '' where SeqNumb = @seqNumb
			end
		end
		if CHARINDEX(':32A:',@msg) > 0
		begin
			select @str_curr = RTRIM(SUBSTRING(@Msg,CHARINDEX(':32A:', @Msg) + 11 , 3))
			select @amt = replace(replace(replace(replace(SUBSTRING(substring(@Msg,CHARINDEX(':32A:', @Msg) + 14, 30),1,CHARINDEX(':',substring(@Msg,CHARINDEX(':32A:', @Msg) + 14, 30))),':',''),',','.'), char(10),''),char(13),'')

			goto ChkCurrency
		end
		else if CHARINDEX(':33B:',@msg) > 0 begin

			select @str_curr = RTRIM(SUBSTRING(@Msg,CHARINDEX(':33B:', @Msg) + 5 , 3))
			select @amt = replace(replace(replace(replace(SUBSTRING(substring(@Msg,CHARINDEX(':33B:', @Msg) + 8, 30),1,CHARINDEX(':',substring(@Msg,CHARINDEX(':33B:', @Msg) + 8, 30))),':',''),',','.'), char(10),''),char(13),'')

			goto ChkCurrency
		end
	  end

	  --20201117 add for check currency, if not 'A'-'Z' then set default value 'USD'
	  ChkCurrency:

	  if len(@str_curr) = 0 or len(@str_curr) < 3 or len(@str_curr) > 3
		 select @str_curr='USD'
  
	  select @str_curr=upper(@str_curr)

	  if ISNUMERIC (@amt) = 0
		 select @amt='0.00'

	  if substring(@str_curr,1,1)>='A' and substring(@str_curr,1,1)<='Z' and substring(@str_curr,2,1)>='A' and substring(@str_curr,2,1)<='Z' and substring(@str_curr,3,1)>='A' and substring(@str_curr,3,1)<='Z'
		 --select @str_curr
		 update FilterTranTable set Currency = @str_curr, Amount = @amt where SeqNumb = @seqNumb;
	  else 
		 --select 'USD'
		 update FilterTranTable set Currency = 'USD', Amount = @amt where SeqNumb = @seqNumb;
  end

  EndProc:
  --20201117 add for check currency, if not 'A'-'Z' then set default value 'USD'
  
  select @stat = @@ERROR
  return @stat



GO


