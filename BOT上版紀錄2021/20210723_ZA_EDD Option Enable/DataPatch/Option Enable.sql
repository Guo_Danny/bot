USE [PBSA]
GO

select * from OptionTbl
where code in ('GenCustChg', 'EDDRISK', 'KYCC', 'OnBoardCust','AutoGenRisk')

--AutoGenRisk(BSA EOD時自動Generate Customer Risk) 功能目前南非確認不用，先備註，若有其他分行使用再則打開
update OptionTbl set Enabled = 1 where code in (
'GenCustChg', 'EDDRISK', 'KYCC', 'OnBoardCust'
--,'AutoGenRisk'
)
