use pbsa
go

select * from Customer c
join RiskScoreHist rsh on c.Id = rsh.CustomerId
where c.KYCStatus = 2
and convert(varchar(8), c.ReviewDate, 112) = c.ProbationStartDate 
and isnull(rsh.DateAccepted,'') = ''
and substring(convert(varchar(20),rsh.CreateDate, 120),12,2) = '10'

begin tran
delete RiskScoreHist from Customer c
join RiskScoreHist rsh on c.Id = rsh.CustomerId
where c.KYCStatus = 2
and convert(varchar(8), c.ReviewDate, 112) = c.ProbationStartDate 
and isnull(rsh.DateAccepted,'') = ''
and substring(convert(varchar(20),rsh.CreateDate, 120),12,2) = '10'

commit
