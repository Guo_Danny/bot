use OFAC
go

alter table SwiftTag drop column [FieldName]
alter table SwiftTag drop column [Format]
alter table SwiftTag drop column [Remark]
alter table SwiftTag drop column [Condition]
