USE [OFAC]
GO

/****** Object:  UserDefinedFunction [dbo].[OFS_fnGetMatchText]    Script Date: 11/11/2021 4:13:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


--OFAC CASE with OFAC Web Service , GET Match Text VALUE
ALTER FUNCTION [dbo].[OFS_fnGetMatchText](  @sSrhName VARCHAR(8000), @sMatchName VARCHAR(1000) )
RETURNS VARCHAR(200)
  As
BEGIN

		declare @testsrings varchar(1000),@sStr varchar(1000),@sStr1 varchar(1000)
		declare @sStrTmp varchar(1000), @sStrTmp1 varchar(1000), @sStrTmp2 varchar(1000)
		--declare @sSrhName varchar(1000),@sMatchName varchar(1000)
		declare @iDeff int, @iPos int, @iLen1 int , @iLen2 int, @iCnt int

		--select @sSrhName='REAL & ESTATE INVESTMENT FUND'
		--select @sMatchName='CHINA REAL ESTATE RESEARCH ASSOCIATION'

        --SELECT @sSrhName=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@sSrhName,',',' '),'-', ' '),'''',''),'.',' '),'/',' '),')',' '),'(' ,' '),'&',' '),'   ',' '),'  ',' ')
        SELECT @sSrhName=UPPER(replace(@sSrhName,'-',' ')) + ' '
		--SELECT @sMatchName=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@sMatchName,',',' '),'-', ' '),'''',''),'.',' '),'/',' '),')',' '),'(' ,' '),'&',' '),'   ',' '),'  ',' ')
		select @sMatchName=UPPER(@sMatchName) + ' '
		
		--0. compare Search name , Match Name 
		select @sStrTmp='', @sStrTmp1=''		
		 
		--2021-09-29 bug fix, start
		--by match name, from the first word, the second word, the third word,...
		select @sStrTmp=@sMatchName, @iPos=0
		
		--2021-11-11 if match matched text return,start
		if (CHARINDEX(@smatchname, @ssrhname)>0 )
		begin
		   select @sStrTmp1=@sMatchName
		   goto endProc
		end
		--2021-11-11 if match matched text return,end

		while (@iPos < len(@sStrTmp) )
		begin
		  IF ( (CHARINDEX(' ',@sStrTmp) > 0 AND  PATINDEX('%[,]%',@sStrTmp) = 0 ) or  
		       (PATINDEX('%[,]%',@sStrTmp) > 0 and CHARINDEX(' ',@sStrTmp) <  PATINDEX('%[,]%',@sStrTmp) ) 
			 ) 
		  BEGIN
		     select @sStrTmp1=SUBSTRING(@sStrTmp,@iPos+1,CHARINDEX(' ',@sStrTmp)-1) 
			 select @sStrTmp2='%' + @sStrTmp1 +'%'
		     select @iDeff=PATINDEX(@sStrTmp2,@sSrhName)
			
			 if (@iDeff > 0)
		       select @iPos=Len(@sStrTmp)+1
             else
		       select @iPos=CHARINDEX(' ',@sStrTmp,@iPos+len(@sStrTmp1)-2)  
		  END

		 ELSE
		  IF ( CHARINDEX(' ',@sStrTmp) >  PATINDEX('%[,]%',@sStrTmp)  )
		  BEGIN
		     select @sStrTmp1=SUBSTRING(@sStrTmp,@iPos+1,CHARINDEX(' ',@sStrTmp)-1) 
			 select @sStrTmp2='%' + @sStrTmp1 +'%'
		     select @iDeff=PATINDEX(@sStrTmp2,@sSrhName)
			
			 if (@iDeff > 0)
		       select @iPos=Len(@sStrTmp)+1
             else
		       select @iPos=CHARINDEX(' ',@sStrTmp,@iPos+len(@sStrTmp1)-2)  
		  END
		  
		  IF ( @iDeff=0  and PATINDEX('%[,]%',@sStrTmp)  > 0 )
			 BEGIN
			    select @sStrTmp1=SUBSTRING(@sStrTmp,@iPos+1,PATINDEX('%[,]%',@sStrTmp))  --2021-09-29 BUG FIX
			    select @sStrTmp2='%' + @sStrTmp1 +'%'
		        select @iDeff=PATINDEX(@sStrTmp2,@sSrhName)			
				
			    --SELECT @sstr as TMP,@SSTRTMP1 AS TMP1, @sStrTmp2 AS TMP2, @sSrhname as SearchText ,@iDeff AS DIFF

			    if (@iDeff > 0)
		           select @iPos=Len(@sStrTmp)+1
                else
				   select @iPos=@iPos+len(@sStrTmp1)
             END

        end
		--2021-09-29 bug fix, end

		-- if partial match
		if ( @iDeff > 0 )
		begin
		   select @icnt=0, @iLen1=1, @iLen2=1, @sStr=@sMatchName, @sStrTmp='', @sStrTmp1=''
		   while( @iLen2 !=0 )
		   begin
			   
			  select @iLen2=CHARINDEX(' ', @sStr,@ilen1)  
			  if ( @ilen2=len(@sstr) )
			  begin
				  select @testsrings=substring(@sStr,@ilen1,@ilen2)
				  select @sStr=REPLACE(@sStr,@testsrings,'')
				  --select @ilen2 as i2, @testsrings as Tstr,@sStr as MatchName
			  end

			  else	  
			  begin			     
			      select @iLen2=PATINDEX('%[ ,]%', @sStr) 
				  select @testsrings=substring(@sStr,@ilen1,@ilen2)
				  ----2021-11-11 bugfix when the duplicate word in MatchName & Process last word in MatchName , start
				  --select @sStr=ltrim(REPLACE(@sStr,@testsrings,''))  --bug must be mask and change
				  if ( @iLen2 > 0 and len(@sStr) > 0 and @testsrings <> '' ) --last word of matchname
				     select @sStr=SUBSTRING(@sStr,len(@testsrings)+2,len(@sStr)-len(@testsrings)-1)  
				  else 
				      select @testsrings=@sStr
                  ----2021-11-11 bugfix when the duplicate word in MatchName & Process last word in MatchName , end
				  --select @ilen2 as i2, @testsrings as Tstr,@sStr as MatchName
			  end

			  -- 1. Search Singal word in Search name
			  --if @teststring have one special char ,- , and into @sSrhname did not find, then delete space
              if (  PATINDEX('%[,]%', @testsrings) > 0 and PATINDEX('%[ ]%', @testsrings) > 0 and CHARINDEX(@testsrings, @sSrhName)= 0 )
			     select @testsrings=REPLACE(@testsrings,' ','')

			  select @iPos=CHARINDEX(@testsrings, @sSrhName)
			  
			 --select @iPos as Pos, @testsrings as Str, @sSrhName as SearchName

			  if ( @iPos > 0 )
			  begin      
				  select @sStrTmp = RTRIM(@testsrings)
                  
				  if (@sStrTmp1 = '' )   -- first word
					 select @sStrTmp1=RTRIM(@testsrings)

				  else if (@sStrTmp1 <> '' )   -- not first word
				  begin	     
					 --2.  Search two  words in Search name
					 if ( CHARINDEX(@sStrTmp1 + ' ', @sSrhName) > 0 ) --if srhname after @sStrTmp1  have space
					    select @sStr1 =  RTRIM(@sStrTmp1) + ' ' + RTRIM(@testsrings)
                     else --if srhname after @sStrTmp1 no space
					    select @sStr1 =  RTRIM(@sStrTmp1) +  RTRIM(@testsrings)

					 select @iPos = CHARINDEX(@sStr1, @sSrhName)
					 --select @iPos pos, @sStr1 as sStr , @sSrhName as SearchName

					 if @ipos > 0
					 begin
					   if ( CHARINDEX(@sStrTmp1 + ' ', @sSrhName) > 0 ) --if srhname after @sStrTmp1  have space
						 select @sStrTmp1 = RTRIM(@sStrTmp1) + ' ' + RTRIM(@sStrTmp)
                       else --if srhname after @sStrTmp1 no space
					      select @sStrTmp1 = RTRIM(@sStrTmp1)  + RTRIM(@sStrTmp)
                     end

				  end

				  --select @sStrTmp1 as StrTmp1
			  end
		   end
		end


		if Len(RTRIM(@sStrTmp1)) = 0 
		   select @sStrTmp1=SUBSTRING(@sSrhName,1,350)

		else if Len(RTRIM(@sStrTmp1)) >= 350
		   select @sStrTmp1=SUBSTRING(@sStrTmp1,1,350)

EndProc:
        --select @sStrTmp1

		RETURN @sStrTmp1

END
GO


