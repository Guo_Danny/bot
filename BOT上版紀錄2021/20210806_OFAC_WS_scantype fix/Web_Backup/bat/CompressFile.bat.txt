@echo on 
set SetDate=%date:~10,4%%date:~4,2%%date:~7,2%

for /F "tokens=1-4 delims=/ " %%a IN ("%date%") DO (
set _week=%%a
set _month=%%b
set _day=%%c
set _year=%%d
set _today=%%a%%b%%c
)

if not exist "D:\Web_Backup\%SetDate%\AML Interface\Backup\CIF" mkdir "D:\Web_Backup\%SetDate%\AML Interface\Backup\CIF"
if not exist "D:\Web_Backup\%SetDate%\AML Interface\Backup\Result" mkdir "D:\Web_Backup\%SetDate%\AML Interface\Backup\Result"
if not exist "D:\Web_Backup\%SetDate%\AML Interface\Backup\Swift" mkdir "D:\Web_Backup\%SetDate%\AML Interface\Backup\Swift"
if not exist "D:\Web_Backup\%SetDate%\AML Interface\log" mkdir "D:\Web_Backup\%SetDate%\AML Interface\log"
if not exist "D:\Web_Backup\%SetDate%\AMLImport\Backup\CIF" mkdir "D:\Web_Backup\%SetDate%\AMLImport\Backup\CIF"
if not exist "D:\Web_Backup\%SetDate%\AMLImport\Backup\CIF0084" mkdir "D:\Web_Backup\%SetDate%\AMLImport\Backup\CIF0084"
if not exist "D:\Web_Backup\%SetDate%\AMLImport\Backup\CIF0094" mkdir "D:\Web_Backup\%SetDate%\AMLImport\Backup\CIF0094"
if not exist "D:\Web_Backup\%SetDate%\AMLImport\Backup\SancParty" mkdir "D:\Web_Backup\%SetDate%\AMLImport\Backup\SancParty"
if not exist "D:\Web_Backup\%SetDate%\AMLImport\Log" mkdir "D:\Web_Backup\%SetDate%\AMLImport\Log"
if not exist "D:\Web_Backup\%SetDate%\BSA Interface\Account" mkdir "D:\Web_Backup\%SetDate%\BSA Interface\Account"
if not exist "D:\Web_Backup\%SetDate%\BSA Interface\Activity" mkdir "D:\Web_Backup\%SetDate%\BSA Interface\Activity"
if not exist "D:\Web_Backup\%SetDate%\BSA Interface\Customer" mkdir "D:\Web_Backup\%SetDate%\BSA Interface\Customer"
if not exist "D:\Web_Backup\%SetDate%\HostGW\WinSwfAml\Backup" mkdir "D:\Web_Backup\%SetDate%\HostGW\WinSwfAml\Backup"
if not exist "D:\Web_Backup\%SetDate%\Prime\BOTOSB\0084_SWIFT\Confirm" mkdir "D:\Web_Backup\%SetDate%\Prime\BOTOSB\0084_SWIFT\Confirm"
if not exist "D:\Web_Backup\%SetDate%\Prime\BOTOSB\0084_SWIFT\OK" mkdir "D:\Web_Backup\%SetDate%\Prime\BOTOSB\0084_SWIFT\OK"
if not exist "D:\Web_Backup\%SetDate%\Prime\BOTOSB\0094_SWIFT\Confirm" mkdir "D:\Web_Backup\%SetDate%\Prime\BOTOSB\0094_SWIFT\Confirm"
if not exist "D:\Web_Backup\%SetDate%\Prime\BOTOSB\0094_SWIFT\OK" mkdir "D:\Web_Backup\%SetDate%\Prime\BOTOSB\0094_SWIFT\OK"
if not exist "D:\Web_Backup\%SetDate%\Prime\BOTOSB\Backup\PBSA" mkdir "D:\Web_Backup\%SetDate%\Prime\BOTOSB\Backup\PBSA"

if not exist "D:\Web_Backup\%SetDate%\Prime\BOTOSB\Logs\PBSA" mkdir "D:\Web_Backup\%SetDate%\Prime\BOTOSB\Logs\PBSA"
if not exist "D:\Web_Backup\%SetDate%\ftproot\ReconReport" mkdir "D:\Web_Backup\%SetDate%\ftproot\ReconReport"

echo========================================>> D:\Web_Backup\log\%SetDate%.log
date /t >> D:\Web_Backup\log\%SetDate%.log
time /t >> D:\Web_Backup\log\%SetDate%.log
echo "move" >> D:\Web_Backup\log\%SetDate%.log
echo "move file to D:\Web_Backup\%SetDate%\AML Interface\Backup\CIF" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\AML Interface\Backup\CIF\*.IN" (move "D:\AML Interface\Backup\CIF\*.IN" "D:\Web_Backup\%SetDate%\AML Interface\Backup\CIF")
echo "move file to D:\Web_Backup\%SetDate%\AML Interface\Backup\Result" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\AML Interface\Backup\Result\*.*"  (move "D:\AML Interface\Backup\Result\*.*" "D:\Web_Backup\%SetDate%\AML Interface\Backup\Result")	
echo "move file to D:\Web_Backup\%SetDate%\AML Interface\Backup\Swift" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\AML Interface\Backup\Swift\*.*"  (move "D:\AML Interface\Backup\Swift\*.*" "D:\Web_Backup\%SetDate%\AML Interface\Backup\Swift")	
echo "move file to D:\Web_Backup\%SetDate%\AML Interface\log" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\AML Interface\log\*.0" (move "D:\AML Interface\log\*.0" "D:\Web_Backup\%SetDate%\AML Interface\log")
echo "move file to D:\Web_Backup\%SetDate%\AMLImport\Backup\CIF" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\AMLImport\Backup\CIF\*.*" (move "D:\AMLImport\Backup\CIF\*.*" "D:\Web_Backup\%SetDate%\AMLImport\Backup\CIF")
echo "move file to D:\Web_Backup\%SetDate%\AMLImport\Backup\CIF0084" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\AMLImport\Backup\CIF0084\*.*" (move "D:\AMLImport\Backup\CIF0084\*.*" "D:\Web_Backup\%SetDate%\AMLImport\Backup\CIF0084")
echo "move file to D:\Web_Backup\%SetDate%\AMLImport\Backup\CIF0094" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\AMLImport\Backup\CIF0094\*.*" (move "D:\AMLImport\Backup\CIF0094\*.*" "D:\Web_Backup\%SetDate%\AMLImport\Backup\CIF0094")
echo "move file to D:\Web_Backup\%SetDate%\AMLImport\Backup\SancParty" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\AMLImport\Backup\SancParty\*.*" (move "D:\AMLImport\Backup\SancParty\*.*" "D:\Web_Backup\%SetDate%\AMLImport\Backup\SancParty")
echo "move file to D:\Web_Backup\%SetDate%\AMLImport\Log" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\AMLImport\Log\*.0" (move "D:\AMLImport\Log\*.0" "D:\Web_Backup\%SetDate%\AMLImport\Log")
echo "move file to D:\Web_Backup\%SetDate%\BSA Interface\Account" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\BSA Interface\Account\*.*" (move "D:\BSA Interface\Account\*.*" "D:\Web_Backup\%SetDate%\BSA Interface\Account")
echo "move file to D:\Web_Backup\%SetDate%\BSA Interface\Activity" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\BSA Interface\Activity\*.*" (move "D:\BSA Interface\Activity\*.*" "D:\Web_Backup\%SetDate%\BSA Interface\Activity")
echo "move file to D:\Web_Backup\%SetDate%\BSA Interface\Customer" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\BSA Interface\Customer\*.*" (move "D:\BSA Interface\Customer\*.*" "D:\Web_Backup\%SetDate%\BSA Interface\Customer")
echo "move file to D:\Web_Backup\%SetDate%\HostGW\WinSwfAml\Backup" >> D:\Web_Backup\log\%SetDate%.log
if %_day% leq 7 if exist "D:\HostGW\WinSwfAml\Winswfaml*.log" (move "D:\HostGW\WinSwfAml\Winswfaml*.log" "D:\Web_Backup\%SetDate%\HostGW\WinSwfAml")
if %_day% leq 7 if exist "D:\HostGW\WinSwfAml\Winswfaml*.RCV" (move "D:\HostGW\WinSwfAml\Winswfaml*.RCV" "D:\Web_Backup\%SetDate%\HostGW\WinSwfAml")
if %_day% leq 7 if exist "D:\HostGW\WinSwfAml\Winswfaml*.SND" (move "D:\HostGW\WinSwfAml\Winswfaml*.SND" "D:\Web_Backup\%SetDate%\HostGW\WinSwfAml")

echo "move file to D:\Web_Backup\%SetDate%\Prime\BOTOSB\0084_SWIFT\Confirm" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\Prime\BOTOSB\0084_SWIFT\Confirm\*.*" (move "D:\Prime\BOTOSB\0084_SWIFT\Confirm\*.*" "D:\Web_Backup\%SetDate%\Prime\BOTOSB\0084_SWIFT\Confirm")
echo "move file to D:\Web_Backup\%SetDate%\Prime\BOTOSB\0084_SWIFT\OK" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\Prime\BOTOSB\0084_SWIFT\OK\*.*" (move "D:\Prime\BOTOSB\0084_SWIFT\OK\*.*" "D:\Web_Backup\%SetDate%\Prime\BOTOSB\0084_SWIFT\OK")
echo "move file to D:\Web_Backup\%SetDate%\Prime\BOTOSB\0094_SWIFT\Confirm" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\Prime\BOTOSB\0094_SWIFT\Confirm\*.*" (move "D:\Prime\BOTOSB\0094_SWIFT\Confirm\*.*" "D:\Web_Backup\%SetDate%\Prime\BOTOSB\0094_SWIFT\Confirm")
echo "move file to D:\Web_Backup\%SetDate%\Prime\BOTOSB\0094_SWIFT\OK" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\Prime\BOTOSB\0094_SWIFT\OK\*.*" (move "D:\Prime\BOTOSB\0094_SWIFT\OK\*.*" "D:\Web_Backup\%SetDate%\Prime\BOTOSB\0094_SWIFT\OK")
echo "move file to D:\Web_Backup\%SetDate%\Prime\BOTOSB\Backup\PBSA" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\Prime\BOTOSB\Backup\PBSA\*.*" (move "D:\Prime\BOTOSB\Backup\PBSA\*.*" "D:\Web_Backup\%SetDate%\Prime\BOTOSB\Backup\PBSA")

if exist "D:\Prime\BOTOSB\Logs\PBSA\*.*" (move "D:\Prime\BOTOSB\Logs\PBSA\*.*" "D:\Web_Backup\%SetDate%\Prime\BOTOSB\Logs\PBSA")
if exist "D:\ftproot\ReconReport\ZXDiffs.txt" (move "D:\ftproot\ReconReport\ZXDiffs.txt" "D:\Web_Backup\%SetDate%\ftproot\ReconReport")


time /t >> D:\Web_Backup\log\%SetDate%.log
echo "compress" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\Web_Backup\%SetDate%\*.*" (7z a "D:\Web_Backup\CompressedFile\%SetDate%.zip" "D:\Web_Backup\%SetDate%\" >> D:\Web_Backup\log\%SetDate%.log)
time /t >> D:\Web_Backup\log\%SetDate%.log
echo "delete" >> D:\Web_Backup\log\%SetDate%.log
if exist "D:\Web_Backup\CompressedFile\%SetDate%.zip" (RD /s /q D:\Web_Backup\%SetDate%\ >> D:\Web_Backup\log\%SetDate%.log) 
time /t >> D:\Web_Backup\log\%SetDate%.log
echo "finish" >> D:\Web_Backup\log\%SetDate%.log

@echo off

DEL \\TOSBAMLDBV4\Prime\BOTOSB\DAT\OFAC\SDNDET.BAK.*

REM 2019.07 data clean process, delete data 7 years ago.
SET DTNumber=-2557

echo %DTNumber%

FORFILES /p D:\Prime\BOTOSB\Logs\ /d %DTNumber% /c "cmd /c RD /S /Q @path"
FORFILES /p D:\Web_Backup\CompressedFile\ /d %DTNumber% /c "cmd /c del /Q @path"
FORFILES /p D:\ftproot\ReconReport\Backup\ /d %DTNumber% /c "cmd /c del /Q @path"

SQLCMD  -S TOSBAMLDBV4 -U primeuser -P Passw0rd -i"D:\Web_Backup\sql\DataBaseClean.sql" -o"D:\Web_Backup\log\DataBaseClean.log"
