
--NEWCUST
update watchlist set params = '<Params><Param Name="@RiskClassList" Alias="Risk Class" Value="-ALL-" DataType="String" /><Param Name="@InitialTimeFrame" Alias="Initial Period (days)" Value="90" DataType="Int" /><Param Name="@AvgPeriod" Alias="Average Period (days)" Value="7" DataType="Int" /><Param Name="@GenerateAlertsBeforeInitialTimeFrame" Alias="Generate Alerts for activity before the Initial Period(1 for Yes 0 for No)" Value="1" DataType="Int" /><Param Name="@MinimumAmount" Alias="Minimum Amount" Value="0" DataType="Money" /><Param Name="@ExcludeActChar" Alias="Exclude specific activity type characters (–None- for no use)" Value="-None-" DataType="String" /></Params>', 
LastOper = 'primeadmin' where WLCode = 'NEWCUST'
