-- USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_NewCust]    Script Date: 9/27/2019 5:04:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[USR_NewCust] (@WLCode SCode, 
			@testAlert INT, 
			@RiskClassList VARCHAR(8000),
			@InitialTimeFrame INT,
			@AvgPeriod INT,
			@GenerateAlertsBeforeInitialTimeFrame BIT,
			@MinimumAmount MONEY,
			@ExcludeActChar VARCHAR(300))
AS

/******************************************************************************
Procedure Name: USR_NewCust
Description:
Monitors new Customer activity for initial activity timeframe 
(first @InitialTimeFrame days specified as  parameter) and displays the aggregated 
values for amounts and counts. When the   initial activity timeframe is over,
system will suggest the profile  values  for  In/Out  amounts and counts based on a
average calculated during period of time (@AvgPeriod parameter number of days)
specified within initial  activity timeframe. 
Accepts comma separated risk  class codes. 
Use '-ALL-' for all Risk Classes. Designed to run Pre-Eod. 

Based on the parameter "@GenerateAlertsBeforeInitialTimeFrame" value,
For customers created within initial activity timeframe 
(less than @InitialTimeFrame days old),
when transaction that meets the criteria are are present, 
generates an alert New Customer ID C had $N of activity type 
A on account XXX on YYYYMMDD. 

Parameters: 
	@WLCode [in] SCode = Rule Code				
	@testAlert = Test Alert or Not (1 for Test, 0 for No)
	@RiskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@InitialTimeFrame = Initial Period (days)
	@AvgPeriod = Average Period (days)
	@GenerateAlertsBeforeInitialTimeFrame = Whether to Generate Alerts for activity 
								  before the Initial Period	
	@MinimumAmount = 0
	@ExcludeActChar  -none- for no use, exclude specific activity type name character.
*/

/*  Declarations */
DECLARE @description VARCHAR(2000),
    @Id INT,
    @WLType INT,
	@stat INT,
    @bookdate INT,
    @trnCnt INT,
	@RecvCount INT,
	@PayCount INT,
	@RecvAvg Money,
	@PayAvg Money,	
	@EODDays INT,
	@ActivityTot Money,
	@ActivityCount INT,
	@ActivityTot1 Money,
	@ActivityCount1 INT,
	@Account varchar(35),
	@Type varchar (11),
	@ActivityName varchar(80),
	@AlertString Varchar(8000),
	@AlertStringType Varchar(8000),
	@AlertStringAcc Varchar(8000),
	@AvgPeriodsCnt int

Declare @Cust VARCHAR(35), 
	@TranNo INT, 
	@CustAge INT

SET NOCOUNT ON
SET @stat = 0
SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())
DECLARE @AlertDate  DATETIME

/* Start standard stored procedure transaction header */
SELECT @trnCnt = @@trancount    -- Save the current trancount
IF @trnCnt = 0
   -- Transaction has not begun
	BEGIN TRAN USR_NewCust
Else
    -- Already in a transaction
    SAVE TRAN USR_NewCust
/* End standard stored procedure transaction header */

IF @AvgPeriod = 0
	SET @AvgPeriodsCnt = 1
ELSE IF @InitialTimeFrame < @AvgPeriod
	SET @AvgPeriodsCnt = 1
ELSE
	SET @AvgPeriodsCnt = @InitialTimeFrame / @AvgPeriod 

/*  standard Rules Header */
SELECT @description = [Desc], @WLType = WLType
FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

	-- Call BSA_fnListParams for each of the Paramters that support comma separated values
SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	
-- Date options
-- If UseSysDate = 0 or 1 then use current/system date
-- IF UseSysDate = 2 then use Business date FROM Sysparam
DECLARE @StartDate DATETIME

SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE, @StartDate = 
	CASE 	
		WHEN UseSysDate in (0,1) THEN
			-- use System date
			GETDATE()
		WHEN UseSysDate = 2 THEN
			-- use business date
			(SELECT BusDate FROM dbo.SysParam)
		ELSE
			GETDATE()
	END
FROM dbo.WatchList
WHERE WLCode = @WlCode

SELECT  @EODDays = DATEDIFF(dd, EODDate, @StartDate) FROM SysParam 

SET @EODDays = ISNULL(@EODDays, 0)

DECLARE CustomerCur CURSOR FAST_FORWARD FOR 
SELECT Id,  DATEDIFF(dd, c.CreateDate, @StartDate)
FROM  Customer c
WHERE 
	((ISNULL(@RiskClassList, '') = '' OR
		CHARINDEX(',' + ltrim(rtrim(c.RiskClass)) + ',',   @RiskClassList ) > 0))
	AND 
	DATEDIFF(dd, c.CreateDate, @StartDate) <= (@InitialTimeFrame + @EODDays)
----including only branch's customer 2019.11.7
    And try_cast(id as int) is not null
	--SEP.29.2021 add to check account
	and c.id in (select Cust from AccountOwner ao where c.id = ao.Cust and ISNUMERIC(ao.Account) = 1)
--
	
ORDER BY 1

OPEN CustomerCur
FETCH NEXT FROM CustomerCur INTO @cust,  @CustAge
WHILE @@FETCH_STATUS = 0
BEGIN
	IF (@CustAge <= @InitialTimeFrame)	
	BEGIN
		IF(@GenerateAlertsBeforeInitialTimeFrame =1)
		BEGIN
		SELECT @ActivityTot1 = 0, @ActivityCount1 = 0

		SELECT @ActivityTot1 = SUM(BaseAmt), @ActivityCount1 = Count(*) FROM Activity
			join ActivityType ATy on Activity.Type = ATy.Type
			join Customer C On Activity.Cust = C.[id]
			WHERE Cust = @cust AND 
			NOT EXISTS (Select * from ExemptionType where Code = Activity.ExemptionStatus)
			--SEP.2021 add activity type name
			and (ISNULL(@ExcludeActChar,'') = '' or ATy.Name not like '%' + @ExcludeActChar + '%')
			and bookdate >= dbo.ConvertSqlDatetoInt(C.createDate)
			HAVING SUM(BaseAmt) >= @minimumamount
			
		IF (@ActivityCount1 > 0 )
		BEGIN
			SELECT @ActivityTot = ISNULL(@ActivityTot1, 0), @ActivityCount = @ActivityCount1
			SELECT @ActivityTot1 = 0, @ActivityCount1 = 0
			
			SELECT @ActivityTot1 = SUM(BaseAmt), @ActivityCount1 = Count(*) FROM Activity A
				Join Customer C On A.Cust = C.[id]
				WHERE Cust = @cust  AND 
				bookdate >= dbo.ConvertSqlDatetoInt(C.createDate) AND
				NOT EXISTS (Select * from ExemptionType where Code = A.ExemptionStatus )
				HAVING SUM(BaseAmt) >= @minimumamount
			SELECT @ActivityTot = @ActivityTot + ISNULL(@ActivityTot1, 0), 
				@ActivityCount = @ActivityCount + ISNULL(@ActivityCount1, 0)
	
			SET @AlertString = 'New Customer ID '''+ @Cust +''' had $' + 
				isnull(Convert(Varchar,@ActivityTot),'') + ' of activity type : '
	
			-- Do the Activity Type 
			SELECT  DISTINCT CurrentArchActivity.Type TYPE, ATy.Name ActName INTO #TMPActType
				FROM CurrentArchActivity, ActivityType ATy
				WHERE Cust = @cust AND
				CurrentArchActivity.Type = ATy.Type AND
				NOT EXISTS (Select * from ExemptionType where Code = 																	CurrentArchActivity.ExemptionStatus)
				--SEP.2021 add activity type name
				and (ISNULL(@ExcludeActChar,'') = '' or ATy.Name not like '%' + @ExcludeActChar + '%')
			SET @AlertStringType = ''
			SELECT @AlertStringType = 		
				COALESCE(@AlertStringType + ', ','' )  + ActName
				FROM #TMPActType
			SELECT @AlertStringType = RIGHT(@AlertStringType, LEN(@AlertStringType) - 1)
			
			--Do the Account Type
			SELECT DISTINCT Account ActvAcc INTO #TMPActivityAcct 
				From CurrentArchActivity , ActivityType ATy
				WHERE CurrentArchActivity.Type = ATy.Type AND Cust = @Cust AND 
				NOT EXISTS (Select * from ExemptionType where Code = CurrentArchActivity.ExemptionStatus)
				--SEP.2021 add activity type name
				and (ISNULL(@ExcludeActChar,'') = '' or ATy.Name not like '%' + @ExcludeActChar + '%')
	
			SET @AlertStringAcc = ''
			SELECT @AlertStringAcc = 		
				COALESCE(@AlertStringAcc + ', ','' )  + ActvAcc
				FROM #TMPActivityAcct
			SELECT @AlertStringAcc = RIGHT(@AlertStringAcc, LEN(@AlertStringAcc) - 1)

			SELECT @AlertString = ISNULL(rtrim(@AlertString), '') +  
						ISNULL(ltrim(rtrim(@AlertStringType )), '')
						+	' on Account: ' + ISNULL(LTRIM(RTRIM(@AlertStringAcc)), '')
			
			IF EXISTS (SELECT * FROM tempdb.dbo.SYSOBJECTS 
						WHERE id = object_id('[tempdb].[dbo].[#TMPActType]'))	
			BEGIN
				DROP TABLE #TMPActType
			END
			IF EXISTS (SELECT * FROM tempdb.dbo.SYSOBJECTS 
						WHERE id =object_id('[tempdb].[dbo].[#TMPActivityAcct]'))
			BEGIN
				DROP TABLE #TMPActivityAcct
			END
			IF (@ActivityCount > 0)
			BEGIN
				IF @testAlert = 1 
				BEGIN
					SELECT @AlertDate = GETDATE()
					INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
						LASTMODIFY, CUST, ACCOUNT, IsTest) 
						VALUES ( @WLCode, @AlertString, 0, 
						@AlertDate, NULL, NULL, @Cust, NULL, 1)
					SELECT @stat = @@ERROR 
					IF @stat <> 0 GOTO EndOfProc
					
					INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				   	SELECT 'ALERT', ALERTNO, TRANNO 
					FROM 
					(	
						SELECT TranNo, Cust FROM CurrentArchActivity A
						Join Customer C On A.Cust = C.[id]
						Join ActivityType ATy On A.Type = ATy.Type
						WHERE Cust = @Cust AND
						bookdate >= dbo.ConvertSqlDatetoInt(C.createDate) AND
						NOT EXISTS (Select * from ExemptionType where Code = A.ExemptionStatus)
						--SEP.2021 add activity type name
						and (ISNULL(@ExcludeActChar,'') = '' or ATy.Name not like '%' + @ExcludeActChar + '%')
					)As t, Alert 
					WHERE
					t.Cust = Alert.Cust AND
					Alert.WLCode = @WLCODE AND
					Alert.CreateDate = @AlertDate
				
					SELECT @stat = @@ERROR 
					IF @stat <> 0 GOTO EndOfProc
				END 
				ELSE 
				BEGIN
					IF @WLType = 0 
					BEGIN
						SELECT @AlertDate = GETDATE()
						INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
							LASTMODIFY, CUST, ACCOUNT, IsTest) 
						   	VALUES (@WLCode, @AlertString, 0, 
							@AlertDate, NULL, NULL, @Cust, NULL, 0)
						SELECT @stat = @@ERROR 
						IF @stat <> 0 GOTO EndOfProc
				    END 
					ELSE 
					IF @WLType = 1 
					BEGIN
						SELECT @AlertDate = GETDATE()
						INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
							ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
							RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
							ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
							EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
							OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
							REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
							WLCode, WLDESC, CREATETIME )
						VALUES(NULL, @bookDate, @Cust, NULL,
							NULL, 'RULE', NULL, NULL, NULL, NULL,
							NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
							NULL, NULL, 0, 0, 0, 0, 
							NULL, NULL, NULL, NULL, 0, NULL, NULL,
							@WLCode, @AlertString , @AlertDate )
						SELECT @stat = @@ERROR 
						IF @stat <> 0 GOTO EndOfProc	
					END
					IF @WLTYPE = 0 BEGIN	
					INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
					   SELECT 'ALERT', ALERTNO, TRANNO 
						FROM 
						(
							SELECT TranNo, Cust FROM CurrentArchActivity A
							Join Customer C On A.Cust = C.[id]
							Join ActivityType ATy On A.Type = ATy.Type
							WHERE Cust = @Cust AND
							bookdate >= dbo.ConvertSqlDatetoInt(C.createDate) AND
							NOT EXISTS (Select * from ExemptionType where Code = 																	A.ExemptionStatus)
							--SEP.2021 add activity type name
							and (ISNULL(@ExcludeActChar,'') = '' or ATy.Name not like '%' + @ExcludeActChar + '%')
						)As t, Alert 
						WHERE
						t.Cust = Alert.Cust AND
						Alert.WLCode = @WLCODE AND
						Alert.CreateDate = @AlertDate
					
					SELECT @STAT = @@ERROR 
					IF @stat <> 0 GOTO EndOfProc
					END ELSE IF @WLTYPE = 1 BEGIN
					INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
					  SELECT 'SUSPACT', RECNO, TRANNO 
						FROM 
						(	
							SELECT TranNo, Cust FROM CurrentArchActivity A
							Join Customer C On A.Cust = C.[id]
							Join ActivityType ATy On A.Type = ATy.Type
							WHERE Cust = @Cust AND
							bookdate >= dbo.ConvertSqlDatetoInt(C.createDate) AND
							NOT EXISTS (Select * from ExemptionType where Code = 																	A.ExemptionStatus)
							--SEP.2021 add activity type name
							and (ISNULL(@ExcludeActChar,'') = '' or ATy.Name not like '%' + @ExcludeActChar + '%')
						)As t, SUSPICIOUSACTIVITY S 
						WHERE
						t.Cust = S.Cust AND
						S.WLCode = @WLCODE AND
						S.CREATETIME = @AlertDate
						
					SELECT @STAT = @@ERROR 
					IF @stat <> 0 GOTO EndOfProc
					END	
				END
					
			END
		END
		END -- End of IF(@GenerateAlertsBeforeInitialTimeFrame =1)
	END
	ELSE --Else part for @CustAge < @InitialTimeFrame ...
	BEGIN
		--Transactions are not needed to add to the SASActivity Table
		SELECT  @RecvCount=0, @RecvAvg =0,@PayCount=0, @PayAvg = 0
		SELECT @RecvCount=COUNT(*)/@AvgPeriodsCnt, @RecvAvg = SUM(BaseAmt)/@AvgPeriodsCnt FROM ActivityHist, customer C
			WHERE C.[id] = cust  AND
			Cust = @cust AND RecvPay = 1 AND
			bookdate <= dbo.ConvertSqlDatetoInt(DATEADD(dd, @InitialTimeFrame, C.createDate)) AND
			bookdate >= dbo.ConvertSqlDatetoInt(C.createDate)
			HAVING SUM(BaseAmt) >= @minimumamount

		SELECT @PayCount=COUNT(*)/@AvgPeriodsCnt, @PayAvg = SUM(BaseAmt)/@AvgPeriodsCnt FROM ActivityHist, customer C
			WHERE C.[id] = cust AND 
			Cust = @cust AND RecvPay = 2 AND
			bookdate <= dbo.ConvertSqlDatetoInt(DATEADD(dd, @InitialTimeFrame, C.createDate)) AND
			bookdate >= dbo.ConvertSqlDatetoInt(C.createDate)
			HAVING SUM(BaseAmt) >= @minimumamount
		IF (@PayCount > 0 OR @RecvCount > 0 ) 
		BEGIN
			IF @testAlert = 1 
			BEGIN
				INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
					LASTMODIFY, CUST, ACCOUNT, IsTest) 
				VALUES ( @WLCode, 'Customer: ''' + @Cust + 
					'''  Average In Amount: ' + Isnull(Convert(Varchar, @RecvAvg),'0 ')+
						'  In Count: ' + Isnull(Convert(varchar,@RecvCount),'')+ 
					'  Average Out Amount: ' + Isnull(Convert(Varchar, @PayAvg),'0 ')+
					'  Out Count: ' + Isnull(Convert(varchar,@PayCount),''), 0, 
					GETDATE(), NULL, NULL, @Cust, NULL, 1)
				IF @stat <> 0 GOTO EndOfProc
			END 
			ELSE 
			BEGIN
				IF @WLType = 0 
				BEGIN
					INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
						LASTMODIFY, CUST, ACCOUNT, IsTest) 
						VALUES (@WLCode, 'Customer: ''' + @Cust + 
						'''  Average In Amount: ' + Isnull(Convert(Varchar, @RecvAvg),'0 ')+
						'  In Count: ' + Isnull(Convert(varchar,@RecvCount),'')+ 
						'  Average Out Amount: ' + Isnull(Convert(Varchar, @PayAvg),'0 ')+
						'  Out Count: ' + Isnull(Convert(varchar,@PayCount),''), 0, 
						GETDATE(), NULL, NULL, @Cust, NULL, 0)
					IF @stat <> 0 GOTO EndOfProc
				END 
				ELSE 
				IF @WLType = 1 
				BEGIN
					INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
						ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
						RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
						ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
						EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
						OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
						REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
						WLCode, WLDESC, CREATETIME )
					VALUES(NULL, @bookDate, @Cust, NULL,
						NULL, 'RULE', NULL, NULL, NULL, NULL,
						NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
						NULL, NULL, 0, 0, 0, 0, 
						NULL, NULL, NULL, NULL, 0, NULL, NULL,
						@WLCode, 'Customer: ''' + @Cust + 
						'''  Average In Amount: ' + Isnull(Convert(Varchar, @RecvAvg),'0 ')+
			 			'  In Count: ' + Isnull(Convert(varchar,@RecvCount),'0 ')+ 
						'  Average Out Amount: ' + Isnull(Convert(Varchar, @PayAvg),'')+
						'  Out Count: ' + Isnull(Convert(varchar,@PayCount),'') , GETDATE() )
					IF @stat <> 0 GOTO EndOfProc	
				END
			END
		END
	END
	FETCH NEXT FROM CustomerCur INTO @cust, @CustAge
END


CLOSE CustomerCur
DEALLOCATE CustomerCur

EndOfProc:
IF (@stat <> 0) BEGIN
   ROLLBACK TRAN USR_NewCust
   RETURN @stat
END

IF @trnCnt = 0
   COMMIT TRAN USR_NewCust
RETURN @stat

GO


