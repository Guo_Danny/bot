use pbsa
go

declare @para1 varchar(8000);
DECLARE @strInCust varchar(500), @strExCust varchar(500), @strRel varchar(500)
declare @strTmp varchar(5000), @strTmp1 varchar(5000),@strTmp2 varchar(5000), @strTmp3 varchar(5000)
declare @strEnd varchar(5000), @i1 int, @i2 int, @i3 int

--ILgTrans90
select @strTmp='', @strTmp1='',@strTmp2='',@strTmp3=''
select @strTmp2 = '<Param Name="@customerList" Alias="Customer (comma separated list or -ALL-)" Value="-ALL-"'
select @strTmp3 = '<Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-"'

select  @strTmp1=Params from Watchlist  where WLCode like 'ILgTrans90%'

SELECT @strTmp3=REPLACE(@strTmp1, @strTmp2, @strTmp3)

UPDATE  Watchlist  SET Params=@strTmp3 where WLCode like 'ILgTrans90%'


--OLgTrans90
select @strTmp='', @strTmp1='',@strTmp2='',@strTmp3=''
select @strTmp2 = '<Param Name="@customerList" Alias="Customer (comma separated list or -ALL-)" Value="-ALL-"'
select @strTmp3 = '<Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-"'

select  @strTmp1=Params from Watchlist  where WLCode like 'OLgTrans90%'

SELECT @strTmp3=REPLACE(@strTmp1, @strTmp2, @strTmp3)

UPDATE  Watchlist  SET Params=@strTmp3 where WLCode like 'OLgTrans90%'


--IHRCtry1Mth
select @para1 = Params from Watchlist where WLCode = 'IHRCtry1Mth'

set @para1 = replace(@para1,'Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month"','Name="@lastnoofdays" Alias="Last No Of Days" Value="30"')

update Watchlist set Params = @para1, LastOper = 'primeadmin'
where WLCode = 'IHRCtry1Mth'

--OHRCtry1Mth
select @para1 = Params from Watchlist where WLCode = 'OHRCtry1Mth'

set @para1 = replace(@para1,'Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month"','Name="@lastnoofdays" Alias="Last No Of Days" Value="30"')

update Watchlist set Params = @para1, LastOper = 'primeadmin'
where WLCode = 'OHRCtry1Mth'

--IHRCtry3Mth
select @para1 = Params from Watchlist where WLCode = 'IHRCtry3Mth'

set @para1 = replace(@para1,'Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Quarter"','Name="@month" Alias="Last No Of Months" Value="3"')

update Watchlist set Params = @para1, LastOper = 'primeadmin'
where WLCode = 'IHRCtry3Mth'

--OHRCtry3Mth
select @para1 = Params from Watchlist where WLCode = 'OHRCtry3Mth'

set @para1 = replace(@para1,'Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Quarter"','Name="@month" Alias="Last No Of Months" Value="3"')

update Watchlist set Params = @para1, LastOper = 'primeadmin'
where WLCode = 'OHRCtry3Mth'

--IRndAmtAct
select @para1 = Params from Watchlist where WLCode = 'IRndAmtAct'

set @para1 = replace(@para1,'Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month"','Name="@lastnoofdays" Alias="Last No Of Days" Value="30"')

update Watchlist set Params = @para1, LastOper = 'primeadmin'
where WLCode = 'IRndAmtAct'

select @strInCust='<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="" /><Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'

select @strEnd='</Params>'
select @strTmp='', @strTmp1='',@strTmp2='',@strTmp3=''

select  @strTmp1=Params from Watchlist  where WLCode like 'IRndAmtAct%'

select @strTmp3= @strInCust + @strEnd

select len(@strtmp1),@strTmp1
select len(@strTmp3), @strTmp3
select @strend

SELECT @STRTMP=REPLACE(@STRTMP1, @STREND, @STRTMP3)
SELECT @STRTMP = replace(@STRTMP,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ')

SELECT LEN(@STRTMP), @strTmp

UPDATE  Watchlist  SET Params=@strTmp where WLCode like 'IRndAmtAct%'

--ORndAmtAct
select @para1 = Params from Watchlist where WLCode = 'ORndAmtAct'

set @para1 = replace(@para1,'Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month"','Name="@lastnoofdays" Alias="Last No Of Days" Value="30"')

update Watchlist set Params = @para1, LastOper = 'primeadmin'
where WLCode = 'ORndAmtAct'

select @strInCust='<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="" /><Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'

select @strEnd='</Params>'
select @strTmp='', @strTmp1='',@strTmp2='',@strTmp3=''

select  @strTmp1=Params from Watchlist  where WLCode like 'ORndAmtAct%'

select @strTmp3= @strInCust + @strEnd

select len(@strtmp1),@strTmp1
select len(@strTmp3), @strTmp3
select @strend

SELECT @STRTMP=REPLACE(@STRTMP1, @STREND, @STRTMP3)
SELECT @STRTMP = replace(@STRTMP,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ')

SELECT LEN(@STRTMP), @strTmp

UPDATE  Watchlist  SET Params=@strTmp where WLCode like 'ORndAmtAct%'
--LgBankHRCty
select @para1 = Params from Watchlist where WLCode = 'LgBankHRCty'

set @para1 = replace(@para1,'Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month"','Name="@lastnoofdays" Alias="Last No Of Days" Value="30"')

update Watchlist set Params = @para1, LastOper = 'primeadmin'
where WLCode = 'LgBankHRCty'

--IMultiBene
select @para1 = Params from Watchlist where WLCode = 'IMultiBene'

set @para1 = replace(@para1,'Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month"','Name="@lastnoofdays" Alias="Last No Of Days" Value="30"')

set @para1 = replace(@para1,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ')


update Watchlist set Params = @para1, LastOper = 'primeadmin'
where WLCode = 'IMultiBene'


select @strInCust='<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="" /><Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'

select @strEnd='</Params>'
select @strTmp='', @strTmp1='',@strTmp2='',@strTmp3=''

select  @strTmp1=Params from Watchlist  where WLCode like 'IMultiBene%'

select @strTmp3= @strInCust + @strEnd

select len(@strtmp1),@strTmp1
select len(@strTmp3), @strTmp3
select @strend

SELECT @STRTMP=REPLACE(@STRTMP1, @STREND, @STRTMP3)
SELECT @STRTMP = replace(@STRTMP,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ')

SELECT LEN(@STRTMP), @strTmp

UPDATE  Watchlist  SET Params=@strTmp where WLCode like 'IMultiBene%'


--OMultiBene
select @para1 = Params from Watchlist where WLCode = 'OMultiBene'

set @para1 = replace(@para1,'Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month"','Name="@lastnoofdays" Alias="Last No Of Days" Value="30"')

set @para1 = replace(@para1,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ')


update Watchlist set Params = @para1, LastOper = 'primeadmin'
where WLCode = 'OMultiBene'

select @strInCust='<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="" /><Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'

select @strEnd='</Params>'
select @strTmp='', @strTmp1='',@strTmp2='',@strTmp3=''

select  @strTmp1=Params from Watchlist  where WLCode like 'OMultiBene%'

select @strTmp3= @strInCust + @strEnd

select len(@strtmp1),@strTmp1
select len(@strTmp3), @strTmp3
select @strend

SELECT @STRTMP=REPLACE(@STRTMP1, @STREND, @STRTMP3)
SELECT @STRTMP = replace(@STRTMP,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ')

SELECT LEN(@STRTMP), @strTmp

UPDATE  Watchlist  SET Params=@strTmp where WLCode like 'OMultiBene%'

--IMultiOrg
select @para1 = Params from Watchlist where WLCode = 'IMultiOrg'

set @para1 = replace(@para1,'Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month"','Name="@lastnoofdays" Alias="Last No Of Days" Value="30"')

set @para1 = replace(@para1,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ')


update Watchlist set Params = @para1, LastOper = 'primeadmin'
where WLCode = 'IMultiOrg'


select @strInCust='<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="" /><Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'

select @strEnd='</Params>'
select @strTmp='', @strTmp1='',@strTmp2='',@strTmp3=''

select  @strTmp1=Params from Watchlist  where WLCode like 'IMultiOrg%'

select @strTmp3= @strInCust + @strEnd

select len(@strtmp1),@strTmp1
select len(@strTmp3), @strTmp3
select @strend

SELECT @STRTMP=REPLACE(@STRTMP1, @STREND, @STRTMP3)

SELECT @STRTMP = replace(@STRTMP,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ')

SELECT LEN(@STRTMP), @strTmp

UPDATE  Watchlist  SET Params=@strTmp where WLCode like 'IMultiOrg%'


--OMultiOrg
select @para1 = Params from Watchlist where WLCode = 'OMultiOrg'

set @para1 = replace(@para1,'Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month"','Name="@lastnoofdays" Alias="Last No Of Days" Value="30"')

set @para1 = replace(@para1,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ')


update Watchlist set Params = @para1, LastOper = 'primeadmin'
where WLCode = 'OMultiOrg'

select @strInCust='<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="" /><Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'

select @strEnd='</Params>'
select @strTmp='', @strTmp1='',@strTmp2='',@strTmp3=''

select  @strTmp1=Params from Watchlist  where WLCode like 'OMultiOrg%'

select @strTmp3= @strInCust + @strEnd

select len(@strtmp1),@strTmp1
select len(@strTmp3), @strTmp3
select @strend

SELECT @STRTMP=REPLACE(@STRTMP1, @STREND, @STRTMP3)

SELECT @STRTMP = replace(@STRTMP,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ')

SELECT LEN(@STRTMP), @strTmp

UPDATE  Watchlist  SET Params=@strTmp where WLCode like 'OMultiOrg%'

--U1MnthBetXY
select @para1 = Params from Watchlist where WLCode = 'U1MnthBetXY'

set @para1 = replace(@para1,'<Params>','<Params><Param Name="@lastnoofdays" Alias="Last No Of Days" Value="30" DataType="String" />')

update Watchlist set Params = @para1, LastOper = 'primeadmin'
where WLCode = 'U1MnthBetXY'

select @strInCust='<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="" /><Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'

select @strEnd='</Params>'
select @strTmp='', @strTmp1='',@strTmp2='',@strTmp3=''

select  @strTmp1=Params from Watchlist  where WLCode like 'U1MnthBetXY%'

select @strTmp3= @strInCust + @strEnd

select len(@strtmp1),@strTmp1
select len(@strTmp3), @strTmp3
select @strend

SELECT @STRTMP=REPLACE(@STRTMP1, @STREND, @STRTMP3)
SELECT LEN(@STRTMP), @strTmp

UPDATE  Watchlist  SET Params=@strTmp where WLCode like 'U1MnthBetXY%'

--SameBeneOrg

select @strInCust='<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="" /><Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'

select @strEnd='</Params>'
select @strTmp='', @strTmp1='',@strTmp2='',@strTmp3=''

select  @strTmp1=Params from Watchlist  where WLCode like 'SameBeneOrg%'

select @strTmp3= @strInCust + @strEnd

select len(@strtmp1),@strTmp1
select len(@strTmp3), @strTmp3
select @strend

SELECT @STRTMP=REPLACE(@STRTMP1, @STREND, @STRTMP3)
SELECT LEN(@STRTMP), @strTmp

UPDATE  Watchlist  SET Params=@strTmp where WLCode like 'SameBeneOrg%'


update Watchlist set Schedule = 1, IsPreEOD = 1, UseSysDate = 1  where WLCode in (
'IHRCtry1Mth',
'OHRCtry1Mth',
'IRndAmtAct',
'LgBankHRCty',
'ORndAmtAct',
'ILgTrans90',
'OLgTrans90',
'IMultiBene',
'OMultiBene',
'IMultiOrg',
'OMultiOrg',
'U1MnthBetXY',
'UAcctMthVol',
'UAcctRskCty',
'SameBeneOrg')

update Watchlist set UseSysDate = 1  where WLCode in (
'IHRCtry3Mth',
'OHRCtry3Mth')


INSERT INTO PBSA.dbo.Watchlist([WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], [CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], [ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], [OverrideExemption], [TemplateWLCode])
  VALUES('DomToActive', 'Detects accounts that have been dormant then become active', 'Detects accounts that have been dormant for at least a specified number of days and then becomes active. Designed to be run Pre-EOD.', 1, 'USR_DomToActive', 'Rule', 0, 1, NULL, NULL, NULL, 'primeadmin', getdate(), 'PRIMEADMIN', getdate(), NULL, 0, 1, '', 0, 15, 0, 1, 1, '<Params><Param Name="@day" Alias="Day" Value="365" DataType="Int" /><Param Name="@branchlist" Alias="Branches" Value="-ALL-" DataType="String" /><Param Name="@deptlist" Alias="Departments" Value="-ALL-" DataType="String" /><Param Name="@ThresholdAmt" Alias="ThresholdAmount" Value="1" DataType="Int" /><Param Name="@ActvTypeList" Alias="Comma seperated list of activity types to be included. -ALL- for all" Value="-ALL-" DataType="String" /><Param Name="@ExcludeActvTypeList" Alias="Comma seperated list of activity types to be excluded. -NONE- for none." Value="-NONE-" DataType="String" /></Params>', 0, 1, 'User Defined', 0, 1, 'DormToAct')
GO
INSERT INTO PBSA.dbo.Watchlist([WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], [CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], [ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], [OverrideExemption], [TemplateWLCode])
  VALUES('XCntAmt', 'Detects Customers with more than X transactions and/or have aggregate greater than or equal to dollar threshold within a specific time frame.', 'Detects Customers that had more than X Transactions, using a specified activity type(cash,wires etc), that has aggregate equal or greater to dollar threshold, within specified time frame. An alert or SA case is generated for each customer that has more than X transactions and/or the total amount equals or exceeds the threshold amount over a certain time period. Filtered by Customer Type and Risk Class. Designed to be run Post-EOD on a weekly, monthly or quarterly schedule.', 1, 'USR_XCntAmt', 'Rule', 0, 1, NULL, NULL, NULL, 'primeadmin', getdate(), 'PRIMEADMIN', getdate(), NULL, 0, 1, '', 0, 15, 0, 1, 1, '<Params><Param Name="@LastNoOfDays" Alias="Last No Of Days" Value="30" DataType="Int" /><Param Name="@ActivityTypeList" Alias="List of activity Types Separated by comma.-ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@ThresholdAmt" Alias="Threshold Amount" Value="10000" DataType="Money" /><Param Name="@NoOfTransactions" Alias="No Of Transactions" Value="1" DataType="Int" /><Param Name="@RiskClassList" Alias="List of Risk Classes separated by comma. -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@CustTypeList" Alias="List of Customer Types separated by comma. -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@AmtCompareOper" Alias="Amount/Count Comparison (1 for OR, 2 for AND)" Value="2" DataType="Int" /><Param Name="@branchList" Alias="List of Branches separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String" /></Params>', 0, 1, 'User Defined', 1, 0, 'CustCntAmt')
GO
INSERT INTO PBSA.dbo.Watchlist([WLCode], [Title], [Desc], [WLType], [SPName], [SuspType], [Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], [CreateOper], [CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat], [RuleType], [RuleText], [RuleFormat], [ExecTime], [CaseScore], [UseAssignedScore], [IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate], [OverrideExemption], [TemplateWLCode])
  VALUES('XInTOutT', 'Detect incoming transactions followed by outgoing transactions of specified type within a designated percent over a period of time', 'Detects customers with incoming (receive) transactions of a specified activity type and outgoing (pay) transactions of a specified activity type where the total amounts differ by less than or equal to a specified percentage.  This rule evaluates transactions that are within the specified number of days and the aggregate amount of the qualifying transactions exceeds the specified minimum amount.  Additional constraints include Cash/Non-Cash for inbound and outbound activity types, Customer Type and Risk Class.  Designed to be run Post-EOD', 1, 'USR_XInTOutT', 'Rule', 0, 1, NULL, NULL, NULL, 'primeadmin', getdate(), 'PRIMEADMIN', getdate(), NULL, 0, 1, '', 0, 15, 0, 1, 1, '<Params><Param Name="@day" Alias="Number of Days" Value="30" DataType="Int" /><Param Name="@percent" Alias="Percent" Value="10" DataType="Int" /><Param Name="@inActivityTypeList" Alias="List of inbound activity Types separated by comma. Use -ALL- for any activity type." Value="-ALL-" DataType="String" /><Param Name="@outActivityTypeList" Alias="List of outbound activity Types separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@minSumAmt" Alias="Minimum aggregated transaction amount that must be exceeded." Value="0" DataType="Money" /><Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String" /><Param Name="@inboundCashType" Alias="Cash or non-Cash (inbound). 1 for Cash, 0 for NonCash, 2 for both" Value="2" DataType="Int" /><Param Name="@outboundCashType" Alias="Cash or non-Cash (outbound). 1 for Cash, 0 for NonCash, 2 for both" Value="2" DataType="Int" /><Param Name="@UseRelAcct" Alias="Include Related Accounts (1=yes, 0=no) " Value="0" DataType="Int" /><Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="Int" /><Param Name="@ExcludeRelList" Alias="Exclude Relationship Types (comma separated list or -NONE-)" Value="-NONE-" DataType="String" /></Params>', 0, 1, 'User Defined', 1, 1, 'XPrcInTOutT')
GO
