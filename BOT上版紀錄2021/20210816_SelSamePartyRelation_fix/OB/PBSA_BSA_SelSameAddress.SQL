USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[BSA_SelSameAddress]    Script Date: 6/7/2021 9:13:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER Procedure [dbo].[BSA_SelSameAddress]
AS

DECLARE	@TT TABLE 
(
		CustId		VARCHAR(40),
		PartyId 	VARCHAR(40),
		Code		VARCHAR(40)
)

DECLARE	@TT1 TABLE 
(
		Address		VARCHAR(4000),
		CustId 	    VARCHAR(40)
)

DECLARE @UniAddr TABLE
(
       Addr		VARCHAR(4000)
)

DECLARE @TTid TABLE (
        no int, --IDENTITY(1,1),  
		id VARCHAR(40)
)

DECLARE @TTaddress TABLE (
      address VARCHAR(4000)
)

DECLARE @Party TABLE (
		PartyId	       VARCHAR(40),
		RelatedCode    VARCHAR(40),
		RelatedParty   VARCHAR(40),
	    sCode          VARCHAR(255),
		cOper		   VARCHAR(11),
		cDate          DATETIME,
		lOper          VARCHAR(11),
		lModify        DATETIME,
		Del            bit,
		Past           bit
)

DECLARE @Naddress VARCHAR(4000), @Oaddress VARCHAR(4000),@oAddr VARCHAR(4000),@oID VARCHAR(40);
DECLARE @iCnt int, @iTotCnt int, @jCnt int;
DECLARE @IdBuff VARCHAR(400), @DestRelatedCode VARCHAR(400), @txt VARCHAR(8000);

SELECT @DestRelatedCode='SameAddress'

insert into @TT1
select c.address,c.id from customer c(nolock) 
where isnull(c.address,'') != '' 
and id in (select cust from accountowner Ao(nolock) inner join  account A(nolock) on Ao.account=A.id where Ao.relationship='OWNER' and A.type in ('Q','K'))
and c.address in (Select b.address from customer b(nolock) where isnull(b.address,'') != ''  group by b.address having count(b.address)>1 )
group by c.Address, c.id
order by c.address,c.id

insert into @UniAddr select Address from @TT1 group by Address order by Address
--select * from @UniAddr

declare cur_id CURSOR FOR
select Addr from @UniAddr group by Addr order by Addr
open cur_id;
fetch next from cur_id into @oAddr;
while @@FETCH_STATUS=0
begin 

	--select @oAddr as UniAddress  
	 
	declare cur_id1 CURSOR FOR
	Select CustId from @TT1 where Address = @oAddr group by CustId order by CustId

	open cur_id1;
	fetch next from cur_id1 into @oID;

	while @@FETCH_STATUS=0
	begin 

	  insert into @TTid
	  Select ROW_NUMBER() OVER(ORDER BY CustId) AS ROWID ,CustId from @TT1 where Address=@oAddr group by CustId  order by CustId

	   select @iTotCnt=0, @iCnt=0
	   select @iTotCnt=count(*) from @TTid
	   --select @iTotCnt as totrec, @oRid as ReletedParty, @nRid as NewRelatedParty
	   --select * from @TTid

	   if ( @iTotCnt > 0   )
		  WHILE( @iCnt < @iTotCnt )
		  BEGIN
			   select @iCnt=@iCnt+1
			   insert into @TT select id, null, 'Same Address' from @TTid --where no!=@iCnt
			   select @jCnt=0
			   select @jCnt=@jCnt+1
			   select @IdBuff=id from @TTid where no=@iCnt
			   delete @TT where isnull(PartyId,'')='' and CustId=@IdBuff
			   update @TT SET PartyId= @IdBuff where isnull(PartyId,'')='' 
			   --select @iCnt as iCnt, @jCnt as jCnt, @IdBuff as IdBuff
			   --select * from @TT
		END 

		if ( @iTotCnt > 0   )
		begin
			--select * from @TTid
			--select * from @TT
			delete @TTid	
			--select * from @TTid
		end
	  
	   fetch next from cur_id1 into @oID;  
	   --SELECT @oRid AS MASTERRELATED

	end
	CLOSE cur_id1;
    DEALLOCATE cur_id1;
	
	fetch next from cur_id into @oAddr;
end

CLOSE cur_id;
DEALLOCATE cur_id;

--select * from @TT

insert into @Party
select distinct CustId, @DestRelatedCode, PartyId, Code, 'PrimeAdmin', GETDATE(),'PrimeAdmin',Getdate(),0,0 from @TT
select @jCnt=count(*) from @Party
--select * from @Party
 
--for test mask
--FOR PRODUCT : Delete mask

insert into PartyRelation ( PartyId,Relationship,RelatedParty,RelationSource, CreateOper,CreateDate,LastOper,LastModify,Deleted,RelatedInPast )
  select PartyId	,RelatedCode ,RelatedParty ,sCode ,cOper ,cDate ,lOper ,lModify ,Del ,Past  from @Party


--insert into event log
Select @txt = 'Batch Add Same Address to  ''' + @DestRelatedCode + ''', Total Records: ' + convert(VARCHAR(20),@jCnt)
Insert into Event (TrnTime,Oper,type,ObjectType,ObjectId,LogText,EvtDetail) values
(getdate(), 'PrimeAdmin', 'Ann', 'PartyRel', @DestRelatedCode, '',@txt)


