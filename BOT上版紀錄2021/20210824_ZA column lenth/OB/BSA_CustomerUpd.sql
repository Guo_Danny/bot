USE [PBSA]
GO

/****** Object:  Trigger [dbo].[BSA_CustomerUpd]    Script Date: 9/16/2021 9:39:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-------------------------------------------------------------------------------

ALTER TRIGGER [dbo].[BSA_CustomerUpd] on [dbo].[Customer]   For Update As


	Declare @Action		varchar(255)
	Declare @LastOper	SCode
/*
  --20201126 add for notes input over size bug
  Declare @iNotesLen int ,  @strNotes varchar(8000), @txt varchar(8000) 
  
	--20201126 polly add for Notes input over size bug , start
	--get field size from system table 
	select @iNotesLen=2000
	select @iNotesLen=b.max_length 
	    from sys.tables a join sys.all_columns b on a.object_id = b.object_id 
		where a.name = 'CUSTOMER' and b.name = 'Notes'
	--20201126 polly add for Notes too long bug , end
*/
	
  If @@rowcount > 0 begin	

	if exists (select 1 from optiontbl where Code='KYCC' and enabled = 1)
	begin
		
		if exists (select 1 from inserted i, deleted d where i.id = d.id and i.Name <> d.Name ) 
		begin
			Update KYCCNames 
			set CustName= I.[Name],
				KYCCName = dbo.BSA_RemoveNoiseWords(I.[Name])
			 from KYCCNames K, Inserted I, deleted d
			where I.id = K.id and
				  K.CustName = d.[Name]
		end

	end

	select @LastOper = i.LastOper
	from Inserted i
	
	/* Profile Enable / Disable Changes - Start
			This code block has been added for Enable / Disable the Profile based on Customer / Account Exemption.
			1. If a customer is exempted from profiling, then the profile should not be generated.  
			2. If the customer is not exempted, but an account is assigned a status that is exempt from profiling, 
			   then just that account should not be included in the profile.
			3. If the customer is not exempted and the account is not exempted, enable the profile associated with Customer / Account
			*/

		--Update profiles for Customer if the exemption status has changed
			--Disable the profile for the Customer if Exempt from Profiling
			UPDATE P
			SET Enabled = 0,
			LastOper = @LastOper
			FROM Profile P INNER JOIN Inserted I ON P.Cust = I.Id
			INNER JOIN Deleted D ON D.Id = I.Id
			INNER JOIN ExemptionType e WITH (NOLOCK) ON
			e.Code = i.ExemptionStatus AND SUBSTRING(e.ExemptEntity,1,1) = 1 
			AND SUBSTRING(e.ExemptFrom,1,1) = 1
			WHERE P.Enabled <> 0 AND IsNull(d.ExemptionStatus,0) <> IsNull(i.ExemptionStatus,0)

			-- Non-Exempted Customer Cases
			-- Case 2 : If the Accout is exempted, disable the profile associated with the Account
			UPDATE P
			SET Enabled = 0,
			LastOper = @LastOper
			FROM Profile P INNER JOIN Inserted I ON P.Cust = I.Id
			INNER JOIN Deleted D ON D.Id = I.Id
			INNER JOIN Account a WITH (NOLOCK) ON p.Account = a.Id
			INNER JOIN ExemptionType et WITH (NOLOCK) ON a.ExemptionStatus = et.Code
			WHERE SUBSTRING(et.ExemptEntity,2,1) = 1 and SUBSTRING(et.ExemptFrom,1,1) = 1
			AND P.Enabled <> 0 AND IsNull(d.ExemptionStatus,0) <> IsNull(i.ExemptionStatus,0)

			-- Case 3 : 
				--A. Enable the Profile if the Customer is not exempt and the Account on the profile record is null
				--B. Enable the Profile if the Customer is not exempt and the Accout on the profile is not exempt 
			UPDATE P
			SET Enabled = 1,
			LastOper = @LastOper
			FROM Profile P INNER JOIN Inserted I ON P.Cust = I.Id
			INNER JOIN Deleted D ON D.Id = I.Id
			LEFT JOIN ExemptionType e WITH (NOLOCK) ON e.Code = i.ExemptionStatus
			LEFT JOIN Account a WITH (NOLOCK) ON p.Account = a.Id
			LEFT JOIN ExemptionType et WITH (NOLOCK) ON a.ExemptionStatus = et.Code
			where 
			(P.ACCOUNT IS NULL 
			  OR
			(SUBSTRING(et.ExemptEntity,2,1) = 0 OR SUBSTRING(et.ExemptFrom,1,1) = 0
					OR a.ExemptionStatus IS NULL)) 
			AND
			(SUBSTRING(e.ExemptEntity,1,1) = 0 OR SUBSTRING(e.ExemptFrom,1,1) = 0
					 OR i.ExemptionStatus Is Null)
			AND 
			P.Enabled <> 1 
			AND 
			IsNull(d.ExemptionStatus,0) <> IsNull(i.ExemptionStatus,0)	
	
	Update Customer Set LastModify = GetDate()
	From Inserted I, Customer d Where  i.Id = d.Id

	  --2018.07 modify for sg ExemptionStatus do not be override
	  --Update Customer Set Notes = isnull(d.Notes,'') + ',D:' + isnull(d.ExemptionStatus,'') + ',I:' + isnull(i.ExemptionStatus ,'') 
	  --from Inserted I, Customer d Where  i.Id = d.Id
	  --and i.ExemptionStatus is null 
	  --and d.ExemptionStatus is not null

	  -- Disable profiles where Risk Class has changed and profiling is disabled
	  update p set enabled = 0, RiskClass = r.Code, lastOper = 'Prime'
	  From Profile P join Inserted C on p.Cust = c.[Id] 
	  Join Deleted D on D.[Id] = C.[Id] AND D.RiskClass <> C.RiskClass
	  Join RiskClass R on c.RiskClass = r.Code
	  Where P.OverRideEnableProfiling = 1 AND P.ActiveProfile = 1
	  AND P.enabled = 1 AND R.EnableProfiling = 0

	  -- Enable profiles where Risk Class has changed and profiling is enabled
	  update p set enabled = 1, RiskClass = r.Code, lastOper = 'Prime'
	  From Profile P join Inserted C on p.Cust = c.[Id] 
	  Join Deleted D on D.[Id] = C.[Id] AND D.RiskClass <> C.RiskClass
	  Join RiskClass R on c.RiskClass = r.Code
	  Where P.OverRideEnableProfiling = 1 AND P.ActiveProfile = 1
	  AND P.enabled = 0 AND R.EnableProfiling = 1

	------------------------------------------------------------
	--Disable Risk Scoring and Acceptance for closed Customers
	------------------------------------------------------------
	if exists (select i.id from Inserted i 
				inner merge join Deleted d on i.id = d.id 
			    where IsNull(d.Closed, 0) = 0 and
					  IsNull(i.Closed, 0) = 1)
		begin
			if dbo.BSA_fnGetEDDOption('DRSCC') = '1'
			  begin
				--Disable Risk Scoring for closed Customers
				update Customer
				set RFCalculate = 0
				where id in (select i.id from Inserted i 
				inner merge join Deleted d on i.id = d.id 
			    where IsNull(d.Closed, 0) = 0 and
					  IsNull(i.Closed, 0) = 1)


				--Process pending Risk Scores 
				--for closed Customers
				select @Action = dbo.BSA_fnGetEDDOption('APRSCC')

				set @Action = ltrim(rtrim(IsNull(@Action, '')))

				if @Action = 'delete'
				 begin
					-- Delete Pending Risk Scores for 
					--closed Customers 
					Delete RiskFactorData
					Where Cust in (select i.id from Inserted i 
						inner merge join Deleted d on i.id = d.id 
						where IsNull(d.Closed, 0) = 0 and
							  IsNull(i.Closed, 0) = 1) and
							  RiskFactorData.Status = 0 --pending

					--Delete Proposed Score from Risk Score History
					--for Closed Customers
					delete from RiskScoreHist
					where DateAccepted is null and
					CustomerId in (select i.id from Inserted i 
						inner merge join Deleted d on i.id = d.id 
						where IsNull(d.Closed, 0) = 0 and
							  IsNull(i.Closed, 0) = 1)

				 end
				else if @Action = 'auto accept'
				 begin
					-- Delete Old accepted scores other than the ones
					-- recently accepted, for closed customers
					Delete RiskFactorData
					Where Status = 1 and
					Cust in (select i.id from Inserted i 
						inner merge join Deleted d on i.id = d.id 
						where IsNull(d.Closed, 0) = 0 and
							  IsNull(i.Closed, 0) = 1)

					--Accept pending items for closed customer					
					Update RiskFactorData
					Set  Status = 1, -- Reviewed/Accepted
						 LastReview = GetDate(),
						 LastReviewOper = i.LastOper
					 from RiskFactorData rfd
						inner join Inserted i on rfd.Cust = i.Id
						inner merge join Deleted d on i.id = d.id 
					 where IsNull(d.Closed, 0)   = 0 and
							IsNull(i.Closed, 0)   = 1 and
							IsNull(rfd.Status, 0) = 0


					--Accept Proposed Risk Class for closed Customer
					update Customer
					set RiskClass = rsh.RiskClass
					from Customer c
						inner join RiskScoreHist rsh on c.Id= rsh.CustomerId 
						inner join Inserted i on rsh.CustomerId = i.Id
						inner merge join Deleted d on i.id = d.id 
					where IsNull(d.Closed, 0) = 0 and
						  IsNull(i.Closed, 0) = 1 and
						  rsh.DateAccepted is null 

					--Accept Proposed Score for closed Customer
					update RiskScoreHist
					set DateAccepted = GetDate(),
						AcceptOper   = i.LastOper									
					from RiskScoreHist rsh 
						inner join Inserted i on rsh.CustomerId = i.Id
						inner merge join Deleted d on i.id = d.id 
					where IsNull(d.Closed, 0) = 0 and
						  IsNull(i.Closed, 0) = 1 and
						  rsh.DateAccepted is null 

				 end
			  end

			if dbo.BSA_fnGetEDDOption('DACC') = '1' 					  
			  begin
				--Disable Acceptance for closed Customers
				update Customer
				set DoCIP = 0
				where id in (select i.id from Inserted i 
				inner merge join Deleted d on i.id = d.id 
			    where IsNull(d.Closed, 0) = 0 and
					  IsNull(i.Closed, 0) = 1)
			   end

			--Process pending Acceptance Items 
			--for closed Customers
			select @Action = dbo.BSA_fnGetEDDOption('APAICC')

			set @Action = ltrim(rtrim(IsNull(@Action, '')))
			
			if @Action = 'delete'
			 begin
				-- Delete pending Acceptance Items
				-- for closed Customers 
				Delete from CheckListData
				Where IsNull(Status,0) = 0 and 
					Cust in (select i.id from Inserted i 
					inner merge join Deleted d on i.id = d.id 
					where IsNull(d.Closed, 0) = 0 and
						  IsNull(i.Closed, 0) = 1)

			 end
			else if @Action = 'set waived, completed and approved'
			 begin
				
				-- waive and complete pending Acceptance Items 
				-- for closed Customers 
				UPDATE CheckListData
				SET Decision = 4, --waived
					Status	= Case 	When cli.AppRequired = 1
						Then 3 -- Approved
						else Case When cli.SecCheckRequired = 1
						Then 2 -- Second Checked
						Else 1 -- Completed
						End End,

					ApprovedBy = Case 
						When cli.AppRequired = 1 
						Then i.LastOper
						else ApprovedBy end,

					ApproveDate = Case 
						When cli.AppRequired = 1 
						Then GetDate()
						else ApproveDate end,

					SecondCheckBy = Case 
						When cli.SecCheckRequired = 1
						Then i.LastOper
						else SecondCheckBy end,

					SecondCheckDate	=  Case 
						When cli.SecCheckRequired = 1
						Then GetDate()
						else SecondCheckDate end,	

					CompleteDate = GetDate(),				
					CompletedBy  = i.LastOper,
					LastOper	 = i.LastOper					
				from CheckListData cld
					inner join Inserted i on cld.Cust = i.Id
					inner merge join Deleted d  on i.id = d.id 
					inner join CheckListItem cli 
						on cli.Code = cld.CLICode
				where IsNull(d.Closed,   0) = 0 and
					  IsNull(i.Closed,   0) = 1 and 
					  IsNull(cld.Status, 0) = 0
				
			 end
		end

    ------------------------------------------------------------
	--Disable Risk Scoring when Customers Risk Class is locked 
	------------------------------------------------------------
	if exists (select i.id from Inserted i 
				inner merge join Deleted d on i.id = d.id 
				inner merge join RiskClass rc on i.RiskClass = rc.Code
			    where IsNull(d.RiskClass, '') <>
					  IsNull(i.RiskClass, '') and 
					  IsNull(rc.Locked,0) = 1)
		begin
			if dbo.BSA_fnGetEDDOption('DRSRCL') = '1'
			  begin
				-- Disable Risk Scoring, when Customers 
				-- Risk Class is locked.
				update Customer
				set RFCalculate = 0
				where id in (select i.id from Inserted i 
				inner merge join Deleted d on i.id = d.id 
				inner merge join RiskClass rc on i.RiskClass = rc.Code
			    where IsNull(d.RiskClass, '') <>
					  IsNull(i.RiskClass, '') and 
					  IsNull(rc.Locked,0) = 1)


				--Process pending Risk Scores when Customers 
				-- Risk Class is locked.			
				select @Action = dbo.BSA_fnGetEDDOption('APRSRCL')

				set @Action = ltrim(rtrim(IsNull(@Action, '')))

				if @Action = 'delete'
				 begin
					-- Delete Pending Risk Scores when Customers 
				    -- Risk Class is locked.
					Delete RiskFactorData
					Where Cust in (select i.id from Inserted i 
						inner merge join Deleted d on i.id = d.id 
						inner merge join RiskClass rc on i.RiskClass = rc.Code
						where IsNull(d.RiskClass, '') <>
							  IsNull(i.RiskClass, '') and 
							  IsNull(rc.Locked,0) = 1) and
							  RiskFactorData.Status = 0 --pending


					--Delete Proposed Score from Risk Score History
					--when Customers Risk Class is locked.
					delete from RiskScoreHist
					where DateAccepted is null and
					CustomerId in (select i.id from Inserted i 
							inner merge join Deleted d on i.id = d.id 
							inner merge join RiskClass rc on i.RiskClass = rc.Code
							where IsNull(d.RiskClass, '') <>
								  IsNull(i.RiskClass, '') and 
								  IsNull(rc.Locked,0) = 1)

				 end
				else if @Action = 'auto accept'
				 begin
					-- Delete Old accepted scores other than the ones
					-- recently accepted, when Customers 
					-- Risk Class is locked.
					Delete RiskFactorData
					Where Status = 1 and
						  Cust in (select i.id from Inserted i 
							inner merge join Deleted d on i.id = d.id 
							inner merge join RiskClass rc on i.RiskClass = rc.Code
							where IsNull(d.RiskClass, '') <>
								  IsNull(i.RiskClass, '') and 
								  IsNull(rc.Locked,0) = 1)

					-- Accept pending items when Customers 
				    -- Risk Class is locked.				
					Update RiskFactorData
					Set Status = 1,			-- Reviewed/Accepted
						 LastReview = GetDate(),
						 LastReviewOper = i.LastOper					  
					  from RiskFactorData rfd 
					  inner merge join Inserted i on rfd.Cust = i.Id
					  inner merge join Deleted d on i.id = d.id 
					  inner merge join RiskClass rc on i.RiskClass = rc.Code
					  where IsNull(d.RiskClass, '') <>
							IsNull(i.RiskClass, '') and 
							IsNull(rc.Locked,   0) = 1 and
							IsNull(rfd.Status,  0) = 0


					-- Accept Proposed Score when Customers 
				    -- Risk Class is locked.
					update RiskScoreHist
					set DateAccepted = GetDate(),
						AcceptOper   = i.LastOper,
						RiskClass	 = i.RiskClass --Lock Risk Class									
					from RiskScoreHist rsh
					inner merge join Inserted i on rsh.CustomerId = i.Id
					inner merge join Deleted d on i.id = d.id 
					inner merge join RiskClass rc on i.RiskClass = rc.Code
					where IsNull(d.RiskClass, '') <>
						  IsNull(i.RiskClass, '') and 
						  IsNull(rc.Locked,0) = 1 and
						  rsh.DateAccepted is null

				 end
			  end

     end

  
  Insert Event (TrnTime, Oper, Type, ObjectType, ObjectId , LogText, EvtDetail) 
   Select getDate(), i.LastOper, 'Mod', 'Cust', convert(varchar, i.Id), 	 Case when i.status = d.status and 
	IsNull(i.ExemptionStatus,'') = IsNull(d.ExemptionStatus,'') then ''
	When i.status <> d.status and IsNull(i.ExemptionStatus,'') = IsNull(d.ExemptionStatus,'') then 
	'Customer status was changed from ' + 
	case when d.status = 1 then 'Accepted' 
	when d.status = 2 then 'Over Due' 
	when d.status  = 3 then 'Rejected' 
	 End + ' to ' + case 
	when i.Status = 1 then 'Accepted' 
	when i.status = 2 then 'Over Due' 
	when i.status = 3 then 'Rejected'
	End
	when i.status = d.status and 
	(IsNull(i.ExemptionStatus,'') <> IsNull(d.ExemptionStatus,'')
	and i.ExemptionStatus Is Not Null) then 
'Added an Exemption which deleted all Profiles for this customer.'
	when i.status <> d.status and 
	(IsNull(i.ExemptionStatus,'') <> IsNull(d.ExemptionStatus,'')	and i.ExemptionStatus Is Not Null) then
	'Customer status was changed from ' +
	case when d.status = 1 then 'Accepted'
	when d.status = 2 then 'Over Due' 
	when d.status  = 3 then 'Rejected' 
	End + ' to ' + case
	when i.Status = 1 then 'Accepted' 
	when i.status = 2 then 'Over Due' 
	when i.status = 3 then 'Rejected' 
	End + '. Added an Exemption which deleted all Profiles for this customer.' End, 
Case 
	WHEN ((i.[Id] Is Null And d.[Id] Is Not Null ) Or 
	(i.[Id] Is Not Null And d.[Id] Is Null ) Or 
	(i.[Id] != d.[Id])) 
	THEN '| Id OLD: ' + Convert(varchar, IsNull(d.[Id],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Id],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Parent] Is Null And d.[Parent] Is Not Null ) Or 
	(i.[Parent] Is Not Null And d.[Parent] Is Null ) Or 
	(i.[Parent] != d.[Parent])) 
	THEN '| Parent OLD: ' + Convert(varchar, IsNull(d.[Parent],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Parent],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Name] Is Null And d.[Name] Is Not Null ) Or 
	(i.[Name] Is Not Null And d.[Name] Is Null ) Or 
	(i.[Name] != d.[Name])) 
	THEN '| Name OLD: ' + Convert(varchar(100), IsNull(d.[Name],'Null')) + 
	' NEW: ' + Convert(varchar(100), IsNull(i.[Name],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[DBA] Is Null And d.[DBA] Is Not Null ) Or 
	(i.[DBA] Is Not Null And d.[DBA] Is Null ) Or 
	(i.[DBA] != d.[DBA])) 
	THEN '| DBA OLD: ' + Convert(varchar, IsNull(d.[DBA],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[DBA],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[SecCode] Is Null And d.[SecCode] Is Not Null ) Or 
	(i.[SecCode] Is Not Null And d.[SecCode] Is Null ) Or 
	(i.[SecCode] != d.[SecCode])) 
	THEN '| SecCode OLD: ' + Convert(varchar, IsNull(d.[SecCode],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[SecCode],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Address] Is Null And d.[Address] Is Not Null ) Or 
	(i.[Address] Is Not Null And d.[Address] Is Null ) Or 
	(i.[Address] != d.[Address])) 
	THEN '| Address OLD: ' + Convert(varchar, IsNull(d.[Address],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Address],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[City] Is Null And d.[City] Is Not Null ) Or 
	(i.[City] Is Not Null And d.[City] Is Null ) Or 
	(i.[City] != d.[City])) 
	THEN '| City OLD: ' + Convert(varchar, IsNull(d.[City],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[City],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[State] Is Null And d.[State] Is Not Null ) Or 
	(i.[State] Is Not Null And d.[State] Is Null ) Or 
	(i.[State] != d.[State])) 
	THEN '| State OLD: ' + Convert(varchar, IsNull(d.[State],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[State],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Zip] Is Null And d.[Zip] Is Not Null ) Or 
	(i.[Zip] Is Not Null And d.[Zip] Is Null ) Or 
	(i.[Zip] != d.[Zip])) 
	THEN '| Zip OLD: ' + Convert(varchar, IsNull(d.[Zip],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Zip],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Country] Is Null And d.[Country] Is Not Null ) Or 
	(i.[Country] Is Not Null And d.[Country] Is Null ) Or 
	(i.[Country] != d.[Country])) 
	THEN '| Country OLD: ' + Convert(varchar, IsNull(d.[Country],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Country],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Telephone] Is Null And d.[Telephone] Is Not Null ) Or 
	(i.[Telephone] Is Not Null And d.[Telephone] Is Null ) Or 
	(i.[Telephone] != d.[Telephone])) 
	THEN '| Telephone OLD: ' + Convert(varchar, IsNull(d.[Telephone],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Telephone],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Email] Is Null And d.[Email] Is Not Null ) Or 
	(i.[Email] Is Not Null And d.[Email] Is Null ) Or 
	(i.[Email] != d.[Email])) 
	THEN '| Email OLD: ' + Convert(varchar, IsNull(d.[Email],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Email],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[TIN] Is Null And d.[TIN] Is Not Null ) Or 
	(i.[TIN] Is Not Null And d.[TIN] Is Null ) Or 
	(i.[TIN] != d.[TIN])) 
	THEN '| SSN/TIN(TIN) OLD: ' + Convert(varchar, IsNull(d.[TIN],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[TIN],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[LicenseNo] Is Null And d.[LicenseNo] Is Not Null ) Or 
	(i.[LicenseNo] Is Not Null And d.[LicenseNo] Is Null ) Or 
	(i.[LicenseNo] != d.[LicenseNo])) 
	THEN '| License/Cedula No(LicenseNo) OLD: ' + Convert(varchar, IsNull(d.[LicenseNo],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[LicenseNo],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[LicenseState] Is Null And d.[LicenseState] Is Not Null ) Or 
	(i.[LicenseState] Is Not Null And d.[LicenseState] Is Null ) Or 
	(i.[LicenseState] != d.[LicenseState])) 
	THEN '| IncomeDate(LicenseState) OLD: ' + Convert(varchar, IsNull(d.[LicenseState],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[LicenseState],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[PassportNo] Is Null And d.[PassportNo] Is Not Null ) Or 
	(i.[PassportNo] Is Not Null And d.[PassportNo] Is Null ) Or 
	(i.[PassportNo] != d.[PassportNo])) 
	THEN '| PassportNo OLD: ' + Convert(varchar, IsNull(d.[PassportNo],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[PassportNo],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[DOB] Is Null And d.[DOB] Is Not Null ) Or 
	(i.[DOB] Is Not Null And d.[DOB] Is Null ) Or 
	(i.[DOB] != d.[DOB])) 
	THEN '| DOB OLD: ' + Convert(varchar, IsNull(d.[DOB],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[DOB],0))
	ELSE '' End +
Case 
	WHEN ((i.[TypeOfBusiness] Is Null And d.[TypeOfBusiness] Is Not Null ) Or 
	(i.[TypeOfBusiness] Is Not Null And d.[TypeOfBusiness] Is Null ) Or 
	(i.[TypeOfBusiness] != d.[TypeOfBusiness])) 
	THEN '| TypeOfBusiness OLD: ' + Convert(varchar, IsNull(d.[TypeOfBusiness],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[TypeOfBusiness],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[SourceOfFunds] Is Null And d.[SourceOfFunds] Is Not Null ) Or 
	(i.[SourceOfFunds] Is Not Null And d.[SourceOfFunds] Is Null ) Or 
	(i.[SourceOfFunds] != d.[SourceOfFunds])) 
	THEN '| PrimarySourceIncome(SourceOfFunds) OLD: ' + Convert(varchar, IsNull(d.[SourceOfFunds],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[SourceOfFunds],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[AccountOfficer] Is Null And d.[AccountOfficer] Is Not Null ) Or 
	(i.[AccountOfficer] Is Not Null And d.[AccountOfficer] Is Null ) Or 
	(i.[AccountOfficer] != d.[AccountOfficer])) 
	THEN '| AccountOfficer OLD: ' + Convert(varchar, IsNull(d.[AccountOfficer],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[AccountOfficer],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[AcctOffTel] Is Null And d.[AcctOffTel] Is Not Null ) Or 
	(i.[AcctOffTel] Is Not Null And d.[AcctOffTel] Is Null ) Or 
	(i.[AcctOffTel] != d.[AcctOffTel])) 
	THEN '| AcctOffTel OLD: ' + Convert(varchar, IsNull(d.[AcctOffTel],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[AcctOffTel],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[AcctOffEmail] Is Null And d.[AcctOffEmail] Is Not Null ) Or 
	(i.[AcctOffEmail] Is Not Null And d.[AcctOffEmail] Is Null ) Or 
	(i.[AcctOffEmail] != d.[AcctOffEmail])) 
	THEN '| AcctOffEmail OLD: ' + Convert(varchar, IsNull(d.[AcctOffEmail],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[AcctOffEmail],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[CompOfficer] Is Null And d.[CompOfficer] Is Not Null ) Or 
	(i.[CompOfficer] Is Not Null And d.[CompOfficer] Is Null ) Or 
	(i.[CompOfficer] != d.[CompOfficer])) 
	THEN '| CompOfficer OLD: ' + Convert(varchar, IsNull(d.[CompOfficer],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[CompOfficer],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[CompOffTel] Is Null And d.[CompOffTel] Is Not Null ) Or 
	(i.[CompOffTel] Is Not Null And d.[CompOffTel] Is Null ) Or 
	(i.[CompOffTel] != d.[CompOffTel])) 
	THEN '| CompOffTel OLD: ' + Convert(varchar, IsNull(d.[CompOffTel],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[CompOffTel],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[CompOffEmail] Is Null And d.[CompOffEmail] Is Not Null ) Or 
	(i.[CompOffEmail] Is Not Null And d.[CompOffEmail] Is Null ) Or 
	(i.[CompOffEmail] != d.[CompOffEmail])) 
	THEN '| CompOffEmail OLD: ' + Convert(varchar, IsNull(d.[CompOffEmail],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[CompOffEmail],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[IdList] Is Null And d.[IdList] Is Not Null ) Or 
	(i.[IdList] Is Not Null And d.[IdList] Is Null ) Or 
	(i.[IdList] != d.[IdList])) 
	THEN '| IdList OLD: ' + Convert(varchar, IsNull(d.[IdList],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IdList],'Null'))
	ELSE '' End +
Case 
    ----20201126 change for Notes field size extend from 2000 to 4000
	WHEN (((i.[Notes] Is Null And d.[Notes] Is Not Null ) Or 
	(i.[Notes] Is Not Null And d.[Notes] Is Null ) Or 
	(i.[Notes] != d.[Notes]))  and
	(len(Convert(varchar(4000), IsNull(i.[Notes],'Null'))) > len(Convert(varchar(4000), IsNull(d.[Notes],'Null')))))
	THEN '| Notes ADDED: ' + right(Convert(varchar(4000), IsNull(i.[Notes],'Null')), 
        len(Convert(varchar(4000), IsNull(i.[Notes],'Null')))-len(Convert(varchar(4000), IsNull(d.[Notes],'Null'))))
	ELSE '' End +
Case 
	WHEN ((i.[ExemptionStatus] Is Null And d.[ExemptionStatus] Is Not Null ) Or 
	(i.[ExemptionStatus] Is Not Null And d.[ExemptionStatus] Is Null ) Or 
	(i.[ExemptionStatus] != d.[ExemptionStatus])) 
	THEN '| ExemptionStatus OLD: ' + Convert(varchar, IsNull(d.[ExemptionStatus],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ExemptionStatus],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[LastActReset] Is Null And d.[LastActReset] Is Not Null ) Or 
	(i.[LastActReset] Is Not Null And d.[LastActReset] Is Null ) Or 
	(i.[LastActReset] != d.[LastActReset])) 
	THEN '| LastActReset OLD: ' + case when d.[LastActReset] is Null then 'Null' else 
	Convert(varchar, d.[LastActReset]) End + 
	' NEW: ' + case when i.[LastActReset] is Null then 'Null' else 
	Convert(varchar, i.[LastActReset]) End
	ELSE '' End +
Case 
	WHEN ((i.[LastReview] Is Null And d.[LastReview] Is Not Null ) Or 
	(i.[LastReview] Is Not Null And d.[LastReview] Is Null ) Or 
	(i.[LastReview] != d.[LastReview])) 
	THEN '| LastReview OLD: ' + case when d.[LastReview] is Null then 'Null' else 
	Convert(varchar, d.[LastReview]) End + 
	' NEW: ' + case when i.[LastReview] is Null then 'Null' else 
	Convert(varchar, i.[LastReview]) End
	ELSE '' End +
Case 
	WHEN ((i.[LastReviewOper] Is Null And d.[LastReviewOper] Is Not Null ) Or 
	(i.[LastReviewOper] Is Not Null And d.[LastReviewOper] Is Null ) Or 
	(i.[LastReviewOper] != d.[LastReviewOper])) 
	THEN '| LastReviewOper OLD: ' + Convert(varchar, IsNull(d.[LastReviewOper],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[LastReviewOper],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[CreateOper] Is Null And d.[CreateOper] Is Not Null ) Or 
	(i.[CreateOper] Is Not Null And d.[CreateOper] Is Null ) Or 
	(i.[CreateOper] != d.[CreateOper])) 
	THEN '| CreateOper OLD: ' + Convert(varchar, IsNull(d.[CreateOper],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[CreateOper],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[CreateDate] Is Null And d.[CreateDate] Is Not Null ) Or 
	(i.[CreateDate] Is Not Null And d.[CreateDate] Is Null ) Or 
	(i.[CreateDate] != d.[CreateDate])) 
	THEN '| CreateDate OLD: ' + case when d.[CreateDate] is Null then 'Null' else 
	Convert(varchar, d.[CreateDate]) End + 
	' NEW: ' + case when i.[CreateDate] is Null then 'Null' else 
	Convert(varchar, i.[CreateDate]) End
	ELSE '' End +
Case 
	WHEN ((i.[LastOper] Is Null And d.[LastOper] Is Not Null ) Or 
	(i.[LastOper] Is Not Null And d.[LastOper] Is Null ) Or 
	(i.[LastOper] != d.[LastOper])) 
	THEN '| LastOper OLD: ' + Convert(varchar, IsNull(d.[LastOper],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[LastOper],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[LastModify] Is Null And d.[LastModify] Is Not Null ) Or 
	(i.[LastModify] Is Not Null And d.[LastModify] Is Null ) Or 
	(i.[LastModify] != d.[LastModify])) 
	THEN '| LastModify OLD: ' + case when d.[LastModify] is Null then 'Null' else 
	Convert(varchar, d.[LastModify]) End + 
	' NEW: ' + case when i.[LastModify] is Null then 'Null' else 
	Convert(varchar, i.[LastModify]) End
	ELSE '' End +
Case 
	WHEN ((i.[OwnerBranch] Is Null And d.[OwnerBranch] Is Not Null ) Or 
	(i.[OwnerBranch] Is Not Null And d.[OwnerBranch] Is Null ) Or 
	(i.[OwnerBranch] != d.[OwnerBranch])) 
	THEN '| OwnerBranch OLD: ' + Convert(varchar, IsNull(d.[OwnerBranch],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[OwnerBranch],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[OwnerDept] Is Null And d.[OwnerDept] Is Not Null ) Or 
	(i.[OwnerDept] Is Not Null And d.[OwnerDept] Is Null ) Or 
	(i.[OwnerDept] != d.[OwnerDept])) 
	THEN '| OwnerDept OLD: ' + Convert(varchar, IsNull(d.[OwnerDept],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[OwnerDept],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[OwnerOper] Is Null And d.[OwnerOper] Is Not Null ) Or 
	(i.[OwnerOper] Is Not Null And d.[OwnerOper] Is Null ) Or 
	(i.[OwnerOper] != d.[OwnerOper])) 
	THEN '| OwnerOper OLD: ' + Convert(varchar, IsNull(d.[OwnerOper],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[OwnerOper],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[RiskClass] Is Null And d.[RiskClass] Is Not Null ) Or 
	(i.[RiskClass] Is Not Null And d.[RiskClass] Is Null ) Or 
	(i.[RiskClass] != d.[RiskClass])) 
	THEN '| RiskClass OLD: ' + Convert(varchar, IsNull(d.[RiskClass],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[RiskClass],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Closed] Is Null And d.[Closed] Is Not Null ) Or 
	(i.[Closed] Is Not Null And d.[Closed] Is Null ) Or 
	(i.[Closed] != d.[Closed])) 
	THEN '| Closed OLD: ' + case when IsNull(d.[Closed],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[Closed],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[User1] Is Null And d.[User1] Is Not Null ) Or 
	(i.[User1] Is Not Null And d.[User1] Is Null ) Or 
	(i.[User1] != d.[User1])) 
	THEN '| Pri.SourceIn.Amt(User1) OLD: ' + Convert(varchar, IsNull(d.[User1],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User1],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[User2] Is Null And d.[User2] Is Not Null ) Or 
	(i.[User2] Is Not Null And d.[User2] Is Null ) Or 
	(i.[User2] != d.[User2])) 
	THEN '| SourceOfIncome1(User2) OLD: ' + Convert(varchar, IsNull(d.[User2],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User2],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[User3] Is Null And d.[User3] Is Not Null ) Or 
	(i.[User3] Is Not Null And d.[User3] Is Null ) Or 
	(i.[User3] != d.[User3])) 
	THEN '| SourceIncome1Amt(User3) OLD: ' + Convert(varchar, IsNull(d.[User3],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User3],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[User4] Is Null And d.[User4] Is Not Null ) Or 
	(i.[User4] Is Not Null And d.[User4] Is Null ) Or 
	(i.[User4] != d.[User4])) 
	THEN '| SourceOfIncome2(User4) OLD: ' + Convert(varchar, IsNull(d.[User4],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User4],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[User5] Is Null And d.[User5] Is Not Null ) Or 
	(i.[User5] Is Not Null And d.[User5] Is Null ) Or 
	(i.[User5] != d.[User5])) 
	THEN '| SourceIncome2Amt(User5) OLD: ' + Convert(varchar, IsNull(d.[User5],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[User5],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[CountryOfOrigin] Is Null And d.[CountryOfOrigin] Is Not Null ) Or 
	(i.[CountryOfOrigin] Is Not Null And d.[CountryOfOrigin] Is Null ) Or 
	(i.[CountryOfOrigin] != d.[CountryOfOrigin])) 
	THEN '| CountryOfOrigin OLD: ' + Convert(varchar, IsNull(d.[CountryOfOrigin],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[CountryOfOrigin],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[CountryOfIncorp] Is Null And d.[CountryOfIncorp] Is Not Null ) Or 
	(i.[CountryOfIncorp] Is Not Null And d.[CountryOfIncorp] Is Null ) Or 
	(i.[CountryOfIncorp] != d.[CountryOfIncorp])) 
	THEN '| CountryOfIncorp OLD: ' + Convert(varchar, IsNull(d.[CountryOfIncorp],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[CountryOfIncorp],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[OffshoreCorp] Is Null And d.[OffshoreCorp] Is Not Null ) Or 
	(i.[OffshoreCorp] Is Not Null And d.[OffshoreCorp] Is Null ) Or 
	(i.[OffshoreCorp] != d.[OffshoreCorp])) 
	THEN '| OffshoreCorp OLD: ' + Convert(varchar, IsNull(d.[OffshoreCorp],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[OffshoreCorp],0))
	ELSE '' End +
Case 
	WHEN ((i.[BearerShares] Is Null And d.[BearerShares] Is Not Null ) Or 
	(i.[BearerShares] Is Not Null And d.[BearerShares] Is Null ) Or 
	(i.[BearerShares] != d.[BearerShares])) 
	THEN '| BearerShares OLD: ' + case when IsNull(d.[BearerShares],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[BearerShares],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[NomeDePlume] Is Null And d.[NomeDePlume] Is Not Null ) Or 
	(i.[NomeDePlume] Is Not Null And d.[NomeDePlume] Is Null ) Or 
	(i.[NomeDePlume] != d.[NomeDePlume])) 
	THEN '| NomeDePlume OLD: ' + case when IsNull(d.[NomeDePlume],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[NomeDePlume],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[Type] Is Null And d.[Type] Is Not Null ) Or 
	(i.[Type] Is Not Null And d.[Type] Is Null ) Or 
	(i.[Type] != d.[Type])) 
	THEN '| Type OLD: ' + Convert(varchar, IsNull(d.[Type],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Type],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Resident] Is Null And d.[Resident] Is Not Null ) Or 
	(i.[Resident] Is Not Null And d.[Resident] Is Null ) Or 
	(i.[Resident] != d.[Resident])) 
	THEN '| Resident OLD: ' + case when IsNull(d.[Resident],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[Resident],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[BusRelationNature] Is Null And d.[BusRelationNature] Is Not Null ) Or 
	(i.[BusRelationNature] Is Not Null And d.[BusRelationNature] Is Null ) Or 
	(i.[BusRelationNature] != d.[BusRelationNature])) 
	THEN '| BusRelationNature OLD: ' + Convert(varchar, IsNull(d.[BusRelationNature],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[BusRelationNature],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[PrevRelations] Is Null And d.[PrevRelations] Is Not Null ) Or 
	(i.[PrevRelations] Is Not Null And d.[PrevRelations] Is Null ) Or 
	(i.[PrevRelations] != d.[PrevRelations])) 
	THEN '| PrevRelations OLD: ' + Convert(varchar, IsNull(d.[PrevRelations],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[PrevRelations],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[PEP] Is Null And d.[PEP] Is Not Null ) Or 
	(i.[PEP] Is Not Null And d.[PEP] Is Null ) Or 
	(i.[PEP] != d.[PEP])) 
	THEN '| PEP OLD: ' + case when IsNull(d.[PEP],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[PEP],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[PoliticalPos] Is Null And d.[PoliticalPos] Is Not Null ) Or 
	(i.[PoliticalPos] Is Not Null And d.[PoliticalPos] Is Null ) Or 
	(i.[PoliticalPos] != d.[PoliticalPos])) 
	THEN '| PoliticalPos OLD: ' + Convert(varchar, IsNull(d.[PoliticalPos],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[PoliticalPos],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[CorrBankRelation] Is Null And d.[CorrBankRelation] Is Not Null ) Or 
	(i.[CorrBankRelation] Is Not Null And d.[CorrBankRelation] Is Null ) Or 
	(i.[CorrBankRelation] != d.[CorrBankRelation])) 
	THEN '| CorrBankRelation OLD: ' + case when IsNull(d.[CorrBankRelation],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[CorrBankRelation],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[Sex] Is Null And d.[Sex] Is Not Null ) Or 
	(i.[Sex] Is Not Null And d.[Sex] Is Null ) Or 
	(i.[Sex] != d.[Sex])) 
	THEN '| Sex OLD: ' + Convert(varchar, IsNull(d.[Sex],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Sex],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[ShellBank] Is Null And d.[ShellBank] Is Not Null ) Or 
	(i.[ShellBank] Is Not Null And d.[ShellBank] Is Null ) Or 
	(i.[ShellBank] != d.[ShellBank])) 
	THEN '| ShellBank OLD: ' + case when IsNull(d.[ShellBank],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[ShellBank],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[HighRiskRespBank] Is Null And d.[HighRiskRespBank] Is Not Null ) Or 
	(i.[HighRiskRespBank] Is Not Null And d.[HighRiskRespBank] Is Null ) Or 
	(i.[HighRiskRespBank] != d.[HighRiskRespBank])) 
	THEN '| HighRiskRespBank OLD: ' + case when IsNull(d.[HighRiskRespBank],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[HighRiskRespBank],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[OffshoreBank] Is Null And d.[OffshoreBank] Is Not Null ) Or 
	(i.[OffshoreBank] Is Not Null And d.[OffshoreBank] Is Null ) Or 
	(i.[OffshoreBank] != d.[OffshoreBank])) 
	THEN '| OffshoreBank OLD: ' + case when IsNull(d.[OffshoreBank],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[OffshoreBank],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[PayThroughAC] Is Null And d.[PayThroughAC] Is Not Null ) Or 
	(i.[PayThroughAC] Is Not Null And d.[PayThroughAC] Is Null ) Or 
	(i.[PayThroughAC] != d.[PayThroughAC])) 
	THEN '| PayThroughAC OLD: ' + case when IsNull(d.[PayThroughAC],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[PayThroughAC],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[RegulatedAffiliate] Is Null And d.[RegulatedAffiliate] Is Not Null ) Or 
	(i.[RegulatedAffiliate] Is Not Null And d.[RegulatedAffiliate] Is Null ) Or 
	(i.[RegulatedAffiliate] != d.[RegulatedAffiliate])) 
	THEN '| RegulatedAffiliate OLD: ' + case when IsNull(d.[RegulatedAffiliate],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[RegulatedAffiliate],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[USPerson] Is Null And d.[USPerson] Is Not Null ) Or 
	(i.[USPerson] Is Not Null And d.[USPerson] Is Null ) Or 
	(i.[USPerson] != d.[USPerson])) 
	THEN '| USPerson OLD: ' + Convert(varchar, IsNull(d.[USPerson],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[USPerson],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[RFCalculate] Is Null And d.[RFCalculate] Is Not Null ) Or 
	(i.[RFCalculate] Is Not Null And d.[RFCalculate] Is Null ) Or 
	(i.[RFCalculate] != d.[RFCalculate])) 
	THEN '| RFCalculate OLD: ' + case when IsNull(d.[RFCalculate],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[RFCalculate],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[Status] Is Null And d.[Status] Is Not Null ) Or 
	(i.[Status] Is Not Null And d.[Status] Is Null ) Or 
	(i.[Status] != d.[Status])) 
	THEN '| Status OLD: ' + Convert(varchar, IsNull(d.[Status],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Status],0))
	ELSE '' End +
Case 
	WHEN ((i.[ReviewDate] Is Null And d.[ReviewDate] Is Not Null ) Or 
	(i.[ReviewDate] Is Not Null And d.[ReviewDate] Is Null ) Or 
	(i.[ReviewDate] != d.[ReviewDate])) 
	THEN '| ReviewDate OLD: ' + case when d.[ReviewDate] is Null then 'Null' else 
	Convert(varchar, d.[ReviewDate]) End + 
	' NEW: ' + case when i.[ReviewDate] is Null then 'Null' else 
	Convert(varchar, i.[ReviewDate]) End
	ELSE '' End +
Case 
	WHEN ((i.[CTRAmt] Is Null And d.[CTRAmt] Is Not Null ) Or 
	(i.[CTRAmt] Is Not Null And d.[CTRAmt] Is Null ) Or 
	(i.[CTRAmt] != d.[CTRAmt])) 
	THEN '| CTRAmt OLD: ' + Convert(varchar, IsNull(d.[CTRAmt],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[CTRAmt],0))
	ELSE '' End +
Case 
	WHEN ((i.[AmtRange] Is Null And d.[AmtRange] Is Not Null ) Or 
	(i.[AmtRange] Is Not Null And d.[AmtRange] Is Null ) Or 
	(i.[AmtRange] != d.[AmtRange])) 
	THEN '| AmtRange OLD: ' + Convert(varchar, IsNull(d.[AmtRange],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[AmtRange],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[SwiftTID] Is Null And d.[SwiftTID] Is Not Null ) Or 
	(i.[SwiftTID] Is Not Null And d.[SwiftTID] Is Null ) Or 
	(i.[SwiftTID] != d.[SwiftTID])) 
	THEN '| SwiftTID OLD: ' + Convert(varchar, IsNull(d.[SwiftTID],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[SwiftTID],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[Embassy] Is Null And d.[Embassy] Is Not Null ) Or 
	(i.[Embassy] Is Not Null And d.[Embassy] Is Null ) Or 
	(i.[Embassy] != d.[Embassy])) 
	THEN '| Embassy OLD: ' + case when IsNull(d.[Embassy],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[Embassy],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[ForeignGovt] Is Null And d.[ForeignGovt] Is Not Null ) Or 
	(i.[ForeignGovt] Is Not Null And d.[ForeignGovt] Is Null ) Or 
	(i.[ForeignGovt] != d.[ForeignGovt])) 
	THEN '| ForeignGovt OLD: ' + case when IsNull(d.[ForeignGovt],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[ForeignGovt],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[CharityOrg] Is Null And d.[CharityOrg] Is Not Null ) Or 
	(i.[CharityOrg] Is Not Null And d.[CharityOrg] Is Null ) Or 
	(i.[CharityOrg] != d.[CharityOrg])) 
	THEN '| CharityOrg OLD: ' + case when IsNull(d.[CharityOrg],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[CharityOrg],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[DoCIP] Is Null And d.[DoCIP] Is Not Null ) Or 
	(i.[DoCIP] Is Not Null And d.[DoCIP] Is Null ) Or 
	(i.[DoCIP] != d.[DoCIP])) 
	THEN '| DoCIP OLD: ' + case when IsNull(d.[DoCIP],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[DoCIP],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[Prospect] Is Null And d.[Prospect] Is Not Null ) Or 
	(i.[Prospect] Is Not Null And d.[Prospect] Is Null ) Or 
	(i.[Prospect] != d.[Prospect])) 
	THEN '| Prospect OLD: ' + case when IsNull(d.[Prospect],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[Prospect],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[AssetSize] Is Null And d.[AssetSize] Is Not Null ) Or 
	(i.[AssetSize] Is Not Null And d.[AssetSize] Is Null ) Or 
	(i.[AssetSize] != d.[AssetSize])) 
	THEN '| AssetSize OLD: ' + Convert(varchar, IsNull(d.[AssetSize],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[AssetSize],0))
	ELSE '' End +
Case 
	WHEN ((i.[Income] Is Null And d.[Income] Is Not Null ) Or 
	(i.[Income] Is Not Null And d.[Income] Is Null ) Or 
	(i.[Income] != d.[Income])) 
	THEN '| AnnualIncome(Income) OLD: ' + Convert(varchar, IsNull(d.[Income],0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[Income],0))
	ELSE '' End +
Case 
	WHEN ((i.[IndOrBusType] Is Null And d.[IndOrBusType] Is Not Null ) Or 
	(i.[IndOrBusType] Is Not Null And d.[IndOrBusType] Is Null ) Or 
	(i.[IndOrBusType] != d.[IndOrBusType])) 
	THEN '| IndOrBusType OLD: ' + Convert(varchar, IsNull(d.[IndOrBusType],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[IndOrBusType],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[FEP] Is Null And d.[FEP] Is Not Null ) Or 
	(i.[FEP] Is Not Null And d.[FEP] Is Null ) Or 
	(i.[FEP] != d.[FEP])) 
	THEN '| FEP OLD: ' + case when IsNull(d.[FEP],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[FEP],0) = 0 then 'False' else 'True' End
	ELSE '' End +
Case 
	WHEN ((i.[MSB] Is Null And d.[MSB] Is Not Null ) Or 
	(i.[MSB] Is Not Null And d.[MSB] Is Null ) Or 
	(i.[MSB] != d.[MSB])) 
	THEN '| MSB OLD: ' + case when IsNull(d.[MSB],0) = 0 then 'False' else 'True' End + 
	' NEW: ' + case when IsNull(i.[MSB],0) = 0 then 'False' else 'True' End
	ELSE '' End  +
Case 
	WHEN ((i.[KYCStatus] Is Null And d.[KYCStatus] Is Not Null ) Or 
	(i.[KYCStatus] Is Not Null And d.[KYCStatus] Is Null ) Or 
	(i.[KYCStatus] != d.[KYCStatus]) Or (d.KYCStatus Is NULL)) 
	THEN '| OnBoard Status has changed from ''' + 
	Case 
		When isnull(d.KYCstatus,0) = 0 Then 'No Status'
		When d.KYCstatus = 1 Then 'CDD Incomplete' 
		When d.KYCstatus = 2 Then 'CDD Complete' 
		When d.KYCstatus  = 3 Then 'CDD 1st Approval Required' 
		When d.KYCstatus  = 4 Then 'CDD 2nd Approval Required' 
		When d.KYCstatus  = 5 Then 'CDD 3rd Approval Required' 
		When d.KYCstatus  = 6 Then 'CDD Rejected' 
		else 'Not Defined'
	End 
	+ ''' to ''' + 
	Case 
		When isnull(i.KYCstatus,0) = 0 Then 'No Status'
		When i.KYCstatus = 1 Then 'CDD Incomplete' 
		When i.KYCstatus = 2 Then 'CDD Complete' 
		When i.KYCstatus  = 3 Then 'CDD 1st Approval Required' 
		When i.KYCstatus  = 4 Then 'CDD 2nd Approval Required' 
		When i.KYCstatus  = 5 Then 'CDD 3rd Approval Required' 
		When i.KYCstatus  = 6 Then 'CDD Rejected'
		else 'Not Defined'
	End
	+ '''' 
	ELSE '' END +
-- 	End  + 			
Case 
	WHEN ((i.[KYCDataCreateDate] Is Null And d.[KYCDataCreateDate] Is Not Null ) Or 
	(i.[KYCDataCreateDate] Is Not Null And d.[KYCDataCreateDate] Is Null ) Or 
	(i.[KYCDataCreateDate] != d.[KYCDataCreateDate])) 
	THEN '| OnBoard Date OLD: ' + case when d.[KYCDataCreateDate] is Null then 'Null' else 
	Convert(varchar, d.[KYCDataCreateDate]) End + 
	' NEW: ' + case when i.[KYCDataCreateDate] is Null then 'Null' else 
	Convert(varchar, i.[KYCDataCreateDate]) End
	ELSE '' End +
Case 
	WHEN ((i.[KYCDataModifyDate] Is Null And d.[KYCDataModifyDate] Is Not Null ) Or 
	(i.[KYCDataModifyDate] Is Not Null And d.[KYCDataModifyDate] Is Null ) Or 
	(i.[KYCDataModifyDate] != d.[KYCDataModifyDate])) 
	THEN '| Re-OnBoard Date OLD: ' + case when d.[KYCDataModifyDate] is Null then 'Null' else 
	Convert(varchar, d.[KYCDataModifyDate]) End + 
	' NEW: ' + case when i.[KYCDataModifyDate] is Null then 'Null' else 
	Convert(varchar, i.[KYCDataModifyDate]) End
	ELSE '' End +
Case 
	WHEN ((i.[KYCOperator] Is Null And d.[KYCOperator] Is Not Null ) Or 
	(i.[KYCOperator] Is Not Null And d.[KYCOperator] Is Null ) Or 
	(i.[KYCOperator] != d.[KYCOperator])) 
	THEN '| KYC Operator OLD: ' + Convert(varchar, IsNull(d.[KYCOperator],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[KYCOperator],'Null'))
	ELSE '' End +
Case 
	WHEN ((i.[OnProbation] Is Null And d.[OnProbation] Is Not Null ) Or 
	(i.[OnProbation] Is Not Null And d.[OnProbation] Is Null ) Or 
	(i.[OnProbation] != d.[OnProbation])) 
	THEN '| OnProbation OLD: ' + Convert(varchar, IsNull(d.[OnProbation],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[OnProbation],'Null'))
	ELSE '' End +
	
Case 
	WHEN ((i.[ProbationReason] Is Null And d.[ProbationReason] Is Not Null ) Or 
	(i.[ProbationReason] Is Not Null And d.[ProbationReason] Is Null ) Or 
	(i.[ProbationReason] != d.[ProbationReason])) 
	THEN '| ProbationReason OLD: ' + Convert(varchar, IsNull(d.[ProbationReason],'Null')) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ProbationReason],'Null'))
	ELSE '' End +
	

Case 
	WHEN ((i.[ProbationStartDate] Is Null And d.[ProbationStartDate] Is Not Null ) Or 
	(i.[ProbationStartDate] Is Not Null And d.[ProbationStartDate] Is Null ) Or 
	(i.[ProbationStartDate] != d.[ProbationStartDate])) 
	THEN '| ReverificationDate(ProbationStartDate) OLD: ' + Convert(varchar, IsNull(d.[ProbationStartDate], 0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ProbationStartDate], 0))
	ELSE '' End +

Case 
	WHEN ((i.[ProbationEndDate] Is Null And d.[ProbationEndDate] Is Not Null ) Or 
	(i.[ProbationEndDate] Is Not Null And d.[ProbationEndDate] Is Null ) Or 
	(i.[ProbationEndDate] != d.[ProbationEndDate])) 
	THEN '| ProbationEndDate OLD: ' + Convert(varchar, IsNull(d.[ProbationEndDate], 0)) + 
	' NEW: ' + Convert(varchar, IsNull(i.[ProbationEndDate], 0))
	ELSE '' End 

	From Inserted i inner merge join deleted d on i.Id = d.Id
End
GO


