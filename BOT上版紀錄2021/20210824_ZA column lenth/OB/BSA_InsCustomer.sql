USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_InsCustomer]    Script Date: 8/24/2021 6:08:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER Procedure [dbo].[BSA_InsCustomer] ( @id as ObjectId, @parent as ObjectId, 
					@name as varchar(100), @dBA as varchar(40), 
					@secCode as varchar(10), @address as Address, 
					@city as City, @state as State, @zip as Zip, 
					@country as varchar(11), @telephone as varchar(20), 
					@email as varchar(60), @tIN as varchar(20), 
                                        @LicenseNo As Varchar(35), @LicenseState As State,
					@passportNo as varchar(20), @dOB as GenericDate, 
					@typeOfBusiness as varchar(80),
					@sourceOfFunds as varchar(80), @accountOfficer as varchar(40),
					@acctOffTel as varchar(20), @acctOffEmail as varchar(60),
					@compOfficer as varchar(40), @compOffTel as varchar(20),
					@compOffEmail as varchar(60), @idList as varchar(255),
					@notes as varchar(2000), @exemptionStatus as varchar(11),
					@lastActReset as GenericTime,
					@lastReview as GenericTime, @lastReviewOper as varchar(11),
					@ownerBranch varchar(11), @ownerDept varchar(11), @ownerOper varchar(11), 
					@riskClass varchar(11), @createOper as varchar(11),
					@countryOfOrigin varchar(11), @countryOfIncorp varchar(11), 
					@OffshoreCorp int, @BearerShares bit, @NomeDePlume bit,
					@Type varchar(11),
					@Resident Bit, @BusRelationNature VarChar(40),
					@PrevRelations VarChar(40), 
					@PEP Bit, @PoliticalPos VarChar(40), @FEP Bit, @MSB Bit,
					@CorrBankRelation bit, 
					@Sex Char(1), @ShellBank Bit, @HighRiskRespBank Bit,
					@OffshoreBank Bit, @PayThroughAC Bit, 
					@RegulatedAffiliate Bit, @USPerson VarChar(40),
					@RFCalculate Bit, @Status Int, @ReviewDate DateTime,
					@CTRAmt money, @User1 varchar(40), @User2 varchar(40),
					@User3 varchar(40), @User4 varchar(40), 
					@User5 varchar(40),
					@SwiftTID varchar(11), @embassy bit, 
					@ForeignGovt bit, @charityOrg bit, @DoCIP bit,
					@Prospect bit, @AssetSize money, @Income money,
					@IndOrBusType char(1),
					@ProfiledCountries Varchar(4000), 
					@OnProbation Bit = 0, 
					@ProbationReason varchar(11) = NULL, 
					@ProbationStartDate as GenericDate,
					@ProbationEndDate as GenericDate,
					@Closed Bit, @ClosedDate as GenericDate,
					@ClosedReason ObjectId = NULL, 
					@OpenDate as GenericDate,
					@CountryofResidence varchar(11),
					@CountryofCitizenship varchar(11),
					@KYCStatus int,
					@KYCOperator varchar(20),
					@KYCDataCreateDate datetime
)

   As

  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_InsCustomer
  else
    -- Already in a transaction
	save tran BSA_InsCustomer
  -- End standard stored procedure transaction header

  declare @stat int

  if @parent = '' Select @parent = @id
  
  insert into Customer ( 
	  Id, Parent, Name, 
	  DBA, SecCode, Address, 
	  City, State, Zip, 
	  Country, Telephone, Email,
	  TIN, LicenseNo, LicenseState, PassportNo, DOB, 
	  TypeOfBusiness, SourceOfFunds, 
	  AccountOfficer, AcctOffTel, 
	  AcctOffEmail, CompOfficer, 
	  CompOffTel, CompOffEmail, 
	  IdList, Notes, ExemptionStatus,
	  LastActReset, LastReview, 
	  LastReviewOper, CreateOper, CreateDate,
	  OwnerBranch, OwnerDept, OwnerOper, RiskClass,
	  CountryOfOrigin, CountryOfIncorp , OffshoreCorp,
	  BearerShares, NomeDePlume, Type,
	  Resident, BusRelationNature, PrevRelations,
	  PEP, PoliticalPos, FEP, MSB, CorrBankRelation,
	  Sex, ShellBank, HighRiskRespBank,
	  OffshoreBank, PayThroughAC, RegulatedAffiliate,
	  USPerson, RFCalculate, Status, ReviewDate,
	  CTRAmt, User1 , User2, User3, User4, User5,
	  SwiftTid, Embassy, ForeignGovt, Charityorg, DoCIP,
	  Prospect, AssetSize, Income, IndOrBusType, 
	  OnProbation, ProbationReason, ProbationStartDate,
	  ProbationEndDate,
	Closed,ClosedDate,ClosedReason, 
	OpenDate, 
	CountryOfResidence,CountryOfCitizenship,KYCDataCreateDate, KYCStatus, KYCOperator)
    values (
	  NullIf(@id, ''), NullIf(@parent, ''), NullIf(@name, ''), 
	  NullIf(@dBA, ''), NullIf(@secCode, ''), NullIf(@address, ''), 
	  NullIf(@city, ''), NullIf(@state, ''), NullIf(@zip, ''), 
	  NullIf(@country, ''), NullIf(@telephone, ''), NullIf(@email, ''),
	  NullIf(@tIN, ''), NullIf(@LicenseNo, ''),NullIf(@LicenseState, ''),
          NullIf(@passportNo, ''), NullIf(@dOB, 0), 
	  NullIf(@typeOfBusiness, ''), NullIf(@sourceOfFunds, ''), 
	  NullIf(@accountOfficer, ''), NullIf(@acctOffTel, ''), 
	  NullIf(@acctOffEmail, ''), NullIf(@compOfficer, ''), 
	  NullIf(@compOffTel, ''), NullIf(@compOffEmail, ''), 
	  NullIf(@idList, ''), NullIf(@notes, ''), NullIf(@exemptionStatus, ''),
	  NullIf(@lastActReset, 0), NullIf(@lastReview, 0), 
	  NullIf(@lastReviewOper, ''), NullIf(@createOper, ''), GetDate(),
	  NullIf(RTrim(@ownerBranch), ''), NullIf(RTrim(@ownerDept), ''), 
	  NullIf(RTrim(@ownerOper), ''), NullIf(RTrim(@riskClass), ''),
	  NullIf(RTrim(@countryOfOrigin), ''), NullIf(RTrim(@countryOfIncorp), ''), 
	  NullIf(@OffshoreCorp, 0), @BearerShares, @NomeDePlume, 
	  NullIf(RTrim(@Type), ''),
	  @Resident, NullIf(@BusRelationNature,''), NullIf(@PrevRelations,''),
	  @PEP, NullIf(@PoliticalPos,''), @FEP, @MSB,
	  @CorrBankRelation, 
	  NullIf(@Sex,''), @ShellBank, @HighRiskRespBank,
	  @OffshoreBank, @PayThroughAC, @RegulatedAffiliate,
	  NullIf(@USPerson,''), @RFCalculate, @Status, @ReviewDate,
	  @CTRAmt,
	  NullIf(@User1,''), NullIf(@User2,''), NullIf(@User3,''), 
	  NullIf(@User4,''), NullIf(@User5,''),
	  NUllIf(RTrim(@SwiftTID),''), @embassy, @ForeignGovt, @CharityOrg, @DoCIP,
	  @Prospect, NullIf(@AssetSize,0), NullIf(@Income,0), 
	  NullIf(RTrim(@IndOrBusType),''), @OnProbation, @ProbationReason,
	  NullIf(@ProbationStartDate, 0), NullIf(@ProbationEndDate, 0),
	@Closed,NullIf(@ClosedDate, 0),@ClosedReason, 
	NullIf(@OpenDate, 0), 
	NullIf(@CountryOfResidence, ''),NullIf(@CountryofCitizenship, ''),
	@KYCDataCreateDate, @KYCStatus, NullIf(@KYCOperator, '')
	 )

  Select @stat = @@error
  if @stat = 0 begin
	-- Insert Profile Countries (Comma delimited with a comma at the end)
	if len(@profiledCountries) > 0 begin
		exec BSA_InsCountryOwners @profiledCountries, @id, @createoper
		select stat = @@error
	end
  end

  if exists(select 1 from UnprocCustomer where id = rtrim(@id)) and @stat = 0
  begin
	delete UnprocCustomer where id=rtrim(@id)
  end

  select @stat = @stat + @@error


  -- Evaluate results of the transaction
  If @stat <> 0 begin
	rollback tran BSA_InsCustomer
	return @stat
  end
  If @trnCnt = 0
    commit tran BSA_InsCustomer
  return @stat
GO


