USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_InsParty]    Script Date: 11/11/2020 10:41:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER Procedure [dbo].[BSA_InsParty] ( @id as ObjectId, @parent as ObjectId, 
					@name as varchar(100), @dBA as varchar(40), 
					@secCode as varchar(10), @address as Address, 
					@city as City, @state as State, @zip as Zip, 
					@country as varchar(11), @telephone as varchar(20), 
					@email as varchar(60), @tIN as varchar(20), 
                    @LicenseNo As Varchar(35), @LicenseState As State,
					@passportNo as varchar(20), @dOB as GenericDate, 
					@typeOfBusiness as varchar(80),
					@sourceOfFunds as varchar(80), @accountOfficer as varchar(40),
					@acctOffTel as varchar(20), @acctOffEmail as varchar(60),
					@compOfficer as varchar(40), @compOffTel as varchar(20),
					@compOffEmail as varchar(60), @idList as varchar(255),
					@notes as varchar(2000), @exemptionStatus as varchar(11),
					@lastActReset as GenericTime,
					@lastReview as GenericTime, @lastReviewOper as varchar(11),
					@ownerBranch varchar(11), @ownerDept varchar(11), @ownerOper varchar(11), 
					@riskClass varchar(11), @createOper as varchar(11),
					@countryOfOrigin varchar(11), @countryOfIncorp varchar(11), 
					@OffshoreCorp int, @BearerShares bit, @NomeDePlume bit,
					@Type varchar(11),
					@Resident Bit, @BusRelationNature VarChar(40),
					@PrevRelations VarChar(40), 
					@PEP Bit, @PoliticalPos VarChar(40), @FEP Bit, @MSB Bit,
					@CorrBankRelation bit, 
					@Sex Char(1), @ShellBank Bit, @HighRiskRespBank Bit,
					@OffshoreBank Bit, @PayThroughAC Bit, 
					@RegulatedAffiliate Bit, @USPerson VarChar(40),
					@RFCalculate Bit, @Status Int, @ReviewDate DateTime,
					@CTRAmt money, @User1 varchar(40), @User2 varchar(40),
					@User3 varchar(40), @User4 varchar(40), 
					@User5 varchar(40),
					@SwiftTID varchar(11), @embassy bit, 
					@ForeignGovt bit, @charityOrg bit, @DoCIP bit,
					@Prospect bit, @AssetSize money, @Income money,
					@IndOrBusType char(1),
					@ProfiledCountries Varchar(4000), 
					@OnProbation Bit = 0, 
					@ProbationReason varchar(11) = NULL, 
					@ProbationStartDate as GenericDate,
					@ProbationEndDate as GenericDate,
					@Closed Bit, @ClosedDate as GenericDate,
					@ClosedReason ObjectId = NULL, 
					@OpenDate as GenericDate,
					@CountryofResidence varchar(11),
					@CountryofCitizenship varchar(11),
					@KYCStatus int,
					@KYCDataCreateDate datetime,
					@NamePrefix varchar(25), @FirstName varchar(50),
					@MiddleName varchar(50), @LastName varchar(50),      @NameSuffix varchar(25),
					@Alias varchar(50),      @PlaceofBirth varchar(50),  @MothersMaidenName varchar(50), @Introducer varchar(50),
					@YrsAtCurrAddress int,   @NoOfDependants int,        @Employername varchar(50),
					@JobTitle varchar(50),   @JobTime varchar(50),       @LengthOfEmp varchar(50),
					@IsAlien bit,            @CountryofDualCitizenship varchar(11), @SecondaryEmployer varchar(50),
					@OriginOfFunds varchar(50), @IsPersonalRelationship bit,
					@DateOfInception datetime,@LegalEntityType varchar(50),
					@NoofEmployees int,      @TotalAnnualSales money,    @ProductsSold varchar(255),
					@ServicesProvided varchar(255), @GeographicalAreasServed varchar(255), @IsPubliclyTraded bit,
					@TickerSymbol varchar(10), @Exchange varchar(50),    @IsPensionFund bit,
					@IsEndowmentFund bit,    @IsGovernmentSponsored bit, @IsSubsidiary bit,
					@IsForeignCorrBnk bit,   @RequireCashTrans bit,      @RequireForeignTrans bit,
					@RequireWireTrans bit,   @HasForeignBranches bit,    @ForeignBranchCountries varchar(255),
					@TypeofMonInstruments varchar(255), @URL varchar(255), @HasPatriotActCertification bit,
					@PatriotActCertExpiryDate datetime, @MoneyOrders bit,  @MORegistration varchar(10),
					@MORegConfirmationNo varchar(50),   @MOActingasAgentfor varchar(20), @MOMaxDailyAmtPerPerson money,
					@MOMonthlyAmt money,                @MOPercentIncome varchar(5),     @TravelersCheck bit,
					@TCRegistration varchar(10),        @TCRegConfirmationNo varchar(50),@TCActingasAgentfor varchar(20),
					@TCMaxDailyAmtPerPerson money,      @TCMonthlyAmt money,             @TCPercentIncome varchar(5),
					@MoneyTransmission bit,             @MTRegistration varchar(10),     @MTRegConfirmationNo varchar(50),
					@MTActingasAgentfor varchar(20),    @MTMaxDailyAmtPerPerson money,   @MTMonthlyAmt money,
					@MTPercentIncome varchar(5),        @CheckCashing bit,               @CCRegistration varchar(10),
					@CCRegConfirmationNo varchar(50),   @CCActingasAgentfor varchar(20), @CCMaxDailyAmtPerPerson money,
					@CCMonthlyAmt money,                @CCPercentIncome varchar(5),     @CurrencyExchange bit,
					@CERegistration varchar(10),        @CERegConfirmationNo varchar(50),@CEActingasAgentfor varchar(20),
					@CEMaxDailyAmtPerPerson money,      @CEMonthlyAmt money,             @CEPercentIncome varchar(5),
					@CurrencyDealing bit,               @CDRegistration varchar(10),     @CDRegConfirmationNo varchar(50),
					@CDActingasAgentfor varchar(20),    @CDMaxDailyAmtPerPerson money,   @CDMonthlyAmt money,
					@CDPercentIncome varchar(5),        @StoredValue bit,                @SVRegistration varchar(10),
					@SVRegConfirmationNo varchar(50),   @SVActingasAgentfor varchar(20), @SVMaxDailyAmtPerPerson money,
					@SVMonthlyAmt money,                @SVPercentIncome varchar(5),     @CDDUser1 varchar(40),         	
					@CDDUser2 varchar(40),		    @CDDUser3 varchar(40),           @CDDUser4 varchar(40),		
					@CDDUser5 varchar(40),         	    @CDDUser6 varchar(40),           @CDDUser7 varchar(40),		
					@CDDUser8 varchar(40),         	    @CDDUser9 varchar(40),           @CDDUser10 varchar(40),		
					@CDDUser11 varchar(40),             @CDDUser12 varchar(40),          @CDDUser13 varchar(40),		
					@CDDUser14 varchar(40),             @CDDUser15 varchar(40),          @CDDUser16 varchar(40),		
					@CDDUser17 varchar(40),             @CDDUser18 varchar(40),          @CDDUser19 varchar(40),
					@CDDUser20 varchar(40),             @CDDUser21 varchar(40),          @CDDUser22 varchar(40),		
					@CDDUser23 varchar(40),             @CDDUser24 varchar(40),          @CDDUser25 varchar(40),		
					@CDDUser26 varchar(40),             @CDDUser27 varchar(40),          @CDDUser28 varchar(40),		
					@CDDUser29 varchar(40),             @CDDUser30 varchar(40),          @CDDUser31 varchar(1000),		
					@CDDUser32 varchar(1000),            @CDDUser33 varchar(1000),         @CDDUser34 varchar(1000),		
					@CDDUser35 varchar(1000),            @CDDUser36 varchar(1000),         @CDDUser37 varchar(1000),		
					@CDDUser38 varchar(1000),            @CDDUser39 varchar(1000),         @CDDUser40 varchar(3000),
					@AreKYCFieldsPresent bit,
					--		Begin: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw
					@RelationshipWithBankAffl bit ,					-- Relationship with Bank Affiliate
					@PrivInvestmentCompany bit ,					-- Private Investiment Company
					@ResultsOfNegativePressSearch varchar(255)      -- Results of Negative Press Search	    
					--		End: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw	    					
)
  As

  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_InsParty
  else
    -- Already in a transaction
	save tran BSA_InsParty
  -- End standard stored procedure transaction header

  declare @stat int

  if @parent = '' Select @parent = @id
  
  insert into Customer ( 
	  Id, Parent, Name, 
	  DBA, SecCode, Address, 
	  City, State, Zip, 
	  Country, Telephone, Email,
	  TIN, LicenseNo, LicenseState, PassportNo, DOB, 
	  TypeOfBusiness, SourceOfFunds, 
	  AccountOfficer, AcctOffTel, 
	  AcctOffEmail, CompOfficer, 
	  CompOffTel, CompOffEmail, 
	  IdList, Notes, ExemptionStatus,
	  LastActReset, LastReview, 
	  LastReviewOper, CreateOper, CreateDate,
	  OwnerBranch, OwnerDept, OwnerOper, RiskClass,
	  CountryOfOrigin, CountryOfIncorp , OffshoreCorp,
	  BearerShares, NomeDePlume, Type,
	  Resident, BusRelationNature, PrevRelations,
	  PEP, PoliticalPos, FEP, MSB, CorrBankRelation,
	  Sex, ShellBank, HighRiskRespBank,
	  OffshoreBank, PayThroughAC, RegulatedAffiliate,
	  USPerson, RFCalculate, Status, ReviewDate,
	  CTRAmt, User1 , User2, User3, User4, User5,
	  SwiftTid, Embassy, ForeignGovt, Charityorg, DoCIP,
	  Prospect, AssetSize, Income, IndOrBusType, 
	  OnProbation, ProbationReason, ProbationStartDate,
	  ProbationEndDate,
	  Closed,ClosedDate,ClosedReason, 
	  OpenDate, 
	  CountryOfResidence,CountryOfCitizenship,KYCDataCreateDate, KYCStatus)
  values (
	  NullIf(@id, ''), NullIf(@parent, ''), NullIf(@name, ''), 
	  NullIf(@dBA, ''), NullIf(@secCode, ''), NullIf(@address, ''), 
	  NullIf(@city, ''), NullIf(@state, ''), NullIf(@zip, ''), 
	  NullIf(@country, ''), NullIf(@telephone, ''), NullIf(@email, ''),
	  NullIf(@tIN, ''), NullIf(@LicenseNo, ''),NullIf(@LicenseState, ''),
          NullIf(@passportNo, ''), NullIf(@dOB, 0), 
	  NullIf(@typeOfBusiness, ''), NullIf(@sourceOfFunds, ''), 
	  NullIf(@accountOfficer, ''), NullIf(@acctOffTel, ''), 
	  NullIf(@acctOffEmail, ''), NullIf(@compOfficer, ''), 
	  NullIf(@compOffTel, ''), NullIf(@compOffEmail, ''), 
	  NullIf(@idList, ''), NullIf(@notes, ''), NullIf(@exemptionStatus, ''),
	  NullIf(@lastActReset, 0), NullIf(@lastReview, 0), 
	  NullIf(@lastReviewOper, ''), NullIf(@createOper, ''), GetDate(),
	  NullIf(RTrim(@ownerBranch), ''), NullIf(RTrim(@ownerDept), ''), 
	  NullIf(RTrim(@ownerOper), ''), NullIf(RTrim(@riskClass), ''),
	  NullIf(RTrim(@countryOfOrigin), ''), NullIf(RTrim(@countryOfIncorp), ''), 
	  NullIf(@OffshoreCorp, 0), @BearerShares, @NomeDePlume, 
	  NullIf(RTrim(@Type), ''),
	  @Resident, NullIf(@BusRelationNature,''), NullIf(@PrevRelations,''),
	  @PEP, NullIf(@PoliticalPos,''), @FEP, @MSB,
	  @CorrBankRelation, 
	  NullIf(@Sex,''), @ShellBank, @HighRiskRespBank,
	  @OffshoreBank, @PayThroughAC, @RegulatedAffiliate,
	  NullIf(@USPerson,''), @RFCalculate, @Status, @ReviewDate,
	  @CTRAmt,
	  NullIf(@User1,''), NullIf(@User2,''), NullIf(@User3,''), 
	  NullIf(@User4,''), NullIf(@User5,''),
	  NUllIf(RTrim(@SwiftTID),''), @embassy, @ForeignGovt, @CharityOrg, @DoCIP,
	  @Prospect, NullIf(@AssetSize,0), NullIf(@Income,0), 
	  NullIf(RTrim(@IndOrBusType),''), @OnProbation, @ProbationReason,
	  NullIf(@ProbationStartDate, 0), NullIf(@ProbationEndDate, 0),
	  @Closed,NullIf(@ClosedDate, 0),@ClosedReason, 
	  NullIf(@OpenDate, 0), 
	  NullIf(@CountryOfResidence, ''),NullIf(@CountryofCitizenship, ''),
	  @KYCDataCreateDate, @KYCStatus 
  )

  Select @stat = @@error
  if @stat = 0 begin
	-- Insert Profile Countries (Comma delimited with a comma at the end)
	if len(@profiledCountries) > 0 begin
		exec BSA_InsCountryOwners @profiledCountries, @id, @createoper
		select stat = @@error
	end
  end

  -- Evaluate results of the transaction
  If @stat <> 0 begin
	rollback tran BSA_InsParty
	return @stat
  end
  
  If @AreKYCFieldsPresent = 1 begin
  
  Insert Into KYCData (	CustomerId, 	NamePrefix ,		FirstName ,
  		MiddleName ,		LastName ,		NameSuffix ,
  		Alias ,			PlaceofBirth ,		MothersMaidenName ,
  		Introducer,		YrsAtCurrAddress,	NoOfDependants ,
  		EmployerName,		JobTitle ,		JobTime , 
  		LengthOfEmp,		IsAlien ,		CountryofDualCitizenship,
  		SecondaryEmployer ,	OriginOfFunds ,		IsPersonalRelationship ,
  		DateOfInception,	LegalEntityType,	NoofEmployees ,
  		TotalAnnualSales ,	ProductsSold ,		ServicesProvided ,
  		GeographicalAreasServed,IsPubliclyTraded,	TickerSymbol ,
  		Exchange ,		IsPensionFund ,		IsEndowmentFund ,
  		IsGovernmentSponsored ,	IsSubsidiary ,		IsForeignCorrBnk ,
  		RequireCashTrans ,	RequireForeignTrans , 	RequireWireTrans ,
  		HasForeignBranches ,	ForeignBranchCountries ,TypeofMonInstruments ,
  		URL ,			HasPatriotActCertification ,PatriotActCertExpiryDate ,
  		MoneyOrders ,		MORegistration ,	MORegConfirmationNo ,
  		MOActingasAgentfor ,	MOMaxDailyAmtPerPerson ,MOMonthlyAmt ,
  		MOPercentIncome ,	TravelersCheck ,	TCRegistration ,
  		TCRegConfirmationNo ,	TCActingasAgentfor ,	TCMaxDailyAmtPerPerson ,
  		TCMonthlyAmt ,		TCPercentIncome ,	MoneyTransmission ,
  		MTRegistration ,	MTRegConfirmationNo,	MTActingasAgentfor ,
  		MTMaxDailyAmtPerPerson ,MTMonthlyAmt ,		MTPercentIncome ,
  		CheckCashing ,		CCRegistration ,	CCRegConfirmationNo ,
  		CCActingasAgentfor ,	CCMaxDailyAmtPerPerson ,CCMonthlyAmt ,
  		CCPercentIncome ,	CurrencyExchange ,	CERegistration ,
  		CERegConfirmationNo ,	CEActingasAgentfor ,	CEMaxDailyAmtPerPerson ,
  		CEMonthlyAmt ,		CEPercentIncome ,	CurrencyDealing ,
  		CDRegistration ,	CDRegConfirmationNo ,	CDActingasAgentfor ,
  		CDMaxDailyAmtPerPerson ,CDMonthlyAmt ,		CDPercentIncome ,
  		StoredValue ,		SVRegistration ,	SVRegConfirmationNo ,
  		SVActingasAgentfor ,	SVMaxDailyAmtPerPerson ,SVMonthlyAmt,
  		SVPercentIncome ,	CDDUser1,		CDDUser2,
  		CDDUser3,		CDDUser4,		CDDUser5,
  		CDDUser6,		CDDUser7,		CDDUser8,
  		CDDUser9,		CDDUser10,		CDDUser11,
  		CDDUser12,		CDDUser13,		CDDUser14,
  		CDDUser15,		CDDUser16,		CDDUser17,
  		CDDUser18,		CDDUser19,		CDDUser20,
  		CDDUser21,		CDDUser22,		CDDUser23,
  		CDDUser24,		CDDUser25,		CDDUser26,
  		CDDUser27,		CDDUser28,		CDDUser29,
  		CDDUser30,		CDDUser31,		CDDUser32,
  		CDDUser33,		CDDUser34,		CDDUser35,
  		CDDUser36,		CDDUser37,		CDDUser38,
  		CDDUser39,		CDDUser40,		CreateOper,
  		CreateDate,
		--		Begin: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw
		RelationshipWithBankAffl,		-- Relationship with Bank Affiliate
		PrivInvestmentCompany,			-- Private Investiment Company
		ResultsOfNegativePressSearch    -- Results of Negative Press Search	    
		--		End: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw	    					  		
  	) 
  	values (RTrim(@id),NullIf(RTrim(@nameprefix),''), NullIf(RTrim(@firstname),''), 
  		NullIf(RTrim(@middlename),''),	NullIf(RTrim(@lastname),''),	NullIf(RTrim(@namesuffix),''),
  		NullIf(RTrim(@alias),''),	NullIf(RTrim(@placeofbirth),''),NullIf(RTrim(@mothersmaidenname),''),
  		NullIf(RTrim(@introducer),''),	NullIf(@yrsatcurraddress,0),	NullIf(@noofdependants,0),
  		NullIf(RTrim(@EmployerName),''),NullIf(RTrim(@jobtitle),''),	NullIf(RTrim(@jobtime),''),
  		NullIF(RTRIM(@LengthOfEmp),''),	@isalien,			NullIf(RTrim(@countryofdualcitizenship),''),
  		NullIf(RTrim(@secondaryemployer),''),NullIf(RTrim(@OriginOfFunds),''),@ispersonalrelationship,
  		@dateofinception,		NullIf(RTrim(@legalentitytype),''),@noofemployees,
  		@totalannualsales,		NullIf(RTrim(@productssold), ''),NullIf(RTrim(@servicesprovided), ''),
  		NullIf(RTrim(@geographicalareasserved), ''),@IsPubliclyTraded,	NullIf(RTrim(@tickersymbol), ''),
  		NullIf(RTrim(@exchange), ''),	@ispensionfund,			@isendowmentfund,
  		@isgovernmentsponsored,		@issubsidiary,			@isforeigncorrbnk,
  		@requirecashtrans,		@requireforeigntrans,		@requirewiretrans,
  		@hasforeignbranches,		NullIf(RTrim(@ForeignBranchCountries), ''), NullIf(RTrim(@TypeofMonInstruments), ''), 
  		NullIf(RTrim(@URL), ''),	@HasPatriotActCertification,	@PatriotActCertExpiryDate,
  		@moneyorders,			NullIf(RTrim(@MORegistration), ''), NullIf(RTrim(@MORegConfirmationNo), ''), 
  		NullIf(RTrim(@MOActingasAgentfor), ''),@MOMaxDailyAmtPerPerson,	@MOMonthlyAmt,
  		NullIf(RTrim(@MOPercentIncome), ''),@TravelersCheck ,		NullIf(RTrim(@TCRegistration), '') ,
  		NullIf(RTrim(@TCRegConfirmationNo), '') ,NullIf(RTrim(@TCActingasAgentfor), '') ,@TCMaxDailyAmtPerPerson ,
  		@TCMonthlyAmt,			NullIf(RTrim(@TCPercentIncome), ''),@MoneyTransmission,
  		NullIf(RTrim(@MTRegistration), '') ,NullIf(RTrim(@MTRegConfirmationNo), '') ,NullIf(RTrim(@MTActingasAgentfor), '') ,
  		@MTMaxDailyAmtPerPerson ,	@MTMonthlyAmt ,			NullIf(RTrim(@MTPercentIncome), '') ,
  		@CheckCashing ,			NullIf(RTrim(@CCRegistration), '') ,NullIf(RTrim(@CCRegConfirmationNo), '') ,
  		NullIf(RTrim(@CCActingasAgentfor), ''), @CCMaxDailyAmtPerPerson ,@CCMonthlyAmt ,
  		NullIf(RTrim(@CCPercentIncome), '') ,@CurrencyExchange ,	NullIf(RTrim(@CERegistration), '') ,
  		NullIf(RTrim(@CERegConfirmationNo), ''),NullIf(RTrim(@CEActingasAgentfor), '') ,@CEMaxDailyAmtPerPerson ,
  		@CEMonthlyAmt ,			NullIf(RTrim(@CEPercentIncome), ''),@CurrencyDealing ,
  		NullIf(RTrim(@CDRegistration), '') ,NullIf(RTrim(@CDRegConfirmationNo), '') ,NullIf(RTrim(@CDActingasAgentfor), ''),
  		@CDMaxDailyAmtPerPerson ,	@CDMonthlyAmt,			NullIf(RTrim(@CDPercentIncome), '') ,
  		@StoredValue ,			NullIf(RTrim(@SVRegistration), '') ,NullIf(RTrim(@SVRegConfirmationNo), '') ,
  		NullIf(RTrim(@SVActingasAgentfor), ''), @SVMaxDailyAmtPerPerson , @SVMonthlyAmt,
  		NullIf(RTrim(@SVPercentIncome), '') , NullIf(RTrim(@CDDUser1) ,''), NullIf(RTrim(@CDDUser2) ,''),
  		NullIf(RTrim(@CDDUser3) ,''),	NullIf(RTrim(@CDDUser4) ,''),	NullIf(RTrim(@CDDUser5) ,''),         
  		NullIf(RTrim(@CDDUser6) ,''),    NullIf(RTrim(@CDDUser7) ,''),	NullIf(RTrim(@CDDUser8) ,''),         
  		NullIf(RTrim(@CDDUser9) ,''),    NullIf(RTrim(@CDDUser10) ,''),	NullIf(RTrim(@CDDUser11) ,''),         
  		NullIf(RTrim(@CDDUser12) ,''),   NullIf(RTrim(@CDDUser13) ,''),	NullIf(RTrim(@CDDUser14) ,''),         
  		NullIf(RTrim(@CDDUser15) ,''),   NullIf(RTrim(@CDDUser16) ,''),	NullIf(RTrim(@CDDUser17) ,''),         
  		NullIf(RTrim(@CDDUser18) ,''),   NullIf(RTrim(@CDDUser19) ,''),	NullIf(RTrim(@CDDUser20) ,''),         
  		NullIf(RTrim(@CDDUser21) ,''),   NullIf(RTrim(@CDDUser22) ,''),	NullIf(RTrim(@CDDUser23) ,''),         
  		NullIf(RTrim(@CDDUser24) ,''),   NullIf(RTrim(@CDDUser25) ,''),	NullIf(RTrim(@CDDUser26) ,''),         
  		NullIf(RTrim(@CDDUser27) ,''),   NullIf(RTrim(@CDDUser28) ,''),	NullIf(RTrim(@CDDUser29) ,''),         
  		NullIf(RTrim(@CDDUser30) ,''),   NullIf(RTrim(@CDDUser31) ,''),	NullIf(RTrim(@CDDUser32) ,''),         
  		NullIf(RTrim(@CDDUser33) ,''),   NullIf(RTrim(@CDDUser34) ,''),	NullIf(RTrim(@CDDUser35) ,''),         
  		NullIf(RTrim(@CDDUser36) ,''),   NullIf(RTrim(@CDDUser37) ,''),	NullIf(RTrim(@CDDUser38) ,''),         
  		NullIf(RTrim(@CDDUser39) ,''),   NullIf(RTrim(@CDDUser40) ,''),	RTrim(@CreateOper),
		GetDate(),
		--		Begin: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw
		@RelationshipWithBankAffl,							-- Relationship with Bank Affiliate
		@PrivInvestmentCompany,								-- Private Investiment Company
		NullIf(RTrim(@ResultsOfNegativePressSearch) ,'')	-- Results of Negative Press Search	    
		--		End: Adding 3 Customer New Fields for any future Banks' PBSA Database of PCS Project use  --	yw	    					  				
  )  
		
		Select @stat = @@error

  if exists(select 1 from UnprocCustomer where id = rtrim(@id)) and @stat = 0
  begin
	delete UnprocCustomer where id=rtrim(@id)
	delete UnprocKYCData where customerid=rtrim(@id)
  end

  select @stat = @stat + @@error

		
		 -- Evaluate results of the transaction
		If @stat <> 0 begin
		 rollback tran BSA_InsParty
		 return @stat
  		end
  
  End
  
  
  
  If @trnCnt = 0
    commit tran BSA_InsParty
  return @stat
GO


