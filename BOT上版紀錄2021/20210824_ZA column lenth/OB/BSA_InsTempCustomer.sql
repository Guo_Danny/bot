USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_InsTempCustomer]    Script Date: 8/25/2021 2:17:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-------------------------------------------------------------------------------

ALTER Procedure [dbo].[BSA_InsTempCustomer] ( @id as ObjectId, @parent as ObjectId, 
     @name as LongName1, @dBA as LongName, 
     @secCode as varchar(10), @address as Address, 
     @city as City, @state as State, @zip as Zip, 
     @country as SCode, @telephone as varchar(20), 
     @email as varchar(60), @tIN as varchar(20), 
                                        @LicenseNo As Varchar(35), @LicenseState As State,
     @passportNo as varchar(20), @dOB as GenericDate, 
     @typeOfBusiness as varchar(80),
     @sourceOfFunds as varchar(80), @accountOfficer as LongName,
     @acctOffTel as varchar(20), @acctOffEmail as varchar(60),
     @compOfficer as LongName, @compOffTel as varchar(20),
     @compOffEmail as varchar(60), @idList as varchar(255),
     @notes as varchar(2000), @exemptionStatus as SCode,
     @lastActReset as GenericTime,
     @lastReview as GenericTime, @lastReviewOper as SCode,
     @ownerBranch SCode, @ownerDept SCode, @ownerOper SCode, 
     @riskClass SCode, @createOper as SCode,
     @countryOfOrigin SCode, @countryOfIncorp SCode, 
     @OffshoreCorp int, @BearerShares bit, @NomeDePlume bit,
     @Type SCode,
     @Resident Bit, @BusRelationNature VarChar(40),
     @PrevRelations VarChar(40), 
     @PEP Bit, @PoliticalPos VarChar(40), @FEP Bit, @MSB Bit,
     @CorrBankRelation bit, 
     @Sex Char(1), @ShellBank Bit, @HighRiskRespBank Bit,
     @OffshoreBank Bit, @PayThroughAC Bit, 
     @RegulatedAffiliate Bit, @USPerson VarChar(40),
     @RFCalculate Bit, @Status Int, @ReviewDate DateTime,
     @CTRAmt money, @User1 varchar(40), @User2 varchar(40),
     @User3 varchar(40), @User4 varchar(40), 
     @User5 varchar(40),
     @SwiftTID varchar(11), @embassy bit, 
     @ForeignGovt bit, @charityOrg bit, @DoCIP bit,
     @Prospect bit, @AssetSize money, @Income money,
     @IndOrBusType char(1), @OnProbation Bit, 
     @ProbationReason SCode, @ProbationStartDate as GenericDate,
     @ProbationEndDate as GenericDate,
     @Closed Bit, @ClosedDate as GenericDate ,@ClosedReason objectid , 
     @OpenDate as GenericDate ,
     @CountryofResidence SCode,@CountryofCitizenship SCode)
 
   As
 
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount -- Save the current trancount
  If @trnCnt = 0
 -- Transaction has not begun
 begin tran BSA_InsTempCustomer
  else
    -- Already in a transaction
 save tran BSA_InsTempCustomer
  -- End standard stored procedure transaction header
 
  declare @stat int
 
  if @parent = '' Select @parent = @id
  
  insert into TempCustomer ( 
   Id, Parent, Name, 
   DBA, SecCode, Address, 
   City, State, Zip, 
   Country, Telephone, Email,
   TIN, LicenseNo, LicenseState, PassportNo, DOB, 
   TypeOfBusiness, SourceOfFunds, 
   AccountOfficer, AcctOffTel, 
   AcctOffEmail, CompOfficer, 
   CompOffTel, CompOffEmail, 
   IdList, Notes, ExemptionStatus,
   LastActReset, LastReview, 
   LastReviewOper, CreateOper, CreateDate,
   OwnerBranch, OwnerDept, OwnerOper, RiskClass,
   CountryOfOrigin, CountryOfIncorp , OffshoreCorp,
   BearerShares, NomeDePlume, Type,
   Resident, BusRelationNature, PrevRelations,
   PEP, PoliticalPos, FEP, MSB, CorrBankRelation,
   Sex, ShellBank, HighRiskRespBank,
   OffshoreBank, PayThroughAC, RegulatedAffiliate,
   USPerson, RFCalculate, Status, ReviewDate,
   CTRAmt, User1 , User2, User3, User4, User5,
   SwiftTid, Embassy, ForeignGovt, Charityorg, DoCIP,
   Prospect, AssetSize, Income, IndOrBusType,
 OnProbation, ProbationReason, ProbationStartDate,
 ProbationEndDate,
 Closed,ClosedDate,ClosedReason, 
 OpenDate, 
 CountryOfResidence,CountryOfCitizenship)
    values (
   NullIf(@id, ''), NullIf(@parent, ''), NullIf(@name, ''), 
   NullIf(@dBA, ''), NullIf(@secCode, ''), NullIf(@address, ''), 
   NullIf(@city, ''), NullIf(@state, ''), NullIf(@zip, ''), 
   NullIf(@country, ''), NullIf(@telephone, ''), NullIf(@email, ''),
   NullIf(@tIN, ''), NullIf(@LicenseNo, ''),NullIf(@LicenseState, ''),
          NullIf(@passportNo, ''), NullIf(@dOB, 0), 
   NullIf(@typeOfBusiness, ''), NullIf(@sourceOfFunds, ''), 
   NullIf(@accountOfficer, ''), NullIf(@acctOffTel, ''), 
   NullIf(@acctOffEmail, ''), NullIf(@compOfficer, ''), 
   NullIf(@compOffTel, ''), NullIf(@compOffEmail, ''), 
   NullIf(@idList, ''), NullIf(@notes, ''), NullIf(@exemptionStatus, ''),
   NullIf(@lastActReset, 0), NullIf(@lastReview, 0), 
   NullIf(@lastReviewOper, ''), NullIf(@createOper, ''), GetDate(),
   NullIf(RTrim(@ownerBranch), ''), NullIf(RTrim(@ownerDept), ''), 
   NullIf(RTrim(@ownerOper), ''), NullIf(RTrim(@riskClass), ''),
   NullIf(RTrim(@countryOfOrigin), ''), NullIf(RTrim(@countryOfIncorp), ''), 
   NullIf(@OffshoreCorp, 0), @BearerShares, @NomeDePlume, 
   NullIf(RTrim(@Type), ''),
   @Resident, NullIf(@BusRelationNature,''), NullIf(@PrevRelations,''),
   @PEP, NullIf(@PoliticalPos,''), @FEP, @MSB,
   @CorrBankRelation, 
   NullIf(@Sex,''), @ShellBank, @HighRiskRespBank,
   @OffshoreBank, @PayThroughAC, @RegulatedAffiliate,
   NullIf(@USPerson,''), @RFCalculate, @Status, @ReviewDate,
   @CTRAmt,
   NullIf(@User1,''), NullIf(@User2,''), NullIf(@User3,''), 
   NullIf(@User4,''), NullIf(@User5,''),
   NUllIf(RTrim(@SwiftTID),''), @embassy, @ForeignGovt, @CharityOrg, @DoCIP,
   @Prospect, NullIf(@AssetSize,0), NullIf(@Income,0),
   NUllIf(RTrim(@IndOrBusType),''), @OnProbation, NullIf(@ProbationReason, ''),
   NullIf(@ProbationStartDate, 0), NullIf(@ProbationEndDate, 0),
   @Closed,NullIf(@ClosedDate, 0), NullIf(@ClosedReason,'') , 
   NullIf(@OpenDate, 0), 
   NullIf(@CountryOfResidence, ''),NullIf(@CountryofCitizenship, '') )
  Select @stat = @@error
 
  -- Evaluate results of the transaction
  If @stat <> 0 begin
 rollback tran BSA_InsTempCustomer
 return @stat
  end
  If @trnCnt = 0
    commit tran BSA_InsTempCustomer
  return @stat
GO


