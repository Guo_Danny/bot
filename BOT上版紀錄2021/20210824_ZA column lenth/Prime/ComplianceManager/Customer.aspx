<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Customer.aspx.vb" Inherits="CMBrowser.Customer" validateRequest="false" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="GoWeb" Namespace="Northwoods.GoWeb" Assembly="Northwoods.GoWeb" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucActivityList" Src="ucActivityList.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucSupportingInfo" Src="ucSupportingInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucItemSelector" Src="ucItemSelector.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNote" Src="ucNote.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//Dtd HTML 4.0 transitional//EN">
<html>
	<head>
		<title>Customer</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="Styles.css" type="text/css" rel="stylesheet"/>
		<style type="text/css">.tgridheader { BORDER-RIGHT: white outset; PADDING-RIGHT: 0px; BORDER-TOP: white outset; PADDING-LEFT: 0px; FONT-WEIGHT: bold; FONT-SIZE: 12px; PADDING-BOTTOM: 2px; BORDER-LEFT: white outset; COLOR: white; PADDING-TOP: 2px; BORDER-BOTTOM: white outset; BACKGROUND-COLOR: #990000; TEXT-ALIGN: left }
	.lblgridcell { FONT-SIZE: 11px; Font-Names: Verdana, Arial, Helvetica, sans-serif }
	.grdHeader { FONT-WEIGHT: bold; FONT-SIZE: 11px; Font-Names: Verdana, Arial, Helvetica, sans-serif }
		</style>
		<script language="JavaScript" src="js/nav.js" type="text/javascript"></script>
		<script language="JavaScript" src="js/Customer.js" type="text/javascript"></script>
		<script language="JavaScript" src="js/GoWeb.js" type="text/javascript"></script>
		<script language="JavaScript" type="text/javascript">
		    //Result of Negative Press Search Character count script
             function JStextCount(text, maxlimit) {

            if (text.value.length < maxlimit)
                return true;
            else {
                if ((event.keyCode >= 37 && event.keyCode <= 40) || (event.keyCode == 8))
                    event.returnValue = true;

                else
                    event.returnValue = false;
            }
        }
			function LocaleDate(ctrlID)
			    {
			        var ctrlName=ctrlID;
			        var msg;
			            
			            switch(ctrlName)
			                {
			                    case "IDcell5" :
			                    msg="* ID Issue date should be in proper format, Invalid date value. <br>";
			                    break;
			                    case "IDcell6" :
			                    msg="* ID Expiry date should be in proper format, Invalid date value. <br>";
			                    break;
			                }
			                
			                try
			                    {
			                        var varDate=document.forms[0].item(ctrlName).value;
			                        var varIDcell=Date.parseLocale(varDate, "d");
			                        if (varIDcell==null || varIDcell=="")
			                            {
			                            document.getElementById("IDWarning").innerHTML=msg;
			                            }
			                        else
			                            {
			                             var varIDcell=String.localeFormat("{0:d}", varIDcell);
			                             document.forms[0].item(ctrlName).value=varIDcell;
			                             document.getElementById("IDWarning").innerHTML="";
			                            }
			                    }
			                    catch(e)
			                    {
			                    
			                    }
			    }
 
			function goDblClick(e, id) {
				
				goInfo = goFindInfoAt(goDX, goDY, id);
				var newQueryString;
				var pageName = new String("");
				var pageName2;
				var param;
				var partyId;
				var relatedPartyId;
				var relationship;
				
				if (goInfo == null) return;
				pageName = goInfo.PageName;				
				
				if (pageName == "Relationship") {
					//pagename is "Relationship"
					param=getwindowparam(4);
					partyId = goInfo.PartyId;
					relatedPartyId =  goInfo.RelatedPartyId;
					relationship = goInfo.Relationship;					
					if (goInfo.MultiRelations == "1") {
						//pagename is "PartyRelations" and pageName2 is empty
						pageName = "PartyRelations";							
					}	
					// passed the pagename as a parameter in the framed querystring 
					newQueryString = pageName + '.aspx?PartyId=' + partyId + '&RelatedPartyId=' + 
							relatedPartyId + '&relationship=' + relationship + '&ActionID=2&RowNum=0&NoScroll=1';								
					var nw=window.open(newQueryString, pageName, param);
				}
				else  {
					//pagename will hold the current page name
					pageName2= filterNum(pageName +  String( goInfo.PartyID));					
					param=getwindowparam(1);
					newQueryString = pageName + '.aspx?RecordId=' + goInfo.PartyID + '&ActionID=2&RowNum=0&NoScroll=1';								
					var nw=window.open(newQueryString, pageName, param);
					}
			
				return;
				if (goSt == 0) return;
				goSt = 0;
				if (goTm) clearTimeout(goTm);
				goImg = goFindImg(id);
				goUX = goMouseX(e);
				goUY = goMouseY(e);
				goVX = e.clientX;
				goVY = e.clientY;
				if (goMouseHasMoved()) {
					if (!goNoClick(0, id)) return;
				} else {
					if (!goDoClick(0, 2, id)) return;
				}
				goPost(id, goQuery(e) + '&dblclick=1');
			}
			
			function CheckProbationFields(sender, args)
			{
				args.isvalid = false;
			}
			
			function Save_Click(sender, args)
			{
				args.isvalid = false;
			}
			function ChangeDate()
			{
			    try
			    {	
				    var CurrDate = new Date();
				    var strRiskClassValue, strNameValuePair;
				    var dtEndDate, intProbationDays;
				    strNameValuePair = document.forms[0].item("hdnRiskClass").value;
				    strRiskClassValue = document.forms[0].item("ddlRiskClass").value;
				    intProbationDays = getProbationDays(strNameValuePair, strRiskClassValue);
				    dtEndDate = DateAdd("d", intProbationDays, CurrDate);
				    document.forms[0].item("txtProbationEndDate").value = parseInt(dtEndDate.getMonth()+ 1 ) + '/' + dtEndDate.getdate() + '/' + dtEndDate.getFullYear();
				}
				catch(ex){}
			}
			function getProbationDays(xmltext, code)
			{
				if (code.length == 0) return 0;
				var startindex = xmltext.indexOf(code) + code.length + 1;
				var endindex = xmltext.indexOf(';', startindex);
				return parseInt(xmltext.substring(startindex,endindex));
			}
		function DateAdd(timeU,byMany,dateObj) {
			var millisecond=1;
			var second=millisecond*1000;
			var minute=second*60;
			var hour=minute*60;
			var day=hour*24;
			var year=day*365;

			var newDate;
			var dVal=dateObj.valueOf();
			switch(timeU) {
				case "ms": newDate=new Date(dVal+millisecond*byMany); break;
				case "s": newDate=new Date(dVal+second*byMany); break;
				case "mi": newDate=new Date(dVal+minute*byMany); break;
				case "h": newDate=new Date(dVal+hour*byMany); break;
				case "d": newDate=new Date(dVal+day*byMany); break;
				case "y": newDate=new Date(dVal+year*byMany); break;
			}
			return newDate;
		}
		function PopupWindow(mode) {
				var qstring;
				
				switch(mode) {
				case 1 : //Add
					qstring = "Document.aspx?ActionID=1&RowNum=0&NoScroll=1"
					break;
				case 2 : //Display
					if ( document.getElementById("txtdocument").value == "" ) 
						return;
					qstring =  "Document.aspx?"+ "RecordID="+ document.getElementById("txtdocument").value + 
							 "&ActionID=2&RowNum=0&NoScroll=1"
					break;
				default:
					break;
					}
				
				
				var nw=window.open(qstring,"Document", "scrollbars=yes,resizable=yes,width=880,height=600");	
				nw.focus();	
		
			}
			
 
	function AddMandatoryStarOnLabel (objID)
	{
		RemoveMandatoryStarOnLabel(objID)
		var str=document.getElementById(objID).innerHTML; 
		str ='<font class="MandatoryStar">*&nbsp;</font>' + str;
		document.getElementById(objID).innerHTML = str;	
	}

	function RemoveMandatoryStarOnLabel (objID)
	{
		var str=document.getElementById(objID).innerHTML; 
		document.getElementById(objID).innerHTML = str.replace ('<FONT class=MandatoryStar>*&nbsp;</FONT>', '');
	}

	function SetValidation (status, controlToValidate)
	{
		for(var i = 0; i < Page_Validators.length; i++)
		{
			if(Page_Validators[i].controltovalidate != null && Page_Validators[i].controltovalidate == controlToValidate)
			{
				Page_Validators[i].enabled = status; 
			}			
		}
	}
	
	function tmpSource_onchange(ddl_id, txt_id)
	{
		var tsource = ddl_id;//document.getElementById(ddl_id);
		var txtdource = txt_id;//document.getElementById(txt_id);
		txtdource.value = tsource.value;
	}
	
	function ddlUD1_Load()
	{
		var tsource = document.getElementById('ddlUD1');
		var txtdource = document.getElementById('txtUserDefined2');
		tsource.value = txtdource.value;
	}
	function ddlUD2_Load()
	{
		var tsource = document.getElementById('ddlUD2');
		var txtdource = document.getElementById('txtUserDefined4');
		tsource.value = txtdource.value;
	}
	function ddlSourceofFound_Load()
	{
		var tsource = document.getElementById('ddlSourceoffound');
		var txtdource = document.getElementById('txtSourceOfFunds');
		tsource.value = txtdource.value;
	}

	function changeTOB(ddlcustomertype)
    {
        var dcustomertype = ddlcustomertype;

        if (dcustomertype.value == 'I') {
            document.getElementById('ddltob_I').style.display="block";
            document.getElementById('ddltob_E').style.display="none";
            document.getElementById('ddltob_B').style.display="none";

            document.getElementById('ddlcuststat_i').style.display="block";
            document.getElementById('ddlcuststat_e').style.display="none";
            document.getElementById('ddlcuststat_b').style.display="none";

            document.getElementById('ddlIndustry_i').style.display="block";
            document.getElementById('ddlIndustry_e').style.display="none";
            document.getElementById('ddlIndustry_b').style.display="none";
        }
        else if (dcustomertype.value == 'E')
        {
            document.getElementById('ddltob_I').style.display="none";
            document.getElementById('ddltob_E').style.display="block";
            document.getElementById('ddltob_B').style.display="none";

            document.getElementById('ddlcuststat_i').style.display="none";
            document.getElementById('ddlcuststat_e').style.display="block";
            document.getElementById('ddlcuststat_b').style.display="none";

            document.getElementById('ddlIndustry_i').style.display="none";
            document.getElementById('ddlIndustry_e').style.display="block";
            document.getElementById('ddlIndustry_b').style.display="none";
        }
        else if (dcustomertype.value == 'B')
        {
            document.getElementById('ddltob_I').style.display="none";
            document.getElementById('ddltob_E').style.display="none";
            document.getElementById('ddltob_B').style.display="block";

            document.getElementById('ddlcuststat_i').style.display="none";
            document.getElementById('ddlcuststat_e').style.display="none";
            document.getElementById('ddlcuststat_b').style.display="block";

            document.getElementById('ddlIndustry_i').style.display="none";
            document.getElementById('ddlIndustry_e').style.display="none";
            document.getElementById('ddlIndustry_b').style.display="block";
        }
    }
	
	function group_Load()
	{
	
		var ddl2 = document.getElementById('ddlCDDUserField2');
		var txt2 = document.getElementById('txtCDDUserField2');
		var ddl3 = document.getElementById('ddlCDDUserField3');
		var txt3 = document.getElementById('txtCDDUserField3');
		var ddl4 = document.getElementById('ddlCDDUserField4');
		var txt4 = document.getElementById('txtCDDUserField4');
		var ddl5 = document.getElementById('ddlCDDUserField5');
		var txt5 = document.getElementById('txtCDDUserField5');
		var ddl6 = document.getElementById('ddlCDDUserField6');
		var txt6 = document.getElementById('txtCDDUserField6');
		
		
		var ddl8 = document.getElementById('ddlCDDUserField8');
		var txt8 = document.getElementById('txtCDDUserField8');
		var ddl9 = document.getElementById('ddlCDDUserField9');
		var txt9 = document.getElementById('txtCDDUserField9');
		var ddl10 = document.getElementById('ddlCDDUserField10');
		var txt10 = document.getElementById('txtCDDUserField10');
		var ddl11 = document.getElementById('ddlCDDUserField11');
		var txt11 = document.getElementById('txtCDDUserField11');
		var ddl12 = document.getElementById('ddlCDDUserField12');
		var txt12 = document.getElementById('txtCDDUserField12');
		
		var ddl14 = document.getElementById('ddlCDDUserField14');
		var txt14 = document.getElementById('txtCDDUserField14');
		var ddl15 = document.getElementById('ddlCDDUserField15');
		var txt15 = document.getElementById('txtCDDUserField15');
		var ddl16 = document.getElementById('ddlCDDUserField16');
		var txt16 = document.getElementById('txtCDDUserField16');
		
		var ddl19 = document.getElementById('ddlCDDUserField19');
		var txt19 = document.getElementById('txtCDDUserField19');
		var ddl20 = document.getElementById('ddlCDDUserField20');
		var txt20 = document.getElementById('txtCDDUserField20');
		var ddl21 = document.getElementById('ddlCDDUserField21');
		var txt21 = document.getElementById('txtCDDUserField21');
		var ddl22 = document.getElementById('ddlCDDUserField22');
		var txt22 = document.getElementById('txtCDDUserField22');
		
		var dll_riskrate = document.getElementById('ddlCDDUserField25');
		var txt_riskrate = document.getElementById('txtCDDUserField25');
		
		var ddl26 = document.getElementById('ddlCDDUserField26');
		var txt26 = document.getElementById('txtCDDUserField26');
		var ddl28 = document.getElementById('ddlCDDUserField28');
		var txt28 = document.getElementById('txtCDDUserField28');
		var ddl30 = document.getElementById('ddlCDDUserField30');
		var txt30 = document.getElementById('txtCDDUserField30');
		
		ddl2.value = txt2.value;
		ddl3.value = txt3.value;
		ddl4.value = txt4.value;
		ddl5.value = txt5.value;
		ddl6.value = txt6.value;
		
		ddl8.value = txt8.value;
		ddl9.value = txt9.value;
		ddl10.value = txt10.value;
		ddl11.value = txt11.value;
		ddl12.value = txt12.value;
		
		ddl14.value = txt14.value;
		ddl15.value = txt15.value;
		ddl16.value = txt16.value;

		ddl19.value = txt19.value;
		ddl20.value = txt20.value;
		ddl21.value = txt21.value;
		ddl22.value = txt22.value;
		dll_riskrate.value = txt_riskrate.value;
		
		
		ddl26.value = txt26.value;
		ddl28.value = txt28.value;
		ddl30.value = txt30.value;
		
	}
	
	function LoadTOB()
    {
        var dtobi = document.getElementById('ddltob_I');
        var dtobe = document.getElementById('ddltob_E');
        var dtobb = document.getElementById('ddltob_B');
		var txttob = document.getElementById('txtCDDUserField17');
		var dcustomertype = document.getElementById('ddlcustomertype');
		
        if (dcustomertype.value == 'I') {
            document.getElementById('ddltob_I').style.display="block";
            document.getElementById('ddltob_E').style.display="none";
            document.getElementById('ddltob_B').style.display="none";
			dtobi.value = txttob.value;
        }
        else if (dcustomertype.value == 'E')
        {
            document.getElementById('ddltob_I').style.display="none";
            document.getElementById('ddltob_E').style.display="block";
            document.getElementById('ddltob_B').style.display="none";
			dtobe.value = txttob.value;
        }
        else if (dcustomertype.value == 'B')
        {
            document.getElementById('ddltob_I').style.display="none";
            document.getElementById('ddltob_E').style.display="none";
            document.getElementById('ddltob_B').style.display="block";
			dtobb.value = txttob.value;
        }
		else
		{
            document.getElementById('ddltob_I').style.display="block";
            document.getElementById('ddltob_E').style.display="none";
            document.getElementById('ddltob_B').style.display="none";
		}
    }
	
	function LoadCustStat()
    {
        var dtobi = document.getElementById('ddlcuststat_i');
        var dtobe = document.getElementById('ddlcuststat_e');
        var dtobb = document.getElementById('ddlcuststat_b');
		var txttob = document.getElementById('txtCDDUserField18');
		var dcustomertype = document.getElementById('ddlcustomertype');
        if (dcustomertype.value == 'I') {
            document.getElementById('ddlcuststat_i').style.display="block";
            document.getElementById('ddlcuststat_e').style.display="none";
            document.getElementById('ddlcuststat_b').style.display="none";
			dtobi.value = txttob.value;
        }
        else if (dcustomertype.value == 'E')
        {
            document.getElementById('ddlcuststat_i').style.display="none";
            document.getElementById('ddlcuststat_e').style.display="block";
            document.getElementById('ddlcuststat_b').style.display="none";
			dtobe.value = txttob.value;
        }
        else if (dcustomertype.value == 'B')
        {
            document.getElementById('ddlcuststat_i').style.display="none";
            document.getElementById('ddlcuststat_e').style.display="none";
            document.getElementById('ddlcuststat_b').style.display="block";
			dtobb.value = txttob.value;
        }
		else
		{
            document.getElementById('ddlcuststat_i').style.display="block";
            document.getElementById('ddlcuststat_e').style.display="none";
            document.getElementById('ddlcuststat_b').style.display="none";
		}
    }
	
	function LoadIndustry()
    {
        var dtobi = document.getElementById('ddlIndustry_i');
        var dtobe = document.getElementById('ddlIndustry_e');
        var dtobb = document.getElementById('ddlIndustry_b');
		var txttob = document.getElementById('txtCDDUserField24');
		var dcustomertype = document.getElementById('ddlcustomertype');
        if (dcustomertype.value == 'I') {
            document.getElementById('ddlIndustry_i').style.display="block";
            document.getElementById('ddlIndustry_e').style.display="none";
            document.getElementById('ddlIndustry_b').style.display="none";
			dtobi.value = txttob.value;
        }
        else if (dcustomertype.value == 'E')
        {
            document.getElementById('ddlIndustry_i').style.display="none";
            document.getElementById('ddlIndustry_e').style.display="block";
            document.getElementById('ddlIndustry_b').style.display="none";
			dtobe.value = txttob.value;
        }
        else if (dcustomertype.value == 'B')
        {
            document.getElementById('ddlIndustry_i').style.display="none";
            document.getElementById('ddlIndustry_e').style.display="none";
            document.getElementById('ddlIndustry_b').style.display="block";
			dtobb.value = txttob.value;
        }
		else
		{
            document.getElementById('ddlIndustry_i').style.display="block";
            document.getElementById('ddlIndustry_e').style.display="none";
            document.getElementById('ddlIndustry_b').style.display="none";
		}
    }
	
	function OpenOfacCase() {
		var qstring;
		var custid;
		custid = document.getElementById("txtCode").value;
		//alert('<%= txtCode.ClientID %>');
		qstring = "https://10.1.10.48/showofaccase.asp?id=" + custid;
		//alert(qstring);
		window.open(qstring, '', 'scrollbars=yes,resizable=yes,width=880,height=600'); return false;
		//window.open(qstring,"*OFAC PAST CASE", "scrollbars=yes,resizable=yes,width=880,height=600");	

	}
			
		</script>

        <link href="Styles.css" rel="stylesheet" type="text/css" />        
        <link href="Styles.css" rel="stylesheet" type="text/css" />

	</head>
	<body class="formbody" onload="AddWindowToPCSWindowCookie(window.name);LoadCustStat(); ddlSourceofFound_Load(); ddlUD1_Load();ddlUD2_Load();LoadTOB();group_Load();LoadIndustry();" onUnload="RemoveWindowFromPCSWindowCookie(window.name);">
		<form id="Form1" method="post" runat="server">
            <asp:ScriptManager id="smnCustomer" runat="server" EnableScriptGlobalization="True">
                        </asp:ScriptManager>
			<table id="PageLayout" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr>
								<td width="600"><asp:imagebutton id="imgPrime" runat="server" ImageUrl="images/compliance-manager.gif" AlternateText="Home"
										width="804px"></asp:imagebutton></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordMessage" runat="server" width="804px" cssClass="RecordMessage">Record Message</asp:label></td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" Width="616px" Font-size="X-Small" Height="24px"></asp:validationsummary></td>
				</tr>
				<tr>
					<td>
						<table id="table2" style="WIDTH: 804px; HEIGHT: 40px" cellspacing="0" cellpadding="0" width="816"
							border="0">
							<tr align="center">
								<td align="right" width="60%"><asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder></td>
								<td align="left" width="40%"><uc1:ucrecordscrollbar id="RecordScrollBar" runat="server"></uc1:ucrecordscrollbar></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><uc1:uctabcontrol id="MainTabControl" runat="server"></uc1:uctabcontrol>
</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordHeader" runat="server" width="804px" CSSClass="paneltitle"></asp:label></td>
				</tr>
			</table>
			<asp:panel id="Panel1" runat="server" Width="804px" Height="200px" Cssclass="Panel" BorderWidth="1px"
				BorderStyle="None">
				<table id="table40" cellspacing="0" cellpadding="0" width="750" border="0">
					<tr>
						<td align="right">
							<asp:label id="Label1" runat="server" Width="144px" CssClass="LABEL">Id:</asp:label></td>
						<td width="175">
							<asp:textbox id="txtCode" runat="server" Width="175px" CssClass="TEXTBOX" MaxLength="35"></asp:textbox></td>
						<td align="right">
							<asp:label id="Label8" runat="server" Width="75px" CssClass="LABEL">Name:</asp:label></td>
						<td width="398" colspan="3">
							<asp:textbox id="txtName" runat="server" Width="398px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td style="HEIGHT: 20px" align="right">
							<asp:LinkButton id="lnkRiskClass" CssClass="Label" runat="server" CausesValidation="False">Risk Class:</asp:LinkButton>
							<asp:label id="lblRiskClass" runat="server" Width="144px" CssClass="LABEL" Visible="False">Risk Class:</asp:label></td>
						<td style="HEIGHT: 20px">
							<asp:DropDownList id="ddlRiskClass" runat="server" Width="175px" CssClass="DropDownList"></asp:DropDownList></td>
						<td style="HEIGHT: 20px" align="right">
							<asp:label id="Label9" runat="server" Width="75px" CssClass="LABEL">Address:</asp:label></td>
						<td style="HEIGHT: 20px" colspan="3">
							<asp:textbox id="txtAddress" runat="server" Width="398" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td  align="right">
							<asp:label id="Label3" runat="server" CssClass="LABEL">Customer Type:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlCustomerType" runat="server" Width="175px" CssClass="DropDownList" onchange="changeTOB(ddlCustomerType);"></asp:DropDownList></td>
						<td align="right">
							<asp:label id="Label10" runat="server" Width="75px" CssClass="LABEL">City:</asp:label></td>
						<td style="WIDTH: 219px">
							<asp:textbox id="txtCity" runat="server" Width="150px" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="WIDTH: 95px" align="right">
							<asp:label id="Label11" runat="server" Width="75px" CssClass="LABEL">State:</asp:label></td>
						<td>
							<asp:textbox id="txtState" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td  align="right">
							<asp:label id="Label46" runat="server" CssClass="LABEL">Individual/Business:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlIndOrBus" runat="server" Width="175px" CssClass="DropDownList"></asp:DropDownList></td>
						<td align="right">
							<asp:label id="Label12" runat="server" Width="75px" CssClass="LABEL">Zip:</asp:label></td>
						<td style="WIDTH: 219px">
							<asp:textbox id="txtZip" runat="server" Width="150px" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="WIDTH: 95px" align="right">
							<asp:label id="Label13" runat="server" Width="75px" CssClass="LABEL">Country:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlCountry" runat="server" Width="160px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td align="right">
							<asp:label id="Label4" runat="server" CssClass="LABEL">Type of Business:</asp:label></td>
						<td>
							<asp:textbox id="txtTypeofBusiness" runat="server" Width="175px" CssClass="TEXTBOX"></asp:textbox></td>
						<td align="right">
							<asp:label id="Label14" runat="server" Width="75px" CssClass="LABEL">Phone:</asp:label></td>
						<td style="WIDTH: 219px">
							<asp:textbox id="txtCustomerTelephone" runat="server" Width="150px" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="WIDTH: 95px" align="right">
							<asp:label id="Label15" runat="server" Width="75px" CssClass="LABEL" >Email:</asp:label></td>
						<td>
							<asp:textbox id="txtCustomerEmail" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td align="right">
							<asp:label id="Label47" runat="server" CssClass="LABEL">DBA:</asp:label></td>
						<td>
							<asp:textbox id="txtdBA" runat="server" Width="175px" CssClass="TEXTBOX"></asp:textbox></td>
						<td align="right">
							<asp:label id="Label16" runat="server" Width="75px" CssClass="LABEL">Sex:</asp:label></td>
						<td style="WIDTH: 219px">
							<asp:DropDownList id="ddlSex" runat="server" Width="150px" CssClass="DropDownList"></asp:DropDownList></td>
						<td style="WIDTH: 95px" align="right">
							<asp:label id="Label17" runat="server" Width="75px" CssClass="LABEL">DOB:</asp:label></td>
						<td>
							<asp:textbox id="txtdateOfBirth" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td align="right">
							<asp:label id="Label5" runat="server" CssClass="LABEL" ForeColor="Red">Annual Income<br/>/Turnover:</asp:label></td>
						<td>
							<asp:textbox id="txtAnnualIncome" runat="server" Width="175px" CssClass="TEXTBOX"></asp:textbox></td>
						<td align="right">
							<asp:label id="Label50" runat="server" Width="75px" CssClass="LABEL">FEP:</asp:label></td>
						<td style="WIDTH: 219px" align="left">
							<asp:CheckBox id="chkFEP" runat="server" Width="80px" CssClass="CheckBox"></asp:CheckBox></td>
						<td style="WIDTH: 95px" align="right">
							<asp:label id="Label29" runat="server" Width="75px" CssClass="LABEL" ForeColor="Red">SSN/TIN:</asp:label></td>
						<td>
							<asp:textbox id="txtSSNOrTIN" onclick ="DisplayWarning()" Width="160px" runat="server" CssClass="TEXTBOX"></asp:textbox>
							<input id="HdnDSPNPIDATA" type="hidden" name="HdnDSPNPIDATA" runat="server" />
							</td>
					</tr>
					<tr>
						<td align="right">
							<asp:label id="Label6" runat="server" CssClass="LABEL" ForeColor="Red">Primary Source <br/>Income:</asp:label><asp:textbox id="txtSourceOfFunds" runat="server" Width="0px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:DropDownList id="ddlSourceoffound" runat="server" Width="175px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlSourceoffound,txtSourceOfFunds);">
								<asp:ListItem Text="" Value="0"></asp:ListItem>
								<asp:ListItem Text="1.Salary" Value="1"></asp:ListItem>
								<asp:ListItem Text="2.Rental" Value="2"></asp:ListItem>
								<asp:ListItem Text="3.Business" Value="3"></asp:ListItem>
								<asp:ListItem Text="4.Family Support" Value="4" Selected="True"></asp:ListItem>
								<asp:ListItem Text="5.Other" Value="5"></asp:ListItem>
							</asp:DropDownList>
						</td>
						<td align="right">
							<asp:label id="Label49" runat="server" Width="75px" CssClass="LABEL">PEP:</asp:label></td>
						<td style="WIDTH: 219px" align="left">
							<asp:CheckBox id="chkPEP" runat="server" CssClass="CheckBox"></asp:CheckBox></td>
						<td style="WIDTH: 95px" align="center">&nbsp;&nbsp;&nbsp;
							<asp:label id="Label7" runat="server" Font-size="11px" Width="57" Font-Bold="true">Political  Position:</asp:label></td>
						<td>
							<asp:textbox id="txtPoliticalPosition" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td style="HEIGHT: 27px" align="right">
							<asp:label id="Label2" runat="server" CssClass="LABEL">Status:</asp:label></td>
						<td style="HEIGHT: 27px">
							<asp:DropDownList id="ddlStatus" runat="server" Width="175px" CssClass="DropDownList"></asp:DropDownList></td>
						<td style="HEIGHT: 27px" align="right">
							<asp:label id="Label54" runat="server" Width="75px" CssClass="LABEL">Probation:</asp:label></td>
						<td style="WIDTH: 219px; HEIGHT: 27px" align="left">
							<asp:CheckBox id="chkOnProbation" runat="server" Width="80px" CssClass="CheckBox"></asp:CheckBox></td>
						<td style="WIDTH: 95px; HEIGHT: 27px" align="right">
							<asp:label id="Label72" runat="server" Font-size="11px" Width="79px" Font-Bold="true">Reason:</asp:label></td>
						<td style="HEIGHT: 27px">
							<asp:DropDownList id="ddlProbationReasons" runat="server" Width="160px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td style="HEIGHT: 25px" align="right">
							<asp:label id="Label56" runat="server" CssClass="LABEL">Open Date:</asp:label></td>
						<td style="HEIGHT: 25px">
							<asp:textbox id="txtOpenDate" runat="server" Width="175px" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="HEIGHT: 25px" align="right">
							<asp:label id="Label57" runat="server" Width="75px" CssClass="LABEL" ForeColor="Red">Reverification<br/>Date:</asp:label></td>
						<td style="WIDTH: 219px; HEIGHT: 25px" align="left">
							<asp:textbox id="txtProbationStartdate" runat="server" Width="150px" CssClass="TEXTBOX" BackColor="LightGray"></asp:textbox>
							</td>
						<td style="WIDTH: 95px; HEIGHT: 25px" align="right">
                            <asp:label id="Label71" runat="server" Width="75px" CssClass="LABEL" >End Date:</asp:label></td>
						<td style="HEIGHT: 25px">
							<asp:textbox id="txtProbationEndDate" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td style="HEIGHT: 25px" align="right">
							<asp:label id="Label155" runat="server" CssClass="LABEL">OnBoard Status:</asp:label></td>
						<td style="HEIGHT: 25px">
							<asp:DropDownList id="ddlCDDStatus" runat="server" Width="175px" CssClass="DropDownList"></asp:DropDownList></td>
						<td style="HEIGHT: 25px" align="right">
							<asp:label id="Label156" runat="server" Width="75px" CssClass="LABEL">Create date:</asp:label></td>
						<td style="WIDTH: 219px; HEIGHT: 25px" align="left">
							<asp:textbox id="txtCDDCreateDate" runat="server" Width="150px" CssClass="TEXTBOX" BackColor="LightGray"></asp:textbox></td>
						<td style="WIDTH: 95px; HEIGHT: 25px" align="right">
							<asp:label id="Label157" runat="server" Width="75px" CssClass="LABEL">Modify date:</asp:label></td>
						<td style="HEIGHT: 25px">
							<asp:textbox id="txtCDDModifydate" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td colspan="6">
							<table id="table11" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td style="HEIGHT: 7px"></td>
									<td style="HEIGHT: 7px"></td>
									<td style="HEIGHT: 7px"></td>
									<td style="HEIGHT: 7px"></td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table width="804">
					<tr>
						<td style="width: 87px; height: 14px" align="left">
							<asp:label id="Label24" runat="server" width="804px" CSSClass="paneltitle">
						Accounts with primary relationship</asp:label></td>
						</tr>
				</table>
				<table style="WIDTH: 804px; HEIGHT: 48px" cellpadding="0" cellspacing="0">
					<tr valign="top">
						<td>
							<uc1:ucView id="vwCustomerAccounts" runat="server"></uc1:ucView></td>
					</tr>
				</table>
			</asp:panel><asp:panel id="Panel2" runat="server" width="712px" Height="46px" cssclass="Panel">&nbsp; 
<table id="table5" cellspacing="0" cellpadding="0" width="300" border="0">
					<tr>
						<td>
							<asp:label id="Label20" runat="server" Width="144px" CssClass="LABEL">Parent ID:</asp:label></td>
						<td>
							<asp:textbox id="txtParentId" runat="server" CssClass="TEXTBOX" AutoPostBack="true"></asp:textbox></td>
						<td>
							<asp:label id="Label30" runat="server" Width="144px" CssClass="LABEL">Industry Code:</asp:label></td>
						<td>
							<asp:textbox id="txtSICCode" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label48" runat="server" Width="144px" CssClass="LABEL">Parent Name:</asp:label></td>
						<td>
							<asp:textbox id="txtParentName" runat="server" CssClass="TEXTBOX" BackColor="LightGray" ReadOnly="true"></asp:textbox></td>
						<td>
							<asp:label id="Label23" runat="server" Width="144px" CssClass="LABEL" ForeColor="Red">Passport No:</asp:label></td>
						<td>
							<asp:textbox id="txtPassportNo" onclick ="DisplayWarning()" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label18" runat="server" Width="144px" CssClass="LABEL">Account Officer:</asp:label></td>
						<td>
							<asp:textbox id="txtAccountOfficer" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label27" runat="server" Width="144px" CssClass="LABEL">Asset Size:</asp:label></td>
						<td>
							<asp:textbox id="txtAssetSize" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label19" runat="server" Width="144px" CssClass="LABEL">Telephone:</asp:label></td>
						<td>
							<asp:textbox id="txtAccountOfficerPhone" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label26" runat="server" Width="144px" CssClass="LABEL" ForeColor="Red">License/</br>Cedula No:</asp:label></td>
						<td>
							<asp:textbox id="txtLicenseNo" onclick ="DisplayWarning()" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label28" runat="server" Width="144px" CssClass="LABEL">Email:</asp:label></td>
						<td>
							<asp:textbox id="txtAccountOfficerEmail" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label31" runat="server" Width="144px" CssClass="LABEL" ForeColor="Red">Income Date:</asp:label></td>
						<td>
							<asp:textbox id="txtLicenseState" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label21" runat="server" Width="144px" CssClass="LABEL">Compliance Officer:</asp:label></td>
						<td>
							<asp:textbox id="txtComplianceOfficer" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label70" runat="server" CssClass="Label" ForeColor="Red">Citizen of:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlCountryOfCitizenship" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label22" runat="server" Width="144px" CssClass="LABEL">Telephone:</asp:label></td>
						<td>
							<asp:textbox id="txtComplianceOfficerPhone" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label34" runat="server" Width="145" CssClass="LABEL">Ctr Amount:</asp:label></td>
						<td>
							<asp:textbox id="txtCtrAmount" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label32" runat="server" Width="145" CssClass="LABEL">Email:</asp:label></td>
						<td>
							<asp:textbox id="txtComplianceOfficerEmail" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label25" runat="server" Width="145" CssClass="LABEL">Exemption Status:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlExemptionStatus" runat="server" Width="209" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label40" runat="server" Width="145px" CssClass="Label">US Person:</asp:label></td>
						<td>
							<asp:textbox id="txtUSPerson" runat="server" Width="209" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label75" runat="server" Width="145px" CssClass="LABEL">Open/Closed:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOpenClosed" runat="server" Width="209px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label73" runat="server" Width="145px" CssClass="LABEL">Closed Date:</asp:label></td>
						<td>
							<asp:textbox id="txtClosedDate" runat="server" Width="209px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label69" runat="server" Width="145px" CssClass="LABEL">Closed Reason:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlClosedReason" runat="server" Width="208px"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label94" runat="server" Width="145px" CssClass="LABEL" ForeColor="Red">Country Of Residence:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlCntryOfResidence" runat="server" Width="208px"></asp:DropDownList></td>
					</tr>
				</table><br/>
<hr width="100%" size="1"/>

<table id="table6" style="WIDTH: 712px; HEIGHT: 54px" cellspacing="0" cellpadding="0" width="712"
					border="0">
					<tr>
						<td valign="top" width="50%">
							<table id="table8" cellspacing="0" cellpadding="0" width="350" border="0">
								<tr>
									<td align="right">
										<asp:CheckBox id="chkProspect" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Prospect"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkMSB" runat="server" CssClass="CheckBox" TextAlign="Left" Text="MSB"></asp:CheckBox></td>
								</tr>
								<tr>
									<td align="right">
										<asp:CheckBox id="chkResident" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Resident"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkShellBank" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Shell"></asp:CheckBox></td>
								</tr>
								<tr>
									<td align="right">
										<asp:CheckBox id="chkOffShoreBank" runat="server" CssClass="CheckBox" TextAlign="Left" Text="OffShore Bank"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkPayThroughAccount" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Pay Through Account"></asp:CheckBox></td>
								</tr>
								<tr>
									<td align="right">
										<asp:CheckBox id="chkCorrespBankRelation" runat="server" CssClass="CheckBox" TextAlign="Left"
											Text="Corr. Bank Relation"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkEmbassy" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Embassy"></asp:CheckBox></td>
								</tr>
								<tr>
									<td align="right">
										<asp:CheckBox id="chkHighRiskRespBank" runat="server" CssClass="CheckBox" TextAlign="Left" Text="High Risk Resp Bank"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkForeignGovt" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Foreign Government"></asp:CheckBox></td>
								</tr>
								<tr>
									<td align="right">
										<asp:CheckBox id="chkRegulatedAffiliate" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Regulated Affiliate"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkBearerShares" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Bearer Shares"></asp:CheckBox></td>
								</tr>
								<tr>
									<td align="right">
										<asp:CheckBox id="chkCharityOrganization" runat="server" CssClass="CheckBox" TextAlign="Left"
											Text="Charity Organization"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkNomeDePlume" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Nome De Plume"></asp:CheckBox></td>
								</tr>
							</table>
						</td>
						<td style="width:50%; vertical-align:top;">
							<table id="table7" style="WIDTH: 350px; HEIGHT: 62px" cellspacing="0" cellpadding="0" width="424"
								border="0">
								<tr>
									<td>
										<asp:label id="Label39" runat="server" CssClass="Label">Swift BIC:</asp:label></td>
									<td>
										<asp:textbox id="txtSwiftID" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label33" runat="server" CssClass="Label">Country of Origin:</asp:label></td>
									<td>
										<asp:DropDownList id="ddlCountryOfOrigin" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label35" runat="server" CssClass="Label" ForeColor="Red">Country Of Incorp:</asp:label></td>
									<td>
										<asp:DropDownList id="ddlCountryOfIncorporation" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label36" runat="server" CssClass="Label">OffShore Corp:</asp:label></td>
									<td>
										<asp:DropDownList id="ddlOffShoreCorporation" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label37" runat="server" CssClass="Label">Business Relation:</asp:label></td>
									<td>
										<asp:textbox id="txtBusinessRelation" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label38" runat="server" CssClass="Label">Previous Relation:</asp:label></td>
									<td>
										<asp:textbox id="txtPreviousRelation" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
<hr style="width:100%; size:1;"/>

<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td>
							<asp:label id="Label45" runat="server" CssClass="Label" ForeColor="Red">Pri. Source In. Amt:</asp:label></td>
						<td>
							<asp:textbox id="txtUserDefined1" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label51" runat="server" CssClass="LABEL">Owner Branch:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerBranch" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label41" runat="server" CssClass="Label" ForeColor="Red">Source of Income1:</asp:label><asp:textbox id="txtUserDefined2" runat="server" Width="0px" Height="0px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:DropDownList id="ddlUD1" runat="server" Width="208px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlUD1,txtUserDefined2);">
								<asp:ListItem Text="" Value="0"></asp:ListItem>
								<asp:ListItem Text="1.Salary" Value="1"></asp:ListItem>
								<asp:ListItem Text="2.Rental" Value="2"></asp:ListItem>
								<asp:ListItem Text="3.Business" Value="3"></asp:ListItem>
								<asp:ListItem Text="4.Family Support" Value="4" Selected="True"></asp:ListItem>
								<asp:ListItem Text="5.Other" Value="5"></asp:ListItem>
							</asp:DropDownList>
						</td>
						<td>
							<asp:label id="Label52" runat="server" CssClass="LABEL">Owner Department:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerDept" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td >
							<asp:label id="Label42" runat="server" CssClass="Label" ForeColor="Red">Source Income1 Amt:</asp:label></td>
						<td>
							<asp:textbox id="txtUserDefined3" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label53" runat="server" CssClass="LABEL">Owner Operator:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerOper" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label43" runat="server" CssClass="Label" ForeColor="Red">Source of Income2:</asp:label><asp:textbox id="txtUserDefined4" runat="server" Width="0px" Height="0px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:DropDownList id="ddlUD2" runat="server" Width="208px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlUD2,txtUserDefined4);">
								<asp:ListItem Text="" Value="0"></asp:ListItem>
								<asp:ListItem Text="1.Salary" Value="1"></asp:ListItem>
								<asp:ListItem Text="2.Rental" Value="2"></asp:ListItem>
								<asp:ListItem Text="3.Business" Value="3"></asp:ListItem>
								<asp:ListItem Text="4.Family Support" Value="4" Selected="True"></asp:ListItem>
								<asp:ListItem Text="5.Other" Value="5"></asp:ListItem>
							</asp:DropDownList>
						</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label44" runat="server" CssClass="Label" ForeColor="Red">Source Income2 Amt:</asp:label></td>
						<td>
							<asp:textbox id="txtUserDefined5" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
						<td></td>
						<td></td>
					</tr>
				</table></asp:panel><asp:panel id="Panel3" runat="server" Width="712px" Height="15px" cssclass="Panel">
				<table id="tblProfiles" style="WIDTH: 856px; HEIGHT: 72px" runat="server">
					<tr>
						<td style="width:15%; vertical-align:top;">
							<table cellspacing="1" cellpadding="1" width="100%" border="1">
								<tr>
									<td>
										<asp:LinkButton id="lbCustomerProfiles" runat="server" CssClass="SupportingInfo">Customer Profiles</asp:LinkButton></td>
								</tr>
								<tr>
									<td>
										<asp:LinkButton id="lbCountryProfiles" runat="server" cssclass="SupportingInfo">Country Profiles</asp:LinkButton></td>
								</tr>
							</table>
						</td>
						<td valign="top" width="85%">
							<asp:placeholder id="phCustomerProfiles" runat="server"></asp:placeholder>
							<uc1:ucItemSelector id="CountrySelector" runat="server"></uc1:ucItemSelector></td>
					</tr>
				</table>
			</asp:panel><asp:panel id="Panel4" runat="server" Width="712px" Height="15px" cssclass="Panel">&nbsp; 
<table cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<asp:Button id="cmdAddRelationship" runat="server" Text="Add Relationship"></asp:Button>&nbsp;&nbsp;
						</td>
						<td>
							<asp:Button id="cmdRefreshRelationships" runat="server" Text="Refresh"></asp:Button></td>
						<td valign="middle">
							<asp:Label id="Label154" runat="server" CssClass="LABEL">Draw Relationships:</asp:Label></td>
						<td>
							<asp:DropDownList id="ddlDrawRelationships" runat="server" Width="150px" AutoPostBack="true">
								<asp:ListItem Value="0">Accounts Only</asp:ListItem>
								<asp:ListItem Value="1" Selected="true">Parties Only</asp:ListItem>
								<asp:ListItem Value="2">Parties and Accounts</asp:ListItem>
								<asp:ListItem Value="3">Party Only - Deleted</asp:ListItem>
							</asp:DropDownList></td>
					</tr>
					<tr>
					    <td></td>
						<td colspan="2"><asp:Label id="lblRelationshipsMsg" 
						runat="server" CssClass="griderror"></asp:Label></td>
					</tr>		
				</table>
<GOWEB:GOVIEW id="MyView" tabIndex="1" runat="Server" Height="688px" Width="888px" NoPost="true"
					ScriptFile="js/GoWeb.js" CssFile="none"></GOWEB:GOVIEW></asp:panel><asp:panel id="Panel5" runat="server" Width="712px" Height="15px" cssclass="Panel">
				<uc1:ucActivityList id="ActivityList" runat="server"></uc1:ucActivityList>
				
			</asp:panel><asp:panel id="Panel6" runat="server" Width="804px" cssclass="Panel">
			    <table width="804px">
			        <tr>
			            <td style="width:50px">               
			                <asp:Label id="Label159" runat="server" CssClass="LABEL" Width="50px">List:</asp:Label>
			            </td>

			            <td style="width:255px">
			                <asp:DropDownList id="ddlListCases" runat="server" Width="200px" AutoPostBack="true">
								<asp:ListItem Value="0">Current Only</asp:ListItem>
								<asp:ListItem Value="1" >Archived Only</asp:ListItem>
								<asp:ListItem Value="2" Selected="true">Both Current and Archived</asp:ListItem>
							</asp:DropDownList>
			            </td>

                        <td style="width:250px">
			               <LI>
						     <a class="lnktask" OnClick="OpenOfacCase();">*OFAC PAST CASE</a>
				       </LI>
                        </td>
			        </tr>
			    </table>
			</asp:panel><asp:panel id="Panel7" runat="server" Width="712px" Height="15px" cssclass="Panel">
				<uc1:ucSupportingInfo id="supportingInfo" runat="server"></uc1:ucSupportingInfo>
			</asp:panel><asp:panel id="Panel8" runat="server" Width="712px" Height="15px" cssclass="Panel">
				<uc1:ucNote id="CustomerNotes" runat="server"></uc1:ucNote>
			</asp:panel><asp:panel id="Panel9" runat="server" Width="804px" Height="15px" cssclass="Panel">
				<table>
					<tr>
						<td>
							<asp:label id="Label60" runat="server" Width="200px" CssClass="LABEL">Enable Risk Analysis:</asp:label></td>
						<td>
							<asp:CheckBox id="chkEnableRiskAnalysis" runat="server" CssClass="CheckBox"></asp:CheckBox></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblRiskModel" runat="server" Width="200px" CssClass="LABEL">Risk Model:</asp:label></td>
						<td>
							<asp:textbox id="txtriskModel" runat="server" CssClass="TextBox"></asp:textbox></td>
						<td></td>
						<td></td>
					</tr>
				</table>
				<br/>
				<table>
					<tr>
						<td class="grdheader">
							<asp:label id="lblLastAccepted" runat="server" Font-size="11px" Width="220px" Font-Bold="true">Last Accepted Score Details</asp:label></td>
					</tr>
					<tr>
						<td>
							<asp:DataGrid id="dgScoreDetails" runat="server" Width="804px" BorderStyle="None" BorderWidth="1px"
								BackColor="White" cellpadding="4" AutoGenerateColumns="False" BorderColor="#CC9966">
								<FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
								<SelectedItemStyle Font-Bold="true" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
								<ItemStyle Font-size="11px" ForeColor="#330099" BackColor="White"></ItemStyle>
								<HeaderStyle Font-size="11px" Font-Bold="true" ForeColor="White" BackColor="#990000"></HeaderStyle>
								<Columns>
						            <asp:TemplateColumn HeaderText="Item" >
										<HeaderStyle Width="150px"></HeaderStyle>
							            <ItemTemplate>
    							            <asp:Label id="RiskItem" runat="server" text='<%#Microsoft.Security.Application.Encoder.HtmlEncode(Databinder.Eval(Container, "DataItem.RiskItem").ToString())%>' />
							            </ItemTemplate>
						            </asp:TemplateColumn>									
									<asp:TemplateColumn HeaderText="Req">
										<HeaderStyle Width="25px"></HeaderStyle>
										<ItemTemplate>
											<asp:CheckBox ID ="chkRequired" runat="server" Enabled="false" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Required").ToString())%>'>
											</asp:CheckBox>
										</ItemTemplate>
									</asp:TemplateColumn>
						            <asp:TemplateColumn HeaderText="Prompt" >
										<HeaderStyle Width="100px"/>
							            <ItemTemplate>
    							            <asp:Label id="Prompt" runat="server" text='<%#Microsoft.Security.Application.Encoder.HtmlEncode(Databinder.Eval(Container, "DataItem.Prompt").ToString())%>' />
							            </ItemTemplate>
						            </asp:TemplateColumn>									
									<asp:TemplateColumn HeaderText="Value">
										<HeaderStyle Width="125px"></HeaderStyle>
										<ItemTemplate>
											<asp:TextBox id="txtValue" cssclass="TextBoxWithNoBorder" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Value")%>'/>
										</ItemTemplate>
									</asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Weight(%)">
										<HeaderStyle HorizontalAlign="Right" Width="75px"/>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Weight" runat="server" Text='<%#Microsoft.Security.Application.Encoder.HtmlEncode(Databinder.Eval(Container, "DataItem.Weight").ToString())%>'/>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Def.Score">
										<HeaderStyle Width="25px"></HeaderStyle>
										<ItemTemplate>
											<asp:CheckBox ID ="chkDefaultScore" runat="server" Enabled="false" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.DefaultScore").ToString())%>'>
											</asp:CheckBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score">
										<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox id="txtScore" style="text-align:right;" cssclass="TextBoxWithNoBorder" AutoPostBack="true" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Score").ToString())%>'>
											</asp:TextBox>
											<input id="hdfldScoreType"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ScoreType").ToString())%>' name="hdfldScoreType"/>
											<input id="hdfldRiskItemId" runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.RiskItemId").ToString())%>' name="hdfldRiskItemId"/>
											<input id="hdfldOldScore" runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.OldScore").ToString())%>' name="hdfldOldScore"/>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:DataGrid></td>
					</tr>
				</table>
				<table style="WIDTH: 1088px; HEIGHT: 24px">
					<tr>
						<td>
							<asp:label id="lblRiskScoreLastreview" runat="server" Font-size="11px"></asp:label>
						</td>						
					</tr>					
				</table>
				<br /><br />		
			</asp:panel><asp:panel id="Panel10" runat="server" Width="1002px" Height="15px" cssclass="Panel">
				<table>
					<tr>
						<td>
							<asp:label id="lblEnableCustomerAcceptance" runat="server" Width="220px" CssClass="LABEL">Enable Customer Acceptance:</asp:label></td>
						<td>
							<asp:CheckBox id="chkEnableCustomerAcceptance" runat="server" CssClass="CheckBox"></asp:CheckBox></td>
						<td>
							<asp:label id="Label68" runat="server" CssClass="LABEL">Customer Status:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlCustomerStatus" runat="server" Width="218px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:LinkButton id="lnkAcceptanceModel" Width="150px" CausesValidation="False" runat="server" CssClass="Label">Acceptance Model:</asp:LinkButton></td>
						<td>
							<asp:textbox id="txtAcceptanceModel" runat="server" CssClass="TextBox"></asp:textbox></td>
						<td>
							<asp:LinkButton id="lnkCDDModel" Width="150px" CausesValidation="False" runat="server" CssClass="Label">CDD Model:</asp:LinkButton></td>
						<td>
							<asp:textbox id="txtCDDModel" runat="server" CssClass="TextBox"></asp:textbox></td>
					</tr>
				</table>
				<br/>
				<table>
					<tr>
						<td class="grdheader">
							<asp:label id="lblAcceptanceItems" runat="server" Width="360px" CssClass="LABEL">Acceptance Items Detail (Current and Archived)</asp:label></td>
					</tr>
					<tr>
						<td>
							<div style="OVERFLOW: auto; WIDTH: 1024px; HEIGHT: 360px">
								<asp:DataGrid id="dgItemDetails" runat="server" Width="1004px" BorderStyle="None" BorderWidth="1px"
									BackColor="White" cellpadding="2" AutoGenerateColumns="False" BorderColor="#CC9966">
									<FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
									<SelectedItemStyle Font-Bold="true" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
									<ItemStyle Font-size="11px" BackColor="White"></ItemStyle>
									<HeaderStyle Font-size="11px" Font-Bold="true" ForeColor="White" 
									        BackColor="#990000" HorizontalAlign="Center"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn HeaderText="Id">
											<HeaderStyle Width="25px"></HeaderStyle>
											<ItemTemplate>
												<asp:LinkButton id="lnkbtnItemId" OnClick="AcceptanceItemChanged" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Id").ToString())%>' runat="server" CssClass="lblgridcell">
												</asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="Name" HeaderText="Name">
											<HeaderStyle Width="210px"></HeaderStyle>
										</asp:BoundColumn>
										<asp:TemplateColumn HeaderText="Item Type">
											<HeaderStyle Width="100px"></HeaderStyle>
											<ItemTemplate>
												<asp:TextBox ReadOnly="true" id="txtMaptableName" cssclass="TextBoxWithNoBorder" Width="70px" onclick="ControlClicked();" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.MaptableName").ToString())%>'>
												</asp:TextBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Model">
								                <HeaderStyle Width="100px"></HeaderStyle>
								                <ItemTemplate>
									            <asp:TextBox  ReadOnly="True"  id="txtModel" cssclass="TextBoxWithNoBorder" Width="100px" onclick="ControlClicked();" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CLTCode").ToString())%>'>
									            </asp:TextBox>
								            </ItemTemplate>
							            </asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Record ID">
											<HeaderStyle Width="100px"></HeaderStyle>
											<ItemTemplate>
												<asp:TextBox id="txtGroupID" ReadOnly="true" cssclass="TextBoxWithNoBorder" Width="70px" onclick="ControlClicked();" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GroupID")%>'>
												</asp:TextBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Value">
											<HeaderStyle Width="100px"></HeaderStyle>
											<ItemTemplate>
												<asp:TextBox id="txtAcptVaue" ReadOnly="true" cssclass="TextBoxWithNoBorder" Width="70px" onclick="ControlClicked();" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Value")%>'>
												</asp:TextBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Resp. Person">
											<HeaderStyle Width="100px"></HeaderStyle>
											<ItemTemplate>
												<asp:TextBox id="txtresponsiblePerson" cssclass="TextBoxWithNoBorder" Width="100px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ResponsiblePerson")%>'>
												</asp:TextBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Create Date">
								            <HeaderStyle Width="70px"></HeaderStyle>
								            <ItemTemplate>
									            <asp:TextBox id="txtCreateDate" Width="70px" cssclass="TextBoxWithNoBorder" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CreateDate").ToString())%>'>
									            </asp:TextBox>
								            </ItemTemplate>
							            </asp:TemplateColumn>
							            <asp:TemplateColumn HeaderText="Review Due Date">
											<HeaderStyle Width="70px"></HeaderStyle>
											<ItemTemplate>
												<asp:TextBox id="txtdueDate" Width="70px" cssclass="TextBoxWithNoBorder" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.DueDate").ToString())%>'>
												</asp:TextBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="IDV Status">
											<HeaderStyle Width="40px"></HeaderStyle>
											<ItemTemplate>
												<a id="lnkIDVRequest" href='javascript:<%# 
															GetPopupScript("IDVRequest.aspx?RecordID=" & Microsoft.Security.Application.Encoder.UrlEncode(DataBinder.Eval(Container, "DataItem.IDVRequestNo").ToString()) & _
															"&ActionID=2&RowNum=0&NoScroll=1", "scrollbars=no,resizable=yes,width=880,height=600") 
															%>'>
													<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.IDVStatus").ToString())%>
												</a>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Decision">
											<HeaderStyle Width="110px"></HeaderStyle>
											<ItemTemplate>
												<asp:DropDownList id="ddlDecision" Width="110px" SelectedIndex='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Decision").ToString())%>' runat="server">
													<asp:ListItem Value="0"> Undetermined </asp:ListItem>
													<asp:ListItem Value="1"> Satisfactory </asp:ListItem>
													<asp:ListItem Value="2"> Unsatisfactory </asp:ListItem>
													<asp:ListItem Value="3"> Needs Review </asp:ListItem>
													<asp:ListItem Value="4"> Waived </asp:ListItem>
													<asp:ListItem Value="5" Text =" ">&nbsp;</asp:ListItem>
												</asp:DropDownList>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Complete">
											<HeaderStyle Width="25px"></HeaderStyle>
											<ItemTemplate>
												<asp:CheckBox ID ="chkComplete" runat="server" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Complete").ToString())%>'>
												</asp:CheckBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="2nd.Check">
											<HeaderStyle Width="25px"></HeaderStyle>
											<ItemTemplate>
												<asp:CheckBox ID ="chkSecondCheck" runat="server" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheck").ToString())%>'>
												</asp:CheckBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Approve">
											<HeaderStyle Width="25px"></HeaderStyle>
											<ItemTemplate>
												<asp:CheckBox ID ="chkApprove" runat="server" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Approve").ToString())%>'>
												</asp:CheckBox>
												<input id="hdfldCompletedBy"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CompletedBy").ToString())%>' name="hdfldCompletedBy"/>
												<input id="hdfldSecondCheckedBy"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheckBy").ToString())%>' name="Hidden1"/>
												<input id="hdfldApprovedBy"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ApprovedBy").ToString())%>' name="Hidden2"/>
												<input id="hdfldCompletedDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CompletedDate").ToString())%>' name="Hidden3"/>
												<input id="hdfldSecondCheckedDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheckDate").ToString())%>' name="Hidden4"/>
												<input id="hdfldApprovedDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ApprovedDate").ToString())%>' name="Hidden5"/>
												<input id="hdfldCreateDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CreateDate").ToString())%>' name="Hidden6"/>
												<input id="hdfldNotes"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Notes").ToString())%>' name="Hidden7"/>
												<input id="hdfldDocument"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.DocumentNo").ToString())%>' name="Hidden8"/>
												<input id="hdfldStatus"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Status").ToString())%>' name="Hidden8"/>
												<input id="hdfldCLICode"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CLICode").ToString())%>' name="Hidden8"/>
												<input id="hdfldCompleteRight"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CompleteRight").ToString())%>' name="Hidden8"/>
												<input id="hdfldApproveRight"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ApproveRight").ToString())%>' name="Hidden8"/>
												<input id="hdfldSecondCheckRight"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheckRight").ToString())%>' name="Hidden8"/>
												<input id="hdfldRequired"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Required").ToString())%>' name="Hidden8"/>
												<input id="hdfldApproveRequired"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ApproveRequired").ToString())%>' name="Hidden8"/>
												<input id="hdfldSecondCheckRequired"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheckRequired").ToString())%>' name="Hidden8"/>
												<input id="hdfldItemName"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Name").ToString())%>' name="Hidden8"/>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
								</asp:DataGrid></div>
						</td>
					</tr>
				</table>
				<table>
				</table>
				<table>
					<tr>
						<td>
							<asp:label id="lblItemDetails" runat="server" width="872px" CSSClass="paneltitle"></asp:label></td>
					</tr>
				</table>
				<table id="table1" style="WIDTH: 872px; HEIGHT: 178px" cellspacing="0" cellpadding="0"
					width="872" border="0">
					<tr>
						<td style="WIDTH: 424px" valign="top">
							<table id="table3" style="WIDTH: 400px; HEIGHT: 72px" cellspacing="0" cellpadding="0" width="400"
								border="0">
								<tr>
									<td>
										<asp:label id="Label58" runat="server" Width="104px" CssClass="LABEL">Completed By:</asp:label></td>
									<td>
										<asp:textbox id="txtCompletedBy" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
									<td>
										<asp:label id="Label61" runat="server" Width="50px" CssClass="LABEL">Date:</asp:label></td>
									<td>
										<asp:textbox id="txtCompletedDate" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label59" runat="server" Width="104px" CssClass="LABEL">2nd. Check By:</asp:label></td>
									<td>
										<asp:textbox id="txtSecondCheckedBy" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
									<td>
										<asp:label id="Label62" runat="server" Width="50px" CssClass="LABEL">Date:</asp:label></td>
									<td>
										<asp:textbox id="txtSecondCheckedDate" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label63" runat="server" Width="104px" CssClass="LABEL">Approved By:</asp:label></td>
									<td>
										<asp:textbox id="txtApprovedBy" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
									<td>
										<asp:label id="Label64" runat="server" Width="50px" CssClass="LABEL">Date:</asp:label></td>
									<td>
										<asp:textbox id="txtApprovedDate" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td></td>
									<td align="right" colspan="2">
										<asp:label id="Label65" runat="server" Width="50px" CssClass="LABEL">Create Date:</asp:label></td>
									<td>
										<asp:textbox id="txtCreateDate" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table id="table41" cellspacing="0" cellpadding="0" width="300" border="0">
								<tr>
									<td valign="top" align="right">
										<asp:label id="Label66" runat="server" Width="50px" CssClass="LABEL">Note:</asp:label></td>
									<td>
										<asp:textbox id="txtAcceptanceItemNotes" runat="server" Height="86px" Width="308px" CssClass="TEXTBOX"
											TextMode="MultiLine"></asp:textbox></td>
								</tr>
								<tr>
									<td valign="top" align="right">
										<asp:label id="Label67" runat="server" Width="50px" CssClass="LABEL">Document:</asp:label></td>
									<td>
										<asp:textbox id="txtdocument" runat="server" Width="56px" CssClass="TEXTBOX"></asp:textbox>&nbsp;
										<asp:HyperLink id="lnkViewDocument" onclick="PopupWindow(2);" runat="server" CssClass="lblgridcell"
											Font-Underline="true" ForeColor="Blue">View</asp:HyperLink></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<input id="hdfldSelectedItem" type="hidden" name="hdfldSelectedItem" runat="server"/>
				<input id="hdfldOperator" type="hidden" name="hdfldOperator" runat="server"/> <input id="hdfldValidDocument" type="hidden" value="1" name="hdfldValidDocument" runat="server"/>
			</asp:panel><asp:panel id="Panel11" runat="server" Width="712px" Height="15px" cssclass="Panel"><br/>
				<uc1:ucTabControl id="ucKYCDataDetail" runat="server"></uc1:ucTabControl>
				<asp:panel id="KYCPanel1" runat="server" Height="15px" Width="712px" cssclass="Panel">
					<table id="table22" style="WIDTH: 654px" cellspacing="1" cellpadding="1" width="654" border="0">
						<tr>
							<td style="WIDTH: 213px; HEIGHT: 20px" valign="middle" align="center">
								<asp:label id="Label55" runat="server" Height="24px" Width="220px" CssClass="LABEL">Prefix:</asp:label></td>
							<td>
								<asp:textbox id="txtNamePrefix" runat="server" Height="24px" Width="38" CssClass="TEXTBOX" MaxLength="25"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 213px; HEIGHT: 20px" valign="middle" align="center">
								<asp:label id="Label74" runat="server" Height="24px" Width="220px" CssClass="LABEL">First Name:</asp:label></td>
							<td>
								<asp:textbox id="txtFirstName" runat="server" Height="24px" Width="387px" CssClass="TEXTBOX"
									MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 213px; HEIGHT: 20px" valign="middle" align="center">
								<asp:label id="Label76" runat="server" Height="24px" Width="220px" CssClass="LABEL">Middle Name:</asp:label></td>
							<td>
								<asp:textbox id="txtMiddleName" runat="server" Height="24px" Width="387px" CssClass="TEXTBOX"
									MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 213px; HEIGHT: 20px" valign="middle" align="center">
								<asp:label id="Label77" runat="server" Height="24px" Width="220px" CssClass="LABEL">Last Name:</asp:label></td>
							<td>
								<asp:textbox id="txtLastName" runat="server" Height="24px" Width="387px" CssClass="TEXTBOX" MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 213px; HEIGHT: 20px" valign="middle" align="center">
								<asp:label id="Label78" runat="server" Height="24px" Width="220px" CssClass="LABEL">Suffix:</asp:label></td>
							<td>
								<asp:textbox id="txtNameSuffix" runat="server" Height="24px" Width="38" CssClass="TEXTBOX" MaxLength="25"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 213px">
								<asp:label id="Label79" runat="server" Height="24px" Width="220px" CssClass="LABEL">Alias Name (AKA):</asp:label></td>
							<td>
								<asp:textbox id="txtAlias" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX" MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 213px">
								<asp:label id="Label80" runat="server" Height="24px" Width="220px" CssClass="LABEL">URL:</asp:label></td>
							<td>
								<asp:textbox id="txtURL" runat="server" Height="24px" Width="387" CssClass="TEXTBOX" MaxLength="255"></asp:textbox></td>
						</tr>
					</table>
					<hr style="WIDTH: 146.98%; HEIGHT: 1px" width="146.98%" size="1"/>
					<table id="IndividualDetails">
						<tr>
							<td>
								<asp:label id="Label81" runat="server" Height="24px" Width="220px" CssClass="LABEL">Mother's Maiden Name:</asp:label></td>
							<td>
								<asp:textbox id="txtMothersMaidenName" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									Text=""></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label82" runat="server" Height="24px" Width="220px" CssClass="LABEL">Number of Dependents:</asp:label></td>
							<td>
								<asp:textbox id="txtNoOfDependants" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									Text=""></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label83" runat="server" Height="24px" Width="220px" CssClass="LABEL">Place of Birth:</asp:label></td>
							<td>
								<asp:textbox id="txtPlaceofBirth" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									MaxLength="50" Text=""></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label84" runat="server" Height="24px" Width="220px" CssClass="LABEL">Years at Current Address:</asp:label></td>
							<td>
								<asp:textbox id="txtYrsAtCurrAddress" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									Text=""></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label85" runat="server" Height="24px" Width="220px" CssClass="LABEL">Alien (Non-Citizen):</asp:label></td>
							<td>
								<asp:checkbox id="chkIsAlien" runat="server" Height="24px" Width="160px" CssClass="CheckBox" Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label86" runat="server" Height="24px" Width="220px" CssClass="LABEL">Dual Citizenship Country Names:</asp:label></td>
							<td>
								<asp:DropDownList id="ddlCountryofDualCitizenship" runat="server" Width="158px"></asp:DropDownList></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label87" runat="server" Height="24px" Width="220px" CssClass="LABEL">Employer Name:</asp:label></td>
							<td>
								<asp:textbox id="txtEmployerName" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									MaxLength="50" Text=""></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label88" runat="server" Height="24px" Width="220px" CssClass="LABEL">Job Title:</asp:label></td>
							<td>
								<asp:textbox id="txtJobTitle" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX" MaxLength="50"
									Text=""></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label89" runat="server" Height="24px" Width="220px" CssClass="LABEL">Job Type (Full/Part time):</asp:label></td>
							<td>
								<asp:textbox id="txtJobTime" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX" MaxLength="50"
									Text=""></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label90" runat="server" Height="24px" Width="220px" CssClass="LABEL">Secondary Employer Name:</asp:label></td>
							<td>
								<asp:textbox id="txtSecondaryEmployer" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									MaxLength="50" Text=""></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label91" runat="server" Height="24px" Width="220px" CssClass="LABEL">Length of Employment:</asp:label></td>
							<td>
								<asp:textbox id="txtLengthOfEmp" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									MaxLength="25" Text=""></asp:textbox></td>
							<td>
								<asp:label id="Label92" runat="server" Height="24px" Width="220px" CssClass="LABEL"  ForeColor="Red">Mgr.Override:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsPersonalRelationship" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox>
							</td>		
						</tr>
						<tr>
							<td style="height: 27px">
								<asp:label id="Label161" runat="server" Height="24px" Width="220px" CssClass="LABEL"></asp:label></td>
							<td style="height: 27px">
								<asp:label id="lblHidden" runat="server" Height="24px" Width="160px" CssClass="LABEL"
									Text=""></asp:label></td>
							<td style="height: 27px">
                                <asp:label id="lblRelWB" runat="server" CssClass="LABEL" Height="24px" Width="220px" ForeColor="Red">HQ Sharing List:</asp:label>
                            </td>        
                            <td style="height: 27px">
                                <asp:CheckBox ID="chkRelWB" Text="" CssClass="CHECKBOX" runat="server" />
							</td>							
						</tr>
					</table>
					<hr style="WIDTH: 146.98%; HEIGHT: 1px" width="146.98%" size="1"/>
					<table id="Details">
						<tr>
							<td>
								<asp:label id="Label93" runat="server" Height="24px" Width="220px" CssClass="LABEL">Origin Of Funds:</asp:label></td>
							<td style="WIDTH: 177px">
								<asp:textbox id="txtOriginOfFunds" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									Text=""></asp:textbox></td>
							<td>
                                <asp:Label ID="Label158" runat="server" Height="24px" Width="220px" CssClass="Label" Text="Introducer:"></asp:Label></td>
							<td style="WIDTH: 177px"><asp:textbox id="txtIntroducer" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									Text=""></asp:textbox></td>
						</tr>
					</table>
					<hr style="WIDTH: 146.98%; HEIGHT: 1px" width="146.98%" size="1"/>
					<table id="otherDetails">
						<tr>
							<td>
								<asp:label id="Label95" runat="server" Height="24px" Width="220px" CssClass="LABEL">Date Of Inception:</asp:label></td>
							<td>
								<asp:textbox id="txtdateOfInception" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label96" runat="server" Height="24px" Width="220px" CssClass="LABEL">No OF Employees:</asp:label></td>
							<td>
								<asp:textbox id="txtNoofEmployees" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label97" runat="server" Height="24px" Width="220px" CssClass="LABEL">Legal Enity Type:</asp:label></td>
							<td>
								<asp:textbox id="txtLegalEntityType" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"
									MaxLength="50"></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label98" runat="server" Height="24px" Width="220px" CssClass="LABEL" ForeColor="Red">STR:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsPubliclytraded" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
						</tr>
						<tr>
							<td style="HEIGHT: 28px">
								<asp:label id="Label99" runat="server" Height="24px" Width="220px" CssClass="LABEL">Ticker Symbol:</asp:label></td>
							<td style="HEIGHT: 28px">
								<asp:textbox id="txtTickerSymbol" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 200px; HEIGHT: 28px">
								<asp:label id="Label100" runat="server" Height="24px" Width="220px" CssClass="LABEL">Stock Exchange:</asp:label></td>
							<td style="HEIGHT: 28px">
								<asp:textbox id="txtExchange" runat="server" Height="24" Width="162px" CssClass="TEXTBOX" MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label101" runat="server" Height="24px" Width="220px" CssClass="LABEL">Products Sold:</asp:label></td>
							<td>
								<asp:textbox id="txtProductsSold" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"
									MaxLength="255"></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label102" runat="server" Height="24px" Width="220px" CssClass="LABEL">Services Provided:</asp:label></td>
							<td>
								<asp:textbox id="txtServicesProvided" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"
									MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label103" runat="server" Height="24px" Width="220px" CssClass="LABEL">Geographical Area:</asp:label></td>
							<td>
								<asp:textbox id="txtGeographicalAreasServed" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"
									MaxLength="255"></asp:textbox></td>
							<td>
								<asp:label id="Label104" runat="server" Height="24px" Width="220px" CssClass="LABEL">Total Annual Sale:</asp:label></td>
							<td>
								<asp:textbox id="txtTotalAnnualSales" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label105" runat="server" Height="24px" Width="220px" CssClass="LABEL" ForeColor="Red">SANCTION:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsPensionFund" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label106" runat="server" Height="24px" Width="220px" CssClass="LABEL" ForeColor="Red">Negative News:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsEndowmentFund" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label107" runat="server" Height="24px" Width="220px" CssClass="LABEL" ForeColor="Red">Complex Holding Stru.:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsGovernmentSponsored" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label108" runat="server" Height="24px" Width="220px" CssClass="LABEL">Foreign Correspondent Bank:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsForeignCorrBnk" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label109" runat="server" Height="24px" Width="220px" CssClass="LABEL">Subsidiary:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsSubsidiary" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label110" runat="server" Height="24px" Width="220px" CssClass="LABEL">Requires Cash transactions:</asp:label></td>
							<td>
								<asp:checkbox id="chkRequireCashtrans" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label111" runat="server" Height="24px" Width="220px" CssClass="LABEL">Requires Foreign transactions:</asp:label></td>
							<td>
								<asp:checkbox id="chkRequireForeigntrans" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label112" runat="server" Height="24px" Width="220px" CssClass="LABEL">Requires Wire transactions:</asp:label></td>
							<td>
								<asp:checkbox id="chkRequireWiretrans" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label113" runat="server" Height="24px" Width="220px" CssClass="LABEL">Patriotic Act Certification:</asp:label></td>
							<td>
								<asp:checkbox id="chkHasPatriotActCertification" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label114" runat="server" Height="24px" Width="220px" CssClass="LABEL">Certification Expiry Date:</asp:label></td>
							<td>
								<asp:textbox id="txtPatriotActCertExpiryDate" runat="server" Height="24px" Width="161" CssClass="TEXTBOX"
									Text=""></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label115" runat="server" Height="24px" Width="220px" CssClass="LABEL">Foreign Branches:</asp:label></td>
							<td>
								<asp:checkbox id="chkHasForeignBranches" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label116" runat="server" Height="24px" Width="220px" CssClass="LABEL">Foreign Branch Countries:</asp:label></td>
							<td>
								<asp:textbox id="txtForeignBranchCountries" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									MaxLength="255" Text=""></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="lblPIC" runat="server" Height="24px" Width="220px" CssClass="LABEL">Private Investment Company:</asp:label>
							</td>
							<td>
								<asp:checkbox id="chkPIC" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox>
							</td>						
							<td>
								<asp:label id="Label117" runat="server" Height="24px" Width="220px" CssClass="LABEL">Type of Monetary Instruments:</asp:label>
							</td>
							<td>
								<asp:textbox id="txtTypeofMonInstruments" runat="server" Height="24px" Width="161" CssClass="TEXTBOX"
									MaxLength="255" Text=""></asp:textbox>
							</td>
						</tr>
					</table>
					<hr style="WIDTH: 146.98%; HEIGHT: 1px" width="146.98%" size="1"/>
					<table id="table111" cellspacing="0" cols="4" cellpadding="0" width="750" border="0">
						<tr>
							<td style="WIDTH: 104px" colspan="2">
								<asp:checkbox id="chkMoneyOrders" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text="Money Order Business"></asp:checkbox></td>
							<td style="WIDTH: 159px" colspan="2">
								<asp:checkbox id="chktravelersCheck" runat="server" Height="24px" Width="108px" CssClass="CheckBox"
									Text="Traveler's Check Business"></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="lblMORegistrationNo" runat="server" Height="24px" Width="200px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMORegistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="lblMORegistrattion" runat="server" Height="24px" Width="160px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td>
								<asp:textbox id="txtTCRegistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 104px">
								<asp:label id="lbMOConfirmationNo" runat="server" Height="24px" Width="200px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMORegConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label118" runat="server" Height="24px" Width="160px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td>
								<asp:textbox id="txtTCRegConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="lblMOAgentOf" runat="server" Height="24px" Width="200px" CssClass="LABEL">Acting Agent of:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMOActingasAgentfor" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label119" runat="server" Height="24px" Width="160px" CssClass="LABEL">Acting Agent of:</asp:label></td>
							<td>
								<asp:textbox id="txtTCActingasAgentfor" runat="server" Height="26" Width="266" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="lblMOMonthlyIncome" runat="server" Height="24px" Width="200px" CssClass="LABEL"> Monthly Income:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMOPercentIncome" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label120" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Income:</asp:label></td>
							<td>
								<asp:textbox id="txtTCPercentIncome" runat="server" Height="26" Width="266" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label121" runat="server" Height="24px" Width="200px" CssClass="LABEL"> Monthly Amount :</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMOMonthlyAmt" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label122" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Amount:</asp:label></td>
							<td>
								<asp:textbox id="txtTCMonthlyAmt" runat="server" Height="26" Width="266" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="HEIGHT: 2px">
								<asp:label id="lblMOMaxDailyAmt" runat="server" Height="24px" Width="200px" CssClass="LABEL">Max Daily Amount:</asp:label></td>
							<td style="WIDTH: 266px; HEIGHT: 2px">
								<asp:textbox id="txtMOMaxDailyAmtPerPerson" runat="server" Height="23px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label123" runat="server" Height="24px" Width="160px" CssClass="LABEL">Max Daily Amount:</asp:label></td>
							<td style="HEIGHT: 2px">
								<asp:textbox id="txtTCMaxDailyAmtPerPerson" runat="server" Height="26" Width="266" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 104px" colspan="2">
								<asp:checkbox id="chkMoneytransmission" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text="Money transmission Business "></asp:checkbox></td>
							<td style="WIDTH: 159px" colspan="2">
								<asp:checkbox id="chkCheckCashing" runat="server" Height="24px" Width="108px" CssClass="CheckBox"
									Text="Check Cashing Business"></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label124" runat="server" Height="24px" Width="200px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMtregistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label125" runat="server" Height="24px" Width="160px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td>
								<asp:textbox id="txtCCRegistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 104px">
								<asp:label id="Label126" runat="server" Height="24px" Width="200px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMtregConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label127" runat="server" Height="24px" Width="160px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td>
								<asp:textbox id="txtCCRegConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label128" runat="server" Height="24px" Width="200px" CssClass="LABEL">Acting Agent of :</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMTActingasAgentfor" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label129" runat="server" Height="24px" Width="160px" CssClass="LABEL">Acting Agent of:</asp:label></td>
							<td>
								<asp:textbox id="txtCCActingasAgentfor" runat="server" Height="26" Width="266" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label130" runat="server" Height="24px" Width="200px" CssClass="LABEL"> Monthly Income :</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMTPercentIncome" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label131" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Income:</asp:label></td>
							<td>
								<asp:textbox id="txtCCPercentIncome" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label132" runat="server" Height="24px" Width="200px" CssClass="LABEL"> Monthly Amount :</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMTMonthlyAmt" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label133" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Amount:</asp:label></td>
							<td>
								<asp:textbox id="txtCCMonthlyAmt" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="HEIGHT: 2px">
								<asp:label id="Label134" runat="server" Height="24px" Width="200px" CssClass="LABEL">Max Daily Amount:</asp:label></td>
							<td style="WIDTH: 266px; HEIGHT: 2px">
								<asp:textbox id="txtMTMaxDailyAmtPerPerson" runat="server" Height="23px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label135" runat="server" Height="24px" Width="160px" CssClass="LABEL">Max Daily Amount:</asp:label></td>
							<td style="HEIGHT: 2px">
								<asp:textbox id="txtCCMaxDailyAmtPerPerson" runat="server" Height="23px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 104px" colspan="2">
								<asp:checkbox id="chkCurrencyExchange" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text="Currency Cashing Business"></asp:checkbox></td>
							<td style="WIDTH: 159px" colspan="2">
								<asp:checkbox id="chkCurrencyDealing" runat="server" Height="24px" Width="108px" CssClass="CheckBox"
									Text="Currency Dealing Business"></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label136" runat="server" Height="24px" Width="200px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtCERegistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label137" runat="server" Height="24px" Width="160px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td>
								<asp:textbox id="txtCDRegistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 104px">
								<asp:label id="Label138" runat="server" Height="24px" Width="200px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtCERegConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label139" runat="server" Height="24px" Width="160px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td>
								<asp:textbox id="txtCDRegConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label140" runat="server" Height="24px" Width="200px" CssClass="LABEL">Acting Agent of:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtCEActingasAgentfor" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label141" runat="server" Height="24px" Width="160px" CssClass="LABEL">Acting Agent of:</asp:label></td>
							<td>
								<asp:textbox id="txtCDActingasAgentfor" runat="server" Height="26" Width="266" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label142" runat="server" Height="24px" Width="200px" CssClass="LABEL"> Monthly Income:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtCEPercentIncome" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label143" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Income:</asp:label></td>
							<td>
								<asp:textbox id="txtCDPercentIncome" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label144" runat="server" Height="24px" Width="200px" CssClass="LABEL"> Monthly Amount:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtCEMonthlyAmt" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label145" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Amount:</asp:label></td>
							<td>
								<asp:textbox id="txtCDMonthlyAmt" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="HEIGHT: 2px">
								<asp:label id="Label146" runat="server" Height="24px" Width="200px" CssClass="LABEL">Max Daily Amount:</asp:label></td>
							<td style="WIDTH: 266px; HEIGHT: 2px">
								<asp:textbox id="txtCEMaxDailyAmtPerPerson" runat="server" Height="23px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label147" runat="server" Height="24px" Width="160px" CssClass="LABEL">Max Daily Amount:</asp:label></td>
							<td style="HEIGHT: 2px">
								<asp:textbox id="txtCDMaxDailyAmtPerPerson" runat="server" Height="23px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 104px" colspan="2">
								<asp:checkbox id="chkStoredValue" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text="Stored Value Business"></asp:checkbox></td>
							<td style="WIDTH: 159px"></td>
							<td></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label148" runat="server" Height="24px" Width="200px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtSVRegistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
                                <asp:label id="Label151" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Income:</asp:label>							
							</td>
							<td>
                                <asp:textbox id="txtSVPercentIncome" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox>							
							</td>
						</tr>
						<tr>
							<td style="WIDTH: 104px">
								<asp:label id="Label149" runat="server" Height="24px" Width="200px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtSVRegConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
							    <asp:label id="Label152" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Amount:</asp:label>
							</td>
							<td>
                                <asp:textbox id="txtSVMonthlyAmt" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox>							
							</td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label150" runat="server" Height="24px" Width="200px" CssClass="LABEL">Acting Agent of:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtSVActingasAgentfor" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
                                <asp:label id="Label153" runat="server" Height="24px" Width="160px" CssClass="LABEL">Max Daily Amount:</asp:label>							
							</td>
							<td>
                                <asp:textbox id="SVMaxDailyAmtPerPerson" runat="server" Height="23px" Width="265px" CssClass="TEXTBOX"></asp:textbox>							
							</td>
						</tr>
					</table>
					<hr style="WIDTH: 146.98%; HEIGHT: 1px" width="146.98%" size="1"/>						
					<table>
						<tr>
							<td style="HEIGHT: 2px;">
								<asp:label id="lblRONPRES" runat="server" Height="51px" Width="192px" CssClass="LABELML" ForeColor="Red">Result Neg.<br/>Press Search or comments:</asp:label>
							</td>
							<td style="HEIGHT: 2px; ">
                               <asp:TextBox ID="txtRONPRES" runat="server" TextMode="MultiLine" Height="55px" Width="690px"
                                onkeydown="JStextCount(this,255);" onpaste="javascript:if(this.value.length+window.clipboardData.getData('Text').length>255){event.returnValue = false;};"></asp:TextBox>   
							</td>							
						</tr>		
					</table>
					<hr style="WIDTH: 146.98%; HEIGHT: 1px" width="146.98%" size="1"/>
				</asp:panel>
				<asp:panel id="KYCPanel2" runat="server" Height="15px" Width="712px" cssclass="Panel"><input id="DisplayAddAddress" onclick="HideAndDisplay('display')" type="button" value="New Address"
						name="DisplayAddAddress" runat="server"/> 

<div id="popUpAddModify" style="DISPLAY: none">
						<div id="DIVADDRESSWARNING">
							<h5 id="AddressWarning" style="COLOR: red"></h5>
						</div>
						<table class="" id="AllAdresses1" border="1" runat="server">
							<tr>
								<td>
									<table class="KYCtable" id="AllAdresses" bgColor="#990000" border="1">
										<tr class="KYCtabletr">
											<td>*Address Type</td>
											<td>
												<asp:dropdownlist id="cell0" runat="server" Width="160px"></asp:dropdownlist></td>
										</tr>
										<tr class="KYCtabletr">
											<td>Care of</td>
											<td><input id="cell1" style="WIDTH: 160px; HEIGHT: 22px" disabled="disabled" type="text" maxlength="50"
													size="21"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>*Street Name</td>
											<td><input id="cell2" style="WIDTH: 161px" disabled="disabled" type="text" maxlength="255"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>City</td>
											<td><input id="cell3" style="WIDTH: 161px" disabled="disabled" type="text" maxlength="50"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>State</td>
											<td><input id="cell4" style="WIDTH: 161px" disabled="disabled" type="text" maxlength="50"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>Country</td>
											<td>
												<asp:DropDownList id="cell5" runat="server" Width="161"></asp:DropDownList></td>
										</tr>
										<tr class="KYCtabletr">
											<td>ZIP Code</td>
											<td><input id="cell6" style="WIDTH: 161px" disabled="disabled" type="text" maxlength="10"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>Telephone</td>
											<td><input id="cell7" style="WIDTH: 161px" disabled="disabled" type="text" maxlength="25"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>Fax</td>
											<td><input id="cell8" style="WIDTH: 161px" disabled="disabled" type="text" maxlength="25"/></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><input id="addaddress" onclick="AddOrModifyAddess('ADD', '')" type="button" value="Add"
										name="addaddress" runat="server"/> <input id="hideaddress" onclick="HideAndDisplay('hide')" type="button" value="Cancel"/>
								</td>
							</tr>
						</table>
					</div>
                    <table borderColor="#ffffff" cellspacing="0" cellpadding="0" border="0">
						<tr valign="top">
							<td valign="top">
								<table class="KYCtable" id="PartyAddress" border="1" runat="server">
									<tr class="KYCtabletr">
										<th>
											Address Type</th>
										<th>
											Care Of</th>
										<th>
											Street</th>
										<th>
											City</th>
										<th>
											State</th>
										<th>
											Country</th>
										<th>
											ZIP</th>
										<th>
											Telephone</th>
										<th>
											Fax</th>
										<th>
											Action</th></tr>
								</table>
							</td>
						</tr>
					</table>&nbsp; <input id="hfld1" type="hidden" name="hfld1" runat="server"/><input id="hDeletedAddrs" type="hidden" name="hDeletedAddrs" runat="server"/> </asp:panel>
				<asp:panel id="KYCPanel3" runat="server" Height="15px" Width="712px" cssclass="Panel">
					<input id="DisplayAddID" onclick="HideAndDisplayID('display')" type="button" value="New ID"
						name="DisplayAddID" runat="server"/>
					<div id="popUpAddModifyID" style="DISPLAY: none">
						<div id="DIVIDWARNING">
							<h5 id="IDWarning" style="COLOR: red"></h5>
						</div>
						<table id="AllIDs1" border="1" runat="server">
							<tr>
								<td>
									<table class="KYCtable" id="AllIDs" bgColor="#990000" border="1">
										<tr class="KYCtabletr">
											<td>*ID Type</td>
											<td>
												<asp:dropdownlist id="IDcell0" runat="server" Width="152px"></asp:dropdownlist></td>
										</tr>
										<tr class="KYCtabletr">
											<td>*ID Number</td>
											<td><input id="IDcell1" disabled="disabled" onclick ="DisplayWarning()" type="text" maxlength="25"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>*Issue Agency</td>
											<td><input id="IDcell2" disabled="disabled" type="text" maxlength="255"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>Issue Place</td>
											<td><input id="IDcell3" disabled="disabled" type="text" maxlength="255"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>Issue Country</td>
											<td>
												<asp:DropDownList id="IDcell4" runat="server" Width="152px"></asp:DropDownList></td>
										</tr>
										<tr class="KYCtabletr">
											<td>*Issue Date</td>
											<td><input id="IDcell5" onblur="LocaleDate('IDcell5')" disabled="disabled" type="text"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>*Expiry Date</td>
											<td><input id="IDcell6" onblur="LocaleDate('IDcell6')" disabled="disabled" type="text"/></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><input id="addid" onclick="AddOrModifyID('ADD', '')" type="button" value="Add"/> <input id="hideid" onclick="HideAndDisplayID('hide')" type="button" value="Cancel"/>
								</td>
							</tr>
						</table>
					</div>
					<input id="hFldIDs" type="hidden" name="hFldIDs" runat="server"/> <input id="hDeletedIDs" type="hidden" name="hDeletedIDs" runat="server"/>
					<table class="KYCtable" id="PartyID" border="1" runat="server">
						<tr class="KYCtabletr">
							<th>
								ID Type</th>
							<th>
								ID Number</th>
							<th>
								Issue Agency</th>
							<th>
								Issue Place</th>
							<th>
								Issue Country</th>
							<th>
								Issue Date</th>
							<th>
								Expiry Date</th>
							<th>
								Action</th></tr>
					</table>
				</asp:panel>
				<asp:panel id="KYCPanel4" runat="server" Height="15px" Width="712px" cssclass="Panel">
					<table style="WIDTH: 688px; HEIGHT: 1186px">
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField1" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Type of Products-OnBdr:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField1" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX" disabled="disabled"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField2" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Type of Products-OnBdr1:</asp:Label><asp:TextBox id="txtCDDUserField2" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField2" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField2,txtCDDUserField2);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Call accounts" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Investment accounts" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Mortgage Loans" Value="3"></asp:ListItem>
									<asp:ListItem Text="4.Cross-Border Transactions" Value="4"></asp:ListItem>
									<asp:ListItem Text="5.short term loan" Value="5"></asp:ListItem>
									<asp:ListItem Text="6.Non-resident Rand accounts" Value="6" Selected="True"></asp:ListItem>
									<asp:ListItem Text="7.Customer Foreign Currency Account" Value="7"></asp:ListItem>
									<asp:ListItem Text="8.Commercial Property loans" Value="8"></asp:ListItem>
									<asp:ListItem Text="9.Syndication Loans" Value="9"></asp:ListItem>
									<asp:ListItem Text="10.Trade Finance" Value="10"></asp:ListItem>
									<asp:ListItem Text="11.Foreign Exchange Forward Contracts" Value="11"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField3" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Type of Products-OnBdr2:</asp:Label><asp:TextBox id="txtCDDUserField3" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField3" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField3,txtCDDUserField3);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Call accounts" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Investment accounts" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Mortgage Loans" Value="3"></asp:ListItem>
									<asp:ListItem Text="4.Cross-Border Transactions" Value="4"></asp:ListItem>
									<asp:ListItem Text="5.short term loan" Value="5"></asp:ListItem>
									<asp:ListItem Text="6.Non-resident Rand accounts" Value="6" Selected="True"></asp:ListItem>
									<asp:ListItem Text="7.Customer Foreign Currency Account" Value="7"></asp:ListItem>
									<asp:ListItem Text="8.Commercial Property loans" Value="8"></asp:ListItem>
									<asp:ListItem Text="9.Syndication Loans" Value="9"></asp:ListItem>
									<asp:ListItem Text="10.Trade Finance" Value="10"></asp:ListItem>
									<asp:ListItem Text="11.Foreign Exchange Forward Contracts" Value="11"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField4" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Type of Products-OnBdr3:</asp:Label><asp:TextBox id="txtCDDUserField4" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField4" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField4,txtCDDUserField4);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Call accounts" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Investment accounts" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Mortgage Loans" Value="3"></asp:ListItem>
									<asp:ListItem Text="4.Cross-Border Transactions" Value="4"></asp:ListItem>
									<asp:ListItem Text="5.short term loan" Value="5"></asp:ListItem>
									<asp:ListItem Text="6.Non-resident Rand accounts" Value="6" Selected="True"></asp:ListItem>
									<asp:ListItem Text="7.Customer Foreign Currency Account" Value="7"></asp:ListItem>
									<asp:ListItem Text="8.Commercial Property loans" Value="8"></asp:ListItem>
									<asp:ListItem Text="9.Syndication Loans" Value="9"></asp:ListItem>
									<asp:ListItem Text="10.Trade Finance" Value="10"></asp:ListItem>
									<asp:ListItem Text="11.Foreign Exchange Forward Contracts" Value="11"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField5" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Type of Products-OnBdr4:</asp:Label><asp:TextBox id="txtCDDUserField5" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField5" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField5,txtCDDUserField5);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Call accounts" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Investment accounts" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Mortgage Loans" Value="3"></asp:ListItem>
									<asp:ListItem Text="4.Cross-Border Transactions" Value="4"></asp:ListItem>
									<asp:ListItem Text="5.short term loan" Value="5"></asp:ListItem>
									<asp:ListItem Text="6.Non-resident Rand accounts" Value="6" Selected="True"></asp:ListItem>
									<asp:ListItem Text="7.Customer Foreign Currency Account" Value="7"></asp:ListItem>
									<asp:ListItem Text="8.Commercial Property loans" Value="8"></asp:ListItem>
									<asp:ListItem Text="9.Syndication Loans" Value="9"></asp:ListItem>
									<asp:ListItem Text="10.Trade Finance" Value="10"></asp:ListItem>
									<asp:ListItem Text="11.Foreign Exchange Forward Contracts" Value="11"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField6" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Type of Products-OnBdr5:</asp:Label><asp:TextBox id="txtCDDUserField6" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField6" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField6,txtCDDUserField6);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Call accounts" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Investment accounts" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Mortgage Loans" Value="3"></asp:ListItem>
									<asp:ListItem Text="4.Cross-Border Transactions" Value="4"></asp:ListItem>
									<asp:ListItem Text="5.short term loan" Value="5"></asp:ListItem>
									<asp:ListItem Text="6.Non-resident Rand accounts" Value="6" Selected="True"></asp:ListItem>
									<asp:ListItem Text="7.Customer Foreign Currency Account" Value="7"></asp:ListItem>
									<asp:ListItem Text="8.Commercial Property loans" Value="8"></asp:ListItem>
									<asp:ListItem Text="9.Syndication Loans" Value="9"></asp:ListItem>
									<asp:ListItem Text="10.Trade Finance" Value="10"></asp:ListItem>
									<asp:ListItem Text="11.Foreign Exchange Forward Contracts" Value="11"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField7" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Trade Country-OnBdr:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField7" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX" disabled="disabled"></asp:TextBox></td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField8" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Trade Country-OnBdr1:</asp:Label><asp:TextBox id="txtCDDUserField8" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField8" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField8,txtCDDUserField8);">
									<asp:ListItem Text="AFGHANISTAN" Value="AF"></asp:ListItem>
									<asp:ListItem Text="ALBANIA" Value="AL"></asp:ListItem>
									<asp:ListItem Text="ALGERIA" Value="DZ"></asp:ListItem>
									<asp:ListItem Text="AMERICAN SAMOA" Value="AS"></asp:ListItem>
									<asp:ListItem Text="ANDORRA" Value="AD"></asp:ListItem>
									<asp:ListItem Text="ANGOLA" Value="AO"></asp:ListItem>
									<asp:ListItem Text="ANGUILLA" Value="AI"></asp:ListItem>
									<asp:ListItem Text="ANTARCTICA" Value="AQ"></asp:ListItem>
									<asp:ListItem Text="ANTIGUA AND BARBUDA" Value="AG"></asp:ListItem>
									<asp:ListItem Text="ARGENTINA" Value="AR"></asp:ListItem>
									<asp:ListItem Text="ARMENIA" Value="AM"></asp:ListItem>
									<asp:ListItem Text="ARUBA" Value="AW"></asp:ListItem>
									<asp:ListItem Text="AUSTRALIA" Value="AU"></asp:ListItem>
									<asp:ListItem Text="AUSTRIA" Value="AT"></asp:ListItem>
									<asp:ListItem Text="AZERBAIJAN" Value="AZ"></asp:ListItem>
									<asp:ListItem Text="BAHAMAS" Value="BS"></asp:ListItem>
									<asp:ListItem Text="BAHRAIN" Value="BH"></asp:ListItem>
									<asp:ListItem Text="BANGLADESH" Value="BD"></asp:ListItem>
									<asp:ListItem Text="BARBADOS" Value="BB"></asp:ListItem>
									<asp:ListItem Text="BELARUS" Value="BY"></asp:ListItem>
									<asp:ListItem Text="BELGIUM" Value="BE"></asp:ListItem>
									<asp:ListItem Text="BELIZE" Value="BZ"></asp:ListItem>
									<asp:ListItem Text="BENIN" Value="BJ"></asp:ListItem>
									<asp:ListItem Text="BERMUDA" Value="BM"></asp:ListItem>
									<asp:ListItem Text="BHUTAN" Value="BT"></asp:ListItem>
									<asp:ListItem Text="BOLIVIA" Value="BO"></asp:ListItem>
									<asp:ListItem Text="BOSNIA AND HERZEGOVINA" Value="BA"></asp:ListItem>
									<asp:ListItem Text="BOTSWANA" Value="BW"></asp:ListItem>
									<asp:ListItem Text="BOUVET ISLAND" Value="BV"></asp:ListItem>
									<asp:ListItem Text="BRAZIL" Value="BR"></asp:ListItem>
									<asp:ListItem Text="BRITISH INDIAN OCEAN TERRITORY" Value="IO"></asp:ListItem>
									<asp:ListItem Text="BRUNEI DARUSSALAM" Value="BN"></asp:ListItem>
									<asp:ListItem Text="BULGARIA" Value="BG"></asp:ListItem>
									<asp:ListItem Text="BURKINA FASO" Value="BF"></asp:ListItem>
									<asp:ListItem Text="BURUNDI" Value="BI"></asp:ListItem>
									<asp:ListItem Text="CAMBODIA" Value="KH"></asp:ListItem>
									<asp:ListItem Text="CAMEROON" Value="CM"></asp:ListItem>
									<asp:ListItem Text="CANADA" Value="CA"></asp:ListItem>
									<asp:ListItem Text="CAPE VERDE" Value="CV"></asp:ListItem>
									<asp:ListItem Text="CAYMAN ISLANDS" Value="KY"></asp:ListItem>
									<asp:ListItem Text="CENTRAL AFRICAN REPUBLIC" Value="CF"></asp:ListItem>
									<asp:ListItem Text="CHAD" Value="TD"></asp:ListItem>
									<asp:ListItem Text="CHILE" Value="CL"></asp:ListItem>
									<asp:ListItem Text="CHINA" Value="CN"></asp:ListItem>
									<asp:ListItem Text="CHRISTMAS ISLAND" Value="CX"></asp:ListItem>
									<asp:ListItem Text="COCOS (KEELING) ISLANDS" Value="CC"></asp:ListItem>
									<asp:ListItem Text="COLOMBIA" Value="CO"></asp:ListItem>
									<asp:ListItem Text="COMOROS" Value="KM"></asp:ListItem>
									<asp:ListItem Text="CONGO" Value="CG"></asp:ListItem>
									<asp:ListItem Text="CONGO.THE DEMOCRATIC REP.OFTHE" Value="CD"></asp:ListItem>
									<asp:ListItem Text="COOK ISLANDS" Value="CK"></asp:ListItem>
									<asp:ListItem Text="COSTA RICA" Value="CR"></asp:ListItem>
									<asp:ListItem Text="COTE D'IOVIRE" Value="CI"></asp:ListItem>
									<asp:ListItem Text="CROATIA" Value="HR"></asp:ListItem>
									<asp:ListItem Text="CUBA" Value="CU"></asp:ListItem>
									<asp:ListItem Text="CYPRUS" Value="CY"></asp:ListItem>
									<asp:ListItem Text="CZECH REPUBLIC" Value="CZ"></asp:ListItem>
									<asp:ListItem Text="DENMARK" Value="DK"></asp:ListItem>
									<asp:ListItem Text="DJIBOUTI" Value="DJ"></asp:ListItem>
									<asp:ListItem Text="DOMINICA" Value="DM"></asp:ListItem>
									<asp:ListItem Text="DOMINICAN REPUBLIC" Value="DO"></asp:ListItem>
									<asp:ListItem Text="EAST TIMOR" Value="TP"></asp:ListItem>
									<asp:ListItem Text="ECUADOR" Value="EC"></asp:ListItem>
									<asp:ListItem Text="EGYPT" Value="EG"></asp:ListItem>
									<asp:ListItem Text="EL SALVADOR" Value="SV"></asp:ListItem>
									<asp:ListItem Text="EQUATORIAL GUINEA" Value="GQ"></asp:ListItem>
									<asp:ListItem Text="ERITREA" Value="ER"></asp:ListItem>
									<asp:ListItem Text="ESTONIA" Value="EE"></asp:ListItem>
									<asp:ListItem Text="ETHIOPIA" Value="ET"></asp:ListItem>
									<asp:ListItem Text="FAEROE ISLANDS" Value="FO"></asp:ListItem>
									<asp:ListItem Text="FALKLAND ISLANDS (MALVINAS)" Value="FK"></asp:ListItem>
									<asp:ListItem Text="FIJI" Value="FJ"></asp:ListItem>
									<asp:ListItem Text="FINLAND" Value="FI"></asp:ListItem>
									<asp:ListItem Text="FRANCE" Value="FR"></asp:ListItem>
									<asp:ListItem Text="FRENCH GUIANA" Value="GF"></asp:ListItem>
									<asp:ListItem Text="FRENCH POLYNESIA" Value="PF"></asp:ListItem>
									<asp:ListItem Text="FRENCH SOUTHERN TERRITORIES" Value="TF"></asp:ListItem>
									<asp:ListItem Text="GABON" Value="GA"></asp:ListItem>
									<asp:ListItem Text="GAMBIA" Value="GM"></asp:ListItem>
									<asp:ListItem Text="GEORGIA" Value="GE"></asp:ListItem>
									<asp:ListItem Text="GERMANY" Value="DE"></asp:ListItem>
									<asp:ListItem Text="GHANA" Value="GH"></asp:ListItem>
									<asp:ListItem Text="GIBRALTAR" Value="GI"></asp:ListItem>
									<asp:ListItem Text="GREECE" Value="GR"></asp:ListItem>
									<asp:ListItem Text="GREENLAND" Value="GL"></asp:ListItem>
									<asp:ListItem Text="GRENADA" Value="GD"></asp:ListItem>
									<asp:ListItem Text="GUADELOUPE" Value="GP"></asp:ListItem>
									<asp:ListItem Text="GUAM" Value="GU"></asp:ListItem>
									<asp:ListItem Text="GUATEMALA" Value="GT"></asp:ListItem>
									<asp:ListItem Text="GUERNSEY.C.I" Value="GG"></asp:ListItem>
									<asp:ListItem Text="GUINEA" Value="GN"></asp:ListItem>
									<asp:ListItem Text="GUINEA-BISSAU" Value="GW"></asp:ListItem>
									<asp:ListItem Text="GUYANA" Value="GY"></asp:ListItem>
									<asp:ListItem Text="HAITI" Value="HT"></asp:ListItem>
									<asp:ListItem Text="HEARD AND McDonald ISLANDS" Value="HM"></asp:ListItem>
									<asp:ListItem Text="HOLY SEE(VATICAN CITY STATE)" Value="VA"></asp:ListItem>
									<asp:ListItem Text="HONDURAS" Value="HN"></asp:ListItem>
									<asp:ListItem Text="HONG KONG" Value="HK"></asp:ListItem>
									<asp:ListItem Text="HUNGARY" Value="HU"></asp:ListItem>
									<asp:ListItem Text="ICELAND" Value="IS"></asp:ListItem>
									<asp:ListItem Text="INDIA" Value="IN"></asp:ListItem>
									<asp:ListItem Text="INDONESIA" Value="ID"></asp:ListItem>
									<asp:ListItem Text="IRAN(ISLAMIC REPUBLIC OF)" Value="IR"></asp:ListItem>
									<asp:ListItem Text="IRAQ" Value="IQ"></asp:ListItem>
									<asp:ListItem Text="IRELAND" Value="IE"></asp:ListItem>
									<asp:ListItem Text="ISLE OF MAN" Value="IM"></asp:ListItem>
									<asp:ListItem Text="ISRAEL" Value="IL"></asp:ListItem>
									<asp:ListItem Text="ITALY" Value="IT"></asp:ListItem>
									<asp:ListItem Text="JAMAICA" Value="JM"></asp:ListItem>
									<asp:ListItem Text="JAPAN" Value="JP"></asp:ListItem>
									<asp:ListItem Text="JERSEY. C.I" Value="JE"></asp:ListItem>
									<asp:ListItem Text="JORDAN" Value="JO"></asp:ListItem>
									<asp:ListItem Text="KAZAKHSTAN" Value="KZ"></asp:ListItem>
									<asp:ListItem Text="KENYA" Value="KE"></asp:ListItem>
									<asp:ListItem Text="KIRIBATI" Value="KI"></asp:ListItem>
									<asp:ListItem Text="KOREA.DEMOCRATIC PEOPLE'S OF" Value="KP"></asp:ListItem>
									<asp:ListItem Text="KOREA.REPUBLIC OF" Value="KR"></asp:ListItem>
									<asp:ListItem Text="KUWAIT" Value="KW"></asp:ListItem>
									<asp:ListItem Text="KYRGYZSTAN" Value="KG"></asp:ListItem>
									<asp:ListItem Text="LAO PEOPLES DEMOCRATIC REPUBLI" Value="LA"></asp:ListItem>
									<asp:ListItem Text="LATVIA" Value="LV"></asp:ListItem>
									<asp:ListItem Text="LEBANON" Value="LB"></asp:ListItem>
									<asp:ListItem Text="LESOTHO" Value="LS"></asp:ListItem>
									<asp:ListItem Text="LIBERIA" Value="LR"></asp:ListItem>
									<asp:ListItem Text="LIBYAN ARAB JAMAHIRIYA" Value="LY"></asp:ListItem>
									<asp:ListItem Text="LIECHTENSTEIN" Value="LI"></asp:ListItem>
									<asp:ListItem Text="LITHUANIA" Value="LT"></asp:ListItem>
									<asp:ListItem Text="LUXEMBOURG" Value="LU"></asp:ListItem>
									<asp:ListItem Text="MACAU" Value="MO"></asp:ListItem>
									<asp:ListItem Text="MACEDONIA.THE FORMER YUGOSLA" Value="MK"></asp:ListItem>
									<asp:ListItem Text="MADAGASCAR" Value="MG"></asp:ListItem>
									<asp:ListItem Text="MALAWI" Value="MW"></asp:ListItem>
									<asp:ListItem Text="MALAYSIA" Value="MY"></asp:ListItem>
									<asp:ListItem Text="MALDIVES" Value="MV"></asp:ListItem>
									<asp:ListItem Text="MALI" Value="ML"></asp:ListItem>
									<asp:ListItem Text="MALTA" Value="MT"></asp:ListItem>
									<asp:ListItem Text="MARSHALL ISLANDS" Value="MH"></asp:ListItem>
									<asp:ListItem Text="MARTINIQUE" Value="MQ"></asp:ListItem>
									<asp:ListItem Text="MAURITANIA" Value="MR"></asp:ListItem>
									<asp:ListItem Text="MAURITIUS" Value="MU"></asp:ListItem>
									<asp:ListItem Text="MAYOTTE" Value="YT"></asp:ListItem>
									<asp:ListItem Text="MEXICO" Value="MX"></asp:ListItem>
									<asp:ListItem Text="MICRONESIA(FEDERATED STATES OF" Value="FM"></asp:ListItem>
									<asp:ListItem Text="MOLDOVA.REPUBLIC OF" Value="MD"></asp:ListItem>
									<asp:ListItem Text="MONACO" Value="MC"></asp:ListItem>
									<asp:ListItem Text="MONGOLIA" Value="MN"></asp:ListItem>
									<asp:ListItem Text="MONTSERRAT" Value="MS"></asp:ListItem>
									<asp:ListItem Text="MOROCCO" Value="MA"></asp:ListItem>
									<asp:ListItem Text="MOZAMBIQUE" Value="MZ"></asp:ListItem>
									<asp:ListItem Text="MYANMAR" Value="MM"></asp:ListItem>
									<asp:ListItem Text="NAMIBIA" Value="NA"></asp:ListItem>
									<asp:ListItem Text="NAURU" Value="NR"></asp:ListItem>
									<asp:ListItem Text="NEPAL" Value="NP"></asp:ListItem>
									<asp:ListItem Text="NETH.ANTILLES" Value="AN"></asp:ListItem>
									<asp:ListItem Text="NETHERLANDS" Value="NL"></asp:ListItem>
									<asp:ListItem Text="NEW CALEDONIA" Value="NC"></asp:ListItem>
									<asp:ListItem Text="NEW ZEALAND" Value="NZ"></asp:ListItem>
									<asp:ListItem Text="NICARAGUA" Value="NI"></asp:ListItem>
									<asp:ListItem Text="NIGER" Value="NE"></asp:ListItem>
									<asp:ListItem Text="NIGERIA" Value="NG"></asp:ListItem>
									<asp:ListItem Text="NIUE" Value="NU"></asp:ListItem>
									<asp:ListItem Text="NORFOLK ISLAND" Value="NF"></asp:ListItem>
									<asp:ListItem Text="NORTHERN MARIANA ISLANDS" Value="MP"></asp:ListItem>
									<asp:ListItem Text="NORWAY" Value="NO"></asp:ListItem>
									<asp:ListItem Text="OMAN" Value="OM"></asp:ListItem>
									<asp:ListItem Text="PAKISTAN" Value="PK"></asp:ListItem>
									<asp:ListItem Text="PALAU" Value="PW"></asp:ListItem>
									<asp:ListItem Text="Palestinian Territories" Value="PS"></asp:ListItem>
									<asp:ListItem Text="PANAMA" Value="PA"></asp:ListItem>
									<asp:ListItem Text="PANAMA CANAL ZONE" Value="PZ"></asp:ListItem>
									<asp:ListItem Text="PAPUA NEW GUINEA" Value="PG"></asp:ListItem>
									<asp:ListItem Text="PARAGUAY" Value="PY"></asp:ListItem>
									<asp:ListItem Text="PERU" Value="PE"></asp:ListItem>
									<asp:ListItem Text="PHILIPPINES" Value="PH"></asp:ListItem>
									<asp:ListItem Text="PITCAIRN" Value="PN"></asp:ListItem>
									<asp:ListItem Text="POLAND" Value="PL"></asp:ListItem>
									<asp:ListItem Text="PORTUGAL" Value="PT"></asp:ListItem>
									<asp:ListItem Text="PUERTO RICO" Value="PR"></asp:ListItem>
									<asp:ListItem Text="QATAR" Value="QA"></asp:ListItem>
									<asp:ListItem Text="TAIWAN" Value="TW"></asp:ListItem>
									<asp:ListItem Text="REUNION" Value="RE"></asp:ListItem>
									<asp:ListItem Text="ROMANIA" Value="RO"></asp:ListItem>
									<asp:ListItem Text="RUSSIAN FEDERATION" Value="RU"></asp:ListItem>
									<asp:ListItem Text="RWANDA" Value="RW"></asp:ListItem>
									<asp:ListItem Text="SAINT HELENA" Value="SH"></asp:ListItem>
									<asp:ListItem Text="SAINT KITTS AND NEVIS" Value="KN"></asp:ListItem>
									<asp:ListItem Text="SAINT LUCIA" Value="LC"></asp:ListItem>
									<asp:ListItem Text="SAINT PIERRE AND MIQUELON" Value="PM"></asp:ListItem>
									<asp:ListItem Text="SAINT VINCENT AND THE GRENADIN" Value="VC"></asp:ListItem>
									<asp:ListItem Text="SAMOA" Value="WS"></asp:ListItem>
									<asp:ListItem Text="SAN MARINO" Value="SM"></asp:ListItem>
									<asp:ListItem Text="SAO TOME AND PRINCIPE" Value="ST"></asp:ListItem>
									<asp:ListItem Text="SAUDI ARABIA" Value="SA"></asp:ListItem>
									<asp:ListItem Text="SENEGAL" Value="SN"></asp:ListItem>
									<asp:ListItem Text="Serbia" Value="RS"></asp:ListItem>
									<asp:ListItem Text="SERBIA AND MONTENEGRO" Value="CS"></asp:ListItem>
									<asp:ListItem Text="SEYCHELLES" Value="SC"></asp:ListItem>
									<asp:ListItem Text="SIERRA LEONE" Value="SL"></asp:ListItem>
									<asp:ListItem Text="SINGAPORE" Value="SG"></asp:ListItem>
									<asp:ListItem Text="SLOVAKIA" Value="SK"></asp:ListItem>
									<asp:ListItem Text="SLOVENIA" Value="SI"></asp:ListItem>
									<asp:ListItem Text="SOLOMON ISLANDS" Value="SB"></asp:ListItem>
									<asp:ListItem Text="SOMALIA" Value="SO"></asp:ListItem>
									<asp:ListItem Text="SOUTH AFRICA" Value="ZA"></asp:ListItem>
									<asp:ListItem Text="SOUTH GEORGIA AND SOUTH SANDWI" Value="GS"></asp:ListItem>
									<asp:ListItem Text="SPAIN" Value="ES"></asp:ListItem>
									<asp:ListItem Text="SRILANKA" Value="LK"></asp:ListItem>
									<asp:ListItem Text="SUDAN" Value="SD"></asp:ListItem>
									<asp:ListItem Text="SUPRANATIONAL" Value="XP"></asp:ListItem>
									<asp:ListItem Text="SUPRANATIONAL" Value="ZZ"></asp:ListItem>
									<asp:ListItem Text="SURINAME" Value="SR"></asp:ListItem>
									<asp:ListItem Text="SVALBARD AND JAN MAYEN ISLANDS" Value="SJ"></asp:ListItem>
									<asp:ListItem Text="SWAZILAND" Value="SZ"></asp:ListItem>
									<asp:ListItem Text="SWEDEN" Value="SE"></asp:ListItem>
									<asp:ListItem Text="SWITZERLAND" Value="CH"></asp:ListItem>
									<asp:ListItem Text="SYRIAN ARAB REPUBLIC" Value="SY"></asp:ListItem>
									<asp:ListItem Text="TAJIKISTAN" Value="TJ"></asp:ListItem>
									<asp:ListItem Text="TANZANIA.UNITED REPUBLIC OF" Value="TZ"></asp:ListItem>
									<asp:ListItem Text="THAILAND" Value="TH"></asp:ListItem>
									<asp:ListItem Text="TOGO" Value="TG"></asp:ListItem>
									<asp:ListItem Text="TOKELAU" Value="TK"></asp:ListItem>
									<asp:ListItem Text="TONGA" Value="TO"></asp:ListItem>
									<asp:ListItem Text="TRINIDAD AND TOBAGO" Value="TT"></asp:ListItem>
									<asp:ListItem Text="TUNISIA" Value="TN"></asp:ListItem>
									<asp:ListItem Text="TURKEY" Value="TR"></asp:ListItem>
									<asp:ListItem Text="TURKMENISTAN" Value="TM"></asp:ListItem>
									<asp:ListItem Text="TURKS AND CAICONS ISLANDS" Value="TC"></asp:ListItem>
									<asp:ListItem Text="TUVALU" Value="TV"></asp:ListItem>
									<asp:ListItem Text="UGANDA" Value="UG"></asp:ListItem>
									<asp:ListItem Text="UKRAINE" Value="UA"></asp:ListItem>
									<asp:ListItem Text="UNITED ARAB EMIRATES" Value="AE"></asp:ListItem>
									<asp:ListItem Text="UNITED KINGDOM" Value="GB"></asp:ListItem>
									<asp:ListItem Text="UNITED STATES" Value="US"></asp:ListItem>
									<asp:ListItem Text="UNITED STATES MINOR OUTLYING I" Value="UM"></asp:ListItem>
									<asp:ListItem Text="URUGUAY" Value="UY"></asp:ListItem>
									<asp:ListItem Text="UZBEKISTAN" Value="UZ"></asp:ListItem>
									<asp:ListItem Text="VANUATU" Value="VU"></asp:ListItem>
									<asp:ListItem Text="VENEZUELA" Value="VE"></asp:ListItem>
									<asp:ListItem Text="VIETNAM" Value="VN"></asp:ListItem>
									<asp:ListItem Text="VIRGIN ISLANDS U.S" Value="VI"></asp:ListItem>
									<asp:ListItem Text="VIRGIN ISLANDS.BRITISH" Value="VG"></asp:ListItem>
									<asp:ListItem Text="WALLIS AND FUTUNA ISLANDS" Value="WF"></asp:ListItem>
									<asp:ListItem Text="WESTERN SAHARA" Value="EH"></asp:ListItem>
									<asp:ListItem Text="YEMEN" Value="YE"></asp:ListItem>
									<asp:ListItem Text="YUGOSLAVIA" Value="YU"></asp:ListItem>
									<asp:ListItem Text="ZAMBIA" Value="ZM"></asp:ListItem>
									<asp:ListItem Text="ZIMBABWE" Value="ZW"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField9" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Trade Country-OnBdr2:</asp:Label><asp:TextBox id="txtCDDUserField9" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField9" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField9,txtCDDUserField9);">
									<asp:ListItem Text="AFGHANISTAN" Value="AF"></asp:ListItem>
									<asp:ListItem Text="ALBANIA" Value="AL"></asp:ListItem>
									<asp:ListItem Text="ALGERIA" Value="DZ"></asp:ListItem>
									<asp:ListItem Text="AMERICAN SAMOA" Value="AS"></asp:ListItem>
									<asp:ListItem Text="ANDORRA" Value="AD"></asp:ListItem>
									<asp:ListItem Text="ANGOLA" Value="AO"></asp:ListItem>
									<asp:ListItem Text="ANGUILLA" Value="AI"></asp:ListItem>
									<asp:ListItem Text="ANTARCTICA" Value="AQ"></asp:ListItem>
									<asp:ListItem Text="ANTIGUA AND BARBUDA" Value="AG"></asp:ListItem>
									<asp:ListItem Text="ARGENTINA" Value="AR"></asp:ListItem>
									<asp:ListItem Text="ARMENIA" Value="AM"></asp:ListItem>
									<asp:ListItem Text="ARUBA" Value="AW"></asp:ListItem>
									<asp:ListItem Text="AUSTRALIA" Value="AU"></asp:ListItem>
									<asp:ListItem Text="AUSTRIA" Value="AT"></asp:ListItem>
									<asp:ListItem Text="AZERBAIJAN" Value="AZ"></asp:ListItem>
									<asp:ListItem Text="BAHAMAS" Value="BS"></asp:ListItem>
									<asp:ListItem Text="BAHRAIN" Value="BH"></asp:ListItem>
									<asp:ListItem Text="BANGLADESH" Value="BD"></asp:ListItem>
									<asp:ListItem Text="BARBADOS" Value="BB"></asp:ListItem>
									<asp:ListItem Text="BELARUS" Value="BY"></asp:ListItem>
									<asp:ListItem Text="BELGIUM" Value="BE"></asp:ListItem>
									<asp:ListItem Text="BELIZE" Value="BZ"></asp:ListItem>
									<asp:ListItem Text="BENIN" Value="BJ"></asp:ListItem>
									<asp:ListItem Text="BERMUDA" Value="BM"></asp:ListItem>
									<asp:ListItem Text="BHUTAN" Value="BT"></asp:ListItem>
									<asp:ListItem Text="BOLIVIA" Value="BO"></asp:ListItem>
									<asp:ListItem Text="BOSNIA AND HERZEGOVINA" Value="BA"></asp:ListItem>
									<asp:ListItem Text="BOTSWANA" Value="BW"></asp:ListItem>
									<asp:ListItem Text="BOUVET ISLAND" Value="BV"></asp:ListItem>
									<asp:ListItem Text="BRAZIL" Value="BR"></asp:ListItem>
									<asp:ListItem Text="BRITISH INDIAN OCEAN TERRITORY" Value="IO"></asp:ListItem>
									<asp:ListItem Text="BRUNEI DARUSSALAM" Value="BN"></asp:ListItem>
									<asp:ListItem Text="BULGARIA" Value="BG"></asp:ListItem>
									<asp:ListItem Text="BURKINA FASO" Value="BF"></asp:ListItem>
									<asp:ListItem Text="BURUNDI" Value="BI"></asp:ListItem>
									<asp:ListItem Text="CAMBODIA" Value="KH"></asp:ListItem>
									<asp:ListItem Text="CAMEROON" Value="CM"></asp:ListItem>
									<asp:ListItem Text="CANADA" Value="CA"></asp:ListItem>
									<asp:ListItem Text="CAPE VERDE" Value="CV"></asp:ListItem>
									<asp:ListItem Text="CAYMAN ISLANDS" Value="KY"></asp:ListItem>
									<asp:ListItem Text="CENTRAL AFRICAN REPUBLIC" Value="CF"></asp:ListItem>
									<asp:ListItem Text="CHAD" Value="TD"></asp:ListItem>
									<asp:ListItem Text="CHILE" Value="CL"></asp:ListItem>
									<asp:ListItem Text="CHINA" Value="CN"></asp:ListItem>
									<asp:ListItem Text="CHRISTMAS ISLAND" Value="CX"></asp:ListItem>
									<asp:ListItem Text="COCOS (KEELING) ISLANDS" Value="CC"></asp:ListItem>
									<asp:ListItem Text="COLOMBIA" Value="CO"></asp:ListItem>
									<asp:ListItem Text="COMOROS" Value="KM"></asp:ListItem>
									<asp:ListItem Text="CONGO" Value="CG"></asp:ListItem>
									<asp:ListItem Text="CONGO.THE DEMOCRATIC REP.OFTHE" Value="CD"></asp:ListItem>
									<asp:ListItem Text="COOK ISLANDS" Value="CK"></asp:ListItem>
									<asp:ListItem Text="COSTA RICA" Value="CR"></asp:ListItem>
									<asp:ListItem Text="COTE D'IOVIRE" Value="CI"></asp:ListItem>
									<asp:ListItem Text="CROATIA" Value="HR"></asp:ListItem>
									<asp:ListItem Text="CUBA" Value="CU"></asp:ListItem>
									<asp:ListItem Text="CYPRUS" Value="CY"></asp:ListItem>
									<asp:ListItem Text="CZECH REPUBLIC" Value="CZ"></asp:ListItem>
									<asp:ListItem Text="DENMARK" Value="DK"></asp:ListItem>
									<asp:ListItem Text="DJIBOUTI" Value="DJ"></asp:ListItem>
									<asp:ListItem Text="DOMINICA" Value="DM"></asp:ListItem>
									<asp:ListItem Text="DOMINICAN REPUBLIC" Value="DO"></asp:ListItem>
									<asp:ListItem Text="EAST TIMOR" Value="TP"></asp:ListItem>
									<asp:ListItem Text="ECUADOR" Value="EC"></asp:ListItem>
									<asp:ListItem Text="EGYPT" Value="EG"></asp:ListItem>
									<asp:ListItem Text="EL SALVADOR" Value="SV"></asp:ListItem>
									<asp:ListItem Text="EQUATORIAL GUINEA" Value="GQ"></asp:ListItem>
									<asp:ListItem Text="ERITREA" Value="ER"></asp:ListItem>
									<asp:ListItem Text="ESTONIA" Value="EE"></asp:ListItem>
									<asp:ListItem Text="ETHIOPIA" Value="ET"></asp:ListItem>
									<asp:ListItem Text="FAEROE ISLANDS" Value="FO"></asp:ListItem>
									<asp:ListItem Text="FALKLAND ISLANDS (MALVINAS)" Value="FK"></asp:ListItem>
									<asp:ListItem Text="FIJI" Value="FJ"></asp:ListItem>
									<asp:ListItem Text="FINLAND" Value="FI"></asp:ListItem>
									<asp:ListItem Text="FRANCE" Value="FR"></asp:ListItem>
									<asp:ListItem Text="FRENCH GUIANA" Value="GF"></asp:ListItem>
									<asp:ListItem Text="FRENCH POLYNESIA" Value="PF"></asp:ListItem>
									<asp:ListItem Text="FRENCH SOUTHERN TERRITORIES" Value="TF"></asp:ListItem>
									<asp:ListItem Text="GABON" Value="GA"></asp:ListItem>
									<asp:ListItem Text="GAMBIA" Value="GM"></asp:ListItem>
									<asp:ListItem Text="GEORGIA" Value="GE"></asp:ListItem>
									<asp:ListItem Text="GERMANY" Value="DE"></asp:ListItem>
									<asp:ListItem Text="GHANA" Value="GH"></asp:ListItem>
									<asp:ListItem Text="GIBRALTAR" Value="GI"></asp:ListItem>
									<asp:ListItem Text="GREECE" Value="GR"></asp:ListItem>
									<asp:ListItem Text="GREENLAND" Value="GL"></asp:ListItem>
									<asp:ListItem Text="GRENADA" Value="GD"></asp:ListItem>
									<asp:ListItem Text="GUADELOUPE" Value="GP"></asp:ListItem>
									<asp:ListItem Text="GUAM" Value="GU"></asp:ListItem>
									<asp:ListItem Text="GUATEMALA" Value="GT"></asp:ListItem>
									<asp:ListItem Text="GUERNSEY.C.I" Value="GG"></asp:ListItem>
									<asp:ListItem Text="GUINEA" Value="GN"></asp:ListItem>
									<asp:ListItem Text="GUINEA-BISSAU" Value="GW"></asp:ListItem>
									<asp:ListItem Text="GUYANA" Value="GY"></asp:ListItem>
									<asp:ListItem Text="HAITI" Value="HT"></asp:ListItem>
									<asp:ListItem Text="HEARD AND McDonald ISLANDS" Value="HM"></asp:ListItem>
									<asp:ListItem Text="HOLY SEE(VATICAN CITY STATE)" Value="VA"></asp:ListItem>
									<asp:ListItem Text="HONDURAS" Value="HN"></asp:ListItem>
									<asp:ListItem Text="HONG KONG" Value="HK"></asp:ListItem>
									<asp:ListItem Text="HUNGARY" Value="HU"></asp:ListItem>
									<asp:ListItem Text="ICELAND" Value="IS"></asp:ListItem>
									<asp:ListItem Text="INDIA" Value="IN"></asp:ListItem>
									<asp:ListItem Text="INDONESIA" Value="ID"></asp:ListItem>
									<asp:ListItem Text="IRAN(ISLAMIC REPUBLIC OF)" Value="IR"></asp:ListItem>
									<asp:ListItem Text="IRAQ" Value="IQ"></asp:ListItem>
									<asp:ListItem Text="IRELAND" Value="IE"></asp:ListItem>
									<asp:ListItem Text="ISLE OF MAN" Value="IM"></asp:ListItem>
									<asp:ListItem Text="ISRAEL" Value="IL"></asp:ListItem>
									<asp:ListItem Text="ITALY" Value="IT"></asp:ListItem>
									<asp:ListItem Text="JAMAICA" Value="JM"></asp:ListItem>
									<asp:ListItem Text="JAPAN" Value="JP"></asp:ListItem>
									<asp:ListItem Text="JERSEY. C.I" Value="JE"></asp:ListItem>
									<asp:ListItem Text="JORDAN" Value="JO"></asp:ListItem>
									<asp:ListItem Text="KAZAKHSTAN" Value="KZ"></asp:ListItem>
									<asp:ListItem Text="KENYA" Value="KE"></asp:ListItem>
									<asp:ListItem Text="KIRIBATI" Value="KI"></asp:ListItem>
									<asp:ListItem Text="KOREA.DEMOCRATIC PEOPLE'S OF" Value="KP"></asp:ListItem>
									<asp:ListItem Text="KOREA.REPUBLIC OF" Value="KR"></asp:ListItem>
									<asp:ListItem Text="KUWAIT" Value="KW"></asp:ListItem>
									<asp:ListItem Text="KYRGYZSTAN" Value="KG"></asp:ListItem>
									<asp:ListItem Text="LAO PEOPLES DEMOCRATIC REPUBLI" Value="LA"></asp:ListItem>
									<asp:ListItem Text="LATVIA" Value="LV"></asp:ListItem>
									<asp:ListItem Text="LEBANON" Value="LB"></asp:ListItem>
									<asp:ListItem Text="LESOTHO" Value="LS"></asp:ListItem>
									<asp:ListItem Text="LIBERIA" Value="LR"></asp:ListItem>
									<asp:ListItem Text="LIBYAN ARAB JAMAHIRIYA" Value="LY"></asp:ListItem>
									<asp:ListItem Text="LIECHTENSTEIN" Value="LI"></asp:ListItem>
									<asp:ListItem Text="LITHUANIA" Value="LT"></asp:ListItem>
									<asp:ListItem Text="LUXEMBOURG" Value="LU"></asp:ListItem>
									<asp:ListItem Text="MACAU" Value="MO"></asp:ListItem>
									<asp:ListItem Text="MACEDONIA.THE FORMER YUGOSLA" Value="MK"></asp:ListItem>
									<asp:ListItem Text="MADAGASCAR" Value="MG"></asp:ListItem>
									<asp:ListItem Text="MALAWI" Value="MW"></asp:ListItem>
									<asp:ListItem Text="MALAYSIA" Value="MY"></asp:ListItem>
									<asp:ListItem Text="MALDIVES" Value="MV"></asp:ListItem>
									<asp:ListItem Text="MALI" Value="ML"></asp:ListItem>
									<asp:ListItem Text="MALTA" Value="MT"></asp:ListItem>
									<asp:ListItem Text="MARSHALL ISLANDS" Value="MH"></asp:ListItem>
									<asp:ListItem Text="MARTINIQUE" Value="MQ"></asp:ListItem>
									<asp:ListItem Text="MAURITANIA" Value="MR"></asp:ListItem>
									<asp:ListItem Text="MAURITIUS" Value="MU"></asp:ListItem>
									<asp:ListItem Text="MAYOTTE" Value="YT"></asp:ListItem>
									<asp:ListItem Text="MEXICO" Value="MX"></asp:ListItem>
									<asp:ListItem Text="MICRONESIA(FEDERATED STATES OF" Value="FM"></asp:ListItem>
									<asp:ListItem Text="MOLDOVA.REPUBLIC OF" Value="MD"></asp:ListItem>
									<asp:ListItem Text="MONACO" Value="MC"></asp:ListItem>
									<asp:ListItem Text="MONGOLIA" Value="MN"></asp:ListItem>
									<asp:ListItem Text="MONTSERRAT" Value="MS"></asp:ListItem>
									<asp:ListItem Text="MOROCCO" Value="MA"></asp:ListItem>
									<asp:ListItem Text="MOZAMBIQUE" Value="MZ"></asp:ListItem>
									<asp:ListItem Text="MYANMAR" Value="MM"></asp:ListItem>
									<asp:ListItem Text="NAMIBIA" Value="NA"></asp:ListItem>
									<asp:ListItem Text="NAURU" Value="NR"></asp:ListItem>
									<asp:ListItem Text="NEPAL" Value="NP"></asp:ListItem>
									<asp:ListItem Text="NETH.ANTILLES" Value="AN"></asp:ListItem>
									<asp:ListItem Text="NETHERLANDS" Value="NL"></asp:ListItem>
									<asp:ListItem Text="NEW CALEDONIA" Value="NC"></asp:ListItem>
									<asp:ListItem Text="NEW ZEALAND" Value="NZ"></asp:ListItem>
									<asp:ListItem Text="NICARAGUA" Value="NI"></asp:ListItem>
									<asp:ListItem Text="NIGER" Value="NE"></asp:ListItem>
									<asp:ListItem Text="NIGERIA" Value="NG"></asp:ListItem>
									<asp:ListItem Text="NIUE" Value="NU"></asp:ListItem>
									<asp:ListItem Text="NORFOLK ISLAND" Value="NF"></asp:ListItem>
									<asp:ListItem Text="NORTHERN MARIANA ISLANDS" Value="MP"></asp:ListItem>
									<asp:ListItem Text="NORWAY" Value="NO"></asp:ListItem>
									<asp:ListItem Text="OMAN" Value="OM"></asp:ListItem>
									<asp:ListItem Text="PAKISTAN" Value="PK"></asp:ListItem>
									<asp:ListItem Text="PALAU" Value="PW"></asp:ListItem>
									<asp:ListItem Text="Palestinian Territories" Value="PS"></asp:ListItem>
									<asp:ListItem Text="PANAMA" Value="PA"></asp:ListItem>
									<asp:ListItem Text="PANAMA CANAL ZONE" Value="PZ"></asp:ListItem>
									<asp:ListItem Text="PAPUA NEW GUINEA" Value="PG"></asp:ListItem>
									<asp:ListItem Text="PARAGUAY" Value="PY"></asp:ListItem>
									<asp:ListItem Text="PERU" Value="PE"></asp:ListItem>
									<asp:ListItem Text="PHILIPPINES" Value="PH"></asp:ListItem>
									<asp:ListItem Text="PITCAIRN" Value="PN"></asp:ListItem>
									<asp:ListItem Text="POLAND" Value="PL"></asp:ListItem>
									<asp:ListItem Text="PORTUGAL" Value="PT"></asp:ListItem>
									<asp:ListItem Text="PUERTO RICO" Value="PR"></asp:ListItem>
									<asp:ListItem Text="QATAR" Value="QA"></asp:ListItem>
									<asp:ListItem Text="TAIWAN" Value="TW"></asp:ListItem>
									<asp:ListItem Text="REUNION" Value="RE"></asp:ListItem>
									<asp:ListItem Text="ROMANIA" Value="RO"></asp:ListItem>
									<asp:ListItem Text="RUSSIAN FEDERATION" Value="RU"></asp:ListItem>
									<asp:ListItem Text="RWANDA" Value="RW"></asp:ListItem>
									<asp:ListItem Text="SAINT HELENA" Value="SH"></asp:ListItem>
									<asp:ListItem Text="SAINT KITTS AND NEVIS" Value="KN"></asp:ListItem>
									<asp:ListItem Text="SAINT LUCIA" Value="LC"></asp:ListItem>
									<asp:ListItem Text="SAINT PIERRE AND MIQUELON" Value="PM"></asp:ListItem>
									<asp:ListItem Text="SAINT VINCENT AND THE GRENADIN" Value="VC"></asp:ListItem>
									<asp:ListItem Text="SAMOA" Value="WS"></asp:ListItem>
									<asp:ListItem Text="SAN MARINO" Value="SM"></asp:ListItem>
									<asp:ListItem Text="SAO TOME AND PRINCIPE" Value="ST"></asp:ListItem>
									<asp:ListItem Text="SAUDI ARABIA" Value="SA"></asp:ListItem>
									<asp:ListItem Text="SENEGAL" Value="SN"></asp:ListItem>
									<asp:ListItem Text="Serbia" Value="RS"></asp:ListItem>
									<asp:ListItem Text="SERBIA AND MONTENEGRO" Value="CS"></asp:ListItem>
									<asp:ListItem Text="SEYCHELLES" Value="SC"></asp:ListItem>
									<asp:ListItem Text="SIERRA LEONE" Value="SL"></asp:ListItem>
									<asp:ListItem Text="SINGAPORE" Value="SG"></asp:ListItem>
									<asp:ListItem Text="SLOVAKIA" Value="SK"></asp:ListItem>
									<asp:ListItem Text="SLOVENIA" Value="SI"></asp:ListItem>
									<asp:ListItem Text="SOLOMON ISLANDS" Value="SB"></asp:ListItem>
									<asp:ListItem Text="SOMALIA" Value="SO"></asp:ListItem>
									<asp:ListItem Text="SOUTH AFRICA" Value="ZA"></asp:ListItem>
									<asp:ListItem Text="SOUTH GEORGIA AND SOUTH SANDWI" Value="GS"></asp:ListItem>
									<asp:ListItem Text="SPAIN" Value="ES"></asp:ListItem>
									<asp:ListItem Text="SRILANKA" Value="LK"></asp:ListItem>
									<asp:ListItem Text="SUDAN" Value="SD"></asp:ListItem>
									<asp:ListItem Text="SUPRANATIONAL" Value="XP"></asp:ListItem>
									<asp:ListItem Text="SUPRANATIONAL" Value="ZZ"></asp:ListItem>
									<asp:ListItem Text="SURINAME" Value="SR"></asp:ListItem>
									<asp:ListItem Text="SVALBARD AND JAN MAYEN ISLANDS" Value="SJ"></asp:ListItem>
									<asp:ListItem Text="SWAZILAND" Value="SZ"></asp:ListItem>
									<asp:ListItem Text="SWEDEN" Value="SE"></asp:ListItem>
									<asp:ListItem Text="SWITZERLAND" Value="CH"></asp:ListItem>
									<asp:ListItem Text="SYRIAN ARAB REPUBLIC" Value="SY"></asp:ListItem>
									<asp:ListItem Text="TAJIKISTAN" Value="TJ"></asp:ListItem>
									<asp:ListItem Text="TANZANIA.UNITED REPUBLIC OF" Value="TZ"></asp:ListItem>
									<asp:ListItem Text="THAILAND" Value="TH"></asp:ListItem>
									<asp:ListItem Text="TOGO" Value="TG"></asp:ListItem>
									<asp:ListItem Text="TOKELAU" Value="TK"></asp:ListItem>
									<asp:ListItem Text="TONGA" Value="TO"></asp:ListItem>
									<asp:ListItem Text="TRINIDAD AND TOBAGO" Value="TT"></asp:ListItem>
									<asp:ListItem Text="TUNISIA" Value="TN"></asp:ListItem>
									<asp:ListItem Text="TURKEY" Value="TR"></asp:ListItem>
									<asp:ListItem Text="TURKMENISTAN" Value="TM"></asp:ListItem>
									<asp:ListItem Text="TURKS AND CAICONS ISLANDS" Value="TC"></asp:ListItem>
									<asp:ListItem Text="TUVALU" Value="TV"></asp:ListItem>
									<asp:ListItem Text="UGANDA" Value="UG"></asp:ListItem>
									<asp:ListItem Text="UKRAINE" Value="UA"></asp:ListItem>
									<asp:ListItem Text="UNITED ARAB EMIRATES" Value="AE"></asp:ListItem>
									<asp:ListItem Text="UNITED KINGDOM" Value="GB"></asp:ListItem>
									<asp:ListItem Text="UNITED STATES" Value="US"></asp:ListItem>
									<asp:ListItem Text="UNITED STATES MINOR OUTLYING I" Value="UM"></asp:ListItem>
									<asp:ListItem Text="URUGUAY" Value="UY"></asp:ListItem>
									<asp:ListItem Text="UZBEKISTAN" Value="UZ"></asp:ListItem>
									<asp:ListItem Text="VANUATU" Value="VU"></asp:ListItem>
									<asp:ListItem Text="VENEZUELA" Value="VE"></asp:ListItem>
									<asp:ListItem Text="VIETNAM" Value="VN"></asp:ListItem>
									<asp:ListItem Text="VIRGIN ISLANDS U.S" Value="VI"></asp:ListItem>
									<asp:ListItem Text="VIRGIN ISLANDS.BRITISH" Value="VG"></asp:ListItem>
									<asp:ListItem Text="WALLIS AND FUTUNA ISLANDS" Value="WF"></asp:ListItem>
									<asp:ListItem Text="WESTERN SAHARA" Value="EH"></asp:ListItem>
									<asp:ListItem Text="YEMEN" Value="YE"></asp:ListItem>
									<asp:ListItem Text="YUGOSLAVIA" Value="YU"></asp:ListItem>
									<asp:ListItem Text="ZAMBIA" Value="ZM"></asp:ListItem>
									<asp:ListItem Text="ZIMBABWE" Value="ZW"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField10" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Trade Country-OnBdr3:</asp:Label><asp:TextBox id="txtCDDUserField10" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField10" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField10,txtCDDUserField10);">
									<asp:ListItem Text="AFGHANISTAN" Value="AF"></asp:ListItem>
									<asp:ListItem Text="ALBANIA" Value="AL"></asp:ListItem>
									<asp:ListItem Text="ALGERIA" Value="DZ"></asp:ListItem>
									<asp:ListItem Text="AMERICAN SAMOA" Value="AS"></asp:ListItem>
									<asp:ListItem Text="ANDORRA" Value="AD"></asp:ListItem>
									<asp:ListItem Text="ANGOLA" Value="AO"></asp:ListItem>
									<asp:ListItem Text="ANGUILLA" Value="AI"></asp:ListItem>
									<asp:ListItem Text="ANTARCTICA" Value="AQ"></asp:ListItem>
									<asp:ListItem Text="ANTIGUA AND BARBUDA" Value="AG"></asp:ListItem>
									<asp:ListItem Text="ARGENTINA" Value="AR"></asp:ListItem>
									<asp:ListItem Text="ARMENIA" Value="AM"></asp:ListItem>
									<asp:ListItem Text="ARUBA" Value="AW"></asp:ListItem>
									<asp:ListItem Text="AUSTRALIA" Value="AU"></asp:ListItem>
									<asp:ListItem Text="AUSTRIA" Value="AT"></asp:ListItem>
									<asp:ListItem Text="AZERBAIJAN" Value="AZ"></asp:ListItem>
									<asp:ListItem Text="BAHAMAS" Value="BS"></asp:ListItem>
									<asp:ListItem Text="BAHRAIN" Value="BH"></asp:ListItem>
									<asp:ListItem Text="BANGLADESH" Value="BD"></asp:ListItem>
									<asp:ListItem Text="BARBADOS" Value="BB"></asp:ListItem>
									<asp:ListItem Text="BELARUS" Value="BY"></asp:ListItem>
									<asp:ListItem Text="BELGIUM" Value="BE"></asp:ListItem>
									<asp:ListItem Text="BELIZE" Value="BZ"></asp:ListItem>
									<asp:ListItem Text="BENIN" Value="BJ"></asp:ListItem>
									<asp:ListItem Text="BERMUDA" Value="BM"></asp:ListItem>
									<asp:ListItem Text="BHUTAN" Value="BT"></asp:ListItem>
									<asp:ListItem Text="BOLIVIA" Value="BO"></asp:ListItem>
									<asp:ListItem Text="BOSNIA AND HERZEGOVINA" Value="BA"></asp:ListItem>
									<asp:ListItem Text="BOTSWANA" Value="BW"></asp:ListItem>
									<asp:ListItem Text="BOUVET ISLAND" Value="BV"></asp:ListItem>
									<asp:ListItem Text="BRAZIL" Value="BR"></asp:ListItem>
									<asp:ListItem Text="BRITISH INDIAN OCEAN TERRITORY" Value="IO"></asp:ListItem>
									<asp:ListItem Text="BRUNEI DARUSSALAM" Value="BN"></asp:ListItem>
									<asp:ListItem Text="BULGARIA" Value="BG"></asp:ListItem>
									<asp:ListItem Text="BURKINA FASO" Value="BF"></asp:ListItem>
									<asp:ListItem Text="BURUNDI" Value="BI"></asp:ListItem>
									<asp:ListItem Text="CAMBODIA" Value="KH"></asp:ListItem>
									<asp:ListItem Text="CAMEROON" Value="CM"></asp:ListItem>
									<asp:ListItem Text="CANADA" Value="CA"></asp:ListItem>
									<asp:ListItem Text="CAPE VERDE" Value="CV"></asp:ListItem>
									<asp:ListItem Text="CAYMAN ISLANDS" Value="KY"></asp:ListItem>
									<asp:ListItem Text="CENTRAL AFRICAN REPUBLIC" Value="CF"></asp:ListItem>
									<asp:ListItem Text="CHAD" Value="TD"></asp:ListItem>
									<asp:ListItem Text="CHILE" Value="CL"></asp:ListItem>
									<asp:ListItem Text="CHINA" Value="CN"></asp:ListItem>
									<asp:ListItem Text="CHRISTMAS ISLAND" Value="CX"></asp:ListItem>
									<asp:ListItem Text="COCOS (KEELING) ISLANDS" Value="CC"></asp:ListItem>
									<asp:ListItem Text="COLOMBIA" Value="CO"></asp:ListItem>
									<asp:ListItem Text="COMOROS" Value="KM"></asp:ListItem>
									<asp:ListItem Text="CONGO" Value="CG"></asp:ListItem>
									<asp:ListItem Text="CONGO.THE DEMOCRATIC REP.OFTHE" Value="CD"></asp:ListItem>
									<asp:ListItem Text="COOK ISLANDS" Value="CK"></asp:ListItem>
									<asp:ListItem Text="COSTA RICA" Value="CR"></asp:ListItem>
									<asp:ListItem Text="COTE D'IOVIRE" Value="CI"></asp:ListItem>
									<asp:ListItem Text="CROATIA" Value="HR"></asp:ListItem>
									<asp:ListItem Text="CUBA" Value="CU"></asp:ListItem>
									<asp:ListItem Text="CYPRUS" Value="CY"></asp:ListItem>
									<asp:ListItem Text="CZECH REPUBLIC" Value="CZ"></asp:ListItem>
									<asp:ListItem Text="DENMARK" Value="DK"></asp:ListItem>
									<asp:ListItem Text="DJIBOUTI" Value="DJ"></asp:ListItem>
									<asp:ListItem Text="DOMINICA" Value="DM"></asp:ListItem>
									<asp:ListItem Text="DOMINICAN REPUBLIC" Value="DO"></asp:ListItem>
									<asp:ListItem Text="EAST TIMOR" Value="TP"></asp:ListItem>
									<asp:ListItem Text="ECUADOR" Value="EC"></asp:ListItem>
									<asp:ListItem Text="EGYPT" Value="EG"></asp:ListItem>
									<asp:ListItem Text="EL SALVADOR" Value="SV"></asp:ListItem>
									<asp:ListItem Text="EQUATORIAL GUINEA" Value="GQ"></asp:ListItem>
									<asp:ListItem Text="ERITREA" Value="ER"></asp:ListItem>
									<asp:ListItem Text="ESTONIA" Value="EE"></asp:ListItem>
									<asp:ListItem Text="ETHIOPIA" Value="ET"></asp:ListItem>
									<asp:ListItem Text="FAEROE ISLANDS" Value="FO"></asp:ListItem>
									<asp:ListItem Text="FALKLAND ISLANDS (MALVINAS)" Value="FK"></asp:ListItem>
									<asp:ListItem Text="FIJI" Value="FJ"></asp:ListItem>
									<asp:ListItem Text="FINLAND" Value="FI"></asp:ListItem>
									<asp:ListItem Text="FRANCE" Value="FR"></asp:ListItem>
									<asp:ListItem Text="FRENCH GUIANA" Value="GF"></asp:ListItem>
									<asp:ListItem Text="FRENCH POLYNESIA" Value="PF"></asp:ListItem>
									<asp:ListItem Text="FRENCH SOUTHERN TERRITORIES" Value="TF"></asp:ListItem>
									<asp:ListItem Text="GABON" Value="GA"></asp:ListItem>
									<asp:ListItem Text="GAMBIA" Value="GM"></asp:ListItem>
									<asp:ListItem Text="GEORGIA" Value="GE"></asp:ListItem>
									<asp:ListItem Text="GERMANY" Value="DE"></asp:ListItem>
									<asp:ListItem Text="GHANA" Value="GH"></asp:ListItem>
									<asp:ListItem Text="GIBRALTAR" Value="GI"></asp:ListItem>
									<asp:ListItem Text="GREECE" Value="GR"></asp:ListItem>
									<asp:ListItem Text="GREENLAND" Value="GL"></asp:ListItem>
									<asp:ListItem Text="GRENADA" Value="GD"></asp:ListItem>
									<asp:ListItem Text="GUADELOUPE" Value="GP"></asp:ListItem>
									<asp:ListItem Text="GUAM" Value="GU"></asp:ListItem>
									<asp:ListItem Text="GUATEMALA" Value="GT"></asp:ListItem>
									<asp:ListItem Text="GUERNSEY.C.I" Value="GG"></asp:ListItem>
									<asp:ListItem Text="GUINEA" Value="GN"></asp:ListItem>
									<asp:ListItem Text="GUINEA-BISSAU" Value="GW"></asp:ListItem>
									<asp:ListItem Text="GUYANA" Value="GY"></asp:ListItem>
									<asp:ListItem Text="HAITI" Value="HT"></asp:ListItem>
									<asp:ListItem Text="HEARD AND McDonald ISLANDS" Value="HM"></asp:ListItem>
									<asp:ListItem Text="HOLY SEE(VATICAN CITY STATE)" Value="VA"></asp:ListItem>
									<asp:ListItem Text="HONDURAS" Value="HN"></asp:ListItem>
									<asp:ListItem Text="HONG KONG" Value="HK"></asp:ListItem>
									<asp:ListItem Text="HUNGARY" Value="HU"></asp:ListItem>
									<asp:ListItem Text="ICELAND" Value="IS"></asp:ListItem>
									<asp:ListItem Text="INDIA" Value="IN"></asp:ListItem>
									<asp:ListItem Text="INDONESIA" Value="ID"></asp:ListItem>
									<asp:ListItem Text="IRAN(ISLAMIC REPUBLIC OF)" Value="IR"></asp:ListItem>
									<asp:ListItem Text="IRAQ" Value="IQ"></asp:ListItem>
									<asp:ListItem Text="IRELAND" Value="IE"></asp:ListItem>
									<asp:ListItem Text="ISLE OF MAN" Value="IM"></asp:ListItem>
									<asp:ListItem Text="ISRAEL" Value="IL"></asp:ListItem>
									<asp:ListItem Text="ITALY" Value="IT"></asp:ListItem>
									<asp:ListItem Text="JAMAICA" Value="JM"></asp:ListItem>
									<asp:ListItem Text="JAPAN" Value="JP"></asp:ListItem>
									<asp:ListItem Text="JERSEY. C.I" Value="JE"></asp:ListItem>
									<asp:ListItem Text="JORDAN" Value="JO"></asp:ListItem>
									<asp:ListItem Text="KAZAKHSTAN" Value="KZ"></asp:ListItem>
									<asp:ListItem Text="KENYA" Value="KE"></asp:ListItem>
									<asp:ListItem Text="KIRIBATI" Value="KI"></asp:ListItem>
									<asp:ListItem Text="KOREA.DEMOCRATIC PEOPLE'S OF" Value="KP"></asp:ListItem>
									<asp:ListItem Text="KOREA.REPUBLIC OF" Value="KR"></asp:ListItem>
									<asp:ListItem Text="KUWAIT" Value="KW"></asp:ListItem>
									<asp:ListItem Text="KYRGYZSTAN" Value="KG"></asp:ListItem>
									<asp:ListItem Text="LAO PEOPLES DEMOCRATIC REPUBLI" Value="LA"></asp:ListItem>
									<asp:ListItem Text="LATVIA" Value="LV"></asp:ListItem>
									<asp:ListItem Text="LEBANON" Value="LB"></asp:ListItem>
									<asp:ListItem Text="LESOTHO" Value="LS"></asp:ListItem>
									<asp:ListItem Text="LIBERIA" Value="LR"></asp:ListItem>
									<asp:ListItem Text="LIBYAN ARAB JAMAHIRIYA" Value="LY"></asp:ListItem>
									<asp:ListItem Text="LIECHTENSTEIN" Value="LI"></asp:ListItem>
									<asp:ListItem Text="LITHUANIA" Value="LT"></asp:ListItem>
									<asp:ListItem Text="LUXEMBOURG" Value="LU"></asp:ListItem>
									<asp:ListItem Text="MACAU" Value="MO"></asp:ListItem>
									<asp:ListItem Text="MACEDONIA.THE FORMER YUGOSLA" Value="MK"></asp:ListItem>
									<asp:ListItem Text="MADAGASCAR" Value="MG"></asp:ListItem>
									<asp:ListItem Text="MALAWI" Value="MW"></asp:ListItem>
									<asp:ListItem Text="MALAYSIA" Value="MY"></asp:ListItem>
									<asp:ListItem Text="MALDIVES" Value="MV"></asp:ListItem>
									<asp:ListItem Text="MALI" Value="ML"></asp:ListItem>
									<asp:ListItem Text="MALTA" Value="MT"></asp:ListItem>
									<asp:ListItem Text="MARSHALL ISLANDS" Value="MH"></asp:ListItem>
									<asp:ListItem Text="MARTINIQUE" Value="MQ"></asp:ListItem>
									<asp:ListItem Text="MAURITANIA" Value="MR"></asp:ListItem>
									<asp:ListItem Text="MAURITIUS" Value="MU"></asp:ListItem>
									<asp:ListItem Text="MAYOTTE" Value="YT"></asp:ListItem>
									<asp:ListItem Text="MEXICO" Value="MX"></asp:ListItem>
									<asp:ListItem Text="MICRONESIA(FEDERATED STATES OF" Value="FM"></asp:ListItem>
									<asp:ListItem Text="MOLDOVA.REPUBLIC OF" Value="MD"></asp:ListItem>
									<asp:ListItem Text="MONACO" Value="MC"></asp:ListItem>
									<asp:ListItem Text="MONGOLIA" Value="MN"></asp:ListItem>
									<asp:ListItem Text="MONTSERRAT" Value="MS"></asp:ListItem>
									<asp:ListItem Text="MOROCCO" Value="MA"></asp:ListItem>
									<asp:ListItem Text="MOZAMBIQUE" Value="MZ"></asp:ListItem>
									<asp:ListItem Text="MYANMAR" Value="MM"></asp:ListItem>
									<asp:ListItem Text="NAMIBIA" Value="NA"></asp:ListItem>
									<asp:ListItem Text="NAURU" Value="NR"></asp:ListItem>
									<asp:ListItem Text="NEPAL" Value="NP"></asp:ListItem>
									<asp:ListItem Text="NETH.ANTILLES" Value="AN"></asp:ListItem>
									<asp:ListItem Text="NETHERLANDS" Value="NL"></asp:ListItem>
									<asp:ListItem Text="NEW CALEDONIA" Value="NC"></asp:ListItem>
									<asp:ListItem Text="NEW ZEALAND" Value="NZ"></asp:ListItem>
									<asp:ListItem Text="NICARAGUA" Value="NI"></asp:ListItem>
									<asp:ListItem Text="NIGER" Value="NE"></asp:ListItem>
									<asp:ListItem Text="NIGERIA" Value="NG"></asp:ListItem>
									<asp:ListItem Text="NIUE" Value="NU"></asp:ListItem>
									<asp:ListItem Text="NORFOLK ISLAND" Value="NF"></asp:ListItem>
									<asp:ListItem Text="NORTHERN MARIANA ISLANDS" Value="MP"></asp:ListItem>
									<asp:ListItem Text="NORWAY" Value="NO"></asp:ListItem>
									<asp:ListItem Text="OMAN" Value="OM"></asp:ListItem>
									<asp:ListItem Text="PAKISTAN" Value="PK"></asp:ListItem>
									<asp:ListItem Text="PALAU" Value="PW"></asp:ListItem>
									<asp:ListItem Text="Palestinian Territories" Value="PS"></asp:ListItem>
									<asp:ListItem Text="PANAMA" Value="PA"></asp:ListItem>
									<asp:ListItem Text="PANAMA CANAL ZONE" Value="PZ"></asp:ListItem>
									<asp:ListItem Text="PAPUA NEW GUINEA" Value="PG"></asp:ListItem>
									<asp:ListItem Text="PARAGUAY" Value="PY"></asp:ListItem>
									<asp:ListItem Text="PERU" Value="PE"></asp:ListItem>
									<asp:ListItem Text="PHILIPPINES" Value="PH"></asp:ListItem>
									<asp:ListItem Text="PITCAIRN" Value="PN"></asp:ListItem>
									<asp:ListItem Text="POLAND" Value="PL"></asp:ListItem>
									<asp:ListItem Text="PORTUGAL" Value="PT"></asp:ListItem>
									<asp:ListItem Text="PUERTO RICO" Value="PR"></asp:ListItem>
									<asp:ListItem Text="QATAR" Value="QA"></asp:ListItem>
									<asp:ListItem Text="TAIWAN" Value="TW"></asp:ListItem>
									<asp:ListItem Text="REUNION" Value="RE"></asp:ListItem>
									<asp:ListItem Text="ROMANIA" Value="RO"></asp:ListItem>
									<asp:ListItem Text="RUSSIAN FEDERATION" Value="RU"></asp:ListItem>
									<asp:ListItem Text="RWANDA" Value="RW"></asp:ListItem>
									<asp:ListItem Text="SAINT HELENA" Value="SH"></asp:ListItem>
									<asp:ListItem Text="SAINT KITTS AND NEVIS" Value="KN"></asp:ListItem>
									<asp:ListItem Text="SAINT LUCIA" Value="LC"></asp:ListItem>
									<asp:ListItem Text="SAINT PIERRE AND MIQUELON" Value="PM"></asp:ListItem>
									<asp:ListItem Text="SAINT VINCENT AND THE GRENADIN" Value="VC"></asp:ListItem>
									<asp:ListItem Text="SAMOA" Value="WS"></asp:ListItem>
									<asp:ListItem Text="SAN MARINO" Value="SM"></asp:ListItem>
									<asp:ListItem Text="SAO TOME AND PRINCIPE" Value="ST"></asp:ListItem>
									<asp:ListItem Text="SAUDI ARABIA" Value="SA"></asp:ListItem>
									<asp:ListItem Text="SENEGAL" Value="SN"></asp:ListItem>
									<asp:ListItem Text="Serbia" Value="RS"></asp:ListItem>
									<asp:ListItem Text="SERBIA AND MONTENEGRO" Value="CS"></asp:ListItem>
									<asp:ListItem Text="SEYCHELLES" Value="SC"></asp:ListItem>
									<asp:ListItem Text="SIERRA LEONE" Value="SL"></asp:ListItem>
									<asp:ListItem Text="SINGAPORE" Value="SG"></asp:ListItem>
									<asp:ListItem Text="SLOVAKIA" Value="SK"></asp:ListItem>
									<asp:ListItem Text="SLOVENIA" Value="SI"></asp:ListItem>
									<asp:ListItem Text="SOLOMON ISLANDS" Value="SB"></asp:ListItem>
									<asp:ListItem Text="SOMALIA" Value="SO"></asp:ListItem>
									<asp:ListItem Text="SOUTH AFRICA" Value="ZA"></asp:ListItem>
									<asp:ListItem Text="SOUTH GEORGIA AND SOUTH SANDWI" Value="GS"></asp:ListItem>
									<asp:ListItem Text="SPAIN" Value="ES"></asp:ListItem>
									<asp:ListItem Text="SRILANKA" Value="LK"></asp:ListItem>
									<asp:ListItem Text="SUDAN" Value="SD"></asp:ListItem>
									<asp:ListItem Text="SUPRANATIONAL" Value="XP"></asp:ListItem>
									<asp:ListItem Text="SUPRANATIONAL" Value="ZZ"></asp:ListItem>
									<asp:ListItem Text="SURINAME" Value="SR"></asp:ListItem>
									<asp:ListItem Text="SVALBARD AND JAN MAYEN ISLANDS" Value="SJ"></asp:ListItem>
									<asp:ListItem Text="SWAZILAND" Value="SZ"></asp:ListItem>
									<asp:ListItem Text="SWEDEN" Value="SE"></asp:ListItem>
									<asp:ListItem Text="SWITZERLAND" Value="CH"></asp:ListItem>
									<asp:ListItem Text="SYRIAN ARAB REPUBLIC" Value="SY"></asp:ListItem>
									<asp:ListItem Text="TAJIKISTAN" Value="TJ"></asp:ListItem>
									<asp:ListItem Text="TANZANIA.UNITED REPUBLIC OF" Value="TZ"></asp:ListItem>
									<asp:ListItem Text="THAILAND" Value="TH"></asp:ListItem>
									<asp:ListItem Text="TOGO" Value="TG"></asp:ListItem>
									<asp:ListItem Text="TOKELAU" Value="TK"></asp:ListItem>
									<asp:ListItem Text="TONGA" Value="TO"></asp:ListItem>
									<asp:ListItem Text="TRINIDAD AND TOBAGO" Value="TT"></asp:ListItem>
									<asp:ListItem Text="TUNISIA" Value="TN"></asp:ListItem>
									<asp:ListItem Text="TURKEY" Value="TR"></asp:ListItem>
									<asp:ListItem Text="TURKMENISTAN" Value="TM"></asp:ListItem>
									<asp:ListItem Text="TURKS AND CAICONS ISLANDS" Value="TC"></asp:ListItem>
									<asp:ListItem Text="TUVALU" Value="TV"></asp:ListItem>
									<asp:ListItem Text="UGANDA" Value="UG"></asp:ListItem>
									<asp:ListItem Text="UKRAINE" Value="UA"></asp:ListItem>
									<asp:ListItem Text="UNITED ARAB EMIRATES" Value="AE"></asp:ListItem>
									<asp:ListItem Text="UNITED KINGDOM" Value="GB"></asp:ListItem>
									<asp:ListItem Text="UNITED STATES" Value="US"></asp:ListItem>
									<asp:ListItem Text="UNITED STATES MINOR OUTLYING I" Value="UM"></asp:ListItem>
									<asp:ListItem Text="URUGUAY" Value="UY"></asp:ListItem>
									<asp:ListItem Text="UZBEKISTAN" Value="UZ"></asp:ListItem>
									<asp:ListItem Text="VANUATU" Value="VU"></asp:ListItem>
									<asp:ListItem Text="VENEZUELA" Value="VE"></asp:ListItem>
									<asp:ListItem Text="VIETNAM" Value="VN"></asp:ListItem>
									<asp:ListItem Text="VIRGIN ISLANDS U.S" Value="VI"></asp:ListItem>
									<asp:ListItem Text="VIRGIN ISLANDS.BRITISH" Value="VG"></asp:ListItem>
									<asp:ListItem Text="WALLIS AND FUTUNA ISLANDS" Value="WF"></asp:ListItem>
									<asp:ListItem Text="WESTERN SAHARA" Value="EH"></asp:ListItem>
									<asp:ListItem Text="YEMEN" Value="YE"></asp:ListItem>
									<asp:ListItem Text="YUGOSLAVIA" Value="YU"></asp:ListItem>
									<asp:ListItem Text="ZAMBIA" Value="ZM"></asp:ListItem>
									<asp:ListItem Text="ZIMBABWE" Value="ZW"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField11" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Trade Country-OnBdr4:</asp:Label><asp:TextBox id="txtCDDUserField11" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField11" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField11,txtCDDUserField11);">
									<asp:ListItem Text="AFGHANISTAN" Value="AF"></asp:ListItem>
									<asp:ListItem Text="ALBANIA" Value="AL"></asp:ListItem>
									<asp:ListItem Text="ALGERIA" Value="DZ"></asp:ListItem>
									<asp:ListItem Text="AMERICAN SAMOA" Value="AS"></asp:ListItem>
									<asp:ListItem Text="ANDORRA" Value="AD"></asp:ListItem>
									<asp:ListItem Text="ANGOLA" Value="AO"></asp:ListItem>
									<asp:ListItem Text="ANGUILLA" Value="AI"></asp:ListItem>
									<asp:ListItem Text="ANTARCTICA" Value="AQ"></asp:ListItem>
									<asp:ListItem Text="ANTIGUA AND BARBUDA" Value="AG"></asp:ListItem>
									<asp:ListItem Text="ARGENTINA" Value="AR"></asp:ListItem>
									<asp:ListItem Text="ARMENIA" Value="AM"></asp:ListItem>
									<asp:ListItem Text="ARUBA" Value="AW"></asp:ListItem>
									<asp:ListItem Text="AUSTRALIA" Value="AU"></asp:ListItem>
									<asp:ListItem Text="AUSTRIA" Value="AT"></asp:ListItem>
									<asp:ListItem Text="AZERBAIJAN" Value="AZ"></asp:ListItem>
									<asp:ListItem Text="BAHAMAS" Value="BS"></asp:ListItem>
									<asp:ListItem Text="BAHRAIN" Value="BH"></asp:ListItem>
									<asp:ListItem Text="BANGLADESH" Value="BD"></asp:ListItem>
									<asp:ListItem Text="BARBADOS" Value="BB"></asp:ListItem>
									<asp:ListItem Text="BELARUS" Value="BY"></asp:ListItem>
									<asp:ListItem Text="BELGIUM" Value="BE"></asp:ListItem>
									<asp:ListItem Text="BELIZE" Value="BZ"></asp:ListItem>
									<asp:ListItem Text="BENIN" Value="BJ"></asp:ListItem>
									<asp:ListItem Text="BERMUDA" Value="BM"></asp:ListItem>
									<asp:ListItem Text="BHUTAN" Value="BT"></asp:ListItem>
									<asp:ListItem Text="BOLIVIA" Value="BO"></asp:ListItem>
									<asp:ListItem Text="BOSNIA AND HERZEGOVINA" Value="BA"></asp:ListItem>
									<asp:ListItem Text="BOTSWANA" Value="BW"></asp:ListItem>
									<asp:ListItem Text="BOUVET ISLAND" Value="BV"></asp:ListItem>
									<asp:ListItem Text="BRAZIL" Value="BR"></asp:ListItem>
									<asp:ListItem Text="BRITISH INDIAN OCEAN TERRITORY" Value="IO"></asp:ListItem>
									<asp:ListItem Text="BRUNEI DARUSSALAM" Value="BN"></asp:ListItem>
									<asp:ListItem Text="BULGARIA" Value="BG"></asp:ListItem>
									<asp:ListItem Text="BURKINA FASO" Value="BF"></asp:ListItem>
									<asp:ListItem Text="BURUNDI" Value="BI"></asp:ListItem>
									<asp:ListItem Text="CAMBODIA" Value="KH"></asp:ListItem>
									<asp:ListItem Text="CAMEROON" Value="CM"></asp:ListItem>
									<asp:ListItem Text="CANADA" Value="CA"></asp:ListItem>
									<asp:ListItem Text="CAPE VERDE" Value="CV"></asp:ListItem>
									<asp:ListItem Text="CAYMAN ISLANDS" Value="KY"></asp:ListItem>
									<asp:ListItem Text="CENTRAL AFRICAN REPUBLIC" Value="CF"></asp:ListItem>
									<asp:ListItem Text="CHAD" Value="TD"></asp:ListItem>
									<asp:ListItem Text="CHILE" Value="CL"></asp:ListItem>
									<asp:ListItem Text="CHINA" Value="CN"></asp:ListItem>
									<asp:ListItem Text="CHRISTMAS ISLAND" Value="CX"></asp:ListItem>
									<asp:ListItem Text="COCOS (KEELING) ISLANDS" Value="CC"></asp:ListItem>
									<asp:ListItem Text="COLOMBIA" Value="CO"></asp:ListItem>
									<asp:ListItem Text="COMOROS" Value="KM"></asp:ListItem>
									<asp:ListItem Text="CONGO" Value="CG"></asp:ListItem>
									<asp:ListItem Text="CONGO.THE DEMOCRATIC REP.OFTHE" Value="CD"></asp:ListItem>
									<asp:ListItem Text="COOK ISLANDS" Value="CK"></asp:ListItem>
									<asp:ListItem Text="COSTA RICA" Value="CR"></asp:ListItem>
									<asp:ListItem Text="COTE D'IOVIRE" Value="CI"></asp:ListItem>
									<asp:ListItem Text="CROATIA" Value="HR"></asp:ListItem>
									<asp:ListItem Text="CUBA" Value="CU"></asp:ListItem>
									<asp:ListItem Text="CYPRUS" Value="CY"></asp:ListItem>
									<asp:ListItem Text="CZECH REPUBLIC" Value="CZ"></asp:ListItem>
									<asp:ListItem Text="DENMARK" Value="DK"></asp:ListItem>
									<asp:ListItem Text="DJIBOUTI" Value="DJ"></asp:ListItem>
									<asp:ListItem Text="DOMINICA" Value="DM"></asp:ListItem>
									<asp:ListItem Text="DOMINICAN REPUBLIC" Value="DO"></asp:ListItem>
									<asp:ListItem Text="EAST TIMOR" Value="TP"></asp:ListItem>
									<asp:ListItem Text="ECUADOR" Value="EC"></asp:ListItem>
									<asp:ListItem Text="EGYPT" Value="EG"></asp:ListItem>
									<asp:ListItem Text="EL SALVADOR" Value="SV"></asp:ListItem>
									<asp:ListItem Text="EQUATORIAL GUINEA" Value="GQ"></asp:ListItem>
									<asp:ListItem Text="ERITREA" Value="ER"></asp:ListItem>
									<asp:ListItem Text="ESTONIA" Value="EE"></asp:ListItem>
									<asp:ListItem Text="ETHIOPIA" Value="ET"></asp:ListItem>
									<asp:ListItem Text="FAEROE ISLANDS" Value="FO"></asp:ListItem>
									<asp:ListItem Text="FALKLAND ISLANDS (MALVINAS)" Value="FK"></asp:ListItem>
									<asp:ListItem Text="FIJI" Value="FJ"></asp:ListItem>
									<asp:ListItem Text="FINLAND" Value="FI"></asp:ListItem>
									<asp:ListItem Text="FRANCE" Value="FR"></asp:ListItem>
									<asp:ListItem Text="FRENCH GUIANA" Value="GF"></asp:ListItem>
									<asp:ListItem Text="FRENCH POLYNESIA" Value="PF"></asp:ListItem>
									<asp:ListItem Text="FRENCH SOUTHERN TERRITORIES" Value="TF"></asp:ListItem>
									<asp:ListItem Text="GABON" Value="GA"></asp:ListItem>
									<asp:ListItem Text="GAMBIA" Value="GM"></asp:ListItem>
									<asp:ListItem Text="GEORGIA" Value="GE"></asp:ListItem>
									<asp:ListItem Text="GERMANY" Value="DE"></asp:ListItem>
									<asp:ListItem Text="GHANA" Value="GH"></asp:ListItem>
									<asp:ListItem Text="GIBRALTAR" Value="GI"></asp:ListItem>
									<asp:ListItem Text="GREECE" Value="GR"></asp:ListItem>
									<asp:ListItem Text="GREENLAND" Value="GL"></asp:ListItem>
									<asp:ListItem Text="GRENADA" Value="GD"></asp:ListItem>
									<asp:ListItem Text="GUADELOUPE" Value="GP"></asp:ListItem>
									<asp:ListItem Text="GUAM" Value="GU"></asp:ListItem>
									<asp:ListItem Text="GUATEMALA" Value="GT"></asp:ListItem>
									<asp:ListItem Text="GUERNSEY.C.I" Value="GG"></asp:ListItem>
									<asp:ListItem Text="GUINEA" Value="GN"></asp:ListItem>
									<asp:ListItem Text="GUINEA-BISSAU" Value="GW"></asp:ListItem>
									<asp:ListItem Text="GUYANA" Value="GY"></asp:ListItem>
									<asp:ListItem Text="HAITI" Value="HT"></asp:ListItem>
									<asp:ListItem Text="HEARD AND McDonald ISLANDS" Value="HM"></asp:ListItem>
									<asp:ListItem Text="HOLY SEE(VATICAN CITY STATE)" Value="VA"></asp:ListItem>
									<asp:ListItem Text="HONDURAS" Value="HN"></asp:ListItem>
									<asp:ListItem Text="HONG KONG" Value="HK"></asp:ListItem>
									<asp:ListItem Text="HUNGARY" Value="HU"></asp:ListItem>
									<asp:ListItem Text="ICELAND" Value="IS"></asp:ListItem>
									<asp:ListItem Text="INDIA" Value="IN"></asp:ListItem>
									<asp:ListItem Text="INDONESIA" Value="ID"></asp:ListItem>
									<asp:ListItem Text="IRAN(ISLAMIC REPUBLIC OF)" Value="IR"></asp:ListItem>
									<asp:ListItem Text="IRAQ" Value="IQ"></asp:ListItem>
									<asp:ListItem Text="IRELAND" Value="IE"></asp:ListItem>
									<asp:ListItem Text="ISLE OF MAN" Value="IM"></asp:ListItem>
									<asp:ListItem Text="ISRAEL" Value="IL"></asp:ListItem>
									<asp:ListItem Text="ITALY" Value="IT"></asp:ListItem>
									<asp:ListItem Text="JAMAICA" Value="JM"></asp:ListItem>
									<asp:ListItem Text="JAPAN" Value="JP"></asp:ListItem>
									<asp:ListItem Text="JERSEY. C.I" Value="JE"></asp:ListItem>
									<asp:ListItem Text="JORDAN" Value="JO"></asp:ListItem>
									<asp:ListItem Text="KAZAKHSTAN" Value="KZ"></asp:ListItem>
									<asp:ListItem Text="KENYA" Value="KE"></asp:ListItem>
									<asp:ListItem Text="KIRIBATI" Value="KI"></asp:ListItem>
									<asp:ListItem Text="KOREA.DEMOCRATIC PEOPLE'S OF" Value="KP"></asp:ListItem>
									<asp:ListItem Text="KOREA.REPUBLIC OF" Value="KR"></asp:ListItem>
									<asp:ListItem Text="KUWAIT" Value="KW"></asp:ListItem>
									<asp:ListItem Text="KYRGYZSTAN" Value="KG"></asp:ListItem>
									<asp:ListItem Text="LAO PEOPLES DEMOCRATIC REPUBLI" Value="LA"></asp:ListItem>
									<asp:ListItem Text="LATVIA" Value="LV"></asp:ListItem>
									<asp:ListItem Text="LEBANON" Value="LB"></asp:ListItem>
									<asp:ListItem Text="LESOTHO" Value="LS"></asp:ListItem>
									<asp:ListItem Text="LIBERIA" Value="LR"></asp:ListItem>
									<asp:ListItem Text="LIBYAN ARAB JAMAHIRIYA" Value="LY"></asp:ListItem>
									<asp:ListItem Text="LIECHTENSTEIN" Value="LI"></asp:ListItem>
									<asp:ListItem Text="LITHUANIA" Value="LT"></asp:ListItem>
									<asp:ListItem Text="LUXEMBOURG" Value="LU"></asp:ListItem>
									<asp:ListItem Text="MACAU" Value="MO"></asp:ListItem>
									<asp:ListItem Text="MACEDONIA.THE FORMER YUGOSLA" Value="MK"></asp:ListItem>
									<asp:ListItem Text="MADAGASCAR" Value="MG"></asp:ListItem>
									<asp:ListItem Text="MALAWI" Value="MW"></asp:ListItem>
									<asp:ListItem Text="MALAYSIA" Value="MY"></asp:ListItem>
									<asp:ListItem Text="MALDIVES" Value="MV"></asp:ListItem>
									<asp:ListItem Text="MALI" Value="ML"></asp:ListItem>
									<asp:ListItem Text="MALTA" Value="MT"></asp:ListItem>
									<asp:ListItem Text="MARSHALL ISLANDS" Value="MH"></asp:ListItem>
									<asp:ListItem Text="MARTINIQUE" Value="MQ"></asp:ListItem>
									<asp:ListItem Text="MAURITANIA" Value="MR"></asp:ListItem>
									<asp:ListItem Text="MAURITIUS" Value="MU"></asp:ListItem>
									<asp:ListItem Text="MAYOTTE" Value="YT"></asp:ListItem>
									<asp:ListItem Text="MEXICO" Value="MX"></asp:ListItem>
									<asp:ListItem Text="MICRONESIA(FEDERATED STATES OF" Value="FM"></asp:ListItem>
									<asp:ListItem Text="MOLDOVA.REPUBLIC OF" Value="MD"></asp:ListItem>
									<asp:ListItem Text="MONACO" Value="MC"></asp:ListItem>
									<asp:ListItem Text="MONGOLIA" Value="MN"></asp:ListItem>
									<asp:ListItem Text="MONTSERRAT" Value="MS"></asp:ListItem>
									<asp:ListItem Text="MOROCCO" Value="MA"></asp:ListItem>
									<asp:ListItem Text="MOZAMBIQUE" Value="MZ"></asp:ListItem>
									<asp:ListItem Text="MYANMAR" Value="MM"></asp:ListItem>
									<asp:ListItem Text="NAMIBIA" Value="NA"></asp:ListItem>
									<asp:ListItem Text="NAURU" Value="NR"></asp:ListItem>
									<asp:ListItem Text="NEPAL" Value="NP"></asp:ListItem>
									<asp:ListItem Text="NETH.ANTILLES" Value="AN"></asp:ListItem>
									<asp:ListItem Text="NETHERLANDS" Value="NL"></asp:ListItem>
									<asp:ListItem Text="NEW CALEDONIA" Value="NC"></asp:ListItem>
									<asp:ListItem Text="NEW ZEALAND" Value="NZ"></asp:ListItem>
									<asp:ListItem Text="NICARAGUA" Value="NI"></asp:ListItem>
									<asp:ListItem Text="NIGER" Value="NE"></asp:ListItem>
									<asp:ListItem Text="NIGERIA" Value="NG"></asp:ListItem>
									<asp:ListItem Text="NIUE" Value="NU"></asp:ListItem>
									<asp:ListItem Text="NORFOLK ISLAND" Value="NF"></asp:ListItem>
									<asp:ListItem Text="NORTHERN MARIANA ISLANDS" Value="MP"></asp:ListItem>
									<asp:ListItem Text="NORWAY" Value="NO"></asp:ListItem>
									<asp:ListItem Text="OMAN" Value="OM"></asp:ListItem>
									<asp:ListItem Text="PAKISTAN" Value="PK"></asp:ListItem>
									<asp:ListItem Text="PALAU" Value="PW"></asp:ListItem>
									<asp:ListItem Text="Palestinian Territories" Value="PS"></asp:ListItem>
									<asp:ListItem Text="PANAMA" Value="PA"></asp:ListItem>
									<asp:ListItem Text="PANAMA CANAL ZONE" Value="PZ"></asp:ListItem>
									<asp:ListItem Text="PAPUA NEW GUINEA" Value="PG"></asp:ListItem>
									<asp:ListItem Text="PARAGUAY" Value="PY"></asp:ListItem>
									<asp:ListItem Text="PERU" Value="PE"></asp:ListItem>
									<asp:ListItem Text="PHILIPPINES" Value="PH"></asp:ListItem>
									<asp:ListItem Text="PITCAIRN" Value="PN"></asp:ListItem>
									<asp:ListItem Text="POLAND" Value="PL"></asp:ListItem>
									<asp:ListItem Text="PORTUGAL" Value="PT"></asp:ListItem>
									<asp:ListItem Text="PUERTO RICO" Value="PR"></asp:ListItem>
									<asp:ListItem Text="QATAR" Value="QA"></asp:ListItem>
									<asp:ListItem Text="TAIWAN" Value="TW"></asp:ListItem>
									<asp:ListItem Text="REUNION" Value="RE"></asp:ListItem>
									<asp:ListItem Text="ROMANIA" Value="RO"></asp:ListItem>
									<asp:ListItem Text="RUSSIAN FEDERATION" Value="RU"></asp:ListItem>
									<asp:ListItem Text="RWANDA" Value="RW"></asp:ListItem>
									<asp:ListItem Text="SAINT HELENA" Value="SH"></asp:ListItem>
									<asp:ListItem Text="SAINT KITTS AND NEVIS" Value="KN"></asp:ListItem>
									<asp:ListItem Text="SAINT LUCIA" Value="LC"></asp:ListItem>
									<asp:ListItem Text="SAINT PIERRE AND MIQUELON" Value="PM"></asp:ListItem>
									<asp:ListItem Text="SAINT VINCENT AND THE GRENADIN" Value="VC"></asp:ListItem>
									<asp:ListItem Text="SAMOA" Value="WS"></asp:ListItem>
									<asp:ListItem Text="SAN MARINO" Value="SM"></asp:ListItem>
									<asp:ListItem Text="SAO TOME AND PRINCIPE" Value="ST"></asp:ListItem>
									<asp:ListItem Text="SAUDI ARABIA" Value="SA"></asp:ListItem>
									<asp:ListItem Text="SENEGAL" Value="SN"></asp:ListItem>
									<asp:ListItem Text="Serbia" Value="RS"></asp:ListItem>
									<asp:ListItem Text="SERBIA AND MONTENEGRO" Value="CS"></asp:ListItem>
									<asp:ListItem Text="SEYCHELLES" Value="SC"></asp:ListItem>
									<asp:ListItem Text="SIERRA LEONE" Value="SL"></asp:ListItem>
									<asp:ListItem Text="SINGAPORE" Value="SG"></asp:ListItem>
									<asp:ListItem Text="SLOVAKIA" Value="SK"></asp:ListItem>
									<asp:ListItem Text="SLOVENIA" Value="SI"></asp:ListItem>
									<asp:ListItem Text="SOLOMON ISLANDS" Value="SB"></asp:ListItem>
									<asp:ListItem Text="SOMALIA" Value="SO"></asp:ListItem>
									<asp:ListItem Text="SOUTH AFRICA" Value="ZA"></asp:ListItem>
									<asp:ListItem Text="SOUTH GEORGIA AND SOUTH SANDWI" Value="GS"></asp:ListItem>
									<asp:ListItem Text="SPAIN" Value="ES"></asp:ListItem>
									<asp:ListItem Text="SRILANKA" Value="LK"></asp:ListItem>
									<asp:ListItem Text="SUDAN" Value="SD"></asp:ListItem>
									<asp:ListItem Text="SUPRANATIONAL" Value="XP"></asp:ListItem>
									<asp:ListItem Text="SUPRANATIONAL" Value="ZZ"></asp:ListItem>
									<asp:ListItem Text="SURINAME" Value="SR"></asp:ListItem>
									<asp:ListItem Text="SVALBARD AND JAN MAYEN ISLANDS" Value="SJ"></asp:ListItem>
									<asp:ListItem Text="SWAZILAND" Value="SZ"></asp:ListItem>
									<asp:ListItem Text="SWEDEN" Value="SE"></asp:ListItem>
									<asp:ListItem Text="SWITZERLAND" Value="CH"></asp:ListItem>
									<asp:ListItem Text="SYRIAN ARAB REPUBLIC" Value="SY"></asp:ListItem>
									<asp:ListItem Text="TAJIKISTAN" Value="TJ"></asp:ListItem>
									<asp:ListItem Text="TANZANIA.UNITED REPUBLIC OF" Value="TZ"></asp:ListItem>
									<asp:ListItem Text="THAILAND" Value="TH"></asp:ListItem>
									<asp:ListItem Text="TOGO" Value="TG"></asp:ListItem>
									<asp:ListItem Text="TOKELAU" Value="TK"></asp:ListItem>
									<asp:ListItem Text="TONGA" Value="TO"></asp:ListItem>
									<asp:ListItem Text="TRINIDAD AND TOBAGO" Value="TT"></asp:ListItem>
									<asp:ListItem Text="TUNISIA" Value="TN"></asp:ListItem>
									<asp:ListItem Text="TURKEY" Value="TR"></asp:ListItem>
									<asp:ListItem Text="TURKMENISTAN" Value="TM"></asp:ListItem>
									<asp:ListItem Text="TURKS AND CAICONS ISLANDS" Value="TC"></asp:ListItem>
									<asp:ListItem Text="TUVALU" Value="TV"></asp:ListItem>
									<asp:ListItem Text="UGANDA" Value="UG"></asp:ListItem>
									<asp:ListItem Text="UKRAINE" Value="UA"></asp:ListItem>
									<asp:ListItem Text="UNITED ARAB EMIRATES" Value="AE"></asp:ListItem>
									<asp:ListItem Text="UNITED KINGDOM" Value="GB"></asp:ListItem>
									<asp:ListItem Text="UNITED STATES" Value="US"></asp:ListItem>
									<asp:ListItem Text="UNITED STATES MINOR OUTLYING I" Value="UM"></asp:ListItem>
									<asp:ListItem Text="URUGUAY" Value="UY"></asp:ListItem>
									<asp:ListItem Text="UZBEKISTAN" Value="UZ"></asp:ListItem>
									<asp:ListItem Text="VANUATU" Value="VU"></asp:ListItem>
									<asp:ListItem Text="VENEZUELA" Value="VE"></asp:ListItem>
									<asp:ListItem Text="VIETNAM" Value="VN"></asp:ListItem>
									<asp:ListItem Text="VIRGIN ISLANDS U.S" Value="VI"></asp:ListItem>
									<asp:ListItem Text="VIRGIN ISLANDS.BRITISH" Value="VG"></asp:ListItem>
									<asp:ListItem Text="WALLIS AND FUTUNA ISLANDS" Value="WF"></asp:ListItem>
									<asp:ListItem Text="WESTERN SAHARA" Value="EH"></asp:ListItem>
									<asp:ListItem Text="YEMEN" Value="YE"></asp:ListItem>
									<asp:ListItem Text="YUGOSLAVIA" Value="YU"></asp:ListItem>
									<asp:ListItem Text="ZAMBIA" Value="ZM"></asp:ListItem>
									<asp:ListItem Text="ZIMBABWE" Value="ZW"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField12" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Trade Country-OnBdr5:</asp:Label><asp:TextBox id="txtCDDUserField12" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField12" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField12,txtCDDUserField12);">
									<asp:ListItem Text="AFGHANISTAN" Value="AF"></asp:ListItem>
									<asp:ListItem Text="ALBANIA" Value="AL"></asp:ListItem>
									<asp:ListItem Text="ALGERIA" Value="DZ"></asp:ListItem>
									<asp:ListItem Text="AMERICAN SAMOA" Value="AS"></asp:ListItem>
									<asp:ListItem Text="ANDORRA" Value="AD"></asp:ListItem>
									<asp:ListItem Text="ANGOLA" Value="AO"></asp:ListItem>
									<asp:ListItem Text="ANGUILLA" Value="AI"></asp:ListItem>
									<asp:ListItem Text="ANTARCTICA" Value="AQ"></asp:ListItem>
									<asp:ListItem Text="ANTIGUA AND BARBUDA" Value="AG"></asp:ListItem>
									<asp:ListItem Text="ARGENTINA" Value="AR"></asp:ListItem>
									<asp:ListItem Text="ARMENIA" Value="AM"></asp:ListItem>
									<asp:ListItem Text="ARUBA" Value="AW"></asp:ListItem>
									<asp:ListItem Text="AUSTRALIA" Value="AU"></asp:ListItem>
									<asp:ListItem Text="AUSTRIA" Value="AT"></asp:ListItem>
									<asp:ListItem Text="AZERBAIJAN" Value="AZ"></asp:ListItem>
									<asp:ListItem Text="BAHAMAS" Value="BS"></asp:ListItem>
									<asp:ListItem Text="BAHRAIN" Value="BH"></asp:ListItem>
									<asp:ListItem Text="BANGLADESH" Value="BD"></asp:ListItem>
									<asp:ListItem Text="BARBADOS" Value="BB"></asp:ListItem>
									<asp:ListItem Text="BELARUS" Value="BY"></asp:ListItem>
									<asp:ListItem Text="BELGIUM" Value="BE"></asp:ListItem>
									<asp:ListItem Text="BELIZE" Value="BZ"></asp:ListItem>
									<asp:ListItem Text="BENIN" Value="BJ"></asp:ListItem>
									<asp:ListItem Text="BERMUDA" Value="BM"></asp:ListItem>
									<asp:ListItem Text="BHUTAN" Value="BT"></asp:ListItem>
									<asp:ListItem Text="BOLIVIA" Value="BO"></asp:ListItem>
									<asp:ListItem Text="BOSNIA AND HERZEGOVINA" Value="BA"></asp:ListItem>
									<asp:ListItem Text="BOTSWANA" Value="BW"></asp:ListItem>
									<asp:ListItem Text="BOUVET ISLAND" Value="BV"></asp:ListItem>
									<asp:ListItem Text="BRAZIL" Value="BR"></asp:ListItem>
									<asp:ListItem Text="BRITISH INDIAN OCEAN TERRITORY" Value="IO"></asp:ListItem>
									<asp:ListItem Text="BRUNEI DARUSSALAM" Value="BN"></asp:ListItem>
									<asp:ListItem Text="BULGARIA" Value="BG"></asp:ListItem>
									<asp:ListItem Text="BURKINA FASO" Value="BF"></asp:ListItem>
									<asp:ListItem Text="BURUNDI" Value="BI"></asp:ListItem>
									<asp:ListItem Text="CAMBODIA" Value="KH"></asp:ListItem>
									<asp:ListItem Text="CAMEROON" Value="CM"></asp:ListItem>
									<asp:ListItem Text="CANADA" Value="CA"></asp:ListItem>
									<asp:ListItem Text="CAPE VERDE" Value="CV"></asp:ListItem>
									<asp:ListItem Text="CAYMAN ISLANDS" Value="KY"></asp:ListItem>
									<asp:ListItem Text="CENTRAL AFRICAN REPUBLIC" Value="CF"></asp:ListItem>
									<asp:ListItem Text="CHAD" Value="TD"></asp:ListItem>
									<asp:ListItem Text="CHILE" Value="CL"></asp:ListItem>
									<asp:ListItem Text="CHINA" Value="CN"></asp:ListItem>
									<asp:ListItem Text="CHRISTMAS ISLAND" Value="CX"></asp:ListItem>
									<asp:ListItem Text="COCOS (KEELING) ISLANDS" Value="CC"></asp:ListItem>
									<asp:ListItem Text="COLOMBIA" Value="CO"></asp:ListItem>
									<asp:ListItem Text="COMOROS" Value="KM"></asp:ListItem>
									<asp:ListItem Text="CONGO" Value="CG"></asp:ListItem>
									<asp:ListItem Text="CONGO.THE DEMOCRATIC REP.OFTHE" Value="CD"></asp:ListItem>
									<asp:ListItem Text="COOK ISLANDS" Value="CK"></asp:ListItem>
									<asp:ListItem Text="COSTA RICA" Value="CR"></asp:ListItem>
									<asp:ListItem Text="COTE D'IOVIRE" Value="CI"></asp:ListItem>
									<asp:ListItem Text="CROATIA" Value="HR"></asp:ListItem>
									<asp:ListItem Text="CUBA" Value="CU"></asp:ListItem>
									<asp:ListItem Text="CYPRUS" Value="CY"></asp:ListItem>
									<asp:ListItem Text="CZECH REPUBLIC" Value="CZ"></asp:ListItem>
									<asp:ListItem Text="DENMARK" Value="DK"></asp:ListItem>
									<asp:ListItem Text="DJIBOUTI" Value="DJ"></asp:ListItem>
									<asp:ListItem Text="DOMINICA" Value="DM"></asp:ListItem>
									<asp:ListItem Text="DOMINICAN REPUBLIC" Value="DO"></asp:ListItem>
									<asp:ListItem Text="EAST TIMOR" Value="TP"></asp:ListItem>
									<asp:ListItem Text="ECUADOR" Value="EC"></asp:ListItem>
									<asp:ListItem Text="EGYPT" Value="EG"></asp:ListItem>
									<asp:ListItem Text="EL SALVADOR" Value="SV"></asp:ListItem>
									<asp:ListItem Text="EQUATORIAL GUINEA" Value="GQ"></asp:ListItem>
									<asp:ListItem Text="ERITREA" Value="ER"></asp:ListItem>
									<asp:ListItem Text="ESTONIA" Value="EE"></asp:ListItem>
									<asp:ListItem Text="ETHIOPIA" Value="ET"></asp:ListItem>
									<asp:ListItem Text="FAEROE ISLANDS" Value="FO"></asp:ListItem>
									<asp:ListItem Text="FALKLAND ISLANDS (MALVINAS)" Value="FK"></asp:ListItem>
									<asp:ListItem Text="FIJI" Value="FJ"></asp:ListItem>
									<asp:ListItem Text="FINLAND" Value="FI"></asp:ListItem>
									<asp:ListItem Text="FRANCE" Value="FR"></asp:ListItem>
									<asp:ListItem Text="FRENCH GUIANA" Value="GF"></asp:ListItem>
									<asp:ListItem Text="FRENCH POLYNESIA" Value="PF"></asp:ListItem>
									<asp:ListItem Text="FRENCH SOUTHERN TERRITORIES" Value="TF"></asp:ListItem>
									<asp:ListItem Text="GABON" Value="GA"></asp:ListItem>
									<asp:ListItem Text="GAMBIA" Value="GM"></asp:ListItem>
									<asp:ListItem Text="GEORGIA" Value="GE"></asp:ListItem>
									<asp:ListItem Text="GERMANY" Value="DE"></asp:ListItem>
									<asp:ListItem Text="GHANA" Value="GH"></asp:ListItem>
									<asp:ListItem Text="GIBRALTAR" Value="GI"></asp:ListItem>
									<asp:ListItem Text="GREECE" Value="GR"></asp:ListItem>
									<asp:ListItem Text="GREENLAND" Value="GL"></asp:ListItem>
									<asp:ListItem Text="GRENADA" Value="GD"></asp:ListItem>
									<asp:ListItem Text="GUADELOUPE" Value="GP"></asp:ListItem>
									<asp:ListItem Text="GUAM" Value="GU"></asp:ListItem>
									<asp:ListItem Text="GUATEMALA" Value="GT"></asp:ListItem>
									<asp:ListItem Text="GUERNSEY.C.I" Value="GG"></asp:ListItem>
									<asp:ListItem Text="GUINEA" Value="GN"></asp:ListItem>
									<asp:ListItem Text="GUINEA-BISSAU" Value="GW"></asp:ListItem>
									<asp:ListItem Text="GUYANA" Value="GY"></asp:ListItem>
									<asp:ListItem Text="HAITI" Value="HT"></asp:ListItem>
									<asp:ListItem Text="HEARD AND McDonald ISLANDS" Value="HM"></asp:ListItem>
									<asp:ListItem Text="HOLY SEE(VATICAN CITY STATE)" Value="VA"></asp:ListItem>
									<asp:ListItem Text="HONDURAS" Value="HN"></asp:ListItem>
									<asp:ListItem Text="HONG KONG" Value="HK"></asp:ListItem>
									<asp:ListItem Text="HUNGARY" Value="HU"></asp:ListItem>
									<asp:ListItem Text="ICELAND" Value="IS"></asp:ListItem>
									<asp:ListItem Text="INDIA" Value="IN"></asp:ListItem>
									<asp:ListItem Text="INDONESIA" Value="ID"></asp:ListItem>
									<asp:ListItem Text="IRAN(ISLAMIC REPUBLIC OF)" Value="IR"></asp:ListItem>
									<asp:ListItem Text="IRAQ" Value="IQ"></asp:ListItem>
									<asp:ListItem Text="IRELAND" Value="IE"></asp:ListItem>
									<asp:ListItem Text="ISLE OF MAN" Value="IM"></asp:ListItem>
									<asp:ListItem Text="ISRAEL" Value="IL"></asp:ListItem>
									<asp:ListItem Text="ITALY" Value="IT"></asp:ListItem>
									<asp:ListItem Text="JAMAICA" Value="JM"></asp:ListItem>
									<asp:ListItem Text="JAPAN" Value="JP"></asp:ListItem>
									<asp:ListItem Text="JERSEY. C.I" Value="JE"></asp:ListItem>
									<asp:ListItem Text="JORDAN" Value="JO"></asp:ListItem>
									<asp:ListItem Text="KAZAKHSTAN" Value="KZ"></asp:ListItem>
									<asp:ListItem Text="KENYA" Value="KE"></asp:ListItem>
									<asp:ListItem Text="KIRIBATI" Value="KI"></asp:ListItem>
									<asp:ListItem Text="KOREA.DEMOCRATIC PEOPLE'S OF" Value="KP"></asp:ListItem>
									<asp:ListItem Text="KOREA.REPUBLIC OF" Value="KR"></asp:ListItem>
									<asp:ListItem Text="KUWAIT" Value="KW"></asp:ListItem>
									<asp:ListItem Text="KYRGYZSTAN" Value="KG"></asp:ListItem>
									<asp:ListItem Text="LAO PEOPLES DEMOCRATIC REPUBLI" Value="LA"></asp:ListItem>
									<asp:ListItem Text="LATVIA" Value="LV"></asp:ListItem>
									<asp:ListItem Text="LEBANON" Value="LB"></asp:ListItem>
									<asp:ListItem Text="LESOTHO" Value="LS"></asp:ListItem>
									<asp:ListItem Text="LIBERIA" Value="LR"></asp:ListItem>
									<asp:ListItem Text="LIBYAN ARAB JAMAHIRIYA" Value="LY"></asp:ListItem>
									<asp:ListItem Text="LIECHTENSTEIN" Value="LI"></asp:ListItem>
									<asp:ListItem Text="LITHUANIA" Value="LT"></asp:ListItem>
									<asp:ListItem Text="LUXEMBOURG" Value="LU"></asp:ListItem>
									<asp:ListItem Text="MACAU" Value="MO"></asp:ListItem>
									<asp:ListItem Text="MACEDONIA.THE FORMER YUGOSLA" Value="MK"></asp:ListItem>
									<asp:ListItem Text="MADAGASCAR" Value="MG"></asp:ListItem>
									<asp:ListItem Text="MALAWI" Value="MW"></asp:ListItem>
									<asp:ListItem Text="MALAYSIA" Value="MY"></asp:ListItem>
									<asp:ListItem Text="MALDIVES" Value="MV"></asp:ListItem>
									<asp:ListItem Text="MALI" Value="ML"></asp:ListItem>
									<asp:ListItem Text="MALTA" Value="MT"></asp:ListItem>
									<asp:ListItem Text="MARSHALL ISLANDS" Value="MH"></asp:ListItem>
									<asp:ListItem Text="MARTINIQUE" Value="MQ"></asp:ListItem>
									<asp:ListItem Text="MAURITANIA" Value="MR"></asp:ListItem>
									<asp:ListItem Text="MAURITIUS" Value="MU"></asp:ListItem>
									<asp:ListItem Text="MAYOTTE" Value="YT"></asp:ListItem>
									<asp:ListItem Text="MEXICO" Value="MX"></asp:ListItem>
									<asp:ListItem Text="MICRONESIA(FEDERATED STATES OF" Value="FM"></asp:ListItem>
									<asp:ListItem Text="MOLDOVA.REPUBLIC OF" Value="MD"></asp:ListItem>
									<asp:ListItem Text="MONACO" Value="MC"></asp:ListItem>
									<asp:ListItem Text="MONGOLIA" Value="MN"></asp:ListItem>
									<asp:ListItem Text="MONTSERRAT" Value="MS"></asp:ListItem>
									<asp:ListItem Text="MOROCCO" Value="MA"></asp:ListItem>
									<asp:ListItem Text="MOZAMBIQUE" Value="MZ"></asp:ListItem>
									<asp:ListItem Text="MYANMAR" Value="MM"></asp:ListItem>
									<asp:ListItem Text="NAMIBIA" Value="NA"></asp:ListItem>
									<asp:ListItem Text="NAURU" Value="NR"></asp:ListItem>
									<asp:ListItem Text="NEPAL" Value="NP"></asp:ListItem>
									<asp:ListItem Text="NETH.ANTILLES" Value="AN"></asp:ListItem>
									<asp:ListItem Text="NETHERLANDS" Value="NL"></asp:ListItem>
									<asp:ListItem Text="NEW CALEDONIA" Value="NC"></asp:ListItem>
									<asp:ListItem Text="NEW ZEALAND" Value="NZ"></asp:ListItem>
									<asp:ListItem Text="NICARAGUA" Value="NI"></asp:ListItem>
									<asp:ListItem Text="NIGER" Value="NE"></asp:ListItem>
									<asp:ListItem Text="NIGERIA" Value="NG"></asp:ListItem>
									<asp:ListItem Text="NIUE" Value="NU"></asp:ListItem>
									<asp:ListItem Text="NORFOLK ISLAND" Value="NF"></asp:ListItem>
									<asp:ListItem Text="NORTHERN MARIANA ISLANDS" Value="MP"></asp:ListItem>
									<asp:ListItem Text="NORWAY" Value="NO"></asp:ListItem>
									<asp:ListItem Text="OMAN" Value="OM"></asp:ListItem>
									<asp:ListItem Text="PAKISTAN" Value="PK"></asp:ListItem>
									<asp:ListItem Text="PALAU" Value="PW"></asp:ListItem>
									<asp:ListItem Text="Palestinian Territories" Value="PS"></asp:ListItem>
									<asp:ListItem Text="PANAMA" Value="PA"></asp:ListItem>
									<asp:ListItem Text="PANAMA CANAL ZONE" Value="PZ"></asp:ListItem>
									<asp:ListItem Text="PAPUA NEW GUINEA" Value="PG"></asp:ListItem>
									<asp:ListItem Text="PARAGUAY" Value="PY"></asp:ListItem>
									<asp:ListItem Text="PERU" Value="PE"></asp:ListItem>
									<asp:ListItem Text="PHILIPPINES" Value="PH"></asp:ListItem>
									<asp:ListItem Text="PITCAIRN" Value="PN"></asp:ListItem>
									<asp:ListItem Text="POLAND" Value="PL"></asp:ListItem>
									<asp:ListItem Text="PORTUGAL" Value="PT"></asp:ListItem>
									<asp:ListItem Text="PUERTO RICO" Value="PR"></asp:ListItem>
									<asp:ListItem Text="QATAR" Value="QA"></asp:ListItem>
									<asp:ListItem Text="TAIWAN" Value="TW"></asp:ListItem>
									<asp:ListItem Text="REUNION" Value="RE"></asp:ListItem>
									<asp:ListItem Text="ROMANIA" Value="RO"></asp:ListItem>
									<asp:ListItem Text="RUSSIAN FEDERATION" Value="RU"></asp:ListItem>
									<asp:ListItem Text="RWANDA" Value="RW"></asp:ListItem>
									<asp:ListItem Text="SAINT HELENA" Value="SH"></asp:ListItem>
									<asp:ListItem Text="SAINT KITTS AND NEVIS" Value="KN"></asp:ListItem>
									<asp:ListItem Text="SAINT LUCIA" Value="LC"></asp:ListItem>
									<asp:ListItem Text="SAINT PIERRE AND MIQUELON" Value="PM"></asp:ListItem>
									<asp:ListItem Text="SAINT VINCENT AND THE GRENADIN" Value="VC"></asp:ListItem>
									<asp:ListItem Text="SAMOA" Value="WS"></asp:ListItem>
									<asp:ListItem Text="SAN MARINO" Value="SM"></asp:ListItem>
									<asp:ListItem Text="SAO TOME AND PRINCIPE" Value="ST"></asp:ListItem>
									<asp:ListItem Text="SAUDI ARABIA" Value="SA"></asp:ListItem>
									<asp:ListItem Text="SENEGAL" Value="SN"></asp:ListItem>
									<asp:ListItem Text="Serbia" Value="RS"></asp:ListItem>
									<asp:ListItem Text="SERBIA AND MONTENEGRO" Value="CS"></asp:ListItem>
									<asp:ListItem Text="SEYCHELLES" Value="SC"></asp:ListItem>
									<asp:ListItem Text="SIERRA LEONE" Value="SL"></asp:ListItem>
									<asp:ListItem Text="SINGAPORE" Value="SG"></asp:ListItem>
									<asp:ListItem Text="SLOVAKIA" Value="SK"></asp:ListItem>
									<asp:ListItem Text="SLOVENIA" Value="SI"></asp:ListItem>
									<asp:ListItem Text="SOLOMON ISLANDS" Value="SB"></asp:ListItem>
									<asp:ListItem Text="SOMALIA" Value="SO"></asp:ListItem>
									<asp:ListItem Text="SOUTH AFRICA" Value="ZA"></asp:ListItem>
									<asp:ListItem Text="SOUTH GEORGIA AND SOUTH SANDWI" Value="GS"></asp:ListItem>
									<asp:ListItem Text="SPAIN" Value="ES"></asp:ListItem>
									<asp:ListItem Text="SRILANKA" Value="LK"></asp:ListItem>
									<asp:ListItem Text="SUDAN" Value="SD"></asp:ListItem>
									<asp:ListItem Text="SUPRANATIONAL" Value="XP"></asp:ListItem>
									<asp:ListItem Text="SUPRANATIONAL" Value="ZZ"></asp:ListItem>
									<asp:ListItem Text="SURINAME" Value="SR"></asp:ListItem>
									<asp:ListItem Text="SVALBARD AND JAN MAYEN ISLANDS" Value="SJ"></asp:ListItem>
									<asp:ListItem Text="SWAZILAND" Value="SZ"></asp:ListItem>
									<asp:ListItem Text="SWEDEN" Value="SE"></asp:ListItem>
									<asp:ListItem Text="SWITZERLAND" Value="CH"></asp:ListItem>
									<asp:ListItem Text="SYRIAN ARAB REPUBLIC" Value="SY"></asp:ListItem>
									<asp:ListItem Text="TAJIKISTAN" Value="TJ"></asp:ListItem>
									<asp:ListItem Text="TANZANIA.UNITED REPUBLIC OF" Value="TZ"></asp:ListItem>
									<asp:ListItem Text="THAILAND" Value="TH"></asp:ListItem>
									<asp:ListItem Text="TOGO" Value="TG"></asp:ListItem>
									<asp:ListItem Text="TOKELAU" Value="TK"></asp:ListItem>
									<asp:ListItem Text="TONGA" Value="TO"></asp:ListItem>
									<asp:ListItem Text="TRINIDAD AND TOBAGO" Value="TT"></asp:ListItem>
									<asp:ListItem Text="TUNISIA" Value="TN"></asp:ListItem>
									<asp:ListItem Text="TURKEY" Value="TR"></asp:ListItem>
									<asp:ListItem Text="TURKMENISTAN" Value="TM"></asp:ListItem>
									<asp:ListItem Text="TURKS AND CAICONS ISLANDS" Value="TC"></asp:ListItem>
									<asp:ListItem Text="TUVALU" Value="TV"></asp:ListItem>
									<asp:ListItem Text="UGANDA" Value="UG"></asp:ListItem>
									<asp:ListItem Text="UKRAINE" Value="UA"></asp:ListItem>
									<asp:ListItem Text="UNITED ARAB EMIRATES" Value="AE"></asp:ListItem>
									<asp:ListItem Text="UNITED KINGDOM" Value="GB"></asp:ListItem>
									<asp:ListItem Text="UNITED STATES" Value="US"></asp:ListItem>
									<asp:ListItem Text="UNITED STATES MINOR OUTLYING I" Value="UM"></asp:ListItem>
									<asp:ListItem Text="URUGUAY" Value="UY"></asp:ListItem>
									<asp:ListItem Text="UZBEKISTAN" Value="UZ"></asp:ListItem>
									<asp:ListItem Text="VANUATU" Value="VU"></asp:ListItem>
									<asp:ListItem Text="VENEZUELA" Value="VE"></asp:ListItem>
									<asp:ListItem Text="VIETNAM" Value="VN"></asp:ListItem>
									<asp:ListItem Text="VIRGIN ISLANDS U.S" Value="VI"></asp:ListItem>
									<asp:ListItem Text="VIRGIN ISLANDS.BRITISH" Value="VG"></asp:ListItem>
									<asp:ListItem Text="WALLIS AND FUTUNA ISLANDS" Value="WF"></asp:ListItem>
									<asp:ListItem Text="WESTERN SAHARA" Value="EH"></asp:ListItem>
									<asp:ListItem Text="YEMEN" Value="YE"></asp:ListItem>
									<asp:ListItem Text="YUGOSLAVIA" Value="YU"></asp:ListItem>
									<asp:ListItem Text="ZAMBIA" Value="ZM"></asp:ListItem>
									<asp:ListItem Text="ZIMBABWE" Value="ZW"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField13" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Transaction Channels-OnBdr:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField13" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX" disabled="disabled"></asp:TextBox></td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField14" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Transaction Channels-OnBdr1:</asp:Label><asp:TextBox id="txtCDDUserField14" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField14" runat="server" CssClass="DropDownList" Width="528px" onchange="tmpSource_onchange(ddlCDDUserField14,txtCDDUserField14);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Branch" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Fax Instructions" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Email" Value="3"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField15" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Transaction Channels-OnBdr2:</asp:Label><asp:TextBox id="txtCDDUserField15" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField15" runat="server" CssClass="DropDownList" Width="528px" onchange="tmpSource_onchange(ddlCDDUserField15,txtCDDUserField15);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Branch" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Fax Instructions" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Email" Value="3"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField16" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Transaction Channels-OnBdr3:</asp:Label><asp:TextBox id="txtCDDUserField16" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField16" runat="server" CssClass="DropDownList" Width="528px" onchange="tmpSource_onchange(ddlCDDUserField16,txtCDDUserField16);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Branch" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Fax Instructions" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Email" Value="3"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField17" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Customer Type-OnBdr:</asp:Label><asp:TextBox id="txtCDDUserField17" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddltob_I" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddltob_I,txtCDDUserField17);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Individual" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Sole Proprietor" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Minor" Value="3"></asp:ListItem>
									<asp:ListItem Text="4.Student" Value="4"></asp:ListItem>
									<asp:ListItem Text="5.Financial Dependant/Family support" Value="5"></asp:ListItem>
									<asp:ListItem Text="6.Staff" Value="6"></asp:ListItem>
								</asp:DropDownList>
								<asp:DropDownList id="ddltob_E" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddltob_E,txtCDDUserField17);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Private Company" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Listed Company" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Close Corporation" Value="3"></asp:ListItem>
									<asp:ListItem Text="4.Partnership" Value="4"></asp:ListItem>
									<asp:ListItem Text="5.Trust" Value="5"></asp:ListItem>
									<asp:ListItem Text="6.Association/Informal Body" Value="6"></asp:ListItem>
									<asp:ListItem Text="7.Religious Organization / Church/Charity" Value="7"></asp:ListItem>
									<asp:ListItem Text="8.Other legal person (Club)" Value="8"></asp:ListItem>
								</asp:DropDownList>
								<asp:DropDownList id="ddltob_B" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddltob_B,txtCDDUserField17);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Publick Bank" Value="1.Publick Bank"></asp:ListItem>
									<asp:ListItem Text="2.Private Bank" Value="2.Private Bank"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField18" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Customer Status-OnBdr:</asp:Label><asp:TextBox id="txtCDDUserField18" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlcuststat_i" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlcuststat_i,txtCDDUserField18);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.SA National/Resident" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Foreign National" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Staff" Value="3"></asp:ListItem>
								</asp:DropDownList>
								<asp:DropDownList id="ddlcuststat_e" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlcuststat_e,txtCDDUserField18);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.SA Entity" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Foreign Entity" Value="2"></asp:ListItem>
								</asp:DropDownList>
								<asp:DropDownList id="ddlcuststat_b" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlcuststat_b,txtCDDUserField18);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField19" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">PEP Status-OnBdr:</asp:Label><asp:TextBox id="txtCDDUserField19" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField19" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField19,txtCDDUserField19);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Non-PEP" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.PEP Local" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.PIP" Value="3"></asp:ListItem>
									<asp:ListItem Text="4.Connected Person" Value="4"></asp:ListItem>
									<asp:ListItem Text="5.PEP Foreign" Value="5"></asp:ListItem>
									<asp:ListItem Text="6.Previous PEP > 10 Years" Value="6"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField20" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Reverification Channel-OnBdr:</asp:Label><asp:TextBox id="txtCDDUserField20" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField20" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField20,txtCDDUserField20);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Face-to-Face" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Non Face-to-Face" Value="2"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField21" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Sanction List-OnBdr:</asp:Label><asp:TextBox id="txtCDDUserField21" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField21" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField21,txtCDDUserField21);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.No sanctions" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Sanctioned Person/Entity" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Previously Sanctioned Person/Entity" Value="3"></asp:ListItem>
									<asp:ListItem Text="4.Other Lists" Value="4"></asp:ListItem>
									<asp:ListItem Text="5.Head Office High Concern List" Value="5"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField22" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">ShareHolder Listing-OnBdr:</asp:Label><asp:TextBox id="txtCDDUserField22" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlCDDUserField22" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField22,txtCDDUserField22);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Listed" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Trust" Value="2"></asp:ListItem>
									<asp:ListItem Text="3.PEP Foreign" Value="3"></asp:ListItem>
									<asp:ListItem Text="4.Not Listed" Value="4"></asp:ListItem>
									<asp:ListItem Text="5.Complicated Structure" Value="5"></asp:ListItem>
									<asp:ListItem Text="6.Foreign Shareholding" Value="6"></asp:ListItem>
									<asp:ListItem Text="7.Simple structurel" Value="7"></asp:ListItem>
									<asp:ListItem Text="8.PEP Local" Value="8"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField23" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Mgr. OverrideOnBdr:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField23" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField24" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Industry-OnBdr:</asp:Label><asp:TextBox id="txtCDDUserField24" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td>
								<asp:DropDownList id="ddlIndustry_i" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlIndustry_i,txtCDDUserField24);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Unemployed  family support" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Student  family support." Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Minor  family support." Value="3"></asp:ListItem>
									<asp:ListItem Text="4.Staff BOTSA" Value="4"></asp:ListItem>
									<asp:ListItem Text="5.Accountant" Value="5"></asp:ListItem>
									<asp:ListItem Text="6.Amusement and recreational" Value="6"></asp:ListItem>
									<asp:ListItem Text="7.Artisans" Value="7"></asp:ListItem>
									<asp:ListItem Text="8.Attorney" Value="8"></asp:ListItem>
									<asp:ListItem Text="9.Automotive dealers and repairs" Value="9"></asp:ListItem>
									<asp:ListItem Text="10.Building materials and garden supplies" Value="10"></asp:ListItem>
									<asp:ListItem Text="11.Business dealing in second hand engines, car and body and spare parts  employee in non-managerial position." Value="11"></asp:ListItem>
									<asp:ListItem Text="12.Business services" Value="12"></asp:ListItem>
									<asp:ListItem Text="13.Chemical and allied products" Value="13"></asp:ListItem>
									<asp:ListItem Text="14.Communications" Value="14"></asp:ListItem>
									<asp:ListItem Text="15.Construction" Value="15"></asp:ListItem>
									<asp:ListItem Text="16.Dealing in clothing  owner/director/senior management position." Value="16"></asp:ListItem>
									<asp:ListItem Text="17.Educational services" Value="17"></asp:ListItem>
									<asp:ListItem Text="18.Educational services" Value="18"></asp:ListItem>
									<asp:ListItem Text="19.Engineers" Value="19"></asp:ListItem>
									<asp:ListItem Text="20.Farming" Value="20"></asp:ListItem>
									<asp:ListItem Text="21.Financial institutions" Value="21"></asp:ListItem>
									<asp:ListItem Text="22.Fire arms/ammunition/explosives  employee in non-managerial position." Value="22"></asp:ListItem>
									<asp:ListItem Text="23.Fire arms/Ammunition/Explosives-Business owner/person in senior management position." Value="23"></asp:ListItem>
									<asp:ListItem Text="24.Fishing/hunting  employee not in managerial position." Value="24"></asp:ListItem>
									<asp:ListItem Text="25.Fishing/hunting  owner/director/senior management position." Value="25"></asp:ListItem>
									<asp:ListItem Text="26.Freight services - International" Value="26"></asp:ListItem>
									<asp:ListItem Text="27.Freight services not related to International trade." Value="27"></asp:ListItem>
									<asp:ListItem Text="28.Government/State owned enterprise/Municipal employee." Value="28"></asp:ListItem>
									<asp:ListItem Text="29.Government/State owned enterprise/Municipal position (Political Appointment)  Director General/CEO/Mayor." Value="29"></asp:ListItem>
									<asp:ListItem Text="30.Health services" Value="30"></asp:ListItem>
									<asp:ListItem Text="31.Import and/or exports business  owner/director/senior management position." Value="31"></asp:ListItem>
									<asp:ListItem Text="32.Industrial Machinery and equipment" Value="32"></asp:ListItem>
									<asp:ListItem Text="33.Instruments and related products" Value="33"></asp:ListItem>
									<asp:ListItem Text="34.Insurance industry" Value="34"></asp:ListItem>
									<asp:ListItem Text="35.Leather and allied products" Value="35"></asp:ListItem>
									<asp:ListItem Text="36.Mining not related to Precious metals or stones" Value="36"></asp:ListItem>
									<asp:ListItem Text="37.Mining of precious metals/stones" Value="37"></asp:ListItem>
									<asp:ListItem Text="38.Miscellaneous Manufacturing Industries" Value="38"></asp:ListItem>
									<asp:ListItem Text="39.Manufacturing - Plastics" Value="39"></asp:ListItem>
									<asp:ListItem Text="40.National security (military etc.)" Value="40"></asp:ListItem>
									<asp:ListItem Text="41.Organisations dependant on financial support from public or government  persons in senior management positions." Value="41"></asp:ListItem>
									<asp:ListItem Text="42.Pensioner (Provide proof of income)" Value="42"></asp:ListItem>
									<asp:ListItem Text="43.Polices/prosecuting services" Value="43"></asp:ListItem>
									<asp:ListItem Text="44.Precious metals/stones  Employee in non-managerial position." Value="44"></asp:ListItem>
									<asp:ListItem Text="45.Precious metals/stones  Owner/senior management position." Value="45"></asp:ListItem>
									<asp:ListItem Text="46.Private security" Value="46"></asp:ListItem>
									<asp:ListItem Text="47.Property investment company  owner/director/senior manager." Value="47"></asp:ListItem>
									<asp:ListItem Text="48.Religious organisations" Value="48"></asp:ListItem>
									<asp:ListItem Text="49.Restaurant employee" Value="49"></asp:ListItem>
									<asp:ListItem Text="50.Restaurant owner." Value="50"></asp:ListItem>
									<asp:ListItem Text="51.Retail business  owner/director/senior management position." Value="51"></asp:ListItem>
									<asp:ListItem Text="52.Scrap metal  Owner/director/senior management position." Value="52"></asp:ListItem>
									<asp:ListItem Text="53.Second hand engines, car body and spare parts  Owner/director/senior manager." Value="53"></asp:ListItem>
									<asp:ListItem Text="54.Senior government/State owned enterprise/municipal employee (Manager/Director)." Value="54"></asp:ListItem>
									<asp:ListItem Text="55.Senior political position" Value="55"></asp:ListItem>
									<asp:ListItem Text="56.Social services" Value="56"></asp:ListItem>
									<asp:ListItem Text="57.Textile products  owner/director/senior management position." Value="57"></asp:ListItem>
									<asp:ListItem Text="58.Textile/clothing  employee in non-managerial position." Value="58"></asp:ListItem>
									<asp:ListItem Text="59.Tobacco products  owner/director/senior management position." Value="59"></asp:ListItem>
									<asp:ListItem Text="60.Tourism Industry" Value="60"></asp:ListItem>
									<asp:ListItem Text="61.Trucking and warehousing" Value="61"></asp:ListItem>
									<asp:ListItem Text="62.Wholesale business  owner/director/senior management position." Value="62"></asp:ListItem>
									<asp:ListItem Text="63.estate agent" Value="63"></asp:ListItem>
									<asp:ListItem Text="64.Investment customer earning interest only" Value="64"></asp:ListItem>
									<asp:ListItem Text="65.Taipei Laison-Employee" Value="65"></asp:ListItem>
									<asp:ListItem Text="66.Taipei Laison-Representative" Value="66"></asp:ListItem>
									<asp:ListItem Text="67.Bank employee" Value="67"></asp:ListItem>
								</asp:DropDownList>
								<asp:DropDownList id="ddlIndustry_e" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlIndustry_e,txtCDDUserField24);">
									<asp:ListItem Text="1.Accountancy business" Value="1"></asp:ListItem>
									<asp:ListItem Text="2.Amusement parks, etc." Value="2"></asp:ListItem>
									<asp:ListItem Text="3.Attorney" Value="3"></asp:ListItem>
									<asp:ListItem Text="4.Automotive dealers and repairs" Value="4"></asp:ListItem>
									<asp:ListItem Text="5.Building materials and garden supplies" Value="5"></asp:ListItem>
									<asp:ListItem Text="6.Business services" Value="6"></asp:ListItem>
									<asp:ListItem Text="7.Chemical and allied products" Value="7"></asp:ListItem>
									<asp:ListItem Text="8.Clothing" Value="8"></asp:ListItem>
									<asp:ListItem Text="9.Communications" Value="9"></asp:ListItem>
									<asp:ListItem Text="10.Construction" Value="10"></asp:ListItem>
									<asp:ListItem Text="11.Educational services" Value="11"></asp:ListItem>
									<asp:ListItem Text="12.Electrical/electronic suppliers and services" Value="12"></asp:ListItem>
									<asp:ListItem Text="13.Engineering and management services" Value="13"></asp:ListItem>
									<asp:ListItem Text="14.Fabricated metal products" Value="14"></asp:ListItem>
									<asp:ListItem Text="15.Farming" Value="15"></asp:ListItem>
									<asp:ListItem Text="16.Financial institutions" Value="16"></asp:ListItem>
									<asp:ListItem Text="17.Fire arms/Ammunition/ Explosives." Value="17"></asp:ListItem>
									<asp:ListItem Text="18.Fishing/hunting and trapping" Value="18"></asp:ListItem>
									<asp:ListItem Text="19.Food and kindred products" Value="19"></asp:ListItem>
									<asp:ListItem Text="20.Food manufacturing/stores" Value="20"></asp:ListItem>
									<asp:ListItem Text="21.Forestry" Value="21"></asp:ListItem>
									<asp:ListItem Text="22.Freight services  big well known international freight companies i.e. MSC." Value="22"></asp:ListItem>
									<asp:ListItem Text="23.Freight services  small to medium sized companies" Value="23"></asp:ListItem>
									<asp:ListItem Text="24.Freight services (Local)" Value="24"></asp:ListItem>
									<asp:ListItem Text="25.Furniture and fittings" Value="25"></asp:ListItem>
									<asp:ListItem Text="26.General Merchandise stores" Value="26"></asp:ListItem>
									<asp:ListItem Text="27.Government/State owned enterprise/Municipality" Value="27"></asp:ListItem>
									<asp:ListItem Text="28.Health services" Value="28"></asp:ListItem>
									<asp:ListItem Text="29.Hotels and other lodging places" Value="29"></asp:ListItem>
									<asp:ListItem Text="30.Imports and exports" Value="30"></asp:ListItem>
									<asp:ListItem Text="31.Industrial Machinery and equipment" Value="31"></asp:ListItem>
									<asp:ListItem Text="32.Instruments and related products" Value="32"></asp:ListItem>
									<asp:ListItem Text="33.Insurance agents, broker services" Value="33"></asp:ListItem>
									<asp:ListItem Text="34.Insurance industry" Value="34"></asp:ListItem>
									<asp:ListItem Text="35.Leather and allied products" Value="35"></asp:ListItem>
									<asp:ListItem Text="36.Membership organisations" Value="36"></asp:ListItem>
									<asp:ListItem Text="37.Mining not related to precious metals or stones" Value="37"></asp:ListItem>
									<asp:ListItem Text="38.Mining of precious metals/stones" Value="38"></asp:ListItem>
									<asp:ListItem Text="39.Miscellaneous Manufacturing Industries" Value="39"></asp:ListItem>
									<asp:ListItem Text="40.National security (military etc.)" Value="40"></asp:ListItem>
									<asp:ListItem Text="41.Organisations dependant on financial support from public or government" Value="41"></asp:ListItem>
									<asp:ListItem Text="42.Plumbing supplies and services" Value="42"></asp:ListItem>
									<asp:ListItem Text="43.Police/prosecuting services" Value="43"></asp:ListItem>
									<asp:ListItem Text="44.Precious metals/stones." Value="44"></asp:ListItem>
									<asp:ListItem Text="45.Private security" Value="45"></asp:ListItem>
									<asp:ListItem Text="46.Project management services" Value="46"></asp:ListItem>
									<asp:ListItem Text="47.Property investment" Value="47"></asp:ListItem>
									<asp:ListItem Text="48.Recreational i.e. Golf estate" Value="48"></asp:ListItem>
									<asp:ListItem Text="49.Religious organisations" Value="49"></asp:ListItem>
									<asp:ListItem Text="50.Respondent bank (Correspondent bank relationship)." Value="50"></asp:ListItem>
									<asp:ListItem Text="51.Restaurant" Value="51"></asp:ListItem>
									<asp:ListItem Text="52.Retail (general) not specifically mentioned" Value="52"></asp:ListItem>
									<asp:ListItem Text="53.Second hand engines, car body and spare parts." Value="53"></asp:ListItem>
									<asp:ListItem Text="54.Senior political position" Value="54"></asp:ListItem>
									<asp:ListItem Text="55.Social services" Value="55"></asp:ListItem>
									<asp:ListItem Text="56.Textile products." Value="56"></asp:ListItem>
									<asp:ListItem Text="57.Tobacco and related products." Value="57"></asp:ListItem>
									<asp:ListItem Text="58.Tourism Industry" Value="58"></asp:ListItem>
									<asp:ListItem Text="59.Travel service i.e. car rental" Value="59"></asp:ListItem>
									<asp:ListItem Text="60.Trucking and warehousing" Value="60"></asp:ListItem>
									<asp:ListItem Text="61.Wholesale (general) not specifically mentioned" Value="61"></asp:ListItem>
								</asp:DropDownList>
								<asp:DropDownList id="ddlIndustry_b" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlIndustry_b,txtCDDUserField24);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:label id="lblCDDUserField25" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">Risk Rating Code:</asp:label><asp:textbox id="txtCDDUserField25" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 266px">
								<asp:DropDownList id="ddlCDDUserField25" runat="server" CssClass="DropDownList" Width="528px" onchange="tmpSource_onchange(ddlCDDUserField25,txtCDDUserField25);">
									<asp:ListItem Text="00.00" Value="00"></asp:ListItem>
									<asp:ListItem Text="01.Politically Exposed Person" Value="01"></asp:ListItem>
									<asp:ListItem Text="02.Minor" Value="02"></asp:ListItem>
									<asp:ListItem Text="03.Student" Value="03"></asp:ListItem>
									<asp:ListItem Text="04.FIC" Value="04"></asp:ListItem>
									<asp:ListItem Text="05.SARS" Value="05"></asp:ListItem>
									<asp:ListItem Text="06.FSD" Value="06"></asp:ListItem>
									<asp:ListItem Text="07.LAW" Value="07"></asp:ListItem>
									<asp:ListItem Text="08.National Prosecuting Authority" Value="08"></asp:ListItem>
									<asp:ListItem Text="09.Risk" Value="09"></asp:ListItem>
									<asp:ListItem Text="10.Mgr.Override" Value="10"></asp:ListItem>
									<asp:ListItem Text="11.High Risk Country" Value="11"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField26" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="Red">Source of Funds1:</asp:Label><asp:TextBox id="txtCDDUserField26" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td style="WIDTH: 266px">
								<asp:DropDownList id="ddlCDDUserField26" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField26,txtCDDUserField26);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Company profits" Value="Company profits"></asp:ListItem>
									<asp:ListItem Text="2.Company sale or sale of interest in company" Value="Company sale or sale of interest in company"></asp:ListItem>
									<asp:ListItem Text="3.Court Order" Value="Court Order"></asp:ListItem>
									<asp:ListItem Text="4.Gift/donation" Value="Gift/donation"></asp:ListItem>
									<asp:ListItem Text="5.Loan" Value="Loan"></asp:ListItem>
									<asp:ListItem Text="6.Maturing investments" Value="Maturing investments"></asp:ListItem>
									<asp:ListItem Text="7.Pension" Value="Pension"></asp:ListItem>
									<asp:ListItem Text="8.Sale of shares" Value="Sale of shares"></asp:ListItem>
									<asp:ListItem Text="9.Divorce settlement" Value="Divorce settlement"></asp:ListItem>
									<asp:ListItem Text="10.Lottery/gambling winnings" Value="Lottery/gambling winnings"></asp:ListItem>
									<asp:ListItem Text="11.Dividends from investments" Value="Dividends from investments"></asp:ListItem>
									<asp:ListItem Text="12.Encashment claim" Value="Encashment claim"></asp:ListItem>
									<asp:ListItem Text="13.Income from previous employment" Value="Income from previous employment"></asp:ListItem>
									<asp:ListItem Text="14.New investment/capital" Value="New investment/capital"></asp:ListItem>
									<asp:ListItem Text="15.Savings" Value="Savings"></asp:ListItem>
									<asp:ListItem Text="16.Inheritance" Value="Inheritance"></asp:ListItem>
									<asp:ListItem Text="17.Sale of property" Value="Sale of property"></asp:ListItem>
									<asp:ListItem Text="18.Deceased estates" Value="Deceased estates"></asp:ListItem>
									<asp:ListItem Text="19.Cryptocurrency" Value="Cryptocurrency"></asp:ListItem>
									<asp:ListItem Text="20.Rental income" Value="Rental income"></asp:ListItem>
									<asp:ListItem Text="21.Trust Distributions" Value="Trust Distributions"></asp:ListItem>
									<asp:ListItem Text="22.and other" Value="and other"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField27" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="Red">Source of Funds1 AMT:</asp:Label></td>
							<td style="WIDTH: 266px">
								<asp:TextBox id="txtCDDUserField27" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField28" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="Red">Source of Funds2:</asp:Label><asp:TextBox id="txtCDDUserField28" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td style="WIDTH: 266px">
								<asp:DropDownList id="ddlCDDUserField28" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField28,txtCDDUserField28);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Company profits" Value="Company profits"></asp:ListItem>
									<asp:ListItem Text="2.Company sale or sale of interest in company" Value="Company sale or sale of interest in company"></asp:ListItem>
									<asp:ListItem Text="3.Court Order" Value="Court Order"></asp:ListItem>
									<asp:ListItem Text="4.Gift/donation" Value="Gift/donation"></asp:ListItem>
									<asp:ListItem Text="5.Loan" Value="Loan"></asp:ListItem>
									<asp:ListItem Text="6.Maturing investments" Value="Maturing investments"></asp:ListItem>
									<asp:ListItem Text="7.Pension" Value="Pension"></asp:ListItem>
									<asp:ListItem Text="8.Sale of shares" Value="Sale of shares"></asp:ListItem>
									<asp:ListItem Text="9.Divorce settlement" Value="Divorce settlement"></asp:ListItem>
									<asp:ListItem Text="10.Lottery/gambling winnings" Value="Lottery/gambling winnings"></asp:ListItem>
									<asp:ListItem Text="11.Dividends from investments" Value="Dividends from investments"></asp:ListItem>
									<asp:ListItem Text="12.Encashment claim" Value="Encashment claim"></asp:ListItem>
									<asp:ListItem Text="13.Income from previous employment" Value="Income from previous employment"></asp:ListItem>
									<asp:ListItem Text="14.New investment/capital" Value="New investment/capital"></asp:ListItem>
									<asp:ListItem Text="15.Savings" Value="Savings"></asp:ListItem>
									<asp:ListItem Text="16.Inheritance" Value="Inheritance"></asp:ListItem>
									<asp:ListItem Text="17.Sale of property" Value="Sale of property"></asp:ListItem>
									<asp:ListItem Text="18.Deceased estates" Value="Deceased estates"></asp:ListItem>
									<asp:ListItem Text="19.Cryptocurrency" Value="Cryptocurrency"></asp:ListItem>
									<asp:ListItem Text="20.Rental income" Value="Rental income"></asp:ListItem>
									<asp:ListItem Text="21.Trust Distributions" Value="Trust Distributions"></asp:ListItem>
									<asp:ListItem Text="22.and other" Value="and other"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField29" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="Red">Source of Funds2 AMT:</asp:Label></td>
							<td style="WIDTH: 266px">
								<asp:TextBox id="txtCDDUserField29" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField30" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="Red">Source of Funds3:</asp:Label><asp:TextBox id="txtCDDUserField30" runat="server" Height="24px" Width="0px" CssClass="TEXTBOX"></asp:TextBox></td>
							<td style="WIDTH: 266px">
								<asp:DropDownList id="ddlCDDUserField30" runat="server" Width="528px" CssClass="DropDownList" onchange="tmpSource_onchange(ddlCDDUserField30,txtCDDUserField30);">
									<asp:ListItem Text="" Value=""></asp:ListItem>
									<asp:ListItem Text="1.Company profits" Value="Company profits"></asp:ListItem>
									<asp:ListItem Text="2.Company sale or sale of interest in company" Value="Company sale or sale of interest in company"></asp:ListItem>
									<asp:ListItem Text="3.Court Order" Value="Court Order"></asp:ListItem>
									<asp:ListItem Text="4.Gift/donation" Value="Gift/donation"></asp:ListItem>
									<asp:ListItem Text="5.Loan" Value="Loan"></asp:ListItem>
									<asp:ListItem Text="6.Maturing investments" Value="Maturing investments"></asp:ListItem>
									<asp:ListItem Text="7.Pension" Value="Pension"></asp:ListItem>
									<asp:ListItem Text="8.Sale of shares" Value="Sale of shares"></asp:ListItem>
									<asp:ListItem Text="9.Divorce settlement" Value="Divorce settlement"></asp:ListItem>
									<asp:ListItem Text="10.Lottery/gambling winnings" Value="Lottery/gambling winnings"></asp:ListItem>
									<asp:ListItem Text="11.Dividends from investments" Value="Dividends from investments"></asp:ListItem>
									<asp:ListItem Text="12.Encashment claim" Value="Encashment claim"></asp:ListItem>
									<asp:ListItem Text="13.Income from previous employment" Value="Income from previous employment"></asp:ListItem>
									<asp:ListItem Text="14.New investment/capital" Value="New investment/capital"></asp:ListItem>
									<asp:ListItem Text="15.Savings" Value="Savings"></asp:ListItem>
									<asp:ListItem Text="16.Inheritance" Value="Inheritance"></asp:ListItem>
									<asp:ListItem Text="17.Sale of property" Value="Sale of property"></asp:ListItem>
									<asp:ListItem Text="18.Deceased estates" Value="Deceased estates"></asp:ListItem>
									<asp:ListItem Text="19.Cryptocurrency" Value="Cryptocurrency"></asp:ListItem>
									<asp:ListItem Text="20.Rental income" Value="Rental income"></asp:ListItem>
									<asp:ListItem Text="21.Trust Distributions" Value="Trust Distributions"></asp:ListItem>
									<asp:ListItem Text="22.and other" Value="and other"></asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField31" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="Red">Source of Funds3 AMT:</asp:Label></td>
							<td style="WIDTH: 266px">
								<asp:TextBox id="txtCDDUserField31" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField32" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">COMMENTS LINE 1:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField32" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField33" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">COMMENTS LINE 2:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField33" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField34" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">COMMENTS LINE 3:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField34" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField35" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">COMMENTS LINE 4:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField35" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField36" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">COMMENTS LINE 5:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField36" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField37" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">CHECK LISTS - FORM TYPE:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField37" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField38" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">RE-VERIFICATION DATE:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField38" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField39" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">PROPOSED RISK:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField39" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td align="right">
								<asp:Label id="lblCDDUserField40" runat="server" Height="12px" Width="136px" CssClass="LABEL" ForeColor="blue">COMMENTS:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField40" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
					</table>
				</asp:panel>
				<!-- PartyAlias panel---->
				<asp:panel id="KYCPanel5" runat="server" Height="15px" Width="712px" cssclass="Panel">
					<input id="DisplayAddPartyAlias" onclick="HideAndDisplayPartyAlias('display')" type="button" value="New Alias"
						name="DisplayAddPartyAlias" runat="server"/>
					<div id="popUpAddModifyPartyAlias" style="DISPLAY: none">
						<div id="DIVPARTYALIASWARNING">
							<h5 id="PARTYALIASWarning" style="COLOR: red"></h5>
						</div>
						<table id="AllPartyAlias1" border="1" runat="server">
							<tr>
								<td>
									<table class="KYCtable" id="AllPartyAliases" bgColor="#990000" border="1">
										<tr class="KYCtabletr">
											<td>*Alias</td>
											<td><input id="Aliascell0" type="text" maxlength="40"/></td>
										</tr>										
									</table>
								</td>
							</tr>
							<tr>
								<td><input id="addpartyalias" onclick="AddOrModifyPartyAlias('ADD', '')" type="button" value="Add"/> <input id="hidepartyalias" onclick="HideAndDisplayPartyAlias('hide')" type="button" value="Cancel"/>
								</td>
							</tr>
						</table>
					</div>
					<input id="hFldPartyAliases" type="hidden" name="hFldPartyAliases" runat="server"/> <input id="hDeletedPartyAliases" type="hidden" name="hDeletedPartyAliases" runat="server"/>
					<table class="KYCtable" id="PartyAlias" border="1" runat="server">
						<tr class="KYCtabletr">
							<th>
								Alias</th>
								<th>
								Action</th>
							</tr>
					</table>
				</asp:panel>
				<!-- PartyAlias panel --->
			</asp:panel><asp:panel id="cPanelValidators" runat="server" Width="864px" Height="46px"><input id="checkedRows" type="hidden" runat="server" /><input id="hdnRiskClass" type="hidden"
                    name="hdnRiskClass" runat="server" /></asp:panel></form>
	</body>
</html>
