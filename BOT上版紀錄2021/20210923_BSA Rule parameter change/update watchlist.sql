use pbsa
go

declare @para1 varchar(8000);
DECLARE @strInCust varchar(500), @strExCust varchar(500), @strRel varchar(500)
declare @strTmp varchar(5000), @strTmp1 varchar(5000),@strTmp2 varchar(5000), @strTmp3 varchar(5000)
declare @strEnd varchar(5000), @i1 int, @i2 int, @i3 int

--IMultiBene
select  @STRTMP=Params from Watchlist  where WLCode like 'IMultiBene%'
SELECT @STRTMP = replace(@STRTMP,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ')

UPDATE  Watchlist  SET Params=@strTmp where WLCode like 'IMultiBene%'

--OMultiBene
select  @STRTMP=Params from Watchlist  where WLCode like 'OMultiBene%'
SELECT @STRTMP = replace(@STRTMP,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ')

UPDATE  Watchlist  SET Params=@strTmp where WLCode like 'OMultiBene%'

--IMultiOrg
select  @STRTMP=Params from Watchlist  where WLCode like 'IMultiOrg%'
SELECT @STRTMP = replace(@STRTMP,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ')

UPDATE  Watchlist  SET Params=@strTmp where WLCode like 'IMultiOrg%'

--OMultiOrg
select  @STRTMP=Params from Watchlist  where WLCode like 'OMultiOrg%'
SELECT @STRTMP = replace(@STRTMP,'<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="2" ','<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" ')

UPDATE  Watchlist  SET Params=@strTmp where WLCode like 'OMultiOrg%'


--re run bsa eod

--select * from  Watchlist  where WLCode in ('IMultiBene','OMultiBene','IMultiOrg','OMultiOrg')
update Watchlist set LastEval = NULL, LastOper = 'primeadmin' where WLCode in ('IMultiBene','OMultiBene','IMultiOrg','OMultiOrg')
--select * from SysParam
update SysParam set BusDate = '2021-09-01'




--re run bsa eod for system DATE

--select * from  Watchlist  where WLCode in ('IMultiBene','OMultiBene','IMultiOrg','OMultiOrg')
update Watchlist set LastEval = NULL, UseSysDate = 2, LastOper = 'primeadmin' where WLCode in ('IMultiBene','OMultiBene','IMultiOrg','OMultiOrg')
--select * from SysParam
update SysParam set BusDate = '2021-09-01'
--after bsa eod change use sysdate to 1 
update Watchlist set UseSysDate = 1, LastOper = 'primeadmin' where WLCode in ('IMultiBene','OMultiBene','IMultiOrg','OMultiOrg')


