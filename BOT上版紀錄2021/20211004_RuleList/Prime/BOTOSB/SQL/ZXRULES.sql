--exec pbsa..CRY_RuleList 'USERDEFINED','-ALL-','-BOTH-','-BOTH-'
use PBSA
GO

SET NOCOUNT ON;

--Declare @xmlText as nvarchar(max)
Declare @xmlText as nvarchar(max)
Declare @List as nvarchar(max)
DECLARE @hDoc int 
DECLARe @wlCode varchar(1000)
DECLARE @tt table (wlcode varchar(35), Alias varchar(1000), Value nvarchar(max) )
DECLARE @tt1 table (wlcode varchar(35), params nvarchar(max))

DECLARE cur Cursor for
Select wlcode from watchlist with (NOLOCK)

OPEN cur
FETCH NEXT FROM cur into @wlCode

While @@FETCH_STATUS = 0 
Begin

	select @xmlText = Params from watchlist with(nolock) where wlcode = @wlCode and createtype != 1 

	exec sp_xml_preparedocument @hDoc OUTPUT, @xmlText

	INSERT INTO @tt 
	SELECT @wlCode, Alias, Alias + ' = ''' + Value + '''' as value
	FROM OPENXML (@hDoc, '/Params/Param')
	WITH (WlCode varchar(35), Alias varchar(1000), Value nvarchar(max))

	EXEC sp_xml_removedocument @hDoc

	Select @list = COALESCE(@List + char(10)+ char(13), '') + Value from @tt
	set @list = replace(replace(@list,char(13),'  '),char(10),'')
	Insert into @tt1 values (@wlcode, @list)
	
	delete from @tt

	FETCH NEXT FROM cur into @wlCode	
	
	set @list = NULL
	set @xmlText = NULL

End

Close cur
DeAllocate cur

--select * from @tt1

/*
select wl.WLCode,[Title], SuspType ,
case Schedule 
when 1 then 'Every EOD'
when 2 then 'Daily'
when 3 then 'Weekly'
when 4 then 'Monthly'
when 5 then 'Quarterly' else Null end Freqschedule,
case ispreeod when 1 then 'Pre-EOD' 
when 0 then 'Post-EOD' else Null end 'Eval Time', 
wl.LastModify, wl.LastEval,
convert(nvarchar(2500),tt1.params) params
from @tt1 tt1
join Watchlist wl on tt1.wlcode = wl.WLCode
where IsUserDefined = 1
*/

select '!' + wl.wlcode as 'RuleCode', [title],SPName,UPPER(susptype) as 'RuleType',
case schedule
  when 0 then 'Disable'
  when 1 then 'Enable, Every EOD'
  when 2 then 'Enable, Daily'
  when 3 then 'Enable, Weekly'
  when 4 then 'Enable, Monthly'
  when 5 then 'Enable, Quarterly' else Null end 'Schedule',
case IsPreEOD
  when 1 then 'Pre-EOD' 
  when 0 then 'Post-EOD' 
  else Null end 'IsPreEOD', 
case UseSysDate
  when 0 then 'UseSysDate'
  when 1 then 'UseSysDate'
  when 2 then 'UseBusDate' end 'UseSysDate', 
case isnull(LastModify,'') 
  when '' then convert(varchar(20),'')
  else convert(varchar(20),LastModify,111) + ' ' +  convert(varchar(20),LastModify,114)  end 'ModifyDate',
case isnull(LastEval,'')
  when '' then convert(varchar(20),'')
  else convert(varchar(20),LastEval,111) + ' ' +  convert(varchar(20),LastEval,114)  end 'EvalDate', convert(nvarchar(2500),tt1.params) params, convert(varchar(10),getdate(),101) as dt
from @tt1 tt1
join Watchlist wl on tt1.wlcode = wl.WLCode
where IsUserDefined=1

