USE [OFAC]
GO

/****** Object:  StoredProcedure [dbo].[CRY_OFACViolStat]    Script Date: 10/13/2021 3:50:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[CRY_OFACViolStat] (@calling_oper Code,@source_code varchar(40),@startDate DATETIME, @endDate DATETIME)
   AS
	 Set NOCOUNT ON
/******************************************************************************
Procedure Name: CRY_OFACViolStat
Description:    Lists the OFAC Violation Summary between given dates

Parameters: @StartDate = Start Date for the Report to be generated
        @EndDate = End Date for the Report to be generated
******************************************************************************/
DECLARE @ViolStats TABLE (Source varchar(40),TotalViolations INT,
        TotalReviewedViolations INT,
        TotalNonReviewedViolations INT,
        TotalConfirmedViolations INT,
        TotalWaivedViolations INT,
        TotalDelegatedViolations INT,
        TotalPendingApprovalViol INT,
        HistTotalViolations INT,
        HistTotalReviewedViolations INT,
        HistTotalNonReviewedViolations INT,
        HistTotalConfirmedViolations INT,
        HistTotalWaivedViolations INT,
        HistTotalDelegatedViolations INT,
        HistTotalPendingApprovalViol INT)

Declare @FilterTable TABLE(Seqnumb Int,Source varchar(40),confirmstate int,trantime datetime,App Bit)
Declare @FilterHistTable TABLE(Seqnumb Int,Source varchar(40),confirmstate int,trantime datetime,App Bit)


 CREATE TABLE #OperBranchDept_Current (Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept_Current SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 115)

CREATE TABLE #OperBranchDept_Archived (Branch VARCHAR(10), Dept VARCHAR(10))
	INSERT INTO #OperBranchDept_Archived SELECT Branch, Dept from Psec.dbo.fnGetBranchDeptForOperByViewId(@calling_oper, 120)

--Get the current OFAC cases that oper has right to access
Insert into @FilterTable
Select seqnumb,source,confirmstate,trantime,App from FilterTranTable FT
WHERE EXISTS (select * from #OperBranchDept_Current cbd where FT.Branch = cbd.Branch and FT.Dept = cbd.Dept)
and (isnull(@source_code,'') = '' or ft.Source = @source_code)

--Get the archived OFAC cases that oper has right to access
Insert into @FilterHistTable
Select seqnumb, Source,confirmstate,trantime,App from FilterTranHistTable FH
WHERE EXISTS (select * from #OperBranchDept_Archived cbd where FH.Branch = cbd.Branch and FH.Dept = cbd.Dept)
and (isnull(@source_code,'') = '' or FH.Source = @source_code)

if object_id('tempdb..#OperBranchDept_Current') is not null 
		DROP TABLE #OperBranchDept_Current

if object_id('tempdb..#OperBranchDept_Archived') is not null 
		DROP TABLE #OperBranchDept_Archived
/* Oct.13.2021 add source
INSERT INTO @ViolStats(TotalViolations,
        TotalReviewedViolations,
        TotalNonReviewedViolations,
        TotalConfirmedViolations,
        TotalWaivedViolations,
        TotalDelegatedViolations,
        HistTotalViolations,
        HistTotalReviewedViolations,
        HistTotalNonReviewedViolations,
        HistTotalConfirmedViolations,
        HistTotalWaivedViolations,
        HistTotalDelegatedViolations,
        TotalPendingApprovalViol,
        HistTotalPendingApprovalViol)
    SELECT  (
         SELECT COUNT(*)    --Total No. of OFAC Violations
         FROM @FilterTable
         WHERE trantime BETWEEN @startDate AND @endDate
        ) AS TotalViolations ,
        (
         SELECT COUNT(*)    --Total No. of Reviewed Violations
         FROM @FilterTable
         WHERE confirmstate IS NOT NULL
         AND trantime BETWEEN @startDate AND @endDate
        ) AS TotalReviewedViolations,
        (
         SELECT COUNT(*)    --Total No. of Non-Reviewed Violations
         FROM @FilterTable
         WHERE confirmstate IS NULL
         AND trantime BETWEEN @startDate AND @endDate
        ) AS TotalNonReviewedViolations,
        (
         SELECT COUNT(*)    --Total No. of Confirmed Violations
         FROM @FilterTable
         WHERE confirmstate= 1
         AND trantime BETWEEN @startDate AND @endDate
        ) AS TotalConfirmedViolations,
        (
         SELECT COUNT(*)    --Total No. of Waived Violations
         FROM @FilterTable
         WHERE confirmstate= 2
         AND trantime BETWEEN @startDate AND @endDate
        ) AS TotalWaivedViolations,
        (
         SELECT COUNT(*)    --Total No. of Delegated Violations
         FROM @FilterTable
         WHERE confirmstate= 3
         AND trantime BETWEEN @startDate AND @endDate
        ) AS TotalDelegatedViolations,
        (
         SELECT COUNT(*)    --Total No. of OFAC Violations in History
         FROM @FilterHistTable
         WHERE trantime BETWEEN @startDate AND @endDate
        ) AS HistTotalViolations ,
        (
         SELECT COUNT(*)    --Total No. of Reviewed Violations in History
         FROM @FilterHistTable
         WHERE confirmstate IS NOT NULL
         AND trantime BETWEEN @startDate AND @endDate
        ) AS HistTotalReviewedViolations,
        (SELECT COUNT(*)    --Total No. of Non-Reviewed Violations in History
         FROM @FilterHistTable
         WHERE confirmstate IS NULL
         AND trantime BETWEEN @startDate AND @endDate
        ) AS HistTotalNonReviewedViolations,
        (
         SELECT COUNT(*)    --Total No. of Confirmed Violations in History
         FROM @FilterHistTable
         WHERE confirmstate= 1
         AND trantime BETWEEN @startDate AND @endDate
        ) AS HistTotalConfirmedViolations,
        (
         SELECT COUNT(*)    --Total No. of Waived Violations in History
         FROM @FilterHistTable
         WHERE confirmstate= 2
         AND trantime BETWEEN @startDate AND @endDate
        ) AS HistTotalWaivedViolations,
        (
         SELECT COUNT(*)    --Total No. of Delegated Violations in History
         FROM @FilterHistTable
         WHERE confirmstate= 3
         AND trantime BETWEEN @startDate AND @endDate
        ) AS HistTotalDelegatedViolations,
        0 AS TotalPendingApprovalViol,
        0 AS HistTotalPendingApprovalViol
		*/
		
		insert into @ViolStats(Source,TotalViolations,TotalReviewedViolations,TotalNonReviewedViolations,TotalConfirmedViolations,TotalWaivedViolations,TotalDelegatedViolations,TotalPendingApprovalViol,
		HistTotalViolations,HistTotalReviewedViolations,HistTotalNonReviewedViolations,HistTotalConfirmedViolations,HistTotalWaivedViolations,HistTotalDelegatedViolations,HistTotalPendingApprovalViol)
		select source,0,0,0,0,0,0,0,0,0,0,0,0,0,0 from @FilterTable WHERE trantime BETWEEN @startDate AND @endDate  group by source
		union
		select source,0,0,0,0,0,0,0,0,0,0,0,0,0,0 from @FilterhistTable WHERE trantime BETWEEN @startDate AND @endDate  group by source

		--current
		update @ViolStats set TotalViolations = 
		A.cnt from (
		select source,count(*) cnt from @FilterTable WHERE trantime BETWEEN @startDate AND @endDate group by source
		) A join @ViolStats v on A.source = v.Source

		update @ViolStats set TotalReviewedViolations = 
		A.cnt from (
		select source,count(*) cnt from @FilterTable WHERE confirmstate IS NOT NULL AND trantime BETWEEN @startDate AND @endDate group by source
		) A join @ViolStats v on A.source = v.Source

		update @ViolStats set TotalNonReviewedViolations = 
		A.cnt from (
		select source,count(*) cnt from @FilterTable WHERE confirmstate IS NULL AND trantime BETWEEN @startDate AND @endDate group by source
		) A join @ViolStats v on A.source = v.Source

		update @ViolStats set TotalConfirmedViolations = 
		A.cnt from (
		select source,count(*) cnt from @FilterTable WHERE confirmstate = 1 AND trantime BETWEEN @startDate AND @endDate group by source
		) A join @ViolStats v on A.source = v.Source

		update @ViolStats set TotalWaivedViolations = 
		A.cnt from (
		select source,count(*) cnt from @FilterTable WHERE confirmstate = 2 AND trantime BETWEEN @startDate AND @endDate group by source
		) A join @ViolStats v on A.source = v.Source

		update @ViolStats set TotalDelegatedViolations = 
		A.cnt from (
		select source,count(*) cnt from @FilterTable WHERE confirmstate = 3 AND trantime BETWEEN @startDate AND @endDate group by source
		) A join @ViolStats v on A.source = v.Source

		--hist
		update @ViolStats set HistTotalViolations = 
		A.cnt from (
		select source,count(*) cnt from @FilterHistTable WHERE trantime BETWEEN @startDate AND @endDate group by source
		) A join @ViolStats v on A.source = v.Source

		update @ViolStats set HistTotalReviewedViolations = 
		A.cnt from (
		select source,count(*) cnt from @FilterHistTable WHERE confirmstate IS NOT NULL AND trantime BETWEEN @startDate AND @endDate group by source
		) A join @ViolStats v on A.source = v.Source

		update @ViolStats set HistTotalNonReviewedViolations = 
		A.cnt from (
		select source,count(*) cnt from @FilterHistTable WHERE confirmstate IS NULL AND trantime BETWEEN @startDate AND @endDate group by source
		) A join @ViolStats v on A.source = v.Source

		update @ViolStats set HistTotalConfirmedViolations = 
		A.cnt from (
		select source,count(*) cnt from @FilterHistTable WHERE confirmstate = 1 AND trantime BETWEEN @startDate AND @endDate group by source
		) A join @ViolStats v on A.source = v.Source

		update @ViolStats set HistTotalWaivedViolations = 
		A.cnt from (
		select source,count(*) cnt from @FilterHistTable WHERE confirmstate = 2 AND trantime BETWEEN @startDate AND @endDate group by source
		) A join @ViolStats v on A.source = v.Source

		update @ViolStats set HistTotalDelegatedViolations = 
		A.cnt from (
		select source,count(*) cnt from @FilterHistTable WHERE confirmstate = 3 AND trantime BETWEEN @startDate AND @endDate group by source
		) A join @ViolStats v on A.source = v.Source


        IF EXISTS(SELECT *
            FROM OptionTbl
            WHERE OptionTbl.Code = 'AppWaivViol'
            AND OptionTbl.Enabled = 1)  -- If Second Approval is turned OFF
        BEGIN
			/*Oct.13.2021 add source
            UPDATE @ViolStats SET TotalPendingApprovalViol = TotalPendingApprovalViol+
                (SELECT count(*) FROM  @FilterTable
                WHERE confirmstate = 2
                AND app = 0) -- Total No. of Violations Pending Approval

            UPDATE @ViolStats SET HistTotalPendingApprovalViol = HistTotalPendingApprovalViol+
                (SELECT count(*) FROM  @FilterHistTable
                WHERE confirmstate = 2
                AND app = 0) -- Total No. of Violations Pending Approval in History
			*/
			UPDATE @ViolStats SET TotalPendingApprovalViol = TotalPendingApprovalViol +
			A.cnt from (
			select source,count(*) cnt from @FilterTable WHERE confirmstate = 2 AND app = 0 
			AND trantime BETWEEN @startDate AND @endDate group by source
			) A join @ViolStats v on A.source = v.Source

			UPDATE @ViolStats SET HistTotalPendingApprovalViol = HistTotalPendingApprovalViol +
			A.cnt from (
			select source,count(*) cnt from @FilterHistTable WHERE confirmstate = 2 AND app = 0 
			AND trantime BETWEEN @startDate AND @endDate group by source
			) A join @ViolStats v on A.source = v.Source

        END
        IF EXISTS(SELECT *
            FROM OptionTbl
            WHERE OptionTbl.Code = 'AppConfViol'
            AND OptionTbl.Enabled = 1)  -- If Second Approval is turned OFF
        BEGIN
			/*Oct.13.2021 add source
            UPDATE @ViolStats SET TotalPendingApprovalViol = TotalPendingApprovalViol+
                (SELECT count(*) FROM  @FilterTable
                WHERE confirmstate = 1
                AND app = 0) -- Total No. of Violations Pending Approval

            UPDATE @ViolStats SET HistTotalPendingApprovalViol = HistTotalPendingApprovalViol+
                (SELECT count(*) FROM  @FilterHistTable
                WHERE confirmstate = 1
                AND app = 0) -- Total No. of Violations Pending Approval in History
			*/
			UPDATE @ViolStats SET TotalPendingApprovalViol = TotalPendingApprovalViol +
			A.cnt from (
			select source,count(*) cnt from @FilterTable WHERE confirmstate = 1 AND app = 0 
			AND trantime BETWEEN @startDate AND @endDate group by source
			) A join @ViolStats v on A.source = v.Source

			UPDATE @ViolStats SET HistTotalPendingApprovalViol = HistTotalPendingApprovalViol +
			A.cnt from (
			select source,count(*) cnt from @FilterHistTable WHERE confirmstate = 1 AND app = 0 
			AND trantime BETWEEN @startDate AND @endDate group by source
			) A join @ViolStats v on A.source = v.Source
        END

  SELECT * FROM @ViolStats

GO


