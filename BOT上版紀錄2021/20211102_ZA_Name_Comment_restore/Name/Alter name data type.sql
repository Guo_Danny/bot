USE [PBSA]
GO

--update Customer set Name = LEFT(name, 40), LastOper = 'primeadmin' where LEN(name) > 40


DROP INDEX [IdxCustomer_Name] ON [dbo].[Customer] WITH ( ONLINE = OFF )
GO
--Customer
ALTER TABLE customer ALTER COLUMN [name] [LongName] NULL;

CREATE CLUSTERED INDEX [IdxCustomer_Name] ON [dbo].[Customer]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE customerhist ALTER COLUMN [name] [LongName] NULL;

ALTER TABLE Account ALTER COLUMN [name] [LongName] NULL;
ALTER TABLE CustomerOwner ALTER COLUMN [name] [LongName] NULL;
ALTER TABLE CustomerReference ALTER COLUMN [refname] [LongName] NULL;
ALTER TABLE KYCCNames ALTER COLUMN [Custname] [LongName] NULL;

/****** Object:  Index [IdxKYCCNames_KYCCName]    Script Date: 9/9/2020 4:43:38 PM ******/
DROP INDEX [IdxKYCCNames_KYCCName] ON [dbo].[KYCCNames] WITH ( ONLINE = OFF )
GO

--KYCCNames
ALTER TABLE KYCCNames ALTER COLUMN [KYCCname] [LongName] NOT NULL;

SET ANSI_PADDING ON
GO

/****** Object:  Index [IdxKYCCNames_KYCCName]    Script Date: 9/9/2020 4:43:38 PM ******/
CREATE CLUSTERED INDEX [IdxKYCCNames_KYCCName] ON [dbo].[KYCCNames]
(
	[KYCCName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

--2020.11 add
alter table customer alter column notes varchar(2000)

--AUG 25 2021 ADD
--TempCustomer
ALTER TABLE TempCustomer ALTER COLUMN [name] [LongName] NULL;
