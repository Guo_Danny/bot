USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_UpdCustomer]    Script Date: 4/27/2021 4:05:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER Procedure [dbo].[BSA_UpdCustomer] ( @id as ObjectId, @parent as ObjectId, 
					@name as LongName, @dBA as LongName, 
					@secCode as varchar(10), @address as Address,@city as City, 
					@state as State, @zip as Zip, @country as SCode, 
					@telephone as varchar(20), @email as varchar(60),
					@tIN as varchar(20), @LicenseNo As Varchar(35), @LicenseState As State,
                    @passportNo as varchar(20), 
					@dOB as GenericDate, @typeOfBusiness as varchar(80),
					@sourceOfFunds as varchar(80), @accountOfficer as LongName,
					@acctOffTel as varchar(20), @acctOffEmail as varchar(60),
					@compOfficer as LongName, @compOffTel as varchar(20),
					@compOffEmail as varchar(60), @idList as varchar(255),
					@notes as varchar(4000), @exemptionStatus as SCode,
					@lastActReset as GenericTime,
					@lastReview as GenericTime, @lastReviewOper as SCode,
					@ownerBranch SCode, @ownerDept SCode, @ownerOper SCode, 
					@riskClass SCode, @oper SCode, @ts timestamp,
					@countryOfOrigin SCode, @countryOfIncorp SCode, 
					@OffshoreCorp int, @BearerShares bit, @NomeDePlume bit,
					@Type SCode,
					@Resident Bit, @BusRelationNature VarChar(40),
					@PrevRelations VarChar(40),	
					@PEP Bit, @PoliticalPos VarChar(40), @FEP Bit, @MSB Bit,
					@CorrBankRelation bit, 
					@Sex Char(1), @ShellBank Bit, @HighRiskRespBank Bit,
					@OffshoreBank Bit, @PayThroughAC Bit, 
					@RegulatedAffiliate Bit, @USPerson VarChar(40),
					@RFCalculate Bit, @Status Int, @ReviewDate DateTime,
					@CTRAmt money, @User1 varchar(40), @User2 varchar(40),
					@User3 varchar(40), @User4 varchar(40), 
					@User5 varchar(40),
					@SwiftTID varchar(11), @embassy bit, 
					@ForeignGovt bit, @charityOrg bit, @DoCIP bit,
					@Prospect bit, @AssetSize Money, @Income Money,
					@IndOrBusType char(1),
					@profiledCountries Varchar(4000), @OnProbation Bit, 
					@ProbationReason SCode, @ProbationStartDate as GenericDate,
					@ProbationEndDate as GenericDate,
					@Closed Bit, @ClosedDate as GenericDate ,@ClosedReason ObjectId , 
					@OpenDate as GenericDate ,
					@CountryofResidence SCode,@CountryofCitizenship SCode,
					@KYCStatus int, @KYCOperator varchar(20),
					@KYCDataCreateDate datetime,@KYCDataModifyDate datetime
					)
  As

  -- Start standard stored procedure transaction header
  declare @trnCnt int

  Select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_UpdCustomer
  else
    -- Already in a transaction
	save tran BSA_UpdCustomer
  -- End standard stored procedure transaction header

  declare @stat int,	@cnt int
  
  --20201030 change, 20201126 add for notes input over size
  Declare @ProposedRisk SCode ,  @strNotes varchar(8000), @txt varchar(8000), @iNotsLen int, @iNewCust int
  
	--20201126 polly add for Notes input over size bug , start
	--get field size from system table 
	select @iNotsLen=2000
	select @iNotsLen=b.max_length 
	    from sys.tables a join sys.all_columns b on a.object_id = b.object_id 
		where a.name = 'CUSTOMER' and b.name = 'Notes'
    
	--if notes size is closed max length, insert to Event log , 
	--   and get last time newline of data + this time data 
	Select @strNotes='', @txt=''
	Select @strNotes=NullIf(RTrim(@notes), '')
	if len(@strNotes) > (@iNotsLen - 100)
	   begin
	        --insert into event log
			Select @txt = 'Notes closed to max of ' + @id + ', Notes PREVIOUS: ' + @strNotes
			Exec BSA_InsEvent @oper, 'Mod', 'Cust', @id, @txt
			
			--get last time newline data + this time info
			select @txt=''
			Select @txt=reverse(substring(reverse(@strNotes),1,charindex(';01#&;31#&',reverse(@strNotes))-1 ))
			Select @notes=@txt
		end
	--20201126 polly add for Notes input over size bug  fix, end

  if @parent = '' Select @parent = @id

  --20210525 add for bug fix reject @Kycoperator must be reset, start
   if @KYCStatus=6
   begin
     select @KYCOperator= NullIf((select CreateOper from RiskScoreHist where DateAccepted is null and customerid = @id), '')                         
   end
   --20210525 add for bug fix reject @Kycoperator must be reset, end

  Update Customer Set 
	Id = NullIf(RTrim(@id), ''), Parent = NullIf(RTrim(@parent), ''), 
	Name = NullIf(RTrim(@name), ''), DBA = NullIf(RTrim(@dBA), ''), 
	SecCode = NullIf(RTrim(@secCode), ''), 
	Address = NullIf(RTrim(@address), ''), City = NullIf(RTrim(@city), ''), 
	State = NullIf(RTrim(@state), ''), Zip = NullIf(RTrim(@zip), ''), 
            Country = NullIf(RTrim(@country), ''), 
	Telephone = NullIf(RTrim(@telephone), ''), 
	Email = NullIf(RTrim(@email), ''), TIN = NullIf(RTrim(@tIN), ''), 
        LicenseNo = NullIf(RTrim(@LicenseNo), ''), 
        LicenseState = NullIf(RTrim(@LicenseState), ''),
	PassportNo = NullIf(RTrim(@passportNo), ''), DOB = NullIf(@dOB, 0), 
	TypeOfBusiness = NullIf(RTrim(@typeOfBusiness), ''), 
	SourceOfFunds = NullIf(RTrim(@sourceOfFunds), ''), 
	AccountOfficer = NullIf(RTrim(@accountOfficer), ''), 
	AcctOffTel = NullIf(RTrim(@acctOffTel), ''), 
	AcctOffEmail = NullIf(RTrim(@acctOffEmail), ''), 
	CompOfficer = NullIf(RTrim(@compOfficer), ''), 
	CompOffTel = NullIf(RTrim(@compOffTel), ''), 
	CompOffEmail = NullIf(RTrim(@compOffEmail), ''), 
	IdList = NullIf(RTrim(@idList), ''), 
	Notes = NullIf(RTrim(@notes), ''), 
	ExemptionStatus = NullIf(RTrim(@exemptionStatus), ''),
	LastActReset = @lastActReset, 
	LastReview = NullIf(@lastReview, 0), 
	LastReviewOper = NullIf(RTrim(@lastReviewOper), ''), 
	LastOper = RTrim(@oper),
	OwnerBranch = NullIf(RTrim(@ownerBranch), ''), 
	OwnerDept = NullIf(RTrim(@ownerDept), ''), 
	OwnerOper = NullIf(RTrim(@ownerOper), ''),
	RiskClass = @riskClass,
	CountryOfOrigin = NullIf(RTrim(@countryOfOrigin), ''),
	CountryOfIncorp= NullIf(RTrim(@countryOfIncorp), ''), 
	OffshoreCorp = NullIf(@OffshoreCorp, 0),
	BearerShares = @BearerShares,
    	NomeDePlume = @NomeDePlume,
	Type = NullIf(@Type, ''),
	Resident = @Resident, BusRelationNature = NullIf(@BusRelationNature,''),
	PrevRelations = NullIf(@PrevRelations,''), 
	PEP = @PEP, PoliticalPos = NullIf(@PoliticalPos,''), FEP = @FEP, MSB = @MSB,
	CorrBankRelation = @CorrBankRelation,
	Sex = NullIf(@Sex,''), ShellBank = @ShellBank,
	HighRiskRespBank = @HighRiskRespBank, OffshoreBank = @OffshoreBank,
	PayThroughAC = @PayThroughAC, RegulatedAffiliate = @RegulatedAffiliate,
	USPerson = NullIf(@USPerson,''), RFCalculate = @RFCalculate,
	Status = @Status, ReviewDate = @ReviewDate,
	CTRAmt = @CTRAmt,
	User1 = NullIf(@User1,''), 
	User2 = NullIf(@User2,''), 
	User3 = NullIf(@User3,''), 
	User4 = NullIf(@User4,''),
	User5 = NullIf(@User5,''),
	SwiftTID = NUllIf(RTrim(@SwiftTID),''), 
	Embassy = @embassy, 
	ForeignGovt = @ForeignGovt, 
	CharityOrg = @CharityOrg, 
	DoCIP = @DoCIP,
	Prospect = @Prospect,
	AssetSize = NullIf(@AssetSize,0),
	Income = NullIf(@Income,0),
	IndOrBusType = NullIf(RTrim(@IndOrBusType),''),
	OnProbation = @OnProbation,
	ProbationReason = NullIf(@ProbationReason, ''),
	ProbationStartDate = NullIf(@ProbationStartDate, 0),
	ProbationEndDate = NullIf(@ProbationEndDate, 0),
	Closed = @Closed, 
	ClosedDate = NullIf(@ClosedDate, 0),
	ClosedReason = NullIf(@ClosedReason, ''), 
	OpenDate = NullIf(@OpenDate, 0), 
	CountryofResidence = NullIf(RTrim(@CountryofResidence), ''), 
	CountryofCitizenship = NullIf(RTrim(@CountryofCitizenship), '') ,
	KYCDataCreateDate = @KYCDataCreateDate,
	KYCDataModifyDate = @KYCDataModifyDate,
	KYCStatus = @KYCStatus,
	KYCOperator = NullIf(@KYCOperator, '')
	
   Where Id = @id and Ts = @ts
  Select @stat = @@error, @cnt = @@rowcount
  -- Evaluate results of the transaction
  If ( @stat = 0 and @cnt = 0 ) 
	  Select @stat = 250001	-- Concurrent Update
  else
	 if @stat = 0 begin
		-- Insert Profile Countries (Comma delimited with a comma at the end)
		if len(@profiledCountries) >= 0 begin
			exec BSA_InsCountryOwners @profiledCountries, @id, @oper
			select stat = @@error
		end
	  end

  --2020-10-22 new request approved (onboard) , risk= default, low, miduem did not MLCO approve risk score, start line
  --2020-10-30 add condition if customer old risk is medhi-risk, or high risk  or derisk must approve from MLCO.
  if  @KYCStatus=2
   begin
       --2020-12-14 change from riskclass , if new cust get AcceptCnt, old cust get autoAcceptAmt
	   select @iNewCust=0
	   select @iNewCust=1 from Customer(nolock) where id=@id and (ProbationStartDate is null or ProbationStartDate ='19000101' or isnull(ProbationStartDate,'')='' ) 
	         and datediff(day,KYCDataCreateDate,getdate()) <10 
       declare @sData varchar(600), @mList varchar(1000),@amlcoList varchar(1000),@ccoList varchar(1000), @gmList varchar(1000), @count int; 
   
	   --2020-10-28 bug fix proposed risk class, did not customer riskclass
	   --2020-12-28 CHANGE FOR IT CHANGE: IN RiskFactorDataView: ProposedRiskClass IS CURRENT, RiskClass IS PREVIOUS
	   SELECT @amlcoList='0'
	   SELECT @amlcoList=isnull(RTRIM(CONVERT(VARCHAR(10),ProfileByAcct)),'0')
	   FROM  RiskFactorDataView(NOLOCK) R, RiskClass(NOLOCK) RC 
	   WHERE ( (R.RiskClass=RC.Code AND RC.ProfileByAcct=1 ) OR (R.ProposedRiskClass=RC.Code AND  RC.ProfileByAcct=1) )
	   AND  R.Cust=@id

/*
	   if @iNewCust=0 
	      begin
			   select top 1 @ProposedRisk=RiskClass from RiskScoreHist  
			   where CustomerId = @id and DateAccepted is null 
			   --and RiskClass in (select code from RiskClass where AutoAcceptCountThreshold=0)
			   order by CreateDate desc   

			   --get risk class for approved by manager
			   select @sData = '', @mList = '', @amlcoList='', @ccoList='', @gmList='', @count = 0;   
			   
			   --select E.* , R.ProposedRiskClass, R.RiskClass, RC.RiskFactor,RC.ProbationDays 
			   SELECT @mList=E.Manager, @amlcoList=E.AMLCO, @ccoList=E.CCO, @gmList=E.GM
			   FROM EddRiskApproval(NOLOCK) E, RiskFactorDataView(NOLOCK) R, RiskClass(NOLOCK) RC 
               where E.PreviousRisk=R.RiskClass and R.ProposedRiskClass=RC.Code
               and E.CurrentRisk=R.ProposedRiskClass and E.PreviousRisk=R.RiskClass
               and R.Cust=@id
			   


	      end

     else if @iNewCust=1
	      begin
			   select top 1 @ProposedRisk=RiskClass from RiskScoreHist  
			   where CustomerId = @id and DateAccepted is null 
			   --and RiskClass in  (select code from RiskClass where AutoAcceptAmountThreshold=0)
			   order by CreateDate desc  

			   --get risk class for approved by manager
			   select @sData = '', @mList = '', @amlcoList='', @ccoList='', @gmList='', @count = 0;   
			   
			   --select E.* , R.ProposedRiskClass, R.RiskClass, RC.RiskFactor,RC.ProbationDays 
			   SELECT @mList=E.Manager, @amlcoList=E.AMLCO, @ccoList=E.CCO, @gmList=E.GM
			   FROM EddRiskApproval(NOLOCK) E, RiskFactorDataView(NOLOCK) R, RiskClass(NOLOCK) RC 
			   WHERE R.ProposedRiskClass=RC.Code and E.NewCustomer=1
               AND E.CurrentRisk=R.ProposedRiskClass and ISNULL(E.PreviousRisk,'')=''
			   AND R.Cust=@id
			   --select @mList, @gmList

	      end			   

*/
     ----20201214 change control get from riskclass, Start
	 ----20201214 mask, if ( @ProposedRisk='MediumRisk' or  @ProposedRisk='LowRisk' or  @ProposedRisk='DefClass') and (@riskClass<>'MedHiRisk' and @riskClass<>'HighRisk' and @riskClass<>'DeRisk')  
       ----if CHARINDEX(@proposedRisk, @mList)>0 and CHARINDEX(@riskClass, @gmList)=0 
	   ----IF @mList='1'
	   IF @amlcoList='0'
	      exec BSA_ReviewRFData @id, @oper
    ----20201214 change control get from riskclass, End

   end
  -- 2020-10-22 new request approved (onboard) , risk= default, low, miduem did not MLCO approve risk score, end line

  If ( @stat <> 0 ) begin
	rollback tran BSA_UpdCustomer
	return @stat
  end
  If @trnCnt = 0
     commit tran BSA_UpdCustomer
  return @stat
GO


