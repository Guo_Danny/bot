--ILgTrans90
DECLARE @strInCust varchar(2500), @strExCust varchar(2500), @strRel varchar(2500)
declare @strTmp varchar(5000), @strTmp1 varchar(5000),@strTmp2 varchar(5000), @strTmp3 varchar(5000)
declare @strEnd varchar(5000), @i1 int, @i2 int, @i3 int

select @strTmp='', @strTmp1='',@strTmp2='',@strTmp3=''
select @strTmp2 = '<Param Name="@customerList" Alias="Customer (comma separated list or -ALL-)" Value="-ALL-" DataType="" />'
select @strTmp3 = '<Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'

select  @strTmp1=Params from Watchlist  where WLCode like 'ILgTrans90%'

SELECT @strTmp3=REPLACE(@strTmp1, @strTmp2, @strTmp3)

UPDATE  Watchlist  SET Params=@strTmp3 where WLCode like 'ILgTrans90%'

--OLgTrans90

select @strTmp='', @strTmp1='',@strTmp2='',@strTmp3=''
select @strTmp2 = '<Param Name="@customerList" Alias="Customer (comma separated list or -ALL-)" Value="-ALL-" DataType="" />'
select @strTmp3 = '<Param Name="@CustRule" Alias="Enable Include/ Exclude Customer list (-1:Disable 1:Enable Include Customer 2: Enable Exclude Customer)" Value="-1" DataType=""/><Param Name="@customerList" Alias="Include or Exclude Customer List (comma separated list)" Value="-ALL-" DataType="" />'

select  @strTmp1=Params from Watchlist  where WLCode like 'OLgTrans90%'

SELECT @strTmp3=REPLACE(@strTmp1, @strTmp2, @strTmp3)

UPDATE  Watchlist  SET Params=@strTmp3 where WLCode like 'OLgTrans90%'

--check rule status and the other configurations.
select LastEval, LastEvalStat,IsPreEOD ,WLType, Schedule, UseSysDate,* 
from Watchlist where IsUserDefined = 1 and Schedule != 0 order by 1

--clean last eval date
update Watchlist set LastEval = null where WLCode = 'ILgTrans90'
