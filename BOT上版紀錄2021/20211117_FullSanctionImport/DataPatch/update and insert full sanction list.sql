use OFAC
GO
--update sanctions
update WCKeywordslistTable set Used = 1, SDNPROGRAM = 'SANCTIONS'
where KWType in ('SANCTIONS')
and Used = 0 and isnull(SDNPROGRAM,'') = ''


--insert sanctions where not exists
declare @date datetime; set @date = getdate();
begin tran
insert into SDNTable(EntNum,
  Name,
  FirstName,LastName,
  ListType,
  ListID,
  Program,
  Title,
  [type],
  Dob,PrimeAdded,
  Country,
  Remarks,
  Remarks1,
  Remarks2,
  SDNType,DataType,
  UserRec,Deleted,DuplicRec,IgnoreDerived,Status,ListCreateDate,ListModifDate,CreateDate,LastModifDate,LastOper)
select distinct wc.Entnum, 
	ltrim(case when isnull(FirstName,'') = '-' then '' else isnull(FirstName,'') end + ' ' +isnull(LastName,'')) name, 
	case when isnull(wc.FirstName,'') = '-' then '' when isnull(wc.[type],'') in ('M','F','I','U') then isnull(wc.FirstName,'') else '' end FirstName, 
	case when isnull(wc.[type],'') in ('M','F','I','U') then isnull(wc.LastName,'') else '' end  LastName, 
	'World-Check' listtype,
	wc.UID,
	'SANCTIONS',
	SUBSTRING(isnull(Position,''),0,200) Position,
	case when isnull(wc.[type],'') in ('M','F','I','U') then 'individual' 
		when wc.[type] = 'E' and wc.category like '%vessel%' then 'Vessel'
		when wc.[type] = 'E' and wc.category not like '%vessel%' then 'Other' 
		else 'other' end,
	dob,'0' primeadded,
	Country,
	FurtherInfo + ',Keywords:' + isnull(Keywords,'')+',isnull(ExSources,''):'+ExSources,
	SUBSTRING(isnull(Keywords,''),0,255) Keywords,
	SUBSTRING(isnull(Location,''),0,255) Location,
	case when wc.[type] = 'E' and wc.category not like '%vessel%' then '4'
	when wc.[type] = 'E' and wc.category like '%vessel%' then '3' 
	when wc.[type] in ('m','f', 'i','u') then '1' end,
	case 
	when wc.Category in ('Country','EMBARGO') then '7' 
	when wc.Category = 'PORT' and wc.Keywords like '%EC-PRT%' then '6'
	when wc.Category = 'PORT' and wc.Keywords like '%EC-AIRPORT%' then '5'
	else '1' end datatype, '1' userrec,'0' deleted,'0' duplicRec,
	'0' ignorederived,'2' [status],Entered,Updated,@date createdate,@date lastmodifdate,'Prime' lastoper
from WorldCheck wc
join WCKeywordslistTable wckl on CHARINDEX(wckl.abbreviation, wc.keywords) > 0
where wckl.SDNPROGRAM = 'sanctions'
and not exists(select * from SDNTable sdn where sdn.EntNum = wc.Entnum)
and isnull(wc.Keywords,'') != ''

commit

--update SANCTIONS list where program != SANCTIONS
declare @date datetime; set @date = getdate();
begin tran
--select distinct s.EntNum, s.ListType, s.Program, s.Status, w.Category, w.SubCategory, wckl.Abbreviation 
update SDNTable set ListType = 'World-Check', Program = 'SANCTIONS', LastModifDate = @date
from WorldCheck w 
join SDNTable s on w.Entnum = s.EntNum
join WCKeywords wck on wck.UID = w.UID
join WCKeywordslistTable wckl on wckl.Abbreviation = wck.Word
where isnull(w.DelFlag,0) = 0
and wckl.SDNPROGRAM = 'SANCTIONS' and wckl.used = 1
and s.Program != 'SANCTIONS'

commit