use pbsa
GO

--Nov.2.2021 update comments
select customerid, CDDUser32,CDDUser33,CDDUser34,CDDUser35,CDDUser36 
from KYCData where (isnull(CDDUser32,'') + isnull(CDDUser33,'') + isnull(CDDUser34,'') + isnull(CDDUser35,'') + isnull(CDDUser36,'')) != ''

begin tran
update KYCData 
set CDDUser32 = NULL, CDDUser33 = NULL, CDDUser34 = NULL, CDDUser35 = NULL, CDDUser36 = NULL,
LastOper = 'primeadmin' 
where (isnull(CDDUser32,'') + isnull(CDDUser33,'') + isnull(CDDUser34,'') + isnull(CDDUser35,'') + isnull(CDDUser36,'')) != ''

commit