use pbsa
GO

--Nov.2.2021 update comments
select * from KYCData where (isnull(CDDUser32,'') + isnull(CDDUser33,'') + isnull(CDDUser34,'') + isnull(CDDUser35,'') + isnull(CDDUser36,'')) != ''

begin tran
update KYCData 
set ProductsSold = isnull(CDDUser32,'') + ' ' + isnull(CDDUser33,''), 
ServicesProvided = isnull(CDDUser34,'') + ' ' + isnull(CDDUser35,''),
GeographicalAreasServed = isnull(CDDUser36,''), 
LastOper = 'primeadmin' 
where (isnull(CDDUser32,'') + isnull(CDDUser33,'') + isnull(CDDUser34,'') + isnull(CDDUser35,'') + isnull(CDDUser36,'')) != ''

commit