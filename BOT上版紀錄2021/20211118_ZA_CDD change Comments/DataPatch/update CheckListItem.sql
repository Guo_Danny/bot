use pbsa
go


select * from CheckListItem

update CheckListItem set name = 'Name 2', LastOper = 'primeadmin' where Code = 'IndivMidN'

update CheckListItem set name = 'Comments Part I', LastOper = 'primeadmin' where Code = 'COMMENTS1'

update CheckListItem set name = 'Comments Part II', LastOper = 'primeadmin' where Code = 'COMMENTS2'

update CheckListItem set name = 'Comments Part III', LastOper = 'primeadmin' where Code = 'COMMENTS3'

select CLSectionCode, CLItemCode, AllowMultiple, OrderID, CreateOper, CreateDate from CheckListSectionItem where CLSectionCode like '%CustInfo%'
order by CLSectionCode,OrderID


delete CheckListSectionItem where CLSectionCode like '%CustInfo%'

INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoEnt', 'CustNo', 1, 1, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoEnt', 'CustName', 1, 2, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoEnt', 'IndivMidN', 1, 3, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoEnt', 'MotherMName', 1, 4, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoEnt', 'Alias', 1, 5, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoEnt', 'LicenseNo', 1, 6, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoEnt', 'TINNumb', 1, 7, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoEnt', 'OwnBranch', 1, 8, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoEnt', 'OwnDepart', 1, 9, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoInd', 'CustNo', 1, 1, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoInd', 'CustName', 1, 2, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoInd', 'IndivMidN', 1, 3, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoInd', 'MotherMName', 1, 4, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoInd', 'Alias', 1, 5, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoInd', 'LicenseNo', 1, 6, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoInd', 'CustPass', 1, 7, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoInd', 'OwnBranch', 1, 8, 'primeadmin', getdate())
INSERT INTO [PBSA]..[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CustInfoInd', 'OwnDepart', 1, 9, 'primeadmin', getdate())
GO

delete CheckListSectionItem where CLSectionCode like '%CDD%'

INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'TypOfProd1', 1, 1, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'TypOfProd2', 1, 2, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'TypOfProd3', 1, 3, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'TypOfProd4', 1, 4, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'TypOfProd5', 1, 5, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'TradCoun1', 1, 6, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'TradCoun2', 1, 7, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'TradCoun3', 1, 8, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'TradCoun4', 1, 9, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'TradCoun5', 1, 10, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'TrnChannel1', 1, 11, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'TrnChannel2', 1, 12, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'TrnChannel3', 1, 13, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'CustTypEnt', 1, 14, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'CustStatEnt', 1, 15, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'PEPStats', 1, 16, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'IndustryEnt', 1, 17, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'Nationality', 1, 18, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'ConunOfResd', 1, 19, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'SancListing', 1, 20, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'SharehdList', 1, 21, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'RiskRate', 1, 22, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'COMMENTS1', 1, 23, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'COMMENTS2', 1, 24, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'COMMENTS3', 1, 25, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'COMMENTS6', 1, 26, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'COMMENTS7', 1, 27, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDEnt', 'COMMENTS8', 1, 28, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'TypOfProd1', 1, 1, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'TypOfProd2', 1, 2, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'TypOfProd3', 1, 3, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'TypOfProd4', 1, 4, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'TypOfProd5', 1, 5, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'TradCoun1', 1, 6, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'TradCoun2', 1, 7, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'TradCoun3', 1, 8, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'TradCoun4', 1, 9, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'TradCoun5', 1, 10, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'TrnChannel1', 1, 11, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'TrnChannel2', 1, 12, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'TrnChannel3', 1, 13, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'CustTypInd', 1, 14, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'CustStatInd', 1, 15, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'PEPStats', 1, 16, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'IndustryInd', 1, 17, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'Nationality', 1, 18, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'CounOfBirth', 1, 19, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'ConunOfResd', 1, 20, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'SancListing', 1, 21, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'RiskRate', 1, 22, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'COMMENTS1', 1, 23, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'COMMENTS2', 1, 24, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'COMMENTS3', 1, 25, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'COMMENTS6', 1, 26, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'COMMENTS7', 1, 27, 'primeadmin', getdate())
INSERT INTO [DBO].[CheckListSectionItem]([CLSectionCode], [CLItemCode], [AllowMultiple], [OrderID], [CreateOper], [CreateDate]) VALUES('CDDInd', 'COMMENTS8', 1, 28, 'primeadmin', getdate())
GO





