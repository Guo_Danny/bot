/*
**  File Name:        ConvOFAC.sql
**
**  Functional Description:
**
**  This module contains SQL conversion procedures for Prime version 10.1.3
**
**  Creation Date:    03/23/2018
**
****************************************************************************
***                                                                     ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2011                                                   ***
*** FIS               			                                 ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in Prime Associates, Inc.                                            ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|----------------------------------------------------     
03/14/2018      SN       Modified OFS_SelViolByRef stored procedure to get the Seqnum based on �Ref� or �SystemRef� field 
03/14/2018      SN       Modified dbo.OFS_SelMatchesBySeqNum stored procedure to populate integer value (1-4) in MatchType field.						 
03/14/2018	SN		I170175383: OFAC Reporter Highlighting to capture additional Match Text Changes
						Schema Changes:
						New Tables : 
						MatchTextTable and MatchTextHistTable to capture additional matches
						New Fields: 
							OrigMatchID and DupMatch in OFACSearchResults Table
							OrigMatchID in MatchTable and MatchHistTable Tables
						 New Option:
							OFACADLMTCH is added to turn on/off new highlighting changes
						 Stored Procedure Changes:
							OFS_GetAdditionalMatches, WOFS_SaveSearchResults, OFS_AddFilterMatch, 
							WOFS_GetSearchResults, WOFS_GetSDNNameSearchResults, OFS_ArchiveTran
--------------------------------Merge 10.1.3 patch#08 code Merge------------------------------------
11/01/2018		JS      1802092515 - Dow Jones PEP/RCA programs changes
						Added new SP named OFS_DelUpdateRCAExcludeList for hard/soft delete for RCA Relationship Exclude List
						Create new temp table - TempRCAExcludeList 
						Create UDT - RCAExclude
						Added new SP named OFS_DelOtherDowJones_Program to soft delete the Programs 
						that was disabled in ProramList Configuration.						
						Also modified the SP OFS_AddUpdateDowJonesSDN to soft delete DJ-PEP20 records.
01/08/2019		MK		1801984837 - Created new rule to add hyphen in Regex
02/04/2019		MK		1800921030: Re-institute Prime code for World Check Parser utility. Added NoLock and Query performance.
02/26/2019		SM		Fix for last modified and last oper not properly updated in SDN Sub Tables.
03/01/2019		AS		Fixed Bug 23890 :Batch filter is not working for operator with hyphen symbol.
03/06/2019		JS		Bug 23888 : EntNum range enhancement for Dowjones utility
03/08/2019		AS		Bug 23888 :  EntNum range enhancement world check utility.Also added with (nolock) for the select statements.  
25/03/2019		JS      24238 - Added parameter in OFS_RemoveAllWorldCheckEntities to get the deleted row count
06/12/2019		VS      1900889112: OFAC Enhancement to Increase, 'Program' field size.

08/05/2019		JS		22755: Weak AKA changes - New column added in  SDNAltTable,SanctionDataRule.
						updated OFS_InsSanctionDataRule,OFS_UpdSanctionDataRule stored procedure to inclue 'ExcludeWeakAka' field.
08/12/2019      MJK     1801151463: Weak AKA's (Prime and databank) - Load distribution changes
08/08/2019		AS		1801151463 Weak AKA exclude changes.
08/22/2019		AS		Added changes in stored procedures OFS_SelectEnabledSanctionDataRule and OFS_SelectSanctionDataRule
						to add new column ExcludeWeakAka in the select query. 
13/04/2020		KM		30745 - Fixed the issue by properly inserting "USER" list type into ListType and ListTypeAssociation Tables

*/


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Starting Conversion of OFAC Database to version 10.1.3 '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

Use OFAC
GO

Set ANSI_NULLS ON
Go

Set NOCOUNT ON 
Go
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns  
 	WHERE table_name = 'TempSDNTable' and column_name = 'Program' ) 
Begin
Alter table TempSDNTable Alter column Program  varchar(200)  null
End
Go
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns  
 	WHERE table_name = 'SDNTable' and column_name = 'Program' ) 
Begin
Alter table SDNTable Alter column Program  varchar(200)  null
End
Go
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns  
 	WHERE table_name = 'TempLoadSDNTable' and column_name = 'Program' ) 
Begin
Alter table TempLoadSDNTable Alter column Program  varchar(200)  null
End
Go
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns  
 	WHERE table_name = 'OFACSearchResults' and column_name = 'Program' ) 
Begin
Alter table OFACSearchResults Alter column Program  varchar(200)  null
End
Go
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns  
 	WHERE table_name = 'MatchTable' and column_name = 'MatchCountry' ) 
Begin
Alter table MatchTable Alter column MatchCountry  varchar(200)  null
End
Go
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns  
 	WHERE table_name = 'MatchHistTable' and column_name = 'MatchCountry' ) 
Begin
Alter table MatchHistTable Alter column MatchCountry  varchar(200)  null
End
Go

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns  
 	WHERE table_name = 'TempLoadAltTable' and column_name = 'Category' ) 
Begin
ALTER TABLE  TempLoadAltTable ADD Category  varchar (50) null
End
Go

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns  
 	WHERE table_name = 'SDNAltTable' and column_name = 'Category' ) 
Begin
Alter Table SDNAltTable add Category nvarchar(50) 
End
Go

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns  
 	WHERE table_name = 'SanctionDataRule' and column_name = 'ExcludeWeakAka' ) 
Begin
Alter Table SanctionDataRule add ExcludeWeakAka	bit Not Null
						CONSTRAINT DF_SanctionDataRule_ExcludeWeakAka Default 0
End
Go
If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_SelViolByRef]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_SelViolByRef]
Go
create Procedure dbo.OFS_SelViolByRef (@ref as varchar(255),@seqnum as int Output)
With Encryption As

  Declare @stat int
  -- get SeqNumb based on �Ref� or �SystemRef� field and return highest SeqNumb if more than one record exists for same Ref
  select @seqnum = SeqNumb from FilterTranTable WITH (NOLOCK) where Ref = @ref or SystemRef= @ref order by SeqNumb
  if @seqnum IS NOT NULL
  begin
		-- If it is a current OFAC Case, then a valid recordset needs to be returned.
         Select *,CASE COALESCE(ConfirmState,0)
             When 0 Then 'Unconfirmed'
             When 1 Then 'Confirmed'
             When 2 Then 'Waived'
             When 3 Then 'Delegated'
             Else ''
             End  as 'ConfirmStatus' from FilterTranTable WITH (NOLOCK) where SeqNumb = @seqnum
  end
  else 
  BEGIN
		-- If it is an Archived OFAC Case, then a valid recordset needs to be returned.
		-- If the case does not exist, then an empty recordset needs to be returned.
		-- getSearchResults Web Service expects an empty recordset if the case does not exist.
         select @seqnum = SeqNumb from FilterTranHistTable WITH (NOLOCK) where Ref = @ref or SystemRef= @ref order by SeqNumb
             Select *,CASE COALESCE(ConfirmState,0)
             When 0 Then 'Unconfirmed'
             When 1 Then 'Confirmed'
             When 2 Then 'Waived'
             When 3 Then 'Delegated'
             Else ''
             End  as 'ConfirmStatus'from FilterTranHistTable WITH (NOLOCK) where SeqNumb = @seqnum
  END
           
  select @seqnum
  select @stat = @@ERROR
  return @stat
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_SelMatchesBySeqNum]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_SelMatchesBySeqNum]
Go
CREATE Procedure dbo.OFS_SelMatchesBySeqNum (@seqnum as int)
With Encryption As

  Declare @stat int
if exists(select * from matchtable where seqnumb=@seqnum and entnum is not null)
begin
--return the integer value (1-4) in MatchType field
Select MatchTable.SeqNumb, MatchTable.MatchCountry,MatchTable.MatchName,MatchTable.MatchText, MatchType = 
case when ISNUMERIC(MatchType)=1 then MatchTable.MatchType
when MatchTable.MatchType='individual' then '1'
when MatchTable.MatchType='shipper' then '2'
when MatchTable.MatchType='vessel' then '3'
when MatchTable.MatchType='other' then '4'
else '4'
End, MatchTable.OriginalSDNName,MatchTable.Score,MatchTable.entNum, 
   SdnTable.Title as 'Title',SdnTable.Program as 'Program',SdnTable.Remarks as 'Remarks',
   Case 
    When DataLength(SdnTable.Remarks) > 1024 THEN 'The match text exceeds the limit of 1024 characters.  Please inspect the entire text to determine where the matches occurred.' 
	ELSE ''  END as 'Info'
   from MatchTable left outer join SdnTable 
   on MatchTable.entnum = SdnTable.entnum 
   Where MatchTable.SeqNumb = @seqnum
end
else
begin
if exists(select * from matchhisttable where seqnumb=@seqnum and entnum is not null)
begin
--return the integer value (1-4) in MatchType field
Select MatchHistTable.SeqNumb, MatchHistTable.MatchCountry,MatchHistTable.MatchName,MatchHistTable.MatchText, MatchType = 
case 
when ISNUMERIC(MatchType)=1 then MatchHistTable.MatchType 
when MatchHistTable.MatchType='individual' then '1'
when MatchHistTable.MatchType='shipper' then '2'
when MatchHistTable.MatchType='vessel' then '3'
when MatchHistTable.MatchType='other' then '4'
else '4'
End, MatchHistTable.OriginalSDNName,MatchHistTable.Score,MatchHistTable.entNum, 
    SdnTable.Title as 'Title',SdnTable.Program as 'Program',SdnTable.Remarks as 'Remarks',
   Case 
    When DataLength(SdnTable.Remarks) > 1024 THEN 'The match text exceeds the limit of 1024 characters.  Please inspect the entire text to determine where the matches occurred.' 
	ELSE ''  END as 'Info'
   from MatchHistTable left outer join SdnTable 
   on MatchHistTable.entnum = SdnTable.entnum  
   Where MatchHistTable.SeqNumb = @seqnum
end
else
begin
declare @tmp table (rowID int identity (1,1), seqnumb int, matchname SanctionName,originalsdnname SanctionName,matchtext varchar(1024),matchcountry varchar(200),matchtype char(20), score int,entnum int)
declare @tmp1 table (rowID int identity (1,1), seqnumb int, matchname SanctionName,originalsdnname SanctionName,matchtext varchar(1024),matchcountry varchar(200),matchtype char(20), score int,entnum int,title varchar(200),program varchar(200),remarks text,info varchar(150))
declare @i int
declare @max int

insert into @tmp
select seqnumb,matchname,originalsdnname,matchtext,matchcountry,matchtype,score,entnum from matchtable where MatchTable.SeqNumb = @seqnum
select @max = count(*) from @tmp
set @i=1

While (@i <= @max)
begin
declare @seqnumb1 int
declare @matchname SanctionName
declare @matchtext varchar(1024)
declare @originalsdnname SanctionName
declare @matchcountry varchar(200)
declare @matchtype char(20)
declare @score int

select @seqnumb1 = seqnumb,@matchname=matchname ,@originalsdnname = originalsdnname,
@matchcountry=matchcountry,@matchtype=matchtype,@score=score from @tmp where rowid=@i

insert into 
@tmp1(seqnumb,matchname,originalsdnname,matchcountry,matchtype,score,title,program,remarks,info) 
select top(1) @seqnumb1,@matchname,@originalsdnname,@matchcountry,@matchtype,@score, Title ,program,Remarks,
   Case 
    When DataLength(SdnTable.Remarks) > 1024 THEN 'The match text exceeds the limit of 1024 characters.  Please inspect the entire text to determine where the matches occurred.' 
	ELSE ''  END from sdntable where name=@originalsdnname
set @i= @i + 1
end
--return the integer value (1-4) in MatchType field
select rowID,seqnumb,matchname,originalsdnname,matchtext,matchcountry,matchtype = 
case 
when ISNUMERIC(matchtype)=1 then matchtype 
when matchtype='individual' then '1'
when matchtype='shipper' then '2'
when matchtype='vessel' then '3'
when matchtype='other' then '4'
else '4'
End ,score,entnum,title,program,remarks,info from @tmp1
if @@rowcount = 0 
   begin
insert into @tmp
select seqnumb,matchname,originalsdnname,matchtext,matchcountry,matchtype,score,entnum from matchhisttable where MatchHistTable.SeqNumb = @seqnum
select @max = count(*) from @tmp
set @i=1

While (@i <= @max)
begin
select @seqnumb1 = seqnumb,@matchname=matchname ,@originalsdnname = originalsdnname,
@matchcountry=matchcountry,@matchtype=matchtype,@score=score from @tmp where rowid=@i

insert into 
@tmp1(seqnumb,matchname,originalsdnname,matchcountry,matchtype,score,title,program,remarks,info) 
select top(1) @seqnumb1,@matchname,@originalsdnname,@matchcountry,@matchtype,@score, Title ,program,Remarks,
   Case 
    When DataLength(SdnTable.Remarks) > 1024 THEN 'The match text exceeds the limit of 1024 characters.  Please inspect the entire text to determine where the matches occurred.' 
	ELSE ''  END from sdntable where name=@originalsdnname
set @i= @i + 1
end
--return the integer value (1-4) in MatchType field
select rowID,seqnumb,matchname,originalsdnname,matchtext,matchcountry,matchtype = 
case 
when ISNUMERIC(matchtype)=1 then matchtype
when matchtype='individual' then '1'
when matchtype='shipper' then '2'
when matchtype='vessel' then '3'
when matchtype='other' then '4'
else '4'
End ,score,entnum,title,program,remarks,info from @tmp1
   end
end
end
select @stat = @@ERROR
  return @stat
GO
--OFAC MATCH Highlighting Changes START
--SCHEMA CHANGES
IF NOT EXISTS (SELECT * FROM dbo.sysobjects 
		where id = object_id(N'[dbo].[MatchTextTable]') and 
					OBJECTPROPERTY(id, N'IsTable') = 1)
					Begin
CREATE TABLE [dbo].[MatchTextTable](
	[SeqNumb] [int] NOT NULL,
	[OrigMatchID] [int] NOT NULL,
	[MatchText] [varchar](1024) NOT NULL
) ON [PRIMARY]
End
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects 
		where id = object_id(N'[dbo].[MatchTextHistTable]') and 
					OBJECTPROPERTY(id, N'IsTable') = 1)
					Begin
CREATE TABLE [dbo].[MatchTextHistTable](
	[SeqNumb] [int] NOT NULL,
	[OrigMatchID] [int] NOT NULL,
	[MatchText] [varchar](1024) NOT NULL
) ON [PRIMARY]
End
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns  
 	WHERE table_name = 'OFACSearchResults' and column_name = 'OrigMatchID' ) 
Begin
ALTER TABLE  OFACSearchResults ADD OrigMatchID  INT null
End
Go
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns  
 	WHERE table_name = 'OFACSearchResults' and column_name = 'DupMatch' ) 
Begin
ALTER TABLE  OFACSearchResults ADD DupMatch  INT null
End
Go
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns  
 	WHERE table_name = 'MatchTable' and column_name = 'OrigMatchID' )  
 	Begin  
	ALTER TABLE dbo.MatchTable ADD OrigMatchID INT NULL 
End  
Go  

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.Columns  
 	WHERE table_name = 'MatchHistTable' and column_name = 'OrigMatchID' )  
 	Begin  
	ALTER TABLE dbo.MatchHistTable ADD OrigMatchID INT NULL  
End  
Go 
 
IF NOT EXISTS (SELECT * FROM OptionTbl where Code='OFACADLMTCH')
INSERT INTO [dbo].[OptionTbl]
           ([Code]
           ,[Name]
           ,[Enabled]
           ,[ModEnable]
           ,[CreateOper]
           ,[CreateDate]
           ,[LastOper]
           ,[LastModify])
     VALUES
           ('OFACADLMTCH',
		   'Highlight all possible Match Text in Transaction Text in OFAC Case',
		   1,1,'Prime',Getdate(),'Prime',GetDate()
		   )
GO


IF EXISTS (SELECT * FROM dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_GetAdditionalMatches]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_GetAdditionalMatches]
GO 

CREATE PROCEDURE [dbo].[OFS_GetAdditionalMatches] @SeqNumber INT,  @Archive INT
With Encryption 
As 
BEGIN
IF @Archive=0
     BEGIN
        /* Current Matches */
	SELECT OrigMatchID, SeqNumb, MatchText FROM dbo.MatchTextTable WHERE SeqNumb = @SeqNumber
	END
    ELSE
     BEGIN
	 /* Archived Matches */
	SELECT OrigMatchID, SeqNumb, MatchText FROM dbo.MatchTextHistTable WHERE SeqNumb = @SeqNumber
	END
END
GO

if exists (select * from dbo.sysobjects where
    id = object_id(N'[dbo].[WOFS_SaveSearchResults]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[WOFS_SaveSearchResults]
GO

CREATE PROCEDURE WOFS_SaveSearchResults
    (
        @OperCode       SCode,
        @OriginalSDNName    SanctionName,
        @Program        varchar(200),
        @MatchName      SanctionName,
        @MatchText      varchar(255),
        @SysRef         MessageReference,
        @Score          int,
        @MatchType      char(20),
		@EntNum			int,
		@OrigMatchId	int,
		@dupMatch		int
    )

With Encryption As

  declare @trnCnt int


  select @trnCnt = @@trancount  -- Save the current trancount

  if @trnCnt = 0
    -- Transaction has not begun
    begin tran  WOFS_SaveSearchResults
  else
    -- Already in a transaction
    save tran  WOFS_SaveSearchResults

  declare @stat int


	if @EntNum = 0
	 begin
		--Lookup for EntNum using Original Name / Program combination
		insert into OFACSearchResults(OperCode, OriginalSDNName, Program,
			MatchName, DateCreated, MatchText, SystemRef, Score, matchType, EntNum, OrigMatchId, dupMatch)
		select @OperCode, @OriginalSDNName,  @Program, @MatchName, GetDate(),
			@MatchText, @SysRef, @Score, @MatchType, EntNum, @OrigMatchId, @dupMatch
		from SDNTable
		where [Name] = @OriginalSDNName and Program = @Program
	 end
	else
	 begin
		--Use Ent Num provided by SFE
		insert into OFACSearchResults(OperCode, OriginalSDNName, Program,
			MatchName, DateCreated, MatchText, SystemRef, Score, matchType, EntNum, OrigMatchId, dupMatch)
		values(@OperCode, @OriginalSDNName,  @Program, @MatchName, GetDate(),
			@MatchText, @SysRef, @Score, @MatchType, @EntNum, @OrigMatchId, @dupMatch)
	  end

    select @stat = @@Error

      if (@stat <> 0)
          rollback tran  WOFS_SaveSearchResults
      else
        begin
          if @trnCnt = 0
            commit tran  WOFS_SaveSearchResults
        end
    return @stat
GO

if exists (select * from dbo.sysobjects where
    id = object_id(N'[dbo].[OFS_AddFilterMatch]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[OFS_AddFilterMatch]
GO

CREATE PROCEDURE dbo.OFS_AddFilterMatch @seqNumb int, @matchName SanctionName,
        @origSDNName SanctionName, @matchText varchar(1024),
        @matchCountry varchar(200), @matchType char(20), @score int, @entNum int, @origMatchId int, @dupMatch int
With Encryption As
  declare @stat int

  -- Not using begin / commit tran since this routine is used with other
  -- table updates in ofacsrvr.cpp
	if IsNull(@dupMatch, 0) = 0
		Begin
		  insert into MatchTable
			  (SeqNumb, MatchName, OriginalSDNName, MatchText, MatchCountry, MatchType,
			 Score,entNum, origMatchId)
			values
			  (@seqNumb, @matchName, @origSDNName, @matchText, @matchCountry, @matchType,
			 @score, @entNum, @origMatchId)
		End
	else
		Begin
		IF NOT EXISTS (SELECT 1 FROM MatchTextTable WITH (NOLOCK) WHERE
						SeqNumb = @seqNumb AND origMatchId = @origMatchId AND MatchText = @matchText)
		  insert into MatchTextTable
			  (SeqNumb, origMatchId, MatchText)
			values
			  (@seqNumb, @origMatchId, @matchText)
		End
  select @stat = @@ERROR
  return @stat
GO

if exists (select * from dbo.sysobjects where
    id = object_id(N'[dbo].[WOFS_GetSearchResults]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[WOFS_GetSearchResults]
GO

CREATE PROCEDURE [dbo].[WOFS_GetSearchResults]
    (
        @OperCode       SCode,
        @SysRef         MessageReference

    )

With Encryption As
    --Get All Checked Search Results
    select  distinct SDNTable.EntNum, SDNTable.[Name], OFACSearchResults.[Program], SDNTable.[Type],
    SDNTable.Remarks1, OFACSearchResults.MatchName, OFACSearchResults.MatchText,
	OFACSearchResults.Score, OFACSearchResults.OrigMatchId, OFACSearchResults.DupMatch
    FROM SDNTable with(nolock) left outer join OFACSearchResults with(nolock)
    on SDNTable.[Name] = OFACSearchResults.OriginalSDNName and 
    (SDNTable.[Program] = OFACSearchResults.[Program] or OFACSearchResults.[Program] = 'KEY WORD') and
    OFACSearchResults.OperCode = @OperCode and
    OFACSearchResults.SystemRef=@SysRef
    where SDNTable.EntNum in (select Convert(int, [RowId]) from PCDB.dbo.ViewCheckRows with(nolock)
    where ViewId =100 and OperCode=@OperCode and CheckStatus=1)
Go

if exists (select * from dbo.sysobjects where
    id = object_id(N'[dbo].[WOFS_GetSDNNameSearchResults]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[WOFS_GetSDNNameSearchResults]
GO

Create PROCEDURE dbo.WOFS_GetSDNNameSearchResults
        (
            @OperCode       SCode,
            @SysRef         MessageReference

        )

    With Encryption As

        --Get All Checked Search Results
        select  Distinct SDNNamesView.EntNum, SDNNamesView.[Name], SDNNamesView.[Program], SDNNamesView.[Type],
        SDNNamesView.Remarks, OFACSearchResults.MatchName, OFACSearchResults.MatchText,
        OFACSearchResults.Score, OFACSearchResults.OrigMatchId, OFACSearchResults.DupMatch
        FROM SDNNamesView inner join OFACSearchResults
        on (SDNNamesView.[Name] = OFACSearchResults.OriginalSDNName or
        SDNNamesView.[Name] = OFACSearchResults.MatchName)  and
        SDNNamesView.[Program] = OFACSearchResults.[Program] and
        OFACSearchResults.OperCode = @OperCode and
        OFACSearchResults.SystemRef=@SysRef
        where SDNNamesView.EntNum in (select Convert(int, [RowId]) from PCDB.dbo.ViewCheckRows
        where ViewId =130 and OperCode=@OperCode and CheckStatus=1)

Go

if exists (select * from dbo.sysobjects where
    id = object_id(N'[dbo].[OFS_ArchiveTran]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[OFS_ArchiveTran]
GO

CREATE PROCEDURE dbo.OFS_ArchiveTran
With Encryption As

  -- This procedure moves all completed transaction records to the history tables.
  -- debug - add error logging.

  declare @now datetime
  declare @trnCnt int
  declare @useWaiv2ndApp BIT
  declare @useConf2ndApp BIT

  select @trnCnt = @@trancount  -- Save the current trancount

  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_ArchiveTran
  else
    -- Already in a transaction
    save tran OFS_ArchiveTran

  -- Processing starts here:

  select @now = GETDATE()
  select @useWaiv2ndApp = Enabled From OptionTbl Where Code = 'AppWaivViol'
  select @useConf2ndApp = Enabled From OptionTbl Where Code = 'AppConfViol'

  -- Create a list of transactions to archive
  Insert into TempTranTable Select SeqNumb from FilterTranTable
    where ((ConfirmState = 2 AND @useWaiv2ndApp = 0) OR
        (ConfirmState = 1 AND @useConf2ndApp = 0) OR App = 1) or ConfirmState = 3
  if (@@error <> 0)
    begin
      rollback tran OFS_ArchiveTran
      return 25 -- or other error code - need to define
    end

  Insert into FilterTranHistTable ( SeqNumb, Source, Ref, Branch,
              Dept, ReqTime, ClientId, TranTime, Result, MatchCount,
              MsgType, ConfirmTime, ConfirmOper, ConfirmState, ReplyTime,
              ReplyStatus, AnnotTxt, App, AppTime, AppOper,
            currency,KeyField1,KeyField2, CIFFieldsDelimiter,
            LengthKF1, LengthKF2, TransactionType,FileImage,
            CIFRule, SystemRef, Amount, IsIncremental,
            lockoper,locktime, LastOper, LastModifdate, UserMessageReference, ts)
          Select a.SeqNumb, a.Source, a.Ref, a.Branch,
                a.Dept, a.ReqTime, a.ClientId, a.TranTime, a.Result,
                a.MatchCount, a.MsgType, a.ConfirmTime, a.ConfirmOper,
                a.ConfirmState, a.ReplyTime,
                a.ReplyStatus,a.AnnotTxt, a.App, a.AppTime,a.AppOper,
                a.currency, a.KeyField1,a.KeyField2,a.CIFFieldsDelimiter,
                a.LengthKF1,a.LengthKF2,a.TransactionType,FileImage,
                a.CIFRule, a.SystemRef, a.Amount, a.IsIncremental,
                NULL, NULL, LastOper, LastModifdate, UserMessageReference, NULL
          from FilterTranTable a, TempTranTable t
          where a.SeqNumb = t.SeqNumb

  if (@@error <> 0)
    begin
      rollback tran OFS_ArchiveTran
      return 25 -- or other error code - need to define
    end

	--Insert OFAC cases and List types corresponding to the matches in the respective cases
	--into FilterTranHistListTypes table as part of the OFAC EOD process
	insert into FilterTranHistListTypes(Seqnumb, ListType)
	SELECT DISTINCT Seqnumb, ListType from vw_GetFilterTranListTypes a
		where exists (Select SeqNumb from TempTranTable t WHERE t.Seqnumb = a.Seqnumb)
  if (@@error <> 0)
    begin
      rollback tran OFS_ArchiveTran
      return 25 
    end

    --************Relink documents
    --Relink Filter Transaction
    UPDATE PBSA..Documentation
    SET ObjectType = 'TranHist', LastOper = 'Prime'
    WHERE Subsystem = 'OFAC' AND ObjectType = 'Tran'
    AND EXISTS(SELECT * FROM TempTranTable WHERE ObjectId = Convert(VARCHAR, SeqNumb))
  if (@@error <> 0)
    begin
      rollback tran OFS_ArchiveTran
      return 25 -- or other error code - need to define
    end

    --************Relink correspondence
    --Relink Filter Transaction
    UPDATE PBSA..Correspondence
    SET ObjectType = 'TranHist', LastOper = 'Prime'
    WHERE Subsystem = 'OFAC' AND ObjectType = 'Tran'
    AND EXISTS(SELECT * FROM TempTranTable WHERE ObjectId = Convert(VARCHAR, SeqNumb))
  if (@@error <> 0)
    begin
      rollback tran OFS_ArchiveTran
      return 25 -- or other error code - need to define
    end

  Delete from FilterTranTable where SeqNumb in
    (Select SeqNumb from TempTranTable)
  if (@@error <> 0)
    begin
      rollback tran OFS_ArchiveTran
      return 25 -- or other error code - need to define
    end

  Insert into MatchHistTable Select a.* from MatchTable a, TempTranTable t
    where a.SeqNumb = t.SeqNumb
  if (@@error <> 0)
    begin
      rollback tran OFS_ArchiveTran
      return 25 -- or other error code - need to define
    end

  Delete from MatchTable where SeqNumb in
    (Select SeqNumb from TempTranTable)
  if (@@error <> 0)
    begin
      rollback tran OFS_ArchiveTran
      return 25 -- or other error code - need to define
    end

  Insert into MatchTextHistTable Select a.* from MatchTextTable a, TempTranTable t
    where a.SeqNumb = t.SeqNumb
  if (@@error <> 0)
    begin
      rollback tran OFS_ArchiveTran
      return 25 -- or other error code - need to define
    end

  Delete from MatchTextTable where SeqNumb in
    (Select SeqNumb from TempTranTable)
  if (@@error <> 0)
    begin
      rollback tran OFS_ArchiveTran
      return 25 -- or other error code - need to define
    end
	
  Insert into MsgHistTable Select a.* from MsgTable a, TempTranTable t
    where a.SeqNumb = t.SeqNumb
  if (@@error <> 0)
    begin

      rollback tran OFS_ArchiveTran
      return 25 -- or other error code - need to define
    end

  Delete from MsgTable where SeqNumb in
    (Select SeqNumb from TempTranTable)
  if (@@error <> 0)
    begin
      rollback tran OFS_ArchiveTran
      return 25 -- or other error code - need to define
    end

  Delete TempTranTable
  if (@@error <> 0)
    begin
      rollback tran OFS_ArchiveTran
      return 25 -- or other error code - need to define
    end

    if @trnCnt = 0
    commit tran OFS_ArchiveTran

  return 0  -- Success

GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_AddUpdateDowJonesSDN]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_AddUpdateDowJonesSDN]
Go

CREATE PROCEDURE dbo.OFS_AddUpdateDowJonesSDN( @entNum int,
                            @name SanctionName,
                            @firstName SanctionName,
                            @middleName SanctionName,
                            @lastName SanctionName,
                            @type char (12),
                            @program char(200),
                            @title varchar(200),
                            @callSign char(8),
                            @vessType char(25),
                            @tonnage char(14),
                            @grt char(8),
                            @vessFlag varchar(40),
                            @vessOwner varchar(150) ,
                            @dob varchar(150) ,
                            @remarks text ,
							@remarks1 varchar(255) ,
							@remarks2 varchar(255) ,
                            @sdnType int,
                            @dataType int,
                            @userRec int ,
                            @sex char,
                            @listID varchar(15),
                            @listType varchar(15),
                            @primeAdded bit,
                            --@standardOrder bit,
                            @effectiveDate datetime,
                            @expiryDate datetime,
                            @status int,
                            @listCreateDate datetime,
                            @listModifDate dateTime,
                            @oper Code,
                            @ignDrvNames BIT, 
							@DJPEP20Split BIT,
							@retEntnum int OUTPUT)
WITH ENCRYPTION 
AS
BEGIN
  declare @event		 varchar(3)
  --Transaction 
  declare @trnCnt        int
  -- Processing starts here:
  declare @now datetime
  declare @pos int
  declare @STAT int
  declare @text varchar(255)
  declare @maxCurrentNum int
  declare @minSDNIndex int = 7000000
  declare @maxSDNIndex int = 9999999
  declare @newMinSDNIndex int = 20000000
  declare @pep20program char (200)
  declare @newstatus int
  declare @retPEP20status int
  select @now = GETDATE()
  set @newstatus=null
  set @retPEP20status =null
  select @trnCnt = @@trancount  -- Save the current trancount

  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_AddUpdateDowJonesSDN
  else
    -- Already in a transaction
    save tran OFS_AddUpdateDowJonesSDN

  --If @type = ''
   -- select @type = 'other'

    Select ISNULL(@ignDrvNames, 1)

If @DJPEP20Split = 1
BEGIN
	set @pep20program= NULL
	-- checking whether the record exists in sdntable with DJ-PEP20, else check for DJ-PEP.
	-- if DJ-PEP20 is disabled from config,  and the record does not exists with both DJ-PEP20 and DJ-PEP20 then do not load DJ-PEP20 to db.
	-- if DJ-PEP20 is enabled and the record does not exists with both DJ-PEP20 and DJ-PEP20, then the existing logic will work and load the DJ-PEP20 record into db.

	If @program='DJ-PEP20'
	BEGIN
		If NOT Exists(SELECT EntNum FROM SDNTable WITH(NOLOCK) WHERE ListId=@listid AND Program=@program AND [Type] = @type)
		BEGIN
			IF  NOT Exists (SELECT EntNum FROM SDNTable WITH(NOLOCK) WHERE ListId=@listid AND Program='DJ-PEP' AND [Type] = @type)                                   
			BEGIN
				If  @status=6
				-- @status will be passed as 6 from Dow Jones utliity, if DJ-PEP20 is disabled in ProgramList configuration.In this case, do not load this record.
				BEGIN
					SELECT @retEntNum = 0
					GOTO ENDOFPROC
				END
			END
			ELSE
			BEGIN
				-- PEP exists block
				-- @status 6 will be passed from Utility if PEP20 is disabled. This status will not be updated anywhere.
				If  @status=6 
					Begin
					--DJ-PEP20 Disabled in ProgramList and PEP record exists in db which matches to the PEP20 input data. 
					--In this scenario change status =  4 in db. Also PEP wil be changed to PEP20 in below block.
						SET @newstatus=4					
					End
					
				--DJ-PEP20 Enabled in ProgramList and PEP record exists in db which matches to the PEP20 input data. 
				-- In this scenario remain status =  2 in db and change program PEP to PEP20.
				SET @pep20program=@program
				SET @program='DJ-PEP'
				
				SET @retPEP20status = 8888
				-- return this value 8888 to  calling program to identify PEP is changed to PEP20 
				-- setting the @program = 'DJ-PEP', so that below "if exists" block will satisfy and it will update the DJ-PEP record to DJ-PEP20
				-- @pep20program (DJ-PEP20) will be checked before updating @program in the below block.
			
			END
		END
		ELSE 
		BEGIN
			-- PEP20 exists block.			
			If  @status=6 
			-- DJ-PEP20 Disabled in ProgramList and PEP20 record exists in db.
			BEGIN
				SET @newstatus=4
			END
			
		END
		
	END
END

If Exists(SELECT EntNum FROM SDNTable WITH(NOLOCK) WHERE ListId=@listid AND Program=@program AND [Type] = @type)
BEGIN
	SELECT @EntNum=EntNum FROM SDNTable WITH(NOLOCK) WHERE ListId=@listid AND Program=@program AND [Type] = @type
	
	DECLARE @ProgramPEP20  varchar (200)
	DECLARE @StatusPEP20 int
	
	IF @DJPEP20Split = 1
	BEGIN
		SET @ProgramPEP20 = UPPER(isnull(@pep20program,@program)) 
		SET @StatusPEP20 = isnull(@newstatus,@status) 
	END
	ELSE
	BEGIN
		SET @ProgramPEP20=UPPER(@program) 
		SET @StatusPEP20=@status 
	END
	UPDATE SDNTable SET
                            [name]=@name,
                            firstName=@firstName ,
                            middleName=@middleName,
                            lastName=@lastName ,
                            type=@type ,
                            Program = @ProgramPEP20, 
                            title=@title ,
                            callSign=@callSign ,
                            vessType=@vessType ,
                            tonnage=@tonnage ,
                            grt=@grt ,
                            vessFlag=@vessFlag ,
                            vessOwner=@vessOwner ,
                            dob=@dob ,
                            remarks=@remarks ,
			    remarks1=@remarks1 ,
			    remarks2=@remarks2 ,
                            sdnType=@sdnType ,
                            dataType=@dataType ,
                            userRec=@userRec ,
                            IgnoreDerived = @ignDrvNames ,
                            sex=@sex ,
                            listID=@listID ,
                            listType=@listType ,
                            primeAdded=@primeAdded ,
                            --standardOrder=@standardOrder ,
                            --effectiveDate=@effectiveDate ,
                            --expiryDate=@expiryDate ,
                            Status = @StatusPEP20, 
                            --listCreateDate=@listCreateDate ,
                            listModifDate=@listModifDate ,
                            lastoper=@oper ,
                            LastModifDate=@now
		WHERE EntNum = @EntNum

  SELECT @STAT = @@ERROR
  IF @STAT <> 0  GOTO ENDOFPROC


	update Notes
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	update sdnaddrtable
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	update sdnalttable
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	update urls 
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	Update dobs
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	Update relationship
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC


   Select @event = 'MOD'
END
ELSE
BEGIN
		Select @maxCurrentNum = Max(EntNum) From SDNTable WHERE (EntNum between @minSDNIndex and @maxSDNIndex) OR (EntNum >= @newMinSDNIndex) 
		Select @STAT = @@ERROR

		If @STAT <> 0 Begin
			return @STAT
		End

		If @maxCurrentNum Is NULL Set @maxCurrentNum = 0

		If @maxCurrentNum < @minSDNIndex
		  Set @maxCurrentNum = @minSDNIndex
		else if @maxCurrentNum = @maxSDNIndex
			Set @maxCurrentNum = @newMinSDNIndex
		else
		  Set @maxCurrentNum = @maxCurrentNum +1
		Set @entNum = @maxCurrentNum

	
		Insert into SDNTable (EntNum, [Name], FirstName,
				MiddleName, LastName,Title,
				Program,Type, 
				CallSign,
				VessType, Tonnage, GRT,
				VessFlag, VessOwner, Dob,
				Remarks, Remarks1, Remarks2, SDNType,
				DataType, userRec, deleted,
				duplicRec, IgnoreDerived,
				ListID, ListType,
				Sex, 
				PrimeAdded, --StandardOrder, 
				--EffectiveDate,ExpiryDate, 
				[Status], ListCreateDate,
				ListModifDate, CreateDate, LastModifDate,
				LastOper)
		Values
			   (@entNum, @name, NULLIF(@firstName,''),
				NULLIF(@middleName,''), NULLIF(@lastName,''), @title,
				UPPER(@program), @type
				, @callSign,
				@vessType, @tonnage, @grt,
				@vessFlag, @vessOwner, NULLIF(@dob,''),
				@remarks,
				@remarks1,
				@remarks2,				
				@sdnType,
				@dataType, @userRec, 0,
				0, @ignDrvNames,
				@listId, @listType,
				@sex,
				@primeAdded, --@standardOrder,
				 --@effectiveDate,@expiryDate, 
				@status, @listCreateDate,
				@listModifDate, @now, @now, @oper)
  Select @STAT = @@ERROR
  IF @STAT <> 0  GOTO ENDOFPROC
		Select @event = 'CRE'
END
	IF @DJPEP20Split = 1
	BEGIN
		If @program='DJ-PEP20' or @pep20program = 'DJ-PEP20'
			Select @retEntNum = isnull(@retPEP20status,@entNum)
		Else
			Select @retEntNum = @entNum
	END
	ELSE
		Select @retEntNum = @entNum

    If @STAT = 0 Begin
      Exec OFS_InsEvent @oper, @event, 'SanName', @name, Null
      select @STAT = @@ERROR
      If @STAT = 0 Begin
          Exec OFS_SetFileImageFlag
      End
  End
  
  EndofProc:
  IF @trnCnt = 0 BEGIN
	COMMIT TRAN OFS_AddUpdateDowJonesSDN
  END ELSE IF @STAT <> 0 BEGIN
	ROLLBACK TRAN OFS_AddUpdateDowJonesSDN
  END 
  RETURN @STAT
END
GO

----------------------------------------------------------------------------------------------
If EXISTS (select * from dbo.sysobjects 
	where id = object_id(N'[dbo].[OFS_DelOtherDowJones_Program]') and 
				OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OFS_DelOtherDowJones_Program]
	
Go

CREATE PROCEDURE dbo.OFS_DelOtherDowJones_Program

(@Program VARCHAR(200))
WITH ENCRYPTION AS

BEGIN

  DECLARE @stat INT
  DECLARE @trnCnt INT
  SET NOCOUNT ON

  DECLARE @ROWCOUNT INT = 1 
  DECLARE @C INT = 10000
  DECLARE @BatchIDMax Bigint
  DECLARE @oper SCode  
  SELECT @trnCnt = @@trancount  -- Save the current trancount
  SET @oper = 'Prime'

  IF @trnCnt = 0
    -- Transaction has not begun
    BEGIN TRAN OFS_DelOtherDowJones_Program
  ELSE
    -- Already in a transaction
    SAVE TRAN OFS_DelOtherDowJones_Program
	-- ParseDelimiterString will split the comma seperated list and indert into a table.
	-- stringvalue will be having program values that is passed from DowJones config file, that needs to be excluded.
  SELECT stringvalue INTO #ProgramListExclude FROM dbo.ParseDelimiterString (@Program,',')

DECLARE @EntNums AS TABLE
(
ID int IDENTITY(1,1),
EntNum bigint,
Program varchar(200),
UNIQUE(EntNum)
)

DECLARE @BatchEntNums AS TABLE
(
ID int,
EntNum bigint,
UNIQUE(EntNum)
)

	-- Charindex() function is used because DJ-SIP and DJ_SIE should be matched properly.
	-- For example DJ-SIE will be  coming from Utility and it should be matched for programs like DJ-SIE-594,DJ-SIE-2491                                                         

	DECLARE @POS INT
	SET @POS  = Charindex('DJ-PEP' ,@Program)
	--IF PEP is Passed, it should not match againt PEP20 while using Charindex function and it should not soft delete soft delete.
	--Using PATINDEX for exact match.
	IF @POS> 0               
	BEGIN
		INSERT INTO @EntNums 
		SELECT s.EntNum, rtrim(ltrim(s.Program)) FROM SDNTable s
		INNER JOIN #ProgramListExclude p ON p.stringvalue  = rtrim(ltrim(s.Program)) 
		WHERE s.ListType='USER' and p.stringvalue IN('DJ-PEP','DJ-PEP20')
		
	END
	
	-- Below block is for other programs like DJ-SIE-594,DJ-SIE-2491 and RCA, where DJ-SIE-594 should be matching for DJ-SIE
	
		INSERT INTO @EntNums 
		SELECT s.EntNum, rtrim(ltrim(s.Program)) FROM SDNTable s
		INNER JOIN #ProgramListExclude p ON Charindex(p.stringvalue ,rtrim(ltrim(s.Program))) > 0
		WHERE s.ListType='USER' and p.stringvalue NOT IN('DJ-PEP', 'DJ-PEP20')
	

 
	SELECT @stat = @@ERROR 
	IF @stat <> 0  GOTO ENDOFPROC

	
	INSERT INTO @BatchEntNums 
	SELECT TOP(@C) ID,Entnum  FROM @EntNums 

	SELECT @BatchIDMax = MAX(ID) from @BatchEntNums  
	SET @ROWCOUNT = @@ROWCOUNT 

	WHILE (@ROWCOUNT > 0) 
	BEGIN 

		UPDATE Notes set  Status = 4, LastModify  = getDate() , LastOper = @oper
		FROM Notes INNER JOIN @BatchEntNums Ent
		ON Notes.EntNum = Ent.EntNum where status<>4

		SELECT @stat = @@ERROR 
		IF @stat <> 0  GOTO ENDOFPROC

		UPDATE SDNAddrTable Set Status = 4, LastModifDate = getDate(),LastOper = @oper
		FROM SDNAddrTable INNER JOIN @BatchEntNums Ent
		ON SDNAddrTable.EntNum = Ent.EntNum where status<>4

		SELECT @stat = @@ERROR 
		IF @stat <> 0  GOTO ENDOFPROC

		UPDATE SDNAltTable Set Status = 4, LastModifDate = getDate(),LastOper = @oper
		FROM SDNAltTable INNER JOIN @BatchEntNums Ent
		ON SDNAltTable.EntNum = Ent.EntNum where status<>4
  
		SELECT @stat = @@ERROR 
		IF @stat <> 0  GOTO ENDOFPROC

		UPDATE DOBs Set Status = 4, LastModify  = getDate(),LastOper = @oper
		FROM DOBs INNER JOIN @BatchEntNums Ent
		ON DOBs.EntNum = Ent.EntNum where status<>4

		SELECT @stat = @@ERROR 
		IF @stat <> 0  GOTO ENDOFPROC

		UPDATE URLs Set Status = 4, LastModify = getDate(),LastOper = @oper
		FROM URLs INNER JOIN @BatchEntNums Ent
		ON URLs.EntNum = Ent.EntNum where status<>4

		SELECT @stat = @@ERROR 
		IF @stat <> 0  GOTO ENDOFPROC

  
		UPDATE Relationship Set Status = 4, LastModify  = getDate(),LastOper = @oper
		FROM Relationship INNER JOIN @BatchEntNums Ent
		ON Relationship.RelatedId = Ent.EntNum where status<>4
 
		SELECT @stat = @@ERROR 
		IF @stat <> 0  GOTO ENDOFPROC


		Update Keywords Set Status = 4, LastModify = getDate(),LastOper = @oper
		FROM Keywords INNER JOIN @BatchEntNums Ent on
		Keywords.EntNum = Ent.EntNum where Keywords.Status <> 4

		SELECT @stat = @@ERROR 
		IF @stat <> 0  GOTO ENDOFPROC

		Update KeywordsAlt Set Status = 4, LastModify = getDate(),LastOper = @oper
		FROM KeywordsAlt k with (nolock) INNER JOIN  SDNAltTable sa with(nolock) ON 
		sa.AltNum = k.AltNum
		INNER JOIN  @BatchEntNums Ent on 
		Ent.EntNum =sa.EntNum 
		Where       k.Status <> 4


		SELECT @stat = @@ERROR 
		IF @stat <> 0  GOTO ENDOFPROC


		UPDATE SDNTable  Set Status = 4, LastModifDate = getDate(),LastOper = @oper
		FROM SDNTable INNER JOIN @BatchEntNums Ent
		ON SDNTable.EntNum = Ent.EntNum where SDNTable.status<>4
		
		SELECT @stat = @@ERROR
		IF @stat <> 0  GOTO ENDOFPROC

		DELETE FROM @BatchEntNums 

		SELECT @stat = @@ERROR
		IF @stat <> 0  GOTO ENDOFPROC

		INSERT INTO @BatchEntNums
		SELECT TOP(@C) ID, EntNum  FROM @Entnums WHERE ID>@BatchIDMax


		SELECT @stat = @@ERROR
		IF @stat <> 0  GOTO ENDOFPROC

		SELECT @BatchIDMax = MAX(ID), @ROWCOUNT = count(entnum)  from @BatchEntNums  

	END


	ENDOFPROC:  
		IF @trnCnt = 0 BEGIN
			COMMIT TRAN OFS_DelOtherDowJones_Program
		END ELSE IF @stat <> 0 BEGIN
			ROLLBACK TRAN OFS_DelOtherDowJones_Program
		END
		RETURN @stat

END
GO


IF NOT EXISTS (SELECT * FROM dbo.sysobjects 
		where id = object_id(N'[dbo].[TempRCAExcludeList]') and 
					OBJECTPROPERTY(id, N'IsTable') = 1)
					Begin

CREATE TABLE [dbo].[TempRCAExcludeList](
Relationship varchar(50)
)
End
Go


-- *** Caution Below code block should be executed before creating stored procedure [OFS_DelUpdateRCAExcludeList] *** ----
IF EXISTS(SELECT 1 FROM sys.types WHERE name = 'RCAExclude' AND is_table_type = 1)
BEGIN
	--Before deleting the user defined types, other dependent  objects from db also needs to be deleted. In this case, OFS_DelUpdateRCAExcludeList is related to 
	--RCAExclude type and hence dropping it, Otherwise below error will be thrown if we try to delete the user defined type RCAExclude.
	-- Cannot drop type 'dbo.RCAExclude' because it is being referenced by object 'OFS_DelUpdateRCAExcludeList'. There may be other objects that reference this type.
	If exists (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_DelUpdateRCAExcludeList]') and 
                                                                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                drop procedure [dbo].[OFS_DelUpdateRCAExcludeList]
				
	DROP TYPE dbo.RCAExclude
END

CREATE TYPE dbo.RCAExclude
AS TABLE
(
  RelatioshipList NVARCHAR(200)
)

GO
----------------------------------------------------------------------------------------------------------------------------------------
If EXISTS (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_DelUpdateRCAExcludeList]') and 
                                                                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                DROP PROCEDURE [dbo].[OFS_DelUpdateRCAExcludeList]
Go

CREATE PROCEDURE dbo.OFS_DelUpdateRCAExcludeList(@CreateDate datetime, @RCAExcludeList AS dbo.RCAExclude READONLY) With Encryption
As 
BEGIN

  SET NOCOUNT ON

  DECLARE @ROWCOUNT INT = 1 
  DECLARE @C INT = 10000
  DECLARE @stat INT
  DECLARE @trnCnt INT
  DECLARE @BatchIDMax int
  DECLARE @oper SCode
  DECLARE @Del VARCHAR(5)
  DECLARE @Mod VARCHAR(5)
  
  SELECT @trnCnt = @@trancount  
  SET @oper = 'Prime'
  SET @Del ='DEL'
  SET @Mod ='MOD'

  IF @trnCnt = 0
    -- Transaction has not begun
    BEGIN TRAN OFS_DelUpdateRCAExcludeList
  ELSE
    -- Already in a transaction
    SAVE TRAN OFS_DelUpdateRCAExcludeList

	INSERT dbo.TempRCAExcludeList(Relationship) 
	  SELECT RelatioshipList 
	  FROM @RCAExcludeList

SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

DECLARE @TempTable#01 AS TABLE
(
EntNum int,
Name varchar(350),
UNIQUE (Entnum)
)

DECLARE @TempTable#02 AS TABLE
(
EntNum int,
UNIQUE (Entnum)
)

DECLARE @TempFinal AS TABLE
(
Name varchar(350),
EntNum int,
UNIQUE (Entnum)
)

DECLARE @TempSDNDelete AS TABLE
(
ID int IDENTITY(1,1),
Name varchar(350),
EntNum int,
RelatedID int,
ListID varchar(15)
)

DECLARE @TempSDNDeleteBatch AS TABLE
(
ID int,
EntNum int,
UNIQUE (Entnum)
)

DECLARE @TempSDNUpdate AS TABLE
(
ID int IDENTITY(1,1),
Name varchar(350),
EntNum int,
UNIQUE (Entnum)
)

DECLARE @TempSDNUpdateBatch AS TABLE
(
ID int,
EntNum int,
UNIQUE (Entnum)
)

DECLARE @TempEventLog AS TABLE
(
LogDate Datetime DEFAULT CURRENT_TIMESTAMP,
Oper varchar(10) DEFAULT 'Prime',
LogText varchar(2000) DEFAULT 'NULL',
Type varchar(11),
ObjectType varchar(11) DEFAULT 'SanName',
ObjectId varchar(350),
EvtDetail varchar(16) DEFAULT 'NULL'
)

INSERT INTO @TempTable#01 
SELECT DISTINCT S.ENTNUM, S.Name FROM SDNTable S WITH (NOLOCK) 
INNER JOIN Relationship R with (nolock)  ON S.ENTNUM = R.ENTNUM
INNER JOIN SDNTable SP with (nolock) ON R.RelatedID = SP.ENTNUM
WHERE S.Program ='DJ-RCA' and S.Status <> 4 AND SP.Program ='DJ-PEP'
AND R.RelationshipPToR IN (select Relationship from TempRCAExcludeList) OPTION (RECOMPILE)

SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

INSERT INTO @TempTable#02
SELECT DISTINCT S.ENTNUM  FROM SDNTable S WITH (NOLOCK) 
INNER JOIN Relationship R with (nolock)  ON S.ENTNUM = R.ENTNUM
INNER JOIN SDNTable SP with (nolock)  ON R.RelatedID = SP.ENTNUM
WHERE S.Program ='DJ-RCA' and S.Status <> 4 AND SP.Program ='DJ-PEP'
AND R.RelationshipPToR NOT IN (select Relationship from TempRCAExcludeList) OPTION (RECOMPILE)

SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

INSERT INTO @TempFinal 
SELECT T1.Name, T1.EntNum from 
@TempTable#01 T1 left join @TempTable#02 T2 on t1.EntNum = t2.EntNum where T2.EntNum is null OPTION (RECOMPILE)

SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

INSERT INTO @TempSDNDelete 
SELECT S.Name,T1.ENTNUM, R.RelatedID,S.ListID FROM @TempFinal T1 
INNER JOIN SDNTABLE S WITH (NOLOCK) ON T1.ENTNUM = S.ENTNUM
LEFT JOIN Relationship R WITH (NOLOCK) ON T1.ENTNUM = R.RelatedID
WHERE S.STATUS = 2 AND R.RelatedID IS NULL AND S.CREATEDATE >= @CreateDate OPTION (RECOMPILE)

SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

INSERT INTO @TempSDNUpdate 
SELECT T1.Name,T1.EntNum from 
@TempFinal T1 left join @TempSDNDelete T2 on t1.EntNum = t2.EntNum where T2.EntNum is null OPTION (RECOMPILE)

SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

INSERT INTO @TempSDNDeleteBatch
	SELECT TOP(@C) ID,Entnum  FROM @TempSDNDelete

SELECT @BatchIDMax = MAX(ID) from @TempSDNDeleteBatch 
SET @ROWCOUNT = @@ROWCOUNT 
 
WHILE (@ROWCOUNT > 0) 
	BEGIN 

		DELETE N FROM Notes N WITH (NOLOCK)
		INNER JOIN @TempSDNDeleteBatch t
			ON t.Entnum = N.Entnum 

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		DELETE SA FROM sdnaddrtable SA WITH (NOLOCK)
		INNER JOIN @TempSDNDeleteBatch t
			ON t.Entnum = SA.Entnum 

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		DELETE  SAl FROM sdnalttable SAl WITH (NOLOCK)
		INNER JOIN @TempSDNDeleteBatch t
			ON t.Entnum = SAl.Entnum 

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		DELETE D FROM dobs D WITH (NOLOCK)
		INNER JOIN @TempSDNDeleteBatch t
			ON t.Entnum = D.Entnum 

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		DELETE U FROM urls U WITH (NOLOCK)
		INNER JOIN @TempSDNDeleteBatch t
			ON t.Entnum = U.Entnum 

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		DELETE R FROM Relationship R WITH (NOLOCK)
		INNER JOIN @TempSDNDeleteBatch t
			ON t.Entnum = R.Entnum 

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		DELETE K FROM Keywords K WITH (NOLOCK)
		INNER JOIN @TempSDNDeleteBatch t
			ON t.Entnum = K.Entnum 

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		DELETE KA FROM KeywordsAlt KA WITH (NOLOCK)
		INNER JOIN SDNAltTable SA ON 
              SA.AltNum = KA.AltNum
        INNER JOIN  @TempSDNDeleteBatch t 
              ON t.EntNum =sa.EntNum 

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		DELETE S FROM SDNTable S WITH (NOLOCK)
		INNER JOIN @TempSDNDeleteBatch t
			ON t.Entnum = S.Entnum 

		SELECT @STAT = @@ERROR 
		IF @STAT <> 0  GOTO ENDOFPROC

		DELETE FROM @TempSDNDeleteBatch 

		SELECT @STAT = @@ERROR 
		IF @STAT <> 0  GOTO ENDOFPROC
		INSERT INTO @TempSDNDeleteBatch
			SELECT TOP(@C) ID, EntNum  FROM @TempSDNDelete WHERE ID>@BatchIDMax

	     SELECT @STAT = @@ERROR 
		 IF @STAT <> 0  GOTO ENDOFPROC
		SELECT @BatchIDMax = MAX(ID), @ROWCOUNT = count(entnum)  from @TempSDNDeleteBatch  
END

SET @C = 10000

INSERT INTO @TempSDNUpdateBatch 
	SELECT TOP(@C) ID,Entnum  FROM @TempSDNUpdate 

SELECT @BatchIDMax = MAX(ID) from @TempSDNUpdateBatch  
SET @ROWCOUNT = @@ROWCOUNT 

WHILE (@ROWCOUNT > 0) 
	BEGIN 

		UPDATE N set Status=4,LastModify = getDate(), LastOper = @oper FROM Notes N WITH (NOLOCK)
		INNER JOIN @TempSDNUpdateBatch  t
			ON t.Entnum = N.Entnum WHERE N.Status <> 4 

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		UPDATE SA set Status=4,LastModifDate = getDate(), LastOper = @oper  FROM sdnaddrtable SA WITH (NOLOCK)
		INNER JOIN @TempSDNUpdateBatch t
			ON t.Entnum = SA.Entnum WHERE SA.Status <> 4

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		UPDATE SAl set Status=4,LastModifDate = getDate(), LastOper = @oper  FROM sdnalttable SAl WITH (NOLOCK)
		INNER JOIN @TempSDNUpdateBatch t
			ON t.Entnum = SAl.Entnum WHERE SAl.Status <> 4

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		UPDATE D set Status=4,LastModify = getDate(), LastOper = @oper FROM dobs D WITH (NOLOCK)
		INNER JOIN @TempSDNUpdateBatch t
			ON t.Entnum = D.Entnum WHERE D.Status <> 4

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		UPDATE U set Status=4,LastModify = getDate(), LastOper = @oper FROM urls U WITH (NOLOCK)
		INNER JOIN @TempSDNUpdateBatch t
			ON t.Entnum = U.Entnum WHERE U.Status <> 4

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		UPDATE R set Status=4,LastModify = getDate(), LastOper = @oper FROM Relationship R WITH (NOLOCK)
		INNER JOIN @TempSDNUpdateBatch t
			ON t.Entnum = R.Entnum WHERE R.Status <> 4

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		UPDATE K set Status = 4, LastModify = getDate(), LastOper = @oper
        FROM Keywords K WITH (NOLOCK)
		INNER JOIN @TempSDNUpdateBatch t on
        K.EntNum = t.EntNum WHERE K.Status <> 4

        SELECT @stat = @@ERROR 
        IF @stat <> 0  GOTO ENDOFPROC

        UPDATE KA Set Status = 4, LastModify = getDate(), LastOper = @oper
        FROM KeywordsAlt kA WITH (NOLOCK) INNER JOIN  SDNAltTable SA WITH (NOLOCK) ON 
        SA.AltNum = KA.AltNum
        INNER JOIN  @TempSDNUpdateBatch t on 
        t.EntNum = SA.EntNum Where KA.Status <> 4

		SELECT @stat = @@ERROR 
        IF @stat <> 0  GOTO ENDOFPROC

		UPDATE S set Status=4,LastModifDate = getDate(), LastOper = @oper
		FROM SDNTable S WITH (NOLOCK) INNER JOIN @TempSDNUpdateBatch t
			ON t.Entnum = S.Entnum Where S.Status <> 4

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		DELETE FROM @TempSDNUpdateBatch  

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		INSERT INTO @TempSDNUpdateBatch
			SELECT TOP(@C) ID, EntNum  FROM @TempSDNUpdate WHERE ID>@BatchIDMax

		SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

		SELECT @BatchIDMax = MAX(ID), @ROWCOUNT = count(entnum)  from @TempSDNUpdateBatch   

END

-- Insert soft/hard deleted records into Event table
INSERT INTO @TempEventLog (Type,ObjectId) 
 SELECT @Del,Left(Name,100) FROM @TempSDNDelete

SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

INSERT INTO @TempEventLog (Type,ObjectId)
       SELECT @Mod,Left(Name,100) FROM @TempSDNUpdate 

SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

INSERT INTO SDNChangeLog 
	SELECT * FROM @TempEventLog

SELECT @STAT = @@ERROR 
		  IF @STAT <> 0  GOTO ENDOFPROC

EndOfProc:
  
  IF @trnCnt = 0 BEGIN
	COMMIT TRAN OFS_DelUpdateRCAExcludeList
  END ELSE IF @stat <> 0 BEGIN
    	ROLLBACK TRAN OFS_DelUpdateRCAExcludeList
  END
  DELETE FROM TempRCAExcludeList
  SELECT ListID from @TempSDNDelete 
  SELECT EntNum from @TempSDNUpdate 

 END
GO

IF NOT EXISTS(select * from sys.sysobjects  where name = 'CodeSpecialChar' and xtype='R')
BEGIN
	DECLARE @sql NVARCHAR(max)
    SET @sql = N'Create rule CodeSpecialChar as @codeSplChar not like ''%[^0-9A-Za-z\-]%''' 
	EXECUTE sp_executesql @sql
END

GO
--Overwrite the rule 'Code' with the new rule 'CodeSpecialChar'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'AbbrevTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'BlockDerivedNames.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ClientIntfTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CommonWords.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ExcludeNames.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'FilterTranHistTable.ConfirmOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'FilterTranHistTable.AppOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'FilterTranHistTable.LockOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'FilterTranHistTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'FilterTranTable.ConfirmOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'FilterTranTable.AppOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'FilterTranTable.LockOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'FilterTranTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'InEqualityTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'InfixWords.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IPCTypeTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'NoiseWords.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'OFACConfigTable.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'OFACConfigTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PrefixWords.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SDNAddrTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SDNAltTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SDNChangeLog.Oper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SDNTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuffixWords.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SystemParams.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempDelKeywordsAltEntry.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempDelKeywordsEntry.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadAddrTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadAltTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadSDNTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempSDNAddrTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempSDNAltTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempSDNTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TranTypeTable.LastOper'

EXEC sp_bindrule 'dbo.CodeSpecialChar', 'BlockDerivedNames.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CIFData.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CIFData.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CIFRules.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CIFRules.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'DOBs.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'DOBs.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'EventType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'EventType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'InfixWords.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Keywords.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Keywords.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'KeywordsAlt.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'KeywordsAlt.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ListType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ListType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Notes.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Notes.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'NoteType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'NoteType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ObjectType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ObjectType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'OFACSearchResults.OperCode'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'OptionTbl.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'OptionTbl.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PrefixWords.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Relationship.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Relationship.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Report.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Report.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SanctionDataRule.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SanctionDataRule.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SourceTable.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SourceTable.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuffixWords.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadDOBs.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadDOBs.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadKeywords.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadKeywords.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadKeywordsAlt.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadKeywordsAlt.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadNotes.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadNotes.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadRelationship.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadRelationship.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadURLs.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempLoadURLs.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'URLs.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'URLs.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CIFData.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CIFData.CreateOper'

Go
If EXISTS (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_AddUpdateWorldCheckSDN]') and 
                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                DROP PROCEDURE [dbo].[OFS_AddUpdateWorldCheckSDN]
Go

CREATE PROCEDURE dbo.OFS_AddUpdateWorldCheckSDN( @entNum int,
                            @name SanctionName,
                            @firstName SanctionName,
                            @middleName SanctionName,
                            @lastName SanctionName,
                            @type char (12),
                            @program varchar(200),
                            @title varchar(200),
                            @callSign char(8),
                            @vessType char(25),
                            @tonnage char(14),
                            @grt char(8),
                            @vessFlag varchar(40),
                            @vessOwner varchar(150) ,
                            @dob varchar(150) ,
                            @remarks text ,
                            @remarks1 varchar(255) ,
                            @remarks2 varchar(255) ,
                            @sdnType int,
                            @dataType int,
                            @userRec int ,
                            @country varchar(250),
                            @sex char,
                            @build varchar(15),
                            @complexion varchar(15),
                            @race varchar(15),
                            @eyes varchar(15),
                            @hair varchar(15),
                            @height int,
                            @weight int,
                            @listID varchar(15),
                            @listType varchar(15),
                            @primeAdded bit,
                            @standardOrder bit,
                            @effectiveDate datetime,
                            @expiryDate datetime,
                            @status int,
                            @listCreateDate datetime,
                            @listModifDate dateTime,
                            @oper Code,
                            @ignDrvNames BIT, 
                @retEntnum int OUTPUT)
WITH ENCRYPTION AS
BEGIN
  declare @event		 varchar(3)
  --Transaction 
  declare @trnCnt        int
  -- Processing starts here:
  declare @now datetime
  declare @pos int
  declare @stat int
  declare @text varchar(255)
  declare @maxNum int
  declare @minSDNIndex int = 7000000
  declare @maxSDNIndex int = 9999999
  declare @newMinSDNIndex int = 20000000
  select @now = GETDATE()

  select @trnCnt = @@trancount  -- Save the current trancount

  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_AddUpdateWorldCheckSDN
  else
    -- Already in a transaction
    save tran OFS_AddUpdateWorldCheckSDN

  If @type = ''
    select @type = 'other'

    Select ISNULL(@ignDrvNames, 1)

If Exists(SELECT 1 FROM SDNTable WITH (NOLOCK) WHERE ListId=@listid AND Program=@program and remarks2=@remarks2)
BEGIN
	SELECT @EntNum=EntNum FROM SDNTable WITH (NOLOCK) WHERE ListId=@listid AND Program=@program and remarks2=@remarks2
	UPDATE SDNTable SET
                            [name]=@name,
                            firstName=@firstName ,
                            middleName=@middleName,
                            lastName=@lastName ,
                            type=@type ,
                            program=@program ,
                            title=@title ,
                            callSign=@callSign ,
                            vessType=@vessType ,
                            tonnage=@tonnage ,
                            grt=@grt ,
                            vessFlag=@vessFlag ,
                            vessOwner=@vessOwner ,
                            dob=@dob ,
                            remarks=@remarks ,
                            remarks1=@remarks1 ,
                            remarks2=@remarks2 ,
                            sdnType=@sdnType ,
                            dataType=@dataType ,
                            userRec=@userRec ,
                            IgnoreDerived = @ignDrvNames ,
                            country=@country ,
                            sex=@sex ,
                            build=@build ,
                            complexion=@complexion ,
                            race=@race ,
                            eyes=@eyes ,
                            hair=@hair ,
                            height=@height ,
                            weight=@weight ,
                            listID=@listID ,
                            listType=@listType ,
                            primeAdded=@primeAdded ,
                            standardOrder=@standardOrder ,
                            effectiveDate=@effectiveDate ,
                            expiryDate=@expiryDate ,
                            status=3 ,
                            listCreateDate=@listCreateDate ,
                            listModifDate=@listModifDate ,
                            lastoper=@oper
		WHERE EntNum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC


	update Notes
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	update sdnaddrtable
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	update sdnalttable
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	update urls 
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	Update dobs
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	Update relationship
    Set Status = 4 --deleted
    where entnum = @EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC


   Select @event = 'MOD'
END
ELSE
BEGIN
		Select @maxNum = Max(EntNum) From SDNTable WHERE (EntNum between @minSDNIndex and @maxSDNIndex)  OR (EntNum >= @newMinSDNIndex)
		Select @stat = @@ERROR

		If @stat <> 0 Begin
			return @stat
		End

		If @maxNum Is NULL Set @maxNum = 0		
		
		If @maxNum < @minSDNIndex
		  Set @maxNum = @minSDNIndex
		else If @maxNum = @maxSDNIndex 
		  Set @maxNum = @newMinSDNIndex
		else
		  Set @maxNum = @maxNum +1
		Set @entNum = @maxNum

		Insert into SDNTable (EntNum, [Name], FirstName,
				MiddleName, LastName,Title,
				Program, Type, CallSign,
				VessType, Tonnage, GRT,
				VessFlag, VessOwner, Dob,
				Remarks, Remarks1, Remarks2,SDNType,
				DataType, userRec, deleted,
				duplicRec, IgnoreDerived,
				ListID, ListType, Country,
				Sex, Build, Height, Weight,
				Race, Complexion, eyes, hair,
				PrimeAdded, StandardOrder, EffectiveDate,
				ExpiryDate, Status, ListCreateDate,
				ListModifDate, CreateDate, LastModifDate,
				LastOper)
		Values
			   (@entNum, @name, NULLIF(@firstName,''),
				NULLIF(@middleName,''), NULLIF(@lastName,''), @title,
				UPPER(@program),  @type,  @callSign,
				@vessType, @tonnage, @grt,
				@vessFlag, @vessOwner, NULLIF(@dob,''),
				@remarks, @remarks1, @remarks2, @sdnType,
				@dataType, @userRec, 0,
				0, @ignDrvNames,
				@listId, @listType, @country,
				@sex, @build, NullIf(@height,0), NullIf(@weight,0),
				@race, @complexion, @eyes, @hair,
				@primeAdded, @standardOrder, @effectiveDate,
				@expiryDate, 2, @listCreateDate,
				@listModifDate, @now, @now, @oper )

  Select @stat = @@ERROR
  IF @STAT <> 0  GOTO ENDOFPROC

		Select @event = 'CRE'
END
  Select @retEntNum = @entNum
    If @stat = 0 Begin
      Exec OFS_InsEvent @oper, @event, 'SanName', @name, Null
      select @stat = @@ERROR
      If @stat = 0 Begin
          Exec OFS_SetFileImageFlag
      End
  End
  
  EndofProc:
  IF @trnCnt = 0 BEGIN
	COMMIT TRAN OFS_AddUpdateWorldCheckSDN
  END ELSE IF @stat <> 0 BEGIN
	ROLLBACK TRAN OFS_AddUpdateWorldCheckSDN
  END 
  RETURN @stat
END
GO

If EXISTS (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_InsNotes]') and 
                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                DROP PROCEDURE [dbo].[OFS_InsNotes]
Go
Create PROCEDURE OFS_InsNotes (
    @notesId  IdType,
    @entnum   IdType,
    @NoteType IDType,
    @Note     NoteDataType,
    @Status   INT,
    @ListCreateDate datetime,
    @ListModifDate  datetime,
    @oper     SCode
) WITH ENCRYPTION AS
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_InsNotes
  else -- Already in a transaction
    save tran OFS_InsNotes
  -- End standard stored procedure transaction header

  declare @now datetime
  declare @stat int

  declare @Existing  int 
  declare @Unchanged int 
  declare @newMinSDNIndex int = 20000000

  set @Existing  = 0
  set @Unchanged = 0

  select @now = GETDATE()

  --World Check block. Persist ID, Set status
  if ((@entNum between 7000000 and 9999999) OR (@entNum >= @newMinSDNIndex))
   begin
		if ( @notesId  != 0)
		 begin
			--Check by Primary Key
			select @Existing = count(NotesId)
			from Notes WITH (NOLOCK)

			where EntNum = @entNum and 
				  NotesID = @notesId and
				  Note = @Note and
				  NoteType = @NoteType 

			if @Existing = 0
			 begin
				--Insert new record
				set  @notesId  = 0
			 end
			else
			 begin
				-- Existing record unchanged
				update Notes
				set Status = 1 --unchanged
				where EntNum = @entNum and 
				      NotesId = @notesId  
			 end	
		  end

		if @Existing = 0
		 begin
			--Find record by Note
			select @Existing = count(NotesID)
			from Notes WITH (NOLOCK)

			where EntNum = @entNum and 
				  Status = 4 and
				  Note = @Note and
				  NoteType = @NoteType
              
        
			if @Existing = 0
				set @status = 2 --added
			else
			 begin
				select @Unchanged = count(NotesID)
				from Notes
				where EntNum = @entNum and 
					  Status = 4 and
					  Note = @Note and
					  NoteType = @NoteType 
			
                  

				if @Existing = 1
				begin
					--Unique entry exists
					if @Unchanged  = 0
						update Notes
						set  NoteType = @NoteType,
							 ListModifDate = @listModifDate,
							LastModify =  @now, 
							LastOper = @oper,
							Status = 3 --Modified					 
						where EntNum  = @entNum and
							  Status = 4 and 
							Note = @Note and
							NoteType = @NoteType
					else
						update Notes
						set  Status = 1 -- unchanged					 
						where EntNum  = @entNum and
							  Status = 4 and 
							  Note = @Note and
							  NoteType = @NoteType		   
                      
			     end

			else if @Existing > 1 and @Unchanged  > 1
			  begin
				--Existing entry is not unique but no changes
				update Notes
				set  Status = 1 -- unchanged					 
				where EntNum = @entNum and 
					  Status = 4 and
					  Note = @Note and
					  NoteType = @NoteType 

			  end
			else
			  begin
				--Set new entry
				set @Existing = 0
				set @status = 2 --added
			  end
		   end
	end
   end

  if  @Existing = 0
   begin
		-- the following if block is added for
		-- user entered notes (attributes)
		if @notesId = 0
		 begin
			--if there are no notes available in the notes table
			--then set the notesid to 1
			select @notesId = max(notesid) from notes 
			select @notesId = isnull(@notesid, 10000000)
			if @notesid < 10000000
				SET @notesid = 10000000

			select @notesId = @notesId + 1
		 end
		 else
		 begin
			-- check whether notes available in the notes table
			-- if exists then set notesid using 10000000
			
			if exists (select 1 from Notes with(nolock) where NotesId = @notesId)
			begin
				select @notesId = max(notesid) from notes with(nolock)
				select @notesId = isnull(@notesid, 10000000)
				if @notesid < 10000000
					SET @notesid = 10000000

				select @notesId = @notesId + 1
			end			
		 end

		if IsNull(@status, 0) = 0 -- if unknown status
			set @status = 2 -- set new status

		 insert into Notes (Entnum, NotesId, NoteType, Note, Status, CreateOper, CreateDate, LastOper, LastModify)
		values (@entnum, @notesId, @NoteType, @Note, @Status, @oper, @now, @oper, @now)

 end

  -- Start standard stored procedure transaction footer
  select @stat = @@ERROR
  if @stat = 0
  begin
    Exec OFS_SetFileImageFlag
    if @trnCnt = 0 commit tran OFS_InsNotes
  end
  else
    rollback tran OFS_InsNotes
  return @stat
  -- End standard stored procedure transaction footer
GO

If EXISTS (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_InsDOBs]') and 
                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                DROP PROCEDURE [dbo].[OFS_InsDOBs]
Go
Create PROCEDURE dbo.OFS_InsDOBs (
            @DOBsId         IDType,
            @EntNum         IDType,
            @dob            char (8),
            @status         int,
            @ListCreateDate Datetime,
            @ListModifDate  Datetime,
            @oper           SCode
) WITH ENCRYPTION AS
 -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_InsDOBs
  else -- Already in a transaction
    save tran OFS_InsDOBs
  -- End standard stored procedure transaction header

  declare @now datetime
  declare @stat int

declare @Existing  int 
  declare @Unchanged int 

  set @Existing  = 0
  set @Unchanged = 0

  select @now = GETDATE()

  --World Check block. Persist ID, Set status
   if @entNum >= 7000000 
   begin

		if ( @DOBsId != 0)
		 begin
			--Check by Primary Key
			select @Existing = count(DOBsId)
			from DOBs WITH (NOLOCK)
			where EntNum = @entNum and 
				  DOBsId = @DOBsId and
				  DOB = @dob

			if @Existing = 0
			 begin
				If @entNum>= 10000000 and @EntNum <= 19999999 --19999999 user defined max range
					begin
					select @Existing =  count(DOBsId)
						from DOBs with (nolock)
						where EntNum = @entNum and 
							  DOBsId = @DOBsId and
							  (IsNull(DOB,'') <> IsNull(@dob,''))
					 if @Existing = 0
					 Begin
						--Insert new record
						set  @DOBsId  = 0
					 End
					 Else
					 Begin
						-- Existing record modified
						update DOBs
						set 
						Status  = 3 , --modified
						DOB = @dob,
						LastModify = @now,
						LastOper = @oper
						where EntNum = @entNum and 
						 DOBsId = @DOBsId
					 End
				End
				Else 				  	
				begin	
					--Insert new record
					set  @DOBsId  = 0
				end
			 end
			else
			-- Existing record modified or undeleted status
			  if @Unchanged = 0 
				begin
					update DOBs
					set 
					Status  = 3 , --modified
					DOB = @dob,
					LastModify = @now,
					LastOper = @oper
					where EntNum = @entNum and 
					DOBsId = @DOBsId
				end
			 else
			 begin
				-- Existing record unchanged
				update DOBs
				set Status = 1 --unchanged
				where EntNum = @entNum and 
				       DOBsId = @DOBsId
			 end	
		  end

		if @Existing = 0
		 begin
			--Find record by DOB
			select @Existing = count(DOBsID)
			from DOBs WITH (NOLOCK)
			where EntNum = @entNum and 
				  Status = 4 and
				DOB = @dob
              
        
			if @Existing = 0
				set @status = 2 --added
			else
		 		--DOB exists
				update DOBs
				set  Status = 1 -- unchanged					 
				where EntNum  = @entNum and
					  Status = 4 and 
					  DOB = @dob			   
	  end
	end

  if  @Existing = 0
   begin

		-- the following if block is added for
		-- user entered dobs
		if @DOBsId = 0
		 begin
			--if there are no dobs available in the DOBs table
			--then set the DOBsId to 10000000
			select @DOBsId = max(DOBsId) from DOBs 
			select @DOBsId = isnull(@DOBsId, 10000000)
			If @DOBsId < 10000000
				SET @DOBsId = 10000000

				select @DOBsId = @DOBsId + 1
		  end

			insert into DOBs (DOBsId, EntNum, DOB, Status, ListCreateDate,
                ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
			values (@DOBsId, @EntNum, @dob, @status, @listCreateDate,
  
              @ListModifDate, @oper, @now, @oper, @now)

    end

	-- Start standard stored procedure transaction footer
	select @stat = @@ERROR
	if @stat = 0
	  begin
		Exec OFS_SetFileImageFlag
		if @trnCnt = 0 commit tran OFS_InsDOBs
	  end
	else
		rollback tran OFS_InsDOBs

	return @stat
  -- End standard stored procedure transaction footer
GO

If EXISTS (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_InsURLs]') and 
                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                DROP PROCEDURE [dbo].[OFS_InsURLs]
GO
Create PROCEDURE dbo.OFS_InsURLs (
            @urlsId         IDType,
            @entnum         IDType,
            @url            NoteDataType,
            @description    NoteDataType,
            @status         int,
            @listCreateDate datetime,
            @listModifDate  datetime,
            @oper           SCode
) WITH ENCRYPTION AS
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_InsURLs
  else -- Already in a transaction
    save tran OFS_InsURLs
  -- End standard stored procedure transaction header

  declare @now datetime
  declare @stat int

  declare @Existing  int 
  declare @Unchanged int 
  declare @minSDNIndex int = 7000000
  declare @maxSDNIndex int = 9999999
  declare @newMinSDNIndex int = 20000000

  set @Existing  = 0
  set @Unchanged = 0

  select @now = GETDATE()

  --World OFS_InsNotes block. Persist ID, Set status
  if ((@entNum between @minSDNIndex and @maxSDNIndex) OR (@entNum >= @newMinSDNIndex)) 
   begin

		if ( @urlsId  != 0)
		 begin
			--Check by Primary Key
			select @Existing = count(URLsId)
			from URLs WITH (NOLOCK)

			where EntNum = @entNum and 
				  URLsID = @urlsId and
				  URL = @url and
				  IsNull(Description, '') = IsNull(@description, '')

			if @Existing = 0
			 begin
				--Insert new record
				set  @urlsId  = 0
			 end
			else
			 begin
				-- Existing record unchanged
				update URLs
				set Status = 1 --unchanged
				where EntNum = @entNum and 
				      URLsID = @urlsId  
			 end	
		  end

		if @Existing = 0
		 begin
			--Find record by URL
			select @Existing = count(URLsID)
			from URLs WITH (NOLOCK)

			where EntNum = @entNum and 
				  Status = 4 and
				  URL = @url
              
        
			if @Existing = 0
				set @status = 2 --added
			else
			  begin
				select @Unchanged = count(URLsID)
				from URLs WITH (NOLOCK)

				where EntNum = @entNum and 
					  Status = 4 and
					URL = @url and
					IsNull(Description, '') = IsNull(@description, '')

				if @Existing = 1
				  begin
						--Unique entry exists
						if @Unchanged  = 0
							update URLs
							set  Description = @description,
								 ListModifDate = @listModifDate,
								 LastModify =  @now, 
								 LastOper = @oper,
								 Status = 3 --Modified					 
							where EntNum  = @entNum and
								  Status = 4 and 
								  URL = @url
						else
							update URLs
							set  Status = 1 -- unchanged					 
							where EntNum  = @entNum and
								  Status = 4 and 
								  URL = @url
				   
                      
				 end
				else if @Existing > 1 and @Unchanged  > 1
				 begin
						--Existing entry is not unique but no changes
						update URLs
						set  Status = 1 -- unchanged					 
						where EntNum = @entNum and 
							  Status = 4 and
							  URL = @url and
							  IsNull(Description, '') = IsNull(@description, '')

				 end
				else
				  begin
						--Set new entry
						set @Existing = 0
						set @status = 2 --added
				  end
			end
	end
   end

  if  @Existing = 0
   begin

		-- the following if block is added for
		-- user entered urls
		if @urlsId = 0
		 begin
			--if there are no URL available in the urls table
			--then set the urlsid to 1
			select @urlsId = max(urlsId) from urls 
			select @urlsId = isnull(@urlsId, 10000000)

			if @urlsId < 10000000
				SET @urlsId = 10000000

		   select @urlsId = @urlsId + 1
		  end
		  else
		 begin
			-- check whether URL available in the urls table
			-- if exists then set urlsId using 10000000
			
			if exists (select 1 from urls with(nolock) where urlsId = @urlsId)
			begin
				select @urlsId = max(urlsId) from urls with (nolock)
			select @urlsId = isnull(@urlsId, 10000000)

			if @urlsId < 10000000
				SET @urlsId = 10000000

		   select @urlsId = @urlsId + 1
			end			
		 end

		  insert into URLs (URLsId, EntNum, URL, [Description],
			        Status, ListCreateDate, ListModifDate,
				    CreateOper, CreateDate, LastOper, LastModify)
		  values (@urlsId, @entnum, @url, @description,
			      @Status, @listCreateDate, @listModifDate,
                @oper, @now, @oper, @now)
	 end


  -- Start standard stored procedure transaction footer
  select @stat = @@ERROR
  if @stat = 0
  begin
    Exec OFS_SetFileImageFlag
    if @trnCnt = 0 commit tran OFS_InsURLs
  end
  else
    rollback tran OFS_InsURLs
  return @stat
  -- End standard stored procedure transaction footer
GO

If EXISTS (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[Ofs_addsdnalt]') and 
                                OBJECTPROPERTY(id, N'IsProcedure') = 1)


                DROP PROCEDURE [dbo].[Ofs_addsdnalt]
Go
CREATE PROCEDURE dbo.Ofs_addsdnalt @entNum INT,
                                   @altNum         INT,
                                   @altType        CHAR(8),
                                   @altName        SANCTIONNAME,
                                   @firstName      SANCTIONNAME,
                                   @middleNames    SANCTIONNAME,
                                   @lastName       SANCTIONNAME,
                                   @remarks        VARCHAR(200),
                                   @userRec        INT,
                                   @listId         VARCHAR(15),
                                   @status         INT,
                                   @ListCreateDate DATETIME,
                                   @listModifDate  DATETIME,
                                   @oper           CODE
WITH encryption
AS
    DECLARE @trnCnt INT


                DECLARE @minSDNIndex int = 7000000
                DECLARE @maxSDNIndex int = 9999999
				DECLARE @newMinSDNIndex int = 20000000
    SELECT @trnCnt = @@trancount -- Save the current trancount
    --Do not proceed with null in Alt. Names
    IF @altName IS NULL
       AND ((@entNum BETWEEN @minSDNIndex AND @maxSDNIndex) OR (@entNum >= @newMinSDNIndex))
      RETURN 0

    IF @trnCnt = 0
      -- Transaction has not begun
      BEGIN TRAN ofs_addsdnalt
    ELSE
      -- Already in a transaction
      SAVE TRAN ofs_addsdnalt

    -- Processing starts here:
    DECLARE @now DATETIME
    DECLARE @stat INT
    DECLARE @text VARCHAR(255)
    DECLARE @Existing INT
    DECLARE @Unchanged INT

    SET @Existing = 0
    SET @Unchanged = 0

    SELECT @now = Getdate()

    IF Len(Rtrim(Ltrim(@altType))) = 0
      SELECT @altType = 'a.k.a.'

    --World Check block. Persist ID, Set status
    IF @entNum >= @minSDNIndex 
      BEGIN
          IF ( @altNum != 0 )
            BEGIN
                --Check by Primary Key
                SELECT @Existing = Count(altnum)
                FROM   sdnalttable WITH (NOLOCK)

                WHERE  entnum = @entNum
                       AND altnum = @altnum
                       AND Isnull(altname, '') = Isnull(@altName, '')
                       AND Isnull(alttype, '') = Isnull(@altType, '')
                       AND Isnull(firstname, '') = Isnull(@firstName, '')
                       AND Isnull(middlename, '') = Isnull(@middleNames, '')
                       AND Isnull(lastname, '') = Isnull(@lastName, '')
                       AND Isnull(listid, '') = Isnull(@ListID, '')
                       AND Isnull(remarks, '') = Isnull(@remarks, '')

                IF @Existing = 0
                  BEGIN
							if @entNum>= 10000000  and @entNum<=19999999  --19999999 user defined max range
                         Begin     
                              
                         select @Existing =  count(AltNum)     
                         from SDNAltTable with (nolock)     
                         where EntNum = @entNum and      
                               AltNum = @altnum and     
                               (IsNull(AltName,    '') <> IsNull(@altName, '') OR     
                               IsNull(AltType,       '') <> IsNull(@alttype,       '') OR     
                               IsNull(FirstName,      '') <> IsNull(@firstname,      '') OR     
                               IsNull(MiddleName, '') <> IsNull(@middleNames, '') OR     
                               IsNull(LastName,    '') <> IsNull(@lastname,    '') OR     
                               IsNull(ListID,   '') <> IsNull(@ListID,   '') OR     
                               IsNull(Remarks,    '') <> IsNull(@remarks,    ''))             
      
                             if @Existing = 0     
                             Begin  

		                      --Insert New record

		                      SET @altNum = 0
     
                             End     
                             Else     
                             Begin     
                                 -- Existing record modified     
                                 update SDNAltTable     
                                 set      
                                 Status  = 3 , --modified     
                                 AltName = @altName,      
                                     AltType = @alttype,            
                                     FirstName = @firstname,     
                                     MiddleName = @middleNames,      
                                     LastName =   @lastname,        
                                     ListID =  @ListID,
									 LastModifDate = @now,
									 LastOper = @oper,     
                                 Remarks= @remarks     
                                 where EntNum = @entNum and      
                                       AltNum = @altnum     
                             End     
                                                              
                         end     
                         else     
                         begin     
                             --Insert New record     
                             set @altNum = 0     
                         end  

                  END
                ELSE
				-- Existing record modified or undeleted status
				   if @Unchanged = 0 
				   begin    
						update SDNAltTable     
						set      
						Status  = 3 , --modified     
						AltName = @altName,      
							AltType = @alttype,            
							FirstName = @firstname,     
							MiddleName = @middleNames,      
							LastName =   @lastname,        
							ListID =  @ListID,						
							LastModifDate = @now ,   
						Remarks= @remarks,
						LastOper = @oper     
						where EntNum = @entNum and      
							AltNum = @altnum 
				  end
				  else
                  BEGIN
                      -- Existing record unchanged
                      UPDATE sdnalttable

                      SET    status = 1 --unchanged
                      WHERE  entnum = @entNum
                             AND altnum = @altnum
                  END
            END

          IF @Existing = 0
            BEGIN
                --Find record by alt name
                SELECT @Existing = Count(altnum)
                FROM   sdnalttable WITH (NOLOCK)

                WHERE  entnum = @entNum
                       AND status = 4
                       AND Isnull(altname, '') = Isnull(@altName, '')

                IF @Existing = 0
                  SET @status = 2 --added
                ELSE
                  BEGIN
                      SELECT @Unchanged = Count(altnum)
                      FROM   sdnalttable WITH (NOLOCK)

                      WHERE  entnum = @entNum
                             AND status = 4
                             AND Isnull(altname, '') = Isnull(@altName, '')
                             AND Isnull(alttype, '') = Isnull(@altType, '')
                             AND Isnull(firstname, '') = Isnull(@firstName, '')
                             AND Isnull(middlename, '') =
                                 Isnull(@middleNames, '')
                             AND Isnull(lastname, '') = Isnull(@lastName, '')
                             AND Isnull(listid, '') = Isnull(@ListID, '')
                             AND Isnull(remarks, '') = Isnull(@remarks, '')

                      IF @Existing = 1
                        BEGIN
                            --Unique entry exists
                            IF @Unchanged = 0
                              UPDATE sdnalttable
                              SET    alttype = @altType,
                                     firstname = @firstName,
                                     middlename = @middleNames,
                                     lastname = @lastName,
                                     listid = @ListID,
                                     remarks = @remarks,
                                     listmodifdate = @listModifDate,
                                     lastmodifdate = @now,
                                     lastoper = @oper,
                                     status = 3 --Modified					 
                              WHERE  entnum = @entNum
                                     AND status = 4
                                     AND Isnull(altname, '') =
                                         Isnull(@altName, '')
                            ELSE
                              UPDATE sdnalttable
                              SET    status = 1 -- unchanged					 
                              WHERE  entnum = @entNum
                                     AND status = 4
                                     AND Isnull(altname, '') =
                                         Isnull(@altName, '')
                        END
                      ELSE IF @Existing > 1
                         AND @Unchanged > 1
                        BEGIN
                            --Existing entry is not unique but no changes
                            UPDATE sdnalttable
                            SET    status = 1 -- unchanged					 
                            WHERE  entnum = @entNum
                                   AND status = 4
                                   AND Isnull(altname, '') =
                                       Isnull(@altName, '')
                                   AND Isnull(alttype, '') =
                                       Isnull(@altType, '')
                                   AND Isnull(firstname, '') =
                                       Isnull(@firstName, '')
                                   AND Isnull(middlename, '') =
                                       Isnull(@middleNames, '')
                                   AND Isnull(lastname, '') =
                                       Isnull(@lastName, '')
                                   AND Isnull(listid, '') = Isnull(@ListID, '')
                                   AND Isnull(remarks, '') =
                                       Isnull(@remarks, '')
                        END
                      ELSE
                        BEGIN
                            --Set new entry
                            SET @Existing = 0
                            SET @status = 2 --added
                        END
                  END
            END
		  if (@entNum BETWEEN @minSDNIndex AND @maxSDNIndex ) or (@entNum >=  @newMinSDNIndex)   
     	  begin 

          IF EXISTS (SELECT 1
                     FROM   sdnalttable WITH (NOLOCK)

                     WHERE  entnum = @entNum
                            AND Isnull(altname, '') = Isnull(@altName, '')
                            AND Isnull(alttype, '') = Isnull(@altType, ''))
            BEGIN
                --Do not insert duplicated Alt Names with different case of letters
                IF Isnull(@status, 0) = 0 -- if unknown status
                  SET @status = 2 -- set new status
                UPDATE sdnalttable
                SET    alttype = @altType,
                       firstname = @firstName,
                       middlename = @middleNames,
                       lastname = @lastName,
                       listid = @ListID,
                       remarks = @remarks,
                       [status] = @status,
                       --listcreatedate = @listCreateDate,
                       listmodifdate = @listModifDate,
                       lastmodifdate = @now,
                       lastoper = @oper
                WHERE  entnum = @entNum
                       AND Isnull(altname, '') = Isnull(@altName, '')
                       AND Isnull(alttype, '') = Isnull(@altType, '')

                SET @Existing = 1
            END
		END
      END

    IF @Existing = 0
      BEGIN
          -- the following if block is added for
          -- user entered alt names
          IF @altNum = 0
            BEGIN
                --if there are no altnames available in the sdnalttable table
                --then set the altnum to 10000000
                SELECT @altNum = Max(altnum)
                FROM   sdnalttable 

                SELECT @altNum = Isnull(@altNum, 10000000)

                IF @altNum < 10000000
                  SET @altnum = 10000000

				SELECT @altNum = @altNum + 1
            END

          IF Isnull(@status, 0) = 0 -- if unknown status
            SET @status = 2 -- set new status
          INSERT INTO sdnalttable
                      (entnum,
                       altnum,
                       alttype,
                       altname,
                       firstname,
                       middlename,
                       lastname,
                       listid,
                       remarks,
                       status,
                       listcreatedate,
                       listmodifdate,
                       createdate,
                       lastmodifdate,
                       lastoper)
          VALUES      (@entNum,
                       @altNum,
                       @altType,
                       @altName,
                       @firstName,
                       @middleNames,
                       @lastName,
                       @ListID,
                       @remarks,
                       @status,
                       @listCreateDate,
                       @listModifDate,
                       @now,
                       @now,
                       @oper )
      END

    SELECT @stat = @@ERROR

    IF @stat = 0
      BEGIN
          EXEC Ofs_setfileimageflag

          IF @trnCnt = 0
            COMMIT TRANSACTION ofs_addsdnalt
      END
    ELSE
      ROLLBACK TRANSACTION ofs_addsdnalt

    RETURN @stat
GO

If EXISTS (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_AddSDNAddr]') and 
                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                DROP PROCEDURE [dbo].[OFS_AddSDNAddr]
Go
Create PROCEDURE dbo.OFS_AddSDNAddr @entNum int,
        @addrNum int,
        @address varchar(100),
        @city varchar (50),
        @country varchar(250),
        @remarks varchar(200),
        @userRec int,
        @listId varchar(15),
        @freeformat SDNAddress,
        @address2 varchar(50),
        @address3 varchar(50),
        @address4 varchar(50),
        @state varchar(50),
        @postalcode varchar(16),
        @status int,
        @listCreateDate datetime,
        @listModifDate datetime,
        @oper Code
With Encryption As
  declare @trnCnt int

  select @trnCnt = @@trancount  -- Save the current trancount
  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_AddSDNAddr
  else
    -- Already in a transaction
    save tran OFS_AddSDNAddr

  -- Processing starts here:

  declare @now datetime
  declare @stat int
  declare @text varchar(255)

 declare @Existing  int 
  declare @Unchanged int 

  set @Existing  = 0
  set @Unchanged = 0

  select @now = GETDATE()

  --World Check block. Persist ID, Set status
  if @entNum >= 7000000 
   begin

		if (  @addrNum  != 0)
		 begin
			--Check by Primary Key
			select @Existing =  count(AddrNum)
			from SDNAddrTable WITH (NOLOCK)
			where EntNum = @entNum and 
				  AddrNum =  @addrNum and
				  IsNull(Address,    '') = IsNull(@address,    '') and
				  IsNull(City,       '') = IsNull(@city,       '') and
				  IsNull(State,      '') = IsNull(@State,      '') and
				  IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
				  IsNull(Country,    '') = IsNull(@Country,    '') and
				  IsNull(Address2,   '') = IsNull(@address2,   '') and
				  IsNull(Address3,   '') = IsNull(@address3,   '') and
				  IsNull(Address4,   '') = IsNull(@address4,   '') and
				  IsNull(FreeFormat, '') = IsNull(@freeformat, '') and
				  IsNull(ListID,     '') = IsNull(@listid,     '') and 
				  IsNull(Remarks,    '') = IsNull(@remarks,    '')

			if @Existing = 0
			 begin
					if @entNum>= 10000000 and @entNum <= 19999999 --19999999 user defined max range
						Begin
						--Check by Primary Key
						select @Existing =  count(AddrNum)
						from SDNAddrTable with (nolock)
						where EntNum = @entNum and 
							  AddrNum =  @addrNum and
							  (IsNull(Address,    '') <> IsNull(@address,    '') OR
							  IsNull(City,       '') <> IsNull(@city,       '') OR
							  IsNull(State,      '') <> IsNull(@State,      '') OR
							  IsNull(PostalCode, '') <> IsNull(@PostalCode, '') OR
							  IsNull(Country,    '') <> IsNull(@Country,    '') OR
							  IsNull(Address2,   '') <> IsNull(@address2,   '') OR
							  IsNull(Address3,   '') <> IsNull(@address3,   '') OR
							  IsNull(Address4,   '') <> IsNull(@address4,   '') OR
							  IsNull(FreeFormat, '') <> IsNull(@freeformat, '') OR
							  IsNull(ListID,     '') <> IsNull(@listid,     '') OR 
							  IsNull(Remarks,    '') <> IsNull(@remarks,    ''))		

							if @Existing = 0
							Begin

								--Insert new record
								set  @addrNum = 0

							End
							Else
							Begin
								-- Existing record modified
								update SDNAddrTable
								set 
								Status  = 3 , --modified
								Address =  @address, 
								City     = @city,       
								State    = @State,     
								PostalCode = @PostalCode,
								Country    =   @Country,
								Address2   =  @address2,  
								Address3   = @address3,  
								Address4   = @address4,  
								FreeFormat  = @freeformat,
								ListID     = @listid,     
								Remarks =   @remarks,
								LastModifDate = @now,
								LastOper = @oper  
								where EntNum = @entNum and 
									  AddrNum =  @addrNum
							End								
						end
					else  -- else for @entNum>= 10000000
			 		begin
						--Insert new record
						set  @addrNum = 0
			 		end
				end	
			else
			 -- Existing record modified or deleted unchecked status
				if @Unchanged = 0
					Begin
						update SDNAddrTable
						set 
						Status  = 3 , --modified
						Address =  @address, 
						City     = @city,       
						State    = @State,     
						PostalCode = @PostalCode,
						Country    =   @Country,
						Address2   =  @address2,  
						Address3   = @address3,  
						Address4   = @address4,  
						FreeFormat  = @freeformat,
						ListID     = @listid,  
						LastModifDate = @now,   
						Remarks =   @remarks,
						LastOper = @oper  
						where EntNum = @entNum and 
								AddrNum =  @addrNum
					End
				else
				    Begin
				-- Existing record unchanged
				update SDNAddrTable
				set Status = 1 --unchanged
				where EntNum = @entNum and 
				      AddrNum =  @addrNum  
			 end	


		  end

		if @Existing = 0
		 begin
			--Find record by Address
			select @Existing = count(AddrNum)
			from SDNAddrTable WITH (NOLOCK)
			where EntNum = @entNum and 
				  Status = 4 and
				IsNull(Address, '') = IsNull(@address, '') and
				IsNull(City, '') = IsNull(@city, '') and
				IsNull(State, '') = IsNull(@State, '') and
				IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
				IsNull(Country, '') = IsNull(@Country, '') 

              
        
			if @Existing = 0
				set @status = 2 --added
			else
			 begin
				select @Unchanged = count(AddrNum)
				from SDNAddrTable WITH (NOLOCK)
				where EntNum = @entNum and 
				      Status = 4 and
					  IsNull(Address,    '') = IsNull(@address,    '') and
					  IsNull(City,       '') = IsNull(@city,       '') and
					  IsNull(State,      '') = IsNull(@State,      '') and
					  IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
					  IsNull(Country,    '') = IsNull(@Country,    '') and
					  IsNull(Address2,   '') = IsNull(@address2,   '') and
					  IsNull(Address3,   '') = IsNull(@address3,   '') and
					  IsNull(Address4,   '') = IsNull(@address4,   '') and
					  IsNull(FreeFormat, '') = IsNull(@freeformat, '') and
					  IsNull(ListID,     '') = IsNull(@listid,     '') and 
					  IsNull(Remarks,    '') = IsNull(@remarks,    '')
             

				if @Existing = 1
				  begin
					--Unique entry exists
					if @Unchanged  = 0
						update SDNAddrTable
						set  Address2   = @address2,
							 Address3   = @address3,
							 Address4   = @address4,
							 FreeFormat = @freeformat,
							 ListID = @ListID, 
							 Remarks = @remarks,
							 ListModifDate = @listModifDate,
							 LastModifDate =  @now, 
							 LastOper = @oper,
							 Status = 3 --Modified					 
						where EntNum  = @entNum and
							  Status = 4 and 
							  IsNull(Address,    '') = IsNull(@address,    '') and
							  IsNull(City,       '') = IsNull(@city,       '') and
							  IsNull(State,      '') = IsNull(@State,      '') and
							  IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
							  IsNull(Country,    '') = IsNull(@Country,    '') 

					else
						update SDNAddrTable
						set  Status = 1 -- unchanged					 
						where EntNum  = @entNum and
							  Status = 4 and 
							  IsNull(Address,    '') = IsNull(@address,    '') and
							IsNull(City,       '') = IsNull(@city,       '') and
							IsNull(State,      '') = IsNull(@State,      '') and
							IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
							IsNull(Country,    '') = IsNull(@Country,    '') 
                      
				 end
				else if @Existing > 1 and @Unchanged  > 1
				 begin
					--Existing entry is not unique but no changes
					update SDNAddrTable
					set  Status = 1 -- unchanged					 
					where EntNum = @entNum and 
						  Status = 4 and
						  IsNull(Address,    '') = IsNull(@address,    '') and
						  IsNull(City,       '') = IsNull(@city,       '') and
						  IsNull(State,      '') = IsNull(@State,      '') and
						  IsNull(PostalCode, '') = IsNull(@PostalCode, '') and
						  IsNull(Country,    '') = IsNull(@Country,    '') and
						  IsNull(Address2,   '') = IsNull(@address2,   '') and
						  IsNull(Address3,   '') = IsNull(@address3,   '') and
						  IsNull(Address4,   '') = IsNull(@address4,   '') and
						  IsNull(FreeFormat, '') = IsNull(@freeformat, '') and
						  IsNull(ListID,     '') = IsNull(@listid,     '') and 
						  IsNull(Remarks,    '') = IsNull(@remarks,    '')

				 end
				else
				 begin
					--Set new entry
					set @Existing = 0
					set @status = 2 --added
				 end
			end
		end
   end

  if  @Existing = 0
   begin

		-- the following if block is added for
		-- user entered addr
		if @addrNum = 0
		 begin
			--if there are no addr available in the SDNAddrTable
			--then set the addrnum to 10000000
			select @addrNum = max(addrNum) from SDNAddrTable 
			select @addrNum = isnull(@addrNum, 10000000)
			If @addrNum < 10000000
				SET @addrNum = 10000000

				select @addrNum = @addrNum + 1
		  end

		Insert into SDNAddrTable (EntNum, AddrNum, Address, City, Country, Remarks,
					ListId, FreeFormat, Address2, Address3, Address4,
					State, PostalCode, Status, ListCreateDate, ListModifDate,
					CreateDate, LastModifDate, LastOper)
		Values (@entNum, @addrNum, @address, @city, @country, @remarks,
				@listId, @freeFormat, @address2, @address3, @address4,
				@state, @postalCode, @status, @listCreateDate, @listModifDate,
				@now, @now, @oper )
	end

	Select @stat = @@ERROR

	If @stat = 0
	 Begin
		Exec OFS_SetFileImageFlag
		if @trnCnt = 0
			commit transaction OFS_AddSDNAddr
	 End
	Else
		Rollback Transaction OFS_AddSDNAddr

  return @stat
GO

If EXISTS (select * from dbo.sysobjects 
                                where id = object_id(N'[dbo].[OFS_InsWorldCheckRelationship]') and 
                                OBJECTPROPERTY(id, N'IsProcedure') = 1)
                DROP PROCEDURE [dbo].[OFS_InsWorldCheckRelationship]
GO
Create PROCEDURE dbo.OFS_InsWorldCheckRelationship (
            @RelationshipId     IDType,
            @entnum             IDType,
            @relatedid          IDType,
            @relationshipPtoR   varchar(50),
            @relationshipRtoP   varchar(50),
            @status             int,
            @listCreateDate     Datetime,
            @listModifDate      Datetime,
            @oper               SCode
) WITH ENCRYPTION AS
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_InsWorldCheckRelationship
  else -- Already in a transaction
    save tran OFS_InsWorldCheckRelationship
  -- End standard stored procedure transaction header

  declare @now datetime
  declare @stat int

  declare @Existing  int 
  declare @Unchanged int 
  declare @newMinSDNIndex int = 20000000

  set @Existing  = 0
  set @Unchanged = 0

  select @now = GETDATE()

  --World Check block. Persist ID, Set status
  if ((@entNum between 7000000 and 9999999) OR (@entNum >= @newMinSDNIndex))
   begin

		if ( @RelationshipId != 0)
		 begin
			--Check by Primary Key
			select @Existing = count(RelationshipId)
			from Relationship
			where EntNum = @entNum and 
				  RelationshipId = @RelationshipId and
				  RelatedId = @RelatedId and
				  RelationshipPToR  = @relationshipPtoR and
				  RelationshipRToP = @relationshipRtoP

			if @Existing = 0
			 begin
				--Insert new record
				set  @RelationshipId  = 0
			 end
			else
			 begin
				-- Existing record unchanged
				update Relationship
				set Status = 1 --unchanged
				where EntNum = @entNum and 
				      RelationshipId = @RelationshipId
			 end	
		  end

		if @Existing = 0
		 begin
			--Find record by Relationship ID
			select @Existing = count(RelationshipId)
			from Relationship
			where EntNum = @entNum and 
				  Status = 4 and
				  RelatedId = @RelatedId
              
        
			if @Existing = 0
				set @status = 2 --added
			else
			 begin
				select @Unchanged = count(RelationshipId)
				from Relationship
				where EntNum = @entNum and 
					  Status = 4 and
					  RelatedId = @RelatedId and
					  RelationshipPToR  = @relationshipPtoR and
					  RelationshipRToP = @relationshipRtoP

				if @Existing = 1
				 begin
					--Unique entry exists
					if @Unchanged  = 0
						update Relationship
						set  RelationshipPToR  = @relationshipPtoR,
							 RelationshipRToP  = @relationshipRtoP,
							 ListModifDate = @listModifDate,
							 LastModify =  @now, 
							 LastOper = @oper,
							 Status = 3 --Modified					 
						where EntNum  = @entNum and
							  Status = 4 and 
							  RelatedId = @RelatedId
					else
						update Relationship
						set  Status = 1 -- unchanged					 
						where EntNum  = @entNum and
							  Status = 4 and 
							  RelatedId = @RelatedId
				   
                      
				  end
				else if @Existing > 1 and @Unchanged  > 1
				  begin
					--Existing entry is not unique but no changes
					update Relationship
					set  Status = 1 -- unchanged					 
					where EntNum = @entNum and 
						  Status = 4 and
						  RelatedId = @RelatedId and 
						  RelationshipPToR  = @relationshipPtoR and
						  RelationshipRToP = @relationshipRtoP
				  end
				else
				  begin
					--Set new entry
					set @Existing = 0
					set @status = 2 --added
				  end
			end
		end
   end

  if  @Existing = 0
   begin
		if @RelationshipId = 0
		 begin
			--if there are no notes available in the notes table
			--then set the notesid to 1
			select @RelationshipId = max(RelationshipId) from relationship 
			select @RelationshipId = isnull(@RelationshipId, 10000000)
			if @RelationshipId < 10000000
				SET @RelationshipId = 10000000

			select @RelationshipId = @RelationshipId + 1
	     end

		insert into Relationship (RelationshipId, EntNum, RelatedID, RelationshipPToR,
                    RelationshipRToP, Status, ListCreateDate, ListModifDate,
                    CreateOper, CreateDate, LastOper, LastModify)
		values (@RelationshipId, @entnum, @relatedid, @relationshipPtoR,
			    @relationshipRtoP, @status, @listCreateDate,
				@listModifDate, @oper, @now, @oper, @now)
 end


  -- Start standard stored procedure transaction footer
  select @stat = @@ERROR
  if @stat = 0
  begin
    if @trnCnt = 0 commit tran OFS_InsWorldCheckRelationship
  end
  else
    rollback tran OFS_InsWorldCheckRelationship
  return @stat
  -- End standard stored procedure transaction footer
GO
IF EXISTS (select * from sysobjects 
	where id = object_id(N'[dbo].[OFS_GetEntNumByListIdAndProgram]'))
	Drop Function dbo.OFS_GetEntNumByListIdAndProgram
Go
Create Function OFS_GetEntNumByListIdAndProgram( @ListId VARCHAR(15),
    @Program Char(200))
RETURNS INT
WITH ENCRYPTION As
BEGIN
	DECLARE @EntNum INT
	Select @EntNum = EntNum From SDNTable WITH (NOLOCK)
		Where ListId = @ListId and Program = @Program
	Return @EntNum
END
GO
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'EntNumIdx_Notes')
    DROP INDEX Notes.EntNumIdx_Notes

CREATE INDEX EntNumIdx_Notes
ON Notes
(EntNum)
GO
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'EntNumIdx_URLs')
    DROP INDEX URLs.EntNumIdx_URLs

CREATE INDEX EntNumIdx_URLs
ON URLs
(EntNum)
GO

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'RelatedIdIdx_Relationship')
    DROP INDEX Relationship.RelatedIdIdx_Relationship

CREATE INDEX RelatedIdIdx_Relationship
ON Relationship
(RelatedId)
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_InsDowJonesRelationship]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_InsDowJonesRelationship]
Go

Create PROCEDURE OFS_InsDowJonesRelationship (
            @RelationshipId     IDType,
            @entnum             IDType,
            @relatedid          IDType,
            @relationshipPtoR   varchar(50),
            @relationshipRtoP   varchar(50),
            @status             int,
            @listCreateDate     Datetime,
            @listModifDate      Datetime,
            @oper               SCode
) WITH ENCRYPTION AS
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount    -- Save the current trancount
  if @trnCnt = 0 -- Transaction has not begun
    begin tran OFS_InsDowJonesRelationship
  else -- Already in a transaction
    save tran OFS_InsDowJonesRelationship
  -- End standard stored procedure transaction header

  declare @now datetime
  declare @stat int

  declare @Existing  int 
  declare @ChildExisting int
  declare @Unchanged int 
  declare @minSDNIndex int = 7000000
  declare @maxSDNIndex int = 9999999
  declare @newMinSDNIndex int = 20000000

  set @Existing  = 0
  set @Unchanged = 0

  select @now = GETDATE()

  --World Check block. Persist ID, Set status
  if (@entNum between @minSDNIndex and @maxSDNIndex) OR (@entnum >= @newMinSDNIndex)
   begin

		if ( @RelationshipId != 0)
		 begin
			--Check by Primary Key
			select @Existing = count(RelationshipId)
			from Relationship WITH(NOLOCK)
			where EntNum = @entNum and 
				  RelationshipId = @RelationshipId and
				  RelatedId = @RelatedId and
				  RelationshipPToR  = @relationshipPtoR and
				  RelationshipRToP = @relationshipRtoP

			if @Existing = 0
			 begin
				--Insert new record
				set  @RelationshipId  = 0
			 end
			else
			 begin
				-- Existing record unchanged
				update Relationship
				set Status = 1 --unchanged
				where EntNum = @entNum and 
				      RelationshipId = @RelationshipId
			 end	
		  end

		if @Existing = 0
		 begin
			--Find record by Relationship ID
			select @Existing = count(RelationshipId)
			from Relationship WITH(NOLOCK)
			where EntNum = @entNum and 
				  Status = 4 and
				  RelatedId = @RelatedId
              
        
			if @Existing = 0
				set @status = 2 --added
			else
			 begin
				select @Unchanged = count(RelationshipId)
				from Relationship WITH(NOLOCK)
				where EntNum = @entNum and 
					  RelatedId = @RelatedId and
					  RelationshipPToR  = @relationshipPtoR and
					  IsNull(RelationshipRToP, '') = IsNull(@relationshipRtoP, '')

				if @Existing = 1
				 begin
					--Unique entry exists
					if @Unchanged  = 0
						update Relationship
						set  RelationshipPToR  = @relationshipPtoR,
							 RelationshipRToP  = @relationshipRtoP,
							 ListModifDate = @listModifDate,
							 LastModify =  @now, 
							 LastOper = @oper,
							 Status = 3 --Modified					 
						where EntNum  = @entNum and
							  Status = 4 and 
							  RelatedId = @RelatedId
					else
						update Relationship
						set  Status = 1 -- unchanged					 
						where EntNum  = @entNum and
							  Status = @status and 
							  RelatedId = @RelatedId
				   
                      
				  end
				else if @Existing > 1 and @Unchanged  > 1
				  begin
					--Existing entry is not unique but no changes
					update Relationship
					set  Status = 1 -- unchanged					 
					where EntNum = @entNum and 
						  Status = @status and
						  RelatedId = @RelatedId and 
						  RelationshipPToR  = @relationshipPtoR and
						  RelationshipRToP = @relationshipRtoP
				  end
				else
				  begin
					--Set new entry
					set @Existing = 0
					set @status = 2 --added
				  end
			end
		end
   end

  if  @Existing = 0
   begin
		if @RelationshipId = 0
		 begin
			--if there are no notes available in the notes table
			--then set the notesid to 1
			select @RelationshipId = max(RelationshipId) from relationship WITH(NOLOCK)
			select @RelationshipId = isnull(@RelationshipId, 10000000)
			if @RelationshipId < 10000000
				SET @RelationshipId = 10000000

			select @RelationshipId = @RelationshipId + 1
	     end

		-- Count the number of reverse relationship descriptions in the database
	     select @ChildExisting = count(RelationshipId)
				from Relationship WITH(NOLOCK)
				Where EntNum = @relatedid
				AND RelatedID = @entnum
	     
	     -- if there is only one reverse relationship:
	     --   * get its description and add it to our reverse description variable
	     --   * update this other entry with our current Description as its reverse Description
	     if @ChildExisting = 1
	     Begin
			
			Select @relationshipRtoP = RelationshipPToR From Relationship WITH(NOLOCK) Where EntNum = @relatedid
			 AND RelatedID = @entnum
	     
			Update Relationship
			Set RelationshipRToP = @relationshipPtoR
			Where EntNum = @relatedid
			 AND RelatedID = @entnum
	     End
	     

		insert into Relationship (RelationshipId, EntNum, RelatedID, RelationshipPToR,
                    RelationshipRToP, Status, ListCreateDate, ListModifDate,
                    CreateOper, CreateDate, LastOper, LastModify)
		values (@RelationshipId, @entnum, @relatedid, @relationshipPtoR,
			    @relationshipRtoP, @status, @listCreateDate,
				@listModifDate, @oper, @now, @oper, @now)
 end


  -- Start standard stored procedure transaction footer
  select @stat = @@ERROR
  if @stat = 0
  begin
    if @trnCnt = 0 commit tran OFS_InsDowJonesRelationship
  end
  else
    rollback tran OFS_InsDowJonesRelationship
  return @stat
  -- End standard stored procedure transaction footer
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_RemoveAllWorldCheckEntities]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_RemoveAllWorldCheckEntities]
Go

Create PROCEDURE dbo.OFS_RemoveAllWorldCheckEntities
(@ImportBy CHAR(15),@removeCount Int OUT)
WITH ENCRYPTION As
BEGIN
  
  declare @stat int
  DECLARE @trnCnt        int

  SELECT @trnCnt = @@trancount  -- Save the current trancount

  IF @trnCnt = 0
    -- Transaction has not begun
    BEGIN TRAN OFS_RemoveAllWorldCheckEntities
  ELSE
    -- Already in a transaction
    SAVE TRAN OFS_RemoveAllWorldCheckEntities

  SELECT EntNum INTO #EntNums FROM SDNTable WHERE ListType='USER' and Remarks2=@ImportBy

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	DELETE Notes 
	FROM Notes INNER JOIN #EntNums Ent
		ON Notes.EntNum = Ent.EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	DELETE SDNAddrTable 
	FROM SDNAddrTable INNER JOIN #EntNums Ent
		ON SDNAddrTable.EntNum = Ent.EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	DELETE SDNAltTable 
	FROM SDNAltTable INNER JOIN #EntNums Ent
		ON SDNAltTable.EntNum = Ent.EntNum
  
  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	DELETE DOBs 
	FROM DOBs INNER JOIN #EntNums Ent
		ON DOBs.EntNum = Ent.EntNum
  
  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	DELETE URLs 
	FROM URLs INNER JOIN #EntNums Ent
		ON URLs.EntNum = Ent.EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	DELETE Relationship 
	FROM Relationship INNER JOIN #EntNums Ent
		ON Relationship.EntNum = Ent.EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	DELETE Relationship 
	FROM Relationship INNER JOIN #EntNums Ent
		ON Relationship.RelatedId = Ent.EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

	DELETE SDNTable 
	FROM SDNTable INNER JOIN #EntNums Ent
		ON SDNTable.EntNum = Ent.EntNum

	SELECT @stat = @@ERROR
	
  EndOfProc:
  SELECT @removeCount = count(entnum) FROM #EntNums
  IF @trnCnt = 0 BEGIN
	COMMIT TRAN OFS_RemoveAllWorldCheckEntities
  END ELSE IF @stat <> 0 BEGIN
    	ROLLBACK TRAN OFS_RemoveAllWorldCheckEntities
  END
  DROP TABLE #EntNums

	RETURN @stat
END
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_LoadDistAddSDN]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_LoadDistAddSDN]
Go
CREATE PROCEDURE dbo.OFS_LoadDistAddSDN @entNum int,
                            @name SanctionName,
                            @firstName SanctionName,
                            @middleName SanctionName,
                            @lastName SanctionName,
                            @type char (12),
                            @program varchar(200),
                            @title varchar(200),
                            @callSign char(8),
                            @vessType char(25),
                            @tonnage char(14),
                            @grt char(8),
                            @vessFlag varchar(40),
                            @vessOwner varchar(150) ,
                            @dob varchar(150) ,
                            @remarks text ,
                            @remarks1 varchar(255) ,
                            @remarks2 varchar(255) ,
                            @sdnType int,
                            @dataType int,
                            @userRec int ,
                            @country varchar(250),
                            @sex char,
                            @build varchar(15),
                            @complexion varchar(15),
                            @race varchar(15),
                            @eyes varchar(15),
                            @hair varchar(15),
                            @height int,
                            @weight int,
                            @listID varchar(15),
                            @listType varchar(15),
                            @primeAdded bit,
                            @standardOrder bit,
                            @effectiveDate datetime,
                            @expiryDate datetime,
                            @status int,
                            @listCreateDate datetime,
                            @listModifDate dateTime,
                            @oper Code

With Encryption As

  declare @trnCnt        int
  Declare @ignDrvNames   bit
  select @trnCnt = @@trancount  -- Save the current trancount
  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_LoadDistAddSDN
  else
    -- Already in a transaction
    save tran OFS_LoadDistAddSDN

  -- Processing starts here:
  declare @pos int
  declare @stat int
  declare @text varchar(255)
  declare @maxNum int

  If @type = ''
    select @type = 'other'

  If @userRec = 1 And UPPER(@type) = 'INDIVIDUAL'
     Select @ignDrvNames = 1
  Else
     Select @ignDrvNames = 0


  -- Assure valid sdnType
  If ( @sdnType <> 1 and @sdnType <> 2 and @sdnType <> 3 and @sdnType <> 4 )
   or @sdnType Is Null Begin
    Set @text = 'Unable to add name containing invalid Sdn Type "' +
     COALESCE(Convert(varchar, @sdnType), 'Null') + '".'
      Exec OFS_InsEvent @oper, 'Ann', 'SanName', @name, @text
      Set @stat = 250010  -- Invalid Parameter
      return @stat
  End

  -- First insert into temporary table. These records will be update in the main table
  -- later
  Insert into TempLoadSDNTable (EntNum, [Name], Title, Program,
            Type, CallSign, VessType, Tonnage,
            GRT, VessFlag, VessOwner, Dob, Remarks1,
            Remarks2, SDNType, DataType, userRec, deleted,
            duplicRec, FirstName, MiddleName, LastName,
            IgnoreDerived, ListType, ListID, PrimeAdded,
            Sex, Height, Weight, Build, eyes, hair, Complexion,
            Race, Remarks, Country, EffectiveDate, ExpiryDate,
            StandardOrder, status, ListCreateDate, ListModifDate,
            LastOper
            )
   Values (@entNum, @name, @title, UPPER(@program),
            @type, @callSign, @vessType, @tonnage,
            @grt, @vessFlag, @vessOwner, NULLIF(@dob,''), @remarks1,
            @remarks2, @sdnType, @dataType, @userRec, 0,
            0, NULLIF(@firstName,''), NULLIF(@middleName,''), NULLIF(@lastName,''),
            @ignDrvNames, @listType, @listId, @primeAdded,
            @sex, NullIf(@height,0), NullIf(@weight,0), @build,
                        @eyes, @hair, @complexion,
            @race, @remarks, @country, @effectiveDate, @expiryDate,
            @standardOrder, @status, @listCreateDate, @listModifDate,
            @oper
            )

  select @stat = @@ERROR
  If @stat = 0 Begin
      if @trnCnt = 0
    commit tran OFS_LoadDistAddSDN
  End Else
    rollback tran OFS_LoadDistAddSDN

  return @stat
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_AddSDN]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_AddSDN]
Go
CREATE PROCEDURE dbo.OFS_AddSDN( @entNum int,
                            @name SanctionName,
                            @firstName SanctionName,
                            @middleName SanctionName,
                            @lastName SanctionName,
                            @type char (12),
                            @program varchar(200),
                            @title varchar(200),
                            @callSign char(8),
                            @vessType char(25),
                            @tonnage char(14),
                            @grt char(8),
                            @vessFlag varchar(40),
                            @vessOwner varchar(150) ,
                            @dob varchar(150) ,
                            @remarks text ,
                            @remarks1 varchar(255) ,
                            @remarks2 varchar(255) ,
                            @sdnType int,
                            @dataType int,
                            @userRec int ,
							@country varchar(250),
							@sex char,
							@build varchar(15),
		   					@complexion varchar(15),
							@race varchar(15),
							@eyes varchar(15),
							@hair varchar(15),
							@height int,
							@weight int,
							@listID varchar(15),
							@listType varchar(15),
							@primeAdded bit,
							@standardOrder bit,
							@effectiveDate datetime,
							@expiryDate datetime,
							@status int,
							@listCreateDate datetime,
							@listModifDate dateTime,
							@oper Code,
							@log int,
							@ignDrvNames bit,			    
							@retEntnum int OUTPUT)
With Encryption As

  declare @trnCnt        int

  select @trnCnt = @@trancount	-- Save the current trancount
  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_AddSDN
  else
    -- Already in a transaction
    save tran OFS_AddSDN

  -- Processing starts here:
  declare @now datetime
  declare @pos int
  declare @stat int
  declare @text varchar(255)
  declare @maxNum int

  select @now = GETDATE()

  If @type = ''
    select @type = 'other'

  Select isnull(@ignDrvNames,1)

  -- set status of user records to Added = 2
  If @userRec = 1 and @status <> 2
		Select @status = 2

  -- Assure valid sdnType
  If ( @sdnType <> 1 and @sdnType <> 2 and @sdnType <> 3 and @sdnType <> 4 )
   or @sdnType Is Null Begin
		Set @text = 'Unable to add name containing invalid Sdn Type "' +
			COALESCE(Convert(varchar, @sdnType), 'Null') + '".'
		Exec OFS_InsEvent @oper, 'Ann', 'SanName', @name, @text
		Set @stat = 250010  -- Invalid Parameter
		rollback tran OFS_AddSDN
		return @stat
  End

   If @userRec <> 0 Begin
       Select @maxNum = Max(EntNum) From SDNTable
       Select @stat = @@ERROR

       If @stat <> 0 Begin
		rollback tran OFS_AddSDN
  		return @stat
       End

       If @maxNum Is NULL Set @maxNum = 0
       if @maxNum < 10000000
          Set @maxNum = 10000000
       else
          Set @maxNum = @maxNum +1
       Set @entNum = @maxNum
   End

   Insert into SDNTable (EntNum, [Name], FirstName,
                        MiddleName, LastName,Title,
                        Program, Type, CallSign,
                        VessType, Tonnage, GRT,
                        VessFlag, VessOwner, Dob,
                        Remarks, Remarks1, Remarks2,SDNType,
                        DataType, userRec, deleted,
                        duplicRec, IgnoreDerived,
						ListID, ListType, Country,
						Sex, Build, Height, Weight,
						Race, Complexion, eyes, hair,
						PrimeAdded, StandardOrder, EffectiveDate,
						ExpiryDate, Status, ListCreateDate,
						ListModifDate, CreateDate, LastModifDate,
						LastOper)
    Values
           (@entNum, @name, NULLIF(@firstName,''),
            NULLIF(@middleName,''), NULLIF(@lastName,''), @title,
            UPPER(@program),  @type,  @callSign,
            @vessType, @tonnage, @grt,
            @vessFlag, @vessOwner, NULLIF(@dob,''),
            @remarks, @remarks1, @remarks2, @sdnType,
            @dataType, @userRec, 0,
            0, @ignDrvNames,
			@listId, UPPER(@listType), @country,
			@sex, @build, NullIf(@height,0), NullIf(@weight,0),
			@race, @complexion, @eyes, @hair,
			@primeAdded, @standardOrder, @effectiveDate,
			@expiryDate, @status, @listCreateDate,
			@listModifDate, @now, @now, @oper )

  Select @stat = @@ERROR, @retEntNum = @entNum
  If @stat = 0 Begin
    If @log <> 0 Begin
  	  Exec OFS_InsEvent @oper, 'Cre', 'SanName', @name, Null
	  select @stat = @@ERROR
	  If @stat = 0 Begin
	      Exec OFS_SetFileImageFlag
  	      commit tran OFS_AddSDN
 	  End Else
	    rollback tran OFS_AddSDN
	End else
   	  commit tran OFS_AddSDN
  End else
    rollback tran OFS_AddSDN
  return @stat
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'OFS_ImportSDN')
 Begin
	DROP PROCEDURE [dbo].OFS_ImportSDN
 End
GO
CREATE PROCEDURE dbo.OFS_ImportSDN( @entNum int,      
            @name SanctionName,      
            @firstName SanctionName,      
            @middleName SanctionName,      
            @lastName SanctionName,      
            @type char (12),      
            @program char(200),      
            @title varchar(200),      
            @callSign char(8),      
            @vessType char(25),      
            @tonnage char(14),      
            @grt char(8),      
            @vessFlag varchar(40),      
            @vessOwner varchar(150) ,      
            @dob varchar(150) ,      
            @remarks text ,      
            @remarks1 varchar(255) ,      
            @remarks2 varchar(255) ,      
            @sdnType int,      
            @dataType int,      
            @userRec int ,      
            @country varchar(250),      
            @sex char,      
            @build varchar(15),      
            @complexion varchar(15),      
            @race varchar(15),      
            @eyes varchar(15),      
            @hair varchar(15),      
            @height int,      
            @weight int,      
            @listID varchar(15),      
            @listType varchar(15),      
            @primeAdded bit,      
            @standardOrder bit,      
            @effectiveDate datetime,      
            @expiryDate datetime,      
            @status int,      
            @listCreateDate datetime,      
            @listModifDate dateTime,      
			@oper Code,      
            @ignDrvNames BIT,       
  		    @retEntnum int OUTPUT,      
		    @retStatus int OUTPUT)      
WITH ENCRYPTION       
AS      
BEGIN      
  declare @event   varchar(3)      
  --Transaction       
  declare @trnCnt        int      
  -- Processing starts here:      
  declare @now datetime      
  declare @pos int      
  declare @stat int      
  declare @text varchar(255)      
  declare @maxNum int      
  
  select @now = GETDATE()      
  
  select @trnCnt = @@trancount  -- Save the current trancount      
  
  if @trnCnt = 0      
  -- Transaction has not begun      
  begin tran OFS_ImportSDN      
  else      
  -- Already in a transaction      
  save tran OFS_ImportSDN      
  
  Declare @list TABLE(Code VARCHAR(20))      
  insert into @list      
  SELECT code FROM ListType      
  WHERE code in      
  (      
  SELECT listtype from dbo.OFS_GetScopeForListType a       
   INNER JOIN psec.dbo.SEC_fnGetScopeForOperrights(@oper, 'MODOFACLST,DSPOFACLST') b --Rights hardcoded  
      ON a.branchdept = b.branchdept      
  )       
  AND enabled = 1 and isprimelist = 0      
      
	  if NOT Exists (Select Code from @list where code=@listType)      
	  Begin   
			Select @retStatus=201 --No Access Rights Value.  
			Select @retEntNum=0  
			SET @STAT = 1       
			GOTO ENDOFPROC      
	  END  
	  ELSE  
	  BEGIN   
		   If @type = ''      
		   select @type = 'other'      
		   Select ISNULL(@ignDrvNames, 1)      
		   If Exists(SELECT EntNum FROM SDNTable WHERE ListId=@listid AND userrec=@userRec)      
		   BEGIN      
				BEGIN TRY  
				  SELECT @EntNum=EntNum FROM SDNTable WHERE ListId=@listid AND userrec=@userRec      
				  UPDATE SDNTable SET      
				   [name]=@name,      
				   firstName=@firstName ,      
				   middleName=@middleName,      
				   lastName=@lastName ,      
				   type=@type ,      
				   program=UPPER(@program) ,      
				   title=@title ,      
				   callSign=@callSign ,      
				   vessType=@vessType ,      
				   tonnage=@tonnage ,      
				   grt=@grt ,      
				   vessFlag=@vessFlag ,      
				   vessOwner=@vessOwner ,      
				   dob=@dob ,      
				   remarks=@remarks ,      
				   remarks1=@remarks1 ,      
				   remarks2=@remarks2 ,      
				   sdnType=@sdnType ,      
				   dataType=@dataType ,      
				   userRec=@userRec ,      
				   IgnoreDerived = @ignDrvNames ,      
				   country=@country ,      
				   sex=@sex ,      
				   build=@build ,      
				   complexion=@complexion ,      
				   race=@race ,      
				   eyes=@eyes ,      
				   hair=@hair ,      
				   height=@height ,      
				   weight=@weight ,      
				   listID=@listID,      
				   listType=@listType ,      
				   primeAdded=@primeAdded ,      
				   standardOrder=@standardOrder ,      
				   expiryDate=@expiryDate ,      
				   status=3 , --Insert Status Value     
				   listModifDate=@listModifDate , 
				   LastModifDate=@now,   
				   lastoper=@oper      
				  WHERE EntNum = @EntNum      
				  delete from Notes where entnum = @EntNum      
					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

					update sdnaddrtable
					Set Status = 4 --deleted
					where entnum = @EntNum

					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

					update sdnalttable
					Set Status = 4 --deleted
					where entnum = @EntNum

					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

					delete from urls where entnum = @EntNum      

					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

					Update dobs
					Set Status = 4 --deleted
					where entnum = @EntNum

					SELECT @stat = @@ERROR 
					IF @stat <> 0  GOTO ENDOFPROC

				   delete from relationship where entnum = @EntNum      
				   SELECT @stat = @@ERROR 
					  IF @stat <> 0  GOTO ENDOFPROC
				   delete from relationship where relatedid = @EntNum      
         		   SELECT @stat = @@ERROR 
					  IF @stat <> 0  GOTO ENDOFPROC    
			

				  Select @retStatus=203 --Update Values.     
				  Select @event = 'MOD'      
				END TRY  
				BEGIN CATCH  
				   SELECT ERROR_NUMBER()   
				   Select @stat = @@ERROR      
				   Select @retStatus=204  
				   GOTO ENDOFPROC  
				END CATCH  
		   END      
		   ELSE      
		   BEGIN      
			 BEGIN TRY  
				  Select @maxNum = Max(EntNum) From SDNTable       
				  If @maxNum Is NULL Set @maxNum = 0      
				  if @maxNum < 10000000      
				   Set @maxNum = 10000000      
				  else      
				   Set @maxNum = @maxNum +1      
				  Set @entNum = @maxNum      
			  
				  Insert into SDNTable (EntNum, [Name], FirstName,      
				   MiddleName, LastName,Title,      
				   Program, Type, CallSign,      
				   VessType, Tonnage, GRT,      
				   VessFlag, VessOwner, Dob,      
				   Remarks, Remarks1, Remarks2,SDNType,      
				   DataType, userRec, deleted,      
				   duplicRec, IgnoreDerived,      
				   ListID, ListType, Country,      
				   Sex, Build, Height, Weight,      
				   Race, Complexion, eyes, hair,      
				   PrimeAdded, StandardOrder, EffectiveDate,      
				   ExpiryDate, Status, ListCreateDate,      
				   ListModifDate, CreateDate, LastModifDate,      
				   LastOper)      
				  Values      
				   (@entNum, @name, NULLIF(@firstName,''),      
				   NULLIF(@middleName,''), NULLIF(@lastName,''), @title,      
				   UPPER(@program),  @type,  @callSign,      
				   @vessType, @tonnage, @grt,      
				   @vessFlag, @vessOwner, NULLIF(@dob,''),      
				   @remarks, @remarks1, @remarks2, @sdnType,      
				   @dataType, @userRec, 0,      
				   0, @ignDrvNames,@listId, @listType, @country,      
				   @sex, @build, NullIf(@height,0), NullIf(@weight,0),      
				   @race, @complexion, @eyes, @hair,@primeAdded,  
				   @standardOrder, @effectiveDate,@expiryDate, 2,  
				   @listCreateDate,@listModifDate, @now, @now, @oper )      
				  Select @retStatus=202 --Insert Status Value     
				  Select @event = 'CRE'      
			 END TRY  
			 BEGIN CATCH  
				  SELECT ERROR_NUMBER()   
				  Select @stat = @@ERROR      
				  Select @retStatus=204  
				  GOTO ENDOFPROC  
			 END CATCH  
			END      
	  
		Select @retEntNum = @entNum      
	        
		If @stat = 0 Begin      
			 Exec OFS_InsEvent @oper, @event, 'SanName', @name, Null      
			 select @stat = @@ERROR      
			 If @stat = 0 Begin      
				 Exec OFS_SetFileImageFlag      
			 End      
		End      
    
  End  
  EndofProc:      
  IF @trnCnt = 0 BEGIN      
	   COMMIT TRAN OFS_ImportSDN
	   END ELSE IF @stat <> 0 BEGIN      
	   ROLLBACK TRAN OFS_ImportSDN      
  END     
         
RETURN @stat      
END 
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_GetSDNParty]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_GetSDNParty]
Go
CREATE PROCEDURE OFS_GetSDNParty

    (

        @OriginalSDNName  SanctionName,
        @Program      varchar(200),
        @Type         char(12),
        @Title        varchar(200)

    )
With Encryption As

    select * from SDNTable
    where [Name]=@OriginalSDNName and Program=@Program and
    (@Type Is Null or Len(@Type)=0 or Type=@Type) and
    (@Title Is Null or Len(@Title)=0 or Title=@Title)
    order by EntNum
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_UpdSanctionParty]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_UpdSanctionParty]
Go
CREATE PROCEDURE OFS_UpdSanctionParty (
    @EntNum int,
    @name SanctionName,
	@ListType ListTypeID,
    @IgnDervNames Bit,
    @type char(12),
    @program varchar(200),
    @sdnType int,
    @dataType int,
    @title varchar(200),
    @remarks text,
    @firstName SanctionName,
    @middleName SanctionName,
    @lastName SanctionName,
    @oper Code
)
With Encryption
As

    declare @trnCnt int
    declare @stat int
    declare @text varchar(255)

    -- Start standard stored procedure transaction header
     select @trnCnt = @@trancount   -- Save the current trancount
     If @trnCnt = 0
        -- Transaction has not begun
        begin tran OFS_UpdSanctionParty
    else
        -- Already in a transaction
        save tran OFS_UpdSanctionParty
    -- End standard stored procedure transaction header
    -- Processing starts here:
    If @IgnDervNames = 1
        Select @text = 'Do not Create Derived Names'
    Else
        Select @text = 'Create Derived Names'


    update sdntable set
    [Name] = NullIf(RTrim(@name), ''),
	ListType = @ListType,
    IgnoreDerived = @IgnDervNames,
    Program= NullIf(RTrim(@program), ''),
    Type= NullIf(RTrim(@type), ''),
    Title = NullIf(RTrim(@title), ''),
    SDNType=@sdnType,
    DataType=@dataType,
    Remarks =  @remarks,
    FirstName = @firstName,
    MiddleName = @middleName,
    LastName = @lastName,
    LastOper = @oper,
	LastModifDate = getdate(),
	Status = 3
    where
    EntNum = @EntNum


    Select @stat = @@error
    If @stat = 0
    Begin
        Exec OFS_InsEvent @oper, 'Mod', 'SanName', @Name, @text
            If @trnCnt = 0
            Commit tran OFS_UpdSanctionParty
        End
        Else
            Rollback tran OFS_UpdSanctionParty

Return @stat
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_DelWCEntitiesByProgramName]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_DelWCEntitiesByProgramName]
Go
CREATE PROCEDURE dbo.OFS_DelWCEntitiesByProgramName
(@Program CHAR(200))
WITH ENCRYPTION AS
BEGIN
  DECLARE @stat INT
  DECLARE @trnCnt        INT
  
  SELECT @trnCnt = @@trancount  -- Save the current trancount

  IF @trnCnt = 0
    -- Transaction has not begun
    BEGIN TRAN OFS_DelWCEntitiesByProgramName
  ELSE
    -- Already in a transaction
    SAVE TRAN OFS_DelWCEntitiesByProgramName

  SELECT EntNum INTO #EntNums FROM SDNTable WHERE ListType='USER' and Program = @Program

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

  DELETE Notes 
	FROM Notes INNER JOIN #EntNums Ent
		ON Notes.EntNum = Ent.EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

  DELETE SDNAddrTable 
	FROM SDNAddrTable INNER JOIN #EntNums Ent
		ON SDNAddrTable.EntNum = Ent.EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

  DELETE SDNAltTable 
	FROM SDNAltTable INNER JOIN #EntNums Ent
		ON SDNAltTable.EntNum = Ent.EntNum
  
  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

  DELETE DOBs 
	FROM DOBs INNER JOIN #EntNums Ent
		ON DOBs.EntNum = Ent.EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

  DELETE URLs 
	FROM URLs INNER JOIN #EntNums Ent
		ON URLs.EntNum = Ent.EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

  DELETE Relationship 
	FROM Relationship INNER JOIN #EntNums Ent
		ON Relationship.EntNum = Ent.EntNum

  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC
  
  DELETE Relationship 
	FROM Relationship INNER JOIN #EntNums Ent
		ON Relationship.RelatedId = Ent.EntNum
 
  SELECT @STAT = @@ERROR 
  IF @STAT <> 0  GOTO ENDOFPROC

  DELETE SDNTable 
	FROM SDNTable INNER JOIN #EntNums Ent
		ON SDNTable.EntNum = Ent.EntNum
		
  SELECT @STAT = @@ERROR

EndOfProc:  
  IF @trnCnt = 0 BEGIN
	COMMIT TRAN OFS_DelWCEntitiesByProgramName
  END ELSE IF @stat <> 0 BEGIN
	ROLLBACK TRAN OFS_DelWCEntitiesByProgramName
  END
  RETURN @stat

END
GO

if exists (select * from dbo.sysobjects where
    id = object_id(N'[dbo].[OFS_GetMatchParty]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[OFS_GetMatchParty]
GO
CREATE PROCEDURE OFS_GetMatchParty

    (
        @SeqNumb      int,
        @OriginalSDNName  SanctionName,
        @MatchCountry varchar(200),
        @MatchText    varchar(1024),
        @Archive      int

    )
With Encryption As


    if @Archive=0
     begin
        /* Current Matches */
        select Distinct * from MatchTable
        where SeqNumb=@SeqNumb and
              OriginalSDNName=@OriginalSDNName and
              (MatchCountry = @MatchCountry or
               MatchCountry = 'KEY WORD' or
              @MatchCountry is Null or Len(@MatchCountry)=0)  and
              CharIndex(@MatchText, MatchText)>0
        order by OriginalSDNName, MatchCountry



     end
    else
     begin
        /* Archived Matches */
        select Distinct * from MatchHistTable
        where SeqNumb=@SeqNumb and
              OriginalSDNName=@OriginalSDNName and
               (MatchCountry = @MatchCountry or
            MatchCountry = 'KEY WORD' or
            @MatchCountry is Null or Len(@MatchCountry)=0)  and
              CharIndex(@MatchText, MatchText)>0
        order by OriginalSDNName, MatchCountry


     end
GO

if exists (select * from dbo.sysobjects where
    id = object_id(N'[dbo].[OFS_RecordOFACCase]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[OFS_RecordOFACCase]
GO
Create PROCEDURE [dbo].[OFS_RecordOFACCase] @tranXML text, @msgXML text, @matchXMl text
With encryption As
  declare @trnCnt int
  select @trnCnt = @@trancount  -- Save the current trancount
  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_RecordOFACCase
  else
    -- Already in a transaction
    save tran OFS_RecordOFACCase

  -- Processing starts here:

  DECLARE @trandoc int
  DECLARE @msgdoc int
  DECLARE @matchdoc int
  DECLARE @stat int
  DECLARE @seqnumb int

  EXEC sp_xml_preparedocument @trandoc OUTPUT, @tranxml
  EXEC sp_xml_preparedocument @msgdoc OUTPUT, @msgxml
  EXEC sp_xml_preparedocument @matchdoc OUTPUT, @matchxml

  insert into FilterTranTable (Source, Ref, Branch, Dept, ClientId, Result, MatchCount,
			MsgType, ReqTime, KeyField1, KeyField2,CIFFieldsDelimiter, ConfirmState, 
			LengthKF1,LengthKF2,FileImage, CIFRule,SystemRef,
			TransactionType, Amount, IsIncremental, UserMessageReference )
  SELECT *
	FROM OPENXML (@trandoc, '/ROOT/TRAN',1)
	WITH 	(Source varchar (50) 'Source',
             Ref varchar (255) 'Ref',
    		 Branch varchar(11) 'Branch' ,
             Dept varchar(11)  'Dept',
             ClientId INT 'ClientId',
             Result int 'Result',
             MatchCount int 'MatchCount',
             MsgType int 'MsgType',
             ReqTime datetime 'ReqTime',
             KeyField1 int 'KeyField1',
             KeyField2 int 'KeyField2',
             CIFFieldsDelimiter varchar(10) 'CIFFieldsDelimiter',
			 ConfirmState int 'ConfirmState',
             LengthKF1 int 'LengthKF1',
             LengthKF2 int 'LengthKF2',
             FileImage varchar(11) 'FileImage',
             CIFRule varchar(12) 'CIFRule',
             SystemRef varchar(11) 'SystemRef',
             TransactionType int 'TransactionType',
             Amount Money 'Amount',
             IsIncremental bit 'IsIncremental',
             UserMessageReference varchar(100) 'UserMessageReference')
  select @stat = @@ERROR, @seqnumb = @@IDENTITY
  if @stat <> 0
	rollback transaction OFS_RecordOFACCase
  else
	Begin
		insert into MsgTable (seqnumb, MsgType, MsgLen, Msg)
		SELECT @seqnumb, * 
			FROM OPENXML (@msgdoc, '/ROOT/MSG',1)
		WITH (MsgType int 'MsgType',
		      MsgLen int 'MsgLen',
		      MsgTxt text 'MsgTxt')
		select @stat = @@error
		if @stat <> 0
			rollback transaction OFS_RecordOFACCase
		else
			Begin
				Insert into MatchTable (seqnumb, MatchName, OriginalSDNName, 
							MatchText, MatchCountry, MatchType, Score,EntNum)
				SELECT @seqnumb seqnumb, *
				FROM OPENXML (@matchdoc, '/ROOT/MATCH',1)
				WITH ( Matchname SanctionName 'MatchName', 
			               OriginalSDNName SanctionName 'OriginalSDNName', 
				       MatchText varchar(1024) 'MatchText', 
				       MatchCountry varchar(200) 'MatchCountry', 
				       MatchType varchar(20) 'MatchType',
				       Score int 'Score',
					   entNum int 'EntNum')
				select @stat = @@error
				if @stat <> 0
					rollback transaction OFS_RecordOFACCase
				else	
					commit transaction OFS_RecordOFACCase
			End
	End

  EXEC SP_XML_REMOVEDOCUMENT @trandoc
  EXEC SP_XML_REMOVEDOCUMENT @msgdoc
  EXEC SP_XML_REMOVEDOCUMENT @matchdoc

  return @stat
GO


if exists (select * from dbo.sysobjects where
    id = object_id(N'[dbo].[OFS_LoadDistAddSDNAlt]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[OFS_LoadDistAddSDNAlt]
GO
CREATE PROCEDURE dbo.OFS_LoadDistAddSDNAlt @entNum int, @altNum int,
            @altType char(8),
			@Category varchar(50),
            @altName SanctionName, @firstName SanctionName,
            @middleNames SanctionName, @lastName SanctionName,
            @remarks varchar(200),  @userRec int, @listId varchar(15),
            @status int, @listCreateDate datetime,
            @listModifdate datetime, @oper Code
With Encryption As
  declare @trnCnt int
  select @trnCnt = @@trancount  -- Save the current trancount
  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_LoadDistAddSDNAlt
  else
    -- Already in a transaction
    save tran OFS_LoadDistAddSDNAlt

  -- Processing starts here:

  declare @stat int

  Insert into TempLoadAltTable (EntNum, AltNum, AltType, Category, AltName, FirstName,
          MiddleName, LastName,Remarks, Deleted, status, ListCreateDate,
        ListModifDate, LastOper)
  Values (@entNum, @altNum, @altType, @Category, @altName, @firstName,
        @middleNames, @lastName, @remarks, 0, @status, @listCreateDate,
        @listModifDate, @oper )

  Select @stat = @@ERROR
  If @stat = 0 Begin
      if @trnCnt = 0
        commit transaction OFS_LoadDistAddSDNAlt
  End Else
    Rollback Transaction OFS_LoadDistAddSDNAlt

  return @stat

GO

if exists (select * from dbo.sysobjects where
    id = object_id(N'[dbo].[OFS_CompleteLoadDistribution]')
    and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[OFS_CompleteLoadDistribution]
GO
CREATE PROCEDURE dbo.OFS_CompleteLoadDistribution (
                            @isdiff bit
) With Encryption As

    declare @oper Code
    declare @trnCnt int

    select @oper = 'Prime'
    select @trnCnt = @@trancount  -- Save the current trancount

    if @trnCnt = 0
        -- Transaction has not begun
        begin tran OFS_CompleteLoadDistribution
    else
        -- Already in a transaction
        save tran OFS_CompleteLoadDistribution

    -- Processing starts here:
    declare @stat int

--******** Update Main tables from temp tables ********************************
/* TempLoad SDN tables contains the distribution records
    If ListModifDate is equal then just update the Status with the
    distribution record status else update the whole record except the
    deleted column

    If record is not in main table then insert the record

    Set the Status of all records in main table but not in temp table
    to deleted if not loading a differential file, else set the Status
    to unchanged
*/
--*****************************************************************************

    declare @newstatus int
    If @isdiff = 0
        set @newstatus = 4  --deleted
    Else
        set @newstatus = 1  --unchanged

    Create table #TempUserEntNums (
                EntNum int
    )

    Create table #TempUserAltNums (
                AltNum int
    )

    insert into #TempUserEntNums(EntNum)
    Select  distinct Entnum from SDNTable where UserRec = 1

    insert into #TempUserAltNums(AltNum)
    Select distinct AltNum from SDNAltTable, SDNTable where 
    SDNAltTable.EntNum = SDNTable.Entnum
    and SDNTable.UserRec = 1

   -- For all lists that have been disabled, set the record statuses to deleted
    
                Update SDNTable Set Status = 4, LastModifDate = getDate()
        FROM SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and s.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update SDNAltTable Set Status = 4, LastModifDate = getDate()
        FROM SDNAltTable sa (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = sa.EntNum and sa.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update SDNAddrTable Set Status = 4, LastModifDate = getDate()
        FROM SDNAddrTable sa (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = sa.EntNum and sa.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update DOBs Set Status = 4, LastModify = getDate()
        FROM DOBs d (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = d.EntNum and d.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update Notes Set Status = 4, LastModify = getDate()
        FROM Notes n (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = n.EntNum and n.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update URLs Set Status = 4, LastModify = getDate()
        FROM URLs u (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = u.EntNum and u.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update Relationship Set Status = 4, LastModify = getDate()
        FROM Relationship r (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = r.EntNum and r.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update Keywords Set Status = 4, LastModify = getDate()
        FROM Keywords k (nolock), SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            s.EntNum = k.EntNum and k.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

    Update KeywordsAlt Set Status = 4, LastModify = getDate()
        FROM KeywordsAlt k (nolock), SDNAltTable sa (nolock),
            SDNTable s (nolock), OptionTbl o (nolock)
        Where s.ListType = o.Code and o.Enabled = 0 and
            sa.AltNum = k.AltNum and sa.EntNum = s.EntNum and k.Status <> 4

    Select @stat = @@error
    If (@stat <> 0)
        Goto AbortCompleteLoadDistribution

   -- Update entire record if distribution listmodifdate != last listmodifdate
   -- or (if status not equal to unchanged and listmodifdates are equal)

If Exists (Select EntNum from TempLoadSDNTable) Begin
   Update SDNtable Set
        [Name] = t.[Name],
        FirstName = NULLIF(t.FirstName,''),
        MiddleName = NULLIF(t.MiddleName,''),
        LastName = NullIf(t.LastName,''),
        Title = t.Title,
        Program = UPPER(t.Program),
        Type = t.Type,
        CallSign = t.CallSign,
        VessType = t.VessType,
        Tonnage = t.Tonnage,
        GRT = t.GRT,
        VessFlag = t.VessFlag,
        VessOwner = t.Vessowner,
        Dob = NullIF(t.DOB,''),
        Remarks = t.Remarks,
        Remarks1 = t.Remarks1,
        Remarks2 = t.Remarks2,
        SDNType = t.SDNType,
        DataType = t.DataType,
        userRec = t.userRec,
        duplicRec = t.duplicRec,
        IgnoreDerived = t.IgnoreDerived,
        ListID = t.ListId,
        ListType = t.ListType,
        Country = t.Country,
        Sex = t.Sex,
        Build = t.Build,
        Height = NullIf(t.Height,0),
        Weight = NullIf(t.Weight,0),
        Race = t.Race,
        Complexion = t.Complexion,
        eyes = t.eyes,
        hair = t.hair,
        PrimeAdded = t.PrimeAdded,
        StandardOrder = t.StandardOrder,
        EffectiveDate = t.EffectiveDate,
        ExpiryDate = t.ExpiryDate,
        Status = t.Status,
        ListCreateDate = t.ListCreatedate,
        ListModifDate = t.ListModifDate,
        LastModifDate = GetDate(),
        LastOper = t.LastOper
    From SDNTable s, TempLoadSDNTable t
        Where s.EntNum = t.EntNum And s.UserRec = 0 And
              ( (IsNull(s.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End
   -- Update status to distribution records status
   -- if distribution listmodifdate = last listmodifdate and status = unchanged
If Exists (Select EntNum from TempLoadSDNTable) Begin
   Update SDNTable Set Status = t.Status
        From SDNTable s, TempLoadSDNTable t
            Where s.EntNum = t.EntNum  and s.UserRec = 0 And
               IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0) And
               t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

-- Update record status of records not in temp table
  If (@newstatus=1)
                Begin
                     Update SDNTable Set Status = @newstatus 
                     FROM SDNTable s (nolock) left outer join TempLoadSDNTable t (nolock)
                     on s.EntNum = t.EntNum
                     Where t.EntNum is NULL and s.UserRec = 0 and s.Status in (2,3)
                End
  Else
                Begin
                     Update SDNTable Set Status = @newstatus, LastModifDate = getDate()
                     FROM SDNTable s (nolock) left outer join TempLoadSDNTable t (nolock)
                     on s.EntNum = t.EntNum
                     Where t.EntNum is NULL and s.UserRec = 0 and s.Status <> 4
                END
   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

-- Insert new records
If Exists (Select EntNum from TempLoadSDNTable) Begin
   Insert into SDNTable (EntNum, [Name], FirstName,
                        MiddleName, LastName,Title,
                        Program, Type, CallSign,
                        VessType, Tonnage, GRT,
                        VessFlag, VessOwner, Dob,
                        Remarks, Remarks1, Remarks2,SDNType,
                        DataType, userRec, deleted,
                        duplicRec, IgnoreDerived,
                        ListID, ListType, Country,
                        Sex, Build, Height, Weight,
                        Race, Complexion, eyes, hair,
                        PrimeAdded, StandardOrder, EffectiveDate,
                        ExpiryDate, Status, ListCreateDate,
                        ListModifDate, CreateDate, LastModifDate,
                        LastOper)
    Select t.EntNum, t.[Name], t.FirstName,
                        t.MiddleName, t.LastName, t.Title,
                        t.Program, t.Type, t.CallSign,
                        t.VessType, t.Tonnage, t.GRT,
                        t.VessFlag, t.VessOwner, t.Dob,
                        t.Remarks, t.Remarks1, t.Remarks2, t.SDNType,
                        t.DataType, t.userRec, t.deleted,
                        t.duplicRec, t.IgnoreDerived,
                        t.ListID, t.ListType, t.Country,
                        t.Sex, t.Build, t.Height, t.Weight,
                        t.Race, t.Complexion, t.eyes, t.hair,
                        t.PrimeAdded, t.StandardOrder, t.EffectiveDate,
                        t.ExpiryDate, t.Status, t.ListCreateDate,
                        t.ListModifDate, GetDate(), GetDate(),
                        t.LastOper
    From TempLoadSDNTable t (nolock) left Outer Join SDNTable s (nolock)
        on t.EntNum = s.EntNum
    where s.Entnum is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select AltNum from TempLoadAltTable) Begin
  Update SDNAlttable Set
        EntNum  = t.EntNum,
        AltType = t.AltType,
		Category = t.Category,
        AltName = t.AltName,
        FirstName = t.FirstName,
        MiddleName = t.MiddleName,
        LastName = t.LastName,
        ListId = t.ListId,
        Remarks = t.Remarks,
        status = t.status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModifDate = GetDate(),
        LastOper = t.LastOper
     From SDNAltTable s, TempLoadAltTable t
        Where s.AltNum = t.AltNum And
              ( (IsNull(s.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End
   -- Update status to distribution records status
   -- if distribution listmodifdate = last listmodifdate and status = unchanged
If Exists (Select AltNum from TempLoadAltTable) Begin
   Update SDNAltTable Set Status = t.Status, LastOper = t.LastOper
    From SDNAltTable s, TempLoadAltTable t
            Where s.AltNum = t.AltNum And
               IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End


  -- Update record status of records not in temp table
   If (@newstatus = 1)
                Begin
                                Update SDNAltTable Set Status = @newstatus,
                                                LastOper = @oper
                                From SDNAltTable s (nolock) 
                left outer join  TempLoadAltTable t (nolock) on s.AltNum = t.AltNum
                Where t.AltNum is NULL and s.Status in (2,3)
                End
                Else
                Begin
                                Update SDNAltTable Set Status = @newstatus, LastModifDate = getDate(),
                LastOper = @oper
                From SDNAltTable s (nolock) 
                left outer join TempLoadAltTable t (nolock) on s.AltNum = t.AltNum
                left outer join #TempUserAltNums ta on s.AltNum = ta.AltNum
                                Where t.AltNum is NULL and ta.AltNum is NULL and s.Status <> 4
                End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution



   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select AltNum from TempLoadAltTable) Begin
   Insert into SDNAltTable (EntNum, AltNum, AltType, Category, AltName, FirstName,
      MiddleName, LastName,Remarks, Status, ListCreateDate, ListModifDate,
      CreateDate, LastModifDate, LastOper)
   Select t.EntNum, t.AltNum, t.AltType, t.Category, t.AltName, t.FirstName,
        t.MiddleName, t.LastName, t.Remarks, t.Status, t.ListCreateDate, t.ListModifDate,
        GetDate(), GetDate(), t.LastOper
   From TempLoadAltTable t (nolock) left Outer Join SDNAltTable s (nolock)
            on t.AltNum = s.AltNum
        where s.AltNum is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select AddrNum from TempLoadAddrTable)  Begin
    -- Update the address records
   Update SDNAddrTable Set
        EntNum = t.EntNum,
        Address = t.Address,
        City = t.City,
        Country = t.Country,
        Remarks = t.Remarks,
        ListId = t.ListId,
        FreeFormat = t.FreeFormat,
        Address2 = t.Address2,
        Address3 = t.Address3,
        Address4 = t.Address4,
        State = t.State,
        PostalCode = t.PostalCode,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModifDate = GetDate(),
        LastOper = t.LastOper
    From SDNAddrTable s, TempLoadAddrTable t
        Where s.AddrNum = t.AddrNum And
              ( (IsNull(s.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select AddrNum from TempLoadAddrTable) Begin
   Update SDNAddrTable Set Status = t.Status, LastOper = t.LastOper
    From SDNAddrTable s, TempLoadAddrTable t
            Where s.AddrNum = t.AddrNum And
               IsNull(s.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
   If (@newstatus = 1)
                Begin
                                Update SDNAddrTable Set Status = @newstatus, 
                                                LastOper = @oper
                                From SDNAddrTable s (nolock) 
                left outer join  TempLoadAddrTable t (nolock) on s.AddrNum = t.AddrNum
                Where t.AddrNum is NULL and s.Status in (2,3)
                End
                Else
                Begin
                                Update SDNAddrTable Set Status = @newstatus, LastModifDate = getDate(),
                                                LastOper = @oper
                                From SDNAddrTable s (nolock) left outer join  TempLoadAddrTable t (nolock) on s.AddrNum = t.AddrNum
                Left outer join #TempUserEntNums ta on s.EntNum = ta.EntNum
                                Where t.AddrNum is NULL and ta.EntNum is NULL and s.Status <> 4
                End

  Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select AddrNum from TempLoadAddrTable) Begin
     Insert into SDNAddrTable (EntNum, AddrNum, Address, City, Country, Remarks,
        ListId, FreeFormat, Address2, Address3, Address4,
        State, PostalCode, Status, ListCreateDate, ListModifDate,
        CreateDate, LastModifDate, LastOper)
    Select t.EntNum, t.AddrNum, t.Address, t.City, t.Country, t.Remarks,
        t.ListId, t.FreeFormat, t.Address2, t.Address3, t.Address4,
        t.State, t.PostalCode, t.Status, t.ListCreateDate, t.ListModifDate,
        GetDate(), GetDate(), t.LastOper
    From TempLoadAddrTable t (nolock) left Outer Join SDNAddrTable s (nolock)
        on t.AddrNum = s.AddrNum
    where s.AddrNum is Null


   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsId from TempLoadKeywords)  Begin
    -- Update the address records
   Update Keywords Set
        EntNum = t.EntNum,
        Word = t.Word,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From Keywords k, TempLoadKeywords t
        Where k.keywordsId = t.keywordsId And
              ( (IsNull(k.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsId from TempLoadKeywords) Begin
   Update Keywords Set Status = t.Status, LastOper = t.LastOper
    From Keywords k, TempLoadKeywords t
            Where k.keywordsId = t.keywordsId And
               IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
                If (@newstatus=1)
                Begin
                                Update Keywords Set Status = @newstatus, 
                                                LastOper = @oper
                                From Keywords k (nolock) left outer join TempLoadKeywords t (nolock)
        on k.keywordsId = t.keywordsId
        Where t.keywordsId is NULL and k.Status in (2,3)
                End
                Else
                Begin
                                Update Keywords Set Status = @newstatus, LastModify = getDate(),
                                                LastOper = @oper
                                From Keywords k (nolock) left outer join TempLoadKeywords t (nolock) on k.keywordsId = t.keywordsId
                left outer join #TempUserEntNums ta on k.EntNum = ta.EntNum
                                Where t.keywordsId is NULL and ta.EntNum is NULL and k.Status <> 4
                End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select keywordsId from TempLoadKeywords) Begin
     Insert into keywords (KeywordsId, Entnum, Word, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
     Select t.KeywordsId, t.Entnum, t.Word, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadKeywords t (nolock) left Outer Join keywords k (nolock)
        on t.keywordsId = k.keywordsId
    where k.keywordsId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsAltId from TempLoadKeywordsAlt)  Begin
    -- Update the address records
   Update KeywordsAlt Set
        AltNum = t.AltNum,
        Word = t.Word,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From KeywordsAlt k, TempLoadKeywordsAlt t
        Where k.keywordsAltId = t.keywordsAltId And
              ( (IsNull(k.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select keywordsAltId from TempLoadKeywordsAlt) Begin
   Update KeywordsAlt Set Status = t.Status, LastOper = t.LastOper
    From KeywordsAlt k, TempLoadKeywordsAlt t
            Where k.keywordsAltId = t.keywordsAltId And
               IsNull(k.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
                If (@newstatus=1)
                Begin
                                Update KeywordsAlt Set Status = @newstatus, 
                                                LastOper = @oper
                                From KeywordsAlt k (nolock) left outer join TempLoadKeywordsAlt t (nolock)
        on k.keywordsAltId = t.keywordsAltId
        Where t.keywordsAltId is NULL and k.Status in (2,3)
                End
                Else
                Begin
                                Update KeywordsAlt Set Status = @newstatus, LastModify = getDate(),
                                                LastOper = @oper
                                From KeywordsAlt k (nolock) 
                left outer join TempLoadKeywordsAlt t (nolock) on k.keywordsAltId = t.keywordsAltId
                left outer join #TempUserAltNums ta on k.AltNum = ta.AltNum
                                Where t.keywordsAltId is NULL and ta.AltNum is NULL and k.Status <> 4
                End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select keywordsAltId from TempLoadKeywordsAlt) Begin
     Insert into keywordsAlt (KeywordsAltId, Altnum, Word,
        Status, ListCreateDate, ListModifdate,
        CreateOper, CreateDate, LastOper,
            LastModify)
     Select t.KeywordsAltId, t.Altnum, t.Word,
        t.Status, t.ListCreateDate, t.ListModifdate,
        t.CreateOper, GetDate(), t.LastOper,
            GetDate()
    From TempLoadKeywordsAlt t (nolock) left Outer Join KeywordsAlt k (nolock)
        on t.KeywordsAltId = k.KeywordsAltId
    where k.KeywordsAltId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select DOBsId from TempLoadDOBs)  Begin
    -- Update the address records
   Update DOBs Set
        EntNum = t.EntNum,
        DOB = t.DOB,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From DOBs d, TempLoadDOBs t
        Where d.DOBsId = t.DOBsId And
              ( (IsNull(d.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(d.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select DOBsId from TempLoadDOBs) Begin
   Update DOBs Set Status = t.Status, LastOper = t.LastOper
    From DOBs d, TempLoadDOBs t
            Where d.DOBsId = t.DOBsId And
               IsNull(d.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
                If (@newstatus=1)
                Begin
                                Update DOBs Set Status = @newstatus,
                                                LastOper = @oper
                                From DOBs d (nolock) left outer join TempLoadDOBs t (nolock)
        on d.DOBsId = t.DOBsId
        Where t.DOBsId is NULL and d.Status in (2,3) 
                End
                Else
                Begin
                                Update DOBs Set Status = @newstatus, LastModify = getDate(),
                                                LastOper = @oper
                                From DOBs d (nolock) 
                left outer join TempLoadDOBs t (nolock) on d.DOBsId = t.DOBsId
                left outer join #TempUserEntNums ta on d.EntNum = ta.EntNum
                                Where t.DOBsId is NULL and ta.EntNum is NULL and d.Status <> 4
                End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select DOBsId from TempLoadDOBs) Begin
     Insert into DOBs (DOBsId, Entnum, DOB, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.DOBsId, t.Entnum, t.DOB, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadDOBs t (nolock) left Outer Join DOBs d (nolock)
        on t.DOBsId = d.DOBsId
    where d.DOBsId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select NotesId from TempLoadNotes)  Begin
    -- Update the address records
   Update Notes Set
        EntNum = t.EntNum,
        Note = t.Note,
        NoteType = t.NoteType,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From Notes n, TempLoadNotes t
        Where n.NotesId = t.NotesId And
              ( (IsNull(n.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(n.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select NotesId from TempLoadNotes) Begin
   Update Notes Set Status = t.Status, LastOper = t.LastOper
    From Notes n, TempLoadNotes t
            Where n.NotesId = t.NotesId And
               IsNull(n.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
                If (@newstatus=1)
                Begin
                                Update Notes Set Status = @newstatus,
                                LastOper = @oper
                                From Notes n (nolock) left outer join TempLoadNotes t (nolock)
        on n.NotesId = t.NotesId
        Where t.NotesId is NULL and n.Status in (2,3)
                End
                Else
                Begin
                                Update Notes Set Status = @newstatus, LastModify = getDate(),
                                LastOper = @oper
                                From Notes n (nolock) 
                left outer join TempLoadNotes t (nolock) on n.NotesId = t.NotesId
                left outer join #TempUserEntNums ta on n.EntNum = ta.EntNum
                                Where t.NotesId is NULL and ta.EntNum is NULL and n.Status <> 4
                End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select NotesId from TempLoadNotes) Begin
     Insert into Notes (NotesId, Entnum, Note, NoteType, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.NotesId, t.Entnum, t.Note, t.NoteType, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadNotes t (nolock) left Outer Join Notes n (nolock)
        on t.NotesId = n.NotesId
    where n.NotesId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select URLsId from TempLoadURLs)  Begin
    -- Update the address records
   Update URLs Set
        EntNum = t.EntNum,
        URL = t.URL,
        [Description] = t.[Description],
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From URLs u, TempLoadURLs t
        Where u.URLsId = t.URLsId And
              ( (IsNull(u.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(u.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select URLsId from TempLoadURLs) Begin
   Update URLs Set Status = t.Status, LastOper = t.LastOper
    From URLs u, TempLoadURLs t
            Where u.URLsId = t.URLsId And
               IsNull(u.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
                If (@newstatus=1)
                Begin
                                Update URLs Set Status = @newstatus,
                                LastOper = @oper
                                From URLs u (nolock) left outer join TempLoadURLs t (nolock)
        on u.URLsId = t.URLsId
        Where t.URLsId is NULL and u.Status in (2,3)
                End
                Else
                Begin
                                Update URLs Set Status = @newstatus, LastModify = getDate(),
                                LastOper = @oper
                                From URLs u (nolock) 
                left outer join TempLoadURLs t (nolock) on u.URLsId = t.URLsId
                left outer join #TempUserEntNums ta on u.EntNum = ta.EntNum
                                Where t.URLsId is NULL and ta.EntNum is NULL and u.Status <> 4
                End

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select URLsId from TempLoadURLs) Begin
     Insert into URLs (URLsId, Entnum, URL, [Description], Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.URLsId, t.Entnum, t.URL, t.[Description], t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadURLs t (nolock) left Outer Join URLs u (nolock)
        on t.URLsId = u.URLsId
    where u.URLsId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select RelationshipId from TempLoadRelationship)  Begin
    -- Update the address records
   Update Relationship Set
        EntNum = t.EntNum,
        RelatedID = t.RelatedID,
        RelationshipPToR = t.RelationshipPToR,
        RelationshipRToP = t.RelationshipRToP,
        Status = t.Status,
        ListCreateDate = t.ListCreateDate,
        ListModifDate = t.ListModifDate,
        LastModify = GetDate(),
        LastOper = t.LastOper
    From Relationship r, TempLoadRelationship t
        Where r.RelationshipId = t.RelationshipId And
              ( (IsNull(r.ListModifDate,0) != IsNull(t.ListModifDate,0))
        Or ((IsNull(r.ListModifDate,0) = IsNull(t.ListModifDate,0)) And
            t.Status != 1 )
          )

    Select @stat = @@error
    If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

If Exists (Select RelationshipId from TempLoadRelationship) Begin
   Update Relationship Set Status = t.Status, LastOper = t.LastOper
    From Relationship r, TempLoadRelationship t
            Where r.RelationshipId = t.RelationshipId And
               IsNull(r.ListModifDate,0) = IsNull(t.ListModifDate,0) And
           t.Status = 1

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  -- Update record status of records not in temp table
                If (@newstatus=1)
                Begin
                                Update Relationship Set Status = @newstatus, 
                                LastOper = @oper
                                From Relationship r (nolock) left outer join TempLoadRelationship t (nolock)
        on r.RelationshipId = t.RelationshipId
        Where t.RelationshipId is NULL and r.Status in (2,3)
                End
                Else
                Begin
                                Update Relationship Set Status = @newstatus, LastModify = getDate(),
                                LastOper = @oper
                                From Relationship r (nolock) 
                left outer join TempLoadRelationship t (nolock) on r.RelationshipId = t.RelationshipId
                left outer join #TempUserEntNums ta on r.EntNum = ta.EntNum
                                Where t.RelationshipId is NULL and ta.EntNum is NULL and r.Status <> 4
                End
   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If Exists (Select RelationshipId from TempLoadRelationship) Begin
     Insert into Relationship (RelationshipId, Entnum, RelatedID,
        RelationshipPToR, RelationshipRToP, Status, ListCreateDate,
        ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
        Select t.RelationshipId, t.Entnum, t.RelatedID,
        t.RelationshipPToR, t.RelationshipRToP, t.Status, t.ListCreateDate,
        t.ListModifDate, t.CreateOper, GetDate(), t.LastOper, GetDate()
    From TempLoadRelationship t (nolock) left Outer Join Relationship r (nolock)
        on t.RelationshipId = r.RelationshipId
    where r.RelationshipId is Null

   Select @stat = @@error
   If (@stat <> 0)
    Goto AbortCompleteLoadDistribution
End

  if @isdiff = 0
  begin
      -- set status of undeleted records to unchanged
      Update SDNTable
        Set Status = 1 Where userRec = 1 and Deleted = 0

      Select @stat = @@error
      If (@stat <> 0)
          Goto AbortCompleteLoadDistribution
  end

--populate dob column in SDNTABLE from DOBs table.  DOB column should be shown as CSV value
update  sdntable set
dob=SUBSTRING(dt.dob_csv,1,150)
from (
                select
                                t1.entnum,
                                stuff((
                                                                select ',' + t.[dob]
                                                                from dobs t
                                                                where t.entnum = t1.entnum and
                                                                t.status in (1,2,3)
                                                                order by t.[dob]
                                                                for xml path('')
                                                                ),1,1,'') as dob_csv
                from dobs t1
                group by entnum
) dt
inner join sdntable  s on
dt.entnum=s.entnum
where s.status  in (1,2,3)

/*
                update the status to modified in sdntable if the following condition is met: 
                dobs table contains status of new or modified and status in Sdntable is NOT New.
                The LastModifDate should not be changed if the above conditions are met.
*/
UPDATE sdntable SET
[status]=3
FROM
( 
                SELECT MAX( d.status)AS NewMod, d.entnum 
                FROM dobs as d 
                WHERE d.status in (2,3)
                and 
                not exists
                (select * from sdntable sd where sd.entnum=d.entnum and sd.status=2)
                GROUP BY d.entnum
) AS dt
INNER JOIN sdntable AS st ON
st.entnum=dt.entnum

-- Set Keywords and Keywords Alternate to deleted 
--if the deleted keywords are introduced in the new distribution
Update Keywords 
Set Deleted = 1, 
                LastModify = GetDate() 
where Deleted = 0 and 
                  Word in (select word from Keywords where deleted = 1)

Update KeywordsAlt 
set Deleted = 1, 
                LastModify = GetDate() 
where Deleted = 0 and 
                  Word in (select word from KeywordsAlt where Deleted = 1)


delete from TempLoadSDNTable
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

delete from TempLoadAltTable
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadAddrTable
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadKeywords
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

delete from TempLoadKeywordsAlt
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadDOBs
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadNotes
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadURLs
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

  delete from TempLoadRelationship
  Select @stat = @@error
  If (@stat <> 0)
    Goto AbortCompleteLoadDistribution

If @trnCnt = 0
    commit tran OFS_CompleteLoadDistribution

return 0  -- Success

AbortCompleteLoadDistribution:
    if (@stat <> 0) Begin
        rollback tran OFS_CompleteLoadDistribution
        return @stat
    End
GO

IF EXISTS (SELECT * FROM dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_InsSanctionDataRule]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_InsSanctionDataRule]
GO 

Create Procedure dbo.OFS_InsSanctionDataRule ( @code SCode, @name LongName,
                        @incremental int, @useAddr bit,
                        @useKeyword bit,
                        @branchList varchar(1500),
                        @departmentList varchar(100),
                        @programList varchar(1500),
                        @blockProgramList varchar(1500),
                        @typeList varchar(500),
                        @blockTypeList varchar(500),
                        @excludeBranchList varchar(1500),
                        @excludeDeptList varchar(100),
                        @encWordLen int,
                        @maxEncWords int, @minEncWords int, @minEncChars int,
                        @encFirstChar bit,
                        @ignTrailS bit, @combWords bit, @inEquality bit,
                        @anchorOnFirstName bit, @anchorOnLastName bit,
                        @includeInitials bit, @includeLastName bit,
                        @deriveAllNameCombinations bit,
                        @derivedNameWordsLimit int,
                        @interveningWords int,
                        @doScoring bit,
                        @compTags bit,
                        @annotText varchar(255),
                        @IncrListType int,
                        @ListChangeDays int,
                        @useExcludes bit,
                        @UseUnassignedExcludes bit,
                        @wordWrapMatch bit,
                        @enabled bit,
                        @listTypeList varchar(200),
                        @blockListTypeList varchar(200),
                        @combWords_ListType varchar(150),
						@UseAbbrev bit,
						@UseNoise bit,
						@UseUnassignedAbbreviations bit,
						@UseUnassignedNoiseWords bit,
						@UseUnassignedInEqualities bit,
						@ExcludeWeakAka bit,
						@IncludeAbbrBranchDeptList varchar(1500),
						@ExcludeAbbrBranchDeptList varchar(1500),
						@IncludeNoiseBranchDeptList varchar(1500),
						@ExcludeNoiseBranchDeptList varchar(1500),
						@IncludeInEqBranchDeptList varchar(1500),
						@ExcludeInEqBranchDeptList varchar(1500),
						@EnablePartialDOBMatching bit,
						@DOBToleranceInYears int,
                        @oper SCode)
With Encryption 
As

  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount  -- Save the current trancount
  If @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_InsSanctionDataRule
  else
    -- Already in a transaction
    save tran OFS_InsSanctionDataRule
  -- End standard stored procedure transaction header

  declare @stat int
  if (@BranchList = '')
    select @useUnassignedExcludes = 1 -- Include exclude names with unspecified branch
                                      -- and departments when no choices to include are made

  insert into SanctionDataRule 
			( Code, Name, Incremental, UseAddr, UseKeyword,
			BranchList, DepartmentList, ProgramList, BlockProgramList, TypeList,
			BlockTypeList, ExcludeBranchList, ExcludeDeptList,  ListTypeList, BlockListTypeList,
			EncWordLen, MaxEncWords, MinEncWords, MinEncChars,
			EncFirstChar, IgnTrailS, CombWords, Inequality,
			AnchorOnFirstName, AnchorOnLastName, IncludeInitials, IncludeLastName,
			DeriveAllNameCombinations, DerivedNameWordsLimit,InterveningWords,
			DoScoring, CompMsgTagsToExclTags, remarks, IncrListType,
			ListChangeDays, useExcludes, useUnassignedExcludes, WordWrapMatch,
			Enabled, CombWords_ListType, 
			UseAbbrev, UseNoise, UseUnassignedAbbreviations, UseUnassignedNoiseWords,
			UseUnassignedInEqualities, ExcludeWeakAka, IncludeAbbrevBranchDeptList, ExcludeAbbrevBranchDeptList,
			IncludeNoiseBranchDeptList, ExcludeNoiseBranchDeptList, IncludeInEqBranchDeptList,
			ExcludeInEqBranchDeptList, EnablePartialDOBMatching, DOBToleranceInYears,
			CreateOper, CreateDate )
    values 
			(NullIf(RTrim(@code), ''), NullIf(RTrim(@name), ''), @incremental,
			@useAddr, @useKeyword, NullIf(RTrim(@branchList), ''),
			NullIf(RTrim(@departmentList), ''), NullIf(RTrim(@programList), ''),
			NullIf(RTrim(@blockProgramList), ''), NullIf(RTrim(@typeList), ''),
			NullIf(RTrim(@blockTypeList), ''), NullIf(RTrim(@excludeBranchList), ''),
			NullIf(RTrim(@excludeDeptList), ''), 
			NullIf(RTrim(@listTypeList), ''), NullIf(RTrim(@blockListTypeList), ''),
			@encWordLen, @maxEncWords,
			@minEncWords, @minEncChars,
			@encFirstChar, @ignTrailS, @combWords, @inEquality,
			@anchorOnFirstName , @anchorOnLastName, @includeInitials,
			@includeLastName, @deriveAllNameCombinations,
			@derivedNameWordsLimit,@interveningWords, @doScoring, @compTags,
			@annotText, @IncrListType, @ListChangeDays, @useExcludes, @useUnassignedExcludes,
			@wordWrapMatch, @Enabled, NullIf(RTrim(@CombWords_ListType), ''), 
			@UseAbbrev, @UseNoise, @UseUnassignedAbbreviations, @UseUnassignedNoiseWords,
			@UseUnassignedInEqualities, @ExcludeWeakAka, NullIf(RTrim(@IncludeAbbrBranchDeptList), ''), 
			NullIf(RTrim(@ExcludeAbbrBranchDeptList), ''),
			NullIf(RTrim(@IncludeNoiseBranchDeptList), ''), NullIf(RTrim(@ExcludeNoiseBranchDeptList), ''), 
			NullIf(RTrim(@IncludeInEqBranchDeptList), ''), NullIf(RTrim(@ExcludeInEqBranchDeptList), ''),
			@EnablePartialDOBMatching, @DOBToleranceInYears,
			NullIf(RTrim(@oper),''), GetDate() )

  Select @stat = @@error

  -- Evaluate results of the transaction
  If @stat <> 0 begin
    rollback tran OFS_InsSanctionDataRule
    return @stat
  end

  If @trnCnt = 0
  begin
    commit tran OFS_InsSanctionDataRule
    Exec OFS_SetFileImageFlag
  end
  return @stat
Go

IF EXISTS (SELECT * FROM dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_UpdSanctionDataRule]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_UpdSanctionDataRule]
GO 

Create Procedure dbo.OFS_UpdSanctionDataRule ( @code SCode, @name LongName,
                        @incremental int, @useAddr bit, @useKeyword bit,
                        @branchList varchar(1500),
                        @departmentList varchar(100),
                        @programList varchar(1500),
                        @blockProgramList varchar(1500),
                        @typeList varchar(500),
                        @blockTypeList varchar(500),
                        @excludeBranchList varchar(1500),
                        @excludeDeptList varchar(100), @encWordLen int,
                        @maxEncWords int, @minEncWords int, @minEncChars int,
                        @encMap char(128), @delimMap char(128),
                        @punctMap char(128), @encFirstChar bit,
                        @ignTrailS bit, @combWords bit,
                        @inEquality bit,
                        @anchorOnFirstName bit, @anchorOnLastName bit,
                        @includeInitials bit, @includeLastName bit,
                        @deriveAllNameCombinations bit,
                        @derivedNameWordsLimit int,
                        @interveningWords int,
                        @DoScoring bit,
                        @compTags bit,
                        @annotText varchar(255),
                        @IncrListType int,
                        @ListChangeDays int,
                        @useExcludes bit,
                        @useUnassignedExcludes bit,
                        @wordWrapMatch bit,
                        @enabled bit,
                        @oper SCode, 
                        @listTypeList varchar(200), 
                        @blockListTypeList varchar(200),
						@combWords_ListType varchar(150),
						@UseAbbrev bit,
						@UseNoise bit,
						@UseUnassignedAbbreviations bit,
						@UseUnassignedNoiseWords bit,
						@UseUnassignedInEqualities bit,
						@ExcludeWeakAka bit,
						@IncludeAbbrBranchDeptList varchar(1500),
						@ExcludeAbbrBranchDeptList varchar(1500),
						@IncludeNoiseBranchDeptList varchar(1500),
						@ExcludeNoiseBranchDeptList varchar(1500),
						@IncludeInEqBranchDeptList varchar(1500),
						@ExcludeInEqBranchDeptList varchar(1500),
						@EnablePartialDOBMatching bit,
						@DOBToleranceInYears int,
                        @ts timestamp )
With Encryption 
As

  -- Start standard stored procedure transaction header
  declare @trnCnt int
  select @trnCnt = @@trancount  -- Save the current trancount
  If @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_UpdSanctionDataRule
  else
    -- Already in a transaction
    save tran OFS_UpdSanctionDataRule
  -- End standard stored procedure transaction header

  declare   @stat int,
            @cnt int

  if (@BranchList = '')
    select @useUnassignedExcludes = 1 -- Include exclude names with unspecified branch
                                      -- and departments when no choices to include are made

  Update SanctionDataRule
	Set		Name = NullIf(RTrim(@name), ''),
			[Incremental] = @incremental, UseAddr = @useAddr,
			UseKeyword = @useKeyword,
			UseExcludes = @useExcludes,
			UseUnassignedExcludes = @useUnassignedExcludes,
			enabled = @enabled,
			BranchList = NullIf(RTrim(@branchList), ''),
			DepartmentList = NullIf(RTrim(@departmentList), ''),
			ProgramList = NullIf(RTrim(@programList), ''),
			BlockProgramList = NullIf(RTrim(@blockProgramList), ''),
			TypeList = NullIf(RTrim(@typeList), ''),
			BlockTypeList = NullIf(RTrim(@blockTypeList), ''),
			ExcludeBranchList = NullIf(RTrim(@excludeBranchList), ''),
			ExcludeDeptList = NullIf(RTrim(@excludeDeptList), ''),
			ListTypeList = NullIf(RTrim(@listTypeList), ''),
			BlockListTypeList = NullIf(RTrim(@blockListTypeList), ''),
			EncWordLen = @encWordLen, MaxEncWords = @maxEncWords,
			MinEncWords = @minEncWords, MinEncChars = @minEncChars,
			EncMap = NullIf(RTrim(@encMap), ''),
			DelimMap = NullIf(RTrim(@delimMap), ''),
			PunctMap = NullIf(RTrim(@punctMap), ''),
			EncFirstChar = @encFirstChar, IgnTrailS = @ignTrailS,
			CombWords = @combWords,
			InEquality = @inEquality,
			AnchorOnFirstName = @anchorOnFirstName ,
			AnchorOnLastName = @anchorOnLastName,
			IncludeInitials = @includeInitials,
			IncludeLastName = @includeLastName,
			DeriveAllNameCombinations = @deriveAllNameCombinations,
			DerivedNameWordsLimit = @derivedNameWordsLimit,
			InterveningWords = @interveningWords,
			remarks = @annotText,
			DoScoring = @doScoring,
			CompMsgTagsToExclTags = @compTags,
			IncrListType = @IncrListType,
			ListChangeDays = @ListChangeDays,
			WordWrapMatch = @wordWrapMatch,
			combWords_ListType=NullIf(RTrim(@combWords_ListType), ''),
			UseAbbrev =@UseAbbrev,
			UseNoise =@UseNoise,
			UseUnassignedAbbreviations =@UseUnassignedAbbreviations, 
			UseUnassignedNoiseWords =@UseUnassignedNoiseWords,
			UseUnassignedInEqualities =@UseUnassignedInEqualities,
			ExcludeWeakAka =@ExcludeWeakAka,
			IncludeAbbrevBranchDeptList =@IncludeAbbrBranchDeptList,
			ExcludeAbbrevBranchDeptList =@ExcludeAbbrBranchDeptList,
			IncludeNoiseBranchDeptList =@IncludeNoiseBranchDeptList, 
			ExcludeNoiseBranchDeptList =@ExcludeNoiseBranchDeptList,
			IncludeInEqBranchDeptList =@IncludeInEqBranchDeptList,
			ExcludeInEqBranchDeptList =@ExcludeInEqBranchDeptList,
			EnablePartialDOBMatching = @EnablePartialDOBMatching,
			DOBToleranceInYears = @DOBToleranceInYears,
			LastOper = NullIf(RTrim(@Oper), '')
    Where Code = @code and Ts = @ts
  Select @stat = @@error, @cnt = @@rowcount


  -- Evaluate results of the transaction
  If @stat <> 0 or @cnt = 0 begin
    if @@rowcount = 0 begin
      Select @stat = 250001 -- Concurrent Update
    end
    rollback tran OFS_UpdSanctionDataRule
    return @stat
  end

  If @trnCnt = 0
  begin
    commit tran OFS_UpdSanctionDataRule
    Exec OFS_SetFileImageFlag
  end
  return @stat
Go

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_SelectMemoryImage]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_SelectMemoryImage]
Go

CREATE PROCEDURE dbo.OFS_SelectMemoryImage(@Incremental int,
        @listModDate datetime)
With Encryption As
   declare @stat int
   
  Select 1 as TableType, EntNum, Name, FirstName, MiddleName,
         LastName, DuplicRec, SDNType, DataType, Program, UserRec,
         IgnoreDerived, DOB, Remarks1, Remarks2,
         Title, NULL as Address, NULL as City, NULL as State, NULL as Country,
    	   Case When @Incremental = 0 then 0 Else
    	   Case When ( (CreateDate > Isnull(@listModDate,0) or
                       IsNull(LastModifDate,0) > Isnull(@listModDate,0)) And
	               (Status != 4) ) Then 1 
    	   Else 0 End
           End as IsIncremental, ListType,NULL
  From SDNTable Where Deleted = 0 and Status != 4
UNION
  Select 2 as TableType, SA.EntNum, AltName, SA.FirstName, SA.MiddleName,
    	 SA.LastName, NULL, S.SDNType, NULL, S.Program, S.UserRec,
    	 S.IgnoreDerived, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    	   Case When @Incremental = 0 then 0 Else
           Case When ( SA.CreateDate > Isnull(@listModDate,0) or
                IsNull(SA.LastModifDate,0) > Isnull(@listModDate,0)) Then 1 
           Else 0 End
           End as IsIncremental, ListType,Category
  From SDNTable S, SDNAltTable SA where S.EntNum = SA.Entnum and
      (S.Deleted = 0 and S.Status != 4) And
      (SA.Deleted = 0 and (SA.Status != 4))
UNION
  Select 3 as TableType, SA.EntNum, NULL, NULL, NULL,
    	 NULL, NULL, S.SDNType, NULL, S.Program, S.UserRec,
    	 S.IgnoreDerived, NULL, NULL, NULL, NULL,
    	 address + isnull(' ' + address2, '') + isnull(' ' + address3,'') +
         isnull(' ' + address4,'') as Address, City, State, SA.Country,
    	   Case When @Incremental = 0 Then 0 Else
           Case When ( SA.CreateDate > Isnull(@listModDate,0) or
                IsNull(SA.LastModifDate,0) > Isnull(@listModDate,0) ) Then 1 
           Else 0 End
    	   End as IsIncremental, ListType,NULL
  From SDNTable S, SDNAddrTable SA Where S.EntNum = SA.Entnum And 
       (S.Deleted = 0 and S.Status != 4) And
       (SA.Deleted = 0 and (SA.Status != 4))
UNION
  Select 4 as TableType, K.EntNum, K.Word, NULL, NULL,
        NULL, NULL, 4, 9, S.Program, 0,
        0, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,
    	  Case When @Incremental = 0 Then 0 Else
          Case When ( K.CreateDate > Isnull(@listModDate,0) or
               IsNull(K.LastModify,0) > Isnull(@listModDate,0) ) Then 1 
          Else 0 End
          End as IsIncremental, ListType,NULL
  From SDNTable S, Keywords K Where S.EntNum = K.EntNum And 
       (S.Deleted = 0 and S.Status != 4) And
       (K.Deleted = 0 and (K.Status != 4))
Union
  Select 5 as TableType, SA.EntNum, KA.Word, NULL, NULL,
        NULL, NULL, 4, 9, S.Program, 0,
        0, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,
          Case When @Incremental = 0 Then 0 Else
          Case When ( KA.CreateDate > Isnull(@listModDate,0) or
               IsNull(KA.LastModify,0) > Isnull(@listModDate,0) ) Then 1 
          Else 0 End
          End as IsIncremental,ListType,NULL
  From SDNTable S, SDNAltTable SA, KeywordsAlt KA Where S.EntNum = SA.EntNum And 
       SA.AltNum = KA.AltNum And
       (S.Deleted = 0 and S.Status != 4) And
       (SA.Deleted = 0 and SA.Status != 4) And
       (KA.Deleted = 0 and (KA.Status != 4))
  Order by EntNum, TableType

  select @stat = @@ERROR
  return @stat

GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_SelectSanctionDataRule]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_SelectSanctionDataRule]
Go

CREATE PROCEDURE dbo.OFS_SelectSanctionDataRule (@code SCode)
With Encryption 
As

  declare @stat int

  if (isnull(@code, '') != '')
  select Code, Name, Incremental, UseAddr, UseKeyword, BranchList, 
      DepartmentList,ProgramList, BlockProgramList, TypeList, BlockTypeList,
      ExcludeBranchList, ExcludeDeptList, ListTypeList, BlockListTypeList, 
      EncWordLen, MaxEncWords, MinEncWords, MinEncChars, EncMap, DelimMap, PunctMap, 
      EncFirstChar, IgnTrailS,CombWords, InEquality, AnchorOnFirstName, AnchorOnLastName,
      IncludeInitials, IncludeLastName, DeriveAllNameCombinations,
      DerivedNameWordsLimit,InterveningWords, DoScoring, CompMsgTagsToExclTags,
      Remarks, IncrListType, ListChangeDays, useExcludes, useUnassignedExcludes,
      WordWrapMatch, Enabled, CombWords_ListType, useAbbrev, useUnassignedAbbreviations, 
      IncludeAbbrevBranchDeptList, ExcludeAbbrevBranchDeptList,useNoise, useUnassignedNoiseWords, 
      IncludeNoiseBranchDeptList, ExcludeNoiseBranchDeptList,useUnassignedInequalities, 
      IncludeIneqBranchDeptList, ExcludeIneqBranchDeptList, EnablePartialDOBMatching, DOBToleranceInYears,ExcludeWeakAka,
      CreateOper, CreateDate, LastOper, LastModify, Ts
  from SanctionDataRule  where code = @code
  else
  select Code, Name, Incremental, UseAddr, UseKeyword, BranchList, 
      DepartmentList,ProgramList, BlockProgramList, TypeList, BlockTypeList,
      ExcludeBranchList, ExcludeDeptList, ListTypeList, BlockListTypeList, 
      EncWordLen, MaxEncWords, MinEncWords, MinEncChars, EncMap, DelimMap, PunctMap, 
      EncFirstChar, IgnTrailS,CombWords, InEquality, AnchorOnFirstName, AnchorOnLastName,
      IncludeInitials, IncludeLastName, DeriveAllNameCombinations,
      DerivedNameWordsLimit,InterveningWords, DoScoring, CompMsgTagsToExclTags,
      Remarks, IncrListType, ListChangeDays, useExcludes, useUnassignedExcludes,
      WordWrapMatch, Enabled, CombWords_ListType, useAbbrev, useUnassignedAbbreviations, 
      IncludeAbbrevBranchDeptList, ExcludeAbbrevBranchDeptList,useNoise, useUnassignedNoiseWords, 
      IncludeNoiseBranchDeptList, ExcludeNoiseBranchDeptList,useUnassignedInequalities, 
      IncludeIneqBranchDeptList, ExcludeIneqBranchDeptList, EnablePartialDOBMatching, DOBToleranceInYears,ExcludeWeakAka,
      CreateOper, CreateDate, LastOper, LastModify, Ts
  from SanctionDataRule

  select @stat = @@ERROR
  return @stat
Go
If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[OFS_SelectEnabledSanctionDataRule]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[OFS_SelectEnabledSanctionDataRule]
Go

CREATE PROCEDURE dbo.OFS_SelectEnabledSanctionDataRule (@code SCode)
With Encryption 
As

  declare @stat int

  if (isnull(@code, '') != '')
	  select Code, Name, Incremental, UseAddr, UseKeyword, BranchList, 
	      DepartmentList,ProgramList, BlockProgramList, TypeList, BlockTypeList,
	      ExcludeBranchList, ExcludeDeptList, ListTypeList, BlockListTypeList, 
	      EncWordLen, MaxEncWords, MinEncWords, MinEncChars, EncMap, DelimMap, PunctMap, 
	      EncFirstChar, IgnTrailS,CombWords, InEquality, AnchorOnFirstName, AnchorOnLastName,
	      IncludeInitials, IncludeLastName, DeriveAllNameCombinations,
	      DerivedNameWordsLimit,InterveningWords, DoScoring, CompMsgTagsToExclTags,
	      Remarks, IncrListType, ListChangeDays, useExcludes, useUnassignedExcludes,
	      WordWrapMatch, Enabled, CombWords_ListType, useAbbrev, useUnassignedAbbreviations, 
	      IncludeAbbrevBranchDeptList, ExcludeAbbrevBranchDeptList,useNoise, useUnassignedNoiseWords, 
	      IncludeNoiseBranchDeptList, ExcludeNoiseBranchDeptList,useUnassignedInequalities, 
	      IncludeIneqBranchDeptList, ExcludeIneqBranchDeptList, EnablePartialDOBMatching, DOBToleranceInYears,ExcludeWeakAka,
	      CreateOper, CreateDate, LastOper, LastModify, Ts
	  from SanctionDataRule where Enabled = 1 and code = @code
  else
	  select Code, Name, Incremental, UseAddr, UseKeyword, BranchList, 
	      DepartmentList,ProgramList, BlockProgramList, TypeList, BlockTypeList,
	      ExcludeBranchList, ExcludeDeptList, ListTypeList, BlockListTypeList, 
	      EncWordLen, MaxEncWords, MinEncWords, MinEncChars, EncMap, DelimMap, PunctMap, 
	      EncFirstChar, IgnTrailS,CombWords, InEquality, AnchorOnFirstName, AnchorOnLastName,
	      IncludeInitials, IncludeLastName, DeriveAllNameCombinations,
	      DerivedNameWordsLimit,InterveningWords, DoScoring, CompMsgTagsToExclTags,
	      Remarks, IncrListType, ListChangeDays, useExcludes, useUnassignedExcludes,
	      WordWrapMatch, Enabled, CombWords_ListType, useAbbrev, useUnassignedAbbreviations, 
	      IncludeAbbrevBranchDeptList, ExcludeAbbrevBranchDeptList,useNoise, useUnassignedNoiseWords, 
	      IncludeNoiseBranchDeptList, ExcludeNoiseBranchDeptList,useUnassignedInequalities, 
	      IncludeIneqBranchDeptList, ExcludeIneqBranchDeptList, EnablePartialDOBMatching, DOBToleranceInYears,ExcludeWeakAka,
	      CreateOper, CreateDate, LastOper, LastModify, Ts
	  from SanctionDataRule where Enabled = 1

  select @stat = @@ERROR
  return @stat
Go

-- Script to insert 'User' ListType
If EXISTS (SELECT 1 FROM ListType with (nolock) where code = 'USER')
Begin
	If NOT EXISTS (SELECT 1 FROM [ListTypeAssociation] WITH(nolock) WHERE ListType = 'USER')
		INSERT INTO [ListTypeAssociation] VALUES ('USER',Null,Null)
End
Else
Begin
	INSERT INTO ListType (Code, Name, Description, IssuingAuthority, Country, Region, ListDate, IsPrimeList, Enabled, CreateOper, CreateDate, LastOper, LastModify)
	VALUES ('USER', 'User Entered Records', 'User entered Sanction Records', 'USER','', '', NULL, 0, 1, 'PRIME', GETDATE(), 'PRIME', GETDATE())

	If NOT EXISTS (SELECT 1 FROM [ListTypeAssociation] WITH(nolock) WHERE ListType = 'USER')
		INSERT INTO [ListTypeAssociation] VALUES ('USER',Null,Null)
End 
GO

PRINT ''
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT 'Completed Conversion of OFAC Database to version 10.1.3  '
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT ''
GO