USE [OFAC]
GO

/****** Object:  StoredProcedure [dbo].[OFS_AddSDN]    Script Date: 12/18/2017 3:15:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-------------------------------------------------------------------------------

Alter PROCEDURE [dbo].[OFS_AddSDN]( @entNum int,
                            @name SanctionName,
                            @firstName SanctionName,
                            @middleName SanctionName,
                            @lastName SanctionName,
                            @type char (12),
                            @program varchar(200),
                            @title varchar(200),
                            @callSign char(8),
                            @vessType char(25),
                            @tonnage char(14),
                            @grt char(8),
                            @vessFlag varchar(40),
                            @vessOwner varchar(150) ,
                            @dob varchar(150) ,
                            @remarks text ,
                            @remarks1 varchar(255) ,
                            @remarks2 varchar(255) ,
                            @sdnType int,
                            @dataType int,
                            @userRec int ,
							@country varchar(250),
							@sex char,
							@build varchar(15),
		   					@complexion varchar(15),
							@race varchar(15),
							@eyes varchar(15),
							@hair varchar(15),
							@height int,
							@weight int,
							@listID varchar(15),
							@listType varchar(15),
							@primeAdded bit,
							@standardOrder bit,
							@effectiveDate datetime,
							@expiryDate datetime,
							@status int,
							@listCreateDate datetime,
							@listModifDate dateTime,
							@oper Code,
							@log int,
							@ignDrvNames bit,			    
							@retEntnum int OUTPUT)
  As

  declare @trnCnt        int

  select @trnCnt = @@trancount	-- Save the current trancount
  if @trnCnt = 0
    -- Transaction has not begun
    begin tran OFS_AddSDN
  else
    -- Already in a transaction
    save tran OFS_AddSDN

  -- Processing starts here:
  declare @now datetime
  declare @pos int
  declare @stat int
  declare @text varchar(255)
  declare @maxNum int
  declare @usermaxNum int

  select @now = GETDATE()

  If @type = ''
    select @type = 'other'

  Select isnull(@ignDrvNames,1)

  -- set status of user records to Added = 2
  If @userRec = 1 and @status <> 2
		Select @status = 2

  -- Assure valid sdnType
  If ( @sdnType <> 1 and @sdnType <> 2 and @sdnType <> 3 and @sdnType <> 4 )
   or @sdnType Is Null Begin
		Set @text = 'Unable to add name containing invalid Sdn Type "' +
			COALESCE(Convert(varchar, @sdnType), 'Null') + '".'
		Exec OFS_InsEvent @oper, 'Ann', 'SanName', @name, @text
		Set @stat = 250010  -- Invalid Parameter
		rollback tran OFS_AddSDN
		return @stat
  End

   If @userRec <> 0 Begin
       Select @maxNum = Max(EntNum) From SDNTable
       Select @stat = @@ERROR

       If @stat <> 0 Begin
		rollback tran OFS_AddSDN
  		return @stat
       End

       If @maxNum Is NULL Set @maxNum = 0
       if @maxNum < 10000000
          Set @maxNum = 10000000
       else
	      --modify 2017.12 for World-Check One
	      Select @usermaxNum = Max(EntNum) From SDNTable where EntNum >= 10000000 and EntNum <= 20000000
		  If @usermaxNum Is NULL Set @usermaxNum = 10000000

          Set @maxNum = @usermaxNum +1
       Set @entNum = @maxNum
   End

   Insert into SDNTable (EntNum, [Name], FirstName,
                        MiddleName, LastName,Title,
                        Program, Type, CallSign,
                        VessType, Tonnage, GRT,
                        VessFlag, VessOwner, Dob,
                        Remarks, Remarks1, Remarks2,SDNType,
                        DataType, userRec, deleted,
                        duplicRec, IgnoreDerived,
						ListID, ListType, Country,
						Sex, Build, Height, Weight,
						Race, Complexion, eyes, hair,
						PrimeAdded, StandardOrder, EffectiveDate,
						ExpiryDate, Status, ListCreateDate,
						ListModifDate, CreateDate, LastModifDate,
						LastOper)
    Values
           (@entNum, @name, NULLIF(@firstName,''),
            NULLIF(@middleName,''), NULLIF(@lastName,''), @title,
            UPPER(@program),  @type,  @callSign,
            @vessType, @tonnage, @grt,
            @vessFlag, @vessOwner, NULLIF(@dob,''),
            @remarks, @remarks1, @remarks2, @sdnType,
            @dataType, @userRec, 0,
            0, @ignDrvNames,
			@listId, UPPER(@listType), @country,
			@sex, @build, NullIf(@height,0), NullIf(@weight,0),
			@race, @complexion, @eyes, @hair,
			@primeAdded, @standardOrder, @effectiveDate,
			@expiryDate, @status, @listCreateDate,
			@listModifDate, @now, @now, @oper )

  Select @stat = @@ERROR, @retEntNum = @entNum
  If @stat = 0 Begin
    If @log <> 0 Begin
  	  Exec OFS_InsEvent @oper, 'Cre', 'SanName', @name, Null
	  select @stat = @@ERROR
	  If @stat = 0 Begin
	      Exec OFS_SetFileImageFlag
  	      commit tran OFS_AddSDN
 	  End Else
	    rollback tran OFS_AddSDN
	End else
   	  commit tran OFS_AddSDN
  End else
    rollback tran OFS_AddSDN
  return @stat

GO


