<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucSupportingInfo" Src="ucSupportingInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AcceptanceReview.aspx.vb" Inherits="CMBrowser.AcceptanceReview" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Customer Acceptance</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="Styles.css" type="text/css" rel="stylesheet"/>
		<style type="text/css">.tgridheader { BORDER-RIGHT: white outset; PADDING-RIGHT: 0px; BORDER-TOP: white outset; PADDING-LEFT: 0px; FONT-WEIGHT: bold; FONT-SIZE: 12px; PADDING-BOTTOM: 2px; BORDER-LEFT: white outset; COLOR: white; PADDING-TOP: 2px; BORDER-BOTTOM: white outset; BACKGROUND-COLOR: #990000; TEXT-ALIGN: left }
	.lblgridcell { FONT-SIZE: 11px; Font-Names: Verdana, Arial, Helvetica, sans-serif }
	.noline { FONT-SIZE: 11px; COLOR: blue; TEXT-DECORATION: none }
		</style>
		<script type="text/javascript" language="JavaScript" src="js/nav.js"></script>
		<script language="javascript">


			var prevControlValue;
			var req;
			function InitializeMSXML()
			{  
				req= new ActiveXObject("Msxml2.XMLHTTP");          
			}
			function IsValidDocument(key)
			{
				InitializeMSXML();
				var url="AcceptanceReview.aspx?DocNo="+key;	
				if(req!=null)	{
					req.onreadystatechange = Process;    
					req.open("GET", url, true);        
					req.send(null);		
				}
			}
			function Process()
			{		
				var temp;
				var pos;
				if (req.readyState == 4)
					{
					// only if "OK"
						if (req.status == 200)
						{
							
							if(req.responseText != "")                    
							{				    
								temp = req.responseText;								
								pos = temp.indexOf("<!DOCTYPE");
								temp = temp.substr(0, pos);							
								if (temp.length > 2 ) {							
									alert(temp);
									document.getElementById("txtDocument").value =  "";
								}								
							}
						}           
					}
			}

			
			
			var fldValidDocument=document.getElementById("hdfldValidDocument");
			
			if (fldValidDocument != null) {
				if (fldValidDocument.value == "0")  {
					alert("Please enter a valid document number");
				}
			}
			
			
			function getdate() {
				var dt = new Date();
				var month;
				month = dt.getMonth();				
				month = month +1;
				
			
				
				return month  + "/" + dt.getDate() + "/" + dt.getFullYear() + " " +
					 dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
					
			}
			function checkDueDate() {
			}
			
			function IsItemFinalized(clientid) {
				
				
			    try
				{
				   
				    if (document.getElementById(clientid + "hdfldApproveRequired").value == "True") {
					    if (document.getElementById(clientid + "chkApprove").checked == true) 
						    return true;
					    else
						    return false;
				    }
				
				
				    if (document.getElementById(clientid + "hdfldSecondCheckRequired").value == "True") {					
					    if (document.getElementById(clientid + "chkSecondCheck").checked == true) 
						    return true;
					    else
						    return false;
				    }
				
				
				    if (document.getElementById(clientid + "hdfldRequired").value == "True") {					
					    if (document.getElementById(clientid + "chkComplete").checked == true) 
						    return true;
					    else
						    return false;
				    } 
				
				    return false;
				}
				catch(ex) 
				{ 
				    return false;
				}
			}
			
			
			function ControlChanged()			
			{	
				if (!e) var e = window.event;
				var controltype;
				var controlid;
				var controlname;
				var pos;
				var checkControl;
				var clientid;
				var selecteditem;
				var dt;
				var itemFinalized = false;
				controltype = e.srcElement.type;
				controlid = e.srcElement.id;				
				selecteditem = document.getElementById("hdfldSelectedItem").value;	
				
				pos = selecteditem.indexOf("ctl");
				pos = selecteditem.indexOf("_", pos);					
				clientid =  selecteditem.substr(0,pos+1);
				
				
				
				itemFinalized = IsItemFinalized(clientid);	
				pos = controlid.indexOf("ctl");
				pos = controlid.indexOf("_", pos);
				controlname = controlid.substr(pos+1);
				
				
				
				
				
				switch(controlid) 
				{
					case "txtDocument" :
						
						if(itemFinalized == true) {
							alert("Cannot modify, this item has already been finalized. Undo finalize and try again.");
							e.srcElement.value = prevControlValue;	
							return;						
						}
						
						if(isNaN(e.srcElement.value)) {
							alert("Please enter a numeric value for the document number");
							e.srcElement.value = prevControlValue;	
							return;
						}
						
						IsValidDocument(e.srcElement.value);
						
						document.getElementById(selecteditem.replace("lnkbtnItemId", "hdfldDocument")).value =
							e.srcElement.value;					
						
						break;
					case "txtNotes" :						
						document.getElementById(selecteditem.replace("lnkbtnItemId", "hdfldNotes")).value =
							e.srcElement.value;
					default:
					
						break;
				}
				
			
				
				switch(controlname) 
				{
					case "txtResponsiblePerson" :
						// if approved then no modify is allowed
						if(itemFinalized == true) {
							alert("Cannot modify, this item has already been finalized. Undo finalize and try again.");
							e.srcElement.value = prevControlValue;							
						}
						break;
					case "txtDueDate" :
						// if approved then no modify is allowed
						dt = e.srcElement.value;
						if  (isDate(dt)== false){						
							e.srcElement.focus();											
							break;
						}
						if(itemFinalized == true) {
							alert("Cannot modify, this item has already been finalized. Undo finalize and try again.");
							e.srcElement.value = prevControlValue;							
						}
						else
						{
						
						    // Due date should be between original due date and original due date + allowedDaysForDueDate		
						    var vCurrDueDate = new Date(e.srcElement.value);
						    
						    var vCreateDateText = document.getElementById(selecteditem.replace("lnkbtnItemId", "txtCreateDate")).value;		     							
	    				
			        		var vAllowedDays =  document.getElementById(selecteditem.replace("lnkbtnItemId", "hdfldAllowedDaysForDueDate")).value;		
						    var vCreateDate = new Date(vCreateDateText);
						    var vMaxDueDate = new Date(vCreateDate.getTime() + vAllowedDays*24*60*60*1000);
						    													
						
						    if (vCurrDueDate >= vCreateDate && vCurrDueDate <= vMaxDueDate) {
							    //OK
						    } else {
						    
						        var Msg = "Invalid date. " + 
							        "Review must be done before next EDD cycle starts. \n";
							        
							    if  (vAllowedDays == 0 )
							    {
							        Msg =Msg + "Review Date must be " +
								    "the same day as Create Date, \n" + 
								    "that is " +
								    vCreateDate.toLocaleDateString();
							    }
							    else
							    {
							        Msg =Msg + "Review Date must be " +
								    "no later than " + vAllowedDays +
								    " days from Create Date, \n" + 
								    "that is from " +
								    vCreateDate.toLocaleDateString() + " to " + 
								    vMaxDueDate.toLocaleDateString()  + ".";
							    }
							    alert (Msg);
							       
								e.srcElement.value = prevControlValue
						    }
						}
						break;
						
					case "ddlDecision" :
						// cannot modify id user does not have complete right
						if (document.getElementById(controlid.replace(controlname, "hdfldCanComplete")).value 
								!= "True" ) {
							alert("You do not have rights to complete this item. Please check if you have the '" +
							 document.getElementById(controlid.replace(controlname, "hdfldCompleteRight")).value +
								"' right. " );
							e.srcElement.value = prevControlValue;
							break;
						}					
						// cannot modify if item has been completed						
						if(document.getElementById(controlid.replace(controlname, "chkComplete")).checked == true) {
							alert("Cannot change decision, this item has already been completed. Undo complete and try again.");
							e.srcElement.value = prevControlValue;	
							}
						else {
							if (e.srcElement.value==1 || e.srcElement.value==2 || e.srcElement.value==4) {
								document.getElementById(controlid.replace(controlname, "chkComplete")).checked = true;
								document.getElementById("txtCompletedBy").value = 
											document.getElementById("hdfldOperator").value;	
								document.getElementById(controlid.replace(controlname, "hdfldCompletedBy")).value = 
											document.getElementById("hdfldOperator").value;		
								document.getElementById("txtCompletedDate").value = FormatDate(getdate());
								document.getElementById(controlid.replace(controlname, "hdfldCompletedDate")).value = getdate();
								document.getElementById(controlid.replace(controlname, "hdfldStatus")).value = "1"				
							}
						}
						break;
						
					default:
						break;
						
				}
			
			}
			
			function PopupWindow(mode) {
				var qstring;
				switch(mode) {
				case 1 : //Add
					qstring = "Document.aspx?ActionID=1&RowNum=0&NoScroll=1&" +
					"ParentID=" + escape(document.getElementById("txtCode").value) + 
					"&ParentPage=AcceptanceReview"; 
					break;
				case 2 : //Display
					if ( document.getElementById("txtDocument").value == "" ) 
						return;				
					qstring =  "Document.aspx?"+ "RecordID="+ document.getElementById("txtDocument").value + 
							 "&ActionID=2&RowNum=0&NoScroll=1"
					break;
				default:
					break;
					}
				
				
				var nw=window.open(qstring,"Document", "scrollbars=yes,resizable=yes,width=880,height=600");	
				nw.focus();	
				return 0;
		
			}
			
			function FormatDate(dt) {
				var dt1;
				var pos;
				
				pos = dt.indexOf(" ");
				if (pos > 0) {
					dt1 = dt.substr(0, pos);
					if (dt1 == "12/30/1899") 
						return "";
					else if (dt1 == "30/12/1899") 
						return "";					
					else
						return dt1;
				}
				else
					return "";
							
			}
			function UpdateBottomPanel(clientid) {
				
			
				document.getElementById("lblItemDetails").innerHTML =  "Details for Item: " + document.getElementById(clientid + "hdfldItemName").value;
				document.getElementById("txtCompletedBy").value = document.getElementById(clientid + "hdfldCompletedBy").value;
				document.getElementById("txtSecondCheckedBy").value = document.getElementById(clientid + "hdfldSecondCheckedBy").value;
				document.getElementById("txtApprovedBy").value = document.getElementById(clientid + "hdfldApprovedBy").value;


				document.getElementById("txtCompletedDate").value = FormatDate(document.getElementById(clientid + "hdfldCompletedDate").value);
				document.getElementById("txtSecondCheckedDate").value = FormatDate(document.getElementById(clientid + "hdfldSecondCheckedDate").value);
				document.getElementById("txtApprovedDate").value = FormatDate(document.getElementById(clientid + "hdfldApprovedDate").value);
				document.getElementById("txtCreateDate").value = FormatDate(document.getElementById(clientid + "hdfldCreateDate").value);


				document.getElementById("txtNotes").value = document.getElementById(clientid + "hdfldNotes").value;
				document.getElementById("txtDocument").value = document.getElementById(clientid + "hdfldDocument").value;				
				document.getElementById("hdfldSelectedItem").value = clientid + "lnkbtnItemId";
				
					 
				
			}
			function ControlClicked()	
			
			{ 
				
				if (!e) var e = window.event;
				var controltype;
				var controlid;
				var controlname;
				var pos;
				var checkControl;
				var clientid;
				
				
				controltype = e.srcElement.type;
				controlid = e.srcElement.id;
				switch(controlid) 
				{
					case "txtDocument" :
						prevControlValue = (e.srcElement.value);
						return;
						break;
					default:
						break;
				}
				pos = controlid.indexOf("ctl")
				pos = controlid.indexOf("_", pos)
				clientid =  controlid.substr(0,pos+1);
				controlname = controlid.substr(pos+1);
				
				UpdateBottomPanel(clientid);
				
				
				
				
				switch(controlname) 
				{
					case "chkComplete" :
						
						//Cannot complete if user does not have right
						if (document.getElementById(controlid.replace(controlname, "hdfldCanComplete")).value 
								!= "True" ) {
									alert("You do not have rights to complete this item. Please check if you have the '" +
							 document.getElementById(controlid.replace(controlname, "hdfldCompleteRight")).value +
								"' right. " );
							e.srcElement.checked=false;			
							break;
						}
					
						// cannot complete if decision 0 or 3						
						
						if (e.srcElement.checked) {
							if(document.getElementById(controlid.replace(controlname, "ddlDecision")).value == 0 || 
								document.getElementById(controlid.replace(controlname, "ddlDecision")).value == 3)
							{
								alert("Complete is not allowed for items with decision 'Undetermined' or 'Needs Review'.");
								e.srcElement.checked=false;				
							} else {
								
								document.getElementById("txtCompletedBy").value = 
											document.getElementById("hdfldOperator").value;	
								document.getElementById(controlid.replace(controlname, "hdfldCompletedBy")).value = 
											document.getElementById("hdfldOperator").value;		
								document.getElementById("txtCompletedDate").value = FormatDate(getdate());
								document.getElementById(controlid.replace(controlname, "hdfldCompletedDate")).value = getdate();
								document.getElementById(controlid.replace(controlname, "hdfldStatus")).value = "1"
														
							}
						}
						else {
						// Cannot undo complete if already second checked
						if(document.getElementById(controlid.replace(controlname, "chkSecondCheck")).checked == true) {
								 alert("Cannot modify, this item has already been second checked. Undo second check and try again.");	
								 e.srcElement.checked=true;				
							} else {
								
								document.getElementById("txtCompletedBy").value = "";											
								document.getElementById(controlid.replace(controlname, "hdfldCompletedBy")).value = "";											
								document.getElementById("txtCompletedDate").value = "";
								document.getElementById(controlid.replace(controlname, "hdfldCompletedDate")).value = "";
								document.getElementById(controlid.replace(controlname, "hdfldStatus")).value = "0"														
							} 
						}
						break;
					case "chkSecondCheck" :
						//Cannot second check if user does not have right
						if (document.getElementById(controlid.replace(controlname, "hdfldCanSecondCheck")).value 
								!= "True" ) {
									alert("You do not have rights to second check this item. Please check if you have the '" +
							 document.getElementById(controlid.replace(controlname, "hdfldSecondCheckRight")).value +
								"' right. " );
							e.srcElement.checked=false;			
							break;
						}
					
						// cannot second check if not completed						
						if (e.srcElement.checked) {
							//checkcontrol = controlid.replace(controlname, "chkComplete");
							
							if(document.getElementById(controlid.replace(controlname, "chkComplete")).checked == false) {
								alert("You must first complete an item before second-checking it.");	
								e.srcElement.checked=false;											
							} else {
								
								document.getElementById("txtSecondCheckedBy").value = 
											document.getElementById("hdfldOperator").value;	
								document.getElementById(controlid.replace(controlname, "hdfldSecondCheckedBy")).value = 
											document.getElementById("hdfldOperator").value;	
								document.getElementById("txtSecondCheckedDate").value = FormatDate(getdate());
								document.getElementById(controlid.replace(controlname, "hdfldSecondCheckedDate")).value = getdate();
								document.getElementById(controlid.replace(controlname, "hdfldStatus")).value = "2"
								
											
															
							}
						}
						else {
						// Cannot undo second check if already approved
						if(document.getElementById(controlid.replace(controlname, "chkApprove")).checked == true) {
								 alert("Cannot modify, this item has already been finalized. Undo approve and try again.");	
								 e.srcElement.checked=true;				
							} else {
								
								document.getElementById("txtSecondCheckedBy").value = "";											
								document.getElementById(controlid.replace(controlname, "hdfldSecondCheckedBy")).value = "";											
								document.getElementById("txtSecondCheckedDate").value = "";
								document.getElementById(controlid.replace(controlname, "hdfldSecondCheckedDate")).value = "";
								document.getElementById(controlid.replace(controlname, "hdfldStatus")).value = "1"
														
							} 
						}
						break;

					case "chkApprove" :
						
						//Cannot approve if user does not have right
						if (document.getElementById(controlid.replace(controlname, "hdfldCanApprove")).value 
								!= "True" ) {
									alert("You do not have rights to approve this item. Please check if you have the '" +
							 document.getElementById(controlid.replace(controlname, "hdfldApproveRight")).value +
								"' right. " );
							e.srcElement.checked=false;			
							break;
						}
						
						// cannot approve if not second checked
						if (e.srcElement.checked) {
								if(document.getElementById(controlid.replace(controlname, "chkSecondCheck")).checked == false) {
								 alert("You must first second-check an item before approving it.");
								 e.srcElement.checked=false;				
							} else {
								
								document.getElementById("txtApprovedBy").value = 
											document.getElementById("hdfldOperator").value;
								document.getElementById(controlid.replace(controlname, "hdfldApprovedBy")).value = 
											document.getElementById("hdfldOperator").value;	
								document.getElementById("txtApprovedDate").value = FormatDate(getdate());
								document.getElementById(controlid.replace(controlname, "hdfldApprovedDate")).value = getdate();
								document.getElementById(controlid.replace(controlname, "hdfldStatus")).value = "3"									
							} 
						} else {
								
								document.getElementById("txtApprovedBy").value = "";											
								document.getElementById(controlid.replace(controlname, "hdfldApprovedBy")).value = "";											
								document.getElementById("txtApprovedDate").value = "";
								document.getElementById(controlid.replace(controlname, "hdfldApprovedDate")).value = "";
								document.getElementById(controlid.replace(controlname, "hdfldStatus")).value = "2"
														
							}
						break;
					case "txtResponsiblePerson" :
						prevControlValue = e.srcElement.value;
						break;
					case "txtDueDate" :
						prevControlValue = e.srcElement.value;
						break;
					case "ddlDecision" :
						prevControlValue = e.srcElement.value;
						break;
					default:
						break;
				}
				
				prevControlValue = (e.srcElement.value);
				
			
			}
		
		</script>
	</head>
	<body class="formbody" onload="AddWindowToPCSWindowCookie(window.name);" onUnload="RemoveWindowFromPCSWindowCookie(window.name);">
		<form id="Form1" method="post" runat="server">
			<table id="PageLayout" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr>
								<td width="600"><asp:imagebutton id="imgPrime" runat="server" AlternateText="Home" ImageUrl="images/compliance-manager.gif"
										width="810px"></asp:imagebutton></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordMessage" runat="server" width="810px" cssClass="RecordMessage">Record Message</asp:label></td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" Width="616px" Font-Size="X-Small" Height="24px"></asp:validationsummary></td>
				</tr>
				<tr>
					<td><asp:label id="lblErrorMessage" runat="server" Width="336px" Font-Size="X-Small" EnableViewState="False"
							Visible="False" ForeColor="Red">Error Message</asp:label></td>
				</tr>
				<tr>
					<td>
						<table id="Table2" style="WIDTH: 810px; HEIGHT: 40px" cellspacing="0" cellpadding="0" width="810"
							border="0">
							<tr align="center">
								<td align="right"><asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder></td>
								<td align="left"><uc1:ucrecordscrollbar id="RecordScrollBar" runat="server"></uc1:ucrecordscrollbar></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><uc1:uctabcontrol id="MainTabControl" runat="server"></uc1:uctabcontrol></td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordHeader" runat="server" width="810px" CSSClass="paneltitle"></asp:label></td>
				</tr>
			</table>
			<br />
			<asp:panel id="Panel1" runat="server" Width="1104px" Height="176px" Cssclass="Panel" BorderWidth="1px"
				BorderStyle="None">
				<table id="Table41" cellspacing="0" cellpadding="0" width="808px" border="0">
					<tr>
						<td>
							<asp:LinkButton id="lnkCustomerId"  Width="150px" CausesValidation="False" runat="server" CssClass="Label">Customer Id:</asp:LinkButton></td>
						<td>
							<asp:textbox id="txtCode" Width="200px" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label1" Width="150px" runat="server" CssClass="LABEL">Customer Name:</asp:label></td>
						<td>
							<asp:textbox id="txtCustomerName"  Width="200px" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:LinkButton id="lnkAcceptanceModel" Width="150px" CausesValidation="False" runat="server" CssClass="Label">Acceptance Model:</asp:LinkButton></td>
						<td>
							<asp:textbox id="txtAcceptanceModel" Width="200px" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:LinkButton id="lnkCDDModel" Width="150px" CausesValidation="False" runat="server" CssClass="Label">CDD Model:</asp:LinkButton></td>
						<td>
							<asp:textbox id="txtCDDModel" Width="200px" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:LinkButton id="lnkCustomerType" Width="150px" CausesValidation="False" runat="server" CssClass="Label">Customer Type:</asp:LinkButton></td>
						<td>
							<asp:textbox id="txtCustomerType" Width="200px" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:LinkButton id="lnkRiskClass" Width="150px" CausesValidation="False" runat="server" CssClass="Label">Risk Class:</asp:LinkButton></td>
						<td>
							<asp:textbox id="txtRiskClass" Width="200px" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>	
					<tr>
						<td>
							<asp:label id="Label4" runat="server" Width="150px" CssClass="LABEL">Overall Status:</asp:label></td>
						<td>
							<asp:textbox id="txtOverallStatus" Width="200px" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							&nbsp;</td>
						<td>
							&nbsp;</td>
					</tr>		
				</table>
				<br />
				<table>
					<tr>
						<td>
							<asp:label id="lblItemDetails" runat="server" width="804px" CSSClass="paneltitle"></asp:label></td>
					</tr>
				</table>
				<table id="Table1" style="WIDTH: 872px; HEIGHT: 115px" cellspacing="0" cellpadding="0"
					width="872" border="0">
					<tr>
						<td style="WIDTH: 424px" valign="top">
							<table id="Table3" style="WIDTH: 400px; HEIGHT: 72px" cellspacing="0" cellpadding="0" width="400"
								border="0">
								<tr>
									<td>
										<asp:label id="Label31" runat="server" Width="104px" CssClass="LABEL">Completed By:</asp:label></td>
									<td>
										<asp:textbox id="txtCompletedBy" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
									<td>
										<asp:label id="Label61" runat="server" Width="60px" CssClass="LABEL">Date:</asp:label></td>
									<td>
										<asp:textbox id="txtCompletedDate" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label7" runat="server" Width="104px" CssClass="LABEL">2nd. Check By:</asp:label></td>
									<td>
										<asp:textbox id="txtSecondCheckedBy" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
									<td>
										<asp:label id="Label8" runat="server" Width="60px" CssClass="LABEL">Date:</asp:label></td>
									<td>
										<asp:textbox id="txtSecondCheckedDate" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label10" runat="server" Width="104px" CssClass="LABEL">Approved By:</asp:label></td>
									<td>
										<asp:textbox id="txtApprovedBy" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
									<td>
										<asp:label id="Label9" runat="server" Width="60px" CssClass="LABEL">Date:</asp:label></td>
									<td>
										<asp:textbox id="txtApprovedDate" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td>
										<asp:label id="Label11" runat="server" Width="60px" CssClass="LABEL">Created:</asp:label></td>
									<td>
										<asp:textbox id="txtCreateDate" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td></td>
									<td align="right" colspan="2"></td>
									<td></td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table id="Table4" cellspacing="0" cellpadding="0" width="300" border="0">
								<tr>
									<td valign="top" align="right">
										<asp:label id="Label12" runat="server" Width="50px" CssClass="LABEL">Note:</asp:label></td>
									<td colspan="3">
										<asp:textbox id="txtNotes" runat="server" Height="68px" Width="308px" CssClass="TEXTBOX" onchange="ControlChanged();"
											TextMode="MultiLine"></asp:textbox></td>
								</tr>
								<tr>
									<td valign="top" align="right">
										<asp:label id="Label13" runat="server" Width="50px" CssClass="LABEL">Document:</asp:label></td>
									<td colspan="3">
										<asp:textbox id="txtDocument" onclick="ControlClicked();" runat="server" Width="56px" CssClass="TEXTBOX"
											onchange="ControlChanged();"></asp:textbox>&nbsp;
										<asp:HyperLink id="lnkViewDocument" onclick="PopupWindow(2);" runat="server" ForeColor="Blue" CssClass="lblgridcell"
											Font-Underline="True">View</asp:HyperLink>&nbsp;
										<asp:HyperLink id="lnkAddDocument" onclick="PopupWindow(1);" runat="server" ForeColor="Blue" CssClass="lblgridcell"
											Font-Underline="True">Add New</asp:HyperLink>&nbsp;
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div style="OVERFLOW: auto; WIDTH: 1080px; HEIGHT: 300px">
					<asp:DataGrid id="dgItemDetails" runat="server" Width="808px" BorderStyle="None" BorderWidth="1px"
						BorderColor="#CC9966" AutoGenerateColumns="False" cellpadding="2" BackColor="White">
						<FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
						<SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
						<ItemStyle Font-Size="11px" BackColor="White"></ItemStyle>
						<HeaderStyle Font-Size="11px" Font-Bold="True" ForeColor="White" BackColor="#990000" HorizontalAlign="center"></HeaderStyle>
						<Columns>
							<asp:TemplateColumn HeaderText="Id">
								<HeaderStyle Width="25px"></HeaderStyle>
								<ItemTemplate>
									<asp:LinkButton id="lnkbtnItemId" OnClick="ItemChanged" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Id").ToString())%>' runat="server" CssClass="lblgridcell">
									</asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Names" ItemStyle-Wrap="False">
								<HeaderStyle Width="210px"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id="Name" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Name").ToString())%>' />
								</ItemTemplate>
							</asp:TemplateColumn>							
							<asp:TemplateColumn HeaderText="Item Type">
								<HeaderStyle Width="100px"></HeaderStyle>
								<ItemTemplate>
									<asp:Label  ReadOnly="True"  id="txtMapTableName" cssclass="TextBoxWithNoBorder" Width="70px" onclick="ControlClicked();" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.MapTableName").ToString())%>'>
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Model">
								<HeaderStyle Width="100px"></HeaderStyle>
								<ItemTemplate>
									<asp:Label  ReadOnly="True"  id="txtModel" cssclass="TextBoxWithNoBorder" Width="100px" onclick="ControlClicked();" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CLTCode").ToString())%>'>
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Record ID">
								<HeaderStyle Width="100px"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id="txtGroupID" ReadOnly="True" cssclass="TextBoxWithNoBorder" Width="80px" onclick="ControlClicked();" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GroupID") %>'>
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Value">
								<HeaderStyle Width="100px"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id="txtValue" ReadOnly="True" cssclass="TextBoxWithNoBorder" Width="80px" onclick="ControlClicked();" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Value") %>'>
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>							
							<asp:TemplateColumn HeaderText="Resp. Person">
								<HeaderStyle Width="100px"></HeaderStyle>
								<ItemTemplate>
									<asp:TextBox id="txtResponsiblePerson" cssclass="TEXTBOX" Width="100px" onchange="ControlChanged();" onclick="ControlClicked();" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ResponsiblePerson")%>'>
									</asp:TextBox>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Create Date">
								<HeaderStyle Width="70px"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id="txtCreateDate" Width="70px" cssclass="TextBoxWithNoBorder" onchange="ControlChanged();" onclick="ControlClicked();" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CreateDate").ToString())%>'>
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Review Due Date">
								<HeaderStyle Width="70px"></HeaderStyle>
								<ItemTemplate>
									<asp:TextBox id="txtDueDate" Width="70px" cssclass="TEXTBOX" onchange="ControlChanged();" onclick="ControlClicked();" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DueDate").ToString()%>'>
									</asp:TextBox>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="IDV Status">
								<HeaderStyle Width="40px"></HeaderStyle>
								<ItemTemplate>
									<a id="lnkIDVRequest" href='javascript:<%# 
															GetPopupScript("IDVRequest.aspx?RecordID=" & Microsoft.Security.Application.Encoder.UrlEncode(DataBinder.Eval(Container, "DataItem.IDVRequestNo").ToString()) & _
															"&ActionID=2&RowNum=0&NoScroll=1", "scrollbars=no,resizable=yes,width=880,height=600") 
															%>'>
										<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.IDVStatus").ToString())%>
									</a>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Decision">
								<HeaderStyle Width="110px"></HeaderStyle>
								<ItemTemplate>
									<asp:DropDownList id="ddlDecision" Width="110px" onchange="ControlChanged();" onclick="ControlClicked();" SelectedIndex='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Decision").ToString())%>' runat="server">
										<asp:ListItem Value="0"> Undetermined </asp:ListItem>
										<asp:ListItem Value="1"> Satisfactory </asp:ListItem>
										<asp:ListItem Value="2"> Unsatisfactory </asp:ListItem>
										<asp:ListItem Value="3"> Needs Review </asp:ListItem>
										<asp:ListItem Value="4"> Waived </asp:ListItem>
									</asp:DropDownList>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Complete">
								<HeaderStyle Width="25px"></HeaderStyle>
								<ItemTemplate>
									<asp:CheckBox ID ="chkComplete" onclick="ControlClicked();" runat="server" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Complete").ToString())%>'>
									</asp:CheckBox>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="2nd.Check">
								<HeaderStyle Width="25px"></HeaderStyle>
								<ItemTemplate>
									<asp:CheckBox ID ="chkSecondCheck" onclick="ControlClicked();" runat="server" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheck").ToString())%>'>
									</asp:CheckBox>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Approve">
								<HeaderStyle Width="25px"></HeaderStyle>
								<ItemTemplate>
									<asp:CheckBox ID ="chkApprove" onclick="ControlClicked();" runat="server" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Approve").ToString())%>'>
									</asp:CheckBox>
									<input id="hdfldCompletedBy"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CompletedBy").ToString())%>' name="hdfldCompletedBy"/>
									<input id="hdfldSecondCheckedBy"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheckBy").ToString())%>' name="Hidden1"/>
									<input id="hdfldApprovedBy"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ApprovedBy").ToString())%>' name="Hidden2"/>
									<input id="hdfldCompletedDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CompletedDate").ToString())%>' name="Hidden3"/>
									<input id="hdfldSecondCheckedDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheckDate").ToString())%>' name="Hidden4"/>
									<input id="hdfldApprovedDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ApprovedDate").ToString())%>' name="Hidden5"/>
									<input id="hdfldCreateDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CreateDate").ToString())%>' name="Hidden6"/>
									<input id="hdfldNotes"  runat="server" type="hidden" value='<%# DataBinder.Eval(Container, "DataItem.Notes")%>' name="Hidden7"/>
									<input id="hdfldDocument"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.DocumentNo").ToString())%>' name="Hidden8"/>
									<input id="hdfldStatus"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Status").ToString())%>' name="Hidden8"/>
									<input id="hdfldCLICode"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CLICode").ToString())%>' name="Hidden8"/>
									<!--<INPUT id="hdfldCLTCode"  runat="server" type="hidden" value='<%# DataBinder.Eval(Container, "DataItem.CLTCode")%>' name="Hidden8"/>-->
									<input id="hdfldCompleteRight"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CompleteRight").ToString())%>' name="Hidden8"/>
									<input id="hdfldApproveRight"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ApproveRight").ToString())%>' name="Hidden8"/>
									<input id="hdfldSecondCheckRight"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheckRight").ToString())%>' name="Hidden8"/>
									<input id="hdfldRequired"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Required").ToString())%>' name="Hidden8"/>
									<input id="hdfldApproveRequired"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ApproveRequired").ToString())%>' name="Hidden8"/>
									<input id="hdfldSecondCheckRequired"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheckRequired").ToString())%>' name="Hidden8"/>
									<input id="hdfldItemName"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Name").ToString())%>' name="Hidden8"/>
									<input id="hdfldAllowedDaysForDueDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.AllowedDaysForDueDate").ToString())%>' name="hdfldAllowedDaysForDueDate"/>
									<input id="hdfldOriginalDueDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.OriginalDueDate").ToString())%>' name="hdfldOriginalDueDate"/>
									<input id="hdfldIDVRequestNo"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.IDVRequestNo").ToString())%>' name="hdfldIDVRequestNo"/>
									<input id="hdfldAutoProcType"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.AutoProcType").ToString())%>' name="hdfldAutoProcType"/>
									<input id="hdfldCanComplete"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CanComplete").ToString())%>' name="hdfldCanComplete"/>
									<input id="hdfldCanSecondCheck"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CanSecondCheck").ToString())%>' name="hdfldCanSecondCheck"/>
									<input id="hdfldCanApprove"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CanApprove").ToString())%>' name="hdfldCanApprove"/>
									<input id="hdfldMapTableName"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.MapTableName").ToString())%>' name="hdfldMapTableName"/>
									<input id="hdfldGroupID"  runat="server" type="hidden" value='<%# DataBinder.Eval(Container, "DataItem.GroupID")%>' name="hdfldGroupID"/>
									<input id="hdfldValue"  runat="server" type="hidden" value='<%# DataBinder.Eval(Container, "DataItem.Value")%>' name="hdfldValue"/>
								</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
					</asp:DataGrid>
				</div>
				<input id="hdfldSelectedItem" type="hidden" name="hdfldSelectedItem" runat="server"/>
				<input id="hdfldOperator" type="hidden" name="hdfldOperator" runat="server"/> <input id="hdfldValidDocument" type="hidden" value="1" name="hdfldValidDocument" runat="server"/>
				<input id="hdfldAcceptanceModelId" type="hidden" name="hdfldAcceptanceModelId" runat="server"/>
			</asp:panel>
			<asp:panel id="Panel2" runat="server" Height="15px" Width="712px" cssclass="Panel">
				<table>
					<tr>
						<td>
							<asp:label id="Label51" runat="server" CssClass="LABEL">Owner Branch:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerBranch" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label52" runat="server" CssClass="LABEL">Owner Department:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerDept" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label53" runat="server" CssClass="LABEL">Owner Operator:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerOper" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
				</table>
			</asp:panel>
			<asp:panel id="Panel3" runat="server" Width="712px" Height="15px" cssclass="Panel">
			    <uc1:ucSupportingInfo id="supportingInfo" runat="server"></uc1:ucSupportingInfo>
			</asp:panel>
			<asp:panel id="Panel4" runat="server" Width="712px" Height="15px" cssclass="Panel"></asp:panel><asp:panel id="cPanelValidators" runat="server" Width="864px" Height="46px"></asp:panel></form>
	</body>
</html>
