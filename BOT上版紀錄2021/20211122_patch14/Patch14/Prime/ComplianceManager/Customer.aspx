<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Customer.aspx.vb" Inherits="CMBrowser.Customer" validateRequest="false" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="GoWeb" Namespace="Northwoods.GoWeb" Assembly="Northwoods.GoWeb" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucActivityList" Src="ucActivityList.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucSupportingInfo" Src="ucSupportingInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucItemSelector" Src="ucItemSelector.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNote" Src="ucNote.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//Dtd HTML 4.0 transitional//EN">
<html>
	<head>
		<title>Customer</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="Styles.css" type="text/css" rel="stylesheet"/>
		<style type="text/css">.tgridheader { BORDER-RIGHT: white outset; PADDING-RIGHT: 0px; BORDER-TOP: white outset; PADDING-LEFT: 0px; FONT-WEIGHT: bold; FONT-SIZE: 12px; PADDING-BOTTOM: 2px; BORDER-LEFT: white outset; COLOR: white; PADDING-TOP: 2px; BORDER-BOTTOM: white outset; BACKGROUND-COLOR: #990000; TEXT-ALIGN: left }
	.lblgridcell { FONT-SIZE: 11px; Font-Names: Verdana, Arial, Helvetica, sans-serif }
	.grdHeader { FONT-WEIGHT: bold; FONT-SIZE: 11px; Font-Names: Verdana, Arial, Helvetica, sans-serif }
		</style>
		<script language="JavaScript" src="js/nav.js" type="text/javascript"></script>
		<script language="JavaScript" src="js/Customer.js" type="text/javascript"></script>
		<script language="JavaScript" src="js/GoWeb.js" type="text/javascript"></script>
		<script language="JavaScript" type="text/javascript">
		    //Result of Negative Press Search Character count script
             function JStextCount(text, maxlimit) {

            if (text.value.length < maxlimit)
                return true;
            else {
                if ((event.keyCode >= 37 && event.keyCode <= 40) || (event.keyCode == 8))
                    event.returnValue = true;

                else
                    event.returnValue = false;
            }
        }
			function LocaleDate(ctrlID)
			    {
			        var ctrlName=ctrlID;
			        var msg;
			            
			            switch(ctrlName)
			                {
			                    case "IDcell5" :
			                    msg="* ID Issue date should be in proper format, Invalid date value. <br>";
			                    break;
			                    case "IDcell6" :
			                    msg="* ID Expiry date should be in proper format, Invalid date value. <br>";
			                    break;
			                }
			                
			                try
			                    {
			                        var varDate=document.forms[0].item(ctrlName).value;
			                        var varIDcell=Date.parseLocale(varDate, "d");
			                        if (varIDcell==null || varIDcell=="")
			                            {
			                            document.getElementById("IDWarning").innerHTML=msg;
			                            }
			                        else
			                            {
			                             var varIDcell=String.localeFormat("{0:d}", varIDcell);
			                             document.forms[0].item(ctrlName).value=varIDcell;
			                             document.getElementById("IDWarning").innerHTML="";
			                            }
			                    }
			                    catch(e)
			                    {
			                    
			                    }
			    }
 
			function goDblClick(e, id) {
				
				goInfo = goFindInfoAt(goDX, goDY, id);
				var newQueryString;
				var pageName = new String("");
				var pageName2;
				var param;
				var partyId;
				var relatedPartyId;
				var relationship;
				
				if (goInfo == null) return;
				pageName = goInfo.PageName;				
				
				if (pageName == "Relationship") {
					//pagename is "Relationship"
					param=getwindowparam(4);
					partyId = goInfo.PartyId;
					relatedPartyId =  goInfo.RelatedPartyId;
					relationship = goInfo.Relationship;					
					if (goInfo.MultiRelations == "1") {
						//pagename is "PartyRelations" and pageName2 is empty
						pageName = "PartyRelations";							
					}	
					// passed the pagename as a parameter in the framed querystring 
					newQueryString = pageName + '.aspx?PartyId=' + partyId + '&RelatedPartyId=' + 
							relatedPartyId + '&relationship=' + relationship + '&ActionID=2&RowNum=0&NoScroll=1';								
					var nw=window.open(newQueryString, pageName, param);
				}
				else  {
					//pagename will hold the current page name
					pageName2= filterNum(pageName +  String( goInfo.PartyID));					
					param=getwindowparam(1);
					newQueryString = pageName + '.aspx?RecordId=' + goInfo.PartyID + '&ActionID=2&RowNum=0&NoScroll=1';								
					var nw=window.open(newQueryString, pageName, param);
					}
			
				return;
				if (goSt == 0) return;
				goSt = 0;
				if (goTm) clearTimeout(goTm);
				goImg = goFindImg(id);
				goUX = goMouseX(e);
				goUY = goMouseY(e);
				goVX = e.clientX;
				goVY = e.clientY;
				if (goMouseHasMoved()) {
					if (!goNoClick(0, id)) return;
				} else {
					if (!goDoClick(0, 2, id)) return;
				}
				goPost(id, goQuery(e) + '&dblclick=1');
			}
			
			function CheckProbationFields(sender, args)
			{
				args.isvalid = false;
			}
			
			function Save_Click(sender, args)
			{
				args.isvalid = false;
			}
			function ChangeDate()
			{
			    try
			    {	
				    var CurrDate = new Date();
				    var strRiskClassValue, strNameValuePair;
				    var dtEndDate, intProbationDays;
				    strNameValuePair = document.forms[0].item("hdnRiskClass").value;
				    strRiskClassValue = document.forms[0].item("ddlRiskClass").value;
				    intProbationDays = getProbationDays(strNameValuePair, strRiskClassValue);
				    dtEndDate = DateAdd("d", intProbationDays, CurrDate);
				    document.forms[0].item("txtProbationEndDate").value = parseInt(dtEndDate.getMonth()+ 1 ) + '/' + dtEndDate.getdate() + '/' + dtEndDate.getFullYear();
				}
				catch(ex){}
			}
			function getProbationDays(xmltext, code)
			{
				if (code.length == 0) return 0;
				var startindex = xmltext.indexOf(code) + code.length + 1;
				var endindex = xmltext.indexOf(';', startindex);
				return parseInt(xmltext.substring(startindex,endindex));
			}
		function DateAdd(timeU,byMany,dateObj) {
			var millisecond=1;
			var second=millisecond*1000;
			var minute=second*60;
			var hour=minute*60;
			var day=hour*24;
			var year=day*365;

			var newDate;
			var dVal=dateObj.valueOf();
			switch(timeU) {
				case "ms": newDate=new Date(dVal+millisecond*byMany); break;
				case "s": newDate=new Date(dVal+second*byMany); break;
				case "mi": newDate=new Date(dVal+minute*byMany); break;
				case "h": newDate=new Date(dVal+hour*byMany); break;
				case "d": newDate=new Date(dVal+day*byMany); break;
				case "y": newDate=new Date(dVal+year*byMany); break;
			}
			return newDate;
		}
		function PopupWindow(mode) {
				var qstring;
				
				switch(mode) {
				case 1 : //Add
					qstring = "Document.aspx?ActionID=1&RowNum=0&NoScroll=1"
					break;
				case 2 : //Display
					if ( document.getElementById("txtdocument").value == "" ) 
						return;
					qstring =  "Document.aspx?"+ "RecordID="+ document.getElementById("txtdocument").value + 
							 "&ActionID=2&RowNum=0&NoScroll=1"
					break;
				default:
					break;
					}
				
				
				var nw=window.open(qstring,"Document", "scrollbars=yes,resizable=yes,width=880,height=600");	
				nw.focus();	
		
			}
			
 
	function AddMandatoryStarOnLabel (objID)
	{
		RemoveMandatoryStarOnLabel(objID)
		var str=document.getElementById(objID).innerHTML; 
		str ='<font class="MandatoryStar">*&nbsp;</font>' + str;
		document.getElementById(objID).innerHTML = str;	
	}

	function RemoveMandatoryStarOnLabel (objID)
	{
		var str=document.getElementById(objID).innerHTML; 
		document.getElementById(objID).innerHTML = str.replace ('<FONT class=MandatoryStar>*&nbsp;</FONT>', '');
	}

	function SetValidation (status, controlToValidate)
	{
		for(var i = 0; i < Page_Validators.length; i++)
		{
			if(Page_Validators[i].controltovalidate != null && Page_Validators[i].controltovalidate == controlToValidate)
			{
				Page_Validators[i].enabled = status; 
			}			
		}
	}
			
		</script>

        <link href="Styles.css" rel="stylesheet" type="text/css" />        
        <link href="Styles.css" rel="stylesheet" type="text/css" />

	</head>
	<body class="formbody" onload="AddWindowToPCSWindowCookie(window.name);" onUnload="RemoveWindowFromPCSWindowCookie(window.name);">
		<form id="Form1" method="post" runat="server">
            <asp:ScriptManager id="smnCustomer" runat="server" EnableScriptGlobalization="True">
                        </asp:ScriptManager>
			<table id="PageLayout" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr>
								<td width="600"><asp:imagebutton id="imgPrime" runat="server" ImageUrl="images/compliance-manager.gif" AlternateText="Home"
										width="804px"></asp:imagebutton></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordMessage" runat="server" width="804px" cssClass="RecordMessage">Record Message</asp:label></td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" Width="616px" Font-size="X-Small" Height="24px"></asp:validationsummary></td>
				</tr>
				<tr>
					<td>
						<table id="table2" style="WIDTH: 804px; HEIGHT: 40px" cellspacing="0" cellpadding="0" width="816"
							border="0">
							<tr align="center">
								<td align="right" width="60%"><asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder></td>
								<td align="left" width="40%"><uc1:ucrecordscrollbar id="RecordScrollBar" runat="server"></uc1:ucrecordscrollbar></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><uc1:uctabcontrol id="MainTabControl" runat="server"></uc1:uctabcontrol>
</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordHeader" runat="server" width="804px" CSSClass="paneltitle"></asp:label></td>
				</tr>
			</table>
			<asp:panel id="Panel1" runat="server" Width="804px" Height="200px" Cssclass="Panel" BorderWidth="1px"
				BorderStyle="None">
				<table id="table40" cellspacing="0" cellpadding="0" width="750" border="0">
					<tr>
						<td width="144">
							<asp:label id="Label1" runat="server" Width="144px" CssClass="LABEL">Id:</asp:label></td>
						<td width="175">
							<asp:textbox id="txtCode" runat="server" Width="175px" CssClass="TEXTBOX" MaxLength="35"></asp:textbox></td>
						<td align="right" width="75">
							<asp:label id="Label8" runat="server" Width="75px" CssClass="LABEL">Name:</asp:label></td>
						<td width="398" colspan="3">
							<asp:textbox id="txtName" runat="server" Width="398px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td style="HEIGHT: 20px">
							<asp:LinkButton id="lnkRiskClass" CssClass="Label" runat="server" CausesValidation="False">Risk Class:</asp:LinkButton>
							<asp:label id="lblRiskClass" runat="server" Width="144px" CssClass="LABEL" Visible="False">Risk Class:</asp:label></td>
						<td style="HEIGHT: 20px">
							<asp:DropDownList id="ddlRiskClass" runat="server" Width="175px" CssClass="DropDownList"></asp:DropDownList></td>
						<td style="HEIGHT: 20px" align="right">
							<asp:label id="Label9" runat="server" Width="75px" CssClass="LABEL">Address:</asp:label></td>
						<td style="HEIGHT: 20px" colspan="3">
							<asp:textbox id="txtAddress" runat="server" Width="398" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label3" runat="server" CssClass="LABEL">Customer Type:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlCustomerType" runat="server" Width="175px" CssClass="DropDownList"></asp:DropDownList></td>
						<td align="right">
							<asp:label id="Label10" runat="server" Width="75px" CssClass="LABEL">City:</asp:label></td>
						<td style="WIDTH: 219px">
							<asp:textbox id="txtCity" runat="server" Width="150px" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="WIDTH: 95px" align="right">
							<asp:label id="Label11" runat="server" Width="75px" CssClass="LABEL">State:</asp:label></td>
						<td>
							<asp:textbox id="txtState" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label46" runat="server" CssClass="LABEL">Individual/Business:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlIndOrBus" runat="server" Width="175px" CssClass="DropDownList"></asp:DropDownList></td>
						<td align="right">
							<asp:label id="Label12" runat="server" Width="75px" CssClass="LABEL">Zip:</asp:label></td>
						<td style="WIDTH: 219px">
							<asp:textbox id="txtZip" runat="server" Width="150px" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="WIDTH: 95px" align="right">
							<asp:label id="Label13" runat="server" Width="75px" CssClass="LABEL">Country:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlCountry" runat="server" Width="160px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label4" runat="server" CssClass="LABEL">Type of Business:</asp:label></td>
						<td>
							<asp:textbox id="txtTypeofBusiness" runat="server" Width="175px" CssClass="TEXTBOX"></asp:textbox></td>
						<td align="right">
							<asp:label id="Label14" runat="server" Width="75px" CssClass="LABEL">Phone:</asp:label></td>
						<td style="WIDTH: 219px">
							<asp:textbox id="txtCustomerTelephone" runat="server" Width="150px" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="WIDTH: 95px" align="right">
							<asp:label id="Label15" runat="server" Width="75px" CssClass="LABEL">Email:</asp:label></td>
						<td>
							<asp:textbox id="txtCustomerEmail" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label47" runat="server" CssClass="LABEL">DBA:</asp:label></td>
						<td>
							<asp:textbox id="txtdBA" runat="server" Width="175px" CssClass="TEXTBOX"></asp:textbox></td>
						<td align="right">
							<asp:label id="Label16" runat="server" Width="75px" CssClass="LABEL">Sex:</asp:label></td>
						<td style="WIDTH: 219px">
							<asp:DropDownList id="ddlSex" runat="server" Width="150px" CssClass="DropDownList"></asp:DropDownList></td>
						<td style="WIDTH: 95px" align="right">
							<asp:label id="Label17" runat="server" Width="75px" CssClass="LABEL">DOB:</asp:label></td>
						<td>
							<asp:textbox id="txtdateOfBirth" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label5" runat="server" CssClass="LABEL">Annual Income:</asp:label></td>
						<td>
							<asp:textbox id="txtAnnualIncome" runat="server" Width="175px" CssClass="TEXTBOX"></asp:textbox></td>
						<td align="right">
							<asp:label id="Label50" runat="server" Width="75px" CssClass="LABEL">FEP:</asp:label></td>
						<td style="WIDTH: 219px" align="left">
							<asp:CheckBox id="chkFEP" runat="server" Width="80px" CssClass="CheckBox"></asp:CheckBox></td>
						<td style="WIDTH: 95px" align="right">
							<asp:label id="Label26" runat="server" Width="75px" CssClass="LABEL">SSN/TIN:</asp:label></td>
						<td>
							 <asp:textbox id="txtSSNOrTIN" onclick ="DisplayWarning()"  runat="server" Width="160px" CssClass="TEXTBOX">
							</asp:textbox>
							<input id="HdnDSPNPIDATA" type="hidden" name="HdnDSPNPIDATA" runat="server" />
							</td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label6" runat="server" CssClass="LABEL">Source of Funds:</asp:label></td>
						<td>
							<asp:textbox id="txtSourceOfFunds" runat="server" Width="175px" CssClass="TEXTBOX"></asp:textbox></td>
						<td align="right">
							<asp:label id="Label49" runat="server" Width="75px" CssClass="LABEL">PEP:</asp:label></td>
						<td style="WIDTH: 219px" align="left">
							<asp:CheckBox id="chkPEP" runat="server" CssClass="CheckBox"></asp:CheckBox></td>
						<td style="WIDTH: 95px" align="center">&nbsp;&nbsp;&nbsp;
							<asp:label id="Label7" runat="server" Font-size="11px" Width="57" Font-Bold="true">Political  Position:</asp:label></td>
						<td>
							<asp:textbox id="txtPoliticalPosition" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td style="HEIGHT: 27px">
							<asp:label id="Label2" runat="server" CssClass="LABEL">Status:</asp:label></td>
						<td style="HEIGHT: 27px">
							<asp:DropDownList id="ddlStatus" runat="server" Width="175px" CssClass="DropDownList"></asp:DropDownList></td>
						<td style="HEIGHT: 27px" align="right">
							<asp:label id="Label54" runat="server" Width="75px" CssClass="LABEL">Probation:</asp:label></td>
						<td style="WIDTH: 219px; HEIGHT: 27px" align="left">
							<asp:CheckBox id="chkOnProbation" runat="server" Width="80px" CssClass="CheckBox"></asp:CheckBox></td>
						<td style="WIDTH: 95px; HEIGHT: 27px" align="right">
							<asp:label id="Label72" runat="server" Font-size="11px" Width="79px" Font-Bold="true">Reason:</asp:label></td>
						<td style="HEIGHT: 27px">
							<asp:DropDownList id="ddlProbationReasons" runat="server" Width="160px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td style="HEIGHT: 25px">
							<asp:label id="Label56" runat="server" CssClass="LABEL">Open Date:</asp:label></td>
						<td style="HEIGHT: 25px">
							<asp:textbox id="txtOpenDate" runat="server" Width="175px" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="HEIGHT: 25px" align="right">
							<asp:label id="Label57" runat="server" Width="75px" CssClass="LABEL">Start date:</asp:label></td>
						<td style="WIDTH: 219px; HEIGHT: 25px" align="left">
							<asp:textbox id="txtProbationStartdate" runat="server" Width="150px" CssClass="TEXTBOX" BackColor="LightGray"></asp:textbox>
							</td>
						<td style="WIDTH: 95px; HEIGHT: 25px" align="right">
                            <asp:label id="Label71" runat="server" Width="75px" CssClass="LABEL">End date:</asp:label></td>
						<td style="HEIGHT: 25px">
							<asp:textbox id="txtProbationEndDate" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td style="HEIGHT: 25px">
							<asp:label id="Label155" runat="server" CssClass="LABEL">OnBoard Status:</asp:label></td>
						<td style="HEIGHT: 25px">
							<asp:DropDownList id="ddlCDDStatus" runat="server" Width="175px" CssClass="DropDownList"></asp:DropDownList></td>
						<td style="HEIGHT: 25px" align="right">
							<asp:label id="Label156" runat="server" Width="75px" CssClass="LABEL">Create date:</asp:label></td>
						<td style="WIDTH: 219px; HEIGHT: 25px" align="left">
							<asp:textbox id="txtCDDCreateDate" runat="server" Width="150px" CssClass="TEXTBOX" BackColor="LightGray"></asp:textbox></td>
						<td style="WIDTH: 95px; HEIGHT: 25px" align="right">
							<asp:label id="Label157" runat="server" Width="75px" CssClass="LABEL">Modify date:</asp:label></td>
						<td style="HEIGHT: 25px">
							<asp:textbox id="txtCDDModifydate" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td colspan="6">
							<table id="table11" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td style="HEIGHT: 7px"></td>
									<td style="HEIGHT: 7px"></td>
									<td style="HEIGHT: 7px"></td>
									<td style="HEIGHT: 7px"></td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table width="804">
					<tr>
						<td style="width: 87px; height: 14px" align="left" colspan="4">
							<asp:label id="Label24" runat="server" width="804px" CSSClass="paneltitle">
						Accounts with primary relationship</asp:label></td>
						</tr>
				</table>
				<table style="WIDTH: 804px; HEIGHT: 48px" cellpadding="0" cellspacing="0">
					<tr valign="top">
						<td colspan="4">
							<uc1:ucView id="vwCustomerAccounts" runat="server"></uc1:ucView></td>
					</tr>
				</table>
			</asp:panel><asp:panel id="Panel2" runat="server" width="712px" Height="46px" cssclass="Panel">&nbsp; 
<table id="table5" cellspacing="0" cellpadding="0" width="300" border="0">
					<tr>
						<td>
							<asp:label id="Label20" runat="server" Width="144px" CssClass="LABEL">Parent ID:</asp:label></td>
						<td>
							<asp:textbox id="txtParentId" runat="server" CssClass="TEXTBOX" AutoPostBack="true"></asp:textbox></td>
						<td>
							<asp:label id="Label30" runat="server" Width="144px" CssClass="LABEL">Industry Code:</asp:label></td>
						<td>
							<asp:textbox id="txtSICCode" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label48" runat="server" Width="144px" CssClass="LABEL">Parent Name:</asp:label></td>
						<td>
							<asp:textbox id="txtParentName" runat="server" CssClass="TEXTBOX" BackColor="LightGray" ReadOnly="true"></asp:textbox></td>
						<td>
							<asp:label id="Label23" runat="server" Width="144px" CssClass="LABEL">Passport No:</asp:label></td>
						<td>
							<asp:textbox id="txtPassportNo" onclick ="DisplayWarning()" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label18" runat="server" Width="144px" CssClass="LABEL">Account Officer:</asp:label></td>
						<td>
							<asp:textbox id="txtAccountOfficer" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label27" runat="server" Width="144px" CssClass="LABEL">Asset Size:</asp:label></td>
						<td>
							<asp:textbox id="txtAssetSize" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label19" runat="server" Width="144px" CssClass="LABEL">Telephone:</asp:label></td>
						<td>
							<asp:textbox id="txtAccountOfficerPhone" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label29" runat="server" Width="144px" CssClass="LABEL">License No:</asp:label></td>
						<td>
							<asp:textbox id="txtLicenseNo" onclick ="DisplayWarning()" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label28" runat="server" Width="144px" CssClass="LABEL">Email:</asp:label></td>
						<td>
							<asp:textbox id="txtAccountOfficerEmail" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label31" runat="server" Width="144px" CssClass="LABEL">License State:</asp:label></td>
						<td>
							<asp:textbox id="txtLicenseState" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label21" runat="server" Width="144px" CssClass="LABEL">Compliance Officer:</asp:label></td>
						<td>
							<asp:textbox id="txtComplianceOfficer" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label70" runat="server" CssClass="Label">Citizen of:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlCountryOfCitizenship" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label22" runat="server" Width="144px" CssClass="LABEL">Telephone:</asp:label></td>
						<td>
							<asp:textbox id="txtComplianceOfficerPhone" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label34" runat="server" Width="145" CssClass="LABEL">Ctr Amount:</asp:label></td>
						<td>
							<asp:textbox id="txtCtrAmount" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label32" runat="server" Width="145" CssClass="LABEL">Email:</asp:label></td>
						<td>
							<asp:textbox id="txtComplianceOfficerEmail" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label25" runat="server" Width="145" CssClass="LABEL">Exemption Status:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlExemptionStatus" runat="server" Width="209" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label40" runat="server" Width="145px" CssClass="Label">US Person:</asp:label></td>
						<td>
							<asp:textbox id="txtUSPerson" runat="server" Width="209" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label75" runat="server" Width="145px" CssClass="LABEL">Open/Closed:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOpenClosed" runat="server" Width="209px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label73" runat="server" Width="145px" CssClass="LABEL">Closed Date:</asp:label></td>
						<td>
							<asp:textbox id="txtClosedDate" runat="server" Width="209px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label69" runat="server" Width="145px" CssClass="LABEL">Closed Reason:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlClosedReason" runat="server" Width="208px"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label94" runat="server" Width="145px" CssClass="LABEL">Country Of Residence:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlCntryOfResidence" runat="server" Width="208px"></asp:DropDownList></td>
					</tr>
				</table><br/>
<hr width="100%" size="1"/>

<table id="table6" style="WIDTH: 712px; HEIGHT: 54px" cellspacing="0" cellpadding="0" width="712"
					border="0">
					<tr>
						<td valign="top" width="50%">
							<table id="table8" cellspacing="0" cellpadding="0" width="350" border="0">
								<tr>
									<td align="right">
										<asp:CheckBox id="chkProspect" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Prospect"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkMSB" runat="server" CssClass="CheckBox" TextAlign="Left" Text="MSB"></asp:CheckBox></td>
								</tr>
								<tr>
								</tr>
								<tr>
									<td align="right">
										<asp:CheckBox id="chkResident" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Resident"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkShellBank" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Shell"></asp:CheckBox></td>
								</tr>
								<tr>
									<td align="right">
										<asp:CheckBox id="chkOffShoreBank" runat="server" CssClass="CheckBox" TextAlign="Left" Text="OffShore Bank"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkPayThroughAccount" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Pay Through Account"></asp:CheckBox></td>
								</tr>
								<tr>
									<td align="right">
										<asp:CheckBox id="chkCorrespBankRelation" runat="server" CssClass="CheckBox" TextAlign="Left"
											Text="Corr. Bank Relation"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkEmbassy" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Embassy"></asp:CheckBox></td>
								</tr>
								<tr>
									<td align="right">
										<asp:CheckBox id="chkHighRiskRespBank" runat="server" CssClass="CheckBox" TextAlign="Left" Text="High Risk Resp Bank"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkForeignGovt" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Foreign Government"></asp:CheckBox></td>
								</tr>
								<tr>
									<td align="right">
										<asp:CheckBox id="chkRegulatedAffiliate" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Regulated Affiliate"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkBearerShares" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Bearer Shares"></asp:CheckBox></td>
								</tr>
								<tr>
									<td align="right">
										<asp:CheckBox id="chkCharityOrganization" runat="server" CssClass="CheckBox" TextAlign="Left"
											Text="Charity Organization"></asp:CheckBox></td>
									<td align="right">
										<asp:CheckBox id="chkNomeDePlume" runat="server" CssClass="CheckBox" TextAlign="Left" Text="Nome De Plume"></asp:CheckBox></td>
								</tr>
							</table>
						</td>
						<td style="width:50%; vertical-align:top;">
							<table id="table7" style="WIDTH: 350px; HEIGHT: 62px" cellspacing="0" cellpadding="0" width="424"
								border="0">
								<tr>
									<td>
										<asp:label id="Label39" runat="server" CssClass="Label">Swift BIC:</asp:label></td>
									<td>
										<asp:textbox id="txtSwiftID" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label33" runat="server" CssClass="Label">Country of Origin:</asp:label></td>
									<td>
										<asp:DropDownList id="ddlCountryOfOrigin" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label35" runat="server" CssClass="Label">Country Of Incorp:</asp:label></td>
									<td>
										<asp:DropDownList id="ddlCountryOfIncorporation" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label36" runat="server" CssClass="Label">OffShore Corp:</asp:label></td>
									<td>
										<asp:DropDownList id="ddlOffShoreCorporation" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label37" runat="server" CssClass="Label">Business Relation:</asp:label></td>
									<td>
										<asp:textbox id="txtBusinessRelation" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label38" runat="server" CssClass="Label">Previous Relation:</asp:label></td>
									<td>
										<asp:textbox id="txtPreviousRelation" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
								</tr>
							</table>
						</td>
					</tr>
				</table>
<hr style="width:100%; size:1;"/>

<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td>
							<asp:label id="Label41" runat="server" CssClass="Label">User Defined 1:</asp:label></td>
						<td>
							<asp:textbox id="txtUserDefined1" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label51" runat="server" CssClass="LABEL">Owner Branch:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerBranch" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label42" runat="server" CssClass="Label">User Defined 2:</asp:label></td>
						<td>
							<asp:textbox id="txtUserDefined2" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label52" runat="server" CssClass="LABEL">Owner Department:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerDept" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label43" runat="server" CssClass="Label">User Defined 3:</asp:label></td>
						<td>
							<asp:textbox id="txtUserDefined3" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label53" runat="server" CssClass="LABEL">Owner Operator:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlOwnerOper" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label44" runat="server" CssClass="Label">User Defined 4:</asp:label></td>
						<td>
							<asp:textbox id="txtUserDefined4" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label45" runat="server" CssClass="Label">User Defined 5:</asp:label></td>
						<td>
							<asp:textbox id="txtUserDefined5" runat="server" Width="208px" CssClass="TEXTBOX"></asp:textbox></td>
						<td></td>
						<td></td>
					</tr>
				</table></asp:panel><asp:panel id="Panel3" runat="server" Width="712px" Height="15px" cssclass="Panel">
				<table id="tblProfiles" style="WIDTH: 856px; HEIGHT: 72px" runat="server">
					<tr>
						<td style="width:15%; vertical-align:top;">
							<table cellspacing="1" cellpadding="1" width="100%" border="1">
								<tr>
									<td>
										<asp:LinkButton id="lbCustomerProfiles" runat="server" CssClass="SupportingInfo">Customer Profiles</asp:LinkButton></td>
								</tr>
								<tr>
									<td>
										<asp:LinkButton id="lbCountryProfiles" runat="server" cssclass="SupportingInfo">Country Profiles</asp:LinkButton></td>
								</tr>
							</table>
						</td>
						<td valign="top" width="85%">
							<asp:placeholder id="phCustomerProfiles" runat="server"></asp:placeholder>
							<uc1:ucItemSelector id="CountrySelector" runat="server"></uc1:ucItemSelector></td>
					</tr>
				</table>
			</asp:panel><asp:panel id="Panel4" runat="server" Width="712px" Height="15px" cssclass="Panel">&nbsp; 
<table cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<asp:Button id="cmdAddRelationship" runat="server" Text="Add Relationship"></asp:Button>&nbsp;&nbsp;
						</td>
						<td>
							<asp:Button id="cmdRefreshRelationships" runat="server" Text="Refresh"></asp:Button></td>
						<td valign="middle">
							<asp:Label id="Label154" runat="server" CssClass="LABEL">Draw Relationships:</asp:Label></td>
						<td>
							<asp:DropDownList id="ddlDrawRelationships" runat="server" Width="150px" AutoPostBack="true">
								<asp:ListItem Value="0">Accounts Only</asp:ListItem>
								<asp:ListItem Value="1" Selected="true">Parties Only</asp:ListItem>
								<asp:ListItem Value="2">Parties and Accounts</asp:ListItem>
								<asp:ListItem Value="3">Party Only - Deleted</asp:ListItem>
							</asp:DropDownList></td>
					</tr>
					<tr>
					    <td></td>
						<td colspan="2"><asp:Label id="lblRelationshipsMsg" 
						runat="server" CssClass="griderror"></asp:Label></td>
					</tr>		
				</table>
<GOWEB:GOVIEW id="MyView" tabIndex="1" runat="Server" Height="688px" Width="888px" NoPost="true"
					ScriptFile="js/GoWeb.js" CssFile="none"></GOWEB:GOVIEW></asp:panel><asp:panel id="Panel5" runat="server" Width="712px" Height="15px" cssclass="Panel">
				<uc1:ucActivityList id="ActivityList" runat="server"></uc1:ucActivityList>
			</asp:panel><asp:panel id="Panel6" runat="server" Width="804px" cssclass="Panel">
			    <table width="804px">
			        <tr>
			            <td style="width:50px">
			                <asp:Label id="Label159" runat="server" CssClass="LABEL" Width="50px">List:</asp:Label>
			            </td>
			            <td width="754px">
			                <asp:DropDownList id="ddlListCases" runat="server" Width="200px" AutoPostBack="true">
								<asp:ListItem Value="0">Current Only</asp:ListItem>
								<asp:ListItem Value="1" >Archived Only</asp:ListItem>
								<asp:ListItem Value="2" Selected="true">Both Current and Archived</asp:ListItem>
							</asp:DropDownList>
			            </td>
			        </tr>
			    </table>
			</asp:panel><asp:panel id="Panel7" runat="server" Width="712px" Height="15px" cssclass="Panel">
				<uc1:ucSupportingInfo id="supportingInfo" runat="server"></uc1:ucSupportingInfo>
			</asp:panel><asp:panel id="Panel8" runat="server" Width="712px" Height="15px" cssclass="Panel">
				<uc1:ucNote id="CustomerNotes" runat="server"></uc1:ucNote>
			</asp:panel><asp:panel id="Panel9" runat="server" Width="804px" Height="15px" cssclass="Panel">
				<table>
					<tr>
						<td>
							<asp:label id="Label60" runat="server" Width="200px" CssClass="LABEL">Enable Risk Analysis:</asp:label></td>
						<td>
							<asp:CheckBox id="chkEnableRiskAnalysis" runat="server" CssClass="CheckBox"></asp:CheckBox></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblRiskModel" runat="server" Width="200px" CssClass="LABEL">Risk Model:</asp:label></td>
						<td>
							<asp:textbox id="txtriskModel" runat="server" CssClass="TextBox"></asp:textbox></td>
						<td></td>
						<td></td>
					</tr>
				</table>
				<br/>
				<table>
					<tr>
						<td class="grdheader" colspan="2">
							<asp:label id="lblLastAccepted" runat="server" Font-size="11px" Width="220px" Font-Bold="true">Last Accepted Score Details</asp:label></td>
					</tr>
					<tr>
						<td>
							<asp:DataGrid id="dgScoreDetails" runat="server" Width="804px" BorderStyle="None" BorderWidth="1px"
								BackColor="White" cellpadding="4" AutoGenerateColumns="False" BorderColor="#CC9966">
								<FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
								<SelectedItemStyle Font-Bold="true" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
								<ItemStyle Font-size="11px" ForeColor="#330099" BackColor="White"></ItemStyle>
								<HeaderStyle Font-size="11px" Font-Bold="true" ForeColor="White" BackColor="#990000"></HeaderStyle>
								<Columns>
						            <asp:TemplateColumn HeaderText="Item" >
										<HeaderStyle Width="150px"></HeaderStyle>
							            <ItemTemplate>
    							            <asp:Label id="RiskItem" runat="server" text='<%#Microsoft.Security.Application.Encoder.HtmlEncode(Databinder.Eval(Container, "DataItem.RiskItem").ToString())%>' />
							            </ItemTemplate>
						            </asp:TemplateColumn>									
									<asp:TemplateColumn HeaderText="Req">
										<HeaderStyle Width="25px"></HeaderStyle>
										<ItemTemplate>
											<asp:CheckBox ID ="chkRequired" runat="server" Enabled="false" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Required").ToString())%>'>
											</asp:CheckBox>
										</ItemTemplate>
									</asp:TemplateColumn>
						            <asp:TemplateColumn HeaderText="Prompt" >
										<HeaderStyle Width="100px"/>
							            <ItemTemplate>
    							            <asp:Label id="Prompt" runat="server" text='<%#Microsoft.Security.Application.Encoder.HtmlEncode(Databinder.Eval(Container, "DataItem.Prompt").ToString())%>' />
							            </ItemTemplate>
						            </asp:TemplateColumn>									
									<asp:TemplateColumn HeaderText="Value">
										<HeaderStyle Width="125px"></HeaderStyle>
										<ItemTemplate>
											<asp:TextBox id="txtValue" cssclass="TextBoxWithNoBorder" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Value")%>'/>
										</ItemTemplate>
									</asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Weight(%)">
										<HeaderStyle HorizontalAlign="Right" Width="75px"/>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Weight" runat="server" Text='<%#Microsoft.Security.Application.Encoder.HtmlEncode(Databinder.Eval(Container, "DataItem.Weight").ToString())%>'/>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Def.Score">
										<HeaderStyle Width="25px"></HeaderStyle>
										<ItemTemplate>
											<asp:CheckBox ID ="chkDefaultScore" runat="server" Enabled="false" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.DefaultScore").ToString())%>'>
											</asp:CheckBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score">
										<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox id="txtScore" style="text-align:right;" cssclass="TextBoxWithNoBorder" AutoPostBack="true" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Score").ToString())%>'>
											</asp:TextBox>
											<input id="hdfldScoreType"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ScoreType").ToString())%>' name="hdfldScoreType"/>
											<input id="hdfldRiskItemId" runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.RiskItemId").ToString())%>' name="hdfldRiskItemId"/>
											<input id="hdfldOldScore" runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.OldScore").ToString())%>' name="hdfldOldScore"/>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:DataGrid></td>
					</tr>
				</table>
				<table style="WIDTH: 807px; HEIGHT: 24px">
					<tr>
						<td>
							<asp:label id="lblRiskScoreLastreview" runat="server" Font-size="11px"></asp:label>
						</td>						
					</tr>					
				</table>
				<br /><br />		
			</asp:panel><asp:panel id="Panel10" runat="server" Width="712px" Height="15px" cssclass="Panel">
				<table>
					<tr>
						<td>
							<asp:label id="lblEnableCustomerAcceptance" runat="server" Width="220px" CssClass="LABEL">Enable Customer Acceptance:</asp:label></td>
						<td>
							<asp:CheckBox id="chkEnableCustomerAcceptance" runat="server" CssClass="CheckBox"></asp:CheckBox></td>
						<td>
							<asp:label id="Label68" runat="server" CssClass="LABEL">Customer Status:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlCustomerStatus" runat="server" Width="218px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:LinkButton id="lnkAcceptanceModel" Width="150px" CausesValidation="False" runat="server" CssClass="Label">Acceptance Model:</asp:LinkButton></td>
						<td>
							<asp:textbox id="txtAcceptanceModel" runat="server" CssClass="TextBox"></asp:textbox></td>
						<td>
							<asp:LinkButton id="lnkCDDModel" Width="150px" CausesValidation="False" runat="server" CssClass="Label">CDD Model:</asp:LinkButton></td>
						<td>
							<asp:textbox id="txtCDDModel" runat="server" CssClass="TextBox"></asp:textbox></td>
					</tr>
				</table>
				<br/>
				<table>
					<tr>
						<td class="grdheader" colspan="2">
							<asp:label id="lblAcceptanceItems" runat="server" Width="220px" CssClass="LABEL">Acceptance Items Detail (Current and Archived)</asp:label></td>
					</tr>
					<tr>
						<td>
							<div style="OVERFLOW: auto; WIDTH: 804px; HEIGHT: 220px">
								<asp:DataGrid id="dgItemDetails" runat="server" Width="810px" BorderStyle="None" BorderWidth="1px"
									BackColor="White" cellpadding="2" AutoGenerateColumns="False" BorderColor="#CC9966">
									<FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
									<SelectedItemStyle Font-Bold="true" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
									<ItemStyle Font-size="11px" BackColor="White"></ItemStyle>
									<HeaderStyle Font-size="11px" Font-Bold="true" ForeColor="White" 
									        BackColor="#990000" HorizontalAlign="Center"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn HeaderText="Id">
											<HeaderStyle Width="25px"></HeaderStyle>
											<ItemTemplate>
												<asp:LinkButton id="lnkbtnItemId" OnClick="AcceptanceItemChanged" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Id").ToString())%>' runat="server" CssClass="lblgridcell">
												</asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="Name" HeaderText="Name">
											<HeaderStyle Width="210px"></HeaderStyle>
										</asp:BoundColumn>
										<asp:TemplateColumn HeaderText="Item Type">
											<HeaderStyle Width="100px"></HeaderStyle>
											<ItemTemplate>
												<asp:Label ReadOnly="true" id="txtMaptableName" cssclass="TextBoxWithNoBorder" Width="70px" onclick="ControlClicked();" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.MaptableName").ToString())%>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Model">
								                <HeaderStyle Width="100px"></HeaderStyle>
								                <ItemTemplate>
									            <asp:Label  ReadOnly="True"  id="txtModel" cssclass="TextBoxWithNoBorder" Width="100px" onclick="ControlClicked();" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CLTCode").ToString())%>'>
									            </asp:Label>
								            </ItemTemplate>
							            </asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Record ID">
											<HeaderStyle Width="100px"></HeaderStyle>
											<ItemTemplate>
												<asp:Label id="txtGroupID" ReadOnly="true" cssclass="TextBoxWithNoBorder" Width="70px" onclick="ControlClicked();" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GroupID")%>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Value">
											<HeaderStyle Width="100px"></HeaderStyle>
											<ItemTemplate>
												<asp:Label id="txtAcptVaue" ReadOnly="true" cssclass="TextBoxWithNoBorder" Width="70px" onclick="ControlClicked();" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Value")%>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Resp. Person">
											<HeaderStyle Width="100px"></HeaderStyle>
											<ItemTemplate>
												<asp:Label id="txtresponsiblePerson" cssclass="TextBoxWithNoBorder" Width="100px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ResponsiblePerson")%>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Create Date">
								            <HeaderStyle Width="70px"></HeaderStyle>
								            <ItemTemplate>
									            <asp:Label id="txtCreateDate" Width="70px" cssclass="TextBoxWithNoBorder" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CreateDate").ToString())%>'>
									            </asp:Label>
								            </ItemTemplate>
							            </asp:TemplateColumn>
							            <asp:TemplateColumn HeaderText="Review Due Date">
											<HeaderStyle Width="70px"></HeaderStyle>
											<ItemTemplate>
												<asp:Label id="txtdueDate" Width="70px" cssclass="TextBoxWithNoBorder" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.DueDate").ToString())%>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="IDV Status">
											<HeaderStyle Width="40px"></HeaderStyle>
											<ItemTemplate>
												<a id="lnkIDVRequest" href='javascript:<%# 
															GetPopupScript("IDVRequest.aspx?RecordID=" & Microsoft.Security.Application.Encoder.UrlEncode(DataBinder.Eval(Container, "DataItem.IDVRequestNo").ToString()) & _
															"&ActionID=2&RowNum=0&NoScroll=1", "scrollbars=no,resizable=yes,width=880,height=600") 
															%>'>
													<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.IDVStatus").ToString())%>
												</a>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Decision">
											<HeaderStyle Width="110px"></HeaderStyle>
											<ItemTemplate>
												<asp:DropDownList id="ddlDecision" Width="110px" SelectedIndex='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Decision").ToString())%>' runat="server">
													<asp:ListItem Value="0"> Undetermined </asp:ListItem>
													<asp:ListItem Value="1"> Satisfactory </asp:ListItem>
													<asp:ListItem Value="2"> Unsatisfactory </asp:ListItem>
													<asp:ListItem Value="3"> Needs Review </asp:ListItem>
													<asp:ListItem Value="4"> Waived </asp:ListItem>
													<asp:ListItem Value="5" Text =" ">&nbsp;</asp:ListItem>
												</asp:DropDownList>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Complete">
											<HeaderStyle Width="25px"></HeaderStyle>
											<ItemTemplate>
												<asp:CheckBox ID ="chkComplete" runat="server" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Complete").ToString())%>'>
												</asp:CheckBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="2nd.Check">
											<HeaderStyle Width="25px"></HeaderStyle>
											<ItemTemplate>
												<asp:CheckBox ID ="chkSecondCheck" runat="server" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheck").ToString())%>'>
												</asp:CheckBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Approve">
											<HeaderStyle Width="25px"></HeaderStyle>
											<ItemTemplate>
												<asp:CheckBox ID ="chkApprove" runat="server" Checked='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Approve").ToString())%>'>
												</asp:CheckBox>
												<input id="hdfldCompletedBy"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CompletedBy").ToString())%>' name="hdfldCompletedBy"/>
												<input id="hdfldSecondCheckedBy"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheckBy").ToString())%>' name="Hidden1"/>
												<input id="hdfldApprovedBy"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ApprovedBy").ToString())%>' name="Hidden2"/>
												<input id="hdfldCompletedDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CompletedDate").ToString())%>' name="Hidden3"/>
												<input id="hdfldSecondCheckedDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheckDate").ToString())%>' name="Hidden4"/>
												<input id="hdfldApprovedDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ApprovedDate").ToString())%>' name="Hidden5"/>
												<input id="hdfldCreateDate"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CreateDate").ToString())%>' name="Hidden6"/>
												<input id="hdfldNotes"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Notes").ToString())%>' name="Hidden7"/>
												<input id="hdfldDocument"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.DocumentNo").ToString())%>' name="Hidden8"/>
												<input id="hdfldStatus"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Status").ToString())%>' name="Hidden8"/>
												<input id="hdfldCLICode"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CLICode").ToString())%>' name="Hidden8"/>
												<input id="hdfldCompleteRight"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.CompleteRight").ToString())%>' name="Hidden8"/>
												<input id="hdfldApproveRight"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ApproveRight").ToString())%>' name="Hidden8"/>
												<input id="hdfldSecondCheckRight"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheckRight").ToString())%>' name="Hidden8"/>
												<input id="hdfldRequired"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Required").ToString())%>' name="Hidden8"/>
												<input id="hdfldApproveRequired"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ApproveRequired").ToString())%>' name="Hidden8"/>
												<input id="hdfldSecondCheckRequired"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.SecondCheckRequired").ToString())%>' name="Hidden8"/>
												<input id="hdfldItemName"  runat="server" type="hidden" value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Name").ToString())%>' name="Hidden8"/>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
								</asp:DataGrid></div>
						</td>
					</tr>
				</table>
				<table>
				</table>
				<table>
					<tr>
						<td>
							<asp:label id="lblItemDetails" runat="server" width="804px" CSSClass="paneltitle"></asp:label></td>
					</tr>
				</table>
				<table id="table1" style="WIDTH: 872px; HEIGHT: 178px" cellspacing="0" cellpadding="0"
					width="872" border="0">
					<tr>
						<td style="WIDTH: 424px" valign="top">
							<table id="table3" style="WIDTH: 400px; HEIGHT: 72px" cellspacing="0" cellpadding="0" width="400"
								border="0">
								<tr>
									<td>
										<asp:label id="Label58" runat="server" Width="104px" CssClass="LABEL">Completed By:</asp:label></td>
									<td>
										<asp:textbox id="txtCompletedBy" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
									<td>
										<asp:label id="Label61" runat="server" Width="50px" CssClass="LABEL">Date:</asp:label></td>
									<td>
										<asp:textbox id="txtCompletedDate" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label59" runat="server" Width="104px" CssClass="LABEL">2nd. Check By:</asp:label></td>
									<td>
										<asp:textbox id="txtSecondCheckedBy" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
									<td>
										<asp:label id="Label62" runat="server" Width="50px" CssClass="LABEL">Date:</asp:label></td>
									<td>
										<asp:textbox id="txtSecondCheckedDate" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td>
										<asp:label id="Label63" runat="server" Width="104px" CssClass="LABEL">Approved By:</asp:label></td>
									<td>
										<asp:textbox id="txtApprovedBy" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
									<td>
										<asp:label id="Label64" runat="server" Width="50px" CssClass="LABEL">Date:</asp:label></td>
									<td>
										<asp:textbox id="txtApprovedDate" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
								<tr>
									<td></td>
									<td align="right" colspan="2">
										<asp:label id="Label65" runat="server" Width="50px" CssClass="LABEL">Create Date:</asp:label></td>
									<td>
										<asp:textbox id="txtCreateDate" runat="server" Width="104px" CssClass="TEXTBOX"></asp:textbox></td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table id="table41" cellspacing="0" cellpadding="0" width="300" border="0">
								<tr>
									<td valign="top" align="right">
										<asp:label id="Label66" runat="server" Width="50px" CssClass="LABEL">Note:</asp:label></td>
									<td colspan="3">
										<asp:textbox id="txtAcceptanceItemNotes" runat="server" Height="86px" Width="308px" CssClass="TEXTBOX"
											TextMode="MultiLine"></asp:textbox></td>
								</tr>
								<tr>
									<td valign="top" align="right">
										<asp:label id="Label67" runat="server" Width="50px" CssClass="LABEL">Document:</asp:label></td>
									<td colspan="3">
										<asp:textbox id="txtdocument" runat="server" Width="56px" CssClass="TEXTBOX"></asp:textbox>&nbsp;
										<asp:HyperLink id="lnkViewDocument" onclick="PopupWindow(2);" runat="server" CssClass="lblgridcell"
											Font-Underline="true" ForeColor="Blue">View</asp:HyperLink></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<input id="hdfldSelectedItem" type="hidden" name="hdfldSelectedItem" runat="server"/>
				<input id="hdfldOperator" type="hidden" name="hdfldOperator" runat="server"/> <input id="hdfldValidDocument" type="hidden" value="1" name="hdfldValidDocument" runat="server"/>
			</asp:panel><asp:panel id="Panel11" runat="server" Width="712px" Height="15px" cssclass="Panel"><br/>
				<uc1:ucTabControl id="ucKYCDataDetail" runat="server"></uc1:ucTabControl>
				<asp:panel id="KYCPanel1" runat="server" Height="15px" Width="712px" cssclass="Panel">
					<table id="table22" style="WIDTH: 654px" cellspacing="1" cellpadding="1" width="654" border="0">
						<tr>
							<td style="WIDTH: 213px; HEIGHT: 20px" valign="middle" align="center">
								<asp:label id="Label55" runat="server" Height="24px" Width="220px" CssClass="LABEL">Prefix:</asp:label></td>
							<td>
								<asp:textbox id="txtNamePrefix" runat="server" Height="24px" Width="38" CssClass="TEXTBOX" MaxLength="25"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 213px; HEIGHT: 20px" valign="middle" align="center">
								<asp:label id="Label74" runat="server" Height="24px" Width="220px" CssClass="LABEL">First Name:</asp:label></td>
							<td>
								<asp:textbox id="txtFirstName" runat="server" Height="24px" Width="387px" CssClass="TEXTBOX"
									MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 213px; HEIGHT: 20px" valign="middle" align="center">
								<asp:label id="Label76" runat="server" Height="24px" Width="220px" CssClass="LABEL">Middle Name:</asp:label></td>
							<td>
								<asp:textbox id="txtMiddleName" runat="server" Height="24px" Width="387px" CssClass="TEXTBOX"
									MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 213px; HEIGHT: 20px" valign="middle" align="center">
								<asp:label id="Label77" runat="server" Height="24px" Width="220px" CssClass="LABEL">Last Name:</asp:label></td>
							<td>
								<asp:textbox id="txtLastName" runat="server" Height="24px" Width="387px" CssClass="TEXTBOX" MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 213px; HEIGHT: 20px" valign="middle" align="center">
								<asp:label id="Label78" runat="server" Height="24px" Width="220px" CssClass="LABEL">Suffix:</asp:label></td>
							<td>
								<asp:textbox id="txtNameSuffix" runat="server" Height="24px" Width="38" CssClass="TEXTBOX" MaxLength="25"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 213px">
								<asp:label id="Label79" runat="server" Height="24px" Width="220px" CssClass="LABEL">Alias Name (AKA):</asp:label></td>
							<td>
								<asp:textbox id="txtAlias" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX" MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 213px">
								<asp:label id="Label80" runat="server" Height="24px" Width="220px" CssClass="LABEL">URL:</asp:label></td>
							<td>
								<asp:textbox id="txtURL" runat="server" Height="24px" Width="387" CssClass="TEXTBOX" MaxLength="255"></asp:textbox></td>
						</tr>
					</table>
					<hr style="WIDTH: 146.98%; HEIGHT: 1px" width="146.98%" size="1"/>
					<table id="IndividualDetails">
						<tr>
							<td>
								<asp:label id="Label81" runat="server" Height="24px" Width="220px" CssClass="LABEL">Mother's Maiden Name:</asp:label></td>
							<td>
								<asp:textbox id="txtMothersMaidenName" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									Text=""></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label82" runat="server" Height="24px" Width="220px" CssClass="LABEL">Number of Dependents:</asp:label></td>
							<td>
								<asp:textbox id="txtNoOfDependants" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									Text=""></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label83" runat="server" Height="24px" Width="220px" CssClass="LABEL">Place of Birth:</asp:label></td>
							<td>
								<asp:textbox id="txtPlaceofBirth" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									MaxLength="50" Text=""></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label84" runat="server" Height="24px" Width="220px" CssClass="LABEL">Years at Current Address:</asp:label></td>
							<td>
								<asp:textbox id="txtYrsAtCurrAddress" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									Text=""></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label85" runat="server" Height="24px" Width="220px" CssClass="LABEL">Alien (Non-Citizen):</asp:label></td>
							<td>
								<asp:checkbox id="chkIsAlien" runat="server" Height="24px" Width="160px" CssClass="CheckBox" Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label86" runat="server" Height="24px" Width="220px" CssClass="LABEL">Dual Citizenship Country Names:</asp:label></td>
							<td>
								<asp:DropDownList id="ddlCountryofDualCitizenship" runat="server" Width="158px"></asp:DropDownList></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label87" runat="server" Height="24px" Width="220px" CssClass="LABEL">Employer Name:</asp:label></td>
							<td>
								<asp:textbox id="txtEmployerName" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									MaxLength="50" Text=""></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label88" runat="server" Height="24px" Width="220px" CssClass="LABEL">Job Title:</asp:label></td>
							<td>
								<asp:textbox id="txtJobTitle" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX" MaxLength="50"
									Text=""></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label89" runat="server" Height="24px" Width="220px" CssClass="LABEL">Job Type (Full/Part time):</asp:label></td>
							<td>
								<asp:textbox id="txtJobTime" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX" MaxLength="50"
									Text=""></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label90" runat="server" Height="24px" Width="220px" CssClass="LABEL">Secondary Employer Name:</asp:label></td>
							<td>
								<asp:textbox id="txtSecondaryEmployer" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									MaxLength="50" Text=""></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label91" runat="server" Height="24px" Width="220px" CssClass="LABEL">Length of Employment:</asp:label></td>
							<td>
								<asp:textbox id="txtLengthOfEmp" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									MaxLength="25" Text=""></asp:textbox></td>
							<td>
								<asp:label id="Label92" runat="server" Height="24px" Width="220px" CssClass="LABEL">Have Personal Relationship:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsPersonalRelationship" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox>
							</td>		
						</tr>
						<tr>
							<td style="height: 27px">
								<asp:label id="Label161" runat="server" Height="24px" Width="220px" CssClass="LABEL"></asp:label></td>
							<td style="height: 27px">
								<asp:label id="lblHidden" runat="server" Height="24px" Width="160px" CssClass="LABEL"
									Text=""></asp:label></td>
							<td style="height: 27px">
                                <asp:label id="lblRelWB" runat="server" CssClass="LABEL" Height="24px" Width="220px">Relationship with Bank Affiliate:</asp:label>
                            </td>        
                            <td style="height: 27px">
                                <asp:CheckBox ID="chkRelWB" Text="" CssClass="CHECKBOX" runat="server" />
							</td>							
						</tr>
					</table>
					<hr style="WIDTH: 146.98%; HEIGHT: 1px" width="146.98%" size="1"/>
					<table id="Details">
						<tr>
							<td>
								<asp:label id="Label93" runat="server" Height="24px" Width="220px" CssClass="LABEL">Origin Of Funds:</asp:label></td>
							<td style="WIDTH: 177px">
								<asp:textbox id="txtOriginOfFunds" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									Text=""></asp:textbox></td>
							<td>
                                <asp:Label ID="Label158" runat="server" Height="24px" Width="220px" CssClass="Label" Text="Introducer:"></asp:Label></td>
							<td style="WIDTH: 177px"><asp:textbox id="txtIntroducer" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									Text=""></asp:textbox></td>
						</tr>
					</table>
					<hr style="WIDTH: 146.98%; HEIGHT: 1px" width="146.98%" size="1"/>
					<table id="otherDetails">
						<tr>
							<td>
								<asp:label id="Label95" runat="server" Height="24px" Width="220px" CssClass="LABEL">Date Of Inception:</asp:label></td>
							<td>
								<asp:textbox id="txtdateOfInception" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label96" runat="server" Height="24px" Width="220px" CssClass="LABEL">No OF Employees:</asp:label></td>
							<td>
								<asp:textbox id="txtNoofEmployees" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label97" runat="server" Height="24px" Width="220px" CssClass="LABEL">Legal Enity Type:</asp:label></td>
							<td>
								<asp:textbox id="txtLegalEntityType" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"
									MaxLength="50"></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label98" runat="server" Height="24px" Width="220px" CssClass="LABEL">traded Publicly:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsPubliclytraded" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
						</tr>
						<tr>
							<td style="HEIGHT: 28px">
								<asp:label id="Label99" runat="server" Height="24px" Width="220px" CssClass="LABEL">Ticker Symbol:</asp:label></td>
							<td style="HEIGHT: 28px">
								<asp:textbox id="txtTickerSymbol" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 200px; HEIGHT: 28px">
								<asp:label id="Label100" runat="server" Height="24px" Width="220px" CssClass="LABEL">Stock Exchange:</asp:label></td>
							<td style="HEIGHT: 28px">
								<asp:textbox id="txtExchange" runat="server" Height="24" Width="162px" CssClass="TEXTBOX" MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label101" runat="server" Height="24px" Width="220px" CssClass="LABEL">Products Sold:</asp:label></td>
							<td>
								<asp:textbox id="txtProductsSold" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"
									MaxLength="255"></asp:textbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label102" runat="server" Height="24px" Width="220px" CssClass="LABEL">Services Provided:</asp:label></td>
							<td>
								<asp:textbox id="txtServicesProvided" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"
									MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label103" runat="server" Height="24px" Width="220px" CssClass="LABEL">Geographical Area:</asp:label></td>
							<td>
								<asp:textbox id="txtGeographicalAreasServed" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"
									MaxLength="255"></asp:textbox></td>
							<td>
								<asp:label id="Label104" runat="server" Height="24px" Width="220px" CssClass="LABEL">Total Annual Sale:</asp:label></td>
							<td>
								<asp:textbox id="txtTotalAnnualSales" runat="server" Height="24" Width="161px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label105" runat="server" Height="24px" Width="220px" CssClass="LABEL">Pension Fund:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsPensionFund" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label106" runat="server" Height="24px" Width="220px" CssClass="LABEL">Endowment Fund:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsEndowmentFund" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label107" runat="server" Height="24px" Width="220px" CssClass="LABEL">Goverment Sponsored Fund:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsGovernmentSponsored" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label108" runat="server" Height="24px" Width="220px" CssClass="LABEL">Foreign Correspondent Bank:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsForeignCorrBnk" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label109" runat="server" Height="24px" Width="220px" CssClass="LABEL">Subsidiary:</asp:label></td>
							<td>
								<asp:checkbox id="chkIsSubsidiary" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label110" runat="server" Height="24px" Width="220px" CssClass="LABEL">Requires Cash transactions:</asp:label></td>
							<td>
								<asp:checkbox id="chkRequireCashtrans" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label111" runat="server" Height="24px" Width="220px" CssClass="LABEL">Requires Foreign transactions:</asp:label></td>
							<td>
								<asp:checkbox id="chkRequireForeigntrans" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label112" runat="server" Height="24px" Width="220px" CssClass="LABEL">Requires Wire transactions:</asp:label></td>
							<td>
								<asp:checkbox id="chkRequireWiretrans" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label113" runat="server" Height="24px" Width="220px" CssClass="LABEL">Patriotic Act Certification:</asp:label></td>
							<td>
								<asp:checkbox id="chkHasPatriotActCertification" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label114" runat="server" Height="24px" Width="220px" CssClass="LABEL">Certification Expiry Date:</asp:label></td>
							<td>
								<asp:textbox id="txtPatriotActCertExpiryDate" runat="server" Height="24px" Width="161" CssClass="TEXTBOX"
									Text=""></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label115" runat="server" Height="24px" Width="220px" CssClass="LABEL">Foreign Branches:</asp:label></td>
							<td>
								<asp:checkbox id="chkHasForeignBranches" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox></td>
							<td style="WIDTH: 200px">
								<asp:label id="Label116" runat="server" Height="24px" Width="220px" CssClass="LABEL">Foreign Branch Countries:</asp:label></td>
							<td>
								<asp:textbox id="txtForeignBranchCountries" runat="server" Height="24px" Width="160px" CssClass="TEXTBOX"
									MaxLength="255" Text=""></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="lblPIC" runat="server" Height="24px" Width="220px" CssClass="LABEL">Private Investment Company:</asp:label>
							</td>
							<td>
								<asp:checkbox id="chkPIC" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text=""></asp:checkbox>
							</td>						
							<td>
								<asp:label id="Label117" runat="server" Height="24px" Width="220px" CssClass="LABEL">Type of Monetary Instruments:</asp:label>
							</td>
							<td>
								<asp:textbox id="txtTypeofMonInstruments" runat="server" Height="24px" Width="161" CssClass="TEXTBOX"
									MaxLength="255" Text=""></asp:textbox>
							</td>
						</tr>
					</table>
					<hr style="WIDTH: 146.98%; HEIGHT: 1px" width="146.98%" size="1"/>
					<table id="table111" cellspacing="0" cols="4" cellpadding="0" width="750" border="0">
						<tr>
							<td style="WIDTH: 104px" colspan="2">
								<asp:checkbox id="chkMoneyOrders" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text="Money Order Business"></asp:checkbox></td>
							<td style="WIDTH: 159px" colspan="2">
								<asp:checkbox id="chktravelersCheck" runat="server" Height="24px" Width="108px" CssClass="CheckBox"
									Text="Traveler's Check Business"></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="lblMORegistrationNo" runat="server" Height="24px" Width="200px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMORegistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="lblMORegistrattion" runat="server" Height="24px" Width="160px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td>
								<asp:textbox id="txtTCRegistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 104px">
								<asp:label id="lbMOConfirmationNo" runat="server" Height="24px" Width="200px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMORegConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label118" runat="server" Height="24px" Width="160px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td>
								<asp:textbox id="txtTCRegConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="lblMOAgentOf" runat="server" Height="24px" Width="200px" CssClass="LABEL">Acting Agent of:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMOActingasAgentfor" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label119" runat="server" Height="24px" Width="160px" CssClass="LABEL">Acting Agent of:</asp:label></td>
							<td>
								<asp:textbox id="txtTCActingasAgentfor" runat="server" Height="26" Width="266" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="lblMOMonthlyIncome" runat="server" Height="24px" Width="200px" CssClass="LABEL"> Monthly Income:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMOPercentIncome" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label120" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Income:</asp:label></td>
							<td>
								<asp:textbox id="txtTCPercentIncome" runat="server" Height="26" Width="266" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label121" runat="server" Height="24px" Width="200px" CssClass="LABEL"> Monthly Amount :</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMOMonthlyAmt" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label122" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Amount:</asp:label></td>
							<td>
								<asp:textbox id="txtTCMonthlyAmt" runat="server" Height="26" Width="266" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="HEIGHT: 2px">
								<asp:label id="lblMOMaxDailyAmt" runat="server" Height="24px" Width="200px" CssClass="LABEL">Max Daily Amount:</asp:label></td>
							<td style="WIDTH: 266px; HEIGHT: 2px">
								<asp:textbox id="txtMOMaxDailyAmtPerPerson" runat="server" Height="23px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label123" runat="server" Height="24px" Width="160px" CssClass="LABEL">Max Daily Amount:</asp:label></td>
							<td style="HEIGHT: 2px">
								<asp:textbox id="txtTCMaxDailyAmtPerPerson" runat="server" Height="26" Width="266" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 104px" colspan="2">
								<asp:checkbox id="chkMoneytransmission" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text="Money transmission Business "></asp:checkbox></td>
							<td style="WIDTH: 159px" colspan="2">
								<asp:checkbox id="chkCheckCashing" runat="server" Height="24px" Width="108px" CssClass="CheckBox"
									Text="Check Cashing Business"></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label124" runat="server" Height="24px" Width="200px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMtregistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label125" runat="server" Height="24px" Width="160px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td>
								<asp:textbox id="txtCCRegistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 104px">
								<asp:label id="Label126" runat="server" Height="24px" Width="200px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMtregConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label127" runat="server" Height="24px" Width="160px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td>
								<asp:textbox id="txtCCRegConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label128" runat="server" Height="24px" Width="200px" CssClass="LABEL">Acting Agent of :</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMTActingasAgentfor" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label129" runat="server" Height="24px" Width="160px" CssClass="LABEL">Acting Agent of:</asp:label></td>
							<td>
								<asp:textbox id="txtCCActingasAgentfor" runat="server" Height="26" Width="266" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label130" runat="server" Height="24px" Width="200px" CssClass="LABEL"> Monthly Income :</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMTPercentIncome" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label131" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Income:</asp:label></td>
							<td>
								<asp:textbox id="txtCCPercentIncome" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label132" runat="server" Height="24px" Width="200px" CssClass="LABEL"> Monthly Amount :</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtMTMonthlyAmt" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label133" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Amount:</asp:label></td>
							<td>
								<asp:textbox id="txtCCMonthlyAmt" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="HEIGHT: 2px">
								<asp:label id="Label134" runat="server" Height="24px" Width="200px" CssClass="LABEL">Max Daily Amount:</asp:label></td>
							<td style="WIDTH: 266px; HEIGHT: 2px">
								<asp:textbox id="txtMTMaxDailyAmtPerPerson" runat="server" Height="23px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label135" runat="server" Height="24px" Width="160px" CssClass="LABEL">Max Daily Amount:</asp:label></td>
							<td style="HEIGHT: 2px">
								<asp:textbox id="txtCCMaxDailyAmtPerPerson" runat="server" Height="23px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 104px" colspan="2">
								<asp:checkbox id="chkCurrencyExchange" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text="Currency Cashing Business"></asp:checkbox></td>
							<td style="WIDTH: 159px" colspan="2">
								<asp:checkbox id="chkCurrencyDealing" runat="server" Height="24px" Width="108px" CssClass="CheckBox"
									Text="Currency Dealing Business"></asp:checkbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label136" runat="server" Height="24px" Width="200px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtCERegistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label137" runat="server" Height="24px" Width="160px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td>
								<asp:textbox id="txtCDRegistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 104px">
								<asp:label id="Label138" runat="server" Height="24px" Width="200px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtCERegConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label139" runat="server" Height="24px" Width="160px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td>
								<asp:textbox id="txtCDRegConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label140" runat="server" Height="24px" Width="200px" CssClass="LABEL">Acting Agent of:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtCEActingasAgentfor" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label141" runat="server" Height="24px" Width="160px" CssClass="LABEL">Acting Agent of:</asp:label></td>
							<td>
								<asp:textbox id="txtCDActingasAgentfor" runat="server" Height="26" Width="266" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label142" runat="server" Height="24px" Width="200px" CssClass="LABEL"> Monthly Income:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtCEPercentIncome" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label143" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Income:</asp:label></td>
							<td>
								<asp:textbox id="txtCDPercentIncome" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label144" runat="server" Height="24px" Width="200px" CssClass="LABEL"> Monthly Amount:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtCEMonthlyAmt" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label145" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Amount:</asp:label></td>
							<td>
								<asp:textbox id="txtCDMonthlyAmt" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="HEIGHT: 2px">
								<asp:label id="Label146" runat="server" Height="24px" Width="200px" CssClass="LABEL">Max Daily Amount:</asp:label></td>
							<td style="WIDTH: 266px; HEIGHT: 2px">
								<asp:textbox id="txtCEMaxDailyAmtPerPerson" runat="server" Height="23px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
								<asp:label id="Label147" runat="server" Height="24px" Width="160px" CssClass="LABEL">Max Daily Amount:</asp:label></td>
							<td style="HEIGHT: 2px">
								<asp:textbox id="txtCDMaxDailyAmtPerPerson" runat="server" Height="23px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 104px" colspan="2">
								<asp:checkbox id="chkStoredValue" runat="server" Height="24px" Width="160px" CssClass="CheckBox"
									Text="Stored Value Business"></asp:checkbox></td>
							<td style="WIDTH: 159px"></td>
							<td></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label148" runat="server" Height="24px" Width="200px" CssClass="LABEL">Registration Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtSVRegistration" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
                                <asp:label id="Label151" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Income:</asp:label>							
							</td>
							<td>
                                <asp:textbox id="txtSVPercentIncome" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox>							
							</td>
						</tr>
						<tr>
							<td style="WIDTH: 104px">
								<asp:label id="Label149" runat="server" Height="24px" Width="200px" CssClass="LABEL">Confirmation Number:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtSVRegConfirmationNo" runat="server" Height="24" Width="265" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
							    <asp:label id="Label152" runat="server" Height="24px" Width="160px" CssClass="LABEL"> Monthly Amount:</asp:label>
							</td>
							<td>
                                <asp:textbox id="txtSVMonthlyAmt" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox>							
							</td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label150" runat="server" Height="24px" Width="200px" CssClass="LABEL">Acting Agent of:</asp:label></td>
							<td style="WIDTH: 266px">
								<asp:textbox id="txtSVActingasAgentfor" runat="server" Height="24px" Width="265px" CssClass="TEXTBOX"></asp:textbox></td>
							<td style="WIDTH: 159px">
                                <asp:label id="Label153" runat="server" Height="24px" Width="160px" CssClass="LABEL">Max Daily Amount:</asp:label>							
							</td>
							<td>
                                <asp:textbox id="SVMaxDailyAmtPerPerson" runat="server" Height="23px" Width="265px" CssClass="TEXTBOX"></asp:textbox>							
							</td>
						</tr>
					</table>
					<hr style="WIDTH: 146.98%; HEIGHT: 1px" width="146.98%" size="1"/>						
					<table>
						<tr>
							<td style="HEIGHT: 2px;">
								<asp:label id="lblRONPRES" runat="server" Height="51px" Width="192px" CssClass="LABELML"> Results of Neg. Press Search:</asp:label>
							</td>
							<td style="HEIGHT: 2px; ">
                               <asp:TextBox ID="txtRONPRES" runat="server" TextMode="MultiLine" Height="55px" Width="690px"
                                onkeydown="JStextCount(this,255);" onpaste="javascript:if(this.value.length+window.clipboardData.getData('Text').length>255){event.returnValue = false;};"></asp:TextBox>   
							</td>							
						</tr>		
					</table>
					<hr style="WIDTH: 146.98%; HEIGHT: 1px" width="146.98%" size="1"/>
				</asp:panel>
				<asp:panel id="KYCPanel2" runat="server" Height="15px" Width="712px" cssclass="Panel"><input id="DisplayAddAddress" onclick="HideAndDisplay('display')" type="button" value="New Address"
						name="DisplayAddAddress" runat="server"/> 

<div id="popUpAddModify" style="DISPLAY: none">
						<div id="DIVADDRESSWARNING">
							<h5 id="AddressWarning" style="COLOR: red"></h5>
						</div>
						<table class="" id="AllAdresses1" border="1" runat="server">
							<tr>
								<td>
									<table class="KYCtable" id="AllAdresses" bgColor="#990000" border="1">
										<tr class="KYCtabletr">
											<td>*Address Type</td>
											<td>
												<asp:dropdownlist id="cell0" runat="server" Width="160px"></asp:dropdownlist></td>
										</tr>
										<tr class="KYCtabletr">
											<td>Care of</td>
											<td><input id="cell1" style="WIDTH: 160px; HEIGHT: 22px" disabled="disabled" type="text" maxlength="50"
													size="21"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>*Street Name</td>
											<td><input id="cell2" style="WIDTH: 161px" disabled="disabled" type="text" maxlength="255"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>City</td>
											<td><input id="cell3" style="WIDTH: 161px" disabled="disabled" type="text" maxlength="50"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>State</td>
											<td><input id="cell4" style="WIDTH: 161px" disabled="disabled" type="text" maxlength="50"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>Country</td>
											<td>
												<asp:DropDownList id="cell5" runat="server" Width="161"></asp:DropDownList></td>
										</tr>
										<tr class="KYCtabletr">
											<td>ZIP Code</td>
											<td><input id="cell6" style="WIDTH: 161px" disabled="disabled" type="text" maxlength="10"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>Telephone</td>
											<td><input id="cell7" style="WIDTH: 161px" disabled="disabled" type="text" maxlength="25"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>Fax</td>
											<td><input id="cell8" style="WIDTH: 161px" disabled="disabled" type="text" maxlength="25"/></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><input id="addaddress" onclick="AddOrModifyAddess('ADD', '')" type="button" value="Add"
										name="addaddress" runat="server"/> <input id="hideaddress" onclick="HideAndDisplay('hide')" type="button" value="Cancel"/>
								</td>
							</tr>
						</table>
					</div>
                    <table borderColor="#ffffff" cellspacing="0" cellpadding="0" border="0">
						<tr valign="top">
							<td valign="top">
								<table class="KYCtable" id="PartyAddress" border="1" runat="server">
									<tr class="KYCtabletr">
										<th>
											Address Type</th>
										<th>
											Care Of</th>
										<th>
											Street</th>
										<th>
											City</th>
										<th>
											State</th>
										<th>
											Country</th>
										<th>
											ZIP</th>
										<th>
											Telephone</th>
										<th>
											Fax</th>
										<th>
											Action</th></tr>
								</table>
							</td>
						</tr>
					</table>&nbsp; <input id="hfld1" type="hidden" name="hfld1" runat="server"/><input id="hDeletedAddrs" type="hidden" name="hDeletedAddrs" runat="server"/> </asp:panel>
				<asp:panel id="KYCPanel3" runat="server" Height="15px" Width="712px" cssclass="Panel">
					<input id="DisplayAddID" onclick="HideAndDisplayID('display')" type="button" value="New ID"
						name="DisplayAddID" runat="server"/>
					<div id="popUpAddModifyID" style="DISPLAY: none">
						<div id="DIVIDWARNING">
							<h5 id="IDWarning" style="COLOR: red"></h5>
						</div>
						<table id="AllIDs1" border="1" runat="server">
							<tr>
								<td>
									<table class="KYCtable" id="AllIDs" bgColor="#990000" border="1">
										<tr class="KYCtabletr">
											<td>*ID Type</td>
											<td>
												<asp:dropdownlist id="IDcell0" runat="server" Width="152px"></asp:dropdownlist></td>
										</tr>
										<tr class="KYCtabletr">
											<td>*ID Number</td>
											<td><input id="IDcell1" disabled="disabled" onclick ="DisplayWarning()" type="text" maxlength="25"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>*Issue Agency</td>
											<td><input id="IDcell2" disabled="disabled" type="text" maxlength="255"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>Issue Place</td>
											<td><input id="IDcell3" disabled="disabled" type="text" maxlength="255"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>Issue Country</td>
											<td>
												<asp:DropDownList id="IDcell4" runat="server" Width="152px"></asp:DropDownList></td>
										</tr>
										<tr class="KYCtabletr">
											<td>*Issue Date</td>
											<td><input id="IDcell5" onblur="LocaleDate('IDcell5')" disabled="disabled" type="text"/></td>
										</tr>
										<tr class="KYCtabletr">
											<td>*Expiry Date</td>
											<td><input id="IDcell6" onblur="LocaleDate('IDcell6')" disabled="disabled" type="text"/></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><input id="addid" onclick="AddOrModifyID('ADD', '')" type="button" value="Add"/> <input id="hideid" onclick="HideAndDisplayID('hide')" type="button" value="Cancel"/>
								</td>
							</tr>
						</table>
					</div>
					<input id="hFldIDs" type="hidden" name="hFldIDs" runat="server"/> <input id="hDeletedIDs" type="hidden" name="hDeletedIDs" runat="server"/>
					<table class="KYCtable" id="PartyID" border="1" runat="server">
						<tr class="KYCtabletr">
							<th>
								ID Type</th>
							<th>
								ID Number</th>
							<th>
								Issue Agency</th>
							<th>
								Issue Place</th>
							<th>
								Issue Country</th>
							<th>
								Issue Date</th>
							<th>
								Expiry Date</th>
							<th>
								Action</th></tr>
					</table>
				</asp:panel>
				<asp:panel id="KYCPanel4" runat="server" Height="15px" Width="712px" cssclass="Panel">
					<table style="WIDTH: 688px; HEIGHT: 1186px">
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField1" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 1:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField1" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField2" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 2:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField2" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField3" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 3:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField3" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField4" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 4:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField4" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField5" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 5:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField5" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField6" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 6:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField6" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField7" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 7:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField7" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField8" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 8:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField8" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField9" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 9:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField9" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField10" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 10:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField10" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField11" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 11:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField11" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField12" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 12:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField12" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField13" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 13:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField13" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField14" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 14:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField14" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField15" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 15:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField15" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField16" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 16:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField16" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField17" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 17:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField17" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField18" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 18:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField18" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField19" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 19:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField19" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField20" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 20:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField20" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField21" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 21:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField21" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField22" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 22:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField22" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField23" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 23:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField23" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField24" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 24:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField24" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField25" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 25:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField25" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField26" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 26:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField26" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField27" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 27:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField27" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField28" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 28:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField28" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField29" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 29:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField29" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField30" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 30:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField30" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField31" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 31:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField31" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField32" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 32:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField32" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField33" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 33:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField33" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField34" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 34:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField34" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField35" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 35:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField35" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField36" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 36:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField36" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField37" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 37:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField37" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField38" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 38:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField38" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField39" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 39:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField39" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="WIDTH: 140px">
								<asp:Label id="lblCDDUserField40" runat="server" Height="12px" Width="136px" CssClass="LABEL">CDD User Defined 40:</asp:Label></td>
							<td>
								<asp:TextBox id="txtCDDUserField40" runat="server" Height="24px" Width="528px" CssClass="TEXTBOX"></asp:TextBox></td>
						</tr>
					</table>
				</asp:panel>
				<!-- PartyAlias panel---->
				<asp:panel id="KYCPanel5" runat="server" Height="15px" Width="712px" cssclass="Panel">
					<input id="DisplayAddPartyAlias" onclick="HideAndDisplayPartyAlias('display')" type="button" value="New Alias"
						name="DisplayAddPartyAlias" runat="server"/>
					<div id="popUpAddModifyPartyAlias" style="DISPLAY: none">
						<div id="DIVPARTYALIASWARNING">
							<h5 id="PARTYALIASWarning" style="COLOR: red"></h5>
						</div>
						<table id="AllPartyAlias1" border="1" runat="server">
							<tr>
								<td>
									<table class="KYCtable" id="AllPartyAliases" bgColor="#990000" border="1">
										<tr class="KYCtabletr">
											<td>*Alias</td>
											<td><input id="Aliascell0" type="text" maxlength="40"/></td>
										</tr>										
									</table>
								</td>
							</tr>
							<tr>
								<td><input id="addpartyalias" onclick="AddOrModifyPartyAlias('ADD', '')" type="button" value="Add"/> <input id="hidepartyalias" onclick="HideAndDisplayPartyAlias('hide')" type="button" value="Cancel"/>
								</td>
							</tr>
						</table>
					</div>
					<input id="hFldPartyAliases" type="hidden" name="hFldPartyAliases" runat="server"/> <input id="hDeletedPartyAliases" type="hidden" name="hDeletedPartyAliases" runat="server"/>
					<table class="KYCtable" id="PartyAlias" border="1" runat="server">
						<tr class="KYCtabletr">
							<th>
								Alias</th>
								<th>
								Action</th>
							</tr>
					</table>
				</asp:panel>
				<!-- PartyAlias panel --->
			</asp:panel><asp:panel id="cPanelValidators" runat="server" Width="864px" Height="46px"><input id="checkedRows" type="hidden" runat="server" /><input id="hdnRiskClass" type="hidden"
                    name="hdnRiskClass" runat="server" /></asp:panel></form>
	</body>
</html>
