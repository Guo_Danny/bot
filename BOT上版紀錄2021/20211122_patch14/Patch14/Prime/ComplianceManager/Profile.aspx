<%@ Register TagPrefix="uc1" TagName="ucNote" Src="ucNote.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Profile.aspx.vb" Inherits="CMBrowser.Profile" ValidateRequest="false"  %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucActivityList" Src="ucActivityList.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucSupportingInfo" Src="ucSupportingInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Profile</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="Styles.css" type="text/css" rel="stylesheet"/>
		<script language="JavaScript" src="js/nav.js" type="text/javascript"></script>
		<script language="javascript" id="clientEventHandlersJS" type="text/javascript">			
		function DispUpdProfileMessage() {
			var profileHasGenCaseRecord;
			var saveProfile;
			var profileNo;
			var msgTxt;
			
			try {		
				profileHasGenCaseRecord = "0";
				profileHasGenCaseRecord = document.getElementById("hfldProfileHasSnapShot").value;
				profileNo = document.getElementById("txtCode").value;
				if (profileNo != null) {
					if (profileHasGenCaseRecord == "1") {
						msgTxt = "The profile '" + profileNo + "' has activity that is marked to generate case.\n" +
								"One or more of the profile values have been incremented or changed and the corresponding case may not be generated.\n" +
								"Would you like to proceed?"
						saveProfile = confirm(msgTxt);
					}
							
					if (saveProfile) {
						document.getElementById("hfldProfileHasSnapShot").value = "0";	
						__doPostBack("cmdSave", "");
					}
				}			
				
				var vIDThreshold1 = 'ProfileDetails$ctl04$';
				var vIDThreshold2 = 'ProfileDetails$ctl06$';
				
				SetReadOnly(vIDThreshold1 + 'txtInAmount');
				SetReadOnly(vIDThreshold2 + 'txtInAmount');				
				SetReadOnly(vIDThreshold1 + 'txtOutAmount');
				SetReadOnly(vIDThreshold2 + 'txtOutAmount');
			    SetReadOnly(vIDThreshold1 + 'txtInCount');
				SetReadOnly(vIDThreshold2 + 'txtInCount');				
				SetReadOnly(vIDThreshold1 + 'txtOutCount');
				SetReadOnly(vIDThreshold2 + 'txtOutCount');
						
			}
			catch(e) {}
		}
			
			
		function SetReadOnly(pID)
		{		    
		    var vReadOnly;
		    
		    try
		    {          
		        vReadOnly =   document.createAttribute('readonly');
		        vReadOnly.value= 'readonly';
		        document.getElementById(pID).attributes.setNamedItem(vReadOnly);				   
		    }
		    catch(ex){}
		}
		
		
		</script>
		<script language="javascript" type="text/javascript">
		function CalcProfile(paramName)
			{

				var baseprofile;
				var tol1;
				var tol1percent;
				var tol2;
				var tol3;
				var tol3percent;
				var revthr1;
				var revthr2;
				var re;
				var osource;
				var currvalue;
				var currstring;
		
				if ( (paramName == "InAmount") || (paramName == "OutAmount") ){
					
					baseprofile = Number.parseLocale(document.getElementById("ProfileDetails$ctl01$txt" + paramName).value);
					//baseprofile = replacetxt(baseprofile, ",")
					baseprofile = parseFloat(baseprofile);
					}
				else {
					baseprofile = Number.parseLocale(document.getElementById("ProfileDetails$ctl01$txt" + paramName).value);
					//baseprofile = replacetxt(baseprofile, ",")
					baseprofile = parseInt(baseprofile);
					}
				
				baseprofile = (isNaN(baseprofile) ? 0 : round(baseprofile));
				
				tol1percent = Number.parseLocale(document.getElementById("ProfileDetails$ctl02$txt" + paramName).value);
				
				if (tol1percent == "") tol1percent = 0;
				else tol1percent = Number.parseLocale(String(parseInt(document.getElementById("ProfileDetails$ctl02$txt" + paramName).value)));				
				if(isNaN(tol1percent)) tol1percent = 0;
				if (tol1percent < 0) tol1percent = 0;
				if (tol1percent > 999) tol1percent = 999;								 		 
				tol1 = parseInt((baseprofile*tol1percent)/100);
				
				if (paramName == "InAmount" || paramName == "OutAmount" ) {
					tol2 = Number.parseLocale(document.getElementById("ProfileDetails$ctl03$txt" + paramName).value);
					//tol2 = replacetxt(tol2, ",")
					tol2 = parseFloat(tol2);
					}
				else {
					tol2 = Number.parseLocale(document.getElementById("ProfileDetails$ctl03$txt" + paramName).value);
					//tol2 = replacetxt(tol2, ",")
					tol2 = parseInt(tol2);
					}
				tol2 = (isNaN(tol2) ? 0 : round(tol2));
				
				
				tol3percent = parseInt(Number.parseLocale(document.getElementById("ProfileDetails$ctl05$txt" + paramName).value));
				if(isNaN(tol3percent)) tol3percent = 0;
				if (tol3percent < 0) tol3percent = 0;
				if (tol3percent > 999) tol3percent = 999;
				tol3 = parseInt((baseprofile*tol3percent)/100);
				
				revthr1 = round(baseprofile + tol1 + tol2);
				revthr2 = round(baseprofile + tol1 + tol2 + tol3);
				
				if (paramName == "InAmount" || paramName == "OutAmount")
				{
				        document.getElementById("ProfileDetails$ctl01$txt" + paramName).value =  formatnumber(baseprofile);
				        document.getElementById("ProfileDetails$ctl02$txt" + paramName).value =  formatnumber(tol1percent);
				        document.getElementById("ProfileDetails$ctl03$txt" + paramName).value =  formatnumber(tol2);
				        document.getElementById("ProfileDetails$ctl04$txt" + paramName).value =  formatnumber(revthr1);
				        document.getElementById("ProfileDetails$ctl05$txt" + paramName).value =  formatnumber(tol3percent);
				        document.getElementById("ProfileDetails$ctl06$txt" + paramName).value =  formatnumber(revthr2);	
				}
				else
				{
				        document.getElementById("ProfileDetails$ctl01$txt" + paramName).value =  baseprofile;
				        document.getElementById("ProfileDetails$ctl02$txt" + paramName).value =  tol1percent;
				        document.getElementById("ProfileDetails$ctl03$txt" + paramName).value =  tol2;
				        document.getElementById("ProfileDetails$ctl04$txt" + paramName).value =  revthr1;
				        document.getElementById("ProfileDetails$ctl05$txt" + paramName).value =  tol3percent;
				        document.getElementById("ProfileDetails$ctl06$txt" + paramName).value =  revthr2;				
				}

				

        }

			
			function round(number, X) {
				X = (!X ? 2 : X);
				return Math.round(number* Math.pow(10,X))/ Math.pow(10,X);
			}
			
			function formatnumber(num) {
            var cultureInfo = '<%= System.Globalization.CultureInfo.CurrentCulture.Name %>';
            var groupSymbol = '<%= System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator.ToString()%>';
            if (cultureInfo == "bg-BG") {
                num = String.localeFormat("{0:N}", num).split(groupSymbol).join(" ")
            }
            else {
                num = String.localeFormat("{0:N}", num)
            }
            return num;
        }


			
			function replacetxt(str, repstr) {
				var str1 = "";
				var xx = 0;
				while (xx < 10) {
					str1= str.replace(repstr, '');				
					if (str1 == str) 
						break;
					else
						str = str1;
					xx = xx+1;
					
				}
				return str1;
			}
			
		
		</script>

        <link href="Styles.css" rel="stylesheet" type="text/css" />
        <link href="Styles.css" rel="stylesheet" type="text/css" />
        <link href="Styles.css" rel="stylesheet" type="text/css" />
	</head>
	<body language="javascript" class="formbody" onload="DispUpdProfileMessage();AddWindowToPCSWindowCookie(window.name);" onUnload="RemoveWindowFromPCSWindowCookie(window.name);">
		<form id="Form1" method="post" runat="server">
            <asp:ScriptManager ID="smnProfile" runat="server" EnableScriptGlobalization="True">
            </asp:ScriptManager>
			<table id="PageLayout" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr>
								<td width="600"><asp:imagebutton id="imgPrime" runat="server" width="808px" AlternateText="Home" ImageUrl="images/compliance-manager.gif"></asp:imagebutton></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordMessage" runat="server" width="808px" cssClass="RecordMessage">Record Message</asp:label></td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" Height="24px" Font-Size="X-Small" Width="616px"></asp:validationsummary></td>
				</tr>
				<tr>
					<td>
						<table id="Table2" style="WIDTH: 816px; HEIGHT: 40px" cellspacing="0" cellpadding="0" width="816"
							border="0">
							<tr align="center">
								<td align="right">
                                    &nbsp;<asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder></td>
								<td align="left"><uc1:ucrecordscrollbar id="RecordScrollBar" runat="server"></uc1:ucrecordscrollbar></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
                        <asp:Panel ID="pnlPrompt" runat="server" CssClass="Panel" Height="40px" Width="850px" Visible="False">
                            &nbsp; &nbsp; &nbsp; &nbsp;
                            <asp:Label ID="lblPromptMsg" runat="server" Font-Bold="True" Height="20px">Prompt</asp:Label>&nbsp;
                            <asp:Button ID="cmdYes" runat="server" Text="YES" Width="48px" />&nbsp;
                            <asp:Button ID="cmdNo" runat="server" Text="NO" Width="48px" /></asp:Panel>
                        <uc1:uctabcontrol id="MainTabControl" runat="server"></uc1:uctabcontrol>
                   </td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordHeader" runat="server" width="804px" CSSClass="paneltitle"></asp:label></td>
				</tr>
			</table>
			<asp:panel id="Panel1" runat="server" Height="250px" Width="784px" cssclass="Panel">&nbsp; 
<table id="Table5" style="WIDTH: 776px; HEIGHT: 176px" cellspacing="0" cellpadding="0"
					width="776" border="0">
					<tr>
						<td>
							<asp:label id="Label20" runat="server" Width="144px" CssClass="LABEL">Profile No:</asp:label></td>
						<td>
							<asp:textbox id="txtCode" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:LinkButton id="lnkCustomerId" CssClass="Label" CausesValidation="False" runat="server">Customer Id:</asp:LinkButton></td>
						<td>
							<asp:textbox id="txtCustomerId" runat="server" Width="264px" CssClass="TEXTBOX" AutoPostBack="True"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label18" runat="server" Width="144px" CssClass="LABEL">Profile Start Date:</asp:label></td>
						<td>
							<asp:textbox id="txtProfileStartDate" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label30" runat="server" Width="144px" CssClass="LABEL">Customer Name:</asp:label></td>
						<td>
							<asp:textbox id="txtCustomerName" runat="server" Width="264px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label19" runat="server" Width="144px" CssClass="LABEL">Profile End Date:</asp:label></td>
						<td>
							<asp:textbox id="txtProfileEndDate" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:LinkButton id="lnkCustomerRisk" CssClass="Label" CausesValidation="False" runat="server">Risk Class:</asp:LinkButton></td>
						<td>
							<asp:textbox id="txtCustomerRisk" runat="server" Width="264px" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td style="HEIGHT: 2px">
							<asp:label id="Label28" runat="server" Width="144px" CssClass="LABEL">Activity Start Date:</asp:label></td>
						<td style="HEIGHT: 2px">
							<asp:textbox id="txtActivityStartDate" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label23" runat="server" Width="144px" CssClass="LABEL">Account Id:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlAccountId" runat="server" Width="264px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td style="HEIGHT: 17px">
							<asp:label id="Label21" runat="server" Width="144px" CssClass="LABEL">Activity End Date:</asp:label></td>
						<td style="HEIGHT: 17px">
							<asp:textbox id="txtActivityEndDate" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="HEIGHT: 17px">
							<asp:label id="txtActivityType" runat="server" Width="144px" CssClass="LABEL">Activity Type:</asp:label></td>
						<td style="HEIGHT: 17px">
							<asp:DropDownList id="ddlActivityType" runat="server" Width="264px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label7" runat="server" Width="144px" CssClass="LABEL">Profile Type:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlProfileType" runat="server" CssClass="DropDownList" AutoPostBack="True"></asp:DropDownList></td>
						<td>
							<asp:label id="Label8" runat="server" Width="144px" CssClass="LABEL">Calculation Type:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlCalculationType" runat="server" Width="264px" CssClass="DropDownList"></asp:DropDownList></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label22" runat="server" Width="144px" CssClass="LABEL">Recurrence Type:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlRecurrenceType" runat="server" CssClass="DropDownList" AutoPostBack="True"></asp:DropDownList></td>
						<td>
							<asp:label id="Label2" runat="server" Width="144px" CssClass="LABEL">Active:</asp:label></td>
						<td>
							<asp:CheckBox id="chkActiveProfile" runat="server" CssClass="CheckBox"></asp:CheckBox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label32" runat="server" Width="144px" CssClass="LABEL">Recurrence Value:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlRecurrenceValue" runat="server" CssClass="DropDownList"></asp:DropDownList></td>
						<td>
							<asp:label id="lblEnabled" runat="server" Width="144px" CssClass="LABEL">Enabled:</asp:label></td>
						<td>
							<asp:CheckBox id="chkEnabled" runat="server" CssClass="CheckBox"></asp:CheckBox></td>
					</tr>
				</table><br/>
<uc1:ucTabControl id="DetailsTabControl" runat="server"></uc1:ucTabControl>
<asp:Panel id="Panel11" runat="server" Width="784px" Height="250px" cssclass="Panel">
					<br/>
					<asp:Repeater id="ProfileDetails" runat="server">
							<HeaderTemplate>
							<table Width="824px" BorderStyle="None" BorderWidth="1px" BorderColor="#CC9966" BackColor="White"
								CellPadding="0" cellspacing="0">
								<tr class="gridheader">
								    <th>
										Details
									</th>
									<th>
                                        <asp:Label ID="lblCurrSymbol" runat="server" >In Amount</asp:Label>
									</th>
									<th>
										In Count
									</th>
									<th>
                                        <asp:Label ID="lblCurrSymbol1" runat="server">Out Amount</asp:Label>
										
									</th>
									<th>
										Out Count
									</th>
								</tr>
						</HeaderTemplate>				
						<ItemTemplate>
							<tr>
								<td>
									<asp:Label id="lblParameterName" Font-Names=" Verdana, Arial, Helvetica, sans-serif" Font-Bold="True" Font-Size="12px" runat="server" width="167px" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtml(DataBinder.Eval(Container,"DataItem.ParameterName").ToString())%>'>
									</asp:Label>
								</td>
								<td>
									<asp:TextBox id="txtInAmount" onchange="CalcProfile('InAmount');" cssclass="gridcell" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.InAmount").ToString())%>'>
									</asp:TextBox>
								</td>
								<td>
									<asp:TextBox id="txtInCount" runat="server" onchange="CalcProfile('InCount');" cssclass="gridcell" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.InCount").ToString())%>'>
									</asp:TextBox>
								</td>
								<td>
									<asp:TextBox id="txtOutAmount" runat="server" onchange="CalcProfile('OutAmount');" cssclass="gridcell" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.OutAmount").ToString())%>'>
									</asp:TextBox>
								</td>
								<td>
									<asp:TextBox id="txtOutCount" runat="server" onchange="CalcProfile('OutCount');" cssclass="gridcell" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.OutCount").ToString())%>'>
									</asp:TextBox>
								</td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							</table>
						</FooterTemplate>
					</asp:Repeater>
					<table id="Table1" style="WIDTH: 800px; HEIGHT: 12px" cellspacing="0" cellpadding="0" width="800"
						border="0">
						<tr>
							<td align="left" width="400">
								<asp:CheckBox id="chkGenAtEnd" runat="server" CssClass="CheckBox" Text="Generate cases at the end of recurrence period"></asp:CheckBox></td>
							<td align="right" width="400">
								<asp:CheckBox id="chkProposedProfile" runat="server" CssClass="CheckBox" AutoPostBack="true" Text="Show Proposed Profile"
									Visible="False"></asp:CheckBox></td>
						</tr>
						<tr>
							<td colspan="2">
								<asp:label id="pnlProposedProfile" runat="server" width="824px" CSSClass="paneltitle" Visible="False">Proposed Profile</asp:label></td>
						</tr>
					</table>
					<asp:DataGrid id="dgProposedProfile" runat="server" Width="824px" BackColor="White" BorderWidth="1px"
						BorderStyle="None" Visible="False" CellPadding="4" BorderColor="#CC9966" AutoGenerateColumns="False">
						<ItemStyle HorizontalAlign="Right" CssClass="viewgridcell"></ItemStyle>
						<HeaderStyle CssClass="gridheader"></HeaderStyle>
						<Columns>
						    <asp:TemplateColumn HeaderText="Details">
								<HeaderStyle Width="167px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
							    <ItemTemplate>
								    <asp:label id="ParameterName" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.ParameterName").ToString())%>' />
							    </ItemTemplate>
						    </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="In Amount ($)">
								<HeaderStyle Width="150px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="InAmount" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.InAmount").ToString())%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="In Count">
								<HeaderStyle Width="150px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="InCount" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.InCount").ToString())%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Out Amount ($)">
								<HeaderStyle Width="150px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle><ItemTemplate>
                                    <asp:Label ID="OutAmount" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.OutAmount").ToString())%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Out Count">
								<HeaderStyle Width="150px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="OutCount" runat="server" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.OutCount").ToString())%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
						</Columns>
						<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
					</asp:DataGrid>
					<table id="Table3" style="WIDTH: 824px; HEIGHT: 28px" cellspacing="0" cellpadding="0" width="824"
						border="0">
						<tr>
							<td align="right">
								<asp:Button id="cmdActivateProposedProfile" runat="server" Text="Activate" Visible="False"></asp:Button></td>
						</tr>
					</table>
				</asp:Panel>
<asp:Panel id="Panel12" runat="server" Width="784px" Height="250px" cssclass="Panel">
					<table id="Table4" cellspacing="1" cellpadding="1" width="300" border="0">
						<tr>
							<td colspan="4">
								<asp:label id="Label29" runat="server" Font-Size="11px" Font-Bold="True">Generate Profiles on a </asp:label>&nbsp;
								<asp:DropDownList id="ddlAutoAcceptRecurrence" runat="server" Width="160px" cssclass="DropDownList"></asp:DropDownList>&nbsp;
								<asp:label id="Label25" runat="server" Font-Size="11px" Font-Bold="True">basis</asp:label></td>
						</tr>
						<tr>
							<td colspan="4">
								<asp:label id="Label3" runat="server" Font-Size="11px" Font-Bold="True">Process</asp:label>&nbsp;
								<asp:DropDownList id="ddlProcessActivityType" runat="server" Width="160px" cssclass="DropDownList"
									AutoPostBack="True"></asp:DropDownList>&nbsp;
								<asp:label id="lblLastPeriods" runat="server" Font-Size="11px" Font-Bold="True">with the last</asp:label>&nbsp;
								<asp:textbox id="txtAutoAcceptPeriod" runat="server" Width="56px" CssClass="TEXTBOX"></asp:textbox>&nbsp;
								<asp:label id="lblProcessActivityPeriod" runat="server" Font-Size="11px" Font-Bold="True">monthly recurrence periods</asp:label></td>
						</tr>
						<tr>
							<td colspan="4">
								<asp:CheckBox id="chkAutoAccept" runat="server" Width="200px" cssclass="CheckBox" Text="Automatically accept profiles"></asp:CheckBox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label4" runat="server" CssClass="LABEL">Auto Accept In Amount Threshold:</asp:label></td>
							<td>
								<asp:textbox id="txtAutoAcceptInAmtThreshold" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
							<td>
								<asp:label id="Label24" runat="server" CssClass="LABEL">Auto Accept Out Amount Threshold:</asp:label></td>
							<td>
								<asp:textbox id="txtAutoAcceptOutAmtThreshold" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
						<tr>
							<td>
								<asp:label id="Label5" runat="server" CssClass="LABEL">Auto Accept In Count Threshold:</asp:label></td>
							<td>
								<asp:textbox id="txtAutoAcceptInCntThreshold" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
							<td>
								<asp:label id="Label6" runat="server" CssClass="LABEL">Auto Accept Out Count Threshold:</asp:label></td>
							<td>
								<asp:textbox id="txtAutoAcceptOutCntThreshold" runat="server" Width="160px" CssClass="TEXTBOX"></asp:textbox></td>
						</tr>
					</table>
				</asp:Panel>
<asp:Panel id="Panel13" runat="server" Width="784px" Height="250px" cssclass="Panel">
					<table id="Table8" style="WIDTH: 560px; HEIGHT: 132px" cellspacing="0" cellpadding="0"
						width="560" border="0">
						<tr>
							<td>
								<P>
									<asp:CheckBox id="chkOvrCountAndAmount" runat="server" CssClass="CheckBox" Text="Update count and amount from activity during dynamic profile generation"></asp:CheckBox></P>
								<P>
									<asp:label id="Label31" runat="server" Font-Names="Verdana, Arial, Helvetica, sans-serif" font-size="12px">
									Update profile values from risk class during dynamic profile generation for:</asp:label></P>
							</td>
						</tr>
						<tr>
							<td align="left">
								<asp:CheckBox id="chkOvrGenCasesEndOfRecurrance" runat="server" CssClass="CheckBox" Text="Generate cases at the end of recurrence period"></asp:CheckBox></td>
						</tr>
						<tr>
							<td align="left">
								<asp:CheckBox id="chkOvrTolerances" runat="server" CssClass="CheckBox" Text="Tolerances percentage"></asp:CheckBox></td>
						</tr>
						<tr>
							<td align="left">
								<asp:CheckBox id="chkOvrMinAmtandCountVariance" runat="server" CssClass="CheckBox" Text="Minimum amount and count variance"></asp:CheckBox></td>
						</tr>
						<tr>
							<td align="left">
								<asp:CheckBox id="chkOvrRetriggerTolerances" runat="server" CssClass="CheckBox" Text="Retrigger tolerances percentage"></asp:CheckBox></td>
						</tr>
						<tr>
							<td align="left">
								<asp:CheckBox id="chkOvrProfileType" runat="server" CssClass="CheckBox" Text="ProfileType"></asp:CheckBox></td>
						</tr>
						<tr>
							<td align="left">
								<asp:CheckBox id="chkOvrAutoAccept" runat="server" CssClass="CheckBox" Text="Auto Accept"></asp:CheckBox></td>
						</tr>
						<tr>
							<td align="left">
								<asp:CheckBox id="chkOvrCalculationType" runat="server" CssClass="CheckBox" Text="Calculation Type"></asp:CheckBox></td>
						</tr>
						<tr>
							<td align="left" style="height: 20px">
								<asp:CheckBox id="chkOvrAutoAcceptThresholds" runat="server" CssClass="CheckBox" Text="Auto Accept Amount/Count Thresholds"></asp:CheckBox></td>
						</tr>
                        <tr>
                            <td align="left" style="height: 20px">
                                <asp:CheckBox ID="chkOvrRecurType" runat="server" CssClass="CheckBox" Text="Recurrence Type/Value" /></td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 20px">
                                <asp:CheckBox ID="chkOvrAutoGenFreq" runat="server" CssClass="CheckBox" Text="Auto Generation Frequency" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrProcessActivity" runat="server" CssClass="CheckBox" Text="Activity to consider for Auto Generation" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrEnableProfiling" runat="server" CssClass="CheckBox" Text="Enable Profiling" /></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="chkOvrGenOptions" runat="server" CssClass="CheckBox" Text="Generation Options" /></td>
                        </tr>
					</table>
				</asp:Panel></asp:panel><asp:panel id="Panel2" runat="server" Width="712px" Height="15px" cssclass="Panel">
				<uc1:ucActivityList id="ActivityList" runat="server"></uc1:ucActivityList>
			</asp:panel><asp:panel id="Panel3" runat="server" Height="15px" Width="712px" cssclass="Panel">
				<uc1:ucSupportingInfo id="supportingInfo" runat="server"></uc1:ucSupportingInfo>
			</asp:panel><asp:panel id="Panel4" runat="server" Height="15px" Width="712px" cssclass="Panel">
				<uc1:ucNote id="ProfileNotes" runat="server"></uc1:ucNote>
			</asp:panel><asp:panel id="cPanelValidators" runat="server" Height="46px" Width="864px">
				<asp:CustomValidator id="CVCCustomerCheck" runat="server" ControlToValidate="txtCustomerId" OnServerValidate="isValidCustomer"
					display="None" ErrorMessage="The customer id does not exist in the database"></asp:CustomValidator>
				<asp:TextBox id="txtProposedProfile" runat="server" Visible="False"></asp:TextBox>
				<input id="hfldProfileHasSnapShot" type="hidden" value="-1" name="hfldProfileHasSnapShot"
					runat="server" Width="155px"/>
			</asp:panel></form>
	</body>
</html>
