USE OFAC
GO

--check listid is null
--select * from SDNAltTable where EntNum > 20000000 and Status != 4 and listid is null

--update SDNAltTable set listid = 'ALT' where EntNum > 20000000 and Status != 4 and listid is null

declare @now datetime;
set @now = getdate();

declare @ListCreateDate datetime, @ListModifDate datetime,@CreateDate datetime, @LastModifDate datetime, 
@entnum varchar(15),@SDNName varchar(3000),@Aliases varchar(max), @maxaltnum varchar(15), @location varchar(max), @sdntype varchar(30),
@AltType varchar(200), @Category varchar(200), @SDNName_tmp varchar(200), @Aliases_tmp varchar(max), @location_1 varchar(1000),@location_1_1 varchar(1000),@location_2 varchar(1000) ,@location_2_1 varchar(1000),
@oper varchar(50);
set @ListCreateDate = NULL;
set @ListModifDate = NULL;
set @CreateDate = NULL;
set @LastModifDate = NULL;
set @location_1_1 = '';

--check again and write to notifications

	set @location_1_1 = 1;
	set @Aliases_tmp = '';

declare curnoti CURSOR FOR

select distinct t1.entnum from 
( select count(1) as countt1, entnum from (select distinct entnum,altname from [OFAC].[dbo].[TmpAliasesTable] where AliaType != 'userblocked') dt group by entnum ) T1  
inner join 
( select count(1) as countt2, entnum from (select entnum,altname from SDNAltTable where EntNum > 20000000 and status !=4 and isnull(listid,'') in ('alt','') and isnull(AltType,'') = 'a.k.a.' and entnum in (select entnum from sdntable where program != 'other' and status !=4) ) dt2 group by entnum ) T2 on T1.entnum = T2.EntNum and t1.countt1 != t2.countt2 
inner join worldcheck wc on t1.entnum= wc.entnum
order by t1.Entnum

open curnoti;
fetch next from curnoti into @entnum;
WHILE @@fetch_status = 0 
begin

	if @location_1_1 <= 5 begin
	set @Aliases_tmp = @Aliases_tmp + ',' + @entnum;
	
	
	set @location_1_1 = @location_1_1 + 1;
	end
	fetch next from curnoti into @entnum;
end
close curnoti;
DEALLOCATE curnoti;

if len(@Aliases_tmp) > 1 begin

	declare cur_oper CURSOR FOR
	select Oper from PSEC..OperRights
	where [Right] = 'CTRLCLIEN'
	open cur_oper;
	fetch next from cur_oper into @oper;
	while @@fetch_status = 0 
	begin
		
		insert into PBSA..Notification (Subsystem,ObjectType, ObjectId,NotifyTime,SnoozeTime,NotifyOper,NotifyDescr,NotificationType,RecurType,RecurExpireDate,NextRecurDate)values
		('SDNALTTable DISCREPANCY','SDNALT','SDNALT',GETDATE(), getdate(),@oper,'Altname discrepancy occurs, entnum : ' + @Aliases_tmp + ', etc, please contact supprot team for more details.','SDNALT',0,null,null);
		
		fetch next from cur_oper into @oper;
	end
	close cur_oper;
	DEALLOCATE cur_oper;
end


--Nov.26.2021 add to check SDN records status

select w.Entnum, w.DelFlag, w.UPDDate, s.Status, s.LastModifDate from WorldCheck w 
join SDNTable s on w.Entnum = s.entnum
where isnull(w.DelFlag,0) = 1 and s.Status != 4

update SDNTable set Status = 4, LastModifDate = getdate() from 
WorldCheck w 
join SDNTable s on w.Entnum = s.entnum
where isnull(w.DelFlag,0) = 1 and s.Status != 4

--Nov.26.2021 disable derived names if all used sanction data rule do not check the name combination options

declare @iNoDerived int, @iEnable int

--找出使用中的sanction data rule且没有勾選Name Combination Options 及Include Concatenated Name Parts
select @iNoDerived=Count(Code) from OFAC..SanctionDataRule where AnchorOnFirstName=0 and AnchorOnLastName=0 and IncludeLastName=0 and IncludeInitials=0 and DeriveAllNameCombinations=0  and isnull(CombWords_ListType,'')='' and Enabled=1

--找出使用中的sanction data rule
select @iEnable=Count(code) from OFAC..SanctionDataRule where  Enabled=1
select @iNoDerived as noDerivedCnt, @iEnable as codeCnt

--若筆數相同，便表示skip derived name，故更新sdntable中的IgnoreDerived為1
if ( @iNoDerived=@iEnable )
begin
   select count(*) from OFAC..sdntable(nolock) where IgnoreDerived=0 and EntNum in (select EntNum from OFAC..SDNAltTable(nolock) )
   update OFAC..sdntable set IgnoreDerived=1 where IgnoreDerived=0 and EntNum in (select EntNum from OFAC..SDNAltTable(nolock) )
end

--若筆數不相同，便表示有使用 derived name，故更新sdntable中的IgnoreDerived為0
else
  update OFAC..sdntable set IgnoreDerived=0 where IgnoreDerived=1 and EntNum in (select EntNum from OFAC..SDNAltTable(nolock))


