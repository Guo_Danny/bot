USE [OFAC]
GO
/****** Object:  UserDefinedFunction [dbo].[OFS_fnGetMatchText]    Script Date: 12/24/2021 10:36:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

--OFAC CASE with OFAC Web Service , GET Match Text VALUE
ALTER FUNCTION [dbo].[OFS_fnGetMatchText](  @sSrhName VARCHAR(8000), @sMatchName VARCHAR(1000) )
RETURNS VARCHAR(200)
  As
BEGIN

		declare @testsrings varchar(1000),@sStr varchar(1000),@sStr1 varchar(1000)
		declare @sStrTmp varchar(1000), @sStrTmp1 varchar(1000), @sStrTmp2 varchar(1000)
		--declare @sSrhName varchar(1000),@sMatchName varchar(1000)
		declare @iDeff int, @iPos int, @iLen1 int , @iLen2 int, @iCnt int
		declare @ssSearchName varchar(8000),@ssMatchName varchar(1000), @sNewLine varchar(300)    --2021-12-07 add 
		declare @iProcess int                                                                     --2021-12-15 add
 

		--select @sSrhName='FINO ESTILO SHOES CO., LIMITED0501-2 DONG HUI HUA FU CHAOZHOU DAD AO, CHAOZHOU CITY GUANGDONG PROVINMC |E, CHINA'
		--Select @sMatchName='HUA_FU'
		--select @sSrhName='REAL & ESTATE INVESTMENT FUND'
		--select @sMatchName='CHINA REAL ESTATE RESEARCH ASSOCIATION'
		--select @sMatchName='PETROCHINA INTERNATIONAL (SINGAPORE)'
		--select @sSrhName='PETROCHINA INTERNATIONAL |(SINGAPORE) PTE LTD'
		--Select @sSrhName='/BENEFRES/AE123/XXXXXXXXXXXXXXXXX MANSOUR, Muna Salim Saleh |H & H METALFORM GMBH'
		--select @sMatchName='H + H'
		--select @sMatchName='H+H METALFORM'
		--select @sMatchName='H+H METALFORM MASCHINENBAU UND VERTRIEBS GMBH'
		--select @sSrhName='/0003-006 PETROCHINA INTERNATIONAL |(SINGAPORE) PTE LTD'
		--select @sMatchName='PETROCHINA INTERNATIONAL (SINGAPORE)'
	    --SELECT @sSrhName='/172007006215 CHANG CHIA-CHYANG'
		--SELECT @sMatchName='CHANG,CHI-CHIANG'
	    --SELECT @sSrhName='FLAT c 3/F, YEUNG YIU CHING'
		--SELECT @sMatchName='YEUNG YIU-CHING'
	    --SELECT @sSrhName='/652384918838 SHANGHAI MINGYU MARINE SERVICE CO. LIMITED CN'
		--SELECT @sMatchName='SHINICHI MUNEMASA'
		--SELECT @sSrhName='/ES1921008610180200007228 1/BAYNAH,Yasin Ali 2/ZZZ THE EAGLES L.L.C. YYY'
        --SELECT @sMatchName='BAYNAH, YASIN'
	    --SELECT @sMatchName='MANSOOR,MOUNA SALLIM'
		--SELECT @sMatchName='MANSOUR,MUNA SALEM SALEH'
		--SELECT @sMatchName='MUNA SALIM SALEH MANSOUR'
		--SELECT @sSrhName='/BENEFRES/AE123/XXXXXXXXXXXXXXXXX MANSOUR, MUNA SALIM SALEH |H & H METALFORM GMBH'
        --SELECT @sSrhName='ALWAYS SMOOTH CO LTD ALL ENERGY INVESTMENTS LTD MEN OF THE ARMY OF AL-NAQSHBANDIA WAY'
        --SELECT @sSrhName='SSS DANDONG ZHICHENG METALLIC MATERIAL CO., LTD. UUU'
		--SELECt @sMatchName= 'DANDONG ZHICHENG METALLIC MINERAL CO., LIMITED'	
		--SELECT @sSrhName='FINO ESTILO SHOES CO., LIMITED0501-2 DONG HUI HUA FU CHAOZHOU DAD AO, CHAOZHOU CITY GUANGDONG PROVINMC |E, CHINA'
		--SELECT @sMatchName='GUANGDONG PROVINCIAL CHINA TOBACCO INDUSTRY CO LTD'	
		--SELECT @sSrhName='/172007006215 |CHANG CHIA CHYANG NO.5,ALLEY 12,LANE 590,YUNG FENG RD.,'
	    --SELECT @sMatchName='CHANG,CHI-CHIANG'	
        --SELECT @sSrhName='ALWAYS SMOOTH CO LTD ALL ENERGY INVESTMENTS LTD MEN OF THE ARMY OF AL-NAQSHBANDIA WAY'
		--SELECt @sMatchName= 'AL -'
	    --SELECT @sSrhName='/801401076838 SHANGHAI SHYLON LIGHTING GROUP LIMITED'
		--SELECT @sMatchName='SAMSIAH SULIMAN'
        --SELECT @sSrhName='AFANASYEVA, Yulia Andreevna AGROPECUARIA Y REFORESTADORA HERREBE LTDA.'
		--SELECt @sMatchName= 'AFANASYEVA,YULIA ANDREEVNA'
        --SELECT @sSrhName='AFANASYEVA, Yulia Andreevna AGROPECUARIA Y REFORESTADORA HERREBE LTDA.'
		--SELECt @sSrhName='/0003-006 PETROCHINA INTERNATIONAL |(SINGAPORE) PTE LTD |HONG KONG |HONGYI FUTURES INVESTMENT CO LIMITED |SH |HONG PANG GEMS & JEWELLERY (HK) CO LIMITED |ZA'
		--SELECT @sMatchName='INOVEST LIMITED'
        --SELECt @sSrhName='/0003-006 PETROCHINA INTERNATIONAL |(SINGAPORE) PTE LTD |HONG KONG |HONGYI FUTURES INVESTMENT CO LIMITED |SH |HONG PANG GEMS & JEWELLERY (HK) CO LIMITED |ZA'
        --SELECT @sMatchName='KING HUNG INVESTMENT CO., LTD.'
		--SELECT @sSrhName='/0003-006 PETROCHINA INTERNATIONAL |(SINGAPORE) PTE LTD |HONG KONG |HONGYI FUTURES INVESTMENT CO LIMITED |SH |HONG PANG GEMS & JEWELLERY (HK) CO LIMITED |ZA'
        --SELECT @sMatchName='HONG PANG GEMS & JEWELLERY COMPANY LIMITED'
		--SELECt @sSrhName='USD30, TRANSPORTATION SERVICE CO LTD |TSAI, WEN-FU |SHANGHAI MINGYU MARINE SERVICE CO LIMITED CN |WORLD WAY SHIPPING AGENCY'
		--SELECT @sMatchName='SHINICHI MUNEMASA'
		--SELECT @sSrhName='USD30, |CHANG CHIA-CHYANG |TSAI, WEN-FU |SHANGHAI MINGYU MARINE SERVICE CO LIMITED CN |WORLD WAY SHIPPING AGENCY'
		--SELECT @sMatchName='WEN SHENG CHIANG'
	   -- SELECT @sSrhName='/4367519704 Mayer import LTD LIABILITY CO |83 A CENTRAL AVE RIDGFIELD PARK, N3 07660-0000'
	   --	SELECT @sMatchName='LIMITED LIABILITY COMPANY KERAMICHESKIYE'
		 --SELECT @sSrhName='USD30, TRANSPORTATION SERVICE CO LTD |TSAI, WEN-FU |SHANGHAI MINGYU MARINE SERVICE CO LIMITED CN |WORLD WAY SHIPPING AGENCY'
	     --SELECT @sMatchName='TRANSAFE SERVICES LTD.'


		select @ssSearchName=@sSrhName, @ssMatchName=@sMatchName, @sNewLine=char(13) + char(10)  --2021-12-07 add

		if isnull(@sSrhName,'')='' or ISNULL(@sMatchName,'')=''
		   goto EndEnd
		   				
		--2021-11-11 if match matched text return,start
		if (CHARINDEX(LTRIM(RTRIM(@sMatchName)), @ssrhname)>0 )
		begin
		   select @sStrTmp1=@sMatchName
		   goto endProc
		end
		--2021-11-11 if match matched text return,end

        --SELECT @sSrhName=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@sSrhName,',',' '),'-', ' '),'''',''),'.',' '),'/',' '),')',' '),'(' ,' '),'&',' '),'   ',' '),'  ',' ')
        SELECT @sSrhName=UPPER(replace(@sSrhName,'-',' ')) + ' '
		--SELECT @sMatchName=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@sMatchName,',',' '),'-', ' '),'''',''),'.',' '),'/',' '),')',' '),'(' ,' '),'&',' '),'   ',' '),'  ',' ')
		select @sMatchName=UPPER(@sMatchName) + ' '

		--2021-12-07 add for .ZA matchname, but searchname is ZA  not include '.' , Start
		--process: @sMatchName
		if  ( PATINDEX('%[.]%',@sSrhName) = 0 and PATINDEX('%[.]%',@sMatchName) > 0 )
		begin
		   if CHARINDEX('. ',@sMatchName) > 0 and CHARINDEX('.',@sMatchName) > 0
		      SELECT @sMatchName=REPLACE(@sMatchName,'.','')
           else
		      SELECT @sMatchName=REPLACE(@sMatchName,'.',' ')
		end
		--process: @sSrhName
		if  ( PATINDEX('%[.]%',@sSrhName) > 0 and PATINDEX('%[.]%',@sMatchName) = 0 )
		begin
		   if CHARINDEX('. ',@sSrhName) > 0 and CHARINDEX('.',@sSrhName) > 0
		      SELECT @sSrhName=REPLACE(@sSrhName,'.','')
           else
		      SELECT @sSrhName=REPLACE(@sSrhName,'.',' ')
		end
		--2021-12-07 add for .ZA matchname, but searchname is ZA not include '.' , end

		--2021-12-07 add for SH! matchname, but searchname is SH not include '!' , Start
		--process: @sMatchName
		if  ( PATINDEX('%[!]%',@sSrhName) = 0 and PATINDEX('%[!]%',@sMatchName) > 0 )
		begin
		   if CHARINDEX('! ',@sMatchName) > 0 and CHARINDEX('!',@sMatchName) > 0
		      SELECT @sMatchName=REPLACE(@sMatchName,'!','')
           else
		      SELECT @sMatchName=REPLACE(@sMatchName,'!',' ')
		end
		--process: @sSrhName
		if  ( PATINDEX('%[!]%',@sSrhName) > 0 and PATINDEX('%[!]%',@sMatchName) = 0 )
		begin
		   if CHARINDEX('! ',@sSrhName) > 0 and CHARINDEX('!',@sSrhName) > 0
		      SELECT @sSrhName=REPLACE(@sSrhName,'!','')
           else
		      SELECT @sSrhName=REPLACE(@sSrhName,'!',' ')
		end
		--2021-12-07 add for SH! matchname, but searchname is SH not include '!' , end	
		
		--2021-12-07 add for SearchName ',' replace ', ' , Start
		if  ( PATINDEX('%[,]%',@sSrhName) > 0 and CHARINDEX(', ',@sSrhName) = 0 and CHARINDEX(',',@sSrhName) > 0 )
		    SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,',',', '),'  ', ' ')

        if  ( PATINDEX('%[,]%',@sMatchName) > 0 and CHARINDEX(', ',@sMatchName) = 0 and CHARINDEX(',',@sMatchName) > 0 )
		      SELECT @sMatchName=REPLACE(REPLACE(@sMatchName,',',' '),'  ', ' ')	
		--2021-12-07 add for SearchName ',' replace ', ' , end	

		--2021-12-07 add for SearchName '-' replace ' ' , Start
		if  ( PATINDEX('%[-]%',@sSrhName) > 0 and CHARINDEX('- ',@sSrhName) = 0 and CHARINDEX('-',@sSrhName) > 0 )
		    SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,'-',' '),'  ', ' ')

        if  ( PATINDEX('%[-]%',@sMatchName) > 0 and CHARINDEX('- ',@sMatchName) = 0 and CHARINDEX('-',@sMatchName) > 0 )
		      SELECT @sMatchName=REPLACE(REPLACE(@sMatchName,'-',' '),'  ', ' ')	
		--2021-12-07 add for SearchName '-' replace ' ' , end		
		--2021-12-09 add for HUA_FU matchname, include '_' , Start
		--process: @sMatchName
		if ( PATINDEX('%[_+&]%',@sMatchName) > 0  or  PATINDEX('%[_+&|]%',@sSrhName) > 0  )
		begin
	        if CHARINDEX('_',@sMatchName) > 0 
		       SELECT @sMatchName=REPLACE(REPLACE(@sMatchName,'_',' '),'  ',' ')
       	    if CHARINDEX('_',@sSrhName) > 0 
		      SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,'_',' '),'  ',' ')

	        if CHARINDEX('+',@sMatchName) > 0 
		       SELECT @sMatchName=REPLACE(REPLACE(@sMatchName,'+',' '),'  ',' ')
       	    if CHARINDEX('+',@sSrhName) > 0 
		      SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,'+',' '),'  ',' ')

		    if CHARINDEX('&',@sMatchName) > 0 
		       SELECT @sMatchName=REPLACE(REPLACE(@sMatchName,'&',' '),'  ',' ')
       	    if CHARINDEX('&',@sSrhName) > 0 
		      SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,'&',' '),'  ',' ')
	    end
		--2021-12-09 add for HUA_FU matchname, include '_' ,End

					
		--2021-12-09 add for Char(13) + CHAR(10) AML INTERFERCE using '|' REPLACE, Start
		if  ( PATINDEX('%[|]%',@sSrhName) > 0  or CHARINDEX('|',@sSrhName) > 0 )
 		      SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,'|',' '), '  ',' ')

		if  ( CHARINDEX(@sNewLine,@sSrhName) > 0  )
	      SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,@sNewLine,' '),'  ',' ')	
		--2021-12-09 add for Char(13) + CHAR(10) AML INTERFERCE using '|' REPLACE, End
		
		--2021-12-10 add more replace 2 bytes space
		SELECT @sMatchName=REPLACE(@sMatchName,'  ',' ')
					
		--0. compare Search name , Match Name 
		select @sStrTmp='', @sStrTmp1=''		
		 
		--2021-09-29 bug fix, start
		--by match name, from the first word, the second word, the third word,...
		select @sStrTmp=@sMatchName, @iPos=0


		--2021-11-11 if match matched text return,start

	   if  (CHARINDEX(LTRIM(RTRIM(@sMatchName)), @ssSearchName)= 0 )
	   begin

	               SELECT @iProcess=1
	               goto GetMatchedText

GetProcess1:
			   
				  --select @sStrTmp1=REPLACE(@sMatchName,'|',@sNewLine)

			--2021-12-15 add for firstWord not Match,Start
			--SELECT '!!!GetProcess1:' Mark, @sStr FirstWord, CHARINDEX(@sstr, @ssSearchName) C1, @sStr1 LastWord,CHARINDEX(@sStr1, @ssSearchName) C2, @ssSearchName SearchText, @ssMatchName as PartyName

			if CHARINDEX(@sstr, @ssSearchName)=0 or CHARINDEX(@sstr1, @ssSearchName)= 0
			   goto Process1
			--2021-12-15 add for firstWord not Match,End

		   --select @sStrTmp1=@sMatchName
		   --2021-12-10 add for return null 
		   if len(isnull(@sStrTmp1,'')) = 0
				select @sStrTmp1 = REPLACE(@ssMatchName, '|',@sNewLine)

		   goto endProc

		end
		--2021-11-11 if match matched text return,end



		--2021-12-13 add check special character '-' , Start
		if PATINDEX('%[-]%',@ssSearchName) > 0 and PATINDEX('%[-]%',@ssMatchName) > 0
		begin
		   --SELECT  '!!!0' , PATINDEX('%[-]%',@ssSearchName) posS, PATINDEX('%[-]%',@ssMatchName) PosP
		   
           if  CHARINDEX(' -',@ssSearchName) = 0 and CHARINDEX(' -',@ssMatchName) > 0
		       --SELECT  '!!!1' ,  CHARINDEX(' -',@ssSearchName) posS,  CHARINDEX(' -',@ssMatchName) PosP
			   SELECT @sMatchName=REPLACE(@smatchname,' ','') + ' '
		end

		--select CHARINDEX(@sMatchName,@ssSearchName) as pos, @sMatchName as party
		if  CHARINDEX(RTRIM(@sMatchName),@ssSearchName) > 0
		begin 
		    SELECT @sStrTmp1=@sMatchName
			goto EndProc
        end
		--2021-12-13 add check special character '-' , end		 
		
		--2021-12-13 add check special character ',' , Start
		if PATINDEX('%[,]%',@ssSearchName) > 0 and PATINDEX('%[,]%',@ssMatchName) > 0
		begin
		   --SELECT  '!!!0' , PATINDEX('%[,]%',@ssSearchName) posS, PATINDEX('%[,]%',@ssMatchName) PosP
		   
           if  CHARINDEX(', ',@ssSearchName) = 0 and CHARINDEX(', ',@ssMatchName) > 0
		       --SELECT  '!!!1' ,  CHARINDEX(' -',@ssSearchName) posS,  CHARINDEX(' -',@ssMatchName) PosP
			   SELECT @sMatchName=REPLACE(@smatchname,', ',',') 
		end

		--select CHARINDEX(@sMatchName,@ssSearchName) as pos, @sMatchName as party
		if  CHARINDEX(RTRIM(@sMatchName),@ssSearchName) > 0
		begin 
		    SELECT @sStrTmp1=@sMatchName
			goto EndProc
        end
		--2021-12-13 add check special character ',' , end	

	--2021-12-10 add for partial match, Start
	if @sStrTmp1 <> @sMatchName
    begin
		DECLARE @iLenC1 int, @iLenC2 int, @iLenC3 int, @iCntSp1 int, @iCntSp2 int
		DECLARE @sPartyNa varchar(8000), @sPartyNa1 varchar(8000), @sPartyNa2 varchar(8000)
				   
		SELECT @sStrTmp=@ssSearchName
	    SELECT @iCntSp1=0, @iCntSp2=0
		select @iLenC1=1, @iLenC2=1, @ilenC3=1, @sStr=@sStrTmp, @sStr1=@sStr

		--count space for sMatchName , Start
		While(@iLenC2 > 0 )
		begin
		   select @iLenC2=CHARINDEX(' ',@sMatchName, @ilenC2)
		   if @iLenC2 > 0
              SELECT @iCntSp1=@iCntSp1+1 ,@iLenC2=@iLenC2+1
           --select @iLenC2 as iC2,  @iCntSp1 CNT
		end
		--count space for sMatchName , End
		
		--Do TransactionText , Start
		SELECT @sStr1=SUBSTRING(@sStr,PATINDEX('%[ ]%',@sStr)+1,len(@sStr)-PATINDEX('%[ ]%',@sStr))
		SELECT @sStr=@sStr1
		SELECT @sPartyNa='', @sPartyNa1='', @sPartyNa2=''
		--SELECT '!!' mark, @sStr as SearchText

		SELECT @iLenC2=1
		While( @iLenC2> 0 )
		begin
		    IF SUBSTRING(@sStr, @iLenC2, 1) = ' ' or SUBSTRING(@sStr, @iLenC2, 1) = '-' or SUBSTRING(@sStr, @iLenC2, 1) = ','
			begin
		     
                SELECT @sPartyNa1=SUBSTRING(@sStr, 1, @iLenC2)
			    SELECT @sPartyNa= @sPartyNa1
			    SELECT @iCntSp2=@iCntSp2+1
				--SELECT '!!' mark,@sPartyNa as SearchText, @iCntSp2 as cnt, @iLenC2 pos, SUBSTRING(@sStr, 1, @ilenC2) party
			end

			--SELECT @iLenC2=@iLenC2+1
			if @iCntSp1=@iCntSp2
			   SELECT @iLenC2=0
            else
			   SELECT @ilenC2=@iLenC2+1
            
			--SELECT '!!' mark,@sPartyNa as SearchText, @iCntSp2 as cnt, @iLenC2 pos, SUBSTRING(@sStr, 1, Len(@sStr)-@ilenC2) party
		end

		SELECT @sStrTmp1=REPLACE(@sPartyNa,'|',@sNewLine)
		--Do TransactionText , End
					   
		
		if DIFFERENCE(@sPartyNa,@sMatchName) <> 0
		begin
				--select @sStrTmp1 as SearchText, @sMatchName as PartyName
				SELECT @iLenC1=0
				goto EndProc
         end

    end
	--2021-12-10 add for partial match, End

Process1:

		while (@iPos < len(@sStrTmp) )
		begin
		  IF ( (CHARINDEX(' ',@sStrTmp) > 0 AND  PATINDEX('%[,]%',@sStrTmp) = 0 ) or  
		       (PATINDEX('%[,]%',@sStrTmp) > 0 and CHARINDEX(' ',@sStrTmp) <  PATINDEX('%[,]%',@sStrTmp) ) 
			 ) 
		  BEGIN
		     select @sStrTmp1=SUBSTRING(@sStrTmp,@iPos+1,CHARINDEX(' ',@sStrTmp)-1) 
			 select @sStrTmp2='%' + @sStrTmp1 +'%'
		     select @iDeff=PATINDEX(@sStrTmp2,@sSrhName)
			
			 if (@iDeff > 0)
		       select @iPos=Len(@sStrTmp)+1
             else
		       select @iPos=CHARINDEX(' ',@sStrTmp,@iPos+len(@sStrTmp1)-2)  
		  END

		 ELSE
		  IF ( CHARINDEX(' ',@sStrTmp) >  PATINDEX('%[,]%',@sStrTmp)  )
		  BEGIN
		     select @sStrTmp1=SUBSTRING(@sStrTmp,@iPos+1,CHARINDEX(' ',@sStrTmp)-1) 
			 select @sStrTmp2='%' + @sStrTmp1 +'%'
		     select @iDeff=PATINDEX(@sStrTmp2,@sSrhName)
			
			 if (@iDeff > 0)
		       select @iPos=Len(@sStrTmp)+1
             else
		       select @iPos=CHARINDEX(' ',@sStrTmp,@iPos+len(@sStrTmp1)-2)  
		  END
		  
		  IF ( @iDeff=0  and PATINDEX('%[,]%',@sStrTmp)  > 0 )
			 BEGIN
			    select @sStrTmp1=SUBSTRING(@sStrTmp,@iPos+1,PATINDEX('%[,]%',@sStrTmp))  --2021-09-29 BUG FIX
			    select @sStrTmp2='%' + @sStrTmp1 +'%'
		        select @iDeff=PATINDEX(@sStrTmp2,@sSrhName)			
				
			    --SELECT @sstr as TMP,@SSTRTMP1 AS TMP1, @sStrTmp2 AS TMP2, @sSrhname as SearchText ,@iDeff AS DIFF

			    if (@iDeff > 0)
		           select @iPos=Len(@sStrTmp)+1
                else
				   select @iPos=@iPos+len(@sStrTmp1)
             END

        end
		--2021-09-29 bug fix, end

		-- if partial match
		if ( @iDeff > 0 )
		begin
		   select @icnt=0, @iLen1=1, @iLen2=1, @sStr=@sMatchName, @sStrTmp='', @sStrTmp1=''
		   while( @iLen2 !=0 )
		   begin
			   
			  select @iLen2=CHARINDEX(' ', @sStr,@ilen1)  
			  if ( @ilen2=len(@sstr) )
			  begin
				  select @testsrings=substring(@sStr,@ilen1,@ilen2)
				  select @sStr=REPLACE(@sStr,@testsrings,'')
				  --select @ilen2 as i2, @testsrings as Tstr,@sStr as MatchName
			  end

			  else	  
			  begin			     
			      select @iLen2=PATINDEX('%[ ,]%', @sStr) 
				  select @testsrings=substring(@sStr,@ilen1,@ilen2)
				  ----2021-11-11 bugfix when the duplicate word in MatchName & Process last word in MatchName , start
				  --select @sStr=ltrim(REPLACE(@sStr,@testsrings,''))  --bug must be mask and change
				  if ( @iLen2 > 0 and len(@sStr) > 0 and @testsrings <> '' ) --last word of matchname
				     select @sStr=SUBSTRING(@sStr,len(@testsrings)+2,len(@sStr)-len(@testsrings)-1)  
				  else 
				      select @testsrings=@sStr
                  ----2021-11-11 bugfix when the duplicate word in MatchName & Process last word in MatchName , end
				  --select @ilen2 as i2, @testsrings as Tstr,@sStr as MatchName
			  end

			  -- 1. Search Singal word in Search name
			  --if @teststring have one special char ,- , and into @sSrhname did not find, then delete space
              if (  PATINDEX('%[,]%', @testsrings) > 0 and PATINDEX('%[ ]%', @testsrings) > 0 and CHARINDEX(@testsrings, @sSrhName)= 0 )
			     select @testsrings=REPLACE(@testsrings,' ','')

			  select @iPos=CHARINDEX(@testsrings, @sSrhName)
			  
			 --select @iPos as Pos, @testsrings as Str, @sSrhName as SearchName

			  if ( @iPos > 0 )
			  begin      
				  select @sStrTmp = RTRIM(@testsrings)
                  
				  if (@sStrTmp1 = '' )   -- first word
					 select @sStrTmp1=RTRIM(@testsrings)

				  else if (@sStrTmp1 <> '' )   -- not first word
				  begin	     
					 --2.  Search two  words in Search name
					 if ( CHARINDEX(@sStrTmp1 + ' ', @sSrhName) > 0 ) --if srhname after @sStrTmp1  have space
					    select @sStr1 =  RTRIM(@sStrTmp1) + ' ' + RTRIM(@testsrings)
                     else --if srhname after @sStrTmp1 no space
					    select @sStr1 =  RTRIM(@sStrTmp1) +  RTRIM(@testsrings)

					 select @iPos = CHARINDEX(@sStr1, @sSrhName)
					 --select @iPos pos, @sStr1 as sStr , @sSrhName as SearchName

					 if @ipos > 0
					 begin
					   if ( CHARINDEX(@sStrTmp1 + ' ', @sSrhName) > 0 ) --if srhname after @sStrTmp1  have space
						 SELECT @sStrTmp1 = RTRIM(@sStrTmp1) + ' ' + RTRIM(@sStrTmp)
                       else --if srhname after @sStrTmp1 no space
					     SELECT @sStrTmp1 = RTRIM(@sStrTmp1)  + RTRIM(@sStrTmp)
                     end

				  end

			  end

			  select @iCnt=@iCnt+1
			  if  @iCnt > 100 or @iLen1 =0
			  begin
			      Select @ilen1=0
				  goto ExitLoop
              end

		   end
		end

ExitLoop:

			--2021-12-15 add bug fix, Start
			if ISNULL(@sStrTmp1,'')<>'' and CHARINDEX(@sStrTmp1, @sMatchName)> 0
				SELECT @sMatchName=@sStrTmp1
				  
			--select '!!!' Mark, @sStrTmp1 MatchText, @iCnt iCnt, @iLen1, @sMatchName PartyName
			--2021-12-15 add bug fix, End

           --select '!!!-ExitLoop:' Mark, @sStrTmp1 MatchText, @iCnt iCnt, @iLen1, @sMatchName PartyName, @ssSearchName SearchName, CHARINDEX(LTRIM(RTRIM(@sMatchName)), @ssSearchName) C1

		   if  ( CHARINDEX(LTRIM(RTRIM(@sMatchName)), @ssSearchName)= 0 or len(@sStrTmp1) = 0)
		   begin
		         
		          SELECT @iProcess=2
				  goto GetMatchedText

GetProcess2:


		   end


		if Len(RTRIM(@sStrTmp1)) = 0 
		   select @sStrTmp1=SUBSTRING(@sSrhName,1,350)

		else if Len(RTRIM(@sStrTmp1)) >= 350
		   select @sStrTmp1=SUBSTRING(@sStrTmp1,1,350)


EndProc:
  
  	    --2021-12-20 add for Double Check Match Party name, Start		

		--SELECT '!!!-EndProc-0:' Mark,@sStrTmp1 rText, @ssMatchName partyname, CHARINDEX(@sStrTmp1,@ssMatchName) c, DIFFERENCE(@sStrTmp1, @sMatchName) d, @iLen1 i1, @iLen2 i2

        if (CHARINDEX(@sStrTmp1,@ssMatchName) = 0  and DIFFERENCE(@sStrTmp1, @ssMatchName) <> 4 )  and  ( ABS(LEN(@sStrTmp1)-LEN(@ssMatchName)) > 5 or ABS(LEN(@ssMatchName) - LEN(@sStrTmp1)) >5 )  
			begin
			    
				if PATINDEX('%[-&,.]%', @ssMatchName) > 0 
					SELECT @iCnt=PATINDEX('%[-&,.]%', @ssMatchName), @sStr=SUBSTRING(@ssMatchName,1,@icnt-1), @iLen1=CHARINDEX(@sStr,@ssSearchName), @ilen2=CHARINDEX(@sStr1,@ssSearchName, @ilen1+len(@sStr)+1)
                else
				begin
					SELECT @iLen1=CHARINDEX(@sStr,@ssSearchName,@iLen1+len(@sStr)+1), @icnt=@iLen1,  @sStr=SUBSTRING(@ssMatchName,1,@icnt), @ilen2=Len(@ssSearchName)                          
                end

			    SELECT @iLenC1=1, @iLenC2=1, @iLenC3=1, @sStrTmp2='', @iCnt=1
				WHILE(@icnt > 0  and @iLenC3 > 0)
				BEGIN
				    if CHARINDEX(' ', @ssMatchName,@iLenC1) > 0
					    SELECT @iLenC1=@iLenC2, @iLenC2=CHARINDEX(' ', @ssMatchName,@iLenC1)+1, @iCnt=@iLenC2
                    ELSE
					    SELECT @iLenC1=@iLenC2, @iLenC2=len(@ssMatchName), @iCnt=@iLenC2

					SELECT @sPartyNa1=SUBSTRING(@ssMatchName, @iLenC1, @iLenC2-@iLenC1)		
					   		 
				    --SELECT '!!!EndProc-1-1:' Mark, @sPartyNa1 word, @iLenC1 i1, @iLenC2 i2, @sStrTmp1 mText, @iLenC3 c3, @sStrTmp2 s2, @iCnt cnt, @iLen1 i1, DIFFERENCE(@sStrTmp1,@ssMatchName) d
                    
					if CHARINDEX(@sPartyNA1, @ssSearchName) > 0 
						SELECT @sStrTmp2=@sStrTmp2 + @sPartyNa1, @iLenC3=@iLenC3+1, @iCnt=CHARINDEX(@sPartyNA1, @ssSearchName)
                    
					else  
					Begin

						if @iLenC3 > 0 and ISNULL(@sStrTmp2,'' ) <> '' 
							SELECT @iLenC3=0, @iCnt=0, @iLen1=CHARINDEX(@sStrTmp2, @ssMatchName), @iLen2=@iLen1+len(@sStrTmp2)				 
						else
							SELECT @iLenC3=@iLenC3+1
                    end

					 --SELECT '!!!EndProc-1-2:' Mark, @sPartyNa1 word, @sStrTmp1 mText, CHARINDEX(@sPartyNA1, @sStrTmp1) c
					 --Partial match
					 if  CHARINDEX(@sPartyNA1, @sStrTmp1) = 0 
					 begin
					     SELECT @iLenC3=CHARINDEX(' '+SUBSTRING(@sPartyNa1,1,1),@sStrTmp1, @iLenC1+1)
						 if @iLenC3=0
						    SELECT @iLenC3=CHARINDEX('|'+SUBSTRING(@sPartyNa1,1,1),@sStrTmp1, @iLenC1+1)
						 if @iLenC3=0
						    SELECT @iLenC3=CHARINDEX(SUBSTRING(@sPartyNa1,1,2),@sStrTmp1, @iLenC1+1)

						 if @iLenC3 > 0
						 begin
						    SELECT @ilenC1=CHARINDEX(' ',@sStrTmp1, @iLenC3)
							if @iLenC1 = 0 
							   SELECT @iLenC1=LEN(@sStrTmp1)
                           
						    SELECT @sPartyNa1=SUBSTRING(@sStrTmp1, @iLenC3,@iLenC1-@iLenC3+1)
							
							--if CHARINDEX(@sPartyNa1, @ssSearchName) >0
							   SELECT @sPartyNa2=SUBSTRING(@sStrTmp1, @iLenC3,LEN(@Sstrtmp1)-@ilenC3+1), @sStrTmp1=@sPartyNa2, @iCnt=0, @iLenC3=0
                             
							 --SELECT '!!!EndProc-1-3:' Mark, @sPartyNa1 word,  @sPartyNa2 word2,@sStrTmp1 mText, CHARINDEX(@sPartyNA1, @ssSearchName)
                         end
			     		      
                     end
					
				    SELECT @iLenC1=@iLenC2+1 

					--SELECT '!!!EndProc-1-4:' Mark, @icnt cnt,@iCntSp1 cnt2,@sStrTmp1 mText, @sPartyNa1 s1, @iLenC3 i3
               
			    END
				
            end      

		--SELECT '!!!EndProc-1-x:' Mark, @sStrTmp1 mText, @ssMatchName mName
	
		goto EndEnd


GetMatchedText:
                  
				--space issue that make not match, Start
				if CHARINDEX(@sMatchName, @ssSearchName) = 0
				BEGIN
					SELECT @sPartyNa=REPLACE(@sMatchName,' ','')
					SELECT @sPartyNa1=REPLACE(@sStr+ @sStr1,' ','')
					if  CHARINDEX(@sPartyNa,@ssSearchName) > 0
					BEGIN
						SELECT @sStrTmp1=@sPartyNa
						--SELECT '!!!-GetMatchedText:' Mark,@sStr FirstName, @sStr1 LastName, @sPartyNa P,  @sPartyNa1 P1, @sPartyNa2 P1, @sStrTmp1 MatchText
						goto EndProc
					END
				END
				--space issue that make not match, Start

		           SELECT @sStr='', @sStr1='',@ilen1=1, @ilen2=1

				   --FIRST Word
				   SELECT @iLen1=CHARINDEX(' ',@sMatchName,@iLen1)  --+1
				   SELECT @sStr=SUBSTRING(@sMatchName,1,@iLen1)

				  
				   --Last Word
				   SELECT @iLen2=Len(@sMatchName)-1, @iCnt=1
				   while( @iCnt > 0 )
				   BEGIN
				       if substring(@sMatchName,@iLen2,1)=' '
					   BEGIN
					      SELECT @sStr1=SUBSTRING(@sMatchName,@iLen2+1,Len(@sMatchName)-@iLen2)
						  SELECT @iCnt=0
					   END
					   SELECT @iLen2=@iLen2-1
				   END
                  
				   --SELECT '!!!-GetMatchedText-0:' mark, @sStr as firstWord, @sStr1 as LastWord , len(@sstr) len1, len(@sstr1) len2, @iLen1 P1, @iLen2 P2
				  				   
				   --Fist Word St location#
				   IF   LEN(@sStr) >  1
				      SELECT @iLenC1=CHARINDEX(REPLACE(REPLACE(@sSTR,',',', '),'  ',' '), REPLACE(REPLACE(@ssSearchName,',',', '),'  ',' '),1)   
			  			   
				   IF  LEN(@sStr) = 1
				   BEGIN 
				      SELECT @iLen1=0, @iLen2=0
				      SELECT @iCnt=1
					  SELECT @iLen1=CHARINDEX(@sSTR, @ssSearchName,1) 
					  SELECT @sStrTMP=SUBSTRING(@ssSearchName,@ilen1-1, 1) 
					
					  if PATINDEX('%[ |]%',@sStrTmp) = 0
					  BEGIN
						  While ( @iCnt > 0 )
						  BEGIN	     
								 --SELECT '!!3.' mark, @ilen1 pos1, @sStr	firstword
								 SELECT @iLen1=@iLen1+1, @iCnt=@iCnt+1
								 if SUBSTRING(@ssSearchName,@ilen1,len(@sstr))  = @sStr
									SELECT @iCnt=0                           
						  END
					  END		  	  
                   END
				   
				--2021-12-17 add Get SearchName Location#
				--Last Word End location#	

				SELECT @iLen1=CHARINDEX(LTRIM(RTRIM(@sStr)),@ssSearchName)

				--2021-12-20 add check must new word,Start
				if SUBSTRING(@ssSearchName,@ilen1-1,1) <> ' ' and SUBSTRING(@ssSearchName,@ilen1-1,1) <> '-' and  SUBSTRING(@ssSearchName,@ilen1-1,1) <> ',' and  SUBSTRING(@ssSearchName,@ilen1-1,1) <> '|'
				    SELECT @iLen1=CHARINDEX(@sStr, @ssSearchName,@iLen1+1) 
				--2021-12-20 add check must new word,End				   
							
				SELECT @iLen2=CHARINDEX(LTRIM(RTRIM(@sStr1)),@ssSearchName,@ilen1+1) , @icnt=@iLen2
								  
			
				--SELECT '!!!-GetMatchedText0-a:' mark,@sMatchName PartyName, @ssSearchName SearchText, @sStr first, @sStr1 last,@iLen1 l1, @iLen2 l2

				--Last Word not in SearchName, Try to ReGet , Start	
				if @iCnt=0 or @iLen1 > @ilen2   
				begin
				   -- Using PartyName Vs. SearchName
				   SELECT @iCntSp1=0, @iCntSp2=0, @iCnt=LEN(@sStr)+1,@iLenC2=Len(@sStr)+1
				   SELECT @iLen1=CHARINDEX(@sStr,@ssSearchName)
				   WHILE( @iCnt > 0  AND @iCntSp2 < 4 )
				   BEGIN
				      SELECT @iCnt=@iCnt+1	      
					  if SUBSTRING(@sMatchName,@iCnt,1)=' '
					  begin
						  SELECT @iCntSp2=@iCntSp2+1,@iLenC1=@iLenC2
                          if @iCntSp2 >= 2 and @icnt=LEN(@ssSearchName)
						  Begin
							 SELECT @ilenC2=@iCnt
							 SELECT @sStr1=SUBSTRING(@sMatchName, @iLenC2, @iCnt-@iLenC1+1)
  					         
							 --SELECT '!!!-GetMatchedText0-b:' mark, @sStr1 lastWord,@iCntSp2 CntSP,@iLenC1 len1, @iLenC2 len2, @icnt icnt                    	     
						  end
					  end                          
				   END

				end
				--Last Word not in SearchName, Try to ReGet , end	

				--SELECT '!!!-GetMatchedText-1:' mark, @ilen1 len1, @ilen2 len2, @sStr1 LastWord, DIFFERENCE(@sStr1, @ssSearchName) C2


				IF @iLen1 <>0 AND @iLen2 <>0 	
				   begin		   
				       if Len(@sStr1)=1 
					   begin
					      if SUBSTRING(@ssSearchName,@ilen1-1,1) <> ' ' and SUBSTRING(@ssSearchName,@ilen1-1,1) <> '-' and  SUBSTRING(@ssSearchName,@ilen1-1,1) <> ',' and  SUBSTRING(@ssSearchName,@ilen1-1,1) <> '|'
                             SELECT @iLen1=CHARINDEX(LTRIM(RTRIM(@sStr)),@ssSearchName, @ilen1+1), @iLen2=CHARINDEX(LTRIM(RTRIM(@sStr1)),@ssSearchName,@ilen1+1) , @icnt=@iLen2	
				          SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@ilen1, @ilen2-@ilen1+1) 
                       end

                       else
					       SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@ilen1, @iLen2+len(@sstr1)-@iLen1) 
						  
					   SELECT @sStrTmp1=@sStrTmp2
					   --SELECT '!!!-GetMatchedText-1-a:' mark, @ilen1 len1, @ilen2 len2,@sStrTmp2 matchText, CHARINDEX(@sStrTmp2, @ssSearchName) C1, DIFFERENCE(@sStrTmp2, @sMatchName) C2

					   if CHARINDEX(@sStrTmp2, @ssSearchName,@iLen1) >0  or DIFFERENCE(@sStrTmp2, @sMatchName) > 0
					       goto EndProc
				   end 

				   ELSE 
				   BEGIN

				       IF @iLen1=0 AND @ILEN2 <> 0
					   begin
		      		      SELECT @iLen1=CHARINDEX('|' + SUBSTRING(@sStr,1,2),@ssSearchName) + 1						 
						  if @iLen1=1
						     SELECT @iLen1=CHARINDEX('|' + SUBSTRING(@sStr,1,1),@ssSearchName) + 1
                          if @iLen1=1
						     SELECT @iLen1=CHARINDEX(' ' + SUBSTRING(@sStr,1,2),@ssSearchName) + 1						 
						  if @iLen1=1
						     SELECT @iLen1=CHARINDEX(' ' + SUBSTRING(@sStr,1,1),@ssSearchName) + 1
						  if @iLen1=1
						     SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,3),@ssSearchName)
						  if @iLen1=0  
						     SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,2),@ssSearchName)

						  if @ilen1=0						
						     SELECT @ilen1=LEN(@ssSearchName)


                          SELECT @iCnt=CHARINDEX(' ',@ssSearchName,@iLen1+1)
						  if @iCnt=0
						     SELECT @iCnt=Len(@ssSearchName)
						  if @iCnt > @iLen1
					         SELECT  @sStrTmp=SUBSTRING(@ssSearchName,@ilen1, @icnt-@iLen1+1)
                          else
						     SELECT @sStrTmp=''
                          
						  --SELECT '!!!-ReGetLocFirst-1-2:'Mark, @iLen1 i1, @iLen2 i2, @sStr first, @sStr1 last, @sStrTmp1 mtext,SUBSTRING(@ssSearchName,@ilen1,20) s1, @iCnt cnt

--2021-12-20 add RETRY GET FIRST WORD
ReGetLocFirst:

		--ReGet fist word location, Start
		if @ilen1=0
		begin
			SELECT @iLen1=CHARINDEX('|'+SUBSTRING(@sStr,1,1), @ssSearchName, @ilen1+1)+1
			if @iLen1=1
			   SELECT @iLen1=CHARINDEX(' '+SUBSTRING(@sStr,1,1), @ssSearchName, @ilen1+1)+1
            if @ilen1=0
			   SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+1)
        end

/*
		--2021-12-21 add for move next, Start
		if @ilen1 + len(@sMatchName) < @ilen2
		begin
		    SELECT @iCnt=@ilen1+len(@sStr)+1
			SELECT @iLen1=@iCnt
		   	SELECT @iLen1=CHARINDEX('|'+SUBSTRING(@sStr,1,1), @ssSearchName, @iLen1)
			if @iLen1=0
			   SELECT @ilen1=@icnt, @iLen1=CHARINDEX(' '+SUBSTRING(@sStr,1,1), @ssSearchName, @ilen1+1)+1
            if @ilen1=1
			   SELECT @ilen1=@icnt,@iLen1=CHARINDEX(SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+1)
		end
		---2021-12-21 add for move next, End
*/

        --2021-12-22 add for first Loc# = Last Loc#, Reget Last Loc@, Start
		if @iLen1 <= @iLen2
		BEGIN
		    SELECT @iCnt=@ilen1+len(@sStr)+1
			SELECT @iLen2=@iCnt
			SELECT @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,2), @ssSearchName, @iCnt)
		    if @iLen2=0
		     	SELECT @iLen2=CHARINDEX('|'+SUBSTRING(@sStr1,1,2), @ssSearchName, @iCnt) +1
			if @iLen2=0
			   SELECT @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @ssSearchName, @iCnt) +1
            if @iLen2=1
			   SELECT @iLen2=CHARINDEX(SUBSTRING(@sStr1,1,2), @ssSearchName,@iCnt)
		END
        --2021-12-22 add for first Loc# = Last Loc#, , Reget Last Loc@, end

		--SELECT '!!!-ReGetLocFirst-2:'Mark, @iLen1 i1, @iLen2 i2, @sStr first, @sStr1 last, @sStrTmp1 mtext,SUBSTRING(@ssSearchName,@ilen1,20) s1, @iCnt cnt

		--get Party Name next word???
		if @ilen1=0
		begin
		   SELECT @iCnt=1, @iLen1=@iCnt
		   SELECT @icnt=CHARINDEX(' ',@ssMatchName, @iCnt ) +1
		   if @icnt=1
		      SELECT @icnt=LEN(@ssSearchName)
		   if @icnt<>LEN(@ssSearchName)
		      SELECT @sStrTmp=SUBSTRING(@ssMatchName, @iCnt, CHARINDEX(' ',@ssMatchName, @iCnt )-@icnt )
           else
		     SELECT @sStrTmp=SUBSTRING(@ssMatchName, @ilen1, @icnt-@iLen1+1)

		   if CHARINDEX(@sStrTmp, @ssSearchName) > 0
		      SELECT @sStr=@sStrTmp, @iLen1=CHARINDEX(@sStrTmp, @ssSearchName)
		end
		
		else   
		begin
			SELECT @iCnt=CHARINDEX(' ',@ssSearchName,@iLen1+1)
			if @iCnt=0
			   SELECT @iCnt=Len(@ssSearchName)
			SELECT @sStrTmp=SUBSTRING(@ssSearchName,@ilen1,@icnt-@iLen1)
	    end
        --ReGet fist word location, End
		
											 							    
						  if  LEN(@sStr) > LEN(@sStrTmp) and LEN(@sStr) - LEN(@sStrTmp) > 3
						  BEGIN
						     SELECT @iLen1=CHARINDEX(' '+SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+len(@sStrTmp)+1)
							 if @iLen1 = 0 
							    SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+len(@sStrTmp)+1)
							 --if @iLen1 = 0 
							 --   goto ReGetLocFirst
							 --SELECT '!!!-GetMatchedText-1-a-4-a:' mark, @ilen1 len1, @ilen2 len2,@sStr first, @sStrTmp s1, @sStr1 last, Len(@sStr) len1,len(@sStrTmp) len2
							 SELECT @sStr=@sStrTmp
						  END

						  ELSE if LEN(@sStr) <> LEN(@sStrTmp)
						  BEGIN
						     if @ilen1=0
						        SELECT @iLen1=CHARINDEX(' '+SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+len(@sStrTmp)+1)
							 if @ilen1=0
							    SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+len(@sStrTmp)+1)
							 --if @iLen1 = 0 
							 --   goto ReGetLocFirst							 
							 --SELECT '!!!-GetMatchedText-1-a-4-b:' mark, @ilen1 len1, @ilen2 len2,@sStr first, @sStrTmp s1, @sStr1 last, Len(@sStr) len1,len(@sStrTmp) len2
							 SELECT @sStr=@sStrTmp
						  END

						  ELSE
						  BEGIN
						     --ReGet fist word location
							 if @iLen1=0
						        SELECT @iLen1=CHARINDEX(' '+SUBSTRING(@sStr,1,1), @ssSearchName, @ilen1+len(@sStrTmp)+1)+1
							 if @iLen1=1
							    SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+len(@sStrTmp)+1)
                             --if @ilen1=0
							 --    goto ReGetLocFirst	
							 --SELECT '!!!-GetMatchedText-1-a-4-c:' mark, @ilen1 len1, @ilen2 len2,@sStr first, @sStrTmp s1, @sStr1 last, Len(@sStr) len1,len(@sStrTmp) len2
							 SELECT @sStr=@sStrTmp
						  END

                        -- SELECT '!!!-GetMatchedText-1-a-4:' mark, @ilen1 len1, @ilen2 len2,@sStr first, @sStrTmp s1, @sStr1 last, Len(@sStr) len1,len(@sStrTmp) len2

					   end
					   
                      
                    IF @iLen1<>0 AND @iLen2 =0 
					begin
					    SELECT @iLen2=CHARINDEX(' ' + SUBSTRING(@sStr1,1,3),@ssSearchName, @iLen1+1) + 1
						if @iLen2=1
						    SELECT @iLen2=CHARINDEX(' ' + SUBSTRING(@sStr1,1,2),@ssSearchName, @iLen1+1) + 1
						if @iLen2=1
						    SELECT @iLen2=CHARINDEX(' ' + SUBSTRING(@sStr1,1,1),@ssSearchName, @iLen1+1) + 1
						if @iLen2=1
						    SELECT @iLen2=CHARINDEX(SUBSTRING(@sStr1,1,3),@ssSearchName, @iLen1+1)
						if @iLen2=0
						    SELECT @iLen2=CHARINDEX(SUBSTRING(@sStr1,1,2),@ssSearchName, @iLen1+1)
                        if @ilen2=0
						    SELECT @iLen2=LEN(@ssSearchName)

                        SELECT @iCnt=CHARINDEX(' ',@ssSearchName,@iLen2+1)
						if @iCnt=0
						    SELECT @iCnt=Len(@ssSearchName)

						  --SELECT '!!!-GetMatchedText1-a: ' Mark, @sStrTmp1 TransactionText, @sstr FirstName, @sStr1 LastName, @ssSearchName SearchText, @ilen1 l1, @iLen2 l2, @iCnt icnt
						 
						 if @iCnt > @iLen2
					         SELECT  @sStr1=SUBSTRING(@ssSearchName,@ilen2, @icnt-@iLen2+1)
                          else
						     SELECT @sStr1=''
				   end

				   --2021-12-17 add for Fist word & Last Word is partial match, Start
                   IF @iLen1=0 AND @iLen2 =0 
				   begin
					    --SELECT '!!!-GetMatchedText-1-d:' mark, @ilen1 len1, @ilen2 len2,@sStr first, @sStr1 last, @ssMatchName PartyName
					    --First Name
						  if @iLen1=1	
						  begin					 
						     SELECT @iLen1=CHARINDEX(' ' + SUBSTRING(@sStr,1,1),@ssSearchName) + 1
 							 if @ilen1 > 1 and 	CHARINDEX(' ' + SUBSTRING(@sStr1,1,1),@ssSearchName, @iLen1+1) > @iLen1+1
							     SELECT @iLen2=CHARINDEX(' ' + SUBSTRING(@sStr1,1,1),@ssSearchName, @iLen1+1) + 1
                          end

						  if @iLen1=1
						  begin
						     SELECT @iLen1=CHARINDEX('|' + SUBSTRING(@sStr,1,1),@ssSearchName) +1
							  if @ilen1 > 1 and CHARINDEX(' '+ SUBSTRING(@sStr1,1,1),@ssSearchName, @iLen1+1) > @iLen1+1
							     SELECT @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName, @iLen1+1) 
                          end

						  --2020-12-23 CHANGE BUG FIX, START
						  if @iLen1=0 
						  begin
						     SELECT @iLen1=CHARINDEX(LTRIM(RTRIM(@sStr)),@ssSearchName)
							 if @iLen1=0
						        SELECT @iLen1=CHARINDEX('|'+SUBSTRING(@sStr,1,2),@ssSearchName), @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
							 if @ilen1=0
							    SELECT @iLen1=CHARINDEX(' '+SUBSTRING(@sStr,1,2),@ssSearchName), @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
                             if @ilen1=0
							    SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,2),@ssSearchName), @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
                             if @ilen1=0
							    SELECT @iLen1=CHARINDEX(' '+SUBSTRING(@sStr,1,1),@ssSearchName), @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
                             if @ilen1=0
							    SELECT @iLen1=CHARINDEX('|'+SUBSTRING(@sStr,1,1),@ssSearchName), @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)

							 if @ilen2=0
							    SELECT @iLen2=CHARINDEX('|'+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
							 if @ilen2=0
							    SELECT @iLen2=CHARINDEX(SUBSTRING(@sStr1,1,2),@ssSearchName,@iLen1+1)

							 --2021-12-23 ADD 
                             IF  SUBSTRING(@ssSearchName,@ilen2,1)<>SUBSTRING(@sStr1,1,1)
							 BEGIN
						       SELECT @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,2),@ssSearchName,@iLen1+1)
							   IF @iLen2=0
							      SELECT @iLen2=CHARINDEX('|'+SUBSTRING(@sStr1,1,2),@ssSearchName,@iLen1+1)
                               IF @iLen2=0
							      SELECT @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
                               IF @iLen2=0
							      SELECT @iLen2=CHARINDEX('|'+SUBSTRING(@sStr1,1,2),@ssSearchName,@iLen1+1)
                             END
						  
						  end
			
			             --2020-12-23 CHANGE BUG FIX, END
			
						 --SELECT '!!!-GetMatchedText-1-d-1:' mark, @ilen1 len1, @ilen2 len2,@icnt cnt,@sStr first, @sStr1 last, SUBSTRING(@ssSearchname,@ilen1,10)
						 
						  if @ilen1=0 			
						     SELECT @ilen1=1
						  else if @ilen2=0
						    if CHARINDEX('|',@ssSearchName,@iLen1) > @iLen1
						       SELECT @ilen2=CHARINDEX('|',@ssSearchName,@iLen1)-1
                            else 
							   SELECT @ilen2=len(@ssSearchName)

                        --SELECT @iCnt=CHARINDEX(' ',@ssSearchName,@iLen2+1)
						--if @iCnt=0
						    SELECT @iCnt=@iLen2

					    --SELECT '!!!-GetMatchedText-1-d-2:' mark, @ilen1 len1, @ilen2 len2,@icnt cnt,@sStr first, @sStr1 last
					  
					end					   
					--2021-12-17 add for Fist word & Last Word is partial match, End


					--SELECT '!!!-GetMatchedText1-d-3: ' Mark, @sstr FirstName, @sStr1 LastName,@ilen1 l1, @iLen2 l2, @iCnt icnt--, SUBSTRING(@ssSearchName,@ilen1, @ilen2-@ilen1)
					   
					--2021-12-17 Change, Start					   
					if @ilen2 > @iLen1
					begin
					    if CHARINDEX(' ',@ssSearchName,@iLen2+1) > 0
					       SELECT @iCnt=CHARINDEX(' ',@ssSearchName,@iLen2+1)
						else  
						    SELECT @iCnt=len(@ssSearchName)
						
					   SELECT @sStrTmp2=LTRIM(SUBSTRING(@ssSearchName,@iLen1, @iCnt-@iLen1+1)), @sStrTmp=@sStrTmp2
				       
					   --SELECT '!!!-GetMatchedText1-d-3-1: ' Mark, @sstr First, @sStr1 Last,@ilen1 l1, @iLen2 l2, @iCnt icnt, @sStrTmp2 mtext,SUBSTRING(@ssSearchName,@iCnt,20) s1
					   
					   --2021-12-22 change, start     
					   if  (SUBSTRING(@sStrTmp,@iCnt,1)='|' or SUBSTRING(@sStrTmp,@iCnt,1)=' ')  and SUBSTRING(@sStrTmp,@iCnt+1,1)<>SUBSTRING(@sStr1,1,1)
					        SELECT @sStrTmp2= SUBSTRING(@sStrTmp,1,@iCnt-2)			
					   --2021-12-22 change, start 

					   --2021-12-22 add for fix match name did not include 0-9, Start	
					   if PATINDEX('%[0-9]%',@sStrTmp) > 0 and PATINDEX('%[0-9]%',@sMatchName) = 0
					   Begin
					      SELECT @iCnt=PATINDEX('%[0-9]%',@sStrTmp)
					      if  @iCnt > 0
					        SELECT @iCnt=@iCnt-1, @sStrTmp2= SUBSTRING(@sStrTmp,1,@iCnt)				
					   end

					   else
					      SELECT @sStrTmp2=@sStrTmp
					   --2021-12-22 add for fix match name did not include 0-9, End
				   

					  --SELECT '!!!-GetMatchedText1-d-4: ' Mark, @sStrTmp2 mText, @sstr First, @sStr1 Last, @iLen1 l1, @iLen2 l2, @icnt cnt,SUBSTRING(@ssSearchName,@iLen1,20) s1, SUBSTRING(@ssSearchName,@icnt-5,5) s2 

					   if CHARINDEX(@sStrTmp2, @ssSearchName) >0  or DIFFERENCE(@sStrTmp2, @sMatchName) >0
						 begin
						    
						    --SELECT '!!!-GetMatchedText2-a: ' Mark, @sStrTmp2 TransactionText, @sstr FirstName, @sStr1 LastName, @iLen1 l1, @iLen2 l2,@iCnt cnt, DIFFERENCE(@sStrTmp2, @sMatchName) d
					        select @sStrTmp1=@sStrTmp2
							
							--2021-12-23 change, Start
							if ABS(len(@sstrTmp2) - LEN(@smatchName) ) < 15
                               goto EndEnd   
                            else 
							   goto EndProc
                            --2021-12-23 change, End

						end
					end

                    else
					begin							     
						 --SELECT @sStrTmp2=@sStr + ' ' + @sStr1   
						 --SELECT @sStrTmp2=RIGHT(@ssSearchName,CHARINDEX('|',@ssSearchName)+1)
						 SELECT @iLenC1=CHARINDEX(' '+SUBSTRING(@sStr,1,1) , @ssSearchName),  @iLenC2=CHARINDEX('|'+SUBSTRING(@sStr,1,1) , @ssSearchName)
						 if @iLenC1 >0 and @iLenC2=0
						    SELECT @iLenC3=CHARINDEX(' '+SUBSTRING(@sStr1,1,1) , @ssSearchName, @iLenC1+1)
						 else if @iLenC1=0 and @iLenC2>0
						    SELECT @iLenC3=CHARINDEX(' '+SUBSTRING(@sStr1,1,1) , @ssSearchName, @iLenC2+1)
						 if @iLenC3 =0 
						       SELECT @iLenC3=CHARINDEX(SUBSTRING(@sStr1,1,1) , @ssSearchName, @iLenC2+1), @iLenC1=0
				
						 if @iLenC1 =0 and @iLenC2 >0 and @iLenC3 >0
						    SELECT @sStrTmp2=SUBSTRING(@ssSearchName, @iLenC2, @iLenC3 - @iLenC2 +1+len(@sStr1)-1)
					     else if  @iLenC1 >0 and @iLenC2=0 and @iLenC3 >0 
						    SELECT @sStrTmp2=SUBSTRING(@ssSearchName, @iLenC1, @iLenC3 -@iLenC1+1)
                         else
						    SELECT @sStrTmp2=@ssSearchName
					     
					     --SELECT '!!!-GetMatchedText2: ' Mark, @sStrTmp1 TransactionText, @sstr FirstName, @sStr1 LastName, @ssSearchName SearchText, @ilen1 l1, @iLen2 l2, @iCnt icnt
			
						 if CHARINDEX(@sStrTmp2, @ssSearchName) >0  or DIFFERENCE(@sStrTmp2, @sMatchName) >0
						 begin
						    
						    --SELECT '!!!-GetMatchedText2-a: ' Mark, @sStrTmp2 TransactionText, @sstr FirstName, @sStr1 LastName, @iLenC1 l1, @iLenC2 l2, @iLenC3 l3				        
							--select @sStrTmp1=REPLACE(@sStrTmp2,'|',@sNewLine)
							SELECT @sStrTmp1=@sStrTmp2
							goto EndProc
						end
			
				     end
	
					 --2021-12-17 Change, End			
					 		   
                   END

				   --SELECT '!!!-GetMatchedText2-b: ' Mark, @sStrTmp2 matchName,@ilen1 l1, @iLen2 l2, @iCnt icnt
				
				  --2021-12-22 add for @sStrTmp1 is null, For Partial Match Start
				  if ISNULL(@sStrTmp1,'')='' or  CHARINDEX(@sStr,@ssSearchName)=0 or CHARINDEX(@sStr1,@ssSearchName) = 0 
				  BEGIN
				      --SELECT '!!!-GetMatchedText:' Mark,@sStr FirstName, @sStr1 LastName, CHARINDEX(@sStr,@ssSearchName) C1,  CHARINDEX(@sStr1,@ssSearchName) C2

					  --1.FirstName @sStr, Start Poistion
					  if CHARINDEX(@sStr,@ssSearchName) = 0
					  Begin
						 SELECT @iLen1=CHARINDEX(' ' + substring(@sStr,1,2), @ssSearchName) + 1			  
					  if @iLen1=1
						 SELECT @iLen1=CHARINDEX(' ' +substring(@sStr,1,1), @ssSearchName) + 1
					 if @iLen1=1 
					     SELECT @iLen1=CHARINDEX('|' + substring(@sStr,1,2), @ssSearchName) + 1			  
					  if @iLen1=1
						 SELECT @iLen1=CHARINDEX('|' +substring(@sStr,1,1), @ssSearchName) + 1
					 if @iLen1=1
						 SELECT @iLen1=CHARINDEX(substring(@sStr,1,3), @ssSearchName)
					  if @iLen1=0
						 SELECT @iLen1=CHARINDEX(substring(@sStr,1,2), @ssSearchName)
					  if @iLen1=0
					      SELECT @iLen1=Len(@ssSearchName)

                      --1.1 FirstName @sStr, End Poistion
					  --SELECT '!!!-GetMatchedText0:' Mark, @ssSearchName SearchText,@iLen1 L1
					 
					  SELECT @iCnt=CHARINDEX(' ' , @ssSearchName, @ilen1+1)
					  if @iCnt=0
					     SELECT @iCnt=CHARINDEX('|' , @ssSearchName, @ilen1+1)
					  if @iCnt=0
					     SELECT @iCnt=Len(@ssSearchName)
					  SELECT @sStr=SUBSTRING(@ssSearchName, @ilen1, @icnt-@iLen1)

					  --SELECT '!!!-GetMatchedText0:' Mark, @iLen1 L1, @iCnt L2, SUBSTRING(@ssSearchName, @ilen1, @icnt-@iLen1) Firstname
					  End

					--2.LastName @sStr1, Start Poistion
					if CHARINDEX(@sStr1,@ssSearchName) = 0
					Begin
						SELECT @iLen2=CHARINDEX(' ' + substring(@sStr1,1,3), @ssSearchName,@ilen1+1) + 1			  
					if @iLen2=1
						SELECT @iLen2=CHARINDEX(' ' + substring(@sStr1,1,1), @ssSearchName,@iLen1+1) + 1
					if @iLen2=1
					   SELECT @iLen2=CHARINDEX('|' + substring(@sStr1,1,3), @ssSearchName,@ilen1+1) + 1			  
					if @iLen2=1
						SELECT @iLen2=CHARINDEX('|' + substring(@sStr1,1,1), @ssSearchName,@iLen1+1) + 1
					if @iLen2=1
						SELECT @iLen2=CHARINDEX(substring(@sStr1,1,3), @ssSearchName,@ilen1+1)
					if @iLen2=0
						SELECT @iLen2=CHARINDEX(substring(@sStr1,1,2), @ssSearchName, @ilen1+1)
                    if @iLen2=0
					    SELECT @iLen2=Len(@ssSearchName)

                    --2.1 LastName @sStr1, End Poistion
					-- SELECT '!!!-GetMatchedText0:' Mark, @ssSearchName SearchText,@iLen2 L2
					 
					SELECT @iCnt=CHARINDEX(' ' , @ssSearchName, @iLen2+1)
					if @iCnt=0
					   SELECT @iCnt=CHARINDEX('|' , @ssSearchName, @iLen2+1)
					if @iCnt=0
					    SELECT @iCnt=Len(@ssSearchName)

                    SELECT @sStr1=SUBSTRING(@ssSearchName, @iLen2, @icnt-@iLen2)

					--SELECT '!!!-GetMatchedText0:' Mark, @iLen2 L1, @iCnt L2, @sStr1 Lastname

					end

                      if @ilen2>@ilen1
                         SELECT @sStrTmp1=SUBSTRING(@ssSearchName,@ilen1, @ilen2-@ilen1+Len(@sStr1))
                      else
					     SELECT @sStrTmp1=@sStr

                     -- SELECT '!!!-GetMatchedText2: ' Mark, @sStrTmp1 TransactionText, @sstr FirstName, @sStr1 LastName
				  END
				  --2021-12-22 add for @sStrTmp1 is null, For Partial Match End


				   --select @sStrTmp1=REPLACE(@sStrTmp1,'|',@sNewLine)

GetMatchedText1:
				  if @iProcess=1
				     goto GetProcess1
                  
				  if @iProcess=2 
				     goto GetProcess2


EndEnd:

                --2021-12-23 ADD FOR NULL ISSUE, Start
				if Len(@ssMatchName) >0 and Len(@ssSearchName) >0 and isnull(@sStrTmp1,'')='' 
				   SELECT @sStrTmp1=' '   --SELECT @sStrTmp1=SUBSTRING(@ssMatchName,1,2)
				--2021-12-23 ADD FOR NULL ISSUE, End
               
			    --2021-12-23 add for first char, Start
				if SUBSTRING(@sStrTmp1,1,1)=' ' or SUBSTRING(@sStrTmp1,1,1)='|'
				   SELECT @sStrTmp=SUBSTRING(@sStrTmp1,2,len(@sStrTmp1)-1), @sStrTmp1=LTRIM(@sStrTmp)
				--2021-12-23 add for first char, End
               
			    select @sStrTmp1=REPLACE(@sStrTmp1,'|',@sNewLine)
				SELECT @sStrTmp1=REPLACE( @sStrTmp1, ' ' + CHAR(13) + CHAR(10), CHAR(13)+CHAR(10))
            
	            --Select @sStrTmp1 MatchText, @sMatchName PartyName, @ssSearchName TransactionText 
              
			   return @sStrTmp1
End

