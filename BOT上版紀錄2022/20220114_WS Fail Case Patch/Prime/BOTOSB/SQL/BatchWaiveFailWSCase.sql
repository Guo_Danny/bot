use OFAC
go

select ft.SeqNumb,ft.Source from FilterTranTable ft
where Source = 'WS_SWIFT'
and substring(ref,1,31) in (
select substring(ref,1,31) ref_new from FilterTranTable
where Source in ('SWIFT','WS_SWIFT')
group by substring(ref,1,31)
having count(SeqNumb) > 1
)

--以下為自動waive&approve的script，update的地方我先註解起來，可以根據需求修正內容

--select ft.SeqNumb, ft.Source
update FilterTranTable set ConfirmState = 2, ConfirmOper = 'primeadmin', ConfirmTime = getdate(),
App = 1, AppOper = 'primeadmin', AppTime = getdate(), AnnotTxt = 'Duplicate Cases with empty transaction text',
Source = 'Duplicate'
from FilterTranTable ft
where Source = 'WS_SWIFT'
and substring(ref,1,31) in (
select substring(ref,1,31) ref_new from FilterTranTable
where Source in ('SWIFT','WS_SWIFT')
group by substring(ref,1,31)
having count(SeqNumb) > 1
)
