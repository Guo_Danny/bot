Use PBSA
GO

exec sp_configure 'show advanced options', 1;
GO
RECONFIGURE;
GO
exec sp_configure 'Ole Automation Procedures', 1;
GO
RECONFIGURE;
GO


insert into ObjectType (Code, name, CreateOper, CreateDate)
select 'USRUP', 'User Upload', 'Primeadmin', getdate()

insert into CodeTbl(code,name, ObjectType, CreateOper, CreateDate)
select 'BSAEOD','BSA EOD File Upload','USRUP','Primeadmin', getdate()

insert into CodeTbl(code,name, ObjectType, CreateOper, CreateDate)
select 'CIF','CIF Upload','USRUP','Primeadmin', getdate()

insert into CodeTbl(code,name, ObjectType, CreateOper, CreateDate)
select 'SanctionUpload','Sanction Party Upload','USRUP','Primeadmin', getdate()