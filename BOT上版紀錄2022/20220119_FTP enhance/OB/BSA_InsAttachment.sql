USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_InsAttachment]    Script Date: 1/19/2022 2:22:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER Procedure [dbo].[BSA_InsAttachment] (@ObjectID ObjectID, @objectType SCode, 
	@subSystem varchar(25),@fileName varchar(255), @fileExt varchar(3), 
	@createOper SCode, @fileImage Image)
  As
  Declare @trnCnt int

  Select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	Begin Tran BSA_InsAttachment
  Else
    -- Already in a transaction
	Save Tran BSA_InsAttachment
  -- End standard stored procedure transaction header 

  Declare @stat Int,
          @sRecNo Varchar(15),
	  @logText varchar(2000) 

  Insert Into Attachment(ObjectID, ObjectType, Subsystem, FileName, FileExt,
	CreateOper, FileImage)
  Values (@ObjectID, @objectType, Rtrim(@subSystem),  
	@fileName, @fileExt, @CreateOper, @fileImage)

  Select @stat = @@error

  -- Evaluate results of the transaction
  if (@stat = 0) begin
    select @sRecNo =  convert(varchar, @@identity)

    select @logText = 'Created attachment ' + @fileName + ' on ' 
		+  cast(getdate() as varchar)
		    
	if @subsystem = 'OFAC'
		Exec OFAC.DBO.OFS_InsEvent @createOper, 'Cre', 'Attachment', @ObjectId, @sRecNo
    else if @subsystem = 'LEGL'
		Exec PLEGL.DBO.LEGL_InsEvent @createOper, 'Cre', 'Attachment', @ObjectId, @logText  
	else
		Exec BSA_InsEvent @createOper, 'Cre', 'Attachment', @sRecNo , @logText  

	--Jan.19.2022 add new ftp file upload method
	DECLARE @ImageData varbinary(max);
	SELECT @ImageData = (SELECT convert(varbinary(max), @fileImage, 1) FROM Documentation doc
	where doc.RecNo = @ObjectID and doc.Category in (select Code from CodeTbl where ObjectType = 'USRUP'));

	DECLARE @Path nvarchar(1024);
	SELECT @Path = 'E:\Prime\botosb\Dat';

	SELECT @Filename = (SELECT @fileName FROM Documentation doc
	where doc.RecNo = @ObjectID and doc.Category in (select Code from CodeTbl where ObjectType = 'USRUP'));

	DECLARE @FullPathToOutputFile NVARCHAR(2048);
	SELECT @FullPathToOutputFile = @Path + '\' + @Filename;

	DECLARE @ObjectToken INT
	EXEC sp_OACreate 'ADODB.Stream', @ObjectToken OUTPUT;
	EXEC sp_OASetProperty @ObjectToken, 'Type', 1;
	EXEC sp_OAMethod @ObjectToken, 'Open';
	EXEC sp_OAMethod @ObjectToken, 'Write', NULL, @ImageData;
	EXEC sp_OAMethod @ObjectToken, 'SaveToFile', NULL, @FullPathToOutputFile, 2;
	EXEC sp_OAMethod @ObjectToken, 'Close';
	EXEC sp_OADestroy @ObjectToken;
	--Jan.19.2022 end

    If @trnCnt = 0      
        commit tran BSA_InsAttachment
  end else 
	rollback tran BSA_InsAttachment
  return @stat

GO


