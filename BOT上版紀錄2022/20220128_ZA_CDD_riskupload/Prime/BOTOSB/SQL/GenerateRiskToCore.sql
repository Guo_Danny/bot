
SET NOCOUNT ON;
select distinct convert(char(35),vrsh.CustomerId) + '~' + convert(char(11),isnull(rs.Code,'')) + '~' + convert(char(8),isnull(c.ReviewDate,''),112) + '~' + convert(char(2),isnull(k.CDDUser25,'00')) 
 from PBSA..vwRiskScoreHist (nolock) vrsh
join PBSA..RiskClass (nolock) rs on vrsh.RiskClass = rs.name 
join PBSA..Customer (nolock) c on c.Id = vrsh.CustomerId
join PBSA..KYCData (nolock) k on k.CustomerId = c.Id
where id not like 'Noncust%'
and (DateAccepted >= convert(varchar(10),getdate()-10,111))
and KYCStatus = 2
and isnull(c.ProbationStartDate,'') != isnull(convert(varchar(8),c.ReviewDate,112),'')
and vrsh.CreateDate = (select max(CreateDate) from PBSA..vwRiskScoreHist (nolock) where customerid = vrsh.CustomerId)

