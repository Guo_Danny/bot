use PBSA
go

declare @SDate varchar(20), @logtext varchar(255), @oper varchar(255),@cont int;
select @Sdate=convert( varchar(10), getdate() -1,112);
set @logtext = 'BSA End Of Day Fail, Please contact Taipei center PRIME support team to fix process failed.'
--select @SDate;

    select @cont = count(*) from SysParam
	where EodState != -1

declare cur_oper CURSOR FOR
select Oper from PSEC..RoleMembers rm where [role] like '%caseap%'
open cur_oper;
fetch next from cur_oper into @oper;
while @@fetch_status = 0 
begin

    --select @logtext,@oper;
    if @cont > 0
	begin
    --select @logtext,@oper;
    insert into PBSA..Notification (Subsystem,ObjectType, ObjectId,NotifyTime,SnoozeTime,NotifyOper,NotifyDescr,NotificationType,RecurType,RecurExpireDate,NextRecurDate)values
    ('BSAEOD','BSAEOD','BSAEOD',GETDATE(), getdate(),@oper,@logtext,'BSAEOD',0,null,null);
    	
	insert into event(TrnTime, Oper, Type, ObjectType, ObjectId, LogText, EvtDetail)
	values(getdate(), @oper, 'CED', 'Process', 'BSA EOD',@logtext, @logtext)
	end
  fetch next from cur_oper into @oper;
end
close cur_oper;
DEALLOCATE cur_oper;
