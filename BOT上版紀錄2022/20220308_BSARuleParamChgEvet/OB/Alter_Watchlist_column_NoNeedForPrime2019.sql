USE [PBSA]
GO

ALTER TABLE [dbo].[Watchlist] DROP CONSTRAINT [DF_WatchList_Params]
GO

alter table Watchlist alter column Params varchar(max)

ALTER TABLE [dbo].[Watchlist] ADD  CONSTRAINT [DF_WatchList_Params]  DEFAULT ('') FOR [Params]
GO