USE [PBSA]
GO

/****** Object:  Trigger [dbo].[BSA_WatchListParamsUpd]    Script Date: 3/8/2022 12:08:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[BSA_WatchListParamsUpd] on [dbo].[Watchlist]   For Update As
begin
	if update(Params)
	begin

  Update Watchlist Set LastModify = getDate() 
	From WatchList W JOIN Inserted I On i.WLCode = W.WLCode
    JOIN Deleted D On i.WLCode = W.WLCode
and (i.LastEval = d.LastEval or ( i.lasteval is null and d.lasteval is null))
	and ( i.LastEvalStat = d.LastEvalStat or (i.LastEvalStat is null and 	d.LastEvalStat is null))

--Oct.22.2021 to capture parameter change.
Declare @xmlText as nvarchar(max)
Declare @List_d as nvarchar(max)
Declare @List_i as nvarchar(max)
DECLARE @hDoc int 
DECLARe @wlCode varchar(1000)
DECLARE @tt table (wlcode varchar(35), Alias varchar(1000), Value nvarchar(max) )
DECLARE @ttd table (wlcode varchar(35), Alias varchar(1000), Value nvarchar(max) )
DECLARE @tti table (wlcode varchar(35), Alias varchar(1000), Value nvarchar(max) )
DECLARE @tt_d table (wlcode varchar(35), params nvarchar(max))
DECLARE @tt_i table (wlcode varchar(35), params nvarchar(max))
declare @tt_mod_desc varchar(5000);

--Oct.27.2021 before insert
Begin try

	select @xmlText = Params from deleted with(nolock) where createtype != 1 

	exec sp_xml_preparedocument @hDoc OUTPUT, @xmlText

	INSERT INTO @ttd 
	SELECT @wlCode, Alias, Alias + ' = ''' + Value + '''' as value
	FROM OPENXML (@hDoc, '/Params/Param')
	WITH (WlCode varchar(35), Alias varchar(1000), Value nvarchar(max))

	EXEC sp_xml_removedocument @hDoc

	Select @List_d = COALESCE(@List_d + char(10)+ char(13), '') + Value from @ttd
	set @List_d = replace(replace(@List_d,char(13),'  '),char(10),'')
	Insert into @tt_d values (@wlcode, @List_d)
	
	

	set @List_d = ''
	set @xmlText = NULL

End try

begin catch
	set @List_d = ''
	set @xmlText = NULL
end catch

--Oct.27.2021 Inserted
Begin try

	select @xmlText = Params from inserted with(nolock) where createtype != 1 

	exec sp_xml_preparedocument @hDoc OUTPUT, @xmlText

	INSERT INTO @tti 
	SELECT @wlCode, Alias, Alias + ' = ''' + Value + '''' as value
	FROM OPENXML (@hDoc, '/Params/Param')
	WITH (WlCode varchar(35), Alias varchar(1000), Value nvarchar(max))

	EXEC sp_xml_removedocument @hDoc

	Select @List_i = COALESCE(@List_i + char(10)+ char(13), '') + Value from @tti
	set @List_i = replace(replace(@List_i,char(13),'  '),char(10),'')
	Insert into @tt_i values (@wlcode, @List_i)
	
	set @List_i = ''
	set @xmlText = NULL

End try

begin catch
	set @List_i = ''
	set @xmlText = NULL
end catch

set @tt_mod_desc = ''

DECLARE cur Cursor for

	select tti.Value, ttd.Value
	from @tti tti join @ttd ttd on tti.Alias = ttd.Alias
	where tti.Value != ttd.Value
	
	OPEN cur
	FETCH NEXT FROM cur into @List_i, @List_d
	While @@FETCH_STATUS = 0 
	Begin

	--print 'old: ' + @List_d + ',new: ' + @List_i

	set @tt_mod_desc = @tt_mod_desc + 'old: ' + @List_d + ',new: ' + @List_i + ', ';

	FETCH NEXT FROM cur into @List_i, @List_d
	End

Close cur
DeAllocate cur

  Insert Event (TrnTime, Oper, Type, ObjectType, ObjectId , LogText, EvtDetail) 
   Select getDate(), i.LastOper, 'Mod', 'Rule', convert(varchar, i.WLCode),  '', 
Case 
	WHEN (@tt_mod_desc != '') 
	THEN '| params : ' + @tt_mod_desc else '' End 
	From Inserted i, deleted d where  i.WLCode = d.WLCode
and (i.LastEval = d.LastEval or ( i.lasteval is null and d.lasteval is null))
	and ( i.LastEvalStat = d.LastEvalStat or (i.LastEvalStat is null and 	d.LastEvalStat is null))

	end
end
GO


