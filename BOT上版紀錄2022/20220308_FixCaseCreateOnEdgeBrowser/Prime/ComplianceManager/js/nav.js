//Deleting a record should refresh the parent
	var wasRecordDeleted;
	var recordElement;
	recordElement = document.getElementById("hiddentxtWasRecordDeleted");
	if (recordElement != null) {
		wasRecordDeleted = document.getElementById("hiddentxtWasRecordDeleted").value
		if (wasRecordDeleted.toString() == "1") {
			window.opener.document.forms[0].submit();				
			document.getElementById("hiddentxtWasRecordDeleted").value = "0";
			window.focus();
			}
	}
	
//----------------------------------Section Control------------------------------------------

var timer;
var timeoutHandlingValue = new Array();
var timeoutActionValue = new Array();
var windowNameValues = new Array();
var cookieExpiration = "Thu, 01 Jan 1970 00:00:00 UTC";
              
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
     var c = ca[i];
     while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}

function ClearPCSsessionTimeoutCookies()
{
    document.cookie = "PCSwinnames=; expires=" + cookieExpiration; 
    document.cookie = "PCSTimeoutHandlingValue=; expires=" + cookieExpiration;     
    document.cookie = "PCSTimeoutActionValue=; expires=" + cookieExpiration;    
}
        
//Add Window names to PCSWindowCookie        
function AddWindowToPCSWindowCookie(windowName)
{
  if((windowName != "") && (windowName != null) && (windowName != "undefined"))
  {
      if (getCookie("PCSTimeoutHandlingValue") != "false")
  {
    windowNameValues = getCookie("PCSwinnames");
           if(((windowNameValues != "") || (windowNameValues != null)) && (typeof windowNameValues != 'undefined'))
    {
                setCookie("PCSwinnames", windowNameValues + "|" + windowName + "|", 1);	
    } 
    else   
    {  
			    //Set the Cookie if it is not available
                setCookie("PCSwinnames", "|" + windowName + "|", 1);   
            } 
    }
  }
      
}
                                                
function closeAll() 
{
        
    var winref;
    var windowNameValuesArray = new Array();
    
    windowNameValues = getCookie("PCSwinnames");
    windowNameValuesArray = windowNameValues.split("|");
    timeoutActionValue = getCookie("PCSTimeoutActionValue");
       
    for (var x = 0; x < windowNameValuesArray.length; x++)
    {
      try
      {
       if (windowNameValuesArray[x] != '')
       {
           //winref = window.open('', windowNameValuesArray[x], '', true);
           winref=window.open('',windowNameValuesArray[x], 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no,left=10000, top=10000, width=10, height=10, visible=none,',true);
           
           if(winref.location.host == '')
           {
                // close windows                
                winref.close();
           }
           else
           {
           if (window.location.host == winref.location.host)
           {
                if (timeoutActionValue == "redirect")
                {
                    // redirect windows
                    winref.location.replace("login.aspx?msg=3");
                } else
                {
                    // close windows
                    winref.close();
                }
           } 
           }   
        }
       }
       catch(e)
       {
        // fall through for windows that don't exist 
       }
    }
    ClearPCSsessionTimeoutCookies();                
}
                                                          
function ClearTimer()
{
    window.clearTimeout(timer);
    window.status = '';
}



function SessionTimeoutTimer(sessionTimeout)
{
                         
      if (sessionTimeout <= 2)
      {                      
        window.status = 'Session will expire in approximately ' + sessionTimeout + ' minute(s).';
      } else
      {
        window.status = '';
      }
      
      sessionTimeout = sessionTimeout - 1;
      checktimeout(sessionTimeout);
              
}

function SessionExpirationTimer(SessionTimeout)
{

 // The purpose of this function is the find the main PCS window ("ASP.QUERYPLUS_ASPX", "ASP.VIEW_ASPX", or "ASP.DASHBOARD_ASPX")
 // that holds the session timeout timer and to restart the timer.  We only want this timer running on 
 // the main PCS window, not child windows.  We find the main PCS window by looking from the deepest possible level child page 
 // and working backwards. This function gets called from Global_AcquireRequestState in Global.asax.vb when a request is made to server side.
 
 try 
 {  
        // child of child of child window will fall into here
        if (window.opener.opener.opener && window.opener.opener.opener.open && !window.opener.opener.opener.closed) 
        { 
          window.opener.opener.opener.ClearTimer();
          window.opener.opener.opener.SessionTimeoutTimer(SessionTimeout);  
        }  
        else 
        // child of child window will fall into here
        if (window.opener.opener && !window.opener.opener.closed) 
        { 
          window.opener.opener.ClearTimer(); 
          window.opener.opener.SessionTimeoutTimer(SessionTimeout);  
        }  
       
 } 
 catch(e) 
 { 
        // unable to set property 'opener' of undefined or null reference.
        // window reference was lost before finding main window.
        // continue checking windows higher in chain.

        try
        {
          // child of child window will fall into here
          if (window.opener.opener && !window.opener.opener.closed) 
          { 
             window.opener.opener.ClearTimer();
             window.opener.opener.SessionTimeoutTimer(SessionTimeout);   
          }  
          else 
          // child window will fall into here
          if (window.opener && !window.opener.closed) 
          { 
            window.opener.ClearTimer();
            window.opener.SessionTimeoutTimer(SessionTimeout);
          }
        
        } 
        catch(err)
        {
          // unable to set property 'opener' of undefined or null reference.
          // window reference was lost before finding main window.
          // continue checking windows higher in chain.
 	 
	 try
	 {
	   // see if we are in an iframe by using window.parent.	
           if (window.parent.opener) 
           { 
              window.parent.opener.ClearTimer();
              window.parent.opener.SessionTimeoutTimer(SessionTimeout);
           }
	   else
          
           //  main windows "ASP.QUERYPLUS_ASPX", "ASP.VIEW_ASPX", and "ASP.DASHBOARD_ASPX" will fall into here
           if (window && !window.closed) 
           { 
             window.ClearTimer();
             window.SessionTimeoutTimer(SessionTimeout);
           } 

	 } 
         catch(errr)
	 {
	   if (window && !window.closed) 
           { 
             window.ClearTimer();
             window.SessionTimeoutTimer(SessionTimeout);
           } 
	 
	 } 	
        
        }
   
 } 

}
        
function checktimeout(sessionTimeout)
{
          
    if (sessionTimeout >= 0)
       
    { 
        timer = window.setTimeout(function() {SessionTimeoutTimer(sessionTimeout);}, 60000);
    }
    else
    {
        closeAll();
        top.location.href="login.aspx?msg=3"; 
    }
}
               
function selectTask(sHLinkPage, iHLinkMethod)
{
	var frm = document.forms[0];
	
	switch (iHLinkMethod)
	{

		case 1:
		{
			document.location.href=sHLinkPage;
			break;
		}
		case 2:
		{
			document.location.href=sHLinkPage;
			break;
		}
		case 3:
		{
			frm.submit();
		}
	}
	
	

	return false;
}


function OpenMsg(sPage)
{
    var pos1 = 0;
	var windowNameValues;	
	var param="resizable=yes,status=yes,menubar=no,toolbar=no,scrollbars=yes,left=250,top=250,height=200,width=300";
	var nw=window.open(sPage,"CMBrowserMsg", param);
	windowNameValues = getCookie("PCSwinnames");      
	//Code to check that only one occurence of the window name "CMBrowserMsg" should be added to the cookie
      if((windowNameValues != "") || (windowNameValues != null))
      { 
            pos1 = windowNameValues.indexOf("CMBrowserMsg");             
            if (pos1 < 0)
            {
	AddWindowToPCSWindowCookie("CMBrowserMsg");
            }
       }      
	if (nw != null)
		nw.focus();	
		
	return false;

}

function selectSection(sHLinkPage, iHLinkMethod)
{

	var frm =document.forms[0];

	switch (iHLinkMethod)
	{

		case 1:
		{
			document.location.href=sHLinkPage;
			break;
		}
		case 2:
		{
			document.location.href=sHLinkPage;
			break;
		}
		case 3:
		{
			frm.submit();
		}
	}
	
}
function highlightSection(sMnuName, sSectionClass, sHighlightedSectionClass)
{
	if (document.getElementById(sMnuName).className == sSectionClass)
	{
		document.getElementById(sMnuName).className =sHighlightedSectionClass;
	}
}
function resetSection(sMnuName, sSectionClass, sHighlightedSectionClass)
{	
	if (document.getElementById(sMnuName).className == sHighlightedSectionClass)
	{	
		document.getElementById(sMnuName).className =sSectionClass;
	}
}


//----------------------------------Object Bar------------------------------------------
function highlightObject(objid, objclass)
{

	var objLink=document.getElementById(objid)
	
	if (objLink != null)
	{
		if (objLink.className != objclass)
		{
			objLink.className = objclass;
		}
	}
}




//----------------------------------Task Bar---------------------------------------------
function OpenTaskWindow(sTaskPage, sTask, PageWindowType)
{
	var param=getwindowparam(PageWindowType);
	var queryString = new String(sTaskPage);
	var pos;
	var pageName;
	var windowNameValues = new Array();
	pos = queryString.indexOf(".aspx");
	pageName =  queryString.substr(0,pos);
	var nw=window.open(sTaskPage, pageName, param);
	if(pageName == 'Case')
	{	
	AddWindowToPCSWindowCookie(pageName);
	}
	if (nw != null)
		nw.focus();	
		
	return false;
}


//----------------------------------Tab Control------------------------------------------



//Select tab  when user click on it
function selectTab(iSelectedTab, iTotalTabs, sTabHeaderPrefix, sTabNamePrefix, sPREFIX_PANEL_TITLE, sCLASS_PANEL_TITLE, sHfldTabSelected)
{

	var iTab;
	var sTabName;
	var objTab;
	var objTabHeader;
	try
	{
		try
		{
			//document.forms[0].item(sTabHeaderPrefix + ':' + sHfldTabSelected).value=iSelectedTab;
			document.forms[0].item(sTabHeaderPrefix + '$' + sHfldTabSelected).value=iSelectedTab;
		}
		catch(ex)
		{
			document.getElementById(sHfldTabSelected).value=iSelectedTab;
		}
		
		
		for (iTab=1; iTab<=iTotalTabs; iTab++)
		{
			sTabHeaderName = sTabHeaderPrefix + iTab;
			sTabName	   = sTabNamePrefix + iTab;
			
			objTab = document.getElementById(sTabName);
			objTabHeader = document.getElementById(sTabHeaderName);
			
		
			if (objTabHeader != null)
			{
				if (iTab==iSelectedTab)
				{				
					objTabHeader.className = "TabSelected";
					
					if (objTab != null)
						objTab.className  = "";
						
				}
				else
				{
					try
					{
						if (event.altKey==false)
						{
							if (objTabHeader.className != "TabDisabled")
							{
								objTabHeader.className ="TabUnselected";
								
								if (objTab != null)
									objTab.className="TabHidden";
									
							}
						}
					}
					catch(e)
					{
						if (objTabHeader.className != "TabDisabled")
						{
							objTabHeader.className ="TabUnselected";
							
							if (objTab != null)
								objTab.className="TabHidden";
						}
					}
				}	
			}
		}
		showTabTitle(iTotalTabs, sTabNamePrefix, sPREFIX_PANEL_TITLE, sCLASS_PANEL_TITLE);
	}
	catch(e)
	{
		
	}
}
	
function showTabTitle(iTotalTabs, sTabNamePrefix, sPREFIX_PANEL_TITLE, sCLASS_PANEL_TITLE)
{

	var iTab;
	var sTabId;
	var sTabTitleId
	var bHideTitle=true;
	try
	{
		for (iTab=1; iTab<=iTotalTabs; iTab++)
		{
	
			sTabId	    = sTabNamePrefix + iTab;
			sTabTitleId = sPREFIX_PANEL_TITLE + sTabId;

			if (document.getElementById(sTabId).className  == "")
			{
				if (bHideTitle)
				{
					document.getElementById(sTabTitleId).className="TabHidden";
					bHideTitle=false;
				}
				else
					document.getElementById(sTabTitleId).className=sCLASS_PANEL_TITLE;
			}
			else
				document.getElementById(sTabTitleId).className=sCLASS_PANEL_TITLE;
			
		}
	}
	catch(e)
	{
		
	}
}


//Highlight tab when mouse pointer hover it
function highlightTab(sTabName, sTabUnselected, sTabHighlighted)
{
	
	if (document.getElementById(sTabName).className == sTabUnselected)
	{
		document.getElementById(sTabName).className = sTabHighlighted;
	}
}

//Reset tab  style to original when mouse pointer leaving it
function resetTab(sTabName, sTabUnselected, sTabHighlighted)
{	
	if (document.getElementById(sTabName).className == sTabHighlighted)
	{	
		document.getElementById(sTabName).className = sTabUnselected;
	}
}




/*-------------------------------View Control--------------------------*/
function ShowLAPrompt(chkOpt)
{
	var change = true;
	
	try
	{	
		if (chkOpt == null)
		{
			change = false;
		}
		else
		{		
			if (chkOpt.checked)
			{
				
				change = (confirm("BSA End of Day will require more time " +
						"to complete if you enable this option. \n" +
						"Please contact your Database Administrator to tune " +
						"the PBSA database performance. \n \n " +							
						"Do you want to enable \"Create Party Relationships\" option?"))	
							
				if (change == false)
					chkOpt.checked = false;
																				
			}
	
		}	
	}
	catch (ex) {}
	finally
	{
		if (change)
			__doPostBack(chkOpt.id, "");
	}
}

function DisablePrint(cmdPrintID, lblPageRangeID)
{
	try
	{		
		var cmdPrint = document.getElementById(cmdPrintID);
		if (cmdPrint != null)
			cmdPrint.disabled = true;
			
		var lblPageRange = document.getElementById(lblPageRangeID);
		if (lblPageRange != null)
			lblPageRange.style.display = "";
			
	}
	catch (ex) {}
}

function checkAll(clientId, rowstotal)
{
	var row=0;
	var checkboxId='';
	var isChecked=false;
	var chkRow=null;
	var chkAll=null;
	checkboxId = clientId + "_chkRowAll";
	chkAll=document.getElementById(checkboxId);
	
	if (chkAll != null) 
	{	
		
		for(row = 0; row < rowstotal; ++row)
		{	
			checkboxId = clientId + "_chkRow"+ row;
			chkRow=document.getElementById(checkboxId);
			if (chkRow != null)
			{
				 chkRow.checked=chkAll.checked;			
			}
		}
		
		if (chkAll.checked)
			chkAll.title="Uncheck All"
		else
			chkAll.title="Check All"
	}
}

function checkOne(clientId, rowstotal)
{
	var row=0;
	var checkboxId='';
	var isAllChecked=false;
	var chkRow=null;
	var chkAll=null;
	
	try
	{
		checkboxId = clientId + "_chkRowAll";
		chkAll=document.getElementById(checkboxId);
	
		if (chkAll != null) 
		{	
		
			for(row = 0; row < rowstotal; ++row)
			{
				checkboxId = clientId + "_chkRow"+ row;
				chkRow=document.getElementById(checkboxId);
				if (chkRow != null)
				{
					 if (chkRow.checked)
					 {
						isAllChecked = true;
					 }
					 else
					 {
						isAllChecked = false;
						break;
					 }			
				}
				else
				{
					isAllChecked = false;
					break;
				}
			}
		
			chkAll.checked = isAllChecked
			if (isAllChecked)
				chkAll.title="Uncheck All"
			else
				chkAll.title="Check All"
		}
	}
	catch(ex){}
}

function checkGroupedRow(groupName, objCheck, clientId, rowstotal)
{
	var row			= 0;
	var checkboxId	= "";	
	var chkRow		= null;
	var parent		= null;	
	var rowName		= "";
	var groupCurr	= "";
	var Msg			= ""
	
	try
	{			
		rowName = objCheck.parentElement.title;				
		for(row = 0; row < rowstotal; ++row)
		{
			checkboxId = clientId + "_chkRow"+ row;			
			chkRow = document.getElementById(checkboxId);					
			if (chkRow != null)
			{							
				parent = chkRow.parentElement;
				if (parent != null)
				{			
					if (rowName == parent.title)	
					{							
						groupCurr = GetGroupName(chkRow);	
						if (objCheck.id != checkboxId)
						{
							chkRow.checked = objCheck.checked;	
							Msg = Msg + rowName + " from " + groupCurr + "; \n";	
						}
						
					}
				}																	
			}
		}
		
		if (Msg.length > 0)
		{
			if (objCheck.checked)
				alert("Also checked: \n" + Msg);
			else
			   alert("Also unchecked: \n" + Msg);
		}
	}
	catch(ex){}
}


function GetGroupName(chkRow) {
	var pos1;
	var pos2;
	var groupName;
	var sAttr;
	groupName = new String("");
	
	try
	{
		sAttr = new String(chkRow.getAttribute("OnClick"));
		pos1 = sAttr.indexOf("('");
		if (pos1 > 0)
		{		
			pos2 = sAttr.indexOf("',", pos1);
		
			if (pos2 <= 0)
			{
				//Last parameter
				pos2 = sAttr.length;
			}		
			groupName  =  sAttr.substr(pos1 + 2, pos2 - pos1 - 2);				
		} 
		return groupName;
	}
	catch(ex){}

}

function highlightHeader(obj)
{
	
	if	(obj.style.backgroundColor=="")
		obj.style.backgroundColor="darkgray";
	
}

function resetHeader( obj)
{
		if	(obj.style.backgroundColor=="darkgray")
		obj.style.backgroundColor="";
}


function PopupRecord(iObjectId,  sRecordId )
{
	var sPage;


	switch (iObjectId)
	{

		case 1:
		{
		
			sPage="Customer.aspx?RecordId=" + sRecordId;
			break;
			
		}
		case 2:
		{
		
			break;
			
		}
		default:
		{
		
		
			break;
			
		}
	}
	
	OpenWindow(sPage, '', 1)
	return false;

	
}

function PopupPrint(iViewID, iPageNumber,  iRowsPerPage, iRecordsTotal, iQueryPlusMode, iArchive, sTitleID)
{

	var sTitle="";
	var lblTitle=document.getElementById(sTitleID);
	
	if (lblTitle != null)
		sTitle = lblTitle.innerText;	
	
	var sPage = "Print.aspx?ViewID=" + iViewID + "&PageNumber=" + iPageNumber +
	"&RowsPerPage=" + iRowsPerPage + "&RecordsTotal=" + iRecordsTotal + "&QPM=" + iQueryPlusMode + "&ArchiveMode=" + iArchive +
	"&Title=" + encodeURIComponent(sTitle);

	var param="resizable=yes,status=yes,menubar=no,toolbar=no,scrollbars=yes,left=15,top=15,height=700,width=950";
	var nw=window.open(sPage,'Print', param);
	
	if (nw != null)
		nw.focus();	
		
	return false;
	
}

function PopupExport(iViewID, iPageNumber,  iRowsPerPage, iRecordsTotal, iQueryPlusMode, iArchive, sTitleID)
{

	var sTitle="";
	var lblTitle=document.getElementById(sTitleID);
	
	if (lblTitle != null)
		sTitle = lblTitle.innerText;	
	
	var sPage = "Export.aspx?ViewID=" + iViewID + "&PageNumber=" + iPageNumber +
	"&RowsPerPage=" + iRowsPerPage + "&RecordsTotal=" + iRecordsTotal + "&QPM=" + iQueryPlusMode + "&ArchiveMode=" + iArchive +
	"&Title=" + encodeURIComponent(sTitle);

	var param="resizable=yes,status=yes,menubar=no,toolbar=no,scrollbars=yes,left=15,top=15,height=700,width=950";
	var nw=window.open(sPage,'Export', param);
	
	if (nw != null)
		nw.focus();	
		
	return false;
	
}

function showabout()
{
	var pos1 = 0;
	var param="resizable=no,status=no,menubar=no,toolbar=no,scrollbars=no,left=100,top=100,height=215,width=618";
	var nw=window.open("About.htm", "About", param);
	var windowNameValues;
	windowNameValues = getCookie("PCSwinnames");      
    //Code to check that only one occurence of the window name "About" should be added to the cookie    
      if((windowNameValues != "") || (windowNameValues != null))
      { 
            pos1 = windowNameValues.indexOf("About");             
            if (pos1 < 0)
            {
	AddWindowToPCSWindowCookie("About");
            }
       }      
	
	if (nw != null)
		nw.focus();	
		
	return false;
}

function GetParameterId(sPage, sParameter) {
	var pos1;
	var pos2;
	var param;
	var queryString = new String(sPage)
	param = "";
	
	pos1 = queryString.indexOf(sParameter);
	if (pos1 > 0) {		
		pos2 = queryString.indexOf("&", pos1);
		
		if (pos2 <= 0)
		{
			//Last parameter
			pos2 = queryString.length;
		}		
		param  =  queryString.substr(pos1+sParameter.length,pos2-pos1-sParameter.length);
							
		
	} 
	return param;

}


function OpenWindow(sPage, clientId, pagewindowtype)
{
 	var param;
	// If sPage does not contain RecordId in its string, get the first checked row 
	var queryString = new String(sPage);
	var pageSize;
	var pageNumber;
	var currPageRowNumber;
	var rowStart;
	var rowNumber;
	var checkboxId;
	var xx;
	var isChecked;
	var recordId
	var checkCount; 
	var newQueryString = new String("");
	
	var currId;
	var pos1;
	var pos2;
	var viewId;
	var parameterId1;
	var parameterId2;
	var parameterId3;
	var OnClickAttr = new String("");
	var IsRecChecked = "0";
	
	if (document.getElementById(clientId + "_txtPageSize") != null) {
		pageSize =  parseInt(document.getElementById(clientId + "_txtPageSize").value);
		if (isNaN(pageSize)) {
			pageSize = 20;	
		}
	}
	else
		pageSize = 20;
		
	if (document.getElementById(clientId + "_txtPageNumber") != null) 
		pageNumber = parseInt(document.getElementById(clientId + "_txtPageNumber").value);
	else
		pageNumber = 1;
	
	
	//get new row number based on pagenumber and pagesize and update querystring
	rowStart = pageSize* (pageNumber-1);	
	pos1 = queryString.indexOf("RowNum=");
	if (pos1 > 0) {		
		pos2 = queryString.indexOf("&", pos1);
		if (pos2 > 0)  {			
			currPageRowNumber =  parseInt(queryString.substr(pos1+7,pos2-pos1-7));					
			}
		else{
			currPageRowNumber = parseInt(queryString.substr(pos1+7));			
			}
	} 
	//Defect : 6220 Change is added to handle rowNumber properly
	// while opening thru clicking the record or via review/annotate/route button
	if(currPageRowNumber == -1)
	{
		rowNumber = rowStart 
	}
	else
	{
	rowNumber = rowStart+ currPageRowNumber
	} 
	newQueryString = queryString.replace("RowNum=" + currPageRowNumber, "RowNum="+rowNumber);
		
	// get record id
	currId = "" ;
	pos1 = queryString.indexOf("RecordID=");
	if (pos1 > 0) {		
		pos2 = queryString.indexOf("&", pos1);
		if (pos2 > 0)  {			
			currId =  queryString.substr(pos1+9,pos2-pos1-9)					
			}
		else{
			currId = queryString.substr(pos1+9);			
			}
	} 
	
	
	
	// get checkcount and get the first checked  record id if record id is empty
	checkCount = 0;
	
	for(xx = 0; xx < pageSize; ++xx)
	{	
		checkboxId = clientId + "_chkRow"+ xx;
		if (document.getElementById(checkboxId) != null) {
			isChecked =  document.getElementById(checkboxId).getAttribute("checked");
			if (isChecked == 1) {
				checkCount = checkCount + 1;
			
				if (document.getElementById(clientId + "_lnk"+ xx) != null)
				{
					
					if (queryString.indexOf("WEBAddress.aspx") == 0)
					{
						//WebAddress.aspx page						
						recordId = document.getElementById(clientId + "_lnk"+ xx).innerText;						
						parameterId1 = "";
						parameterId2 = "";
						parameterId3 = "";
					}
					else
					{	
						//other pages
						OnClickAttr=document.getElementById(clientId + "_lnk"+ xx).getAttribute("OnClick")
						recordId = GetParameterId(OnClickAttr, "RecordID=");												
						parameterId1 = GetParameterId(OnClickAttr, "Parameter1=");												
						parameterId2 = GetParameterId(OnClickAttr, "Parameter2=");						
						parameterId3 = GetParameterId(OnClickAttr, "Parameter3=");						
					}			
				}
												
				if (currId == "") 
				{
					//select firsr checked record
					if (checkCount == 1)
					{ 									
						newQueryString = newQueryString + "&RecordId="+ recordId +							
						"&Parameter1="+ parameterId1 + "&Parameter2="+ parameterId2 + "&Parameter3="+ parameterId3;
						IsRecChecked ="1";
						//Fixed the code to pass pagename with recordId while Select Case Number Clicking Review/Annotate/Approve   
                        currId = recordId; 

					}
				}				
				else
				{
					//verify if selected record is checked
					if ((recordId == GetParameterId(queryString, "RecordID=")) &&
					(parameterId1 == GetParameterId(queryString, "Parameter1=")) &&												
					(parameterId2 == GetParameterId(queryString, "Parameter2=")) &&												
					(parameterId3 == GetParameterId(queryString, "Parameter3=")))
					{
						IsRecChecked ="1"
					}
					
				}
			}
		}
	}
	//Fixed the code to pass pagename with recordId while  Clicking Review/Annotate/Approve buttons in thr case without selecting checkbox   
    if (checkCount == 0)     
           {                 
              var defaultrecordID = 0;     
              if (document.getElementById(clientId + "_lnk"+ defaultrecordID) != null)     
               {     
                                
                   if (queryString.indexOf("WEBAddress.aspx") == 0)     
                   {     
                       //WebAddress.aspx page                           
                       recordId = document.getElementById(clientId + "_lnk"+ defaultrecordID).innerText;                             
                       parameterId1 = "";     
                       parameterId2 = "";     
                       parameterId3 = "";     
                   }     
                   else     
                   {         
                       //other pages     
                       OnClickAttr=document.getElementById(clientId + "_lnk"+ defaultrecordID).getAttribute("OnClick")     
                       recordId = GetParameterId(OnClickAttr, "RecordID=");                                                     
                       parameterId1 = GetParameterId(OnClickAttr, "Parameter1=");                                                     
                       parameterId2 = GetParameterId(OnClickAttr, "Parameter2=");                             
                       parameterId3 = GetParameterId(OnClickAttr, "Parameter3=");                             
                   }                 
               } 
			// to load fileimage and dashboard link                            
               if (currId == "" && recordId != null)      
               {     
                  newQueryString = newQueryString + "&RecordId="+ recordId +                                 
                   "&Parameter1="+ parameterId1 + "&Parameter2="+ parameterId2 + "&Parameter3="+ parameterId3;     
                   IsRecChecked ="1";     
                  currId = recordId;     
            
               }     
        }         
	
	if ( checkCount > 0 ) {
		newQueryString = newQueryString + "&CheckCount="+ checkCount + 
		"&IsRecChecked=" + IsRecChecked;
	}
	
	// get the window name
	var pos;
	var pageName;
	pos = queryString.indexOf(".aspx");
	pageName =  queryString.substr(0,pos);
	
	
	// if any of following pages and if action id = add mode then get the parentid and parentpage
	
	var parentid;
	var parentpage;
	var actionid;
	
	pos1 = queryString.indexOf("ActionID=");
	if (pos1 > 0) {		
		pos2 = queryString.indexOf("&", pos1);
		if (pos2 > 0)  {			
			actionid =  parseInt(queryString.substr(pos1+9,pos2-pos1-9));					
			}
		else{
			actionid = parseInt(queryString.substr(pos1+9));			
			}
	} 
	if (actionid == 1 || actionid == 15) { //-- 1 ADD; 15 - Import
	
		parentpage = escape(window.name);
		if (parentpage.substr(0, 10) == "CustomerId")
		    parentpage = "Customer"
	
		if (document.getElementById("txtCode") != null) 
			parentid = escape(document.getElementById("txtCode").value);
		else
			parentid = "";
	
		newQueryString = newQueryString + "&ParentID="+ parentid + "&ParentPage=" + parentpage;
		
	}
	
	var viewid;
	
	pos1 = queryString.indexOf("ViewID=");
	if (pos1 > 0) {		
		pos2 = queryString.indexOf("&", pos1);
		if (pos2 > 0)  {			
			viewid =  parseInt(queryString.substr(pos1+7,pos2-pos1-7));					
			}
		else{
			viewid = parseInt(queryString.substr(pos1+7));			
			}
	} 
	

if (pageName.toUpperCase() == "CASE" || pageName.toUpperCase() == "ALERT" || pageName.toUpperCase() == "OFACCASE") {
	// Following viewid entries enable the type of BSA Case ID clicked obtaining  
    // its own only unique window name, no matter archived or current BSA Cases
	if (viewid == 8         // All BSA Cases
	    || viewid == 21     // All Pending BSA Cases
	    || viewid == 22     // All Closed BSA Cases
	    || viewid == 23     // My BSA Cases to Review
	    || viewid == 24     // My BSA Cases to Approve
	    || viewid == 25     // My BSA Cases to Report
	    || viewid == 26     // All Unassigned BSA Cases
        || viewid == 27     // All Archived BSA Cases
	    || viewid == 225    // All BSA Cases to Report
	    || viewid == 453    // BSA Cases by Review Stat
	    || viewid == 454    // BSA Cases by Oper/Stat
	    || viewid == 705) { // Audited BSA Cases
	    if (currId != null || (typeof(currId) != 'undefined'))
	        pageName = "Case" + currId;
	    else
            pageName = "Case";
	}
    // Following viewid entries enable the type of OfacCase ID clicked obtaining  
    // its own only unique window name, no matter archived or current Ofac Cases
	if (viewid == 115     // All Ofac Cases
	    || viewid == 116  // All Unconfirmed Ofac Cases
	    || viewid == 117  // All Confirmed Ofac Cases
	    || viewid == 118  // All Waived Ofac Cases
	    || viewid == 119  // All Delegated Ofac Cases
	    || viewid == 120  // All Archived Ofac Cases
	    || viewid == 122 ) {  // All Unproved Ofac Cases
	    if (currId != null || (typeof(currId) != 'undefined'))
	        pageName = "OFACCase" + currId;
	    else
            pageName = "OFACCase";
	}
    // Following viewid entries enable the type of Alert ID clicked obtaining its
    // own only unique window name, no matter archived or current Alert
    if (viewid == 7         // All Alerts
	    || viewid == 700    // All Alerts to be Reviewed
	    || viewid == 701    // All Alerts to be Approved
	    || viewid == 702    // All Alerts to be Archived
	    || viewid == 703    // All Archived Alerts
	    || viewid == 704) { // All Audited Alerts
	    if (currId != null || (typeof(currId) != 'undefined'))
	        pageName = "Alert" + currId;
	    else
            pageName = "Alert";
    }
	}
	
		if (viewid == 517 && (actionid == 1 || actionid == 3 || actionid == 4 ) ) {
		if ( document.getElementById("hdfldImportFlag").value == "Y" ) {
			alert("This request cannot be modified as it was created from a file.");
			return false;			
		}
		else 
		{
			if (! checkCount > 0 )
			{
                var param1Exist = newQueryString.indexOf("Parameter1");
                if (param1Exist < 0) {
                    newQueryString = newQueryString + "&Parameter1=" + document.getElementById("hdfldRequestId").value;
                }
			}
		}
	}
	
	
	param=getwindowparam(pagewindowtype);
	
	//Jan.03.2021 replace Ampersand
	var vrep = /&amp;/gi;
	newQueryString = newQueryString.replace(vrep,'&');
	if (pageName=="CrystalReport")	
	{
	   
		var nw=window.open(newQueryString, pageName + currId , param);
	}
	else if (pageName=="Customer")
	{
	  
	    var nw=window.open(newQueryString, "CustomerId" + (new Date()).getTime(), param);
	} 
	else
	{
		var nw=window.open(newQueryString, pageName, param);
	}	
	if (nw != null)
	{
	 
		nw.focus();
	}	
	
	return false;

}


function getwindowparam(pagewindowtype)
{

	var param=""
	var wx=0;
	var wy=0;
	
	if (pagewindowtype==0)
	{	
		//Main Window
		param="resizable=yes,status=yes,menubar=yes,toolbar=yes,scrollbars=yes,left=0,top=0,height=768,width=1024";
	}
	else if (pagewindowtype==2)
	{	
		//Message
		param="resizable=no,status=yes,menubar=no,toolbar=no,scrollbars=yes,left=100,top=100,height=300,width=500";
	}
	else if (pagewindowtype==3)
	{	
		//Dialog
		param="resizable=yes,status=yes,menubar=no,toolbar=no,scrollbars=yes,left=25,top=25,height=470,width=620";
	}
	else if (pagewindowtype==4)
	{	
		//Small Popup
		param="resizable=yes,status=yes,menubar=no,toolbar=no,scrollbars=yes,left=25,top=25,height=400,width=880";
	}
	else if (pagewindowtype==5)
	{	
		//Large Popup
		param="resizable=yes,status=yes,menubar=no,toolbar=no,scrollbars=yes,left=25,top=0,height=720,width=880";
	}
	else if (pagewindowtype==6)
	{	
		//Report Popup
		param="resizable=yes,status=yes,menubar=no,toolbar=no,scrollbars=yes,left=5,top=5,height=720,width=1000";
	}
	else if (pagewindowtype==7)
	{	
		//OFAC Case Popup
		param="resizable=yes,status=yes,menubar=no,toolbar=no,scrollbars=yes,left=5,top=5,height=685,width=830";
	}
	else if (pagewindowtype==10)
	{	
		//Hidden
		param="resizable=yes,status=yes,menubar=no,toolbar=no,scrollbars=yes,left=10000,top=10000,height=600,width=880";
	}
	else
	{
		//By Default General Popup Window (window type=1)
		//Make offset from the parent window
		try
		{
			wx=window.screenLeft;
			if (wx==0)
				wy=0;
			else
				wy=window.screenTop;
	
			
		}
		catch(ex){}
		param="resizable=yes,status=yes,menubar=no,toolbar=no,scrollbars=yes,left=" + (25 + wx) + 
		",top=" + (25 + wy) + ",height=600,width=880";
	}
	return param
}
function deletecheck(viewname, clientid) {
	var dodelete;
	dodelete =  confirm("Are you sure you want to delete this record?");
	if (dodelete == true) {
		getnextchecked(viewname, clientid, 0);
	}
	return dodelete;
	
}

function deleteSDNcheck(viewname, clientid) {
    try
    {
	    var dodelete;
	    var txtID;
	    
	    txtID = document.getElementById("txtCode");
	
	    if (txtID == null)
	    {
	        dodelete =  confirm("Are you sure you want to delete this record?");
	    }
	    else
	    {
	        dodelete =  confirm("Are you sure that you wish to delete " + 
	            "the Party record with the ID " + txtID.value + "?");
	    }
	    if (dodelete == true) {
		    getnextchecked(viewname, clientid, 0);
	    }
	}
	catch(ex){ dodelete = false;}
	return dodelete;
	
}


function deleteCustcheck(viewname, clientid) {
	var dodelete;
	dodelete =  confirm("Are you sure you want to delete this record? (The corresponding Acceptance Items data and Risk item data will be deleted.)");
	if (dodelete == true) {
		getnextchecked(viewname, clientid, 0);
	}
	return dodelete;
	
}

function approveExcludecheck(viewname, clientid) {
	var dodelete;
	dodelete =  confirm("Are you sure you want to approve this record?");
	if (dodelete == true) {
		getnextchecked(viewname, clientid, 0);
	}
	return dodelete;
	
}

function promptaction(action, object) {
	var doaction;
	doaction =  confirm("Are you sure you want to " + action + " the " + object + "?");
	return doaction;
	
}

function promptdisaprove() {
	var doaction;
	// Prompt message to notify user about the Rights and operator changes while clicking disapprove button in Role page
	if(window.name == "Role")
	{	
	    doaction =  confirm("Disapprove action rolls back Operator and Right changes for the Roles and "+ 
        "restores previous Approved status \n" +
        "Are you sure you want to continue? ");
	}
	else
	{
	doaction =  confirm("Disapprove action rolls back only rights changes " +
	"for the operator and restores previous Approved status \n" +
	"Are you sure you want to continue?");
	}
	return doaction;
	
}

function promptConfirm(object)
{
	var oldVal;
	var newVal;
	var mode;
	var msg;
	
	
	oldVal = document.getElementById("hdOldValue").value;
	mode = document.getElementById("hdMode").value;
	if (object == 'ActivityType')
	{
		newVal = document.getElementById("ddlExemptionType").value;
		msg = "Changing the Exemption Status for an Activity Type may result in full processing of Peer Models during End of Day processing. Do you wish to continue?." ;
	}	
	else if (object == 'ExemptionType')
	{
		msg = "Changing the Exemption Status for Peer Profiling will result in full processing of Peer Models during End of Day processing. Do you wish to continue?.";
		if (document.getElementById("chkPeering").checked == true)
			newVal = "true";
		else
			newVal = "false";
	}
	
	if (oldVal != newVal && mode == 'Mod')
	{
		var doaction;
		doaction =  confirm(msg);
		return doaction;
	}
	
}

function getcheckedrows() {
var pagesize;
var checkboxid;
var ctrkey;
var xx;
var newkey;
var ischecked;
var checkedrows;
var viewname;
	viewname ="ViewGrid";
	checkedrows = '';
	if (window.opener.document.getElementById(viewname + "_txtPageSize") != null) 
		pagesize =  parseInt(window.opener.document.getElementById(viewname + "_txtPageSize").value);
	else
		pagesize = 20;
	

	for(xx = 0; xx < pagesize; ++xx) {
		checkboxid = viewname + "_chkRow"+ xx;			
		if ( window.opener.document.getElementById(checkboxid) != null) {
			ischecked =  window.opener.document.getElementById(checkboxid).getAttribute("checked");
			if (ischecked == 1){
				ctrkey = window.opener.document.getElementById(viewname + "_lnk"+ xx).innerText;
				checkedrows = checkedrows + ctrkey + ',' ;
			}
		}
	}
	return checkedrows;
	
}
			
function RightTrim(sString) 
{
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
}


function compareparameters(prevOnClickVal, OnClickVal)
{
	try
	{
		
		       
		if (prevOnClickVal == "")
		{
			return true;
		}
		else
		{

			
			if ( compigncase(parseparameter(OnClickVal, "&Parameter1="),
			      parseparameter(prevOnClickVal, "&Parameter1=")) &&
			     compigncase(parseparameter(OnClickVal, "&Parameter2="),
			      parseparameter(prevOnClickVal, "&Parameter2=")) &&
			     compigncase(parseparameter(OnClickVal, "&Parameter3="),
			      parseparameter(prevOnClickVal, "&Parameter3=")) )  
			{
		     
				    return true;
			}
			else				
			{
					return false;
			}			
		}
	}
	catch(ex){}
	
}

function compigncase(s1, s2)
{
	try
	{
		var Str1 = new String(s1);
		var Str2 = new String(s2);
		
		if(Str1.toLowerCase() == Str2.toLowerCase())
		{
			return true;
		}
		else
		{
			return false
		}
		
	
	}
	catch(ex){}
	
}

function parseparameter(OnClickVal, paramName)
{

	try
	{
		var paramvalue = "";
		var startpos=0;
		var endpos=0;
		
		startpos = OnClickVal.indexOf(paramName);
				
		if (startpos != -1)
		{
			startpos =startpos +  paramName.length;
			endpos = OnClickVal.indexOf('&', startpos);
			
			if (endpos < 0 )
				endpos = OnClickVal.length;
				
				if ( endpos > startpos)
				{
					paramvalue = OnClickVal.substr(startpos, endpos - startpos);
					paramvalue = RightTrim(paramvalue);					
				}
				else
				{
					paramvalue = "";
				}
				
		}
		else
		{
			paramvalue = ""
		}
				
		return paramvalue;		
	}
	catch(ex){}
}

function getnextchecked(viewname, clientid, dosubmit) {
var lastchecked;
var pagesize;
var checkboxid;
var ctrkey;
var prevkey;
var prevkeyfound;
var xx;
var newkey;
var ischecked;
var doscroll;
var OnClickVal;
var prevOnClickVal;

var CheckedRecords=document.forms[0].RecordScrollBar_chkCheckedRecords;

if (CheckedRecords !=null){

	doscroll = CheckedRecords.getAttribute("checked");
	if (doscroll == 1) {

		prevkey = document.forms[0].RecordScrollBar_tb2.value;
		prevOnClickVal = document.forms[0].RecordScrollBar_tb3.value;
		
		newkey = "";
		prevkeyfound = 0;
		
		
		if (window.opener.document.getElementById(viewname + "_txtPageSize") != null) 
			pagesize =  parseInt(window.opener.document.getElementById(viewname + "_txtPageSize").value);
		else
			pagesize = 20;
			
	

		for(xx = 0; xx < pagesize; ++xx) {
			checkboxid = viewname + "_chkRow"+ xx;	
			
			if ( window.opener.document.getElementById(checkboxid) != null) {
				ischecked =  window.opener.document.getElementById(checkboxid).getAttribute("checked");
															
				if (window.name == "WEBAddress")
				{
					//WebAddress.aspx page	
					OnClickVal = "";
					prevOnClickVal = "";					
					ctrkey = window.opener.document.getElementById(viewname + "_lnk"+ xx).innerText;
				}
				else
				{
					//Other pages				
					OnClickVal = window.opener.document.getElementById(viewname + "_lnk"+ xx).getAttribute("OnClick");
					ctrkey = GetParameterId(OnClickVal, "RecordID=");
				}
				
				if ((prevkeyfound == 1) && (ischecked == 1))
				{
					newkey = ctrkey;
					break;	
				}
		
				
				if ( compigncase(RightTrim(ctrkey), RightTrim(prevkey)) )
				{					
					
					
					if ( compareparameters(prevOnClickVal, OnClickVal.toString()) )					
						prevkeyfound = 1;
					
				}
			}
			
			
		}
	
		if (newkey != "") { 
			document.forms[0].RecordScrollBar_tb2.value = newkey;
			document.forms[0].RecordScrollBar_tb3.value = OnClickVal.toString();
			if (dosubmit == 1) {			
				document.forms[0].submit();
				}
			}
		}
	}
}


function getprevchecked(viewname, clientid) {
var lastchecked;
var pagesize;
var checkboxid;
var ctrkey;
var prevkey;
var currkey;

var xx;
var newkey;
var ischecked;
var OnClickVal;
var prevOnClickVal;
var prevSelOnClickVal;

try {
doscroll = document.forms[0].RecordScrollBar_chkCheckedRecords.getAttribute("checked");
if (doscroll == 1) {
	currkey = document.forms[0].RecordScrollBar_tb2.value;
	prevSelOnClickVal = document.forms[0].RecordScrollBar_tb3.value;
	prevkey = "";
	
	newkey = "";

	if (window.opener.document.getElementById(viewname + "_txtPageSize") != null) 
		pagesize =  parseInt(window.opener.document.getElementById(viewname + "_txtPageSize").value);
	else
		pagesize = 20;

	
	for(xx = 0; xx < pagesize; ++xx) {
		checkboxid = viewname + "_chkRow"+ xx;	
		
		
		if ( window.opener.document.getElementById(checkboxid) != null) {
			ischecked =  window.opener.document.getElementById(checkboxid).getAttribute("checked");
		
			if (window.name == "WEBAddress")
			{
				//WebAddress.aspx page	
				OnClickVal		  = "";
				prevOnClickVal	  = "";
				prevSelOnClickVal = "";					
				ctrkey = window.opener.document.getElementById(viewname + "_lnk"+ xx).innerText;
			}
			else
			{
				//Other pages
				OnClickVal = window.opener.document.getElementById(viewname + "_lnk"+ xx).getAttribute("OnClick");
				ctrkey = GetParameterId(OnClickVal, "RecordID=");
			}
			
			if (compigncase(RightTrim(ctrkey), RightTrim(currkey)))
			{		
			
				if ( compareparameters(prevSelOnClickVal, OnClickVal.toString()) )	
				{
					newkey = prevkey;
					OnClickVal = prevOnClickVal;		
					break;
				}
			}	
		
			if (ischecked == 1 ) 
			{
				
				prevkey = ctrkey;
				prevOnClickVal = OnClickVal;	
			}
				
		}
	}
	
		if (newkey != "") { 
			document.forms[0].RecordScrollBar_tb2.value = newkey;			
			document.forms[0].RecordScrollBar_tb3.value = OnClickVal.toString();
			//document.forms[0].submit();
		}
	}
}
catch(ex){}
}


function SlideActionBar(sActionBarID, objLink)
{
try
{

		if (document.getElementById(sActionBarID).style.display =="none")
		{
			document.getElementById(sActionBarID).style.display ="";
			objLink.innerText="hide..."			
			
		}
		else
		{
			document.getElementById(sActionBarID).style.display ="none";
			objLink.innerText="more..."
		}
	
}
catch(e){}	
}


//---------------------------------Query Plus----------------


function ShowMoreParam(sAdditionalParamId, sParamId, objLink)
{
try
{

		if (document.getElementById(sAdditionalParamId).style.display =="none")
		{
			document.getElementById(sParamId).style.display ="none";
			
			
			/*try
			{
				document.getElementById(sAdditionalParamId).filters[0].Apply();
			}
			catch(e){}*/
			

			document.getElementById(sAdditionalParamId).style.display ="";
			objLink.innerText="Show Main Parameters..."
			
			/*try
			{
				document.getElementById(sAdditionalParamId).filters[0].transition=5;
				document.getElementById(sAdditionalParamId).filters[0].Play();
			}
			catch(e){}*/
			
			
		}
		else
		{
			document.getElementById(sParamId).style.display ="";
			document.getElementById(sAdditionalParamId).style.display ="none";
			objLink.innerText="More Parameters..."
		}
	
}
catch(e){}	
}


function selectOperation(obj, sLblFromID, sLblToID, sTxtFromID, sTxtToID,  iRangeOperation, iNullOperation, iNotNullOperation)
{
	if (obj.value == iRangeOperation)
	{
		document.getElementById(sLblFromID).style.display = "";
		document.getElementById(sLblToID).style.display   = "";	
		document.getElementById(sTxtFromID).style.display   = "";	
		document.getElementById(sTxtToID).style.display   = "";	
			 
	}
	else
	{
		document.getElementById(sLblFromID).style.display = "none";
		document.getElementById(sLblToID).style.display   = "none";	
		document.getElementById(sTxtToID).style.display   = "none";	
		document.forms[0].item(sTxtToID).value = "";	
		
		if (obj.value == iNullOperation || obj.value == iNotNullOperation)
		{
			document.getElementById(sTxtFromID).style.display   = "none";	
			document.forms[0].item(sTxtFromID).value = "";	
		}
		else
		{
			document.getElementById(sTxtFromID).style.display   = "";	
			
		}
		
	}
}


function selectOperationDDL(obj, sTxtFromID, sDDLFromID,  iNullOperation, iNotNullOperation, 
                        iInOperation, iNotInOperation)
{	
    try
    {	
       
		if (obj.value == iNullOperation || obj.value == iNotNullOperation)
		{
			document.getElementById(sTxtFromID).style.display   = "none";	
			document.forms[0].item(sTxtFromID).value = "";
			document.getElementById(sDDLFromID).style.display   = "none";	
			document.forms[0].item(sDDLFromID).selectedIndex = 0;
			
			
		}
		else if (obj.value == iInOperation || obj.value == iNotInOperation)
		{
			document.getElementById(sTxtFromID).style.display   = "";
			document.getElementById(sDDLFromID).style.display   = "none";
			document.forms[0].item(sDDLFromID).selectedIndex = 0;	
			
			
		}		
		else
	    {
	        document.getElementById(sTxtFromID).style.display   = "none";
	        document.forms[0].item(sTxtFromID).value = "";
			document.getElementById(sDDLFromID).style.display   = "";
	    }
		
	}
	catch(ex) {}
}



//-------------------------------Rule Builder--------------------
function SelectRuleType(iType, sTemplatesID, sRulesID, sSPNamesID,
 sBuildRuleId, sTypeLabelID, sGenTypesID)
{

	switch(iType)
	{
		case 0:
		{
			document.getElementById(sTemplatesID).style.display = "";
			document.getElementById(sRulesID).style.display = "none";
			document.getElementById(sTypeLabelID).style.display = "none";
			document.getElementById(sGenTypesID).style.display = "none";
			document.getElementById(sSPNamesID).style.display = "none";
			document.getElementById(sBuildRuleId).style.display = "none";
			break;
		}
		
		case 1:
		{
			document.getElementById(sTemplatesID).style.display = "none";
			document.getElementById(sRulesID).style.display = "";
			document.getElementById(sTypeLabelID).style.display = "";
			document.getElementById(sGenTypesID).style.display = "";
			document.getElementById(sSPNamesID).style.display = "none";
			document.getElementById(sBuildRuleId).style.display = "";
			break;
		}
		
		
		case 2:
		{
			document.getElementById(sTemplatesID).style.display = "none";
			document.getElementById(sRulesID).style.display = "none";
			document.getElementById(sTypeLabelID).style.display = "none";
			document.getElementById(sGenTypesID).style.display = "none";
			document.getElementById(sSPNamesID).style.display = "";
			document.getElementById(sBuildRuleId).style.display = "none";
			break;
		}
		
		
		
	}
}

function selectSQLBuilderOperation(obj,  sTxtFromID, iNullOperation, iNotNullOperation, objTable)
{
        
	var txtValue=document.getElementById(sTxtFromID);
	var cboTable = document.getElementById(objTable);
		
	if (txtValue !=null)
	{	
		if (obj.value == iNullOperation || obj.value == iNotNullOperation || cboTable.value == "-99")
		{
			txtValue.disabled =true;
			txtValue.value = "";	
		}
		else
		{
			txtValue.disabled =false;
			
		}
	}
		
	
}


/*---------------------Combobox Control---------------------------------*/
function SelectValue(txtId, lstId) 
{

	var txtValue  = document.getElementById(txtId);	
	var lstValues = document.getElementById(lstId);
	var iSelected = lstValues.selectedIndex;		
	
	
	if (iSelected>=0 && (document.getElementById(txtId).getAttribute("readonly")==false) )
		txtValue.value= lstValues.options[iSelected].text;	
		
	HideList(lstId);
}

function ShowList(lstId)
{
	if (document.getElementById(lstId).style.display=="")
	{
		HideList(lstId);
	}
	else
	{		
		document.getElementById(lstId).style.display="";	
		document.getElementById(lstId).style.zIndex = 1;		
	}
}

function HideList(lstId)
{
	document.getElementById(lstId).style.zIndex=2;
	document.getElementById(lstId).style.display="none";
}



/*-------------  OFAC Search-----------------------------*/

function SelectOFACAlgorithm(obj,  MatchNameMsg, MatchTextMsg, MatchTextAlgorithm)
{
	if (obj.value == MatchTextAlgorithm)
	{
		document.getElementById("txtMatchName").style.display = "none";
		document.getElementById("txtMatchText").style.display   = "";	
		document.getElementById("lblMatchName").innerText=MatchTextMsg
		
			 
	}
	else
	{
		document.getElementById("txtMatchName").style.display = "";
		document.getElementById("txtMatchText").style.display   = "none";	
		document.getElementById("lblMatchName").innerText=MatchNameMsg
		
	}
}

/*------------------- Log off --------------------------------*/
function logoff(bldFileImg)
{
	if (bldFileImg==1)
	{
		if (confirm("A new SDN File Image has not been built. \n" + 
		"Click OK to build File Image now. \n" +
		"Click Cancel to log off without building File Image." ))
		{
			buildfileimage();
			return false;
		}
		
	}	
	
	document.location.href="login.aspx?msg=0";
	return true;
		
}


function buildfileimage()
{	
	OpenWindow("LoadFileImage.aspx", "", 4);	
	return true;
}


function shownotifications()
{	
	OpenWindow("Notifications.aspx", "", 1);	
	return true;
}


var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}

function MakeReadOnly(ctrlid, ctrltype) {
	var ctrl;
	ctrl = document.getElementById(ctrlid);
	var nnm = ctrl.attributes;
	switch(ctrltype) 
	{
		case 0: //Textbox
		{
			ctrl.style.backgroundColor="LightGrey";	
			var namedItem = document.createAttribute("readonly");
			namedItem.value = "readonly";
			nnm.setNamedItem(namedItem);
			ctrl.value = "";
			break;
		}
		case 1: //DropDownlist
		{
			var namedItem = document.createAttribute("disabled");
			namedItem.value = "disabled";
			nnm.setNamedItem(namedItem);
			break;
		}
		case 2: //CheckBox
		{
			var namedItem = document.createAttribute("disabled");
			namedItem.value = "disabled";
			nnm.setNamedItem(namedItem);
			break;			
		}
	}
}

function ClearReadOnly(ctrlid, ctrltype) {
	var ctrl;
	ctrl = document.getElementById(ctrlid);
	switch(ctrltype) 
	{
		case 0: //Textbox
		{
			
			ctrl.style.backgroundColor="White";			
			var namedItem = document.createAttribute("readonly");
			var nnm = ctrl.attributes;
			namedItem.value = "";
			nnm.setNamedItem(namedItem);
			ctrl.removeAttribute("style");	
			
			break;
		}
		case 1: //DropDownlist
		{
			ctrl.removeAttribute("disabled");		
			break;
		}
		case 2: //CheckBox
		{
			ctrl.removeAttribute("disabled");
			break;			
		}
	}
}

function changeWindowName(newName) {

	  window.name = newName;
        AddWindowToPCSWindowCookie(window.name); 
}

/*---------------------  ucNote Control validate textbox---------------------------------*/
function IsTxtNotesValid(objID)
{
	var str=document.getElementById(objID).innerText; 
	if (str.replace(/^\s+/g,'').replace(/\s+$/g,'')=='') 
	{
		alert('Please enter annotation text.');
		return false; 
	}
	else
	{
		return true;
	}
}
function filterNum(str) {
    re = /\$|,|@|#| |~|`|\%|\*|\^|\&amp;|\(|\)|\+|\=|\[|\-|\_|\]|\[|\}|\{|\;|\:|\'|\"|\&lt;|\&gt;|\?|\||\\|\!|\$|\./g;
    // remove special characters like "$" and "," etc...
    return str.replace(re, "");
}
function clearlabelText (objID)
{
	objID.innerHTML = "";
}
			
function checkMaxLength(e,objID1, objID2, objID3, stampLen, MaxLength)
{
	var str1;
	var str2;
	objID3.innerHTML = "";
	
	switch(e.keyCode) 
	{
		case 37: // left
			return true;
		case 38: // up
			return true;
		case 39: // right
			return true;
		case 40: // down
			return true;
		case 8: // backspace
			return true;
		case 46: // delete
			return true;
		case 27: // escape
			return true;
	}
	
	if (event.type=="paste")
	{
		str2 = window.clipboardData.getData("Text");	
		str1 = "The existing notes, the add new note, and the text you are attempting to paste cannot exceed " + MaxLength   + " characters." ;
	}
	
	if (event.type=="keypress")
	{
		str2 = "1";
		str1 = "The existing notes and the note you are attempting to annotate cannot exceed " + MaxLength + " characters." ;
	}
	
	if (objID2.value.length == 0)
	{
		str1 = "Cannot add note because the existing notes reached the maximum number of "+ MaxLength +   " characters.";
	}
	
	if ( objID1.value.length + (objID2.value.length + str2.length) + stampLen <= MaxLength )
	{
		objID3.innerHTML = "";	
		return true;
	}
	else
	{
		objID3.innerHTML = str1;	
		return false;
	}
}

function undeletecheck(viewname, clientid) {
	var doundelete;
	doundelete =  confirm("Are you sure you want to restore this record?");
	if (doundelete == true) {
		getnextchecked(viewname, clientid, 0);
	}
	return doundelete;
	
}

function isDelClicked()
{   
    var rtn = false;
    if(document.getElementById('txtDelReason').value != "")
    {
        rtn = confirm('Are you sure you want to delete this record?');  
    }
    else
    {
        alert('Delete reason is mandatory');
        document.getElementById('txtDelReason').focus();
    }
    
    return rtn;
}
     
function chkValidNum()
{   
    var txtFrom = document.getElementById('txtRecordsFrom');
    var lblRecMsg = document.getElementById('cValidationSummary1');
    if(txtFrom.value != "")
    {
        if(/^([0-9])+$/.test(txtFrom.value))
        { 
            txtFrom.style.backgroundColor = "White";
            txtFrom.title = "";
            lblRecMsg.style.visibility = "hidden";
        }
        else
            txtFrom.style.backgroundColor = "Yellow";
    }
    else
    {
        txtFrom.style.backgroundColor = "Yellow";
    }
    
    var txtTo = document.getElementById('txtRecordsTo');
    
    if(txtTo.value != "")
    {
        if(/^([0-9])+$/.test(txtTo.value))  
        {
            txtTo.style.backgroundColor = "White";
            txtTo.title = "";
            lblRecMsg.style.visibility = "hidden";
        }
        else
            txtTo.style.backgroundColor = "Yellow";
    }
    else
    {
        txtTo.style.backgroundColor = "Yellow";
    } 
}
//ClearAllOFacLock will clear the OFAC Case Lock on closing the Application
function ClearAllOfacLock()
{	
	try
	{
	
		if (window.event)
			keycode = window.event.keyCode;
		else if (e)
		keycode = e.which;	
													
		if(!((keycode == 116) || (keycode == 93)))
		{
			if(window.event.altKey && keycode == 0) 
			 {					
				//Close the Ofac case when ALT+F4 is pressed 
				unlockOfacCases ("CLEARALLOFACCASELOCK");
			 }
			else if(window.event.ctrlKey && keycode == 0) 
			{
				//Close the Ofac case when CTRL + W is pressed 
				unlockOfacCases ("CLEARALLOFACCASELOCK");
			 }  
					  
			else if((!(window.event.altKey)) && keycode==0)
			{ 	    
				var result = document.activeElement; 
				//document.active element is null when we close using'X' button, rightclick+close ,file+close
				//document.active element is not null when we are navigating from one page to other					  		   
				if(result == null)
				 {
				 // Close the Ofac case when Queryplus 'X' button is clicked,
				 unlockOfacCases ("CLEARALLOFACCASELOCK");
				}
			}
		}
	}
	catch(ex){}			
}
//Invoke UnlockRecord to clear OFAC Case lock
function unlockOfacCases (objectname)
{	
	try
	{
	
		var recordid = "";
		var objectname;
		var sessiontype;				
						
		sessiontype = 1;		
		var sPage = "UnlockRecord.aspx?RecordId=" + recordid  + "&ObjectName=" + objectname +
		"&sessiontype=" + sessiontype;
		var param="resizable=no,status=no,menubar=no,toolbar=no,scrollbars=no,left=10000,top=10000,height=1,width=1"; 
		var nw=window.open(sPage,'', param);	
		event.stopPropagation();			    
		window.close();
	}
	catch(ex){}				     
	
}					
//Function to remove the window name and update the cookie.	
function RemoveWindowFromPCSWindowCookie(windowName)
{  
  var pos1 = 0;  
  var newwindowNameValues = "";  
  var windowNameValues = new Array();  
  
 if((windowName != "") && (windowName != null) && (windowName != "undefined")) 
 {  
      if (getCookie("PCSTimeoutHandlingValue") != "false")
      {
          windowNameValues = getCookie("PCSwinnames");  
            
          if((windowNameValues != "") || (windowNameValues != null))
          {                   
                pos1 = windowNameValues.indexOf(windowName); 
                
                if (pos1 >= 0)
	            {	            
	                newwindowNameValues = windowNameValues.replace("|" + windowName + "|",""); 
	                //Set the cookie after the removal of the window name
                        setCookie("PCSwinnames", newwindowNameValues, 1); 
	            }
           }
       }
   }
}

//fucntion to open override modal popup
function openoverride(param) {
    var sFeatures = "dialogHeight: 300px;scroll:no;dialogWidth: 600px"
    var nw = window.showModalDialog(param, "Override",sFeatures);
    return nw;
}