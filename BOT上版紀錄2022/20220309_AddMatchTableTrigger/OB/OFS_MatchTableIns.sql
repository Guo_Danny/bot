USE [OFAC]
GO

/****** Object:  Trigger [dbo].[OFS_MatchTableIns]    Script Date: 3/9/2022 5:05:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE TRIGGER [dbo].[OFS_MatchTableIns] ON [dbo].[MatchTable]  
FOR INSERT
AS
  if @@rowcount > 0
  begin
    declare @ent varchar(50), @seqnumb varchar(50)

    select @ent = entNum, @seqnumb = SeqNumb from Inserted

	update MatchTable set MatchCountry = Program 
	from SDNTable(nolock) s
	join MatchTable(nolock) m on s.EntNum = m.entNum
	where m.SeqNumb = @seqnumb and m.entNum = @ent

    if @@error <> 0
        rollback tran
  end
GO

ALTER TABLE [dbo].[MatchTable] ENABLE TRIGGER [OFS_MatchTableIns]
GO


