use pbsa
go

/*
select distinct c1.id, c1.Name, c2.Id, c2.Name 
from Customer c1 --c1 = noncust, c2 = original
join Customer c2 on c1.name = c2.Name
where c1.id like 'Noncust%' and c2.id like '0%'
*/

--back noncust customer
select distinct C1.*
from Customer c1 --c1 = noncust, c2 = original
join Customer c2 on c1.name = c2.Name
where c1.id like 'Noncust%' and c2.id like '0%'

--back original customer
select distinct C2.*
from Customer c1 --c1 = noncust, c2 = original
join Customer c2 on c1.name = c2.Name
where c1.id like 'Noncust%' and c2.id like '0%'

select distinct C1.id + ',' + C2.Id
from Customer c1 --c1 = noncust, c2 = original
join Customer c2 on c1.name = c2.Name
where c1.id like 'Noncust%' and c2.id like '0%'

