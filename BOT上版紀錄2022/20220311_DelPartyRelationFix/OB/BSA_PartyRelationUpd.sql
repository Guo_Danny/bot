USE [PBSA]
GO

/****** Object:  Trigger [dbo].[BSA_PartyRelationUpd]    Script Date: 3/11/2022 11:09:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER TRIGGER [dbo].[BSA_PartyRelationUpd] on [dbo].[PartyRelation]   For Update 
As

  declare	@code SCode,
			@oper SCode

  If @@rowcount > 0 and UPDATE(Deleted) begin
	insert into PartyRelationHist(PartyId, Relationship, RelatedParty, RelationSource, CreateOper, CreateDate, LastOper, LastModify, Deleted)
	select PartyId, Relationship, RelatedParty, RelationSource, CreateOper, CreateDate, LastOper, LastModify, Deleted
		from inserted where Deleted = 1

	delete PartyRelation 
	from PartyRelation(nolock) PR join inserted i on PR.PartyId = i.PartyId and PR.RelatedParty = i.RelatedParty and PR.Relationship = i.Relationship
	where i.Deleted = 1

	select @code = PartyId, @oper = LastOper from inserted

  	Exec BSA_InsEvent @oper, 'Del', 'PartyRel', @code, Null
  End
GO


