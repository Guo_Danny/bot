﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Unisys.AML.ImportService")]
[assembly: AssemblyDescription("Feb 11, 2022 mark wc add column function. May 06 relation import modify")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Unisys Corp")]
[assembly: AssemblyProduct("Unisys.AML.ImportService")]
[assembly: AssemblyCopyright("Copyright © Unisys Corp 2022")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ec624da0-02b7-4d57-80c1-0c8db30e2cd3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.1.5")]
[assembly: AssemblyFileVersion("2.0.1.5")]
