USE [OFAC]
GO
/****** Object:  UserDefinedFunction [dbo].[OFS_fnGetMatchText]    Script Date: 3/23/2022 10:44:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO



--OFAC CASE with OFAC Web Service , GET Match Text VALUE
ALTER FUNCTION [dbo].[OFS_fnGetMatchText](  @sSrhName VARCHAR(8000), @sMatchName VARCHAR(1000) )
RETURNS VARCHAR(200)
  As
BEGIN

		declare @testsrings varchar(1000),@sStr varchar(1000),@sStr1 varchar(1000)
		declare @sStrTmp varchar(1000), @sStrTmp1 varchar(1000), @sStrTmp2 varchar(1000)
		--declare @sSrhName varchar(1000),@sMatchName varchar(1000)
		declare @iDeff int, @iPos int, @iLen1 int , @iLen2 int, @iCnt int
		declare @ssSearchName varchar(8000),@ssMatchName varchar(1000), @sNewLine varchar(300)    --2021-12-07 add 
		declare @iProcess int ,@iMatchFlag int   --2022-03-31 change                                                            --2021-12-15 add
 
		--select @sSrhName='FINO ESTILO SHOES CO., LIMITED0501-2 DONG HUI HUA FU CHAOZHOU DAD AO, CHAOZHOU CITY GUANGDONG PROVINMC |E, CHINA'
		--Select @sMatchName='HUA_FU'
		--select @sSrhName='REAL & ESTATE INVESTMENT FUND'
		--select @sMatchName='CHINA REAL ESTATE RESEARCH ASSOCIATION'
	    --SELect @sMatchName='PETROCHINA INTERNATIONAL (SINGAPORE)'
	    --select @sSrhName='PETROCHINA INTERNATIONAL |(SINGAPORE) PTE LTD'
		--Select @sSrhName='/BENEFRES/AE123/XXXXXXXXXXXXXXXXX MANSOUR, Muna Salim Saleh |H & H METALFORM GMBH'
		--SELECT @sMatchName='H + H'
		--select @sMatchName='H+H METALFORM'
		--select @sMatchName='H+H METALFORM MASCHINENBAU UND VERTRIEBS GMBH'
		--select @sSrhName='/0003-006 PETROCHINA INTERNATIONAL |(SINGAPORE) PTE LTD'
		--select @sMatchName='PETROCHINA INTERNATIONAL (SINGAPORE)'
	    --SELECT @sSrhName='/172007006215 CHANG CHIA-CHYANG'
		--SELECT @sMatchName='CHANG,CHI-CHIANG'
	    --SELECT @sSrhName='FLAT c 3/F, YEUNG YIU CHING'
		--SELECT @sMatchName='YEUNG YIU-CHING'
	    --SELECT @sSrhName='/652384918838 SHANGHAI MINGYU MARINE SERVICE CO. LIMITED CN'
		--SELECT @sMatchName='SHINICHI MUNEMASA'
		--SELECT @sSrhName='/ES1921008610180200007228 1/BAYNAH,Yasin Ali 2/ZZZ THE EAGLES L.L.C. YYY'
        --SELECT @sMatchName='BAYNAH, YASIN'
	    --SELECT @sMatchName='MANSOOR,MOUNA SALLIM'
		--SELECT @sMatchName='MANSOUR,MUNA SALEM SALEH'
		--SELECT @sMatchName='MUNA SALIM SALEH MANSOUR'
		--SELECT @sSrhName='/BENEFRES/AE123/XXXXXXXXXXXXXXXXX MANSOUR, MUNA SALIM SALEH |H & H METALFORM GMBH'
        --SELECT @sSrhName='ALWAYS SMOOTH CO LTD ALL ENERGY INVESTMENTS LTD MEN OF THE ARMY OF AL-NAQSHBANDIA WAY'
        --SELECT @sSrhName='SSS DANDONG ZHICHENG METALLIC MATERIAL CO., LTD. UUU'
		--SELECt @sMatchName= 'DANDONG ZHICHENG METALLIC MINERAL CO., LIMITED'	
		--SELECT @sMatchName='OOP DANDONG ZHICHENG METALLIC LIMITED'
		--SELECT @sSrhName='FINO ESTILO SHOES CO., LIMITED0501-2 DONG HUI HUA FU CHAOZHOU DAD AO, CHAOZHOU CITY GUANGDONG PROVINMC |E, CHINA'
		--SELECT @sMatchName='GUANGDONG PROVINCIAL CHINA TOBACCO INDUSTRY CO LTD'	
		--SELECT @sSrhName='/172007006215 |CHANG CHIA CHYANG NO.5,ALLEY 12,LANE 590,YUNG FENG RD.,'
	    --SELECT @sMatchName='CHANG,CHI-CHIANG'	
        --SELECT @sSrhName='ALWAYS SMOOTH CO LTD ALL ENERGY INVESTMENTS LTD MEN OF THE ARMY OF AL-NAQSHBANDIA WAY'
		--SELECt @sMatchName= 'AL -'
	    --SELECT @sSrhName='/801401076838 SHANGHAI SHYLON LIGHTING GROUP LIMITED'
		--SELECT @sMatchName='SAMSIAH SULIMAN'
        --SELECT @sSrhName='AFANASYEVA, Yulia Andreevna AGROPECUARIA Y REFORESTADORA HERREBE LTDA.'
		--SELECt @sMatchName= 'AFANASYEVA,YULIA ANDREEVNA'
        --SELECT @sSrhName='AFANASYEVA, Yulia Andreevna AGROPECUARIA Y REFORESTADORA HERREBE LTDA.'
		--SELECt @sSrhName='/0003-006 PETROCHINA INTERNATIONAL |(SINGAPORE) PTE LTD |HONG KONG |HONGYI FUTURES INVESTMENT CO LIMITED |SH |HONG PANG GEMS & JEWELLERY (HK) CO LIMITED |ZA'
		--SELECT @sMatchName='INOVEST LIMITED'
        --SELECt @sSrhName='/0003-006 PETROCHINA INTERNATIONAL |(SINGAPORE) PTE LTD |HONG KONG |HONGYI FUTURES INVESTMENT CO LIMITED |SH |HONG PANG GEMS & JEWELLERY (HK) CO LIMITED |ZA'
        --SELECT @sMatchName='KING HUNG INVESTMENT CO., LTD.'
		--SELECT @sSrhName='/0003-006 PETROCHINA INTERNATIONAL |(SINGAPORE) PTE LTD |HONG KONG |HONG PANG GEMS & JEWELLERY (HK) CO LIMITED |ZA'
        --SELECT @sSrhName='/2012123 |1/MINA INTERNATIONAL LIMITED |HONG KONG |HONG KONG'
		--SELECT @sMatchName='HONG PANG GEMS & JEWELLERY COMPANY LIMITED'
		--SELECT @sSrhName='HONG KONG |HONGYI FUTURES INVESTMENT CO LIMITED |INVESTMENT CO LIMITED'
		--SELECT @sMatchName='HONG KONG HONG PANG GEMS AND JEWELLERY CO., LTD.'
		--SELECt @sSrhName='USD30, TRANSPORTATION SERVICE CO LTD |TSAI, WEN-FU |SHANGHAI MINGYU MARINE SERVICE CO LIMITED CN |WORLD WAY SHIPPING AGENCY'
		--SELECT @sMatchName='SHINICHI MUNEMASA'
		--SELECT @sSrhName='USD30, |CHANG CHIA-CHYANG |TSAI, WEN-FU |SHANGHAI MINGYU MARINE SERVICE CO LIMITED CN |WORLD WAY SHIPPING AGENCY'
		--SELECT @sMatchName='WEN SHENG CHIANG'
	    --SELECT @sSrhName='/4367519704 Mayer import LTD LIABILITY CO |83 A CENTRAL AVE RIDGFIELD PARK, N3 07660-0000'
	    --SELECT @sMatchName='LIMITED LIABILITY COMPANY'
		 --SELECT @sSrhName='USD30, TRANSPORTATION SERVICE CO LTD |TSAI, WEN-FU |SHANGHAI MINGYU MARINE SERVICE CO LIMITED CN |WORLD WAY SHIPPING AGENCY'
	     --SELECT @sMatchName='TRANSAFE SERVICES LTD.'
		 --SELECT @sSrhName='/T059007370 |NI YEN CHIU |NO,147 NAN AN RD,PULI TOWNSHIP, |NANTOU,TAIWAN.R.O.C,' 
		 --SELECT @sMatchName='- NAN'
	     --SELECT @sSrhName='/40702840627000000135 |1/OBSHestvo s ogranichennoy otvetst |1/VENNOSTYU ZARYAD INN 434 |2/PROFSOYUZNAYA STR.,9 |3/RU/KIROV'
		 --SELECT @sMatchName='OBSHCHESTVO S OGRANICHENNOI OTVETSTVENNOSTUI PROMSERVIS18'
		 --SELECT @sMatchName='OBSHCHESTVO S OGRANICHENNOI OTVETSTVENNOSTIU BASTION-2000'
		 --SELECT @sMatchName='OBSHCHESTVO S OGRANICHENNOI OTVETSTVENNOSTIU 1C SEVERO-ZAPAD'
		 --SELECT @sMatchName='OBSHCHESTVO S OGRANICHENNOI OTVETSTVENNOSTIU RFPI UPRAVLENIE INVESTITSIIAMI-9'
		 --SELECT @sMatchName='OBSHCHESTVO S OGRANICHENNOY OTVETSTVENNOSTYU GARAZH-S'
		 --SELECT @sMatchName='OBSHCHESTVO S OGRANICHENNOY OTVETSVENNOSTYU TRAST 4'
	     --SELECT @sSrhName='/40702840627000000135 |1/OBSHESTVO S OGRANICHENNOY OTWETST |1/VENNOSTYU ZARYAD INN 4346052828 |2/PROFSOYUZNAYA STR.,9 |3/RU/KIROV'
		 --SELECT @sMatchName='OBSHCHESTVO S OGRANICHENNOY OTVETSTVENNOSTYU SBD'
		 --SELECT @sMatchName='OBSHCHESTVO S OGRANICHENNOY OTVETSTVENNOSTYU SB-GRUPP'
		 --SELECT @sSrhName='/148007083296 |1/YI-FANG WANG |2/93, ZHONGSHAN ROAD, TAMSUI DISTRI |2/CT |3/TW NEW TAIPEI CITY 251' 
		 --SELECT @sMatchName='- YI'
		 --SELECT @sSrhName='/0200957000623 |1/APEX CIRCUIT THAILAND CO.,LTD. |2/RM. DUNHUA N. |2/RD., SONGSHAN DIST., |3/TW/TAIPEI CITY'
	     --SELECT @sSrhName='/0200957000623 |1/APEX CIRCUIT THAILAND CO.,LTD. |2/RM. 503. 5F., NO. 205, DUNHUA N. |2/RD. SONGSHAN DIST., |3/TW/TAIPEI CITY'
		 --SELECT @sMatchName='N-2'
         --SELECT @sSrhName='/201114110070626 |1/MIMA INTERNATIONAL LIMITED |2/ROOM 3602 LEVEL 36 TOWER 1 KOWLOO |3/HK/HONG KONG'
	     --SELECT @sMatchName='MIMA'
		 --SELECT @sMatchName='ARI INTERNATIONAL Limited'
		 --SELECT @sMatchName='ARI INTERNATIONAL LTD'
		 --SELECT @sSrhName='/201114110070626 |1/MIMA INTERNATIONAL,LTD |2/ENT.SQ FIVE 38 WANG CHIU |2/ ROAD,TOW.1 |HK/HONG KONG-KOWLOON'
		 --SELECT @sMatchName='MENI INTERNATIONAL LIMITED'
		 --SELECT @sSrhName='/4367519704|MAYER IMPORT LTD LIABILITY CO|83 A CENNTRAL AVE|RIDGEFIEL PARK, NJ 07660-0000'
		 --SELECT @sMatchName='DR. A'
		 --SELECT @sMatchName='DR.A'
		 --SELECT @sMatchName='A DR.'
		 --SELECT @sSrhName='/T11994179 |YI WEN LIN |4F-2,NO.631,BO-AL 3RD RD,ZUOYING |Dist., Kaohsiung City, Taiwan'
		 --SELECT @sMatchName='B.O.'
         --SELECT @sSrhName='/T119941279 |YI WEN LIN |4F-2,NO.631,BO-AI 3RD RD, ZUOYING |DIST., KAOHSIUNG CITY ,TAIWAN'
         --SELECT @sMatchName='LI,YI'
		 --SELECT @sSrhName='/201114110070626 |1/MIMA INTERNATIONAL,LTD |2/ENT.SQ FIVE 38 WANG CHIU |2/ ROAD,TOW.1 |3/HK/HONG KONG-KOWLOON'
		 --SELECT @sMatchName='EL 1'
		 --SELECT @sSrhName='/2012123 |1/JOINT STOCLI KAIK COMPANY'
		 --SELECT @sMatchName='JOINT STOCK COMPANY NAUCHNO-PROIZVODSTVENNOYE OBYEDILNENIYE IM. S.A.LAVOCHKINA'
		 --SELECT @sSrhName='/2012123 |1/LIMITED LIABILITY COMPANY'
		 --SELECT @sMatchName='LIMITED LIABILITY COMPANY TSIFROVYE TEKHNOLOGII'
		 --SELECT @sMatchName='LIMITED LIABILITY COMPANY MARKETING ASSOCIATION TVERNEFTEPRODUCT'
		 --SELECT @sSrhName='/138203844657 |LIN, YU WEN |HOUSE 6 SINARAN DRIVE HEX32 |13 SOLEIL AT SINARAN |SINGAPORE 307468'
		 --SELECT @sMatchName='MS-13'
		 --SELECT @sSrhName='DE MARTINI TAMAYO, Sergio Rene |DESARROLLO GEMMA CORPORATION'
		 --SELECT @sMatchName='DIAZ,MANUEL'
		 --SELECT @sMatchName='SCS'
		 --SELECT @sSrhName='PI/NO/111012601/26.01.2022'
		 --SELECT @sMatchName='P.I.'
		 --SELECT @sSrhName='/161051040825 |HSIEH,YI SEN'
		 --SELECT @sMatchName='YI -'
		 --SELECT @sSrhName='/INS/SHSBCSGSGXXX'
		 --SELECT @sMatchName='IOANNIS SKOUFAS'
		 --SELECT @sSrhName='/ATTN/CABLE FEE USD50 AND |DISCREPANCY FEE USD100 |FOR INVOICE MO.A / B - 89912'
		 --SELECT @sMatchName='A -'
		 --SELECT @sSrhName='/228007029589 |RAY YEE TRADING CO LTD |NO152 XINXING 1ST ST,GUISHAN |DIST TAOYUAN CITY,TAIWAN'
		 --SELECT @sMatchName='- YEE'
		 --SELECT @sSrhName='/228007029589 |RAY YEE TRADING CO LTD |NO152 XINXING 1ST ST,GUISHAN |DIST TAOYUAN CITY,TAIWAN'
		 --SELECT @sMatchName=' - YEE'
		 --SELECT @sSrhName='/114-11 004487-3 |CHIN PECH CO., LTD. |3F,NO.272-1,XINSHU RD,XINZHUANG DIS |NEW TAIPEI CITY 242 |VN TAIPEI(R.O.C)XX'
		 --SELECT @sMatchName='XINJIANG DAQO NEW ENERGY, CO. LTD'
		 --SELECT @sMatchName='XINJIANG DAQO NEW ENERGY CO., LTD.'
		 --SELECT @sMatchName='XINJIANG DAQO NEW ENERGY CO'
		 --SELECT @sSrhName='PLS REMIT THE A/M CHARGES TO THE |DESIGNATED ACCOUNT WITH INSTITUTION |BY THREE WORKING DAYS UNDER OUR REF |NO.WE RESRVE OUR RIGHT TO CLAIM FOR ANY POSSIBLE DELAY INTEREST |HENCE DERIVED'
		 --SELECT @sMatchName='DJUNADI,ACHMAD'
		 --SELECT @sSrhName='/2012123 |1/LIMITED LIABILITY COMPANY KVT-RUS |2/AEROSPACE COMMUNICATIONS HOLDINGS COMPANY LIMITED |3/AEROSPACE COMMUNICATIONS HOLDINGS GROUP COMPANY LIMITED'
		 --SELECT @sMatchName='LIMITED LIABILITY COMPANY GEFEST-M'
	     --SELECT @sSrhName='/000111939909 |you could provide to us your financial institutions |The laws of the Republic of Singapore. A person who is |STIRLING ROAD'
		 --SELECT @sMatchName='THE REPUBLIC OF THE UNION OF MYANMAR FOREST PRODUCTS JOINT VENTURE CORPORATION LTD.'
	     --SELECT @sSrhName='ANHUI GREATWALL MILITARY INDUSTRY COMPANY LIMITED |1/140 REPAIR PLANT OPEN JOINT STOCK COMPANY |2/AVIC AVIATION HIGH-TECHNOLOGY COMPANY LIMITED |3/AVIC HEAVY MACHINERY COMPANY LIMITED'
		 --SELECT @sSrhName='AVIC SHENYANG AIRCRAFT COMPANY LIMITED |1/DELVEST HOLDING COMPANY |2/ETCO INTERNATIONAL COMPANY |3/NORDSTRAND MARITIME AND TRADING COMPANY'
		 --SELECT @sSrhName='LIMITED LIABILITY COMPANY ALKON |1/CANCRI GEMS AND JEWELLERY COMPANY LIMITED |2/LIMITED LIABILITY COMPANY CONCORD MANAGEMENT AND CONSULTING |3/UNIJET COMPANY LIMITED'
		 --SELECT @sSrhName='ZHE JIANG JIAYI SMALL COMMODITIES TRADE COMPANY LIMITED |1/KT SERVICES AND LOGISTICS KTSL COMPANY LIMITED |2/MYANMA ECONOMIC HOLDINGS PUBLIC COMPANY LIMITED |3/MYANMAR IMPERIAL JADE GEMS & JEWELLERY COMPANY LIMITED'
		 --SELECT @sSrhName='AZZA AIR TRANSPORT COMPANY LTD |1/AZZA AVIATION COMPANY |2/CHONGCHONGANG SHIPPING COMPANY LIMITED |3/CHO''''NGSONG UNITED TRADING COMPANY |4/DAESONG TRADING COMPANY'
		 --SELECT @sMatchName='LIMITED LIABILITY COMPANY INFORMATION AGENCY ONLY NEWS'
		 --SELECT @sMatchName='LIMITED LIABILITY COMPANY KONSTRUCTION COMPANY KONSOL STROI LTD'
		 --SELECT @sMatchName='LIMITED LIABILITY COMPANY MARKETING ASSOCIATION TVERNEFTEPRODUCT'
		 --SELECT @sSrhName='LIMITED LIABILITY COMPANY ALKON |1/CANCRI GEMS AND JEWELLERY COMPANY LIMITED |2/LIMITED LIABILITY COMPANY CONCORD MANAGEMENT AND CONSULTING |3/UNIJET COMPANY LIMITED'
		 --SELECT @sMatchName='LIMITED LIABILITY COMPANY OMZ-SPETSSTAL'
		 --SELECT @sSrhName='/000111939909 |you could provide to us your financial institutions |the laws of the Republic of Singapore. A person who is |STIRLING ROAD'
		 --SELECT @sMatchName='THE REPUBLIC OF TURKEY'
		 --SELECT @SMATCHNAME='THE REPUBLIC OF THE SUDAN'
		 --SELECT @sMatchName='THE REPUBLIC OF ARMENIA'
		 --SELECT @sMatchName='THE REPUBLIC OF THE PHILIPPINES'
		 --SELECT @sMatchName='THE REPUBLIC OF ZIMBABWE'
         --SELECT @sSrhName='ATTN: REMITTANCE DEPT. |. |RE YOUR MT103 FOR CNY234,870. | REF.20114io00970 VALUE DD 01 MAR 22 |B/O: MR CHANG SHENG-YOU | FOR: CHANG SHENG HSUAN'
		 --SELECT @sMatchName='M.A.R.'
		 --SELECT @sSrhName='BELTECHEXPORT COMPANY |1/BELTECHEXPORT COMPANY CJSC |2/A & M MAHAR COMPANY LIMITED |3/A AND M MAHAR COMPANY LIMITED'
		 --SELECT @sMatchName='A&M MAHAR CO.,LTD.'	
		 --SELECT @sSrhName='BKTWTWTP270 |ORIENTAL IRON COMPANY SPRL 18 Avenue de la paix'
		 --SELECT @sMatchName='DE LI'
		 --SELECT @sSrhName='Donghung-dong |CHO''NGSONG UNITED TRADING COMPANY |c/o Reconnaissance General Bureau Headquarters'
		 --SELECT @sMatchName='DONG DI'
		 --SELECT @sSrhName='Shahid Atefi Street'
		 --SELECT @sMatchName='AL SHAHID'
		 --SELECT @sSrhName='PYONGJIN SHIP MANAGEMENT COMPANY LIMITED |Ryukkyo 1-dong, Pyongchon-guyok |SAENGP IL COMPANY c/o Reconnaissance General Bureau Headquarters'
		 --SELECT @sMatchName='E,DONG'
		 --SELECT @sSrhName='/ACC/0211L4B74B3C002785 |/INS/FED BEN ZIONS BANCORPORATION |NA SALT LAKE CITY, UTAH'
		 --SELECT @sMatchName='LI NA DI'
		 --SELECT @sSrhName='/114110043879 |HONGYI WIPER CO.,LTD |AREA 9 AND 11,MIN YING INDUSTRIA CH |A SHAN TOWN,DONG GUANG CITY |GUANG DONG,CN'
		 --SELECT @SMATCHNAME='LI,LI MIN'
		 --select @sSrhName='/114110043107 |nu tek manufacturing limited |TEL:13809616967 |fu min road,sha jiao village of hu | MEN TOWN, DONGGUAN CITY,GUANGDONE CN'
		 --select @sMatchName='''TEK'''
		 --SELECT @sSrhName='/114110043879 |HONGYI WIPER CO.,LTD |AREA 9 AND 11,MIN YING INDUSTRIA CH |A SHAN TOWN,DONG GUANG CITY |GUANG DONG,CN'
		 --SELECT @sMatchName='LI,LI MIN'
		 --SELECT @sSrhName='Mirdamad Avenue |HORMOZ UREA FERTILIZER COMPANY |IMAM KHOMEINI SHAZAND OIL REFINING COMPANY 20th Km of Borujerd Road |IRAN ADVANCED TECHNOLOGIES COMPANY |IRAN AIRCRAFT MANUFACTURING COMPANY P.O. Box 83145-311'
		 --SELECT @SMATCHNAME='IRÁN AIRCRAFT MANUFACTURING INDUSTRIES'
		 --SELECT @sSrhName='/114-11-004487-3 |CHIN PECH CO., LTD. |3F,NO.272-1,XINSHU RD,XINZHUANG DIS |NEW TAIPEI CITY 242 |VN TAIPEI (R.O.C.)XX'
		 --SELECT @sMatchName='LI,CHIN-E'
		 --SELECT @sSrhName='PLS REMIT THE A/M CHARGES TO THE |DESIGNATED ACCOUNT WITH INSTITUTION |BY THREE WORKING DAYS UNDER OUR REF |NO. WE RESERVE OUR RIGHT TO CLAIM |FOR ANY POSSIBLE DELAY INTEREST |HENCE DERIVED'
		 --SELECT @SMATCHNAME='THE LIMITED'
		 --SELECT @sSrhName='ATTN: REMITTANCE DEPT. |. |RE YOUR MT103 FOR CNY234,870. |REF.20114IO00970 VALUE DD 01 MAR 22 |B/O:MR CHANG SHENG-YOUR |F/O:CHANG SHENG HSUAN |. |PLEASE BE ADVISED THAT WE ARE UNABLE TO APPLY |SUBJECT PAYMENT IN REASON OF INVALID A/C. |KINDLY PROVIDE CORRECT PAYMENT DETAILS BY RETURN |AUTHENTICATED SWIFT MSG QUTOING OUR REF. |I082220301/25001 AS SOON AS POSSIBLE. |.'
		 --SELECT @sMatchName='LI,CHANG LI'
		 --SELECT @sMatchName='LI LI CHANG'
		 --SELECT @sSrhName='/T148192281 |TSAI KAI AN |NO.11-2,ALY.31,LN. 51,XINSHENG ST |TAMSUI DIST. NEW TAIPEI CITY TW'
		 --SELECT @sMatchName='LI KAI'
		 --SELECT @sSrhName='ATTN: REMITTANCE DEPT. |. |RE YOUR MT103 FOR CNY234,870. |REF.20114IO00970 VALUE DD 01 MAR 22 |B/O:MR CHANG SHENG-YOUR |F/O:CHANG SHENG HSUAN |. |PLEASE BE ADVISED THAT WE ARE UNABLE TO APPLY |SUBJECT PAYMENT IN REASON OF INVALID A/C. |KINDLY PROVIDE CORRECT PAYMENT DETAILS BY RETURN |AUTHENTICATED SWIFT MSG QUTOING OUR REF. |I082220301/25001 AS SOON AS POSSIBLE. |.'
		 --SELECT @sMatchName='B SÁ'
		 --SELECT @sSrhName='/GOUR/ |/INS/F043000096 PNC BANK NATIONAL A |SSOCIATION |/INS/D000167720 PNC BANK, NATIONAL |ASSOCIATION TWO PNC PLAZA 620 LIB |ERTY AVE PITTS'
		 --SELECT @sMatchName='DELLA PITTS'
		 --SELECT @sSrhName='ATTN: REMITTANCE DEPT. |. |RE YOUR MT103 FOR CNY234,870. |REF.20114IO00970 VALUE DD 01 MAR 22 |B/O:MR CHANG SHENG-YOUR |F/O:CHANG SHENG HSUAN |. |PLEASE BE ADVISED THAT WE ARE UNABLE TO APPLY |SUBJECT PAYMENT IN REASON OF INVALID A/C. |KINDLY PROVIDE CORRECT PAYMENT DETAILS BY RETURN |AUTHENTICATED SWIFT MSG QUTOING OUR REF. |I082220301/25001 AS SOON AS POSSIBLE. |.'
		 --SELECT @sMatchName='BIN SHENG LI'
		 --SELECT @sMatchName='LI,BIN SHENG'
		 --SELECT @sMatchName='LI,CHANG LI'
         --SELECT @sSrhName='/201114110070626 |1/MIMA INTERNATIONAL,LTD |2/EMT.SQ FIVE 38 WANG CHIU |2/ ROAD,TOW.1 |3/HK/HONG KONG-KOWLOON'
		 --SELECT @sMatchName='D.E.S. INTERNATIONAL'
		 --SELECT @sSrhName='/114-11-004487-3 |CHIN PECH CO., LTD. |3F,NO.272-1,XINSHU RD,XINZHUANG DIS |NEW TAIPEI CITY 242 |VN TAIPEI (R.O.C.)XX'
		 --SELECT @sMatchName='LI,CHIN-E'
		 --SELECT @sSrhName='/201114110070626 |1/MIMA INTERNATIONAL,LTD |2/EMT.SQ FIVE 38 WANG CHIU |2/ ROAD,TOW.1 |3/HK/HONG KONG-KOWLOON'
		 --SELECT @sMatchName='LI WANG'
		 --SELECT @sSrhName='/114110043107 |nu tek manufacturing limited |TEL:13809616967 |fu min road,sha jiao village of hu |MEN TOWN, DONGGUAN CITY,GUANGDONE CN'
		 --SELECT @SMATCHNAME='LI,FU LI'
		 --SELECT @sSrhName='/0003-006 PETROCHINA INTERNATIONAL |(SINGAPORE) PTE LTD |HONG KONG |HONGYI FUTURES INVESTMENT CO LIMITED |SH |HONG PANG GEMS & JEWELLERY (HK) CO LIMITED |ZA'
		 --SELECT @sMatchName='KING HUNG INVESTMENT CO., LTD.'
		 --SELECT @sSrhName='/201114110070626 |1/MIMA INTERNATIONAL LIMITED |2/ROOM 3602 LEVEL 36 TOWER 1 KOWLOO |3/HK/HONG KONG'
		 --SELECT @sMatchName='ARI INTER-NATIONAL LTD'
		 --SELECT @sSrhName='DE MARTINI TAMAYO, Sergio Rene |DESARROLLO GEMMA CORPORATION'
		 --SELECT @sMatchName='DIAZ,MANUEL'
		 --SELECT @sSrhName='ATTN: REMITTANCE DEPT. |. |RE YOUR MT103 FOR CNY234,870. | REF.20114io00970 VALUE DD 01 MAR 22 |B/O: MR CHANG SHENG-YOU | FOR: CHANG SHENG HSUAN'
	     --SELECT @sMatchName='M.A.R.'
		 --SELECT @sSrhName='ATTN: REMITTANCE DEPT. |. |RE YOUR MT103 FOR CNY234,870. |REF.20114IO00970 VALUE DD 01 MAR 22 |B/O:MR CHANG SHENG-YOUR |F/O:CHANG SHENG HSUAN |. |PLEASE BE ADVISED THAT WE ARE UNABLE TO APPLY |SUBJECT PAYMENT IN REASON OF INVALID A/C. |KINDLY PROVIDE CORRECT PAYMENT DETAILS BY RETURN |AUTHENTICATED SWIFT MSG QUTOING OUR REF. |I082220301/25001 AS SOON AS POSSIBLE. |.'
		 --SELECT @sMatchName='DI,CHANG SHENG'
		 --SELECT @sSrhName='/114-11-004487-3 |CHONGCHONGANG SHIPPING COMPANY LIMITED |817, Haeun'
		 --SELECT @sMatchName='CIA'
         --SELECT @sSrhName='ATTN: REMITTANCE DEPT. |. |RE YOUR MT103 FOR CNY234,870. |REF.20114IO00970 VALUE DD 01 MAR 22 |B/O:MR CHANG SHENG-YOUR |F/O:CHANG SHENG HSUAN |. |PLEASE BE ADVISED THAT WE ARE UNABLE TO APPLY |SUBJECT PAYMENT IN REASON OF INVALID A/C. |KINDLY PROVIDE CORRECT PAYMENT DETAILS BY RETURN |AUTHENTICATED SWIFT MSG QUTOING OUR REF. |I082220301/25001 AS SOON AS POSSIBLE. |.'
		 --SELECT @sMatchName='CHANG LI'
		 --SELECT @sSrhName='/114110072959 |U-LI (VN) LIMITED LIABILITY COMPANY |LOT CN3, SONG TR IZ, TAN BINH WAR |D, THAI BINH PROVINCE |THAI BINH,VIET NAM'
		 --SELECT @sMatchName='LI SONG LI'
		 --SELECT @sMatchName='BIN SONG'
		 --SELECT @sSrhName='/114110072959 |U-LI (VN) LIMITED LIABILITY COMPANY |LOT CN3, SONG TR IZ, TAN BINH WAR |D, THAI BINH PROVINCE |THAI BINH,VIET NAM'
		 --SELECT @sMatchName='ZA,U TE'
	     --SELECT @sSrhName='/3300605168 |D. LIGHT DESIGN LTD |TOWER A |CYBER CITY |EBENE 72201'
		 --SELECT @sMatchName='LIGHT DESIGN INC'
		 --SELECT @sMatchName='D.LIGHT DESIGN INC'
		 --SELECT @sSrhName='/20102638179 |Vasiliy Aleksandrovich NIKITIN'
		 --SELECT @sMatchName='VASILIY ALEKSANDROVICH NIKITIN'



        SELECT @iMatchFlag=0  --2022-03-31 add
		select @ssSearchName=@sSrhName, @sMatchName=@sMatchName, @sNewLine=char(13) + char(10)  --2021-12-07 add
        --2022-03-22 ADD FIX, SPECIAL CHAR
		SELECT @sMatchName=REPLACE(UPPER(@sMatchName),'''','') + ' '       --2022-02-09 change fix 'TEK' old: UPPER(@sMatchName) + ' '
		SELECT @sMatchName=REPLACE(REPLACE(@sMatchName,',',' '),'  ',' ')  --2022-03-22 change fix 'ZA,U TE'
		--SELECT @sMatchName=REPLACE(REPLACE(@sMatchName,'-',''),'  ',' ')  
		
		if isnull(@sSrhName,'')='' or ISNULL(@sMatchName,'')=''
		   goto EndEndEnd
		   				
		--2021-11-11 if match matched text return,start
		if (CHARINDEX(LTRIM(RTRIM(@sMatchName)), @ssrhname)>0 )
		begin
		    --2022.01.11 bug fix: not using @sMatchName, should be get @ssSearchName
		    --SELECT @sStrTmp1=@sMatchName   --2022.01.11 mask
		    SELECT @sStrTmp1=SUBSTRING(@ssSearchName,CHARINDEX(LTRIM(RTRIM(@sMatchName)), @ssrhname), Len(@sMatchName))  --2022.1.11 change
		   goto RetEnd  --EndProc
		end
		--2021-11-11 if match matched text return,end
	
	
        --SELECT @sSrhName=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@sSrhName,',',' '),'-', ' '),'''',''),'.',' '),'/',' '),')',' '),'(' ,' '),'&',' '),'   ',' '),'  ',' ')
        SELECT @sSrhName=UPPER(replace(@sSrhName,'-',' ')) + ' '
		--SELECT @sMatchName=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@sMatchName,',',' '),'-', ' '),'''',''),'.',' '),'/',' '),')',' '),'(' ,' '),'&',' '),'   ',' '),'  ',' ')
		select @sMatchName=REPLACE(UPPER(@sMatchName),'''','') + ' '   --2022-02-09 change fix 'TEK' old: UPPER(@sMatchName) + ' '

		--2022-02-09 add for header is sepcial '- ' or '& ',Start
		if SUBSTRING(@sMatchName,1,2)='- ' or SUBSTRING(@sMatchName,1,2)='& '
		   SELECT @sMatchName=REPLACE(REPLACE(@sMatchName,'- ',''),'& ','')
        --2022-02-09 add for header is sepcial '- ' or '& ', End

		--2021-12-07 add for .ZA matchname, but searchname is ZA  not include '.' , Start
		--process: @sMatchName
		if  ( PATINDEX('%[.]%',@sSrhName) = 0 and PATINDEX('%[.]%',@sMatchName) > 0 )
		begin
		   if CHARINDEX('. ',@sMatchName) > 0 and CHARINDEX('.',@sMatchName) > 0
		      SELECT @sMatchName=REPLACE(@sMatchName,'.','')
           else
		      SELECT @sMatchName=REPLACE(@sMatchName,'.',' ')
		end
		--process: @sSrhName
		if  ( PATINDEX('%[.]%',@sSrhName) > 0 and PATINDEX('%[.]%',@sMatchName) = 0 )
		begin
		   if CHARINDEX('. ',@sSrhName) > 0 and CHARINDEX('.',@sSrhName) > 0
		      SELECT @sSrhName=REPLACE(@sSrhName,'.','')
           else
		      SELECT @sSrhName=REPLACE(@sSrhName,'.',' ')
		end
		--2021-12-07 add for .ZA matchname, but searchname is ZA not include '.' , end

		--2021-12-07 add for SH! matchname, but searchname is SH not include '!' , Start
		--process: @sMatchName
		if  ( PATINDEX('%[!]%',@sSrhName) = 0 and PATINDEX('%[!]%',@sMatchName) > 0 )
		begin
		   if CHARINDEX('! ',@sMatchName) > 0 and CHARINDEX('!',@sMatchName) > 0
		      SELECT @sMatchName=REPLACE(@sMatchName,'!','')
           else
		      SELECT @sMatchName=REPLACE(@sMatchName,'!',' ')
		end
		--process: @sSrhName
		if  ( PATINDEX('%[!]%',@sSrhName) > 0 and PATINDEX('%[!]%',@sMatchName) = 0 )
		begin
		   if CHARINDEX('! ',@sSrhName) > 0 and CHARINDEX('!',@sSrhName) > 0
		      SELECT @sSrhName=REPLACE(@sSrhName,'!','')
           else
		      SELECT @sSrhName=REPLACE(@sSrhName,'!',' ')
		end
		--2021-12-07 add for SH! matchname, but searchname is SH not include '!' , end	
		
		--2021-12-07 add for SearchName ',' replace ', ' , Start
		if  ( PATINDEX('%[,]%',@sSrhName) > 0 and CHARINDEX(', ',@sSrhName) = 0 and CHARINDEX(',',@sSrhName) > 0 )
		    SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,',',', '),'  ', ' ')

        if  ( PATINDEX('%[,]%',@sMatchName) > 0 and CHARINDEX(', ',@sMatchName) = 0 and CHARINDEX(',',@sMatchName) > 0 )
		      SELECT @sMatchName=REPLACE(REPLACE(@sMatchName,',',' '),'  ', ' ')	
		--2021-12-07 add for SearchName ',' replace ', ' , end	

		--2021-12-07 add for SearchName '-' replace ' ' , Start
		if  ( PATINDEX('%[-]%',@sSrhName) > 0 and CHARINDEX('- ',@sSrhName) = 0 and CHARINDEX('-',@sSrhName) > 0 )
		    SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,'-',' '),'  ', ' ')

        if  ( PATINDEX('%[-]%',@sMatchName) > 0 and CHARINDEX('- ',@sMatchName) = 0 and CHARINDEX('-',@sMatchName) > 0 )
		      SELECT @sMatchName=REPLACE(REPLACE(@sMatchName,'-',' '),'  ', ' ')	
		--2021-12-07 add for SearchName '-' replace ' ' , end		
		--2021-12-09 add for HUA_FU matchname, include '_' , Start
		--process: @sMatchName
		if ( PATINDEX('%[_+&]%',@sMatchName) > 0  or  PATINDEX('%[_+&|]%',@sSrhName) > 0  )
		begin
	        if CHARINDEX('_',@sMatchName) > 0 
		       SELECT @sMatchName=REPLACE(REPLACE(@sMatchName,'_',' '),'  ',' ')
       	    if CHARINDEX('_',@sSrhName) > 0 
		      SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,'_',' '),'  ',' ')

	        if CHARINDEX('+',@sMatchName) > 0 
		       SELECT @sMatchName=REPLACE(REPLACE(@sMatchName,'+',' '),'  ',' ')
       	    if CHARINDEX('+',@sSrhName) > 0 
		      SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,'+',' '),'  ',' ')

		    if CHARINDEX('&',@sMatchName) > 0 
		       SELECT @sMatchName=REPLACE(REPLACE(@sMatchName,'&',' '),'  ',' ')
       	    if CHARINDEX('&',@sSrhName) > 0 
		      SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,'&',' '),'  ',' ')
	    end
		--2021-12-09 add for HUA_FU matchname, include '_' ,End

					
		--2021-12-09 add for Char(13) + CHAR(10) AML INTERFERCE using '|' REPLACE, Start
		if  ( PATINDEX('%[|]%',@sSrhName) > 0  or CHARINDEX('|',@sSrhName) > 0 )
 		      SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,'|',' '), '  ',' ')

		if  ( CHARINDEX(@sNewLine,@sSrhName) > 0  )
	      SELECT @sSrhName=REPLACE(REPLACE(@sSrhName,@sNewLine,' '),'  ',' ')	
		--2021-12-09 add for Char(13) + CHAR(10) AML INTERFERCE using '|' REPLACE, End
		
		--2021-12-10 add more replace 2 bytes space
		SELECT @sMatchName=REPLACE(@sMatchName,'  ',' ')
					
		--0. compare Search name , Match Name 
		select @sStrTmp='', @sStrTmp1=''		
		 
		--2021-09-29 bug fix, start
		--by match name, from the first word, the second word, the third word,...
		select @sStrTmp=@sMatchName, @iPos=0


		--2021-11-11 if match matched text return,start

	   if  (CHARINDEX(LTRIM(RTRIM(@sMatchName)), @ssSearchName)= 0 )
	   begin

	               SELECT @iProcess=1
	               goto GetMatchedText

GetProcess1:
			   
				  --select @sStrTmp1=REPLACE(@sMatchName,'|',@sNewLine)

			--2021-12-15 add for firstWord not Match,Start
			--SELECT '!!!GetProcess1:' Mark, @sStr FirstWord, CHARINDEX(@sstr, @ssSearchName) C1, @sStr1 LastWord,CHARINDEX(@sStr1, @ssSearchName) C2, @ssSearchName SearchText, @sMatchName as PartyName

			if CHARINDEX(@sstr, @ssSearchName)=0 or CHARINDEX(@sstr1, @ssSearchName)= 0
			   goto Process1
			--2021-12-15 add for firstWord not Match,End

		   --select @sStrTmp1=@sMatchName
		   --2021-12-10 add for return null 
		   if len(isnull(@sStrTmp1,'')) = 0
				select @sStrTmp1 = REPLACE(@ssMatchName, '|',@sNewLine)

		   goto endProc

		end
		--2021-11-11 if match matched text return,end



		--2021-12-13 add check special character '-' , Start
		if PATINDEX('%[-]%',@ssSearchName) > 0 and PATINDEX('%[-]%',@ssMatchName) > 0
		begin
		   --SELECT  '!!!0' , PATINDEX('%[-]%',@ssSearchName) posS, PATINDEX('%[-]%',@ssMatchName) PosP
		   
           if  CHARINDEX(' -',@ssSearchName) = 0 and CHARINDEX(' -',@ssMatchName) > 0
		       --SELECT  '!!!1' ,  CHARINDEX(' -',@ssSearchName) posS,  CHARINDEX(' -',@ssMatchName) PosP
			   SELECT @sMatchName=REPLACE(@smatchname,' ','') + ' '
		end

		--select CHARINDEX(@sMatchName,@ssSearchName) as pos, @sMatchName as party
		if  CHARINDEX(RTRIM(@sMatchName),@ssSearchName) > 0
		begin 
		    --2022.01.11 bug fix: not using @sMatchName, should be get @ssSearchName
		    --SELECT @sStrTmp1=@sMatchName   --2022.01.11 mask
		    SELECT @sStrTmp1=SUBSTRING(@ssSearchName,CHARINDEX(RTRIM(@sMatchName),@ssSearchName), Len(@sMatchName))  --2022.1.11 change
			goto EndProc
        end
		--2021-12-13 add check special character '-' , end		 
		
		--2021-12-13 add check special character ',' , Start
		if PATINDEX('%[,]%',@ssSearchName) > 0 and PATINDEX('%[,]%',@ssMatchName) > 0
		begin
		   --SELECT  '!!!0' , PATINDEX('%[,]%',@ssSearchName) posS, PATINDEX('%[,]%',@ssMatchName) PosP
		   
           if  CHARINDEX(', ',@ssSearchName) = 0 and CHARINDEX(', ',@ssMatchName) > 0
		       --SELECT  '!!!1' ,  CHARINDEX(' -',@ssSearchName) posS,  CHARINDEX(' -',@ssMatchName) PosP
			   SELECT @sMatchName=REPLACE(@smatchname,', ',',') 
		end

		--select CHARINDEX(@sMatchName,@ssSearchName) as pos, @sMatchName as party
		if  CHARINDEX(RTRIM(@sMatchName),@ssSearchName) > 0
		begin 
		    --2022.01.11 bug fix: not using @sMatchName, should be get @ssSearchName
		    --SELECT @sStrTmp1=@sMatchName   --2022.01.11 mask
		    SELECT @sStrTmp1=SUBSTRING(@ssSearchName,CHARINDEX(RTRIM(@sMatchName),@ssSearchName), Len(@sMatchName))  --2022.1.11 change
			goto EndProc
        end
		--2021-12-13 add check special character ',' , end	

	--2021-12-10 add for partial match, Start
	if @sStrTmp1 <> @sMatchName
    begin
		DECLARE @iLenC1 int, @iLenC2 int, @iLenC3 int, @iCntSp1 int, @iCntSp2 int
		DECLARE @sPartyNa varchar(8000), @sPartyNa1 varchar(8000), @sPartyNa2 varchar(8000)
				   
		SELECT @sStrTmp=@ssSearchName
	    SELECT @iCntSp1=0, @iCntSp2=0
		select @iLenC1=1, @iLenC2=1, @ilenC3=1, @sStr=@sStrTmp, @sStr1=@sStr

		--count space for sMatchName , Start
		While(@iLenC2 > 0 )
		begin
		   select @iLenC2=CHARINDEX(' ',@sMatchName, @ilenC2)
		   if @iLenC2 > 0
              SELECT @iCntSp1=@iCntSp1+1 ,@iLenC2=@iLenC2+1
           --select @iLenC2 as iC2,  @iCntSp1 CNT
		end
		--count space for sMatchName , End
		
		--Do TransactionText , Start
		SELECT @sStr1=SUBSTRING(@sStr,PATINDEX('%[ ]%',@sStr)+1,len(@sStr)-PATINDEX('%[ ]%',@sStr))
		SELECT @sStr=@sStr1
		SELECT @sPartyNa='', @sPartyNa1='', @sPartyNa2=''
		--SELECT '!!' mark, @sStr as SearchText

		SELECT @iLenC2=1
		While( @iLenC2> 0 )
		begin
		    IF SUBSTRING(@sStr, @iLenC2, 1) = ' ' or SUBSTRING(@sStr, @iLenC2, 1) = '-' or SUBSTRING(@sStr, @iLenC2, 1) = ','
			begin
		        IF @iLenC2 > 1    --2022-03-09 ADD
                   SELECT @sPartyNa1=SUBSTRING(@sStr, 1, @iLenC2)
                ELSE              --2022-03-09 ADD
				   SELECT @sPartyNa1=''     --2022-03-09 ADD
			    SELECT @sPartyNa= @sPartyNa1
			    SELECT @iCntSp2=@iCntSp2+1
				--SELECT '!!' mark,@sPartyNa as SearchText, @iCntSp2 as cnt, @iLenC2 pos, SUBSTRING(@sStr, 1, @ilenC2) party
			end

			--SELECT @iLenC2=@iLenC2+1
			if @iCntSp1=@iCntSp2
			   SELECT @iLenC2=0
            else
			   SELECT @ilenC2=@iLenC2+1
            
			--2022-01-17 add for loop timeout, start
			if @iLenC2 > len(@sMatchName)
			   SELECT @iLenC2=0

			--SELECT '!!' mark,@sPartyNa as SearchText, @iCntSp2 as cnt, @iLenC2 pos, SUBSTRING(@sStr, 1, Len(@sStr)-@ilenC2) party
		end

		SELECT @sStrTmp1=REPLACE(@sPartyNa,'|',@sNewLine)
		--Do TransactionText , End
					   
		
		if DIFFERENCE(@sPartyNa,@sMatchName) <> 0
		begin
				--select @sStrTmp1 as SearchText, @sMatchName as PartyName
				SELECT @iLenC1=0
				goto EndProc
         end

    end
	--2021-12-10 add for partial match, End

Process1:

		while (@iPos < len(@sStrTmp) )
		begin
		  IF ( (CHARINDEX(' ',@sStrTmp) > 0 AND  PATINDEX('%[,]%',@sStrTmp) = 0 ) or  
		       (PATINDEX('%[,]%',@sStrTmp) > 0 and CHARINDEX(' ',@sStrTmp) <  PATINDEX('%[,]%',@sStrTmp) ) 
			 ) 
		  BEGIN
		     IF CHARINDEX(' ',@sStrTmp) - 1 > 1     --2022-03-09 ADD
		        select @sStrTmp1=SUBSTRING(@sStrTmp,@iPos+1,CHARINDEX(' ',@sStrTmp)-1) 
             ELSE                                   --2022-03-09 ADD
			    select @sStrTmp1=' '                --2022-03-09 ADD
			 select @sStrTmp2='%' + @sStrTmp1 +'%'
		     select @iDeff=PATINDEX(@sStrTmp2,@sSrhName)
			
			 if (@iDeff > 0)
		       select @iPos=Len(@sStrTmp)+1
             else
		       select @iPos=CHARINDEX(' ',@sStrTmp,@iPos+len(@sStrTmp1)-2)  
		  END

		 ELSE
		  IF ( CHARINDEX(' ',@sStrTmp) >  PATINDEX('%[,]%',@sStrTmp)  )
		  BEGIN
		     IF CHARINDEX(' ',@sStrTmp)-1 > 1   --2022-03-09 ADD
		        select @sStrTmp1=SUBSTRING(@sStrTmp,@iPos+1,CHARINDEX(' ',@sStrTmp)-1) 
			 ELSE                                --2022-03-09 ADD
			    SELECT @sStrTmp1= ' '             --2022-03-09 ADD
			 select @sStrTmp2='%' + @sStrTmp1 +'%'
		     select @iDeff=PATINDEX(@sStrTmp2,@sSrhName)
			
			 if (@iDeff > 0)
		       select @iPos=Len(@sStrTmp)+1
             else
		       select @iPos=CHARINDEX(' ',@sStrTmp,@iPos+len(@sStrTmp1)-2)  
		  END
		  
		  IF ( @iDeff=0  and PATINDEX('%[,]%',@sStrTmp)  > 0 )
			 BEGIN
			    IF PATINDEX('%[,]%',@sStrTmp) > @iPos+1   --2022-03-09 ADD
			       select @sStrTmp1=SUBSTRING(@sStrTmp,@iPos+1,PATINDEX('%[,]%',@sStrTmp))  --2021-09-29 BUG FIX
			    ELSE									--2022-03-09 ADD
				   SELECT @sStrTmp1= ' '                --2022-03-09 ADD
			    select @sStrTmp2='%' + @sStrTmp1 +'%'
		        select @iDeff=PATINDEX(@sStrTmp2,@sSrhName)			
				
			    --SELECT @sstr as TMP,@SSTRTMP1 AS TMP1, @sStrTmp2 AS TMP2, @sSrhname as SearchText ,@iDeff AS DIFF

			    if (@iDeff > 0)
		           select @iPos=Len(@sStrTmp)+1
                else
				   select @iPos=@iPos+len(@sStrTmp1)
             END

        end
		--2021-09-29 bug fix, end

		-- if partial match
		if ( @iDeff > 0 )
		begin
		   select @icnt=0, @iLen1=1, @iLen2=1, @sStr=@sMatchName, @sStrTmp='', @sStrTmp1=''
		   while( @iLen2 !=0 )
		   begin
			   
			  select @iLen2=CHARINDEX(' ', @sStr,@ilen1)  
			  if ( @ilen2=len(@sstr) )
			  begin
			      IF @iLen2 > @iLen1        --2022-03-09 ADD
				     select @testsrings=substring(@sStr,@ilen1,@ilen2)
                  ELSE                      --2022-03-09 ADD
				     SELECT @testsrings= ''  --2022-03-09 ADD
				  select @sStr=REPLACE(@sStr,@testsrings,'')
				  --select @ilen2 as i2, @testsrings as Tstr,@sStr as MatchName
			  end

			  else	  
			  begin			     
			      select @iLen2=PATINDEX('%[ ,]%', @sStr) 
				  IF @iLen2 > @iLen1    --2022-03-09 ADD
				     select @testsrings=substring(@sStr,@ilen1,@ilen2)
				  ELSE					    --2022-03-09 ADD
				     SELECT @testsrings=''  --2022-03-09 ADD
				  ----2021-11-11 bugfix when the duplicate word in MatchName & Process last word in MatchName , start
				  --select @sStr=ltrim(REPLACE(@sStr,@testsrings,''))  --bug must be mask and change
				  if ( @iLen2 > 0 and len(@sStr) > 0 and @testsrings <> '' ) --last word of matchname
				     select @sStr=SUBSTRING(@sStr,len(@testsrings)+2,len(@sStr)-len(@testsrings)-1)  
				  else 
				      select @testsrings=@sStr
                  ----2021-11-11 bugfix when the duplicate word in MatchName & Process last word in MatchName , end
				  --select @ilen2 as i2, @testsrings as Tstr,@sStr as MatchName
			  end

			  -- 1. Search Singal word in Search name
			  --if @teststring have one special char ,- , and into @sSrhname did not find, then delete space
              if (  PATINDEX('%[,]%', @testsrings) > 0 and PATINDEX('%[ ]%', @testsrings) > 0 and CHARINDEX(@testsrings, @sSrhName)= 0 )
			     select @testsrings=REPLACE(@testsrings,' ','')

			  select @iPos=CHARINDEX(@testsrings, @sSrhName)
			  
			 --select @iPos as Pos, @testsrings as Str, @sSrhName as SearchName

			  if ( @iPos > 0 )
			  begin      
				  select @sStrTmp = RTRIM(@testsrings)
                  
				  if (@sStrTmp1 = '' )   -- first word
					 select @sStrTmp1=RTRIM(@testsrings)

				  else if (@sStrTmp1 <> '' )   -- not first word
				  begin	     
					 --2.  Search two  words in Search name
					 if ( CHARINDEX(@sStrTmp1 + ' ', @sSrhName) > 0 ) --if srhname after @sStrTmp1  have space
					    select @sStr1 =  RTRIM(@sStrTmp1) + ' ' + RTRIM(@testsrings)
                     else --if srhname after @sStrTmp1 no space
					    select @sStr1 =  RTRIM(@sStrTmp1) +  RTRIM(@testsrings)

					 select @iPos = CHARINDEX(@sStr1, @sSrhName)
					 --select @iPos pos, @sStr1 as sStr , @sSrhName as SearchName

					 if @ipos > 0
					 begin
					   if ( CHARINDEX(@sStrTmp1 + ' ', @sSrhName) > 0 ) --if srhname after @sStrTmp1  have space
						 SELECT @sStrTmp1 = RTRIM(@sStrTmp1) + ' ' + RTRIM(@sStrTmp)
                       else --if srhname after @sStrTmp1 no space
					     SELECT @sStrTmp1 = RTRIM(@sStrTmp1)  + RTRIM(@sStrTmp)
                     end

				  end

			  end

			  select @iCnt=@iCnt+1
			  if  @iCnt > 100 or @iLen1 =0
			  begin
			      Select @ilen1=0
				  goto ExitLoop
              end

		   end
		end

ExitLoop:

			--2021-12-15 add bug fix, Start
			if ISNULL(@sStrTmp1,'')<>'' and CHARINDEX(@sStrTmp1, @sMatchName)> 0
				SELECT @sMatchName=@sStrTmp1
				  
			--select '!!!' Mark, @sStrTmp1 MatchText, @iCnt iCnt, @iLen1, @sMatchName PartyName
			--2021-12-15 add bug fix, End

           --select '!!!-ExitLoop:' Mark, @sStrTmp1 MatchText, @iCnt iCnt, @iLen1, @sMatchName PartyName, @ssSearchName SearchName, CHARINDEX(LTRIM(RTRIM(@sMatchName)), @ssSearchName) C1

		   if  ( CHARINDEX(LTRIM(RTRIM(@sMatchName)), @ssSearchName)= 0 or len(@sStrTmp1) = 0)
		   begin
		         
		          SELECT @iProcess=2
				  goto GetMatchedText

GetProcess2:

		   end

		if Len(RTRIM(@sStrTmp1)) = 0 
		   select @sStrTmp1=SUBSTRING(@sSrhName,1,350)

		else if Len(RTRIM(@sStrTmp1)) >= 350
		   select @sStrTmp1=SUBSTRING(@sStrTmp1,1,350)

EndProc:
  
  		--2022-03-16 add fix INOVEST LIMITED, Start
	    IF ABS(LEN(@sStrTmp1)-LEN(@sMatchName))<10 AND CHARINDEX(@sStrTmp1,@sMatchName)=0 And CHARINDEX(@sStr,@sStrTmp1)=0 AND CHARINDEX(@sStr1,@sStrTmp1)=0 AND (SUBSTRING(@sStr,1,1)=SUBSTRING(@sMatchName,1,1) AND CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @sMatchName)>0 )
		Begin
			SELECT @sStrTmp=@sStrTmp1
			IF SUBSTRING(@sStr,1,1)=SUBSTRING(@sStrTmp1,1,1) AND SUBSTRING(@sStr1,1,1)=SUBSTRING(@sStrTmp1,CHARINDEX(' ',@sStrTmp1)+1,1)      
			   GOTO RetEnd 
			ELSE If SUBSTRING(@sStr,1,1)<>SUBSTRING(@sStrTmp1,1,1) AND CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @sStrTmp1)=0
			BEGIN
			   SELECT @sSTrTmp1=''      
			   GOTO GetMidWord    --GetNextWord
			END
	   
	    End
		
		--2022-03-30 fix 'OBSHCHESTVO S OGRANICHENNOY OTVETSTVENNOSTYU SBD', Start
		ELSE IF ABS(LEN(@sStrTmp1)-LEN(@sMAtchName)) >30 AND LEN(@sStrTmp1) < LEN(@sMAtchName)
		BEGIN
		    SELECT @iLenC1=CHARINDEX(@sStrTmp1,@ssSearchName), @iLenC2=CHARINDEX('|',@ssSearchName,@iLenC1+1)
			IF @iLenc2 > @iLenC1
			   SELECT @sStrTmp=SUBSTRING(@ssSearchName,@iLenC1,@iLenC2-@iLenc1), @sStrTmp1=@sStrTmp
		--SELECT '!!!EndProc:-0-1' Mark,@sStrTmp1 rText, @sMatchName partyname, CHARINDEX(@sStrTmp1,@ssMatchName) c, SUBSTRING(@ssSearchName,@iLenC1+1, 1) isx, SUBSTRING(@sMatchName,@iLenC2,1) imx, @iLenc1 ic1, @iLenc2 ic2
		END
	    --2022-03-30 fix 'OBSHCHESTVO S OGRANICHENNOY OTVETSTVENNOSTYU SBD', End

		--SELECT '!!!EndProc:-0-2' Mark,@sStrTmp1 rText, @sMatchName partyname, CHARINDEX(@sStrTmp1,@ssMatchName) c, DIFFERENCE(@sStrTmp1, @sMatchName) idif, @iLenc1 ic1, @iLenc2 ic2, @sStr First, @sStr1 Last, ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs
		
		--2022-04-07 fix 'AL -' matchName correct do not other process, Start
		if DIFFERENCE(@sStrTmp1,@sMatchName)=4 And ISNULL(@sStr,'')='' AND ISNULL(@sStr1,'')='' AND CHARINDEX(@sStrTmp1, @ssSearchName)> 0
		begin
		   if PATINDEX('%[-,.]%',@sMatchName) >0
		     SELECT @sPartyNa1=LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(@sMatchName,'-',''),',',' '),'.',' '),'  ',' ')))
		   if PATINDEX('%[-,.]%',@sStrTmp1) >0
			 SELECT @sPartyNa2=LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(@sStrTmp1,'-',''),',',' '),'.',' '),'  ',' ')))
		   if CHARINDEX(@sPartyNa2, @sPartyNa1) >0
		   	 SELECT @iMatchFlag=1
           if @iMatchFlag=1
			  GOTO RetEnd2		  
		end
		--2022-04-07 fix 'AL -' matchName correct do not other process, End

		--2022-04-07 fix more word matchtext process,Start
		ELSE if DIFFERENCE(@sStrTmp1,@sMatchName)=4 AND ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) <7 AND 
		        CHARINDEX(@sStrTmp1,@sMatchName) =0 AND CHARINDEX(' '+@sStr1,@sStrTmp1)=0
		Begin
		   SELECT @iLenC1=1
		   WHILE(@iLenC1 >0 )
		   begin
			   SELECT @sPartyNa='',@iLenC2=CHARINDEX(' ', @sStrTmp1,@iLenC1+1)
			   if @iLenC2 > 0
				  SELECT  @sPartyNa1=SUBSTRING(@sStrTmp1,1,@iLenC2-1)
			   if CHARINDEX(@sPartyNa1,@sMatchName)>0
			      SELECT @sPartyNa2=@sPartyNa1, @iLenC1=@iLenC2
               ELSE if CHARINDEX(@sPartyNa1,@sMatchName)=0 AND ISNULL(@sPartyNa2,'')<>''
			      SELECT @sStrTmp1=@sPartyNa2, @iMatchFlag=1, @iLenC1=0  
			   ELSE
			      SELECT @iLenC1=0
			   --SELECT '!!!EndProc:-0-2-1' Mark,@sStrTmp1 rText, @iLenc1 ic1, @iLenc2 ic2, @sStr First, @sStr1 Last, @sPartyNa1 s1, @spartyNa2 s2
		   end 
		   if @iMatchFlag=1
		      GOTO RetEnd2 
		End	
		--2022-04-07 fix more word matchtext process,End
				
		IF ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) > 20 AND (CHARINDEX(@sStr,@sStrTmp1)=0 OR CHARINDEX(@sStr1,@sStrTmp1)=0 )
		BEGIN
		   IF CHARINDEX(@sStr,@sStrTmp1)=0 and DIFFERENCE(@sStrTmp1, @sMatchName)=2
		   Begin
		      SELECT @iCntSp1=CHARINDEX(' '+SUBSTRING(@sStr,1,2),@sStrTmp1 )
			  IF @iCntSp1 > 0
				 SELECT @sStrTmp2=SUBSTRING(@sStrTmp1, @iCntSp1, LEN(@sStrTmp1)-@iCntSp1+1), @sStrTmp1=@sStrTmp2
		   End		   
		END
		--Select '!!!EndProc-0x-1:' Mark,@sStrTmp1 rText, @sMatchName partyname, CHARINDEX(@sStrTmp1,@ssMatchName) c, DIFFERENCE(@sStrTmp1, @sMatchName) d, @iLen1 i1, @iLen2 i2, @sStr First, @sStr1 Last
		--2022-03-16 add fix INOVEST LIMITED, End

  	    --2021-12-20 add for Double Check Match Party name, Start		

		--SELECT '!!!-EndProc-0x-2:' Mark,@sStrTmp1 rText, @sMatchName partyname, CHARINDEX(@sStrTmp1,@ssMatchName) c, DIFFERENCE(@sStrTmp1, @sMatchName) d, @iLen1 i1, @iLen2 i2

        if (CHARINDEX(@sStrTmp1,@ssMatchName) = 0  and DIFFERENCE(@sStrTmp1, @sMatchName) <> 4 )  and  ( ABS(LEN(@sStrTmp1)-LEN(@ssMatchName)) > 5 )  and CHARINDEX(@sStr,@sStrTmp1)=0 and CHARINDEX(@sStr1,@sStrTmp1)=0    --2022-03-15 change fix DONG DI
			begin
			    
				if PATINDEX('%[-&,.]%', @sMatchName) > 0 
				BEGIN
					SELECT @iCnt=PATINDEX('%[-&,.]%', @sMatchName)
					IF @iCnt >2     --2022-03-09 ADD
		      		   SELECT @sStr=SUBSTRING(@ssMatchName,1,@icnt-1), @iLen1=CHARINDEX(@sStr,@ssSearchName), @ilen2=CHARINDEX(@sStr1,@ssSearchName, @ilen1+len(@sStr)+1)
                  
				END
				else
				begin
				    if @ilen1=1 AND CHARINDEX('|',@sStrTmp1) > 0   --2022-03-09 ADD
					   SELECT @iLen2=CHARINDEX('|',@sStrTmp1)-1, @sPartyNa1=SUBSTRING(@sStrTmp1,1,@iLen2), @sStrTmp1=@sPartyNa1  --2022-03-09 ADD
					else             --2022-03-09 ADD
					   SELECT @iLen1=CHARINDEX(@sStr,@ssSearchName,@iLen1+len(@sStr)+1), @icnt=@iLen1
					IF @iCnt > 1     --2022-03-09 ADD
					   SELECT  @sStr=SUBSTRING(@ssMatchName,1,@icnt), @ilen2=Len(@ssSearchName)                          
                end

				--SELECT '!!!-EndProc-1-1-1:' Mark,@sStrTmp1 rText, @sMatchName partyname, CHARINDEX(@sStrTmp1,@ssMatchName) c, DIFFERENCE(@sStrTmp1, @sMatchName) d, @iLen1 i1, @iLen2 i2, @sStr First, @sStr1 Last
			    
				SELECT @iLenC1=1, @iLenC2=1, @iLenC3=1, @sStrTmp2='', @iCnt=1, @iCntSp1=1  --2022-03-09 CHANGE
				WHILE(@icnt > 0  and @iLenC3 > 0 and @iCntSp1 < 10 )    --2022-03-09 CHANGE
				BEGIN
				    SELECT @iCntSp1=@iCntSp1+1   --2022-03-09 ADD
				
				    if CHARINDEX(' ', @sMatchName,@iLenC1) > 0
					    SELECT @iLenC1=@iLenC2, @iLenC2=CHARINDEX(' ', @sMatchName,@iLenC1)+1, @iCnt=@iLenC2
                    ELSE
					    SELECT @iLenC1=@iLenC2, @iLenC2=len(@ssMatchName), @iCnt=@iLenC2
                    if @iLenC2 > @iLenC1    --2022-02-14 change fix substring err
			    		SELECT @sPartyNa1=SUBSTRING(@ssMatchName, @iLenC1, @iLenC2-@iLenC1)		
					   		 
				    --SELECT '!!!EndProc-1-1:' Mark, @sPartyNa1 word, @iLenC1 i1, @iLenC2 i2, @sStrTmp1 mText, @iLenC3 c3, @sStrTmp2 s2, @iCnt cnt, @iLen1 i1, DIFFERENCE(@sStrTmp1,@ssMatchName) d
                    
					if CHARINDEX(@sPartyNA1, @ssSearchName) > 0 AND @iLenC1 <> @iLenC2   --2022-03-09 CHANGE
						SELECT @sStrTmp2=@sStrTmp2 + @sPartyNa1, @iLenC3=@iLenC3+1, @iCnt=CHARINDEX(@sPartyNA1, @ssSearchName)
                    
					else  
					Begin

						if @iLenC3 > 0 and ISNULL(@sStrTmp2,'' ) <> '' 
							SELECT @iLenC3=0, @iCnt=0, @iLen1=CHARINDEX(@sStrTmp2, @sMatchName), @iLen2=@iLen1+len(@sStrTmp2)				 
						else
							SELECT @iLenC3=@iLenC3+1
                    end

					 --SELECT '!!!EndProc-1-2:' Mark, @sPartyNa1 word, @sStrTmp1 mText, CHARINDEX(@sPartyNA1, @sStrTmp1) c
					 --Partial match
					 if  CHARINDEX(@sPartyNA1, @sStrTmp1) = 0 
					 begin
					     SELECT @iLenC3=CHARINDEX(' '+SUBSTRING(@sPartyNa1,1,1),@sStrTmp1, @iLenC1+1)
						 if @iLenC3=0
						    SELECT @iLenC3=CHARINDEX('|'+SUBSTRING(@sPartyNa1,1,1),@sStrTmp1, @iLenC1+1)
						--2022-01-20 add for /, start
						 if @iLenC3=0
						    SELECT @iLenC3=CHARINDEX('/'+SUBSTRING(@sPartyNa1,1,1),@sStrTmp1, @iLenC1+1)
						--2022-01-20 add for /, End
						 if @iLenC3=0
						    SELECT @iLenC3=CHARINDEX(SUBSTRING(@sPartyNa1,1,2),@sStrTmp1, @iLenC1+1)

						 if @iLenC3 > 0
						 begin
						    SELECT @ilenC1=CHARINDEX(' ',@sStrTmp1, @iLenC3)
							if @iLenC1 = 0 
							   SELECT @iLenC1=LEN(@sStrTmp1)
                           
						    if @iLenC1 > @iLenC3   --2022-02-14 change fix substring err
						       SELECT @sPartyNa1=SUBSTRING(@sStrTmp1, @iLenC3,@iLenC1-@iLenC3+1)
							
							if LEN(@Sstrtmp1) > @ilenC3   --2022-02-14 change fix substring err
							   SELECT @sPartyNa2=SUBSTRING(@sStrTmp1, @iLenC3,LEN(@Sstrtmp1)-@ilenC3+1), @sStrTmp1=@sPartyNa2, @iCnt=0, @iLenC3=0
                             
							 --SELECT '!!!EndProc-1-3:' Mark, @sPartyNa1 word,  @sPartyNa2 word2,@sStrTmp1 mText, CHARINDEX(@sPartyNA1, @ssSearchName)
                         end
			     		      
                     end
					
					if @iLenC1 = @iLenC2     --2022-03-09 ADD, fix loop
					   SELECT @iCntSp1=11    --2022-03-09 ADD, Fix loop
				   
				    SELECT @iLenC1=@iLenC2+1 

					--SELECT '!!!EndProc-1-4:' Mark, @icnt cnt,@iCntSp1 cnt2,@sStrTmp1 mText, @sPartyNa1 s1, @iLenC3 i3
               
			    END
				
            end      

		--SELECT '!!!EndProc-1-x:' Mark, @sStrTmp1 mText, @sMatchName mName, @iLenC1 ic1, @iLenC2 ic2, @iCnt icnt,SUBSTRING(@ssSearchName,@icnt,20) s1, Len(@ssSearchName) iShtextlen, @sStr1 lastword
	   
	    --2022-04-02 fix 'GUANGDONG PROVINCIAL CHINA TOBACCO INDUSTRY CO LTD'
		if DIFFERENCE(@sStrTmp1, @sMatchName)=4 AND SUBSTRING(@sStr,1,1)=SUBSTRING(@sStrTmp1,1,1) AND
		  ( @iCnt=LEN(@ssSearchName) OR ABS( LEN(@sStrTmp1)- LEN(@sMatchName) ) < 10 OR CHARINDEX(@sStr1,@sStrTmp1) >0 )
	     begin
			  if PATINDEX('%[|]%',@sStrTmp1) =0 
				  GOTO EndEndEnd
			  else if PATINDEX('%[|]%',@sStrTmp1) =0 
				  GOTO RetEnd
		 end
		goto EndEnd


GetMatchedText:
                  
				--space issue that make not match, Start
				if CHARINDEX(@sMatchName, @ssSearchName) = 0
				BEGIN
					SELECT @sPartyNa=REPLACE(@sMatchName,' ','')
					SELECT @sPartyNa1=REPLACE(@sStr+ @sStr1,' ','')
					if  CHARINDEX(@sPartyNa,@ssSearchName) > 0
					BEGIN
						SELECT @sStrTmp1=@sPartyNa
						--SELECT '!!!-GetMatchedText:' Mark,@sStr FirstName, @sStr1 LastName, @sPartyNa P,  @sPartyNa1 P1, @sPartyNa2 P1, @sStrTmp1 MatchText
						goto EndProc
					END
				END
				--space issue that make not match, Start

		           SELECT @sStr='', @sStr1='',@ilen1=1, @ilen2=1

				   --FIRST Word
				   SELECT @iLen1=CHARINDEX(' ',@sMatchName,@iLen1)  --+1
				   SELECT @sStr=SUBSTRING(@sMatchName,1,@iLen1)

				  
				   --Last Word
				   SELECT @iLen2=Len(@sMatchName)-1, @iCnt=1
				   while( @iCnt > 0 )
				   BEGIN
				       if substring(@sMatchName,@iLen2,1)=' '
					   BEGIN
					      if Len(@sMatchName) > @iLen2    --2022-02-14 change fix substring err
					         SELECT @sStr1=SUBSTRING(@sMatchName,@iLen2+1,Len(@sMatchName)-@iLen2)
						  SELECT @iCnt=0
					   END
					   SELECT @iLen2=@iLen2-1, @iDeff=@iDeff+1    --2022-01-17 change
					   
					   --2022-01-17 add for loop timeout
					   if @iDeff > Len(@sMatchName)
					     SELECT @iCnt=0
					   
				   END
                  
				   --SELECT '!!!-GetMatchedText-0:' mark, @sStr as firstWord, @sStr1 as LastWord , len(@sstr) len1, len(@sstr1) len2, @iLen1 P1, @iLen2 P2
				  				   
				   --Fist Word St location#
				   IF   LEN(@sStr) >  1
				      SELECT @iLenC1=CHARINDEX(REPLACE(REPLACE(@sSTR,',',', '),'  ',' '), REPLACE(REPLACE(@ssSearchName,',',', '),'  ',' '),1)   
			  			   
				   IF  LEN(@sStr) = 1  and PATINDEX('%[-@#$(/+"]%',@sStr)=0   --2022-01-20 change
				   BEGIN 
				      SELECT @iLen1=0, @iLen2=0, @sStr=RTRIM(@sStr)  --2022-01-20 change @sStr 
				      SELECT @iCnt=1, @iDeff=1  --2022-01-17
			  
					  --2022-01-20 change bug fix partynane='N-2', start
					  SELECT @iLen1=CHARINDEX(' '+@sStr, @ssSearchName) 
					  if @iLen1=0 
					     SELECT @iLen1=CHARINDEX('/'+@sSTR, @ssSearchName) 
				      if @iLen1=0 
					     SELECT @iLen1=CHARINDEX('|'+@sSTR, @ssSearchName) 
				      if @iLen1=0 
					     SELECT @iLen1=CHARINDEX(@sSTR, @ssSearchName) 
					  --2022-01-20 change bug fix partynane='N-2', End

					  SELECT @sStrTMP=SUBSTRING(@ssSearchName,@ilen1-1, 1) 
					
					  if PATINDEX('%[ |]%',@sStrTmp) = 0
					  BEGIN
						  While ( @iCnt > 0 )
						  BEGIN	     
								 --SELECT '!!3.' mark, @ilen1 pos1, @sStr	firstword
								 --2022-01-17 add for loop timeout control, Start
								 SELECT @iDeff=@iDeff+1
								 if @iDeff > Len(@ssSearchName)
									goto EndEnd
								 --2022-01-17 add for loop timeout control, End
								 
								 SELECT @iLen1=@iLen1+1, @iCnt=@iCnt+1
								 if SUBSTRING(@ssSearchName,@ilen1,len(@sstr))  = @sStr
									SELECT @iCnt=0      
									
								 --2022-01-17 add for loop timeout control, Start     
								 if (CHARINDEX(@sStr, @ssSearchName)=0 or PATINDEX('%[-@#$(/+"]%',@ssSearchName)=0  or @iCnt >= LEN(@ssSearchName) ) -- and PATINDEX('%[-@#$(/+"]%',@sStr)>0
								   SELECT @iCnt=0    
								 --2022-01-17 add for loop timeout control, End
								 									                     
						  END
					  END		  	  
                   END
				   
				--2021-12-17 add Get SearchName Location#
				--Last Word End location#	
                
				SELECT @iLen1=CHARINDEX(LTRIM(RTRIM(@sStr)),@ssSearchName)
				
				--Check first word is not real header word, Start
				if SUBSTRING(@ssSearchName, @ilen1-1,1)<>' ' and SUBSTRING(@ssSearchName, @ilen1-1,1)<>'|' AND SUBSTRING(@ssSearchName, @ilen1-1,1)<>'/' AND SUBSTRING(@ssSearchName, @ilen1-1,1)<>',' AND SUBSTRING(@ssSearchName, @ilen1-1,1)<>'.' 
				BEGIN
				   SELECT @iCntSp1=@iLen1
				   SELECT @iLen1=CHARINDEX(LTRIM(RTRIM(@sStr)),@ssSearchName,@iCntSp1+1) 
				   IF SUBSTRING(@ssSearchName, @ilen1-1,1)<>' ' and SUBSTRING(@ssSearchName, @ilen1-1,1)<>'|' AND SUBSTRING(@ssSearchName, @ilen1-1,1)<>'/' AND SUBSTRING(@ssSearchName, @ilen1-1,1)<>',' AND SUBSTRING(@ssSearchName, @ilen1-1,1)<>'.' 
				      SELECT @iCntSp1=@iLen1, @iLen1=CHARINDEX(LTRIM(RTRIM(@sStr)),@ssSearchName,@iCntSp1+1)
                   else 
				      SELECT @iLen1=CHARINDEX(' ',@ssSearchName,@iCntSp1+1) +1

				   if @iLen1=1 or @iLen1=0
				      SELECT @iLen1=CHARINDEX(' ',@ssSearchName,@iCntSp1+1) +1
				END
				--Check first word is not real header word, End

				SELECT @iLen2=CHARINDEX(LTRIM(RTRIM(@sStr1)),@ssSearchName,@ilen1+1) , @icnt=@iLen2

/*              --2022-02-09 add for last name , first name reverse, Start
				if CHARINDEX(',',@ssMatchName) = 0  and @iLen1 > @iLen2
				   SELECT @iLen2=CHARINDEX(LTRIM(RTRIM(@sStr1)),@ssSearchName,@ilen1+1) , @icnt=@iLen2
				else			  
			       SELECT @iLen2=@iLen1,@iLen1=CHARINDEX(LTRIM(RTRIM(@sStr1)),@ssSearchName) , @icnt=@iLen2
                --2022-02-09 add for last name , first name reverse, End
*/				
/*
				--2021-12-20 add check must new word,Start
				if SUBSTRING(@ssSearchName,@ilen1-1,1) <> ' ' and SUBSTRING(@ssSearchName,@ilen1-1,1) <> '-' and  SUBSTRING(@ssSearchName,@ilen1-1,1) <> ',' and  SUBSTRING(@ssSearchName,@ilen1-1,1) <> '|'
				    SELECT @iLen1=CHARINDEX(@sStr, @ssSearchName,@iLen1+1) 
				--2021-12-20 add check must new word,End				   
*/									
				--SELECT '!!!-GetMatchedText0-a:' mark,@sMatchName PartyName, @ssSearchName SearchText, @sStr first, @sStr1 last,@iLen1 l1, @iLen2 l2

				--Last Word not in SearchName, Try to ReGet , Start	
				if  @iLen1 > @ilen2  or (@iCnt=0 and @iLen1>0 and @iLen2>0)   --2022-02-16 change old: @iCnt=0 or @iLen1 > @ilen2  
				begin
				   -- Using PartyName Vs. SearchName
				   SELECT @iCntSp1=0, @iCntSp2=0, @iCnt=LEN(@sStr)+1,@iLenC2=Len(@sStr)+1
				   SELECT @iLen1=CHARINDEX(@sStr,@ssSearchName)
				   WHILE( @iCnt > 0  AND @iCntSp2 < 4 )
				   BEGIN
				      SELECT @iCnt=@iCnt+1	      
					  if SUBSTRING(@sMatchName,@iCnt,1)=' '
					  begin
						  SELECT @iCntSp2=@iCntSp2+1,@iLenC1=@iLenC2
                          if @iCntSp2 >= 2 and @icnt=LEN(@ssSearchName)
						  Begin
							 SELECT @ilenC2=@iCnt
							 if @iCnt > @iLenC1     --2022-02-14 change fix substring err
							 SELECT @sStr1=SUBSTRING(@sMatchName, @iLenC2, @iCnt-@iLenC1+1)
  					         
							 --SELECT '!!!-GetMatchedText0-b:' mark, @sStr1 lastWord,@iCntSp2 CntSP,@iLenC1 len1, @iLenC2 len2, @icnt icnt                    	     
						  end
					  end
					  
					  --2022-01-17 add for loop timeout, start
					  if @iCnt>= Len(@ssSearchName)     
					     SELECT @iCnt=0, @iCntSp2=10    
						                            
				   END

				end
				--Last Word not in SearchName, Try to ReGet , end	

				--2022-02-16 FIX Lastword is specital char, Start
				IF Len(RTRIM(LTRIM(@sStr1)))= 1 and PATINDEX('%[-&]%',@sStr1) >0
				   SELECT @sStr1=@sStr
				--2022-02-16 FIX Lastword is specital char, End

				--2022-03-03 fix M.A.R. , Start
				IF @sStr=@sStr1 AND SUBSTRING(@sStr,2,1)='.' AND SUBSTRING(@sStr,4,1)='.' 
				Begin
					SELECT @sStr=REPLACE(@sStr,'.',''),  @sStr1=REPLACE(@sStr1,'.','')
                    if CHARINDEX(@sStr,@ssSearchName) > 0
					   SELECT @sStrTmp1=@sStr, @iLen1=CHARINDEX(@sStr,@ssSearchName), @iLen2=@iLen1+1
				End
				
				SELECT @sPartyNa2=SUBSTRING(@ssSearchName,@iLen1-1,1)
				if PATINDEX('%[A-Za-z]%',@sPartyNa2) > 0
				   SELECT @iCntSp1=@iLen1+1, @iLen1=CHARINDEX(@sStr+' ',@ssSearchName,@iCntSp1+1)

				if @sStr1='LTD' and @iLen2=0 and CHARINDEX('LIMITED',@ssSearchName) >0
				   SELECT @iLen2= CHARINDEX('LIMITED',@ssSearchName, @iLen1+1), @sStr1='LIMITED'
				if @sStr1='LIMITED' and @iLen2=0 and CHARINDEX('LTD',@ssSearchName) >0
				   SELECT @iLen2= CHARINDEX('LTD',@ssSearchName, @iLen1+1),@sStr1='LTD'
                if @sStr='LTD' and @iLen1=0 and CHARINDEX('LIMITED',@ssSearchName) >0
			       SELECT @iLen1= CHARINDEX('LIMITED',@ssSearchName),@sStr='LIMITED'
				if @sStr='LIMITED' and @iLen1=0 and CHARINDEX('LTD',@ssSearchName) >0
				   SELECT @iLen1= CHARINDEX('LTD',@ssSearchName),@sStr='LTD'
				--2022-03-03 fix M.A.R. , End

				--SELECT '!!!-GetMatchedText-1:' mark, @ilen1 len1, @ilen2 len2, @sStr first,@sStr1 LastWord, CHARINDEX(@sStr, @ssSearchName) ichar1, CHARINDEX(@sStr1,@ssSearchName) ichar2

				IF @iLen1 <>0 AND @iLen2 <>0 	
				   begin		   
				       if Len(@sStr1)=1 
					   begin
					      if Len(@sStr)=1 and SUBSTRING(@ssSearchName,@ilen1-1,1) <> ' ' and SUBSTRING(@ssSearchName,@ilen1-1,1) <> '-' and  SUBSTRING(@ssSearchName,@ilen1-1,1) <> ',' and  SUBSTRING(@ssSearchName,@ilen1-1,1) <> '|'  --2022-03-02 change 
                             SELECT @iLen1=CHARINDEX(LTRIM(RTRIM(@sStr)),@ssSearchName, @ilen1+1), @iLen2=CHARINDEX(LTRIM(RTRIM(@sStr1)),@ssSearchName,@ilen1+1) , @icnt=@iLen2	
					     
						  --2022-03-25 change --2022-03-23 change--2022-03-02 CHANGE FIX LASTWORD POSITION IS UNRESONABLE AND REGET POSITION, START
			              --if SUBSTRING(@ssSearchName,@ilen2-1,1) <> ' ' and SUBSTRING(@ssSearchName,@iLen2-1,1) <> '-' and  SUBSTRING(@ssSearchName,@iLen2-1,1) <> ',' and  SUBSTRING(@ssSearchName,@iLen2-1,1) <> '|'
						  if @iLen2-1 > 0
						  BEGIN
						    IF PATINDEX('%[A-Za-z0-9]%',SUBSTRING(@ssSearchName, @ilen2-1, 1) ) > 0
							begin
						       SELECT @iLen2=CHARINDEX(LTRIM(RTRIM(@sStr1)),@ssSearchName,@iLen2+1)
							   SELECT @icnt=@iLen2, @iLen2=CHARINDEX(' ',@ssSearchName,@iCnt+1), @iCnt=@iLen2
							end
							--2022-03-25 change lastword+1 is not special char, Start
							IF PATINDEX('%[A-Za-z0-9]%',SUBSTRING(@ssSearchName, @iLen2+Len(@sStr1), 1) ) > 0
                               SELECT @iLen2=CHARINDEX(' ',@ssSearchName,@iLen2+Len(@sStr1)), @iCnt=@iLen2
						    --2022-03-25 change lastword+1 is not special char, End
						  END
						  --2022-03-23 change--2022-03-02 CHANGE FIX LASTWORD POSITION IS UNRESONABLE AND REGET POSITION, END
			
						  --2022-02-15 fix lastword is number, Start
						  if ISNUMERIC(@sStr1) > 0 and Len(@sStr1)=1
						     SELECT @sStr1=@sStr, @iLen2=CHARINDEX(' ',@ssSearchName,@iLen1+1) 
						  --2022-02-15 fix lastword is number, End

/*						  				         
						  --2022-03-23 change --2022-02-09 add for first name = last name , Start
						  SELECT @sStr=LTRIM(RTRIM(@sStr)), @sStr1=LTRIM(RTRIM(@sStr1))
						  if @sStr=@sStr1
						     SELECT @iLen2=CHARINDEX(@sStr1,@ssSearchName,@ilen1+len(@sStr)+1) , @icnt=@iLen2	
                          --2022-03-23 change --2022-02-09 add for first name = last name , End
*/
						  IF @iLen2 > @iLen1  --2022-02-16 FIX INVALID LENGTH ERR
				             SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@ilen1, @ilen2-@ilen1+1) 
                       end

                       else iF @iLen2 > @iLen1   --2022-03-09 CHANGE
					       SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@ilen1, @iLen2+len(@sstr1)-@iLen1) 
						  
			
					  --2022-03-23 change fix for first name = last name , Start
					  SELECT @sStr=LTRIM(RTRIM(@sStr)), @sStr1=LTRIM(RTRIM(@sStr1))
					  SELECT @iLenC1=CHARINDEX(@sStr, @ssSearchName)
					  if @iLenC1 > 0
					     SELECT @iLenC2=CHARINDEX(' ', @ssSearchName, @iLenC1+1)
					  
					  --check Previous char and next char must be special char
					  if @sStr=@sStr1
					  begin
					     
						 if @iLenC1-1 > 0 
						 begin
						    SELECT @iLen1=@iLenC1
						    if PATINDEX('%[0-9A-Za-z]%', SUBSTRING(@ssSearchName,@iLenC1-1,1) ) > 0 
						       SELECT @iLenC1=CHARINDEX(' ',@ssSearchName,@iLenC1+1), @iLen1=@iLenC1
						    if @iLenC1 =0 
							   SELECT @iLenC1=Len(@ssSearchName), @iLen1=@iLenC1
						 end

						 if @iLenC2 > 0
						 begin
						    SELECT @iLen2=@iLenC2
						    if PATINDEX('%[0-9A-Za-z]%', SUBSTRING(@ssSearchName,@iLenC2,1) ) > 0 
						       SELECT @iLenC2=CHARINDEX(' ',@ssSearchName, @iLenC2+1) , @icnt=@iLen2, @iLenC2=@iLen2
				            if @iLenC2 =0 
							   SELECT @iLenC2=Len(@ssSearchName), @iLen2=@iLenC2, @icnt=@ilen2
						 end
						 
						 if @iLen2 > @iLen1 AND @iLen1 > 0
						    SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@iLen1,@iLen2-@iLen1+1)
                         else 
						    SELECT @sStrTmp2=''
					  end
					
                      --2022-03-23 change fix for first name = last name , End
		   
		              SELECT @sStrTmp1=@sStrTmp2
                      
					  --2022-03-23 fix matchname is individual word, Start
					  if CHARINDEX(@sStrTmp1, @ssSearchName)>0 
					  begin
					     SELECT @iLenC1=CHARINDEX(@sStrTmp1, @ssSearchName)
						 IF @iLenC1 >1 AND PATINDEX('%[0-9A-Za-z]%', SUBSTRING(@ssSearchName,@iLenC1-1,1) ) >0
					        SELECT @sStrTmp1='', @sStrTmp='', @sStrTmp2=''
					  end
					  --2022-03-23 fix matchname is individual word, End

					   --SELECT '!!!-GetMatchedText-1-a:' mark, @ilen1 len1, @ilen2 len2,@sStrTmp2 matchText, CHARINDEX(@sStrTmp2, @ssSearchName) C1, DIFFERENCE(@sStrTmp2, @sMatchName) iDif

					   if CHARINDEX(@sStrTmp2, @ssSearchName,@iLen1) >0  or DIFFERENCE(@sStrTmp2, @sMatchName) > 0
					       goto EndProc
				   end 

				   ELSE 
				   BEGIN

				       IF @iLen1=0 AND @ILEN2 <> 0
					   begin
		      		      SELECT @iLen1=CHARINDEX('|' + SUBSTRING(@sStr,1,2),@ssSearchName) + 1						 
						  if @iLen1=1
						     SELECT @iLen1=CHARINDEX('|' + SUBSTRING(@sStr,1,1),@ssSearchName) + 1
						  --2022-01-20 add for bug : get / firstword , Start
                          if @iLen1=1  
						     SELECT @iLen1=CHARINDEX('/' + SUBSTRING(@sStr,1,2),@ssSearchName) + 1	
						   if @iLen1=1
						     SELECT @iLen1=CHARINDEX('/' + SUBSTRING(@sStr,1,1),@ssSearchName) + 1
                          --2022-01-20 add for bug : get / firstword , End
                          if @iLen1=1
						     SELECT @iLen1=CHARINDEX(' ' + SUBSTRING(@sStr,1,2),@ssSearchName) + 1						 
						  if @iLen1=1
						     SELECT @iLen1=CHARINDEX(' ' + SUBSTRING(@sStr,1,1),@ssSearchName) + 1
						  if @iLen1=1
						     SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,3),@ssSearchName)
						  if @iLen1=0  
						     SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,2),@ssSearchName)

						  --2022-02-09 add
						   if @iLen1=0  
						     SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,1),@ssSearchName)
						  if @ilen1=0						
						     SELECT @ilen1=LEN(@ssSearchName)

                          SELECT @iCnt=CHARINDEX(' ',@ssSearchName,@iLen1+1)
						  if @iCnt=0
						     SELECT @iCnt=Len(@ssSearchName)
						  if @iCnt > @iLen1
					         SELECT  @sStrTmp=SUBSTRING(@ssSearchName,@ilen1, @icnt-@iLen1+1)
                          else
						     SELECT @sStrTmp=''

						  --2022-02-15 add fix matchname is 'MS 13' reget lastword in SearchName, Start
						  if @iLen2 > 0  and SUBSTRING(@ssSearchName,@iLen2-1,1) <> ' ' and SUBSTRING(@ssSearchName,@iLen2-1,1) <> '-' and  SUBSTRING(@ssSearchName,@iLen2-1,1) <> ',' and  SUBSTRING(@ssSearchName,@iLen2-1,1) <> '|'
						  BEGIN
						     SELECT @iCnt=@iLen2, @iLen2=CHARINDEX(@sStr1,@ssSearchName,@iCnt+1)
							 if @iLen2 > 0 and SUBSTRING(@ssSearchName,@iLen2-1,1) <> ' ' and SUBSTRING(@ssSearchName,@iLen2-1,1) <> '-' and  SUBSTRING(@ssSearchName,@iLen2-1,1) <> ',' and  SUBSTRING(@ssSearchName,@iLen2-1,1) <> '|'
						        SELECT @iCnt=@iLen2, @iLen2=CHARINDEX(@sStr1,@ssSearchName,@iCnt+1)        
						  END
						  --2022-02-15 add fix matchname is 'MS 13' reget lastword in SearchName, End
                          
						  --SELECT '!!!-ReGetLocFirst-1-2:'Mark, @iLen1 i1, @iLen2 i2, @sStr first, @sStr1 last, @sStrTmp1 mtext,SUBSTRING(@ssSearchName,@ilen1,20) s1, @iCnt cnt

--2021-12-20 add RETRY GET FIRST WORD
ReGetLocFirst:

		--ReGet fist word location, Start
		if @ilen1=0
		begin
			SELECT @iLen1=CHARINDEX('|'+SUBSTRING(@sStr,1,1), @ssSearchName, @ilen1+1)+1
			--2022-01-20 for bug: / firstname, start
			if @iLen1=1
			   SELECT @iLen1=CHARINDEX('/'+SUBSTRING(@sStr,1,1), @ssSearchName, @ilen1+1)+1
			--2022-01-20 for bug: / firstname, End
			if @iLen1=1
			   SELECT @iLen1=CHARINDEX(' '+SUBSTRING(@sStr,1,1), @ssSearchName, @ilen1+1)+1
            if @ilen1=0
			   SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+1)
        end

/*
		--2021-12-21 add for move next, Start
		if @ilen1 + len(@sMatchName) < @ilen2
		begin
		    SELECT @iCnt=@ilen1+len(@sStr)+1
			SELECT @iLen1=@iCnt
		   	SELECT @iLen1=CHARINDEX('|'+SUBSTRING(@sStr,1,1), @ssSearchName, @iLen1)
			if @iLen1=0
			   SELECT @ilen1=@icnt, @iLen1=CHARINDEX(' '+SUBSTRING(@sStr,1,1), @ssSearchName, @ilen1+1)+1
            if @ilen1=1
			   SELECT @ilen1=@icnt,@iLen1=CHARINDEX(SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+1)
		end
		---2021-12-21 add for move next, End
*/

        --2021-12-22 add for first Loc# = Last Loc#, Reget Last Loc@, Start
		if @iLen1 <= @iLen2
		BEGIN
		    SELECT @iCnt=@ilen1+len(@sStr)+1
			SELECT @iLen2=@iCnt
			SELECT @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,2), @ssSearchName, @iCnt)
		    if @iLen2=0
		     	SELECT @iLen2=CHARINDEX('|'+SUBSTRING(@sStr1,1,2), @ssSearchName, @iCnt) +1
			--2022-01-20 add for /, start
		    if @ilen2=0
			   SELECT @iLen2=CHARINDEX('|'+SUBSTRING(@sStr1,1,2), @ssSearchName, @iCnt) +1
            --2022-01-20 add for /, End
			if @iLen2=0
			   SELECT @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @ssSearchName, @iCnt) +1
            if @iLen2=1
			   SELECT @iLen2=CHARINDEX(SUBSTRING(@sStr1,1,2), @ssSearchName,@iCnt)
		END
        --2021-12-22 add for first Loc# = Last Loc#, , Reget Last Loc@, end

		--2022-03-01 add bug fix - Yee, Start
		if @sStr='' and @iLen1 >0 and CHARINDEX(@sStr1, @ssSearchName) > 0
		   SELECT @iLen1=@iLen2, @iLen2=CHARINDEX(@sStr1, @ssSearchName)
		--2022-03-01 add bug fix - Yee, End

		--SELECT '!!!-ReGetLocFirst-2:'Mark, @iLen1 i1, @iLen2 i2, @sStr first, @sStr1 last, @sStrTmp1 mtext,SUBSTRING(@ssSearchName,@ilen1,20) s1, @iCnt cnt

		--get Party Name next word???
		if @ilen1=0
		begin
		   SELECT @iCnt=1, @iLen1=@iCnt
		   SELECT @icnt=CHARINDEX(' ',@ssMatchName, @iCnt ) +1
		   if @icnt=1
		      SELECT @icnt=LEN(@ssSearchName)
		   if @icnt<>LEN(@ssSearchName)
		      SELECT @sStrTmp=SUBSTRING(@ssMatchName, @iCnt, CHARINDEX(' ',@ssMatchName, @iCnt )-@icnt )
           else
		     SELECT @sStrTmp=SUBSTRING(@ssMatchName, @ilen1, @icnt-@iLen1+1)

		   if CHARINDEX(@sStrTmp, @ssSearchName) > 0
		      SELECT @sStr=@sStrTmp, @iLen1=CHARINDEX(@sStrTmp, @ssSearchName)
		end
		
		else   
		begin
			SELECT @iCnt=CHARINDEX(' ',@ssSearchName,@iLen1+1)
			if @iCnt=0
			   SELECT @iCnt=Len(@ssSearchName)
			if @iCnt > @iLen1  --2022-02-16 add fix SUBSTRING LENGTH ERROR
			   SELECT @sStrTmp=SUBSTRING(@ssSearchName,@ilen1,@icnt-@iLen1)
	    end
        --ReGet fist word location, End
		
											 							    
						  if  LEN(@sStr) > LEN(@sStrTmp) and LEN(@sStr) - LEN(@sStrTmp) > 3
						  BEGIN
						     SELECT @iLen1=CHARINDEX(' '+SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+len(@sStrTmp)+1)
							 if @iLen1 = 0 
							    SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+len(@sStrTmp)+1)
							if @iLen1 = 0
							    SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,1), @ssSearchName)
							 --if @iLen1 = 0 
							 --   goto ReGetLocFirst
							 --SELECT '!!!-GetMatchedText-1-a-4-a:' mark, @ilen1 len1, @ilen2 len2,@sStr first, @sStrTmp s1, @sStr1 last, Len(@sStr) len1,len(@sStrTmp) len2
							 if Len(@sStrTmp) > 0    --2022-01-13 add
							    SELECT @sStr=@sStrTmp
						  END

						  ELSE if LEN(@sStr) <> LEN(@sStrTmp)
						  BEGIN
						     if @ilen1=0
						        SELECT @iLen1=CHARINDEX(' '+SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+len(@sStrTmp)+1)
							 if @ilen1=0
							    SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+len(@sStrTmp)+1)
                             if @iLen1 = 0
							    SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,1), @ssSearchName)
							 --if @iLen1 = 0 
							 --   goto ReGetLocFirst							 
							 --SELECT '!!!-GetMatchedText-1-a-4-b:' mark, @ilen1 len1, @ilen2 len2,@sStr first, @sStrTmp s1, @sStr1 last, Len(@sStr) len1,len(@sStrTmp) len2
							 /*  --2022-03-11 mask
							  if Len(@sStrTmp) > 0   --2022-01-13 add
							    SELECT @sStr=@sStrTmp
							*/

							 IF LEN(RTRIM(@sStr1)) >1 and CHARINDEX(@sStr1,@ssMatchName) >0  AND  CHARINDEX(@sStr,@ssMatchName) =0 --2022-03-11 add
							 BEGIN   --2022-03-15 ADD
							    --2022-03-15 MASK --SELECT @sStr=@sStr1, @sStrTmp=@sStr1               --2022-03-15 MASK FIX DI CHANG SHENG  --2022-03-11 add
						        SELECT @sStrTmp=''   --2022-03-15 ADD
								GOTO GETNEXTWORD     --2022-03-15 ADD
							 END    --2022-03-15 ADD
						  END

						  ELSE
						  BEGIN
						     --ReGet fist word location
							 if @iLen1=0
						        SELECT @iLen1=CHARINDEX(' '+SUBSTRING(@sStr,1,1), @ssSearchName, @ilen1+len(@sStrTmp)+1)+1
							 if @iLen1=1
							    SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,2), @ssSearchName, @ilen1+len(@sStrTmp)+1)
                             if @iLen1 = 0
							    SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,1), @ssSearchName)
                             --if @ilen1=0
							 --    goto ReGetLocFirst	
							 --SELECT '!!!-GetMatchedText-1-a-4-c:' mark, @ilen1 len1, @ilen2 len2,@sStr first, @sStrTmp s1, @sStr1 last, Len(@sStr) len1,len(@sStrTmp) len2
/*							 
							 if Len(@sStrTmp) > 0   --2022-01-13 add
							    SELECT @sStr=@sStrTmp
*/
						  END

                          --SELECT '!!!-GetMatchedText-1-a-4:' mark, @ilen1 len1, @ilen2 len2,@sStr first, @sStrTmp s1, @sStr1 last, Len(@sStr) len1,len(@sStrTmp) len2

					   end
					   
                      
                    IF @iLen1<>0 AND @iLen2 =0 
					begin
					    SELECT @iLen2=CHARINDEX(' ' + SUBSTRING(@sStr1,1,3),@ssSearchName, @iLen1+1) + 1
						if @iLen2=1
						    SELECT @iLen2=CHARINDEX(' ' + SUBSTRING(@sStr1,1,2),@ssSearchName, @iLen1+1) + 1
						if @iLen2=1
						    SELECT @iLen2=CHARINDEX(' ' + SUBSTRING(@sStr1,1,1),@ssSearchName, @iLen1+1) + 1
						if @iLen2=1
						    SELECT @iLen2=CHARINDEX(SUBSTRING(@sStr1,1,3),@ssSearchName, @iLen1+1)
						if @iLen2=0
						    SELECT @iLen2=CHARINDEX(SUBSTRING(@sStr1,1,2),@ssSearchName, @iLen1+1)
                        if @ilen2=0
						    SELECT @iLen2=LEN(@ssSearchName)

                        SELECT @iCnt=CHARINDEX(' ',@ssSearchName,@iLen2+1)
						if @iCnt=0
						    SELECT @iCnt=Len(@ssSearchName), @iLen2=@iCnt  --2022-02-16 change

						--2022-02-14 for not fund lastword, Start
                        if CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName, @iLen1+1) = 0  or CHARINDEX(' ', @ssSearchName, @iLen1+1) = 0
						   SELECT @iLen2=LEN(@ssSearchName), @iCnt=@iLen2   
						 --2022-02-14 for not fund lastword, End

						-- SELECT '!!!-GetMatchedText1-a: ' Mark, @sStrTmp1 TransactionText, @sstr FirstName, @sStr1 LastName, @ssSearchName SearchText, @ilen1 l1, @iLen2 l2, @iCnt icnt

				   end

				   --2021-12-17 add for Fist word & Last Word is partial match, Start
                   IF @iLen1=0 AND @iLen2 =0 
				   begin
					    --SELECT '!!!-GetMatchedText-1-d:' mark, @ilen1 len1, @ilen2 len2,@sStr first, @sStr1 last, @sMatchName PartyName
					    --First Name
						if @iLen1=0  --2022-02-09 change old @iLen1=1	 
						  begin
						    --2022-02-09 add for check account, and from | posiotion geet, Start
						    IF SUBSTRING(@ssSearchName,1,1)='/'
							BEGIN
							   SELECT @iCntSp1=CHARINDEX(' |',@ssSearchName)
							   SELECT @iLen1=CHARINDEX('|' + SUBSTRING(@sStr,1,1),@ssSearchName, @iCntSp1) + 1 
							   if @iLen1=1 --not found
							       SELECT @iLen1=CHARINDEX('/' + SUBSTRING(@sStr,1,1),@ssSearchName, @iCntSp1) + 1 
		                       if @iLen1=1 --not found
							       SELECT @iLen1=CHARINDEX(' ' + SUBSTRING(@sStr,1,1),@ssSearchName, @iCntSp1) + 1 
							   if @iLen1=1 --not found
							       SELECT @iLen1=CHARINDEX(',' + SUBSTRING(@sStr,1,1),@ssSearchName, @iCntSp1) + 1 	
							   if @iLen1=1 --not found
							       SELECT @iLen1=CHARINDEX('.' + SUBSTRING(@sStr,1,1),@ssSearchName, @iCntSp1) + 1 	
							END	

							ELSE
							BEGIN
								--SELECT '!!!-GetMatchedText-1-d-0-1:' mark, @ilen1 len1, @ilen2 len2,@sStr first, @sStr1 last, @sMatchName PartyName
								--2022-02-15 change not found and get min value, Start
								if @iLen1 =0
								Begin
								   SELECT @iCntSp1=CHARINDEX(' ' + SUBSTRING(@sStr,1,2),@ssSearchName), @iCntSp2=CHARINDEX('|' + SUBSTRING(@sStr,1,2),@ssSearchName), @iCnt=CHARINDEX('/' + SUBSTRING(@sStr,1,2),@ssSearchName)
								   if  @iCntSp1 < @iCntSp2 and @iCntSp1 > 0  --2022-02-16 change
								      SELECT @iLen1=@iCntSp1+1
								   if @iCnt > 0 and  @iCnt < @iCntSp1
								      SELECT @iLen1=@iCnt+1
                                End
								--2022-02-15 change not found and get min value, End
							
								--2022-02-15 change fix match position=1 , Start
								IF @iLen1 =1 AND SUBSTRING(@ssSearchName,1,1)=SUBSTRING(@sStr,1,1) --not found
								   SELECT @iLen1=1
								else
								Begin
									if @iLen1 =1  --not foud
									   SELECT @iLen1=CHARINDEX(' ' + SUBSTRING(@sStr,1,1),@ssSearchName) + 1   
									if @iLen1 =1  --not foud
									   SELECT @iLen1=CHARINDEX('|' + SUBSTRING(@sStr,1,1),@ssSearchName) + 1   
									if @iLen1 =1  --not foud
									   SELECT @iLen1=CHARINDEX('/' + SUBSTRING(@sStr,1,1),@ssSearchName) + 1   
									if @iLen1 =1  --not foud
									   SELECT @iLen1=CHARINDEX(',' + SUBSTRING(@sStr,1,1),@ssSearchName) + 1  
									if @iLen1 =1  --not foud
									   SELECT @iLen1=CHARINDEX('.' + SUBSTRING(@sStr,1,1),@ssSearchName) + 1  
								End
								--2022-02-15 change fix match position=1 , End  
							End
							--SELECT '!!!-GetMatchedText-1-d-0:' mark, @ilen1 len1, @ilen2 len2,@sStr first, @sStr1 last, @sMatchName PartyName
							--2022-02-15 change not found , End
							END
							--2022-02-09 add for check account, and from | posiotion geet, End

RegetLastWordPoistion:     --2022-01-20 add

 							 if @ilen1 > 1 and 	CHARINDEX(' ' + SUBSTRING(@sStr1,1,1),@ssSearchName, @iLen1+1) > @iLen1+1
							     SELECT @iLen2=CHARINDEX(' ' + SUBSTRING(@sStr1,1,1),@ssSearchName, @iLen1+1) + 1
                          end

						  if @iLen1 =1   
						  begin
						     --2022-02-15 chang fix poistion=1, Start
						     if SUBSTRING(@ssSearchName,1,1)<>SUBSTRING(@sStr,1,1)
							 Begin
								 SELECT @iLen1=CHARINDEX('|' + SUBSTRING(@sStr,1,1),@ssSearchName) +1
								  if @ilen1 > 1 and CHARINDEX(' '+ SUBSTRING(@sStr1,1,1),@ssSearchName, @iLen1+1) > @iLen1+1
									 SELECT @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName, @iLen1+1) 
							 End
							 --2022-02-15 chang fix poistion=1, End
							 
							  --2022-02-09 add
							  if @iLen1 <=1
							     SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,1),@ssSearchName)                          
						  end

						  --2020-12-23 CHANGE BUG FIX, START
						  if @iLen1=0 
						  begin
						     SELECT @iLen1=CHARINDEX(LTRIM(RTRIM(@sStr)),@ssSearchName)
							 if @iLen1=0
						        SELECT @iLen1=CHARINDEX('|'+SUBSTRING(@sStr,1,2),@ssSearchName), @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
							 if @ilen1=0
							    SELECT @iLen1=CHARINDEX(' '+SUBSTRING(@sStr,1,2),@ssSearchName), @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
                             if @ilen1=0
							    SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,2),@ssSearchName), @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
                           
						    --2022-02-15 change fix match position=1 , Start
                            IF @iLen1 =0 AND SUBSTRING(@ssSearchName,1,1)=SUBSTRING(@sStr,1,1) --not found
							   SELECT @iLen1=1
                            else
							Begin
							  if @ilen1=0
							  --2022-03-01 CHANGE FOR BUG FIX: DJUNADI ACHMAD , Start
							    SELECT @iCntSp1=CHARINDEX(' '+SUBSTRING(@sStr,1,1),@ssSearchName), @iCntSp2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
							  IF @iCntSp1=0 OR @iCntSp1 > CHARINDEX('|'+SUBSTRING(@sStr,1,1),@ssSearchName)
							    SELECT @iCntSp1=CHARINDEX('|'+SUBSTRING(@sStr,1,1),@ssSearchName), @iCntSp2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
                              IF @iCntSp1 > 0 OR @iCntSp2 > 0
							    SELECT @iLen1=@iCntSp1, @iLen2=@iCntSp2
                              --2022-03-01 CHANGE FOR BUG FIX: DJUNADI ACHMAD , End
                             End
                             --2022-02-15 change fix match position=1 , End
							
							 --2022-02-09 add for only get 1char check, Start
							 if @ilen1=0
							 Begin
							    SELECT @iLen1=CHARINDEX(SUBSTRING(@sStr,1,1),@ssSearchName), @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
                                if @iLen1 > 1 
								Begin
								   SELECT @iCnt=@iLen1
								   while( @iCnt > 1 )
								   BEGIN
								      SELECT @iCnt=@iCnt-1							  
								      if SUBSTRING(@ssSearchName,@iCnt,1)=' ' or SUBSTRING(@ssSearchName,@iCnt,1)='|' or SUBSTRING(@ssSearchName,@iCnt,1)='/' or SUBSTRING(@ssSearchName,@iCnt,1)=',' or  SUBSTRING(@ssSearchName,@iCnt,1)='.'
							             SELECT @iLen1=@iCnt+1, @iCnt=0
								   END
								End      
							 End
                             --2022-02-09 add for only get 1char check, End

							 if @ilen2=0
							    SELECT @iLen2=CHARINDEX('|'+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
							 if @ilen2=0
							    SELECT @iLen2=CHARINDEX(SUBSTRING(@sStr1,1,2),@ssSearchName,@iLen1+1) 
							 --2022-02-09 add 
							 if @ilen2=0
							    SELECT @iLen2=CHARINDEX(SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1) 

							 --2021-12-23 ADD 
                             IF  SUBSTRING(@ssSearchName,@ilen2,1)<>SUBSTRING(@sStr1,1,1)
							 BEGIN
						       SELECT @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,2),@ssSearchName,@iLen1+1)
							   IF @iLen2=0
							      SELECT @iLen2=CHARINDEX('|'+SUBSTRING(@sStr1,1,2),@ssSearchName,@iLen1+1)
                               IF @iLen2=0
							      SELECT @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
                               IF @iLen2=0
							      SELECT @iLen2=CHARINDEX('|'+SUBSTRING(@sStr1,1,2),@ssSearchName,@iLen1+1)
							   --2022-02-09 add 
							    IF @iLen2=0
							      SELECT @iLen2=CHARINDEX(SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen1+1)
                             END
						  
						  end
			
			             --2021-12-23 CHANGE BUG FIX, END
			
						 --SELECT '!!!-GetMatchedText-1-d-1:' mark, @ilen1 len1, @ilen2 len2,@icnt cnt,@sStr first, @sStr1 last, SUBSTRING(@ssSearchname,@ilen1,10)
						 
						  if @ilen1=0 			
						     SELECT @ilen1=1
						  else if @ilen2=0
						  --2022-02-15 fix get max value for lastword, Start
						  Begin
						    SELECT @iCntSp1= CHARINDEX(' ',@ssSearchName,@iLen1+len(@sStr)), @iCntSp2= CHARINDEX('|',@ssSearchName,@iLen1+len(@sStr))
							if @iCntSp1 >0 and @iCntSp1 > @iLen1 and ABS((@iCntSp1 - @ilen1) - Len(@sMatchName)) > 15
							   SELECT @ilen2=@icntSp1
                            if  @iCntSp2 >0 and @iCntSp2 > @iLen1 and ABS((@iCntSp2 - @ilen1) - Len(@sMatchName)) >15
							   SELECT @iLen2=@iCntSp2
                         End
                         --2022-02-15 fix get max value for lastword, Start

					    SELECT @iCnt=@iLen2

					    --SELECT '!!!-GetMatchedText-1-d-2:' mark, @ilen1 len1, @ilen2 len2,@icnt cnt,@sStr first, @sStr1 last
					  
					end					   
					--2021-12-17 add for Fist word & Last Word is partial match, End
					
					--2022-02-15 fix poistion first > last, Start
					if @iLen2 < @iLen1 and @iLen1=LEN(@ssSearchName)
					   SELECT @iLen1=@iLen2, @ilen2=CHARINDEX(' ',@ssSearchName, @iLen1)-1
					   if @iLen2=0 
					      SELECT @iLen2=Len(@ssSearchName)
					--2022-02-15 fix poistion first > last , End

				   --SELECT '!!!-GetMatchedText1-d-3: ' Mark, @sstr FirstName, @sStr1 LastName,@ilen1 l1, @iLen2 l2, @iCnt icnt--, SUBSTRING(@ssSearchName,@ilen1, @ilen2-@ilen1)
					   
					--2021-12-17 Change, Start					   
					if @ilen2 > @iLen1
					begin
					    if CHARINDEX(' ',@ssSearchName,@iLen2+1) > 0
					       SELECT @iCnt=CHARINDEX(' ',@ssSearchName,@iLen2+1)
						else  
						    SELECT @iCnt=len(@ssSearchName)
					
					   --2022-02-15 change fix not get full word, 2022-01-20 ADD FOR BUG FIX pROMSERVIS18, Start
					   IF @sStr=@sStr1
					   BEGIN
					      SELECT @iCnt=CHARINDEX(' ',@ssSearchName,@iLen1)
						  if @iCnt =0
						     SELECT @iCnt=CHARINDEX('|',@ssSearchName,@iLen1)
                          if @iCnt =0
						      SELECT @iCnt=Len(@ssSearchName)
					      ---2022-02-15 mask  --SELECT @iLen2=@iLen1+len(@sStr), @iCnt=@iLen2
						  SELECT @iLen2=@iCnt
                       END
					   --2022-02-15 change fix not get full word, 2022-01-20 ADD FOR BUG FIX pROMSERVIS18, End
					   
					   --2022-03-22 ADD RECHECK FIRST WORD PREVIOUS CHAR IS Resonable(Special Char), Start
					   if PATINDEX('%[a-zA-Z0-9]%', SUBSTRING(@ssSearchName,@iLen1-1,1)) = 0
					      if @iLen1 > 0 AND @iCnt > @iLen1
						     SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@iLen1, @iCnt-@iLen1+1), @sStrTmp=@sStrTmp2
					      else 
						     SELECT @sStrTmp2='', @sStrTmp='', @sStrTmp1=''
					   else
					   begin
					      SELECT @sStrTmp2='', @sStrTmp='', @sStrTmp1=''
						  goto GetNextWord
					   end
					   --2022-03-22 ADD RECHECK FIRST WORD PREVIOUS CHAR IS Resonable(Special Char), Start
					  
					   --SELECT '!!!-GetMatchedText1-d-3-1:'Mark, @iLen1 i1, @iLen2 i2, @sStr first, @sStr1 last, @sStrTmp mtext,SUBSTRING(@ssSearchName,@iCnt,5) s1,SUBSTRING(@ssSearchName,@iLen1-1,20) S2, @iCnt cnt ,DIFFERENCE(@sStrTmp, @sMatchName) idff,ABS(LEN(@sStrTmp)-LEN(@ssMatchName)) iABS, @iCntSp1 is1, @iCntSp2 is2
					   
					   --2022-03-16 add fix firstword did not correct, ARI INTER-NATIONAL LTD or MIMA INTERNATIONAL LIMITED, Start
					   IF CHARINDEX(@sStr,@sStrTmp)=0 AND SUBSTRING(@sStr,1,1)+' '=SUBSTRING(@sStrTmp,1,2)
					   BEGIN
					      SELECT @iCntSp1=CHARINDEX(@sStrTmp, @ssSearchName), @sPartyNa1=''
						  if @iCntSp1 > 0
						     SELECT @sPartyNa1=SUBSTRING(@ssSearchName,@iCntSp1-1,1)
                          if PATINDEX('%[|/,.-: ]%', @sPartyNa1) = 0
						  Begin
					         SELECT @iCntSp1=CHARINDEX(' ',@sStrTmp)+1
					         SELECT @sStrTmp2=SUBSTRING(@sStrTmp, @iCntSp1, LEN(@sStrTmp)-@iCntSp1+1)
							 SELECT @sStrTmp=@sStrTmp2
					      End
					   END
					   --2022-03-16 add fix firstword did not correct, ARI INTER-NATIONAL LTD or MIMA INTERNATIONAL LIMITED, End

					   --SELECT '!!!-GetMatchedText1-d-3-2:'Mark, @iLen1 i1, @iLen2 i2, @sStr first, @sStr1 last, @sStrTmp mtext,SUBSTRING(@ssSearchName,@iCnt,5) s1, @iCnt cnt ,DIFFERENCE(@sStrTmp, @sMatchName) lend,ABS(LEN(@sStrTmp)-LEN(@ssMatchName)) lenABS, @iCntSp1 is1, @iCntSp2 is2

					   --2022-02-09 add for OBSHCHESTVO S OGRANICHENNOY OTVETSTVENNOSTYU SBD, Start
					   --Reget last word
					   if  ABS(LEN(@sStrTmp)-LEN(@ssMatchName)) > 20 and CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen2+1) > 0
				       Begin    
						   SELECT @iLen2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1),@ssSearchName,@iLen2+1)+1
 				           SELECT @iCnt=@iLen2
						   SELECT @sStrTmp2=LTRIM(SUBSTRING(@ssSearchName,@iLen1, @iCnt-@iLen1+1)), @sStrTmp=@sStrTmp2 
					   End
					   --2022-02-09 add for OBSHCHESTVO S OGRANICHENNOY OTVETSTVENNOSTYU SBD, End
					   		  			  
					   --2022-03-27 add fix 'LIMITED LIABILITY COMPANY KONSTRUCTION COMPANY KONSOL STROI LIMITED', Start
					   IF CHARINDEX('LIMITED',@sMatchName) > 0 AND CHARINDEX('LIMITED LIABILITY COMPANY', @sMatchName) > 0 AND 
						  CHARINDEX('LIMITED LIABILITY COMPANY', @ssSearchName) > 0
					   BEGIN
						  SELECT @sStrTmp1='LIMITED LIABILITY COMPANY'
						  GOTO RetEnd
					   END
					   ELSE IF CHARINDEX('LIendend:MITED',@sMatchName) > 0 AND CHARINDEX('LIMITED LIABILITY CO', @sMatchName) > 0 AND
					   CHARINDEX('LIMITED LIABILITY CO', @ssSearchName) > 0
					   BEGIN
						  SELECT @sStrTmp1='LIMITED LIABILITY CO'
						  GOTO RetEnd
					   END
					   --2022-03-27 add fix 'LIMITED LIABILITY COMPANY KONSTRUCTION COMPANY KONSOL STROI LIMITED', End

					   --2022-03-27 add fix 'THE REPUBLIC OF ZIMBABWE', Start
					   IF PATINDEX('%'+'THE REPUBLIC OF THE'+'%', @sMatchName) > 0 AND 
						  PATINDEX('%'+'THE REPUBLIC OF THE'+'%', @ssSearchName) > 0
					   BEGIN
						  SELECT @sStrTmp1='THE REPUBLIC OF THE'
						  GOTO RetEnd
					   END
					   ELSE IF PATINDEX('%'+'THE REPUBLIC OF'+'%', @sMatchName) > 0 AND 
						  PATINDEX('%'+'THE REPUBLIC OF'+'%', @ssSearchName) > 0
					   BEGIN
						  SELECT @sStrTmp1='THE REPUBLIC OF'
						  GOTO RetEnd
					   END					   
					   --2022-03-27 add fix 'THE REPUBLIC OF ZIMBABWE', End     

					   	--2022-02-09 add for bug fix: match name have 2 heading char is same, Start
						if  DIFFERENCE(@sStrTmp, @sMatchName) = 4   and  ABS(LEN(@sStrTmp)-LEN(@ssMatchName)) >20 
						begin
						   SELECT @iCntSp1=CHARINDEX(SUBSTRING(@sStr,1,2),@ssMatchName), @iCntSp2=CHARINDEX(SUBSTRING(@sStr1,1,2),@ssMatchName,@iCntSp1+1)
						   --SELECT '!!!-GetMatchedText1-d-3-1:'Mark, @iLen1 i1, @iLen2 i2, @sStr first, @sStr1 last, @sStrTmp mtext,SUBSTRING(@ssSearchName,@ilen1,20) s1, @iCnt cnt ,DIFFERENCE(@sStrTmp1, @sMatchName) lend,ABS(LEN(@sStrTmp1)-LEN(@ssMatchName)) lenABS, @iCntSp1 is1, @iCntSp2 is2
						   
						   if @iCntSp2 > @iCntSp1 
						   begin
							
							  --2022-03-18 add fix, LI WANG, Start
							  IF PATINDEX('%'+@sStr+'%',@sStrTmp)=0 AND PATINDEX('%'+' '+@sStr1+'%',@sStrTmp)>0
							     GOTO GETNextWord
							  ELSE IF PATINDEX('%'+@sStr+'%',@sStrTmp)>0 AND PATINDEX('%'+' '+@sStr1+'%',@sStrTmp)=0
 							     GOTO GETNextWord
							  --2022-03-18 add fix, LI WANG, End

						      SELECT @iLen2=CHARINDEX('|',@ssSearchName,@iLen2+1)
							  if @ilen2 = 0
							     SELECT @iLen2=CHARINDEX(' ',@ssSearchName,@iLen2+1)
                              
							  IF @ilen2 > 0
							  begin
							     SELECT @sPartyNa1=SUBSTRING(@ssSearchName,@ilen2-1,1)                             
							     if (@sPartyNa1=' ' or @sPartyNa1='|'  or @sPartyNa1='/' )  and @iLen2 > @iLen1   --2022-02-14 change fix substring err
								    SELECT @sStrTmp2=LTRIM(SUBSTRING(@ssSearchName,@iLen1, @iLen2-@iLen1)), @sStrTmp=@sStrTmp2
                                 else if @iLen2 > @iLen1    --2022-02-14 change fix substring err
								    SELECT @sStrTmp2=LTRIM(SUBSTRING(@ssSearchName,@iLen1, @iLen2-@iLen1)), @sStrTmp=@sStrTmp2
								 --SELECT '!!!-GetMatchedText1-d-3-1-1:'Mark, @sPartyNa1 tempstr, @iLen2 i2, SUBSTRING(@ssSearchName,@ilen2,20) tmp, @sStrTmp mtext
							  end					
                           end

						   else  IF DIFFERENCE(@sStrTmp, @sMatchName) <> 4
						   begin
						      --reget last word
							  SELECT @iCntSp2=CHARINDEX(@sStr1,@ssMatchName), @iCnt=@iCntSp2-2
							  while( @iCnt > 0)
							  begin
							     SELECT @iCnt=@iCnt-1
								 if SUBSTRING(@ssMatchName,@iCnt,1)=' ' or SUBSTRING(@ssMatchName,@iCnt,1)=',' or SUBSTRING(@ssMatchName,@iCnt,1)='.'
								 begin
								    if @iCntSp2 > @iCnt    --2022-02-14 change fix substring err
								       SELECT @sStr1=SUBSTRING(@ssMatchName,@iCnt+1, @iCntSp2-@iCnt-1), @iCnt=0
								    SELECT @iCntSp1=@iLen2, @iLen2=CHARINDEX(SUBSTRING(@sStr1,1,2),@ssSearchName,@iCntSp1+1)
									if @ilen2 = 0 
									   SELECT @iCntSp1=@iLen2, @iLen2=CHARINDEX(SUBSTRING(@sStr1,1,1),@ssSearchName,@iCntSp1+1)
									if @iLen2 = 0
									   SELECT @ilen2=Len(@ssSearchName)
									SELECT @iCntSp1=CHARINDEX(' ',@ssSearchName,@iLen2+1)
									if @iCntSp1 > @iLen1 and CHARINDEX('|',@ssSearchName,@iLen2+1) < @iCntSp1
									   SELECT @iCntSp1=CHARINDEX('|',@ssSearchName,@iLen2+1)
                                    
									if @iCntSp1 = 0
									   SELECT  @iCntSp1=@iLen2 + len(@sStr1)
                                    if @iCntSp1 > @iLen1    --2022-02-14 change fix substring err
								       SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@iLen1, @iCntSp1-@iLen1+1)								   
								    SELECT @sStrTmp=@sStrTmp2
							    end
						    end	
						  end			   
						end
						
					   if @iCntSp1 > 0
					      SELECT @iCnt=@iCntSp1  
                       else
					      SELECT @iCnt=@iLen2
                       --2022-02-09 add for bug fix: match name have 2 heading char is same, End

					   --2022-03-17 ADD FIX MATCHNAME IS 1 WORD, Start
					   IF CHARINDEX(' ', @sMatchName) =0 --1Word
					   begin
					      SELECT @sStrTmp1=@sStrTmp
					      GOTO RetEnd
                       end
					   --2022-03-16 ADD FIX MATCHNAME IS 1 WORD, End

					   --2022-04-07 fix matchname more 1 word, Start
					   else IF CHARINDEX(' ', @sMatchName) >0 AND CHARINDEX(@sStrTmp, @sMatchName) > 0
					   Begin
					      IF CHARINDEX(@sStrTmp, @sMatchName) =1 
                             SELECT @iLenC1=LEN(@sStrTmp)+2, @iLenC2=CHARINDEX(' ',@sMatchName,@iLenC1)
							 IF @iLenC2 > 0
							    SELECT @sPartyNa1=LTRIM(RTRIM(SUBSTRING(@sMatchName,1, @iLenC2)))
                             IF CHARINDEX(@sPartyNa1, @ssSearchName) > 0
							 begin
							    SELECT @sStrTmp=@sPartyNa1, @sStrTmp1=@sStrTmp, @iMatchFlag=1
							    GOTO RetEnd2
						     end
					   End
					   --2022-04-07 fix matchname more 1 word, End
					   --SELECT '!!!-GetMatchedText1-d-3-3: ' Mark, @sstr First, @sStr1 Last,@ilen1 l1, @iLen2 l2, @iCnt icnt, @sStrTmp mtext,SUBSTRING(@ssSearchName,@ILEN1,20) s0,SUBSTRING(@ssSearchName,@iCnt,20) s1, @iCntSp1 is1, @iCntSp2 is2
					   
					   --2022-01-13 change, start     
					   if CHARINDEX('|/',@sStrTmp) >0 
					       SELECT @sStrTmp2= SUBSTRING(@sStrTmp,1,CHARINDEX('|/',@sStrTmp) -1 )	, @sStrTmp=@sStrTmp2
                       
                       else if  (SUBSTRING(@sStrTmp,@iLen2,2)='| ')  and SUBSTRING(@sStrTmp,@ilen2,1)<>SUBSTRING(@sStr1,1,1)  -- --2022-02-09 change old:SUBSTRING(@sStrTmp,@iCnt,1)='| ')
					        SELECT @sStrTmp2= SUBSTRING(@sStrTmp,1,@iCnt-2), @sStrTmp=@sStrTmp2	
					   else if  (SUBSTRING(@sStrTmp,@iLen2,1)='|' or SUBSTRING(@sStrTmp,@iLen2,1)=' ' or SUBSTRING(@sStrTmp,@iLen2,1)='/' )  and SUBSTRING(@sStrTmp,@iLen2+1,1)<>SUBSTRING(@sStr1,1,1)   --2022-01-20 change
					        SELECT @sStrTmp2= SUBSTRING(@sStrTmp,1,@iLen2-1), @sStrTmp=@sStrTmp2	
					   
					   --2022-02-15 fix matchtext from poistion 1, Start
					   if SUBSTRING(@ssSearchName,1,1)=SUBSTRING(@sStrTmp,1,1) and SUBSTRING(@sStrTmp,1,1)<>'/' and @iLen2 = LEN(@ssSearchName) and CHARINDEX(' ',@sStrTmp)=0
					   Begin
					      if CHARINDEX('/',@sStrTmp) >0
						     SELECT @sStrTmp2= SUBSTRING(@sStrTmp,1,CHARINDEX('/',@sStrTmp) -1 ), @sStrTmp=@sStrTmp2
                          else if CHARINDEX(' ',@sStrTmp) >0
						     SELECT @sStrTmp2= SUBSTRING(@sStrTmp,1,CHARINDEX(' ',@sStrTmp) -1 ), @sStrTmp=@sStrTmp2
						  else if CHARINDEX(',',@sStrTmp) >0
						     SELECT @sStrTmp2= SUBSTRING(@sStrTmp,1,CHARINDEX(',',@sStrTmp) -1 ), @sStrTmp=@sStrTmp2
                       End
					   --2022-02-15 fix matchtext from poistion 1, End
					   --2022-01-13 change, End

					   --2022-03-16 add fix LI LI MIN or LI CHIN E or DIAZ MANUEL  , Start
					   if Difference(@sStrTmp,@sMatchName)=3 AND (CHARINDEX(@sStr,@sStrTmp)=0 OR CHARINDEX(@sStr1,@sStrTmp)=0 )
					   begin
					      --SELECT '!!!-GetMatchedText1-d-3-4: ' mark, @sStrTmp sTmp, @sStrTmp2 stmp2, difference(@sStrTmp,@sMatchname) idiff
  						  --2022-03-16 add fix INOVEST LIMITED, Start
							SELECT @sStrTmp1=@sStrTmp
							IF CHARINDEX(@sStrTmp1,@sMatchName)=0 And CHARINDEX(@sStr,@sStrTmp1)=0 AND CHARINDEX(@sStr1,@sStrTmp1)=0 AND (SUBSTRING(@sStr,1,1)=SUBSTRING(@sMatchName,1,1) AND CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @sMatchName)>0 )
							Begin							
								IF SUBSTRING(@sStr,1,1)=SUBSTRING(@sStrTmp1,1,1) AND SUBSTRING(@sStr1,1,1)=SUBSTRING(@sStrTmp1,CHARINDEX(' ',@sStrTmp1)+1,1)      
									GOTO RetEnd 
								ELSE If SUBSTRING(@sStr,1,1)<>SUBSTRING(@sStrTmp1,1,1) AND CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @sStrTmp1)=0
								BEGIN
									SELECT @sSTrTmp1=''      
									GOTO GetMidWord    --GetNextWord
								END
	   
							End

						  ELSE
						  begin
							  IF CHARINDEX(@sStr,@sStrTmp)=0 AND CHARINDEX(' '+@sStr1,@sStrTmp)>0
								 SELECT @sStrTmp2=@sStr1, @sStrTmp1=@sStr1
							  ELSE IF CHARINDEX(@sStr,@sStrTmp)>0 AND CHARINDEX(' '+@sStr1,@sStrTmp)=0
								 SELECT @sStrTmp2=@sStr, @sStrTmp1=@sStr
         					  ELSE IF CHARINDEX(@sStrTmp2,@sMatchName)=0 AND (SUBSTRING(@sStr,1,1)<>SUBSTRING(@sMatchName,1,1) AND CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @sMatchName)=0) 
								 SELECT @sStrTmp2=''         
							  ELSE IF CHARINDEX(@sStr,@sStrTmp)=0 AND CHARINDEX(' '+@sStr1,@sStrTmp)=0
								  SELECT @sStrTmp2='' 
						    
							  SELECT @sStrTmp1=@sStrTmp2
							  goto GetMidWord
                          end
					   end
					   --2022-03-15 add fix LI LI MIN or LI CHIN E or DIAZ MANUEL , End
					  
					   --2022-03-18 change add : --2021-12-22 add for fix match name did not include 0-9, Start	
					   if PATINDEX('%[0-9:]%',@sStrTmp) > 0 and PATINDEX('%[0-9:]%',@sMatchName) = 0
					   Begin
					      SELECT @iCnt=PATINDEX('%[0-9:]%',@sStrTmp)
					      if @iCnt > 0
					         SELECT @iCnt=@iCnt-1, @sStrTmp2= SUBSTRING(@sStrTmp,1,@iCnt)				
					   end

					   else
					   begin   --2022-01-13 add, Start
					      SELECT @sStrTmp=LTRIM(RTRIM(@sStrTmp))
					      if Len(@sStrTmp) > 1
					         SELECT @sStrTmp2=@sStrTmp
					   end     --2022-01-13 add, end

					  --2022-03-18 change add :  --2021-12-22 add for fix match name did not include 0-9, End
				      --SELECT '!!!-GetMatchedText1-d-4-1: ' Mark, @sStrTmp2 mText, @sstr First, @sStr1 Last, @iLen1 l1, @iLen2 l2, @icnt cnt,SUBSTRING(@ssSearchName,@iLen1,20) s1, SUBSTRING(@ssSearchName,@icnt-5,5) s2 

				      --2022-01-13 add, process / , Start
				      SELECT @sStrTmp=LTRIM(RTRIM(@sStrTmp))
					  if @ilen1=1 and PATINDEX('%[/]%', @sStrTmp) > 0 
					  begin
						 SELECT @sStrTmp2=SUBSTRING(@sStrTmp,PATINDEX('%[|]%',@sStrTmp)+1, Len(@sStrTmp)-PATINDEX('%[|]%',@sStrTmp)+1)
						 if CHARINDEX('/',@sStrTmp2) > 0 and Len(@sStrTmp2) > @iLen1  --2022-02-14 change fix substring err
						    SELECT @ilen1=CHARINDEX('/',@sStrTmp2) +1, @sStrTmp=SUBSTRING(@sStrTmp2,@iLen1, Len(@sStrTmp2)-@iLen1+1), @sStrTmp2=@sStrTmp
					  end
                      --2022-01-13 add, process / , End	   

					  --SELECT '!!!-GetMatchedText1-d-4-2: ' Mark, @sStrTmp2 mText, @sstr First, @sStr1 Last, @iLen1 l1, @iLen2 l2, @icnt cnt,SUBSTRING(@ssSearchName,@iLen1,20) s1, SUBSTRING(@ssSearchName,@icnt-5,5) s2 , DIFFERENCE(@sStrTmp2, @sMatchName) idiff, CHARINDEX(@sStrTmp2, @ssSearchName) ichar

					   if CHARINDEX(@sStrTmp2, @ssSearchName) >0  or DIFFERENCE(@sStrTmp2, @sMatchName) >0
						 begin
						    
						    --SELECT '!!!-GetMatchedText-2-A: ' Mark, @sStrTmp2 TransactionText, @sstr FirstName, @sStr1 LastName, @iLen1 l1, @iLen2 l2,@iCnt cnt, DIFFERENCE(@sStrTmp2, @sMatchName) idiff, ABS(len(@sstrTmp2) - LEN(@smatchName)) lenABS
					        select @sStrTmp1=@sStrTmp2
							
							--2022-03-17 add fix IRÁN AIRCRAFT MANUFACTURING INDUSTRIES, Start
				     		--SELECT '!!!-GetMatchedText-3-A: ' Mark, @sStrTmp1 s1, @sstr FirstName, @sStr1 LastName, @iLen1 l1, @iLen2 l2,@iCnt cnt, DIFFERENCE(@sStrTmp2, @sMatchName) idiff, ABS(len(@sstrTmp2) - LEN(@smatchName)) lenABS,CHARINDEX(@sStr, @sStrTmp2) ic1, CHARINDEX(@sStr1, @sStrTmp2) ic2,SUBSTRING(@sStrTmp2,CHARINDEX(' ',@sStrTmp2)+1,1) ss1
							if CHARINDEX(@sStr, @sStrTmp2)= 0 AND  CHARINDEX(@sStr1, @sStrTmp2)=0  AND  SUBSTRING(@sStr,1,1)<>SUBSTRING(@sStrTmp2,1,1) AND SUBSTRING(@sStr1,1,1)<>SUBSTRING(@sStrTmp2,CHARINDEX(' ',@sStrTmp2)+1,1) 
							BEGIN
							   SELECT @sStrTmp1='', @sStrTmp='', @sStrTmp2=''
							   GOTO GetNextWord
							END					
							--2022-03-17 add fix IRÁN AIRCRAFT MANUFACTURING INDUSTRIES, End

							--2022-03-17 change --2021-12-23 change, Start
							if ABS(len(@sstrTmp2) - LEN(@smatchName) ) < 15	
							begin	
							   --2022-03-22 FIX 'ZA.U TE':Prvious Char not special char,Start
							   SELECT @iLenC1=CHARINDEX(@sStr,@ssSearchName)
							   if @iLenC1 > 0 AND PATINDEX('%[A-Za-z0-9]%', SUBSTRING(@ssSearchName,@iLenC1-1,1) ) >0
							   begin
							      SELECT @sStrTmp1='', @sStrTmp='' , @sStrTmp2=''
								  GOTO GetNextWord
							   end
							   else
							       goto EndEnd 
							end
							else 
							   goto EndProc
                            --2022-03-17 change  --2021-12-23 change, End
						end
					end

                    else
					begin	
											     
						 --2022-01-20 chagne for firstword, Start
						 --SELECT '!!!-GetMatchedText2-1-1: ' Mark, @sStrTmp2 TransactionText, @sstr FirstName, @sStr1 LastName, @ssSearchName SearchText, @ilen1 l1, @iLen2 l2, @iCnt icnt,@iLenC1 ic1, @iLenC2 ic2, @iLenC3 ic3
				
						 --2022-03-16 add fix KING HUNG INVESTMENT CO., LTD. ,Start
						 if ISNULL(@sStrTmp1,'')='' OR ISNULL(@sStrTmp2,'')=''
						    GOTO GetNextWord
                         --2022-03-16 add fix KING HUNG INVESTMENT CO., LTD. ,End

						 --2022-03-11 add fix only lastword match, Start
						 if @iLen2 < @iLen1 and CHARINDEX(@sStr1, @sMatchName) > 0 AND CHARINDEX(@sStr1, @ssSearchName)> 0
						 BEGIN
						    SELECT @sStrTmp1=@sStr1
							GOTO EndProc
						 END
						 --2022-03-11 add fix only lastword match, End
						 						
						 --2022-02-09 add for only find firstword, and firstword not in Partyname, Start
						 --for fix 'EL 1'
						 if CHARINDEX(@sStr,@ssSearchName) > 0 and CHARINDEX(@sStr,@sMatchName) =0 and @iLen1 > @iLen2 --2022-02-18 mask--and @ilen1 < @iCnt
						    SELECT @iLen2=@iCnt, @sStrTmp2=SUBSTRING(@ssSearchName, @iLen1, @iLen1-@iLen2+1)           --2022-02-18 change fix 'XINJIANG DAQO NEW ENERGY CO., LTD.'
						 --2022-02-09 add for only find firstword, and firstword not in Partyname,  End
						
						 --SELECT '!!!-GetMatchedText2-1-1-1: ' Mark, @sStrTmp2 TransactionText, @sstr FirstName, @sStr1 LastName, @ssSearchName SearchText, @ilen1 l1, @iLen2 l2, @iCnt icnt,@iLenC1 ic1, @iLenC2 ic2, @iLenC3 ic3

						 SELECT @iLenC1=CHARINDEX('/'+SUBSTRING(@sStr,1,1) , @ssSearchName)+1,  @iLenC2=CHARINDEX('|'+SUBSTRING(@sStr,1,1) , @ssSearchName)+1
						 if @iLenC1 >1 and @iLenC2=1
						    SELECT @iLenC3=CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @ssSearchName, @iLenC1+1)+1
						 else if @iLenC1=1 and @iLenC2>1
						    SELECT @iLenC3=CHARINDEX(' '+SUBSTRING(@sStr1,1,1) , @ssSearchName, @iLenC2+1)+1
						
						 --2022-02-09 CHANGE FOR DR.A, Start
						 else if @iLenC1=1 and @iLenC2=1
						      SELECT @iLenC3=0

						 if @iLenC3 =1
						    SELECT @iLenC3=CHARINDEX('|'+SUBSTRING(@sStr1,1,1) , @ssSearchName, @iLenC2+1) + 1
                         IF @iLenC3 =1
						    SELECT @iLenC3=CHARINDEX(SUBSTRING(@sStr1,1,1) , @ssSearchName, @iLenC2+1), @iLenC1=1
				         --2022-02-09 CHANGE FOR DR.A, End
				
						 if @iLenC1 =1 and @iLenC2 >1 and @iLenC3 >0 AND @iLenC3 > @iLenC2       --2022-02-09 change 
						    SELECT @sStrTmp2=SUBSTRING(@ssSearchName, @iLenC2, @iLenC3 - @iLenC2 +1+len(@sStr1)-1)
					     else if  @iLenC1 >1 and @iLenC2=1 and @iLenC3 >0 AND @iLenC3 > @iLenC1  --2022-02-09 change 
						    SELECT @sStrTmp2=SUBSTRING(@ssSearchName, @iLenC1, @iLenC3 -@iLenC1+1)
                         --2022-01-20 chagne for firstword, End
                         
			             else if @iLen1 > @iLen2   --2022-01-13 add, Start
						 begin
						    --SELECT '!!!-GetMatchedText2-1-1-1: ' Mark, @sStrTmp2 TransactionText, @sstr FirstName, @sStr1 LastName, @ssSearchName SearchText, @ilen1 l1, @iLen2 l2, @iCnt icnt,@iLenC1 ic1, @iLenC2 ic2, @iLenC3 ic3, CHARINDEX(@sStr1 , @ssSearchName, @iLen1+1) iLast

						    if CHARINDEX(@sStr1 , @ssSearchName, @iLen1+1) = 0     --2022-01-20 change
							begin
								SELECT @ilenC2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1) , @ssSearchName, @iLenC1+1)+1, @ilenC3=CHARINDEX(' ' , @ssSearchName, @iLenC2+1)
							  
								if @ilenC2 > 1 and @iLenC3 > @ilen1   --2022-03-09 CHANGE
								   SELECT @iLen2=@iLenC2,  @sStrTmp2=SUBSTRING(@ssSearchName, @iLenC1,@iLenC3-@ilenC2+1)
								
								--2022-01-20 add for reget poistion of last word, Start
                                if @ilenC2 > 1 and @iLenC2 < @ilen1
								   SELECT @ilenC2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1) , @ssSearchName, @iLen1+1)+1, @ilenC3=CHARINDEX(' ' , @ssSearchName, @iLenC2+1)
                                if @ilenC2 > 1 and @iLenC2 < @ilen1
								   SELECT @ilenC2=CHARINDEX('|'+SUBSTRING(@sStr1,1,1), @ssSearchName, @iLen1+1)+1, @ilenC3=CHARINDEX(' ' , @ssSearchName, @iLenC2+1)
                                if @ilenC2 > 1 and @iLenC2 < @ilen1
								   SELECT @ilenC2=CHARINDEX('/'+SUBSTRING(@sStr1,1,1) , @ssSearchName, @iLen1+1)+1, @ilenC3=CHARINDEX(' ' , @ssSearchName, @iLenC2+1)
								if @iLen2 > 1 and @iLenC2 > @iLen1 and @iLenC3 > @iLen1     --2022-02-14 change fix substring err
								   SELECT @iLen2=@iLenC2,  @sStrTmp2=SUBSTRING(@ssSearchName, @iLen1,@ilenC3-@iLen1+1)
								--2022-01-20 add for reget poistion of last word, End

								else 
								   SELECT @ilen2=Len(@ssSearchName)

								--SELECT @sStrTmp2=SUBSTRING(@ssSearchName, @iLenC2, @ilenC3-@iLenC2+1)  --2022-01-20  mask
								--2022-01-20 change, Start
								if @iLenC3 > @iLen1
								   SELECT @sStrTmp2=SUBSTRING(@ssSearchName, @iLen1, @iLenC3-@iLen1+1)   
                                else if  @iLen2 > @iLen1   --2022-02-14 change fix substring err
								    SELECT @sStrTmp2=SUBSTRING(@ssSearchName, @iLen1, @iLen2-@iLen1+1) 
								--2022-01-20 change, End

                            end

							else if  CHARINDEX(' '+@sStr1 , @ssSearchName) > 0 or CHARINDEX('|'+@sStr1 , @ssSearchName) > 0
							begin
							    --SELECT '!!!-GetMatchedText2-1-1-2: ' Mark, @sStrTmp2 TransactionText, @sstr FirstName, @sStr1 LastName, @ssSearchName SearchText, @ilen1 l1, @iLen2 l2, CHARINDEX(' '+@sStr1 , @ssSearchName) ilast1,CHARINDEX('|'+@sStr1 , @ssSearchName) ilast2
							    --2022-02-09 add for DR.A ,Start
								if  CHARINDEX(' '+@sStr1 , @ssSearchName) > 0
								    SELECT @iLen2=CHARINDEX(' '+@sStr1 , @ssSearchName)+1, @iCnt=CHARINDEX(' ',@ssSearchName, @iLen2+1), @iLen1=@iLen2
                                else if CHARINDEX('|'+@sStr1 , @ssSearchName) > 0
								    SELECT @iLen2=CHARINDEX(' '+@sStr1 , @ssSearchName)+1, @iCnt=CHARINDEX(' ',@ssSearchName, @iLen2+1), @iLen1=@iLen2
                                
								SELECT  @sStrTmp2=SUBSTRING(@ssSearchName, @iLen2, len(@sStr1)), @sStrTmp1=@sStrTmp2
								
								--SELECT '!!!-GetMatchedText2-1-1-3: ' Mark, @sStrTmp2 TransactionText, @sstr FirstName, @sStr1 LastName, @ssSearchName SearchText, @ilen1 l1, @iLen2 l2,@iCnt l3, SUBSTRING(@ssSearchName,@ilen2,10) st, DIFFERENCE(@sStrTmp2,@ssmatchName) idiff
							    if ISNULL(@sStrTmp1,'') <> '' and @iLen1=@iLen2
								    goto EndEnd
								
								--GET PARTY NAME SECOND WORD
								if @ilen1 <> @ilen2 
								Begin
								--2022-02-09 add for DR.A ,End

									SELECT @iCntSp1=CHARINDEX(' ',@sMatchName)+1,  @iCntSp2=CHARINDEX(' ',@sMatchName,CHARINDEX(' ',@sMatchName))  -- Second word end position
									if @iCntSp1=0 and @iCntSp2=0
									   SELECT @iCntSp1=CHARINDEX('|',@sMatchName)+1,  @iCntSp2=CHARINDEX('|',@sMatchName,CHARINDEX('|',@sMatchName))  -- Second word end position
									else if @iCntSp1=0 and @iCntSp2>0
										SELECT @iCntSp1=CHARINDEX('|',@sMatchName)+1,  @iCntSp2=CHARINDEX('|',@sMatchName,CHARINDEX(' ',@sMatchName))  -- Second word end position
									else if @iCntSp1>0 and @iCntSp2>0
										SELECT @iCntSp1=CHARINDEX(' ',@sMatchName)+1,  @iCntSp2=CHARINDEX(' ',@sMatchName,CHARINDEX(' ',@sMatchName))  -- Second word end position
									else
										SELECT @iCntSp1=CHARINDEX(' ',@sMatchName)+1,  @iCntSp2=len(@sMatchName)

									SELECT @sPartyNa1=SUBSTRING(@sMatchName, @iCntSp1, Len(@sMatchName)- @iCntSp1-Len(@sStr1)+1)
									if CHARINDEX(@sPartyNa1, @ssSearchName) > 0
									   SELECT @iLen1=CHARINDEX(@sPartyNa1, @ssSearchName) 
									else if CHARINDEX(' '+ SUBSTRING(@sPartyNa1,1,1),  @ssSearchName) > 0
										SELECT @iLen1=CHARINDEX(' '+ SUBSTRING(@sPartyNa1,1,1),  @ssSearchName) + 1 -- +1 : space issue
									else if CHARINDEX('|'+ SUBSTRING(@sPartyNa1,1,1),  @ssSearchName) > 0
										SELECT @iLen1=CHARINDEX('|'+ SUBSTRING(@sPartyNa1,1,1),  @ssSearchName) + 1 -- +1 : | issue
									else if CHARINDEX('/'+ SUBSTRING(@sPartyNa1,1,1),  @ssSearchName) > 0
										SELECT @iLen1=CHARINDEX('/'+ SUBSTRING(@sPartyNa1,1,1),  @ssSearchName) + 1 -- +1 : / issue

									if @iLen2 > @iLen1
									   SELECT @sStrTmp2=SUBSTRING(@ssSearchName, @iLen1, @iLen2-@iLen1+Len(@sStr1))
                              End   --2022-02-09 add
							end	

							--20222-01-20 CHANG, Start
							IF @iLen1 >= @iLen2 
							begin
								IF CHARINDEX('|1/', @ssSearchName) > 0
								begin
									SELECT @iLenC2=CHARINDEX('|1/', @ssSearchName)+4,@iLenC3=CHARINDEX('|', @ssSearchName,@iLenC2+1)
									if @iLenC3 >0 and @iLenC2 >4  and @iLenC3 > @iLenC2     --2022-02-14 change fix substring err
									   SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@iLenC2, @iLenC3-@iLenC2+1), @iLen1=@iLenC2, @iLen2=@iLenC3
								end
								if CHARINDEX('|/', @ssSearchName) > 0
								begin
								    SELECT @iLenC2=CHARINDEX('|/', @ssSearchName)+3, @iLenC3=CHARINDEX('|', @ssSearchName,@iLenC2+1)
									if @iLenC3>0 and @iLenC2 >3  and @iLenC3 > @iLenC2    --2022-02-14 change fix substring err
									   SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@iLenC2, @iLenC3-@iLenC2+1), @iLen1=@iLenC2, @iLen2=@iLenC3
                                end
                                if CHARINDEX('|', @ssSearchName) > 0
								begin
								    SELECT @iLenC2=CHARINDEX('|', @ssSearchName)+2, @iLenC3=CHARINDEX('|', @ssSearchName,@iLenC2+1)
									if @iLenC2>2 and @iLenC3> 0 and @iLenC3 > @iLenC2    --2022-02-14 change fix substring err
									   SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@iLenC2, @iLenC3-@iLenC2+1), @iLen1=@iLenC2, @iLen2=@iLenC3
                                end
								if CHARINDEX(SUBSTRING(@sStr,1,1), @ssSearchName) > 0
								begin
								    SELECT @iLenC2=CHARINDEX(SUBSTRING(@sStr,1,1), @ssSearchName), @iLenC1=CHARINDEX(' ', @ssSearchName, @iLenC2+1), @iLenC3=CHARINDEX('|', @ssSearchName,@iLenC2+1)
									if @ilenC2 >0 and @iLenC3 >0  and @iLenC3 > @iLenC2    --2022-02-14 change fix substring err
								       SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@iLenC2, @iLenC3-@iLenC2+1), @iLen1=@iLenC2, @iLen2=@iLenC3
									if @iLenC1 >0 and @iLenC2 > 0  and @iLenC1 > @iLenC2   --2022-02-14 change fix substring err
                                       SELECT @sPartyNa1=SUBSTRING(@ssSearchName,@iLenC2, @iLenC1-@iLenC2+1)
									if Len(@sPartyNa1)<=2  and @iLenC3 > @iLenC1           --2022-02-14 change fix substring err
									   SELECT  @sStrTmp2=SUBSTRING(@ssSearchName,@iLenC1, @iLenC3-@iLenC1+1), @iLen1=@iLenC1
								end
								
								else
								begin
								    SELECT @iLenC2=CHARINDEX('|', @ssSearchName)+2, @iLenC3=len(@ssSearchName)
									if @iLenC2 > 0 and @iLenC3 > 0  and @iLenC3 > @iLenC2    --2022-02-14 change fix substring err
									   SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@iLenC2, @iLenC3-@iLenC2+1), @iLen1=@iLenC2, @iLen2=@iLenC3
                                end
							    --SELECT '!!!-GetMatchedText2-1-1-2: ' Mark, @sStrTmp2 TransactionText, @sstr FirstName, @sStr1 LastName, @ssSearchName SearchText, @ilen1 l1, @iLen2 l2, @iCnt icnt,@iLenC1 ic1, @iLenC2 ic2, @iLenC3 ic3, CHARINDEX(@sStr1 , @ssSearchName, @iLen1+1) iLast
							
							end
							--20222-01-20 CHANG, end
							
							--SELECT '!!!-GetMatchedText2-1-2: ' Mark, @sStrTmp2 TransactionText, @sstr FirstName, @sStr1 LastName, @ssSearchName SearchText, @ilen1 l1, @iLen2 l2, @iCnt icnt,@iLenC1 ic1, @iLenC2 ic2, @iLenC3 ic3

						 end  --2022-01-13 add, End

                         else
						    SELECT @sStrTmp2=@ssSearchName

						 --2022-01-20 add for poistion of LastWord Less than FirstWord, Start
						 if @ilen1 > @iLen2 and PATINDEX('%[-@#$(/+"]%',@sStr) = 0 
						    if CHARINDEX(@sStrTmp2, @ssSearchName) = 0
						       GOTO RegetLastWordPoistion
						 --2022-01-20 add for poistion of LastWord Less than FirstWord, End

						 --2022-01-17 add for bug fix party name special '- ', Start
						 if CHARINDEX(@sStr,@ssSearchName)=0 and  Len(@sStr)=1 and PATINDEX('%[-@#$(/+"]%',@sStr) > 0
							if CHARINDEX(@sStr1,@ssSearchName) > 0
							   SELECT @ilen2=CHARINDEX(@sStr1,@ssSearchName), @sStrTmp2=SUBSTRING(@ssSearchName, @iLen2, Len(@sStr1))
                         --2022-01-17 add for bug fix party name special '- ', End					     
					    
						 --SELECT '!!!-GetMatchedText2: ' Mark, @sStrTmp1 TransactionText, @sstr FirstName, @sStr1 LastName, @ssSearchName SearchText, @ilen1 l1, @iLen2 l2, @iCnt icnt
			
						 if CHARINDEX(@sStrTmp2, @ssSearchName) >0  or DIFFERENCE(@sStrTmp2, @sMatchName) >0
						 begin
						    
						    --SELECT '!!!-GetMatchedText2-a: ' Mark, @sStrTmp2 TransactionText, @sstr FirstName, @sStr1 LastName, @iLenC1 l1, @iLenC2 l2, @iLenC3 l3				        
							--select @sStrTmp1=REPLACE(@sStrTmp2,'|',@sNewLine)
							SELECT @sStrTmp1=@sStrTmp2
							goto EndProc
						end
			
				     end
	
					 --2021-12-17 Change, End			
					 		   
                  --SELECT '!!!-GetMatchedText2-b: ' Mark, @sStrTmp2 matchName,@ilen1 l1, @iLen2 l2, @iCnt icnt
				
				  --2021-12-22 add for @sStrTmp1 is null, For Partial Match Start
				  if ISNULL(@sStrTmp1,'')='' or  CHARINDEX(@sStr,@ssSearchName)=0 or CHARINDEX(@sStr1,@ssSearchName) = 0 
				  BEGIN
				      --SELECT '!!!-GetMatchedText:' Mark,@sStr FirstName, @sStr1 LastName, CHARINDEX(@sStr,@ssSearchName) C1,  CHARINDEX(@sStr1,@ssSearchName) C2

					  --1.FirstName @sStr, Start Poistion
					  if CHARINDEX(@sStr,@ssSearchName) = 0
					  Begin
						 SELECT @iLen1=CHARINDEX(' ' + substring(@sStr,1,2), @ssSearchName) + 1			  
					  if @iLen1=1
						 SELECT @iLen1=CHARINDEX(' ' +substring(@sStr,1,1), @ssSearchName) + 1
					 if @iLen1=1 
					     SELECT @iLen1=CHARINDEX('|' + substring(@sStr,1,2), @ssSearchName) + 1			  
					  if @iLen1=1
						 SELECT @iLen1=CHARINDEX('|' +substring(@sStr,1,1), @ssSearchName) + 1
					 --2022-01-20 add for / + firstword, Start
					 if @iLen1=1 
					     SELECT @iLen1=CHARINDEX('/' + substring(@sStr,1,2), @ssSearchName) + 1			  
					 if @iLen1=1
						 SELECT @iLen1=CHARINDEX('/' +substring(@sStr,1,1), @ssSearchName) + 1
                     --2022-01-20 add for / + firstword, End
					 if @iLen1=1
						 SELECT @iLen1=CHARINDEX(substring(@sStr,1,3), @ssSearchName)
					  if @iLen1=0
						 SELECT @iLen1=CHARINDEX(substring(@sStr,1,2), @ssSearchName)
					  if @iLen1=0
					      SELECT @iLen1=Len(@ssSearchName)

                      --1.1 FirstName @sStr, End Poistion
					  --SELECT '!!!-GetMatchedText0:' Mark, @ssSearchName SearchText,@iLen1 L1
					 
					  SELECT @iCnt=CHARINDEX(' ' , @ssSearchName, @ilen1+1)
					  if @iCnt=0
					     SELECT @iCnt=CHARINDEX('|' , @ssSearchName, @ilen1+1)
					  if @iCnt=0
					     SELECT @iCnt=Len(@ssSearchName)
					  if @iCnt > @iLen1     --2022-02-14 change fix substring err
					     SELECT @sStr=SUBSTRING(@ssSearchName, @ilen1, @icnt-@iLen1)

					  --SELECT '!!!-GetMatchedText0:' Mark, @iLen1 L1, @iCnt L2, SUBSTRING(@ssSearchName, @ilen1, @icnt-@iLen1) Firstname
					  End

					--2.LastName @sStr1, Start Poistion
					if CHARINDEX(@sStr1,@ssSearchName) = 0
					Begin
						SELECT @iLen2=CHARINDEX(' ' + substring(@sStr1,1,3), @ssSearchName,@ilen1+1) + 1			  
					if @iLen2=1
						SELECT @iLen2=CHARINDEX(' ' + substring(@sStr1,1,1), @ssSearchName,@iLen1+1) + 1
					if @iLen2=1
					   SELECT @iLen2=CHARINDEX('|' + substring(@sStr1,1,3), @ssSearchName,@ilen1+1) + 1			  
					if @iLen2=1
						SELECT @iLen2=CHARINDEX('|' + substring(@sStr1,1,1), @ssSearchName,@iLen1+1) + 1
					--2022-01-20 add for / + firstname , Start
				   	if @iLen2=1
					   SELECT @iLen2=CHARINDEX('/' + substring(@sStr1,1,3), @ssSearchName,@ilen1+1) + 1			  
					if @iLen2=1
						SELECT @iLen2=CHARINDEX('/' + substring(@sStr1,1,1), @ssSearchName,@iLen1+1) + 1
					--2022-01-20 add for / + firstname , End
					if @iLen2=1
						SELECT @iLen2=CHARINDEX(substring(@sStr1,1,3), @ssSearchName,@ilen1+1)
					if @iLen2=0
						SELECT @iLen2=CHARINDEX(substring(@sStr1,1,2), @ssSearchName, @ilen1+1)
                    if @iLen2=0
					    SELECT @iLen2=Len(@ssSearchName)

                    --2.1 LastName @sStr1, End Poistion
					--SELECT '!!!-GetMatchedText0:' Mark, @ssSearchName SearchText,@iLen2 L2
					 
					SELECT @iCnt=CHARINDEX(' ' , @ssSearchName, @iLen2+1)
					if @iCnt=0
					   SELECT @iCnt=CHARINDEX('|' , @ssSearchName, @iLen2+1)
					--2022-01-20 add for /, start
				    if @iCnt=0
					   SELECT @iCnt=CHARINDEX('/' , @ssSearchName, @iLen2+1)
					--2022-01-20 add for /, End
					if @iCnt=0
					    SELECT @iCnt=Len(@ssSearchName)

					if @iCnt > @iLen2    --2022-02-14 change fix substring err
                       SELECT @sStr1=SUBSTRING(@ssSearchName, @iLen2, @icnt-@iLen2)

					--SELECT '!!!-GetMatchedText0:' Mark, @iLen2 L1, @iCnt L2, @sStr1 Lastname

					end

                      if @ilen2>@ilen1
                         SELECT @sStrTmp1=SUBSTRING(@ssSearchName,@ilen1, @ilen2-@ilen1+Len(@sStr1))
                      else
					     SELECT @sStrTmp1=@sStr

                     -- SELECT '!!!-GetMatchedText2: ' Mark, @sStrTmp1 TransactionText, @sstr FirstName, @sStr1 LastName
				  END
				  --2021-12-22 add for @sStrTmp1 is null, For Partial Match End

GetMatchedText1:
				  if @iProcess=1
				     goto GetProcess1
                  
				  if @iProcess=2 
				     goto GetProcess2


EndEnd:
				--SELECT '!!!EndEnd:' mark, @sStrTmp1 mText,@sMatchName mName, DIFFERENCE(@sStrTmp1, @sMatchName) idif, LEN(@sStrTmp1) ilenTmp1, LEN(@sMatchName) iLenMatch, PATINDEX('%[|/]%',@sStrTmp1) pi, @ssMatchName ss, @sStr first, @sStr1 last

			    --2022-03-27 add fix 'B.O.',  'A&M MAHAR CO LTD ', Start
			    SELECT @sStr=LTRIM(RTRIM(@sStr)), @sStr1=LTRIM(RTRIM(@sStr1))
				SELECT @sStrTmp1=Upper(LTRIM(RTRIM(@sStrTmp1))), @sMatchName=LTRIM(LTRIM(@sMatchName))

			    IF @sStr=@sStr1 AND CHARINDEX(@sStr,@sMatchName)>0 AND CHARINDEX(@sStrTmp1,@sMatchName) > 0 AND DIFFERENCE(@sMatchName, @sStrTmp1)=4 AND ABS(LEN(@sMatchName)-LEN(@sStrTmp1)) < 5
				   GOTO RetEnd
                
				--2022-03-27 fix 'AL -'
                IF @sStrTmp1=@sMatchName   
				    GOTO RetEnd					
			  
				--2022-04-02 fix process more matchtext, Start
				IF DIFFERENCE(@sStrTmp1, @sMatchName)=4 AND PATINDEX('%[|]%',@sStrTmp1) > 0  --AND LEN(@sStrTmp1) > LEN(@sMatchName) 
				Begin
				   --get first word 
				   SELECT @iLenC1=CHARINDEX(' ',@sMatchName)+1
				   if @iLenC1 > 0
				      SELECT @sPartyNa1=SUBSTRING(@sMatchName,1,@iLenC1)
				   else
				      SELECT @sPartyNa1=@sMatchName
                   --Check the same
				   if CHARINDEX(@sPartyNa1, @sStrTmp1) >0
				   begin
				      SELECT @iLenC2=CHARINDEX(@sPartyNa1, @sStrTmp1) 
					  SELECT @iLenC1=PATINDEX('%[|]%',@sStrTmp1)-1
					  if @iLenC2 > @iLenC1
					     SELECT @sStrTmp=RTRIM(LTRIM(SUBSTRING(@sStrTmp1,@iLenC2, LEN(@sStrTmp1)-@iLenC2+1))), @sStrTmp1=@sStrTmp
				      else  if @iLenC1 > @iLenC2
					     SELECT @sStrTmp=RTRIM(LTRIM(SUBSTRING(@sStrTmp1, @iLenC2,@iLenC1-@iLenC2+1))), @sStrTmp1=@sStrTmp
				   end

				   else if CHARINDEX(@sPartyNa1, @sStrTmp1)=0
				   begin
					   SELECT @iLenC1=PATINDEX('%[|]%',@sStrTmp1)-1
					   SELECT @sStrTmp=RTRIM(LTRIM(SUBSTRING(@sStrTmp1,1, @iLenC1)))
					   SELECT @sStrTmp1=@sStrTmp
				   end
				End

				IF DIFFERENCE(@sStrTmp1, @sMatchName)=4 AND CHARINDEX(@sStr,@sStrTmp1)>0 AND CHARINDEX(@sStr1,@sStrTmp1)=0 AND CHARINDEX(@sStr1,@ssSearchName)>0
				Begin
					 SELECT @iLenC1=CHARINDEX(@sStr,@sStrTmp1)
					  if @iLenC1>0 AND PATINDEX('%[0-9A-Za-z]%',SUBSTRING(@sStrTmp1,@iLenC1+LEN(@sStr)+1,1)) > 0 
		              begin
					     SELECT @iLenC1=CHARINDEX(@sStr1,@sStrTmp1)
						 if @iLenC1>0 AND PATINDEX('%[0-9A-Za-z]%',SUBSTRING(@sStrTmp1,@iLenC1+LEN(@sStr1)+1,1)) > 0 
					        SELECT @sStrTmp1=@sStr1
					  end
				End

				----2022-04-02 fix process more matchtext, End

				--2022-03-28 fix too long match text, Start
				--if ( CHARINDEX(' |', @sStrTmp1) > 0  AND (CHARINDEX('/', @sStrTmp1) > 0 AND ISNUMERIC(SUBSTRING(@sStrTmp,CHARINDEX('/', @sStrTmp1)-1,1)) > 0 ) ) --AND
				--Select '!!!EndEnd-10-4-0:' mark, @iMatchFlag i,@sStrTmp1 MatchText, DIFFERENCE(@sStrTmp1,@sMatchname) idif, ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs
				IF CHARINDEX(' |', @sStrTmp1) > 0  OR (PATINDEX('%[/]%', @sStrTmp1) > 0 AND ISNUMERIC(SUBSTRING(@sStrTmp,PATINDEX('%[/]%',@sStrTmp1)-1,1)) > 0  )
				Begin		
				    IF CHARINDEX(' |', @sStrTmp1) > 0  		    
				       SELECT @sStrTmp=SUBSTRING(@sStrTmp1,1,CHARINDEX(' |', @sStrTmp1)-1), @sStrTmp1=@sStrTmp
					ELSE IF PATINDEX('%[/]%', @sStrTmp1) > 0 AND ISNUMERIC(SUBSTRING(@sStrTmp,PATINDEX('%[/]%',@sStrTmp1)-1,1)) > 0
					   SELECT @sStrTmp=SUBSTRING(@sStrTmp1,1,CHARINDEX('/', @sStrTmp1)-1), @sStrTmp1=@sStrTmp
					--Select '!!!EndEnd-10-4-0:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sPartyNa1 s1, CHARINDEX(@sPartyNa1,@ssSearchName) iChIndex, @iCntSp1 iSp1, @iCntSp2 iSp2
				    SELECT @iMatchFlag=1
				End
				--2022-03-28 fix too long match text, End

                --2022-03-28 FIX 'A&M MAHAR CO.,LTD.' OF CO, COMPAY, LTD, LIMITED, START
 				IF CHARINDEX(' COMPANY',@sStrTmp1) > 0  AND CHARINDEX(' COMPANY',@sMatchName) = 0  AND  CHARINDEX(' CO',@sMatchName) > 0
				    SELECT @sStrTmp=REPLACE(@sMatchName,' CO',' COMPANY'), @sMatchName=@sStrTmp
				IF CHARINDEX(' CO.',@sStrTmp1) > 0  AND  CHARINDEX(' CO.',@sMatchName) =0 AND  CHARINDEX(' COMPANY',@sMatchName) > 0
				    SELECT @sStrTmp=REPLACE(@sMatchName,' COMPANY',' CO.'), @sMatchName=@sStrTmp
				IF CHARINDEX(' CO',@sStrTmp1) > 0  AND CHARINDEX(' COMPANY',@sStrTmp1) = 0 AND CHARINDEX(' COMPANY',@sMatchName) > 0
				    SELECT @sStrTmp=REPLACE(@sMatchName,' COMPANY',' CO'), @sMatchName=@sStrTmp
				IF CHARINDEX('LIMITED',@sStrTmp1) > 0  AND  CHARINDEX('LIMITED',@sMatchName) = 0  AND  CHARINDEX('LTD',@sMatchName) > 0
				    SELECT @sStrTmp=REPLACE(@sMatchName,'LTD','LIMITED'), @sMatchName=@sStrTmp
				IF CHARINDEX('LTD',@sStrTmp1) > 0 AND  CHARINDEX('LTD',@sMatchName) = 0  AND  CHARINDEX('LIMITED',@sMatchName) > 0
				    SELECT @sStrTmp=REPLACE(@sMatchName,'LIMITED','LTD'), @sMatchName=@sStrTmp
				IF CHARINDEX('LTD',@sStrTmp1) > 0 AND  CHARINDEX('LTD',@sMatchName) = 0  AND  CHARINDEX('INC',@sMatchName) > 0
				    SELECT @sStrTmp=REPLACE(@sMatchName,'INC','LTD'), @sMatchName=@sStrTmp

			    --SELECT '!!!EndEnd-10-4-0-6:' mark, @iLenC1 ic1,@iLenC2 ic2, @iLenC3 ic3,  @sStrTmp1 smtext, @sMatchName smatchName, CHARINDEX('CO',@sMatchName) ico, CHARINDEX('COMPANY',@ssMatchName) icompany, CHARINDEX('COMPANY',@sStrTmp1) istmp,ABS(Len(@sStrTmp1)-LEN(@sMatchName)) iabs, DIFFERENCE(@sStrTmp1, @sMatchName) idif
				--2022-03-28 FIX 'A&M MAHAR CO.,LTD.' OF CO, COMPAY, LTD, LIMITED, END

			    --2022-03-29 fix 'LI YI', firstword not reasonable word, ReGet Last word, Start
                --firtword first char & lastword first char is the same , and include newline in match text
				if CHARINDEX('|', @sStrTmp1)>0 AND ( CHARINDEX(SUBSTRING(@sStr,1,1),@sStrTmp1)=0 AND CHARINDEX(SUBSTRING(' '+@sStr1,1,1),@sStrTmp1)=0 )
				   SELECT @sStrTmp1='' 
				ELSE IF SUBSTRING(@sStrTmp1,1,1)='|'
				   SELECT @sStrTmp=SUBSTRING(@sStrTmp1,2,LEN(@sStrTmp1)-2+1), @sStrTmp1=@sStrTmp
				
				IF DIFFERENCE(@sStrTmp1, @sMatchName)=4  AND ( CHARINDEX(@sStr+' ', @sStrTmp1)> 0 OR CHARINDEX(@sStr1, @sStrTmp1) >0 )
				BEGIN
				   IF DIFFERENCE(@sStrTmp1, @sMatchName)=4 
				   Begin
				       --2022-04-02 fix fistword & lastword in match text 
				       IF CHARINDEX(@sStr, @sStrTmp1)> 0 AND CHARINDEX(@sStr1, @sStrTmp1) >0 
						  GOTO RetEnd2
				   
					   ELSE IF CHARINDEX(@sStr1, @sStrTmp1)=0 AND CHARINDEX(@sStr+' ', @sStrTmp1)=0 AND CHARINDEX(@sStr1, @ssSearchName)>0  --2022-04-07 CHANGE
                       Begin
					      SELECT @iLenC1=CHARINDEX(' '+@sStr1,@ssSearchName)  --2022-04-07 CHANGE
						  if @iLenC1>0 
						  begin
						     SELECT @iLenC2=CHARINDEX(' ',@ssSearchName, @iLenC1+1)
							 IF @iLenC2 > 0 AND @iLenC2 > @iLenC1
							 Begin
PreCharSearchNameCheck1:							 
							    SELECT @sStrTmp=SUBSTRING(@ssSearchName, @iLenC1, @iLenC2-@ilenC1+1), @sStrTmp1=@sStrTmp
								if PATINDEX('%[0-9A-Za-z]%',SUBSTRING(@ssSearchName,@iLenC1-1,1)) > 0 
							    begin
								  SELECT @iLenC1=@iLenC1 -1
								  if @iLenC1>1
								     goto PreCharSearchNameCheck1
								end	
								

								if CHARINDEX(@sStr1,@sStrTmp1)= 0
								   SELECT @sStrTmp1=' '
			
							 End	   
						  end
						  
					   End
				   End
				   --2022-04-02 fix firstword first char in match text and len match name less 5 char
				   ELSE IF SUBSTRING(@sStr,1,1)=SUBSTRING(@sStrTmp1,1,1) AND ABS(LEN(@sStrTMp1)-LEN(@sMatchName)) <=5
				      GOTO RetEnd
				END

			    --2022-04-07 fix matchname not one word, firstword wrong word,Start				 
                IF  CHARINDEX(' ',@sMatchName)>0 AND CHARINDEX(@sStr+' ',@sStrTmp1)=0 AND CHARINDEX(' '+@sStr+' ',@sStrTmp1)=0 AND
				    CHARINDEX(@sStr1,@ssSearchName)>0 AND CHARINDEX(' '+@sStr1,@ssSearchName)>0 AND 
					DIFFERENCE(@sStrTmp1,@sMatchName)=4 
				begin
				  --SELECT '!!!EndEnd-10-4-0-6x01:' mark, @sStrTmp1 mText
				  IF ( (SUBSTRING(@sStr,1,1)=SUBSTRING(@sStrTmp1,1,1)  AND CHARINDEX(@sStr1,@sStrTmp1,CHARINDEX(SUBSTRING(@sStr,1,1), @sStrTmp1)) >0 ) OR
				       (CHARINDEX(@sStr, @sStrTmp1) >0 AND CHARINDEX(SUBSTRING(@sStr1,1,1), @sStrTmp1, CHARINDEX(@sStr, @sStrTmp1) ) >0 ) OR
					   (SUBSTRING(@sStr,1,1)=SUBSTRING(@sStrTmp1,1,1)  AND CHARINDEX(SUBSTRING(@sStr1,1,1),@sStrTmp1,CHARINDEX(SUBSTRING(@sStr,1,1), @sStrTmp1)) >0 ) )					 
				  Begin
				     SELECT @iMatchFlag=1
					 GOTO  RetEnd2
                  End

				  ELSE
				  Begin
					SELECT @iLenC2=CHARINDEX(' '+@sStr1,@ssSearchName)
					if @iLenC2>0 AND CHARINDEX(' ',@ssSearchName, @iLenC2+1) > 0
					begin
						SELECT @ilenC3=CHARINDEX(' ',@ssSearchName, @iLenC2+1)
						if @ilenC3 >0 AND @ilEnc3 > @ilenC2
							SELECT @sStrTmp1=LTRIM(RTRIM(SUBSTRING(@ssSearchName,@iLenC2+1, @iLenC3-@iLenC2))), @iMatchFlag=1
					    --SELECT '!!!EndEnd-10-4-0-6x02:' mark, @sStrTmp1 mText
					end
				  End
				end	
				--2022-04-07 add fix matchname not one word, firstword wrong word,End					   			   
			    --SELECT '!!!EndEnd-10-4-0-6x1:' mark, @sStrTmp1 mText,CHARINDEX(' ',@sMatchName) ich2w, CHARINDEX(@sStr+' ',@sStrTmp1) ichFst1, CHARINDEX(' '+@sStr+' ',@sStrTmp1) ichFst2,CHARINDEX(@sStr1,@ssSearchName) ichLast1, CHARINDEX(' '+@sStr1,@ssSearchName) ichlast2, DIFFERENCE(@sStrTmp1,@sMatchName) idif
							

				--2022-03-25 add fix 'GUANGDONG PROVINCIAL CHINA TOBACCO INDUSTRY CO LTD' ,START
				--Select '!!!EndEnd-10-4-0-2:' mark,@sStrTmp1 MatchText, @iCnt icnt,DIFFERENCE(@sStrTmp1, @sMatchName) idiff, ABS(Len(@sMatchName)-Len(@sStrTmp1)) iabs,CHARINDEX(@sStrTmp1,@ssSearchName) ich1, CHARINDEX(@sStrTmp1,@sStr) ich2,SUBSTRING(@sStrTmp1,CHARINDEX(' ',@sStrTmp1)+1,1) sub1,SUBSTRING(@sMatchName,CHARINDEX(' ',@sMatchName)+1,1)  sub2
				--get new matchtext, difference of match name and match text, first char is the same
				IF CHARINDEX(@sStrTmp1,@ssSearchName) > 0  AND @iMatchFlag=0 AND   --2022-04-07 change
				   SUBSTRING(@sStrTmp1,CHARINDEX(' ',@sStrTmp1)+1,1) = SUBSTRING(@sMatchName,CHARINDEX(' ',@sMatchName)+1,1) 
				BEGIN   
				   SELECT @sStrTmp='',@sStrTmp2='', @iLenC1=1, @iLenC2=1, @iCnt=0
				   SELECT @iLenC3=@iLenC1
				   WHILE( @iLenC3 > 0 )
				   Begin
					   IF SUBSTRING(@sStrTmp1,@iLenC1,1) = SUBSTRING(@sMatchName,@iLenC2,1) 
					   begin
					       SELECT @sStrTmp=''
						   SELECT @iLenC3=CHARINDEX(' ',@sStrTmp1, @iLenC1+1), @iLenC2=CHARINDEX(' ',@sMatchName,@iLenC2+1) 		
						   IF @iLenC3 > LEN(@sStrTmp1) OR @iLenC3=0
							  SELECT @sStrTmp=SUBSTRING(@sStrTmp1,@iLenC1,LEN(@sStrTmp1)-@iLenC1+1), @iLenC3=0   
						   ELSE IF @iLenC3 > 0 AND @iLenC3 > @iLenC1
							  SELECT @sStrTmp=SUBSTRING(@sStrTmp1,@iLenC1,@iLenC3-@iLenC1+1)
		     		    end
						IF CHARINDEX(@sStrTmp,@sStrTmp2,@iLenC1) = 0
						   SELECT @sStrTmp2 = @sStrTmp2 + @sStrTmp
					    --SELECT '!!!EndEnd-10-4-0-3:' mark, @iLenC1 ic1,@iLenC2 ic2, @iLenC3 ic3,  ABS(Len(@sMatchName)-Len(@sStrTmp1)) iabs,CHARINDEX(@sStrTmp1,@ssSearchName) ich1, CHARINDEX(@sStrTmp1,@sStr) ich2,SUBSTRING(@sStrTmp1,CHARINDEX(' ',@sStrTmp1)+1,1) sub1,SUBSTRING(@sMatchName,CHARINDEX(' ',@sMatchName)+1,1)  sub2 ,@sStrTmp ss0, @sStrTmp2 ss1
					    SELECT @iLenC1=@iLenC3, @iCnt=@iCnt+1
						IF @iCnt > 30 
						   SELECT @iLenC3=0
					End

					SELECT @sStrTmp1=@sStrTmp2
				    GOTO RetEnd
				END

			    IF CHARINDEX(@sStrTmp1,@ssSearchName) > 0  AND
				   SUBSTRING(@sStrTmp1,CHARINDEX(' ',@sStrTmp1)+1,1) <> SUBSTRING(@sMatchName,CHARINDEX(' ',@sMatchName)+1,1) 
				   SELECT @sStrTmp=RTRIM(SUBSTRING(@sStrTmp1,1,CHARINDEX(' ',@sStrTmp1))), @sStrTmp1=@sStrTmp
				--Select '!!!EndEnd-10-4-0-4:' mark,@sStrTmp1 MatchText, @iCnt icnt,DIFFERENCE(@sStrTmp1, @sMatchName) idiff, ABS(Len(@sMatchName)-Len(@sStrTmp1)) iabs,CHARINDEX(@sStrTmp1,@ssSearchName) ich1, CHARINDEX(@sStrTmp1,@sStr) ich2,SUBSTRING(@sStrTmp1,CHARINDEX(' ',@sStrTmp1)+1,1) sub1,SUBSTRING(@sMatchName,CHARINDEX(' ',@sMatchName)+1,1)  sub2
				--2022-03-25 add fix 'GUANGDONG PROVINCIAL CHINA TOBACCO INDUSTRY CO LTD' ,END

                --2022-01-20 add for bug fix 'OBSHCHESTVO S OGRANICHENNOI OTVETSTVENNOSTUI PROMSERVIS18' between first and last word too many word,Start
			    if DIFFERENCE(@sStrTmp1, @sMatchName) = 4 --and ABS(LEN(@sStrTmp1)-LEN(@ssMatchName)) > 11
				Begin
				   --SELECT '!!!EndEnd-10-4-0-5:' mark, @sStrTmp1 mText,@ssMatchName mName, DIFFERENCE(@sStrTmp1, @sMatchName) c, LEN(@sStrTmp1) isss, LEN(@ssMatchName) immm, PATINDEX('%[|/]%',@sStrTmp1) pi
				   SELECT @iCnt=PATINDEX('%[/]%',@sStrTmp1)  --2022-01-20 change
				   if @iCnt > 0 and @iCnt <> CHARINDEX('/'+SUBSTRING(@sStr1,1,1),@sStrTmp1 )   --2022-02-16 change fix /+ lastword first chara
				   BEGIN  --2022-03-14 ADD FIX 'THE A/M', JUST GET 'THE', START
				      IF @iCnt > len(@sStr)+3   --2022-03-14 ADD
					     SELECT @sStrTmp=SUBSTRING(@sStrTmp1,1,@iCnt-1), @sStrTmp1=@sStrTmp
                      ELSE IF CHARINDEX(' ',@sStrTmp1) > 0  --2022-03-14 ADD
					     SELECT @iCnt=CHARINDEX(' ',@sStrTmp1)-1,@sStrTmp=SUBSTRING(@sStrTmp1,1,@iCnt), @sStrTmp1=@sStrTmp   --2022-03-14 ADD
				   --SELECT '!!!EndEnd-10-4-0-6:' mark, @sStrTmp1 mText,@ssMatchName mName, DIFFERENCE(@sStrTmp1, @sMatchName) c, LEN(@sStrTmp1) isss, LEN(@ssMatchName) immm, PATINDEX('%[|/]%',@sStrTmp1) pi, @iCnt ICNT, @sStrTmp SS
				   
				   END   --2022-03-14 ADD FIX 'THE A/M', JUST GET 'THE', END
				  
				   --2022-01-20 add for bug deleted last char is newline, Start
                   IF PATINDEX('%[|]%',@sStrTmp1) >0  AND SUBSTRING(@sStrTmp1,len(@sStrTmp1)-1,1)='|'
				       SELECT @sStrTmp=SUBSTRING(@sStrTmp1,1,len(@sStrTmp1)-2), @sStrTmp1=@sStrTmp
				   --2022-01-20 add for bug, End

				End
				--2022-01-20 add for bug fix 'OBSHCHESTVO S OGRANICHENNOI OTVETSTVENNOSTUI PROMSERVIS18' between first and last word too many word, End

                --2021-12-23 ADD FOR NULL ISSUE, Start
				if Len(@sMatchName) >0 and Len(@ssSearchName) >0 and isnull(@sStrTmp1,'')='' 
				   SELECT @sStrTmp1=' '   --SELECT @sStrTmp1=SUBSTRING(@ssMatchName,1,2)
				--2021-12-23 ADD FOR NULL ISSUE, End
               
			    --2021-12-23 add for first char, Start
				if Len(@sStrTmp1) > 2 --2022-01-17 add for fix leng=0
				if SUBSTRING(@sStrTmp1,1,1)=' ' or SUBSTRING(@sStrTmp1,1,1)='|'
				   SELECT @sStrTmp=SUBSTRING(@sStrTmp1,2,len(@sStrTmp1)-1), @sStrTmp1=LTRIM(@sStrTmp)
				--2021-12-23 add for first char, End
   
                --2022-02-09 add for return Account fix, Start
				if SUBSTRING(@sStrTmp1,1,1)='/' and ISNUMERIC(SUBSTRING(@sStrTmp1,2,len(@sStrTmp1)-1))>0
				   SELECT  @sStrTmp1=' '
				--2022-02-09 add for return Account fix, End

                --2022-02-09 add for return Account fix, Start
				if SUBSTRING(@sStrTmp1,1,1)='/' and CHARINDEX(' ',SUBSTRING(@sStrTmp1,2,len(@sStrTmp1)-1))=0 --ISNUMERIC(SUBSTRING(@sStrTmp1,2,len(@sStrTmp1)-1))>0
				   SELECT  @sStrTmp1=' '
				--2022-02-09 add for return Account fix, End

				--2022-02-09 add for return MatchedText fix firstWord is wrong word, Start
				if CHARINDEX(' ',@sStrTmp1) > 0	AND LEN(SUBSTRING(@sStrTmp1,1,CHARINDEX(' ',@sStrTmp1)-1))=1
				begin
				   --SELECT '!!!EndEnd-7-1:' mark, @sStrTmp1 mText,@ssMatchName mName, LEN(SUBSTRING(@sStrTmp1,1,CHARINDEX(' ',@sStrTmp1)-1)) lenc1, Len(SUBSTRING(@sMatchName,1,CHARINDEX(' ',@sMatchName)-1)) lenc2
				   if LEN(SUBSTRING(@sStrTmp1,1,CHARINDEX(' ',@sStrTmp1)-1)) <> Len(SUBSTRING(@sMatchName,1,CHARINDEX(' ',@sMatchName)-1))
				   begin   
					   --get string from space +1 start position
					   --SELECT  @sStrTmp1=SUBSTRING(@sStrTmp1,CHARINDEX(' ',@sStrTmp1)+1, LEN(@sStrTmp1)-CHARINDEX(' ',@sStrTmp1))
                       --reget firstword, Start position: 1)space +1 or 2)/ +1 or 3)|+1
					   SELECT @iCnt=CHARINDEX(@sStrTmp1, @ssSearchName), @iCntSp1=@iCnt
					   SELECT @sPartyNa2=SUBSTRING(@sStrTmp1,1,CHARINDEX(' ',@sStrTmp1))
					   
					   while( @iCntSp1>0 )
					   begin
					    
						 --SELECT '!!!EndEnd-7-2:' mark, @sStrTmp1 mText,@ssMatchName mName,@iCnt cnt, @iCntSp1 cnt1, @spartyna2 strh,@sPartyNa1 strstr
					     SELECT @iCntSp1=@iCntSp1-1
						 SELECT @sPartyNa1=SUBSTRING(@ssSearchName,@iCntSp1,1)
						 if @sPartyNa1= ' ' or @sPartyNa1= ',' or @sPartyNa1= '.' or @sPartyNa1= '/' or @sPartyNa1= '|' 		    
					        SELECT @sPartyNa1=SUBSTRING(@ssSearchName,@iCntSp1+1,@iCnt-@iCntSp1+1), @sStrTmp1=REPLACE(@sStrTmp1,@sPartyNa2,@sPartyNa1), @iCntSp1=0                       
					   end
				   end
                end
				--2022-02-09 add for return Account fix firstWord is wrong word, End
				
				--2022-02-09 add for final check len reasonable, Start
				if  ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) > 20  --AND DIFFERENCE(@sStrTmp1, @sMatchName) = 4
				Begin
					--for last name , first name reverse, Start				
					if CHARINDEX(',',@sMatchName) > 0  
					Begin
					   SELECT @iLen2=CHARINDEX(LTRIM(RTRIM(@sStr1)),@ssSearchName) , @iLen1=CHARINDEX(LTRIM(RTRIM(@sStr)),@ssSearchName)
					   if @ilen1 > @iLen2 and @iLen2 > 0  --2022-02-09 change old: if @ilen1 > @iLen2
					   BEGIN
					      SELECT @iCnt=@iLen2, @iLen1=@iLen2, @iLen2=CHARINDEX(' ',@ssSearchName,@iCnt+1)
						  SELECT @sStrTmp=SUBSTRING(@ssSearchName,@iLen1,@iCnt-1), @sStrTmp1=@sStrTmp
					   END
					End 				  
					    				   
				   SELECT @iCnt=CHARINDEX('|',@sStrTmp1)
				   if @iCnt > 0
                   --for too long for matchtext, Start
				   BEGIN 
				      SELECT @sStrTmp=SUBSTRING(@sStrTmp1,1,@iCnt-1), @sStrTmp1=@sStrTmp
					  
					  IF DIFFERENCE(@sStrTmp1,@sMatchName) <3 and ABS(LEN(@sStrTmp1)-Len(@sMatchName)) > 20
					     if CHARINDEX(' ',@sStrTmp1) > 0
					        SELECT @sStrTmp=SUBSTRING(@sStrTmp1,1,CHARINDEX(' ',@sStrTmp1)),  @sStrTmp1=@sStrTmp
				   --Select '!!!EndEnd-9:' mark,@sStrTmp1 MatchText, @sMatchName PartyName, @ssSearchName TransactionText, DIFFERENCE(@sStrTmp1, @sMatchName) idiff,ABS(LEN(@sStrTmp1)-Len(@sMatchName)) iabs, @iCnt icnt
				   END
				   --for too long for matchtext, End

				   --2022-02-14 add fix matchtext(fix LIMITED LIABILITY COMPANY, JOINT STOCK COMPANY) reget more 1 word, Start
				   SELECT @sStrTmp1=LTRIM(RTRIM(@sStrTmp1))
				   if Len(@ssSearchName) < Len(@sMatchName) And CHARINDEX(@sStrTmp1, @sMatchName) > 0
				   begin
				      SELECT @iCntSp1= CHARINDEX(@sStrTmp1, @sMatchName)
					  SELECT @iCntSp2=CHARINDEX(' ', @sMatchName, @iCntSp1+len(@sStrTmp1)+1)
					  
					  --Select '!!!EndEnd-9-0:' mark,@sStrTmp1 MatchText, @iCntSp1 i1, @iCntSp2 i2, @sStrTmp st, Len(@sStrTmp1) ilen,  CHARINDEX(@sStrTmp, @ssSearchName) ic, @iCnt cnt					  
					  if (@iCntSp2 - Len(@sStrTmp1)) = 1 and @iCntSp2 > 0
					     SELECT @iCnt=@iCntSp2, @iCntSp2=CHARINDEX(' ', @sMatchName, @iCnt+3)
 
 					  if @iCntSp2 > 0 and @iCntSp2 > @iCntSp1
					     SELECT @sStrTmp=LTRIM(RTRIM(SUBSTRING(@sMatchName, @iCntSp1, @iCntSp2-@iCntSp1+1)))
					  else
					     SELECT  @sStrTmp=LTRIM(RTRIM(SUBSTRING(@sMatchName, @iCntSp1, Len(@sMatchName)-@iCntSp1+1)))
					  if CHARINDEX(@sStrTmp, @ssSearchName) > 0 
					     SELECT @sStrTmp1=@sStrTmp
                      --Select '!!!EndEnd-9-1:' mark,@sStrTmp1 MatchText, @iCntSp1 i1, @iCntSp2 i2, @sStrTmp st, Len(@sStrTmp1) ilen,  CHARINDEX(@sStrTmp, @ssSearchName) ic, @iCnt cnt
				   end
				   --2022-02-14 add fix matchtext(fix LIMITED LIABILITY COMPANY, JOINT STOCK COMPANY) reget more 1 word, End
		
			    End
				--2022-02-09 add for final check len reasonable, End		
			
               	--2022-02-09 add fix account unreasonable matchtext, Start       
                SELECT @sStrTmp1=RTRIM(LTRIM(@sStrTmp1))   
				if SUBSTRING(@sStrTmp1,1,1)='|'      --delete | in heading
				   SELECT @sStrTmp=SUBSTRING(@sStrTmp1,2,len(@sStrTmp1)-1), @sStrTmp1=@sStrTmp

				if SUBSTRING(@sStrTmp1,1,1)='/' AND PATINDEX('%[.,- ]%',@sStrTmp1)=0   --is acccount
				   SELECT @sStrTmp=' ',@sStrTmp1=@sStrTmp
                else if SUBSTRING(@sStrTmp1,1,1)='/' AND  PATINDEX('%[.,- ]%',@sStrTmp1)>0  --not account
				   SELECT @sStrTmp=SUBSTRING(@sStrTmp1,2,len(@sStrTmp1)-1), @sStrTmp1=@sStrTmp
				--2022-02-09 add fix account unreasonable matchtext, End

                --2022-02-18 fix name:XINJIANG DAQO NEW ENERGY CO., LTD. , Start
				--Select '!!!EndEnd-9-1-x1:' mark,@sStrTmp1 MatchText, @iCnt cnt,DIFFERENCE(@sStrTmp1, @sMatchName) idiff, ABS(Len(@sMatchName)-Len(@sStrTmp1)) iabs, CHARINDEX(' |',@sStrTmp1) icharindex, CHARINDEX(@sStr,@sStrTmp1) isStr
				
				if DIFFERENCE(@sStrTmp1, @sMatchName)=4 AND ABS(Len(@sMatchName)-Len(@sStrTmp1)) >= 16 AND CHARINDEX(' |',@sStrTmp1) > 0			  		
			    Begin
				  SELECT @iCnt=CHARINDEX(' |',@sStrTmp1)
				  SELECT @sStrTmp=SUBSTRING(@sStrTmp1,1,@icnt), @sStrTmp1=@sStrTmp
				End
		
		--2022-03-25 add fix 'HONG KONG HONG PANG GEMS JEWELLERY CO LTD' ,START
		--Select '!!!EndEnd-9-1-x1-4:' mark,@sStrTmp1 MatchText, @iCnt icnt,DIFFERENCE(@sStrTmp1, @sMatchName) idiff, ABS(Len(@sMatchName)-Len(@sStrTmp1)) iabs,CHARINDEX(@sStrTmp1,@ssSearchName) ich1, CHARINDEX(@sStrTmp1,@sStr) ich2,SUBSTRING(@sStrTmp1,CHARINDEX(' ',@sStrTmp1)+1,1) sub1,SUBSTRING(@sMatchName,CHARINDEX(' ',@sMatchName)+1,1)  sub2
		IF CHARINDEX(@sStrTmp1,@ssSearchName) > 0  AND 
			 SUBSTRING(@sStrTmp1,CHARINDEX(' ',@sStrTmp1)+1,1) = SUBSTRING(@sMatchName,CHARINDEX(' ',@sMatchName)+1,1) 
		   GOTO RetEnd
		--2022-03-25 add fix 'HONG KONG HONG PANG GEMS JEWELLERY CO LTD' ,END

		--2023-03-24 fix 'BIN SONG': Recheck, Start
		SELECT @sStr=LTRIM(RTRIM(@sStr)), @sStr1=LTRIM(RTRIM(@sStr1))
		IF CHARINDEX(@sStrTmp1,@ssSearchName)>0 AND ( CHARINDEX(@sStr,@sStrTmp1)=0 OR CHARINDEX(@sStr1,@sStrTmp1)=0 )
		begin
		    --Select '!!!EndEnd-9-1-x1-1:' mark,@sStrTmp1 MatchText, @iCnt cnt,DIFFERENCE(@sStrTmp1, @sMatchName) idiff, ABS(Len(@sMatchName)-Len(@sStrTmp1)) iabs,Len(@sStr1) isStr1, CHARINDEX(' '+@sStr1, @sStrTmp1) ichar1,CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @sStrTmp1) ichar2
			--1.one word process=first word
			SELECT @sStrTmp='', @sStrTmp2=''
			if CHARINDEX(' ', @sStrTmp1)=0 
			begin
			  IF PATINDEX('%'+@sStr+'%',@sStrTmp1)>0
			  begin
				  SELECT @iLenC1=PATINDEX('%'+@sStr+'%',@ssSearchName), @iLenC2=@iLenC1+Len(@sStr)+1
				  if @iLenC1-1 >0  
				     SELECT @sStrTmp=SUBSTRING(@ssSearchName, @iLenC1-1,1)
				  IF @iLenC1-1 > 1  AND PATINDEX('%[0-9A-Za-z]%',@sStrTmp) > 0
				  begin
					SELECT @iLenC2=CHARINDEX(' ',@ssSearchName,@iLenC1+1)
					if @iLenC2>0 AND @iLenC2 > @iLenC1
					   SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@iLenC1, @iLenC2-@iLenC1+1), @sStrTmp1=@sStrTmp2
				  end	
				  ELSE
				  begin
				   SELECT @iLenC1=CHARINDEX(@sStr,@sStrTmp1), @iLenC2=CHARINDEX(' ',@sStrTmp1, @iLenC1+1)
				   if @iLenC1 >0 AND @iLenC2 > @iLenC1
				      SELECT @sStrTmp2=SUBSTRING(@sStrTmp1,@iLenC1, @iLenC2-@iLenC1+1), @sStrTmp1=@sStrTmp2
			      end
				end    

			  --2.one word process=last word
			  if PATINDEX('%'+@sStr1+'%',@sStrTmp1)>0
			  begin
				  SELECT @iLenC1=PATINDEX('%'+@sStr1+'%',@ssSearchName), @iLenC2=@iLenC1+Len(@sStr1)+1
				  if @iLenC1-1 >0  
				     SELECT @sStrTmp=SUBSTRING(@ssSearchName, @iLenC1-1,1)
				  --2.check is full word, not partial word
				IF @iLenC1-1 > 1  AND PATINDEX('%[0-9A-Za-z]%',@sStrTmp) > 0
				begin
					SELECT @iLenC2=CHARINDEX(' ',@ssSearchName,@iLenC1+1)
					if @iLenC2>0 AND @iLenC2 > @iLenC1
					   SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@iLenC1, @iLenC2-@iLenC1+1), @sStrTmp1=@sStrTmp2
				end	
				ELSE
				begin
				   SELECT @iLenC1=CHARINDEX(@sStr,@sStrTmp1), @iLenC2=CHARINDEX(' ',@sStrTmp1, @iLenC1+1)
				   if @iLenC1 >0 AND @iLenC2 > @iLenC1
				      SELECT @sStrTmp2=SUBSTRING(@sStrTmp1,@iLenC1, @iLenC2-@iLenC1+1), @sStrTmp1=@sStrTmp2
			    end 
			   end            

				--3.exception
				IF @sStrTmp1='' AND @iLenC2 > @iLenC1 AND @iLenC1 > 0
					SELECT @sStrTmp=SUBSTRING(@ssSearchName,@iLenC1, @iLenC2-@iLenC1+1),@sStrTmp1=@sStrTmp
			 
			end 

			else if CHARINDEX(' ', @sStrTmp1)>0
			Begin
			  IF CHARINDEX(@sStr, @sStrTmp1) >0
			  BEGIN
			    --Select '!!!EndEnd-9-1-x1-2-1' Mark,@sStrTmp1 MatchText, @sSTrTmp s1,@sStrTmp2 s2,@iLenC1 i1,@iLenC2 i2
				--1.more one word process=first word
			    SELECT @iLenC1=CHARINDEX(@sStr,@ssSearchName), @iLenC2=@iLenC1+Len(@sStr)+1
				if @iLenC1-1 > 1 
				  SELECT @sStrTmp=SUBSTRING(@ssSearchName, @iLenC1-1,1)
				
				--2.check is full word, not partial word
				IF @iLenC1-1 > 1  AND PATINDEX('%[0-9A-Za-z]%',@sStrTmp) > 0
				begin
					SELECT @iLenC2=CHARINDEX(' ',@ssSearchName,@iLenC1+1)
					if @iLenC2>0 AND @iLenC2 > @iLenC1
					   SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@iLenC1, @iLenC2-@iLenC1+1), @sStrTmp1=@sStrTmp2
				end
					
				ELSE
				begin
				   SELECT @iLenC1=CHARINDEX(@sStr,@sStrTmp1), @iLenC2=CHARINDEX(' ',@sStrTmp1, @iLenC1+1)
				   if @iLenC1 >0 AND @iLenC2 > @iLenC1
				      SELECT @sStrTmp2=SUBSTRING(@sStrTmp1,@iLenC1, @iLenC2-@iLenC1+1), @sStrTmp1=@sStrTmp2
			    end
				--Select '!!!EndEnd-9-1-x1-2-1' Mark,@sStrTmp1 MatchText, @sSTrTmp s1,@sStrTmp2 s2,@iLenC1 i1,@iLenC2 i2
			  END
			  			  
			  IF CHARINDEX(@sStr1, @sStrTmp1) >0
			  BEGIN
			    --Select '!!!EndEnd-9-1-x1-3' Mark,@sStrTmp1 MatchText, @sSTrTmp s1,@sStrTmp2 s2,@iLenC1 i1,@iLenC2 i2
				--1.more one word process=last word
				if CHARINDEX(@sStr1,@sStrTmp1, CHARINDEX(' ', @sStrTmp1)+1)>0
				begin
				  SELECT @iLenC1=CHARINDEX(@sStr,@ssSearchName), @iLenC2=@iLenC1+Len(@sStr1)+1
				  if @iLenC1-1 > 1
					SELECT @sStrTmp=SUBSTRING(@ssSearchName, @iLenC1-1,1)
				end
				--2.check is full word, not partial word
				IF PATINDEX('%[0-9A-Za-z]%',@sStrTmp) > 0
				begin
					SELECT @iLenC1=CHARINDEX(' ',@ssSearchName,@iLenC1+1), @iLenC2=CHARINDEX(@sStrTmp1,@ssSearchName)+Len(@sStrTmp1)
					if @iLenC2>0 AND @iLenC2 > @iLenC1
					   SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@iLenC1, @iLenC2-@iLenC1+1), @sStrTmp1=@sStrTmp2
				    ELSE 
					   SELECT @sStrTmp2='', @sStrTmp1=''
					   
				end		
				ELSE
				  IF @iLenC1 > 0 AND @iLenC2 > @iLenC1
				     SELECT @sStrTmp2=SUBSTRING(@ssSearchName,@iLenC1, @iLenC2-@iLenC1+1), @sStrTmp1=@sStrTmp2
			  END
			  
			   --Select '!!!EndEnd-9-1-x1-4' Mark,@sStrTmp1 MatchText, @sSTrTmp s1,@iCnt cnt,DIFFERENCE(@sStrTmp1, @sMatchName) idiff, ABS(Len(@sMatchName)-Len(@sStrTmp1)) iabs,Len(@sStr1) isStr1, CHARINDEX(' '+@sStr1, @sStrTmp1) ichar1,CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @sStrTmp1) ichar2

			    --4.exception
			    IF @sStrTmp1='' AND @iLenC2 > @iLenC1 AND @iLenC1 > 0
			        SELECT @sStrTmp=SUBSTRING(@ssSearchName,@iLenC1, @iLenC2-@iLenC1+1),@sStrTmp1=@sStrTmp
			 
		    End
         end
          
		 if CHARINDEX(@sStrTmp1,@ssSearchName) >0 AND DIFFERENCE(@sStrTmp1,@sMatchName)=4
		    GOTO RetEnd
		 --2023-03-24 fix 'BIN SONG': Recheck, End
		    
		--Select '!!!EndEnd-9-1-x2:' mark,@sStrTmp1 MatchText, @iCnt cnt,DIFFERENCE(@sStrTmp1, @sMatchName) idiff, ABS(Len(@sMatchName)-Len(@sStrTmp1)) iabs,Len(@sStr1) isStr1, CHARINDEX(' '+@sStr1, @sStrTmp1) ichar1,CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @sStrTmp1) ichar2
			    
		IF DIFFERENCE(@sStrTmp1, @sMatchName)=4 AND Len(@sStr1)=2 AND CHARINDEX(' '+@sStr1, @sStrTmp1) = 0 AND CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @sStrTmp1) > 0
		Begin
		SELECT @iCntSp1=CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @sStrTmp1)-1
		SELECT @sStrTmp=LTRIM(RTRIM(SUBSTRING(@sStrTmp1,1,@iCntSp1)))
		--get last MatchText LastWord poistion & Previous Char
		WHILE(@iCntSp1 > 0)
		BEGIN
			SELECT @iCntSp1=@iCntSp1-1
			IF SUBSTRING(@sStrTmp,@iCntSp1,1)=' ' or SUBSTRING(@sStrTmp,@iCntSp1,1)='|' or SUBSTRING(@sStrTmp,@iCntSp1,1)=',' or SUBSTRING(@sStrTmp,@iCntSp1,1)='.' or SUBSTRING(@sStrTmp,@iCntSp1,1)='/'
			Begin
				SELECT @sPartyNa1= ' ' +SUBSTRING(@sStrTmp,@icntSp1+1,1)
				if CHARINDEX(@sPartyNa1,@sMatchName) > 0
				SELECT @sStrTmp1=LTRIM(RTRIM(@sStrTmp)), @iCntSp1=0
				else IF @iCntSp1 > 1  --2022-03-09 ADD
					SELECT @sStrTmp=SUBSTRING(@sStrTmp,1,@iCntSp1-1), @sStrTmp1=LTRIM(RTRIM(@sStrTmp)), @iCntSp1=0
			End
		END
	End
	--2022-02-18 fix name:XINJIANG DAQO NEW ENERGY CO., LTD. , End				
	--Select '!!!EndEnd-9-1-x3-1:' mark,@sStrTmp1 MatchText, @iCnt cnt,DIFFERENCE(@sStrTmp1, @sMatchName) idiff, ABS(Len(@sMatchName)-Len(@sStrTmp1)) iabs

GetMidWord: 	--2022-03-15 add
				
			--2022-03-09 fix firstword duplicate and wrong position then reget position, Start
			if ABS(Len(@sStrTmp1) - Len(@sMatchName)) > 30 OR 
				( CHARINDEX('THE REPUBLIC OF',@sMatchName) >0 OR CHARINDEX('LIMITED LIABILITY COMPANY',@sMatchName) >0 OR
				    CHARINDEX('OBSHCHESTVO S OGRANICHENNOY OTVETSTVENNOSTYU', @sMatchName) > 0  OR 
					CHARINDEX('INMOBILIARIA E INVERSIONES',@sMatchName) > 0 OR CHARINDEX('INVERSIONES Y ASESORIAS',@sMatchName) > 0 OR
					CHARINDEX('HOLDINGS CO LTD', @sMatchName) > 0 OR CHARINDEX('HOLDING COMPANY LLC',@sMatchName) >0 OR CHARINDEX('HOLDINGS CO LTD', @sMatchName) > 0 OR
					CHARINDEX('HOLDINGS GROUP CO LTD',@sMatchName) > 0)
			Begin
				SELECT @iCntSp1=CHARINDEX(' ',@sMatchName), @iCntSp2=CHARINDEX(' ',@sMatchName, @iCntSp1+1)
				IF @iCntSp2 >1
				    SELECT @sPartyNa1=LTRIM(RTRIM(SUBSTRING(@sMatchName,1,@iCntSp2)))
				if ( @sStr='' OR @sStr=@sMatchName OR CHARINDEX(' ',@SSTR) >0 ) AND @iCntSp1 >1
				    SELECT @sStr=LTRIM(RTRIM(SUBSTRING(@sMatchName,1,@iCntSp1)))
				if  CHARINDEX(@sStr,@sPartyNa1) > 0 and CHARINDEX(@sPartyNa1,@ssSearchName) > 0
				BEGIN
					SELECT @sStrTmp1=@sPartyNa1, @iCntSp2=CHARINDEX(' ',@sMatchName, @iCntSp2+1)
					IF @iCntSp2> 1
						SELECT @sPartyNa1=SUBSTRING(@sMatchName,1,@iCntSp2), @iCntSp1=CHARINDEX(@sStrTmp1,@sMatchName, @iCntSp2+1)
				END

				if  CHARINDEX(@sStr,@sPartyNa1) > 0 and CHARINDEX(@sPartyNa1,@ssSearchName) > 0
				BEGIN
					SELECT @sStrTmp1=@sPartyNa1, @iCntSp2=CHARINDEX(' ',@sMatchName, @iCntSp2+1)
					IF @iCntSp2 > 1
						SELECT @sPartyNa1=RTRIM(LTRIM(SUBSTRING(@sMatchName,1,@iCntSp2))), @sStrTmp1=@sPartyNa1
                END
				--Select '!!!EndEnd-9-1-x4-1:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sPartyNa1 s1, CHARINDEX(@sPartyNa1,@ssSearchName) iChIndex, @iCntSp1 iSp1, @iCntSp2 iSp2
			End
			--2022-03-09 fix firstword duplicate and wrong position then reget position, End
	   
		    --2022-03-15 fix MIMA INTERNATIONAL LIMITED, START
			SELECT @sStr=LTRIM(RTRIM(@sStr)), @sStr1=LTRIM(RTRIM(@sStr1))
			  
			--2022-03-21 Change fix CHANG LI, Start
			IF DIFFERENCE(@sStrTmp1,@sMatchName)=4 AND ( CHARINDEX(@sStr,@sStrTmp1) > 0 OR  CHARINDEX(@sStr1,@sStrTmp1) > 0 )
			BEGIN
		          --Select '!!!EndEnd-9-1-x4-1-2:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, CHARINDEX(@sStr,@ssSearchName) iChIndex, CHARINDEX(@sStr1,@ssSearchName) iChIndex1
				  if ( CHARINDEX(@sStr, @sStrTmp1) >0 AND CHARINDEX(substring(@sStr1,1,1), @sStrTmp1) > 0 ) OR
				     ( CHARINDEX(@sStr1, @sStrTmp1) >0 AND CHARINDEX(substring(@sStr,1,1), @sStrTmp1) > 0 ) 
			         goto RetEnd
				  else
				  begin
				     --Select '!!!EndEnd-9-1-x4-1-1:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, CHARINDEX(@sStr,@ssSearchName) iChIndex, CHARINDEX(@sStr1,@ssSearchName) iChIndex1
				     if CHARINDEX(@sStr,@sStrTmp1) > 0 
				        SELECT @sStrTmp1=@sStr       
                     else if CHARINDEX(@sStr1,@sStrTmp1) > 0
					    SELECT @sStrTmp1=@sStr1
				     goto GetNextWord
			      end
			   END
			   --2022-03-21 Change fix CHANG LI, End
			  --Select '!!!EndEnd-9-1-x4-1-3:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, CHARINDEX(@sStr,@ssSearchName) iChIndex, CHARINDEX(@sStr1,@ssSearchName) iChIndex1
			   IF @sStrTmp1='' 
			      GOTO GetNextWord

			   IF (@sStr1='LTD' OR @sStr1='LIMITED') AND CHARINDEX(@sStr1,@sMatchName)= 0
 			   BEGIN
			      --Select '!!!EndEnd-10:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName M2, CHARINDEX(@sStr, @sMatchName) cMfirst, CHARINDEX(@sStr1, @sssearchName) cSlast , DIFFERENCE(@sStrTmp1,@sMatchName) idiff
	              GOTO RetEnd
			   END
			   --2022-03-15 fix MIMA INTERNATIONAL LIMITED, END

			   --2022-03-15 fix M.A.R. OR A -, START
			   IF @sStr=@sStr1 AND (@sStr=REPLACE(@sMatchName,'.','') or @sStr=REPLACE(@sMatchName,'-','') )
 			   BEGIN
			      if DIFFERENCE(@sStrTmp1,@sMatchName) < 3 AND ( CHARINDEX(@sStr,@sStrTmp1) > 0 or CHARINDEX(' '+@sStr,@sStrTmp1) > 0 or CHARINDEX('|'+@sStr,@sStrTmp1) >0 )
				     SELECT @sStrTmp1=@sStr			     
			      --Select '!!!EndEnd-10:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName M2, CHARINDEX(@sStr, @sMatchName) cMfirst, CHARINDEX(@sStr1, @sssearchName) cSlast , DIFFERENCE(@sStrTmp1,@sMatchName) idiff
	              GOTO RetEnd
			   END
			   --2022-03-15 fix M.A.R. or A -, END
			  
			   --2022-03-15 fix DONG DI or DIAZ MANUEL or LI NA DI  , START
			   IF DIFFERENCE(@sStrTmp1,@sMatchName)>=3  
 			   BEGIN
			      if CHARINDEX(@sStr,@sStrTmp1)> 0 AND CHARINDEX(@sStr1,@sStrTmp1)= 0
				     SELECT @sStrTmp1=@sStr
                  else if CHARINDEX(@sStr1,@sStrTmp1)> 0 AND CHARINDEX(@sStr,@sStrTmp1)=0
				     SELECT @sStrTmp1=@sStr1
                 
				 --Select '!!!EndEnd-10:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText,  @sMatchName M2, CHARINDEX(@sStr, @sMatchName) cMfirst, CHARINDEX(@sStr1, @sssearchName) cSlast , DIFFERENCE(@sStrTmp1,@sMatchName) idiff            				  
				 --2022-03-25 change fix 'LI,YI', Start
				 IF CHARINDEX(@sStrTmp1,@sMatchName)>0 OR (CHARINDEX(@sStrTmp1,@sMatchName)=0 AND (SUBSTRING(@sStr,1,1)=SUBSTRING(@sMatchName,1,1) AND CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @sMatchName)>0) )
			     BEGIN
				    --if firstword next char is not char, it is not reasonable
					IF CHARINDEX(@sStr1,@ssSearchName) > 0 AND PATINDEX('%[0-9A-Za-z]%',SUBSTRING(@ssSearchName,CHARINDEX(@sStr1,@ssSearchName)-1,1))= 0 AND
					   PATINDEX('%[0-9A-Za-z]%',SUBSTRING(@ssSearchName,CHARINDEX(RTRIM(@sStr1),@ssSearchName)+Len(@sStr1),1))=0
					   SELECT @sStrTmp1=@sStr1
                    ELSE IF CHARINDEX(@sStr,@ssSearchName) > 0 AND  PATINDEX('%[0-9A-Za-z]%',SUBSTRING(@ssSearchName,CHARINDEX(@sStr,@ssSearchName)+Len(@sStr)+1,1))= 0  AND 
					   PATINDEX('%[0-9A-Za-z]%',SUBSTRING(@ssSearchName,CHARINDEX(@sStr,@ssSearchName)-1,1))= 0 
					   SELECT @sStrTmp1=@sStr
				    --Select '!!!EndEnd-10-0:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText,  @sMatchName M2,CHARINDEX(@sStr1,@ssSearchName) iff, PATINDEX('%[0-9A-Za-z]%',SUBSTRING(@ssSearchName,CHARINDEX(@sStr1,@ssSearchName)-1,1)) ichar1, PATINDEX('%[0-9A-Za-z]%',SUBSTRING(@ssSearchName,CHARINDEX(RTRIM(@sStr1),@ssSearchName)+Len(@sStr1),1)) ichar2         				  
                    GOTO RetEnd
                 END
				 --2022-03-25 change fix 'LI,YI', End
			   
			   END
			   --2022-03-15 fix DONG DI or DIAZ MANUEL or LI NA DI , END

			   --2022-03-11 ADD FIX ONLY LASTWORD MATCH, Start
			  if LEN(@sStrTmp1) <=30  --If DIFFERENCE(@sStrTmp1,@sMatchName) < 4 AND DIFFERENCE(@sStrTmp1,@ssSearchName) < 4   --2022-03-14 add fix 'OBSHestvo s ogranichennoy otvetst'
			  Begin   --2022-03-14 add fix 'OBSHestvo s ogranichennoy otvetst'	
			   --Select '!!!EndEnd-10-0:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText,  @sMatchName M2, CHARINDEX(@sStr, @sMatchName) cMfirst, CHARINDEX(@sStr1, @sssearchName) clast , DIFFERENCE(@sStrTmp1,@sMatchName) idiff
			   if CHARINDEX(@sStr+' ',@sStrTmp1)=0  AND  LEN(RTRIM(@sStr1))> 1 AND CHARINDEX(@sStr1,@sStrTmp1)=0 and CHARINDEX(@sStr1,@sMatchName) > 0 and CHARINDEX(@sStr1,@ssSearchName) > 0
			      SELECT @sStrTmp1=@sStr1
               else if CHARINDEX(@sStr1,@sStrTmp1)=0 AND LEN(RTRIM(@sStr))>1 AND CHARINDEX(@sStr,@sStrTmp1)=0 and CHARINDEX(@sStr,@sMatchName) > 0 and CHARINDEX(@sStr,@ssSearchName) > 0
			      SELECT @sStrTmp1=@sStr
			   else if DIFFERENCE(@sStrTmp1, @sMatchName) < 4 AND ( CHARINDEX(' ',@sStrTmp1) > 0 OR CHARINDEX('LTD',@sStrTmp1)> 0 )
			   begin
			      IF SUBSTRING(@sStr,1,1)<>SUBSTRING(@sMatchName,1,1) AND CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @sMatchName)=0
			          SELECT @sStrTmp1=''

			      --Select '!!!EndEnd-10-1:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idiff	      
			   end

             End  --2022-03-14 add fix 'OBSHestvo s ogranichennoy otvetst'

			   --2022-03-11 ADD FIX ONLY LASTWORD MATCH, End

GetNextWord:	
               --2022-03-25 add fix 'ARI INTERNATIONAL LTD', Start
               IF (@sStr1='LIMITED' OR @sStr1='LTD')
			   begin
			      IF CHARINDEX('LIMITED',@sMatchName) > 0 AND CHARINDEX('LIMITED',@ssSearchName)=0 AND CHARINDEX('LTD',@ssSearchName)>0
			         SELECT @sStr1='LTD', @sMatchName=REPLACE(@sMatchName,'LIMITED','LTD')
				  ELSE IF CHARINDEX('LTD',@sMatchName) > 0 AND CHARINDEX('LTD',@ssSearchName)=0 AND CHARINDEX('LIMITED',@ssSearchName)>0
			         SELECT @sStr1='LIMITED',@sMatchName=REPLACE(@sMatchName,'LTD','LIMITED')
			   end  
			   --2022-03-25 add fix 'ARI INTERNATIONAL LTD', END
			   
			   --Select '!!!EndEnd-10-1-1:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName sMna, DIFFERENCE(@sStrTmp1,@sMatchName) idiff, ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs
			   --2022-03-25 add fix 'B.O.' , Start
			   IF @sStr=@sStr1 AND CHARINDEX(@sStr1, REPLACE(@sMatchName,'.','')) >0
			      if CHARINDEX(@sStr+' ', @ssSearchName) >0    --2022-03-29 fix 'SCS'
				     SELECT @sStrTmp1=@sStr
                --2022-03-25 add fix 'B.O.' , End

			   --2022-03-18 FIX --2022-03-11 add fix Null, Start
			   SELECT @sStr=RTRIM(LTRIM(@sStr)), @sStr1=RTRIM(LTRIM(@sStr1))
			   if ISNULL(@sStrTmp1,'')='' or (DIFFERENCE(@sStrTmp1,@sMatchName)<4 AND CHARINDEX(@sStrTmp1,@sMatchName) = 0)
			   BEGIN
			      
				  if @sStr=@sStr1 AND CHARINDEX(@sStr,@sStrTmp1)=0 AND 
				     ( CHARINDEX(@sStr+' ', @ssSearchName) >0 OR CHARINDEX(@sStr+'.', @ssSearchName) >0 OR 
					   CHARINDEX(@sStr+',', @ssSearchName) >0 OR CHARINDEX(@sStr+'/', @ssSearchName) >0 )
				  Begin
				     SELECT @sStrTmp1=@sStr
				     --2022-03-21 fix LIMITED LIABILITY COMPANY KONSTRUCTION COMPANY KONSOL STROI LTD, Start
					 SELECT @iLenC1=CHARINDEX(@sStr,@sMatchName) --, @iLenC2=CHARINDEX(' ',@sMatchName, @iLenC1+1 )
					 --SELECT @iLenC1=@iLenC2
					 --Select '!!!EndEnd-10-1-1-1:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText,@sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idiff	, @iLenC1 ilenc1, @iLenC2 ilenc2
					 --if get next word =0 then no more word
					 if CHARINDEX(' ',@sMatchName, @iLenC1+1 ) =0 OR @iLenC1 > LEN(@sMatchName)
					    if DIFFERENCE(@sStrTmp1,@sMatchName)=4 AND ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) <10
						   GOTO RetEnd
					 --2022-03-21 fix LIMITED LIABILITY COMPANY KONSTRUCTION COMPANY KONSOL STROI LTD, End
					   
				  End
                  
				  SELECT @sStrTmp1=''
				  if CHARINDEX(@sStr+ ' ',@ssSearchName) > 0 OR  PATINDEX('%'+@sStr+' '+'%', @ssSearchName) > 0
				     SELECT @sStrTmp1=@sStr+' ', @iLenC2=CHARINDEX(@sStr+ ' ',@ssSearchName) 
                  else if CHARINDEX(@sStr+ '.',@ssSearchName) > 0
				     SELECT @sStrTmp1=@sStr+'.', @iLenC2=CHARINDEX(@sStr+ '.',@ssSearchName)
			      else if CHARINDEX(@sStr+ ',',@ssSearchName) > 0
				     SELECT @sStrTmp1=@sStr + ',',  @iLenC2=CHARINDEX(@sStr+ ',',@ssSearchName)
                  else if LEN(@sStr) > 4 AND CHARINDEX(SUBSTRING(@sStr,1,4),@ssSearchName) > 0 AND PATINDEX('%[/|.,; -:]%',SUBSTRING(@ssSearchName,CHARINDEX(SUBSTRING(@sStr,1,4),@ssSearchName)-1,1)) >0 
				     SELECT @iLenC2=CHARINDEX(SUBSTRING(@sStr,1,4),@ssSearchName), @sStrTmp1=SUBSTRING(@ssSearchName,@iLenC2,CHARINDEX(' ', @ssSearchName,@iLenC2+1) - @iLenC2+1)

				 --Select '!!!EndEnd-10-1-1-2:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName sM2, DIFFERENCE(@sStrTmp1,@sMatchName) idiff, CHARINDEX(' ',@sMatchName,@iCntSp1+1) icnt2, len(@sMatchName) imLen, @iCntSp1 icnt1, CHARINDEX(@sStr,@ssSearchName) ic1, PATINDEX('%'+@sStr+'%', @ssSearchName)  ic2
				 --2022-03-21 fix N-2, START
                 --if get next word =0 then no more word
				 if DIFFERENCE(@sStrTmp1,@sMatchName)=4 AND ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) <10 AND @sStr=@sStr1 AND @sStr=@sStrTmp1
					GOTO RetEnd
                 --2022-03-21 fix N-2, END
              
				  --Select '!!!EndEnd-10-1-1-3:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName sM2, DIFFERENCE(@sStrTmp1,@sMatchName) idiff, CHARINDEX(' ',@sMatchName,@iCntSp1+1) icnt2, len(@sMatchName) imLen, @iCntSp1 icnt1, CHARINDEX(@sStr,@ssSearchName) ic1, PATINDEX('%'+@sStr+'%', @ssSearchName)  ic2
				  if @sStrTmp1<>'' AND @iLenC2 > 0
				  BEGIN
				     if PATINDEX('%[ /.,-:|]%', SUBSTRING(@ssSearchName,@iLenC2-1,1)) =0
					    SELECT @sStrTmp1=''
				  END             
                  else if @iLenC2 = 0           --2022-03-21 add fix LI LI CHANG
				       SELECT @sPartyNa=''      --2022-03-21 add fix LI LI CHANG

				  SELECT @iCntSp1=CHARINDEX(' ',@sMatchName) 
				 
				  --2022-03-29 fix 'MENI INTERNATIONAL LTD ' firstWord Missed, Start 
				  IF @iCntSp1=0 OR @iCntSp1 > LEN(@sMatchName)   --one word
 				    IF CHARINDEX(@sMatchName+' ',@ssSearchName) > 0
					   SELECT @sStrTmp1=@sMatchName
				  ELSE IF @iCntSp1 > 0 AND CHARINDEX(@sMatchName+' ',@ssSearchName) > 0
				     SELECT @sStrTmp1=SUBSTRING(@sMatchName,1,@iCntSp1)
				  --2022-03-29 fix 'MENI INTERNATIONAL LTD ' firstWord Missed, End

				  if @iCntSp1 > 0 
				  Begin
				     SELECT @iCntSp2=CHARINDEX(' ',@sMatchName,@iCntSp1+1),@sPartyNa=''
					 
					 if @iCntSp2 > LEN(@sMatchName) OR @iCntSp2=0     --2022-03-17 add
					    SELECT @iCntSp2=LEN(@sMatchName)              --2022-03-17 add
					 if @iCntSp2 > 0  AND @iCntSp2 > @iCntSp1 
				        SELECT @sPartyNa=SUBSTRING(@sMatchName,@iCntSp1+1,@iCntSp2-@iCntSp1)  --2022-03-17 change

					 SELECT @sPartyNa1=LTRIM(RTRIM(@sPartyNa)), @sPartyNa=@sPartyNa1
					 
					 --Select '!!!EndEnd-10-1-2:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idiff, CHARINDEX(' ',@sMatchName,@iCntSp1+1) icnt2, len(@sMatchName) imLen, @iCntSp1 icnt1, @sPartyNa s1	 
					 if @sStrTmp1<>'' AND CHARINDEX(@sStrTmp1,@ssSearchName) >0 AND CHARINDEX(@sPartyNa,@sStrTmp1) > 0
					    SELECT @iLenC1=CHARINDEX(@sStrTmp1,@ssSearchName) 

				  SELECT @iLenC2=0
				  if CHARINDEX(@sPartyNa+ ' ',@ssSearchName,@iLenC1+1) > 0
				     SELECT @sPartyNa=@sPartyNa+' ', @iLenC2=CHARINDEX(@sPartyNa+ ' ',@ssSearchName,@iLenC1+1) 
                  else if CHARINDEX(@sPartyNa+ '.',@ssSearchName,@iLenC1+1) > 0
				     SELECT @sPartyNa=@sPartyNa+'.', @iLenC2=CHARINDEX(@sPartyNa+ '.',@ssSearchName,@iLenC1+1)
			      else if CHARINDEX(@sPartyNa+ ',',@ssSearchName,@iLenC1+1) > 0
				     SELECT @sPartyNa=@sPartyNa + ',',  @iLenC2=CHARINDEX(@sPartyNa+ ',',@ssSearchName,@iLenC1+1)
				  else if CHARINDEX(@sPartyNa+ '-',@ssSearchName,@iLenC1+1) > 0
				     SELECT @sPartyNa=@sPartyNa + '-',  @iLenC2=CHARINDEX(@sPartyNa+ '-',@ssSearchName,@iLenC1+1)
                  else if CHARINDEX(@sPartyNa,@ssSearchName,@iLenC1+1) > 0
				     SELECT @sPartyNa=@sPartyNa+' ',  @iLenC2=CHARINDEX(@sPartyNa,@ssSearchName,@iLenC1+1)
                  else if LEN(@sPartyNa) > 4 AND CHARINDEX(SUBSTRING(@sPartyNa,1,4),@ssSearchName,@iLenC1+1) > 0 AND PATINDEX('%[/|.,; -:]%',SUBSTRING(@ssSearchName,CHARINDEX(SUBSTRING(@sPartyNa,1,4),@ssSearchName,@iLenC1+1)-1,1)) >0 
				     SELECT @iLenC2=CHARINDEX(SUBSTRING(@sPartyNa,1,4),@ssSearchName,@iLenC1+1), @sPartyNa1=SUBSTRING(@ssSearchName,@iLenC2,CHARINDEX(' ', @ssSearchName,@iLenC2+1) - @iLenC2+1), @sPartyNa=@sPartyNa1

				  if @sPartyNa<>'' AND @iLenC2 > 0
				  BEGIN
				     --if PATINDEX('%[/|., -:]%', SUBSTRING(@ssSearchName,@iLenC2-1,1)) =0
					 if PATINDEX('%[/,.-)+;: |]%', SUBSTRING(@ssSearchName,@iLenC2-1,4)) =0
					    SELECT @sPartyNa=''
				  END 
                  else if CHARINDEX(@sPartyNa,@ssSearchName)=0    --2022-03-21 add fix LI LI CHANG
				       SELECT @sPartyNa=''                        --2022-03-21 add fix LI LI CHANG

				  if CHARINDEX(@sPartyNa,@ssSearchName) > 0
					   SELECT @iLenC1=CHARINDEX(@sPartyNa,@ssSearchName)
                    else 
					   SELECT @iLenC1=0
        
			
                    if @iLenC1 > 0  OR CHARINDEX(@sPartyNa, @sStrTmp1) =0  --2022-03-21 change fix DR. A
					   SELECT @sStrTmp1=@sStrTmp1 + @sPartyNa

					 --Select '!!!EndEnd-10-1-3:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName sM1, DIFFERENCE(@sStrTmp1,@sMatchName) idiff, CHARINDEX(' ',@sMatchName,@iCntSp1+1) icnt2, len(@sMatchName) imLen, @iCntSp1 icnt1, @sPartyNa S1, len(@sPartyNa) islen, PATINDEX('%'+@sStr+' '+'%',@ssSearchName) ic1, @iLenC1 ilenc1, DIFFERENCE(@sStr,@sMatchName) idf1, SUBSTRING(@ssSearchName,@iLenC2-1,1) ss1, PATINDEX('%[/,.-)+;: |]%', SUBSTRING(@ssSearchName,@iLenC2-1,1)) ipat, PATINDEX('%[a-z,A-Z,0-9]%', SUBSTRING(@ssSearchName,@iLenC2-1,1)) ipt2
					 
					 SELECT @sStrTmp2=@sStrTmp1
					 if @iCntSp2 < LEN(@sMatchName)
					 BEGIN
						 SELECT @iCntSp1=@iCntSp2, @iCntSp2=CHARINDEX(' ',@sMatchName,@iCntSp1+1),@sPartyNa=''
						 if @iCntSp2 > LEN(@sMatchName)       --2022-03-17 add
							SELECT @iCntSp2=LEN(@sMatchName)  --2022-03-17 add
						  if @iCntSp2 > 0 AND @iCntSp2 > @iCntSp1
							SELECT @sPartyNa=SUBSTRING(@sMatchName,@iCntSp1+1,@iCntSp2-@iCntSp1)  --2022-03-17 change
						 
						  SELECT @sPartyNa1=LTRIM(RTRIM(@sPartyNa)), @sPartyNa=@sPartyNa1
						 
						  if Len(@sStrTmp1) > 0
						     SELECT @iLenC1=CHARINDEX(@sStrTmp1, @ssSearchName)
						  else 
						     SELECT @iLenC1=0

						  if CHARINDEX(' '+@sPartyNa, @ssSearchName,@iLenC1+1) > 0 
							 SELECT @sPartyNa=@sPartyNa
						  else if CHARINDEX(@sPartyNa+' ', @ssSearchName,@iLenC1+1) > 0
							 SELECT @sPartyNa=@sPartyNa+' '
						  else if CHARINDEX(@sPartyNa+'.', @ssSearchName,@iLenC1+1) > 0 
							 SELECT @sPartyNa= @sPartyNa+'.'
						  else if CHARINDEX(@sPartyNa+',', @ssSearchName, @iLenC1+1) > 0 
							 SELECT @sPartyNa=@sPartyNa+','
						  else if CHARINDEX(@sPartyNa+'-', @ssSearchName,@iLenC1+1) > 0 
							 SELECT @sPartyNa= @sPartyNa+'-'
						  else if CHARINDEX(@sPartyNa+'/', @ssSearchName, @iLenC1+1) > 0 
							 SELECT @sPartyNa=@sPartyNa+'/'
						  else if LEN(@sPartyNa) > 4 AND CHARINDEX(SUBSTRING(@sPartyNa,1,4),@ssSearchName, @iLenC1+1) > 0 AND PATINDEX('%[/|.,; -:]%',SUBSTRING(@ssSearchName,CHARINDEX(SUBSTRING(@sPartyNa,1,4),@ssSearchName, @iLenC1+1)-1,1)) >0 
							 SELECT @iLenC2=CHARINDEX(SUBSTRING(@sPartyNa,1,4),@ssSearchName, @iLenC1+1), @sPartyNa1=SUBSTRING(@ssSearchName,@iLenC2,CHARINDEX(' ', @ssSearchName,@iLenC2+1) - @iLenC2+1), @sPartyNa=@sPartyNa1

						  if @sPartyNa<>'' AND @iLenC2 > 0
						  BEGIN
							 if PATINDEX('%[a-z,A-Z,0-9]%', SUBSTRING(@ssSearchName,@iLenC2-1,1)) >0
								SELECT @sPartyNa=''
						  END 
                          else if CHARINDEX(@sPartyNa,@ssSearchName)=0    --2022-03-21 add fix LI LI CHANG
				               SELECT @sPartyNa=''                        --2022-03-21 add fix LI LI CHANG

						   if CHARINDEX(@sPartyNa,@ssSearchName,@ILenC1+1)  > 0
					          SELECT @iLenC1=CHARINDEX(@sPartyNa,@ssSearchName,@ILenC1+1)
							else 
							   SELECT @iLenC1=0
										
							if @iLenC1 > 0  AND CHARINDEX(@sPartyNa, @sStrTmp1) =0
							  if PATINDEX('%[/|., -:]%', SUBSTRING(@ssSearchName,@iLenC1-1,1)) >0
							     SELECT @sStrTmp1=@sStrTmp1 +' '+ @sPartyNa

                     END
					
                    --Select '!!!EndEnd-10-1-4:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName sM1, DIFFERENCE(@sStrTmp1,@sMatchName) idiff, CHARINDEX(' ',@sMatchName,@iCntSp1+1) icnt2, len(@sMatchName) imLen, @iCntSp1 icnt1, @sPartyNa S1, len(@sPartyNa) islen, PATINDEX('%'+@sStr1+' '+'%',@ssSearchName) ic1, @iLenC1 ic2, DIFFERENCE(@sStr,@sMatchName) id1,CHARINDEX(' '+@sPartyNa, @ssSearchName,@iLenC1+1) ic4,CHARINDEX(@sPartyNa+' ', @ssSearchName,@iLenC1+1) ic4

					 SELECT @sStrTmp2=@sStrTmp1				 
					 if @iCntSp2 < LEN(@sMatchName)
					 BEGIN
						  SELECT @iCntSp1=@iCntSp2, @iCntSp2=CHARINDEX(' ',@sMatchName,@iCntSp1+1),@sPartyNa=''
						  if @iCntSp2 > LEN(@sMatchName)       --2022-03-17 add
							 SELECT @iCntSp2=LEN(@sMatchName)  --2022-03-17 add
						  if @iCntSp2 > 0 AND @iCntSp2 > @iCntSp1
							SELECT @sPartyNa=SUBSTRING(@sMatchName,@iCntSp1+1,@iCntSp2-@iCntSp1)   --2022-03-17 change
						 
						  SELECT @sPartyNa1=LTRIM(RTRIM(@sPartyNa)), @sPartyNa=@sPartyNa1
						  
						  if CHARINDEX(' '+@sPartyNa, @ssSearchName,@iLenC1+1) > 0  
							 SELECT @sPartyNa=@sPartyNa
						  else if CHARINDEX(@sPartyNa+' ', @ssSearchName,@iLenC1+1) > 0 
							 SELECT @sPartyNa=@sPartyNa+' '
						  else if CHARINDEX(@sPartyNa+'.', @ssSearchName,@iLenC1+1) > 0    
							 SELECT @sPartyNa=@sPartyNa+'.'
						  else if CHARINDEX(@sPartyNa+',', @ssSearchName,@iLenC1+1) > 0  
							 SELECT @sPartyNa=@sPartyNa+','
						  else if CHARINDEX(@sPartyNa+'-', @ssSearchName,@iLenC1+1) > 0  
							 SELECT @sPartyNa=@sPartyNa+'-'
						  else if CHARINDEX(@sPartyNa+'/', @ssSearchName,@iLenC1+1) > 0    
							 SELECT @sPartyNa=@sPartyNa+'/'
						  else if CHARINDEX(@sPartyNa, @ssSearchName,@iLenC1+1) > 0  
							 SELECT @sPartyNa=@sPartyNa
						  else if LEN(@sPartyNa) > 4 AND CHARINDEX(SUBSTRING(@sPartyNa,1,4),@ssSearchName) > 0 AND PATINDEX('%[/|.,; -:]%',SUBSTRING(@ssSearchName,CHARINDEX(SUBSTRING(@sPartyNa,1,4),@ssSearchName)-1,1)) >0 
							 SELECT @iLenC2=CHARINDEX(SUBSTRING(@sPartyNa,1,4),@ssSearchName), @sPartyNa1=SUBSTRING(@ssSearchName,@iLenC2,CHARINDEX(' ', @ssSearchName,@iLenC2+1) - @iLenC2+1), @sPartyNa=@sPartyNa1

						  if @sPartyNa<>'' AND @iLenC2 > 0
						  BEGIN
							 if PATINDEX('%[/|., -:]%', SUBSTRING(@ssSearchName,@iLenC2-1,1)) =0
								SELECT @sPartyNa=''
						  END 
						  else if CHARINDEX(@sPartyNa,@ssSearchName)=0    --2022--03-21 add fix LI LI CHANG
							  SELECT @sPartyNa=''                         --2022--03-21 add fix LI LI CHANG

						   if CHARINDEX(@sPartyNa,@ssSearchName,@ILenC1+1)  > 0
					          SELECT @iLenC1=CHARINDEX(@sPartyNa,@ssSearchName,@ILenC1+1)
							else 
							   SELECT @iLenC1=0				
			
							if @iLenC1 > 0  AND CHARINDEX(@sPartyNa, @sStrTmp1) =0 
							   if PATINDEX('%[/|., -:]%', SUBSTRING(@ssSearchName,@iLenC1-1,1))  > 0
							      SELECT @sStrTmp1=@sStrTmp1 +' '+ @sPartyNa						 						  
						  
                     END

				  End
				  --Select '!!!EndEnd-10-1-5:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName sM2, DIFFERENCE(@sStrTmp1,@sMatchName) idiff, CHARINDEX(' ',@sMatchName,@iCntSp1+1) icnt2, len(@sMatchName) imLen, @iCntSp1 icnt1, @sPartyNa S1				 	
						  
				  --2022-03-17 change --2022-03-15 add fix lastword=CO, LTD, get more word, Start
				  SELECT @sStrTmp1=LTRIM(RTRIM(@sStrTmp1)), @sStr1=LTRIM(RTRIM(@sStr1))
				  IF (CHARINDEX('LTD',@sMatchName) > 0 OR CHARINDEX('LIMITED',@sMatchName) > 0 OR CHARINDEX('CO',@sMatchName)> 0 OR CHARINDEX('COMPANY',@sMatchName) > 0 ) AND
				     (CHARINDEX('LTD',@sStr1) > 0 OR CHARINDEX('LIMIT',@sStr1) > 0 OR CHARINDEX('CO',@sStr1)> 0 OR CHARINDEX('COMPANY',@sStr1) > 0 ) AND
					 (CHARINDEX(@sStr1,@sStrTmp1) = 0 )
				  Begin
				     SELECT @iCntSp1=CHARINDEX(@sStrTmp1,@ssSearchName)
					 SELECT @iCntSp2=CHARINDEX(@sStr1, @ssSearchName, @iCntSp1+1)
					 if @iCntSp2=0 AND LEN(@sStr1) < 4
					 Begin
					    if UPPER(SUBSTRING(@sStr1,1,2))='CO'
					    begin
					       SELECT @iCntSp2=CHARINDEX(' '+SUBSTRING(@sStr1,1,2), @ssSearchName, @iCntSp1+1)
							IF @iCntSp2=0
							   SELECT @iCntSp2=CHARINDEX('.'+SUBSTRING(@sStr1,1,2), @ssSearchName, @iCntSp1+1)
 							IF @iCntSp2=0
							   SELECT @iCntSp2=CHARINDEX(','+SUBSTRING(@sStr1,1,2), @ssSearchName, @iCntSp1+1)                       
 							IF @iCntSp2=0
							   SELECT @iCntSp2=CHARINDEX('|'+SUBSTRING(@sStr1,1,2), @ssSearchName, @iCntSp1+1)                       
					    end
						else
						begin
					       SELECT @iCntSp2=CHARINDEX(' '+SUBSTRING(@sStr1,1,1), @ssSearchName, @iCntSp1+1)
							IF @iCntSp2=0
							   SELECT @iCntSp2=CHARINDEX('.'+SUBSTRING(@sStr1,1,1), @ssSearchName, @iCntSp1+1)
 							IF @iCntSp2=0
							   SELECT @iCntSp2=CHARINDEX(','+SUBSTRING(@sStr1,1,1), @ssSearchName, @iCntSp1+1)                       
 							IF @iCntSp2=0
							   SELECT @iCntSp2=CHARINDEX('|'+SUBSTRING(@sStr1,1,1), @ssSearchName, @iCntSp1+1)                       
						end
											   
					    IF @iCntSp2 > 0
						Begin
						   SELECT @iCnt=@iCntSp2, @iCntSp2= CHARINDEX(' ',@ssSearchName,@iCnt+1)
					       if @iCntSp2=0
						      SELECT @iCntSp2=LEN(@ssSearchName)
					    End
					 End
					 SELECT @sStrTmp2=@sStrTmp1
					 if @iCntSp1 > 0 AND @iCntSp2 > 0 AND @iCntSp2 > @iCntSp1
					    SELECT @iCnt=@iCntSp2-@iCntSp1+1, @sStrTmp=SUBSTRING(@ssSearchName,@iCntSp1, @iCnt), @sStrTmp1=@sStrTmp
				  End
                  --2022-03-17 change --2022-03-15 add fix lastword=CO, LTD, get more word, End

				  --Select '!!!EndEnd-10-2:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName M1, @iCntSp1 isp1, @iCntSp2 isp2, @iCnt icnt
			   END

			   IF Len(RTRIM(LTRIM(@sStrTmp1)))=1 
			   Begin
		      	  if CHARINDEX(@sStrTmp1+' ', @ssSearchName)=0 and CHARINDEX(' '+@sStrTmp1, @ssSearchName)=0 and CHARINDEX(@sStrTmp1+'/', @ssSearchName)=0 and CHARINDEX('/'+@sStrTmp1, @ssSearchName)=0 and  CHARINDEX('|'+@sStrTmp1, @ssSearchName)=0 and  CHARINDEX(@sStrTmp1+'-', @ssSearchName)=0 and CHARINDEX(@sStrTmp1+',', @ssSearchName)=0 and CHARINDEX(@sStrTmp1+'.', @ssSearchName)=0
					 Select @sStrTmp1=''
			   End
			   ELSE IF LEN(RTRIM(@sStr))=1 AND (CHARINDEX(@sStr+' ', @sStrTmp1) > 0 OR CHARINDEX(@sStr+'/', @sStrTmp1) > 0 or CHARINDEX(@sStr+'-', @sStrTmp1) > 0 OR CHARINDEX(@sStr+',', @sStrTmp1) > 0 OR CHARINDEX(@sStr+'.', @sStrTmp1) > 0 OR CHARINDEX(@sStr+':', @sStrTmp1) > 0)  
			   Begin   
			         IF LEN(RTRIM(@sStr1))>1 AND (CHARINDEX(SUBSTRING(@sStr1,1,1)+' ', @sStrTmp1) = 0 OR CHARINDEX(SUBSTRING(@sStr1,1,1)+'/', @sStrTmp1) = 0 or CHARINDEX(SUBSTRING(@sStr1,1,1)+'-', @sStrTmp1) = 0 OR CHARINDEX(SUBSTRING(@sStr1,1,1)+',', @sStrTmp1) = 0 OR CHARINDEX(SUBSTRING(@sStr1,1,1)+'.', @sStrTmp1) = 0 OR CHARINDEX(SUBSTRING(@sStr1,1,1)+':', @sStrTmp1) = 0)  
					    IF CHARINDEX(@sStr+' ',@ssSearchName) > 0   --2022-03-29 fix 
					       SELECT @sStrTmp1=@sStr
               End

			   --Select '!!!EndEnd-10-3:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName M2

               --2022-03-11 add fix Null, End

RetEnd:		

			   IF ( ISNULL(@sStr,'')='' AND ISNULL(@sStr1,'')='' ) OR 
			      ( CHARINDEX('OBS',@sStr) > 0 AND LEN(@sStrTmp1)=35 )	
				    GOTO EndEndEnd	

			   IF CHARINDEX(@sMatchName,@sStrTmp1) > 0 AND LEN(@sStrTmp1) > LEN(@sMatchName)
			   BEGIN
			      SELECT @sStrTmp1=@sMatchName
				  GOTO EndEndEnd
			   END
            
			--2022-04-02 fix process more matchtext, Start
			IF DIFFERENCE(@sStrTmp1, @sMatchName)=4  AND PATINDEX('%[|]%',@sStrTmp1) > 0  AND LEN(@sStrTmp1) > LEN(@sMatchName)
			Begin
			   SELECT @iLenC1=PATINDEX('%[|]%',@sStrTmp1)-1
			   SELECT @sStrTmp=RTRIM(LTRIM(SUBSTRING(@sStrTmp1,1, @iLenC1)))
			   SELECT @sStrTmp1=@sStrTmp, @iMatchFlag=1

			/*
			--2022-04-02 fix
			 SELECT @iLenC1=CHARINDEX(@sStr,@sStrTmp2, @iLenC1+1)
					  if @iLenC1>0 AND PATINDEX('%[0-9A-Za-z]%',SUBSTRING(@sStrTmp2,@iLenC1+LEN(@sStr)+1,1)) > 0 
		*/

			End
			--2022-04-02 fix process more matchtext, End

            --2022-03-29 fix 'SCS', Start
            IF @sStr=@sStr1 AND  @sStr=@sStrTmp1 AND CHARINDEX(@sStr,@ssSearchName)=0 AND CHARINDEX(' '+SUBSTRING(@sStr,1,2),@ssSearchName)=0
			   SELECT @sStrTmp1=' '
            --2022-03-29 fix 'SCS', End

			--2022-04-02 fix 'INOVEST LIMITED',	'HONG PANG GEMS & JEWELLERY COMPANY LIMITED' , Start
			if @iMatchFlag=1 OR ( Difference(@sStrTmp1,@sMatchName)=4 AND SUBSTRING(@sStrTmp1,1,1)=SUBSTRING(@sMatchName,1,1) AND
			   ( ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) <10  OR LEN(@sStrTmp1) < LEN(@sMatchName) )   )
				GOTO EndEndEnd
			--2022-04-02 fix 'INOVEST LIMITED',	'HONG PANG GEMS & JEWELLERY COMPANY LIMITED' , End
		    
			--Select '!!!EndEnd-10-4X:' mark, @iMatchFlag iflag,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName M2, DIFFERENCE(@sStrTmp1, @sMatchName) idif, ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ipv

			--2022-03-18 add for recheck, Start
			SELECT @sStrTmp=RTRIM(LTRIM(@sStrTmp1)), @sStrTmp1=@sStrTmp
		    IF  DIFFERENCE(@sStrTmp1, @sMatchName) > 1 AND DIFFERENCE(@sStrTmp1, @sMatchName) < 4 
			    AND PATINDEX('%'+@sStr+'%',@sStrTmp1)=0 AND PATINDEX('%'+@sStr1+'%',@sStrTmp1)=0 
				AND SUBSTRING(@sStr,1,1)=SUBSTRING(@sStrTmp1,1,1) 
				AND SUBSTRING(@sStr1,1,1)<>SUBSTRING(@sStrTmp1,PATINDEX('%'+ ' ' + SUBSTRING(@sStr1,1,1)+'%', @sStrTmp1),1)					
			BEGIN
			    --Select '!!!EndEnd-10-4X-recheck:' mark,@sStr first,@sStr1 Last,@sStrTmp1 MatchText, @sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idd
				SELECT @iCnt=0, @sStrTmp='', @sStrTmp2=''
				WHILE(@iCnt < LEN(@sMatchName) )
				BEGIN
					SELECT @iCnt=@iCnt+1
					if @iCnt=1 
						SELECT @sPartyNa1=SUBSTRING(@sMatchName,@iCnt,1), @iCntSp1=@iCnt
					ELSE IF SUBSTRING(@sMatchName,@iCnt,1)=' ' or @iCnt=LEN(@sMatchName)
					Begin
						SELECT @sPartyNa2=SUBSTRING(@sMatchName,@iCnt+1,1), @iCntSp2=@iCnt
						SELECT @sPartyNa=LTRIM(RTRIM(SUBSTRING(@sMatchName,@iCntSp1, @iCntSp2-@iCntSp1+1)))
						SELECT @iCntSp1=@iCnt+1
						IF PATINDEX('%'+@sPartyNa +'%', @ssSearchName) >0 
						BEGIN
							if LEN(@sStrTmp)=0
								SELECT @sStrTmp=@sPartyNa
							else
								SELECT @sStrTmp=@sStrTmp + ' ' + @sPartyNa					  
						END	
						--Select '!!!EndEnd-10-4X-1:' mark,@sPartyNa1 first,@sPartyNa2 Last,@sPartyNa s,@sStrTmp mText,@sStrTmp1 mText1, @sMatchName M2, @iCnt ipos
					End

					if Len(@sStrTmp)>0
						SELECT @sStrTmp2=@sStrTmp1, @sStrTmp1=LTRIM(RTRIM(@sStrTmp))
				END
			END	

			 
			ELSE IF DIFFERENCE(@sStrTmp1,@sMatchName)<>4 AND @sStr=@sStr1 AND @sStrTmp1<>@sStr AND PATINDEX('% %',@sStrTmp1)=0 AND PATINDEX('%'+@sStrTmp1 +'%', @sMatchName) =0 AND SUBSTRING(@sStr,1,2)<>SUBSTRING(@sStrTmp1,1,2)
			BEGIN
			    SELECT @sStrTmp1= ' '
			    --Select '!!!EndEnd-10-4X-3:' mark,@sStr first,@sStr1 Last,@sStrTmp1 mText1, @sStrTmp mText2, @sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idd,ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ip, LEN(@sStrTmp1) iL
			END
			--2022-03-18 add for recheck, End

			--2022-03-31 fix 'HONG KONG HONG PANG GEMS AND JEWELLERY CO LIMITED', Start
			ELSE IF DIFFERENCE(@sStrTmp1,@sMatchName)=4 AND LEN(@sMatchName) > 15  AND PATINDEX('%[|]%',@sStrTmp1) > 0   -- AND ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) < 3
			BEGIN
			
		      --SELECT @iCnt=PATINDEX('%[|]%',@sStrTmp1)
			  SELECT @iCnt=PATINDEX('%[/, .-|]%',@sStrTmp1)
			  SELECT @sPartyNa='',@sPartyNa=SUBSTRING(@sStrTmp1,1,@iCnt-1), @sStrTmp2= @sPartyNa
			  --SELECT @iCntSp1=CHARINDEX(@sPartyNa,@sMatchName)+LEN(@sPartyNa) 
			  IF CHARINDEX(@sPartyNa,@sMatchName) >0 OR SUBSTRING(@sPartyNa,1,1)=SUBSTRING(@sMatchName,1,1) 
			  BEGIN
			    IF CHARINDEX(@sPartyNa,@sMatchName)=0  AND  DIFFERENCE(@sPartyNa,@sMatchName)< 4
				Begin
				  SELECT @sPartyNa='',@sPartyNa=SUBSTRING(@sStrTmp1,@iCnt, LEN(@sStrTmp1)-@iCnt+1), @sStrTmp2= @sPartyNa
			      SELECT @iCntSp1=CHARINDEX(@sPartyNa,@sMatchName)+LEN(@sPartyNa) 
				End

			    WHILE(@iCnt > 0)
				BEGIN
					SELECT @sPartyNa=''
					SELECT @iLenC3=CHARINDEX(' ',@sStrTmp1,@iCnt) +1
					IF @iLenC3=1 
					   SELECT @iLenC3=Len(@sStrTmp1)+1
				
					SELECT @iCntSp2=CHARINDEX(' ',@sMatchName, @iCntSp1)+1
					--Select '!!!EndEnd-10-7X-1-2:' mark,@sPartyNa ss,@iCnt ic1, @iLenC3 ic3, @icntSP1 isp1, @icntSp2 isp2,SUBSTRING(@sStrTmp1,@iCnt,1) s1,SUBSTRING(@sMatchName,@iCntSp1,1) s2, LEN(@sStrTmp1) iMaxLen
					IF SUBSTRING(@sStrTmp1,@iCnt,1)=SUBSTRING(@sMatchName,@iCntSp1,1) AND @iLenC3> @iCnt AND @iLenC3 > 1 
						SELECT @sPartyNa=SUBSTRING(@sStrTmp1,@iCnt,@iLenC3-@iCnt), @iCnt=@iLenC3, @iCntSp1=@iCntSp2
					ELSE IF  PATINDEX('%[.,/]%',SUBSTRING(@sStrTmp1,@iCnt,1)) >0 OR PATINDEX('%[.,/]%',SUBSTRING(@sMatchName,@iCntSp1,1)) > 0
					    SELECT @iCnt=@iLenC3, @iCntSp1=@iCntSp2
					ELSE
					    SELECT @iCnt=0
				   

					SELECT @sStrTmp2=@sStrTmp2 + @sPartyNa
				    SELECT @iMatchFlag=1  --2022-03-31 add parameter
				    --Select '!!!EndEnd-10-7X-1-3:' mark,@sPartyNa ss,@iCnt ic1, @iLenC3 ic3, @icntSP1 isp1, @icntSp2 isp2,SUBSTRING(@sStrTmp1,@iCnt,1) s1,SUBSTRING(@sMatchName,@iCntSp1,1) s2, LEN(@sStrTmp1) iMaxLen
				END
			 END

			 SELECT @sStrTmp1='', @sStrTmp1=@sStrTmp2
			 --SELECT '!!!EndEnd-10-7X-2:' mark,@sStrTmp1 mText1, @sStrTmp2 s2, @sPartyNa s, @sMatchName M2
           END
		   --2022-03-31 fix 'HONG KONG HONG PANG GEMS AND JEWELLERY CO LIMITED', End

			--2022-03-27 FIX 'D.LIGHT DESIGN INC' & 'DONG DI' , START
			ELSE IF DIFFERENCE(@sStrTmp1,@sMatchName)>=3 AND ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) > 15 AND PATINDEX('%[|]%',@sStrTmp1) > 0
			BEGIN			  
			    --Select '!!!EndEnd-10-7X-2:' mark,@sStr first,@sStr1 Last,@sStrTmp1 mText1,@sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idif,ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ip, LEN(@sStrTmp1) iL	, CHARINDEX('|',@sStrTmp1) ICHX, @iLenC1 ilenc1, @iLenC2 ilenc2
			    --check lastword position Less |
				SELECT @iLenC1=PATINDEX('%[|]%',@sStrTmp1), @iLenC2=CHARINDEX(@sStr1,@sStrTmp1)
				IF ( @iLenC1 > 0 AND @iLenC1 > @iLenC2) 
					SELECT @sStrTmp=SUBSTRING(@sStrTmp1,1,@iLenC1-1), @sStrTmp1=@sStrTmp
				ELSE IF  @iLenC1> 0 AND CHARINDEX(@sStr, @sStrTmp1) > 0  
					SELECT @sStrTmp=SUBSTRING(@sStrTmp1,1,CHARINDEX(' ',@sStrTmp1)-1), @sStrTmp1=@sStrTmp
			   --Select '!!!EndEnd-10-7X-3:' mark,@sStr first,@sStr1 Last,@sStrTmp1 mText1,@sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idif,ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ip, LEN(@sStrTmp1) iL, @iLenC1 ilenc1, @iLenC2 ilenc2
			END
			--2022-03-27 FIX 'D.LIGHT DESIGN INC' & 'DONG DI', END

RetEnd2:
     
			--2022-03-31 check match name, Start
			SELECT @sStrTmp1=UPPER(LTRIM(RTRIM(@sStrTmp1)))

			IF DIFFERENCE(@sStrTmp1, @sMatchName)=4 
			BEGIN

			   IF ( ISNULL(@sStr,'')='' AND ISNULL(@sStr1,'')='' ) OR 
			      ( CHARINDEX('OBS',@sStr) > 0 AND LEN(@sStrTmp1)=35 )	
				    GOTO EndEndEnd	

			   IF LEN(@sStrTmp1) > LEN(@sMatchName) AND CHARINDEX(@sMatchName,@sStrTmp1) > 0
			   BEGIN
			      SELECT @sStrTmp1=@sMatchName
				  GOTO EndEndEnd
			   END

			    SELECT @iLenC1=CHARINDEX(' ',@sStrTmp1), @sPartyNa=''
				IF @iLenC1 >0 
				Begin
				   if @iLenC1 > LEN(@sStr)
				      SELECT @sPartyNa=SUBSTRING(@sStrTmp1,Len(@sStr)+1,@iLenC1-Len(@sStr))
				   IF ISNULL(@sPartyNa,'')<>'' AND PATINDEX('%[/-.,| ]%', SUBSTRING(@sPartyNa,1,1) )> 0 
	                  SELECT @iMatchFlag=1
				   --SELECT '!!!EndEnd-10-7x-6-2:' mark,@sStr first,@sStr1 Last,@sStrTmp1 mText1, @sPartyNa s,PATINDEX('%[-/., |]%',SUBSTRING(@sPartyNa,1,1)) iptv,@iMatchFlag i
				   
				   SELECT @iLenC2=CHARINDEX(@sStr1, @sStrTmp1, @iLenC1),@sPartyNa2=''
				   IF @iLenC2 > 0  AND @iMatchFlag=0
				   BEGIN
				      SELECT @sPartyNa2=SUBSTRING(@sStrTmp1,@iLenC2+LEN(@sStr1),1)
				      IF ISNULL(@sPartyNa2,'')<>'' AND  PATINDEX('%[-/.,| ]%',SUBSTRING(@sPartyNa2,1,1)) >0
				          SELECT @iMatchFlag=1
				   END
				   ELSE IF @iLenC2 =0  AND CHARINDEX(@sStr, @sStrTmp1) > 0 AND @iMatchFlag=0
				   BEGIN
				      SELECT @sStrTmp1=@sStr
				      SELECT @iLenC1=CHARINDEX(@sStr+' ', @ssSearchName)
				      IF CHARINDEX(@sStr1, @ssSearchName,@iLenC1) > 0 AND CHARINDEX(@sStr1, @sStrTmp1)=0
				      Begin
						  SELECT @iLenC2=CHARINDEX(@sStr1, @ssSearchName)
						  IF @iLenC2 >0
						  begin
							 SELECT @iLenC3=CHARINDEX(' ', @ssSearchName, @iLenC2)
							 IF @iLenC3 > 0 AND @iLenC3 > @iLenC2
								SELECT @sStrTmp=SUBSTRING(@ssSearchName, @iLenC2,@iLenC3-@iLenC2+1), @sStrTmp1=@sStrTmp
							 ELSE 
								SELECT @iLENC3=LEN(@ssSearchName),@sStrTmp=SUBSTRING(@ssSearchName, @iLenC2,@iLenC3-@iLenC2+1), @sStrTmp1=@sStrTmp
						   end
					  End
				   END
				   
				    --SELECT '!!!EndEnd-10-7x-6-3:' mark,@iMatchFlag i,@sStr first,@sStr1 Last,@sStrTmp1 mText1, @sPartyNa s,@sPartyNa2 s2,PATINDEX('%[-/., |]%',SUBSTRING(@sPartyNa,1,1)) iptv, PATINDEX('%[-/., |]%',SUBSTRING(@sPartyNa2,1,1)) iptv2, @iLenC2 ic2
				End
		   END
		   --2022-03-31 check match name, End

			--SELECT '!!!EndEnd-10-7x-7:' mark,@iMatchFlag i,@sStr first,@sStr1 Last,@sStrTmp1 mText1, @sStrTmp mText2, @sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idd,ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ip
			--2022-03-29 fix 'BIN SONG' but wrong more word, Start
			IF @iMatchFlag=0
			Begin
				SELECT @sStrTmp1=RTRIM(LTRIM(@sStrTmp1))
				IF DIFFERENCE(@sStrTmp1,@sMatchName)>3 AND CHARINDEX(@sStrTmp1,@sMatchName)=0 AND @sStr<>@sStr1 AND
				   ( CHARINDEX(@sStr,@sStrTmp1)=0 OR CHARINDEX(@sStr1,@sStrTmp1)=0 ) AND CHARINDEX('OBSH',@sStrTmp1)=0 AND
				   ( CHARINDEX(@sStr+' ', @sStrTmp1)=0 OR CHARINDEX(' '+@sStr1, @sStrTmp1)=0) OR
				   ( CHARINDEX(@sStr+' ', @ssSearchName) >0 AND CHARINDEX(' '+@sStr1, @ssSearchName) >0 )
				BEGIN
				   --Select '!!!EndEnd-10-7x-7-1:' mark,@sStr first,@sStr1 Last,@sStrTmp1 mText1, @sStrTmp mText2, @sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idd,ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ip
				   IF (CHARINDEX(@sStr1, @ssSearchName)>0  AND CHARINDEX(@sStr1, @sStrTmp1) = 0 ) OR (SUBSTRING(@sStr,1,1)<>SUBSTRING(@sStrTmp1,1,1)  )
				   BEGIN
					   --Select '!!!EndEnd-10-7x-7-2:' mark,@sStr first,@sStr1 Last,@sStrTmp1 mText1, @sStrTmp mText2, @sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idd,ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ip
					   IF CHARINDEX(' '+@sStr1, @ssSearchName)=0 AND (CHARINDEX(SUBSTRING(@sStr,1,1),@sStrTmp1) >0 OR CHARINDEX(SUBSTRING(@sStr1,1,1),@sStrTmp1) >0 OR CHARINDEX(@sStr,@sStrTmp1) >0 )
						  SELECT @sStrTmp=SUBSTRING(@sStrTmp1,1,CHARINDEX(' ',@sStrTmp1)-1), @sStrTmp1=@sStrTmp
					   ELSE 
					   Begin
							SELECT @iLenC1=CHARINDEX(' '+@sStr1, @ssSearchName), @iLenC2=CHARINDEX(' ',@ssSearchName, @iLenC1+1)
							--SElect '!!!EndEnd-10-7x-7-3:' mark,@sStr first,@sStr1 Last,@sStrTmp1 mText1, @sStrTmp mText2, @sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idd,ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ip
							if @iLenC1 > 0  AND @iLenC2> @iLenC1
								SELECT @sStrTmp1=SUBSTRING(@ssSearchName,@iLenC1,@iLenC2-@iLenC1+1-1)
							else if  CHARINDEX(@sStr1,@sStrTmp1)=0 AND (CHARINDEX(@sStr1,@ssSearchName)>0 )
							Begin
								SELECT @iLenC1=CHARINDEX(@sStr1,@ssSearchName), @iLenC2=CHARINDEX(' ',@ssSearchName,@iLenC1)
								--Select '!!!EndEnd-10-7x-7-4:' mark,@sStr first,@sStr1 Last,@sStrTmp1 mText1, @sStrTmp mText2, @sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idd,ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ip
								IF @iLenC2 > @iLenC1 AND @iLenC1 > 0 
								   SELECT @sStrTmp1=SUBSTRING(@ssSearchName,@iLenC1,@iLenC2-@iLenC1+1)
							End
							else if  CHARINDEX(@sStr,@sStrTmp1)=0 AND CHARINDEX(@sStr1,@sStrTmp1)=0
								SELECT @sStrTmp1=' '
					   End
				  
				   END
				   --Select '!!!EndEnd-10-7x-7-5:' mark,@sStr first,@sStr1 Last,@sStrTmp1 mText1, @sStrTmp mText2, @sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idf,ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ipts
				END
				--Select '!!!EndEnd-10-7x-8:' mark,@sStr first,@sStr1 Last,@sStrTmp1 mText1, @sStrTmp mText2, @sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idf,ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ipts
			End
			--2022-03-29 fix 'BIN SONG' but wrong more word, End	
						         
			--2022-03-29 fix 'CHANG LI' but get matchtext more word, Start
			SELECT @sStrTmp1=RTRIM(LTRIM(@sStrTmp1))
			if @iMatchFlag=0 AND PATINDEX('%[-/|]%', @sStrTmp1) >0 
			BEGIN
			   SELECT @iLenC1=CHARINDEX(@sStr, @sStrTmp1), @iLenC2=CHARINDEX(@sStr1, @sStrTmp1)
			   SELECT @iCntSp2=PATINDEX('%[-/]%', @sStrTmp1),@iCntSp1=PATINDEX('%[ ]%', @sStrTmp1)
			   if @iLenC1> 0 AND @iLenC2=0 AND @iCntSp1 > @iLenC1 
			      SELECT @sStrTmp=SUBSTRING(@sStrTmp1,1,@iCntSp1-1)
			   ELSE if  @iLenC1> 0  AND @iLenC2>0 AND @iCntSp2 > @ilenC2 
			      SELECT @sStrTmp=subSTRING(@sStrTmp1,1,@iCntSp2-1)
			   
			   --Select '!!!EndEnd-10-7x-9:' mark,@sStr first,@sStr1 Last,@sStrTmp1 mText1, DIFFERENCE(@sStrTmp1,@sMatchName) idf,ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ipts, LEN(@sMatchName)  imL, LEN(@sStrTmp1) iL
			   IF PATINDEX('%[|]%', @sStrTmp) > 0 AND LEN(@sMatchName) < 30 AND DIFFERENCE(@sStrTmp1, @sMatchName)=4 AND ABS(LEN(@sStrTmp1)-LEN(@sMatchName))>3    --2022-03-31 fix 'PETROCHINA INTERNATIONAL (SINGAPORE)'
			   BEGIN
					--check lastword position Less |
					SELECT @iLenC1=PATINDEX('%[|]%',@sStrTmp), @iLenC2=CHARINDEX(@sStr1,@sStrTmp)
					IF ( @iLenC1 > 0 AND @iLenC1 > @iLenC2) 
						SELECT @sStrTmp=SUBSTRING(@sStrTmp,1,@iLenC1-1)
					ELSE IF  @iLenC1> 0 AND CHARINDEX(@sStr, @sStrTmp) > 0  
						SELECT @sStrTmp=SUBSTRING(@sStrTmp,1,PATINDEX('%[| ]%',@sStrTmp)-1)
               END
		       --Select '!!!EndEnd-10-7x-9-1:' mark,@sStr first,@sStr1 Last,@sStrTmp1 mText1, @sStrTmp mText2, DIFFERENCE(@sStrTmp1,@sMatchName) idd,ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ip, LEN(@sMatchName)  imL, LEN(@sStrTmp1) iL
			
			   if @iMatchFlag=0 AND PATINDEX('%[/-]%',@sStrTmp) > 0
                   SELECT @sStrTmp2=REPLACE(REPLACE(@sStrTmp,'/', ' '),'-',' ')
                else 
				   SELECT @sStrTmp2=@sStrTmp

			   if @iMatchFlag=0 AND CHARINDEX(@sStr, @sStrTmp2)=0 OR CHARINDEX(@sStr1, @sStrTmp2)=0
			   BEGIN
				   SELECT @iLenC1=CHARINDEX(@sStr, @sStrTmp2), @iLenC2=CHARINDEX(@sStr1, @sStrTmp2)
				   if @iLenC1>0 AND PATINDEX('%[0-9A-Za-z]%',SUBSTRING(@sStrTmp2,@iLenC1+LEN(@sStr)+1,1)) > 0 
				   Begin
					  SELECT @iLenC1=CHARINDEX(@sStr,@sStrTmp2, @iLenC1+1)
					  if @iLenC1>0 AND PATINDEX('%[0-9A-Za-z]%',SUBSTRING(@sStrTmp2,@iLenC1+LEN(@sStr)+1,1)) > 0 
					  Begin
						  SELECT @iLenC1=CHARINDEX(@sStr,@ssSearchName), @iLenC2=CHARINDEX(' ',@ssSearchName, @iLenC1+1)
						  if @iLenC1 > 0 AND @iLenC2>0 AND @iLenC2 > @iLenC1
							 SELECT @sStrTmp=SUBSTRING(@ssSearchName,@iLenC1, @iLenC2-@iLenC1+1-1)
					  End

					  SELECT @iLenC1=CHARINDEX(@sStr,@sStrTmp2, @iLenC1+1)
					  if PATINDEX('%[0-9A-Za-z]%',SUBSTRING(@sStrTmp2,@iLenC1+LEN(@sStr)+1,1)) =0
						 if @iLenC1 > 0 AND Len(@sStrTmp2) > @iLenC1
							SELECT @sStrTmp=SUBSTRING(@sStrTmp2, @iLenC1, Len(@sStrTmp2)-@iLenC1+1 )

				   End	
			  
				   SELECT @sStrTmp1=@sStrTmp	
				  
				   --2022-03-30 fix 'DONG DI', Start
				   IF @iMatchFlag=0 AND PATINDEX('%[ ]%', @sStrTmp1)=0
				   Begin
				      IF CHARINDEX(@sStr,@sStrTmp1) > 0 AND CHARINDEX(@sStr1,@sStrTmp1)=0
					     SELECT @sStrTmp1=@sStr
                      ELSE IF CHARINDEX(@sStr,@sStrTmp1) = 0 AND CHARINDEX(@sStr1,@sStrTmp1)>0
					     SELECT @sStrTmp1=@sStr1
				   End
				   --2022-03-30 fix 'DONG DI', End

				END   	   
				ELSE IF ABS(Len(@sStrTmp1)-LEN(@sMatchName)) > 15 AND CHARINDEX(@sStr, @sStrTmp1)>0 AND CHARINDEX(@sStr1, @sStrTmp1)>0
				BEGIN
				   --ReGet firstword Position
				   SELECT @iLenC1=CHARINDEX(@sStr,@sStrTmp1), @iLenC2=CHARINDEX(@sStr,@sStrTmp1, @iLenC1+1)
				   IF @iLenC1>0 AND @iLenC2>0 AND @iLenC2 > @iLenC1
				      SELECT @sStrTmp=SUBSTRING(@sStrTmp1,@iLenC2,LEN(@sStrTmp1)-@iLenC2+1), @sStrTmp1=@sStrTmp
				END
			END
			--2022-03-29 fix 'CHANG LI' but get matchtext more word, End


EndEndEnd:
			--Select '!!!EndEnd-10-7:' mark,@sStr first,@sStr1 Last,@sStrTmp1 mText1, @sStrTmp mText2, @sMatchName M2, DIFFERENCE(@sStrTmp1,@sMatchName) idd,ABS(LEN(@sStrTmp1)-LEN(@sMatchName)) iabs, PATINDEX('% %',@sStrTmp1) ip
		   
			select @sStrTmp1=RTRIM(LTRIM(@sStrTmp1))
			SELECT @sStrTmp1=REPLACE(@sStrTmp1,'  ',' ')
			--2022-03-21 fix, delete Special char, Start
			 if PATINDEX('%[.,:/-]%',SUBSTRING(@sStrTmp1,LEN(@sStrTmp1),1) ) >0
			    SELECT @sStrTmp=SUBSTRING(@sStrTmp1,1,LEN(@sStrTmp1)-1), @sStrTmp1=@sStrTmp
			--2022-03-21 fix, delete Special char, End

			--2022-03-25 fix, heading is newline, STart
		    IF SUBSTRING(@sStrTmp1,1,1)='|'
			   SELECT @sStrTmp=SUBSTRING(@sStrTmp1,2,Len(@sStrTmp1)-1), @sStrTmp1=@sStrTmp
			--2022-03-25 fix, heading is newline, End

			SELECT @sStrTmp1=REPLACE(@sStrTmp1,'|',@sNewLine)
			SELECT @sStrTmp1=REPLACE( @sStrTmp1, ' ' + CHAR(13) + CHAR(10), CHAR(13)+CHAR(10))
	

	        -- Select @sStrTmp1 MatchText, @sMatchName PartyName, @ssSearchName TransactionText, DIFFERENCE(@sStrTmp1, @sMatchName) idiff
              
			return @sStrTmp1
End

