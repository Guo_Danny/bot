USE [PBSA]
GO

/****** Object:  Trigger [dbo].[BSA_WatchListIns]    Script Date: 4/11/2022 9:24:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER TRIGGER [dbo].[BSA_WatchListIns] on [dbo].[Watchlist]   For Insert As
       declare @WLCode ObjectID, @oper SCode

       If @@rowcount > 0 begin
              --Apr.7 2022, add to unlock UseSysDate. Begin
              update Watchlist set CreateType = 0, UseSysDate = 1, LastOper = 'primeadmin'
              from Watchlist w
              join inserted i on w.WLCode = i.WLCode
              --Apr.7 2022, add to unlock UseSysDate. End

              Select @WLCode = WLCode, @oper = CreateOper from Inserted
              Exec BSA_InsEvent @oper, 'Cre', 'Rule', @WLCode, Null
       End
GO

