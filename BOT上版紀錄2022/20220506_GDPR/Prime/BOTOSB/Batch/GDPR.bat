
del \\OSBAMLLNDBV\d$\Prime\BOTOSB\Dat\Pbsa\*GDPR*

FORFILES /p "D:\AML Interface\Backup\CIF" /d -1 /M *GDPR* /c "cmd /c copy @file \\OSBAMLLNDBV\prime\BOTOSB\Dat\Pbsa"

FOR %%F IN ("\\OSBAMLLNDBV\prime\BOTOSB\Dat\Pbsa\*GDPR*") DO (
SQLCMD -S OSBAMLLNDBV -E -i "D:\Prime\BOTOSB\SQL\GDPR.SQL" -o "D:\Prime\BOTOSB\BatchLog\GDPRLog.txt" -v PARM1=%%F
)
