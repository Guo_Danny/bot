use PBSA
go


Create table #GDPR
(
	Content VARCHAR(500)
)

Create table #GDPRCustid
(
	id VARCHAR(20)
)

--insert data to temp table
BULK INSERT #GDPR
FROM '$(PARM1)' --- path file in db server 
WITH 
(
    ROWTERMINATOR ='\n'
)

declare @content varchar(500), @custid varchar(20);

-- get the customer id
DECLARE	@cur CURSOR
SET @cur = CURSOR FAST_FORWARD FOR 

select Content from #GDPR

OPEN @cur 
FETCH NEXT FROM @cur INTO @content
WHILE @@FETCH_STATUS = 0 BEGIN
	
	--insert custid to temp table
	declare @id varchar(20), @logtext varchar(6000);
	select @id = SUBSTRING(@content,20,10)
	select @logtext = 'GDPR del id:' + @id
	
	insert into #GDPRCustid(id) 
	select id from customer where id = @id

	if @@ROWCOUNT > 0
		exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', @id, @logtext, @logtext

	FETCH NEXT FROM @cur INTO @content
END

CLOSE @cur
DEALLOCATE @cur

--select * from #GDPRCustid

--del GDPR data start

--ofac
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', @id, 'del FilterTranTable', 'del FilterTranTable'
delete ofac..FilterTranTable where ref in (select id from #GDPRCustid)

exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', @id, 'del FilterTranHistTable', 'del FilterTranHistTable'
delete ofac..FilterTranHistTable where ref in (select id from #GDPRCustid)

exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', @id, 'del OFACScanAudit', 'del OFACScanAudit'
delete ofac..OFACScanAudit where CustId in (select id from #GDPRCustid)

--pbsa
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del Activity', 'del Activity'
delete Activity where Cust in (select id from #GDPRCustid)
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del ActivityHist', 'del ActivityHist'
delete ActivityHist where Cust in (select id from #GDPRCustid)
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del PartyRelation', 'del PartyRelation'
delete PartyRelation where PartyId in (select id from #GDPRCustid) or RelatedParty in (select id from #GDPRCustid)
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del SuspiciousActivity by cust', 'del SuspiciousActivity by cust'
delete SuspiciousActivity where Cust in (select id from #GDPRCustid)
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del SuspiciousActivityHist by cust', 'del SuspiciousActivityHist by cust'
delete SuspiciousActivityHist where Cust in (select id from #GDPRCustid)
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del SuspiciousActivity by AccountOwner', 'del SuspiciousActivity by AccountOwner'
delete SuspiciousActivity where Account in (select account from AccountOwner where Cust in (select id from #GDPRCustid))
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del SuspiciousActivityHist by AccountOwner', 'del SuspiciousActivityHist by AccountOwner'
delete SuspiciousActivityHist where Account in (select account from AccountOwner where Cust in (select id from #GDPRCustid))

exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del Profile by cust', 'del Profile by cust'
delete Profile where cust in (select id from #GDPRCustid)
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del Profile by AccountOwner', 'del Profile by AccountOwner'
delete Profile where Account in (select Account from AccountOwner where cust in (select id from #GDPRCustid))
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del Alert by cust', 'del Alert by cust'
delete Alert where Cust in (select id from #GDPRCustid)
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del Alerthist by cust', 'del Alerthist by cust'
delete Alerthist where Cust in (select id from #GDPRCustid)
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del Alert by AccountOwner', 'del Alert by AccountOwner'
delete Alert where Account in (select Account from AccountOwner where cust in (select id from #GDPRCustid))
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del Alerthist by AccountOwner', 'del Alerthist by AccountOwner'
delete Alerthist where Account in (select Account from AccountOwner where cust in (select id from #GDPRCustid))
--delete Account where id in (select Account from AccountOwner where cust in (select id from #GDPRCustid))
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del AccountOwner', 'del AccountOwner'
delete AccountOwner where Cust in (select id from #GDPRCustid)
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del Account', 'del Account'
delete Account where id not in (select account from AccountOwner)

exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del RiskScoreHist', 'del RiskScoreHist'
delete RiskScoreHist where customerid in (select id from #GDPRCustid)
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del RiskFactorData', 'del RiskFactorData'
delete RiskFactorData where Cust in (select id from #GDPRCustid)
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del KYCData', 'del KYCData'
delete KYCData where CustomerId in (select id from #GDPRCustid)
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del CountryOwner', 'del CountryOwner'
delete CountryOwner where Cust in (select id from #GDPRCustid)
exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del customer', 'del customer'
delete customer where id in (select id from #GDPRCustid)

exec BSA_InsEvent 'primeadmin', 'del','PurgeObject', '', 'del GDPR finished', 'del GDPR finished'
--del GDPR data end


--drop temp table
drop table #GDPR
drop table #GDPRCustid


