--add new note type
insert into OFAC..NoteType (Code, Name, CreateOper, CreateDate, LastOper, LastModify)
select max(Code) + 1, 'PreProgram','PrimeAdmin', GETDATE(), 'PrimeAdmin',GETDATE() from NoteType


begin tran
insert into OFAC..Notes(NotesID, EntNum, Note, NoteType, Status, ListCreateDate, ListModifDate, CreateOper, CreateDate,LastOper,LastModify)
select ROW_NUMBER() OVER(ORDER BY entnum) + (select max(NotesID) from OFAC..Notes(nolock)) AS ROWID, 
EntNum, Program, (select code from OFAC..NoteType(nolock) where name = 'preprogram'), Status, ListCreatedate, 
ListModifDate, 'PrimeAdmin', getdate(), 'PrimeAdmin', GETDATE()
from OFAC..SDNTable(nolock) where EntNum >= 20000000 order by EntNum

commit
