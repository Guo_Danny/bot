/*
**  File Name:        ConvPCDB.sql
**
**  Functional Description:
**
**  This module contains SQL conversion procedures for Prime version 10.1.3
**
**  Creation Date:    03/23/2018
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2011                                                   ***
*** FIS               			                                 		 ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in FIS.                                            					 ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by FIS.					 ***
***                                                                      ***
****************************************************************************
                       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|----------------------------------------------------     
03/13/2018		SN		Defect 6300: Disabled the Report number link for the reg report records whose acknowledgement 
                        Id is not the latest.   
03/13/2018		SN		Defect 6321: OFAC Case screen is throwing error when we click Next / Previous Button 
						in UnConfirmed Cases and in All Cases View on adding Cif fields delimiter column to Grid                 
03/13/2018		SN		Defect 6320: Modified SAR and CTR Reports view to display Acknowledgement errors corrected reports.
03/13/2018		SN		Defect 6315: Addressed view defects
05/22/2018		VS		Added FPEAccessEx function in pcdb database.
07/23/2018		GS		1800885419: Added RecursiveReplace, GetEncryptionColumns and WCDB_GetDecryptionWhereClause procedure in pcdb database.
						Modified WCDB_GetViewData procedure to get matching encrypted fields value in database table.
08/01/2018		VS		Added WCDB_fnGetVoltageError function to handle voltage error and Modified FPEAccessEx,
                        FPEProtectEx, stored procedure CDB_DecryptData and CDB_EncryptData.
08/24/2018		GS		Fixed BUG# 6441 - search by encryption fields like TIN/PassportNo/LicenseNo in query search not working as expected for Like and Not Null operator
09/04/2018	    GS		Fixed the defect 6455 : Modified the stored procedure BSA_GetCustomersBySql for fixing the issue while running the CustExport in
						Licensed Environment where Volgate tool is not available. 
						Added isnull() function to avoid entire @query is getting null, if any of the column value is null.		
09/04/2018      GS      Added changes to fix defect 6455: CustExport Failed to Export in Licensed Environment.
						The fix has been added to WCDB_fnCreateLibraryContextFPE function by returns empty if @librarycontext, @createfpeobject is null
09/06/2018      VS      Placed RecursiveReplace, GetEncryptionColumns and WCDB_GetDecryptionWhereClause stored procedure before WCDB_GetViewData stored proc. 
09/07/2018      VS		Placed latest updated WCDB_fnCreateLibraryContextFPE function and Added fix for defect 6455: CustExport Failed to Export in Licensed Environment. 
09/19/2018      VS		Fixed QUOTED_IDENTIFIER issue in GetEncryptionColumns stored proc.
10/03/2018      VS		1801838590 - Added RestrictNPISearch field in CustomObjects and CustomSettings table.Also, added IsNPISearchRestricted and GetNPISearchOperators stored procedure
10/04/2018      GS		1801838590 - Modified WCDB_GetDecryptionWhereClause stored procedure.
10/04/2018      GS		1801838590 - Added RecursiveReplaceRestrictNPISearch stored procedure.
10/10/2018      GS		Defect 6489 - Changed Datatype from Varchar to DATETIME for table name Requests columns RequestDate and CreateDate.
				Updated in listviewcolumns table for tableid = 517
01/08/2019		JS		1801984837 - Created new rule to add hyphen in Regex
05/16/2019	    MS      1801977031 - Modified datatype for vwRiskScoreHist view for Override Risk Score Functional Changes.
03/18/2020		AS		Defect 30648 : Fixed the issue while displaying larger amount values 
						in InstrumentQty and InstrumentPrice fields of Activity in query-plus grid.	
03/24/2020		AS		Defect 30648 : Added GO statements for each update state for InstrumentQty and InstrumentPrice fields.
'---------------- Prime 2019 Patch #01 code merge ----------------
11/10/2020		VS		Defect 34026 : Added fix to get all program names in sanction data rule in WCDB_GetViewData if column exceed more than 2000.
11/30/2020		VS		33212 : Join Command -SQL Query- Modified WCDB_GetViewData stored procedure to support join command.
01/08/2021		AS		32104 : Included new configuration options for restricting the file type extensions for Documents/Correspondence pages
						while uploading attachments.
01/20/2021		AS		Removed wrongly merged INSERT ControlTbl statement as it is already available in database.
02/03/2021		AS		Removed wrongly merged SQL statements as it is already available createpcdb file.
03/16/2021		AS		Added new configuration ThrdPtyLst to list out third party programs.
04/30/2021		AS		Changed the ThrdPtyLst configuration description.
08/16/2021		VS		TFS# 37970: Blank window is displayed when click on log in Edge and Chrome. In Pagetbl table PageWindowType is changed 
					from 1-General Popup to 0-Main window for Logfile.aspx page.
*/


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Starting Conversion of PCDB Database to version 10.1.3 '
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

Use PCDB
GO

if not exists (select * from dbo.CustomObjects with (nolock) 
	where code = 'RestrictNPISearch')
Begin
If Exists(select * from pcdb.dbo.Customobjects with (nolock) where code='VoltageEncryptionConfig' and enabled=1)
	insert into CustomObjects
	( Code ,[Description] ,Createdate ,CreateOper ,LastModifDate ,LastOper ,Enabled	)
	values
	('RestrictNPISearch' ,'RestrictNPISearch',getdate() ,'PRIME' ,getdate() ,'PRIME','1')
Else
	insert into CustomObjects
	( Code ,[Description] ,Createdate ,CreateOper ,LastModifDate ,LastOper ,Enabled	)
	values
	('RestrictNPISearch' ,'RestrictNPISearch',getdate() ,'PRIME' ,getdate() ,'PRIME','0')
End

if not exists (select * from dbo.CustomSettings with (nolock)
	where code = 'RestrictNPISearch' and 
		Product = 'SEC' and 
		[Key] = 'NPISearchOperators')
begin
  -- Values '0,1,9,10' represents operator '=,!=,Is null,Is not null' and set from the CQueryPlus.vb file PARAM_OPERATION Enum values
	Insert into CustomSettings 
	( Code ,Product ,RecordType ,OperCode ,[Key] ,Value ,IsUserEditable,
	  CreateDate ,CreateOper ,LastModifDate ,LastOper)
	Values 
	('RestrictNPISearch', 'SEC', 'SYSTEM', '', 'NPISearchOperators', '0,1,9,10', 0,
		getdate(), 'PRIME', getdate(), 'PRIME')
end
GO

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IsNPISearchRestricted]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[IsNPISearchRestricted]
GO

Create Procedure [dbo].[IsNPISearchRestricted] 
(
@IsRestricted bit  OUTPUT
)
  With Encryption As

  SET @IsRestricted=0
  select @IsRestricted= [Enabled] from pcdb.dbo.Customobjects with (nolock) where code='RestrictNPISearch' and enabled=1
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetNPISearchOperators]')  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[GetNPISearchOperators]
GO
CREATE PROCEDURE [dbo].[GetNPISearchOperators]
( 
  @SearchOperators VARCHAR(250)  OUTPUT
)
WITH ENCRYPTION AS
SET NOCOUNT ON
BEGIN
	SET @SearchOperators = ''

	SELECT @SearchOperators= Value from pcdb.dbo.CustomSettings with (nolock) where code='RestrictNPISearch' and [key]='NPISearchOperators'
RETURN @@ERROR
END
GO

Set ANSI_NULLS ON
Go

Set NOCOUNT ON 
Go

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RecursiveReplace]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[RecursiveReplace]
GO
CREATE PROCEDURE [dbo].[RecursiveReplace]
(
	@InputStr NVARCHAR(3000),
	@FindStr VARCHAR(8000),	
	@ReplacePattern VARCHAR(8000),
	@OutputStr NVARCHAR(3000) OUTPUT
)
WITH ENCRYPTION AS
SET NOCOUNT ON

BEGIN	
	/*
		Search by Encrypted column Like TIN, PassportNo,...
		Find the Encrypted fields in input string and append the column name with Voltage decrypt function in order to search the value given by customer in the UI.
	*/
	DECLARE @CharIndexPos INT
	DECLARE @CurentPos INT
	DECLARE @StartPos INT
	DECLARE @EndPos INT
	DECLARE @NumberOfChars INT	
	DECLARE @StartLocation INT = 1
	DECLARE @StringPattern VARCHAR(8000)
	DECLARE @StringReplacement  VARCHAR(8000)
	DECLARE @FindNextStr VARCHAR(256)			
	
	DECLARE TmpCursor CURSOR FOR   
		SELECT SplitData FROM fnParseDelimitedString(@FindStr,',')
	OPEN TmpCursor  
	FETCH NEXT FROM TmpCursor INTO @FindNextStr
	WHILE @@FETCH_STATUS = 0  
	BEGIN  	
		/* 
			Start from 1st match position. 
			Contiune until end of the finding as last character/string (Encrypted fields. Like TIN, PassportNo) in @inputStr.
			This loop start from here to end FindNext: go statement 
		*/				
		SET @CharIndexPos = CHARINDEX(@FindNextStr,@InputStr,@StartLocation)	
		
		/*  
			----@InfiniteCheck----
			Precaution on the while loop if never ends.
			Limits to 200, the while loop can continue to find an encrypted column name in input string.
		*/
		DECLARE @InfiniteCheck INT = 0 

		WHILE (@CharIndexPos > 0 and @InfiniteCheck < 200)
		BEGIN
			SET @InfiniteCheck = @InfiniteCheck + 1
			SET @CurentPos = @CharIndexPos
		
			/* 
			    Initial validation on found position (@CharIndexPos) is actual column name or not, To elemenate the wrong finding
				ex: 
					* where ActInActAmt = 100 - column contain 'tIn' as character
					* where Name = 'Christina' - Name contain 'tin' as character) 
			*/
			DECLARE @iVal INT = @CharIndexPos		 
			BEGIN
				/* -- Find previous character, if other than '.' or '[' or ' ' (dot, open squar, space) then goto next statement ---- */
				DECLARE @iPrevChar CHAR
				SET @iPrevChar = SUBSTRING(@InputStr, @iVal-1, 1)						
				IF @iPrevChar <> '.' AND @iPrevChar <> '[' AND @iPrevChar <> ' '
				BEGIN				
					SET @StringReplacement = @CharIndexPos + 1
					GOTO FindNext;
				END		
				/* -- Find next character, if other than ']' or ' ' or ')' (open squar, space, close parenthesis) then goto next statement --- */
				DECLARE @iNextChar CHAR
				SET @iNextChar = SUBSTRING(@InputStr, @iVal + LEN(@FindNextStr), 1)						
				IF @iNextChar <> ']' AND @iNextChar <> ' ' AND @iNextChar <> ')'
				BEGIN				
					SET @StringReplacement = @CharIndexPos + 1
					GOTO FindNext;
				END			 
			END		
				
			/*
				Find the column name (whole string) with SQL syntax/format in @InputStr, Like (TIN), C.TIN, [Customer.TIN]
			    Find previous character until ' ' or '('
			*/
			DECLARE @PrevPos INT = @CurentPos
			DECLARE @PrevChar char
			WHILE @PrevPos <> 0 
			BEGIN
				SET @PrevChar = SUBSTRING(@InputStr,@PrevPos,1)					
				IF @PrevChar = '('
				BEGIN		
					/*-- Validate SQL keywords, Like 'Where Len(TIN) = 0' ---------Start-*/				
					DECLARE @KeywordStr VARCHAR(max) = ''
					DECLARE @iChar VARCHAR(1) = ''
					DECLARE @SQLKeywordPos INT = @PrevPos-1
					WHILE @SQLKeywordPos <> 0 
					BEGIN						
						SET @iChar = SUBSTRING(@InputStr,@SQLKeywordPos,1)
						IF @iChar <> ''
						BEGIN
							SET @KeywordStr = @iChar + @KeywordStr
							SET @SQLKeywordPos = @SQLKeywordPos - 1
						END
						ELSE
						BEGIN
							BREAK
						END
					END
					
					IF @KeywordStr = ''
					BEGIN
						BREAK						
					END
					/*
						SQL Key word list
						Note: For now added LEN as SQL keyword. Later we can add list of SQL keywords which we need to include it.
					*/
					ELSE IF @KeywordStr = 'LEN'
					BEGIN
						SET @PrevPos = @PrevPos		-- No Changes Required
					END
					/* -- Validate SQL keywords End--- */
					BREAK
				END
				ELSE IF @PrevChar = ' '
				BEGIN																		
					BREAK
				END					
				ELSE
				BEGIN						
					SET @PrevPos = @PrevPos - 1
				END
			END

			/*
				Find the column name (whole string) with SQL syntax/format in @InputStr, Like (TIN), C.TIN, [Customer.TIN]
			    Find next character until ' ' or ']' or ')'
			*/
			DECLARE @NextPos INT = @CharIndexPos + LEN(@FindNextStr)
			DECLARE @NextChar char				
			WHILE @NextPos < len(@InputStr)
			BEGIN
				SET @NextChar = SUBSTRING(@InputStr,@NextPos,1)
				IF @NextChar = ']'
				BEGIN
					SET @NextPos = @NextPos + 1
					BREAK
				END
				ELSE IF @NextChar = ')'
				BEGIN					
					BREAK
				END
				ELSE IF @NextChar <> ' '
				BEGIN														
					SET @NextPos = @NextPos - 1																					
					BREAK
				END					
				ELSE
				BEGIN						
					SET @NextPos = @NextPos + 1
				END
			END

			SET @StartPos = @PrevPos + 1
			SET @EndPos =  @NextPos

			SET @NumberOfChars = @EndPos - @StartPos
			SET @StringPattern = SUBSTRING(@InputStr,@StartPos,@NumberOfChars)	
			SET @StringReplacement = FORMATMESSAGE(@ReplacePattern, @stringPattern)		
			SET @InputStr = STUFF(@InputStr, @StartPos, LEN(@StringPattern), @StringReplacement) 		
		
			/* N-th matching position, Contiune the loop until end of character in @InputStr */
			FindNext:
			SET @CharIndexPos = CHARINDEX(@FindNextStr,@InputStr, @CharIndexPos+LEN(@StringReplacement))				
		END
		
		FETCH NEXT FROM TmpCursor INTO @FindNextStr
	END   
	CLOSE TmpCursor;  
	DEALLOCATE TmpCursor;  		
	SET @OutputStr = @InputStr
END
GO

SET QUOTED_IDENTIFIER ON
Go

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetEncryptionColumns]')  AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[GetEncryptionColumns]
GO
CREATE PROCEDURE [dbo].[GetEncryptionColumns]
( 
  @EncryptedColumn VARCHAR(MAX)  OUTPUT
)
WITH ENCRYPTION AS
SET NOCOUNT ON
BEGIN
	--It returns the Encrypted column name list in comma separated value from EncryptionMapping table 

	SET @EncryptedColumn = ''

	SELECT 
		@EncryptedColumn = STUFF((SELECT DISTINCT ',' + lvc.ColumnName 
	FROM 
		EncryptionMapping EM 
	JOIN 
		ListViewColumns LVC ON EM.TableId = LVC.TableId AND EM.EncryptColumnId = LVC.Position
	FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')

RETURN @@ERROR
END
GO

SET QUOTED_IDENTIFIER OFF
Go

SET QUOTED_IDENTIFIER ON
Go

Set ANSI_NULLS ON
Go

Set NOCOUNT ON 
Go

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RecursiveReplaceRestrictNPISearch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[RecursiveReplaceRestrictNPISearch]
GO
CREATE PROCEDURE [dbo].[RecursiveReplaceRestrictNPISearch]
(
	@InputStr NVARCHAR(3000),
	@FindStr VARCHAR(8000),	
	@ReplacePattern VARCHAR(8000),
	@librarycontext VARCHAR(8000),
	@OutputStr NVARCHAR(3000) OUTPUT
)
WITH ENCRYPTION AS
SET NOCOUNT ON
BEGIN	
	/*
		Search by Encrypted column Like TIN, PassportNo,...
		Find the Encrypted field(s) in input string and append the value with Protect function in the right hand side of the operator.
		
		This DB object does only support equal, not equal, is null, is not null operators, 
		if other than listed operators then return an error to UI as 'SQL statement contains operator not supported for NPI field'
	*/
	DECLARE @ValidationErr VARCHAR(100)
	DECLARE @CharIndexPos INT
	DECLARE @CurrentPos INT
	DECLARE @StartPos INT
	DECLARE @EndPos INT
	DECLARE @NumberOfChars INT	
	DECLARE @StartLocation INT = 1
	DECLARE @StringPattern VARCHAR(500)
	DECLARE @StringReplacement  VARCHAR(500)
	DECLARE @FindNextStr VARCHAR(256)				
	SET @ValidationErr = ''

	/* Get the Operator(s) list from configuration table */
	DECLARE @SearchOperator VARCHAR(250)	 
	EXEC [dbo].[GetNPISearchOperators] @SearchOperator OUT
	/* Refer CField.vb class for four operators value
		0 - iEQUAL
		1 - iNOT_EQUAL
		9 - iIS_NULL
		10 - iIS_NOT_NULL	
	 */	
	DECLARE @DefaultOperatorList TABLE
	(
		KeyId VARCHAR(10),
		OperatorSign VARCHAR(100)
	)
	INSERT INTO @DefaultOperatorList SELECT '0' AS KeyId, '=' AS OperatorSign
	INSERT INTO @DefaultOperatorList SELECT '1' AS KeyId, '!=' AS OperatorSign
	INSERT INTO @DefaultOperatorList SELECT '9' AS KeyId, 'is null' AS OperatorSign
	INSERT INTO @DefaultOperatorList SELECT '10' AS KeyId, 'is not null' AS OperatorSign

	DECLARE @SearchOperatorList TABLE
	(
		AutoId INT IDENTITY(1,1),
		OperatorSign VARCHAR(100)
	)
	INSERT INTO @SearchOperatorList(OperatorSign)
	SELECT OperatorSign FROM @DefaultOperatorList 
	WHERE (1 = CASE WHEN @SearchOperator <> '' AND @SearchOperator IS NOT NULL THEN 0 ELSE 1 END OR KeyId IN(SELECT SplitData FROM fnParseDelimitedString(@SearchOperator,',')))
	----------------------------------------------------------------------------------	

	DECLARE CursorMain CURSOR FOR   
		SELECT SplitData FROM fnParseDelimitedString(@FindStr,',')
	OPEN CursorMain  
	FETCH NEXT FROM CursorMain INTO @FindNextStr
	WHILE @@FETCH_STATUS = 0  
	BEGIN  	
		/* 
			Start from 1st match position. 
			Continue until end of the last character in @inputStr to find the encrypted field (Like TIN, PassportNo).			
		*/				
		SET @CharIndexPos = CHARINDEX(@FindNextStr,@InputStr,@StartLocation)	
		
		/*  
			----@InfiniteCheck----
			Precaution on the while loop if never ends.
			Limits to 200, the while loop can continue to find an encrypted column name in input string.
		*/
		DECLARE @InfiniteCheck INT = 0 

		WHILE (@CharIndexPos > 0 and @InfiniteCheck < 200)
		BEGIN
			SET @InfiniteCheck = @InfiniteCheck + 1
			SET @CurrentPos = @CharIndexPos
		
			/* 
			    Initial validation to eliminate the wrong finding position found in @CharIndexPos.
				ex: 
					* Where ActInActAmt = 100 - the column name "ctInActAmt" contain 'tIn' as character
					* Where Name = 'Christina' - the value "Christina" contain 'tin' as character
			*/
			DECLARE @iVal INT = @CharIndexPos		 
			BEGIN
				/* -- Find previous character, If other than '.' or '[' or ' ' or '(' (dot, open square, space, open parenthesis) then goto next statement ---- */
				DECLARE @iPrevChar CHAR
				SET @iPrevChar = SUBSTRING(@InputStr, @iVal-1, 1)						
				IF @iPrevChar NOT IN('.','[',' ','(')
				BEGIN				
					SET @StringReplacement = @CharIndexPos + 1
					GOTO FindNext;
				END		
				/* -- Find next character, If other than ']' or ' ' or ')' (open square, space, close parenthesis) then goto next statement --- */
				DECLARE @iNextChar CHAR
				SET @iNextChar = SUBSTRING(@InputStr, @iVal + LEN(@FindNextStr), 1)						
				IF @iNextChar NOT IN(']',' ',')','=','>','<','!')
				BEGIN				
					SET @StringReplacement = @CharIndexPos + 1
					GOTO FindNext;
				END			 
			END		

			/* -- Find the operator -- */			
			DECLARE @NextPos INT = @CharIndexPos + LEN(@FindNextStr)			
			DECLARE @MatchedOperator varchar(100) = ''							
			DECLARE @FindNextOperator VARCHAR(100)
			DECLARE @TmpFindNextOperator VARCHAR(100)
			
			DECLARE CursorFindOper CURSOR FOR   			
				SELECT OperatorSign FROM @SearchOperatorList
			OPEN CursorFindOper  
			FETCH NEXT FROM CursorFindOper INTO @FindNextOperator
			WHILE @@FETCH_STATUS = 0  
			BEGIN				
				IF @NextPos <= LEN(@InputStr)
				BEGIN
					SET @TmpFindNextOperator = space(1) + @FindNextOperator + space(1) 
					IF @TmpFindNextOperator = SUBSTRING(@InputStr,@NextPos,LEN(@TmpFindNextOperator+'X')-1)
					BEGIN							
						SET @MatchedOperator = @TmpFindNextOperator
						BREAK
					END

					SET @TmpFindNextOperator = space(1) + @FindNextOperator 
					IF @TmpFindNextOperator = SUBSTRING(@InputStr,@NextPos,LEN(@TmpFindNextOperator+'X')-1)
					BEGIN							
						SET @MatchedOperator = @TmpFindNextOperator
						BREAK
					END
					
					SET @TmpFindNextOperator = @FindNextOperator + space(1) 
					IF @TmpFindNextOperator = SUBSTRING(@InputStr,@NextPos,LEN(@TmpFindNextOperator+'X')-1)
					BEGIN							
						SET @MatchedOperator = @TmpFindNextOperator
						BREAK
					END
					
					SET @TmpFindNextOperator = @FindNextOperator 
					IF @TmpFindNextOperator = SUBSTRING(@InputStr,@NextPos,LEN(@TmpFindNextOperator+'X')-1)
					BEGIN							
						SET @MatchedOperator = @TmpFindNextOperator
						BREAK
					END										
				END
				FETCH NEXT FROM CursorFindOper INTO @FindNextOperator
			END   
			CLOSE CursorFindOper;  
			DEALLOCATE CursorFindOper; 

			IF LEN(@MatchedOperator) = 0
			BEGIN
				/* 
					To support the below exception when you select 'is null' or 'is not null' from Quick Search in UI
					i.e.
					Where (Customer.tin is null or Len(Customer.tin) = 0)
					Where (Customer.tin is not null or Len(Customer.tin) > 0)

				 */				
				IF SUBSTRING(@InputStr,@NextPos,6) IN (') > 0)',') = 0)')
				BEGIN
					GOTO FindNext				
				END
				ELSE
				BEGIN 	
				SET @ValidationErr = 'SQL statement contains operator not supported for NPI field'				
					BREAK
				END
			END

			/* -- Start Position of the right hand side -- */
			SET @StartPos = 0
			Set @NextPos = @NextPos + LEN(@MatchedOperator+'x') - 1		
						
			DECLARE @NextChar char				
			IF @NextPos <= len(@InputStr)
			BEGIN				
				SET @NextChar = SUBSTRING(@InputStr,@NextPos,1)
				IF @NextChar = ''''
				BEGIN								
					SET @StartPos = @NextPos	
				END								
			END

			IF @StartPos = 0
			BEGIN 
				GOTO FindNext
			END

			-----------------------------------------------------------------
			/*
				Last Position of the right hand side				
			    Find next character until ' single quote
			*/
			SET @EndPos =  0
			SET @NextPos = @NextPos + 1			
			WHILE @NextPos <= len(@InputStr)
			BEGIN
				SET @NextChar = SUBSTRING(@InputStr,@NextPos,1)
				IF @NextChar = ''''
					BEGIN					
						SET @EndPos =  @NextPos + 1
						BREAK									
					END						
				ELSE
					BEGIN						
						SET @NextPos = @NextPos + 1						
					END
			END	
					
			IF @EndPos = 0 
			BEGIN 
				GOTO FindNext
			END

			SET @NumberOfChars = @EndPos - @StartPos
			SET @StringPattern = SUBSTRING(@InputStr,@StartPos,@NumberOfChars)
			SET @StringReplacement = (SELECT pcdb.dbo.FPEProtectEx(@StringPattern, @librarycontext))			
			SET @InputStr = STUFF(@InputStr, @StartPos, LEN(@StringPattern), @StringReplacement) 		
		
			/* -- N-th matching position, Continue the loop until end of character in @InputStr -- */
			FindNext:
			IF LEN(@StringReplacement) = 0
			BEGIN
				SET @CharIndexPos = @CharIndexPos + 1
			END 

			SET @CharIndexPos = CHARINDEX(@FindNextStr,@InputStr, @CharIndexPos+LEN(@StringReplacement))				
		END
		
		IF @ValidationErr <> ''
		BEGIN
			RAISERROR (@ValidationErr,16,1, 'RecursiveReplaceRestrictNPISearch');  
			BREAK
		END  

		FETCH NEXT FROM CursorMain INTO @FindNextStr
	END   
	CLOSE CursorMain;  
	DEALLOCATE CursorMain;  
	SET @OutputStr = @InputStr
END
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[WCDB_GetDecryptionWhereClause]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[WCDB_GetDecryptionWhereClause]
GO
CREATE PROCEDURE [dbo].[WCDB_GetDecryptionWhereClause]
( 
	@WhereClause NVARCHAR(3000),
	@DecryptionWhereClause NVARCHAR(3000) OUTPUT
)
WITH ENCRYPTION AS
SET NOCOUNT ON

BEGIN
	--Get encryption column name list in comma separate values
	DECLARE @EncryptedColumn VARCHAR(8000)
	EXEC dbo.[GetEncryptionColumns]  @EncryptedColumn OUTPUT
	
	--Append decryption function for identified columns 
	IF LEN(@EncryptedColumn) > 0
	BEGIN
		DECLARE @ReplacePattern VARCHAR(8000)
		DECLARE @librarycontext VARCHAR(8000)      
		DECLARE @createfpeobject VARCHAR(MAX) 
		SELECT @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();
		
		DECLARE @RestrictNPISearch BIT = 0

		EXEC [dbo].[IsNPISearchRestricted] @RestrictNPISearch OUT
		
		IF @RestrictNPISearch = 0
		BEGIN
			SET @ReplacePattern = 'pcdb.dbo.FPEAccessEx(%s,'+''''+@librarycontext+''''+')'
			EXEC dbo.RecursiveReplace @WhereClause, @EncryptedColumn, @ReplacePattern, @DecryptionWhereClause OUT			
		END
		ELSE
		BEGIN			
			EXEC dbo.RecursiveReplaceRestrictNPISearch @WhereClause, @EncryptedColumn, @ReplacePattern, @librarycontext, @DecryptionWhereClause OUT			
		END
	END

	RETURN @@ERROR
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WCDB_GetViewData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[WCDB_GetViewData]
GO

Create PROCEDURE [dbo].[WCDB_GetViewData]

	(
	@ViewId   		int,
	@PageNo 		int,
	@RowsPerPage 		int,
	@AllowCheck 		int,
	@OperCode 		Code,
	@QueryPlusMode		int,
	@ArchiveMode		int

	)
  With Encryption As
 	  SET NOCOUNT ON

Declare @SelectClause 		nvarchar(4000)
Declare @FormattingSelectClause	nvarchar(4000)
Declare @Top			int

Declare @TableId		int
Declare @TableName		nvarchar(512)
Declare @ArchiveTableName	nvarchar(512)
Declare @ViewAlias		nvarchar(512)
Declare @FromClause    		nvarchar(4000)

Declare @WhereClause 		nvarchar(3000) 
Declare @RightsWhereClause 	nvarchar(2000)
Declare @DecryptionWhereClause	nvarchar(3000)

Declare @OrderBy		nvarchar(512)
Declare @OrderByReverse 	nvarchar(512)

Declare @ViewQuery     		nvarchar(4000)
Declare @ViewCount     		nvarchar(4000)
Declare @PageQuery 		nvarchar(4000)
Declare @Database		nvarchar(255)

Declare @SelectedViewId		int
Declare @ObjectId               int
Declare @IsColumnsDefined	int
Declare @ViewCategory		int
Declare @EntnumExists		int

set @ViewCategory =0

/* Get View Defintion */
select * from ViewTbl
where ViewId = @ViewId

select @IsColumnsDefined = count(*)  from ViewColumns
where ViewId=@ViewId and (OperCode='Prime' or OperCode= @opercode)


if @IsColumnsDefined=0
  begin
	--Find default view for the given object
	select @ObjectId=NavigationObjectId, @ViewCategory = CategoryId
	from ViewTbl
	where ViewId=@ViewId

	if @ViewCategory in (13,16,17)
	  begin
		--Public User Defined view
		set @SelectedViewId = @ViewId
	  end
	else
	  begin


		select @SelectedViewId = ViewId
		from ViewTbl
		where NavigationObjectId = @ObjectId and 
		      NavigationObjectId = BusinessObjectID and 
		      CategoryId=0 --public default
	  end

			
  end
else
  begin
	set @SelectedViewId = @ViewId
  end


--Get Database Name
select @TableId	= TableId
from ViewTbl
where ViewId= @SelectedViewId

select @Database=DatabaseName
from ListViewTables
where TableId= @TableId

--Select/display all rows for ViewId =132 (sanction data rule)
If @ViewId=132
begin
set @RowsPerPage=0
end

if @QueryPlusMode Not In (6, 7)
  begin
	--Get  View Column Formating 	
	if exists (select GridPosition from ViewColumns 
	where ViewId=@SelectedViewId and OperCode=@OPerCode and CategoryId =2) --Category 2 - private
	  begin
		--Custom Columns layout
		select GridPosition, ColumnId, ViewColumns.TableId, FormatTypeId, GetTotal, GetAverage, PageName, DataType, PageWindowType
		from ViewColumns inner join ListViewColumns 
		on  ViewColumns.ColumnId= ListViewColumns.[Position] and ViewColumns.TableId= ListViewColumns.TableId 
		left outer join ColumnFormats
		on ViewColumns.TableId = ColumnFormats.TableId and
		ViewColumns.ColumnId = ColumnFormats.[Position]
		left Outer join ControlTbl
		on ViewColumns.ControlId= ControlTbl.ControlId
		left Outer Join PageTbl
		on ControlTbl.HLinkPageId = PageTbl.PageId
		
		where ViewId = @SelectedViewId  and 
		(@IsColumnsDefined > 0 and OperCode=@OperCode and CategoryId = 2 or 
		 @IsColumnsDefined = 0 and OperCode='Prime'   and CategoryId in (0,1,10))
		order by GridPosition --always show primary column (GridPosition=0)
	  end
	else if @SelectedViewId < 1000
	  begin
		--Public Column layout for Prime Defined Views
		select GridPosition, ColumnId, ViewColumns.TableId, FormatTypeId, GetTotal, GetAverage, PageName, DataType, PageWindowType
		from ViewColumns inner join ListViewColumns 
		on  ViewColumns.ColumnId= ListViewColumns.[Position]  and ViewColumns.TableId= ListViewColumns.TableId 
		 left outer join ColumnFormats
		on ViewColumns.TableId = ColumnFormats.TableId and
		ViewColumns.ColumnId = ColumnFormats.[Position]
		left Outer join ControlTbl
		on ViewColumns.ControlId= ControlTbl.ControlId
		left Outer Join PageTbl
		on ControlTbl.HLinkPageId = PageTbl.PageId

		where ViewId = @SelectedViewId and OperCode='Prime' and CategoryId in (0,1,10)
		order by GridPosition
	  end 
	else
	 begin
		--Column layout for User Defined Views
		select GridPosition, ColumnId, ViewColumns.TableId, FormatTypeId, GetTotal, GetAverage, PageName, DataType, PageWindowType
		from ViewColumns inner join ListViewColumns 
		on  ViewColumns.ColumnId= ListViewColumns.[Position]  and ViewColumns.TableId= ListViewColumns.TableId 
		 left outer join ColumnFormats
		on ViewColumns.TableId = ColumnFormats.TableId and
		ViewColumns.ColumnId = ColumnFormats.[Position]
		left Outer join ControlTbl
		on ViewColumns.ControlId= ControlTbl.ControlId
		left Outer Join PageTbl
		on ControlTbl.HLinkPageId = PageTbl.PageId

		where (ViewId = @SelectedViewId and OperCode=@OperCode and 
		      CategoryId in (0,1,3,6,7,10)) or --private views
		      (ViewId = @SelectedViewId and @ViewCategory in (13,16,17) and
		      CategoryId in (0,1,2,10,13,16,17)) and
			  ViewColumns.OperCode in (select OperCode
			  from ViewTbl where ViewId = @SelectedViewId) --public views
			  --Take column layout  defined by the 
			  --creator of the public default view.			

		order by GridPosition
	  end 

	----Get View Grouping
	select GridPosition,  GroupingStatusId, OrderStatus
	from ViewGroupingTbl
	where ViewId=@ViewId and OperCode=@OperCode
	order by  GroupingStatusId

end

----Build Select and from clauses
if @PageNo=0
  begin
	----Last Page Optimization 
	set @Top 	=  @RowsPerPage
	
  end
else
  begin
	set @Top = @PageNo * @RowsPerPage
  end

exec WCDB_BuildSQL @ViewId, @OperCode, @Top, @RowsPerPage, @ArchiveMode,
		   @SelectClause  Output, @FormattingSelectClause Output, 
		   @TableName Output, @ArchiveTableName Output, @FromClause Output

-- Defect 6320
-- CTR Reports view should display only the reports with Acknowledgement deatils corrected or the reports are not associated with ACK ID
-- SAR Reports view should display only the reports with Acknowledgement deatils corrected or the reports are not associated with ACK ID
if (@ViewId in (28, 29))
begin
	set @FromClause = ' from (select RegReport.*, 
		Customer.Name AS CustName, 
		SuspiciousActivity.CreateTime AS SuspiActvtyCrtDte, 
		AckFilesRepository.AckErrorCode AS AckErrorCode, AckFilesRepository.AckErrDate AS AckErrDate,
		AckFilesRepository.Id AS AckId, AckFilesRepository.HasErrors AS AckHasErrors
		from PBSA.dbo.RegReport WITH (NOLOCK) 
		Left Outer Join PBSA.dbo.Customer WITH (NOLOCK) on RegReport.Cust = Customer.Id 
		Left Outer Join PBSA.dbo.AckFilesRepository WITH (NOLOCK) on RegReport.RepNo = AckFilesRepository.ReportNumber 
		Left Outer Join PBSA.dbo.SuspiciousActivity WITH (NOLOCK) on RegReport.SArecNo = SuspiciousActivity.recno
		where RegReport.RepNo Not in (Select AckFilesRepository.ReportNumber from AckFilesRepository with (nolock) where IsNull(AckFilesRepository.HasErrors ,0) = 1)
		)  As RegReport '
end


----Build Where Clause
if @QueryPlusMode = 5 --View All
  begin
	--Check if public parameters exist
	--View all
	if exists (select * from ViewParameters where ViewId=@ViewId and CategoryId in (0,1,3,6,7,13,16,17) )
		exec WCDB_QueryPlus @ViewId, @OperCode, 4,	 @WhereClause Output --4 - view filtered
	else
		set @WhereClause = ''
  end
  else
   begin
	if @QueryPlusMode = 7 --Scroll Checked
	  begin
		--Add Query Plus where clause
		exec WCDB_ScrollChecked @ViewId, @OperCode, @PageNo, @WhereClause Output
	  end
	else
	  begin
 		if @viewID=133 --Hidden OFAC Search Results View ID = 133
			--Add Query Plus where clause from the Main OFAC Search View ID = 100
			exec WCDB_QueryPlus 100, @OperCode, @QueryPlusMode,	 @WhereClause Output
		else
			--Add Query Plus where clause
			exec WCDB_QueryPlus @ViewId, @OperCode, @QueryPlusMode,	 @WhereClause Output

	  end	
   end
if Len(@WhereClause)>0
 begin
	-- QueryPlusMode=2 indicates Sql query search and when search is performed from Sql Query do not add "where"
	If @QueryPlusMode!=2
	Begin
		if CHARINDEX('where', @WhereClause)=0 or SUBSTRING(LTRIM(@WhereClause),1,5) != 'where'  					
		set @WhereClause =  ' where (' + @WhereClause + ') '	
	End	
	
	if exists (select 1 from pcdb.dbo.Customobjects with (nolock) where code='VoltageEncryptionConfig' and enabled=1)
	Begin
		exec WCDB_GetDecryptionWhereClause @WhereClause, @DecryptionWhereClause Output
	End

	if len(@DecryptionWhereClause) > 0	
		set @WhereClause = @DecryptionWhereClause
	
  end

---Add Rights Where Clause
-- ignore Rights Where for all My Task Views

if not @ViewId in (31,32,33,34,35,36,37,38)
 begin
	exec WCDB_GetRightsWhereClause @SelectedViewId, @OperCode, @ArchiveMode, @RightsWhereClause Output

	if Len(@RightsWhereClause)> 0 
  	 begin
	   if Len(@WhereClause)>0
	    begin
		set @WhereClause = @WhereClause + ' and ( ' + @RightsWhereClause + ') '	  
	    end
	else
	  begin
	     set @WhereClause = ' where ' + @RightsWhereClause 
	  end
	end
end


if @ArchiveMode = 1 and Len(@WhereClause)>0
 begin
	--Replace Current Table by Archive Table
	--'.' delimiter used to protect the case when 
	--table name incapsulated in the column name 
	--like in Alert.AlertNo
	set @WhereClause =Replace(@WhereClause, 
	@TableName + '.', @ArchiveTableName + '.' )

	--Cover the case when table name inclosed into brackets
	--Replace Current [Table] by [Archive Table]	
	set @WhereClause =Replace(@WhereClause, 
	'[' + @TableName + '].', '[' + @ArchiveTableName + '].' )	   		

 end


----Order by clause to view Page

set @OrderBy = ''

exec WCDB_GetOrderClause @ViewId, @OperCode,@OrderBy  Output, @OrderByReverse Output


----Build View Query
if @ArchiveMode = 1
	set @ViewAlias = @ArchiveTableName
else
	set @ViewAlias = @TableName



if @QueryPlusMode=9
 begin

	Declare @SystemRef varchar(2000)
	Declare @vwOFACSearchResults varchar(2000)
	Declare @vwResultsWhere		 varchar(2000)
	Declare @vwOperCodeWhere	 varchar(2000)


	--Hidden OFAC Search Results (View ID = 133) uses System Reference 
	--from the Main OFAC Search Results (View ID = 100)
	if @viewid=133 
		select  @SystemRef = ParameterValue
		from ViewParameters
		where ViewId =100 and OperCode = @OperCode and ParameterId = -2
    else
		select  @SystemRef = ParameterValue
		from ViewParameters
		where ViewId =@ViewId and OperCode = @OperCode and ParameterId = -2
	

 if @ViewId in (130, 131)
	 begin
		set @vwOFACSearchResults = '(select Distinct EntNum, ' +
								   'OriginalSDNName, MatchName ' +
								   'from OFACSearchResults'
     end
	else
	 begin 
		set @vwOFACSearchResults = '(select Distinct EntNum ' +
								   'from OFACSearchResults'
	  end

	set @vwResultsWhere = ''

	if @SystemRef <> '' and @SystemRef is not null
     begin
		set @vwResultsWhere = 
			' where OFACSearchResults.SystemRef = ' + 
				'''' +  @SystemRef + ''''
     end

	--Do not use Oper code for 
	--All Parties Inquiry Log (view id 129) and
	--All Names Inquiry Log   (view id 131)
	if @ViewID not in (129, 131)
	  begin
			set @vwOperCodeWhere = 'OFACSearchResults.OperCode = ' +
								   '''' + @operCode + ''' '
			if (@vwResultsWhere != '')
				set @vwResultsWhere = @vwResultsWhere +
					' and ' +  @vwOperCodeWhere 
			else
				set @vwResultsWhere = ' where '	+  @vwOperCodeWhere 

       end

	set @vwOFACSearchResults = @vwOFACSearchResults + 
			@vwResultsWhere + ') As vwOFACSearchResults ' 

	-- see if there are entnums
	
	if exists (select top 1 1 from OFAC.dbo.ofacsearchresults with (nolock) where systemref = @SystemRef
	and ISNULL(entnum,0) = 0)
	
	begin
		-- use old logic
		set @EntnumExists = 0
	end
	else
	begin
	    -- use new logic
		set @EntnumExists = 1
	end

	if @ViewId in (130, 131)
	 begin
		
		--use old logic - no entnums
		if @EntnumExists = 0
			
		begin
	 		--Search through OFAC SDNNamesView
			set @FromClause=@FromClause + ' inner join OFACSearchResults ' +
			'on (OFACSearchResults.MatchName = SDNNamesView.[Name] or ' +
		    'OFACSearchResults.OriginalSDNName = SDNNamesView.[Name]) and ' +
			'OFACSearchResults.Program = SDNNamesView.Program and ' +
			'OFAC.dbo.OFS_IsListTypeIncluded(''' + @SystemRef  + ''', SDNNamesView.ListType, SDNNamesView.SDNType) !=0 ' 

			if @ViewId <> 131 --Do not use Oper code for All Names Inquiry Log (view id 131)
				set @FromClause=@FromClause + 'and OFACSearchResults.OperCode = ''' + @operCode + ''' '

			if @SystemRef <> '' and @SystemRef is not null
				set @FromClause=@FromClause + ' and OFACSearchResults.SystemRef = ''' + @SystemRef  + ''' '

			set @SelectClause=Replace(@SelectClause, 'select', 'select distinct')
			set @FormattingSelectClause=Replace(@FormattingSelectClause, 'select', 'select distinct')
			set @ViewCount  =  'select count(*) from ('  + @SelectClause + @FromClause + @WhereClause + ') As ViewQuery'
			set @ViewCount  = Replace(@ViewCount, ' top ' + Convert(varchar, @Top), ' ')
	 
		end
		else
		begin
		--use new logic - we have entnums
		
		--Search through OFAC SDNNamesView
			set @FromClause=@FromClause + 
			' inner join ' + @vwOFACSearchResults +
			'on (vwOFACSearchResults.MatchName = SDNNamesView.[Name] or ' +
			'vwOFACSearchResults.OriginalSDNName = SDNNamesView.[Name]) and ' +
			'vwOFACSearchResults.EntNum = SDNNamesView.EntNum and ' +
			'OFAC.dbo.OFS_IsListTypeIncluded(''' + @SystemRef  + ''', SDNNamesView.ListType, SDNNamesView.SDNType) !=0 ' 

			set @SelectClause=Replace(@SelectClause, 'select', 'select distinct')
			set @FormattingSelectClause=Replace(@FormattingSelectClause, 'select', 'select distinct')
			set @ViewCount  =  'select count(*) from ('  + @SelectClause + @FromClause + @WhereClause + ') As ViewQuery'
			set @ViewCount  = Replace(@ViewCount, ' top ' + Convert(varchar, @Top), ' ')
	 
		end
	 
	 end
	else
	 begin
	 		
		--use old logic - no entnums
		if @EntnumExists = 0
		
		begin
			
			set @FromClause=@FromClause + ' inner join OFACSearchResults ' +
			'on OFACSearchResults.OriginalSDNName = SDNTable.[Name] and ' +
			'OFACSearchResults.Program = SDNTable.Program and ' + 
			'OFAC.dbo.OFS_IsListTypeIncluded(''' + @SystemRef  + ''', SDNTable.ListType, SDNTable.SDNType) != 0 ' 

			if @ViewId = 129 
			begin
				--Do not use Oper code and do not show deleted items 
				--for All Parties Inquiry Log (view id 129)
				set @FromClause=@FromClause + ' and SDNTable.Status <> 4 '
			end
			else
			begin
				set @FromClause=@FromClause + 'and OFACSearchResults.OperCode = ''' + @operCode + ''' '
			 end


			if @SystemRef <> '' and @SystemRef is not null
				set @FromClause=@FromClause + ' and OFACSearchResults.SystemRef = ''' + @SystemRef  + ''' '

			set @ViewCount  =  'select count(*) ' + @FromClause   + @WhereClause
					
		end
		
		else
		--use new logic - we have entnums
		begin 
	 
			set @FromClause=@FromClause + 
			' inner join ' + @vwOFACSearchResults +
			'on vwOFACSearchResults.EntNum = SDNTable.EntNum and ' +
			'OFAC.dbo.OFS_IsListTypeIncluded(''' + @SystemRef  + ''', SDNTable.ListType, SDNTable.SDNType) != 0 ' 

			if @ViewId = 129 
			begin
				--Do not show deleted items 
				--for All Parties Inquiry Log (view id 129)
				set @FromClause=@FromClause + ' and SDNTable.Status <> 4 '
			end

			set @ViewCount  =  'select count(*) ' + @FromClause   + @WhereClause
		
		end
	end	
 end
else if CharIndex( 'Documentation', @FromClause) > 0 and
		CharIndex( 'Attachment', @FromClause) >0 
  begin
		--Obtain correct count for Documentation join with Attachments
		set @ViewCount  =  'select count(Distinct Documentation.RecNo) ' + 
							@FromClause   + @WhereClause
  end
else
 begin
	set @ViewCount  =  'select count(*) ' + @FromClause   + @WhereClause
 end


	if @PageNo=1 
	  begin
		/* Optimization for the first page  */
		set @ViewQuery =  @FormattingSelectClause + @FromClause  + @WhereClause  + @OrderBy
	  end
	else if @PageNo=0 
	  begin
		/* Optimization for the last page */		
		set @ViewQuery =  @FormattingSelectClause + @FromClause   +  @WhereClause + @OrderByReverse 		 
	  end
	else
	  begin

		Declare @OrderByColumnExpression nvarchar(2000)
		exec WCDB_GetOrderbyColumnExpression @ViewId, @OperCode, 
		@SelectClause,	@OrderBy, @OrderByColumnExpression OUTPUT


		if @ArchiveMode = 1
		  begin
			--If search the Archive Table Only (@ArchiveMode = 1)
			--replace Table Name with Archived Table name in the
			--order by clause with column expressions
			set @OrderByColumnExpression =Replace(@OrderByColumnExpression, 
			@TableName + '.' , @ArchiveTableName + '.')
		  end

		set @ViewQuery = @SelectClause + @FromClause  +  @WhereClause  + @OrderByColumnExpression
		

		if @ViewAlias='AccountOwner' or @ViewAlias='[AccountOwner]'
		  begin
			set @FormattingSelectClause=Replace(@FormattingSelectClause, 'Account.', 'AccountOwner.')
			set @FormattingSelectClause=Replace(@FormattingSelectClause, 'Customer.', 'AccountOwner.')
		  end

		set  @PageQuery =  @FormattingSelectClause +
		'from  (' + @ViewQuery +  ') As '  + @ViewAlias + ' '   + @OrderByReverse 

	  end

	



If @tableid in (112, 113) --Creates a temporary table for OFAC Cases (Current and Archived)
Begin

    CREATE TABLE #tmpListTypes (ListType VARCHAR(100))
	-- Gets the List types available for the operator
	INSERT INTO #tmpListTypes 
	SELECT * From OFAC.dbo.fn_GetListTypesForOper(@opercode)

End

---Debug Query Builder
--select @ViewQuery

-- check for sql injection
declare @RestictedListRC int
if @WhereClause <> ''
begin
	set @RestictedListRC = dbo.WCDB_fnRestrictedSQLTermsListCheck(@WhereClause)
	if @RestictedListRC = 1
	begin
		If @tableid in (112, 113) --Deletes the temporary table createed for OFAC Cases (Current and Archived)
		Begin
		if object_id('tempdb..#tmpListTypes') is not null 
			DROP TABLE #tmpListTypes
		End
		If @tableid in (136, 156, 152) --Deletes the temporary table created for SDNParties (Current and Archived)
		BEGIN
		if object_id('tempdb..#SDNPartiesListTypes') is not null 
			DROP TABLE #SDNPartiesListTypes
		END
		return 250002	
	end
end

if @PageNo=1 
  begin
	 ----Optimized for the First Page 
	
	--high volume exec [PRIME-DEV-DORIS].pbsa.dbo.sp_executesql @ViewQuery 
	
	
	--Debug Block
	--select @ViewQuery	
--	select @TableId
--	select @ViewQuery
--	select @Database
			
	--exec pbsa.dbo.sp_executesql @ViewQuery
	exec WCDB_ExecQuery @ViewQuery,	@Database

	-----Get Records Total			
	if @QueryPlusMode = 7 --Scroll Checked
	  begin
		select Count(RowId)
		from ViewCheckRows
		where ViewId=@ViewId and OperCode=@OperCode
	  end
	else
	  begin

		--select @viewcount
		--high volume exec [PRIME-DEV-DORIS].pbsa.dbo.sp_executesql  @ViewCount 
		--exec pbsa.dbo.sp_executesql  @ViewCount 
		exec WCDB_ExecQuery @ViewCount,	@Database
		
	  end


 end
else
begin
 	if @PageNo=0
	  begin		
		 --Optimized for the Last Page 
		
		--High volume exec [PRIME-DEV-DORIS].pbsa.dbo.sp_executesql @ViewQuery
		--exec pbsa.dbo.sp_executesql @ViewQuery
		exec WCDB_ExecQuery @ViewQuery,	@Database

	  end
	else
	  begin
		
		--High Volume exec [PRIME-DEV-DORIS].pbsa.dbo.sp_executesql @PageQuery
		 --exec pbsa.dbo.sp_executesql @PageQuery
		exec WCDB_ExecQuery @PageQuery,	@Database

		--select @PageQuery
	 end
	
	select 0 -- number of records queried only for first page

end
If @tableid in (112, 113) --Deletes the temporary table createed for OFAC Cases (Current and Archived)
Begin
	if object_id('tempdb..#tmpListTypes') is not null 
		DROP TABLE #tmpListTypes
End

--CD10/28/2010 New code to improve performance for SDN Parties
If @tableid in (136, 156, 152) --Deletes the temporary table created for SDNParties (Current and Archived)
BEGIN
	if object_id('tempdb..#SDNPartiesListTypes') is not null 
		DROP TABLE #SDNPartiesListTypes

END


if @AllowCheck = 0
  begin
	select  '', 0
  end
else
  begin
	select RowId, CheckStatus
	from ViewCheckRows
	where ViewId=@ViewId and OperCode=@OperCode
	order by RowId
  end


--Get View Actions
select TaskTbl.TaskId, TaskTbl.TaskName, TaskTbl.ToolTipText, 
       TaskTbl.HLinkPageId, TaskTbl.HLinkMethodId,  TaskTbl.PageMode,
       PageTbl.PageName, PageTbl.PageType,
       TaskTbl.Rights, TaskTbl.License, PageWindowType, ViewTasks.PageId
from TaskTbl inner join PageTbl 
     on TaskTbl.HLinkPageId = PageTbl.PageId 
     inner join ViewTasks
     on TaskTbl.TaskId=ViewTasks.TaskId
where ViewTasks.ViewId=@ViewId
order by TaskTbl.SortOrder

GO

-- Defect 6321
if exists (select * from listviewcolumns with(nolock) where Displayname = 'CIFFieldsDelimiter')
begin
	update listviewcolumns set columnname = 'CIFFieldsDelimiter' where Displayname = 'CIFFieldsDelimiter'
end
Go

--Defect 6315, 6336, 6329
-- Removal of the parameter value set for the report number
if exists (select 1 from ViewParameters with (nolock) where ColumnId is null and TableId is null and ViewId in (951, 952))
begin
	update ViewParameters set parametervalue = null where ColumnId is null and TableId is null and ViewId in (951, 952)
end
Go

-- Remove existing ViewParameterSelections value set for Viewid 951 and 952
if exists (Select 1 from ViewParameterSelections with (nolock) where viewid in (951, 952) and OperCode <> 'Prime')
Begin
	Delete from ViewParameterSelections where viewid in (951, 952) and OperCode <> 'Prime'
End	
Go

-- Remove existing ViewParameters value set for Viewid 951 and 952
if exists (Select 1 from ViewParameters with (nolock) where viewid in (951, 952) and OperCode <> 'Prime')
begin
	Delete from ViewParameters where viewid in (951, 952) and OperCode <> 'Prime'
end	
Go

-- CTR Report with errors
If Not Exists (Select 1 from BusinessObjectTbl with(nolock) WHERE ObjectId = 929)
Begin
Insert into BusinessObjectTbl (ObjectId, ObjectName, HLinkPageId, HLinkMethodId, Rights, SortOrder, CategoryId)
Values (929, 'BSA Government CTR Report with Errors', 5, 4, 'DSPREGRPT,MODREGRPT,FINREGRPT,FILEREGRPT',1,0)
end
Go

-- SAR Report with errors
If Not Exists (Select 1 from BusinessObjectTbl with(nolock) WHERE ObjectId = 930)
Begin
Insert into BusinessObjectTbl (ObjectId, ObjectName, HLinkPageId, HLinkMethodId, Rights, SortOrder, CategoryId)
Values (930, 'BSA Government SAR Report with Errors', 5, 4, 'DSPREGRPT,MODREGRPT,FINREGRPT,FILEREGRPT',1,0)
end
Go

If Exists (Select 1 from ViewTbl with(nolock) WHERE Viewid = 951)
Begin
Update ViewTbl Set BusinessObjectId = 929, CategoryId = 0 where Viewid = 951 
end

If Exists (Select 1 from ViewTbl with(nolock) WHERE Viewid = 952)
Begin
Update ViewTbl Set BusinessObjectId = 930, CategoryId = 0 where Viewid = 952
end
Go

-- Query Plus Parameters configuration for Object ID 929
If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 929 AND ParameterId = 1)
Begin
		insert into [pcdb].[dbo].QueryPlusParameters values (929,1,59,0,0)
End	
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 929 AND ParameterId = 2)
Begin
		insert into [pcdb].[dbo].QueryPlusParameters values (929,2,59,2,0) 
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 929 AND ParameterId = 3)
Begin
		insert into [pcdb].[dbo].QueryPlusParameters values (929,3,59,3,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 929 AND ParameterId = 4)
Begin
		insert into [pcdb].[dbo].QueryPlusParameters values (929,4,59,4,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 929 AND ParameterId = 5)
Begin
		insert into [pcdb].[dbo].QueryPlusParameters values (929,5,59,10,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 929 AND ParameterId = 6)
Begin
		insert into [pcdb].[dbo].QueryPlusParameters values (929,6,59,1,0) 
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 929 AND ParameterId = 7)
Begin
		insert into [pcdb].[dbo].QueryPlusParameters values (929,7,59,5,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 929 AND ParameterId = 8)
Begin
		insert into [pcdb].[dbo].QueryPlusParameters values (929,8,59,7,0) 
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 929 AND ParameterId = 9)
Begin
		insert into [pcdb].[dbo].QueryPlusParameters values (929,9,59,30,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 929 AND ParameterId = 10)
Begin
		insert into [pcdb].[dbo].QueryPlusParameters values (929,10,59,11,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 929 AND ParameterId = 11)
Begin
		insert into [pcdb].[dbo].QueryPlusParameters values (929,11,59,32,0) 
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 929 AND ParameterId = 12)
Begin
		 insert into [pcdb].[dbo].QueryPlusParameters values (929,12,59,33,0) 
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 929 AND ParameterId = 13)
Begin
		insert into [pcdb].[dbo].QueryPlusParameters values (929,13,59,34,0)
End
Go

-- Query Plus Parameters configuration for Object ID 930
        

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 930 AND ParameterId = 1)
Begin
	insert into [pcdb].[dbo].QueryPlusParameters values (930,1,59,0,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 930 AND ParameterId = 2)
Begin
	insert into [pcdb].[dbo].QueryPlusParameters values (930,2,59,2,0)
End
Go
If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 930 AND ParameterId = 3)
Begin
	insert into [pcdb].[dbo].QueryPlusParameters values (930,3,59,3,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 930 AND ParameterId = 4)
Begin
	insert into [pcdb].[dbo].QueryPlusParameters values (930,4,59,4,0)
End
Go	    	    

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 930 AND ParameterId = 5)
Begin
	insert into [pcdb].[dbo].QueryPlusParameters values (930,5,59,10,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 930 AND ParameterId = 6)
Begin
	insert into [pcdb].[dbo].QueryPlusParameters values (930,6,59,1,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 930 AND ParameterId = 7)
Begin
	insert into [pcdb].[dbo].QueryPlusParameters values (930,7,59,5,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 930 AND ParameterId = 8)
Begin
	insert into [pcdb].[dbo].QueryPlusParameters values (930,8,59,7,0) 
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 930 AND ParameterId = 9)
Begin
	insert into [pcdb].[dbo].QueryPlusParameters values (930,9,59,30,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 930 AND ParameterId = 10)
Begin
	insert into [pcdb].[dbo].QueryPlusParameters values (930,10,59,11,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 930 AND ParameterId = 11)
Begin
	insert into [pcdb].[dbo].QueryPlusParameters values (930,11,59,32,0)
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 930 AND ParameterId = 12)
Begin
	insert into [pcdb].[dbo].QueryPlusParameters values (930,12,59,33,0) 
End
Go

If Not Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 930 AND ParameterId = 13)
Begin
	insert into [pcdb].[dbo].QueryPlusParameters values (930,13,59,34,0)
End
Go

-- Rmove Query Plus parameters for 1100
If Exists (Select 1 from QueryPlusParameters with(nolock) WHERE ObjectId = 1100)
begin
	delete from QueryPlusParameters WHERE ObjectId = 1100
end
Go

-- Removal of the BussinessObjectId 1100
if exists (select 1 from BusinessObjectTbl with(nolock) where ObjectId = 1100)
begin
	delete from BusinessObjectTbl where ObjectId = 1100
end
Go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WCDB_ClearViewControls]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[WCDB_ClearViewControls]
GO

CREATE PROCEDURE dbo.WCDB_ClearViewControls

 (
	@ViewId 		 int,	
	@OperCode 		 Code
)

With Encryption As

  Declare @trnCnt int

  select @trnCnt = @@trancount	-- Save the current trancount

  if @trnCnt = 0
	-- Transaction has not begun
	begin tran WCDB_ClearViewControls
  else
    -- Already in a transaction
	save tran  WCDB_ClearViewControls

	if (@ViewId = 9)
		begin
			delete from ViewParameterSelections
			where (ViewId in (@ViewId, 0, 951, 952, 910)) and 
				OperCode = @OperCode and
				ParameterId in (select ParameterID from ViewParameters 
				where (ViewId in (@ViewId, 0, 951, 952, 910)) and 
				OperCode = @OperCode and CategoryID=2)  --2- Temporary Query Plus Parameters

			delete from ViewParameters 
			where (ViewId in (@ViewId, 0, 951, 952, 910)) and
				OperCode = @OperCode and CategoryID=2
		end

	else
		begin
			delete from ViewParameterSelections
			where 	(ViewId = @ViewId  or @ViewId=0) and 
				OperCode = @OperCode and
				ParameterId in (select ParameterID from ViewParameters 
				where ( ViewId = @ViewId  or @ViewId=0) and 
				OperCode = @OperCode and CategoryID=2)  --2- Temporary Query Plus Parameters

			delete from ViewParameters 
			where (ViewId = @ViewId  or @ViewId=0) and 
				OperCode = @OperCode and CategoryID=2
		end	


	  if @@error <> 0
		  rollback tran  WCDB_ClearViewControls
	  else	 
	    begin		 
		  if @trnCnt = 0
			commit tran  WCDB_ClearViewControls
	    end
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WCDB_SaveQueryPlusParameter]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[WCDB_SaveQueryPlusParameter]
GO

CREATE PROCEDURE dbo.WCDB_SaveQueryPlusParameter

 (
	@ObjectId 		 int,
	@ParameterId 		 int,
	@OperCode 		 Code,
	@ParameterOperation      int,
	@ParameterValue 	 varchar(3000),
	@ParameterRangeValue	 varchar(255)
	
 
)

With Encryption As

Declare @HistoryId int

  declare @trnCnt int


  select @trnCnt = @@trancount	-- Save the current trancount

  if @trnCnt = 0
	-- Transaction has not begun
	begin tran WCDB_SaveQueryPlusParameter
  else
    -- Already in a transaction
	save tran WCDB_SaveQueryPlusParameter

	Declare @ViewId       int
	Declare @TableId      int
	Declare @ColumnId   int
	Declare @cnt 	        int


	select  @ViewId=ViewId
	from ViewTbl
	where  BusinessObjectId = @ObjectId and CategoryId=0  --default view

	delete from ViewParameterSelections
	where 	ViewId = @ViewId and 
		ParameterId = @ParameterId and
		OperCode = @OperCode



	if @ParameterValue is null and @ParameterOperation not in (9,10) --is null, is  not null
	  begin

			delete from ViewParameters
			where 	ViewId = @ViewId and 
				ParameterId = @ParameterId and
				OperCode = @OperCode

	  end
	else
	  begin

		if Len(@ParameterValue)=0 and @ParameterOperation not in (9,10) --is null, is  not null
		  begin

			delete from ViewParameters
			where 	ViewId = @ViewId and 
				ParameterId = @ParameterId and
				OperCode = @OperCode

			
		  end
		else
		  begin
			
			select  @cnt = count(*)
			from ViewParameters
			where 	ViewId = @ViewId and 
				ParameterId = @ParameterId and
				OperCode = @OperCode

			if @cnt=0
			  begin


				select @ColumnId=ColumnId, @TableId=TableId
				from QueryPlusParameters
				where ObjectId=@ObjectId and ParameterId = @ParameterId 

				if (@ViewId = 951 or @ViewId = 952)
					begin
						if not exists(Select 1 from ViewParameters with (nolock) where 
									ViewId = @ViewId and ColumnId = @ColumnId and TableId = @TableId
									and SQLOperationId = @ParameterOperation and ParameterValue = @ParameterValue)
									Begin
										insert into ViewParameters
										values(@ViewId, @ParameterId,  @OperCode,
										@ColumnId, @TableId,@ParameterOperation,
										@ParameterValue, 0, 1,2) 

										insert into ViewParameterSelections
										values	(@ViewId,  @ParameterId, @OperCode, 1, @ParameterRangeValue, 'to')										
									End
					end
				else
				begin
					insert into ViewParameters
					values(@ViewId, @ParameterId,  @OperCode,
					@ColumnId, @TableId,@ParameterOperation,
					@ParameterValue, 0, 1,2) 				

					insert into ViewParameterSelections
						values	(@ViewId,  @ParameterId, @OperCode, 1, @ParameterRangeValue, 'to')
				end
			  end
			else
			  begin
				
				update ViewParameters
				set ParameterValue = @ParameterValue,
				      SQLOperationId=@ParameterOperation
				where 	ViewId = @ViewId and 
					ParameterId = @ParameterId and
					OperCode = @OperCode

				insert into ViewParameterSelections
				values	(@ViewId,  @ParameterId, @OperCode, 1, @ParameterRangeValue, 'to')
			  end
		  end	
                end

	  if @@error <> 0
		  rollback tran WCDB_SaveQueryPlusParameter
	  else	 
	    begin		 
		  if @trnCnt = 0
			commit tran WCDB_SaveQueryPlusParameter
	    end

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WCDB_fnGetVoltageError]') AND  type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP Function [dbo].[WCDB_fnGetVoltageError]
GO
--	USER DEFINED FUNCTIONS  
CREATE function [dbo].[WCDB_fnGetVoltageError](@VoltageError nvarchar(max) ,@inputText nvarchar(max),@method nvarchar(20) )
Returns nvarchar(max)
AS BEGIN
    	DECLARE @ErrorTxt nvarchar(max) 
		if  @VoltageError like '%VE_ERROR_FPE_INPUT_LENGTH_TOO_SHORT%' or @VoltageError like '%VE_ERROR_FPE_INPUT_LENGTH_ZERO%'
		begin
		Set @ErrorTxt=@inputText
		end
		else
		begin
		if @method='FPEProtect'
		Set @ErrorTxt=' '
		else
		Set @ErrorTxt='DATA UNAVAILABLE-CONTACT SUPPORT'

		Declare @evterror varchar(2000)=@VoltageError
		EXEC master.dbo.xp_logevent 60000, @evterror, Error
		end
	RETURN @ErrorTxt
END
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FPEAccessEx]') and type IN (N'FN'))
drop function [dbo].[FPEAccessEx]
GO

CREATE FUNCTION dbo.[FPEAccessEx] (
   @cipherText [nvarchar](max),
   @librarycontextStatus [nvarchar](max)
   )
RETURNS [nvarchar](max) 
WITH ENCRYPTION
AS
BEGIN
DECLARE @exText as [nvarchar](max) 
IF @librarycontextStatus like '%VE_ERROR%'
BEGIN
	set @exText='DATA UNAVAILABLE-CONTACT SUPPORT'
	Declare @evterror varchar(2000)=@librarycontextStatus
	EXEC master.dbo.xp_logevent 60000, @evterror, Error
END
ELSE
BEGIN
SELECT @exText = Case When @cipherText is null then null else dbo.FPEAccess(@cipherText) end
IF @exText like '%VE_ERROR%'
	set @exText= [dbo].[WCDB_fnGetVoltageError](@exText,@cipherText,'FPEAccess')
END
RETURN @exText
END
Go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FPEProtectEx]') and type IN (N'FN'))
drop function [dbo].[FPEProtectEx]
GO

CREATE FUNCTION dbo.[FPEProtectEx] (
   @plainText [nvarchar](max),
   @librarycontextStatus [nvarchar](max)
   )
RETURNS [nvarchar](max) 
WITH ENCRYPTION
AS
BEGIN
DECLARE @exText as [nvarchar](max) 
IF @librarycontextStatus like '%VE_ERROR%'
BEGIN
	set @exText=' '
	Declare @evterror varchar(2000)=@librarycontextStatus
	EXEC master.dbo.xp_logevent 60000, @evterror, Error
END
ELSE
BEGIN
SELECT @exText = [dbo].[FPEProtect](@plainText)
IF @exText like '%VE_ERROR%'
	set @exText= [dbo].[WCDB_fnGetVoltageError](@exText,@plainText,'FPEProtect')
END
RETURN @exText
END
Go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CDB_DecryptData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CDB_DecryptData]
GO
Create Procedure [dbo].[CDB_DecryptData](@var1 as varchar(max), @var2 as varchar(max)=null, @var3 as varchar(max)=null)
As
begin
      declare @librarycontext varchar(max)
      declare @createfpeobject varchar(max)
      select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();

      select case when isnull(@var1,'') ='' then '' else pcdb.dbo.FPEAccessEx(@var1,@librarycontext) end as 'var1',
	  case when isnull(@var2,'') ='' then '' else pcdb.dbo.FPEAccessEx(@var2,@librarycontext) end as 'var2',
	  case when isnull(@var3,'') ='' then '' else pcdb.dbo.FPEAccessEx(@var3,@librarycontext) end as 'var3'
  end
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CDB_EncryptData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CDB_EncryptData]
GO

Create Procedure [dbo].[CDB_EncryptData](@var1 as varchar(max), @var2 as varchar(max)=null, @var3 as varchar(max)=null)
As
begin
      declare @librarycontext varchar(max)
      declare @createfpeobject varchar(max)

  select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();

     select case when isnull(@var1,'') ='' then '' else pcdb.dbo.FPEProtectEx(@var1,@librarycontext) end as 'var1',
	 case when isnull(@var2,'') =''  then '' else pcdb.dbo.FPEProtectEx(@var2,@librarycontext) end as 'var2',
	 case when isnull(@var3,'') ='' then '' else pcdb.dbo.FPEProtectEx(@var3,@librarycontext) end as 'var3'
  end
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WCDB_fnCreateLibraryContextFPE]') AND  type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP Function [dbo].[WCDB_fnCreateLibraryContextFPE]
GO
-- Function is created to access voltage server by creating global librarycontext 
--and createfpeobject with respective credentials for encryption/decryption

Create FUNCTION [dbo].[WCDB_fnCreateLibraryContextFPE]()
RETURNS @librarycontext_and_createfpe TABLE 
(librarycontext varchar(max),
    createfpeobject varchar(max))
/*
This function returns a table of two column containing the details about creating librarycontext and fpe object 
- initLibraryContext  creates a global LibraryContext object using provided values.
- createFPE  creates a global FPE object using provided values and the created LibraryContext object.

*/
WITH ENCRYPTION
AS 

BEGIN
 declare @librarycontext varchar(max)
 declare @createfpeobject varchar(max)
 declare @policyurl nvarchar(256)
 declare @identity nvarchar(100)
 declare @sharedsecret nvarchar(256)
 declare @cachepath nvarchar(256)
 declare @code varchar(50)
 declare @username nvarchar(256)
 declare @pswd nvarchar(256)  
 select @code=code from pcdb.dbo.Customobjects with (nolock) where code='VoltageEncryptionConfig' and enabled=1

 IF(@code<>'')
  BEGIN
    select @policyurl=value from pcdb.dbo.CustomSettings with (nolock) where [key]='policyurl' and code='VoltageEncryptionConfig'
    select @identity=value from pcdb.dbo.CustomSettings with (nolock)  where [key]='identity' and code='VoltageEncryptionConfig'
    select @sharedsecret=value from pcdb.dbo.CustomSettings with (nolock) where [key]='sharedsecret' and code='VoltageEncryptionConfig'
    select @username=value from pcdb.dbo.CustomSettings with (nolock) where [key]='username' and code='VoltageEncryptionConfig'
    select @pswd=value from pcdb.dbo.CustomSettings with (nolock) where [key]='password' and code='VoltageEncryptionConfig'
    select @cachepath=value from pcdb.dbo.CustomSettings with (nolock) where [key]='sqlclrcachepath' and code='VoltageEncryptionConfig'

      ----if the below calls fail error message will be returned which might vary depending on error and passed value is returned on success
      SELECT @librarycontext=pcdb.dbo.CreateLibraryContext(''+@policyurl+'', null, 1, ''+@cachepath+'', 0, 0);
      if(@username<>'' and @pswd<>'')
      SELECT @createfpeobject=pcdb.dbo.CreateFPE(''+@identity+'', 'Auto', null, ''+@username+'', ''+@pswd+'')
      else  
      SELECT @createfpeobject=pcdb.dbo.CreateFPE(''+@identity+'', 'Auto', ''+@sharedsecret+'', null, null)

   END
      -- Insert these value into table
      BEGIN
        INSERT @librarycontext_and_createfpe
        SELECT ISNULL(@librarycontext,''), ISNULL(@createfpeobject,'');
      END;
      RETURN;

END;
GO
/* Defect 6489 */
IF EXISTS (SELECT 1 FROM listviewcolumns WITH(NOLOCK) WHERE TableId = 517 AND Position = 8)
BEGIN
	UPDATE ListViewColumns SET DataType = 'DATETIME' WHERE TableId = 517 AND Position = 8 
END
GO
IF EXISTS (SELECT 1 FROM listviewcolumns WITH(NOLOCK) WHERE TableId = 517 AND Position = 12)
BEGIN
	UPDATE ListViewColumns SET DataType = 'DATETIME' WHERE TableId = 517 AND Position = 12
END
GO

IF NOT EXISTS(select * from sys.sysobjects  where name = 'CodeSpecialChar' and xtype='R')
BEGIN
	DECLARE @sql NVARCHAR(max)
    SET @sql = N'Create rule CodeSpecialChar as @codeSplChar not like ''%[^0-9A-Za-z\-]%''' 
	EXECUTE sp_executesql @sql
END

GO
--Overwrite the rule 'Code' with the new rule 'CodeSpecialChar'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'AddressEquivalents.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'AddressEquivalents.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ChipsAba.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ChipsUid.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ChipsUidDetails.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Country.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CountryCodeType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CountryCodeType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CountryEquivalents.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CountryEquivalents.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CountryName.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CountryName.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Currency.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustInterBranch.Oper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Customer.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerMap.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomObjects.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomObjects.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomSettings.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomSettings.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'FedAba.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ListType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ListType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ListTypeProduct.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ListTypeProduct.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ListValue.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ListValue.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Loro.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'MailingList.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'MLMembership.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PageStateHistoryTbl.OperCode'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Product.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Product.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Swift.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TaskOperators.OperCode'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempPDFTbl.OperCode'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'UserDefaultPageTbl.OperCode'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ViewColumns.OperCode'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ViewJoinTbl.OperCode'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ViewParameters.OperCode'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ViewParameterSelections.OperCode'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ViewRows.OperCode'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ViewTbl.OperCode'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Vostro.LastOper'

EXEC sp_bindrule 'dbo.CodeSpecialChar', 'EventLog.Oper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'EventType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'EventType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ObjectType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ObjectType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'OptionTbl.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'OptionTbl.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Report.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Report.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SysParam.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SysParam.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'WebRequest.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'WebRequest.LastModifyOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'WebRequestMessage.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'WebRequestParameter.CreateOper'

GO

IF EXISTS (SELECT 1 FROM listviewcolumns WITH(NOLOCK) WHERE TableId =240 and Position = 2)
BEGIN
	UPDATE ListViewColumns SET DataType = 'VARCHAR' WHERE TableId =240 and Position = 2
END
GO

IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'Activity' AND ColumnName='InstrumentPrice')
	 UPDATE ListViewColumns SET ColumnExpression='Activity.InstrumentPrice'  WHERE TableName= 'Activity' AND ColumnName='InstrumentPrice'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'Activity' AND ColumnName='InstrumentQty')
	 UPDATE ListViewColumns SET ColumnExpression='Activity.InstrumentQty'  WHERE TableName= 'Activity' AND ColumnName='InstrumentQty'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'ActivityDeleted' AND ColumnName='InstrumentPrice')
	 UPDATE ListViewColumns SET ColumnExpression='ActivityDeleted.InstrumentPrice'  WHERE TableName= 'ActivityDeleted' AND ColumnName='InstrumentPrice'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'ActivityDeleted' AND ColumnName='InstrumentQty')
	 UPDATE ListViewColumns SET ColumnExpression='ActivityDeleted.InstrumentQty'  WHERE TableName= 'ActivityDeleted' AND ColumnName='InstrumentQty'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'ActivityHist' AND ColumnName='InstrumentPrice')
	 UPDATE ListViewColumns SET ColumnExpression='ActivityHist.InstrumentPrice'  WHERE TableName= 'ActivityHist' AND ColumnName='InstrumentPrice'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'ActivityHist' AND ColumnName='InstrumentQty')
	 UPDATE ListViewColumns SET ColumnExpression='ActivityHist.InstrumentQty'  WHERE TableName= 'ActivityHist' AND ColumnName='InstrumentQty'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'CurrentArchActivity' AND ColumnName='InstrumentPrice')
	 UPDATE ListViewColumns SET ColumnExpression='CurrentArchActivity.InstrumentPrice'  WHERE TableName= 'CurrentArchActivity' AND ColumnName='InstrumentPrice'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'CurrentArchActivity' AND ColumnName='InstrumentQty')
	 UPDATE ListViewColumns SET ColumnExpression='CurrentArchActivity.InstrumentQty'  WHERE TableName= 'CurrentArchActivity' AND ColumnName='InstrumentQty'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'MonetaryInstrumentView' AND ColumnName='InstrumentPrice')
	 UPDATE ListViewColumns SET ColumnExpression='MonetaryInstrumentView.InstrumentPrice'  WHERE TableName= 'MonetaryInstrumentView' AND ColumnName='InstrumentPrice'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'MonetaryInstrumentView' AND ColumnName='InstrumentQty')
	 UPDATE ListViewColumns SET ColumnExpression='MonetaryInstrumentView.InstrumentQty'  WHERE TableName= 'MonetaryInstrumentView' AND ColumnName='InstrumentQty'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'MonetaryInstrumentViewHist' AND ColumnName='InstrumentPrice')
	 UPDATE ListViewColumns SET ColumnExpression='MonetaryInstrumentViewHist.InstrumentPrice'  WHERE TableName= 'MonetaryInstrumentViewHist' AND ColumnName='InstrumentPrice'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'MonetaryInstrumentViewHist' AND ColumnName='InstrumentQty')
	 UPDATE ListViewColumns SET ColumnExpression='MonetaryInstrumentViewHist.InstrumentQty'  WHERE TableName= 'MonetaryInstrumentViewHist' AND ColumnName='InstrumentQty'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'SASActivityDetails' AND ColumnName='InstrumentPrice')
	 UPDATE ListViewColumns SET ColumnExpression='SASActivityDetails.InstrumentPrice'  WHERE TableName= 'SASActivityDetails' AND ColumnName='InstrumentPrice'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'SASActivityDetails' AND ColumnName='InstrumentQty')
	 UPDATE ListViewColumns SET ColumnExpression='SASActivityDetails.InstrumentQty'  WHERE TableName= 'SASActivityDetails' AND ColumnName='InstrumentQty'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'UnprocActivity' AND ColumnName='InstrumentPrice')
	 UPDATE ListViewColumns SET ColumnExpression='UnprocActivity.InstrumentPrice'  WHERE TableName= 'UnprocActivity' AND ColumnName='InstrumentPrice'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'UnprocActivity' AND ColumnName='InstrumentQty')
	 UPDATE ListViewColumns SET ColumnExpression='UnprocActivity.InstrumentQty'  WHERE TableName= 'UnprocActivity' AND ColumnName='InstrumentQty'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'vwActivitiesByProfile' AND ColumnName='InstrumentPrice')
	 UPDATE ListViewColumns SET ColumnExpression='vwActivitiesByProfile.InstrumentPrice'  WHERE TableName= 'vwActivitiesByProfile' AND ColumnName='InstrumentPrice'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'vwActivitiesByProfile' AND ColumnName='InstrumentQty')
	 UPDATE ListViewColumns SET ColumnExpression='vwActivitiesByProfile.InstrumentQty'  WHERE TableName= 'vwActivitiesByProfile' AND ColumnName='InstrumentQty'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'vwActivityAudit' AND ColumnName='InstrumentPrice')
	 UPDATE ListViewColumns SET ColumnExpression='vwActivityAudit.InstrumentPrice'  WHERE TableName= 'vwActivityAudit' AND ColumnName='InstrumentPrice'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'vwActivityAudit' AND ColumnName='InstrumentQty')
	 UPDATE ListViewColumns SET ColumnExpression='vwActivityAudit.InstrumentQty'  WHERE TableName= 'vwActivityAudit' AND ColumnName='InstrumentQty'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'vwActivityHistAudit' AND ColumnName='InstrumentPrice')
	 UPDATE ListViewColumns SET ColumnExpression='vwActivityHistAudit.InstrumentPrice'  WHERE TableName= 'vwActivityHistAudit' AND ColumnName='InstrumentPrice'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'vwActivityHistAudit' AND ColumnName='InstrumentQty')
	 UPDATE ListViewColumns SET ColumnExpression='vwActivityHistAudit.InstrumentQty'  WHERE TableName= 'vwActivityHistAudit' AND ColumnName='InstrumentQty'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'vwCustActivity' AND ColumnName='InstrumentPrice')
	 UPDATE ListViewColumns SET ColumnExpression='vwCustActivity.InstrumentPrice'  WHERE TableName= 'vwCustActivity' AND ColumnName='InstrumentPrice'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'vwCustActivity' AND ColumnName='InstrumentQty')
	 UPDATE ListViewColumns SET ColumnExpression='vwCustActivity.InstrumentQty'  WHERE TableName= 'vwCustActivity' AND ColumnName='InstrumentQty'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'vwCustActivityHist' AND ColumnName='InstrumentPrice')
	 UPDATE ListViewColumns SET ColumnExpression='vwCustActivityHist.InstrumentPrice'  WHERE TableName= 'vwCustActivityHist' AND ColumnName='InstrumentPrice'
GO
IF EXISTS(SELECT 1 FROM ListViewColumns WHERE  TableName= 'vwCustActivityHist' AND ColumnName='InstrumentQty')
	 UPDATE ListViewColumns SET ColumnExpression='vwCustActivityHist.InstrumentQty'  WHERE TableName= 'vwCustActivityHist' AND ColumnName='InstrumentQty'
GO

-- Insert AttRstrct Option in OptionTbl Table
IF NOT EXISTS (SELECT 1 FROM OptionTbl WITH(NOLOCK) WHERE Code='AttRstrct')
	BEGIN
		INSERT INTO
			OptionTbl
			(Code, Name, Enabled, ModEnable, CreateOper, CreateDate, LastOper, LastModify)
			VALUES
			('AttRstrct', 'Restrict Attachment File Extensions', 0, 1, 'Prime', getdate(), NULL, NULL)
	END
GO

-- Insert FileExtOpt Option in CustomSettings Table
IF NOT EXISTS(SELECT 1 FROM dbo.CustomSettings WITH(NOLOCK) WHERE Code='FileExtOpt' and Product='CDB' and SettingCode='FileExtRst')
	BEGIN
		 INSERT INTO CustomSettings
		 (Code, Product, RecordType, OperCode, [Key], Value, IsUserEditable, SettingCode, CreateDate, CreateOper, LastModifDate, LastOper)
		 Values
 ('FileExtOpt','CDB','SYSTEM','','Restricted file extensions should be entered as comma separated values.Example: .EXE,.DLL,.SYS', '.EXE,.DLL,.SYS', 1, 'FileExtRst', GETDATE(), 'PRIME', GETDATE(), 'PRIME') 
	END
GO

-- Insert VirusWarn Option in CustomSettings Table
IF NOT EXISTS(SELECT 1 FROM dbo.CustomSettings WITH (NOLOCK) WHERE Code='VirusWarn' and Product='CDB' and SettingCode='VirusWarn')
	BEGIN
		 INSERT INTO CustomSettings
		 (Code, Product, RecordType, OperCode, [Key], Value, IsUserEditable, SettingCode, CreateDate, CreateOper, LastModifDate, LastOper)
		 Values
		 ('VirusWarn','CDB','SYSTEM','','', '*All files uploaded should be confirmed to be virus free before uploading.', 1, 'VirusWarn', GETDATE(), 'PRIME', GETDATE(), 'PRIME') 
	END
GO
-- Insert FilExtWarn Option in CustomSettings Table
IF NOT EXISTS(SELECT 1 FROM dbo.CustomSettings WITH(NOLOCK)WHERE Code='FilExtWarn' and Product='CDB' and SettingCode='FilExtWarn')
	BEGIN
		 INSERT INTO CustomSettings
		 (Code, Product, RecordType, OperCode, [Key], Value, IsUserEditable, SettingCode, CreateDate, CreateOper, LastModifDate, LastOper)
		 Values
		 ('FilExtWarn','CDB','SYSTEM','','', '*As per the system configuration, files with the following extensions are not allowed to be uploaded.', 1, 'FilExtWarn', GETDATE(), 'PRIME', GETDATE(), 'PRIME') 
	END
GO


-- Update the ViewParameters to display the all the Common Options in vwPCDBOptions(SQLOperationId = 11) page
-- Also Updating ViewParameters with "Restrict Attachment File Extensions" for displaying the Restrict file extensions option.
IF EXISTS (Select 1 from ViewParameters WITH(NOLOCK) WHERE ViewID ='452')
BEGIN
	 UPDATE ViewParameters SET SQLOperationId = 11, ParameterValue = 'DashBoard, Attachment, Restrict Attachment File Extensions' WHERE ViewID = '452' 
END
GO

-- Insert ThrdPtyLst Option in CustomSettings Table
IF NOT EXISTS(SELECT 1 FROM dbo.CustomSettings WITH(NOLOCK) WHERE Code='OFACOptions' and Product='OFAC' and SettingCode='ThrdPtyLst')
INSERT INTO CustomSettings
	(Code, Product, RecordType, OperCode, [Key], Value, IsUserEditable, SettingCode, CreateDate, CreateOper, LastModifDate, LastOper)
	Values
	('OFACOptions','OFAC','SYSTEM','','Option for configuring OFAC third party programs. Enter list of programs separated by a comma. Each program should have atleast one alphanumeric character. Use asterisk(*) as a wildcard character and cannot contain multiple asterisks. Example: DJ-*,WC-*', 'DJ-*,WC-*', 1, 'ThrdPtyLst', GETDATE(), 'PRIME', GETDATE(), 'PRIME') 
GO

-- Update the PageWindowType of LogFile.aspx page, PageId of LogFile.aspx is 52
IF EXISTS(SELECT PageId from dbo.PageTbl with(nolock) where PageId=52 and PageName='LogFile.aspx' )
Update PageTbl set PageWindowType=0 where PageId=52 and PageName='LogFile.aspx'
GO

PRINT ''
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT 'Completed Conversion of PCDB Database to version 10.1.3  '
PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
PRINT ''
GO
