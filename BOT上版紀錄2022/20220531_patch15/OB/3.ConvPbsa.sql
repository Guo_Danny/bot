﻿/*
**  File Name:		ConvPbsa.sql
**
**  Functional Description:
**
**  This module contains SQL conversion procedures for Prime version 10.1.3
**
**  Facility	    The Prime Central Database
**  Creation Date:  03/23/2018
**
****************************************************************************
***                                                                      ***
***                             COPYRIGHT                                ***
***                                                                      ***
*** (c) Copyright 2009 - 2011 						 ***
*** FIS                                                                  ***
***                                                                      ***
*** This software is furnished under a license for use only on a single  ***
*** computer system and may be copied only with the inclusion of the     ***
*** above copyright notice. This software or any other copies thereof,   ***
*** may not be provided or otherwise made available to any other person  ***
*** except for use on such system and to one who agrees to these license ***
*** terms. Title and ownership of the software shall at all times remain ***
*** in FIS.                                                              ***
***                                                                      ***
*** The information in this software is subject to change without notice ***
*** and should not be construed as a commitment by Prime Associates, Inc.***
***                                                                      ***
****************************************************************************
                   
				       Maintenance History                 
------------|----------|----------------------------------------------------
   Date     |  Person  |  Description of Modification              
------------|----------|----------------------------------------------------
03/14/2018	   SN       CTR Template name has been updated as CTR08282017.pdf as per Fincen new requirement.
03/14/2018	   SN       Defect Fix 6258 : Added the code to auto populate Item 41 and Item 42 to the Part III of new CTR.
						Changes has been made in BSA_ActivityBranchesForCase stored proc.
03/14/2018	   SN		Modified BSA_GetRegReport to result FileType field to identify the Acknowledgement file from XML.
03/14/2018	   SN		Added new stored procedure "BSA_SelCTRRegReportForEFiling" to E-file new CTRS 
03/14/2018	   SN		Modified "BSA_UpdRegReportNew" to restrict the Updation of Acknowledgement HasErrors 
						column on Finalizing the regreport.
03/14/2018	   SN		Fixed the Defect #6297(Issue 2) inside Stored Proc ActivityBranchForCase
03/14/2018	   SN 		Fixed the issue to properly handle Contents field for new CTR templates.
03/20/2018	   SN		Added a new function BSA_fnGetAllCTRRegReport to return all the CTR RepNo that needs to be exported.
04/05/2018	   VS		Defect Fix 6365: Added the code to decrypt NPI data in BSA_GetIDInformation and BSA_GetCTRConductors stored procedure.
05/22/2018     VS       Added Fix: Issue with CustExport (related to OFACScanAudit) - since upgrade to Prime 10.1.3 - 1800811821
                        Modified BSA_GetCustomersBySql function to call FPEAccessEx function to decrypt NPI data.
05/24/2018     VS       Added a code to decrypt Customer TIN Value if it is encrypted in USR_Unrelated3rdParty
05/25/2018	   VS       Added Fix : Issue with CustExport from prime UI 
                        Modified BSA_GetCustomers function to call FPEAccessEx function to decrypt NPI data. 
08/01/2018	   VS       Modified stored procedures that decrypt data to call the FPEAccessEx function instead of FPEAccess
08/09/2018     DB       1801276792 : Added EfileFormate parameter and Source parameter in the BSA_UpdateFinalizeRegReport procedure.
08/28/2018	   VS       Defect Fix 6448 : Modified CRY_CaseManagement,CRY_CasesClosed,CRY_CasesPending,CRY_SAByDate,BSA_GetCasesForExport stored procedure
                        to handle NPI data value to store in temp table if voltage is unavailable.
'09/14/2018	   MV 		1801567952 - FINCEN SAR Changes. Changed exiting SAR template name from SAR03292012.pdf to SAR01262018.pdf in the proc BSA_SelRegReportsForEFiling
11/14/2018	   VS		1801370863: Modified USR_HighRiskCountry rule if CountryList field conatins space in county name.
						I170358404 - Fixed performace issue: Removed looping on countylist and used Temp table TempAddrTable in
						search criteria to match the country information with Address. Removed country and pos variables.
						Replaced all table variables(@TT1,@TT1,@TempAddrTable) with Temp table(#TT1,#TT1,#TempAddrTable).
11/01/2018     JS		1802005952: The rules(MulOrg1Bene, 1OrgMulBene, SameBeneOrd, BeneEQByOrd, BneEQOrgAmt, XTypeYBeOr) 
						has been modified to add the new Parameter to Remove special characters from Originator/Beneficiary Field.
11/12/2018	   JS		1801561733 - Added condition in the BSA_GetSARAttachment procedure to filter csv file.  	
01/10/2019	   JS		1800695452: updated SARFollowup Rule description length and append truncated message.
01/08/2019	   MK		1801984837 - Created new rule to add hyphen in Regex
03/07/2019     CD       VeraCode SQL Injection Remediation, Flaw Id 459 - Added BSA_CLItemExistsInModel
03/04/2019	   VS		1802220286 - In Watchlist table updated rule "AtpActvCmps" description and param column to add new parameter to perform analysis on all account having NULL anticipated values. 
03/05/2019	   VS		1802220286 - Modified AntpActvComps rule to perform analysis on all account having NULL anticipated values,based on the new Ignore Null Anticipated Value parameter.
						Added new parameter @ignoreZeroAntVals in AntpActvComps rule.
						Added BSA_GetAccountAnticipatedValues stored procedure to get anticipated values of account.
03/13/2019	   VS		Bug 24235:AtpActvCmps - Case created for the account with transaction value zero
05/16/2019	   MS		1801977031 - Override Risk Score Enhancement - Create new stored proc for Override and GetRejectScore
05/16/2019	   VS		1900804999 - DataLoad Enhancement - Adde new stored procedure BSA_GetBranchandDeptfromAccount and BSA_GetBranchandDeptfromCustomer to get branch and department information.
08/14/2019     DB       1802005952: The rules(MulOrg1Bene, 1OrgMulBene) has been modified to Remove special characters from Originator/Beneficiary Fields.
09/05/2019     DB       1900446519/1901308609: The rule USR_1MnthExceedAverage has been modified to generate alerts/cases, if monthly total amount and average monthly volume with equal or $0 in specified timeframe.
03/06/2020     VS       30448:BSA - Disable "Get Change History" Report. CRY_GetChangeHistory.rpt is deleted from report table if there is no data in ChangeHistory table.
04/15/2021     VS       35902: Modified USR_XPercInTypeOutType, USR_XPercOutInTypeTransfer and USR_XPercInOutTypeTransfer.			
*/
					 
USE PBSA
GO


Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print '  Starting Conversion of Pbsa Objects To Version 10.1.3'
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go
IF EXISTS (select * from sysobjects 
	where id = object_id(N'[dbo].[BSA_fnGetAllCTRRegReport]'))
	Drop Function dbo.BSA_fnGetAllCTRRegReport
Go

CREATE Function dbo.BSA_fnGetAllCTRRegReport ()  
	Returns varchar(8000)  WITH ENCRYPTION As  
/*
This function  returns a 
string of all CTR's RepNo that needs to be exported
*/
Begin 
 
 Declare @RegRepList varchar(8000)

 Set @RegRepList = ''
 
 Select @RegRepList = @RegRepList + cast(RepNo as varchar(20)) + ','
	
	from regreport r (nolock) join psec.dbo.branch b on r.OwnerBranch = b.Code 
WHERE b.branchnumber is not null AND 

(type = 'CTR' AND 1 IN
(select enabled from optiontbl where Code = 'CTRElect')) AND
IsFinalized = 1 and EFileFormat = 0 and RptFiled = 0 and  PdfFileName = 'CTR08282017.pdf'
--order by r.type, r.tccCode, b.branchnumber, r.createdate
order by r.RepNo 
 
 if Len(@RegRepList) > 0 -- Remove last comma
	Select @RegRepList = Left(@RegRepList, Len(@RegRepList)-1)

 Return @RegRepList

End
Go
If Exists(Select * From sysobjects
          where id = object_id(N'[dbo].[BSA_SelRegReportsForEFiling]')
              and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    Drop Procedure  dbo.BSA_SelRegReportsForEFiling
go



CREATE Procedure dbo.BSA_SelRegReportsForEFiling
With Encryption As
Begin
select RepNo, SARecNo, Type, Prepared, 
       PreparedBy, Filed, FiledBy , Approved, ApprovedBy , Method, 
       ContentsTyp, RptImageTyp, CreateOper, r.CreateDate, r.LastOper,
       r.LastModify, RptFiled, IsFinalized, FinalizedBy, EFileFormat, 
       Notes, Cust, Account, PdfFileName, OwnerBranch, ownerDept,
       ownerOper, ts, MailReceiptNo, TccCode, AdditionalPersons, Reason, 
	   Contents,LegacyContents, Contents as ContentsCTR, RptImage

from regreport r (nolock) join psec.dbo.branch b on r.OwnerBranch = b.Code 
WHERE b.branchnumber is not null AND 
((type in ('SAR', 'SFISAR') AND 1 IN 
(select enabled from optiontbl where Code = 'SARElect')) OR
(type = 'CTR' AND 1 IN
(select enabled from optiontbl where Code = 'CTRElect'))) AND
IsFinalized = 1 and EFileFormat = 0 and RptFiled = 0 and (PdfFileName = 'SAR01262018.pdf' or PdfFileName = 'CTR08282017.pdf')
order by r.type, r.tccCode, b.branchnumber, r.createdate
End
GO

IF EXISTS (select * from sysobjects 
	where id = object_id(N'[dbo].[BSA_ActivityBranchesForCase]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [DBO].[BSA_ActivityBranchesForCase]
    
GO

Create PROCEDURE dbo.BSA_ActivityBranchesForCase(@ObjectID 	sCode)
With Encryption as

    Declare @CaseType varchar(11)
    Declare @Branch varchar(11)
    Declare @stat int
	Declare @paytran Int

    Select @caseType = SuspType 
	From SuspiciousActivity with (nolock)	
	Where recno =@ObjectID 

	set @paytran = 0
	If Exists (Select * from SuspiciousActivity with (nolock) where RecNo=@ObjectID )
		SELECT @Paytran=CASE When ActInActCnt>0 Then 1 Else 2 End
		FROM SuspiciousActivity with (nolock)
		WHERE recno = @ObjectID
	Else 
		SELECT @Paytran=CASE When ActInActCnt>0 Then 1 Else 2 End
		FROM SuspiciousActivityHist with (nolock)	
		WHERE recno = @ObjectID


    If   @caseType <> 'ExcCurrency'
    Begin 
		If Exists(Select Top 1 a.Branch
		From dbo.SAsActivity sas with (nolock)
		Join dbo.Activity a with (nolock) on sas.TranNo=a.TranNo
		Where sas.objectID=@ObjectID 
		and sas.Included=1 and upper(sas.objecttype) = 'SUSPACT')

		Select distinct sas.objectID,a.Branch, sum(a.BaseAmt) as BaseAmt, a.RecvPay,a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		From dbo.SAsActivity sas with (nolock)
		Join dbo.Activity a with (nolock) on sas.TranNo=a.TranNo
		Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch
		Where sas.objectID=@ObjectID 
		and sas.Included=1 and upper(sas.objecttype) = 'SUSPACT'
		group by  sas.objectID,a.Branch,  a.RecvPay,a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
        Order By a.Branch,a.BookDate

        Else
			Select distinct sas.objectID,ah.Branch, sum(ah.BaseAmt) as BaseAmt, ah.RecvPay, ah.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			From dbo.SAsActivity sas with (nolock)
			Join dbo.ActivityHist ah with (nolock) on sas.TranNo=ah.TranNo
			Join PSEC.dbo.Branch b with (nolock) on b.Code = ah.Branch
			Where sas.objectID=@ObjectID 
			and sas.Included=1 and upper(sas.objecttype) = 'SUSPACT'
			group by sas.objectID,ah.Branch,  ah.RecvPay, ah.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			Order By ah.Branch,ah.BookDate

        Select @stat = @@error
        Return @stat
	End

    If (Select Enabled From OptionTbl where code='BeneCTR')=1
    Begin 
		--Exceed currency. Trans not is sasactivity.
		If exists(Select Top 1 a.Branch
			From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
			on case when a.BeneCustID is NULL Then a.Cust Else a.BeneCustID End = sa.Cust
			AND a.BookDate = sa.BookDate
			Where sa.recno = @objecTID)

			Select distinct a.Branch, sum(a.BaseAmt) as BaseAmt, a.RecvPay, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
			on case when a.BeneCustID is NULL Then a.Cust Else a.BeneCustID End = sa.Cust AND a.BookDate = sa.BookDate 
			and (@paytran = a.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch
			and a.CashTran = 1
			Where sa.recno = @objecTID
			group by a.Branch, a.RecvPay, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			Order By a.Branch,a.BookDate

		Else
			Select distinct ah.Branch, sum(ah.BaseAmt) as BaseAmt, ah.RecvPay, ah.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		    From SuspiciousActivity sa  with (nolock) Join activityhist ah with (nolock)
		    on case when ah.BeneCustID is NULL Then ah.Cust Else ah.BeneCustID End = sa.Cust AND ah.BookDate = sa.BookDate 
			and (@paytran = ah.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = ah.Branch		    
			and ah.CashTran = 1
		    Where sa.recno = @OBJECTID
			group by ah.Branch, ah.RecvPay, ah.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		    Order By ah.Branch,ah.BookDate
    End
    Else
    Begin
		--Exceed currency. Trans not is sasactivity.
		If exists(Select Top 1 a.Branch
			From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
			on a.Cust = sa.Cust AND a.BookDate = sa.BookDate
			Where sa.recno = @objecTID)

			Select distinct a.Branch, sum(a.BaseAmt) as BaseAmt, a.RecvPay, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			From SuspiciousActivity sa  with (nolock) Join activity a with (nolock)
			on a.Cust = sa.Cust AND a.BookDate = sa.BookDate 
			and (@paytran = a.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = a.Branch		    
			and a.CashTran = 1
			Where sa.recno = @objecTID
			group by a.Branch, a.RecvPay, a.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
			Order By a.Branch,a.BookDate

		Else
			Select distinct ah.Branch, sum(ah.BaseAmt) as BaseAmt, ah.RecvPay, ah.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		    From SuspiciousActivity sa  with (nolock) Join activityhist ah with (nolock)
		    on ah.Cust = sa.Cust AND ah.BookDate = sa.BookDate 
			and (@paytran = ah.RecvPay or @paytran=0)
			Join PSEC.dbo.Branch b with (nolock) on b.Code = ah.Branch		    
			and ah.CashTran = 1
		    Where sa.recno = @OBJECTID
			group by ah.Branch, ah.RecvPay ,ah.BookDate, b.State, b.City, b.Zip, b.Address, b.Country, b.name, b.MICR, b.EIN
		    Order By ah.Branch,ah.BookDate
    End
							
	 Select @stat = @@error
     Return @stat	 

GO
IF EXISTS (select * from sysobjects 
	where id = object_id(N'[dbo].[BSA_GetRegReport]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [DBO].[BSA_GetRegReport]
    
GO

Create Procedure [dbo].[BSA_GetRegReport] (
	 @repNo SeqNo1) 
 With encryption as
 
 begin
Select RegReport.*,AckFilesRepository.Id,AckFilesRepository.AckErrorCode ,AckFilesRepository.BSAIdentifier, AckFilesRepository.HasErrors,AckFilesRepository.FileType from
  dbo.RegReport with (nolock) left outer join dbo.AckFilesRepository with (nolock)
on dbo.RegReport.RepNo=dbo.AckFilesRepository.ReportNumber  where RepNo=@repNo order by AckFilesRepository.id desc
end  
go


If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[BSA_UpdateFinalizeRegReport]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[BSA_UpdateFinalizeRegReport]
GO

Create Procedure dbo.BSA_UpdateFinalizeRegReport(@RepNo SeqNo1,@IsFinalize bit,@EFileFormat bit =0, @Source varchar(3)='COM', @ts timestamp = null output) 
 With Encryption As
 
  -- Start standard stored procedure transaction header
  declare @trnCnt int
  declare @txt varchar(200)
  declare @saRecNo SeqNo1
   declare @susTableName varchar(20)

  select @trnCnt = @@trancount	-- Save the current trancount
  If @trnCnt = 0
	-- Transaction has not begun
	begin tran BSA_UpdateFinalizeRegReport
  else
    -- Already in a transaction
	save tran BSA_UpdateFinalizeRegReport
  -- End standard stored procedure transaction header
  
  declare	@stat		int
  declare @oper SCode
	
   set @oper = 'Admin'
select  @saRecNo = SARecNo from RegReport with (nolock) where RepNo= @RepNo
   Set @susTableName='SuspAct'
   If Exists (select * from SuspiciousActivityHist Where RecNo = @saRecNo)
		Set @susTableName='SuspActHst'
  
  Update RegReport 
	set IsFinalized=@IsFinalize,
	EFileFormat= @EFileFormat,
	FinalizedBy=null,lastoper = 'ADMIN'
	where RepNo=@RepNo

  if ( @@error <> 0 ) 
	select @stat = @@error
  else
    select @stat = 0 -- OK
  
  select @ts=ts from regreport where repno=@repNo
  
  -- The Event message text is set based on the calling functionality(CompEod-COM, Acknowledgement Utility-ACK) , Default = COM.
  IF @Source ='ACK' 
	  BEGIN
		SELECT @txt= 'The Report has been Acknowledged with errors/warnings. The status of Finalized and EFileFormat has been reverted for the Report Number'+''''+cast(@repNo as varchar) + ''''
	  END
  ELSE
	  BEGIN
		SELECT @txt= 'Export failed while validating the XML and Reverted the status of Finalized for the Report Number'+''''+cast(@repNo as varchar) + ''''
	  END

Exec BSA_InsEvent @oper, 'Mod', @susTableName, @saRecNo,@txt
  
  -- Evaluate results of the transaction
  if (@stat = 0) begin
    If @trnCnt = 0
      commit tran BSA_UpdateFinalizeRegReport
  end else 
	rollback tran BSA_UpdateFinalizeRegReport
  
  return @stat
GO

If exists (select * from dbo.sysobjects 
		where id = object_id(N'[dbo].[BSA_GetIDInformation]') and 
					OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[BSA_GetIDInformation]
GO

Create PROCEDURE dbo.BSA_GetIDInformation
(
	@custId	ObjectID
)
with encryption AS
declare @librarycontext varchar(max)      
declare @createfpeobject varchar(max)
declare @isIDNumberEncrypted int
set @isIDNumberEncrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('PartyID','IDNumber')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();
------------------------------------------------------------
-- Returns ID information for customer, used by RegRep
--
-- Include records for customer first because later records
-- with the same id type will overwrite previous and id table 
-- has a proprity.
-------------------------------------------------------------
Select 'DLID' as [Type],
case when LicenseNo is not null and LicenseNo <>'' and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('Customer','LicenseNo')>0 then pcdb.dbo.FPEAccessEx(LicenseNo,@librarycontext) else LicenseNo end as Number, 
LicenseState as IssuedBy,'' as IssueCountry
From Customer 
Where id=@custID and LicenseNo is not NULL
Union all
Select 'PASSPORTID' as [Type],
case when PassportNo is not null and PassportNo <>'' and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('Customer','PassportNo')>0 then pcdb.dbo.FPEAccessEx(PassportNo,@librarycontext) else PassportNo end as Number,
'' as IssuedBy, '' as IssueCountry
From Customer 
Where id=@custID AND PassportNo IS NOT NULL
Union all
Select UPPER(IDType) as [Type],
case when IDNumber is not null and IDNumber <> '' and @isIDNumberEncrypted>0 then pcdb.dbo.FPEAccessEx(IDNumber,@librarycontext) else IDNumber end as Number,
IssuePlace as IssuedBy,IssueCountry
From dbo.PartyID Join Customer
On PartyID.CustomerId=Customer.ID
Where id=@custID

go

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'BSA_GetCTRConductors')
    DROP PROCEDURE [DBO].[BSA_GetCTRConductors]
GO

Create PROCEDURE dbo.BSA_GetCTRConductors 
(
	@caseId	SeqNo1
)
With Encryption AS
declare @librarycontext varchar(max)      
declare @createfpeobject varchar(max)
declare @isTinEncrypted int
declare @isLicenseNoEncrypted int
declare @isPassportNoEncrypted int
set @isTinEncrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('Customer','TIN')
set @isLicenseNoEncrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('Customer','LicenseNo')
set @isPassportNoEncrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('Customer','PassportNo')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();
----------------------------------------------------
-- Returns conductor information from activities 
-- related to the case.

-- RecvPay 2 = Pay
--		   1 = Receive
----------------------------------------------------

	DECLARE @activity int
	DECLARE @caseType SCode
    DECLARE @benectr bit
    DECLARE @paytran int

    -- Table to hold activities that created the case
    DECLARE @Activities Table (Cust varchar(35),TranNo int, BaseAmt money,
							  Curr char(3),FxAmt money,RecvPay int, CashAcc varchar(35))
    -- Table to hold transactor id's
    DECLARE @TransActors Table (ord int identity primary key,CustID varchar(35))

    -- Table to hold customer id's
    DECLARE @Customers Table (ord int identity primary key,CustID varchar(35))
	
    --Get Case Record
  If Exists (Select * from SuspiciousActivity where RecNo=@CaseiD )
	SELECT @activity = activity, @caseType = SuspType ,
	@benectr = case when Descr like '%This case was created with beneficiary activity.%' Then 1 Else 0 end,
    @Paytran=CASE When ActInActCnt>0 Then 1 Else 2 End
	FROM SuspiciousActivity 	
	WHERE recno = @caseid
  ELSE 
    SELECT @activity = activity, @caseType = SuspType ,
	@benectr = case when Descr like '%This case was created with beneficiary activity.%' Then 1 Else 0 end,
    @Paytran=CASE When ActInActCnt>0 Then 1 Else 2 End
	FROM SuspiciousActivityHist 	
	WHERE recno = @caseid
	 
    --debug
    --select @benectr
 
        ----------------------------------------------------------------------
        -- These are the only cases we need at this moment
        ----------------------------------------------------------------------
		If @caseType = 'ExcCurrency' or @caseType='Manual' or @caseType='Rule'
			Begin
				-- Get Activities that created the case
              If   @caseType = 'ExcCurrency' 
				Begin

                    If @benectr = 0
                    Begin 
						Insert Into @Activities --For exeed curr cases activities have to be searched.
							Select ah.cust,ah.TranNo,ah.BaseAmt,ah.Curr,ah.FxAmt, ah.RecvPay, ah.Account 
							From SuspiciousActivity sa  Join activityhist ah 
							on ah.Cust = sa.Cust AND ah.BookDate = sa.BookDate 
							and ah.CashTran = 1
							Where sa.recno = @caseId and (@paytran=ah.RecvPay or @paytran=0)
								UNION
							Select a.cust,a.TranNo,a.BaseAmt,a.Curr,a.FxAmt, a.RecvPay, a.Account
							From SuspiciousActivity sa  Join activity a 
							on a.Cust = sa.Cust AND a.BookDate = sa.BookDate 
							AND a.CashTran = 1
							Where sa.recno = @caseId and (@paytran=a.RecvPay or @paytran=0)
                   End
                   Else
                   Begin
                            Insert Into @Activities
							Select Case when ah.BeneCustId is null Then ah.Cust Else ah.BeneCustId End,
							ah.TranNo,ah.BaseAmt,ah.Curr,ah.FxAmt, ah.RecvPay, ah.Account
							From SuspiciousActivity sa  Join activityhist ah 
							on case when ah.BeneCustId is null Then ah.Cust Else ah.BeneCustId End = sa.Cust AND ah.BookDate = sa.BookDate 
							and ah.CashTran = 1
							Where sa.recno = @caseId and (@paytran=ah.RecvPay or @paytran=0)
								UNION
							Select Case when a.BeneCustId is null Then a.Cust Else a.BeneCustId End,
							a.TranNo,a.BaseAmt,a.Curr,a.FxAmt, a.RecvPay, a.Account
							From SuspiciousActivity sa  Join activity a 
							on case when a.BeneCustId is null Then a.Cust Else a.BeneCustId End  = sa.Cust AND a.BookDate = sa.BookDate 
							AND a.CashTran = 1
							Where sa.recno = @caseId  and (@paytran=A.RecvPay or @paytran=0)
                   end

				End
              Else
				Begin
				-- Manual Case
                   Insert Into @Activities 
                   Select ah.cust,ah.TranNo,ah.BaseAmt,ah.Curr,ah.FxAmt, ah.RecvPay, ah.Account
				   From activityhist ah Join dbo.SAsActivity sas
                   on ah.TranNo=sas.TranNo --and ah.CashTran = 1
				   Where sas.ObjectType in ('SuspActHst','SuspAct') and sas.ObjectID=@caseId and sas.Included=1
						UNION
                   Select a.cust,a.TranNo,a.BaseAmt,a.Curr,a.FxAmt, a.RecvPay, a.Account
				   From activity a Join dbo.SAsActivity sas
                   on a.TranNo=sas.TranNo --and a.CashTran = 1
				   Where sas.ObjectType in ('SuspActHst','SuspAct') and sas.ObjectID=@caseId and sas.Included=1
                End

				-- Get Transactors
				Insert Into @TransActors (CustID)
                -- From Activity
				Select distinct ByOrderCustID AS CustID from dbo.Activity where TranNo IN (Select TranNo From @Activities) and ByOrderCustID is not null
				UNION all Select distinct IntermediaryCustId As CustID from dbo.Activity where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId is not null
				UNION all Select distinct IntermediaryCustId3 As CustID from dbo.Activity where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId3 is not null
                -- From Activity History
                UNION all Select distinct ByOrderCustID AS CustID from dbo.ActivityHist where TranNo IN (Select TranNo From @Activities) and ByOrderCustID is not null
				UNION all Select distinct IntermediaryCustId As CustID from dbo.ActivityHist where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId is not null
				UNION all Select distinct IntermediaryCustId3 As CustID from dbo.ActivityHist where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId3 is not null

                --Get Customers
                If @benectr =1 
                Begin
                    
					Insert Into @Customers (CustID)
                    -- From Case
                    Select Cust as CustID from SuspiciousActivity where SuspiciousActivity.recno=@caseID
                    Union Select Cust as CustID from SuspiciousActivityHist where SuspiciousActivityHist.recno=@caseID
                    UNION
					-- From Activity
					Select distinct BeneBankCustId AS CustID from dbo.Activity where TranNo IN (Select TranNo From @Activities) and BeneBankCustId is not null
					UNION all Select distinct IntermediaryCustId2 As CustID from dbo.Activity where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId2 is not null
					UNION all Select distinct IntermediaryCustId4 As CustID from dbo.Activity where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId4 is not null
					-- From Activity History
					UNION all Select distinct BeneBankCustId AS CustID from dbo.ActivityHist where TranNo IN (Select TranNo From @Activities) and BeneBankCustId is not null
					UNION all Select distinct IntermediaryCustId2 As CustID from dbo.ActivityHist where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId2 is not null
					UNION all Select distinct IntermediaryCustId4 As CustID from dbo.ActivityHist where TranNo IN (Select TranNo From @Activities) and IntermediaryCustId4 is not null
                End
 
                  
            End

     -- Return Transactor information

   -- From customer table
   Select top 23 'Transactors' as TableID,Sum.* From (
     Select ID,Name,Address,
	 case when TIN is not null and TIN <>'' and @isTinEncrypted>0 then pcdb.dbo.FPEAccessEx(TIN,@librarycontext) else TIN end as TIN,
	 City,State,Zip,Country,DOB,
	 case when LicenseNo is not null and LicenseNo <> '' and @isLicenseNoEncrypted>0 then pcdb.dbo.FPEAccessEx(LicenseNo,@librarycontext) else LicenseNo end as LicenseNo,
	 LicenseState,
	 case when PassportNo is not null and PassportNo <> '' and @isPassportNoEncrypted>0 then pcdb.dbo.FPEAccessEx(PassportNo,@librarycontext) else PassportNo end as PassportNo,
	 DBA,TypeOfBusiness,Telephone,Sex,Email,IndOrBusType
	 From Customer
     join @TransActors tr on customer.id=tr.custid
     Union all
     -- From activity ByOrderCustID if the id is missing
     Select '0' as ID, IsNull(ByOrder,'') as Name,Isnull(ByOrderAddress,'') as Address,
     '' as TIN, IsNull(ByOrderCity,'') as City,Isnull(ByOrderState,'') as State,
     IsNull(ByOrderZip,'') as Zip, IsNull(ByOrderCountry,'') as Country, Null as DOB,
     '' AS LicenseNo, '' as LicenseState, '' as PaasportNo, '' AS DBA,'' AS TypeOfBusiness,'' AS Telephone,'' AS Sex,'' AS Email
	 , Null AS IndOrBusType
     From CurrentArchActivity a Join @Activities b 
     on a.TranNo=b.TranNo 
     where ByOrderCustID is Null and (IsNull(ByOrder,'')<>''  or IsNull(ByOrderAddress,'')<>''  )
     Union all
     -- From activity IntermediaryCustID if the id is missing
     Select '0' as ID, IsNull(Intermediary,'') as Name,Isnull(IntermediaryAddress,'') as Address,
     '' as TIN, IsNull(IntermediaryCity,'') as City,Isnull(IntermediaryState,'') as State,
     IsNull(IntermediaryZip,'') as Zip, IsNull(IntermediaryCountry,'') as Country, Null as DOB,
     '' AS LicenseNo, '' as LicenseState, '' as PaasportNo, '' AS DBA,'' AS TypeOfBusiness,'' AS Telephone,'' AS Sex,'' AS Email
	 , Null AS IndOrBusType
     From CurrentArchActivity a Join @Activities b 
     on a.TranNo=b.TranNo 
     where IntermediaryCustID is Null and (IsNull(Intermediary,'')<>''  or IsNull(IntermediaryAddress,'')<>''  )
     Union all
     -- From activity IntermediaryCustID3 if the id is missing
     Select '0' as ID, IsNull(Intermediary3,'') as Name,Isnull(IntermediaryAddress3,'') as Address,
     '' as TIN, IsNull(IntermediaryCity3,'') as City,Isnull(IntermediaryState3,'') as State,
     IsNull(IntermediaryZip3,'') as Zip, IsNull(IntermediaryCountry3,'') as Country, Null as DOB,
     '' AS LicenseNo, '' as LicenseState, '' as PaasportNo, '' AS DBA,'' AS TypeOfBusiness,'' AS Telephone,'' AS Sex,'' AS Email
	 , Null AS IndOrBusType
     From CurrentArchActivity a Join @Activities b 
     on a.TranNo=b.TranNo 
     where IntermediaryCustID3 is Null and (IsNull(Intermediary3,'')<>''  or IsNull(IntermediaryAddress3,'')<>''  )
     ) as sum

     -- Return Amounts
	 Select 'Amounts' as TableID, TranNo,BaseAmt,Curr,ISNULL(FxAmt,0) AS FxAmt,RecvPay, CashAcc from @Activities

     -- Return Customers
    iF @benectr=1
    Begin
    Select top 23 'Customers' AS TableID,Summary.* From (
     Select ID,Name,Address,
	 case when TIN is not null and TIN <> '' and @isTinEncrypted>0 then pcdb.dbo.FPEAccessEx(TIN,@librarycontext) else TIN end as TIN,
	 City,State,Zip,Country,DOB,
	 case when LicenseNo is not null and LicenseNo <> '' and @isLicenseNoEncrypted>0 then pcdb.dbo.FPEAccessEx(LicenseNo,@librarycontext) else LicenseNo end as LicenseNo,
	 LicenseState,
	 case when PassportNo is not null and PassportNo <> '' and @isPassportNoEncrypted>0 then pcdb.dbo.FPEAccessEx(PassportNo,@librarycontext) else PassportNo end as PassportNo,
	 DBA,TypeOfBusiness,Telephone,Sex,Email,IndOrBusType
	 From Customer
     join @Customers tr on customer.id=tr.custid
     Union all
     -- From activity BeneBank if the id is missing
     Select '0' as ID, IsNull(BeneBank,'') as Name,Isnull(BeneBankAddress,'') as Address,
     '' as TIN, IsNull(BeneBankCity,'') as City,Isnull(BeneBankState,'') as State,
     IsNull(BeneBankZip,'') as Zip, IsNull(BeneBankCountry,'') as Country, Null as DOB,
     '' AS LicenseNo, '' as LicenseState, '' as PaasportNo, '' AS DBA,'' AS TypeOfBusiness,'' AS Telephone,'' AS Sex,'' AS Email
	 , Null AS IndOrBusType
     From CurrentArchActivity a Join @Activities b 
     on a.TranNo=b.TranNo 
     where BeneBankCustId is Null and (IsNull(BeneBank,'')<>''  or IsNull(BeneBankAddress,'')<>''  )
     Union all
     -- From activity IntermediaryCustID2 if the id is missing
     Select '0' as ID, IsNull(Intermediary2,'') as Name,Isnull(IntermediaryAddress2,'') as Address,
     '' as TIN, IsNull(IntermediaryCity2,'') as City,Isnull(IntermediaryState2,'') as State,
     IsNull(IntermediaryZip2,'') as Zip, IsNull(IntermediaryCountry2,'') as Country, Null as DOB,
     '' AS LicenseNo, '' as LicenseState, '' as PaasportNo, '' AS DBA,'' AS TypeOfBusiness,'' AS Telephone,'' AS Sex,'' AS Email
	 , Null AS IndOrBusType
     From CurrentArchActivity a Join @Activities b 
     on a.TranNo=b.TranNo 
     where IntermediaryCustID2 is Null and (IsNull(Intermediary2,'')<>''  or IsNull(IntermediaryAddress2,'')<>''  )
     Union all
     -- From activity Intermediary4 if the id is missing
     Select '0' as ID, IsNull(Intermediary4,'') as Name,Isnull(IntermediaryAddress4,'') as Address,
     '' as TIN, IsNull(IntermediaryCity4,'') as City,Isnull(IntermediaryState4,'') as State,
     IsNull(IntermediaryZip4,'') as Zip, IsNull(IntermediaryCountry4,'') as Country, Null as DOB,
     '' AS LicenseNo, '' as LicenseState, '' as PaasportNo, '' AS DBA,'' AS TypeOfBusiness,'' AS Telephone,'' AS Sex,'' AS Email
	 , Null AS IndOrBusType
     From CurrentArchActivity a Join @Activities b 
     on a.TranNo=b.TranNo 
     where IntermediaryCustID4 is Null and (IsNull(Intermediary4,'')<>''  or IsNull(IntermediaryAddress4,'')<>''  )
     ) as summary
    end
Go

if exists (select * from sysobjects 
		where id = object_id(N'[dbo].[BSA_GetCustomersBySql]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		Drop Procedure dbo.BSA_GetCustomersBySql
	Go

Create Procedure dbo.BSA_GetCustomersBySql 
(      
@filtertype Varchar(50),
@strDate varchar(50),
@sqlquery varchar(max)
) 
/*
This stored procedure is used by CustExport utility only. It is created to improve custExport utility performance issue (defect #6090)
*/
WITH ENCRYPTION  AS      
Begin

       declare @query nvarchar(MAX)
	   declare @subquery nvarchar(MAX)
	   declare @completequery nvarchar(MAX)
	  
       declare @librarycontext varchar(max)
declare @createfpeobject varchar(max)
declare @istinencrypted int
set @istinencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','tin')
declare @islicensenoencrypted int
set @islicensenoencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','LicenseNo')
declare @ispassportnoencrypted int
set @ispassportnoencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','PassportNo')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();


       set @query='select [Id]
      ,[Parent]
      ,[Name]
      ,[DBA]
      ,[SecCode]
      ,[Address]
      ,[City]
      ,[State]
      ,[Zip]
      ,[Country]
      ,[Telephone]
      ,[Email]
         ,case when TIN is not null and '+CONVERT(VARCHAR, @istinencrypted)+'>0 then pcdb.dbo.fpeAccessEx(TIN,'+''''+@librarycontext+''''+') else TIN end as TIN
      ,case when LicenseNo is not null and '+CONVERT(VARCHAR, @islicensenoencrypted)+'>0 then pcdb.dbo.fpeAccessEx(LicenseNo,'+''''+@librarycontext+''''+') else LicenseNo end as LicenseNo
      ,[LicenseState]
      ,case when PassportNo is not null and '+CONVERT(VARCHAR, @ispassportnoencrypted)+'>0 then pcdb.dbo.fpeAccessEx(PassportNo,'+''''+@librarycontext+''''+') else PassportNo end as PassportNo
      ,[DOB]
      ,[TypeOfBusiness]
      ,[SourceOfFunds]
      ,[AccountOfficer]
      ,[AcctOffTel]
      ,[AcctOffEmail]
      ,[CompOfficer]
      ,[CompOffTel]
      ,[CompOffEmail]
      ,[IdList]
      ,[Notes]
      ,[ExemptionStatus]
      ,[LastActReset]
      ,[LastReview]
      ,[LastReviewOper]
      ,[CreateOper]
      ,[CreateDate]
      ,[LastOper]
      ,[LastModify]
      ,[OwnerBranch]
      ,[OwnerDept]
      ,[OwnerOper]
      ,[RiskClass]
      ,[Closed]
      ,[User1]
      ,[User2]
      ,[User3]
      ,[User4]
      ,[User5]
      ,[Ts]
      ,[CountryOfOrigin]
      ,[CountryOfIncorp]
      ,[OffshoreCorp]
      ,[BearerShares]
      ,[NomeDePlume]
      ,[Type]
      ,[Resident]
      ,[BusRelationNature]
      ,[PrevRelations]
      ,[PEP]
      ,[PoliticalPos]
      ,[FEP]
      ,[MSB]
      ,[CorrBankRelation]
      ,[Sex]
      ,[ShellBank]
      ,[HighRiskRespBank]
      ,[OffshoreBank]
      ,[PayThroughAC]
      ,[RegulatedAffiliate]
      ,[USPerson]
      ,[RFCalculate]
      ,[Status]
      ,[ReviewDate]
      ,[CTRAmt]
      ,[AmtRange]
      ,[SwiftTID]
      ,[Embassy]
      ,[ForeignGovt]
      ,[CharityOrg]
      ,[DoCIP]
      ,[Prospect]
      ,[AssetSize]
      ,[Income]
      ,[IndOrBusType]
      ,[OnProbation]
      ,[ProbationReason]
      ,[ProbationStartDate]
      ,[ProbationEndDate]
      ,[ClosedDate]
      ,[ClosedReason]
      ,[OpenDate]
      ,[CountryOfResidence]
      ,[CountryOfCitizenship]
      ,[KYCDataCreateDate]
      ,[KYCDataModifyDate]
      ,[KYCOperator]
      ,[KYCStatus] from customer as c (nolock)'

  if (@filtertype='RISKGEN')
  begin
  set @subquery=' WHERE RFCalculate = 1 AND c.Id IN (SELECT r.Cust FROM RiskFactorData r WITH (NOLOCK) WHERE dbo.BSA_CountMissingScores(r.Cust) = 0 AND (Status is Null Or Status = 0))'
  set @completequery = @query + @subquery
  end
  else
  begin
      if (@filtertype='LASTRUN')
      begin
	   set @subquery=' WHERE (CreateDate > CONVERT(DATETIME, ''' + @strDate + ''') OR LastModify > CONVERT(DATETIME, '''+ @strDate + '''))'
       set @completequery = @query + @subquery
      end
      else
      if (@filtertype='SQL')
	  begin
	    set @completequery = @query +' '+ @sqlquery
	  end
	  else
      begin
	     set @completequery=@query
      end
  end

       exec sp_executesql @completequery 
end
GO
Go

if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_Unrelated3rdParty]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure USR_Unrelated3rdParty
Go

CREATE PROCEDURE Dbo.USR_Unrelated3rdParty(@WLCode SCode, @testAlert INT,
	@productType SCode,
	@percent  INT,
	@month  INT,
	@RiskClass SCode)
AS
/* RULE AND PARAMETER DESCRIPTION
Detects a deposit W received and Y% is transferred within Z months to an unrelated third party.

	@productType = account type e.g. checking, check,
	@percent = % of withdrawn from received
	@month = period to check,
	@RiskClass = risk class to filter on
*/
SET NOCOUNT ON

-- /*  Declarations */
DECLARE @iWLcode SCode, 
	@iTestAlert INT,
	@iProductType SCode,
	@iPercent  INT,
	@iMonth  INT ,
	@iRiskClass SCode

--speeds up select statements if use of local variable
SELECT @iWLcode = @WLcode , 
	@iTestAlert = @TestAlert ,
	@iProductType = 	@ProductType ,
	@iPercent  = ABS(@Percent)  ,
	@iMonth = ABS(@Month) ,
	@iRiskClass = @RiskClass

DECLARE	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,  --0 for alert, 1 for case
	@stat INT,
	@trnCnt INT,
	@minDate INT,
	@cur CURSOR


Declare @account LONGNAME,
	@currDate INT,
	@recvAmt MONEY,
	@date int,
	@payAmt MONEY


-- THE FOLLOWING SECTION DEFINES ALL THE TEMPORARY TABLES.

-- This table contains summary of the withdrawals
DECLARE	@WDSummary TABLE (
	Account VARCHAR(40),
	Amount MONEY
)

-- This table contains the withdrawals entries
DECLARE	@WDEntries TABLE (
	Account VARCHAR(40),
	TranNo  int,
	Amount  MONEY
)

-- This table contains the deposit entries
DECLARE	@DepEntries TABLE (
	Account VARCHAR(40),
	TranNo  int,
	Amount  MONEY
)

-- This tables to hold the accounts.
DECLARE @accts TABLE (
	Account VARCHAR(40),
	Amount money
)

--  TEMPORARY TABLE DEFINITION ENDS HERE.


declare @startDate datetime
declare @endDate datetime
declare @bookDate int

SET @stat = 0
Declare @txt VARCHAR(1000)
IF @testAlert = 0 BEGIN
	SET @txt = 'Started Rule Evaluation'
	Exec BSA_InsEvent 'PRIME', 'Evl', 'Rule', @WLCode , @txt
END
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_Unrelated3rdParty
ELSE
	-- Already in a transaction
	SAVE TRAN USR_Unrelated3rdParty
/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @WLType = WLType  
FROM WatchList (NOLOCK) WHERE WLCode = @iWLCode

SET @minDate = dbo.ConvertSqlDateToInt(
	DATEADD(m, -1 * @iMonth, CONVERT(VARCHAR, GETDATE())))
SET @currDate = dbo.ConvertSqlDateToInt(GETDATE())

declare @isTinEncrypted int
declare @librarycontext varchar(max)      
declare @createfpeobject varchar(max)
set @isTinEncrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('Customer','TIN')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();


INSERT INTO @WDEntries(Account, TranNo, Amount)
	SELECT a.Account, a.TranNo, a.baseamt
	FROM Activity a (NOLOCK) INNER JOIN 
		Account acct
		ON a.Account = acct.Id 
		INNER JOIN Customer cust 
		ON a.Cust = Cust.Id AND cust.RiskClass = @iRiskClass 
	WHERE a.bookdate > @minDate
		AND acct.Type = @iProductType
                AND a.recvpay = 2 		-- Get only withdrawals
		AND (a.Bene <> (case when cust.TIN is not null and cust.TIN <>'' and @isTinEncrypted>0 then pcdb.dbo.FPEAccessEx(cust.TIN,@librarycontext) else cust.TIN end ) OR ISNULL(a.Bene, '') = '')
		AND a.Account IS NOT NULL 


-- Get all the accounts into temporary @aacts table so you left with unique values.
-- This has to be after the first retrieval of withdrawals for current activity
INSERT INTO @accts (account)
SELECT DISTINCT Account FROM @WDEntries

/*
  Get all the WithDrawals activity to temporary withdrawal table from 
  ActivityHist (RecvType =2). We need to get the activity for those 
  accounts which exists in @WDEntries.	
  Store transcation numbers Also. Ofcourse for a given period of dates
*/

INSERT INTO @WDEntries(Account, TranNo, Amount)
	SELECT ah.Account, ah.TranNo, ah.baseamt 
	FROM ActivityHist ah (NOLOCK) INNER JOIN 
				@accts WD
					ON ah.Account = WD.account
        		INNER JOIN Customer cust 
		ON ah.Cust = Cust.Id AND cust.RiskClass = @iRiskClass 
	WHERE ah.bookdate > @minDate 
                AND ah.recvpay = 2 		-- Get the withdrawals
		AND (ah.Bene <> (case when cust.TIN is not null and cust.TIN <>'' and @isTinEncrypted>0 then pcdb.dbo.FPEAccessEx(cust.TIN,@librarycontext) else cust.TIN end ) OR ISNULL(ah.Bene, '') = '')

-- Select the records into the summary table , where it has account and associates 
-- withdrawal summary.

INSERT INTO @WDSummary (Account,  Amount)
SELECT Account, SUM(Amount) from @WDEntries group by Account

--  Get all the deposits from activity
insert into @DepEntries (Account, TranNo, Amount) 
	SELECT a.Account,  a.TranNo, a.baseamt 
	FROM Activity a (NOLOCK) INNER JOIN 
				@accts WD
					ON a.Account = WD.account
			INNER JOIN Customer cust 
		ON a.Cust = Cust.Id AND cust.RiskClass = @iRiskClass 
	WHERE a.bookdate > @minDate 
                AND a.recvpay = 1 		-- Get the deposits

--  Get all the deposits from activity history
insert into @DepEntries (Account, TranNo, Amount) 
	SELECT ah.Account,  ah.TranNo, ah.baseamt 
	FROM ActivityHist ah (NOLOCK) INNER JOIN 
				@accts WD
					ON ah.Account = WD.account
			INNER JOIN Customer cust 
		ON ah.Cust = Cust.Id AND cust.RiskClass = @iRiskClass 
	WHERE ah.bookdate > @minDate 
                AND ah.recvpay = 1 		-- Get the deposits

-- Clear Temporary accounts table for reuse.
delete @accts


insert into @accts (Account, Amount)
select A.Account, A.Amount from
@wdsummary A  where exists (select B.Account from @DepEntries B 
where A.account = B.Account and  
(B.Amount * @iPercent <= A.Amount * 100))


IF @testAlert = 1 BEGIN
 		SELECT @startDate = GETDATE()
		INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
					LASTMODIFY, CUST, ACCOUNT, IsTest)
		select @WLCODE, 'Account:  ''' + a.Account 
				+ '''' +  ' had ' +  CONVERT(VARCHAR, @iPercent) + '% or more amount'  +
				' withdrawn ( ' + CONVERT(VARCHAR,a.Amount ) + ' ) in the last ' +  
				CONVERT(VARCHAR, @imonth) + ' months to an unrelated third party.' , 0, 
			GETDATE(), NULL, NULL, NULL, a.Account, 1 from
		@accts a
		SELECT @STAT = @@ERROR	
		SELECT @endDate = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
	
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   SELECT 'ALERT', ALERTNO, TRANNO 
			FROM (
					SELECT Account, TranNo, Amount FROM @depentries
					UNION 
					SELECT Account, TranNo, Amount FROM @wdentries
					)  a 
				INNER JOIN @accts t
					ON a.account = t.account 				
				INNER JOIN ALERT 
					ON t.Account = ALERT.Account 
			WHERE			
			ALERT.WLCODE = @WLCODE AND
			ALERT.CREATEDATE BETWEEN @startDate AND @endDate

 		SELECT @STAT = @@ERROR 
		IF @STAT <> 0  GOTO ENDOFPROC
	END ELSE BEGIN
		IF @WLTYPE = 0 BEGIN
		SELECT @startDate = GETDATE()
 		INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
					LASTMODIFY, CUST, ACCOUNT)
		select @WLCODE, 'Account:  ''' + a.Account 
				+ '''' +  ' had ' +  CONVERT(VARCHAR, @iPercent) + '% or more amount'  +
				' withdrawn ( ' + CONVERT(VARCHAR,a.Amount ) + ' ) in the last ' +  
				CONVERT(VARCHAR, @imonth) + ' months to an unrelated third party.'   , 0, 
			GETDATE(), NULL, NULL, NULL, a.Account from
		@accts a
	 
		SELECT @STAT = @@ERROR	
		SELECT @endDate = GETDATE()
 		IF @STAT <> 0  GOTO ENDOFPROC
		END ELSE IF @WLTYPE = 1 BEGIN
			SELECT @startDate = GETDATE()
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			WLCODE, WLDESC, CREATETIME )
			SELECT	NULL, @currDate, Null, Account,
			NULL, 'Rule', NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, 0, 0, 0, 0, 
			NULL, NULL, NULL, NULL, 0, NULL, NULL,
			@WLCODE, 'Account:  ''' + a.Account 
				+ '''' +  ' had ' +  CONVERT(VARCHAR, @iPercent) + '% or more amount'  +
				' withdrawn ( ' + CONVERT(VARCHAR,a.Amount ) + ' ) in the last ' +  
				CONVERT(VARCHAR, @imonth) + ' months to an unrelated third party.' , GETDATE() 
			FROM @accts A 
		SELECT @STAT = @@ERROR	
		SELECT @endDate = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
		END
		
		IF @WLTYPE = 0 BEGIN
 		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
 		   SELECT 'ALERT', ALERTNO, TRANNO 
			FROM (
					SELECT Account, TranNo, Amount FROM @depentries
					UNION 
					SELECT Account, TranNo, Amount FROM @wdentries
					) a 
				INNER JOIN @accts t
					ON a.account = t.account 				
				INNER JOIN ALERT 
					ON t.Account = ALERT.Account 
			WHERE			
			ALERT.WLCODE = @WLCODE AND
			ALERT.CREATEDATE BETWEEN @startDate AND @endDate

 		SELECT @STAT = @@ERROR 
 		END ELSE IF @WLTYPE = 1 BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   SELECT 'SUSPACT', RECNO, TRANNO 
			FROM (
					SELECT Account, TranNo, Amount FROM @depentries
					UNION 
					SELECT Account, TranNo, Amount FROM @wdentries
					) a 
				INNER JOIN @accts t
					ON a.account = t.account 				
				INNER JOIN suspiciousactivity s
					ON t.Account = S.Account 
			WHERE			
			S.WLCODE = @WLCODE AND
			S.CREATETIME BETWEEN @startDate AND @endDate
		SELECT @STAT = @@ERROR 
		END
	END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_Unrelated3rdParty
	IF @testAlert = 0 BEGIN
		SET @txt = 	'Error occured while evaluating rule'
		Exec BSA_InsEvent 'PRIME', 'Evl', 'Rule', @WLCode , @txt
	END
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_Unrelated3rdParty
	IF @testAlert = 0 BEGIN
		SET @txt = 	'Succesfully finished evaluating rule'			
		Exec BSA_InsEvent 'PRIME', 'Evl', 'Rule', @WLCode , @txt
	END
RETURN @stat
Go

if exists (select * from sysobjects 
		where id = object_id(N'[dbo].[BSA_GetCustomers]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		Drop Procedure dbo.BSA_GetCustomers
	Go


Create Procedure dbo.BSA_GetCustomers 
(      
@stpStr Varchar(200)     

) 
WITH ENCRYPTION  AS      
Begin
       declare @query nvarchar(MAX)


       declare @librarycontext varchar(max)
declare @createfpeobject varchar(max)
declare @istinencrypted int
set @istinencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','tin')
declare @islicensenoencrypted int
set @islicensenoencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','LicenseNo')
declare @ispassportnoencrypted int
set @ispassportnoencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','PassportNo')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();



       set @query='select '+@stpStr+' [Id]
      ,[Parent]
      ,[Name]
      ,[DBA]
      ,[SecCode]
      ,[Address]
      ,[City]
      ,[State]
      ,[Zip]
      ,[Country]
      ,[Telephone]
      ,[Email]
         ,case when TIN is not null and '+CONVERT(VARCHAR, @istinencrypted)+'>0 then pcdb.dbo.fpeAccessEx(TIN,'+''''+@librarycontext+''''+') else TIN end as TIN
      ,case when LicenseNo is not null and '+CONVERT(VARCHAR, @islicensenoencrypted)+'>0 then pcdb.dbo.fpeAccessEx(LicenseNo,'+''''+@librarycontext+''''+') else LicenseNo end as LicenseNo
      ,[LicenseState]
      ,case when PassportNo is not null and '+CONVERT(VARCHAR, @ispassportnoencrypted)+'>0 then pcdb.dbo.fpeAccessEx(PassportNo,'+''''+@librarycontext+''''+') else PassportNo end as PassportNo
      ,[DOB]
      ,[TypeOfBusiness]
      ,[SourceOfFunds]
      ,[AccountOfficer]
      ,[AcctOffTel]
      ,[AcctOffEmail]
      ,[CompOfficer]
      ,[CompOffTel]
      ,[CompOffEmail]
      ,[IdList]
      ,[Notes]
      ,[ExemptionStatus]
      ,[LastActReset]
      ,[LastReview]
      ,[LastReviewOper]
      ,[CreateOper]
      ,[CreateDate]
      ,[LastOper]
      ,[LastModify]
      ,[OwnerBranch]
      ,[OwnerDept]
      ,[OwnerOper]
      ,[RiskClass]
      ,[Closed]
      ,[User1]
      ,[User2]
      ,[User3]
      ,[User4]
      ,[User5]
      ,[Ts]
      ,[CountryOfOrigin]
      ,[CountryOfIncorp]
      ,[OffshoreCorp]
      ,[BearerShares]
      ,[NomeDePlume]
      ,[Type]
      ,[Resident]
      ,[BusRelationNature]
      ,[PrevRelations]
      ,[PEP]
      ,[PoliticalPos]
      ,[FEP]
      ,[MSB]
      ,[CorrBankRelation]
      ,[Sex]
      ,[ShellBank]
      ,[HighRiskRespBank]
      ,[OffshoreBank]
      ,[PayThroughAC]
      ,[RegulatedAffiliate]
      ,[USPerson]
      ,[RFCalculate]
      ,[Status]
      ,[ReviewDate]
      ,[CTRAmt]
      ,[AmtRange]
      ,[SwiftTID]
      ,[Embassy]
      ,[ForeignGovt]
      ,[CharityOrg]
      ,[DoCIP]
      ,[Prospect]
      ,[AssetSize]
      ,[Income]
      ,[IndOrBusType]
      ,[OnProbation]
      ,[ProbationReason]
      ,[ProbationStartDate]
      ,[ProbationEndDate]
      ,[ClosedDate]
      ,[ClosedReason]
      ,[OpenDate]
      ,[CountryOfResidence]
      ,[CountryOfCitizenship]
      ,[KYCDataCreateDate]
      ,[KYCDataModifyDate]
      ,[KYCOperator]
      ,[KYCStatus] from customer (nolock)'

  

       exec sp_executesql @query 
end
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRY_PartyID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CRY_PartyID]
GO

CREATE Procedure [dbo].[CRY_PartyID] (@ID varchar(35),@showNPIData int)
With Encryption
As
begin
declare @librarycontext varchar(max)
declare @createfpeobject varchar(max)
declare @isidnumberencrypted int
set @isidnumberencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('PartyID','IDNumber')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();

      If (@ID = '-ALL-')
            select CustomerId, IDType, 
Case when @showNPIData=1 and IDNumber is not null and @isidnumberencrypted>0 then pcdb.dbo.FPEAccessEx(IDNumber,@librarycontext) 
	 when @showNPIData=1 then IDNumber
	 when LEN(IDNumber)> 4 and @isidnumberencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(IDNumber,@librarycontext))-4)+ RIGHT(pcdb.dbo.FPEAccessEx(IDNumber,@librarycontext),4) 
	 when LEN(IDNumber)> 4 and @isidnumberencrypted=0 then REPLICATE('*',LEN(IDNumber)-4)+ RIGHT(IDNumber,4) 
	 when(IDNumber is not null) and @isidnumberencrypted>0 then pcdb.dbo.FPEAccessEx(IDNumber,@librarycontext) 
	else IDNumber end IDNumber,
IssueAgency, IssuePlace, IssueCountry, IssueDate, ExpiryDate, CreateOper, CreateDate, LastOper, LastModify,Ts from PartyID WITH (NOLOCK)
      Else
            select CustomerId, IDType, 
Case when @showNPIData=1 and IDNumber is not null and @isidnumberencrypted>0 then pcdb.dbo.FPEAccessEx(IDNumber,@librarycontext) 
	 when @showNPIData=1 then IDNumber
	 when LEN(IDNumber)> 4 and @isidnumberencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(IDNumber,@librarycontext))-4)+ RIGHT(pcdb.dbo.FPEAccessEx(IDNumber,@librarycontext),4) 
	 when LEN(IDNumber)> 4 and @isidnumberencrypted=0 then REPLICATE('*',LEN(IDNumber)-4)+ RIGHT(IDNumber,4) 
	 when(IDNumber is not null) and @isidnumberencrypted>0 then pcdb.dbo.FPEAccessEx(IDNumber,@librarycontext) 
	else IDNumber end IDNumber,
IssueAgency, IssuePlace, IssueCountry, IssueDate, ExpiryDate, CreateOper, CreateDate, LastOper, LastModify,Ts from PartyID WITH (NOLOCK) Where CustomerID = @ID
End
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRY_CaseManagement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CRY_CaseManagement]
GO

CREATE Procedure [dbo].[CRY_CaseManagement] (
	@BranchSelection VARCHAR(20),
	@StartDate DATETIME,
	@EndDate DATETIME,
@showNPIData int 
)

WITH ENCRYPTION AS
BEGIN

/******************************************************************************
Procedure Name: CRY_CaseManagement
Description: Cases Management Report
Parameters:
@BranchSelection = '-ALL- for across all branches or each Branch seperated by Commas
@StartDate = Start of the period
@EndDate = End of period
RETURNS TABLE
******************************************************************************/

DECLARE @Branch VARCHAR(11)
DECLARE @IndvBranch VARCHAR(10)
DECLARE	@Pos INT
declare @librarycontext varchar(max)
declare @createfpeobject varchar(max)
declare @istinencrypted int
set @istinencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','tin')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();

IF @BranchSelection = '-ALL-'
BEGIN
	SELECT
		SuspiciousActivity.CreateTime  CreatedDate,	--Date Case Created
		SuspiciousActivity.RecNo CaseNo,	--Case #
		Customer.[Name] CustomerName,	--Customer Name
		SuspiciousActivity.Cust CustomerId,	--Customer #
		Case when @showNPIData=1 and Customer.TIN is not null and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext) 
	         when @showNPIData=1 then Customer.TIN
	         when LEN(Customer.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext))-4)+ RIGHT(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),4) 
	         when LEN(Customer.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(Customer.TIN)-4)+ RIGHT(Customer.TIN,4) 
	         when(Customer.TIN is not null) and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext) 
	         else Customer.TIN end SSI,
		Account.Branch,	--Branch Name
		Customer.USPerson RelationshipManager,	--Relationship Manager
		Account.AccountOfficer AccountOfficer,	--Acct Officer
		Account.OpenDate DateAcctOpened,	--Date Acct Opened
		Customer.SecCode SECCode,	--SEC Code
		SuspiciousActivityType.[Name] Reason,	--Reason for Suspicion
		CASE Account.Closed
			WHEN	1 	THEN	'Closed'
			ELSE	'Open'	
		END	AcctStatus,	--Account Status
		SuspiciousActivity.ReviewOper Analyst,	--Analyst
		RegReport.RptFiled RptFiled,	--ReportFiled
		RegReport.Filed		Filed,	--Filed
		CASE 	WHEN	SuspiciousActivity.ClosedTime IS NULL 	THEN	'Open'
			ELSE	'Closed'	
		END	CaseStatus,	--Case Status 
		SuspiciousActivity.StartDate StartDate,	--Date SAR Filed
		SuspiciousActivity.ClosedTime DateCaseClosed,	--Date Case Closed
		CASE WHEN Customer.Onprobation = 1 THEN 'Yes' ELSE 'No' END Onprobation,	-- On Probation
		CASE WHEN Customer.PEP=1 AND Customer.FEP = 1 THEN 'Both' WHEN Customer.FEP=1 THEN 'FEP' WHEN Customer.PEP=1 THEN 'PEP' ELSE 'None' END PepFep,		
		Account.Id AccountId	-- Account Id
	FROM
		SuspiciousActivity WITH (NOLOCK)
	LEFT OUTER JOIN
		Customer WITH (NOLOCK)
	ON SuspiciousActivity.Cust = Customer.[Id]
	LEFT OUTER JOIN
		AccountOwner WITH (NOLOCK)
	ON AccountOwner.Cust = Customer.[Id]
	LEFT OUTER JOIN
		Account WITH (NOLOCK)
	ON AccountOwner.Account = Account.[Id]
	LEFT OUTER JOIN
		Country WITH (NOLOCK)
	ON Customer.Country = Country.Code
	LEFT OUTER JOIN
		SuspiciousActivityType WITH (NOLOCK)
	ON SuspiciousActivity.SuspType = SuspiciousActivityType.[Code]
	LEFT OUTER JOIN
		ActivityType WITH (NOLOCK)
	ON ActivityType.Type = SuspiciousActivity.Activity
	LEFT OUTER JOIN
		RegReport WITH (NOLOCK)
	ON Customer.[Id] = RegReport.Cust
	WHERE 
		CreateTime >= @StartDate and CreateTime <=@EndDate
	UNION
	
	SELECT
		SuspiciousActivityHist.CreateTime CreatedDate,	--Date Case Created
		SuspiciousActivityHist.RecNo CaseNo,	--Case #
		Customer.[Name] CustomerName,	--Customer Name
		SuspiciousActivityHist.Cust CustomerId,	--Customer #
		Case when @showNPIData=1 and Customer.TIN is not null and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext) 
	         when @showNPIData=1 then Customer.TIN
	         when LEN(Customer.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext))-4)+ RIGHT(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),4) 
	         when LEN(Customer.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(Customer.TIN)-4)+ RIGHT(Customer.TIN,4) 
	         when(Customer.TIN is not null) and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext) 
	         else Customer.TIN end SSI,
		Account.Branch,	--Branch Name
		Customer.USPerson RelationshipManager,	--Relationship Manager
		Account.AccountOfficer AccountOfficer,	--Acct Officer
		Account.OpenDate DateAcctOpened,	--Date Acct Opened
		Customer.SecCode SECCode,	--SEC Code
		SuspiciousActivityType.[Name] Reason,	--Reason for Suspicion
		CASE Account.Closed
			WHEN	1 	THEN	'Closed'
			ELSE	'Open'	
		END	AcctStatus,	--Account Status
		SuspiciousActivityHist.ReviewOper Analyst,	--Analyst
		RegReport.RptFiled RptFiled,	--ReportFiled
		RegReport.Filed		Filed,	--Filed
		CASE 	WHEN	SuspiciousActivityHist.ClosedTime IS NULL 	THEN	'Open'
			ELSE	'Closed'	
		END	ClosedStatus,	--Case Status 
		SuspiciousActivityHist.StartDate StartDate,	--Date SAR Filed
		SuspiciousActivityHist.ClosedTime DateCaseClosed,	--Date Case Closed
		CASE WHEN Customer.Onprobation = 1 THEN 'Yes' ELSE 'No' END Onprobation,	-- On Probation
		CASE WHEN Customer.PEP=1 AND Customer.FEP = 1 THEN 'Both' WHEN Customer.FEP=1 THEN 'FEP' WHEN Customer.PEP=1 THEN 'PEP' ELSE 'None' END PepFep,		
		Account.Id AccountId	-- Account Id
	FROM
		SuspiciousActivityHist WITH (NOLOCK)
	LEFT OUTER JOIN
		Customer WITH (NOLOCK)
	ON SuspiciousActivityHist.Cust = Customer.[Id]
	LEFT OUTER JOIN
		AccountOwner WITH (NOLOCK)
	ON AccountOwner.Cust = Customer.[Id]
	LEFT OUTER JOIN
		Account WITH (NOLOCK)
	ON AccountOwner.Account = Account.[Id]
	LEFT OUTER JOIN
		Country WITH (NOLOCK)
	ON Customer.Country = Country.Code
	LEFT OUTER JOIN
		SuspiciousActivityType WITH (NOLOCK)
	ON SuspiciousActivityHist.SuspType = SuspiciousActivityType.[Code]
	LEFT OUTER JOIN
		ActivityType WITH (NOLOCK)
	ON ActivityType.Type = SuspiciousActivityHist.Activity
	LEFT OUTER JOIN
		RegReport WITH (NOLOCK)
	ON Customer.[Id] = RegReport.Cust
	WHERE 
		CreateTime >= @StartDate and CreateTime <=@EndDate
	ORDER BY
		CustomerId

END
ELSE
BEGIN

	CREATE TABLE #TT (CreatedDate DATETIME,
			  CaseNo INT,
			  CustomerName VARCHAR(40),
			  CustomerId VARCHAR(35),
			  SSI VARCHAR(20),
			  Branch VARCHAR(11), 
			  RelationshipManager VARCHAR(40),
			  AccountOfficer VARCHAR(40),
			  DateAcctOpened DATETIME,
			  SECCode VARCHAR(10),
			  Reason VARCHAR(40),
			  AcctStatus VARCHAR(11), 
			  Analyst VARCHAR(11), 
			  RptFiled BIT,
			  Filed DATETIME,
			  CaseStatus VARCHAR(11), 
			  StartDate DATETIME,
			  DateCaseClosed DATETIME,
			  Onprobation VARCHAR(5),
			  PepFep VARCHAR(5),
			  AccountId VARCHAR(35)
			)


	SET @BranchSelection = LTRIM(RTRIM(@BranchSelection))+ ','
	SET @Pos = CHARINDEX(',', @BranchSelection, 1)

	-- Logic for sorting the values based upon the Branches entered by the User

	IF REPLACE(@BranchSelection, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
	 
			SET @IndvBranch = LTRIM(RTRIM(LEFT(@BranchSelection, @Pos - 1)))
			IF @IndvBranch <> ''
			BEGIN
				
				INSERT INTO #TT
				SELECT
					SuspiciousActivity.CreateTime  CreatedDate,	--Date Case Created
					SuspiciousActivity.RecNo CaseNo,	--Case #
					Customer.[Name] CustomerName,	--Customer Name
					SuspiciousActivity.Cust CustomerId,	--Customer #
					Case when @showNPIData=1 and Customer.TIN is not null and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20) 
	                     when @showNPIData=1 then Customer.TIN
	                     when LEN(Customer.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20))-4)+ RIGHT(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20),4) 
	                     when LEN(Customer.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(Customer.TIN)-4)+ RIGHT(Customer.TIN,4) 
	                     when(Customer.TIN is not null) and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20) 
	                     else Customer.TIN end SSI,
					Account.Branch,	--Branch Name
					Customer.USPerson RelationshipManager,	--Relationship Manager
					Account.AccountOfficer AccountOfficer,	--Acct Officer
					Account.OpenDate DateAcctOpened,	--Date Acct Opened
					Customer.SecCode SECCode,	--SEC Code
					SuspiciousActivityType.[Name] Reason,	--Reason for Suspicion
					CASE Account.Closed
						WHEN	1 	THEN	'Closed'
						ELSE	'Open'	
					END	AcctStatus,	--Account Status
					SuspiciousActivity.ReviewOper Analyst,	--Analyst
					RegReport.RptFiled RptFiled,	--ReportFiled
					RegReport.Filed		Filed,	--Filed
					CASE 	WHEN	SuspiciousActivity.ClosedTime IS NULL 	THEN	'Open'
						ELSE	'Closed'	
					END	ClosedStatus,	--Case Status 
					SuspiciousActivity.StartDate StartDate,	--Date SAR Filed
					SuspiciousActivity.ClosedTime DateCaseClosed,	--Date Case Closed
					CASE WHEN Customer.Onprobation = 1 THEN 'Yes' ELSE 'No' END Onprobation,	-- On Probation
					CASE WHEN Customer.PEP=1 AND Customer.FEP = 1 THEN 'Both' WHEN Customer.FEP=1 THEN 'FEP' WHEN Customer.PEP=1 THEN 'PEP' ELSE 'None' END PepFep,		
					Account.Id AccountId	-- Account Id
				FROM
					SuspiciousActivity WITH (NOLOCK)
				LEFT OUTER JOIN
					Customer WITH (NOLOCK)
				ON SuspiciousActivity.Cust = Customer.[Id]
				LEFT OUTER JOIN
					AccountOwner WITH (NOLOCK)
				ON AccountOwner.Cust = Customer.[Id]
				LEFT OUTER JOIN
					Account WITH (NOLOCK)
				ON AccountOwner.Account = Account.[Id]
				LEFT OUTER JOIN
					Country WITH (NOLOCK)
				ON Customer.Country = Country.Code
				LEFT OUTER JOIN
					SuspiciousActivityType WITH (NOLOCK)
				ON SuspiciousActivity.SuspType = SuspiciousActivityType.[Code]
				LEFT OUTER JOIN
					ActivityType WITH (NOLOCK)
				ON ActivityType.Type = SuspiciousActivity.Activity
				LEFT OUTER JOIN
					RegReport WITH (NOLOCK)
				ON Customer.[Id] = RegReport.Cust
				WHERE 
					CreateTime >= @StartDate and CreateTime <=@EndDate
					AND Account.Branch = @IndvBranch
				UNION
				
				SELECT
					SuspiciousActivityHist.CreateTime CreatedDate,	--Date Case Created
					SuspiciousActivityHist.RecNo CaseNo,	--Case #
					Customer.[Name] CustomerName,	--Customer Name
					SuspiciousActivityHist.Cust CustomerId,	--Customer #
					Case when @showNPIData=1 and Customer.TIN is not null and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20) 
	                     when @showNPIData=1 then Customer.TIN
	                     when LEN(Customer.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20))-4)+ RIGHT(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20),4) 
	                     when LEN(Customer.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(Customer.TIN)-4)+ RIGHT(Customer.TIN,4) 
	                     when(Customer.TIN is not null) and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20) 
	                     else Customer.TIN end SSI,
					Account.Branch,	--Branch Name
					Customer.USPerson RelationshipManager,	--Relationship Manager
					Account.AccountOfficer AccountOfficer,	--Acct Officer
					Account.OpenDate DateAcctOpened,	--Date Acct Opened
					Customer.SecCode SECCode,	--SEC Code
					SuspiciousActivityType.[Name] Reason,	--Reason for Suspicion
					CASE Account.Closed
						WHEN	1 	THEN	'Closed'
						ELSE	'Open'	
					END	AcctStatus,	--Account Status
					SuspiciousActivityHist.ReviewOper Analyst,	--Analyst
					RegReport.RptFiled RptFiled,	--ReportFiled
					RegReport.Filed		Filed,	--Filed
					CASE 	WHEN	SuspiciousActivityHist.ClosedTime IS NULL 	THEN	'Open'
						ELSE	'Closed'	
					END	ClosedStatus,	--Case Status 
					SuspiciousActivityHist.StartDate StartDate,	--Date SAR Filed
					SuspiciousActivityHist.ClosedTime DateCaseClosed,	--Date Case Closed
					CASE WHEN Customer.Onprobation = 1 THEN 'Yes' ELSE 'No' END Onprobation,	-- On Probation
					CASE WHEN Customer.PEP=1 AND Customer.FEP = 1 THEN 'Both' WHEN Customer.FEP=1 THEN 'FEP' WHEN Customer.PEP=1 THEN 'PEP' ELSE 'None' END PepFep,		
					Account.Id	-- Account Id
				FROM
					SuspiciousActivityHist WITH (NOLOCK)
				LEFT OUTER JOIN
					Customer WITH (NOLOCK)
				ON SuspiciousActivityHist.Cust = Customer.[Id]
				LEFT OUTER JOIN
					AccountOwner WITH (NOLOCK)
				ON AccountOwner.Cust = Customer.[Id]
				LEFT OUTER JOIN
					Account WITH (NOLOCK)
				ON AccountOwner.Account = Account.[Id]
				LEFT OUTER JOIN
					Country WITH (NOLOCK)
				ON Customer.Country = Country.Code
				LEFT OUTER JOIN
					SuspiciousActivityType WITH (NOLOCK)
				ON SuspiciousActivityHist.SuspType = SuspiciousActivityType.[Code]
				LEFT OUTER JOIN
					ActivityType WITH (NOLOCK)
				ON ActivityType.Type = SuspiciousActivityHist.Activity
				LEFT OUTER JOIN
					RegReport WITH (NOLOCK)
				ON Customer.[Id] = RegReport.Cust
				WHERE 
					CreateTime >= @StartDate and CreateTime <=@EndDate
					AND Account.Branch = @IndvBranch
				ORDER BY
					CustomerId

			END
			SET @BranchSelection = RIGHT(@BranchSelection, LEN(@BranchSelection) - @Pos)
			SET @Pos = CHARINDEX(',', @BranchSelection, 1)
	 
		END
	END	
	SELECT CreatedDate,CaseNo,CustomerName,CustomerId,SSI,Branch, RelationshipManager,AccountOfficer,DateAcctOpened,
				SECCode,Reason,AcctStatus, Analyst, RptFiled,Filed,CaseStatus, StartDate,DateCaseClosed,Onprobation,PepFep,AccountId
	 FROM #TT order by Branch, CustomerId 
	
	DROP TABLE #TT
	END

END
GO





IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRY_CustInfoByID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CRY_CustInfoByID]
GO

CREATE Procedure [dbo].[CRY_CustInfoByID] (@custID ObjectID, @showNPIData int)
WITH ENCRYPTION AS
Begin
      declare @librarycontext varchar(max)
      declare @createfpeobject varchar(max)
      declare @istinencrypted int
      set @istinencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','tin') 
      declare @islicensenoencrypted int
      set @islicensenoencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','LicenseNo')
      declare @ispassportnoencrypted int
      set @ispassportnoencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','PassportNo')
      select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();

       SELECT
      Id,Parent,Name,DBA,
      SecCode,Address,City,State,Zip,Country,
      Telephone,Email,
		Case when @showNPIData=1 and TIN is not null and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(TIN,@librarycontext) 
	         when @showNPIData=1 then TIN
	         when LEN(TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(TIN,@librarycontext))-4)+ RIGHT(pcdb.dbo.FPEAccessEx(TIN,@librarycontext),4) 
	         when LEN(TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(TIN)-4)+ RIGHT(TIN,4) 
	         when (TIN is not null) and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(TIN,@librarycontext) 
	         else TIN end TIN,	--RUT/EIN/SSI
	    Case when @showNPIData=1 and LicenseNo is not null and @islicensenoencrypted>0 then pcdb.dbo.FPEAccessEx(LicenseNo,@librarycontext) 
	         when @showNPIData=1 then LicenseNo
	         when LEN(LicenseNo)> 4 and @islicensenoencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(LicenseNo,@librarycontext))-4)+ RIGHT(pcdb.dbo.FPEAccessEx(LicenseNo,@librarycontext),4) 
	         when LEN(LicenseNo)> 4 and @islicensenoencrypted=0 then REPLICATE('*',LEN(LicenseNo)-4)+ RIGHT(LicenseNo,4) 
	         when (LicenseNo is not null) and @islicensenoencrypted>0 then pcdb.dbo.FPEAccessEx(LicenseNo,@librarycontext) 
	         else LicenseNo end LicenseNo,	
	  LicenseState,
	    Case when @showNPIData=1 and PassportNo is not null and @ispassportnoencrypted>0 then pcdb.dbo.FPEAccessEx(PassportNo,@librarycontext) 
	  	     when @showNPIData=1 then PassportNo
	         when LEN(PassportNo)> 4 and @ispassportnoencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(PassportNo,@librarycontext))-4)+ RIGHT(pcdb.dbo.FPEAccessEx(PassportNo,@librarycontext),4) 
	         when LEN(PassportNo)> 4 and @ispassportnoencrypted=0 then REPLICATE('*',LEN(PassportNo)-4)+ RIGHT(PassportNo,4) 
	         when (PassportNo is not null) and @ispassportnoencrypted>0 then pcdb.dbo.FPEAccessEx(PassportNo,@librarycontext) 
	         else PassportNo end PassportNo,
	   DOB,
      TypeOfBusiness,SourceOfFunds,
      AccountOfficer,AcctOffTel,AcctOffEmail,
      CompOfficer,CompOffTel,CompOffEmail,
      IdList,Notes,ExemptionStatus,
      LastActReset,LastReview,LastReviewOper,
      CreateOper,CreateDate,LastOper,LastModify,
      OwnerBranch,OwnerDept,OwnerOper,RiskClass,Closed,
      CountryOfOrigin,
      CountryOfIncorp,OffshoreCorp,BearerShares,NomeDePlume,
      AssetSize,Income,
      ClosedDate,OpenDate,
      OnProbation,ProbationReason,ProbationStartDate,ProbationEndDate,
      CountryOfResidence,CountryOfCitizenship,ClosedReason,
    KYCDataCreateDate, KYCDataModifyDate, KYCStatus
      FROM dbo.customer c WITH (NOLOCK)
      WHERE       RTRIM(UPPER(@custID)) = 'ALL' or c.ID = @custID

  End
Go


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRY_ProbationListLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CRY_ProbationListLog]
GO

CREATE Procedure [dbo].[CRY_ProbationListLog] (
@BranchList         VARCHAR(1000),
@showNPIData int
)
WITH ENCRYPTION AS
BEGIN
/**********
Procedure Name: CRY_ProbationListLog
Description: A list of all customers currently on the Probation list
Parameters:
RETURNS TABLE
********/
declare @librarycontext varchar(max)
declare @createfpeobject varchar(max)
declare @istinencrypted int
set @istinencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','tin')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();

--Check what value is passed in BranchList parameter.
--If NULL is passed, we will set BranchList to NULL. Otherwise we will pass the value in the query.
IF(ISNULL(@BranchList, '') = '' OR UPPER(ISNULL(@BranchList, '-ALL-')) = '-ALL-')
    SELECT @BranchList = NULL
ELSE
    -- We need to remove the white space from the branch list.
    --For example, if user has entered 'NY, Miami' (notice the space after comma) 
    --we want it to be 'NY,Miami'.
    SELECT @BranchList = ',' + Replace(IsNull(LTRIM(RTRIM(@BranchList)), ''),', ',',') + ','

SELECT Customer.[Id] CustomerId,	        --Customer#
	   dbo.BSA_ConvertIntToSqlDate(Customer.ProbationStartDate, 'mm/dd/yyyy') DateOnList,	--Date On List
	   dbo.BSA_ConvertIntToSqlDate(Customer.ProbationEndDate, 'mm/dd/yyyy') ExpirationDateOfProbation,	--Expiration Date of Probation
	   Customer.[Name] CustomerName,	    --Customer Name
	   CodeTbl.[Name] ReasonForProbation,	--Reason for Probation
	   Account.[Name] AccountName,	        --Account Name
	   Account.[Id] AccountId,	            --Account #
	  Case when @showNPIData=1 and Customer.TIN is not null and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext) 
	       when @showNPIData=1 then Customer.TIN
	       when LEN(Customer.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext))-4)+ RIGHT(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),4) 
	       when LEN(Customer.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(Customer.TIN)-4)+ RIGHT(Customer.TIN,4) 
	       when(Customer.TIN is not null) and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext) 
	       else Customer.TIN end SSI,
	   Account.Branch USBranchName,	        --Branch Name
	   Customer.USPerson RelationshipManager,	--Relationship Manager
	   Account.AccountOfficer AccountOfficer,	--Acct Officer
	   Account.OpenDate DateOpened,	        --Date Acct Opened
	   Customer.SecCode Occupation,	        --Occupation/Business Type
	   CASE WHEN Customer.PEP=1 AND Customer.FEP = 1 THEN 'Both' WHEN Customer.FEP=1 THEN 'FEP' WHEN Customer.PEP=1 THEN 'PEP' ELSE 'None' END PepFep
  FROM Customer WITH (NOLOCK),
	   AccountOwner WITH (NOLOCK),
       Account WITH (NOLOCK),
	   CodeTbl WITH (NOLOCK)
 WHERE Customer.OnProbation = 1	--Look for Customers on Probation list
   AND AccountOwner.Cust = Customer.[Id]
   AND Account.[Id] = AccountOwner.Account
   AND Customer.ProbationReason = CodeTbl.Code
   AND (@BranchList IS NULL OR CharIndex(',' + CONVERT(VARCHAR, Account.Branch) + ',', @BranchList) > 0)
 ORDER 
    BY CustomerName

END
GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRY_SAByDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CRY_SAByDate]
GO

CREATE Procedure [dbo].[CRY_SAByDate] (
	@RegReportType          VARCHAR(6),
	@StartDate              DATETIME,
	@EndDate                DATETIME,
    @BranchList             VARCHAR(1000),
@showNPIData int)
WITH ENCRYPTION AS
BEGIN
/******************************************************************************
Procedure Name: CRY_SAByDate
Description: List of SARs/CTRs filed by date filed 
Parameters:
@RegReportType = Type of Regulatory Report (CTR/SAR)
@StartDate = Start of the period
@EndDate = End of period
RETURNS TABLE
******************************************************************************/
declare @librarycontext varchar(max)
declare @createfpeobject varchar(max)
declare @istinencrypted int
set @istinencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','tin')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();

CREATE TABLE #TT (
	CaseNo INT, --Case number
	CustomerName VARCHAR(40),	--CustomerName 
	PEP CHAR(3), -- PEP 
	CustomerId VARCHAR(35),	-- Customer no 
	SSI VARCHAR(20),	--SSI/EIN/RUT 
	USBranchName VARCHAR(40),	--US Branch Name 
	AcctStatus CHAR(6),	--Account Status 
	AcctClosedDate DATETIME,	--Account Closed Date 
	IdentifiedAsSuspicious DATETIME,	--IdentifiedAsSuspicious 
	RptFiled CHAR(3),
	FiledDate DATETIME,
	Reason VARCHAR(40),	--Reason 
	Preparer VARCHAR(11),	--Preparer 
	MailReceiptNo VARCHAR(40),	--MailReceiptNo  
	TccCode Varchar(11),  --TccCode
	Notes VARCHAR(2000),	--Notes	
	InitialDate DATETIME
)

--Check what value is passed in BranchList parameter.
--If NULL is passed, we will set BranchList to NULL. Otherwise we will pass the value in the query.
IF(ISNULL(@BranchList, '') = '' OR UPPER(ISNULL(@BranchList, '-ALL-')) = '-ALL-')
    SELECT @BranchList = NULL
ELSE
    -- We need to remove the white space from the branch list.
    --For example, if user has entered 'NY, Miami' (notice the space after comma) 
    --we want it to be 'NY,Miami'.
    SELECT @BranchList = ',' + Replace(IsNull(LTRIM(RTRIM(@BranchList)), ''),', ',',') + ','


INSERT INTO #TT 
	SELECT
		SuspiciousActivity.RecNo CaseNo, --Case number 
		Customer.[Name] CustomerName,	--CustomerName 
		CASE Customer.PEP WHEN 1 THEN 'Yes' ELSE 'No' END PEP, -- PEP 
		SuspiciousActivity.Cust CustomerId,	-- Customer no 
		Case when @showNPIData=1 and Customer.TIN is not null and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20) 
	         when @showNPIData=1 then Customer.TIN
	         when LEN(Customer.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20))-4)+ RIGHT(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20),4) 
	         when LEN(Customer.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(Customer.TIN)-4)+ RIGHT(Customer.TIN,4) 
	         when(Customer.TIN is not null) and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20) 
	         else Customer.TIN end SSI,
		Account.Branch USBranchName,	--US Branch Name 
		CASE Account.Closed
			WHEN	1 	THEN	'Closed'
			ELSE	'Open'
		END	AcctStatus,	--Account Status 
		Account.CloseDate AcctClosedDate,	--Account Closed Date 
		RegReport.Approved IdentifiedAsSuspicious,	--IdentifiedAsSuspicious 
		RptFiled = Case RegReport.RptFiled
			WHEN 1 THEN 'Yes' 
			ELSE 'No' 
		END,	--SARFiled 
		RegReport.Filed FiledDate,
		RegReport.Reason,	--Reason 
		RegReport.PreparedBy Preparer,	--Preparer 
		RegReport.MailReceiptNo MailReceiptNo,	--MailReceiptNo   
		Regreport.TccCode, -- TccCode
		SuspiciousActivity.Descr Notes,	--Notes	
		NULL
	FROM
		SuspiciousActivity WITH (NOLOCK)
		INNER JOIN
		RegReport WITH (NOLOCK)
		ON SuspiciousActivity.RecNo = RegReport.SARecNo
		LEFT OUTER JOIN
		Customer WITH (NOLOCK)
		ON SuspiciousActivity.Cust = Customer.[Id]
		LEFT OUTER JOIN
		AccountOwner WITH (NOLOCK)
		ON AccountOwner.Cust = Customer.[Id]
		LEFT OUTER JOIN
		Account WITH (NOLOCK)
		ON AccountOwner.Account = Account.[Id]
		LEFT OUTER JOIN
		SuspiciousActivityType WITH (NOLOCK)
		ON SuspiciousActivity.SuspType = SuspiciousActivityType.[Code]
	WHERE 
		CreateTime >= @StartDate AND CreateTime <= @EndDate
		AND RegReport.Type = @RegReportType
        AND (@BranchList IS NULL OR CharIndex(',' + CONVERT(VARCHAR, Account.Branch) + ',', @BranchList) > 0)
	UNION
	
	SELECT
		SuspiciousActivityHist.RecNo CaseNo, --Case number 
		Customer.[Name] CustomerName,	--CustomerName 
		CASE Customer.PEP WHEN 1 THEN 'Yes' ELSE 'No' END PEP, -- PEP 
		SuspiciousActivityHist.Cust CustomerId,	-- Customer no 
		Case when @showNPIData=1 and Customer.TIN is not null and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20) 
	         when @showNPIData=1 then Customer.TIN
	         when LEN(Customer.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20))-4)+ RIGHT(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20),4) 
	         when LEN(Customer.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(Customer.TIN)-4)+ RIGHT(Customer.TIN,4) 
	         when(Customer.TIN is not null) and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20) 
	         else Customer.TIN end SSI,
		Account.Branch USBranchName,	--US Branch Name 
		CASE Account.Closed
			WHEN	1 	THEN	'Closed'
			ELSE	'Open'
		END	AcctStatus,	--Account Status 
		Account.CloseDate AcctClosedDate,	--Account Closed Date
		RegReport.Approved IdentifiedAsSuspicious,	--IdentifiedAsSuspicious 
		RptFiled = Case RegReport.RptFiled
			WHEN 1 THEN 'Yes' 
			ELSE 'No' 
		END,	--SARFiled 
		RegReport.Filed FiledDate,
		RegReport.Reason,	--Reason,
		RegReport.PreparedBy Preparer,	--Preparer RegReport.PreparedBy
		RegReport.MailReceiptNo,	--MailReceiptNo  
		Regreport.TccCode, -- TccCode
		SuspiciousActivityHist.Descr Notes,	--Notes
		NULL
	FROM
		SuspiciousActivityHist WITH (NOLOCK)
		INNER JOIN
		RegReport WITH (NOLOCK)
		ON SuspiciousActivityHist.RecNo = RegReport.SARecNo
		LEFT OUTER JOIN
		Customer WITH (NOLOCK)
		ON SuspiciousActivityHist.Cust = Customer.[Id]
		LEFT OUTER JOIN
		AccountOwner WITH (NOLOCK)
		ON AccountOwner.Cust = Customer.[Id]
		LEFT OUTER JOIN
		Account WITH (NOLOCK)
		ON AccountOwner.Account = Account.[Id]
		LEFT OUTER JOIN
		SuspiciousActivityType WITH (NOLOCK)
		ON SuspiciousActivityHist.SuspType = SuspiciousActivityType.[Code]
	WHERE 
		CreateTime >= @StartDate AND CreateTime <= @EndDate
		AND RegReport.Type = @RegReportType
        AND (@BranchList IS NULL OR CharIndex(',' + CONVERT(VARCHAR, Account.Branch) + ',', @BranchList) > 0)
	ORDER BY
		CaseNo

UPDATE #TT SET
	initialDate = (SELECT MIN(CONVERT(DATETIME, TrnTime, 101)) FROM Event WHERE
	CONVERT(VARCHAR(10), #tt.CaseNo) = Event.ObjectId and
	Event.objectType = 'SuspAct' 
	and Event.Type = 'Mod')

SELECT * FROM #TT

DROP TABLE #TT
END
GO





IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRY_CasesPending]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CRY_CasesPending]
GO

CREATE Procedure [dbo].[CRY_CasesPending] (
@BranchSelection VARCHAR(30),
@showNPIData int 
)
WITH ENCRYPTION AS
BEGIN
/******************************************************************************
Procedure Name: CRY_CasesPending
Description: Cases pending
Parameters:
@BranchSelection = '-ALL- for across all branches or each Branch seperated by Commas
RETURNS TABLE
******************************************************************************/


DECLARE @Branch VARCHAR(11)
DECLARE @IndvBranch VARCHAR(11)
DECLARE	@Pos INT
declare @librarycontext varchar(max)
declare @createfpeobject varchar(max)
declare @istinencrypted int
set @istinencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','tin')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();
IF @BranchSelection = '-ALL-'
BEGIN 
	SELECT
		SuspiciousActivity.CreateTime CreatedDate,	--Date Case Created
		SuspiciousActivity.RecNo CaseNo,	--Case #
		Case when @showNPIData=1 and Customer.TIN is not null and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext) 
	         when @showNPIData=1 then Customer.TIN
	         when LEN(Customer.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext))-4)+ RIGHT(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),4) 
	         when LEN(Customer.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(Customer.TIN)-4)+ RIGHT(Customer.TIN,4) 
	         when(Customer.TIN is not null) and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext) 
	         else Customer.TIN end SSI,
		Account.Branch,	--Branch Name
		Customer.USPerson RelationshipManager,	--Relationship Manager
		Account.AccountOfficer AccountOfficer,	--Acct Officer
		SuspiciousActivity.StartDate StartDate,  --Date Case Started
		Account.OpenDate DateOpened,	--Date Acct Opened
		Customer.SecCode SECCode,	--SEC Code : Occupation/Business Type
		SuspiciousActivityType.[Name] Reason,	--Reason for Suspicion
		dbo.GetReviewState(SuspiciousActivity.ReviewState) CaseStatus,	--"Case Status"
		SuspiciousActivity.ReviewOper Analyst,	--Analyst
		Customer.[Name] CustomerName,	--Customer Name
		SuspiciousActivity.Cust CustomerId,	--Customer #
		CASE WHEN Customer.Onprobation = 1 THEN 'Yes' ELSE 'No' END Onprobation,	-- On Probation
		Account.Id AccountId,	-- Account Id
		CASE WHEN Customer.PEP=1 AND Customer.FEP = 1 THEN 'Both' WHEN Customer.FEP=1 THEN 'FEP' WHEN Customer.PEP=1 THEN 'PEP' ELSE 'None' END PepFep
	FROM
		SuspiciousActivity WITH (NOLOCK)
		LEFT OUTER JOIN
		Customer WITH (NOLOCK)
		ON SuspiciousActivity.Cust = Customer.[Id]
		LEFT OUTER JOIN
		AccountOwner WITH (NOLOCK)
		ON Customer.[Id] = AccountOwner.Cust
		LEFT OUTER JOIN
		Account WITH (NOLOCK)
		ON AccountOwner.Account = Account.[Id]
		INNER JOIN
		SuspiciousActivityType WITH (NOLOCK)
		ON SuspiciousActivity.SuspType = SuspiciousActivityType.[Code]
		LEFT OUTER JOIN
		ActivityType WITH (NOLOCK)
		ON ActivityType.Type = SuspiciousActivity.Activity
		LEFT OUTER JOIN
		RegReport WITH (NOLOCK)
		ON Customer.[Id] = RegReport.Cust
	WHERE 
		SuspiciousActivity.ClosedTime IS NULL --Check for all the pending cases
	ORDER BY
		CustomerId 
END
ELSE
BEGIN

	CREATE TABLE #TT (CreatedDate DATETIME,
			  CaseNo INT,
			  SSI VARCHAR(20),
			  Branch VARCHAR(11), 
			  RelationshipManager VARCHAR(40),
			  AccountOfficer VARCHAR(40),
			  StartDate DATETIME,
			  DateOpened DATETIME,
			  SECCode VARCHAR(10),
			  Reason VARCHAR(40),
			  CaseStatus VARCHAR(30), 
			  Analyst VARCHAR(11), 
			  CustomerName VARCHAR(40),
			  CustomerId VARCHAR(35),
			  Onprobation VARCHAR(5),
			  AccountId VARCHAR(35),
			  PepFep VARCHAR(5)
			)

	SET @BranchSelection = LTRIM(RTRIM(@BranchSelection))+ ','
	SET @Pos = CHARINDEX(',', @BranchSelection, 1)

	-- Logic for sorting the values based upon the Branches entered by the User

	IF REPLACE(@BranchSelection, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
	 
			SET @IndvBranch = LTRIM(RTRIM(LEFT(@BranchSelection, @Pos - 1)))
			IF @IndvBranch <> ''
			BEGIN
				
				INSERT INTO #TT
				SELECT
					SuspiciousActivity.CreateTime CreatedDate,	--Date Case Created
					SuspiciousActivity.RecNo CaseNo,	--Case #
					Case when @showNPIData=1 and Customer.TIN is not null and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20) 
	                     when @showNPIData=1 then Customer.TIN
	                     when LEN(Customer.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20))-4)+ RIGHT(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20),4) 
	                     when LEN(Customer.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(Customer.TIN)-4)+ RIGHT(Customer.TIN,4) 
	                     when(Customer.TIN is not null) and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20)
	                     else Customer.TIN end SSI,
					Account.Branch,	--Branch Name
					Customer.USPerson RelationshipManager,	--Relationship Manager
					Account.AccountOfficer AccountOfficer,	--Acct Officer
					SuspiciousActivity.StartDate StartDate,  --Date Case Started
					Account.OpenDate DateOpened,	--Date Acct Opened
					Customer.SecCode SECCode,	--SEC Code : Occupation/Business Type
					SuspiciousActivityType.[Name] Reason,	--Reason for Suspicion
					dbo.GetReviewState(SuspiciousActivity.ReviewState) CaseStatus,	--"Case Status"
					SuspiciousActivity.ReviewOper Analyst,	--Analyst
					Customer.[Name] CustomerName,	--Customer Name
					SuspiciousActivity.Cust CustomerId,	--Customer #
					CASE WHEN Customer.Onprobation = 1 THEN 'Yes' ELSE 'No' END Onprobation, -- On Probation
					Account.Id,	-- Account Id
					CASE WHEN Customer.PEP=1 AND Customer.FEP = 1 THEN 'Both' WHEN Customer.FEP=1 THEN 'FEP' WHEN Customer.PEP=1 THEN 'PEP' ELSE 'None' END PepFep
				FROM
					SuspiciousActivity WITH (NOLOCK)
					LEFT OUTER JOIN
					Customer WITH (NOLOCK)
					ON SuspiciousActivity.Cust = Customer.[Id]
					LEFT OUTER JOIN
					AccountOwner WITH (NOLOCK)
					ON Customer.[Id] = AccountOwner.Cust
					LEFT OUTER JOIN
					Account WITH (NOLOCK)
					ON AccountOwner.Account = Account.[Id]
					INNER JOIN
					SuspiciousActivityType WITH (NOLOCK)
					ON SuspiciousActivity.SuspType = SuspiciousActivityType.[Code]
					LEFT OUTER JOIN
					ActivityType WITH (NOLOCK)
					ON ActivityType.Type = SuspiciousActivity.Activity
					LEFT OUTER JOIN
					RegReport WITH (NOLOCK)
					ON Customer.[Id] = RegReport.Cust
				WHERE 
					SuspiciousActivity.ClosedTime IS NULL --Check for all the pending cases
					AND Account.Branch = @IndvBranch
				ORDER BY
					CustomerId 
			END
			SET @BranchSelection = RIGHT(@BranchSelection, LEN(@BranchSelection) - @Pos)
			SET @Pos = CHARINDEX(',', @BranchSelection, 1)
	 
		END
	END	
	SELECT * FROM #TT order by Branch, CustomerId  
	
	DROP TABLE #TT
	END

END
GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRY_CasesClosed]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CRY_CasesClosed]
GO

CREATE Procedure [dbo].[CRY_CasesClosed] (@BranchSelection VARCHAR(20),
	@StartDate DATETIME,
	@EndDate DATETIME,
@showNPIData int) 
WITH ENCRYPTION AS
BEGIN
/******************************************************************************
Procedure Name: CRY_CasesClosed
Description:Closed cases by date
Parameters:
@BranchSelection = '-ALL- for across all branches or each Branch seperated by Commas
@StartDate = Start of the period
@EndDate = End of period
RETURNS TABLE
******************************************************************************/

DECLARE @Branch VARCHAR(11)
DECLARE @IndvBranch VARCHAR(10)
DECLARE	@Pos INT
declare @librarycontext varchar(max)
declare @createfpeobject varchar(max)
declare @istinencrypted int
set @istinencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','tin')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();

IF @BranchSelection = '-ALL-'
BEGIN
	SELECT
		SuspiciousActivity.StartDate StartDate,	--Date Case Started
		dbo.GetReviewState(SuspiciousActivity.ReviewState) ActionClosingCase,	--Action Closing Case
		SuspiciousActivity.RecNo CaseNo,	--Case #
		Case when @showNPIData=1 and Customer.TIN is not null and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext) 
	         when @showNPIData=1 then Customer.TIN
	         when LEN(Customer.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext))-4)+ RIGHT(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),4) 
	         when LEN(Customer.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(Customer.TIN)-4)+ RIGHT(Customer.TIN,4) 
	         when(Customer.TIN is not null) and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext) 
	         else Customer.TIN end SSI,
		Account.Branch,	--Branch Name
		Customer.USPerson RelationshipManager,	--Relationship Manager
		Account.AccountOfficer AccountOfficer,	--Acct Officer
		Account.OpenDate DateOpened,	--Date Acct Opened
		Customer.SecCode SECCode,	--SEC Code : Occupation/Business Type
		SuspiciousActivityType.[Name] Reason,	--Reason for Suspicion
		CASE Account.Closed
			WHEN	1 	THEN	'Closed'
			ELSE	'Open'	
		END	AcctStatus,	--Account Status
		SuspiciousActivity.ReviewOper Analyst,	--Analyst
		RegReport.RptFiled, -- if Report is filed or not
		RegReport.Filed, -- Report Filed Date
		SuspiciousActivity.ClosedTime DateCaseClosed,	--Date Case Closed
		Customer.[Name] CustomerName,	--Customer Name
		CASE WHEN Customer.Onprobation = 1 THEN 'Yes' ELSE 'No' END Onprobation,	-- On Probation
		SuspiciousActivity.Cust CustomerId,	--Customer #
		CASE WHEN Customer.PEP=1 AND Customer.FEP = 1 THEN 'Both' WHEN Customer.FEP=1 THEN 'FEP' WHEN Customer.PEP=1 THEN 'PEP' ELSE 'None' END PepFep,		
		Account.Id AccountId	-- Account Id
	FROM
		SuspiciousActivity WITH (NOLOCK)
		LEFT OUTER JOIN
		Customer WITH (NOLOCK)
		ON SuspiciousActivity.Cust = Customer.[Id]
		LEFT OUTER JOIN
		AccountOwner WITH (NOLOCK)
		ON Customer.[Id] = AccountOwner.Cust
		LEFT OUTER JOIN
		Account WITH (NOLOCK)
		ON AccountOwner.Account = Account.[Id]
		LEFT OUTER JOIN
		Country WITH (NOLOCK)
		ON Customer.Country = Country.Code
		LEFT OUTER JOIN
		SuspiciousActivityType WITH (NOLOCK)
		ON SuspiciousActivity.SuspType = SuspiciousActivityType.[Code]
		LEFT OUTER JOIN
		ActivityType WITH (NOLOCK)
		ON ActivityType.Type = SuspiciousActivity.Activity
		LEFT OUTER JOIN
		RegReport WITH (NOLOCK)
		ON Customer.[Id] = RegReport.Cust
	WHERE 
		SuspiciousActivity.ClosedTime >=@StartDate and SuspiciousActivity.ClosedTime <=@EndDate --Check for Cases closed between the StartDate and EndDate
	
	UNION
	
	SELECT
		SuspiciousActivityHist.StartDate StartDate,	--Date Case Started
		dbo.GetReviewState(SuspiciousActivityHist.ReviewState) ActionClosingCase,	--Action Closing Case
		SuspiciousActivityHist.RecNo CaseNo,	--Case #
		Case when @showNPIData=1 and Customer.TIN is not null and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext) 
	         when @showNPIData=1 then Customer.TIN
	         when LEN(Customer.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext))-4)+ RIGHT(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),4) 
	         when LEN(Customer.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(Customer.TIN)-4)+ RIGHT(Customer.TIN,4) 
	         when(Customer.TIN is not null) and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext) 
	         else Customer.TIN end SSI,
		Account.Branch,	--Branch Name
		Customer.USPerson RelationshipManager,	--Relationship Manager
		Account.AccountOfficer AccountOfficer,	--Acct Officer
		Account.OpenDate DateOpened,	--Date Acct Opened
		Customer.SecCode SECCode,	--SEC Code : Occupation/Business Type
		SuspiciousActivityType.[Name] Reason,	--Reason for Suspicion
		CASE Account.Closed
			WHEN	1 	THEN	'Closed'
			ELSE	'Open'	
		END	AcctStatus,	--Account Status
		SuspiciousActivityHist.ReviewOper Analyst,	--Analyst
		RegReport.RptFiled, -- if Report is filed or not
		RegReport.Filed, -- Report Filed Date
		SuspiciousActivityHist.ClosedTime DateCaseClosed,	--Date Case Closed
		Customer.[Name] CustomerName,	--Customer Name
		CASE WHEN Customer.Onprobation = 1 THEN 'Yes' ELSE 'No' END Onprobation,	-- On Probation
		SuspiciousActivityHist.Cust CustomerId,	--Customer #
		CASE WHEN Customer.PEP=1 AND Customer.FEP = 1 THEN 'Both' WHEN Customer.FEP=1 THEN 'FEP' WHEN Customer.PEP=1 THEN 'PEP' ELSE 'None' END PepFep,		
		Account.Id AccountId	-- Account Id
	FROM
		SuspiciousActivityHist WITH (NOLOCK)
		LEFT OUTER JOIN
		Customer WITH (NOLOCK)
		ON SuspiciousActivityHist.Cust = Customer.[Id]
		LEFT OUTER JOIN
		AccountOwner WITH (NOLOCK)
		ON Customer.[Id] = AccountOwner.Cust
		LEFT OUTER JOIN
		Account WITH (NOLOCK)
		ON AccountOwner.Account = Account.[Id]
		LEFT OUTER JOIN
		Country WITH (NOLOCK)
		ON Customer.Country = Country.Code
		LEFT OUTER JOIN
		SuspiciousActivityType WITH (NOLOCK)
		ON SuspiciousActivityHist.SuspType = SuspiciousActivityType.[Code]
		LEFT OUTER JOIN
		ActivityType WITH (NOLOCK)
		ON ActivityType.Type = SuspiciousActivityHist.Activity
		LEFT OUTER JOIN
		RegReport WITH (NOLOCK)
		ON Customer.[Id] = RegReport.Cust
	WHERE 
		SuspiciousActivityHist.ClosedTime >=@StartDate and SuspiciousActivityHist.ClosedTime <=@EndDate --Check for Cases closed between the StartDate and EndDate
	ORDER BY 
		CustomerId 

END
ELSE
BEGIN

	CREATE TABLE #TT (StartDate DATETIME,
			  ActionClosingCase VARCHAR(20),
			  CaseNo INT,
			  SSI VARCHAR(20),
			  Branch VARCHAR(11), 
			  RelationshipManager VARCHAR(40),
			  AccountOfficer VARCHAR(40), 
			  DateOpened DATETIME,
			  SECCode VARCHAR(10),
			  Reason VARCHAR(40),
			  AcctStatus VARCHAR(11), 
			  Analyst VARCHAR(11), 
			  RptFiled BIT,
			  Filed DATETIME,
			  DateCaseClosed DATETIME,
			  CustomerName VARCHAR(40), 
			  Onprobation VARCHAR(5),			
			  CustomerId VARCHAR(35), 
			  PepFep VARCHAR(5),
			  AccountId VARCHAR(35)
			)

	SET @BranchSelection = LTRIM(RTRIM(@BranchSelection))+ ','
	SET @Pos = CHARINDEX(',', @BranchSelection, 1)

	-- Logic for sorting the values based upon the Branches entered by the User

	IF REPLACE(@BranchSelection, ',', '') <> ''
	BEGIN
 		WHILE @Pos > 0
		BEGIN
	 
			SET @IndvBranch = LTRIM(RTRIM(LEFT(@BranchSelection, @Pos - 1)))
			IF @IndvBranch <> ''
			BEGIN

				INSERT INTO #TT
				SELECT
					SuspiciousActivity.StartDate StartDate,	--Date Case Started
					dbo.GetReviewState(SuspiciousActivity.ReviewState) ActionClosingCase,	--Action Closing Case
					SuspiciousActivity.RecNo CaseNo,	--Case #
					Case when @showNPIData=1 and Customer.TIN is not null and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20) 
	                     when @showNPIData=1 then Customer.TIN
	                     when LEN(Customer.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20))-4)+ RIGHT(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20),4) 
	                     when LEN(Customer.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(Customer.TIN)-4)+ RIGHT(Customer.TIN,4) 
	                     when(Customer.TIN is not null) and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20) 
	                     else Customer.TIN end SSI,
					Account.Branch,	--Branch Name
					Customer.USPerson RelationshipManager,	--Relationship Manager
					Account.AccountOfficer AccountOfficer,	--Acct Officer
					Account.OpenDate DateOpened,	--Date Acct Opened
					Customer.SecCode SECCode,	--SEC Code : Occupation/Business Type
					SuspiciousActivityType.[Name] Reason,	--Reason for Suspicion
					CASE Account.Closed
						WHEN	1 	THEN	'Closed'
						ELSE	'Open'	
					END	AcctStatus,	--Account Status
					SuspiciousActivity.ReviewOper Analyst,	--Analyst
					RegReport.RptFiled, -- if Report is filed or not
					RegReport.Filed, -- Report Filed Date
					SuspiciousActivity.ClosedTime DateCaseClosed,	--Date Case Closed
					Customer.[Name] CustomerName,	--Customer Name
					CASE WHEN Customer.Onprobation = 1 THEN 'Yes' ELSE 'No' END Onprobation,	-- On Probation
					SuspiciousActivity.Cust CustomerId,	--Customer #
					CASE WHEN Customer.PEP=1 AND Customer.FEP = 1 THEN 'Both' WHEN Customer.FEP=1 THEN 'FEP' WHEN Customer.PEP=1 THEN 'PEP' ELSE 'None' END PepFep,		
					Account.Id AccountId	-- Account Id
				FROM
					SuspiciousActivity WITH (NOLOCK)
					LEFT OUTER JOIN
					Customer WITH (NOLOCK)
					ON SuspiciousActivity.Cust = Customer.[Id]
					LEFT OUTER JOIN
					AccountOwner WITH (NOLOCK)
					ON Customer.[Id] = AccountOwner.Cust
					LEFT OUTER JOIN
					Account WITH (NOLOCK)
					ON AccountOwner.Account = Account.[Id]
					LEFT OUTER JOIN
					Country WITH (NOLOCK)
					ON Customer.Country = Country.Code
					LEFT OUTER JOIN
					SuspiciousActivityType WITH (NOLOCK)
					ON SuspiciousActivity.SuspType = SuspiciousActivityType.[Code]
					LEFT OUTER JOIN
					ActivityType WITH (NOLOCK)
					ON ActivityType.Type = SuspiciousActivity.Activity
					LEFT OUTER JOIN
					RegReport WITH (NOLOCK)
					ON Customer.[Id] = RegReport.Cust
				WHERE 
					ClosedTime >=@StartDate and ClosedTime <=@EndDate --Check for Cases closed between the StartDate and EndDate
					AND Account.Branch = @IndvBranch
				
				UNION
				
				SELECT
					SuspiciousActivityHist.StartDate StartDate,	--Date Case Started
					dbo.GetReviewState(SuspiciousActivityHist.ReviewState) ActionClosingCase,	--Action Closing Case
					SuspiciousActivityHist.RecNo CaseNo,	--Case #
					Case when @showNPIData=1 and Customer.TIN is not null and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20) 
	                     when @showNPIData=1 then Customer.TIN
	                     when LEN(Customer.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20))-4)+ RIGHT(Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20),4) 
	                     when LEN(Customer.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(Customer.TIN)-4)+ RIGHT(Customer.TIN,4) 
	                     when(Customer.TIN is not null) and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(Customer.TIN,@librarycontext),20) 
	                 else Customer.TIN end SSI,
					Account.Branch,	--Branch Name
					Customer.USPerson RelationshipManager,	--Relationship Manager
					Account.AccountOfficer AccountOfficer,	--Acct Officer
					Account.OpenDate DateOpened,	--Date Acct Opened
					Customer.SecCode SECCode,	--SEC Code : Occupation/Business Type
					SuspiciousActivityType.[Name] Reason,	--Reason for Suspicion
					CASE Account.Closed
						WHEN	1 	THEN	'Closed'
						ELSE	'Open'	
					END	AcctStatus,	--Account Status
					SuspiciousActivityHist.ReviewOper Analyst,	--Analyst
					RegReport.RptFiled, -- if Report is filed or not
					RegReport.Filed, -- Report Filed Date
					SuspiciousActivityHist.ClosedTime DateCaseClosed,	--Date Case Closed
					Customer.[Name] CustomerName,	--Customer Name
					CASE WHEN Customer.Onprobation = 1 THEN 'Yes' ELSE 'No' END Onprobation,	-- On Probation
					SuspiciousActivityHist.Cust CustomerId,	--Customer #
					CASE WHEN Customer.PEP=1 AND Customer.FEP = 1 THEN 'Both' WHEN Customer.FEP=1 THEN 'FEP' WHEN Customer.PEP=1 THEN 'PEP' ELSE 'None' END PepFep,		
					Account.Id AccountId	-- Account Id
				FROM
					SuspiciousActivityHist WITH (NOLOCK)
					LEFT OUTER JOIN
					Customer WITH (NOLOCK)
					ON SuspiciousActivityHist.Cust = Customer.[Id]
					LEFT OUTER JOIN
					AccountOwner WITH (NOLOCK)
					ON Customer.[Id] = AccountOwner.Cust
					LEFT OUTER JOIN
					Account WITH (NOLOCK)
					ON AccountOwner.Account = Account.[Id]
					LEFT OUTER JOIN
					Country WITH (NOLOCK)
					ON Customer.Country = Country.Code
					LEFT OUTER JOIN
					SuspiciousActivityType WITH (NOLOCK)
					ON SuspiciousActivityHist.SuspType = SuspiciousActivityType.[Code]
					LEFT OUTER JOIN
					ActivityType WITH (NOLOCK)
					ON ActivityType.Type = SuspiciousActivityHist.Activity
					LEFT OUTER JOIN
					RegReport WITH (NOLOCK)
					ON Customer.[Id] = RegReport.Cust
				WHERE 
					SuspiciousActivityHist.ClosedTime >=@StartDate and SuspiciousActivityHist.ClosedTime <=@EndDate --Check for Cases closed between the StartDate and EndDate
					AND Account.Branch = @IndvBranch
				ORDER BY 
					CustomerId 

			END
			SET @BranchSelection = RIGHT(@BranchSelection, LEN(@BranchSelection) - @Pos)
			SET @Pos = CHARINDEX(',', @BranchSelection, 1)
	 
		END
	END	
	

	SELECT * FROM #TT order by Branch, CustomerId  
	
	DROP TABLE #TT
	END

END
GO




IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRY_SAArchCaseReport_Main]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CRY_SAArchCaseReport_Main]
GO

CREATE Procedure [dbo].[CRY_SAArchCaseReport_Main] (@caseNo SeqNo1, @showNPIData int)
 With Encryption As 
	 
	 begin     
      
	  declare @librarycontext varchar(max)
declare @createfpeobject varchar(max)
declare @istinencrypted int
set @istinencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','tin')
declare @ispassportnoencrypted int
set @ispassportnoencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','PassportNo')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();


     Select sa.RecNo, sa.Cust, sa.Account saAccount, sa.BookDate, sa.ProfileNo,
    sa.ExpMaxInActAmt, sa.ExpMaxOutActAmt, sa.ExpAvgInActCnt, 
    sa.ExpAvgOutActCnt, sa.InAmtTolPerc, sa.OutAmtTolPerc,
    sa.OutAmountVarianceThreshold,
    sa.InAmountVarianceThreshold, sa.OutCountVarianceThreshold,
    sa.InCountVarianceThreshold, 
    sa.InCntTolPerc, sa.OutCntTolPerc, sa.ActInActAmt, 
    sa.ActOutActAmt, sa.ActInActCnt, sa.ActOutActCnt, sa.Descr,
    sa.CountriesOutOfProf, sa.ActCurrReportAmt, sa.CurrReportAmt,
    sa.cubeName, sa.PeerGroup,

    c.Name, c.Address, 
    Case when @showNPIData=1 and c.TIN is not null and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(c.TIN,@librarycontext) 
	     when @showNPIData=1 then c.TIN
	     when LEN(c.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(c.TIN,@librarycontext))-4)+RIGHT(pcdb.dbo.FPEAccessEx(c.TIN,@librarycontext),4) 
	     when LEN(c.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(c.TIN)-4)+ RIGHT(c.TIN,4) 
	     when(c.TIN is not null) and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(c.TIN,@librarycontext) 
	     else c.TIN end TIN,	--RUT/EIN/SSI
	c.City, c.SecCode, c.State,
    c.DBA, c.Zip,
	Case when @showNPIData=1 and c.PassportNo is not null and @ispassportnoencrypted>0 then pcdb.dbo.FPEAccessEx(c.PassportNo,@librarycontext) 
	     when @showNPIData=1 then c.PassportNo
	     when LEN(c.PassportNo)> 4 and @ispassportnoencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(c.PassportNo,@librarycontext))-4)+RIGHT(pcdb.dbo.FPEAccessEx(c.PassportNo,@librarycontext),4) 
	     when LEN(c.PassportNo)> 4 and @ispassportnoencrypted=0 then REPLICATE('*',LEN(c.PassportNo)-4)+ RIGHT(c.PassportNo,4) 
	     when(c.PassportNo is not null) and @ispassportnoencrypted>0 then pcdb.dbo.FPEAccessEx(c.PassportNo,@librarycontext) 
	     else c.PassportNo end PassportNo,
	c.Country, c.DOB, c.Telephone,
    c.IdList, c.Email, c.TypeOfBusiness, c.SourceOfFunds,
    c.ExemptionStatus, c.AccountOfficer, c.AcctOffEmail,
    c.AcctOffTel, c.CompOfficer, c.CompOffEmail, c.CompOffTel,
    c.CreateDate as cCreateDate, c.CreateOper as cCreateOper, 
      c.LastReview as cLastReview, c.LastReviewOper as cLastReviewOper,
    c.LastModify as cLastModify, c.LastOper as cLastOper, 
      c.Notes as cNotes, c.RiskClass,

    p.Account, p.Activity, p.StartDate,
    p.EndDate, p.RecurType, p.RecurValue, 
      p.LastReview as pLastReview, p.LastReviewOper as pLastReviewOper, 
      p.CreateDate as pCreateDate, p.CreateOper as pCreateOper, 
      p.LastModify as pLastModify, p.LastOper as pLastOper, 
      p.Notes as pNotes, sa.wlCode, sa.wldesc, sa.SuspType,

    pp.ProfileStartDate, pp.ProfileEndDate, pp.ProfilePeriod, pp.DynamicPeriod,
    pp.RecurType as ppRecurType, 
      pp.CreateDate as ppCreateDate, pp.CreateOper as ppCreateOper, 
      pp.LastModify as ppLastModify, pp.LastOper as ppLastOper, 
      pp.Description as ppDescription, pp.Dimension,

      wl.Title, wl.[Desc] as wlDefaultDesc, wl.IsPreEod, 
      wl.SuspType as wlSuspType, wl.SPName, wl.Schedule, sa.suspType, sa.CaseScore
      From SuspiciousActivityHist sa WITH (NOLOCK)
            LEFT OUTER JOIN Profile p WITH (NOLOCK) on sa.ProfileNo = p.RecNo
            LEFT OUTER JOIN Customer c WITH (NOLOCK) on sa.Cust = c.ID
            LEFT OUTER JOIN WatchList wl WITH (NOLOCK) ON sa.WLCode = wl.WlCode
            LEFT OUTER JOIN PeerModel pp WITH (NOLOCK) ON sa.cubeName = pp.[id]
    Where sa.RecNo = @caseNo


	End
GO




IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRY_SACaseReport_Main]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CRY_SACaseReport_Main]
GO

CREATE Procedure [dbo].[CRY_SACaseReport_Main] ( @caseNo SeqNo1, @showNPIData int)
With Encryption As

    ;
	  
	begin
	declare @librarycontext varchar(max)
declare @createfpeobject varchar(max)
declare @istinencrypted int
set @istinencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','tin')
declare @ispassportnoencrypted int
set @ispassportnoencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','PassportNo')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();
	WITH customers As  (
		SELECT @caseNo  As CaseNo, cust.*, 'Primary Suspect' As iOrder  From SuspiciousActivity sa WITH (NOLOCK) LEFT OUTER JOIN Customer cust WITH (NOLOCK) on sa.Cust = cust.ID  where sa.RecNo = @caseNo
	)

		
    Select sa.RecNo, C.Id AS Cust, sa.Account saAccount, sa.BookDate, sa.ProfileNo,
    sa.ExpMaxInActAmt, sa.ExpMaxOutActAmt, sa.ExpAvgInActCnt, 
    sa.ExpAvgOutActCnt, sa.InAmtTolPerc, sa.OutAmtTolPerc,
    sa.InCntTolPerc, sa.OutCntTolPerc, sa.OutAmountVarianceThreshold,
    sa.InAmountVarianceThreshold, sa.OutCountVarianceThreshold,
    sa.InCountVarianceThreshold, sa.ActInActAmt, 
    sa.ActOutActAmt, sa.ActInActCnt, sa.ActOutActCnt, sa.Descr,
    sa.CountriesOutOfProf, sa.ActCurrReportAmt, sa.CurrReportAmt,
    sa.cubeName, sa.PeerGroup,

    c.Name, c.Address, 
	Case when @showNPIData=1 and c.TIN is not null and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(c.TIN,@librarycontext) 
	     when @showNPIData=1 then c.TIN
	     when LEN(c.TIN)> 4 and @istinencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(c.TIN,@librarycontext))-4)+ RIGHT(pcdb.dbo.FPEAccessEx(c.TIN,@librarycontext),4) 
	     when LEN(c.TIN)> 4 and @istinencrypted=0 then REPLICATE('*',LEN(c.TIN)-4)+ RIGHT(c.TIN,4) 
	     when(c.TIN is not null) and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(c.TIN,@librarycontext) 
	     else c.TIN end  TIN,	--RUT/EIN/SSI
	c.City, c.SecCode, c.State,
	c.DBA, c.Zip,
	Case when @showNPIData=1 and c.PassportNo is not null and @ispassportnoencrypted>0 then pcdb.dbo.FPEAccessEx(c.PassportNo,@librarycontext) 
	     when @showNPIData=1 then c.PassportNo
	     when LEN(c.PassportNo)> 4 and @ispassportnoencrypted>0 then REPLICATE('*',LEN(pcdb.dbo.FPEAccessEx(c.PassportNo,@librarycontext))-4)+ RIGHT(pcdb.dbo.FPEAccessEx(c.PassportNo,@librarycontext),4) 
	     when LEN(c.PassportNo)> 4 and @ispassportnoencrypted=0 then REPLICATE('*',LEN(c.PassportNo)-4)+ RIGHT(c.PassportNo,4) 
	     when(c.PassportNo is not null) and @ispassportnoencrypted>0 then pcdb.dbo.FPEAccessEx(c.PassportNo,@librarycontext) 
	     else c.PassportNo end  PassportNo,
	  c.Country, c.DOB, c.Telephone,
    c.IdList, c.Email, c.TypeOfBusiness, c.SourceOfFunds,
    c.ExemptionStatus, c.AccountOfficer, c.AcctOffEmail,
    c.AcctOffTel, c.CompOfficer, c.CompOffEmail, c.CompOffTel,
    c.CreateDate as cCreateDate, c.CreateOper as cCreateOper, 
      c.LastReview as cLastReview, c.LastReviewOper as cLastReviewOper,
    c.LastModify as cLastModify, c.LastOper as cLastOper, 
      c.Notes as cNotes, c.RiskClass,

    p.Account, p.Activity, p.StartDate,
    p.EndDate, p.RecurType, p.RecurValue, 
      p.LastReview as pLastReview, p.LastReviewOper as pLastReviewOper, 
      p.CreateDate as pCreateDate, p.CreateOper as pCreateOper, 
      p.LastModify as pLastModify, p.LastOper as pLastOper, 
      p.Notes as pNotes, sa.wlCode, sa.wldesc, sa.SuspType,
      
    pp.ProfileStartDate, pp.ProfileEndDate, pp.ProfilePeriod, pp.DynamicPeriod,
    pp.RecurType as ppRecurType, 
      pp.CreateDate as ppCreateDate, pp.CreateOper as ppCreateOper, 
      pp.LastModify as ppLastModify, pp.LastOper as ppLastOper, 
      pp.Description as ppDescription, pp.Dimension,

      wl.Title, wl.[Desc] as wlDefaultDesc, wl.IsPreEod, 
      wl.SuspType as wlSuspType, wl.SPName, wl.Schedule, sa.suspType, sa.CaseScore, iOrder
      
      From Customers c 
			JOIN SuspiciousActivity sa WITH (NOLOCK) on sa.RecNo = c.CaseNo
            LEFT OUTER JOIN Profile p WITH (NOLOCK) on sa.ProfileNo = p.RecNo
            LEFT OUTER JOIN WatchList wl WITH (NOLOCK) ON sa.WLCode = wl.WlCode
            LEFT OUTER JOIN PeerModel pp WITH (NOLOCK) ON sa.cubeName = pp.[id]            
            Where c.CaseNo = @caseNo 
      Order by c.iOrder, c.ID;

	  End
GO


if exists (select * from sysobjects 
		where id = object_id(N'[dbo].[BSA_GetCasesForExport]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		Drop Procedure dbo.BSA_GetCasesForExport
	Go


	CREATE Procedure dbo.BSA_GetCasesForExport(@CaseCloseBeginDate  datetime , @CaseCloseEndDate datetime,@MaxRecordCount int,@pageno int,@LastRecNo int, @CurrencySymbol varchar(1), @FeedbackOperatorToExclude varchar(10))
	WITH ENCRYPTION
	As
		DECLARE @FirstRow INT
		DECLARE @LastRow INT
		
		DECLARE @PrevRecNo INT
		DECLARE @InvParam INT
		DECLARE @InvState INT
		DECLARE @FunctionErrors INT
		DECLARE @IsDelegationOn bit, @DelegationSetting varchar(255)
		DECLARE @DelegateCaseMgt bit, @DelegateCTRFiling bit, @DelegateSARFiling bit

		   declare @librarycontext varchar(max)
declare @createfpeobject varchar(max)
declare @istinencrypted int
set @istinencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','tin')
declare @islicensenoencrypted int
set @islicensenoencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','LicenseNo')
declare @ispassportnoencrypted int
set @ispassportnoencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','PassportNo')
declare @isidnumberencrypted int
set @isidnumberencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('PartyID','IDNumber')
select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();
 


		SET @InvParam=250010	--Generic Error Code for Invalid Input Parameters
		SET @InvState=250030	--Error code for Invalid State

		IF (@LastRecNo<0) or (@pageno<0) or (@MaxRecordCount<=0) -- If input parameters are  less than zero, returning generic error code
				return @InvParam 

		If @CurrencySymbol is null
			SET @CurrencySymbol = '$'
		
		--Get Delegation options
		SELECT @IsDelegationOn = (select isnull(o.Enabled,0) from OptionTbl o where Code='DlgtOptions')
		
		IF @IsDelegationOn = 0
			SELECT @DelegateCaseMgt = 0, @DelegateSARFiling = 0, @DelegateCTRFiling = 0
		ELSE
			BEGIN
				SELECT @DelegationSetting = dbo.BSA_GetCustomSetting('BSADlgtOpt')
				
				SELECT 
					@DelegateCaseMgt = CASE WHEN @DelegationSetting = 'CASE' THEN 1 ELSE 0 END,
					@DelegateSARFiling = CASE WHEN CHARINDEX('SAR',@DelegationSetting)>0 THEN 1 ELSE 0 END,
					@DelegateCTRFiling = CASE WHEN CHARINDEX('CTR',@DelegationSetting)>0 THEN 1 ELSE 0 END
			END
		
		
		DECLARE @need2ndApp BIT
		SELECT @need2ndApp = Enabled FROM OptionTbl WHERE Code = 'SecondApp' 

			
			
		Create table #TempExport
		(
		ROW INT Identity(1,1),
		RecordType varchar(3),
		ID 	int,
		[Type] 	varchar(11),
		PartyID 	varchar(35),
		PartyRiskClass 	varchar(11),
		PartyName 	varchar(40),
		PartyTIN 	varchar(20),
		[Description] 	varchar(3000),
		Score 	int,
		CreateTime 	datetime,
		OwnerBranchID 	varchar(11),
		OwnerBranchName 	varchar(35),
		OwnerDepartmentID 	varchar(11),
		OwnerDepartmentName 	varchar(35),
		OwnerOperatorID 	varchar(11),
		OwnerOperatorName	varchar(35),
		AccountID 	varchar(35),
		StartDate 	datetime,
		EndDate	datetime,
		ReviewState	smallint,
		ReviewTime	datetime,
		ReviewOperator	varchar(11),
		LastState	varchar(11),
		LastModifiedTime	datetime,
		LastOperator	varchar(11),
		Approved	bit,
		ApproveTime	datetime,
		ApproveOperator	varchar(11),
		ActivityInCount	int,
		ActivityOutCount	int,
		ActivityInAmount	money,
		ActivityOutAmount	money,
		ExpectedActivityInCount	int,
		ExpectedActivityOutCount	int,
		ExpectedActivityInAmount	money,
		ExpectedActivityOutAmount	money,
		InCountVariance	int,
		OutCountVariance	int,
		InAmountVariance	money,
		OutAmountVariance	money,
		RecurrenceType	smallint,
		RecurrenceValue	int,
		CubeName	varchar(11),
		PeerGroupName	varchar(1000),
		CountriesExpected	varchar(500),
		CountriesNotExpected	varchar(500),
		RuleID	varchar(11),
		RuleName	varchar(255),
		RuleDescription	varchar(2000),
		PurgeAllowed	bit,
		ClosedTime	datetime,
		GovernmentReportsFiled	bit,
		PastCasesIdList	varchar(3000),
		Notes	varchar(3000),
--		ActivityIdList			varchar(3000),
		BranchAddress			varchar(255),
		BranchCity				varchar(40),
		BranchState				varchar(40),
		BranchZip				varchar(15),
		CustomerDBA				varchar(40),
		CustomerAddress			varchar(120),
		CustomerCity			varchar(40),
		CustomerState			varchar(40),
		CustomerZip				varchar(15),
		CustomerCountry			varchar(100),
		CustomerTelephone		varchar(20),
		CustomerWorkphone		varchar(20),
		CustomerTypeOfBusiness	varchar(80),
		CustomerDOB				datetime,
		CustomerLicenseNo		varchar(35),
		CustomerPassportNo		varchar(20),
		CustomerAlienId			varchar(25),
		RelatedCustomerNames	varchar(3000),
		RelationshipDescription	varchar(40),
		PrimaryRelation			bit,
		AccountStatus			varchar(35),
		LicenseState			varchar(40),
		PassportIssuingCountry	varchar(100),
		AlienIdIssuingCountry	varchar(100),
		IsArchived bit
		)
	 

	INSERT INTO #TempExport

		SELECT                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
			'CAS',  --RecordType
			sa.RecNo ,
			sa.SuspType,
			sa.Cust,
			c.RiskClass ,
			c.Name ,
			--c.TIN ,
			case when c.TIN is not null and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(c.TIN,@librarycontext),20) else c.TIN end as TIN,
			'Description' = dbo.BSA_GetCaseDetails(sa.RecNo,sa.SuspType,@CurrencySymbol),
			sa.CaseScore ,
			sa.CreateTime  ,
			sa.OwnerBranch ,
			b.Name  ,
			sa.OwnerDept ,
			d.Name ,
			sa.OwnerOper ,
			o.Name ,
			sa.Account ,
			sa.StartDate ,
			sa.EndDate ,
			sa.ReviewState,
			sa.ReviewTime ,
			sa.ReviewOper ,
			sa.LastState ,
			sa.LastModify,
			sa.LastOper ,
			sa.App ,
			sa.AppTime ,
			sa.AppOper ,
			sa.ActInActCnt,
			sa.ActOutActCnt ,
			sa.ActInActAmt ,
			sa.ActOutActAmt,
			sa.ExpAvgInActCnt ,
			sa.ExpAvgOutActCnt,
			sa.ExpMaxInActAmt ,
			sa.ExpMaxOutActAmt ,
			CASE
				WHEN UPPER(sa.SuspType) = 'EXCAMTORCNT' THEN
					sa.ActInActCnt - (sa.ExpAvgInActCnt + 
										round((sa.ExpAvgInActCnt * sa.InCntTolPerc)/100,0) + 
										sa.InCountVarianceThreshold)
				WHEN UPPER(sa.SuspType) = 'PEERPROFILE' THEN
					sa.ActInActCnt -  (sa.ExpAvgInActCnt + InCountTolerance)
				ELSE NULL
			END AS InCountVariance,
			CASE
				WHEN UPPER(sa.SuspType) = 'EXCAMTORCNT' THEN
					sa.ActOutActCnt - (sa.ExpAvgOutActCnt + 
										round((sa.ExpAvgOutActCnt * sa.OutCntTolPerc)/100,0) + 
										sa.OutCountVarianceThreshold)
				WHEN UPPER(sa.SuspType) = 'PEERPROFILE' THEN
					sa.ActOutActCnt -  (sa.ExpAvgOutActCnt + OutCountTolerance)
				ELSE NULL
			END	AS OutCountVariance,
			CASE
				WHEN UPPER(sa.SuspType) = 'EXCAMTORCNT' THEN
					sa.ActInActAmt - 
					(sa.ExpMaxInActAmt + (round((sa.ExpMaxInActAmt*sa.InAmtTolPerc)/100,2)) + 
										(round(InAmountVarianceThreshold, 2)) + 
										0)
				WHEN UPPER(sa.SuspType) = 'PEERPROFILE' THEN
					sa.ActInActAmt - (sa.ExpMaxInActAmt + sa.InAmountTolerance)
				ELSE NULL
			END AS InAmountVariance,
			CASE
				WHEN UPPER(sa.SuspType) = 'EXCAMTORCNT' THEN
					sa.ActOutActAmt - 
					(sa.ExpMaxOutActAmt + (round((sa.ExpMaxOutActAmt*sa.OutAmtTolPerc)/100,2)) + 
										(round(OutAmountVarianceThreshold, 2)) + 
										0)
				WHEN UPPER(sa.SuspType) = 'PEERPROFILE' THEN
					sa.ActOutActAmt - (sa.ExpMaxOutActAmt + sa.OutAmountTolerance)
				ELSE NULL
			END AS OutAmountVariance,
			sa.RecurType ,
			sa.RecurValue,
			sa.cubeName  ,
			sa.PeerGroup ,
			sa.CountriesInProf ,
			sa.CountriesOutOfProf ,
			sa.WLCode ,
			wl.Title ,
			wl.[Desc],
			sa.Purge ,
			sa.ClosedTime , 
			(Select count(RepNo) from dbo.RegReport  Where SARecNo = sa.RecNo and  RptFiled = 1), 
			CASE WHEN sa.cust is null THEN NULL ELSE DBO.BSA_GetPastCustClosedCases(sa.cust,sa.ClosedTime) END,  -- Getting Past IDs for that customer  
			sa.Descr,
--			dbo.BSA_GetActivityIdList(sa.recNo),   --ActivityIdList, call sproc. like GetPastCustClosedCases - create new one. activity table and activityhist.
			b.Address,
			b.City,
			b.State,
			b.Zip,
			c.DBA,
			c.Address,
			c.City,
			c.State,
			c.Zip,
			ct.Name,
			c.Telephone,
			adr.Telephone,
			c.TypeOfBusiness,
			dbo.BSA_ConvertIntToShortDate(c.DOB),
			--c.LicenseNo,
			case when c.LicenseNo is not null and @islicensenoencrypted>0 then Left(pcdb.dbo.FPEAccessEx(c.LicenseNo,@librarycontext),35) else c.LicenseNo end as LicenseNo,

			CASE	-- if in customertable use this, otherwise if in partyid, override with this from partyid table.
				WHEN c.PassportNo <> ''  and @ispassportnoencrypted>0 THEN Left(pcdb.dbo.FPEAccessEx(c.PassportNo,@librarycontext),20)
				WHEN c.PassportNo <> '' THEN c.PassportNo
				WHEN pp.IDNumber <> '' and @isidnumberencrypted>0 THEN Left(pcdb.dbo.FPEAccessEx(pp.IDNumber,@librarycontext),25)
				WHEN pp.IDNumber <> '' THEN pp.IDNumber
				ELSE NULL
			END,
			pa.IDNumber,   --CustomerAlienId
			dbo.BSA_GetRelatedPartiesNameList(sa.cust), --RelatedCustomerNames,
			rt.Name, --RelationshipDescription,
			rt.PrimaryRelation, --PrimaryRelation,
			a.AcStatus,  
			c.LicenseState,
			cp.Name,  --passportIssuingCountry
			ca.Name,   --AlienIdIssuingCountry 
			1	--IsArchived
			
		FROM
			pbsa.dbo.SuspiciousActivityHist sa (nolock)
		LEFT OUTER JOIN
			pbsa.dbo.Customer c(nolock)
			on
			sa.Cust = c.Id
		LEFT OUTER JOIN
			pbsa.dbo.Country ct(nolock)  -- kk added
			on 
			c.Country = ct.Code
		LEFT OUTER JOIN
			pbsa.dbo.PartyID pa(nolock)  --kk added 
			on
			sa.Cust = pa.CustomerId
			and pa.IDType = 'AlienID'
		LEFT OUTER JOIN
			pbsa.dbo.Country ca(nolock)
			on
			pa.IssueCountry=ca.Code 
		LEFT OUTER JOIN
			pbsa.dbo.PartyID pp(nolock)  --kk added 
			on
			sa.Cust = pp.CustomerId
			and pp.IDType = 'PassportID'
		LEFT OUTER JOIN
			pbsa.dbo.Country cp(nolock)
			on
			pp.IssueCountry = cp.Code 
		LEFT OUTER JOIN
			pbsa.dbo.PartyAddress adr (nolock)
			ON
			sa.Cust = adr.CustomerId
			AND
			adr.AddressType = 'EmplyAddrs'
		LEFT OUTER JOIN
			PSEC.dbo.Branch b(nolock)
			on
			sa.OwnerBranch = b.Code
		LEFT OUTER JOIN
			PSEC.dbo.Dept d(nolock)
			on
			sa.OwnerDept = d.code
		LEFT OUTER JOIN
			PSEC.dbo.Operator o(nolock)
			on
			sa.OwnerOper = o.Code
		LEFT OUTER JOIN
			pbsa.dbo.WatchList wl(nolock)
			on
			sa.WLCode = wl.WLCode
		LEFT OUTER JOIN
			pbsa.dbo.Account a(nolock)   -- kk added
			on
			sa.Account = a.Id
		LEFT OUTER JOIN
			pbsa.dbo.AccountOwner ao(nolock)
			on
			sa.Account = ao.Account 
			and
			sa.Cust = ao.Cust
		LEFT OUTER JOIN
			pbsa.dbo.RelationshipType rt(nolock) 
			on 
			ao.Relationship=rt.Code
		WHERE  
			ISNULL(sa.LastOper,'NULL')<>ISNULL(@FeedbackOperatorToExclude,'')
			And
			((
			--If nothing is delegated then export all archived cases within the date range
			@DelegateCaseMgt=0 AND @DelegateSARFiling=0 AND @DelegateCTRFiling=0
			AND
			((@CaseCloseBeginDate is NULL and @CaseCloseEndDate is not NULL and ClosedTime <= @CaseCloseEndDate) 
			or 
			(@CaseCloseBeginDate is not NULL and @CaseCloseEndDate is NULL and ClosedTime >= @CaseCloseBeginDate)
			or
			(@CaseCloseBeginDate is NULL and @CaseCloseEndDate is NULL)
			or
			(@CaseCloseBeginDate is not NULL and @CaseCloseEndDate is not NULL and 
				ClosedTime BETWEEN @CaseCloseBeginDate and @CaseCloseEndDate)) 
			)
			OR
			(
			--If SAR filing is delegated then export archived unconfirmed SAR cases within the date range
			--and archived CTR cases
			@DelegateCaseMgt=0 AND @DelegateSARFiling=1
			AND
			(sa.ReviewState<>3 OR (@DelegateCTRFiling=0 AND pbsa.dbo.BSA_IsCaseCTRCase(sa.RecNo) = 1))
			AND
			((@CaseCloseBeginDate is NULL and @CaseCloseEndDate is not NULL and ClosedTime <= @CaseCloseEndDate) 
			or 
			(@CaseCloseBeginDate is not NULL and @CaseCloseEndDate is NULL and ClosedTime >= @CaseCloseBeginDate)
			or
			(@CaseCloseBeginDate is NULL and @CaseCloseEndDate is NULL)
			or
			(@CaseCloseBeginDate is not NULL and @CaseCloseEndDate is not NULL and 
				ClosedTime BETWEEN @CaseCloseBeginDate and @CaseCloseEndDate)) 
			)
			OR 
			(
			--If CTR filing is delegated then export archived waived CTR cases within the date range
			--and archived SAR cases
			@DelegateCaseMgt=0 AND @DelegateCTRFiling=1
			AND
			(sa.ReviewState<>3 OR (@DelegateSARFiling=0 AND pbsa.dbo.BSA_IsCaseCTRCase(sa.RecNo) = 0)) 
			AND
			((@CaseCloseBeginDate is NULL and @CaseCloseEndDate is not NULL and ClosedTime <= @CaseCloseEndDate) 
			or 
			(@CaseCloseBeginDate is not NULL and @CaseCloseEndDate is NULL and ClosedTime >= @CaseCloseBeginDate)
			or
			(@CaseCloseBeginDate is NULL and @CaseCloseEndDate is NULL)
			or
			(@CaseCloseBeginDate is not NULL and @CaseCloseEndDate is not NULL and 
				ClosedTime BETWEEN @CaseCloseBeginDate and @CaseCloseEndDate)) 
			))
				
		UNION ALL
				
		SELECT                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
			'CAS',  --RecordType
			sa.RecNo ,
			sa.SuspType,
			sa.Cust,
			c.RiskClass ,
			c.Name ,
			--c.TIN ,
			case when c.TIN is not null and @istinencrypted>0 then Left(pcdb.dbo.FPEAccessEx(c.TIN,@librarycontext),20) else c.TIN end as TIN,
			'Description' = dbo.BSA_GetCaseDetails(sa.RecNo,sa.SuspType,@CurrencySymbol),
			sa.CaseScore ,
			sa.CreateTime  ,
			sa.OwnerBranch ,
			b.Name  ,
			sa.OwnerDept ,
			d.Name ,
			sa.OwnerOper ,
			o.Name ,
			sa.Account ,
			sa.StartDate ,
			sa.EndDate ,
			sa.ReviewState,
			sa.ReviewTime ,
			sa.ReviewOper ,
			sa.LastState ,
			sa.LastModify,
			sa.LastOper ,
			sa.App ,
			sa.AppTime ,
			sa.AppOper ,
			sa.ActInActCnt,
			sa.ActOutActCnt ,
			sa.ActInActAmt ,
			sa.ActOutActAmt,
			sa.ExpAvgInActCnt ,
			sa.ExpAvgOutActCnt,
			sa.ExpMaxInActAmt ,
			sa.ExpMaxOutActAmt ,
			CASE
				WHEN UPPER(sa.SuspType) = 'EXCAMTORCNT' THEN
					sa.ActInActCnt - (sa.ExpAvgInActCnt + 
										round((sa.ExpAvgInActCnt * sa.InCntTolPerc)/100,0) + 
										sa.InCountVarianceThreshold)
				WHEN UPPER(sa.SuspType) = 'PEERPROFILE' THEN
					sa.ActInActCnt -  (sa.ExpAvgInActCnt + InCountTolerance)
				ELSE NULL
			END AS InCountVariance,
			CASE
				WHEN UPPER(sa.SuspType) = 'EXCAMTORCNT' THEN
					sa.ActOutActCnt - (sa.ExpAvgOutActCnt + 
										round((sa.ExpAvgOutActCnt * sa.OutCntTolPerc)/100,0) + 
										sa.OutCountVarianceThreshold)
				WHEN UPPER(sa.SuspType) = 'PEERPROFILE' THEN
					sa.ActOutActCnt -  (sa.ExpAvgOutActCnt + OutCountTolerance)
				ELSE NULL
			END	AS OutCountVariance,
			CASE
				WHEN UPPER(sa.SuspType) = 'EXCAMTORCNT' THEN
					sa.ActInActAmt - 
					(sa.ExpMaxInActAmt + (round((sa.ExpMaxInActAmt*sa.InAmtTolPerc)/100,2)) + 
										(round(InAmountVarianceThreshold, 2)) + 
										0)
				WHEN UPPER(sa.SuspType) = 'PEERPROFILE' THEN
					sa.ActInActAmt - (sa.ExpMaxInActAmt + sa.InAmountTolerance)
				ELSE NULL
			END AS InAmountVariance,
			CASE
				WHEN UPPER(sa.SuspType) = 'EXCAMTORCNT' THEN
					sa.ActOutActAmt - 
					(sa.ExpMaxOutActAmt + (round((sa.ExpMaxOutActAmt*sa.OutAmtTolPerc)/100,2)) + 
										(round(OutAmountVarianceThreshold, 2)) + 
										0)
				WHEN UPPER(sa.SuspType) = 'PEERPROFILE' THEN
					sa.ActOutActAmt - (sa.ExpMaxOutActAmt + sa.OutAmountTolerance)
				ELSE NULL
			END AS OutAmountVariance,
			sa.RecurType ,
			sa.RecurValue,
			sa.cubeName  ,
			sa.PeerGroup ,
			sa.CountriesInProf ,
			sa.CountriesOutOfProf ,
			sa.WLCode ,
			wl.Title ,
			wl.[Desc],
			sa.Purge ,
			sa.ClosedTime , 
			(Select count(RepNo) from dbo.RegReport  Where SARecNo = sa.RecNo and  RptFiled = 1), 
			CASE WHEN sa.cust is null THEN NULL ELSE DBO.BSA_GetPastCustClosedCases(sa.cust,sa.ClosedTime) END,  -- Getting Past IDs for that customer   
			sa.Descr,
			--dbo.BSA_GetActivityIdList(sa.recNo),   --ActivityIdList, call sproc. like GetPastCustClosedCases - create new one. activity table and activityhist.
			b.Address,
			b.City,
			b.State,
			b.Zip,
			c.DBA,
			c.Address,
			c.City,
			c.State,
			c.Zip,
			ct.Name,
			c.Telephone,
			adr.Telephone,
			c.TypeOfBusiness,
			dbo.BSA_ConvertIntToShortDate(c.DOB),
			--c.LicenseNo,
			case when c.LicenseNo is not null and @islicensenoencrypted>0 then Left(pcdb.dbo.FPEAccessEx(c.LicenseNo,@librarycontext),35) else c.LicenseNo end as LicenseNo,

             CASE	-- if in customertable use this, otherwise if in partyid, override with this from partyid table.
				WHEN c.PassportNo <> ''  and @ispassportnoencrypted>0 THEN Left(pcdb.dbo.FPEAccessEx(c.PassportNo,@librarycontext),20)
				WHEN c.PassportNo <> '' THEN c.PassportNo
				WHEN pp.IDNumber <> '' and @isidnumberencrypted>0 THEN Left(pcdb.dbo.FPEAccessEx(pp.IDNumber,@librarycontext),25)
				WHEN pp.IDNumber <> '' THEN pp.IDNumber
				ELSE NULL
			END,
			pa.IDNumber,   --CustomerAlienId
			dbo.BSA_GetRelatedPartiesNameList(sa.cust), --RelatedCustomerNames,
			rt.Name, --RelationshipDescription,
			rt.PrimaryRelation, --PrimaryRelation,
			a.AcStatus,  
			c.LicenseState,
			cp.Name,  --passportIssuingCountry
			ca.Name,   --AlienIdIssuingCountry 
			0	--IsArchived
			
		FROM
			pbsa.dbo.SuspiciousActivity sa (nolock)
		LEFT OUTER JOIN
			pbsa.dbo.Customer c(nolock)
			on
			sa.Cust = c.Id
		LEFT OUTER JOIN
			pbsa.dbo.Country ct(nolock)  -- kk added
			on 
			c.Country = ct.Code
		LEFT OUTER JOIN
			pbsa.dbo.PartyID pa(nolock)  --kk added (alien)
			on
			sa.Cust = pa.CustomerId
			and pa.IDType = 'AlienID'
		LEFT OUTER JOIN
			pbsa.dbo.Country ca(nolock)
			on
			pa.IssueCountry=ca.Code 
		LEFT OUTER JOIN
			pbsa.dbo.PartyID pp(nolock)  --kk added (passport)
			on
			sa.Cust = pp.CustomerId
			and pp.IDType = 'PassportID'
		LEFT OUTER JOIN
			pbsa.dbo.Country cp(nolock)
			on
			pp.IssueCountry = cp.Code 
		LEFT OUTER JOIN
			pbsa.dbo.PartyAddress adr (nolock)
			ON
			sa.Cust = adr.CustomerId
			AND
			adr.AddressType = 'EmplyAddrs'
		LEFT OUTER JOIN
			PSEC.dbo.Branch b(nolock)
			on
			sa.OwnerBranch = b.Code
		LEFT OUTER JOIN
			PSEC.dbo.Dept d(nolock)
			on
			sa.OwnerDept = d.code
		LEFT OUTER JOIN
			PSEC.dbo.Operator o(nolock)
			on
			sa.OwnerOper = o.Code
		LEFT OUTER JOIN
			pbsa.dbo.WatchList wl(nolock)
			on
			sa.WLCode = wl.WLCode
		LEFT OUTER JOIN
			pbsa.dbo.Account a(nolock)   -- kk added
			on
			sa.Account = a.Id
		LEFT OUTER JOIN
			pbsa.dbo.AccountOwner ao(nolock)
			on
			sa.Account = ao.Account 
			and
			sa.Cust = ao.Cust
		LEFT OUTER JOIN
			pbsa.dbo.RelationshipType rt(nolock) 
			on 
			ao.Relationship=rt.Code
		WHERE  
			ISNULL(sa.LastOper,'NULL')<>ISNULL(@FeedbackOperatorToExclude,'')
			AND
			((
			--Pull by CreateTime when Case Delegation is on
			@DelegateCaseMgt = 1
			AND
			sa.ReviewState is NULL 	--Only export "Not Yet Determined" cases for case delegation
			AND
			((@CaseCloseBeginDate is NULL and @CaseCloseEndDate is not NULL and sa.CreateTime <= @CaseCloseEndDate) 
			or 
			(@CaseCloseBeginDate is not NULL and @CaseCloseEndDate is NULL and sa.CreateTime >= @CaseCloseBeginDate)
			or
			(@CaseCloseBeginDate is NULL and @CaseCloseEndDate is NULL)
			or
			(@CaseCloseBeginDate is not NULL and @CaseCloseEndDate is not NULL and 
				sa.CreateTime BETWEEN @CaseCloseBeginDate and @CaseCloseEndDate)) 
			)
			OR
			(
			--Pull by ReviewTime when SAR Filing Delegation is on
			@DelegateSARFiling = 1
			AND
			sa.ReviewState = 3 --Confirmed cases only
			AND
			PBSA.DBO.BSA_IsCaseCTRCase(sa.RecNo) = 0
			AND
			((@CaseCloseBeginDate is NULL and @CaseCloseEndDate is not NULL and CASE WHEN @need2ndApp = 1 THEN sa.AppTime ELSE sa.ReviewTime END <= @CaseCloseEndDate) 
			or 
			(@CaseCloseBeginDate is not NULL and @CaseCloseEndDate is NULL and CASE WHEN @need2ndApp = 1 THEN sa.AppTime ELSE sa.ReviewTime END >= @CaseCloseBeginDate)
			or
			(@CaseCloseBeginDate is NULL and @CaseCloseEndDate is NULL)
			or
			(@CaseCloseBeginDate is not NULL and @CaseCloseEndDate is not NULL and 
				CASE WHEN @need2ndApp = 1 THEN sa.AppTime ELSE sa.ReviewTime END BETWEEN @CaseCloseBeginDate and @CaseCloseEndDate)) 
			)
			OR
			(
			--Pull by ReviewTime when CTR Filing Delegation is on
			@DelegateCTRFiling = 1
			AND
			sa.ReviewState = 3 --Confirmed cases only
			AND
			PBSA.DBO.BSA_IsCaseCTRCase(sa.RecNo) = 1
			AND
			((@CaseCloseBeginDate is NULL and @CaseCloseEndDate is not NULL and CASE WHEN @need2ndApp = 1 THEN sa.AppTime ELSE sa.ReviewTime END <= @CaseCloseEndDate) 
			or 
			(@CaseCloseBeginDate is not NULL and @CaseCloseEndDate is NULL and CASE WHEN @need2ndApp = 1 THEN sa.AppTime ELSE sa.ReviewTime END >= @CaseCloseBeginDate)
			or
			(@CaseCloseBeginDate is NULL and @CaseCloseEndDate is NULL)
			or
			(@CaseCloseBeginDate is not NULL and @CaseCloseEndDate is not NULL and 
				CASE WHEN @need2ndApp = 1 THEN sa.AppTime ELSE sa.ReviewTime END BETWEEN @CaseCloseBeginDate and @CaseCloseEndDate)) 
			))
			
		ORDER BY sa.Recno


		IF(isnull(@LastRecNo,0)=0 and isnull(@pageno,0)=0)                -- Initial Call to the Stored Proc
		BEGIN	
			
			SET @FirstRow=1
			SET @LastRow=@MaxRecordCount
			SET @PrevRecNo = @LastRecNo
			SELECT  
						RecordType,ID,[Type],PartyID,
						PartyRiskClass,PartyName,PartyTIN ,
						[Description],Score,CreateTime ,
						OwnerBranchID,OwnerBranchName,OwnerDepartmentID ,
						OwnerDepartmentName,OwnerOperatorID,OwnerOperatorName,
						AccountID,StartDate,EndDate,ReviewState,
						ReviewTime,ReviewOperator,LastState,
						LastModifiedTime,LastOperator,Approved,
						ApproveTime,ApproveOperator,ActivityInCount,
						ActivityOutCount,ActivityInAmount,ActivityOutAmount,
						ExpectedActivityInCount,ExpectedActivityOutCount,ExpectedActivityInAmount,
						ExpectedActivityOutAmount,InCountVariance,OutCountVariance,
						InAmountVariance,OutAmountVariance,RecurrenceType,
						RecurrenceValue,CubeName,PeerGroupName,
						CountriesExpected,CountriesNotExpected,	RuleID,
						RuleName,RuleDescription,PurgeAllowed,
						ClosedTime,	'GovernmentReportsFiled' = Case   when GovernmentReportsFiled >0 then 'Y' else 'N' end,
						PastCasesIdList,Notes,
						--ActivityIdList,
						BranchAddress,BranchCity,BranchState,BranchZip,CustomerDBA,
						CustomerAddress,CustomerCity,CustomerState,CustomerZip,CustomerCountry,CustomerTelephone,		
						CustomerWorkphone,CustomerTypeOfBusiness,CustomerDOB,CustomerLicenseNo,	
						CustomerPassportNo,CustomerAlienId,RelatedCustomerNames,RelationshipDescription,	
						PrimaryRelation,AccountStatus,LicenseState,PassportIssuingCountry,AlienIdIssuingCountry,
						IsArchived 	
						FROM #TempExport  WHERE row between @FirstRow and @LastRow 
						ORDER BY row
		END
		ELSE IF (@LastRecNo>0 and @pageno>0) 
				BEGIN
					
					SELECT @FirstRow = (@pageno-1) * @MaxRecordCount + 1 
					SELECT @LastRow = (@pageno-1) * @MaxRecordCount +  @MaxRecordCount		
							--Getting the Previous lastRecno  from page-1 recordset , shown below
					SELECT @PrevRecNo= Max(ID)  from #TempExport  Where row between @FirstRow and @LastRow  


					IF @LastRecNo <> @PrevRecNo 
						BEGIN
							RETURN @InvState  -- returning for representing out of sync,in case any new records inserted between multiple Stored proc calls
						END
					ELSE
						BEGIN
						
							SELECT @FirstRow = (@pageno) * @MaxRecordCount + 1 
							SELECT @LastRow = (@pageno) * @MaxRecordCount +  @MaxRecordCount
							

							SELECT   
								RecordType,ID,[Type],PartyID,
								PartyRiskClass,PartyName,PartyTIN ,
								[Description],Score,CreateTime ,
								OwnerBranchID,OwnerBranchName,OwnerDepartmentID ,
								OwnerDepartmentName,OwnerOperatorID,OwnerOperatorName,
								AccountID,StartDate,EndDate,ReviewState,
								ReviewTime,ReviewOperator,LastState,
								LastModifiedTime,LastOperator,Approved,
								ApproveTime,ApproveOperator,ActivityInCount,
								ActivityOutCount,ActivityInAmount,ActivityOutAmount,
								ExpectedActivityInCount,ExpectedActivityOutCount,ExpectedActivityInAmount,
								ExpectedActivityOutAmount,InCountVariance,OutCountVariance,
								InAmountVariance,OutAmountVariance,RecurrenceType,
								RecurrenceValue,CubeName,PeerGroupName,
								CountriesExpected,CountriesNotExpected,	RuleID,
								RuleName,RuleDescription,PurgeAllowed,ClosedTime,
								--If any reports filed return 'Y' or else 'N' shown below
								'GovernmentReportsFiled' = Case   when GovernmentReportsFiled >0 then 'Y' else 'N' end,
								PastCasesIdList,Notes,
								--ActivityIdList,
								BranchAddress,BranchCity,BranchState,BranchZip,CustomerDBA,
								CustomerAddress,CustomerCity,CustomerState,CustomerZip,CustomerCountry,CustomerTelephone,		
								CustomerWorkphone,CustomerTypeOfBusiness,CustomerDOB,CustomerLicenseNo,	
								CustomerPassportNo,CustomerAlienId,RelatedCustomerNames,RelationshipDescription,	
								PrimaryRelation,AccountStatus,LicenseState,PassportIssuingCountry,AlienIdIssuingCountry,
								IsArchived 	
								FROM #TempExport  Where row between @FirstRow and @LastRow 
								ORDER BY row
						END

				END


		--Check for errors returned by functions
		--Check PastCasesIdList for -@InvParam since the function could return the @InvParam value of 250010 as a valid Case Id
		SELECT @FunctionErrors = (SELECT COUNT(*) FROM #TempExport WHERE [Description] = CONVERT(VARCHAR, @InvParam) or PastCasesIdList = CONVERT(VARCHAR, -@InvParam))
		IF @FunctionErrors>0
			BEGIN
				RETURN @InvParam
			END

	GO


if exists (select * from sysobjects 
		where id = object_id(N'[dbo].[BSA_getPartyIds]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		Drop Procedure dbo.BSA_getPartyIds
	Go


-- Stored Procedures for Getting PartyId

CREATE PROCEDURE dbo.BSA_getPartyIds(@CustId as Objectid)
WITH ENCRYPTION AS
 declare @librarycontext varchar(max)      
 declare @createfpeobject varchar(max)      
 select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();

SELECT  CustomerId, IDType, 
--IDNumber, 
    case when IDNumber is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('PartyID','IDNumber')>0 then pcdb.dbo.FPEAccessEx(IDNumber,@librarycontext) else IDNumber end as IDNumber,
    IssueAgency, IssuePlace, IssueCountry, 
	IssueDate, ExpiryDate, CreateOper, CreateDate, LastOper, LastModify, Ts
FROM PartyId WITH (NOLOCK)
WHERE CustomerId = @CustId
Go


if exists (select * from sysobjects 
		where id = object_id(N'[dbo].[BSA_getPartyIdsbyIdType]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		Drop Procedure dbo.BSA_getPartyIdsbyIdType
	Go

CREATE PROCEDURE dbo.BSA_getPartyIdsbyIdType(@CustId as Objectid, @Type as Objectid)
WITH ENCRYPTION AS
 declare @librarycontext varchar(max)      
 declare @createfpeobject varchar(max)      
 select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();
SELECT  CustomerId, IDType,
        case when IDNumber is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('PartyID','IDNumber')>0 then pcdb.dbo.FPEAccessEx(IDNumber,@librarycontext) else IDNumber end as IDNumber,
        IssueAgency, IssuePlace, IssueCountry, 
	IssueDate, ExpiryDate, CreateOper, CreateDate, LastOper, LastModify, Ts
FROM PartyId WITH (NOLOCK)
WHERE CustomerId = @CustId and
	IdType = @Type
Go


if exists (select * from sysobjects 
		where id = object_id(N'[dbo].[BSA_getPartiesByRiskClass]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		Drop Procedure dbo.BSA_getPartiesByRiskClass
	Go


CREATE PROCEDURE dbo.BSA_getPartiesByRiskClass (@RiskClass as Scode)
WITH ENCRYPTION AS

 declare @librarycontext varchar(max)      
 declare @createfpeobject varchar(max)
 declare @istinencrypted int
 set @istinencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','tin')
 declare @ispassportnoencrypted int
 set @ispassportnoencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','PassportNo') 
 declare @islicensenoencrypted int
 set @islicensenoencrypted=[pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','LicenseNo')      
 select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE(); 

SELECT  [Id], Parent, [Name], DBA, SecCode, SecCode As SicCode, 
	Address, City, State, Zip, Country, Telephone, Email,
        case when TIN is not null and @istinencrypted>0 then pcdb.dbo.FPEAccessEx(TIN,@librarycontext) else TIN end as TIN,
        case when PassportNo is not null and @ispassportnoencrypted>0 then pcdb.dbo.FPEAccessEx(PassportNo,@librarycontext) else PassportNo end as PassportNo, 
        DOB, TypeOfBusiness, SourceOfFunds, 
	AccountOfficer, AcctOffTel,  AcctOffEmail, CompOfficer, CompOffTel, CompOffEmail, IdList, Notes, ExemptionStatus, LastActReset, LastReview, LastReviewOper,  
	Customer.CreateOper As CreateOper, Customer.CreateDate As CreateDate, Customer.LastOper As LastOper, Customer.LastModify As LastModify, OwnerBranch, 
	OwnerDept, OwnerOper, RiskClass, Closed, User1, User2, User3, User4, User5, CountryOfOrigin, CountryOfIncorp, OffshoreCorp, BearerShares, NomeDePlume, CTRAmt, 
	Type As CustomerType,  Resident, BusRelationNature, PrevRelations, PEP, PoliticalPos, CorrBankRelation,  Sex, ShellBank, HighRiskRespBank, OffshoreBank, 
	PayThroughAC, RegulatedAffiliate, USPerson, RFCalculate,  	
	Case Status
		When 1 Then 'Accepted'
		When 2 Then 'OverDue'
		When 3 Then 'Rejected'
		When 4 Then 'Needs Review' 
		Else 'Unknown'
	End As Status, 
	ReviewDate, AmtRange,
	case when LicenseNo is not null and @islicensenoencrypted>0 then pcdb.dbo.FPEAccessEx(LicenseNo,@librarycontext) else LicenseNo end as LicenseNo, 
        LicenseState, SwiftTid, Embassy, ForeignGovt, Charityorg, DoCIP,  
	OnProbation, OnProbation as IsOnProbation, ProbationReason, ProbationStartDate, ProbationEndDate, ClosedDate, ClosedReason, OpenDate, CountryOfResidence, CountryOfCitizenship, Prospect, 
	AssetSize, Income, IndOrBusType, 
	Case IndOrBusType 
		When  'I' Then 'Individual' 
		When 'B' Then 'Business' 
		Else 'Unknown' 
	End As IndOrBusTypeText,
	FEP, MSB, KYCDataCreateDate, KYCDataModifyDate, 	
	Case KYCStatus 
		When 1 Then 'IntermediateSave' 
		When 2 Then 'OnBoard' 
		Else 'Unknown' 
	End as KYCStatus, 
	NamePrefix, FirstName, MiddleName, LastName, NameSuffix, Alias, 
	PlaceofBirth, MothersMaidenName,  Introducer, YrsAtCurrAddress, NoOfDependants, EmployerName, JobTitle, JobTime,  LengthOfEmp, IsAlien, CountryofDualCitizenship, 
	SecondaryEmployer, OriginOfFunds, IsPersonalRelationship, DateOfInception, LegalEntityType, NoofEmployees, TotalAnnualSales, ProductsSold, ServicesProvided, 
	GeographicalAreasServed, IsPubliclyTraded, TickerSymbol, Exchange, IsPensionFund, IsEndowmentFund, IsGovernmentSponsored, IsSubsidiary, IsForeignCorrBnk, 
	RequireCashTrans, RequireForeignTrans, RequireWireTrans, HasForeignBranches, ForeignBranchCountries, TypeofMonInstruments, URL, HasPatriotActCertification, 
	PatriotActCertExpiryDate, MoneyOrders, MORegistration, MORegConfirmationNo, MOActingasAgentfor, MOMaxDailyAmtPerPerson, MOMonthlyAmt,  MOPercentIncome,  
	TravelersCheck, TCRegistration, TCRegConfirmationNo, TCActingasAgentfor, TCMaxDailyAmtPerPerson, TCMonthlyAmt, TCPercentIncome, MoneyTransmission, 
	MTRegistration, MTRegConfirmationNo, MTActingasAgentfor,  MTMaxDailyAmtPerPerson, MTMonthlyAmt, MTPercentIncome, CheckCashing, CCRegistration, 
	CCRegConfirmationNo, CCActingasAgentfor, CCMaxDailyAmtPerPerson, CCMonthlyAmt, CCPercentIncome, CurrencyExchange, CERegistration, CERegConfirmationNo, 
	CEActingasAgentfor, CEMaxDailyAmtPerPerson, CEMonthlyAmt, CEPercentIncome, CurrencyDealing, CDRegistration, CDRegConfirmationNo, CDActingasAgentfor, 
	CDMaxDailyAmtPerPerson, CDMonthlyAmt, CDPercentIncome, StoredValue, SVRegistration, SVRegConfirmationNo, SVActingasAgentfor, SVMaxDailyAmtPerPerson, 
	SVMonthlyAmt,  SVPercentIncome, CDDUser1, CDDUser2, CDDUser3, CDDUser4, CDDUser5, CDDUser6, CDDUser7, CDDUser8, CDDUser9, CDDUser10, CDDUser11, CDDUser12, 
	CDDUser13, CDDUser14, CDDUser15, CDDUser16, CDDUser17, CDDUser18, CDDUser19, CDDUser20, CDDUser21, CDDUser22, CDDUser23,  CDDUser24, CDDUser25, CDDUser26, 
	CDDUser27, CDDUser28, CDDUser29, CDDUser30, CDDUser31, CDDUser32, CDDUser33, CDDUser34, CDDUser35,  CDDUser36, CDDUser37, CDDUser38, CDDUser39, CDDUser40
FROM 
	Customer WITH (NOLOCK) LEFT OUTER JOIN KYCData WITH (NOLOCK) ON Customer.id= KYCData.CustomerID
WHERE 
	Customer.RiskClass = @RiskClass
Go


if exists (select * from sysobjects 
		where id = object_id(N'[dbo].[BSA_getPartiesByPartyType]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		Drop Procedure dbo.BSA_getPartiesByPartyType
	Go


-- Stored Procedures for Getting Parties based on partytype
CREATE PROCEDURE dbo.BSA_getPartiesByPartyType (@CustType as Scode)
WITH ENCRYPTION AS

 declare @librarycontext varchar(max)      
 declare @createfpeobject varchar(max)      
 select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();  

SELECT  [Id], Parent, [Name], DBA, SecCode, SecCode As SicCode, 
	Address, City, State, Zip, Country, Telephone, Email, 
	case when TIN is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','TIN')>0 then pcdb.dbo.FPEAccessEx(TIN,@librarycontext) else TIN end as TIN,
	case when PassportNo is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','PassportNo')>0 then pcdb.dbo.FPEAccessEx(PassportNo,@librarycontext) else PassportNo end as PassportNo, 
	DOB, TypeOfBusiness, SourceOfFunds, 
	AccountOfficer, AcctOffTel,  AcctOffEmail, CompOfficer, CompOffTel, CompOffEmail, IdList, Notes, ExemptionStatus, LastActReset, LastReview, LastReviewOper,  
	Customer.CreateOper As CreateOper, Customer.CreateDate As CreateDate, Customer.LastOper As LastOper, Customer.LastModify As LastModify, OwnerBranch, 
	OwnerDept, OwnerOper, RiskClass, Closed, User1, User2, User3, User4, User5, CountryOfOrigin, CountryOfIncorp, OffshoreCorp, BearerShares, NomeDePlume, CTRAmt, 
	Type As CustomerType,  Resident, BusRelationNature, PrevRelations, PEP, PoliticalPos, CorrBankRelation,  Sex, ShellBank, HighRiskRespBank, OffshoreBank, 
	PayThroughAC, RegulatedAffiliate, USPerson, RFCalculate,  	
	Case Status
		When 1 Then 'Accepted'
		When 2 Then 'OverDue'
		When 3 Then 'Rejected'
		When 4 Then 'Needs Review' 
		Else 'Unknown'
	End As Status, 
	ReviewDate, AmtRange,
	case when LicenseNo is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','LicenseNo')>0 then pcdb.dbo.FPEAccessEx(LicenseNo,@librarycontext) else LicenseNo end as LicenseNo,
	LicenseState, SwiftTid, Embassy, ForeignGovt, Charityorg, DoCIP,  
	OnProbation, OnProbation as IsOnProbation, ProbationReason, ProbationStartDate, ProbationEndDate, ClosedDate, ClosedReason, OpenDate, CountryOfResidence, CountryOfCitizenship, Prospect, 
	AssetSize, Income, IndOrBusType, 
	Case IndOrBusType 
		When  'I' Then 'Individual' 
		When 'B' Then 'Business' 
		Else 'Unknown' 
	End As IndOrBusTypeText,
	FEP, MSB, KYCDataCreateDate, KYCDataModifyDate, 	
	Case KYCStatus 
		When 1 Then 'IntermediateSave' 
		When 2 Then 'OnBoard' 
		Else 'Unknown' 
	End as KYCStatus, 
	NamePrefix, FirstName, MiddleName, LastName, NameSuffix, Alias, 
	PlaceofBirth, MothersMaidenName,  Introducer, YrsAtCurrAddress, NoOfDependants, EmployerName, JobTitle, JobTime,  LengthOfEmp, IsAlien, CountryofDualCitizenship, 
	SecondaryEmployer, OriginOfFunds, IsPersonalRelationship, DateOfInception, LegalEntityType, NoofEmployees, TotalAnnualSales, ProductsSold, ServicesProvided, 
	GeographicalAreasServed, IsPubliclyTraded, TickerSymbol, Exchange, IsPensionFund, IsEndowmentFund, IsGovernmentSponsored, IsSubsidiary, IsForeignCorrBnk, 
	RequireCashTrans, RequireForeignTrans, RequireWireTrans, HasForeignBranches, ForeignBranchCountries, TypeofMonInstruments, URL, HasPatriotActCertification, 
	PatriotActCertExpiryDate, MoneyOrders, MORegistration, MORegConfirmationNo, MOActingasAgentfor, MOMaxDailyAmtPerPerson, MOMonthlyAmt,  MOPercentIncome,  
	TravelersCheck, TCRegistration, TCRegConfirmationNo, TCActingasAgentfor, TCMaxDailyAmtPerPerson, TCMonthlyAmt, TCPercentIncome, MoneyTransmission, 
	MTRegistration, MTRegConfirmationNo, MTActingasAgentfor,  MTMaxDailyAmtPerPerson, MTMonthlyAmt, MTPercentIncome, CheckCashing, CCRegistration, 
	CCRegConfirmationNo, CCActingasAgentfor, CCMaxDailyAmtPerPerson, CCMonthlyAmt, CCPercentIncome, CurrencyExchange, CERegistration, CERegConfirmationNo, 
	CEActingasAgentfor, CEMaxDailyAmtPerPerson, CEMonthlyAmt, CEPercentIncome, CurrencyDealing, CDRegistration, CDRegConfirmationNo, CDActingasAgentfor, 
	CDMaxDailyAmtPerPerson, CDMonthlyAmt, CDPercentIncome, StoredValue, SVRegistration, SVRegConfirmationNo, SVActingasAgentfor, SVMaxDailyAmtPerPerson, 
	SVMonthlyAmt,  SVPercentIncome, CDDUser1, CDDUser2, CDDUser3, CDDUser4, CDDUser5, CDDUser6, CDDUser7, CDDUser8, CDDUser9, CDDUser10, CDDUser11, CDDUser12, 
	CDDUser13, CDDUser14, CDDUser15, CDDUser16, CDDUser17, CDDUser18, CDDUser19, CDDUser20, CDDUser21, CDDUser22, CDDUser23,  CDDUser24, CDDUser25, CDDUser26, 
	CDDUser27, CDDUser28, CDDUser29, CDDUser30, CDDUser31, CDDUser32, CDDUser33, CDDUser34, CDDUser35,  CDDUser36, CDDUser37, CDDUser38, CDDUser39, CDDUser40
FROM 	Customer WITH (NOLOCK) LEFT OUTER JOIN KYCData WITH (NOLOCK) ON Customer.id= KYCData.CustomerID
WHERE 	Customer.Type = @CustType
Go


if exists (select * from sysobjects 
		where id = object_id(N'[dbo].[BSA_getParty]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		Drop Procedure dbo.BSA_getParty
	Go


-- Stored Proc GetPartyInfo for Web Service
CREATE PROCEDURE dbo.BSA_getParty (@CustId as ObjectId)
WITH ENCRYPTION AS

  declare @librarycontext varchar(max)         
    declare @createfpeobject varchar(max)         
    select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE();   


SELECT 	[Id], Parent, [Name], DBA, SecCode, SecCode As SicCode, 
	Address, City, State, Zip, Country, Telephone, Email,  
case when TIN is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','TIN')>0 then pcdb.dbo.FPEAccessEx(TIN,@librarycontext) else TIN end as TIN,   
    case when PassportNo is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','PassportNo')>0 then pcdb.dbo.FPEAccessEx(PassportNo,@librarycontext) else PassportNo end as PassportNo,   

DOB, TypeOfBusiness, SourceOfFunds, 
	AccountOfficer, AcctOffTel,  AcctOffEmail, CompOfficer, CompOffTel, CompOffEmail, IdList, Notes, ExemptionStatus, LastActReset, LastReview, LastReviewOper,  
	Customer.CreateOper As CreateOper, Customer.CreateDate As CreateDate, Customer.LastOper As LastOper, Customer.LastModify As LastModify, OwnerBranch, 
	OwnerDept, OwnerOper, RiskClass, Closed, User1, User2, User3, User4, User5, CountryOfOrigin, CountryOfIncorp, OffshoreCorp, BearerShares, NomeDePlume, CTRAmt, 
	Type As CustomerType,  Resident, BusRelationNature, PrevRelations, PEP, PoliticalPos, CorrBankRelation,  Sex, ShellBank, HighRiskRespBank, OffshoreBank, 
	PayThroughAC, RegulatedAffiliate, USPerson, RFCalculate,  	
	Case Status
		When 1 Then 'Accepted'
		When 2 Then 'OverDue'
		When 3 Then 'Rejected'
		When 4 Then 'Needs Review' 
		Else 'Unknown'
	End As Status, 
	ReviewDate, AmtRange, case when LicenseNo is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','LicenseNo')>0 then pcdb.dbo.FPEAccessEx(LicenseNo,@librarycontext) else LicenseNo end as LicenseNo,

 LicenseState, SwiftTid, Embassy, ForeignGovt, Charityorg, DoCIP,  
	OnProbation, OnProbation as IsOnProbation, ProbationReason, ProbationStartDate, ProbationEndDate, ClosedDate, ClosedReason, OpenDate, CountryOfResidence, CountryOfCitizenship, Prospect, 
	AssetSize, Income, IndOrBusType, 
	Case IndOrBusType 
		When  'I' Then 'Individual' 
		When 'B' Then 'Business' 
		Else 'Unknown' 
	End As IndOrBusTypeText,
	FEP, MSB, KYCDataCreateDate, KYCDataModifyDate, 	
	Case KYCStatus 
		When 1 Then 'IntermediateSave' 
		When 2 Then 'OnBoard' 
		Else 'Unknown' 
	End as KYCStatus, 
	NamePrefix, FirstName, MiddleName, LastName, NameSuffix, Alias, 
	PlaceofBirth, MothersMaidenName,  Introducer, YrsAtCurrAddress, NoOfDependants, EmployerName, JobTitle, JobTime,  LengthOfEmp, IsAlien, CountryofDualCitizenship, 
	SecondaryEmployer, OriginOfFunds, IsPersonalRelationship, DateOfInception, LegalEntityType, NoofEmployees, TotalAnnualSales, ProductsSold, ServicesProvided, 
	GeographicalAreasServed, IsPubliclyTraded, TickerSymbol, Exchange, IsPensionFund, IsEndowmentFund, IsGovernmentSponsored, IsSubsidiary, IsForeignCorrBnk, 
	RequireCashTrans, RequireForeignTrans, RequireWireTrans, HasForeignBranches, ForeignBranchCountries, TypeofMonInstruments, URL, HasPatriotActCertification, 
	PatriotActCertExpiryDate, MoneyOrders, MORegistration, MORegConfirmationNo, MOActingasAgentfor, MOMaxDailyAmtPerPerson, MOMonthlyAmt,  MOPercentIncome,  
	TravelersCheck, TCRegistration, TCRegConfirmationNo, TCActingasAgentfor, TCMaxDailyAmtPerPerson, TCMonthlyAmt, TCPercentIncome, MoneyTransmission, 
	MTRegistration, MTRegConfirmationNo, MTActingasAgentfor,  MTMaxDailyAmtPerPerson, MTMonthlyAmt, MTPercentIncome, CheckCashing, CCRegistration, 
	CCRegConfirmationNo, CCActingasAgentfor, CCMaxDailyAmtPerPerson, CCMonthlyAmt, CCPercentIncome, CurrencyExchange, CERegistration, CERegConfirmationNo, 
	CEActingasAgentfor, CEMaxDailyAmtPerPerson, CEMonthlyAmt, CEPercentIncome, CurrencyDealing, CDRegistration, CDRegConfirmationNo, CDActingasAgentfor, 
	CDMaxDailyAmtPerPerson, CDMonthlyAmt, CDPercentIncome, StoredValue, SVRegistration, SVRegConfirmationNo, SVActingasAgentfor, SVMaxDailyAmtPerPerson, 
	SVMonthlyAmt,  SVPercentIncome, CDDUser1, CDDUser2, CDDUser3, CDDUser4, CDDUser5, CDDUser6, CDDUser7, CDDUser8, CDDUser9, CDDUser10, CDDUser11, CDDUser12, 
	CDDUser13, CDDUser14, CDDUser15, CDDUser16, CDDUser17, CDDUser18, CDDUser19, CDDUser20, CDDUser21, CDDUser22, CDDUser23,  CDDUser24, CDDUser25, CDDUser26, 
	CDDUser27, CDDUser28, CDDUser29, CDDUser30, CDDUser31, CDDUser32, CDDUser33, CDDUser34, CDDUser35,  CDDUser36, CDDUser37, CDDUser38, CDDUser39, CDDUser40
FROM Customer WITH (NOLOCK) LEFT OUTER JOIN KYCData WITH (NOLOCK) ON Customer.id= KYCData.CustomerID
WHERE Customer.Id = @CustId
Go


if exists (select * from sysobjects 
		where id = object_id(N'[dbo].[BSA_GetRegReportData]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		Drop Procedure dbo.BSA_GetRegReportData
	Go


create Procedure [dbo].[BSA_GetRegReportData] (
	@caseid Int,
	@custid varchar(35),
	@accountid varchar(35)
)
with encryption as
	declare @librarycontext varchar(max)      
	declare @createfpeobject varchar(max)      
	select @librarycontext=librarycontext,@createfpeobject=createfpeobject from pcdb.dbo.WCDB_fnCreateLibraryContextFPE(); 
    -- Check if Case is archived
	Declare @isArchivedCase bit
    Select @isArchivedCase = Case 
		When Exists (Select * From dbo.SuspiciousActivity WITH (NOLOCK) Where RecNo=@caseid)
		Then 0 Else 1 End

    -- Get CustID from case if no id is provided
    If @custid is null and @caseid is not null
		If @isArchivedCase=1
				Select @custid=Cust From dbo.SuspiciousActivityHist WITH (NOLOCK)
				where  RecNo=@caseid
        Else
				Select @custid=Cust From dbo.SuspiciousActivity WITH (NOLOCK) 
				where  RecNo=@caseid


     -- Get AccountID from case if no id is provided
    If @accountid is null and @caseid is not null
		If @isArchivedCase=1
				Select @accountid=Account From dbo.SuspiciousActivityHist WITH (NOLOCK) 
				where  RecNo=@caseid
        Else
				Select @accountid=Account  From dbo.SuspiciousActivity WITH (NOLOCK)
				where  RecNo=@caseid


	-- Get Sys Param Info
    Select 'SysParam' as TableID, sys.* From dbo.SysParam sys WITH (NOLOCK)

    --Get Case Info
    If @caseid is not Null
		If @isArchivedCase=1
			Select 'Case' AS TableID,sh.* From dbo.SuspiciousActivityHist sh WITH (NOLOCK)
			Where sh.RecNo=@caseid
        Else
			Select 'Case' AS TableID,s.* From dbo.SuspiciousActivity s WITH (NOLOCK)
			Where s.RecNo=@caseid

    --Get Customer Info
	;
	WITH AllSuspects As  (
		Select 'Customer' as TableID, cust.Id, cust.Parent,cust.Name, cust.DBA, cust.SecCode,
		cust.Address, cust.City, cust.State, cust.Zip, cust.Country, cust.Telephone ,cust.Email, 
		case when cust.TIN is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','TIN')>0 then pcdb.dbo.FPEAccessEx(cust.TIN,@librarycontext) else cust.TIN end as TIN, 
		case when cust.LicenseNo is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','LicenseNo')>0 then pcdb.dbo.FPEAccessEx(cust.LicenseNo,@librarycontext) else cust.LicenseNo end as LicenseNo,
		cust.LicenseState,
		case when cust.PassportNo is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','PassportNo')>0 then pcdb.dbo.FPEAccessEx(cust.PassportNo,@librarycontext) else cust.PassportNo end as PassportNo,
		cust.DOB, cust.TypeOfBusiness, cust.SourceOfFunds, cust.AccountOfficer, cust.AcctOffTel, cust.AcctOffEmail, cust.CompOfficer,
		cust.CompOffTel, cust.CompOffEmail, cust.IdList, cust.Notes, cust.ExemptionStatus, cust.LastActReset,
                cust.LastReview,cust.LastReviewOper, cust.CreateOper, cust.CreateDate, cust.LastOper, cust.LastModify,
		cust.OwnerBranch, cust.OwnerDept, cust.OwnerOper, cust.RiskClass, cust.Closed, cust.User1,
		cust.User2, cust.User3, cust.User4, cust.User5, cust.Ts, cust.CountryOfOrigin, cust.CountryOfIncorp,
		cust.OffshoreCorp,cust.BearerShares ,cust.NomeDePlume ,cust.Type,cust.Resident,cust.BusRelationNature,
		cust.PrevRelations, cust.PEP, cust.PoliticalPos, cust.FEP, cust.MSB, cust.CorrBankRelation,
		cust.Sex, cust.ShellBank, cust.HighRiskRespBank, cust.OffshoreBank,
		cust.PayThroughAC, cust.RegulatedAffiliate, cust.USPerson, cust.RFCalculate, cust.Status, cust.ReviewDate, cust.CTRAmt, cust.AmtRange, cust.SwiftTID,
		cust.Embassy, cust.ForeignGovt, cust.CharityOrg, cust.DoCIP, cust.Prospect, cust.AssetSize, cust.Income,
		cust.IndOrBusType, cust.OnProbation, cust.ProbationReason, cust.ProbationStartDate, cust.ProbationEndDate,
		cust.ClosedDate, cust.ClosedReason, cust.OpenDate, cust.CountryOfResidence, cust.CountryOfCitizenship, cust.KYCDataCreateDate,
		cust.KYCDataModifyDate, cust.KYCOperator, cust.KYCStatus,
		0 As iOrder From dbo.Customer cust WITH (NOLOCK) where cust.Id =  @custid   -- '6298'  -- @custid
		UNION
		SELECT 'Customer' as TableID, cust.Id, cust.Parent,cust.Name, cust.DBA, cust.SecCode,
		cust.Address, cust.City, cust.State, cust.Zip, cust.Country, cust.Telephone ,cust.Email, 
		case when cust.TIN is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','TIN')>0 then pcdb.dbo.FPEAccessEx(cust.TIN,@librarycontext) else cust.TIN end as TIN, 
		case when cust.LicenseNo is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','LicenseNo')>0 then pcdb.dbo.FPEAccessEx(cust.LicenseNo,@librarycontext) else cust.LicenseNo end as LicenseNo,
		cust.LicenseState,
		case when cust.PassportNo is not null and [pcdb].[dbo].[WCDB_fnIsTableColumnEncrypted]('customer','PassportNo')>0 then pcdb.dbo.FPEAccessEx(cust.PassportNo,@librarycontext) else cust.PassportNo end as PassportNo,
		cust.DOB, cust.TypeOfBusiness, cust.SourceOfFunds, cust.AccountOfficer, cust.AcctOffTel, cust.AcctOffEmail, cust.CompOfficer,
		cust.CompOffTel, cust.CompOffEmail, cust.IdList, cust.Notes, cust.ExemptionStatus, cust.LastActReset,
                cust.LastReview,cust.LastReviewOper, cust.CreateOper, cust.CreateDate, cust.LastOper, cust.LastModify,
		cust.OwnerBranch, cust.OwnerDept, cust.OwnerOper, cust.RiskClass, cust.Closed, cust.User1,
		cust.User2, cust.User3, cust.User4, cust.User5, cust.Ts, cust.CountryOfOrigin, cust.CountryOfIncorp,
		cust.OffshoreCorp,cust.BearerShares ,cust.NomeDePlume ,cust.Type,cust.Resident,cust.BusRelationNature,
		cust.PrevRelations, cust.PEP, cust.PoliticalPos, cust.FEP, cust.MSB, cust.CorrBankRelation,
		cust.Sex, cust.ShellBank, cust.HighRiskRespBank, cust.OffshoreBank,
		cust.PayThroughAC, cust.RegulatedAffiliate, cust.USPerson, cust.RFCalculate, cust.Status, cust.ReviewDate, cust.CTRAmt, cust.AmtRange, cust.SwiftTID,
		cust.Embassy, cust.ForeignGovt, cust.CharityOrg, cust.DoCIP, cust.Prospect, cust.AssetSize, cust.Income,
		cust.IndOrBusType, cust.OnProbation, cust.ProbationReason, cust.ProbationStartDate, cust.ProbationEndDate,
		cust.ClosedDate, cust.ClosedReason, cust.OpenDate, cust.CountryOfResidence, cust.CountryOfCitizenship, cust.KYCDataCreateDate,
		cust.KYCDataModifyDate, cust.KYCOperator, cust.KYCStatus, 
		
		1 As iOrder FROM Customer cust WITH (NOLOCK) JOIN CaseAdditionalSuspects s WITH (NOLOCK) ON cust.ID = s.Cust WHERE s.SARecNo = @caseid 
	)
	select * from  AllSuspects WITH (NOLOCK)
	order by iOrder, ID
    ; 

	--Get KYCData for Customer
    Select 'KYCData' as TableID, custDetail.FirstName, custDetail.MiddleName, custDetail.LastName 
		From dbo.KYCData custDetail WITH (NOLOCK) where custDetail.CustomerId=@custid

    --Get Account Info
    Select 'Account' as TableID, Acc.* from dbo.Account Acc WITH (NOLOCK) Where Acc.ID=@accountID
	--- The End of codes for Adding Additional Suspects of a BSA Case ---
GO

if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_HighRiskCountry]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.USR_HighRiskCountry
Go
CREATE PROCEDURE dbo.USR_HighRiskCountry(@WLCode SCode, @testAlert INT,
	@period varchar(25),
	@startDate varchar(10),
	@endDate varchar(10),  
    @minIndCount int,
	@minIndAmt money, 
    @maxIndAmt money,
	@minSumAmount money,
	@maxSumAmount money,
	@riskClassList varchar(8000),
	@customerTypeList varchar(8000),
	@accountTypeList varchar(8000),
	@activityTypeList varchar(8000),
    @countryList varchar(8000),
	@UseMonitor smallint,
	@recvpay INT,
	@branchList varchar(8000),
    @deptList varchar(8000),
	@cashType smallint,
	@ExcludeTranbyCustCtryCode smallint,
	@IncludeAddressFields smallint,
	@Useround smallInt,
	@Precision smallInt
	)

AS
/* RULE AND PARAMETER DESCRIPTION
Detects a single Originator sending transactions to one or more 
Beneficiaries. A transaction is included in the evaluation based on its 
qualification with: specified transaction types, a specified time period,
the amount exceeds a certain amount, and the cumulative amount of all the
transactions meeting these constraints is over a specified amount.  
Additional constraints include Country, Customer Type, Risk Class, 
Account Types, Cash/Non-Cash, Branch and Department. The alert/case
will display the account information in the description since the account
may not be held at the processing institution. Designed to be run Post-EOD.

	@period = Specifies the previous period accordingly. It Can be left blank,
		Week, Month, Quarter and Semi Annual. If blank the StartDate and EndDate 
  		must be specified. 
	@startDate = Specifies the Starting Date range of transactions to evaluate,  
    		uses the book date. Relevant only if the Period is not specified.
	@endDate = Specifies the Ending Date range of the transaction’s book date.
		Relevant only if the Period is not specified.
        @minIndCount = The Minimum individual transaction count, after applying 
		the Min-Ind-Amt and Max-Ind-Amt parameters
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
   	@maxIndAmt = The Maximum amount the individual transaction cannot exceed 
		to be included in the aggregation.  For no maximum use -1
	@minSumAmount = The Minimum aggregated transaction amount that must be 
		exceeded.  This includes those transactions that are between Min-Ind-Amt
		and Max-Ind-Amt.
	@maxSumAmount = The Maximum aggregated transaction amount that cannot be 
		exceeded. For no maximum use -1
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@customerTypeList = A comma separated list of Customer Types to include 
		in the evaluation, use -ALL- for any customer type.
	@accountTypeList = A comma separated list of Account Types to include 
		in the evaluation, use -ALL- for any account type.
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.
        @countryList = A comma separated list of Countries to include in the 
		evaluation, use -ALL- for any country. Fields evaluated are 
		BeneCountry, BeneBankCountry, ByOrderCountry, ByOrderBankCountry, 
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3 and
		IntermediaryCountry4
	@UseMonitor = Use list of countries from Country table where the "Monitor" flag 
		is set to true instead of country list above (overrides country list).
	@recvpay = Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both
	@branchList = A comma separated list of Branches to include in the evaluation.
	@deptList = A comma separated list of Departments to include in 
		the evaluation.
	@cashType = Specifies Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both
	@ExcludeTranbyCustCtryCode = Exclude all the transactions that produce hit when 
		the country on the transaction is the same as the country of the customer.
		0 for Do not exclude, 
		1 for Exclude if customer country matches, 
		2 for Exclude if any customer country matches" Value="0"/>
	@IncludeAddressFields = Include Address Fields in search to match on country.
					0 - for Do not Include
					1 - for Include
	@UseRound = Indicate if only the high round amounts have to considered.
					0 - Do not round
					1 - Round
	@Precision = Precision for rounding. Specify 3 for 1000
*/
	
/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@fromDate INT,
 	@toDate INT,
	@tranAmt MONEY,
	@tranCnt INT,
	@trnCnt INT
DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME

DECLARE @SetDate DATETIME
DECLARE @Countries varchar(2000)
DECLARE @OrigCountryList varchar(8000)
DECLARE @Accounts varchar(2000)
DECLARE @tmp INT

set @tmp = power(10, @precision)

CREATE  TABLE  #TT(
	BaseAmt MONEY,
	tranNo int,
	Cust VARCHAR(40),
	Account VARCHAR(40),
	benecountry VARCHAR(35), 
	beneBankcountry VARCHAR(35), 
	ByOrderCountry VARCHAR(35), 
	ByOrderBankCountry VARCHAR(35),
	IntermediaryCountry VARCHAR(35),
	IntermediaryCountry2 VARCHAR(35),
	IntermediaryCountry3 VARCHAR(35),
	IntermediaryCountry4 VARCHAR(35)	
)

CREATE  TABLE #TempAddrTable(
	BaseAmt MONEY,
	tranNo int,
	Cust VARCHAR(40),
	Account VARCHAR(40),
	benecountry VARCHAR(35), 
	beneBankcountry VARCHAR(35), 
	ByOrderCountry VARCHAR(35), 
	ByOrderBankCountry VARCHAR(35),
	IntermediaryCountry VARCHAR(35),
	IntermediaryCountry2 VARCHAR(35),
	IntermediaryCountry3 VARCHAR(35),
	IntermediaryCountry4 VARCHAR(35)	,
	BeneAddress	VARCHAR(120),
	BeneBankAddress VARCHAR(120),
	IntermediaryAddress VARCHAR(120),
	ByOrderAddress VARCHAR(120),
	ByOrderBankAddress VARCHAR(120),
	IntermediaryAddress2 VARCHAR(120),
	IntermediaryAddress3 VARCHAR(120),
	IntermediaryAddress4 VARCHAR(120)
)

CREATE  TABLE #TT1(
	Cust VARCHAR(40),
	Account VARCHAR(40),
	country VARCHAR(35)	
)
--created temp table #TempCountryTable to avoid the loop through country list if @IncludeAddressFields=1 
CREATE  TABLE #TempCountryTable (
country varchar(200)
)
-- created temp tables #TempCountries, #TempAccounts,#TempCursor to improve the performance.
CREATE  TABLE #TempCountries (
Cust VARCHAR(40),
countries varchar(2000)
)
CREATE  TABLE #TempAccounts (
Cust VARCHAR(40),
accounts varchar(2000)
)
CREATE  TABLE #TempCursor (
BaseAmt MONEY,
tranNo int,
Cust VARCHAR(40)
)
SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_HighRiskCountry
ELSE
	-- Already in a transaction
	SAVE TRAN USR_HighRiskCountry
/* End standard stored procedure transaction header */

/*  standard Rules Header */
-- Date options

--SET @SetDate = GetDate()

-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @description = [Desc], @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList (NOLOCK)
WHERE WLCode = @WLCode

Declare @BaseCurr char(3)
select @BaseCurr = IsNULL(BaseCurr,'') from SysParam

If(@period is null OR  ltrim(rtrim(@period)) = '') 
BEGIN
	SET @fromDate = dbo.ConvertSqlDateToInt(@startDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@endDate)
END
ELSE
BEGIN
	DECLARE @SQLStartDate datetime, @SQLEndDate datetime
	exec dbo.BSA_GetDateRange @SetDate, @period, 'PREV', 1, 
			@SQLStartDate OUTPUT, @SQLEndDate OUTPUT
	SET @fromDate = dbo.ConvertSqlDateToInt(@SQLStartDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@SQLEndDate)
END

	SELECT @riskClassList = dbo.BSA_fnListParams(@riskClassList)
	SELECT @customerTypeList = dbo.BSA_fnListParams(@customerTypeList)
	SELECT @accountTypeList = dbo.BSA_fnListParams(@accountTypeList)
	SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)
	SELECT @OrigCountryList = @countryList

	IF @UseMonitor = 1
	BEGIN
		SET @countryList = ''
		SELECT @countryList = COALESCE(@countryList + ',', '') 
				+ rtrim(code)
 			FROM Country 
			WHERE Monitor = 1

		IF LEN(LTRIM(RTRIM(@countryList)))> 0
		BEGIN
			SELECT @countryList=LTRIM(RTRIM(@countryList))+','
			IF (@IncludeAddressFields) = 1
			BEGIN  
			INSERT INTO #TempCountryTable
			Select (LTRIM(RTRIM(tblCountry.ID))) from pbsa.dbo.BSA_fnStrSplitter(@countryList) as tblCountry
			where tblCountry.ID IS NOT NULL and tblCountry.ID <> ''
			END
		END
		ELSE
			SELECT @countryList = ''
	END
	ELSE
	BEGIN
		IF (ISNULL(@countryList,'') = '' OR UPPER(ISNULL(@countryList,'-ALL-')) = '-ALL-')
			SELECT @countryList = ''
		ELSE
		BEGIN
			SELECT @countryList = ','+LTRIM(RTRIM(@countryList))+','
			IF (@IncludeAddressFields) = 1
			BEGIN  
			INSERT INTO #TempCountryTable
			Select (LTRIM(RTRIM(tblCountry.ID))) from pbsa.dbo.BSA_fnStrSplitter(@countryList) as tblCountry
			where tblCountry.ID IS NOT NULL and tblCountry.ID <> ''
			END
		END
	END

	SELECT @branchList = dbo.BSA_fnListParams(@branchList)
	SELECT @deptList = dbo.BSA_fnListParams(@deptList)

IF (@cashType = 2)
	SET @cashType = NULL

IF (@recvpay = 3)
	SET @recvpay = NULL
-- To include Address Fields in the search criteria


IF (@IncludeAddressFields) = 1
BEGIN  
	INSERT INTO #TempAddrTable (BaseAmt, tranNo, Cust, Account, benecountry,
			beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
			IntermediaryCountry, IntermediaryCountry2, 
			IntermediaryCountry3, IntermediaryCountry4,BeneAddress,
			BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
			IntermediaryAddress2,IntermediaryAddress3 ,
			IntermediaryAddress4)
			SELECT BaseAmt, tranNo, a.Cust, a.account,
			benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
			IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
			IntermediaryCountry4,BeneAddress,BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
			IntermediaryAddress2,IntermediaryAddress3 ,IntermediaryAddress4
			FROM ActivityHist a (NOLOCK) JOIN Customer (NOLOCK) ON a.cust = Customer.Id
			LEFT JOIN Account ac (NOLOCK) ON a.Account = ac.Id 
			WHERE a.bookdate >= @fromDate AND 
					a.bookdate <= @toDate AND 
					a.recvpay = ISNULL(@recvPay, a.recvpay) AND
					a.BaseAmt >= @minIndAmt AND
					(@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
					(ISNULL(@deptList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.dept,''))) + ',', @deptList) > 0)
					AND (ISNULL(@branchList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.branch,''))) + ',', @branchList) > 0)
					AND (ISNULL(@activityTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.type,''))) + ',', @activityTypeList) > 0)
					AND (ISNULL(@accountTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ac.type,''))) + ',', @accountTypeList) > 0)
					AND (ISNULL(@riskClassList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Customer.RiskClass,''))) + ',', @riskClassList) > 0)
					AND	(ISNULL(@customerTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Customer.type,''))) + ',', @customerTypeList) > 0)
					AND	(CASHTRAN = ISNULL(@cashType, CASHTRAN))
					AND 1 = 
					Case When @UseRound = 1 and (a.baseamt%@tmp = 0) Then 1 
					     When @UseRound = 0 Then 1
					     When @UseRound = 1 and (a.baseamt%@tmp <> 0) Then 0
					Else 1 End
					AND 
					( 
						(ISNULL(@countryList,'') = '') 	
						OR 
 							( exists(Select country from #TempCountryTable where ( CHARINDEX(country, a.BeneAddress) > 0)) 
 							OR 
 							exists(Select country from #TempCountryTable where ( CHARINDEX(country, a.BeneBankAddress) > 0))
 							OR 
 							exists(Select country from #TempCountryTable where ( CHARINDEX(country, a.IntermediaryAddress) > 0))
 							OR 
 							exists(Select country from #TempCountryTable where ( CHARINDEX(country, a.ByOrderAddress) > 0))
 							OR 
 							exists(Select country from #TempCountryTable where ( CHARINDEX(country, a.ByOrderBankAddress) > 0))
 							OR 
 							exists(Select country from #TempCountryTable where ( CHARINDEX(country, a.IntermediaryAddress2) > 0))
 							OR 
 							exists(Select country from #TempCountryTable where ( CHARINDEX(country, a.IntermediaryAddress3) > 0))
 							OR 
 							exists(Select country from #TempCountryTable where ( CHARINDEX(country, a.IntermediaryAddress4) > 0))
							)
						OR
							(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.BeneCountry,''))) + ',', @countryList) > 0 OR 
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.BeneBankCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.ByOrderCountry,''))) + ',', @countryList) > 0  OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry4,''))) + ',', @countryList) > 0 
							) 
					)
					
END
-- do not include address fields in the search criteria
ELSE
BEGIN
	INSERT INTO #TempAddrTable (BaseAmt, tranNo, Cust, Account, benecountry,
			beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
			IntermediaryCountry, IntermediaryCountry2, 
			IntermediaryCountry3, IntermediaryCountry4,BeneAddress,
			BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
			IntermediaryAddress2,IntermediaryAddress3 ,
			IntermediaryAddress4)	
		SELECT BaseAmt, tranNo, a.Cust, a.account,
		benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
		IntermediaryCountry4,BeneAddress,BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
		IntermediaryAddress2,IntermediaryAddress3 ,IntermediaryAddress4
	FROM ActivityHist a (NOLOCK) JOIN Customer (NOLOCK) on a.cust = Customer.Id
     LEFT JOIN Account ac (NOLOCK) ON a.Account = ac.Id 
      WHERE a.bookdate >= @fromDate AND 
		a.bookdate <= @toDate AND 
		a.recvpay = ISNULL(@recvPay, a.recvpay) AND
		a.BaseAmt >= @minIndAmt AND
	    	(@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
		(ISNULL(@deptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.dept,''))) + ',', @deptList) > 0)
		AND (ISNULL(@branchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.branch,''))) + ',', @branchList) > 0)
		AND (ISNULL(@activityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.type,''))) + ',', @activityTypeList) > 0)
		AND (ISNULL(@accountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ac.type,''))) + ',', @accountTypeList) > 0)
		AND (ISNULL(@riskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Customer.RiskClass,''))) + ',', @riskClassList) > 0)
		AND	(ISNULL(@customerTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Customer.type,''))) + ',', @customerTypeList) > 0)
		AND	(CASHTRAN = ISNULL(@cashType, CASHTRAN))
		AND(ISNULL(@countryList,'') = '' OR 
		(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.BeneCountry,''))) + ',', @countryList) > 0 OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.BeneBankCountry,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.ByOrderCountry,''))) + ',', @countryList) > 0  OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry4,''))) + ',', @countryList) > 0 ))
		AND 1 = 
		Case When @UseRound = 1 and (a.baseamt%@tmp = 0) Then 1 
		     When @UseRound = 0 Then 1
		     When @UseRound = 1 and (a.baseamt%@tmp <> 0) Then 0
		Else 1 End
		
END


IF (@ExcludeTranbyCustCtryCode = 0)
BEGIN
	INSERT INTO #TT (BaseAmt, tranNo, Cust, Account, benecountry,
			beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
			IntermediaryCountry, IntermediaryCountry2, 
			IntermediaryCountry3, IntermediaryCountry4)	
		SELECT DISTINCT BaseAmt, tranNo, a.Cust, a.account, 
		benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
		IntermediaryCountry4
FROM #TempAddrTable a INNER JOIN Customer C ON A.Cust = C.ID		
END
ELSE IF @ExcludeTranbyCustCtryCode = 1
BEGIN
	INSERT INTO #TT (BaseAmt, tranNo, Cust, Account, benecountry,
			beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
			IntermediaryCountry, IntermediaryCountry2, 
			IntermediaryCountry3, IntermediaryCountry4)	
		SELECT DISTINCT BaseAmt, tranNo, a.Cust, a.account, 
		benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
		IntermediaryCountry4
	FROM #TempAddrTable A INNER JOIN Customer C ON A.Cust = C.ID
	WHERE (ISNULL(@countryList, '' ) = '' OR 
		(	C.Country is NULL OR a.BeneCountry <> C.Country ) OR
		( C.Country is NULL OR a.BeneBankCountry <> C.Country ) OR
		( C.Country is NULL OR a.ByOrderCountry <> C.Country ) OR
		(C.Country is NULL OR a.ByOrderBankCountry <> C.Country )OR
		( C.Country is NULL OR a.IntermediaryCountry <> C.Country ) OR
		( C.Country is NULL OR a.IntermediaryCountry2 <> C.Country ) OR
		( C.Country is NULL OR a.IntermediaryCountry3 <> C.Country ) OR
		(C.Country is NULL OR a.IntermediaryCountry4 <> C.Country ))
		
END
ELSE IF @ExcludeTranbyCustCtryCode = 2
BEGIN
	INSERT INTO #TT (BaseAmt, tranNo, Cust, Account, benecountry,
			beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
			IntermediaryCountry, IntermediaryCountry2, 
			IntermediaryCountry3, IntermediaryCountry4)	
		SELECT DISTINCT BaseAmt, tranNo, a.Cust, a.account, 
		benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
		IntermediaryCountry4
	FROM #TempAddrTable A INNER JOIN Customer C ON A.Cust = C.ID
	LEFT OUTER JOIN PartyAddress p on a.cust = p.CustomerId
		WHERE (ISNULL(@countryList,'') = '' OR 
		( 		((a.BeneCountry <> c.Country OR c.Country is NULL) 	
				AND (c.CountryOfResidence is NULL OR a.BeneCountry <> c.CountryOfResidence )
				AND (c.CountryOfIncORp is NULL OR a.BeneCountry <> c.CountryOfIncORp )
				AND (c.CountryOfORigin is NULL OR a.BeneCountry <> c.CountryOfORigin  )
				AND (c.CountryOfCitizenship is NULL OR a.BeneCountry <> c.CountryOfCitizenship )
				AND (p.Country is NULL OR a.BeneCountry <> p.Country  ))
		) OR
		( 
			 (( c.Country is NULL OR a.BeneBankCountry <> c.Country)
				AND (c.CountryOfResidence is NULL OR a.BeneBankCountry <> c.CountryOfResidence)
				AND (c.CountryOfIncorp is NULL OR a.BeneBankCountry <> c.CountryOfIncorp)
				AND (c.CountryOfOrigin is NULL OR a.BeneBankCountry <> c.CountryOfORigin)
				AND (c.CountryOfCitizenship is NULL OR a.BeneBankCountry <> c.CountryOfCitizenship)
				AND (p.Country is NULL OR a.BeneBankCountry <> p.Country  ))
		) OR
		( 
				((c.Country is NULL or a.ByOrderCountry <> c.Country)
				AND (c.CountryOfResidence is NULL OR a.ByOrderCountry <> c.CountryOfResidence)
				AND (c.CountryOfIncORp is NULL OR a.ByOrderCountry <> c.CountryOfIncorp )
				AND (c.CountryOfORigin is NULL OR a.ByOrderCountry <> c.CountryOfORigin )
				AND (c.CountryOfCitizenship is NULL OR a.ByOrderCountry <> c.CountryOfCitizenship )
				AND (p.Country is NULL OR a.ByOrderCountry <> p.Country  ))

		) OR
		( 
			 ((a.ByORderBankCountry <> c.Country OR c.Country is NULL)
				AND (c.CountryOfResidence is NULL OR a.ByOrderBankCountry <> c.CountryOfResidence)
				AND (c.CountryOfIncORp is NULL OR a.ByOrderBankCountry <> c.CountryOfIncorp)
				AND (c.CountryOfORigin is NULL OR a.ByOrderBankCountry <> c.CountryOfORigin )
				AND (c.CountryOfCitizenship is NULL OR a.ByOrderBankCountry <> c.CountryOfCitizenship)
				AND (p.Country is NULL OR a.ByOrderBankCountry <> p.Country  ))
		) OR
		( 
			 ((c.Country is NULL OR a.IntermediaryCountry <> c.Country )
				AND (c.CountryOfResidence is NULL OR a.IntermediaryCountry <> c.CountryOfResidence )
				AND (c.CountryOfIncORp is NULL OR a.IntermediaryCountry <> c.CountryOfIncorp )
				AND (c.CountryOfORigin is NULL OR a.IntermediaryCountry <> c.CountryOfORigin )
				AND ( c.CountryOfCitizenship is NULL OR a.IntermediaryCountry <> c.CountryOfCitizenship)
				AND (p.Country is NULL OR a.IntermediaryCountry <> p.Country  ))
		) OR
		( 
			 ((c.Country is NULL OR a.IntermediaryCountry2 <> c.Country)
				AND (c.CountryOfResidence is NULL OR a.IntermediaryCountry2 <> c.CountryOfResidence)
				AND (c.CountryOfIncORp is NULL OR a.IntermediaryCountry2 <> c.CountryOfIncorp)
				AND (c.CountryOfORigin is NULL OR a.IntermediaryCountry2 <> c.CountryOfORigin)
				AND (c.CountryOfCitizenship is NULL OR a.IntermediaryCountry2 <> c.CountryOfCitizenship)
				AND (p.Country is NULL OR a.IntermediaryCountry2 <> p.Country  ))
		) OR
		( 
			 ((c.Country is NULL OR a.IntermediaryCountry3 <> c.Country)
				AND (c.CountryOfResidence is NULL OR a.IntermediaryCountry3 <> c.CountryOfResidence)
				AND (c.CountryOfIncORp is NULL OR a.IntermediaryCountry3 <> c.CountryOfIncorp)
				AND (c.CountryOfORigin is NULL OR a.IntermediaryCountry3 <> c.CountryOfORigin)
				AND (c.CountryOfCitizenship is NULL OR a.IntermediaryCountry3 <> c.CountryOfCitizenship)
				AND (p.Country is NULL OR a.IntermediaryCountry3 <> p.Country  ))
			) OR
		( 
			 ((c.Country is NULL OR a.IntermediaryCountry4 <> c.Country)
				AND ( c.CountryOfResidence is NULL Or a.IntermediaryCountry4 <> c.CountryOfResidence)
				AND (c.CountryOfIncORp is NULL OR a.IntermediaryCountry4 <> c.CountryOfIncorp)
				AND (c.CountryOfORigin is NULL OR a.IntermediaryCountry4 <> c.CountryOfORigin)
				AND (c.CountryOfCitizenship is NULL OR a.IntermediaryCountry4 <> c.CountryOfCitizenship)
				AND (p.Country is NULL OR a.IntermediaryCountry4 <> p.Country  ))
		) )
		
END
--Inserting to #TT1 for building the description
INSERT INTO #TT1(Cust, Account, Country)
	SELECT Cust, Account, benecountry
	FROM #TT 
	WHERE 	
		BeneCountry IS NOT NULL 
	UNION ALL 
	SELECT Cust, Account, beneBankcountry
	FROM #TT 
	WHERE 	
		beneBankcountry IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, IntermediaryCountry
	FROM #TT 
	WHERE 	
		IntermediaryCountry IS NOT NULL 
	UNION ALL 
	SELECT Cust, Account, ByOrderCountry
	FROM #TT 
	WHERE 	
		ByOrderCountry IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, ByOrderBankCountry
	FROM #TT 
	WHERE 	
		ByOrderBankCountry IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, IntermediaryCountry2
	FROM #TT 
	WHERE 	
		IntermediaryCountry2 IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, IntermediaryCountry3
	FROM #TT 
	WHERE 	
		IntermediaryCountry3 IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, IntermediaryCountry4
	FROM #TT 
	WHERE 	
		IntermediaryCountry4 is NOT NULL 
	UNION ALL 
	SELECT Cust, Account, NULL
	FROM #TT 
	WHERE 	
		IsNull(@CountryList, '') = ''


DECLARE	@cur CURSOR
DECLARE @Cust LONGNAME,
	@bookdate INT

SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())

INSERT INTO #TempCursor (BaseAmt,tranNo,Cust) SELECT DISTINCT SUM(BaseAmt) tranAmt, COUNT(tranNo) tranCnt, a.Cust
FROM #TT a 
GROUP BY  a.Cust
HAVING (SUM(BaseAmt) >= @minSumAmount AND 
	(@maxSumAmount = -1 OR SUM(BaseAmt) <= @maxSumAmount) AND
	count(tranno) >= @minIndCount)

INSERT INTO #TempCountries (Cust,countries) SELECT DISTINCT t.Cust,
  STUFF((SELECT DISTINCT ', ' + t1.country
         FROM #TT1 t1
         WHERE t.Cust = t1.Cust and 
		 (@countryList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, t1.country) + ',',@countryList) > 0) 
		 and t.country is not null
            FOR XML PATH(''), TYPE
            ).value('.', 'VARCHAR(2000)') 
        ,1,2,'') countries
FROM #TT1 t inner join  #TempCursor tc on t.Cust=tc.Cust;

INSERT INTO #TempAccounts (Cust,accounts) SELECT DISTINCT tt.Cust,
  STUFF((SELECT DISTINCT ', ' + t2.Account
         FROM #TT1 t2
         WHERE tt.Cust = t2.Cust 
		 and t2.Account is not null
            FOR XML PATH(''), TYPE
            ).value('.', 'VARCHAR(2000)') 
        ,1,2,'') accounts
FROM #TT1 tt inner join  #TempCursor tc on tt.Cust=tc.Cust;

SET @cur = CURSOR FAST_FORWARD FOR 
	SELECT tcu.BaseAmt as tranAmt,tcu.tranNo as tranCnt , tcu.Cust,tco.countries,tac.accounts 
	FROM #TempCursor tcu left join  #TempCountries tco on tcu.Cust=tco.Cust 
	left join #TempAccounts tac on tcu.Cust=tac.Cust
OPEN @cur 
FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @Cust, @Countries, @Accounts

WHILE @@FETCH_STATUS = 0 BEGIN
	IF (@IncludeAddressFields) = 1
	BEGIN
			 --@OrigCountryList is the original countrylist parameter  without extra commas
			SET @desc = 'Customer ' + @Cust 
			+ ' is doing transactions which pass through the specified countries '
			+ ISNULL(@OrigCountryList,'') 
			+ ' in either the country or the address fields '
			+ CASE WHEN ISNULL(@Accounts,'') = '' THEN '' 
			  ELSE ' using the account(s) ' + ISNULL(@Accounts,'') END
			+ ' and the total amount is: ' + CONVERT(VARCHAR, @tranAmt) + ' over '
			+ CONVERT(VARCHAR, @tranCnt) + ' transactions '
			+ 'for a period from ' 
			+ Cast(dbo.BSA_ConvertIntToSqlDate(@fromDate, 'mm/dd/yyyy') AS Char(11)) 
			+ ' to ' 
			+ Cast(dbo.BSA_ConvertIntToSqlDate(@toDate, 'mm/dd/yyyy') AS Char(11))
			
	END
	ELSE 
	BEGIN
		SET @desc = 'Customer ' + @Cust 
			+ ' is doing transactions which pass through the country(s) ' 
			+ ISNULL(@Countries,'') 
			+ CASE WHEN ISNULL(@Accounts,'') = '' THEN '' 
			  ELSE ' using the account(s) ' + ISNULL(@Accounts,'') END
			+ ' and the total amount is: ' + CONVERT(VARCHAR, @tranAmt) + ' over '
			+ CONVERT(VARCHAR, @tranCnt) + ' transactions '
			+ 'for a period from ' 
			+ Cast(dbo.BSA_ConvertIntToSqlDate(@fromDate, 'mm/dd/yyyy') AS Char(11)) 
			+ ' to ' 
			+ Cast(dbo.BSA_ConvertIntToSqlDate(@toDate, 'mm/dd/yyyy') AS Char(11))
	END 
	IF @testAlert = 1
	BEGIN
		EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
		@Cust, NULL, 1
		IF @stat <> 0 GOTO EndOfProc
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 
		BEGIN
			EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
				@Cust, NULL, 0
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE 
		IF @WLTYPE = 1 
		BEGIN
			EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT, 
				@WLCode, @desc, @bookdate, @Cust, NULL
			IF @stat <> 0 GOTO EndOfProc   	
		END	
	END		
	IF (@WLTYPE = 0) OR (@testAlert = 1)
	BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'Alert', @ID, TRANNO 
				FROM #TT t
				WHERE t.cust = @cust
	
			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC
	END 
	ELSE 
	IF @WLTYPE = 1 
	BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'SUSPACT', @ID, TRANNO 
				FROM #TT t
				WHERE t.cust = @cust

			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC 
	END
	FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @Cust, @Countries, @Accounts
END

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_HighRiskCountry
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_HighRiskCountry
RETURN @stat
Go
 IF EXISTS (SELECT * FROM SYSOBJECTS 
		WHERE ID = object_id(N'[dbo].[BSA_FnRemoveSpecialCharacters]'))
		DROP FUNCTION BSA_FnRemoveSpecialCharacters
	GO
	 
	/*
	This function accepts a string of Special Characters
	and returns the string witout Special Characters
			•	Comma (,)
			•	Dot or period (.)
			•	Question mark (?)
			•	At symbol (@)
			•	Ampersand (&)
			•	Exclamation point (!)
			•	Number/pound sign (#)
			•	Single quote (')
			•	Tilde (~)
			•	Asterisk (*) 
			•	Underscore (_)
			•	Minus sign/hyphen (-)
			•	Semi-colon (;)
			•	Plus sign (+)
			•	Open parenthesis (()
			•	Close parenthesis ())
			•	Opening bracket ([)
			•	Closing bracket (])
			•	Colon (:)
			•	Comma (,)
			•	Percent Sign  (%)
			•	Equals (=)
			•	Slash or divide (/)
			•	Backslash (\)
			•	Dollar ($)
			•	Less than or open angled bracket (<)
			•	Greater than or close angled bracket (>)
			•	Double quotes or speech marks (")
			•	Caret ‐ circumflex (^)
			•	Grave accent (`)
			•	Pipe (|)
			•	Open Curly Braces ({)
			•	Close Curly Braces(})

	*/
	CREATE FUNCTION dbo.BSA_FnRemoveSpecialCharacters(@Inputstr LongName)
	RETURNS LongName WITH ENCRYPTION As

	BEGIN 
		WHILE PATINDEX( '%[^a-zA-Z0-9 ]%', @Inputstr ) > 0
			
			SET @Inputstr = REPLACE( @Inputstr, SUBSTRING( @Inputstr, PATINDEX( '%[^a-zA-Z0-9 ]%', @Inputstr ), 1 ),'')
     	
		--If the input value is null it is returned as null
		RETURN LTRIM(RTRIM(@Inputstr))
	END
	GO

--USR_MulByOrderToOneBene
IF NOT EXISTS (SELECT * FROM Watchlist
WHERE WLCode = 'MulOrg1Bene') BEGIN
insert into Watchlist (WLCode, Title, [Desc], WLType, SPName, SuspType,
Schedule, IsPreEOD, OwnerBranch, OwnerDept, OwnerOper, CreateOper, CreateDate,
 Params, IsUserDefined, CreateType,  IsLicensed, UseSysDate,OverrideExemption)
values ('MulOrg1Bene','Detects one or more Originators sending transactions to a single '+ 
'Beneficiary', 'Detects one or more Originators sending transactions to a single  '+
'Beneficiary.  A transaction is included in the evaluation based on its '+ 
'qualification with: specified transaction types, a specified time period, '+ 
'the amount exceeds a certain amount, and the cumulative amount of all the '+
'transactions meeting these constraints is over a specified amount. '+  
'Additional constraints include Country, Customer Type, Risk Class, Account Types, '+
'Cash/Non-Cash, Branch and Department.  The alert/case will display the account '+ 
'information in the description since the account may not be held at the '+ 
'processing institution.'+ 
'When using Start Date and End Date parameters, Disable the rule or Reset parameter after rule executes. Designed to be run Post-EOD. ' +
'If the option to remove special/wildcard characters is enabled, then all non-alpha '+
'numeric characters other than spaces will be removed for name matching in Beneficiary and ByOrder'+
'name fields. For example X.Y.Z Corp become XYZ Corp for matching purpose.', 0 ,
'USR_MulByOrderToOneBene', 'Rule', 4,
0, Null, Null, Null,'Prime', GETDATE(),
'<Params>
    <Param Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month" DataType="String"/>
   	<Param Name="@StartDate" Alias="Start Date for Look Back MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" DataType="String"/>
   	<Param Name="@EndDate" Alias="End Date for Look Back MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" DataType="String"/>
	<Param Name="@minIndCount" Alias="Minimum transaction count" Value="0" DataType="Int"/>
	<Param Name="@minIndAmt" Alias="Minimum individual transaction amount" Value="0" DataType="Money"/>
	<Param Name="@maxIndAmt" Alias="Maximum individual transaction amount. For no maximum use -1" Value="-1" DataType="Money"/>
	<Param Name="@minSumAmount" Alias="Minimum aggregated transaction amount."  Value="0" DataType="Money"/>
	<Param Name="@maxSumAmount" Alias="Maximum aggregated transaction amount. For no maximum use -1"  Value="-1" DataType="Money"/>
	<Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@accountTypeList" Alias="List of Account Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@countryList" Alias="List of Countries separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" DataType="Int"/>
	<Param Name="@branchList" Alias="List of Branches separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
	<Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
	<Param Name="@cashType" Alias="Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both" Value="0" DataType="Int"/>
	<Param Name="@NumByOrder" Alias="Minimum number of Originators" Value="1" DataType="Int"/>
	<Param Name="@RemoveSpecialChar" Alias="Remove Special/Wildcard characters in Beneficiary and ByOrder Name.(0 for No, 1 for Yes)(See rule description for more details)" Value="0" DataType="Int"/>
</Params>',
0,0,0,1,0)
END ELSE BEGIN
UPDATE WATCHLIST
SET
Title = 'Detects one or more Originators sending transactions to a single Beneficiary',
[Desc] = 'Detects one or more Originators sending transactions to a single  '+
'Beneficiary.  A transaction is included in the evaluation based on its '+ 
'qualification with: specified transaction types, a specified time period, '+ 
'the amount exceeds a certain amount, and the cumulative amount of all the '+ 
'transactions meeting these constraints is over a specified amount. '+  
'Additional constraints include Country, Customer Type, Risk Class, Account Types, '+
'Cash/Non-Cash, Branch and Department.  The alert/case will display the account '+ 
'information in the description since the account may not be held at the '+ 
'processing institution. When using Start Date and End Date parameters,'+ 
'Disable the rule or Reset parameter after rule executes. Designed to be run Post-EOD. ' +
'If the option to remove special/wildcard characters is enabled, then all non-alpha '+
'numeric characters other than spaces will be removed for name matching in '+
'Beneficiary and ByOrder name fields. For example X.Y.Z Corp become XYZ Corp for matching purpose.',
WLType = 0, SPName = 'USR_MulByOrderToOneBene', SuspType = 'Rule',
Schedule = 4, IsPreEOD = 0,
OwnerOper = Null, OwnerBranch = Null, OwnerDept = Null,
Params =
'<Params>
    <Param Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month" DataType="String"/>
   	<Param Name="@StartDate" Alias="Start Date for Look Back MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" DataType="String"/>
   	<Param Name="@EndDate" Alias="End Date for Look Back MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" DataType="String"/>
	<Param Name="@minIndCount" Alias="Minimum transaction count" Value="0" DataType="Int"/>
	<Param Name="@minIndAmt" Alias="Minimum individual transaction amount" Value="0" DataType="Money"/>
	<Param Name="@maxIndAmt" Alias="Maximum individual transaction amount. For no maximum use -1" Value="-1" DataType="Money"/>
	<Param Name="@minSumAmount" Alias="Minimum aggregated transaction amount."  Value="0" DataType="Money"/>
	<Param Name="@maxSumAmount" Alias="Maximum aggregated transaction amount. For no maximum use -1"  Value="-1" DataType="Money"/>
	<Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@accountTypeList" Alias="List of Account Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@countryList" Alias="List of Countries separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" DataType="Int"/>
	<Param Name="@branchList" Alias="List of Branches separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
	<Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
	<Param Name="@cashType" Alias="Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both" Value="0" DataType="Int"/>
	<Param Name="@NumByOrder" Alias="Minimum number of Originators" Value="1" DataType="Int"/>
	<Param Name="@RemoveSpecialChar" Alias="Remove Special/Wildcard characters in Beneficiary and ByOrder Name.(0 for No, 1 for Yes)(See rule description for more details)" Value="0" DataType="Int"/>
</Params>',
IsUserDefined = 0, CreateType = 0, IsLicensed = 0, LastOper = 'Prime', UseSysDate=1,OverrideExemption=0
WHERE WLCode = 'MulOrg1Bene'
END
Go

/******************************************************************************
Procedure Name: USR_MulByOrderToOneBene
Description:Detects one or more Originators sending transactions to a single 
Beneficiary.  A transaction is included in the evaluation based on its 
qualification with: specified transaction types, a specified time period, 
the amount exceeds a certain amount, and the cumulative amount of all the 
transactions meeting these constraints is over a specified amount.  
Additional constraints include Country, Customer Type, Risk Class, Account Types,
Cash/Non-Cash, Branch and Department.  The alert/case will display the account 
information in the description since the account may not be held at the 
processing institution. Designed to be run Post-EOD

Parameters:		@WLCode [in] SCode = Rule Code				
				[Return] INT = Return the error status.  0 for success.
******************************************************************************/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_MulByOrderToOneBene]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.USR_MulByOrderToOneBene
Go
CREATE PROCEDURE dbo.USR_MulByOrderToOneBene(@WLCode SCode, @testAlert INT,
	@period varchar(25),
	@startDate varchar(10),
	@endDate varchar(10),   
   	@minIndCount int,
	@minIndAmt money, 
	@maxIndAmt money,
	@minSumAmount money,
	@maxSumAmount money,
	@riskClassList varchar(8000),
	@customerTypeList varchar(8000),
	@accountTypeList varchar(8000),
	@activityTypeList varchar(8000),
   	@countryList varchar(8000),
	@recvpay INT,
	@branchList varchar(8000),
   	@deptList varchar(8000),
	@cashType smallint,
	@NumByOrder smallint,
	@RemoveSpecialChar smallint =0
	)

AS
/* RULE AND PARAMETER DESCRIPTION
Detects one or more Originators sending transactions to a single 
Beneficiary.  A transaction is included in the evaluation based on its 
qualification with: specified transaction types, a specified time period, 
the amount exceeds a certain amount, and the cumulative amount of all the 
transactions meeting these constraints is over a specified amount.  
Additional constraints include Country, Customer Type, Risk Class, Account Types,
Cash/Non-Cash, Branch and Department.  The alert/case will display the account 
information in the description since the account may not be held at the 
processing institution. Designed to be run Post-EOD

	@period = Specifies the previous period accordingly. It Can be left blank,
		Week, Month, Quarter and Semi Annual. If blank the StartDate and EndDate 
  		must be specified. 
	@startDate = Specifies the Starting Date range of transactions to evaluate,  
    	uses the book date. Relevant only if the Period is not specified.
	@endDate = Specifies the Ending Date range of the transaction’s book date.
		Relevant only if the Period is not specified.
    @minIndCount = The Minimum individual transaction count, after applying 
		the Min-Ind-Amt and Max-Ind-Amt parameters
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
    @maxIndAmt = The Maximum amount the individual transaction cannot exceed 
		to be included in the aggregation.  For no maximum use -1
	@minSumAmount = The Minimum aggregated transaction amount that must be 
		exceeded.  This includes those transactions that are between Min-Ind-Amt
		and Max-Ind-Amt.
	@maxSumAmount = The Maximum aggregated transaction amount that cannot be 
		exceeded. For no maximum use -1
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@customerTypeList = A comma separated list of Customer Types to include 
		in the evaluation, use -ALL- for any customer type.
	@accountTypeList = A comma separated list of Account Types to include 
		in the evaluation, use -ALL- for any account type.
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.
    @countryList = A comma separated list of Countries to include in the 
		evaluation, use -ALL- for any country. Fields evaluated are 
		BeneCountry and ByOrderCountry.
	@recvpay = Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both
	@branchList = A comma separated list of Branches to include in the evaluation.
    @deptList = A comma separated list of Departments to include in 
		the evaluation.
	@cashType = Specifies Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both
	@NumByorders = Number of Originators. Alert is generated only if payments are 
		sent by specified number or more Originators.
	@RemoveSpecialChar = Rule is configured to Remove Special/Wildcard characters in the Bene Name Field.

*/
	
/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@fromDate INT,
 	@toDate INT,
	@tranAmt MONEY,
	@tranCnt INT,
	@trnCnt INT,
	@Cust VarChar(35),
	@OwnerBranch VarChar(11),
	@OwnerDept VarChar(11)
DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME

DECLARE @SetDate DATETIME

DECLARE @TransTbl TABLE (
	TranNo INT,
	Bene VARCHAR(40),
	ByOrder VARCHAR(40),
	BookDate INT,
	BaseAmt MONEY,
	Cust VARCHAR(40),
	OwnerBranch VARCHAR(11),
	OwnerDept VARCHAR(11)
)

DECLARE	@TT TABLE (
	tranAmt MONEY,
	tranCnt INT,
	Bene  VARCHAR(40),
	CustCount INT,
	BranchCount INT,
	DeptCount INT,
	Cust VARCHAR(40),
	OwnerBranch VARCHAR(11),
	OwnerDept VARCHAR(11)
)

DECLARE @CustomerExemptionsList VARCHAR(8000)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_MulByOrderToOneBene
ELSE
	-- Already in a transaction
	SAVE TRAN USR_MulByOrderToOneBene
/* End standard stored procedure transaction header */

/*  standard Rules Header */
--Set  @SetDate = GetDate()

-- Date options
-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @description = [Desc], @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList (NOLOCK)
WHERE WLCode = @WLCode

Declare @BaseCurr char(3)
select @BaseCurr = IsNULL(BaseCurr,'') from SysParam

If(@period is null OR  ltrim(rtrim(@period)) = '') 
BEGIN
	SET @fromDate = dbo.ConvertSqlDateToInt(@startDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@endDate)
END
ELSE
BEGIN
	DECLARE @SQLStartDate datetime, @SQLEndDate datetime
	exec dbo.BSA_GetDateRange @SetDate, @period, 'PREV', 1, 
			@SQLStartDate OUTPUT, @SQLEndDate OUTPUT
	SET @fromDate = dbo.ConvertSqlDateToInt(@SQLStartDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@SQLEndDate)
END

SELECT @riskClassList = dbo.BSA_fnListParams(@riskClassList)
SELECT @customerTypeList = dbo.BSA_fnListParams(@customerTypeList)
SELECT @accountTypeList = dbo.BSA_fnListParams(@accountTypeList)
SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)
SELECT @countryList = dbo.BSA_fnListParams(@countryList)
SELECT @branchList = dbo.BSA_fnListParams(@branchList)
SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)

SELECT @CustomerExemptionsList = dbo.BSA_GetExemptionCodes('Customer','Rule')
IF ltrim(rtrim(@CustomerExemptionsList)) = ''
	SET @CustomerExemptionsList = NULL

IF (@cashType = 2)
	SET @cashType = NULL

IF (@recvpay = 3)
	SET @recvpay = NULL

Insert Into @TransTbl(TranNo, Bene, ByOrder, BookDate, BaseAmt, Cust, OwnerBranch, OwnerDept)
Select TranNo, 
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(Bene) ELSE Bene END AS Bene, 
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(ByOrder) ELSE ByOrder END AS ByOrder,
BookDate, BaseAmt, Cust, a.Branch, Dept
FROM ActivityHist a (NOLOCK)
Join Customer (NOLOCK) ON 	a.cust = Customer.Id
Left Join Account Ac (NOLOCK) ON a.Account = ac.Id
WHERE  
	a.bookdate >= @fromDate AND 
	a.bookdate <= @toDate AND 
	a.recvpay = ISNULL(@recvPay, a.recvpay) AND
	a.BaseAmt >= @minIndAmt AND
	(@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
	a.byOrder IS NOT NULL and 
	a.bene IS NOT NULL AND  
	(@deptList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR,  a.dept) + ',',@deptList) > 0) AND
	(@branchList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR,  a.branch) + ',', @branchList) > 0) AND 
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
	(@accountTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, ac.type) + ',',@accountTypeList) > 0) AND
	(@countryList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, a.BeneCountry) + ',',@countryList) > 0 OR
			CHARINDEX(',' + 
				CONVERT(VARCHAR, a.ByOrderCountry) + ',',@countryList) > 0 ) AND
	((@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0)) AND
	((@customerTypeList IS NULL OR
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.type)) + ',', @customerTypeList ) > 0)) AND
	(Customer.Exemptionstatus IS NULL OR @CustomerExemptionsList IS NULL OR
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.Exemptionstatus)) + ',', @CustomerExemptionsList ) = 0) AND
	(CASHTRAN = ISNULL(@cashType, CASHTRAN))

INSERT INTO @tt (tranAmt, tranCnt, Bene, CustCount, BranchCount, DeptCount)
SELECT DISTINCT SUM(BaseAmt) tranAmt, COUNT(tranNo) tranCnt, a.Bene, 
       Count(Distinct Cust), Count(Distinct OwnerBranch), Count(Distinct OwnerDept)
FROM @TransTbl a 
GROUP BY  a.Bene
HAVING (SUM(BaseAmt) >= @minSumAmount AND 
	(@maxSumAmount = -1 OR SUM(BaseAmt) <= @maxSumAmount) AND
	count(tranno) >= @minIndCount) AND 
	COUNT(DISTINCT a.ByOrder) >= @NumByOrder

Update tt Set Cust = trn.Cust
From @tt tt Join @TransTbl trn 
On tt.Bene = trn.Bene
Where CustCount = 1

Update tt Set OwnerBranch = trn.OwnerBranch
From @tt tt Join @TransTbl trn 
On tt.Bene = trn.Bene
Where CustCount > 1 AND BranchCount = 1

Update tt Set OwnerDept = trn.OwnerDept
From @tt tt Join @TransTbl trn 
On tt.Bene = trn.Bene
Where CustCount > 1 AND DeptCount = 1



DECLARE	@cur CURSOR
DECLARE @Bene LONGNAME,
	@bookdate INT

SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())

SET @cur = CURSOR FAST_FORWARD FOR 
SELECT a.tranAmt, a.tranCnt, a.Bene, a.Cust, a.OwnerBranch, a.OwnerDept
FROM @tt a 

OPEN @cur 
FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @Bene, @cust, @OwnerBranch, @Ownerdept

WHILE @@FETCH_STATUS = 0 BEGIN

	SET @desc = @Bene + '(Bene) is ' 
		+ Case @recvPay
			WHEN 1 THEN 'Receiving from '
			WHEN 2 THEN 'Paying To '
			ELSE 'Receiving From/Paying To ' 
		  END
		+ 'multiple ByOrders ' 
		+ 'amount: ' + DBO.BSA_InternalizationMoneyToString(@tranAmt) + Space(1)+ @BaseCurr + ' over '
		+ CONVERT(VARCHAR, @tranCnt) + ' transactions'
		+ ' over a period from ' + Cast(dbo.BSA_InternalizationIntToShortDate(@fromDate) AS Char(11)) + ' to ' 
		+ Cast(dbo.BSA_InternalizationIntToShortDate(@toDate) AS Char(11))

	IF @testAlert = 1
	BEGIN
		EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
			@Cust, NULL, 1
		IF @stat <> 0 GOTO EndOfProc
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 
		BEGIN
			EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
				@Cust, NULL, 0
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE 
		IF @WLTYPE = 1 
		BEGIN
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
				ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
				RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
				ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
				EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
				OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
				REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
				WLCode, WLDESC, CREATETIME, OwnerBranch, OwnerDept )
				SELECT  NULL, DBO.CONVERTSQLDATETOINT(GETDATE()), @Cust, NULL,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, @desc, GETDATE(), @OwnerBranch, @OwnerDept

			set @ID = @@identity

		END	
	END		
	IF (@WLTYPE = 0) OR (@testAlert = 1)
	BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'Alert', @ID, TRANNO 
			FROM  @TransTbl a
			WHERE  a.Bene = @Bene

			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC
	END 
	ELSE 
	IF @WLTYPE = 1 
	BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'SUSPACT', @ID, TRANNO 
			FROM  @TransTbl a
			WHERE  a.Bene = @Bene

		SELECT @STAT = @@ERROR 
		IF @STAT <> 0 GOTO ENDOFPROC	
	END
	FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @Bene, @cust, @OwnerBranch, @Ownerdept
END

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_MulByOrderToOneBene
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_MulByOrderToOneBene
RETURN @stat
Go

--USR_OneByOrderToMulBene
IF NOT EXISTS (SELECT * FROM Watchlist
WHERE WLCode = '1OrgMulBene') BEGIN
insert into Watchlist (WLCode, Title, [Desc], WLType, SPName, SuspType,
Schedule, IsPreEOD, OwnerBranch, OwnerDept, OwnerOper, CreateOper, CreateDate,
 Params, IsUserDefined, CreateType,  IsLicensed, UseSysDate, OverrideExemption)
values ('1OrgMulBene','Detects a single Originator sending transactions to one or more Beneficiaries',
'Detects a single Originator sending transactions to ' +  
'specified number of Beneficiaries or more. A transaction is included in the evaluation based on its '+
'qualification with: specified transaction types, a specified time period, '+
'the amount exceeds a certain amount, and the cumulative amount of all the '+
'transactions meeting these constraints is over a specified amount. '+  
'Additional constraints include Country, Customer Type, Risk Class, '+ 
'Account Types, Cash/Non-Cash, Branch and Department. The alert/case '+
'will display the account information in the description since the account '+
'may not be held at the processing institution.'+
'When using Start Date and End Date parameters, Disable the rule or Reset parameter after rule executes. Designed to be run Post-EOD. ' +
'If the option to remove special/wildcard characters is enabled, then all non-alpha '+
'numeric characters other than spaces will be removed for name matching in ' + 
'ByOrder and Beneficiary name fields. For example X.Y.Z Corp become XYZ Corp for matching purpose.', 0 ,
'USR_OneByOrderToMulBene', 'Rule', 4,
0, Null, Null, Null,'Prime', GETDATE(),
'<Params>
 	<Param Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month" DataType="String"/>
   	<Param Name="@StartDate" Alias="Start Date for Look Back MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" DataType="String"/>
   	<Param Name="@EndDate" Alias="End Date for Look Back MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" DataType="String"/>
	<Param Name="@minIndCount" Alias="Minimum transaction count" Value="0" DataType="Int"/>
	<Param Name="@minIndAmt" Alias="Minimum individual transaction amount" Value="0" DataType="Money"/>
	<Param Name="@maxIndAmt" Alias="Maximum individual transaction amount. For no maximum use -1" Value="-1" DataType="Money"/>
	<Param Name="@minSumAmount" Alias="Minimum aggregated transaction amount."  Value="0" DataType="Money"/>
	<Param Name="@maxSumAmount" Alias="Maximum aggregated transaction amount. For no maximum use -1"  Value="-1" DataType="Money"/>
	<Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@accountTypeList" Alias="List of Account Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@countryList" Alias="List of Countries separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" DataType="Int"/>
	<Param Name="@branchList" Alias="List of Branches separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
	<Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
	<Param Name="@cashType" Alias="Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both" Value="0" DataType="Int"/>
	<Param Name="@NumBene" Alias="Minimum number of beneficiaries" Value="1" DataType="Int"/>
	<Param Name="@RemoveSpecialChar" Alias="Remove Special/Wildcard characters in ByOrder and Beneficiary Name.(0 for No, 1 for Yes)(See rule description for more details)" Value="0" DataType="Int"/>
</Params>',
0,0,0,1,0)
END ELSE BEGIN
UPDATE WATCHLIST
SET
Title = 'Detects a single Originator sending transactions to one or more Beneficiaries',
[Desc] = 'Detects a single Originator sending transactions to  '+
'specified number of Beneficiaries or more. A transaction is included in the evaluation based on its '+
'qualification with: specified transaction types, a specified time period, '+
'the amount exceeds a certain amount, and the cumulative amount of all the '+
'transactions meeting these constraints is over a specified amount. '+
'Additional constraints include Country, Customer Type, Risk Class, '+
'Account Types, Cash/Non-Cash, Branch and Department. The alert/case '+
'will display the account information in the description since the account '+
'may not be held at the processing institution. '+
'When using Start Date and End Date parameters, Disable the rule or Reset parameter after rule executes. Designed to be run Post-EOD. '+
'If the option to remove special/wildcard characters is enabled, then all non-alpha '+
'numeric characters other than spaces will be removed for name matching in '+
'ByOrder and Beneficiary name fields. For example X.Y.Z Corp become XYZ Corp for matching purpose.',
WLType = 0, SPName = 'USR_OneByOrderToMulBene', SuspType = 'Rule',
Schedule = 4, IsPreEOD = 0,
OwnerOper = Null, OwnerBranch = Null, OwnerDept = Null,
Params =
'<Params>
 	<Param Name="@Period" Alias="Activity Period. Week, Month, Quarter, Semi-Annual, if Blank-Use Date Parameters" Value="Month" DataType="String"/>
   	<Param Name="@StartDate" Alias="Start Date for Look Back MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" DataType="String"/>
   	<Param Name="@EndDate" Alias="End Date for Look Back MM/DD/YYYY. (Leave Period Blank. Disable rule or Reset parameter after rule executes)" Value="" DataType="String"/>
	<Param Name="@minIndCount" Alias="Minimum transaction count" Value="0" DataType="Int"/>
	<Param Name="@minIndAmt" Alias="Minimum individual transaction amount" Value="0" DataType="Money"/>
	<Param Name="@maxIndAmt" Alias="Maximum individual transaction amount. For no maximum use -1" Value="-1" DataType="Money"/>
	<Param Name="@minSumAmount" Alias="Minimum aggregated transaction amount."  Value="0" DataType="Money"/>
	<Param Name="@maxSumAmount" Alias="Maximum aggregated transaction amount. For no maximum use -1"  Value="-1" DataType="Money"/>
	<Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@accountTypeList" Alias="List of Account Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@countryList" Alias="List of Countries separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@recvpay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both" Value="1" DataType="Int"/>
	<Param Name="@branchList" Alias="List of Branches separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
	<Param Name="@deptList" Alias="List of Departments separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
	<Param Name="@cashType" Alias="Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both" Value="0" DataType="Int"/>
	<Param Name="@NumBene" Alias="Minimum number of beneficiaries" Value="1" DataType="Int"/>
	<Param Name="@RemoveSpecialChar" Alias="Remove Special/Wildcard characters in ByOrder and Beneficiary Name.(0 for No, 1 for Yes)(See rule description for more details)" Value="0" DataType="Int"/>
</Params>',
IsUserDefined = 0, CreateType = 0, IsLicensed = 0, LastOper = 'Prime', UseSysDate=1, OverrideExemption=0
WHERE WLCode = '1OrgMulBene'
END
Go

/******************************************************************************
Procedure Name: USR_OneByOrderToMulBene
Description:Detects a single Originator sending transactions to one or more 
Beneficiaries. A transaction is included in the evaluation based on its 
qualification with: specified transaction types, a specified time period,
the amount exceeds a certain amount, and the cumulative amount of all the
transactions meeting these constraints is over a specified amount.  
Additional constraints include Country, Customer Type, Risk Class, 
Account Types, Cash/Non-Cash, Branch and Department. The alert/case
will display the account information in the description since the account
may not be held at the processing institution. Designed to be run Post-EOD.

Parameters:		@WLCode [in] SCode = Rule Code				
				[Return] INT = Return the error status.  0 for success.
******************************************************************************/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_OneByOrderToMulBene]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.USR_OneByOrderToMulBene
Go
CREATE PROCEDURE dbo.USR_OneByOrderToMulBene(@WLCode SCode, @testAlert INT,
	@period varchar(25),
	@startDate varchar(10),
	@endDate varchar(10),  
    @minIndCount int,
	@minIndAmt money, 
    @maxIndAmt money,
	@minSumAmount money,
	@maxSumAmount money,
	@riskClassList varchar(8000),
	@customerTypeList varchar(8000),
	@accountTypeList varchar(8000),
	@activityTypeList varchar(8000),
    @countryList varchar(8000),
	@recvpay INT,
	@branchList varchar(8000),
    @deptList varchar(8000),
	@cashType smallint,
	@NumBene smallint,
	@RemoveSpecialChar smallint =0
	)

AS
/* RULE AND PARAMETER DESCRIPTION
Detects a single Originator sending transactions to one or more 
Beneficiaries. A transaction is included in the evaluation based on its 
qualification with: specified transaction types, a specified time period,
the amount exceeds a certain amount, and the cumulative amount of all the
transactions meeting these constraints is over a specified amount.  
Additional constraints include Country, Customer Type, Risk Class, 
Account Types, Cash/Non-Cash, Branch and Department. The alert/case
will display the account information in the description since the account
may not be held at the processing institution. Designed to be run Post-EOD.

	@period = Specifies the previous period accordingly. It Can be left blank,
		Week, Month, Quarter and Semi Annual. If blank the StartDate and EndDate 
  		must be specified. 
	@startDate = Specifies the Starting Date range of transactions to evaluate,  
    		uses the book date. Relevant only if the Period is not specified.
	@endDate = Specifies the Ending Date range of the transaction’s book date.
		Relevant only if the Period is not specified.
    	@minIndCount = The Minimum individual transaction count, after applying 
		the Min-Ind-Amt and Max-Ind-Amt parameters
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
   	@maxIndAmt = The Maximum amount the individual transaction cannot exceed 
		to be included in the aggregation.  For no maximum use -1
	@minSumAmount = The Minimum aggregated transaction amount that must be 
		exceeded.  This includes those transactions that are between Min-Ind-Amt
		and Max-Ind-Amt.
	@maxSumAmount = The Maximum aggregated transaction amount that cannot be 
		exceeded. For no maximum use -1
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@customerTypeList = A comma separated list of Customer Types to include 
		in the evaluation, use -ALL- for any customer type.
	@accountTypeList = A comma separated list of Account Types to include 
		in the evaluation, use -ALL- for any account type.
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.
    	@countryList = A comma separated list of Countries to include in the 
		evaluation, use -ALL- for any country. Fields evaluated are 
		BeneCountry and ByOrderCountry.
	@recvpay = Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both
	@branchList = A comma separated list of Branches to include in the evaluation.
	@deptList = A comma separated list of Departments to include in 
		the evaluation.
	@cashType = Specifies Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both
	@NumBene - Number of Beneficiaries. Alert is generated only if payments are sent to specified number or more Beneficiaries.
	@RemoveSpecialChar = Rule is configured Remove Special/Wildcard characters in the Name Field.

*/
	
/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@fromDate INT,
 	@toDate INT,
	@tranAmt MONEY,
	@tranCnt INT,
	@trnCnt INT,
	@Cust VarChar(35),
	@OwnerBranch VarChar(11),
	@OwnerDept VarChar(11)
DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME

DECLARE @SetDate DATETIME

DECLARE @TransTbl TABLE (
	TranNo INT,
	Bene VARCHAR(40),
	ByOrder VARCHAR(40),
	BookDate INT,
	BaseAmt MONEY,
	Cust VARCHAR(40),
	OwnerBranch VARCHAR(11),
	OwnerDept VARCHAR(11)
)

DECLARE	@TT TABLE (
	tranAmt MONEY,
	tranCnt INT,
	byOrder  VARCHAR(40),
	CustCount INT,
	BranchCount INT,
	DeptCount INT,
	Cust VARCHAR(40),
	OwnerBranch VARCHAR(11),
	OwnerDept VARCHAR(11)
)

DECLARE @CustomerExemptionsList VARCHAR(8000)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_OneByOrderToMulBene
ELSE
	-- Already in a transaction
	SAVE TRAN USR_OneByOrderToMulBene
/* End standard stored procedure transaction header */

/*  standard Rules Header */
-- Date options

-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @description = [Desc], @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList (NOLOCK)
WHERE WLCode = @WLCode

Declare @BaseCurr char(3)
select @BaseCurr = IsNULL(BaseCurr,'') from SysParam

If(@period is null OR  ltrim(rtrim(@period)) = '') 
BEGIN
	SET @fromDate = dbo.ConvertSqlDateToInt(@startDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@endDate)
END
ELSE
BEGIN
	DECLARE @SQLStartDate datetime, @SQLEndDate datetime
	exec dbo.BSA_GetDateRange @SetDate, @period, 'PREV', 1, 
			@SQLStartDate OUTPUT, @SQLEndDate OUTPUT
	SET @fromDate = dbo.ConvertSqlDateToInt(@SQLStartDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@SQLEndDate)
END

SELECT @riskClassList = dbo.BSA_fnListParams(@riskClassList)
SELECT @customerTypeList = dbo.BSA_fnListParams(@customerTypeList)
SELECT @accountTypeList = dbo.BSA_fnListParams(@accountTypeList)
SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)
SELECT @countryList = dbo.BSA_fnListParams(@countryList)
SELECT @branchList = dbo.BSA_fnListParams(@branchList)
SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)

SELECT @CustomerExemptionsList = dbo.BSA_GetExemptionCodes('Customer','Rule')
IF ltrim(rtrim(@CustomerExemptionsList)) = ''
	SET @CustomerExemptionsList = NULL

IF (@cashType = 2)
	SET @cashType = NULL

IF (@recvpay = 3)
	SET @recvpay = NULL

Insert Into @TransTbl(TranNo, Bene, ByOrder, BookDate, BaseAmt, Cust, OwnerBranch, OwnerDept)
Select TranNo, 
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(Bene) ELSE Bene END AS Bene, 
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(ByOrder) ELSE ByOrder END AS ByOrder, 
BookDate, BaseAmt, Cust, a.Branch, Dept
FROM ActivityHist a (NOLOCK)
Join Customer (NOLOCK) ON 	a.cust = Customer.Id
Left Join Account Ac (NOLOCK) ON a.Account = ac.Id
WHERE  	a.bookdate >= @fromDate AND 
	a.bookdate <= @toDate AND 
	a.recvpay = ISNULL(@recvPay, a.recvpay) AND
	a.BaseAmt >= @minIndAmt AND
    (@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
	a.byOrder IS NOT NULL and 
	a.bene IS NOT NULL AND  
	(@deptList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR,  a.dept) + ',',@deptList) > 0) AND
	(@branchList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR,  a.branch) + ',', @branchList) > 0) AND 
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
	(@accountTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, ac.type) + ',',@accountTypeList) > 0) AND
	(@countryList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, a.BeneCountry) + ',',@countryList) > 0 OR
			CHARINDEX(',' + 
				CONVERT(VARCHAR, a.ByOrderCountry) + ',',@countryList) > 0 ) AND
	(@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0) AND
	(@customerTypeList IS NULL OR
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.type)) + ',', @customerTypeList ) > 0) AND
	(Customer.Exemptionstatus IS NULL OR @CustomerExemptionsList IS NULL OR
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.Exemptionstatus)) + ',', @CustomerExemptionsList ) = 0) AND
	(CASHTRAN = ISNULL(@cashType, CASHTRAN))


INSERT INTO @tt (tranAmt, tranCnt, byOrder, CustCount, BranchCount, DeptCount)
SELECT DISTINCT SUM(BaseAmt) tranAmt, COUNT(tranNo) tranCnt, a.byOrder, 
       Count(Distinct Cust), Count(Distinct OwnerBranch), Count(Distinct OwnerDept)
FROM @TransTbl a 
GROUP BY  a.byOrder
HAVING (SUM(BaseAmt) >= @minSumAmount AND 
	(@maxSumAmount = -1 OR SUM(BaseAmt) <= @maxSumAmount) AND
	count(tranno) >= @minIndCount) AND 
	COUNT(DISTINCT a.Bene) >= @NumBene

Update tt Set Cust = trn.Cust
From @tt tt Join @TransTbl trn 
On tt.ByOrder = trn.ByOrder
Where CustCount = 1

Update tt Set OwnerBranch = trn.OwnerBranch
From @tt tt Join @TransTbl trn 
On tt.ByOrder = trn.ByOrder
Where CustCount > 1 AND BranchCount = 1

Update tt Set OwnerDept = trn.OwnerDept
From @tt tt Join @TransTbl trn 
On tt.ByOrder = trn.ByOrder
Where CustCount > 1 AND DeptCount = 1

DECLARE	@cur CURSOR
DECLARE @byOrder LONGNAME,
	@bookdate INT

SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())

SET @cur = CURSOR FAST_FORWARD FOR 
SELECT a.tranAmt, a.tranCnt, a.byOrder, a.Cust, a.OwnerBranch, a.OwnerDept
FROM @tt a 

OPEN @cur 
FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @byOrder, @cust, @OwnerBranch, @Ownerdept

WHILE @@FETCH_STATUS = 0 BEGIN

	SET @desc = @byOrder + '(ByOrder) is sending to '
		+ 'multiple bene ' 
		+ 'amount: ' + DBO.BSA_InternalizationMoneyToString(@tranAmt) + Space(1) + @BaseCurr + ' over '
		+ CONVERT(VARCHAR, @tranCnt) + ' transactions'
		+ ' over a period from ' + Cast(dbo.BSA_InternalizationIntToShortDate(@fromDate) AS Char(11)) + ' to ' 
		+ Cast(dbo.BSA_InternalizationIntToShortDate(@toDate) AS Char(11))

	IF @testAlert = 1
	BEGIN
		EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
		@Cust, NULL, 1
		IF @stat <> 0 GOTO EndOfProc
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 
		BEGIN
			EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
				@Cust, NULL, 0
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE 
		IF @WLTYPE = 1 
		BEGIN
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
				ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
				RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
				ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
				EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
				OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
				REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
				WLCode, WLDESC, CREATETIME, OwnerBranch, OwnerDept )
				SELECT  NULL, DBO.CONVERTSQLDATETOINT(GETDATE()), @Cust, NULL,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, @desc, GETDATE(), @OwnerBranch, @OwnerDept
			SELECT @STAT = @@ERROR, @ID = @@IDENTITY
			IF @stat <> 0 GOTO EndOfProc   	
		END	
	END		
	IF (@WLTYPE = 0) OR (@testAlert = 1)
	BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'Alert', @ID, TRANNO
			from @TransTbl t 
			WHERE  t.byOrder = @byOrder
	
			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC
	END 
	ELSE 
	IF @WLTYPE = 1 
	BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 'SUSPACT', @ID, TRANNO 
				from @TransTbl t 
				WHERE  t.byOrder = @byOrder

			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC 
	END
	FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @byOrder, @cust, @OwnerBranch, @Ownerdept
END

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_OneByOrderToMulBene
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_OneByOrderToMulBene
RETURN @stat
Go

--USR_SameBeneByOrd
IF NOT EXISTS (SELECT * FROM Watchlist 
WHERE WLCode = 'SameBeneOrd') BEGIN
insert into Watchlist (WLCode, Title, [Desc], WLType, SPName, SuspType, 
Schedule, IsPreEOD, OwnerBranch, OwnerDept, OwnerOper, CreateOper, CreateDate, Params, IsUserDefined, CreateType,  IsLicensed)
values ('SameBeneOrd', 'Detect repeated transactions to the same beneficiary or ByOrder',
'Detects transactions where a Beneficiary or ByOrder either 
sends or receives transactions using a specified activity type passing 
through countries in a specified region and the dollar amount for each of these 
transactions is within a designated range and the number of these transactions 
is greater than a specified count and the cumulative amount exceeds a certain 
value. Designed to be run Pre-EOD. If the option to remove special/wildcard 
characters is enabled, then all non-alpha numeric characters other than spaces 
will be removed for name matching in Beneficiary and ByOrder name fields. 
For example X.Y.Z Corp become XYZ Corp for matching purpose.',
1, 'USR_SameBeneByOrd', 'Rule', 2, 1, Null, Null, Null,
'Prime', GetDate(),'<Params>
<Param Name="@activityTypes" Alias="Activity Types" Value="101,102,103" DataType="String"/>
<Param Name="@branches" Alias="List of Branches" Value="NY" DataType="String"/>
<Param Name="@depts" Alias="List of Departments" Value="SEC" DataType="String"/>
<Param Name="@region" Alias="Region" Value="Americas" DataType="String"/>
<Param Name="@minimumAmount" Alias="Minimum Transaction Amount" Value="10" DataType="Money"/>
<Param Name="@maximumAmount" Alias="Maximum Transaction Amount" Value="1000" DataType="Money"/>
<Param Name="@minimumCount" Alias="Minimum number of transactions" Value="2" DataType="Int"/>
<Param Name="@minimumTotalAmount" Alias="Minimum Total Amount" Value="100" DataType="Money"/>
<Param Name="@days" Alias="Number of Calendar Days" Value="7" DataType="Int"/>
<Param Name="@weekends" Alias="Days to be Considered Weekends" Value="Sat,Sun" DataType="String"/>
<Param Name="@RemoveSpecialChar" Alias="Remove Special/Wildcard characters in Beneficiary and ByOrder Name.(0 for No, 1 for Yes)(See rule description for more details)" Value="0" DataType="Int"/>
</Params>', 0, 0, 0)
END ELSE BEGIN
UPDATE Watchlist SET 
[Desc]='Detects transactions where a Beneficiary or ByOrder either 
sends or receives transactions using a specified activity type passing 
through countries in a specified region and the dollar amount for each of these 
transactions is within a designated range and the number of these transactions 
is greater than a specified count and the cumulative amount exceeds a certain 
value. Designed to be run Pre-EOD. If the option to remove special/wildcard 
characters is enabled, then all non-alpha numeric characters other than 
spaces will be removed for name matching in Beneficiary and ByOrder name fields. 
For example X.Y.Z Corp become XYZ Corp for matching purpose.',
Params='<Params>
<Param Name="@activityTypes" Alias="Activity Types" Value="101,102,103" DataType="String"/>
<Param Name="@branches" Alias="List of Branches" Value="NY" DataType="String"/>
<Param Name="@depts" Alias="List of Departments" Value="SEC" DataType="String"/>
<Param Name="@region" Alias="Region" Value="Americas" DataType="String"/>
<Param Name="@minimumAmount" Alias="Minimum Transaction Amount" Value="10" DataType="Money"/>
<Param Name="@maximumAmount" Alias="Maximum Transaction Amount" Value="1000" DataType="Money"/>
<Param Name="@minimumCount" Alias="Minimum number of transactions" Value="2" DataType="Int"/>
<Param Name="@minimumTotalAmount" Alias="Minimum Total Amount" Value="100" DataType="Money"/>
<Param Name="@days" Alias="Number of Calendar Days" Value="7" DataType="Int"/>
<Param Name="@weekends" Alias="Days to be Considered Weekends" Value="Sat,Sun" DataType="String"/>
<Param Name="@RemoveSpecialChar" Alias="Remove Special/Wildcard characters in Beneficiary and ByOrder Name.(0 for No, 1 for Yes)(See rule description for more details)" Value="0" DataType="Int"/>
</Params>',
LastOper = 'Prime'
where WLCode = 'SameBeneOrd'
END
Go

/******************************************************************************
Procedure Name: USR_SameBeneByOrd
Description:	Detects transactions where a Beneficiary or ByOrder either 
sends or receives transactions using a specified activity type passing 
through countries in a specified region and the dollar amount for each of these 
transactions is within a designated range and the number of these transactions 
is greater than a specified count and the cumulative amount exceeds a certain 
value. Designed to be run Pre-EOD.

Parameters:		
        @WLCode SCode = Rule Code	
	@testAlert = Whether the alert is being tested			
	@activityTypes = Types of activity to be checked (comma-separated)
	@branches = Branches to be checked (comma-separated)
	@depts = Departments to be checked (comma-separated)
	@region = Region code to which the country must belong
	@minimumAmount = Threshold amt
	@maximumAmount = Maximum amount
	@minimumCount = Threshold number of transactions
	@minimumTotalAmount = Threshold cumulative amount
	@days = Number of business days to check
	@weekends = Days of the week to consider as weekends (comma-separated)
	@RemoveSpecialChar = Rule is configured to Remove Special/Wildcard characters in the Name Field.

[Return] INT = Return the error status.  0 for success.
******************************************************************************/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_SameBeneByOrd]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.USR_SameBeneByOrd
Go
CREATE PROCEDURE dbo.USR_SameBeneByOrd (@WLCode SCode, @testAlert INT, 
        @activityTypes VARCHAR(8000), @branches VARCHAR(8000), 
        @depts VARCHAR(8000), @region SCode, @minimumAmount MONEY, 
        @maximumAmount MONEY, @minimumCount INT, @minimumTotalAmount MONEY, 
        @days INT, @weekends VARCHAR(8000),@RemoveSpecialChar smallint =0)
		
AS

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@MINDATE INT
DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME

DECLARE @TransTbl TABLE (
	TranNo INT,
	Bene VARCHAR(40),
	ByOrder VARCHAR(40),
	BookDate INT,
	BaseAmt MONEY,
	Cust VARCHAR(40),
	OwnerBranch VARCHAR(11),
	OwnerDept VARCHAR(11)
)
DECLARE	@TT TABLE (
	Bene VARCHAR(40),
	ByOrder VARCHAR(40),
	tranAmt money,
	tranCnt INT,
	descr VARCHAR(2000),
	CustCount INT,
	BranchCount INT,
	DeptCount INT,
	Cust VARCHAR(40),
	OwnerBranch VARCHAR(11),
	OwnerDept VARCHAR(11)
)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_SameBeneByOrd
ELSE
	-- Already in a transaction
	SAVE TRAN USR_SameBeneByOrd
/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @description = [Desc], @WLType = WLType  
FROM WatchList WITH (NOLOCK) WHERE WLCode = @WLCode

SET @minDate = DBO.CONVERTSQLDATETOINT(DATEADD(day, -1 * dbo.calcWeekendRange(
 @days, ',' + @weekends, 
 (datepart(weekday, getdate()) + @@DATEFIRST - 1) % 7 + 1) + 1, GETDATE()))
INSERT INTO @TransTbl(TranNo, Bene, ByOrder, BookDate, BaseAmt, Cust, OwnerBranch, OwnerDept)
SELECT TranNo, 
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(Bene) ELSE Bene END AS Bene,
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(byOrder) ELSE byOrder END AS ByOrder,
BookDate, BaseAmt, Cust, Branch, Dept
FROM Activity a WITH (NOLOCK)
WHERE (BaseAmt BETWEEN @minimumAmount AND @maximumAmount OR
       (@maximumAmount = 0 AND BaseAmt >= @minimumAmount)) AND
      (Bene IS NOT NULL OR ByOrder IS NOT NULL) AND
      BookDate >= @minDate AND (IsNull(@activityTypes, '') = '' OR 
      CHARINDEX(',' + CONVERT(VARCHAR, type) + ',', 
       ',' + @activityTypes + ',') > 0) AND
      (IsNull(@branches, '') = '' OR 
      CHARINDEX(',' + CONVERT(VARCHAR, branch) + ',', 
       ',' + @branches + ',') > 0) AND 
      (IsNull(@depts, '') = '' OR 
      CHARINDEX(',' + CONVERT(VARCHAR, dept) + ',', ',' + @depts + ',') > 0)
      AND (IsNull(@region, '') = '' OR EXISTS (
        SELECT * FROM Country WHERE region = @region AND
        (Code = a.BeneCountry OR Code = a.BeneBankCountry OR 
         Code = a.IntermediaryCountry OR Code = a.ByOrderCountry OR
         Code = a.ByOrderBankCountry OR Code = a.IntermediaryCountry2 OR 
         Code = a.IntermediaryCountry3 OR Code = a.IntermediaryCountry4)
      ))
IF @@ROWCOUNT = 0 GOTO EndOfProc
INSERT INTO @TransTbl(TranNo, Bene, ByOrder, BookDate, BaseAmt, Cust, OwnerBranch, OwnerDept)
SELECT TranNo, 
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(Bene) ELSE Bene END AS Bene,
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(byOrder) ELSE byOrder END AS ByOrder,
BookDate, BaseAmt, Cust, Branch, Dept
FROM ActivityHist a WITH (NOLOCK)
WHERE (BaseAmt BETWEEN @minimumAmount AND @maximumAmount OR
       (@maximumAmount = 0 AND BaseAmt >= @minimumAmount)) AND
      BookDate >= @minDate AND (IsNull(@activityTypes, '') = '' OR 
      CHARINDEX(',' + CONVERT(VARCHAR, type) + ',', 
       ',' + @activityTypes + ',') > 0) AND
      (IsNull(@branches, '') = '' OR 
      CHARINDEX(',' + CONVERT(VARCHAR, branch) + ',', 
       ',' + @branches + ',') > 0) AND 
      (IsNull(@depts, '') = '' OR 
      CHARINDEX(',' + CONVERT(VARCHAR, dept) + ',', ',' + @depts + ',') > 0)
      AND (CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(Bene) ELSE Bene END in (SELECT bene FROM @TransTbl) 
       OR CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(byOrder) ELSE byOrder END in (SELECT ByOrder FROM @TransTbl))
	  --AND (bene in (SELECT bene FROM @TransTbl) 
      -- OR ByOrder in (SELECT ByOrder FROM @TransTbl))

      AND (IsNull(@region, '') = '' OR EXISTS (
        SELECT * FROM Country WHERE region = @region AND
        (Code = a.BeneCountry OR Code = a.BeneBankCountry OR 
         Code = a.IntermediaryCountry OR Code = a.ByOrderCountry OR
         Code = a.ByOrderBankCountry OR Code = a.IntermediaryCountry2 OR 
         Code = a.IntermediaryCountry3 OR Code = a.IntermediaryCountry4)
      ))
INSERT INTO @TT(Bene, ByOrder, tranAmt, tranCnt, descr, CustCount, BranchCount, DeptCount)
SELECT Bene, NULL, SUM(BaseAmt) tranAmt, COUNT(TranNo) tranCnt,
'Beneficiary: ''' + Bene + '''  received amount: ''' 
                  + CONVERT(VARCHAR, SUM(BaseAmt)) 
		  + ''' over ''' 
		  + CONVERT(VARCHAR, COUNT(TranNo)) +  ''''
                  + ' transactions in the region ''' + @region
                  + ''' in the past ' + CONVERT(VARCHAR, @days)
                  + ' days.' descr, Count(Distinct Cust), Count(Distinct OwnerBranch), Count(Distinct OwnerDept)
FROM @TransTbl
WHERE Bene IS NOT NULL
GROUP BY Bene
HAVING (SUM(BaseAmt) >= @minimumTotalAmount AND 
	count(tranno) >= @minimumCount)
UNION

SELECT NULL, ByOrder, SUM(BaseAmt) tranAmt, COUNT(TranNo) tranCnt,
'ByOrder: ''' + ByOrder + '''  sent amount: ''' 
                  + CONVERT(VARCHAR, SUM(BaseAmt)) 
		  + ''' over ''' 
		  + CONVERT(VARCHAR, COUNT(TranNo)) +  ''''
                  + ' transactions in the region ''' + @region
                  + ''' in the past ' + CONVERT(VARCHAR, @days)
                  + ' days.' descr, Count(Distinct Cust), Count(Distinct OwnerBranch), Count(Distinct OwnerDept)
FROM @TransTbl
WHERE ByOrder IS NOT NULL
GROUP BY ByOrder
HAVING (SUM(BaseAmt) >= @minimumTotalAmount AND 
	count(tranno) >= @minimumCount)

Update tt Set Cust = trn.Cust
From @tt tt Join @TransTbl trn 
On (tt.Bene = trn.Bene And tt.ByOrder Is Null) 
Or (tt.ByOrder = trn.ByOrder And tt.Bene Is Null) 
Where CustCount = 1

Update tt Set OwnerBranch = trn.OwnerBranch
From @tt tt Join @TransTbl trn 
On (tt.Bene = trn.Bene And tt.ByOrder Is Null) 
Or (tt.ByOrder = trn.ByOrder And tt.Bene Is Null) 
Where CustCount > 1 AND BranchCount = 1

Update tt Set OwnerDept = trn.OwnerDept
From @tt tt Join @TransTbl trn 
On (tt.Bene = trn.Bene And tt.ByOrder Is Null) 
Or (tt.ByOrder = trn.ByOrder And tt.Bene Is Null) 
Where CustCount > 1 AND DeptCount = 1

IF @WLTYPE = 0 OR @testAlert = 1 BEGIN
	SELECT @STARTALRTDATE = GETDATE()
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT, IsTest) 
          SELECT @WLCode, descr, 0, GETDATE(), NULL, NULL, Cust, NULL, 
                 @testAlert
	  FROM @TT 
	SELECT @STAT = @@ERROR	
	SELECT @ENDALRTDATE = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	  SELECT 'Alert', AlertNO, TRANNO FROM @TransTbl a, @TT t, Alert 
          WHERE Alert.[desc] = t.descr
           AND ((ISNULL(a.Bene, '') <> '' AND a.Bene = t.bene and charindex('Beneficiary:', Alert.[desc])>0
                  AND substring(Alert.[desc], 
                        charindex('Beneficiary:', Alert.[desc]) + len('Beneficiary:')+2, 
                        charindex('received amount', Alert.[desc]) - (charindex('Beneficiary:', Alert.[desc]) + len('Beneficiary:')+5)) = a.bene) 
				  OR
               (ISNULL(a.ByOrder, '') <> '' AND a.ByOrder = t.byOrder and charindex('Byorder:', Alert.[desc])>0
                  AND substring(Alert.[desc], 
                        charindex('Byorder:', Alert.[desc]) + len('byorder:')+2, 
                        charindex('sent amount', Alert.[desc]) - (charindex('Byorder:', Alert.[desc]) + len('byorder:')+5)) = a.ByOrder))
           AND Alert.WLCode = @WLCode
           AND Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
	
	SELECT @STAT = @@ERROR 

END ELSE IF @WLTYPE = 1 BEGIN
	SELECT @STARTALRTDATE = GETDATE()
	INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
		ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
		RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
		ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
		EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
		OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
		REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
		WLCode, WLDESC, CREATETIME, OwnerBranch, OwnerDept )
        SELECT  NULL, DBO.CONVERTSQLDATETOINT(GETDATE()), Cust, NULL,
		NULL, 'RULE', NULL, NULL, NULL, NULL,
		NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
		NULL, NULL, 0, 0, 0, 0, 
		NULL, NULL, NULL, NULL, 0, NULL, NULL,
		@WLCode, descr, GETDATE(), OwnerBranch, OwnerDept
	FROM @TT 
	SELECT @STAT = @@ERROR	
	SELECT @ENDALRTDATE = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC


	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	  SELECT 'SUSPACT', RECNO, TRANNO 
          FROM @TransTbl a, @TT t, SUSPICIOUSACTIVITY S 
	  WHERE S.wlDesc = t.descr
          AND ((ISNULL(a.Bene, '') <> '' and charindex('Beneficiary:', S.wldesc)>0
                  AND substring(S.wldesc, 
                        charindex('Beneficiary:', S.wldesc) + len('Beneficiary:')+2, 
                        charindex('received amount', S.wldesc) - (charindex('Beneficiary:', S.wldesc) + len('Beneficiary:')+5)) = a.bene) 
				  OR
               (ISNULL(a.ByOrder, '') <> '' and charindex('Byorder:', S.wldesc)>0
                  AND substring(S.wldesc, 
                        charindex('Byorder:', S.wldesc) + len('byorder:')+2, 
                        charindex('sent amount', S.wldesc) - (charindex('Byorder:', S.wldesc) + len('byorder:')+5)) = a.ByOrder))
           AND S.WLCode = @WLCode
           AND S.CREATETIME BETWEEN @STARTALRTDATE AND @ENDALRTDATE
	SELECT @STAT = @@ERROR 

END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_SameBeneByOrd
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_SameBeneByOrd
RETURN @stat
Go

--USR_SameByOrderAndBene
IF NOT EXISTS (SELECT * FROM Watchlist 
WHERE WLCode = 'BeneEQByOrd') BEGIN 
Insert into Watchlist (WLCode, Title, [Desc], WLType, SPName, SuspType,  
Schedule, IsPreEOD, OwnerBranch, OwnerDept, OwnerOper, CreateOper, 
CreateDate, Params, IsUserDefined, CreateType,  IsLicensed) 
values ('BeneEQByOrd', 'Detects transactions of specified activity type with ' + 
'same Beneficiary and ByOrder within an amount range', 
'Detects transactions of specified activity type, when the name in ' + 
'the beneficiary data field is the same as the name in the Byorder data field, '+  
'for amounts between the specified minimum amount and maximum amount. ' + 
'Additional constraints include risk class and customer type. Alerts are '+  
'grouped based on ByOrder. Designed to be run Pre-EOD. '+
'If the option to remove special/wildcard characters is enabled, then all non-alpha '+
'numeric characters other than spaces will be removed for name matching in '+
'ByOrder and Beneficiary name fields. For example X.Y.Z Corp become XYZ Corp for matching purpose.', 
 0 , 'USR_SameByOrderAndBene', 'Rule', 1, 
1, Null, Null, Null,'Prime', GETDATE(), 
'<Params>
<Param Name="@minIndAmt" Alias="Minimum individual transaction amount" Value="0" DataType="Money"/>
<Param Name="@maxIndAmt" Alias="Maximum individual transaction amount. For no maximum, use -1" Value="-1" DataType="Money"/>
<Param Name="@minCount" Alias="Minimum transaction count" Value="1" DataType="Int"/>
<Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
<Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
<Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
<Param Name="@RemoveSpecialChar" Alias="Remove Special/Wildcard characters in ByOrder and Beneficiary Name.(0 for No, 1 for Yes)(See rule description for more details)" Value="0" DataType="Int"/>
</Params>', 
0,0,0) 
END ELSE BEGIN 
UPDATE WATCHLIST 
SET 
Title = 'Detects transactions of specified activity type with ' + 
'same Beneficiary and ByOrder within an amount range', 
[Desc] = 'Detects transactions of specified activity type, when the name in ' + 
'the beneficiary data field is the same as the name in the Byorder data field, '+  
'for amounts between the specified minimum amount and maximum amount. ' + 
'Additional constraints include risk class and customer type. Alerts are '+  
'grouped based on ByOrder. Designed to be run Pre-EOD. '+
'If the option to remove special/wildcard characters is enabled, then all non-alpha '+
'numeric characters other than spaces will be removed for name matching in '+ 
'ByOrder and Beneficiary name fields. For example X.Y.Z Corp become XYZ Corp for matching purpose.', 
WLType = 0, SPName = 'USR_SameByOrderAndBene', SuspType = 'Rule', 
Schedule = 1, IsPreEOD = 1, 
OwnerOper = Null, OwnerBranch = Null, OwnerDept = Null, 
Params = 
'<Params>
<Param Name="@minIndAmt" Alias="Minimum individual transaction amount" Value="0" DataType="Money"/>
<Param Name="@maxIndAmt" Alias="Maximum individual transaction amount. For no maximum, use -1" Value="-1" DataType="Money"/>
<Param Name="@minCount" Alias="Minimum transaction count" Value="1" DataType="Int"/>
<Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
<Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
<Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
<Param Name="@RemoveSpecialChar" Alias="Remove Special/Wildcard characters in ByOrder and Beneficiary Name.(0 for No, 1 for Yes)(See rule description for more details)" Value="0" DataType="Int"/>
</Params>', 
IsUserDefined = 0, CreateType = 0, IsLicensed = 0, LastOper = 'Prime', LastModify = GETDATE()
WHERE WLCode = 'BeneEQByOrd'
END
Go
/******************************************************************************
Procedure Name: USR_SameByOrderAndBene
Description:Detects transactions of specified activity type, when the name in 
the beneficiary data field is the same as the name in the Byorder data field, 
for amounts between the specified minimum amount and maximum amount. 
Additional constraints include risk class and customer type. Alerts are 
grouped based on ByOrder. Designed to be run Pre-EOD.

Parameters:		
	@WLCode [in] SCode = Rule Code				
	@testAlert = Test Alert or Not (1 for Test, 0 for No)
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
   	@maxIndAmt = The Maximum amount the individual transaction cannot exceed 
		to be included in the aggregation.  For no maximum use -1	
	@minCount = The Minimum transaction count	
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.    	
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@customerTypeList = A comma separated list of Customer Types to include 
		in the evaluation, use -ALL- for any customer type
	@RemoveSpecialChar = Rule is configured to Remove Special/Wildcard characters in the Name Field.	
******************************************************************************/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_SameByOrderAndBene]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.USR_SameByOrderAndBene
Go
CREATE PROCEDURE dbo.USR_SameByOrderAndBene(@WLCode SCode, @testAlert INT,
	@minIndAmt money, 
	@maxIndAmt money, 
	@minCount int,
	@activityTypeList varchar(8000),
	@riskClassList varchar(8000),
	@customerTypeList varchar(8000),
	@RemoveSpecialChar smallint =0
	)
AS
/* RULE AND PARAMETER DESCRIPTION
Description:Detects transactions of specified activity type, when the name in 
the beneficiary data field is the same as the name in the Byorder data field, 
for amounts between the specified minimum amount and maximum amount. 
Additional constraints include risk class and customer type. Alerts are 
grouped based on ByOrder. Designed to be run Pre-EOD.

	Parameters:		
	@WLCode [in] SCode = Rule Code				
	@testAlert = Test Alert or Not (1 for Test, 0 for No)
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
   	@maxIndAmt = The Maximum amount the individual transaction cannot exceed 
		to be included in the aggregation.  For no maximum use -1	
	@minCount = The Minimum transaction count	
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.    	
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@customerTypeList = A comma separated list of Customer Types to include 
		in the evaluation, use -ALL- for any customer type.	
*/
	
/*  Declarations */
DECLARE	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@tranAmt MONEY,
	@activityCount INT,
	@currentTransactionCount INT

DECLARE	@TT TABLE (
	tranAmt MONEY,
	tranNo INT,
	byOrder  VARCHAR(40),
	bene VARCHAR(40)
)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE *********************************
/* Start standard stored procedure transaction header */
SET @currentTransactionCount = @@TRANCOUNT	-- Save the current trancount
IF @currentTransactionCount = 0
	-- Transaction has not begun
	BEGIN TRAN USR_SameByOrderAndBene
ELSE
	-- Already in a transaction
	SAVE TRAN USR_SameByOrderAndBene
/* End standard stored procedure transaction header */

SELECT @WLType = WLType      
	FROM dbo.WatchList (NOLOCK)
	WHERE WLCode = @WLCode

--Making the List variables Null, if value is '' or -ALL-
--Else Removing space and Trimming
IF (ISNULL(@riskClassList,'') = '' 
	OR UPPER(ISNULL(@riskClassList,'-ALL-')) = '-ALL-')
	SELECT @riskClassList = NULL
ELSE
	SELECT @riskClassList = ',' + 
		REPLACE(LTRIM(RTRIM(@riskClassList)),CHAR(32),'') + ','

IF(ISNULL(@customerTypeList,'') = '' 
	OR UPPER(ISNULL(@customerTypeList,'-ALL-')) = '-ALL-')
	SELECT @customerTypeList = NULL
ELSE
   	SELECT @customerTypeList = ',' + 
		REPLACE(LTRIM(RTRIM(@customerTypeList)),CHAR(32),'') + ','

IF (ISNULL(@activityTypeList,'') = '' 
	OR UPPER(ISNULL(@activityTypeList,'-ALL-')) = '-ALL-')
	SELECT @activityTypeList = NULL
ELSE
	SELECT @activityTypeList = ',' + 
		REPLACE(LTRIM(RTRIM(@activityTypeList)),CHAR(32),'') + ','

INSERT INTO @TT (tranAmt, tranNo, byOrder,bene)
SELECT a.BaseAmt, a.tranNo,
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(a.ByOrder) ELSE a.ByOrder END AS ByOrder,
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(a.bene) ELSE a.bene END aS bene
FROM Activity a (NOLOCK), Customer (NOLOCK)
WHERE a.cust = Customer.Id AND
	a.BaseAmt >= @minIndAmt AND
    (@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
	a.byOrder IS NOT NULL and 
	a.bene IS NOT NULL AND  
	1= CASE when @RemoveSpecialChar=1 
	THEN (case when dbo.BSA_FnRemoveSpecialCharacters(a.byOrder) = dbo.BSA_FnRemoveSpecialCharacters(a.bene) then 1 else 0 end)
	ELSE case when (a.byOrder = a.bene ) then 1 else 0 end
	END AND
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
	(@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0) AND
	(@customerTypeList IS NULL OR
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.type)) + ',', @customerTypeList ) > 0)
DECLARE	@cur CURSOR
DECLARE @byOrder LONGNAME,
	@bookdate INT

SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())

SET @cur = CURSOR FAST_FORWARD FOR 
SELECT SUM(tranAmt) tranAmt, COUNT(tranNo) tranCnt, a.byOrder
FROM @TT a GROUP BY  a.byOrder
HAVING count(tranno) >= @minCount

OPEN @cur 
FETCH NEXT FROM @cur INTO @tranAmt, @activityCount , @byOrder

WHILE @@FETCH_STATUS = 0 BEGIN

	SET @desc = @byOrder + '(ByOrder) is performing '
		+ CONVERT(VARCHAR, @activityCount ) + ' transaction(s) '  
		+ 'to self for a total amount of ' + CONVERT(VARCHAR, @tranAmt)
	IF @testAlert = 1
	BEGIN
		EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
		NULL, NULL, 1
		IF @stat <> 0 GOTO EndOfProc
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 --Alert
		BEGIN
			EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
				NULL, NULL, 0
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE 
		IF @WLTYPE = 1 --Case
		BEGIN
			EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT, 
				@WLCode, @desc, @bookdate, Null, NULL
			IF @stat <> 0 GOTO EndOfProc   	
		END	
	END		
	IF (@WLTYPE = 0) OR (@testAlert = 1)
	BEGIN
		--Inserting records into SASACTIVITY for the @ID
		--obtained from API_InsAlert stored Procedure
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'Alert', @ID, TRANNO 
			FROM @TT t
			WHERE  t.byOrder = @byOrder
	
			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC
	END 
	ELSE 
	IF @WLTYPE = 1 --Case
	BEGIN
		--Inserting records into SASACTIVITY for the @ID
		--obtained from API_InsSuspiciosActivity stored Procedure
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'SUSPACT', @ID, TRANNO 
				FROM @TT t
			WHERE  t.byOrder = @byOrder

			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC 
	END
	FETCH NEXT FROM @cur INTO @tranAmt, @activityCount , @byOrder
END --Cursor While loop Ends

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_SameByOrderAndBene
  RETURN @stat
END	

IF @currentTransactionCount = 0
  COMMIT TRAN USR_SameByOrderAndBene
RETURN @stat
Go

--USR_SameByOrderBeneAndSameAmt
IF NOT EXISTS (SELECT * FROM Watchlist 
WHERE WLCode = 'BneEQOrgAmt') BEGIN 
Insert into Watchlist (WLCode, Title, [Desc], WLType, SPName, SuspType,  
Schedule, IsPreEOD, OwnerBranch, OwnerDept, OwnerOper, CreateOper, 
CreateDate, Params, IsUserDefined, CreateType,  IsLicensed, UseSysDate) 
values ('BneEQOrgAmt', 'Detects any activity where Beneficiary and ByOrder ' +
'party are the same and the transaction amount is the same in N days', 
'Detects activity when the name in the beneficiary data field is the same as ' +
'the name in the by order data field when transactions in the same amount occur ' +
'in a specified time period and the amount exceeds the specified minimum amount.' +
'Checks for activity on current day and looks back for activity for the specified ' +
'period. Alerts are grouped based on ByOrder and Beneficiary, Amount. Designed to be run Pre-EOD. '+
'If the option to remove special/wildcard characters is enabled, then all non-alpha '+
'numeric characters other than spaces will be removed for name matching in '+
'ByOrder and Beneficiary name fields. For example X.Y.Z Corp become XYZ Corp for matching purpose.',
 0 , 'USR_SameByOrderBeneAndSameAmt', 'Rule', 1, 1, Null, Null, Null,'Prime', GETDATE(), 
'<Params>
<Param Name="@minIndAmt" Alias="Minimum individual transaction amount" Value="0" DataType="Money"/>
<Param Name="@minCount" Alias="Minimum transaction count" Value="1" DataType="Int"/>
<Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
<Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
<Param Name="@days" Alias="Number of days to look back(Calendar Days)" Value="7" DataType="Int"/>
<Param Name="@RemoveSpecialChar" Alias="Remove Special/Wildcard characters in ByOrder and Beneficiary Name.(0 for No, 1 for Yes)(See rule description for more details)" Value="0" DataType="Int"/>
</Params>', 
0,0,0,1) 
END ELSE BEGIN 
UPDATE WATCHLIST 
SET 
Title ='Detects any activity where Beneficiary and ByOrder ' +
'party are the same and the transaction amount is the same in N days', 
[Desc] = 'Detects activity when the name in the beneficiary data field is the ' +
'same as the name in the by order data field when transactions in the same amount occur ' +
'in a specified time period and the amount exceeds the specified minimum amount.' +
'Checks for activity on current day and looks back for activity for the specified ' +
'period. Alerts are grouped based on ByOrder and Beneficiary, Amount. Designed to be run Pre-EOD. ' +
'If the option to remove special/wildcard characters is enabled, then all non-alpha '+
'numeric characters other than spaces will be removed for name matching in '+ 
'ByOrder and Beneficiary name fields. For example X.Y.Z Corp become XYZ Corp for matching purpose.', 
WLType = 0, SPName = 'USR_SameByOrderBeneAndSameAmt', SuspType = 'Rule',
Schedule = 1, IsPreEOD = 1, 
OwnerOper = Null, OwnerBranch = Null, OwnerDept = Null, 
Params = 
'<Params>
<Param Name="@minIndAmt" Alias="Minimum individual transaction amount" Value="0" DataType="Money"/>
<Param Name="@minCount" Alias="Minimum transaction count" Value="1" DataType="Int"/>
<Param Name="@activityTypeList" Alias="List of Activity Types separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
<Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
<Param Name="@days" Alias="Number of days to look back(Calendar Days)" Value="7" DataType="Int"/>
<Param Name="@RemoveSpecialChar" Alias="Remove Special/Wildcard characters in ByOrder and Beneficiary Name.(0 for No, 1 for Yes)(See rule description for more details)" Value="0" DataType="Int"/>
</Params>', 
IsUserDefined = 0, CreateType = 0, IsLicensed = 0, LastOper = 'Prime', LastModify = GETDATE(), UseSysDate=1
WHERE WLCode = 'BneEQOrgAmt'
END
Go

/******************************************************************************
Procedure Name: USR_SameByOrderBeneAndSameAmt
Description:Detects activity when the name in the beneficiary data field is the
same as the name in the by order data field when transactions in the same 
amount occur in a specified time period and the amount exceeds the specified 
minimum amount. Checks for activity on current day and looks back for activity
for the specified period. Alerts are grouped based on ByOrder, Amount. 
Designed to be run Pre-EOD.

Parameters:		
	@WLCode [in] SCode = Rule Code				
	@testAlert = Test Alert or Not (1 for Test, 0 for No)
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
   	@minCount = The Minimum transaction count	
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.    	
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@days = Number of days to look back	(Calendar Days)	
	@RemoveSpecialChar = Rule is configured to Remove Special/Wildcard characters in the Name Field.
******************************************************************************/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_SameByOrderBeneAndSameAmt]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.USR_SameByOrderBeneAndSameAmt
Go
CREATE PROCEDURE dbo.USR_SameByOrderBeneAndSameAmt(@WLCode SCode, @testAlert INT,
	@minIndAmt money, 
	@minCount int,
	@activityTypeList varchar(8000),
	@riskClassList varchar(8000),
	@days int,
	@RemoveSpecialChar smallint =0
	)
AS
/* RULE AND PARAMETER DESCRIPTION
Description:Detects activity when the name in the beneficiary data field is the
same as the name in the by order data field when transactions in the same 
amount occur in a specified time period and the amount exceeds the specified 
minimum amount. Checks for activity on current day and looks back for activity
for the specified period. Alerts are grouped based on ByOrder, Amount. 
Designed to be run Pre-EOD.


Parameters:		
	@WLCode [in] SCode = Rule Code				
	@testAlert = Test Alert or Not (1 for Test, 0 for No)
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
   	@minCount = The Minimum transaction count	
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.    	
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@days = Number of days to look back	(Calendar Days)
	@RemoveSpecialChar = Rule is configured to Remove Special/Wildcard characters in the ByOrder Name Field.
*/
	
/*  Declarations */
DECLARE	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@tranAmt MONEY,
	@activityCount  INT,
	@currentTransactionCount INT,
	@IndAmount  MONEY,
	@fromDate INT

DECLARE @SetDate DATETIME
DECLARE	@TT TABLE (
	tranAmt MONEY,
	tranNo INT,
	byOrder  VARCHAR(40) ,
	bene VARCHAR(40)
	
)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @currentTransactionCount = @@TRANCOUNT	-- Save the current trancount
IF @currentTransactionCount = 0
	-- Transaction has not begun
	BEGIN TRAN USR_SameByOrderBeneAndSameAmt
ELSE
	-- Already in a transaction
	SAVE TRAN USR_SameByOrderBeneAndSameAmt
/* End standard stored procedure transaction header */

-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList (NOLOCK)
WHERE WLCode = @WLCode

--Making the List variables Null, if value is '' or -ALL-
--Else Removing space and Trimming
IF (ISNULL(@riskClassList,'') = '' 
	OR UPPER(ISNULL(@riskClassList,'-ALL-')) = '-ALL-')
	SELECT @riskClassList = NULL
ELSE
	SELECT @riskClassList = ',' + 
		REPLACE(LTRIM(RTRIM(@riskClassList)),CHAR(32),'') + ','

IF (ISNULL(@activityTypeList,'') = '' 
	OR UPPER(ISNULL(@activityTypeList,'-ALL-')) = '-ALL-')
	SELECT @activityTypeList = NULL
ELSE
	SELECT @activityTypeList = ',' + 
		REPLACE(LTRIM(RTRIM(@activityTypeList)),CHAR(32),'') + ','

--Subtracting 1, to look at @days-1 days Transactions from ActivityHist Table
--and 1 day from Activity Table
SELECT @days = ABS(@days) - 1

SET @fromDate = dbo.ConvertSqlDateToInt(
	DATEADD(d, -1 * @days, CONVERT(VARCHAR, @SetDate))) 

--Exists clause is used to pick from history, only if corresponding 
--record(s) present in Activity

INSERT INTO @TT (tranAmt, tranNo, byOrder,bene)
SELECT a.BaseAmt, a.tranNo,
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(a.byOrder) ELSE a.byOrder END AS byOrder,
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(a.bene) ELSE a.bene END as bene
FROM Activity a (NOLOCK), Customer (NOLOCK)
WHERE a.cust = Customer.Id AND
	a.bookdate >= @fromDate AND
	a.BaseAmt >= @minIndAmt AND
    a.byOrder IS NOT NULL and 
	a.bene IS NOT NULL AND  
	1 = CASE when @RemoveSpecialChar=1 
	THEN (case when dbo.BSA_FnRemoveSpecialCharacters(a.byOrder) = dbo.BSA_FnRemoveSpecialCharacters(a.bene) then 1 else 0 end)
	ELSE case when (a.byOrder = a.bene ) then 1 else 0 end
	END AND
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
	(@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0)
Union
SELECT ah.BaseAmt, ah.tranNo,
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(ah.byOrder) ELSE ah.byOrder END as byOrder,
CASE WHEN @RemoveSpecialChar =1 THEN dbo.BSA_FnRemoveSpecialCharacters(ah.bene) ELSE ah.bene END as bene
FROM ActivityHist ah (NOLOCK), Customer (NOLOCK)
WHERE ah.cust = Customer.Id AND
	ah.bookdate >= @fromDate AND 
	ah.BaseAmt >= @minIndAmt AND
    ah.byOrder IS NOT NULL and 
	ah.bene IS NOT NULL AND  
	1= CASE when @RemoveSpecialChar=1 
	THEN (case when dbo.BSA_FnRemoveSpecialCharacters(ah.byOrder) = dbo.BSA_FnRemoveSpecialCharacters(ah.bene) then 1 else 0 end)
	ELSE case when (ah.byOrder = ah.bene ) then 1 else 0 end
	END AND
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, ah.type) + ',',@activityTypeList) > 0) AND
	(@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0) AND
	Exists
	(SELECT a.tranNo
		FROM Activity a (NOLOCK), Customer (NOLOCK)
		WHERE
		a.bookdate >= @fromDate AND 
		a.Bene = ah.Bene AND
		a.ByOrder = ah.ByOrder AND
		a.cust = Customer.Id AND
		a.BaseAmt >= @minIndAmt AND
		a.baseamt=ah.baseamt AND
	    a.byOrder IS NOT NULL and 
		a.bene IS NOT NULL AND  
		a.byOrder = a.bene AND
		(@activityTypeList IS NULL OR 
			CHARINDEX(',' + 
				CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
		(@riskClassList IS NULL OR 
			CHARINDEX(',' + 
				LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0)
	)
DECLARE	@cur CURSOR
DECLARE @byOrder LONGNAME,
		@bookdate INT

SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())

SET @cur = CURSOR FAST_FORWARD FOR 
SELECT SUM(tranAmt) tranAmt, COUNT(tranNo) tranCnt, a.byOrder, a.TranAmt
FROM @TT a GROUP BY  a.byOrder, a.tranAmt
HAVING count(tranno) >= @minCount

OPEN @cur 
FETCH NEXT FROM @cur INTO @tranAmt, @activityCount , @byOrder, @IndAmount

WHILE @@FETCH_STATUS = 0 BEGIN

	SET @desc = @byOrder + '(ByOrder) is performing '
		+ CONVERT(VARCHAR, @activityCount ) + ' transaction(s) '  
		+ 'to self with the same amount ' + CONVERT(VARCHAR, @IndAmount)
		+ ' where the total amount is ' + CONVERT(VARCHAR, @tranAmt)
		+ ' over a period of ' + CONVERT(VARCHAR, @days + 1) + ' days'
	IF @testAlert = 1
	BEGIN
		EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
		NULL, NULL, 1
		IF @stat <> 0 GOTO EndOfProc
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 --Alert
		BEGIN
			EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
				NULL, NULL, 0
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE 
		IF @WLTYPE = 1 --Case
		BEGIN
			EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT, 
				@WLCode, @desc, @bookdate, Null, NULL
			IF @stat <> 0 GOTO EndOfProc   	
		END	
	END		
	IF (@WLTYPE = 0) OR (@testAlert = 1)
	BEGIN
		--Inserting records into SASACTIVITY for the @ID
		--obtained from API_InsAlert stored Procedure
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'Alert', @ID, TRANNO 
			FROM @TT t
			WHERE  t.byOrder = @byOrder AND
			t.tranAmt = @IndAmount
	
			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC
	END 
	ELSE 
	IF @WLTYPE = 1 --Case
	BEGIN
		--Inserting records into SASACTIVITY for the @ID
		--obtained from API_InsSuspiciosActivity stored Procedure
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'SUSPACT', @ID, TRANNO 
				FROM @TT t
			WHERE  t.byOrder = @byOrder AND
			t.tranAmt = @IndAmount

			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC 
	END
	FETCH NEXT FROM @cur INTO @tranAmt, @activityCount , @byOrder, @IndAmount
END --Cursor While loop Ends

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_SameByOrderBeneAndSameAmt
  RETURN @stat
END	

IF @currentTransactionCount = 0
  COMMIT TRAN USR_SameByOrderBeneAndSameAmt
RETURN @stat
Go

--USR_XPaymentByOrdBeneOverY
IF NOT EXISTS (SELECT * FROM Watchlist 
WHERE WLCode = 'XTypeYBeOr') BEGIN 
insert into Watchlist (WLCode, Title, [Desc], WLType, SPName, SuspType, 
Schedule, IsPreEOD, OwnerBranch, OwnerDept, OwnerOper, CreateOper, CreateDate, Params, IsUserDefined, CreateType,  IsLicensed)
values ('XTypeYBeOr', 'Order party sending to the same beneficiary of specified amount over specified count',
'Detects transactions based on a specified activity type where the Ordering Party sends 
to the same Beneficiary and the total of these transactions is within a designated 
dollar amount range and the number of these transactions exceeds a selected count. 
The totals are calculated on a monthly basis. Designed to be run Post-EOD. 
If the option to remove special/wildcard characters is enabled, then all non-alpha
 numeric characters other than spaces will be removed for name matching in  
 Beneficiary and ByOrder name fields. For example X.Y.Z Corp become XYZ Corp for matching purpose.',
0, 'USR_XPaymentByOrdBeneOverY', 'Rule', 4, 0, Null, Null, Null,
'Prime', GetDate(),'<Params>
<Param Name="@minimumAmount" Alias="Minimum Amount" Value="400000" DataType="Money"/>
<Param Name="@maximumAmount" Alias="Maximum Amount" Value="500000" DataType="Money"/>
<Param Name="@minimumCount" Alias="Minimum Count" Value="3" DataType="Int"/>
<Param Name="@activityType" Alias="Activity Type" Value="101" DataType="Int"/>
<Param Name="@branch" Alias="Branch" Value="NY" DataType="String"/>
<Param Name="@dept" Alias="Department" Value="SEC" DataType="String"/>
<Param Name="@filterBranch" Alias="Filter Branch (Yes(1) / No(0)" Value="0" DataType="Int"/>
<Param Name="@filterDept" Alias="Filter Department" Value="0" DataType="Int"/>
<Param Name="@RemoveSpecialChar" Alias="Remove Special/Wildcard characters in Beneficiary and ByOrder Name.(0 for No, 1 for Yes)(See rule description for more details)" Value="0" DataType="Int"/>
</Params>', 0, 0, 0)
END ELSE BEGIN 
UPDATE WATCHLIST 
SET 
Title = 'Order party sending to the same beneficiary of specified amount over specified count', 
[Desc] = 'Detects transactions based on a specified activity type where the Ordering Party sends 
to the same Beneficiary and the total of these transactions is within a designated 
dollar amount range and the number of these transactions exceeds a selected count. 
The totals are calculated on a monthly basis. Designed to be run Post-EOD.
If the option to remove special/wildcard characters is enabled, then all non-alpha
numeric characters other than spaces will be removed for name matching in 
Beneficiary and ByOrder name fields. For example X.Y.Z Corp become XYZ Corp for matching purpose.', 
WLType = 0, SPName = 'USR_XPaymentByOrdBeneOverY', SuspType = 'Rule', 
Schedule = 4, IsPreEOD = 0, 
OwnerOper = Null, OwnerBranch = Null, OwnerDept = Null, 
Params = '<Params>
<Param Name="@minimumAmount" Alias="Minimum Amount" Value="400000" DataType="Money"/>
<Param Name="@maximumAmount" Alias="Maximum Amount" Value="500000" DataType="Money"/>
<Param Name="@minimumCount" Alias="Minimum Count" Value="3" DataType="Int"/>
<Param Name="@activityType" Alias="Activity Type" Value="101" DataType="Int"/>
<Param Name="@branch" Alias="Branch" Value="NY" DataType="String"/>
<Param Name="@dept" Alias="Department" Value="SEC" DataType="String"/>
<Param Name="@filterBranch" Alias="Filter Branch (Yes(1) / No(0)" Value="0" DataType="Int"/>
<Param Name="@filterDept" Alias="Filter Department" Value="0" DataType="Int"/>
<Param Name="@RemoveSpecialChar" Alias="Remove Special/Wildcard characters in Beneficiary and ByOrder Name.(0 for No, 1 for Yes)(See rule description for more details)" Value="0" DataType="Int"/>
</Params>',
IsUserDefined = 0, CreateType = 0, IsLicensed = 0, LastOper = 'Prime'
WHERE WLCode = 'XTypeYBeOr'
END
Go

/******************************************************************************
Procedure Name: USR_XPaymentByOrdBeneOverY
Description:	Detects transactions based on a specified activity type 
where the Ordering Party sends to the same Beneficiary and the total 
of these transactions is within a designated dollar amount range and 
the number of these transactions exceeds a selected count. The totals are 
calculated on a monthly basis. Designed to be run Post-EOD.
Parameters:		@WLCode [in] SCode = Rule Code				
			@minimumAmount [in] Money = Threshold amt of deposits or withdrawals
			@maximumAmount [in] Money = Maximum amount of deposits or withdrawals
			@minimumCount [in] INT = Threshold number of deposits or withdrawals
			@activityType [in] INT = Type of activity to be checked
			@branch [in] SCode = branch to filter on
			@dept [in] SCode = dept to filter on 
			@filterBranch [in] BIT = to filter or not filter branch
			@filterDept [in] BIT = to filter or not filter on dept
			@RemoveSpecialChar = Rule is configured to Remove Special/Wildcard characters 
									in the Name Field.
			[Return] INT = Return the error status.  0 for success.
******************************************************************************/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_XPaymentByOrdBeneOverY]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.USR_XPaymentByOrdBeneOverY
Go
CREATE PROCEDURE dbo.USR_XPaymentByOrdBeneOverY (@WLCode SCode, @testAlert INT, 
	@minimumAmount money,
	@maximumAmount money,
	@minimumCount int,
	@activityType int,
	@branch SCode,
	@dept SCode,
	@filterBranch BIT,
	@filterDept BIT,
	@RemoveSpecialChar smallint =0)
AS

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@MINDATE INT,
	@MAXDATE INT
DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME

DECLARE	@TT TABLE (
	Bene VARCHAR(40),
	ByOrder VARCHAR(40),
	BookDate INT,
	tranAmt MONEY,
	tranCnt INT
)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_XPaymentByOrdBeneOverY
ELSE
	-- Already in a transaction
	SAVE TRAN USR_XPaymentByOrdBeneOverY
/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @description = [Desc], @WLType = WLType  
FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

SET @minDate = FLOOR(DBO.CONVERTSQLDATETOINT(DateAdd(month, -1, (GETDATE()))) / 100) * 100 + 1
SET @maxDate = FLOOR(DBO.CONVERTSQLDATETOINT(GETDATE()) / 100) * 100 + 1

IF @RemoveSpecialChar =1
	BEGIN
		INSERT INTO @TT(Bene, ByOrder, Bookdate, tranAmt, tranCnt)
		SELECT 
		dbo.BSA_FnRemoveSpecialCharacters(ah.Bene) AS bene,
		dbo.BSA_FnRemoveSpecialCharacters(ah.ByOrder) AS ByOrder ,
		FLOOR(ah.bookdate / 100) * 100 + 1 , 
			SUM(BaseAmt) tranAmt, 
			count(TranNo) tranCnt
		FROM ActivityHist ah (NOLOCK)
		WHERE @minDate <= bookDate AND bookDate < @maxDate AND bene IS NOT NULL AND ByOrder IS NOT NULL AND type = @activityType AND
			(@dept = dept OR @filterDept = 0)  AND 
			(@branch = branch OR @filterBranch = 0) 
		GROUP BY FLOOR(ah.bookdate / 100) * 100 + 1, dbo.BSA_FnRemoveSpecialCharacters(ah.Bene),dbo.BSA_FnRemoveSpecialCharacters(ah.ByOrder)
		HAVING (SUM(BaseAmt) >= @minimumAmount AND 
			(SUM(BaseAmt) <= @maximumAmount OR @maximumAmount = -1) AND
			count(tranno) >= @minimumCount)

	END 
ELSE
	BEGIN
	INSERT INTO @TT(Bene, ByOrder, Bookdate, tranAmt, tranCnt)
		SELECT 
		ah.Bene,
		ah.ByOrder,
		FLOOR(ah.bookdate / 100) * 100 + 1 , 
			SUM(BaseAmt) tranAmt, 
			count(TranNo) tranCnt
		FROM ActivityHist ah (NOLOCK)
		WHERE @minDate <= bookDate AND bookDate < @maxDate AND bene IS NOT NULL AND ByOrder IS NOT NULL AND type = @activityType AND
			(@dept = dept OR @filterDept = 0)  AND 
			(@branch = branch OR @filterBranch = 0) 
		GROUP BY FLOOR(ah.bookdate / 100) * 100 + 1, ah.Bene, ah.ByOrder
		HAVING (SUM(BaseAmt) >= @minimumAmount AND 
			(SUM(BaseAmt) <= @maximumAmount OR @maximumAmount = -1) AND
			count(tranno) >= @minimumCount)
	END

IF @testAlert = 1 BEGIN

	SELECT @STARTALRTDATE = GETDATE()
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT, isTest) 
	  SELECT @WLCode, 'Ordering Party: ''' + ByOrder + ''' sent Beneficiary: ''' 
			+ Bene + '''' + char(13) + char(10) + 'amount : '''  + 
			CONVERT(VARCHAR, tranAmt) + ''' over ''' + CONVERT(VARCHAR, tranCnt) + 
			''' transactions in ' + char(13) + char(10) + 'the month of ' + 
			convert(varchar, 
			MONTH(convert(datetime, convert(varchar,bookdate)))) + 
			' - ' 	+ convert(varchar, bookdate)+ '.', 0, 
			GETDATE(), NULL, NULL, NULL, NULL, 1 
	  FROM @TT 
	SELECT @STAT = @@ERROR	
	SELECT @ENDALRTDATE = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC

	IF @RemoveSpecialChar =1
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 'Alert', AlertNO, TRANNO 
				FROM ActivityHist a, @TT t, Alert where
				(@dept = a.dept OR @filterDept = 0)  AND 
				(@branch = a.branch OR @filterBranch = 0) AND
				dbo.BSA_FnRemoveSpecialCharacters(a.ByOrder) = dbo.BSA_FnRemoveSpecialCharacters(t.ByOrder) AND
				dbo.BSA_FnRemoveSpecialCharacters(a.bene) = dbo.BSA_FnRemoveSpecialCharacters(t.bene) AND
				(FLOOR(a.BookDate / 100) * 100 + 1)  = t.bookdate and
				t.bookdate = CASE isnumeric(left(right(Alert.[desc], 9),8)) 
						 WHEN 1 THEN convert(int,left(right(Alert.[desc], 9),8))
				 ELSE NULL END AND
				dbo.BSA_FnRemoveSpecialCharacters(a.ByOrder)  = SUBSTRING(Alert.[desc], 
																	CHARINDEX('Ordering Party:', Alert.[desc]) + len('Ordering Party:')+2, 
																	 CHARINDEX('sent', Alert.[Desc]) - (len('Ordering Party:')+5 )) AND
				dbo.BSA_FnRemoveSpecialCharacters(a.Bene) = SUBSTRING(Alert.[desc], 
																CHARINDEX('Beneficiary:', Alert.[desc]) + len('Beneficiary:')+2, 
																CHARINDEX('amount', Alert.[desc]) - (charindex('Beneficiary:', Alert.[desc]) + len('Beneficiary:')+5)) AND
				a.type = @activityType AND
				Alert.WLCode = @WLCode AND
				Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		END
	ELSE
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 'Alert', AlertNO, TRANNO 
				FROM ActivityHist a, @TT t, Alert where
				(@dept = a.dept OR @filterDept = 0)  AND 
				(@branch = a.branch OR @filterBranch = 0) AND
				a.ByOrder = t.ByOrder AND
				a.bene = t.bene AND
				(FLOOR(a.BookDate / 100) * 100 + 1)  = t.bookdate and
				t.bookdate = CASE isnumeric(left(right(Alert.[desc], 9),8)) 
						 WHEN 1 THEN convert(int,left(right(Alert.[desc], 9),8))
				 ELSE NULL END AND
				a.ByOrder = SUBSTRING(Alert.[desc], 
								CHARINDEX('Ordering Party:', Alert.[desc]) + len('Ordering Party:')+2, 
								CHARINDEX('sent', Alert.[Desc]) - (len('Ordering Party:')+5 )) AND
				a.Bene = SUBSTRING(Alert.[desc], 
							CHARINDEX('Beneficiary:', Alert.[desc]) + len('Beneficiary:')+2, 
							CHARINDEX('amount', Alert.[desc]) - (CHARINDEX('Beneficiary:', Alert.[desc]) + len('Beneficiary:')+5)) AND
				a.type = @activityType AND
				Alert.WLCode = @WLCode AND
				Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		END
	SELECT @STAT = @@ERROR 
END ELSE BEGIN

	IF @WLTYPE = 0 BEGIN
	SELECT @STARTALRTDATE = GETDATE()
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT) 
	  SELECT @WLCode, 'Ordering Party: ''' + ByOrder + ''' sent Beneficiary: ''' 
			+ Bene + '''' + char(13) + char(10) + 'amount : '''  + 
			CONVERT(VARCHAR, tranAmt) + ''' over ''' + CONVERT(VARCHAR, tranCnt) + 
			''' transactions in ' + char(13) + char(10) + 'the month of ' + 
			convert(varchar, 
			MONTH(convert(datetime, convert(varchar,bookdate)))) + 
			' - ' 	+ convert(varchar, bookdate)+ '.', 0, 
			GETDATE(), NULL, NULL, NULL, NULL 
	  FROM @TT 
	SELECT @STAT = @@ERROR	
	SELECT @ENDALRTDATE = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC
	END ELSE IF @WLTYPE = 1 BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			WLCode, WLDESC, CREATETIME )
		SELECT	NULL, BookDate, Null, NULL,
			NULL, 'RULE', NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, 0, 0, 0, 0, 
			NULL, NULL, NULL, NULL, 0, NULL, NULL,
			@WLCode, 'Ordering Party: ''' + ByOrder + ''' sent Beneficiary: ''' 
			+ Bene + '''' + char(13) + char(10) + 'amount : '''  + 
			CONVERT(VARCHAR, tranAmt) + ''' over ''' + CONVERT(VARCHAR, tranCnt) + 
			''' transactions in ' + char(13) + char(10) + 'the month of ' + 
			convert(varchar, 
			MONTH(convert(datetime, convert(varchar,bookdate)))) + 
			' - ' 	+ convert(varchar, bookdate)+ '.' , GETDATE() 
	FROM @TT 
	SELECT @STAT = @@ERROR	
	SELECT @ENDALRTDATE = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC
	END
	
	IF @WLTYPE = 0 BEGIN
		IF @RemoveSpecialChar =1
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 'Alert', AlertNO, TRANNO 
				FROM ActivityHist a, @TT t, Alert where
    			(@dept = a.dept OR @filterDept = 0)  AND 
				(@branch = a.branch OR @filterBranch = 0) AND 
				dbo.BSA_FnRemoveSpecialCharacters(a.ByOrder) = dbo.BSA_FnRemoveSpecialCharacters(t.ByOrder) AND
				dbo.BSA_FnRemoveSpecialCharacters(a.bene) = dbo.BSA_FnRemoveSpecialCharacters(t.bene) AND
				(FLOOR(a.BookDate / 100) * 100 + 1)  = t.bookdate AND
				t.bookdate = CASE isnumeric(left(right(Alert.[desc], 9),8)) 
							WHEN 1 THEN convert(int,left(right(Alert.[desc], 9),8))
					ELSE NULL END AND
				dbo.BSA_FnRemoveSpecialCharacters(a.ByOrder)= SUBSTRING(Alert.[desc], 
																CHARINDEX('Ordering Party:', Alert.[desc]) + LEN('Ordering Party:')+2,
																CHARINDEX('sent', Alert.[Desc]) - (len('Ordering Party:')+5 )) AND 
				dbo.BSA_FnRemoveSpecialCharacters(a.bene) = SUBSTRING(Alert.[desc], 
																CHARINDEX('Beneficiary:', Alert.[desc]) + LEN('Beneficiary:')+2, 
																CHARINDEX('amount', Alert.[desc]) - (CHARINDEX('Beneficiary:', Alert.[desc]) + LEN('Beneficiary:')+5)) AND
				a.type = @activityType AND
				Alert.WLCode = @WLCode AND
				Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		END
		ELSE
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 'Alert', AlertNO, TRANNO 
				FROM ActivityHist a, @TT t, Alert where
    			(@dept = a.dept OR @filterDept = 0)  AND 
				(@branch = a.branch OR @filterBranch = 0) AND
				a.ByOrder = t.ByOrder AND 
				a.bene = t.bene  AND
				(FLOOR(a.BookDate / 100) * 100 + 1)  = t.bookdate AND
				t.bookdate = CASE isnumeric(left(right(Alert.[desc], 9),8)) 
							WHEN 1 THEN convert(int,left(right(Alert.[desc], 9),8))
					ELSE NULL END AND
				a.ByOrder =SUBSTRING(Alert.[desc], 
							CHARINDEX('Ordering Party:', Alert.[desc]) + len('Ordering Party:')+2, 
							CHARINDEX('sent', Alert.[Desc]) - (len('Ordering Party:')+5 )) AND
				a.bene=SUBSTRING(Alert.[desc], 
							CHARINDEX('Beneficiary:', Alert.[desc]) + len('Beneficiary:')+2, 
							CHARINDEX('amount', Alert.[desc]) - (CHARINDEX('Beneficiary:', Alert.[desc]) + len('Beneficiary:')+5)) AND
				a.type = @activityType AND
				Alert.WLCode = @WLCode AND
				Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		END
	
	SELECT @STAT = @@ERROR 
	END ELSE IF @WLTYPE = 1 
			BEGIN
				IF @RemoveSpecialChar =1
				BEGIN
					INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
						SELECT 'SUSPACT', RECNO, TRANNO 
						FROM ActivityHist a ,@tt t, SUSPICIOUSACTIVITY S where
						(@dept = a.dept OR @filterDept = 0)  AND 
						(@branch = a.branch OR @filterBranch = 0) AND
						dbo.BSA_FnRemoveSpecialCharacters(a.ByOrder) = dbo.BSA_FnRemoveSpecialCharacters(t.ByOrder) AND 
						dbo.BSA_FnRemoveSpecialCharacters(a.bene) = dbo.BSA_FnRemoveSpecialCharacters(t.bene) AND 
						(FLOOR(a.bookdate / 100) * 100 + 1) = t.bookdate and
						t.bookdate = s.bookdate and
						dbo.BSA_FnRemoveSpecialCharacters(a.ByOrder)  = SUBSTRING(s.[WLdesc], 
																			CHARINDEX('Ordering Party:', s.[WLdesc]) + len('Ordering Party:')+2, 
																			CHARINDEX('sent', s.[WLdesc]) - (len('Ordering Party:')+5 )) AND
						dbo.BSA_FnRemoveSpecialCharacters(a.bene) = SUBSTRING(s.[WLdesc], 
																		CHARINDEX('Beneficiary:', s.[WLdesc]) + len('Beneficiary:')+2, 
																		CHARINDEX('amount', s.[WLdesc]) - (CHARINDEX('Beneficiary:',s.[WLdesc]) + len('Beneficiary:')+5)) AND
						a.type = @activityType AND
						S.WLCode = @WLCode AND
						S.CREATETIME BETWEEN @STARTALRTDATE AND @ENDALRTDATE
						SELECT @STAT = @@ERROR 
				END
				ELSE
				BEGIN
					INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
						SELECT 'SUSPACT', RECNO, TRANNO 
						FROM ActivityHist a ,@tt t, SUSPICIOUSACTIVITY S where
						(@dept = a.dept OR @filterDept = 0)  AND 
						(@branch = a.branch OR @filterBranch = 0) AND
						a.ByOrder = t.ByOrder AND 
						a.bene = t.bene AND
						(FLOOR(a.bookdate / 100) * 100 + 1) = t.bookdate and
						t.bookdate = s.bookdate AND
						a.ByOrder  = SUBSTRING(s.[WLdesc], 
										CHARINDEX('Ordering Party:', s.[WLdesc]) + len('Ordering Party:')+2, 
										CHARINDEX('sent', s.[WLdesc]) - (len('Ordering Party:')+5 )) AND
						a.bene  =SUBSTRING(s.[WLdesc], 
										CHARINDEX('Beneficiary:', s.[WLdesc]) + len('Beneficiary:')+2, 
										CHARINDEX('amount', s.[WLdesc]) - (CHARINDEX('Beneficiary:',s.[WLdesc]) + len('Beneficiary:')+5)) AND
						a.type = @activityType AND
						S.WLCode = @WLCode AND
						S.CREATETIME BETWEEN @STARTALRTDATE AND @ENDALRTDATE
						SELECT @STAT = @@ERROR 
				END
			END
END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_XPaymentByOrdBeneOverY
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_XPaymentByOrdBeneOverY
RETURN @stat
Go

If Exists(Select * From sysobjects 
          where id = object_id(N'[dbo].[BSA_GetSARAttachment]') 
              and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
    Drop Procedure dbo.BSA_GetSARAttachment
Go


CREATE Procedure dbo.BSA_GetSARAttachment ( @RecNo ObjectId) 
With Encryption As
	
	select a.ObjectId as [Recno],a.FileName as [FileName],a.FileImage as [FileImage]  from Attachment a with (nolock) inner join Documentation d with (nolock) on a.ObjectId = d.RecNo 
	where  d.objecttype='RegRep' and d.ObjectId = @RecNo and a.ObjectType ='Document'

GO

/******************************************************************************
Procedure Name: USR_SARFollowUp
Description:	Generate a follow-up case/alert one time per SAR. All the 
                information description comes from the original one containde 
                the SAR. Recommended to be run under Post-EOD.
Parameters:		@WLCode [in] SCode = Rule Code,				
				@testAlert		INT,		
				@NumStartDays	varchar(12),
				@NumEndDays		varchar(12),				
				@custTypesList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
				@riskClassList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All
				@branchList		VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
				@departmentList VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
				@sarNoToExclude	  VARCHAR(8000) = '-NONE-', --  Use -NONE- for None.
				@custListExcluded VARCHAR(8000) = '-NONE-', --  Use -NONE- for None.
				@CurrencySymbol VARCHAR(1)	= '$'	-- Use USD '$' as Default Currency Symbol
				[Return] INT as the error status code or 0 for success.
******************************************************************************/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_SARFollowUp]') 	
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure [dbo].[USR_SARFollowUp]	
Go

CREATE PROCEDURE [dbo].[USR_SARFollowUp] ( 
	@WLCode 		SCode,
	@testAlert		INT,		
	@NumStartDays	VARCHAR(12),
	@NumEndDays		VARCHAR(12),	
	@custTypesList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
	@riskClassList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All
	@branchList		VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
	@departmentList VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
	@sarListToExclude	  VARCHAR(8000) = '-NONE-', --  Use -NONE- for None.
	@custListExcluded VARCHAR(8000) = '-NONE-',		--  Use -NONE- for None.
	@CurrencySymbol VARCHAR(1)	= '$'	-- Use USD '$' as Default Currency Symbol
)
AS
	
/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@numStartDate	INT,
	@numEndDate	INT,	
	@fromDate INT,
 	@toDate INT,
	@strFromDate VARCHAR(10),
	@strToDate VARCHAR(10),
	@strBetweenDates VARCHAR(50),
 	@firstMonitorDate INT, 
 	@strCaseDesc1 VARCHAR(33),
 	@strCaseDesc2 VARCHAR(15),
 	@strCaseDesc3 VARCHAR(16),
 	@strCaseDesc4 VARCHAR(20),
 	@strCaseDesc5 VARCHAR(27),
 	@strCaseDesc6 VARCHAR(5),
 	@strCaseDesc7 VARCHAR(37),
 	@strCaseDesc8 VARCHAR(2), 	
 	@nom INT,					
 	@nod INT,
 	@date2Monitor INT,			
	@STARTALRTDATE DATETIME,
	@ENDALRTDATE DATETIME, 	
	@tranAmt MONEY,
	@tranCnt INT,
	@trnCnt INT,
	@Cust VarChar(35),
	@OwnerBranch VarChar(11),
	@OwnerDept VarChar(11),
	@nMonthComparison INT,
	@CurrDate GenericDate,
	@wrkDate DATETIME,
	@nowDate DATETIME,
	@firstDay DATETIME,
	@lastDay DATETIME
	
SET @strCaseDesc1 = 'A SAR follow-up was generated on '
SET @strCaseDesc2 = ' based on case '
SET @strCaseDesc3 = ' and SAR number '
SET @strCaseDesc4 = ' which was filed on ' 
SET @strCaseDesc5 = '. The Total Amount of case ' 
SET @strCaseDesc6 = ' was ' 
SET @strCaseDesc7 =  '. Below is the Description from case '
SET @strCaseDesc8 = ': ' 

DECLARE @SetDate DATETIME

DECLARE @dsQS TABLE (
	[SarNo] int Not NULL, 
	[CaseNo] int Not NULL, 
	[FiledDate] DateTime NULL, 
	[Type] [varchar](11) NULL,
	[CustID] [varchar](35) NULL,
	[CustName] [varchar](40) NULL,	
	[AcctID] [varchar](35) NULL,
	[TotalAmount] money,
	[WLDesc] varchar(max)
)

DECLARE @dsTT TABLE (
	[ObjectID] [varchar](35) NULL,
	[TranNo]   [int] NULL,
	[BaseAmt]  [money] NULL
)

DECLARE @CustomerExemptionsList VARCHAR(8000)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	BEGIN TRAN USR_SARFollowUp
ELSE
	SAVE TRAN USR_SARFollowUp

SELECT @WLType = WLType, @SetDate =
	CASE
		WHEN UseSysDate = 2 THEN                       
			(SELECT BusDate FROM dbo.SysParam)	-- use business date
		ELSE
            GetDate()	-- use system date
	END
FROM dbo.WatchList (NOLOCK)
WHERE WLCode = @WLCode

SELECT @custTypesList		= dbo.BSA_fnListParams(@custTypesList)
SELECT @riskClassList		= dbo.BSA_fnListParams(@riskClassList)
SELECT @branchList			= dbo.BSA_fnListParams(@branchList)
SELECT @departmentList		= dbo.BSA_fnListParams(@departmentList)
SELECT @sarListToExclude    = dbo.BSA_fnListParams(@sarListToExclude)
SELECT @custListExcluded    = dbo.BSA_fnListParams(@custListExcluded)

If ( ( @NumStartDays Is NULL  AND @NumEndDays Is NULL ) 
OR ( ltrim(rtrim(@NumStartDays)) = '' AND  ltrim(rtrim(@NumEndDays)) = '' )) 
	BEGIN
		--If Both Start and End dates are blank then default dates to previous month per the business or system date determined by @SetDate”. 
		SET @nowDate = (SELECT DATEADD(M, DATEDIFF(M, 0, @SetDate), 0))
		SET @firstDay = (SELECT DATEADD(M, -1, @nowDate))
		SET @lastDay = (SELECT DATEADD(D, 0, @nowDate)) 
		SET @fromDate = dbo.ConvertSqlDateToInt(@firstDay)
		SET @toDate = dbo.ConvertSqlDateToInt(@lastDay)
		SET @strFromDate = CONVERT(varchar(10), CONVERT(date, CONVERT(varchar(8), @fromDate), 112), 101)
		SET @strToDate = CONVERT(varchar(10), CONVERT(date, CONVERT(varchar(8), @toDate), 112), 101)		
	END
ELSE If	( ISNUMERIC(@NumStartDays) = 1 AND ISNUMERIC(@NumEndDays) = 1)
	BEGIN
		--If Both dates are populated then use the provided dates	
		SET @numStartDate = (SELECT CAST(@NumStartDays as int) )
		SET @numEndDate =  (SELECT CAST(@NumEndDays as int) )
		SET @nowDate = (SELECT @SetDate)
		SET @firstDay = (SELECT DATEADD(DAY, -@numStartDate, @nowDate))
		SET @lastDay = (SELECT DATEADD(DAY, 1-@numEndDate, @nowDate)) 
		SET @fromDate = dbo.ConvertSqlDateToInt(@firstDay)
		SET @toDate = dbo.ConvertSqlDateToInt(@lastDay)
		SET @strFromDate = CONVERT(varchar(10), CONVERT(date, CONVERT(varchar(8), @fromDate), 112), 101)
		SET @strToDate = CONVERT(varchar(10), CONVERT(date, CONVERT(varchar(8), @toDate), 112), 101)		
	END
ELSE
BEGIN
	--If only one of the Start or End Date is BLANK, then do nothing per Business decision
	GOTO EndOfProc
END

SET @strBetweenDates = ' between the dates of ' + @strFromDate + ' and ' +  @strToDate +'.'

IF (ISNULL(@sarListToExclude,'') = '' OR UPPER(ISNULL(@sarListToExclude,'-NONE-')) = '-NONE-')
	SET @sarListToExclude = NULL

IF (ISNULL(@custListExcluded,'') = '' OR UPPER(ISNULL(@custListExcluded,'-NONE-')) = '-NONE-')
	SET @custListExcluded = NULL

-- Collect the qualified data source first
Insert into @dsQS
SELECT  rr.RepNo As SarNo, 
		rr.SARecNo As CaseNo, 
		rr.Filed As FiledDate, 
		rr.Type, 
		rr.Cust As CustID, 
		c.Name As CustName, 
		rr.Account As AcctID,
		NULL,
		NULL 
FROM   dbo.RegReport AS rr INNER JOIN dbo.Customer AS c 
ON	   rr.Cust = c.Id
WHERE (rr.Type = 'SAR')
	AND dbo.ConvertSqlDateToInt(rr.Filed) >= @fromDate
	AND dbo.ConvertSqlDateToInt(rr.Filed) < @toDate
	AND ((@custTypesList IS NULL OR
		CHARINDEX(',' + LTRIM(RTRIM(c.Type)) + ',', @custTypesList ) > 0))
	AND ((@riskClassList IS NULL OR 
		CHARINDEX(',' + LTRIM(RTRIM(c.RiskClass)) + ',', @riskClassList ) > 0)) 
	AND ((@branchList IS NULL  OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c.OwnerBranch,''))) + ',', @branchList) > 0))		
	AND ((@departmentList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR,  c.OwnerDept) + ',',@departmentList) > 0))		

-- To excluded List of SAR or Cust if Any
If @sarListToExclude Is Not NULL
	BEGIN
		If EXISTS ( SELECT * FROM  @dsQS	
			WHERE CHARINDEX(',' + CONVERT(VARCHAR, SarNo) + ',', @sarListToExclude )  > 0
		)
		If (@sarListToExclude IS NOT NULL)
			DELETE FROM @dsQS WHERE CHARINDEX(',' + CONVERT(VARCHAR, SarNo) + ',', @sarListToExclude )  > 0
	END

If @custListExcluded Is Not NULL
	BEGIN
		If EXISTS ( SELECT * FROM  @dsQS	
			WHERE CHARINDEX(',' + CONVERT(VARCHAR, CustID) + ',', @custListExcluded )  > 0
		)
		If (@custListExcluded IS NOT NULL)
			DELETE FROM @dsQS WHERE CHARINDEX(',' + CONVERT(VARCHAR, CustID) + ',', @custListExcluded )  > 0
	END

If Exists (Select * FROM  @dsQS ds INNER JOIN dbo.SuspiciousActivity sa
					ON	  ds.CaseNo = sa.RecNo
					WHERE ds.TotalAmount Is NULL AND sa.ActCurrReportAmt Is Not NULL )
	UPDATE @dsQS
		SET	  TotalAmount = sa.ActCurrReportAmt  
		FROM  @dsQS ds INNER JOIN dbo.SuspiciousActivity sa
		ON	  ds.CaseNo = sa.RecNo
		WHERE ds.TotalAmount Is NULL AND sa.ActCurrReportAmt Is Not NULL
Else If Exists (Select * FROM  @dsQS ds INNER JOIN dbo.SuspiciousActivityHist sa
						 ON	  ds.CaseNo = sa.RecNo
						 WHERE ds.TotalAmount Is NULL AND sa.ActCurrReportAmt Is Not NULL )
	UPDATE @dsQS
		SET	  TotalAmount = sa.ActCurrReportAmt  
		FROM  @dsQS ds INNER JOIN dbo.SuspiciousActivityHist sa
		ON	  ds.CaseNo = sa.RecNo
		WHERE ds.TotalAmount Is NULL AND sa.ActCurrReportAmt Is Not NULL
		
If Exists (Select * FROM  @dsQS ds INNER JOIN dbo.SuspiciousActivity sa
						 ON	  CaseNo = sa.RecNo
						 WHERE ds.WLDesc Is NULL AND sa.WLDesc Is Not NULL )
	UPDATE @dsQS
		SET ds.WLDesc = dbo.BSA_GetExistingCaseDescriptions(sa.RecNo, @CurrencySymbol)
		FROM  @dsQS ds INNER JOIN dbo.SuspiciousActivity sa
		ON	  CaseNo = sa.RecNo
		WHERE ds.WLDesc Is NULL 
Else If Exists (Select * FROM  @dsQS ds INNER JOIN dbo.SuspiciousActivityHist sa
					ON	  CaseNo = sa.RecNo
					WHERE ds.WLDesc Is NULL AND sa.WLDesc Is Not NULL )
	UPDATE @dsQS
		SET ds.WLDesc = dbo.BSA_GetExistingCaseDescriptions(sa.RecNo, @CurrencySymbol)
		FROM  @dsQS ds INNER JOIN dbo.SuspiciousActivityHist sa
		ON	  CaseNo = sa.RecNo
		WHERE ds.WLDesc Is NULL 
;

Insert @DSTT
		SELECT DISTINCT sa.ObjectID, sa.TranNo, CASE WHEN a.RecvPay = 2 THEN (- 1) * a.BaseAmt WHEN a.RecvPay = 1 THEN a.BaseAmt END AS BaseAmt
		FROM  dbo.ActivityHist AS a INNER JOIN dbo.SAsActivity AS sa 
		ON	  sa.TranNo = a.TranNo
		WHERE (sa.ObjectID IN (SELECT CaseNo
							FROM   @dsQS))	
;
WITH Tot(CaseNo, TotalAmount)
As 
(
	SELECT DISTINCT ObjectID AS CaseNo, CASE WHEN SUM(BaseAmt) < 0 THEN (- 1) * SUM(BaseAmt) ELSE SUM(BaseAmt) END AS TotalAmount
	FROM @dsTT
	GROUP BY ObjectID
)
UPDATE @dsQS
	SET   ds.TotalAmount = Tot.TotalAmount 
	From  @dsQS ds inner join Tot
	ON	  ds.CaseNo = Tot.CaseNo 
	Where ds.TotalAmount Is NULL AND Tot.TotalAmount Is Not NULL
;
IF ( @testAlert = 1 ) OR ( @testAlert = 0 AND @WLTYPE = 0 )
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()

		If Exists (	SELECT * FROM @dsQS )
			INSERT INTO Alert ( 
					  WLCode
					,[Account]
					,[Cust]
					,[Desc]
					,Status
					,CreateDate
					,LastOper
					,LastModify
					,IsTest
			) 	
			
			
			SELECT 	@WLCode
					,AcctID
					,CustID
					,@strCaseDesc1 + CustName +
					@strCaseDesc2 + cast(CaseNo as varchar) +
					@strCaseDesc3 + cast(SARNo as varchar) +  
					@strCaseDesc4 + CONVERT(varchar(10), FiledDate, 101) + 
					@strCaseDesc5 + cast(CaseNo as varchar) + 
					@strCaseDesc6 + Case When TotalAmount Is Not NULL Then @CurrencySymbol + Convert(varchar, TotalAmount, 1) 
										 Else '(See case ' + cast(CaseNo as varchar) + ' for additional information due to missing TotalAmount!)' 
									End +
					@strCaseDesc7 + cast(CaseNo as varchar) + 
					@strCaseDesc8 + Case When WLDesc IS Not NULL And WLDesc <> '' Then
										Case When len(WLDesc) >1600  Then 
											SUBSTRING(LTRIM(WLDesc),1,1600) + ' (See case ' + cast(CaseNo as varchar) + ' for additional information due to message truncation!)'
										Else WLDesc End
									Else '(See case ' + cast(CaseNo as varchar) + ' for additional information due to missing case description!)'
									END as CaseDesc			
					, 0
					, GETDATE()
					, Null
					, Null
					, @testAlert 
			FROM @dsQS ds		
			ORDER BY ds.SarNo		
			
		SELECT @STAT = @@ERROR
	END 
ELSE
IF 	( @testAlert = 0 ) AND ( @WLTYPE = 1 )
	BEGIN
		if exists ( SELECT * FROM @dsQS ) 
			INSERT INTo SuspiciousActivity (
					ProfileNo, BookDate, Cust, Account, Activity
					, SuspType, StartDate, EndDate, RecurType, RecurValue
					, ActCurrReportAmt, ActInActCnt, ActOutActCnt, ActInActAmt, ActOutActAmt
					, CurrReportAmt, ExpAvgInActCnt, ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt
					, InCntTolPerc, OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr
					, ReviewState, ReviewTime, ReviewOper, App, AppTime
					, AppOper , WLCode, WLDesc, CreateTime )
			SELECT 	Null, dbo.ConvertSqlDateToInt(GETDATE()), CustID, AcctID, Null
					,'Rule', CONVERT(date, CONVERT(varchar(8), @fromDate), 112), CONVERT(date, CONVERT(varchar(8), @toDate), 112), Null, Null
					, TotalAmount, Null, Null, Null, Null
					, Null, Null, Null, Null, Null
					, 0, 0, 0, 0, Null
					, Null, Null, Null, 0, Null
					, Null, @WLCode
					,@strCaseDesc1 + CustName +
					@strCaseDesc2 + cast(CaseNo as varchar) +
					@strCaseDesc3 + cast(SARNo as varchar) +  
					@strCaseDesc4 + CONVERT(varchar(10), FiledDate, 101) + 
					@strCaseDesc5 + cast(CaseNo as varchar) +					
					@strCaseDesc6 + Case When TotalAmount Is Not NULL Then @CurrencySymbol + Convert(varchar, TotalAmount, 1) 
										 Else '(See case ' + cast(CaseNo as varchar) + ' for additional information due to missing TotalAmount!)' 
									End +
					@strCaseDesc7 + cast(CaseNo as varchar) + 
					@strCaseDesc8 +  Case When WLDesc IS Not NULL And WLDesc <> '' Then
										Case When len(WLDesc) >1600  Then 
											SUBSTRING(LTRIM(WLDesc),1,1600) + ' (See case ' + cast(CaseNo as varchar) + ' for additional information due to message truncation!)'
										Else WLDesc End
									Else '(See case ' + cast(CaseNo as varchar) + ' for additional information due to missing case description!)'
									END as CaseDesc				
					,GETDATE()
				FROM @dsQS ds		
				ORDER BY ds.SarNo		
							
		SELECT @STAT = @@ERROR	

		if @STAT <> 0  GOTO ENDOFPROC
	END	

--------   End of ALERTs or CASEs SQL Script Codes Above -------------------

ENDOFPROC:
IF (@stat <> 0) BEGIN  
  ROLLBACK TRAN USR_SARFollowUp
  RETURN @stat
END	

IF @trnCnt = 0 
  COMMIT TRAN USR_SARFollowUp
  RETURN @stat
  
GO



IF NOT EXISTS(select * from sys.sysobjects  where name = 'CodeSpecialChar' and xtype='R')
BEGIN
	DECLARE @sql NVARCHAR(max)
    SET @sql = N'Create rule CodeSpecialChar as @codeSplChar not like ''%[^0-9A-Za-z\-]%''' 
	EXECUTE sp_executesql @sql
END

GO
--Overwrite the rule 'Code' with the new rule 'CodeSpecialChar'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Account.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Account.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Account.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'AccountOwner.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'AccountOwner.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'AccountType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'AccountType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Activity.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ActivityAudit.Operator'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ActivityAudit.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ActivityAudit.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ActivityDeleted.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ActivityHist.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ActivityMap.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ActivityMap.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ActivityType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ActivityType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Alert.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Alert.ReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Alert.AppOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'AlertHist.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'AlertHist.ReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'AlertHist.AppOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Attachment.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Attachment.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'BranchMap.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'BranchMap.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CaseAdditionalSuspects.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CaseAdditionalSuspects.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CaseScoreData.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CaseScoreData.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CaseWeightData.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CaseWeightData.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Categories.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Categories.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CDDCalculatedItem.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CDDCalculatedItem.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckList.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckList.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListData.CompletedBy'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListData.SecondCheckBy'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListData.ApprovedBy'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListData.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListData.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListDataHist.CompletedBy'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListDataHist.SecondCheckBy'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListDataHist.ApprovedBy'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListDataHist.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListDataHist.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListItem.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListItem.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListSection.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListSection.LastModifyOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListSectionItem.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListSectionItem.LastModifyOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CheckListType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CodeTbl.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CodeTbl.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Correspondence.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Correspondence.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Correspondence.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CostCenter.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CostCenter.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Country.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Country.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CountryOwner.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CountryOwner.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CTasK.AssignedBy'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CTasK.AssignedTo'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CTasK.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CTasK.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CTasK.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CTaskObjects.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Customer.LastReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Customer.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Customer.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Customer.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerDoc.LastReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerDoc.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerDoc.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerHist.LastReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerHist.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerHist.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerHist.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerMap.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerMap.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerOwner.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerOwner.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerReference.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerReference.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'CustomerType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'DeptMap.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'DeptMap.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Documentation.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Documentation.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Documentation.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'DocumentType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'DocumentType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Event.Oper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'EventType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'EventType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ExemptionType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ExemptionType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Graph.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDProvider.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDProvider.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDRequest.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDRequest.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDRequest.ReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDRequest.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDRequestHist.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDRequestHist.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDRequestHist.ReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDRequestHist.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDResult.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDResult.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDResult.ReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDService.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDService.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDStatus.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'IDStatus.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'InstrumentType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'InstrumentType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Introducer.LastReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Introducer.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Introducer.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'KYCCPartyMatch.Operator'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'KYCData.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'KYCData.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'KYCDataHist.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'KYCDataHist.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'KYCDataTemp.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'KYCDataTemp.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Notification.NotifyOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'NotificationParties.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'NotificationParties.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'NotificationType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'NotificationType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ObjectType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'ObjectType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'OptionTbl.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'OptionTbl.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyAddress.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyAddress.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyAddressHist.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyAddressHist.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyAlias.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyAlias.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyAliasHist.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyAliasHist.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyID.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyID.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyIdHist.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyIdHist.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyRelation.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PartyRelation.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PaymentMethods.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PaymentMethods.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PeerModel.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PeerModel.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Process.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Process.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Profile.LastReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Profile.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Profile.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Profile.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PurgeObjects.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'PurgeObjects.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Query.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Query.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Region.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Region.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RegReport.PreparedBy'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RegReport.FiledBy'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RegReport.FinalizedBy'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RegReport.ApprovedBy'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RegReport.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RegReport.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RegReport.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RegReportType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RegReportType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RelationshipType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RelationshipType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Report.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Report.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RFCalculatedItem.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RFCalculatedItem.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskClass.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskClass.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskFactorData.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskFactorData.LastReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskFactorData.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskFactorItem.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskFactorItem.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskFactorList.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskFactorList.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskFactorType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskFactorType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskFactorValue.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskFactorValue.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskScoreHist.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RiskScoreHist.AcceptOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RouteRule.CaseReviewer'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RouteRule.CaseApprover'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RouteRule.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RouteRule.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'RouteRule.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SARCTRImport.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SARCTRImport.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Sequences.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Sequences.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SetUpState.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SupervisoryType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SupervisoryType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuspiciousActivity.ReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuspiciousActivity.AppOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuspiciousActivity.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuspiciousActivity.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuspiciousActivity.lockoper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuspiciousActivityHist.ReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuspiciousActivityHist.AppOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuspiciousActivityHist.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuspiciousActivityHist.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuspiciousActivityHist.lockoper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuspiciousActivityType.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SuspiciousActivityType.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SysParam.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'SysParam.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Task.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Task.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempAccount.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempAccount.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempAccount.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempCustomer.LastReviewOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempCustomer.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempCustomer.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'TempCustomer.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Watchlist.OwnerOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Watchlist.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'Watchlist.LastOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'WebSite.CreateOper'
EXEC sp_bindrule 'dbo.CodeSpecialChar', 'WebSite.LastOper'

GO
IF EXISTS (select * from sysobjects 
	where id = object_id(N'[dbo].[BSA_CLItemExistsInModel]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [DBO].[BSA_CLItemExistsInModel]
GO
/*Code moved from cdbmanager.ItemExistsInModel*/
Create Procedure BSA_CLItemExistsInModel (@ItemCode varchar(11))
With Encryption as

	DECLARE @ItemExists bit
	SET @ItemExists = 0

	--If record exists in Checklist or CheckListSectionItem then return count
	Select @ItemExists=count(*) from CheckList (nolock) where CLICode = @ItemCode		
	IF (@ItemExists=0)
		Select @ItemExists=count(*) from CheckListSectionItem (nolock) where clitemcode = @ItemCode

	RETURN @ItemExists
GO

--USR_AntpActvComps
IF NOT EXISTS (SELECT * FROM Watchlist
WHERE WLCode = 'AtpActvCmps') BEGIN
INSERT INTO [Watchlist] ([WLCode], [Title], [Desc], [WLType], [SPName], [SuspType]
	,[Schedule], [IsPreEOD], [OwnerBranch], [OwnerDept], [OwnerOper], [CreateOper]
	,[CreateDate], [LastOper], [LastModify], [LastEval], [LastEvalStat]  
	,[RuleType], [RuleText], [RuleFormat], [ExecTime], [CaseScore], [UseAssignedScore]
	,[IsLicensed], [Params], [CreateType], [IsUserDefined], [Category], [UseSysDate]
    ,[OverrideExemption]
    ) 
values ('AtpActvCmps'
		,'Detects accounts whose transactions for a specified date range exceed the account anticipated values.'
		,'Detects accounts whose transaction counts and/or amount totals for a specified date range and specified activity types exceed the account anticipated values (including the specified tolerance percentage, if provided). The start and end dates should equal a complete month. Leave the start date and end date blank to default to the last completed month based on the system date or current business date as selected during rule configuration. The comparison types are grouped as follows - Cash, Wire, Check, or Deposit. Designed to be run Post-EOD.The rule can include or exclude Accounts that have zero and/or Null values for the anticipated fields associated with the configured comparison type.
To include Accounts in the analysis that have 0 in the anticipated value fields for the selected comparison, set the IgnoreZeroAnticipatedValues parameter to 0; to exclude these accounts set the parameter to 1; default value is 1.
To include Accounts in the analysis that have NULL in the anticipated value fields for the selected comparison, set the IgnoreNullAnticipatedValues parameter to 0;  to exclude these accounts set the parameter to 1; default value is 1.'
		,0, 'USR_AntpActvComps', 'Rule', 1,  0, Null, Null, Null
		,'Prime', GETDATE(), 'Prime'
		,Null, Null, 1, 1, '', 0, 15, 0, 1, 0,
		'<Params> 
			<Param Name="@comparisonType" Alias="Anticipated Activity Comparison Type (1 for Cash, 2 for Wire, 3 for Check, 4 for Deposit)" Value="" DataType="Int"/>
			<Param Name="@startDate" Alias="Start Date for Look Back (MM/DD/YYYY). (Leave blank to use the most recently completed month)" Value="" DataType="String"/> 
			<Param Name="@endDate" Alias="End Date for Look Back (MM/DD/YYYY). (Leave blank to use the most recently completed month)" Value="" DataType="String"/>
			<Param Name="@minTransCount" Alias="Minimum transaction count" Value="" DataType="Int"/>
			<Param Name="@minAggTransAmt" Alias="Minimum aggregated transaction amount" Value="" DataType="Money"/>
			<Param Name="@monitorInd" Alias="Monitor Comparison Indicator: 1 for Amounts, 2 for Counts, 3 for Both" Value="" DataType="Int"/>
			<Param Name="@aggAntpDnFCntAmt" Alias="Aggregate Anticipated Domestic and Foreign Counts and Amounts (1 for Yes, 0 for No)" Value="" DataType="Int"/> 
			<Param Name="@amtTolerance" Alias="Amount Tolerance %" Value="" DataType="Int"/>  
			<Param Name="@cntTolerance" Alias="Count Tolerance %" Value="" DataType="Int"/>  
			<Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. -ALL- for All" Value="-ALL- " DataType="String"/>  
			<Param Name="@branchList" Alias="List of Branches separated by comma. -ALL- for All" Value="-ALL-" DataType="String"/>  
			<Param Name="@departmentList" Alias="List of Departments separated by comma. Use -ALL- for All" Value="-ALL-" DataType="String"/>  
			<Param Name="@acctTypesList" Alias="List of Account Types separated by comma. Use -ALL- for All" Value="-ALL-" DataType="String"/> 
			<Param Name="@custTypesList" Alias="List of Customer Types separated by comma. Use -ALL- for All" Value="-ALL-" DataType="String"/>  
			<Param Name="@actvTypesListIncl" Alias="Activity Types to include (comma separated). -ALL- for All. (Wire rule: list DOM Wire Act. Types)" Value="-ALL-" DataType="String"/> 
			<Param Name="@actvTypesListExcl" Alias="Activity Types to exclude (comma separated). -ALL- for All, -NONE- for None. (Wire rule: list FOR Wire Act. Types to include)" Value="-NONE-" DataType="String"/> 
			<Param Name="@receivedOrPay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both. (Determines to compare with deposit, withdrawal, or both)" Value="" DataType="Int"/>
			<Param Name="@cashType" Alias="Cash or Non-cash. (0 for Non-cash, 1 for Cash, 2 for both)" Value="" DataType="Int"/>
			<Param Name="@numDayMonitor" Alias="Number of Days to Monitor after Account Open Date (0 to ignore this parameter)" Value="0" DataType="Int"/>
			<Param Name="@ignoreZeroAntVals" Alias="Ignore Zero Anticipated Values (0 for No, 1 for Yes)" Value="1" DataType="Int"/>
			<Param Name="@ignoreNullAntVals" Alias="Ignore Null Anticipated Values (0 for No, 1 for Yes)" Value="1" DataType="Int"/>
		</Params>'
		,0,0,'User Defined',1,0)
END ELSE BEGIN
UPDATE WATCHLIST
SET
[Desc]='Detects accounts whose transaction counts and/or amount totals for a specified date range and specified activity types exceed the account anticipated values (including the specified tolerance percentage, if provided). The start and end dates should equal a complete month. Leave the start date and end date blank to default to the last completed month based on the system date or current business date as selected during rule configuration. The comparison types are grouped as follows - Cash, Wire, Check, or Deposit. Designed to be run Post-EOD.The rule can include or exclude Accounts that have zero and/or Null values for the anticipated fields associated with the configured comparison type.  
To include Accounts in the analysis that have 0 in the anticipated value fields for the selected comparison, set the IgnoreZeroAnticipatedValues parameter to 0; to exclude these accounts set the parameter to 1; default value is 1.
To include Accounts in the analysis that have NULL in the anticipated value fields for the selected comparison, set the IgnoreNullAnticipatedValues parameter to 0;  to exclude these accounts set the parameter to 1; default value is 1.
',
Params =
'<Params>
	<Param Name="@comparisonType" Alias="Anticipated Activity Comparison Type (1 for Cash, 2 for Wire, 3 for Check, 4 for Deposit)" Value="" DataType="Int"/>
	<Param Name="@startDate" Alias="Start Date for Look Back (MM/DD/YYYY). (Leave blank to use the most recently completed month)" Value="" DataType="String"/> 
	<Param Name="@endDate" Alias="End Date for Look Back (MM/DD/YYYY). (Leave blank to use the most recently completed month)" Value="" DataType="String"/>
	<Param Name="@minTransCount" Alias="Minimum transaction count" Value="" DataType="Int"/>
	<Param Name="@minAggTransAmt" Alias="Minimum aggregated transaction amount" Value="" DataType="Money"/>
	<Param Name="@monitorInd" Alias="Monitor Comparison Indicator: 1 for Amounts, 2 for Counts, 3 for Both" Value="" DataType="Int"/>
	<Param Name="@aggAntpDnFCntAmt" Alias="Aggregate Anticipated Domestic and Foreign Counts and Amounts (1 for Yes, 0 for No)" Value="" DataType="Int"/> 
	<Param Name="@amtTolerance" Alias="Amount Tolerance %" Value="" DataType="Int"/>  
	<Param Name="@cntTolerance" Alias="Count Tolerance %" Value="" DataType="Int"/>  
	<Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. -ALL- for All" Value="-ALL- " DataType="String"/>  
	<Param Name="@branchList" Alias="List of Branches separated by comma. -ALL- for All" Value="-ALL-" DataType="String"/>  
	<Param Name="@departmentList" Alias="List of Departments separated by comma. Use -ALL- for All" Value="-ALL-" DataType="String"/>  
	<Param Name="@acctTypesList" Alias="List of Account Types separated by comma. Use -ALL- for All" Value="-ALL-" DataType="String"/> 
	<Param Name="@custTypesList" Alias="List of Customer Types separated by comma. Use -ALL- for All" Value="-ALL-" DataType="String"/>  
	<Param Name="@actvTypesListIncl" Alias="Activity Types to include (comma separated). -ALL- for All. (Wire rule: list DOM Wire Act. Types)" Value="-ALL-" DataType="String"/> 
	<Param Name="@actvTypesListExcl" Alias="Activity Types to exclude (comma separated). -ALL- for All, -NONE- for None. (Wire rule: list FOR Wire Act. Types to include)" Value="-NONE-" DataType="String"/> 
	<Param Name="@receivedOrPay" Alias="Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both. (Determines to compare with deposit, withdrawal, or both)" Value="" DataType="Int"/>
	<Param Name="@cashType" Alias="Cash or Non-cash. (0 for Non-cash, 1 for Cash, 2 for both)" Value="" DataType="Int"/>
	<Param Name="@numDayMonitor" Alias="Number of Days to Monitor after Account Open Date (0 to ignore this parameter)" Value="0" DataType="Int"/>
	<Param Name="@ignoreZeroAntVals" Alias="Ignore Zero Anticipated Values (0 for No, 1 for Yes)" Value="1" DataType="Int"/>
	<Param Name="@ignoreNullAntVals" Alias="Ignore Null Anticipated Values (0 for No, 1 for Yes)" Value="1" DataType="Int"/>
</Params>',
LastOper = 'Prime'
WHERE WLCode = 'AtpActvCmps'
END
Go

/******************************************************************************
Procedure Name: USR_AntpActvComps
Description:	Detects accounts that had transactions using a specified activity
				type and the total of these transactions were between a designated 
				dollar amount range within 1 completed month.  Designed to be run 
				under Post-EOD.
Parameters:		@WLCode [in] SCode = Rule Code,				
				@testAlert		INT,		
				@comparisonType	INT,		-- Example 1, 2, 3, 4 for 'Cash', 'Wire', 'Check', 'Deposit' for (Anticipated Activity Comparison Cash | Wire | Check | Deposit respectively)
				@startDate		VARCHAR(10), -- = convert(Date, GetDate()), -- Convert(DateTime, Convert(VarChar, GetDate(), 101)), -- Use Tdaay's Date for default
				@endDate		VARCHAR(10),
				@minTransCount	INT,
				@minAggTransAmt MONEY,
				@monitorInd		INT = 3,	-- 1: Monitor Amount; 2: Monitor Count; 3: Both
				@aggAntpDnFCntAmt   INT = 0, -- set to 0 for handling DOM and FOR WIRE separately, set to 1 for handling them as combined ALL WIREs
				@amtTolerance	INT = 10,  -- default to 10 % if necessary
				@cntTolerance	INT = 10,  -- default to 10 % if necessary
				@riskClassList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All
				@branchList		VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
				@departmentList VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
				@acctTypesList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All
				@custTypesList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
				--@actvTypesList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
				@actvTypesListIncl VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.(For Wire rule list Domestic Wire Activity Types to be included.)
				@actvTypesListExcl VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All or -None- for None. (For Wire rule list Foreign Wire Activity Types to be included.)
				@receivedOrPay	INT,			--  Use 1R-2P-3Both 
				@cashType		smallint,		--  Specifies Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both
				@numDayMonitor	INT = 0,		--  Number of Days to Monitor after Account Open Date (Use 0 to Ignore this parameter)				
				@ignoreZeroAntVals  INT = 1     --  Alias=Ignore Zero Anticipated Values (0 for No, 1 for Yes), Default Value = 1 to ignore
				@ignoreNullAntVals  INT = 1     --  Alias=Ignore Null Anticipated Values (0 for No, 1 for Yes), Default Value = 1 to ignore
				[Return] INT as the error status code or 0 for success.
******************************************************************************/

if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_AntpActvComps]') 	
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure USR_AntpActvComps	
Go

CREATE PROCEDURE [dbo].[USR_AntpActvComps] ( 
	@WLCode 		SCode,
	@testAlert		INT,		
	@comparisonType	INT,		-- Example 1, 2, 3, 4 for 'Cash', 'Wire', 'Check', 'Deposit' for (Anticipated Activity Comparison Cash | Wire | Check | Deposit respectively)
	@startDate		VARCHAR(10), -- = convert(Date, GetDate()), -- Convert(DateTime, Convert(VarChar, GetDate(), 101)), -- Use Tdaay's Date for default
	@endDate		VARCHAR(10),
	@minTransCount	INT,
	@minAggTransAmt MONEY,
	@monitorInd		INT,  	 -- 1: Monitor Amount; 2: Monitor Count; 3: Both
	@aggAntpDnFCntAmt   INT, -- set to 0 for handling DOM and FOR WIRE separately, set to 1 for handling them as combined ALL WIREs
	@amtTolerance	INT,			-- = 10,  default to 10 % if necessary
	@cntTolerance	INT,			-- = 10,  default to 10 % if necessary
	@riskClassList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All
	@branchList		VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
	@departmentList VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
	@acctTypesList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All
	@custTypesList  VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.
	@actvTypesListIncl VARCHAR(8000) = '-ALL-',  --  Use -ALL- for All.(For Wire rule list Domestic Wire Activity Types to be included.)
	@actvTypesListExcl VARCHAR(8000) = '-NONE-',  --  Use -ALL- for All or -None- for None. (For Wire rule list Foreign Wire Activity Types to be included.)
	@receivedOrPay	INT,			--  Use 1R-2P-3Both 
	@cashType		smallint,		-- Specifies Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both
	@numDayMonitor	INT = 0,		--  Number of Days to Monitor after Account Open Date (Use 0 to Ignore this parameter)
	@ignoreZeroAntVals INT = 1,      --  Ignore Zero Anticipated Values (0 for No, 1 for Yes), Default Value = 1 to ignore
	@ignoreNullAntVals INT = 1      --  Ignore Null Anticipated Values (0 for No, 1 for Yes), Default Value = 1 to ignore
)
AS
	
/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@fromDate INT,
 	@toDate INT,
	@strFromDate VARCHAR(10),
	@strToDate VARCHAR(10),
	@strBetweenDates VARCHAR(50),
 	@firstMonitorDate INT, 
 	@strSubject VARCHAR(9),
 	@strExceed1 VARCHAR(26),
 	@strExceed2 VARCHAR(39),
 	@strDueTo VARCHAR(8),
 	@nom INT,					
 	@nod INT,
 	@date2Monitor INT,			
	@STARTALRTDATE DATETIME,
	@ENDALRTDATE DATETIME, 	
	@iZAV bit,
	@iNAV bit,
	@tranAmt MONEY,
	@tranCnt INT,
	@trnCnt INT,
	@Cust VarChar(35),
	@OwnerBranch VarChar(11),
	@OwnerDept VarChar(11),
	@nMonthComparison INT,
	@CurrDate GenericDate,
	@wrkDate DATETIME

SET @strSubject = 'Account: '
SET @strExceed1 = ' exceeded the anticipated '
SET @strExceed2 = ' and exceeded the anticipated number of'
SET @strDueTo = ' due to ' 

DECLARE @SetDate DATETIME

DECLARE @DSQA TABLE (
	[TranNo] [int] NULL,
	[Cust] [varchar](35) NULL,
	[Account] [varchar](35) NULL,
	[ActivityType] [int] NULL,
	[ComparisonType] [int] NULL,
	[CashTran] [int] NULL,
	[BaseAmt] [money] NULL,
	[BookDate] [int] NULL,
	[StartMonitorDate] [int] NULL,
	[FirstMonitorDate] [int] NULL,
	[EndMonitorDate] [int] NULL,
	[RecvPay] [smallint] NULL,
	[Curr] [char](3) NULL,
	[Branch] [varchar](11) NULL,
	[Dept] [varchar](11) NULL
)

DECLARE @DSAACTolerated TABLE(
	[Account] [varchar](35) NULL,
-- Cash Tolerated Account Fields	
	[TolTotCashWithDAmtMnth] [money] NULL,
	[TolNoOfCashWithDMnth] [int] NULL,
	[TolTotCashDepositAmtMnth] [money] NULL,
	[TolNoOfCashDepositMnth] [int] NULL,
-- DOM-Wire Tolerated Account Fields	
	[TolTotDomInWireAmtMnth] [money] NULL,
	[TolNoOfDomInWireMnth] [int] NULL,
	[TolTotDomOutWireAmtMnth] [money] NULL,
	[TolNoOfDomOutWireMnth] [int] NULL,
-- FOR-Wire Tolerated Account Fields	
	[TolTotForInWireAmtMnth] [money] NULL,
	[TolNoOfForInWireMnth] [int] NULL,
	[TolTotForOutWireAmtMnth] [money] NULL,
	[TolNoOfForOutWireMnth] [int] NULL,	
-- ALL-Wire Tolerated Account Fields
	[TolTotAllInWireAmtMnth] [money] NULL,
	[TolNoOfAllInWireMnth] [int] NULL,
	[TolTotAllOutWireAmtMnth] [money] NULL,
	[TolNoOfAllOutWireMnth] [int] NULL,
-- Check Tolerated Account Fields	
	[TolTotCheckWAmtMnth] [money] NULL,
	[TolNoOfCheckWMnth] [int] NULL,
-- Deposit Tolerated Account Fields	
	[TolTotDepositAmtMnth] [money] NULL,
	[TolNoOfDepositMnth] [int] NULL	
)

DECLARE @DSTT TABLE (
	[Account] [varchar](35) NULL,
	[ComparisonType] [int] NULL,	
	[Cust] [varchar](35) NULL,
	[AACResultDesc] [varchar](1000) NULL,
	[RecvPay] [int] NULL,
	[TransAmt] [money] NULL,
	[TransCnt] [int] NULL,
	[boundaryAmt] [money] NULL,
	[boundaryCnt] [int] NULL,
	[AACComparisonTypeDetail] [varchar](100) NULL
)

DECLARE @CustomerExemptionsList VARCHAR(8000)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	BEGIN TRAN USR_AntpActvComps
ELSE
	SAVE TRAN USR_AntpActvComps

SELECT @description = [Desc], @WLType = WLType ,
	@SetDate =
	CASE
		WHEN UseSysDate = 2 THEN                       
			(SELECT BusDate FROM dbo.SysParam)	-- use business date
		ELSE
            GetDate()	-- use system date
	END
FROM dbo.WatchList WITH (NOLOCK)
WHERE WLCode = @WLCode

If( ( @startDate is null ) OR ( ltrim(rtrim(@startDate)) = '' )   
	AND ( @endDate is null ) OR ( ltrim(rtrim(@endDate)) = '' ) ) 
	BEGIN
		--If Both Start and End dates are blank then default dates to previous month per the business or system date determined by @SetDate”. 
		DECLARE @nowDate DATETIME = (SELECT DATEADD(M, DATEDIFF(M, 0, @SetDate), 0))
		DECLARE @firstDay DATETIME = (SELECT DATEADD(M, -1, @nowDate))
		DECLARE @lastDay DATETIME = (SELECT DATEADD(D, -1, @nowDate))
		SET @fromDate = dbo.ConvertSqlDateToInt(@firstDay)
		SET @toDate = dbo.ConvertSqlDateToInt(@lastDay)
		SET @strFromDate = CONVERT(varchar(10), CONVERT(date, CONVERT(varchar(8), @fromDate), 112), 101)
		SET @strToDate = CONVERT(varchar(10), CONVERT(date, CONVERT(varchar(8), @toDate), 112), 101)		
	END
ELSE If	( ( @startDate is not null ) AND ( ltrim(rtrim(@startDate)) <> '' ) 
		AND ( @endDate is not null ) AND ( ltrim(rtrim(@endDate)) <> '' ) ) 
	BEGIN
		--If Both dates are populated then use the provided dates	
		SET @fromDate = dbo.ConvertSqlDateToInt(@startDate)
		SET @toDate = dbo.ConvertSqlDateToInt(@endDate)
		SET @strFromDate = CONVERT(varchar(10), CONVERT(date, CONVERT(varchar(8), @fromDate), 112), 101)
		SET @strToDate = CONVERT(varchar(10), CONVERT(date, CONVERT(varchar(8), @toDate), 112), 101)		
	END
ELSE
BEGIN
	--If only one of the Start or End Date is BLANK, then do nothing per Business decision
	GOTO EndOfProc
END

SET @strBetweenDates = ' between the dates of ' + @strFromDate + ' and ' +  @strToDate +'.'

SELECT  @firstMonitorDate = Case WHEN  @numDayMonitor <= 0 THEN 18000101 ELSE DBO.CONVERTSQLDATETOINT( DATEADD(D, -@numDayMonitor, CONVERT(VARCHAR, GETDATE() ) ) ) END
	-- if user-defined @numDayMonitor = 0 entered, we will SET @firstMonitorDate = 18000101 as the default integer value of the date back to Jan 1, 1800.
SELECT @riskClassList     = dbo.BSA_fnListParams(@riskClassList)
SELECT @branchList        = dbo.BSA_fnListParams(@branchList)
SELECT @departmentList    = dbo.BSA_fnListParams(@departmentList)
SELECT @acctTypesList     = dbo.BSA_fnListParams(@acctTypesList)
SELECT @actvTypesListIncl = dbo.BSA_fnListParams(@actvTypesListIncl)
SELECT @actvTypesListExcl = dbo.BSA_fnListParams(@actvTypesListExcl)
SELECT @custTypesList     = dbo.BSA_fnListParams(@custTypesList)
SELECT @comparisonType	  = ISNULL(@comparisonType, 0) 		-- Default to not comparing
SELECT @minTransCount	  = ISNULL(@minTransCount, 0) 		-- Default as ignore this parameter
SELECT @minAggTransAmt 	  = ISNULL(@minAggTransAmt, 0.00)	-- Default as ignore this parameter 		
SELECT @monitorInd		  = ISNULL(@monitorInd, 2) 			-- Default to monitor count
SELECT @aggAntpDnFCntAmt  = ISNULL(@aggAntpDnFCntAmt, 0) 	-- Deafult as handling DOM and FOR WIRE separately
SELECT @amtTolerance	  = ISNULL(@amtTolerance, 0)		-- Default as ignore this parameter 
SELECT @cntTolerance	  = ISNULL(@cntTolerance, 0) 		-- Default as ignore this parameter 
SELECT @iZAV = Case WHEN @ignoreZeroAntVals = 1  THEN 1 WHEN @ignoreZeroAntVals = 0 THEN 0 ELSE 1 END
SELECT @iNAV = Case WHEN @ignoreNullAntVals = 1  THEN 1 WHEN @ignoreNullAntVals = 0 THEN 0 ELSE 1 END
IF (@cashType = 2)
	SET @cashType = NULL
	
IF (@receivedOrPay = 3)
	SET @receivedOrPay = NULL

IF (ISNULL(@actvTypesListExcl,'') = '' OR UPPER(ISNULL(@actvTypesListExcl,'-NONE-')) = '-NONE-')
	SET @actvTypesListExcl = NULL

If @comparisonType <> 2 -- Cash, Check, or Deposit ActivityType
	BEGIN
		INSERT INTO @DSQA
		SELECT 	 Ach.TranNo
				,Ach.Cust
				,Ach.account
				,Ach.Type AS ActivityType
				,@comparisonType AS ComparisonType
				,Ach.CashTran
				,Ach.baseAmt
				,Ach.bookdate
				,@fromDate AS StartMonitorDate
				,@firstMonitorDate As FirstMonitorDate
				,@toDate AS EndMonitorDate
				,Ach.recvpay 
				,Ach.Curr		
				,Ach.Branch
				,Ach.Dept
				FROM 	ActivityHist Ach WITH (NOLOCK)
				INNER	JOIN Customer WITH (NOLOCK) ON Ach.cust = customer.id 
				INNER   JOIN Account WITH (NOLOCK) ON Ach.account = Account.id		
				WHERE  ( (Account.OpenDate = '') OR (Account.OpenDate Is NULL) OR (dbo.ConvertSqlDateToInt(Account.OpenDate) >= @firstMonitorDate) ) 
					AND (BookDate >= @fromDate) AND (BookDate <= @toDate)
					AND (Ach.RecvPay = ISNULL(@receivedOrPay, Ach.RecvPay))			
					AND (Ach.CASHTRAN = ISNULL(@cashType, Ach.CASHTRAN))	
					AND ((@riskClassList IS NULL OR 
						CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0)) 
					AND ((@branchList IS NULL  OR 
						CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Ach.branch,''))) + ',', @branchList) > 0))		
					AND ((@departmentList IS NULL OR 
						CHARINDEX(',' + CONVERT(VARCHAR,  Ach.dept) + ',',@departmentList) > 0))		
					AND ((@acctTypesList IS NULL OR 
						CHARINDEX(',' + LTRIM(RTRIM(Account.type)) + ',', @acctTypesList ) > 0))
					AND ((@custTypesList IS NULL OR
						CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @custTypesList ) > 0))
					AND (@actvTypesListIncl IS NULL OR 
						CHARINDEX(',' + CONVERT(VARCHAR, Ach.Type) + ',', @actvTypesListIncl ) > 0)
		IF @@ROWCOUNT = 0 GOTO EndOfProc						
	END
ELSE 
IF @comparisonType = 2 -- Wire ActivityType
	BEGIN
		INSERT INTO @DSQA	
		SELECT 	 Ach.TranNo
				,Ach.Cust
				,Ach.account
				,Ach.Type AS ActivityType
				,@comparisonType AS ComparisonType
				,Ach.CashTran
				,Ach.baseAmt
				,Ach.bookdate
				,@fromDate AS StartMonitorDate
				,@firstMonitorDate As FirstMonitorDate				
				,@toDate AS EndMonitorDate
				,Ach.recvpay 
				,Ach.Curr		
				,Ach.Branch
				,Ach.Dept
				FROM 	ActivityHist Ach WITH (NOLOCK)
				INNER	JOIN Customer WITH (NOLOCK) ON Ach.cust = customer.id 
				INNER   JOIN Account WITH (NOLOCK) ON Ach.account = Account.id
				WHERE  ( (Account.OpenDate = '') OR (Account.OpenDate Is NULL) OR (dbo.ConvertSqlDateToInt(Account.OpenDate) >= @firstMonitorDate) ) 
					AND (BookDate >= @fromDate) AND (BookDate <= @toDate)
					AND (Ach.RecvPay = ISNULL(@receivedOrPay, Ach.RecvPay))			
					AND (Ach.CASHTRAN = ISNULL(@cashType, Ach.CASHTRAN))	
					AND ((@riskClassList IS NULL OR 
						CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0)) 
					AND ((@branchList IS NULL  OR 
						CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Ach.branch,''))) + ',', @branchList) > 0))		
					AND ((@departmentList IS NULL OR 
						CHARINDEX(',' + CONVERT(VARCHAR,  Ach.dept) + ',',@departmentList) > 0))		
					AND ((@acctTypesList IS NULL OR 
						CHARINDEX(',' + LTRIM(RTRIM(Account.type)) + ',', @acctTypesList ) > 0))
					AND ((@custTypesList IS NULL OR @actvTypesListExcl IS NULL OR
						CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @custTypesList ) > 0))
					AND (@actvTypesListIncl IS NULL OR  @actvTypesListExcl IS NULL OR
						CHARINDEX(',' + CONVERT(VARCHAR, Ach.Type) + ',', @actvTypesListIncl ) > 0
						OR
						CHARINDEX(',' + CONVERT(VARCHAR, Ach.Type) + ',', @actvTypesListExcl ) > 0)
		IF @@ROWCOUNT = 0 GOTO EndOfProc						

	END

If @comparisonType <> 2
	BEGIN
		If EXISTS ( SELECT * FROM  @DSQA	
			WHERE CHARINDEX(',' + CONVERT(VARCHAR, ActivityType) + ',', @actvTypesListExcl )  > 0
		)
		If (@actvTypesListExcl IS NOT NULL)
			DELETE FROM @DSQA WHERE CHARINDEX(',' + CONVERT(VARCHAR, ActivityType) + ',', @actvTypesListExcl )  > 0
	END
	IF @iNAV = 1
	BEGIN
		Insert Into @DSAACTolerated
		SELECT DISTINCT 		
				 Q.Account
				,TotCashWithDAmtMnth * (1.0 + @amtTolerance/100.0)  AS TolTotCashWithDAmtMnth
				,CONVERT(INT, NoOfCashWithDMnth * (1.0 + @cntTolerance/100.0) + 0.5 ) AS TolNoOfCashWithDMnth
				,TotCashDepositAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotCashDepositAmtMnth
				,CONVERT(INT, NoOfCashDepositMnth * (1.0 + @cntTolerance/100.0) + 0.5 )AS TolNoOfCashDepositMnth
				,TotDomInWireAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotDomInWireAmtMnth
				,CONVERT(INT, NoOfDomInWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ) AS TolNoOfDomInWireMnth
				,TotDomOutWireAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotDomOutWireAmtMnth
				,CONVERT(INT, NoOfDomOutWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ) AS TolNoOfDomOutWireMnth        
				,TotForInWireAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotForInWireAmtMnth
				,CONVERT(INT, NoOfForInWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ) AS TolNoOfForInWireMnth
				,TotForOutWireAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotForOutWireAmtMnth
				,CONVERT(INT, NoOfForOutWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ) AS TolNoOfForOutWireMnth
				,TolTotAllInWireAmtMnth = Case 
					When ( TotDomInWireAmtMnth Is Not NULL AND TotForInWireAmtMnth Is Not NULL ) Then ( TotDomInWireAmtMnth + TotForInWireAmtMnth ) * (1.0 + @amtTolerance/100.0) 
					When ( TotDomInWireAmtMnth Is NULL AND TotForInWireAmtMnth Is NULL ) Then  NULL
					Else ( IsNULL(TotDomInWireAmtMnth, 0.0) + IsNULL(TotForInWireAmtMnth, 0.0) ) * (1.0 + @amtTolerance/100.0) END
				,TolNoOfAllInWireMnth = Case
					When ( NoOfDomInWireMnth Is Not NULL AND NoOfForInWireMnth Is Not NULL ) Then CONVERT(INT, ( NoOfDomInWireMnth + NoOfForInWireMnth ) * (1.0 + @cntTolerance/100.0) + 0.5 ) 
					When ( NoOfDomInWireMnth Is NULL AND NoOfForInWireMnth Is NULL ) Then NULL
					Else CONVERT(INT, ( IsNULL(NoOfDomInWireMnth,0) + IsNULL(NoOfForInWireMnth,0) ) * (1.0 + @cntTolerance/100.0) + 0.5 ) END
				,TolTotAllOutWireAmtMnth = Case 
					When ( TotDomOutWireAmtMnth Is Not Null AND TotForOutWireAmtMnth Is Not Null) Then ( TotDomOutWireAmtMnth + TotForOutWireAmtMnth ) * (1.0 + @amtTolerance/100.0)
					When ( TotDomOutWireAmtMnth Is Null AND TotForOutWireAmtMnth Is Null) Then NULL
					Else ( IsNULL(TotDomOutWireAmtMnth,0.0) + IsNULL(TotForOutWireAmtMnth, 0.0) ) * (1.0 + @amtTolerance/100.0)	END
				,TolNoOfAllOutWireMnth = Case
					When ( NoOfDomOutWireMnth Is Not Null AND NoOfForOutWireMnth Is Not Null ) Then CONVERT(INT, ( NoOfDomOutWireMnth + NoOfForOutWireMnth ) * (1.0 + @cntTolerance/100.0) + 0.5 )
					When ( NoOfDomOutWireMnth Is Null AND NoOfForOutWireMnth Is Null ) Then Null
					Else CONVERT(INT, ( IsNULL(NoOfDomOutWireMnth,0) + IsNULL(NoOfForOutWireMnth,0) ) * (1.0 + @cntTolerance/100.0) + 0.5 ) END
				,TotCheckWAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotCheckWAmtMnth
				,CONVERT(INT, NoOfCheckWMnth * (1.0 + @cntTolerance/100.0) + 0.5 ) AS TolNoOfCheckWMnth
				,TotDepositAmtMnth * (1.0 + @amtTolerance/100.0) AS TolTotDepositAmtMnth
				,CONVERT(INT, NoOfDepositMnth * (1.0 + @cntTolerance/100.0) + 0.5 )  AS TolNoOfDepositMnth		
		FROM	Account A WITH (NOLOCK) INNER JOIN @DSQA Q
		ON		A.Id = Q.Account
	END
	ELSE
	BEGIN
		IF @iNAV = 0 and @iZAV = 0
		BEGIN
			Insert Into @DSAACTolerated
			SELECT DISTINCT 		
				 Q.Account
				,IsNULL( TotCashWithDAmtMnth * (1.0 + @amtTolerance/100.0), 0.0) AS TolTotCashWithDAmtMnth
				,IsNULL( CONVERT(INT, NoOfCashWithDMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfCashWithDMnth
				,IsNULL( TotCashDepositAmtMnth * (1.0 + @amtTolerance/100.0),0.0) AS TolTotCashDepositAmtMnth
				,IsNULL( CONVERT(INT, NoOfCashDepositMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfCashDepositMnth
				,IsNULL( TotDomInWireAmtMnth * (1.0 + @amtTolerance/100.0),0.0) AS TolTotDomInWireAmtMnth
				,IsNULL( CONVERT(INT, NoOfDomInWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfDomInWireMnth
				,IsNULL( TotDomOutWireAmtMnth * (1.0 + @amtTolerance/100.0),0.0) AS TolTotDomOutWireAmtMnth
				,IsNULL( CONVERT(INT, NoOfDomOutWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfDomOutWireMnth   
				,IsNULL( TotForInWireAmtMnth * (1.0 + @amtTolerance/100.0),0.0) AS TolTotForInWireAmtMnth
				,IsNULL( CONVERT(INT, NoOfForInWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfForInWireMnth
				,IsNULL( TotForOutWireAmtMnth * (1.0 + @amtTolerance/100.0),0.0)  AS TolTotForOutWireAmtMnth
				,IsNULL( CONVERT(INT, NoOfForOutWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfForOutWireMnth
				,TolTotAllInWireAmtMnth = Case 
					When ( TotDomInWireAmtMnth Is Not NULL AND TotForInWireAmtMnth Is Not NULL ) Then ( TotDomInWireAmtMnth + TotForInWireAmtMnth ) * (1.0 + @amtTolerance/100.0) 
					When ( TotDomInWireAmtMnth Is NULL AND TotForInWireAmtMnth Is NULL ) Then  0.0
					Else ( IsNULL(TotDomInWireAmtMnth, 0.0) + IsNULL(TotForInWireAmtMnth, 0.0) ) * (1.0 + @amtTolerance/100.0) END
				,TolNoOfAllInWireMnth = Case
					When ( NoOfDomInWireMnth Is Not NULL AND NoOfForInWireMnth Is Not NULL ) Then CONVERT(INT, ( NoOfDomInWireMnth + NoOfForInWireMnth ) * (1.0 + @cntTolerance/100.0) + 0.5 ) 
					When ( NoOfDomInWireMnth Is NULL AND NoOfForInWireMnth Is NULL ) Then 0
					Else CONVERT(INT, ( IsNULL(NoOfDomInWireMnth,0) + IsNULL(NoOfForInWireMnth,0) ) * (1.0 + @cntTolerance/100.0) + 0.5 ) END
				,TolTotAllOutWireAmtMnth = Case 
					When ( TotDomOutWireAmtMnth Is Not Null AND TotForOutWireAmtMnth Is Not Null) Then ( TotDomOutWireAmtMnth + TotForOutWireAmtMnth ) * (1.0 + @amtTolerance/100.0)
					When ( TotDomOutWireAmtMnth Is Null AND TotForOutWireAmtMnth Is Null) Then 0.0
					Else ( IsNULL(TotDomOutWireAmtMnth,0.0) + IsNULL(TotForOutWireAmtMnth, 0.0) ) * (1.0 + @amtTolerance/100.0)	END
				,TolNoOfAllOutWireMnth = Case
					When ( NoOfDomOutWireMnth Is Not Null AND NoOfForOutWireMnth Is Not Null ) Then CONVERT(INT, ( NoOfDomOutWireMnth + NoOfForOutWireMnth ) * (1.0 + @cntTolerance/100.0) + 0.5 )
					When ( NoOfDomOutWireMnth Is Null AND NoOfForOutWireMnth Is Null ) Then 0
					Else CONVERT(INT, ( IsNULL(NoOfDomOutWireMnth,0) + IsNULL(NoOfForOutWireMnth,0) ) * (1.0 + @cntTolerance/100.0) + 0.5 ) END
				,IsNULL( TotCheckWAmtMnth * (1.0 + @amtTolerance/100.0),0.0) AS TolTotCheckWAmtMnth
				,IsNULL( CONVERT(INT, NoOfCheckWMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfCheckWMnth
				,IsNULL( TotDepositAmtMnth * (1.0 + @amtTolerance/100.0),0.0) AS TolTotDepositAmtMnth
				,IsNULL( CONVERT(INT, NoOfDepositMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfDepositMnth	
				FROM Account A WITH (NOLOCK) INNER JOIN @DSQA Q
				ON	A.Id = Q.Account
		END
		ELSE
		BEGIN
			Insert Into @DSAACTolerated
			SELECT DISTINCT 		
				 Q.Account
				,IsNULL( TotCashWithDAmtMnth * (1.0 + @amtTolerance/100.0), 0.0) AS TolTotCashWithDAmtMnth
				,IsNULL( CONVERT(INT, NoOfCashWithDMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfCashWithDMnth
				,IsNULL( TotCashDepositAmtMnth * (1.0 + @amtTolerance/100.0),0.0) AS TolTotCashDepositAmtMnth
				,IsNULL( CONVERT(INT, NoOfCashDepositMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfCashDepositMnth
				,IsNULL( TotDomInWireAmtMnth * (1.0 + @amtTolerance/100.0),0.0) AS TolTotDomInWireAmtMnth
				,IsNULL( CONVERT(INT, NoOfDomInWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfDomInWireMnth
				,IsNULL( TotDomOutWireAmtMnth * (1.0 + @amtTolerance/100.0),0.0) AS TolTotDomOutWireAmtMnth
				,IsNULL( CONVERT(INT, NoOfDomOutWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfDomOutWireMnth   
				,IsNULL( TotForInWireAmtMnth * (1.0 + @amtTolerance/100.0),0.0) AS TolTotForInWireAmtMnth
				,IsNULL( CONVERT(INT, NoOfForInWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfForInWireMnth
				,IsNULL( TotForOutWireAmtMnth * (1.0 + @amtTolerance/100.0),0.0)  AS TolTotForOutWireAmtMnth
				,IsNULL( CONVERT(INT, NoOfForOutWireMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfForOutWireMnth
				,TolTotAllInWireAmtMnth = Case 
					When ( TotDomInWireAmtMnth Is Not NULL AND TotForInWireAmtMnth Is Not NULL ) Then ( TotDomInWireAmtMnth + TotForInWireAmtMnth ) * (1.0 + @amtTolerance/100.0) 
					When ( TotDomInWireAmtMnth Is NULL AND TotForInWireAmtMnth Is NULL ) Then  0.0
					Else ( IsNULL(TotDomInWireAmtMnth, 0.0) + IsNULL(TotForInWireAmtMnth, 0.0) ) * (1.0 + @amtTolerance/100.0) END
				,TolNoOfAllInWireMnth = Case
					When ( NoOfDomInWireMnth Is Not NULL AND NoOfForInWireMnth Is Not NULL ) Then CONVERT(INT, ( NoOfDomInWireMnth + NoOfForInWireMnth ) * (1.0 + @cntTolerance/100.0) + 0.5 ) 
					When ( NoOfDomInWireMnth Is NULL AND NoOfForInWireMnth Is NULL ) Then 0
					Else CONVERT(INT, ( IsNULL(NoOfDomInWireMnth,0) + IsNULL(NoOfForInWireMnth,0) ) * (1.0 + @cntTolerance/100.0) + 0.5 ) END
				,TolTotAllOutWireAmtMnth = Case 
					When ( TotDomOutWireAmtMnth Is Not Null AND TotForOutWireAmtMnth Is Not Null) Then ( TotDomOutWireAmtMnth + TotForOutWireAmtMnth ) * (1.0 + @amtTolerance/100.0)
					When ( TotDomOutWireAmtMnth Is Null AND TotForOutWireAmtMnth Is Null) Then 0.0
					Else ( IsNULL(TotDomOutWireAmtMnth,0.0) + IsNULL(TotForOutWireAmtMnth, 0.0) ) * (1.0 + @amtTolerance/100.0)	END
				,TolNoOfAllOutWireMnth = Case
					When ( NoOfDomOutWireMnth Is Not Null AND NoOfForOutWireMnth Is Not Null ) Then CONVERT(INT, ( NoOfDomOutWireMnth + NoOfForOutWireMnth ) * (1.0 + @cntTolerance/100.0) + 0.5 )
					When ( NoOfDomOutWireMnth Is Null AND NoOfForOutWireMnth Is Null ) Then 0
					Else CONVERT(INT, ( IsNULL(NoOfDomOutWireMnth,0) + IsNULL(NoOfForOutWireMnth,0) ) * (1.0 + @cntTolerance/100.0) + 0.5 ) END
				,IsNULL( TotCheckWAmtMnth * (1.0 + @amtTolerance/100.0),0.0) AS TolTotCheckWAmtMnth
				,IsNULL( CONVERT(INT, NoOfCheckWMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfCheckWMnth
				,IsNULL( TotDepositAmtMnth * (1.0 + @amtTolerance/100.0),0.0) AS TolTotDepositAmtMnth
				,IsNULL( CONVERT(INT, NoOfDepositMnth * (1.0 + @cntTolerance/100.0) + 0.5 ),0) AS TolNoOfDepositMnth	
				FROM Account A WITH (NOLOCK) INNER JOIN @DSQA Q
				ON	A.Id = Q.Account
				WHERE (((TotCashDepositAmtMnth Is NULL or TotCashDepositAmtMnth <> 0.00) AND (NoOfCashDepositMnth Is NULL or NoOfCashDepositMnth<>0)) AND ( (TotCashWithDAmtMnth Is NULL or TotCashWithDAmtMnth<>0.00) AND (NoOfCashWithDMnth Is NULL or NoOfCashWithDMnth<>0 ))) 
					OR (((TotDomInWireAmtMnth Is NULL or TotDomInWireAmtMnth<>0.00) AND (NoOfDomInWireMnth Is NULL or NoOfDomInWireMnth<>0))  AND  ((TotDomOutWireAmtMnth Is NULL or TotDomOutWireAmtMnth<>0.00) AND (NoOfDomOutWireMnth Is NULL or NoOfDomOutWireMnth<>0 )) 
					AND  ((TotForInWireAmtMnth Is NULL or TotForInWireAmtMnth<>0.00) AND (NoOfForInWireMnth Is NULL or NoOfForInWireMnth<>0)) AND  ((TotForOutWireAmtMnth Is NULL or TotForOutWireAmtMnth<>0.00) AND (NoOfForOutWireMnth Is NULL or NoOfForOutWireMnth<>0 )))
					OR (((TotCheckWAmtMnth Is NULL or TotCheckWAmtMnth<>0.00) AND (NoOfCheckWMnth Is NULL or NoOfCheckWMnth<>0)))
					OR (((TotDepositAmtMnth Is NULL or TotDepositAmtMnth<>0.00) AND (NoOfDepositMnth Is NULL or NoOfDepositMnth<>0)))
		END
	END

               
--
if @comparisonType = 1
	Begin
		Delete From @DSAACTolerated									 
		Where ( TolTotCashDepositAmtMnth Is NULL AND TolNoOfCashDepositMnth Is NULL ) 
		  AND ( TolTotCashWithDAmtMnth Is NULL AND TolNoOfCashWithDMnth Is NULL )
	
		INSERT INTO @DSTT 					   
		SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
		  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
		  + CASE WHEN ( (( T.TolTotCashDepositAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolTotCashDepositAmtMnth <> 0.00)) AND ( T.TolTotCashDepositAmtMnth Is Not NULL ) AND (( T.TolNoOfCashDepositMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolNoOfCashDepositMnth <> 0 )) AND ( T.TolNoOfCashDepositMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashDepositAmtMnth) AND (COUNT(Q.TranNo) > T.TolNoOfCashDepositMnth) AND (COUNT(Q.TranNo) >= @minTransCount)
					THEN 'cash deposit transactions of $' + CONVERT(varchar, T.TolTotCashDepositAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
				      + @strExceed2 + ' cash deposit transactions of ' + CONVERT(VARCHAR, T.TolNoOfCashDepositMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
				 WHEN ( (( T.TolTotCashDepositAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolTotCashDepositAmtMnth <> 0.00)) AND ( T.TolTotCashDepositAmtMnth Is Not NULL ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashDepositAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) ) 
					THEN 'cash deposit transactions of $' + CONVERT(varchar, T.TolTotCashDepositAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
				 WHEN ( (( T.TolNoOfCashDepositMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolNoOfCashDepositMnth <> 0 )) AND ( T.TolNoOfCashDepositMnth Is Not NULL ) AND (COUNT(Q.TranNo) >= @minTransCount) AND (COUNT(Q.TranNo) > T.TolNoOfCashDepositMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN 'number of cash deposit transactions of ' + CONVERT(VARCHAR, T.TolNoOfCashDepositMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
			END + @strDueTo 
		  + CASE WHEN ( (( T.TolTotCashDepositAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolTotCashDepositAmtMnth <> 0.00)) AND ( T.TolTotCashDepositAmtMnth Is Not NULL ) AND (( T.TolNoOfCashDepositMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolNoOfCashDepositMnth <> 0 )) AND ( T.TolNoOfCashDepositMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashDepositAmtMnth) AND (COUNT(Q.TranNo) > T.TolNoOfCashDepositMnth) AND (COUNT(Q.TranNo) >= @minTransCount)
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' cash deposit transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( T.TolTotCashDepositAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolTotCashDepositAmtMnth <> 0.00)) AND ( T.TolTotCashDepositAmtMnth Is Not NULL ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashDepositAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'the total cash deposit transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( T.TolNoOfCashDepositMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolNoOfCashDepositMnth <> 0 )) AND ( T.TolNoOfCashDepositMnth Is Not NULL ) AND (COUNT(Q.TranNo) >= @minTransCount) AND (COUNT(Q.TranNo) > T.TolNoOfCashDepositMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' cash deposit transactions'
			END + @strBetweenDates
			, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
			, Case WHEN T.TolTotCashDepositAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotCashDepositAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
			, CASE WHEN T.TolNoOfCashDepositMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfCashDepositMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
			, ' cash ' + Case WHEN Q.RecvPay = 1 THEN 'deposit ' ELSE 'withdrawal ' END + 'transactions' AS AACComparisonTypeDetail  			
		FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
		WHERE Q.CashTran = 1 AND (Q.RecvPay = 1) 
		GROUP BY Q.Account, Q.RecvPay, T.TolTotCashDepositAmtMnth, T.TolNoOfCashDepositMnth
		HAVING  
		(
				( (( T.TolTotCashDepositAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolTotCashDepositAmtMnth <> 0.00)) AND ( T.TolTotCashDepositAmtMnth Is Not NULL ) AND (( T.TolNoOfCashDepositMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolNoOfCashDepositMnth <> 0 )) AND ( T.TolNoOfCashDepositMnth Is Not NULL ) )
				AND ( @monitorInd = 3 ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND ( SUM(Q.BaseAmt) > T.TolTotCashDepositAmtMnth AND COUNT(Q.TranNo) > T.TolNoOfCashDepositMnth AND (COUNT(Q.TranNo) >= @minTransCount) )
			OR
				( (( T.TolTotCashDepositAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolTotCashDepositAmtMnth <> 0.00)) AND ( T.TolTotCashDepositAmtMnth Is Not NULL ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashDepositAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) ) 
			OR
				( (( T.TolNoOfCashDepositMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolNoOfCashDepositMnth <> 0 )) AND ( T.TolNoOfCashDepositMnth Is Not NULL ) AND (COUNT(Q.TranNo) >= @minTransCount) AND (COUNT(Q.TranNo) > T.TolNoOfCashDepositMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
		)
		UNION
		SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
		  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
		  + CASE WHEN ( (( T.TolTotCashWithDAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolTotCashWithDAmtMnth <> 0.00)) AND ( T.TolTotCashWithDAmtMnth Is Not NULL ) AND (( T.TolNoOfCashWithDMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolNoOfCashWithDMnth <> 0 )) AND ( T.TolNoOfCashWithDMnth Is Not NULL ) )
						AND ( @monitorInd = 3 ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND ( SUM(Q.BaseAmt) > T.TolTotCashWithDAmtMnth AND (COUNT(Q.TranNo) >= @minTransCount) AND COUNT(Q.TranNo) > T.TolNoOfCashWithDMnth )
					THEN 'cash withdrawal transactions of $' + CONVERT(varchar, T.TolTotCashWithDAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
				      + @strExceed2 + ' cash withdrawal transactions of ' + CONVERT(VARCHAR, T.TolNoOfCashWithDMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
				 WHEN ( (( T.TolTotCashWithDAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolTotCashWithDAmtMnth <> 0.00)) AND ( T.TolTotCashWithDAmtMnth Is Not NULL ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashWithDAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'cash withdrawal transactions of $' + CONVERT(varchar, T.TolTotCashWithDAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
				 WHEN ( (( T.TolNoOfCashWithDMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolNoOfCashWithDMnth <> 0 )) AND ( T.TolNoOfCashWithDMnth Is Not NULL ) AND (COUNT(Q.TranNo) >= @minTransCount) AND (COUNT(Q.TranNo) > T.TolNoOfCashWithDMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN 'number of cash withdrawal transactions of ' + CONVERT(VARCHAR, T.TolNoOfCashWithDMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
			END + @strDueTo
		  + CASE WHEN ( (( T.TolTotCashWithDAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolTotCashWithDAmtMnth <> 0.00)) AND ( T.TolTotCashWithDAmtMnth Is Not NULL ) AND (( T.TolNoOfCashWithDMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolNoOfCashWithDMnth <> 0 )) AND ( T.TolNoOfCashWithDMnth Is Not NULL ) )
						AND ( @monitorInd = 3 ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND ( SUM(Q.BaseAmt) > T.TolTotCashWithDAmtMnth AND (COUNT(Q.TranNo) >= @minTransCount) AND COUNT(Q.TranNo) > T.TolNoOfCashWithDMnth )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' cash withdrawal transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( T.TolTotCashWithDAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolTotCashWithDAmtMnth <> 0.00)) AND ( T.TolTotCashWithDAmtMnth Is Not NULL ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashWithDAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'the total cash withdrawal transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( T.TolNoOfCashWithDMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolNoOfCashWithDMnth <> 0 )) AND ( T.TolNoOfCashWithDMnth Is Not NULL ) AND (COUNT(Q.TranNo) >= @minTransCount) AND (COUNT(Q.TranNo) > T.TolNoOfCashWithDMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' cash withdrawal transactions'
			END + @strBetweenDates			
			, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
			, Case WHEN T.TolTotCashWithDAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotCashWithDAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
			, CASE WHEN T.TolNoOfCashWithDMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfCashWithDMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
			, ' cash ' + Case WHEN Q.RecvPay = 1 THEN 'deposit ' ELSE 'withdrawal ' END + 'transactions' AS AACComparisonTypeDetail  
		FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
		WHERE Q.CashTran = 1 AND (Q.RecvPay = 2)
		GROUP BY Q.Account, Q.RecvPay, T.TolTotCashWithDAmtMnth, T.TolNoOfCashWithDMnth  
		HAVING 
		(
				( (( T.TolTotCashWithDAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolTotCashWithDAmtMnth <> 0.00)) AND ( T.TolTotCashWithDAmtMnth Is Not NULL ) AND (( T.TolNoOfCashWithDMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolNoOfCashWithDMnth <> 0 )) AND ( T.TolNoOfCashWithDMnth Is Not NULL ) )
				AND ( @monitorInd = 3 ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND ( SUM(Q.BaseAmt) > T.TolTotCashWithDAmtMnth AND (COUNT(Q.TranNo) >= @minTransCount) AND COUNT(Q.TranNo) > T.TolNoOfCashWithDMnth ) 
			OR
				( (( T.TolTotCashWithDAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolTotCashWithDAmtMnth <> 0.00)) AND ( T.TolTotCashWithDAmtMnth Is Not NULL ) AND (SUM(Q.BaseAmt) >= @minAggTransAmt) AND (SUM(Q.BaseAmt) > T.TolTotCashWithDAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
			OR
				( (( T.TolNoOfCashWithDMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( T.TolNoOfCashWithDMnth <> 0 )) AND ( T.TolNoOfCashWithDMnth Is Not NULL ) AND (COUNT(Q.TranNo) >= @minTransCount) AND (COUNT(Q.TranNo) > T.TolNoOfCashWithDMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
		)
	End
ELSE
if @comparisonType =  2
	Begin
		Delete From @DSAACTolerated									 
		Where  ( TolTotDomInWireAmtMnth Is NULL AND TolNoOfDomInWireMnth Is NULL )  
		  AND  ( TolTotDomOutWireAmtMnth Is NULL AND TolNoOfDomOutWireMnth Is NULL )
		  AND  ( TolTotForInWireAmtMnth Is NULL AND TolNoOfForInWireMnth Is NULL ) 
		  AND  ( TolTotForOutWireAmtMnth Is NULL AND TolNoOfForOutWireMnth Is NULL )
		  									 
		if @aggAntpDnFCntAmt = 0
			BEGIN	
				INSERT INTO @DSTT 					   			  
				SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
				  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
				  + CASE WHEN ( (( TolTotDomInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDomInWireAmtMnth <> 0.00)) AND ( TolTotDomInWireAmtMnth Is Not NULL ) AND (( TolNoOfDomInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDomInWireMnth <> 0 )) AND ( TolNoOfDomInWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDomInWireAmtMnth AND COUNT(TranNo) > TolNoOfDomInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN 'incoming DOM-wire transactions of $' + CONVERT(varchar, T.TolTotDomInWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
							  + @strExceed2 + ' incoming DOM-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfDomInWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
						 WHEN (( TolTotDomInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDomInWireAmtMnth <> 0.00)) AND (TolTotDomInWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotDomInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'incoming DOM-wire transactions of $' + CONVERT(varchar, T.TolTotDomInWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
						 WHEN (( TolNoOfDomInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDomInWireMnth <> 0 )) AND ( TolNoOfDomInWireMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfDomInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN 'number of incoming DOM-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfDomInWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
					END + @strDueTo
				  + CASE WHEN ( (( TolTotDomInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDomInWireAmtMnth <> 0.00)) AND ( TolTotDomInWireAmtMnth Is Not NULL ) AND (( TolNoOfDomInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDomInWireMnth <> 0 )) AND ( TolNoOfDomInWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDomInWireAmtMnth AND COUNT(TranNo) > TolNoOfDomInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' incoming DOM-wire transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolTotDomInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDomInWireAmtMnth <> 0.00)) AND (TolTotDomInWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotDomInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'the total incoming DOM-wire transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolNoOfDomInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDomInWireMnth <> 0 )) AND ( TolNoOfDomInWireMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfDomInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' incoming DOM-wire transactions'
					END + @strBetweenDates
					, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
					, Case WHEN T.TolTotDomInWireAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotDomInWireAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
					, CASE WHEN T.TolNoOfDomInWireMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfDomInWireMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
					, ' DOM-wire ' + Case WHEN Q.RecvPay = 1 THEN 'incoming ' ELSE 'outgoing ' END + 'transactions' AS AACComparisonTypeDetail  			
				FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
				WHERE (RecvPay = 1)
					   AND (@actvTypesListIncl IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, ActivityType) + ',', @actvTypesListIncl ) > 0)
				GROUP BY Q.Account, RecvPay, TolTotDomInWireAmtMnth, TolNoOfDomInWireMnth
				HAVING 
				(
						( (( TolTotDomInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDomInWireAmtMnth <> 0.00)) AND ( TolTotDomInWireAmtMnth Is Not NULL ) AND (( TolNoOfDomInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDomInWireMnth <> 0 )) AND ( TolNoOfDomInWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDomInWireAmtMnth AND COUNT(TranNo) > TolNoOfDomInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt) 
					OR
						(( TolTotDomInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDomInWireAmtMnth <> 0.00)) AND (TolTotDomInWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotDomInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
					OR
						(( TolNoOfDomInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDomInWireMnth <> 0 )) AND ( TolNoOfDomInWireMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfDomInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
				)
				UNION 
				SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
				  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
				  + CASE WHEN ( (( TolTotDomOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDomOutWireAmtMnth <> 0.00)) AND ( TolTotDomOutWireAmtMnth Is Not NULL ) AND (( TolNoOfDomOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDomOutWireMnth <> 0 )) AND ( TolNoOfDomOutWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDomOutWireAmtMnth AND COUNT(TranNo) > TolNoOfDomOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN 'outgoing DOM-wire transactions of $' + CONVERT(varchar, T.TolTotDomOutWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
							  + @strExceed2 + ' outgoing DOM-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfDomOutWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
						 WHEN (( TolTotDomOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDomOutWireAmtMnth <> 0.00)) AND (TolTotDomOutWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotDomOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'outgoing DOM-wire transactions of $' + CONVERT(varchar, T.TolTotDomOutWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
						 WHEN (( TolNoOfDomOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDomOutWireMnth <> 0 )) AND (TolNoOfDomOutWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfDomOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) 
							THEN 'number of outgoing DOM-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfDomOutWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
					END + @strDueTo
				  + CASE WHEN ( (( TolTotDomOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDomOutWireAmtMnth <> 0.00)) AND ( TolTotDomOutWireAmtMnth Is Not NULL ) AND (( TolNoOfDomOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDomOutWireMnth <> 0 )) AND ( TolNoOfDomOutWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDomOutWireAmtMnth AND COUNT(TranNo) > TolNoOfDomOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' outgoing DOM-wire transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolTotDomOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDomOutWireAmtMnth <> 0.00)) AND (TolTotDomOutWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotDomOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'the total outgoing DOM-wire transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolNoOfDomOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDomOutWireMnth <> 0 )) AND (TolNoOfDomOutWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfDomOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) 
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' outgoing DOM-wire transactions'
					END + @strBetweenDates
					, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
					, Case WHEN T.TolTotDomOutWireAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotDomOutWireAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
					, CASE WHEN T.TolNoOfDomOutWireMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfDomOutWireMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
					, ' DOM-wire ' + Case WHEN Q.RecvPay = 1 THEN 'incoming ' ELSE 'outgoing ' END + 'transactions' AS AACComparisonTypeDetail  			
				FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
				WHERE (RecvPay = 2)
					   AND (@actvTypesListIncl IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, ActivityType) + ',', @actvTypesListIncl ) > 0)
				GROUP BY Q.Account, RecvPay, TolTotDomOutWireAmtMnth, TolNoOfDomOutWireMnth
				HAVING 
				(
						( (( TolTotDomOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDomOutWireAmtMnth <> 0.00)) AND ( TolTotDomOutWireAmtMnth Is Not NULL ) AND (( TolNoOfDomOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDomOutWireMnth <> 0 )) AND ( TolNoOfDomOutWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDomOutWireAmtMnth AND COUNT(TranNo) > TolNoOfDomOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
					OR
						(( TolTotDomOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDomOutWireAmtMnth <> 0.00)) AND (TolTotDomOutWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotDomOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
					OR
						(( TolNoOfDomOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDomOutWireMnth <> 0 )) AND (TolNoOfDomOutWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfDomOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) 
				)			
				UNION
				SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
				  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
				  + CASE WHEN ( (( TolTotForInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotForInWireAmtMnth <> 0.00)) AND ( TolTotForInWireAmtMnth Is Not NULL ) AND (( TolNoOfForInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfForInWireMnth <> 0 )) AND ( TolNoOfForInWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotForInWireAmtMnth AND COUNT(TranNo) > TolNoOfForInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN 'incoming FOR-wire transactions of $' + CONVERT(varchar, T.TolTotForInWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
							  + @strExceed2 + ' incoming FOR-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfForInWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
						 WHEN (( TolTotForInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotForInWireAmtMnth <> 0.00)) AND (TolTotForInWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotForInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'incoming FOR-wire transactions of $' + CONVERT(varchar, T.TolTotForInWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
						 WHEN (( TolNoOfForInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfForInWireMnth <> 0 )) AND (TolNoOfForInWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfForInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN 'number of incoming FOR-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfForInWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
					END + @strDueTo
				  + CASE WHEN ( (( TolTotForInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotForInWireAmtMnth <> 0.00)) AND ( TolTotForInWireAmtMnth Is Not NULL ) AND (( TolNoOfForInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfForInWireMnth <> 0 )) AND ( TolNoOfForInWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotForInWireAmtMnth AND COUNT(TranNo) > TolNoOfForInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' incoming FOR-wire transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolTotForInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotForInWireAmtMnth <> 0.00)) AND (TolTotForInWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotForInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'the total incoming FOR-wire transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolNoOfForInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfForInWireMnth <> 0 )) AND (TolNoOfForInWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfForInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' incoming FOR-wire transactions'
					END + @strBetweenDates
					, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
					, Case WHEN T.TolTotForInWireAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotForInWireAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
					, CASE WHEN T.TolNoOfForInWireMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfForInWireMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
					, ' FOR-wire ' + Case WHEN Q.RecvPay = 1 THEN 'incoming ' ELSE 'outgoing ' END + 'transactions' AS AACComparisonTypeDetail  			
				FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
				WHERE (RecvPay = 1)
					   AND (@actvTypesListExcl IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, ActivityType) + ',', @actvTypesListExcl ) > 0)
				GROUP BY Q.Account, RecvPay, TolTotForInWireAmtMnth, TolNoOfForInWireMnth
				HAVING 
				(
						( (( TolTotForInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotForInWireAmtMnth <> 0.00)) AND ( TolTotForInWireAmtMnth Is Not NULL ) AND (( TolNoOfForInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfForInWireMnth <> 0 )) AND ( TolNoOfForInWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotForInWireAmtMnth AND COUNT(TranNo) > TolNoOfForInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
					OR
						(( TolTotForInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotForInWireAmtMnth <> 0.00)) AND (TolTotForInWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotForInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
					OR
						(( TolNoOfForInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfForInWireMnth <> 0 )) AND (TolNoOfForInWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfForInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
				) 
				UNION 
				SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
				  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
				  + CASE WHEN ( (( TolTotForOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotForOutWireAmtMnth <> 0.00)) AND ( TolTotForOutWireAmtMnth Is Not NULL ) AND (( TolNoOfForOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfForOutWireMnth <> 0 )) AND ( TolNoOfForOutWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotForOutWireAmtMnth AND COUNT(TranNo) > TolNoOfForOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN 'outgoing FOR-wire transactions of $' + CONVERT(varchar, T.TolTotForOutWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
							  + @strExceed2 + ' outgoing FOR-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfForOutWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
						 WHEN (( TolTotForOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotForOutWireAmtMnth <> 0.00)) AND (TolTotForOutWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotForOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'outgoing FOR-wire transactions of $' + CONVERT(varchar, T.TolTotForOutWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
						 WHEN (( TolNoOfForOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfForOutWireMnth <> 0 )) AND (TolNoOfForOutWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfForOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN 'number of outgoing FOR-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfForOutWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
					END + @strDueTo
				  + CASE WHEN  ( (( TolTotForOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotForOutWireAmtMnth <> 0.00)) AND ( TolTotForOutWireAmtMnth Is Not NULL ) AND (( TolNoOfForOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfForOutWireMnth <> 0 )) AND ( TolNoOfForOutWireMnth Is Not NULL ) )
								AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotForOutWireAmtMnth AND COUNT(TranNo) > TolNoOfForOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' outgoing FOR-wire transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolTotForOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotForOutWireAmtMnth <> 0.00)) AND (TolTotForOutWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotForOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'the total outgoing FOR-wire transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolNoOfForOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfForOutWireMnth <> 0 )) AND (TolNoOfForOutWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfForOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' outgoing FOR-wire transactions'
					END + @strBetweenDates
					, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
					, Case WHEN T.TolTotForOutWireAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotForOutWireAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
					, CASE WHEN T.TolNoOfForOutWireMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfForOutWireMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
					, ' FOR-wire ' + Case WHEN Q.RecvPay = 1 THEN 'incoming ' ELSE 'outgoing ' END + 'transactions' AS AACComparisonTypeDetail  			
				FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
				WHERE (RecvPay = 2)
					  AND (@actvTypesListExcl IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, ActivityType) + ',', @actvTypesListExcl ) > 0)
				GROUP BY Q.Account, RecvPay, TolTotForOutWireAmtMnth, TolNoOfForOutWireMnth
				HAVING 
				(
						( (( TolTotForOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotForOutWireAmtMnth <> 0.00)) AND ( TolTotForOutWireAmtMnth Is Not NULL ) AND (( TolNoOfForOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfForOutWireMnth <> 0 )) AND ( TolNoOfForOutWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotForOutWireAmtMnth AND COUNT(TranNo) > TolNoOfForOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt)
					OR
						(( TolTotForOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotForOutWireAmtMnth <> 0.00)) AND (TolTotForOutWireAmtMnth Is Not NULL) AND (SUM(BaseAmt) > TolTotForOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
					OR
						(( TolNoOfForOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfForOutWireMnth <> 0 )) AND (TolNoOfForOutWireMnth Is Not NULL) AND (COUNT(TranNo) > TolNoOfForOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
				)
			END
		ELSE if @aggAntpDnFCntAmt = 1
			BEGIN
				INSERT INTO @DSTT 	
				SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
				  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
				  + CASE WHEN ( (( TolTotAllInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotAllInWireAmtMnth <> 0.00)) AND ( TolTotAllInWireAmtMnth Is Not NULL ) AND (( TolNoOfAllInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfAllInWireMnth <> 0 )) AND ( TolNoOfAllInWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND ( SUM(BaseAmt) > TolTotAllInWireAmtMnth AND COUNT(TranNo) > TolNoOfAllInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt )
							THEN 'incoming all-wire transactions of $' + CONVERT(varchar, T.TolTotAllInWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, Convert(int, @amtTolerance))) + '% tolerance)'  
							  + @strExceed2 + ' incoming all-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfAllInWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
						 WHEN (( TolTotAllInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotAllInWireAmtMnth <> 0.00)) AND (TolTotAllInWireAmtMnth IS Not NULL) AND (SUM(BaseAmt) > TolTotAllInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) 
							THEN 'incoming all-wire transactions of $' + CONVERT(varchar, T.TolTotAllInWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, Convert(int, @amtTolerance))) + '% tolerance)' 
						 WHEN (( TolNoOfAllInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfAllInWireMnth <> 0 )) AND ( TolNoOfAllInWireMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfAllInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN 'number of incoming all-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfAllInWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
					END + @strDueTo
				  + CASE WHEN ( (( TolTotAllInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotAllInWireAmtMnth <> 0.00)) AND ( TolTotAllInWireAmtMnth Is Not NULL ) AND (( TolNoOfAllInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfAllInWireMnth <> 0 )) AND ( TolNoOfAllInWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND ( SUM(BaseAmt) > TolTotAllInWireAmtMnth AND COUNT(TranNo) > TolNoOfAllInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' incoming all-wire transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolTotAllInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotAllInWireAmtMnth <> 0.00)) AND (TolTotAllInWireAmtMnth IS Not NULL) AND (SUM(BaseAmt) > TolTotAllInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) 
							THEN 'the total incoming all-wire transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolNoOfAllInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfAllInWireMnth <> 0 )) AND ( TolNoOfAllInWireMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfAllInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' incoming all-wire transactions'
					END + @strBetweenDates
					, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
					, Case WHEN T.TolTotAllInWireAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotAllInWireAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
					, CASE WHEN T.TolNoOfAllInWireMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfAllInWireMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
					, ' all-wire ' + Case WHEN Q.RecvPay = 1 THEN 'incoming ' ELSE 'outgoing ' END + 'transactions' AS AACComparisonTypeDetail  			
				FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
				WHERE (RecvPay = 1)
				GROUP BY Q.Account, RecvPay, TolTotAllInWireAmtMnth, TolNoOfAllInWireMnth
				HAVING 
				(
						( (( TolTotAllInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotAllInWireAmtMnth <> 0.00)) AND ( TolTotAllInWireAmtMnth Is Not NULL ) AND (( TolNoOfAllInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfAllInWireMnth <> 0 )) AND ( TolNoOfAllInWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND ( SUM(BaseAmt) > TolTotAllInWireAmtMnth AND COUNT(TranNo) > TolNoOfAllInWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt )
					OR
						(( TolTotAllInWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotAllInWireAmtMnth <> 0.00)) AND (TolTotAllInWireAmtMnth IS Not NULL) AND (SUM(BaseAmt) > TolTotAllInWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) 
					OR
						(( TolNoOfAllInWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfAllInWireMnth <> 0 )) AND ( TolNoOfAllInWireMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfAllInWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
				)
				UNION 
				SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
				  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
				  + CASE WHEN ( (( TolTotAllOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotAllOutWireAmtMnth <> 0.00)) AND ( TolTotAllOutWireAmtMnth Is Not NULL ) AND (( TolNoOfAllOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfAllOutWireMnth <> 0 )) AND ( TolNoOfAllOutWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND ( SUM(BaseAmt) > TolTotAllOutWireAmtMnth AND COUNT(TranNo) > TolNoOfAllOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt )
							THEN 'outgoing all-wire transactions of $' + CONVERT(varchar, T.TolTotAllOutWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
							  + @strExceed2 + ' outgoing all-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfAllOutWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
						 WHEN (( TolTotAllOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotAllOutWireAmtMnth <> 0.00)) AND (TolTotAllOutWireAmtMnth IS Not NULL) AND (SUM(BaseAmt) > TolTotAllOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'outgoing all-wire transactions of $' + CONVERT(varchar, T.TolTotAllOutWireAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
						 WHEN (( TolNoOfAllOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfAllOutWireMnth <> 0 )) AND (TolNoOfAllOutWireMnth IS Not NULL) AND (COUNT(TranNo) > TolNoOfAllOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN 'number of outgoing all-wire transactions of ' + CONVERT(VARCHAR, T.TolNoOfAllOutWireMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
					END + @strDueTo
				  + CASE WHEN ( (( TolTotAllOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotAllOutWireAmtMnth <> 0.00)) AND ( TolTotAllOutWireAmtMnth Is Not NULL ) AND (( TolNoOfAllOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfAllOutWireMnth <> 0 )) AND ( TolNoOfAllOutWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND ( SUM(BaseAmt) > TolTotAllOutWireAmtMnth AND COUNT(TranNo) > TolNoOfAllOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' outgoing all-wire transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolTotAllOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotAllOutWireAmtMnth <> 0.00)) AND (TolTotAllOutWireAmtMnth IS Not NULL) AND (SUM(BaseAmt) > TolTotAllOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
							THEN 'the total outgoing all-wire transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
						 WHEN (( TolNoOfAllOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfAllOutWireMnth <> 0 )) AND (TolNoOfAllOutWireMnth IS Not NULL) AND (COUNT(TranNo) > TolNoOfAllOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
							THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' outgoing all-wire transactions'
					END + @strBetweenDates
					, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
					, Case WHEN T.TolTotAllOutWireAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotAllOutWireAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
					, CASE WHEN T.TolNoOfAllOutWireMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfAllOutWireMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
					, ' all-wire ' + Case WHEN Q.RecvPay = 1 THEN 'incoming ' ELSE 'outgoing ' END + 'transactions' AS AACComparisonTypeDetail  			
				FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
				WHERE (RecvPay = 2)
				GROUP BY Q.Account, RecvPay, TolTotAllOutWireAmtMnth, TolNoOfAllOutWireMnth
				HAVING 
				(
						( (( TolTotAllOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotAllOutWireAmtMnth <> 0.00)) AND ( TolTotAllOutWireAmtMnth Is Not NULL ) AND (( TolNoOfAllOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfAllOutWireMnth <> 0 )) AND ( TolNoOfAllOutWireMnth Is Not NULL ) )
						AND (@monitorInd = 3) AND ( SUM(BaseAmt) > TolTotAllOutWireAmtMnth AND COUNT(TranNo) > TolNoOfAllOutWireMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt )
					OR
						(( TolTotAllOutWireAmtMnth = 0.00) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotAllOutWireAmtMnth <> 0.00)) AND (TolTotAllOutWireAmtMnth IS Not NULL) AND (SUM(BaseAmt) > TolTotAllOutWireAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) )
					OR
						(( TolNoOfAllOutWireMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfAllOutWireMnth <> 0 )) AND (TolNoOfAllOutWireMnth IS Not NULL) AND (COUNT(TranNo) > TolNoOfAllOutWireMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) )
				)																			
			END
	END
ELSE
if @comparisonType =  3
	BEGIN
		Delete From @DSAACTolerated									 
		Where ( TolTotCheckWAmtMnth Is NULL AND TolNoOfCheckWMnth Is NULL )
			
		INSERT INTO @DSTT 
		SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
		  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
		  + CASE WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is Not NULL ) AND (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is Not NULL ) 
						AND ( @monitorInd = 3 ) AND ( SUM(BaseAmt) > TolTotCheckWAmtMnth AND COUNT(TranNo) > TolNoOfCheckWMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt ) )
					THEN 'check deposit transactions of $' + CONVERT(varchar, T.TolTotCheckWAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)'  
				      + @strExceed2 + ' check deposit transactions of ' + CONVERT(VARCHAR, T.TolNoOfCheckWMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
				 WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is NOT NULL ) AND (SUM(BaseAmt) >= @minAggTransAmt) AND (SUM(BaseAmt) > TolTotCheckWAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'check deposit transactions of $' + CONVERT(varchar, T.TolTotCheckWAmtMnth) + ' (includes ' + CONVERT(VARCHAR, Convert(int, @amtTolerance)) + '% tolerance)' 
				 WHEN ( (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is NOT NULL ) AND (COUNT(TranNo) >= @minTransCount) AND (COUNT(TranNo) > TolNoOfCheckWMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN 'number of check deposit transactions of ' + CONVERT(VARCHAR, T.TolNoOfCheckWMnth) + ' (includes ' + CONVERT(VARCHAR,@cntTolerance) + '% tolerance)'
			END + @strDueTo
		  + CASE WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is Not NULL ) AND (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is Not NULL ) 
						AND ( @monitorInd = 3 ) AND ( SUM(BaseAmt) > TolTotCheckWAmtMnth AND COUNT(TranNo) > TolNoOfCheckWMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' check deposit transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is NOT NULL ) AND (SUM(BaseAmt) >= @minAggTransAmt) AND (SUM(BaseAmt) > TolTotCheckWAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'the total check deposit transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is NOT NULL ) AND (COUNT(TranNo) >= @minTransCount) AND (COUNT(TranNo) > TolNoOfCheckWMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' check deposit transactions'
				 END + @strBetweenDates
			, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
			, Case WHEN T.TolTotCheckWAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotCheckWAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
			, CASE WHEN T.TolNoOfCheckWMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfCheckWMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
			, ' check ' + Case WHEN Q.RecvPay = 1 THEN 'deposit ' ELSE 'withdrawal ' END + 'transactions' AS AACComparisonTypeDetail  			
		FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
		WHERE (RecvPay = 1)
		GROUP BY Q.Account, RecvPay, TolTotCheckWAmtMnth, TolNoOfCheckWMnth
		HAVING
		(
				( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is Not NULL ) AND (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is Not NULL ) 
				AND ( @monitorInd = 3 ) AND ( SUM(BaseAmt) > TolTotCheckWAmtMnth AND COUNT(TranNo) > TolNoOfCheckWMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt ) )
			OR
				( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is NOT NULL ) AND (SUM(BaseAmt) >= @minAggTransAmt) AND (SUM(BaseAmt) > TolTotCheckWAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
			OR
				( (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is NOT NULL ) AND (COUNT(TranNo) >= @minTransCount) AND (COUNT(TranNo) > TolNoOfCheckWMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
		)		
		UNION 
		SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
		  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
		  + CASE WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is Not NULL ) AND (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is Not NULL ) 
						AND ( @monitorInd = 3 ) AND ( SUM(BaseAmt) > TolTotCheckWAmtMnth AND COUNT(TranNo) > TolNoOfCheckWMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt ) )
					THEN 'check withdrawal transactions of $' + CONVERT(varchar, T.TolTotCheckWAmtMnth) + ' (includes ' + CONVERT(VARCHAR,  Convert(int, @amtTolerance)) + '% tolerance)'  
				      + @strExceed2 + ' check withdrawal transactions of ' + CONVERT(VARCHAR, T.TolNoOfCheckWMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
				 WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is NOT NULL ) AND (SUM(BaseAmt) >= @minAggTransAmt) AND (SUM(BaseAmt) > TolTotCheckWAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )			
					THEN 'check withdrawal transactions of $' + CONVERT(varchar, T.TolTotCheckWAmtMnth) + ' (includes ' + CONVERT(VARCHAR,  Convert(int, @amtTolerance)) + '% tolerance)' 
				 WHEN ( (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is NOT NULL ) AND (COUNT(TranNo) >= @minTransCount) AND (COUNT(TranNo) > TolNoOfCheckWMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN 'number of check withdrawal transactions of ' + CONVERT(VARCHAR, T.TolNoOfCheckWMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
			END + @strDueTo
		  + CASE WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is Not NULL ) AND (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is Not NULL ) 
						AND ( @monitorInd = 3 ) AND ( SUM(BaseAmt) > TolTotCheckWAmtMnth AND COUNT(TranNo) > TolNoOfCheckWMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' check withdrawal transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is NOT NULL ) AND (SUM(BaseAmt) >= @minAggTransAmt) AND (SUM(BaseAmt) > TolTotCheckWAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )			
					THEN 'the total check withdrawal transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is NOT NULL ) AND (COUNT(TranNo) >= @minTransCount) AND (COUNT(TranNo) > TolNoOfCheckWMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' check withdrawal transactions'
			END + @strBetweenDates
			, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
			, Case WHEN T.TolTotCheckWAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotCheckWAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
			, CASE WHEN T.TolNoOfCheckWMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfCheckWMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
			, ' check ' + Case WHEN Q.RecvPay = 1 THEN 'deposit ' ELSE 'withdrawal ' END + 'transactions' AS AACComparisonTypeDetail  			
		FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
		WHERE (RecvPay = 2)
		GROUP BY Q.Account, RecvPay, TolTotCheckWAmtMnth, TolNoOfCheckWMnth
		HAVING 
		(
				( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is Not NULL ) AND (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is Not NULL ) 
				AND ( @monitorInd = 3 ) AND ( SUM(BaseAmt) > TolTotCheckWAmtMnth AND COUNT(TranNo) > TolNoOfCheckWMnth AND COUNT(TranNo) >= @minTransCount AND SUM(BaseAmt) >= @minAggTransAmt ) )
			OR
				( (( TolTotCheckWAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotCheckWAmtMnth <> 0.00 )) AND ( TolTotCheckWAmtMnth Is NOT NULL ) AND (SUM(BaseAmt) >= @minAggTransAmt) AND (SUM(BaseAmt) > TolTotCheckWAmtMnth) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )			
			OR
				( (( TolNoOfCheckWMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfCheckWMnth <> 0 )) AND ( TolNoOfCheckWMnth Is NOT NULL ) AND (COUNT(TranNo) >= @minTransCount) AND (COUNT(TranNo) > TolNoOfCheckWMnth) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
		)		
	END
ELSE if @comparisonType =  4
	BEGIN
		Delete From @DSAACTolerated									 
		Where ( TolTotDepositAmtMnth Is NULL AND TolNoOfDepositMnth Is NULL )
			
		INSERT INTO @DSTT 
		SELECT DISTINCT  Q.Account, @comparisonType AS ComparisonType, Null
		  , @strSubject + LTRIM(RTRIM(Q.Account)) + @strExceed1 
		  + CASE WHEN ( (( TolTotDepositAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDepositAmtMnth <> 0.00 )) AND ( TolTotDepositAmtMnth Is Not NULL ) AND (( TolNoOfDepositMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDepositMnth <> 0 )) AND ( TolNoOfDepositMnth Is Not NULL ) 
						AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDepositAmtMnth AND COUNT(TranNo) > TolNoOfDepositMnth AND (COUNT(TranNo) >= @minTransCount) AND (SUM(BaseAmt) >= @minAggTransAmt) ) )
					THEN 'deposit transactions of $' + CONVERT(varchar, T.TolTotDepositAmtMnth) + ' (includes ' + CONVERT(VARCHAR,  Convert(int, @amtTolerance)) + '% tolerance)'  
				      + @strExceed2 + ' deposit transactions of ' + CONVERT(VARCHAR, T.TolNoOfDepositMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
				 WHEN ( (( TolTotDepositAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDepositAmtMnth <> 0.00 )) AND ( TolTotDepositAmtMnth Is Not NULL ) AND (SUM(BaseAmt) > TolTotDepositAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'deposit transactions of $' + CONVERT(varchar, T.TolTotDepositAmtMnth) + ' (includes ' + CONVERT(VARCHAR,  Convert(int, @amtTolerance)) + '% tolerance)' 
				 WHEN ( (( TolNoOfDepositMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDepositMnth <> 0 )) AND ( TolNoOfDepositMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfDepositMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN 'number of deposit transactions of ' + CONVERT(VARCHAR, T.TolNoOfDepositMnth) + ' (includes ' + CONVERT(VARCHAR, @cntTolerance) + '% tolerance)'
			END + @strDueTo
		  + CASE 
				 WHEN ( (( TolTotDepositAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDepositAmtMnth <> 0.00 )) AND ( TolTotDepositAmtMnth Is Not NULL ) AND (( TolNoOfDepositMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDepositMnth <> 0 )) AND ( TolNoOfDepositMnth Is Not NULL ) 
						AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDepositAmtMnth AND COUNT(TranNo) > TolNoOfDepositMnth AND (COUNT(TranNo) >= @minTransCount) AND (SUM(BaseAmt) >= @minAggTransAmt) ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' deposit transactions totaling $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				 WHEN ( (( TolTotDepositAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDepositAmtMnth <> 0.00 )) AND ( TolTotDepositAmtMnth Is Not NULL ) AND (SUM(BaseAmt) > TolTotDepositAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
					THEN 'the total deposit transaction amount of $' + CONVERT(VARCHAR, SUM(Q.BaseAmt))
				WHEN ( (( TolNoOfDepositMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDepositMnth <> 0 )) AND ( TolNoOfDepositMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfDepositMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
					THEN CONVERT(VARCHAR, COUNT(Q.TranNo)) + ' deposit transactions'
			END + @strBetweenDates
			, Q.RecvPay, SUM(Q.BaseAmt) AS TransAmt, COUNT(Q.TranNo) AS TransCnt
			, Case WHEN T.TolTotDepositAmtMnth >= @minAggTransAmt THEN CONVERT(varchar, T.TolTotDepositAmtMnth) ELSE CONVERT(VARCHAR, @minAggTransAmt) END AS boundaryAmt
			, CASE WHEN T.TolNoOfDepositMnth >= @minTransCount THEN CONVERT(VARCHAR, T.TolNoOfDepositMnth) ELSE CONVERT(VARCHAR, @minTransCount) END As BoundaryCnt 
			, ' ' + Case WHEN Q.RecvPay = 1 THEN 'deposit ' ELSE 'withdrawal ' END + 'transactions' AS AACComparisonTypeDetail  			
		FROM  @DSQA AS Q JOIN @DSAACTolerated AS T  ON Q.Account = T.Account
		WHERE (RecvPay = 1) 
		GROUP BY Q.Account, RecvPay, TolTotDepositAmtMnth, TolNoOfDepositMnth
		HAVING  
		(
				( (( TolTotDepositAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDepositAmtMnth <> 0.00 )) AND ( TolTotDepositAmtMnth Is Not NULL ) AND (( TolNoOfDepositMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDepositMnth <> 0 )) AND ( TolNoOfDepositMnth Is Not NULL ) 
				AND (@monitorInd = 3) AND (SUM(BaseAmt) > TolTotDepositAmtMnth AND COUNT(TranNo) > TolNoOfDepositMnth AND (COUNT(TranNo) >= @minTransCount) AND (SUM(BaseAmt) >= @minAggTransAmt) ) )
			OR
				( (( TolTotDepositAmtMnth = 0.00 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolTotDepositAmtMnth <> 0.00 )) AND ( TolTotDepositAmtMnth Is Not NULL ) AND (SUM(BaseAmt) > TolTotDepositAmtMnth) AND (SUM(BaseAmt) >= @minAggTransAmt) AND ( (@monitorInd = 1) OR (@monitorInd = 3) ) )
			OR
				( (( TolNoOfDepositMnth = 0 ) AND (@iZAV = 0 Or @iNAV=0) Or ( TolNoOfDepositMnth <> 0 )) AND ( TolNoOfDepositMnth Is Not NULL ) AND (COUNT(TranNo) > TolNoOfDepositMnth) AND (COUNT(TranNo) >= @minTransCount) AND ( (@monitorInd = 2) OR (@monitorInd = 3) ) )
		)				   
	End

IF ( @testAlert = 1 ) OR ( @testAlert = 0 AND @WLTYPE = 0 )
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		INSERT INTO Alert ( 
				  WLCode
				,[Account]
				,[Cust]
				,[Desc]
				,Status
				,CreateDate
				,LastOper
				,LastModify
				,IsTest
		) 		 
		SELECT @WLCode
				, Account	
				, NULL
				, AACResultDesc 
				, 0
				, GETDATE()
				, Null
				, Null
				, @testAlert 
		FROM @DSTT 
		
		SELECT @STAT = @@ERROR	
		SELECT @ENDALRTDATE = GETDATE()
			
		if @STAT <> 0  GOTO ENDOFPROC
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	DISTINCT 'Alert', AlertNo, Q.TRANNO
			FROM  @DSQA Q INNER JOIN @DSTT T ON Q.Account = T.Account INNER JOIN 
                  Alert A ON T.Account = A.Account
			WHERE	T.Account = A.Account AND A.WLCode = @WLCode 
			AND 	A.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		SELECT @STAT = @@ERROR
	END 
ELSE
IF 	( @testAlert = 0 ) AND ( @WLTYPE = 1 )
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		--SET StartDate and EndDate with @fromDate and @toDate in Date format
		
		INSERT INTo SuspiciousActivity (
				ProfileNo, BookDate, Cust, Account, Activity
				, SuspType, StartDate, EndDate, RecurType, RecurValue
				, ActCurrReportAmt, ActInActCnt, ActOutActCnt, ActInActAmt, ActOutActAmt
				, CurrReportAmt, ExpAvgInActCnt, ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt
				, InCntTolPerc, OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr
				, ReviewState, ReviewTime, ReviewOper, App, AppTime
				, AppOper , WLCode, WLDesc, CreateTime )
		SELECT	Null, dbo.ConvertSqlDateToInt(GETDATE()), Null, Account, Null
			, 'Rule', CONVERT(date, CONVERT(varchar(8), @fromDate), 112), CONVERT(date, CONVERT(varchar(8), @toDate), 112), Null, Null
			, Null, Null, Null, Null, Null
			, Null, Null, Null, Null, Null
			, 0, 0, 0, 0, Null
			, Null, Null, Null, 0, Null
			, Null, @WLCode, AACResultDesc ,GETDATE()
		FROM  @DSTT
		
		SELECT @ENDALRTDATE = GETDATE()
		SELECT @STAT = @@ERROR	

		if @STAT <> 0  GOTO ENDOFPROC
	
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	DISTINCT 'SUSPACT',A.RecNo,  Q.TRANNO
			FROM  @DSQA Q INNER JOIN @DSTT T ON Q.Account = T.Account INNER JOIN 
				  SuspiciousActivity A ON T.Account = A.Account
			WHERE	T.Account = A.Account AND A.WLCode = @WLCode 
			AND 	A.CreateTime BETWEEN @STARTALRTDATE AND @ENDALRTDATE

		SELECT @STAT = @@ERROR		
		IF @STAT <> 0 GOTO ENDOFPROC	
	END	

--------   End of ALERTs or CASEs SQL Script Codes Above -------------------

ENDOFPROC:
IF (@stat <> 0) BEGIN  
  ROLLBACK TRAN USR_AntpActvComps
  RETURN @stat
END	

IF @trnCnt = 0 
  COMMIT TRAN USR_AntpActvComps
  RETURN @stat
GO

if exists (select * from sysobjects 
		where id = object_id(N'[dbo].[BSA_GetAccountAnticipatedValues]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		Drop Procedure dbo.BSA_GetAccountAnticipatedValues
Go
CREATE PROCEDURE DBO.BSA_GetAccountAnticipatedValues 
	(@AccountID varchar(35))
WITH ENCRYPTION AS

SELECT NoOfDepositMnth,TotDepositAmtMnth,NoOfCashDepositMnth,TotCashDepositAmtMnth
      ,NoOfCashWithDMnth,TotCashWithDAmtMnth,NoOfCheckWMnth,TotCheckWAmtMnth
      ,NoOfDomInWireMnth,TotDomInWireAmtMnth,NoOfDomOutWireMnth,TotDomOutWireAmtMnth
	  ,NoOfForInWireMnth,TotForInWireAmtMnth,NoOfForOutWireMnth,TotForOutWireAmtMnth
FROM Account with (nolock) WHERE Id = @AccountID 
Go

if exists (select * from sysobjects 
		where id = object_id(N'[dbo].[BSA_CheckRejectScore]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		Drop Procedure dbo.BSA_CheckRejectScore
Go
Create Procedure dbo.BSA_CheckRejectScore ( @Cust ObjectID)
  With Encryption As
 
	-- Start standard stored procedure transaction header
	declare @trnCnt int
	select @trnCnt = @@trancount	-- Save the current trancount
	If @trnCnt = 0
		-- Transaction has not begun
		begin tran BSA_CheckRejectScore
	else
		-- Already in a transaction
		save tran BSA_CheckRejectScore
	-- End standard stored procedure transaction header
	
	Declare @cnt int,
	@stat int

	If Exists (Select 1 From RiskScoreHist with (nolock)
			    Where CustomerId = @Cust) 
	Begin
		select distinct  RiskScore,AcceptOper,DateAccepted,Name from RiskScoreHist His with (nolock)
			INNER JOIN RiskFactorData RFD with (nolock) ON His.CustomerId = RFD.Cust 
			INNER JOIN RiskFactorType RFT with (nolock) ON RFD.RFTCode = RFT.Code 
			where CustomerId = @Cust 
			and RiskScore is NULL and DateAccepted is not NULL 
			and DateAccepted in (select top 1 DateAccepted from RiskScoreHist with (nolock)  where CustomerId = @Cust order by DateAccepted desc)

		 select @stat = @@error
	End
	If @stat <> 0 
	begin
			rollback tran BSA_CheckRejectScore
			return @stat
	end
		If @trnCnt = 0
			commit tran BSA_CheckRejectScore
Go
if exists (select * from sysobjects 
		where id = object_id(N'[dbo].[BSA_RejectRiskFactorData]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		Drop Procedure dbo.BSA_RejectRiskFactorData
Go
CREATE PROCEDURE dbo.BSA_RejectRiskFactorData( 
				@Cust OBJECTID,
				@RiskClass SCODE, 
				@Reason VARCHAR(200),
				@Oper SCODE
				)
WITH ENCRYPTION AS

	-- Start standard stored procedure transaction header

	DECLARE @trnCnt int
	SELECT @trnCnt = @@trancount	-- Save the current trancount
	
	IF @trnCnt = 0
		BEGIN TRAN BSA_RejectRiskFactorData  -- Transaction has not begun
	ELSE	
		SAVE TRAN BSA_RejectRiskFactorData    -- Already in a transaction

	-- End standard stored procedure transaction header
  
	DECLARE	@stat 	INT,
			@cnt 	INT,
			@dt		DateTime,
			@txt	VARCHAR(6000),
			@logtxt VARCHAR(6000),
			@oldRiskClass SCode,
			@oldLastModify SCode,
			@LastReview	DateTime

	IF EXISTS (SELECT 1 FROM RiskFactorData with (nolock) WHERE cust = @Cust AND Status = 0)
		BEGIN 
			
			set @dt = GetDate()

			--Set Last Review time after customer's LastModify time
			set @LastReview = DateAdd(s, 10, @dt)

			If dbo.BSA_CountMissingScores(@Cust) > 0 Begin
			Select @stat = 250010   -- DB_INV_PARAM
			GoTo EndOfProc
			End

			/*
				 Delete Old accepted scores other than the ones
				 recently accepted, for this customer
			*/
			Delete RiskFactorData
					Where Cust = @Cust And Status = 1

			SELECT @oldRiskClass = RiskClass FROM RiskScoreHist with (nolock)
					WHERE CustomerId = @Cust AND DateAccepted IS NULL
			
			--update proposed record from the Risk Score History	
			UPDATE RiskScoreHist SET RiskClass = @RiskClass ,
										RiskScore = NULL, 
										AcceptOper =@Oper,
										DateAccepted =@LastReview
										WHERE CustomerId = @Cust 
										AND DateAccepted IS NULL

			Update RiskFactorData SET  Status = 1,		-- Reviewed/Accepted
										LastOper = @Oper,
										LastReview = @LastReview,
										LastModify = @LastReview
										WHERE Cust = @Cust AND Status = 0

			SELECT @stat = @@error, @cnt = @@rowcount
					IF @stat <> 0
						BEGIN
							GOTO EndOfProc
						END
					ELSE IF @cnt = 0 
						BEGIN
							SELECT @stat = 250006	-- DB_UNKNOWN
							GOTO EndOfProc
					END

			/**************************************************
				Assign the RiskClass to the Customer
				after modifying RiskFactorData to avoid
				conflicts with locked risk class actions
			**************************************************/
			UPDATE Customer SET RiskClass = @RiskClass, 
								LastOper = @Oper, 
								LastModify = @dt
								WHERE [ID] = @Cust

			--Check if customer was updated
			SELECT @stat = @@error, @cnt = @@rowcount
					IF @stat <> 0
						BEGIN
							GOTO EndOfProc
						END
					ELSE IF @cnt = 0 
						BEGIN
							SELECT @stat = 250006    -- DB_UNKNOWN
							GOTO EndOfProc --exit
						End

		END
	ELSE
		BEGIN
			SELECT @stat = 250006	-- Object does not exist -- DB_UNKNOWN
		END

EndOfProc:

	IF ( @stat = 0 and @cnt = 0 )
		SELECT @stat = 250001 -- concurent update
	IF ( @stat <> 0 ) 
		BEGIN 
			ROLLBACK TRAN BSA_RejectRiskFactorData
			RETURN @stat
		END	
	SELECT @logtxt = 'Risk Class Overridden'
	SELECT @txt	= 'RiskClass Proposed: ' + CONVERT(VARCHAR,ISNULL(@oldRiskClass,'NULL')) + '| RiskClass Overridden: ' + CONVERT(VARCHAR,ISNULL(@RiskClass,'NULL'))+
				  ' | Reason: ' + @Reason 

	EXEC BSA_InsEvent @Oper, 'Mod', 'Cust', @Cust,@logtxt, @txt

	IF @trnCnt = 0
		COMMIT TRAN BSA_RejectRiskFactorData
	RETURN @stat
Go

if exists (select * from sysobjects 
		where id = object_id(N'[dbo].[vwRiskScoreHist]') 
		and OBJECTPROPERTY(id, N'IsView') = 1)
		Drop View dbo.vwRiskScoreHist
Go
Create View dbo.vwRiskScoreHist
As
Select dbo.BSA_fnGetRiskScoreHistStatus(rsh.CustomerId, 
	   rsh.CreateDate, rsh.DateAccepted) As HistoryStatus,
	   rsh.CustomerId,	Isnull(cast(rsh.RiskScore as varchar(50)),'Overridden') As RiskScore, rc.[Name] As RiskClass,
	   rsh.CreateOper,	rsh.CreateDate,	
	   rsh.AcceptOper, rsh.DateAccepted
from RiskScoreHist rsh inner join RiskClass rc
on rsh.RiskClass = rc.Code
Go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BSA_GetBranchandDeptfromAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[BSA_GetBranchandDeptfromAccount]
GO

Create Procedure [dbo].[BSA_GetBranchandDeptfromAccount](@acctid varchar(35))
With Encryption As
	begin
	Select OwnerBranch as Branch,OwnerDept as Dept
		From Account  WITH (NOLOCK) where Id=@acctid
	end
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BSA_GetBranchandDeptfromCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[BSA_GetBranchandDeptfromCustomer]
GO

Create Procedure [dbo].[BSA_GetBranchandDeptfromCustomer](@custid varchar(35))
With Encryption As
	begin
	Select OwnerBranch as Branch,OwnerDept as Dept
		From Customer  WITH (NOLOCK) where Id=@custid
	end
GO

IF NOT EXISTS (SELECT * FROM Watchlist WHERE WLCode = '1MthExcAvg') 
BEGIN 
	INSERT INTO Watchlist (WLCode, Title, [Desc], WLType, SPName, SuspType,  
	Schedule, IsPreEOD, OwnerBranch, OwnerDept, OwnerOper, CreateOper, CreateDate,
	Params, IsUserDefined, CreateType,  IsLicensed,UseSysDate,OverrideExemption) 
	VALUES ('1MthExcAvg','Detect Accounts with transactions exceeding average monthly total by specified amount' +
	' within 1 calendar month.','Detects Accounts that had transactions using a specified activity type' +
	' and the total of these transactions for the last month exceeds the average monthly volume of specified' +
	' period (in months) by specified amount. Designed to be run Post-EOD on a weekly, monthly or quarterly schedule.' +
	' If the only activity for an account is in the last month (based on the configured rule parameters), the total for' +
	' the last month is directly compared to the threshold and the average is not considered.',
	0,'USR_1MnthExceedAverage','Rule',4, 
	0, Null, Null, Null,'Prime', GETDATE(),
	'<Params>   
	   <Param Name="@activityTypeList" Alias="List of Activity Types separated by Comma. -ALL- for All." Value="-ALL-" DataType="String"/>
	   <Param Name="@CustTypeList" Alias="List of Customer Types separated by Comma. -ALL- for All." Value="-ALL-" DataType="String"/>
	   <Param Name="@RiskClassList" Alias="List of Risk Classes separated by Comma. -ALL- for All." Value="-ALL-" DataType="String"/>
	   <Param Name="@TypeOfBusinessList" Alias="List of Type of Businesses separated by Comma. -ALL- for All." Value="-ALL-" DataType="String"/>
	   <Param Name="@BranchList" Alias="List of Branches separated by Comma. -ALL- for All." Value="-ALL-" DataType="String"/> 
	   <Param Name="@RecvPay" Alias="Receive/Pay flag. 1 - for Receive, 2 - Pay, 3 - both" Value="3" DataType="Int"/>
	   <Param Name="@NumMonths" Alias="Number of Months." Value="12" DataType="Int"/> 
	   <Param Name="@ExcAmtThreshold" Alias="Exceed Average Amount Threshold" Value="10000" DataType="Money"/> 
	   <Param Name="@InclLastMth" Alias="Include Last Month in Average. 1-Yes 0-No" Value="1" DataType="Int"/>
	</Params>',0,0,0,1,0)
END
ELSE
BEGIN
	UPDATE WATCHLIST 
	SET Title = 'Detect Accounts with transactions exceeding average monthly total by specified amount' +
		' within 1 calendar month.',[Desc] = 'Detects Accounts that had transactions using a specified activity type' +
		' and the total of these transactions for the last month exceeds the average monthly volume of specified' +
		' period (in months) by specified amount. Designed to be run Post-EOD on a weekly, monthly or quarterly schedule.' +
		' If the only activity for an account is in the last month (based on the configured rule parameters), the total for' + 
		' the last month is directly compared to the threshold and the average is not considered.', 
	WLType = 0, SPName = 'USR_1MnthExceedAverage', SuspType = 'Rule',Schedule = 4, IsPreEOD = 0, OwnerOper = Null,
	OwnerBranch = Null, OwnerDept = Null, Params = 
	'<Params>   
	   <Param Name="@activityTypeList" Alias="List of Activity Types separated by Comma. -ALL- for All." Value="-ALL-" DataType="String"/>
	   <Param Name="@CustTypeList" Alias="List of Customer Types separated by Comma. -ALL- for All." Value="-ALL-" DataType="String"/>
	   <Param Name="@RiskClassList" Alias="List of Risk Classes separated by Comma. -ALL- for All." Value="-ALL-" DataType="String"/>
	   <Param Name="@TypeOfBusinessList" Alias="List of Type of Businesses separated by Comma. -ALL- for All." Value="-ALL-" DataType="String"/>
	   <Param Name="@BranchList" Alias="List of Branches separated by Comma. -ALL- for All." Value="-ALL-" DataType="String"/> 
	   <Param Name="@RecvPay" Alias="Receive/Pay flag. 1 - for Receive, 2 - Pay, 3 - both" Value="3" DataType="Int"/>
	   <Param Name="@NumMonths" Alias="Number of Months." Value="12" DataType="Int"/> 
	   <Param Name="@ExcAmtThreshold" Alias="Exceed Average Amount Threshold" Value="10000" DataType="Money"/> 
	   <Param Name="@InclLastMth" Alias="Include Last Month in Average. 1-Yes 0-No" Value="1" DataType="Int"/>
	</Params>', 
	IsUserDefined = 0, CreateType = 0, IsLicensed = 0, LastOper = 'Prime',UseSysDate = 1, OverrideExemption = 0, 
	LastModify = GetDate()
	WHERE WLCode = '1MthExcAvg'

END
GO

/******************************************************************************
Procedure Name: USR_1MnthExceedAverage
Description:	Detects Accounts that had transactions using a specified activity 
				type and the total of these transactions for the month exceeds 
				the average monthly average of specified period (in months) by 
				specified amount.Designed to be run Post-EOD.

Parameters:
   @WLCode				-- Rule Code
   @testAlert			-- 1 for Test			
   @activityTypeList	-- List of Activity Types separated by comma. 
			   			-ALL-, blank or NULL for all types
	@CustTypeList		-- List of Customer Types seperated by comma.
						-ALL-, blank or NULL for all customer types
   @RiskClassList		-- List of Risk Classes that will be considered. 
			   			-ALL-, blank or NULL for all Risk Classes
   @TypeOfBusinessList	-- List of type of businesses that will be considered. 
			   			-ALL-, blank or NULL for all types
   @BranchList			-- List of Branches that will be considerd.
						-ALL-, blank or NULL for all types
   @NumMonths			-- Number of Months
   @ExcAmtThreshold		-- Exceed Average Amount Threshold
   @InclLastMth			-- Include Last Month in Average 1-Yes 0-No
	[Return] INT = Return the error status.  0 for success.
******************************************************************************/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[USR_1MnthExceedAverage]') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[USR_1MnthExceedAverage]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.USR_1MnthExceedAverage (@WLCode SCode, @TestAlert INT,
												@activityTypeList varchar(8000), 
												@CustTypeList  Varchar(8000),
												@RiskClassList Varchar(8000),
												@TypeOfBusinessList Varchar(8000),
												@BranchList Varchar(8000),
												@RecvPay Int,
												@NumMonths int,
												@ExcAmtThreshold Money,
												@InclLastMth tinyint

)
AS
/* RULE AND PARAMETER Description
    Detects Accounts with wire transfers between specified amount range
    AND for over a period of a month.
	@ActivityTypeList	- Type of activity to be detected.
	@CustTypeList - List of Customer Types that will be considered.
					-ALL-, blank or NULL for all Customer Types
	@RiskClassList	- List of Risk Classes that will be considered. 
					-ALL-, blank or NULL for all Risk Classes
	@TypeOfBusinessList	-- List of type of businesses that will be considered. 
			   			-ALL-, blank or NULL for all types
   	@BranchList	- List of Branches that will be considered. 
					-ALL-, blank or NULL for all Branches
	@NumMonths	- Number of Months to be evaluated
	@ExcAmtThreshold - Amount by which Avereage Amount should exceed aggregate for the month
	@InclLastMth	- Include Last Month in Average

*/
/*  Declarations */
	DECLARE	@Description VARCHAR(2000),
		    @Desc VARCHAR(2000),
		    @Id INT, 
		    @WLType INT,
		    @Stat INT,
		    @TrnCnt INT,
		    @MinDate INT,
		    @MaxDate INT,
		    @MinDateAvg INT,
		    @MaxDateAvg INT,
		    @ActivityType  INT,
	            @Pos	  INT

	DECLARE @StartAlrtDate  DATETIME
	DECLARE @EndAlrtDate    DATETIME

	SET NOCOUNT ON
	SET @Stat = 0

	--- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */
	SET @TrnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @TrnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN USR_1MnthExceedAverage
	ELSE
		-- Already in a transaction
		SAVE TRAN USR_1MnthExceedAverage
	/* End stANDard stored procedure transaction header */

	/*****************************************/
	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	SET 	@StartDate = GETDATE()
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE, @StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode
	/******************************************/

	SET @MinDate = dbo.FirstDayOfMonth(DATEADD(MONTH, -1,@StartDate))
	SET @MaxDate = dbo.FirstDayOfMonth(@StartDate)

	IF @InclLastmth = 1
	BEGIN
		SET @MaxDateAvg = @MaxDate
		SET @MinDateAvg = dbo.ConvertSqlDateToInt(DATEADD(MM, @NumMonths * -1, 
				dbo.BSA_ConvertintToSqlDate(@MaxDate,'MM/DD/YYYY')))
	END
	ELSE
	BEGIN
		SET @MaxDateAvg = @MinDate
		SET @MinDateAvg = dbo.ConvertSqlDateToInt(DATEADD(MM, @NumMonths * -1, 
				dbo.BSA_ConvertintToSqlDate(@MinDate,'MM/DD/YYYY')))
	END

	-- Call BSA_fnListParams for each of the Paramters that support comma separated values

	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)
	SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
	SELECT @TypeofBusinessList = dbo.BSA_fnListParams(@TypeofBusinessList)	
	
	Create table #TT (
		   Account varchar(40),
		   Recvpay INT,
		   BaseAmt MONEY,
		   TranNo INT,
		   SortValue INT
	)
	DECLARE	@TT1 TABLE (
		   Account varchar(40),
			Recvpay INT,
		   SumBaseAmt MONEY,
		   SortValue INT
	)
	DECLARE	@TTMerge TABLE (
		   Account varchar(40),
		   Recvpay INT,

		   TotalAmt MONEY,
		   SortValue INT
	)
	DECLARE	@TTMerge1 TABLE (
		   Account varchar(40),
		   Recvpay INT,
		   AvgTotalAmt MONEY
	)
	DECLARE @ActType TABLE
	(
		Type INT
	)

		--If RecvPay = 3 Consider both Deposits ans WithDrawals
		IF (@RecvPay = 3)
		SET @RecvPay = NULL

	-- Get all the Activity Types that have not been exempted.
	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	-- Get the transactions for the last month that match the input criteria specified
	INSERT INTO #TT ( Account, SortValue, BaseAmt, TranNo, RecvPay)
		SELECT 	AH.Account, FLOOR(BookDate / 100) * 100 + 1 SortValue, BaseAmt, TranNo, RecvPay 
		FROM 	ActivityHist AH (NOLOCK)
			INNER JOIN Account Ac ON Ac.[Id] = AH.Account
			INNER JOIN @ActType Act ON AH.Type = Act.Type
			INNER JOIN Customer C ON AH.Cust = C.[Id]
		WHERE	BookDate >= @MinDate AND BookDate < @MaxDate
			AND	((ISNULL(@CustTypeList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(C.Type)) + ',', @CustTypeList ) > 0)) 		
			AND	((ISNULL(@RiskClassList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(C.RiskClass)) + ',', @RiskClassList ) > 0)) 
			AND	((ISNULL(@TypeOfBusinessList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(C.TypeOfBusiness)) + ',', @TypeOfBusinessList ) > 0)) 
			AND	((ISNULL(@BranchList, '') = '' OR
					CHARINDEX(',' + LTRIM(RTRIM(Ac.Branch)) + ',', @BranchList ) > 0))
		
	-- Get the aggregate amounts for the transactions in the last month
	INSERT INTO @TTMerge (Account, RecvPay, TotalAmt, SortValue)
	SELECT 	Account, RecvPay, SUM(BaseAmt), SortValue
	FROM 	#TT
	WHERE 	Account IS NOT NULL
	GROUP 	BY Account, sortValue, RecvPay

	-- Get the average monthly total for all the accounts for the period specified;
	-- Add 0 as AvgTotalAmt if account does not have any transactions for the past @NumMonths months
	INSERT INTO @TTMerge1 (Account, RecvPay, AvgTotalAmt)
		SELECT Account, RecvPay, 
			 -- Following CASE expects that there will always be 1 record for the last month 
			 -- and a record for each prior month in the @NumMonths months where there was activity
			 -- @InclLastMth is a 0 or 1 indicator 
			 -- If @InclLastMth=0, then number of records should be one or more.
			 -- If @InclLastMth=1, then number of records should be more than one.	
			(CASE
				WHEN Count(1) > @InclLastMth THEN
					-- There are transaction(s) in the past @NumMonths months.
					-- So update the actual average amount.
					AVG(SumBaseAmt) 
				ELSE
					-- There is no transaction in the past @NumMonths months.
					-- So update 0 as the average amount.
					0
			END) AvgTotalAmt
		FROM 	(
			SELECT 	AH.Account,  Sum(AH.BaseAmt) SumBaseAmt,AH.RecvPay
			FROM 	ActivityHist AH (NOLOCK)
			INNER JOIN @ActType Act ON AH.Type = Act.Type
			INNER JOIN Customer C ON AH.Cust = C.[Id]
			WHERE  	AH.BookDate >= @MinDateAvg AND AH.BookDate < @MaxDateAvg
			AND		EXISTS (SELECT NULL FROM #TT T WHERE T.Account = AH.Account)
			AND	((ISNULL(@CustTypeList, '') = '' OR
						CHARINDEX(',' + LTRIM(RTRIM(C.Type)) + ',', @CustTypeList ) > 0)) 		
			AND	((ISNULL(@RiskClassList, '') = '' OR
						CHARINDEX(',' + LTRIM(RTRIM(C.RiskClass)) + ',', @RiskClassList ) > 0)) 
			AND	((ISNULL(@TypeOfBusinessList, '') = '' OR
						CHARINDEX(',' + LTRIM(RTRIM(C.TypeOfBusiness)) + ',', @TypeOfBusinessList ) > 0)) 
			GROUP 	BY AH.Account,  FLOOR(BookDate / 100) * 100 + 1,AH.RecvPay
				) TT1
		GROUP 	BY Account, RecvPay

	-- Add 0 as average monthly total for the accounts that are not having any 
	-- transactions in the past @NumMonths months;
	INSERT INTO @TTMerge1 (Account, RecvPay, AvgTotalAmt)
	SELECT 	Account, RecvPay, 0
	FROM @TTMerge TTM
	WHERE NOT EXISTS (SELECT 1 FROM @TTMerge1 TTM1
		WHERE TTM.Account = TTM1.Account AND TTM.RecvPay = TTM1.RecvPay)
		
--1 means test, 0 means no
	IF @testAlert = 1 Or @WLType = 0
	BEGIN
		SELECT @StartAlrtDate = GETDATE()
		INSERT INTO Alert ( WLCode, [DESC], Status, CreateDate, LastOper, 
			LastModify, Cust, Account, IsTest) 
		   SELECT @WLCode, 'Account: ''' + T1.Account + ''' had transactions totalling ''' 
				+ CONVERT(VARCHAR, T1.TotalAmt) + 
				''' for the month of ''' + CONVERT(VARCHAR, SortValue) + ''' of ' + 
				CASE WHEN T1.RecvPay = 1 THEN 'incoming' 
					WHEN T1.RecvPay = 2 THEN 'outgoing' 
				END + 
				CASE WHEN T2.AvgTotalAmt=0 THEN ' types specified where the total for the month exceeded '+ CONVERT(VARCHAR, @ExcAmtThreshold) +''
							+'. Since the only qualifying activity for the analysis period on this account was in the last month,' 
							+' the total for the last month is directly compared to the threshold and the average is not considered.'
					 ELSE ' types specified where the average monthly total was exceeded by more than ' + CONVERT(VARCHAR, @ExcAmtThreshold)+'.' 
				END
			    , 0, GETDATE(), NULL, NULL, NULL, T1.Account , @testAlert
		   FROM @TTMerge T1, @TTMerge1 T2
		   WHERE T1.Account = T2.Account
			AND T1.RecvPay = T2.RecvPay AND T1.RecvPay = ISNULL(@RecvPay,T1.RecvPay)
			AND (T1.TotalAmt - T2.AvgTotalAmt) > @ExcAmtThreshold

		SELECT @STAT = @@ERROR	
		SELECT @EndAlrtDate = GETDATE()

		INSERT INTO SASActivity (ObjectType, ObjectID, TranNo)
		SELECT 	'Alert', AlertNo, TranNo
		FROM 	@TTMerge a 
			INNER JOIN #TT b ON a.SortValue = b.SortValue 
				AND a.Account = b.Account 
				AND a.RecvPay = b.RecvPay 
			INNER JOIN Alert ON a.Account = Alert.Account 
		WHERE Alert.WLCode = @WLCode 
				AND Alert.CreateDate BETWEEN @StartAlrtDate AND @EndAlrtDate
				AND a.RecvPay = CASE WHEN Alert.[DESC] like '%incoming%' THEN 1
									 WHEN Alert.[DESC] like '%outgoing%' THEN 2
								END					
		SELECT @STAT = @@ERROR 
	END
	ELSE IF @WLTYPE = 1 
	BEGIN
		SELECT @StartAlrtDate = GETDATE()
		INSERT INTO SuspiciousActivity (ProfileNo, BookDate, Cust, Account, 
			Activity, SuspType, StartDate, EndDate, RecurType, 
			RecurValue, ActCurrReportAmt, ActInActCnt, ActOutActCnt, 
			ActInActAmt, ActOutActAmt, CurrReportAmt, ExpAvgInActCnt, 
			ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt, InCntTolPerc, 
			OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr, ReviewState, 
			ReviewTime, ReviewOper, App, AppTime, AppOper, 
			WLCode, WLDesc, CreateTime )
			SELECT	NULL, SortValue, Null, T1.Account,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, 'Account: ''' + T1.Account + ''' had transactions totalling ''' 
				+ CONVERT(VARCHAR, T1.TotalAmt) + 
				''' for the month of ''' + CONVERT(VARCHAR, SortValue) +
				''' of ' + CASE WHEN T1.RecvPay = 1 THEN 'incoming' 
								WHEN T1.RecvPay = 2 THEN 'outgoing' 
							END +
							CASE WHEN T2.AvgTotalAmt=0 THEN ' types specified where the total for the month exceeded '+ CONVERT(VARCHAR, @ExcAmtThreshold) +''
							+'. Since the only qualifying activity for the analysis period on this account was in the last month,' 
							+' the total for the last month is directly compared to the threshold and the average is not considered.'
							ELSE ' types specified where the average monthly total was exceeded by more than ' + CONVERT(VARCHAR, @ExcAmtThreshold) END +'.'
				 , GETDATE() 
			FROM 	@TTMerge T1, @TTMerge1 T2
			WHERE 	T1.Account = T2.Account
				AND 	T1.RecvPay = T2.RecvPay AND T1.RecvPay = ISNULL(@RecvPay,T1.RecvPay)
				AND 	(T1.TotalAmt - T2.AvgTotalAmt) > @ExcAmtThreshold

		SELECT @STAT = @@ERROR	
		SELECT @EndAlrtDate = GETDATE()
		IF @STAT <> 0  GOTO EndOfProc

		INSERT INTO SASActivity (ObjectType, ObjectID, TranNo)
			SELECT 	'SUSPACT', RecNo, TranNo 
			FROM 	@TTMerge a INNER JOIN #TT b ON a.SortValue = b.SortValue AND
				 a.Account = b.Account AND
				 a.RecvPay = b.RecvPay  
				INNER 	JOIN SuspiciousActivity s ON a.Account = s.Account 
			WHERE 	S.WLCode = @WLCode 
				AND S.CreateTime BETWEEN @StartAlrtDate AND @EndAlrtDate
				AND a.RecvPay = CASE WHEN s.[WLDesc] like '%incoming%' THEN 1
									 WHEN s.[WLDesc] like '%outgoing%' THEN 2
								END	

		SELECT @STAT = @@ERROR 		
	END

EndOfProc:
IF (@Stat <> 0) BEGIN 
    ROLLBACK TRAN USR_1MnthExceedAverage
    RETURN @Stat
END	

IF @TrnCnt = 0
    COMMIT TRAN USR_1MnthExceedAverage

drop table #tt

RETURN @Stat



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

IF NOT EXISTS (SELECT 1 FROM ChangeHistory with (nolock))
	DECLARE @Oper VARCHAR(11)
	SET @Oper = 'ADMIN'
	If exists (select 1 from report with (nolock)  where code =  'GetChgHist')
	BEGIN
		Delete from Report where Code =  'GetChgHist'
		IF @@ROWCOUNT = 1 
		Exec BSA_InsEvent @Oper, 'Del', 'Report', 'GetChgHist','BSA Report "Get Change History" (GetChgHist) has been deleted from the list of BSA Reports.'
	END
GO

--USR_XPercInTypeOutType
IF NOT EXISTS (SELECT * FROM Watchlist 
WHERE WLCode = 'XPrcInTOutT') BEGIN
insert into Watchlist (WLCode, Title, [Desc], WLType, SPName, SuspType, 
Schedule, IsPreEOD, OwnerBranch, OwnerDept, OwnerOper, CreateOper, CreateDate, 
Params, IsUserDefined, CreateType,  IsLicensed, UseSysDate)
values ('XPrcInTOutT','Detect incoming transactions and outgoing '+
'transactions of specified type within a designated percent over a period of time',
'Detects customers with incoming (receive) transactions of a  '+
'specified activity type and outgoing (pay) transactions of a specified  '+
'activity type where the total amounts differ by less than or equal to a  '+
'specified percentage.  This rule evaluates transactions that are within '+
'the specified number of days and the aggregate amount of the qualifying '+
'transactions exceeds the specified minimum amount.  Additional  '+
'constraints include Cash/Non-Cash for inbound and outbound activity types, '+
'Customer Type and Risk Class.  Designed to be run Post-EOD',
0, 'USR_XPercInTypeOutType', 'Rule', 1, 0, Null, Null, Null,
'Prime', GetDate(),
'<Params>
	<Param Name="@day" Alias="Number of Days" Value="30" DataType="Int"/>
	<Param Name="@percent" Alias="Percent" Value="10" DataType="Int"/>
	<Param Name="@inActivityTypeList" Alias="List of inbound activity Types separated by comma. Use -ALL- for any activity type." Value="-ALL-" DataType="String"/>
	<Param Name="@outActivityTypeList" Alias="List of outbound activity Types separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
	<Param Name="@minSumAmt" Alias="Minimum aggregated transaction amount that must be exceeded."  Value="0" DataType="Money"/>
	<Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@inboundCashType" Alias="Cash or non-Cash (inbound). 1 for Cash, 0 for NonCash, 2 for both" Value="2" DataType="Int"/>
	<Param Name="@outboundCashType" Alias="Cash or non-Cash (outbound). 1 for Cash, 0 for NonCash, 2 for both" Value="2" DataType="Int"/>
	<Param Name="@UseRelAcct" Alias="Include Related Accounts (1=yes, 0=no) " Value="0" DataType="Int"/>
	<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="Int"/>
	<Param Name="@ExcludeRelList" Alias="Exclude Relationship Types (comma separated list or -NONE-)" Value="-NONE-" DataType="String"/>
</Params>', 0, 0, 0, 1)	
END ELSE BEGIN
--Put any modifications here
UPDATE WATCHLIST
SET
Title = 'Detect incoming transactions and outgoing transactions of specified ' +
'type within a designated percent over a period of time',
[Desc] = 'Detects customers with incoming (receive) transactions of a '+
'specified activity type and outgoing (pay) transactions of a specified '+
'activity type where the total amounts differ by less than or equal to a '+
'specified percentage.  This rule evaluates transactions that are within '+
'the specified number of days and the aggregate amount of the qualifying '+
'transactions exceeds the specified minimum amount.  Additional '+
'constraints include Cash/Non-Cash for inbound and outbound activity types, '+
'Customer Type and Risk Class.  Designed to be run Post-EOD',
WLType = 0, SPName = 'USR_XPercInTypeOutType', SuspType = 'Rule',
Schedule = 4, IsPreEOD = 0,
OwnerOper = Null, OwnerBranch = Null, OwnerDept = Null,
Params =
'<Params>
	<Param Name="@day" Alias="Number of Days" Value="30" DataType="Int"/>
	<Param Name="@percent" Alias="Percent" Value="10" DataType="Int"/>
	<Param Name="@inActivityTypeList" Alias="List of inbound activity Types separated by comma. Use -ALL- for any activity type." Value="-ALL-" DataType="String"/>
	<Param Name="@outActivityTypeList" Alias="List of outbound activity Types separated by comma. Use -ALL- for All." Value="-ALL-" DataType="String"/>
	<Param Name="@minSumAmt" Alias="Minimum aggregated transaction amount that must be exceeded."  Value="0" DataType="Money"/>
	<Param Name="@riskClassList" Alias="List of Risk Classes separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@customerTypeList" Alias="List of Customer Types separated by comma. Use -ALL- for All."  Value="-ALL-" DataType="String"/>
	<Param Name="@inboundCashType" Alias="Cash or non-Cash (inbound). 1 for Cash, 0 for NonCash, 2 for both" Value="2" DataType="Int"/>
	<Param Name="@outboundCashType" Alias="Cash or non-Cash (outbound). 1 for Cash, 0 for NonCash, 2 for both" Value="2" DataType="Int"/>
	<Param Name="@UseRelAcct" Alias="Include Related Accounts (1=yes, 0=no) " Value="0" DataType="Int"/>
	<Param Name="@UseRelCust" Alias="Include Related Parties (1=yes, 0=no) " Value="0" DataType="Int"/>
	<Param Name="@ExcludeRelList" Alias="Exclude Relationship Types (comma separated list or -NONE-)" Value="-NONE-" DataType="String"/>
</Params>',
IsUserDefined = 0, CreateType = 0, IsLicensed = 0, LastOper = 'Prime', UseSysDate=1
WHERE WLCode = 'XPrcInTOutT'
END
Go

/******************************************************************************
Procedure Name: USR_XPercInTypeOutType
Description:Detects customers with incoming (receive) transactions of a 
specified activity type and outgoing (pay) transactions of a specified 
activity type where the total amounts differ by less than or equal to a 
specified percentage.  This rule evaluates transactions that are within
the specified number of days and the aggregate amount of the qualifying
transactions exceeds the specified minimum amount.  Additional 
constraints include Cash/Non-Cash for inbound and outbound activity types,
Customer Type and Risk Class.  Designed to be run Post-EOD

Parameters:		@WLCode [in] SCode = Rule Code				
				[Return] INT = Return the error status.  0 for success.
******************************************************************************/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_XPercInTypeOutType]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure USR_XPercInTypeOutType
Go
CREATE  PROCEDURE dbo.USR_XPercInTypeOutType (@WLCode SCode, @testAlert INT,
	@day  int,
	@percent  int,
	@inActivityTypeList varchar(8000),
	@outActivityTypeList varchar(8000),
	@minSumAmt money,
	@riskClassList varchar(8000),
	@customerTypeList varchar(8000),
	@inboundCashType smallint,
	@outboundCashType smallint,
	@UseRelAcct Bit, 
	@UseRelCust Bit, 
    @ExcludeRelList varchar(8000)
	)
AS
/* RULE AND PARAMETER DESCRIPTION
	Detects customers with incoming (receive) transactions of a 
	specified activity type and outgoing (pay) transactions of a specified 
	activity type where the total amounts differ by less than or equal to a 
	specified percentage.  This rule evaluates transactions that are within
	the specified number of days and the aggregate amount of the qualifying
	transactions exceeds the specified minimum amount.  Additional 
	constraints include Cash/Non-Cash for inbound and outbound activity types,
	Customer Type and Risk Class.  Designed to be run Post-EOD
	
	@day = Indicates the number of days that are included in the evaluation of 
		transactions. This includes business and non-business days prior to
		the day the rule is executed.
	@percent = Percentage of the difference in amounts between the deposits 
		(receive) and the withdrawals (pay)
	@inActivityType = A comma separated list of inbound activity types to 
		be evaluated
        @outActivityType = A comma separated list of outbound activity types to 
		be evaluated 
	@minSumAmt = The Minimum aggregated transaction amount that must be exceeded.  
	@riskClass = A comma separated list of Risk Classes to include in the evaluation,
		 use -ALL- for any risk class.
	@customerType = A comma separated list of Customer Types to include in the 
		evaluation, use -ALL- for any customer type.
	@inboundCashType = For the activity types specified in the parameter 
		InActivityType, use 1 to restrict them to only Cash transactions,
		use 0 to restrict them to Non-Cash, 2 for both
	@outboundCashType = For the activity types specified in the parameter 
		OutActivityType, use 1 to restrict them to only 
			    Cash transactions, use 0 to restrict them to Non-Cash, 2 for both
	@UseRelAcct = Include Related Accounts (1=yes, 0=no) 
	@UseRelCust = Include Related Parties (1=yes, 0=no) 
	@ExcludeRelList = Exclude Relationship Types (comma separated list or '-NONE-')
*/

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@MINDATE INT
Declare @cust LONGNAME
Declare @currDateTime datetime 
Declare @CurrBookDate int

DECLARE @SetDate DATETIME

Create table #TT 
(
	Cust VARCHAR(40),
	Recv MONEY,
	Pay MONEY,
	Mindate INT,
	Maxdate INT,
	Descr VARCHAR(2000)
)

Create table #TT1 
(
	Cust	VARCHAR(35),
	TranNo	INT,
	BaseAmt MONEY, 
	RecvPay INT,
	BookDate INT)


SET NOCOUNT ON
SET @stat = 0

IF (ISNULL(@inActivityTypeList,'') = '' 
		OR UPPER(ISNULL(@inActivityTypeList,'-ALL-')) = '-ALL-')
	SELECT @inActivityTypeList = NULL
ELSE
	SELECT @inActivityTypeList = ',' + 
		REPLACE(ltrim(rtrim(@inActivityTypeList)),CHAR(32),'') + ','

IF (ISNULL(@outActivityTypeList,'') = '' 
		OR UPPER(ISNULL(@outActivityTypeList,'-ALL-')) = '-ALL-')
	SELECT @outActivityTypeList = NULL
ELSE
	SELECT @outActivityTypeList = ',' + 
		REPLACE(ltrim(rtrim(@outActivityTypeList)),CHAR(32),'') + ','

IF (ISNULL(@riskClassList,'') = '' 
		OR UPPER(ISNULL(@riskClassList,'-ALL-')) = '-ALL-')
	SELECT @riskClassList = NULL
ELSE
	SELECT @riskClassList = ',' +
		REPLACE(ltrim(rtrim(@riskClassList)),CHAR(32),'') + ','

IF (ISNULL(@customerTypeList,'') = '' 
		OR UPPER(ISNULL(@customerTypeList,'-ALL-')) = '-ALL-')
	SELECT @customerTypeList = NULL
ELSE
	SELECT @customerTypeList = ',' +
		REPLACE(ltrim(rtrim(@customerTypeList)),CHAR(32),'') + ','

IF LTRIM(RTRIM(@ExcludeRelList)) = '-NONE-' OR LTRIM(RTRIM(@ExcludeRelList)) = ''
	SELECT @ExcludeRelList = NULL
ELSE
SELECT @ExcludeRelList = ',' + 
	REPLACE(LTRIM(RTRIM(@ExcludeRelList)),' ','')+ ','

IF (@inboundCashType = 2)
	SET @inboundCashType = NULL

IF (@OutboundCashType = 2)
	SET @OutboundCashType = NULL

--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_XPercInTypeOutType
ELSE
	-- Already in a transaction
	SAVE TRAN USR_XPercInTypeOutType
/* End standard stored procedure transaction header */

/*  standard Rules Header */

-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @description = [Desc], @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList WITH (NOLOCK)
WHERE WLCode = @WLCode

SELECT @day = ABS(@day)

--When the transactions are picked up
SET @minDate = dbo.ConvertSqlDateToInt(
	DATEADD(d, -1 * @day, CONVERT(VARCHAR, @SetDate)))

Insert Into #TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select cust, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist WITH (NOLOCK) 
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
)
AND bookdate >= @minDate  

-- Related Parties
Insert Into #TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select Distinct pr.PartyId, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist ah WITH (NOLOCK) JOIN PartyRelation pr ON ah.Cust = pr.RelatedParty
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
)
AND bookdate >= @minDate 
AND @UseRelCust = 1 AND ah.Cust <> pr.PartyID AND pr.Deleted <> 1  
AND ((ISNULL(@ExcludeRelList,'') = '' OR 
	CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
AND Exists (select * from #tt1 tt Where tt.Cust = pr.PartyId)
AND Not Exists (select * from #tt1 tt Where tt.Cust = pr.PartyId AND tt.TranNo = ah.tranno)

-- Related Parties (reverse direction)
Insert Into #TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select Distinct pr.RelatedParty, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist ah WITH (NOLOCK)  JOIN PartyRelation pr ON ah.Cust = pr.PartyID
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
)
AND bookdate >= @minDate 
AND @UseRelCust = 1 AND ah.Cust <> pr.RelatedParty AND pr.Deleted <> 1
AND ((ISNULL(@ExcludeRelList,'') = '' OR 
	CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
AND Exists (select * from #tt1 tt Where tt.Cust = pr.RelatedParty)
AND Not Exists (select * from #tt1 tt Where tt.Cust = pr.RelatedParty AND tt.TranNo = ah.tranno)

-- Related Accounts
Insert Into #TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select Distinct ao.Cust, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist ah WITH (NOLOCK) JOIN AccountOwner ao 
On ah.Account = ao.Account and ah.Cust <> ao.Cust 
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
)
AND bookdate >= @minDate
AND @UseRelAcct = 1
AND ((ISNULL(@ExcludeRelList,'') = '' OR 
	CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ao.Relationship,''))) + ',', @ExcludeRelList) = 0))
AND Exists (select * from #tt1 tt Where tt.Cust = ao.cust)
AND Not Exists (select * from #tt1 tt Where tt.Cust = ao.Cust AND tt.TranNo = ah.tranno)


INSERT INTO #TT(Cust, recv, pay, mindate, maxdate, Descr)
SELECT cust, SUM(recv) recv, SUM(pay) pay, MIN(mindate) mindate, 
       MAX(maxdate) maxdate, 'Cust:  ''' + cust 
	+ ''' has incoming transfer '''
	+ CONVERT(VARCHAR, SUM(recv))
	+ ''' and outgoing transfer '''
	+ CONVERT(VARCHAR, SUM(pay))
	+ ''' where the percentage '
	+ 'differs by '
        + CASE 
          WHEN SUM(pay) >= SUM(recv) AND SUM(pay) <> 0 THEN 
           CONVERT(VARCHAR, ABS(100 - 100 * SUM(recv)/SUM(pay))) 
          WHEN SUM(pay) < SUM(recv) AND SUM(recv) <> 0 THEN 
           CONVERT(VARCHAR, ABS(100 - 100 * SUM(pay)/SUM(recv)))
          END
	+ ' percent from bookdate ''' 
	+ CONVERT(VARCHAR, MIN(mindate)) + ''' to '''
	+ CONVERT(VARCHAR, MAX(maxdate)) + '''' descr
FROM  (
	SELECT cust, 		
	CASE WHEN recvpay = 1 THEN SUM(baseamt) 
		ELSE 0
		END recv,
	CASE WHEN recvpay = 2 THEN SUM(baseamt) 
		ELSE 0
		END pay,
	MIN(bookdate) AS mindate,
	MAX(bookdate) AS maxdate
	FROM #TT1
	GROUP BY cust, recvpay
) a, 
	Customer WITH (NOLOCK)
	WHERE   a.cust = Customer.Id
	AND ((@riskClassList IS NULL OR 
			CHARINDEX(',' + 
				LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0))
	AND ((@customerTypeList IS NULL OR
			CHARINDEX(',' + 
				LTRIM(RTRIM(Customer.type)) + ',', @customerTypeList ) > 0))
GROUP BY cust
HAVING SUM(recv) <> 0 AND SUM(pay) <> 0 
AND (100 * SUM(recv) between (100 - @percent)*SUM(pay) and (100 + @percent)*SUM(pay)
 OR  100 * SUM(pay) between (100 - @percent)*SUM(recv) and (100 + @percent)*SUM(recv))
AND (SUM(recv) > @MinSumAmt OR SUM(pay) > @MinSumAmt)
Set @currDateTime = GETDATE()
set @CurrBookDate = dbo.ConvertSqlDateToInt(GetDate())

IF @testAlert = 1 BEGIN
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, CUST, IsTest) 
	  SELECT @WLCode, descr, 0, @currDateTime, cust, 1 FROM #TT 

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	  SELECT Distinct 'Alert', AlertNo, TRANNO 
		FROM #tt1 A 
         JOIN #tt T ON a.Cust = t.Cust
         JOIN Alert AL ON a.Cust = al.Cust
        WHERE 
	al.WLCode = @WLCode
	AND al.CreateDate between @currDateTime AND GETDATE()
	SELECT @STAT = @@ERROR 
	IF @stat <> 0 GOTO EndOfProc
END ELSE BEGIN
	IF @WLType = 0 BEGIN
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, CUST) 
	  SELECT @WLCode, descr, 0, @currDateTime, cust FROM #TT 
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	  SELECT Distinct 'Alert', AlertNo, TRANNO 
		FROM #tt1 A 
         JOIN #tt T ON a.Cust = t.Cust
         JOIN Alert AL ON a.Cust = al.Cust
        WHERE 
	al.WLCode = @WLCode
	AND al.CreateDate between @currDateTime AND GETDATE()

	SELECT @STAT = @@ERROR 
	IF @stat <> 0 GOTO EndOfProc
	END ELSE IF @WLType = 1 BEGIN	
                INSERT INTO SUSPICIOUSACTIVITY (BOOKDATE, CUST, SUSPTYPE, 
                        INCNTTOLPERC, OUTCNTTOLPERC, INAMTTOLPERC, 
			OUTAMTTOLPERC, WLCode, WLDESC, CreateTime)
		SELECT	@CurrBookDate, Cust, 'RULE', 0, 0, 0, 0, 
			@WLCode, descr, @currDateTime
		FROM #tt
		IF @stat <> 0 GOTO EndOfProc
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT Distinct 'SUSPACT', RecNo, TRANNO 
			FROM #tt1 A
        	JOIN #tt T ON a.Cust = t.Cust
             	JOIN SuspiciousActivity s ON a.Cust = s.Cust
        	WHERE 
	        s.WLCode = @WLCode
	        AND s.bookdate = @CurrBookDate
			AND s.createTime between @currDateTime AND GETDATE()

		SELECT @STAT = @@ERROR
		IF @stat <> 0 GOTO EndOfProc
	END
END
EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_XPercInTypeOutType

  drop table #tt
  drop table #tt1

  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_XPercInTypeOutType

drop table #tt
drop table #tt1

RETURN @stat

Go

--USR_XPercOutInTypeTransfer
IF NOT EXISTS (SELECT * FROM Watchlist 
WHERE WLCode = 'XPercOutIn') BEGIN 
insert into Watchlist (WLCode, Title, [Desc], WLType, SPName, SuspType,  
Schedule, IsPreEOD, OwnerBranch, OwnerDept, OwnerOper, CreateOper, CreateDate, Params, IsUserDefined, CreateType,  IsLicensed) 
values ('XPercOutIn', 'Detect outgoing transactions and incoming transactions of same type within a designated percent over a period of time', 
'Detects customers who had outgoing transactions of a specified activity type 
and incoming transactions of the same activity type where the total 
amounts differ by less than or equal to a selected percentage within a 
designated number of days. Designed to be run Post-EOD.', 0 , 'USR_XPercOutInTypeTransfer', 'Rule', 3, 
0, Null, Null, Null,'Prime', GETDATE(), '
<Params>
<Param Name="@day" Alias="Number of Days" Value="7" DataType="Int"/>
<Param Name="@percent" Alias="Percent" Value="5" DataType="Int"/>
<Param Name="@activityType" Alias="Activity Type" Value="101" DataType="Int"/>
</Params>', 
0,0,0) 
END ELSE BEGIN 
UPDATE WATCHLIST 
SET 
Title = 'Detect outgoing transactions and incoming transactions of same type within a designated percent over a period of time', 
[Desc] = 'Detects customers who had outgoing transactions of a specified activity type 
and incoming transactions of the same activity type where the total 
amounts differ by less than or equal to a selected percentage within a 
designated number of days. Designed to be run Post-EOD.', 
WLType = 0, SPName = 'USR_XPercOutInTypeTransfer', SuspType = 'Rule', 
Schedule = 3, IsPreEOD = 0, 
OwnerOper = Null, OwnerBranch = Null, OwnerDept = Null, 
Params = '<Params>
<Param Name="@day" Alias="Number of Days" Value="7" DataType="Int"/>
<Param Name="@percent" Alias="Percent" Value="5" DataType="Int"/>
<Param Name="@activityType" Alias="Activity Type" Value="101" DataType="Int"/>
</Params>',
IsUserDefined = 0, CreateType = 0, IsLicensed = 0, LastOper = 'Prime'
WHERE WLCode = 'XPercOutIn'
END
Go


/******************************************************************************
Procedure Name: USR_XPercOutInTypeTransfer
Description:	Detects customers who had outgoing transactions of a 
specified activity type and incoming transactions of the same 
activity type where the total amounts differ by less than or equal 
to a selected percentage within a designated number of days. 
Designed to be run Post-EOD.

Parameters:		@WLCode [in] SCode = Rule Code				
				[Return] INT = Return the error status.  0 for success.
******************************************************************************/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_XPercOutInTypeTransfer]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure USR_XPercOutInTypeTransfer
Go

CREATE PROCEDURE USR_XPercOutInTypeTransfer (@WLCode SCode, @testAlert INT,
	@day  int,
	@percent  int,
	@activityType int)
AS
/* RULE AND PARAMETER DESCRIPTION
	Detects customers who had outgoing transactions of a 
	specified activity type and incoming transactions of the same 
	activity type where the total amounts differ by less than or equal 
	to a selected percentage within a designated number of days. 
	Designed to be run Post-EOD.

	@day = No of days activity to be considered
	@percent = percentage difference in amount of deposits or withdrawals
	@activityType = Type of activity to be checked
*/
/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@MINDATE INT,
	@ArchDate INT,
	@lBound int,
	@uBound int
DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME
DECLARE @cur CURSOR
Declare @cust LONGNAME,
	@bookdate INT,
	@recv MONEY,
	@date int,
	@pay MONEY,
	@payAmt MONEY,
	@recvAmt MONEY
DECLARE	@TT TABLE (
	Cust VARCHAR(40),
	BookDate INT,
	Recv MONEY,
	Pay MONEY
)

SET NOCOUNT ON
SET @stat = 0
SET @lbound = 100 - @percent
SET @ubound = 100 + @percent
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_XPercOutInTypeTransfer
ELSE
	-- Already in a transaction
	SAVE TRAN USR_XPercOutInTypeTransfer
/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @description = [Desc], @WLType = WLType  
FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

SELECT @day = ABS(@day)

--a week to to today
SET @minDate = dbo.ConvertSqlDateToInt(
	DATEADD(d, -1 * @day, CONVERT(VARCHAR, GETDATE())))

--2 weeks to weekago
SET @archDate = dbo.ConvertSqlDateToInt(
	DATEADD(d, -1 * 2 * @day, CONVERT(VARCHAR, GETDATE())))

INSERT INTO @TT(Cust, Bookdate, Recv, Pay)
SELECT Tbl.cust, Tbl.bookdate, SUM(Tbl.recv), SUM(Tbl.pay)
FROM (	SELECT cust, bookdate,
		CASE WHEN recvpay = 1 THEN SUM(baseamt) 
			ELSE 0
			END recv,
		CASE WHEN recvpay = 2 THEN SUM(baseamt) 
			ELSE 0
			END pay
	FROM ActivityHist (NOLOCK) 
	WHERE bookdate > @archDate AND Type = @activityType
	GROUP BY cust, bookdate, recvpay	
) Tbl
GROUP BY Tbl.cust, Tbl.bookdate

SET @cur = CURSOR FAST_FORWARD FOR 
SELECT cust, bookdate, recv, pay
FROM @TT
WHERE bookdate > @minDate
OPEN @cur
FETCH NEXT FROM @cur INTO @cust, @bookdate, @recv, @pay
WHILE @@FETCH_STATUS = 0
	BEGIN

	SET @date = dbo.ConvertSqlDateToInt(
		DATEADD(d, -1 * @day, CONVERT(VARCHAR, @bookdate)))

	SELECT @recvAmt = SUM(a.recv), @payAmt = SUM(a.pay)
	FROM @TT a
	WHERE @bookdate >= a.bookdate and a.bookdate > @date and
		@cust = a.cust
	HAVING SUM(a.recv) > 0 and SUM(a.pay) > 0 

	if @@ROWCOUNT > 0 BEGIN
		IF @ubound >= 100 * @payAmt/@recvAmt AND 
		   @lbound <= 100 * @payAmt/@recvAmt BEGIN
				SET @desc = 'Cust:  ''' + @cust 
					+ ''' has outgoing transfer '''
					+ CONVERT(VARCHAR, @payAmt)
					+ ''' and incoming transfer '''
					+ CONVERT(VARCHAR, @recvAmt)
					+ ''' where the percentage '
					+ 'differs by 5 percent from bookdate ''' 
					+ CONVERT(VARCHAR, @date) + ''' to '''
					+ CONVERT(VARCHAR, @bookdate) + ''''	


			IF @testAlert = 1 BEGIN
				EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, 
						@desc, @cust, NULL, 1		
				IF @stat <> 0 GOTO EndOfProc
				INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				   SELECT 'Alert', @ID, TRANNO 
					FROM ACTIVITYHIST A (NOLOCK) 
					WHERE @bookdate >= a.bookdate and a.bookdate > @date 
						AND a.Type = @activityType AND a.Cust = @cust 
					SELECT @STAT = @@ERROR 
				IF @stat <> 0 GOTO EndOfProc
			END ELSE BEGIN
				IF @WLType = 0 BEGIN
					EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, 
							@desc, @cust, NULL		
					IF @stat <> 0 GOTO EndOfProc
					INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
					   SELECT 'Alert', @ID, TRANNO 
						FROM ACTIVITYHIST A (NOLOCK) 
						WHERE @bookdate >= a.bookdate and a.bookdate > @date 
							AND a.Type = @activityType AND a.Cust = @cust
						SELECT @STAT = @@ERROR 
					IF @stat <> 0 GOTO EndOfProc
				END ELSE IF @WLType = 1 BEGIN	
					EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT, 
						@WLCode, @desc, @bookdate, @cust, Null
					IF @stat <> 0 GOTO EndOfProc
					INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
					   SELECT 'SUSPACT', @ID, TRANNO 
						FROM ACTIVITYHIST A (NOLOCK) 
						WHERE  @bookdate >= a.bookdate and a.bookdate > @date 
							AND a.Type = @activityType AND a.Cust = @cust 
						SELECT @STAT = @@ERROR 
					IF @stat <> 0 GOTO EndOfProc
				END
			END
		END ELSE BEGIN
			GOTO SkipRec
		END
	END	
SkipRec:
	FETCH NEXT FROM @cur INTO @cust, @bookdate, @recv, @pay
	END

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_XPercOutInTypeTransfer
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_XPercOutInTypeTransfer
RETURN @stat
Go

--USR_XPercInOutTypeTransfer
IF NOT EXISTS (SELECT * FROM Watchlist 
WHERE WLCode = 'XPercInOut') BEGIN 
insert into Watchlist (WLCode, Title, [Desc], WLType, SPName, SuspType,  
Schedule, IsPreEOD, OwnerBranch, OwnerDept, OwnerOper, CreateOper, 
CreateDate, Params, IsUserDefined, CreateType,  IsLicensed,UseSysDate,OverrideExemption) 
values ('XPercInOut', 
'Detect incoming transactions and outgoing transactions of 
same type within a designated percent over a period of time', 
'Detects customers who had incoming transactions for a selected activity type 
and outgoing transactions for the same activity type, 
where the difference between the incoming total amount and outgoing 
total amount is less than or equal to the designated percentage within a specified 
time frame and greater the minimum amount. Designed to be run Post-EOD.', 0 , 
'USR_XPercInOutTypeTransfer', 'Rule', 3, 
0, Null, Null, Null,'Prime', GETDATE(), '
<Params>
	<Param Name="@day" Alias="Number of Days" Value="7" DataType="Int"/>
	<Param Name="@percent" Alias="Percent" Value="5" DataType="Int"/>
	<Param Name="@ActivityTypeList" Alias="List of Activity Types. -ALL- for all" Value="-ALL-" DataType="String"/>
	<Param Name="@minAmount" Alias="Minimum Amount" Value="0" DataType="Money"/>
</Params>', 
0,0,0,1,0) 
END ELSE BEGIN 
UPDATE WATCHLIST 
SET 
Title = 'Detect incoming transactions and outgoing transactions of 
same type within a designated percent over a period of time', 
[Desc] = 'Detects customers who had incoming transactions for a selected activity type 
and outgoing transactions for the same activity type, 
where the difference between the incoming total amount and outgoing  
total amount is less than or equal to the designated percentage within a specified 
time frame and greater the minimum amount. Designed to be run Post-EOD.', 
WLType = 0, SPName = 'USR_XPercInOutTypeTransfer', SuspType = 'Rule', 
Schedule = 3, IsPreEOD = 0, 
OwnerOper = Null, OwnerBranch = Null, OwnerDept = Null, 
Params = '<Params>
	<Param Name="@day" Alias="Number of Days" Value="7" DataType="Int"/>
	<Param Name="@percent" Alias="Percent" Value="5" DataType="Int"/>
	<Param Name="@ActivityTypeList" Alias="List of Activity Types. -ALL- for all" Value="-ALL-" DataType="String"/>
	<Param Name="@minAmount" Alias="Minimum Amount" Value="0" DataType="Money"/>
</Params>',
IsUserDefined = 0, CreateType = 0, IsLicensed = 0,UseSysDate = 1, LastOper = 'Prime', LastModify = GetDate(),
OverrideExemption=0
WHERE WLCode = 'XPercInOut'
END
Go

/******************************************************************************
Procedure Name: USR_XPercInOutTypeTransfer
Description:	Detects customers who had incoming transactions for a selected 
activity type and outgoing transactions for the same 
activity type, where the difference between the incoming dollar total and 
outgoing dollar total is less than or equal to the designated percentage 
within a specified time frame and greater than the minimum amount. Designed to be run Post-EOD.	

Parameters:		@WLCode [in] SCode = Rule Code				
				[Return] INT = Return the error status.  0 for success.
******************************************************************************/
if exists (select * from sysobjects 
	where id = object_id(N'[dbo].[USR_XPercInOutTypeTransfer]') 
	and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	Drop Procedure dbo.USR_XPercInOutTypeTransfer
GO

CREATE PROCEDURE dbo.USR_XPercInOutTypeTransfer (
	@WLCode 		SCode, 
	@testAlert 		INT,
	@day  			INT,
	@percent  		INT,
	@ActivityTypeList 	VARCHAR(8000),
	@minAmount 		MONEY
)
AS
	/* RULE AND PARAMETER DESCRIPTION
		Detects customers who had incoming transactions for a selected 
		activity type and outgoing transactions for the same 
		activity type, where the difference between the incoming dollar total and 
		outgoing dollar total is less than or equal to the designated percentage 
		within a specified time frame. Designed to be run Post-EOD.	
	
		@day -- No of days activity to be considered
		@percent -- percentage difference in amount of deposits or withdrawals
		@ActivityTypeList  -- Comma separated list of activity types to be considered
		@minAmount  -- Minimum amount to consider for selection	
		
	*/
	
	/*  Declarations */
	DECLARE	@description VARCHAR(2000),
		@desc VARCHAR(2000),
		@Id INT, 
		@WLType INT,
		@stat INT,
		@trnCnt INT,
		@MINDATE INT,
		@ArchDate INT,
		@lBound int,
		@uBound int,
		@STARTALRTDATE  DATETIME,
		@ENDALRTDATE    DATETIME
	DECLARE @cur CURSOR
	Declare @cust LONGNAME,
		@bookdate INT,
		@recv MONEY,
		@date int,
		@pay MONEY,
		@payAmt MONEY,
		@recvAmt MONEY
	DECLARE	@TT TABLE 
	(
		Cust 	 VARCHAR(40),
		BookDate INT,
		Recv 	 MONEY,
		Pay 	 MONEY
	)
	
	-- Temporary table of Activity Types that have not been specified as Exempt
	DECLARE @ActType TABLE 
	(
		Type	INT
	)

	SET NOCOUNT ON
	SET @stat = 0
	SET @lbound = 100 - @percent
	SET @ubound = 100 + @percent
	--- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */
	SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN USR_XPercInOutTypeTransfer
	ELSE
		-- Already in a transaction
		SAVE TRAN USR_XPercInOutTypeTransfer
	/* End standard stored procedure transaction header */

	Declare @BaseCurr char(3)
	select @BaseCurr = IsNULL(BaseCurr,'') from SysParam
	
	/*  standard Rules Header */
	SELECT @description = [Desc], @WLType = WLType  
	FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE,@StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM 	dbo.WatchList
	WHERE 	WLCode = @WlCode


	
	SELECT @day = ABS(@day)
	
	--a week to to today
	SET @minDate = dbo.ConvertSqlDateToInt(
		DATEADD(d, -1 * @day, CONVERT(VARCHAR, @startDate)))
	
	--2 weeks to weekago
	SET @archDate = dbo.ConvertSqlDateToInt(
		DATEADD(d, -1 * 2 * @day, CONVERT(VARCHAR, @startDate)))

	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	
	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	INSERT INTO @TT(Cust, Bookdate, Recv, Pay)
		SELECT Tbl.Cust, Tbl.BookDate, SUM(Tbl.Recv), SUM(Tbl.Pay)
		FROM (	
			SELECT 	Cust, BookDate,
				CASE WHEN recvpay = 1 THEN SUM(baseamt) 
				ELSE 0
				END recv,
				CASE WHEN recvpay = 2 THEN SUM(baseamt) 
				ELSE 0
				END pay
			FROM 	ActivityHist Ach(NOLOCK) 
			INNER	JOIN @ActType Act ON Act.Type = Ach.Type
			WHERE 	Ach.BookDate >=@archDate  AND Ach.BookDate <= dbo.ConvertSqlDateToInt(@startDate)
			AND 	BaseAmt >= @minAmount
			GROUP 	BY Cust, BookDate, RecvPay	
	) Tbl
	GROUP BY Tbl.Cust, Tbl.BookDate
	
	SET @cur = CURSOR FAST_FORWARD FOR 
	SELECT 	Cust, BookDate, Recv, pay
	FROM 	@TT
	WHERE 	Bookdate >= @minDate AND Bookdate <= dbo.ConvertSqlDateToInt(@startDate)
	OPEN @cur
	FETCH NEXT FROM @Cur INTO @cust, @bookdate, @recv, @pay
	WHILE @@FETCH_STATUS = 0
		BEGIN
	
		SET @date = dbo.ConvertSqlDateToInt(
			DATEADD(d, -1 * @day, CONVERT(VARCHAR, @bookdate)))
	
		SELECT 	@recvAmt = SUM(a.recv), @payAmt = SUM(a.pay)
		FROM 	@TT A
		WHERE 	@BookDate >= A.BookDate AND A.BookDate >= @Date 
		AND	@cust = a.cust
		HAVING 	SUM(A.Recv) > 0 AND SUM(A.Pay) > 0 
	
		IF @@ROWCOUNT > 0 
		BEGIN
			IF @ubound >= 100 * @recvAmt/@payAmt AND 
			   @lbound <= 100 * @recvAmt/@payAmt 
			BEGIN
				SET @desc = 'Cust:  ''' + @cust 
					+ ''' has incoming transaction amount '
					+ DBO.BSA_InternalizationMoneyToString ( @recvAmt) + ' ' + @BaseCurr +
					+ ' and outgoing transaction amount '
					+ DBO.BSA_InternalizationMoneyToString ( @payAmt) + ' ' + @BaseCurr +
					+ ' where the percentage '
					+ 'differs by '+ CONVERT(VARCHAR, @percent) 
					+ ' percent from bookdate ''' 
					+ dbo.BSA_InternalizationIntToShortDate (@date) + ''' to '''
					+ dbo.BSA_InternalizationIntToShortDate (@bookdate) + '''' 
					+ ' greater than ' 
					+ DBO.BSA_InternalizationMoneyToString ( @minAmount) + ' ' + @BaseCurr

				IF @testAlert = 1 
				BEGIN
					EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, 
							@desc, @cust, NULL, 1		
					IF @stat <> 0 GOTO EndOfProc
					
					INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
						SELECT 	'Alert', @ID, TRANNO 
						FROM 	ACTIVITYHIST A (NOLOCK) 
						INNER	JOIN @ActType Act ON Act.Type = A.Type
						WHERE 	@BookDate >= A.BookDate AND A.Bookdate >= @Date
						AND 	A.Cust = @Cust 
						AND 	A.BaseAmt >= @minAmount
						SELECT @STAT = @@ERROR 
						IF @stat <> 0 GOTO EndOfProc
				END 
				ELSE 
				BEGIN
					IF @WLType = 0 
					BEGIN
						EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, 
								@desc, @cust, NULL		
						IF @stat <> 0 GOTO EndOfProc
						INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
							SELECT 	'Alert', @ID, TRANNO 
							FROM 	ACTIVITYHIST A (NOLOCK) 
							INNER	JOIN @ActType Act ON Act.Type = A.Type
							WHERE 	@BookDate >= A.BookDate AND A.Bookdate >= @Date
							AND 	A.Cust = @Cust 
							AND 	A.BaseAmt >= @minAmount
							SELECT @STAT = @@ERROR 
						IF @stat <> 0 GOTO EndOfProc
					END 
					ELSE IF @WLType = 1 
					BEGIN	
						EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT, 
							@WLCode, @desc, @bookdate, @cust, Null
						IF @stat <> 0 GOTO EndOfProc
						INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
							SELECT 	'SUSPACT', @ID, TRANNO 
							FROM 	ACTIVITYHIST A (NOLOCK) 
							INNER	JOIN @ActType Act ON Act.Type = A.Type
							WHERE 	@BookDate >= A.BookDate and a.bookdate >= @date
							AND 	A.Cust = @Cust 
							AND 	A.BaseAmt >= @minAmount						
						SELECT @STAT = @@ERROR 
						IF @stat <> 0 GOTO EndOfProc
					END
					
				END
			END 
			ELSE 
			BEGIN
				GOTO SkipRec
			END
		END	
	SkipRec:
		FETCH NEXT FROM @cur INTO @cust, @bookdate, @recv, @pay
		END
	
	CLOSE @cur
	DEALLOCATE @cur
	
	EndOfProc:
	IF (@stat <> 0) BEGIN 
	  ROLLBACK TRAN USR_XPercInOutTypeTransfer
	  RETURN @stat
	END	
	
	IF @trnCnt = 0
	  COMMIT TRAN USR_XPercInOutTypeTransfer
	RETURN @stat
	

GO
Print ''
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print 'Completed conversion of Pbsa objects To Version 10.1.3'
Print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
Print ''
Go

