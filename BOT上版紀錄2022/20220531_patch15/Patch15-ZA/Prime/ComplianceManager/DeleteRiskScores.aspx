<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DeleteRiskScores.aspx.vb" Inherits="CMBrowser.DeleteRiskScores" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Delete Unaccepted Risk Scores</title>
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="litMeta" runat="server"></asp:literal><LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" language="JavaScript" src="js/nav.js"></script>
		<script type="text/javascript" language="JavaScript">
		function deletecheck() {
			var dodelete;
			dodelete =  confirm("Are you sure you want to delete all unaccepted risk scores ?");
			return dodelete;			
		}
		</script>
		<style type="text/css">.BoldRed {
	FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: red
}
.BoldBlack {
	FONT-WEIGHT: bold; FONT-SIZE: small; COLOR: black
}
.NormalRed {
	FONT-WEIGHT: normal; FONT-SIZE: x-small; COLOR: red
}
.NormalBlack {
	FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: black
}
.BoldBlue {
	FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: blue
}
		</style>
	</HEAD>
	<body class="formbody" MS_POSITIONING="FlowLayout" onload="AddWindowToPCSWindowCookie(window.name);" onUnload="RemoveWindowFromPCSWindowCookie(window.name);">
		<form id="Form1" method="post" runat="server">
			<table id="PageLayout" cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td><asp:label id="clblRecordMessage" runat="server" cssClass="RecordMessage" width="808px">Record Message</asp:label></td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" Width="616px" Font-Size="Small" Height="24px"></asp:validationsummary></td>
					</tr>
				<tr>
					<td>
						<TABLE id="Table2" style="WIDTH: 500px; HEIGHT: 40px" cellSpacing="2" cellPadding="0" width="816"
							border="0">
							<TR align="center">
								<TD align="center"><asp:button id="cmdDeleteRiskScores" runat="server" Width="132px" Visible="False" Text="Delete Risk Scores"></asp:button>&nbsp;
									<asp:button id="cmdCancelDeletion" runat="server" Width="126px" Visible="False" Text="Cancel Deletion"></asp:button>&nbsp;
									<asp:button id="cmdClose" runat="server" Width="99px" Text="Close"></asp:button></TD>
								<td align="left"></td>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordHeader" runat="server" width="500px" CSSClass="paneltitle"></asp:label></td>
				</tr>
			</table>
			<asp:panel id="Panel1" runat="server" Width="744px" Height="176px" Cssclass="Panel" BorderWidth="1px"
				BorderStyle="None">&nbsp; <BR>
<asp:Literal id="litResult" runat="server"></asp:Literal><INPUT id="hdfldRequestId" type="hidden" name="hdlfldRequestId" runat="server"> </asp:panel><asp:panel id="Panel2" runat="server" Width="712px" Height="15px" cssclass="Panel"></asp:panel><asp:panel id="cPanelValidators" runat="server" Width="864px" Height="46px">
				<uc1:ucrecordscrollbar id="RecordScrollBar" runat="server"></uc1:ucrecordscrollbar>
				<asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder>
			</asp:panel></form>
	</body>
</HTML>
