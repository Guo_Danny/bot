<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Login.aspx.vb" Inherits="CMBrowser.Login" %>
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Login</title>
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="/primecompliancesuite/Styles.css" type="text/css" rel="stylesheet">
		<META http-equiv="Site-Enter" content="revealTrans(Duration=1.0,Transition=2)">
		<META http-equiv="Page-Enter" content="revealTrans(Duration=1.0,Transition=2)">
		<META http-equiv="Page-Exit" content="revealTrans(Duration=1.0,Transition=3)">
		<script type="text/javascript" language="JavaScript" src="js/nav.js"></script>
        
		<script type="text/javascript" language="javascript">
			
	    function PopulateSessionTimeoutFlags()
	    {
	        TimeoutActionValue = '<%=System.Web.Configuration.WebConfigurationManager.AppSettings.Get("ChildWindowSessionTimeoutAction") %>';
            setCookie("PCSTimeoutActionValue", TimeoutActionValue, 1);
            TimeoutHandlingValue = '<%=System.Web.Configuration.WebConfigurationManager.AppSettings.Get("EnableSessionTimeoutHandling") %>';
            setCookie("PCSTimeoutHandlingValue", TimeoutHandlingValue, 1);
        }
        
        </script>
               
    </HEAD>
    <body class="formbody" onload="javascript:ClearPCSsessionTimeoutCookies();PopulateSessionTimeoutFlags();">
		<form id="Form1" method="post" runat="server">
			
            
            <img id="imgPrime" alt="Prime Compliance Suite" src="images/compliance-manager-2.gif"
				class="PageHeaderImg">
                <!--  20191227 for BOT SECURITY ISSUE
                <div><asp:Label id="lblServerName" runat="server" CssClass="SmallLabel"></asp:Label></div> 	
                20191227 for BOT SECURITY ISSUE  -->
			<table width="100%" cellspacing="3" cellpadding="3">
				<tr>
					<td width="10%">&nbsp;</td>
					<td align="right" width="20%">&nbsp;</td>
					<td align="right" width="10%">&nbsp;</td>
					<td width="50%">&nbsp;</td>
					<td width="10%">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2"></td>
					<td align="left" colspan="2">
						<asp:Label id="lblMsg" runat="server"></asp:Label>
						<br>
					</td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2"></td>
					<td align="left" colspan="2">
						<asp:ValidationSummary id="cValidationSummary1" runat="server" Font-Size="Small"></asp:ValidationSummary>
						<br>
					</td>
					<td></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right" colspan="2">
						<asp:Label id="lblOrg" runat="server" CssClass="lblfld">Organization Code</asp:Label>
					</td>
					<td>
						<asp:TextBox id="txtOrgCode" runat="server" Width="200px"></asp:TextBox>
						<asp:RequiredFieldValidator id="RequiredOrgCode" runat="server" ErrorMessage="Please Enter Organization Code"
							ControlToValidate="txtOrgCode" Display="None"></asp:RequiredFieldValidator>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2" align="right">
						<asp:Label id="Label2" runat="server" CssClass="lblfld">User Name</asp:Label>
					</td>
					<td>
						<asp:TextBox id="txtUser" runat="server" Width="200px"></asp:TextBox>
						<asp:RequiredFieldValidator id="RequiredUserName" runat="server" ErrorMessage="Please Enter User Name" ControlToValidate="txtUser"
							Display="None"></asp:RequiredFieldValidator>
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td align="right" colspan="2">
						<asp:Label id="Label3" runat="server" CssClass="lblfld">Password</asp:Label>
					</td>
					<td>
						<INPUT id="txtPwd" type="password" runat="server" style="WIDTH: 200px" size="28" autocomplete="off">
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td align="center" colspan="3">
						<br>
						<asp:CheckBox id="chkSaveLogin" runat="server" CssClass="lblfld" Text="Save Login Credentials"></asp:CheckBox></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="3" align="center">
						<br>
						<hr>
						<br>
						<asp:Button id="cmdLogin" runat="server" CssClass="cmd" Text="Login"></asp:Button>
					</td>
					<td></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
