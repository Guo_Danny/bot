<%@ Register TagPrefix="uc1" TagName="ucItemSelector" Src="ucItemSelector.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucSupportingInfo" Src="ucSupportingInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucRecordScrollBar" Src="ucRecordScrollBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucComboBox" Src="ucComboBox.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="OfacCase.aspx.vb" Inherits="CMBrowser.OfacCase" validateRequest="false"  enableViewState="true"%>
<%@ Register Namespace="Infragistics.WebUI.WebSpellChecker" Assembly="Infragistics2.WebUI.WebSpellChecker.v8.3" TagPrefix="ig_spell" %>

<!DOCTYPE HTML PUBLIC "-//W3C//Dtd HTML 4.0 transitional//EN">
<html>
	<head>
		<title>Ofac Case</title>
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<LINK href="Styles.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" language="JavaScript" src="js/nav.js"></script>
		<style type="text/css">.tranTextLink A:link { FONT-WEIGHT: normal; font-size: 11px; COLOR: blue }
	.tranTextLink A:visited { FONT-WEIGHT: normal; font-size: 11px; COLOR: blue }
	.tranTextLink A:hover { FONT-WEIGHT: normal; font-size: 11px; COLOR: blue }
	.tranTextLink A:active { FONT-WEIGHT: normal; font-size: 11px; COLOR: red }
	.NormalBlue { FONT-WEIGHT: normal; font-size: 11px; CURSOR: hand; COLOR: blue; TEXT-DECORATION: underline }
	.BoldRed { FONT-WEIGHT: bold; font-size: 11px; COLOR: red }
	.BoldBlack { FONT-WEIGHT: bold; font-size: 11px; COLOR: black }
	.NormalRed { FONT-WEIGHT: normal; font-size: 11px; COLOR: red }
	.NormalBlack { FONT-WEIGHT: normal; font-size: 11px; COLOR: black }
	.BoldBlue { FONT-WEIGHT: bold; font-size: 11px; COLOR: blue }
	.cgridheading { PADDING-RIGHT: 15px; FONT-WEIGHT: bold; font-size: x-small; PADDING-BOTTOM: 3px; VERTICAL-ALIGN: baseline; COLOR: white; PADDING-TOP: 1px; BACKGROUND-COLOR: #3366cc; TEXT-ALIGN: left }
		</style>
		<script type="text/javascript" language="JavaScript" >
			var text = "";
			var CurrWindowName = "";
   			var refreshKeyPressed = false;
			var unLocked = false; 
			document.onkeydown = checkKeycode;
			
			
			function checkKeycode(e) {
			try
                {
				var keycode;
			if (window.event)
				keycode = window.event.keyCode;
			else if (e)
			keycode = e.which;
			if(keycode == 116 || (window.event.ctrlKey && keycode == 82)|| (keycode == 93)) 
			{
				refreshKeyPressed = true;
			}
			else  
            {  
                refreshKeyPressed = false;  
            }  
			}
                catch(ex){}

			}	
			
			function rightclick()   
               {  
			    try
                {
                       var rightclick;  
                       var e = window.event;  
                       if (e.which) rightclick = (e.which == 3);  
                       else if (e.button) rightclick = (e.button == 2);  
                       if(rightclick)  {  
                       refreshKeyPressed = true;  
                   }  
                       else    {  
                       refreshKeyPressed = false;  
                   }     
				   }
                catch(ex){}
               }  
			   
      


            function SetTranTextInitialScrollBarPos()
            {
                try
                {			       
                    var topPos = document.getElementById('tranTextHighlightedTextForScrollBarPos').offsetTop - 36;
                    document.getElementById('pnlTranTextContainerDiv').scrollTop = topPos;
                }
                catch(ex){}
               
            }
			function GetActiveText(e) { 
				try
				{
					text = (document.all) ? document.selection.createRange().text : document.getSelection();
					document.Form1.hdfldHighlightedText.value = text;
					return true;
				}
				catch(e)
				{
					
				}	
			}
			function OpenExcludeWindow(RecordId) { 
				try {
					var url;
					var options;
					var highlightedText = new String("");				
					highlightedText = document.Form1.hdfldHighlightedText.value;
					if (highlightedText != "" ) {					
						highlightedText = escape(highlightedText.replace(/\s+/g, " "));										
					} 				
					RecordId = escape(RecordId);
              		url = "ExcludeName.aspx" + "?HighlightedText=" + highlightedText + 
							"&ParentPage=OfacCase&ActionID=1&RowNum=0&NoScroll=1" + "&CaseId=" + RecordId + "&Parameter1=" + document.Form1.ddlBranch.value + "&Parameter2=" + document.Form1.ddlDepartment.value;
					options = "scrollbars=yes,resizable=yes,status=yes,menubar=no,toolbar=no,width=980,height=800";			
					
					var w = window.open(url, 'ExcludeName', options); 			
					w.focus();			
					return true;
				}
				catch(e)
				{
					
				}					
			}
			function scrolltomatch()
			{
				var lnkId;
				var objLnk
				try
				{
					lnkId = document.forms[0].elements["hfldSelectedLinkId"].value;
					if (lnkId.length > 0)
					{
						objLnk =document.getElementById(lnkId);
						if (objLnk != null)
						{
							objLnk.scrollIntoView();
							window.scrollTo(0,0);
						}
					}
				}
				catch(e)
				{
				
				}
			}
			function SetWindowName()
			{
			    try
			    {	
			        var recordid;
                    var casewindowname;
			        recordid = document.getElementById("txtCode").value;	
					
// updated the window name from OFACCase to OFACCaseArchive to properly set the object type when adding document or tasks
			 if (document.forms[0].elements["hfldArchived"].value == 'archived')
                   casewindowname = 'OFACCaseArchive';
             else
                   casewindowname = 'OFACCase';

                                                    
              if (recordid != null || (typeof(recordid) != 'undefined'))
                     window.name = casewindowname + recordid;
              else 
                     window.name = casewindowname;

					CurrWindowName = window.name;					
					AddWindowToPCSWindowCookie(CurrWindowName);
			    }
			    catch(ex){}
			   
			}
			
			function UnlockRecord()
			{
      
			    try
			    {
					RemoveWindowFromPCSWindowCookie(CurrWindowName);
					CurrWindowName = "";
				var selected = document.activeElement;  
				if (!selected) 
                {   
				  if ((!refreshKeyPressed) && (!unLocked))				
                    {			 
			            closecasewindow ("Violation");
			        }
                }
			    }
			    catch(ex){}
			}
					
			function closecasewindow (objectname)
			{	
				var recordid;
				var objectname;
				var sessiontype;
				var itab;			
				try
				{
				    var unLockCase = document.getElementById("hdfldunlockcase").value;
				    if (unLockCase == "0")
				    {
					recordid = document.getElementById("txtCode").value;				
					sessiontype = 1;			
					var sPage = "UnlockRecord.aspx?RecordId=" + recordid + "&ObjectName=" + objectname +
					"&sessiontype=" + sessiontype;
					var param="resizable=no,status=no,menubar=no,toolbar=no,scrollbars=no,left=10000,top=10000,height=1,width=1"; 
			     	var nw=window.open(sPage,'', param);				    
			     	}			    
			     	window.close();
			     	unLocked = true; 
				}
				catch(e)
				{
				
				}
			}	
			document.onmouseup = GetActiveText;
			if (!document.all) document.captureEvents(Event.MOUSEUP);
		</script>

        <link href="Styles.css" rel="stylesheet" type="text/css" />
	</head>
	<body class="formbody" onload="SetWindowName();SetTranTextInitialScrollBarPos();" onUnload="UnlockRecord();" onmousedown="rightclick();"> 
		<form id="Form1" method="post" runat="server">
			<table id="PageLayout" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<table cellspacing="2" cellpadding="0" width="100%" border="0">
							<tr>
								<td width="600"><asp:imagebutton id="imgPrime" runat="server" width="808px" AlternateText="Home" ImageUrl="images/compliance-manager.gif"></asp:imagebutton></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordMessage" runat="server" width="808px" cssclass="RecordMessage">Record Message</asp:label></td>
				</tr>
				<tr>
					<td><asp:validationsummary id="cValidationSummary1" runat="server" font-size="Small" width="616px"></asp:validationsummary></td>
				</tr>
				<!-- no start tag </td> -->
				<tr>
					<td>
						<table id="table2" style="width: 816px; height: 40px" cellspacing="2" cellpadding="0" width="816"
							border="0">
							<tr align="center">
								<td align="right" width="73%"><asp:placeholder id="cPlaceHolder1" runat="server"></asp:placeholder></td>
								<td align="left" width="27%"><uc1:ucrecordscrollbar id="RecordScrollBar" runat="server"></uc1:ucrecordscrollbar></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><uc1:uctabcontrol id="MainTabControl" runat="server"></uc1:uctabcontrol></td>
				</tr>
				<tr>
					<td><asp:label id="clblRecordHeader" runat="server" width="808px" cssclass="paneltitle"></asp:label></td>
				</tr>
			</table>
			<asp:panel id="Panel1" runat="server" height="200px" width="750px" BorderStyle="None" Borderwidth="1px"
				cssclass="Panel">
				<table id="table9" cellspacing="2" cellpadding="0" border="0">
					<tr>
						<td>
							<asp:label id="Label1" runat="server" width="144px" cssclass="LABEL">Case Number:</asp:label></td>
						<td>
							<asp:textbox id="txtCode" runat="server" cssclass="TEXTBOX"></asp:textbox></td>
						<td style="text-align:right">
							<asp:label id="Label2" runat="server" cssclass="LABEL">Status:</asp:label></td>
						<td>
							<asp:textbox id="txtCaseStatus" runat="server" width="206px" cssclass="TEXTBOX"></asp:textbox></td>
						<td></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label24" runat="server" width="144px" cssclass="LABEL">Reference:</asp:label></td>
						<td>
							<asp:textbox id="txtreference" runat="server" cssclass="TEXTBOX"></asp:textbox></td>
						<td style="text-align:right">
							<asp:label id="lblSource" runat="server" cssclass="LABEL">Source:</asp:label></td>
						<td>
							<uc1:ucComboBox id="cboSource" runat="server"></uc1:ucComboBox></td>
						<td></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblUserMsgReference" runat="server" Width="144px" CssClass="LABEL">User Msg Reference:</asp:label></td>
						<td>
							<asp:textbox id="txtUserMsgReference" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="lblScore" runat="server" Width="144px" CssClass="LABEL">Score:</asp:label></td>
						<td>
							<asp:textbox id="txtScore" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td></td>
					</tr>					
					<tr>
						<td>
							<asp:label id="lblBranch" runat="server" width="144px" cssclass="LABEL">Branch:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlBranch" runat="server" width="208px"></asp:DropDownList></td>
						<td>
							<asp:label id="lblDepartment" runat="server" width="144px" cssclass="LABEL">Department:</asp:label></td>
						<td>
							<asp:DropDownList id="ddlDepartment" runat="server" width="208px"></asp:DropDownList></td>
						<td></td>
					</tr>
				</table>

				<table style="width:808px" cellspacing="2" cellpadding="0" border="0">
				    <tr>
				        <td style="width:195px;">
				            <!--Matched Text-->
				            <span id="lblMatchedText" class="gridheader" style="width:220px;display:block;">Matched Text</span>
				            <div style="OVERFLOW: auto; height: 200px">
                                <asp:DataGrid id="dgMatchedText" runat="server" width="220px" backcolor="White" cellpadding="3" GridLines="None" ForeColor="Black" showheader="False" autogeneratecolumns="False">
		                            <FooterStyle backcolor="#CCCCCC"></FooterStyle>
		                            <Selecteditemstyle font-bold="true" ForeColor="White" backcolor="#000099"></Selecteditemstyle>
		                            <Alternatingitemstyle backcolor="#CCCCCC"></Alternatingitemstyle>
		                            <headerstyle font-bold="true" font-size="11px" ForeColor="White" backcolor="#990000"></headerstyle>
		                            <Columns>
		                                <asp:TemplateColumn>
		                                    <ItemStyle font-size="11px" />
		                                    <ItemTemplate>
                                                <asp:LinkButton ID="lnkMatchedText" runat="server" Text='<%# ReplaceCRLF(Microsoft.Security.Application.Encoder.HtmlEncode(Container.DataItem)) %>' CausesValidation="false" OnClick="MatchedTextClick">
		                                        </asp:LinkButton>
		                                    </ItemTemplate>
		                                </asp:TemplateColumn>
		                            </Columns>
	                            </asp:DataGrid>
				            </div>
				        </td>
				        <td style="width:5px">&nbsp;</td>
				        <td style="width:608px">
				            <!--Transaction Text -->
				            <asp:label id="Label4" runat="server" width="100%" cssclass="paneltitle">Transaction Text</asp:label>
				            <div id="pnlTranTextContainerDiv" style="OVERFLOW: scroll; width: 100%; height: 200px">
					            <asp:Panel id="pnltranText" runat="server" height="200px" cssclass="tranTextLink" EnableViewState="False"></asp:Panel>
				            </div>
				        </td>
				    </tr>
				</table>

				<asp:label id="lblMatchDetails" runat="server" width="808px" cssclass="paneltitle">Details</asp:label>
				<asp:Panel id="pnlMatchDetails" runat="server" width="808px" height="148px">
					<table id="table1" cellspacing="2" cellpadding="2" width="808" border="0">
						<tr>
							<td valign="top" width="220">
								<asp:label id="lblMatchedPartiesHeader" runat="server" width="220px" cssclass="gridheader">Matched Party Name</asp:label>
								<DIV style="OVERFLOW: auto; height: 226px">
									<asp:DataGrid id="dgMatchedPartyNames" runat="server" width="220px" backcolor="White" cellpadding="3"
										GridLines="None" ForeColor="Black" showheader="False" autogeneratecolumns="False">
										<FooterStyle backcolor="#CCCCCC"></FooterStyle>
										<Selecteditemstyle font-bold="true" ForeColor="White" backcolor="#000099"></Selecteditemstyle>
										<Alternatingitemstyle backcolor="#CCCCCC"></Alternatingitemstyle>
										<headerstyle font-bold="true" font-size="11px" ForeColor="White" backcolor="#990000"></headerstyle>
										<Columns>
											<asp:TemplateColumn HeaderText="Sanction Party Name">
												<itemstyle font-size="11px"></itemstyle>
												<HeaderTemplate>
													<b>Matched Party Name </b>
												</HeaderTemplate>
												<ItemTemplate>
													<asp:LinkButton id="lnkPartyName" Text='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container.DataItem, "PartyName").ToString()) %>' CausesValidation="false" OnClick="MatchedPartyClick" runat="server">
														<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container.DataItem, "PartyName").ToString())%>
													</asp:LinkButton>
													<asp:Label ID="lblDeletedStarus" Font-Italic="true" 
													 ForeColor="red" runat="server"
													 Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtml(DataBinder.Eval(Container.DataItem, "DeletedStatus").ToString()) %>'></asp:Label>
													<input id="hdfldPartyNameLength"  runat="server" type="hidden" 
													value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container.DataItem,"PartyNameLength").ToString())%>' name="hdfldPartyNameLength" />
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<itemstyle></itemstyle>
												<ItemTemplate>
												    <input id="hdfldMatchName" runat="server" type="hidden"
												    value='<%# DataBinder.Eval(Container, "DataItem.MatchName")%>' name="hdfldMatchName"/>
													<input id="hdfldMatchText"  runat="server" type="hidden" 
													value='<%# DataBinder.Eval(Container, "DataItem.MatchText")%>' name="hdfldMatchText" />
													<input id="hdfldEntNum"  runat="server" type="hidden" 
													value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.EntNum").ToString())%>' name="hdfldEntNum" />
													<input id="hdfldProgram"  runat="server" type="hidden" 
													value='<%# Microsoft.Security.Application.Encoder.HtmlEncode(DataBinder.Eval(Container, "DataItem.Program").ToString())%>' name="hdfldProgram" />
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle horizontalalign="Center" ForeColor="Black" backcolor="#999999"></PagerStyle>
									</asp:DataGrid></DIV>
							</td>
							<td>&nbsp;</td>
							<td valign="top" align="left" width="548px">
								<asp:label id="lblPartyDeatailsHeader" runat="server" width="548px" cssclass="gridheader">Matched Party Name</asp:label>								
                                <asp:Panel ID="pnlEntNumbers" runat="server" Width="540px" Visible="false" >                                
							        <table width="540px" cellpadding="0" cellspacing="2" >
							            <tr>
							                <td style="height: 36px">								        
							                    <asp:label id="lblPartyID" runat="server" 
							                    width="248px" cssclass="LABEL" >Party ID:</asp:label>
							                </td>
							                <td style="height: 36px">
							                    <asp:DropDownList ID="ddlEntNumbers" runat="server" 
							                    Width="92px" AutoPostBack="true" ></asp:DropDownList>
							                </td>
							                <td style="height: 36px">
							                    &nbsp;<asp:label id="lblEntNumbers" runat="server" 
							                    Font-Bold="true" width="200px" Font-Size="Smaller" ></asp:label>
							                </td>								
							            </tr>
							        </table>
                                </asp:Panel>
                                 <DIV style="OVERFLOW: auto; width: 540px; height: 226px">								
									<asp:DataGrid id="dgMatchedPartyDetails" runat="server" width="548px" backcolor="White" cellpadding="3"
										GridLines="None" ForeColor="Black" showheader="False" autogeneratecolumns="False">
										<FooterStyle backcolor="#CCCCCC"></FooterStyle>
										<Selecteditemstyle font-bold="true" ForeColor="White" backcolor="#000099"></Selecteditemstyle>
										<Alternatingitemstyle backcolor="#CCCCCC"></Alternatingitemstyle>
										<headerstyle font-size="11px" font-bold="true" ForeColor="White" backcolor="#990000"></headerstyle>
										<Columns>
											<asp:boundcolumn DataField="Name">
												<headerstyle ForeColor="#CCCCE5" backcolor="#CCCCE5"></headerstyle>
												<itemstyle font-size="11px" font-bold="true" horizontalalign="Right" verticalalign="Top" backcolor="#CCCCE5"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn DataField="Value" HeaderText="Matched Party Details">
												<itemstyle font-size="11px" width="420px" verticalalign="Top"></itemstyle>
											</asp:boundcolumn>
										</Columns>
										<PagerStyle horizontalalign="Center" ForeColor="Black" backcolor="#999999"></PagerStyle>
									</asp:DataGrid></DIV>
							</td>
						</tr>
					</table>
					<table cellspacing="2" cellpadding="0" width="300" border="0">
						<tr>
							<td valign="bottom" style="height: 24px">
								<asp:label id="Label8" runat="server" width="100px" Height="15px" cssclass="LABEL">Add New Note:</asp:label>
                                <asp:Label ID="lblNoteValidateMsg1" runat="server" CssClass="usererror"></asp:Label></td>
							<td align="right" style="height: 24px"><button id="btnSpell1"  runat="server"   type="button">Spell Check</button> 
                                &nbsp;<asp:Button id="cmdAnnotate1" runat="server" cssclass="Button" Text="Annotate"></asp:Button></td>
						</tr>
						<tr>
							<td colspan="2">
								<asp:textbox id="txtAddNote1" runat="server" width="808px" height="40px" TextMode="MultiLine"></asp:textbox></td>
						</tr>
						<tr>
							<td valign="top" colspan="2">
								<asp:label id="Label9" runat="server" width="100px" Height="15px" cssclass="LABEL">Existing Notes:</asp:label></td>
						</tr>
						<tr>
							<td colspan="2">
								<asp:textbox id="txtNotes1" runat="server" width="808px" height="100px" TextMode="MultiLine"></asp:textbox></td>
						</tr>
					</table>
				</asp:Panel>
				<INPUT id="hdfldtrantext" type="hidden" runat="server"> <INPUT id="hdfldHighlightedText" type="hidden" name="hdfldHighlightedText" runat="server">
				<INPUT id="hdfldpreviouscaseid" type="hidden" runat="server"> 				
                <INPUT id="hdfldunlockcase" type="hidden" runat="server" name="hdfldunlockcase">
			</asp:panel><asp:panel id="Panel2" runat="server" height="46px" width="712px" cssclass="Panel">&nbsp; 
<table id="table5" cellspacing="2" cellpadding="0" width="300" border="0">
					<tr>
						<td>
							<asp:label id="Label18" runat="server" Width="144px" CssClass="LABEL">File Image:</asp:label></td>
						<td>
							<asp:textbox id="txtFileImage" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label19" runat="server" Width="144px" CssClass="LABEL">Request Date:</asp:label></td>
						<td>
							<asp:textbox id="txtRequestDate" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label27" runat="server" Width="144px" CssClass="LABEL">Reply Status:</asp:label></td>
						<td>
							<asp:textbox id="txtReplyStatus" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label23" runat="server" Width="144px" CssClass="LABEL">Reply Date:</asp:label></td>
						<td>
							<asp:textbox id="txtReplyDate" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td style="HEIGHT: 2px">
							<asp:label id="Label29" runat="server" Width="144px" CssClass="LABEL">Review Status:</asp:label></td>
						<td style="HEIGHT: 2px">
							<asp:textbox id="txtReviewStatus" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label25" runat="server" Width="144px" CssClass="LABEL">Approve Status:</asp:label></td>
						<td>
							<asp:textbox id="txtApproveStatus" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="Label21" runat="server" Width="144px" CssClass="LABEL">Review Date:</asp:label></td>
						<td>
							<asp:textbox id="txtReviewDate" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="HEIGHT: 2px">
							<asp:label id="Label31" runat="server" Width="144px" CssClass="LABEL">Approve Date:</asp:label></td>
						<td style="HEIGHT: 2px">
							<asp:textbox id="txtApproveDate" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td style="HEIGHT: 7px">
							<asp:label id="Label28" runat="server" Width="144px" CssClass="LABEL">Review Oper:</asp:label></td>
						<td style="HEIGHT: 7px">
							<asp:textbox id="txtReviewOper" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td style="HEIGHT: 7px">
							<asp:label id="Label34" runat="server" Width="144px" CssClass="LABEL">Approve Oper:</asp:label></td>
						<td style="HEIGHT: 7px">
							<asp:textbox id="txtApproveOper" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblAmount" runat="server" Width="144px" CssClass="LABEL">Amount:</asp:label></td>
						<td>
							<asp:textbox id="txtAmount" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
						<td>
							<asp:label id="Label7" runat="server" Width="144px" CssClass="LABEL">System Reference:</asp:label></td>
						<td>
							<asp:textbox id="txtSysRef" runat="server" CssClass="TEXTBOX"></asp:textbox></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</table>
<asp:label id="Label5" runat="server" width="808px" cssclass="paneltitle">Options</asp:label>

<table id="table3" cellspacing="2" cellpadding="0" width="300" border="0">
					<tr>
						<td>
							<asp:checkbox id="chkPromptExcludeOnWaive" runat="server" cssclass="CheckBox" Text="Prompt Exclude On Waive"
								autopostback="true"></asp:checkbox></td>
						<td>

						</td>
					</tr>
					<tr>
						<td>
							<asp:radiobutton id="rbExcludeText" runat="server" cssclass="CheckBox" Text="Exclude Text" autopostback="true"
								groupname="ExcludeType"></asp:radiobutton>
                            </td>
                        <td>
							<asp:radiobutton id="rbExcludeCustomer" runat="server" cssclass="CheckBox" Text="Exclude Customer"
								autopostback="true" groupname="ExcludeType"></asp:radiobutton>
						</td>
						
					</tr>
					
				</table></asp:panel><asp:panel id="Panel3" runat="server" height="15px" width="808px" cssclass="Panel">
				<table id="table4" cellspacing="2" cellpadding="0" width="100%" border="0">
					<tr>
						<td colspan="4" height="5">&nbsp;</td>
					</tr>
					<tr>
						<td width="7">&nbsp;</td>
						<td>
							<asp:DropDownList id="ddlRelatedCases" runat="server" width="256px" cssclass="DropDownList" autopostback="true"></asp:DropDownList></td>
						<td valign="bottom">
							<asp:Label id="lblRelatedCases" runat="server" width="190px" height="20px" cssclass="LABEL"></asp:Label></td>
						<td>
							<asp:DropDownList id="ddlMatchNames" runat="server" width="349px" autopostback="true" Visible="False"></asp:DropDownList></td>
					</tr>
					<tr>
						<td colspan="4" height="5">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="4">
							<uc1:ucView id="vwRelatedCases" runat="server"></uc1:ucView></td>
					</tr>
				</table>
			</asp:panel><asp:panel id="Panel4" runat="server" height="15px" width="712px" cssclass="Panel">
				<uc1:ucSupportingInfo id="supportingInfo" runat="server"></uc1:ucSupportingInfo>
			</asp:panel><asp:panel id="Panel5" runat="server" height="15px" width="712px" cssclass="Panel">
				<table cellspacing="2" cellpadding="2" width="300" border="0">
					<tr>
						<td valign="bottom">
							<asp:label id="Label6" runat="server" width="100px" cssclass="LABEL">Add New Note:</asp:label>
                            <asp:Label ID="lblNoteValidateMsg" runat="server" CssClass="usererror"></asp:Label></td>
						<td align="right"><button id="btnSpell"  runat="server"   type="button">Spell Check</button>
							<asp:Button id="cmdAnnotate" runat="server" cssclass="Button" Text="Annotate"></asp:Button></td>
					</tr>
					<tr>
						<td colspan="2">
							<asp:textbox id="txtAddNote" runat="server" width="808px" height="50px" TextMode="MultiLine"></asp:textbox></td>
					</tr>
					<tr>
						<td valign="top" colspan="2">
							<asp:label id="Label3" runat="server" width="104px" height="15px" cssclass="LABEL">Existing Notes:</asp:label></td>
					</tr>
					<tr>
						<td colspan="2">
							<asp:textbox id="txtNotes" runat="server" width="808px" height="400px" TextMode="MultiLine"></asp:textbox></td>
					</tr>
				</table>
			</asp:panel><asp:panel id="Panel6" runat="server" height="38px" width="712px" cssclass="Panel">
				<table cellspacing="2" cellpadding="0" width="300" border="0">
					<tr>
						<td style="height: 16px">
							<asp:label id="lblReviewOper" runat="server" cssclass="LABEL">Assign Branch:</asp:label></td>
						<td style="width: 210px; height: 16px">
							<asp:DropDownList id="ddlAssignBranch" runat="server" cssclass="DropDownList" Visible="true"></asp:DropDownList></td>
						<td style="height: 16px">
							<asp:Button id="cmdRouteOk" runat="server" cssclass="Button" Text="Route" CausesValidation="False"></asp:Button></td>
					</tr>
					<tr>
						<td>
							<asp:label id="lblApproveOper" runat="server" cssclass="LABEL">Assign Department:</asp:label></td>
						<td style="width: 210px">
							<asp:DropDownList id="ddlAssignDepartment" runat="server" cssclass="DropDownList"></asp:DropDownList></td>
						<td></td>
					</tr>
				</table>
			</asp:panel><asp:panel id="cPanelValidators" runat="server" height="46px" width="864px"></asp:panel><INPUT id="hfldSelectedLinkId" type="hidden" runat="server">
			<INPUT id="hfldArchived" type="hidden" runat="server">
			<ig_spell:WebSpellChecker ID="WebSpellChecker1" runat="server"  ></ig_spell:WebSpellChecker>
			<ig_spell:WebSpellChecker ID="WebSpellChecker2" runat="server"  ></ig_spell:WebSpellChecker>
		</form>
	</body>
</html>
