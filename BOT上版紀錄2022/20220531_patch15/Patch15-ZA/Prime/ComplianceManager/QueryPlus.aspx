<%@ Page Language="vb" AutoEventWireup="false" Codebehind="QueryPlus.aspx.vb" Inherits="CMBrowser.QueryPlus" smartNavigation="True" validateRequest=false%>
<%@ Register TagPrefix="uc1" TagName="ucActivityList" Src="ucActivityList.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucTabControl" Src="ucTabControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucPageHeader" Src="ucPageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucTaskBar" Src="ucTaskBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucView" Src="ucView.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>QueryPlus</title>
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" language="JavaScript" src="js/nav.js"></script>
		<script type="text/javascript" language="javascript" id="clientEventHandlersJS">

	
	<!--
var rightclickrefreshkeypressed = false;
document.onkeydown = fkey;

var f5keypressed = false;

function fkey(e){
        e = e || window.event;
       if( f5keypressed ) return; 

        if (e.keyCode == 116) {
           f5keypressed = true;
        }
 }
 function RightClick() {
          rightclickrefreshkeypressed=true;
        };

function UnLockRecord()		
{	
	try
	{
		var keycode;
		var result = 0;
		var revviol = 0;			
		// Code to handle f5 key and rightclick+refresh                
		if(!rightclickrefreshkeypressed && !f5keypressed)
		   {         
			revviol = document.getElementById("hdfldRevviol").value;			        
			if (revviol)
			{
				ClearAllOfacLock();
			}				    
		}
	}
	catch(ex){}
}
		function document_onkeypress() 
		{

			try
			{
				if (event.keyCode == 13)	
				{	
	
					//Do not cancel Enter key event fot TextArea controls
					if (event.srcElement.id != "txtNLQ" && event.srcElement.id != "txtSQL" &&
					    event.srcElement.id != "txtMatchText")
					{
				
						//If you want, simulate cmdQuickSearch_Click event 
						//or any other ASP.NET server site event using
						// the same way ASP.NET does.
						//If you do not want any postback just comment out next line
						__doPostBack("cmdQuickSearch", "");
					
					
						//Cancel the postback event from the Enter key
						window.event.returnValue=false;
					}
				}
				
			}
			catch(ex){}	

		}
		function SetFullTextIndexTextBoxState() 
		{
			var ctrl;	
			try
			{			
			    if (document.getElementById("hdfldObjectId").value == "3") 
			    {
			        ctrl = document.getElementById("txtParam17");				
				    if (document.getElementById("cboArchive").value == "1") 
				    {		   
					    ctrl.style.width = "180px"
					    ctrl.attributes.removeNamedItem("disabled");
                        ctrl.style.color = "black";
                        ctrl.style.backgroundColor = "white";
					}
			    }
			 }
			 catch(ex) {}
		}
		
		function ChangeName()
		{
			var viewtitle = document.getElementById('ViewGrid_lblViewTitle');
			
			if(!viewtitle && typeof(viewtitle) !== 'undefined' && viewtitle != 0)
			{
				
			}
			else if(viewtitle.innerHTML.replace('&nbsp;','').replace('&nbsp;','').replace('&nbsp;','') == '*RiskScore-MLCO')
			{
				document.forms[0].elements["ViewGrid$ctl05"].value = 'Reject';
			}
			
		}
//-->
		</script>
		<script type="text/javascript" language="javascript" event="onkeypress" for="document">
<!--

return document_onkeypress()
//-->
		</script>
	</HEAD>
	<body onunload="UnLockRecord();" oncontextmenu="RightClick(this); return true;" onload="ChangeName();">
		<form id="Form1" method="post" runat="server">
			<uc1:ucpageheader id="PageHeader" runat="server"></uc1:ucpageheader>
			<table id="PageLayout" cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td id="TaskBarLayout" vAlign="top"><br>
						<uc1:uctaskbar id="TaskBar" runat="server"></uc1:uctaskbar></td>
					<td id="FormLayout" vAlign="top">
						<table cellSpacing="2" cellPadding="0" width="850" border="0">
							<tr>
								<td width="1">&nbsp;</td>
								<td align="center" width="768"><asp:label id="lblMessage" runat="server"></asp:label>
								</td>
								<td width="1">&nbsp;</td>
							</tr>
							<tr>
								<td></td>
								<td vAlign="top"><uc1:uctabcontrol id="TabControl" runat="server"></uc1:uctabcontrol><asp:panel id="TabControlPad" runat="server" Width="100%" HorizontalAlign="Right" CSSClass="tabpad"
										Height="16px" BackColor="#3366CC">
										<asp:Label id="Label1" runat="server"  style= "padding-top: 0px;" CSSClass="paneltitle">Query Plus</asp:Label>
									</asp:panel><asp:panel id="Panel1" runat="server">
										<TABLE class="tblFields" cellSpacing="2" cellPadding="0" width="100%" border="0">
                                            <tr style="height:5px"><td></td></tr>
											<TR class="TabSelected">
												<TD class="TabSelected">
													<asp:Label id="lblTitlePanel1" runat="server" CSSClass="paneltitle">Specify Search Parameters</asp:Label></TD>
											</TR>
											<TR>
												<TD>
													<asp:Label id="lblActivityMsg" Visible="False" Runat="server"></asp:Label>
													<asp:Label id="lblQuickSearchMsg" runat="server" CssClass="usererrorsummary"></asp:Label>
													<TABLE>
														<TR>
															<TD vAlign="top">
																<asp:Table id="tblQuickSearch" runat="server"></asp:Table>
																<asp:Table id="tblAdditionalParameters" runat="server"></asp:Table>
																<asp:HyperLink id="lnkAdditionalParameters" runat="server" Visible="False" CssClass="lnktask">More parameters...</asp:HyperLink></TD>
															<TD vAlign="top">
																<asp:Panel id="pnlListBy" runat="server" Width="150" Visible="False" BorderStyle="Outset">
																	<asp:Table id="grdListBy" runat="server" Visible="True"></asp:Table>
																</asp:Panel></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD align="left">
													<asp:Panel id="pnlOFAC" runat="server" Visible="False">
														<TABLE cellSpacing="2" cellPadding="2" width="500">
															<TR>
																<TD align="center" colSpan="4">
																	<HR>
																	<asp:Label id="lblOFAC" runat="server" CssClass="lblfld">Sanctioned Parties Advanced Search</asp:Label><BR>
																	<asp:Label id="lblOFACMsg" runat="server"></asp:Label></TD>
															</TR>
															<TR>
																<TD align="right" width="180">
																	<asp:Label id="lblOFACAlgorithm" runat="server" CssClass="lblfld" Width="200px">Matching Algorithm</asp:Label></TD>
																<TD width="180">
																	<asp:DropDownList id="cboAlgorithm" runat="server">
																		<asp:ListItem Value="0" Selected="True">Match Partial</asp:ListItem>
																		<asp:ListItem Value="1">Match Full Name</asp:ListItem>
																		<asp:ListItem Value="2">Match Text</asp:ListItem>
																	</asp:DropDownList></TD>
																<TD align="right" width="180">
																	<asp:Label id="lblFileImage" runat="server" CssClass="lblfld" Width="200px">Sanctioned Data Rule</asp:Label></TD>
																<TD align="left" width="260">
																	<asp:DropDownList id="cboFileImage" runat="server" Width="250px"></asp:DropDownList></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblMatchName" runat="server" CssClass="lblfld"> Name to match</asp:Label></TD>
																<TD colSpan="3">
																	<asp:TextBox id="txtMatchName" runat="server" Width="610px" ToolTip="Name to Match against Sanctioned Parties List(s)"></asp:TextBox>
																	<asp:TextBox id="txtMatchText" runat="server" Height="54px" Width="610px" ToolTip="Text to Match for Sanctioned Parties mentioned in it"
																		TextMode="MultiLine" Columns="75"></asp:TextBox></TD>
															</TR>
														</TABLE>
													</asp:Panel></TD>
											</TR>
											<TR>
												<TD align="center">
													<HR>
													<asp:label id="Label2" runat="server" CssClass="lblfld">Query Timeout:</asp:label>
													<asp:textbox id="txtTimeout" runat="server" Width="50px">15</asp:textbox>
													<asp:label id="Label5" runat="server" CssClass="lblfld">minutes</asp:label>&nbsp;&nbsp;
													<asp:Button id="cmdQuickSearch" runat="server" CssClass="cmd" Text="Search"></asp:Button>&nbsp;
													<asp:Button id="cmdClear" runat="server" Text="Clear"></asp:Button>&nbsp;&nbsp;
													<asp:label id="Label6" runat="server" CssClass="lblfld">View Name:</asp:label>
													<asp:textbox id="txtViewName" runat="server"></asp:textbox>
													<asp:CheckBox id="chkPublicQuickSearch" runat="server" CssClass="lblfld" Text="Public"></asp:CheckBox>
													<asp:button id="cmdSave" runat="server" CssClass="cmd" Text="Save"></asp:button><INPUT id="hfldDelView" type="hidden" name="hfldDelView" runat="server">
													<BR>
												</TD>
											</TR>
										</TABLE>
									</asp:panel><asp:panel id="Panel2" runat="server">
										<TABLE class="tblFields" cellSpacing="2" cellPadding="0" width="100%" border="0">
											<TR class="TabSelected">
												<TD class="TabSelected" colspan="9">
													<asp:Label id="lblTitlePanel3" runat="server" CSSClass="paneltitle">SQL Query</asp:Label></TD>
											</TR>
											<TR>
												<TD  colspan="9"><BR>
													<asp:panel id="pnlSQL" runat="server">
														<asp:Label id="Label3" runat="server" CssClass="lblfld">Here is the last executed SQL statement. You can modify where clause of this statement and perform custom search</asp:Label>
														<HR>
														<asp:Label id="lblSQLMsg" runat="server"></asp:Label>
														<BR>
														<asp:Label id="lblSQL" runat="server"></asp:Label>
													</asp:panel></TD>
											</TR>
											<TR>
												<TD  colspan="9" align="center" width="100%"><TEXTAREA id="txtSQL" name="txtNLQ" rows="15" cols="95" runat="server" style="width: 860px"></TEXTAREA><BR>
												</TD>
											</TR>
                                            <tr>
                                                <td  colspan="9" style="height:5px"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="9" style="height:10px"><HR></td>
                                            </tr>
											<TR>
                                                
												<TD align="center">													
													<asp:label id="Label9" runat="server" CssClass="lblfld">Query Timeout:</asp:label>
                                                </td>
                                                <td>
													<asp:textbox id="txtTimeOutSQL" runat="server" Width="50px">15</asp:textbox>        
                                                </td>
                                                <td align="center">
													<asp:label id="Label10" runat="server" CssClass="lblfld">minutes</asp:label>&nbsp;&nbsp;</td>
                                                <td>
													<asp:Button id="cmdSQLFind" runat="server" CssClass="cmd" Text="Search"></asp:Button>&nbsp;
													<asp:Button id="cmdClearSQL" runat="server" Text="Clear"></asp:Button>&nbsp;&nbsp;
                                                    </td>
                                                <td>
													<asp:label id="Label11" runat="server" CssClass="lblfld">View Name:</asp:label>
                                                </td>
                                                <td>
													<asp:textbox id="txtViewNameSQL" runat="server"></asp:textbox>
                                                </td>
                                                <td>
													<asp:CheckBox id="chkPublicSQL" runat="server" Text=""></asp:CheckBox>
                                                </td>
                                                <td align="left">
                                                    <asp:label id="Label4" runat="server" CssClass="lblfld">Public</asp:label>
                                                </td>
                                                <td>
													<asp:button id="cmdSaveSQL" runat="server" CssClass="cmd" Text="Save"></asp:button><BR>
												</TD>
											</TR>
										</TABLE>
									</asp:panel><asp:panel id="Panel3" runat="server">
										<TABLE class="tblFields" cellSpacing="0" cellPadding="0" width="100%" border="0">
                                           
											<TR class="TabSelected">
												<TD class="TabSelected">
													<asp:Label id="lblTitlePanel4" runat="server" style="vertical-align: text-top;" CSSClass="paneltitle">Search Results</asp:Label></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:Label id="lblListTitle" runat="server" CssClass="lblfld"></asp:Label><BR>
													<uc1:ucView id="ViewGrid" runat="server"></uc1:ucView><BR>
													<asp:Label id="lblDetailsTitle" runat="server" CssClass="lblfld"></asp:Label><BR>
													<uc1:ucView id="ViewDetailsGrid" runat="server"></uc1:ucView></TD>
											</TR>
										</TABLE>
										<INPUT id="hdfldObjectId" type="hidden" name="hdfldObjectId" runat="server">
										<INPUT id="hdfldRevviol" type="hidden" name="hdfldRevviol" runat="server">
									</asp:panel></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td align="center"></td>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
