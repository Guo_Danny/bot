	function AddOrModifyAddess(actn, recordID) {
			
	var editFirstPart = '<A href="#" onclick="AddOrModifyAddess(\'MODIFY\', \'';
	var editSecondPart = '\')">Edit</A>';
	var deleteFirstPart = '<A href="#" onclick="AddOrModifyAddess(\'DELETE\', \'';
	var deleteSecondPart = '\')">Delete</A>';

	var addressArray = new Array();

	// ADD ACTION.
	if (actn == 'ADD') 
	{    

		if (document.getElementById("addaddress").name == 'SAVE') 
		{
			var mytable=document.getElementById("PartyAddress") ;
			var rowArray = mytable.rows;
			var addressTypeCode = new String("");
			var recID = new String("");


			for (var xx = 1; xx < rowArray.length ; xx++)
			{
				addressTypeCode = rowArray[xx].cells[0].innerHTML;	
				// addressTypeCode = addressTypeCode.toUpperCase();
				recID = document.getElementById("cell0").value; 
				if ( addressTypeCode == recID) 
				{
					for (var yy =0; yy < 9 ; yy++) 
					{
						rowArray[xx].cells[yy].innerHTML = document.getElementById("cell" + yy).value; 
						if (rowArray[xx].cells[yy].innerHTML == '') 
							rowArray[xx].cells[yy].innerHTML = '&nbsp';
					}	
				}			
			} 
			HideAndDisplay('none');
			UpdateServerControl();

		} // SAVE ACTION
		else
		{

			//Check if the Code has been entered or not.		
			if (!ValidateAddress()) return;

			for (xx = 0; xx < 9 ; xx++)
			{
				elmnt = document.getElementById("cell"+ xx);
				addressArray[xx] = elmnt.value;
				elmnt.value = "";			
			}
			addressArray[9] = editFirstPart + addressArray[0] + editSecondPart + ' ' + deleteFirstPart + addressArray[0] + deleteSecondPart ;
			var mytable=document.getElementById("PartyAddress") ;
			var newrow=mytable.insertRow(-1) //add new row to end of table
            
            newrow.className = "gridborder";

			for (var i=0; i<10; i++)
			{ 	
                var newcell = newrow.insertCell(i); //insert new cell to row
                newcell.className = "gridborder";
				if (addressArray[i] == '')
					newcell.innerHTML= '&nbsp';	
				else 
					newcell.innerHTML= addressArray[i];	
			}

			// After adding the row make Add invisible	
			document.getElementById('popUpAddModify').style.display = 'none';
			UpdateServerControl();
		}
	}

	// MODIFY ACTION
	if (actn == 'MODIFY') {

		HideAndDisplay('none');
		var mytable=document.getElementById("PartyAddress") ;
		var rowArray = mytable.rows;
		var addressTypeCode = new String("");
		var recFound = 0;

		for (var xx = 1; xx < rowArray.length ; xx++)
		{
			addressTypeCode = rowArray[xx].cells[0].innerHTML;	
			// addressTypeCode = addressTypeCode.toUpperCase();
			// recordID = recordID.toUpperCase();
			// alert(addressTypeCode);

			if ( addressTypeCode == recordID) {

				// Display the <DIV> tag.
				styl = document.getElementById('popUpAddModify').style;
				styl.display = 'block';

				var elementName = new String();
				for (var yy = 0; yy < 9 ; yy++)
				{
					elementName = "cell" + yy;
					elmnt = document.getElementById(elementName);

					if (rowArray[xx].cells[yy].innerHTML == '&nbsp;')
						elmnt.value = '';
					else
						elmnt.value = rowArray[xx].cells[yy].innerHTML;	
				}

				// Disable the first Text box which is code.
				document.getElementById("cell0").disabled = true;
				document.getElementById("addaddress").name = 'SAVE';
				document.getElementById("addaddress").value = 'Save';
				recFound = 1;
			}			
		} 
		
		if (recFound < 1 ) {
			document.getElementById("AddressWarning").innerHTML = '* Address Type not found, cannot modify';
		}
	} 
	// MODIFY ACTION

	// DELETE ACTION
	if (actn == 'DELETE') {

		var mytable=document.getElementById("PartyAddress") ;
		var rowArray = mytable.rows;
		var addressTypeCode = new String("");

		for (var xx = 1; xx < rowArray.length ; xx++)
		{
			addressTypeCode = rowArray[xx].cells[0].innerHTML;	
			// addressTypeCode = addressTypeCode.toUpperCase();

			if ( addressTypeCode == recordID) {
				mytable.deleteRow(xx);
				elmntDeletedIDs = document.getElementById("hDeletedAddrs") ;
				if (elmntDeletedIDs.value == '') {elmntDeletedIDs.value = ','; }
				elmntDeletedIDs.value = elmntDeletedIDs.value + recordID + ',';
				HideAndDisplay('none');
			}			
		} 
	} 
	// DELETE ACTION


	return 0;
	} // End of the function.	

	
	// Displays or hides the <DIV> tag.
	function HideAndDisplay(optn) {
		ClearControls();
		UpdateServerControl();		
		styl = document.getElementById('popUpAddModify').style;
		if (optn == 'display') {
		    styl.display = 'block';
		} 
		else {
		    styl.display = 'none' ;
		}	
		return 0;
	}

	// Clears all the Text box controls and enable them.
	function ClearControls()
	{
		for ( var xx = 0; xx < 9 ; xx++)
		{
			elmnt = document.getElementById("cell"+ xx);
			elmnt.value = "";
			elmnt.disabled = false;	
		}
		
		for ( var xx = 0; xx < 7 ; xx++)
		{
			elmnt = document.getElementById("IDcell"+ xx);
			elmnt.value = "";
			elmnt.disabled = false;			
		}		
		
		elmnt = document.getElementById('addaddress');
		if ( elmnt != null) 
		{
			elmnt.value = 'Add';
			document.getElementById('addaddress').name = '';
		}

		elmnt = document.getElementById('addid');
		if ( elmnt != null) 
		{
			elmnt.value = 'Add';
			document.getElementById('addid').name = '';
		}
		
		elmnt = document.getElementById('addpartyalias');
		if ( elmnt != null) 
		{
			elmnt.value = 'Add';
			document.getElementById('addpartyalias').name = '';
		}
		
        document.getElementById("DIVIDWARNING").innerHTML = '<H3 id="IDWarning" style="COLOR: red"> </H3>';
        document.getElementById("DIVADDRESSWARNING").innerHTML = '<H3 id="AddressWarning" style="COLOR: red"> </H3>';
        document.getElementById("DIVPARTYALIASWARNING").innerHTML = '<H3 id="PARTYALIASWarning" style="COLOR: red"> </H3>';				
		return 0;
	}

	function ValidateAddress()
	{
		var elmntValue = new String();
		var noWarnings;
		
	
		//Check If Duplicate Codes Exists.
		var mytable=document.getElementById("PartyAddress") ;
		var rowArray = mytable.rows;
		var addressTypeCode = new String("");

		noWarnings = true;
		//Clear the Contents.
		document.getElementById("DIVADDRESSWARNING").innerHTML = '<H3 id="AddressWarning" style="COLOR: red"> </H3>';
		
		if ( rowArray != null) 
		{
			for (var xx = 1; xx < rowArray.length ; xx++)
			{
				addressTypeCode = rowArray[xx].cells[0].innerHTML;	
				if ( addressTypeCode == document.getElementById("cell0").value) 
				{
				//alert('Please select different Partid , same ID type exists');
					document.getElementById("AddressWarning").innerHTML = document.getElementById("AddressWarning").innerHTML + 
							'* Please select different Address Type , Type already exists.<br>';			  
					noWarnings = false;
				}
			}
		}		
		
		elmntValue = document.getElementById("cell0").value;
		if (elmntValue == '' || elmntValue == '&nbsp;' ) {
			//alert("Address Type is mandatory, Please select address type.");
			document.getElementById("AddressWarning").innerHTML = document.getElementById("AddressWarning").innerHTML + 
							'* Address Type is mandatory';
			noWarnings = false;
		}
		 
		elmntValue = document.getElementById("cell2").value;
		if (elmntValue == '' || elmntValue == '&nbsp;') {
			//alert("Stree name is mandatory, Please enter value.");
			document.getElementById("AddressWarning").innerHTML = document.getElementById("AddressWarning").innerHTML + 
							'* Street name is mandatory, Please enter value.<br>';			
			noWarnings = false;
		} 
		
		return noWarnings;
	}

	function CleanUpChars(strInput)
	{
		var str = new String();
		if (strInput == null) strInput = '';
		str = strInput;

		// Replace all the Carriage Return and Line Feeds into spaces.
		while ( str.indexOf('\r',0) > -1)
		{
			str = str.replace("\r"," ");			
		}
		while ( str.indexOf('\n',0) > -1)
		{
			str = str.replace("\n"," ");			
		}
		while ( str.indexOf('\t',0) > -1)
		{
			str = str.replace("\t"," ");			
		}		
		return str;
	}

	function UpdateServerControl()
	{
		var mytable=document.getElementById("PartyAddress") ;
		
		var rowArray = mytable.rows;
		var strCol = new String();
		var strAll = new String();
		
		strAll = '';
		
		for (var xx = 1; xx < rowArray.length ; xx++)
		{
			for (var yy = 0; yy < 9 ; yy++)
			{
				strCol = rowArray[xx].cells[yy].innerHTML;
				if (strCol == '&nbsp;') strCol = '';
				strCol = CleanUpChars(strCol);
				if (yy == 8)
				{
					strAll = strAll + strCol + String.fromCharCode(13);
				}
				else
				{
					strAll = strAll + strCol + '\t';
				}
			}
		}
		elmnt = document.getElementById("hfld1");
		elmnt.value = strAll;
	}
	
	
	// *****************************************************************************
	// *****************************************************************************
	// *****************************************************************************
	// *****************************************************************************
	// ************************ KYC ID DATA Functions **************************
	
	function AddOrModifyID(actn, recordID) {
			
	var editFirstPart = '<A href="#" onclick="AddOrModifyID(\'MODIFY\', \'';
	var editSecondPart = '\')">Edit</A>';
	var deleteFirstPart = '<A href="#" onclick="AddOrModifyID(\'DELETE\', \'';
	var deleteSecondPart = '\')">Delete</A>';

	var addressArray = new Array();
	// ADD ACTION.
	if (actn == 'ADD') 
	{    

		if (document.getElementById("addid").name == 'SAVE') 
		{
			var mytable=document.getElementById("PartyID") ;
			var rowArray = mytable.rows;
			var addressTypeCode = new String("");
			var recID = new String("");
			
			

			for (var xx = 1; xx < rowArray.length ; xx++)
			{
				addressTypeCode = rowArray[xx].cells[0].innerHTML;	
				// addressTypeCode = addressTypeCode.toUpperCase();
				recID = document.getElementById("IDcell0").value; 
				if ( addressTypeCode == recID) 
				{
					for (var yy =0; yy < 7 ; yy++) 
					{
						rowArray[xx].cells[yy].innerHTML = document.getElementById("IDcell" + yy).value; 
						
						if (rowArray[xx].cells[yy].innerHTML == '') 
							rowArray[xx].cells[yy].innerHTML = '&nbsp';						
					}	
				}			
			} 
			HideAndDisplayID('none');
			UpdateServerControlID();
		} // SAVE ACTION
		else
		{

			//Check if the Code has been entered or not.		
			if (!ValidateID()) return;

			for (xx = 0; xx < 7 ; xx++)
			{
				elmnt = document.getElementById("IDcell"+ xx);
				addressArray[xx] = elmnt.value;
				elmnt.value = "";			
			}
			addressArray[7] = editFirstPart + addressArray[0] + editSecondPart + ' ' + deleteFirstPart + addressArray[0] + deleteSecondPart ;
			var mytable=document.getElementById("PartyID") ;
			var newrow=mytable.insertRow(-1) //add new row to end of table
            
            newrow.className = "gridborder";
			for (var i=0; i<8; i++)
			{ 	
                var newcell = newrow.insertCell(i); //insert new cell to row
                newcell.className = "gridborder";
				if (addressArray[i] == '')
					newcell.innerHTML= '&nbsp';	
				else 
					newcell.innerHTML= addressArray[i];					
			}

			// After adding the row make Add invisible	
			document.getElementById('popUpAddModifyID').style.display = 'none';
			UpdateServerControlID();
		}
	}

	// MODIFY ACTION
	if (actn == 'MODIFY') {

		HideAndDisplayID('none');
		var mytable=document.getElementById("PartyID") ;
		var rowArray = mytable.rows;
		var addressTypeCode = new String("");
		var recFound = 0;

		for (var xx = 1; xx < rowArray.length ; xx++)
		{
			addressTypeCode = rowArray[xx].cells[0].innerHTML;	
			// addressTypeCode = addressTypeCode.toUpperCase();
			// recordID = recordID.toUpperCase();
			// alert(addressTypeCode);

			if ( addressTypeCode == recordID) {

				// Display the <DIV> tag.
				styl = document.getElementById('popUpAddModifyID').style;
				styl.display = 'block';

				var elementName = new String();
				for (var yy = 0; yy < 7 ; yy++)
				{
					elementName = "IDcell" + yy;
					elmnt = document.getElementById(elementName);
					if (rowArray[xx].cells[yy].innerHTML == '&nbsp;')
						elmnt.value = '';
					else
						elmnt.value = rowArray[xx].cells[yy].innerHTML;						
				}

				// Disable the first Text box which is code.
				document.getElementById("IDcell0").disabled = true;
				document.getElementById("addid").name = 'SAVE';
				document.getElementById("addid").value = 'Save';
				recFound = 1;
			}			
		} 
		
		if (recFound < 1 ) {
			document.getElementById("IDWarning").innerHTML = '* ID Type not found, cannot modify';
		}
	} 
	// MODIFY ACTION

	// DELETE ACTION
	if (actn == 'DELETE') {

		var mytable=document.getElementById("PartyID") ;
		var rowArray = mytable.rows;
		var addressTypeCode = new String("");

		for (var xx = 1; xx < rowArray.length ; xx++)
		{
			addressTypeCode = rowArray[xx].cells[0].innerHTML;	
			// addressTypeCode = addressTypeCode.toUpperCase();

			if ( addressTypeCode == recordID) {
				mytable.deleteRow(xx);
				elmntDeletedIDs = document.getElementById("hDeletedIDs") ;
				if (elmntDeletedIDs.value == '') {elmntDeletedIDs.value = ','; }
				elmntDeletedIDs.value = elmntDeletedIDs.value + recordID + ',';
				HideAndDisplayID('none');				
			}			
		} 
	} 
	// DELETE ACTION
	return 0;
	} // End of the function.	

	// Displays or hides the <DIV> tag.
	function HideAndDisplayID(optn) {
		ClearControls();
		UpdateServerControlID();		
		styl = document.getElementById('popUpAddModifyID').style;
		if (optn == 'display') {
		    styl.display = 'block';
		} 
		else {
		    styl.display = 'none' ;
		}	
		return 0;
	}
	
	function UpdateServerControlID()
	{
		var mytable=document.getElementById("PartyID") ;
		
		var rowArray = mytable.rows;
		var strCol = new String();
		var strAll = new String();
		
		strAll = '';
		
		for (var xx = 1; xx < rowArray.length ; xx++)
		{
			for (var yy = 0; yy < 7 ; yy++)
			{
				strCol = rowArray[xx].cells[yy].innerHTML;
				strCol = CleanUpChars(strCol);
				if (yy == 6)
				{
					strAll = strAll + strCol + String.fromCharCode(13);
				}
				else
				{
					strAll = strAll + strCol + '\t';
				}
			}
		}
		elmnt = document.getElementById("hFldIDs");
		elmnt.value = strAll;
	}
	
	function ValidateID()
	{
		var elmntValue = new String();
		var noWarnings ;

		//Check If Duplicate Codes Exists.
		var mytable=document.getElementById("PartyID") ;
		var rowArray = mytable.rows;
		var addressTypeCode = new String("");
		//var RegExPattern = /^(?=\d)(?:(?:(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})|(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))|(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2}))($|\ (?=\d)))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\ [AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;

		noWarnings = true;
		//Clear the Contents.
        document.getElementById("DIVIDWARNING").innerHTML = '<H3 id="IDWarning" style="COLOR: red"> </H3>';
		
		for (var xx = 1; xx < rowArray.length ; xx++)
		{
			addressTypeCode = rowArray[xx].cells[0].innerHTML;	
			if ( addressTypeCode == document.getElementById("IDcell0").value) 
			{
			  //alert('Please select different Partid , same ID type exists');
				document.getElementById("IDWarning").innerHTML = document.getElementById("IDWarning").innerHTML + 
						'* Please select different Partid , same ID type exists <br>';			  
			  noWarnings = false;
			}
		}		
		
		elmntValue = document.getElementById("IDcell0").value;
		if (elmntValue == '' || elmntValue == '&nbsp;' ) {
			//alert("ID Type is mandatory, Please select ID type.");
			document.getElementById("IDWarning").innerHTML = document.getElementById("IDWarning").innerHTML + 
				'* ID Type is mandatory, Please select ID type. <br>';
			noWarnings = false;
		}
		 
		elmntValue = document.getElementById("IDcell1").value;
		if (elmntValue == '' || elmntValue == '&nbsp;') {
			//alert("ID number is mandatory, Please enter value.");
			document.getElementById("IDWarning").innerHTML = document.getElementById("IDWarning").innerHTML + 
						'* ID number is mandatory, Please enter value. <br>';
			noWarnings = false;
		} 
				 
		elmntValue = document.getElementById("IDcell2").value;
		if (elmntValue == '' || elmntValue == '&nbsp;') {
			//alert("ID issue agency is mandatory, Please enter value.");
			document.getElementById("IDWarning").innerHTML = document.getElementById("IDWarning").innerHTML + 
						'* ID issue agency is mandatory, Please enter value. <br>';			
			noWarnings = false;
		} 
		
		// Verify the ID Issue date.
		var fld = document.getElementById("IDcell5");
		/*
		if (!((fld.value.match(RegExPattern)) && (fld.value!=''))) {
			document.getElementById("IDWarning").innerHTML = document.getElementById("IDWarning").innerHTML + 
						'* ID Issue date should follow MM/DD/YY format, Invalid date value. <br>';			
					noWarnings = false;		
		
		}
        */
		// Verify the ID Expiry date.		
		fld = document.getElementById("IDcell6");
		/*
		if (!((fld.value.match(RegExPattern)) && (fld.value!=''))) {
			document.getElementById("IDWarning").innerHTML = document.getElementById("IDWarning").innerHTML + 
						'* ID Expiry date should follow MM/DD/YY format, Invalid date value. <br>';			
			noWarnings = false;
					
		}		
		*/
		return noWarnings;
	}
	
	
	//**********************************************************************************************
	// Party Alias Functions
	//**********************************************************************************************
	
	// *****************************************************************************
	// *****************************************************************************
	// *****************************************************************************
	// *****************************************************************************
	// ************************ Party Alias DATA Functions **************************
	
	function AddOrModifyPartyAlias(actn, recordID) {
			
	
	var deleteFirstPart = '<A href="#" onclick="AddOrModifyPartyAlias(\'DELETE\', \'';
	var deleteSecondPart = '\')">Delete</A>';

	var addressArray = new Array();
	// ADD ACTION.
	if (actn == 'ADD') 
	{    

		if (document.getElementById("addpartyalias").name == 'SAVE') 
		{
			var mytable=document.getElementById("PartyAlias") ;
			var rowArray = mytable.rows;
			var addressTypeCode = new String("");
			var recID = new String("");
			
			

			for (var xx = 1; xx < rowArray.length ; xx++)
			{
				addressTypeCode = rowArray[xx].cells[0].innerHTML;	
				recID = document.getElementById("Aliascell0").value; 
				if ( addressTypeCode == recID) 
				{
					for (var yy =0; yy < 1 ; yy++) 
					{
						rowArray[xx].cells[yy].innerHTML = document.getElementById("Aliascell" + yy).value; 
						
						if (rowArray[xx].cells[yy].innerHTML == '') 
							rowArray[xx].cells[yy].innerHTML = '&nbsp';						
					}	
				}			
			} 
			HideAndDisplayPartyAlias('none');
			UpdateServerControlPartyAlias();
		} // SAVE ACTION
		else
		{

			//Check if the Code has been entered or not.		
			if (!ValidatePartyAlias()) return;

			for (xx = 0; xx < 1 ; xx++)
			{
				elmnt = document.getElementById("Aliascell"+ xx);
				addressArray[xx] = elmnt.value;
				elmnt.value = "";			
			}
			addressArray[1] = deleteFirstPart + JSEscapeEncode(addressArray[0]) + deleteSecondPart ;
			var mytable=document.getElementById("PartyAlias") ;
			var newrow=mytable.insertRow(-1) //add new row to end of table
            
            newrow.className = "gridborder";
			for (var i=0; i<2; i++)
			{ 	
                var newcell = newrow.insertCell(i); //insert new cell to row
                newcell.className = "gridborder";
				if (addressArray[i] == '')
					newcell.innerHTML= '&nbsp';	
				else 
					newcell.innerHTML= addressArray[i];					
			}

			// After adding the row make Add invisible	
			document.getElementById('popUpAddModifyPartyAlias').style.display = 'none';
			UpdateServerControlPartyAlias();
		}
	}

	
	// DELETE ACTION
	if (actn == 'DELETE') {

		var mytable=document.getElementById("PartyAlias") ;
		var rowArray = mytable.rows;
		var addressTypeCode = new String("");

		for (var xx = 1; xx < rowArray.length ; xx++)
		{
			addressTypeCode = rowArray[xx].cells[0].innerHTML;	
			// addressTypeCode = addressTypeCode.toUpperCase();

			if ( addressTypeCode == JSEscapeDecode(recordID)) {
				mytable.deleteRow(xx);
				elmntDeletedIDs = document.getElementById("hDeletedPartyAliases") ;
				if (elmntDeletedIDs.value == '') {elmntDeletedIDs.value = '\t'; }
				elmntDeletedIDs.value = elmntDeletedIDs.value + recordID + '\t';
				HideAndDisplayPartyAlias('none');				
			}			
		} 
	} 
	// DELETE ACTION
	return 0;
	} // End of the function.	

	// Displays or hides the <DIV> tag.
	function HideAndDisplayPartyAlias(optn) {
		ClearControls();
		UpdateServerControlPartyAlias();		
		styl = document.getElementById('popUpAddModifyPartyAlias').style;
		if (optn == 'display') {
		    styl.display = 'block';
		} 
		else {
		    styl.display = 'none' ;
		}	
		return 0;
	}
	
	function UpdateServerControlPartyAlias()
	{
		var mytable=document.getElementById("PartyAlias") ;
		
		var rowArray = mytable.rows;
		var strCol = new String();
		var strAll = new String();
		
		strAll = '';
		
		for (var xx = 1; xx < rowArray.length ; xx++)
		{
			for (var yy = 0; yy < 1 ; yy++)
			{
				strCol = rowArray[xx].cells[yy].innerHTML;
				strCol = CleanUpChars(strCol);
				strAll = strAll + strCol + '\t' + String.fromCharCode(13);		
			}
		}
		elmnt = document.getElementById("hFldPartyAliases");
		elmnt.value = strAll;
	}
	
	function ValidatePartyAlias()
	{
		var elmntValue = new String();
		var noWarnings ;

		//Check If Duplicate Codes Exists.
		var mytable=document.getElementById("PartyAlias") ;
		var rowArray = mytable.rows;
		var addressTypeCode = new String("");
		

		noWarnings = true;
		//Clear the Contents.
        document.getElementById("DIVPARTYALIASWARNING").innerHTML = '<H3 id="PARTYALIASWarning" style="COLOR: red"> </H3>';
		
		for (var xx = 1; xx < rowArray.length ; xx++)
		{
			addressTypeCode = rowArray[xx].cells[0].innerHTML;	
			if ( addressTypeCode == document.getElementById("Aliascell0").value) 
			{
			  //alert('Please select different Partid , same ID type exists');
				document.getElementById("PARTYALIASWarning").innerHTML = document.getElementById("PARTYALIASWarning").innerHTML + 
						'* Please select different Party Alias , same Alias exists <br>';			  
			  noWarnings = false;
			}
		}		
		
		elmntValue = document.getElementById("Aliascell0").value;
		if (elmntValue == '' || elmntValue == '&nbsp;' ) {
			//alert("ID Type is mandatory, Please select ID type.");
			document.getElementById("PARTYALIASWarning").innerHTML = document.getElementById("PARTYALIASWarning").innerHTML + 
				'* Alias is mandatory, Please enter value. <br>';
			noWarnings = false;
		}
		 
		return noWarnings;
	}
	
	function JSEscapeDecode(str){
	 var escapedvalue ='';
	    if (!(str.length == 0)) {
	        escapedvalue = str ;
	        escapedvalue = decodeURIComponent(escapedvalue);
	        escapedvalue=escapedvalue.replace(new RegExp("%qt%", "g"),"'");
	        escapedvalue=escapedvalue.replace(new RegExp("&gt;", "g"),">");
	        escapedvalue=escapedvalue.replace(new RegExp("&lt;", "g"),"<");	
	        escapedvalue=escapedvalue.replace(new RegExp("&amp;", "g"),"&");
	        escapedvalue=escapedvalue.replace(new RegExp("&", "g"),"&amp;");
	        escapedvalue=escapedvalue.replace(new RegExp(">", "g"),"&gt;");
	        escapedvalue=escapedvalue.replace(new RegExp("<", "g"),"&lt;");	    
	    }	
	 return escapedvalue;	
	}
	
	function JSEscapeEncode(str){
	 var escapedvalue ='';

	    if (!(str.length == 0)) {
	        escapedvalue = str ;
	        escapedvalue=escapedvalue.replace(new RegExp("'", "g"), "%qt%");
	        escapedvalue = encodeURIComponent(escapedvalue);	      	            
	    }	
	 return escapedvalue;	
	}
	
	function DisplayWarning()
	{
		var elmntValue = new String();		
		elmntValue = document.getElementById("txtSSNOrTIN");
		if(!elmntValue.readOnly)
		{
		    elmntValue = document.getElementById("HdnDSPNPIDATA").value;			  
            if(elmntValue == "1")
            {		   	   
                alert(" To change this value your new value cannot contain any asterisks (*)");				    	
		    }
		    
		}
		return true;
	}
	
	