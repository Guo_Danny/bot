/****** Object:  StoredProcedure [dbo].[USR_OMultiOrg]    Script Date: 10/05/2017 23:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[USR_OMultiOrg](@WLCode SCode, @testAlert INT,
	@period varchar(25),
	@startDate varchar(10),
	@endDate varchar(10),   
   	@minIndCount int,
	@minIndAmt money, 
	@maxIndAmt money,
	@minSumAmount money,
	@maxSumAmount money,
	@riskClassList varchar(8000),
	@customerTypeList varchar(8000),
	@accountTypeList varchar(8000),
	@activityTypeList varchar(8000),
   	@countryList varchar(8000),
	@recvpay INT,
	@branchList varchar(8000),
   	@deptList varchar(8000),
	@cashType smallint,
	@NumByOrder smallint,
	@customerList VARCHAR(8000),
	@CustRule int,
	@UseRelCust Bit,
	@ExcludeRelList varchar(8000)
	)

AS
/* RULE AND PARAMETER DESCRIPTION
Detects one or more Originators sending transactions to a single 
Beneficiary.  A transaction is included in the evaluation based on its 
qualification with: specified transaction types, a specified time period, 
the amount exceeds a certain amount, and the cumulative amount of all the 
transactions meeting these constraints is over a specified amount.  
Additional constraints include Country, Customer Type, Risk Class, Account Types,
Cash/Non-Cash, Branch and Department.  The alert/case will display the account 
information in the description since the account may not be held at the 
processing institution. Designed to be run Post-EOD

	@period = Specifies the previous period accordingly. It Can be left blank,
		Week, Month, Quarter and Semi Annual. If blank the StartDate and EndDate 
  		must be specified. 
	@startDate = Specifies the Starting Date range of transactions to evaluate,  
    	uses the book date. Relevant only if the Period is not specified.
	@endDate = Specifies the Ending Date range of the transactionAs book date.
		Relevant only if the Period is not specified.
    @minIndCount = The Minimum individual transaction count, after applying 
		the Min-Ind-Amt and Max-Ind-Amt parameters
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
    @maxIndAmt = The Maximum amount the individual transaction cannot exceed 
		to be included in the aggregation.  For no maximum use -1
	@minSumAmount = The Minimum aggregated transaction amount that must be 
		exceeded.  This includes those transactions that are between Min-Ind-Amt
		and Max-Ind-Amt.
	@maxSumAmount = The Maximum aggregated transaction amount that cannot be 
		exceeded. For no maximum use -1
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@customerTypeList = A comma separated list of Customer Types to include 
		in the evaluation, use -ALL- for any customer type.
	@accountTypeList = A comma separated list of Account Types to include 
		in the evaluation, use -ALL- for any account type.
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.
    @countryList = A comma separated list of Countries to include in the 
		evaluation, use -ALL- for any country. Fields evaluated are 
		BeneCountry and ByOrderCountry.
	@recvpay = Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both
	@branchList = A comma separated list of Branches to include in the evaluation.
    @deptList = A comma separated list of Departments to include in 
		the evaluation.
	@cashType = Specifies Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both
	@NumByorders = Number of Originators. Alert is generated only if payments are 
		sent by specified number or more Originators.
	@CustomerList = Include or Exclude Customer List (comma separated list)
	@CustRule = include / exclude customer (-1:Disable  1: include  2: exclude)
	@UseRelCust = Include Related Parties (1=yes, 0=no) 
	OCT.12 add @ExcludeRelList = Exclude Relationship Types (comma separated list or '-NONE-')
*/
	
/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@fromDate INT,
 	@toDate INT,
	@tranAmt MONEY,
	@tranCnt INT,
	@trnCnt INT,
	@Cust VarChar(35),
	@OwnerBranch VarChar(11),
	@OwnerDept VarChar(11)
DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME

DECLARE @SetDate DATETIME

DECLARE @TransTbl TABLE (
	TranNo INT,
	Bene VARCHAR(40),
	BeneCustId VARCHAR(40),
	ByOrder VARCHAR(40),
	ByOrderCustId VARCHAR(40),
	BookDate INT,
	BaseAmt MONEY,
	Cust VARCHAR(40),
	OwnerBranch VARCHAR(11),
	OwnerDept VARCHAR(11)
)

DECLARE	@TT TABLE (
	tranAmt MONEY,
	tranCnt INT,
	Bene  VARCHAR(40),
	BeneCustId VARCHAR(40),
	CustCount INT,
	BranchCount INT,
	DeptCount INT,
	Cust VARCHAR(40),
	OwnerBranch VARCHAR(11),
	OwnerDept VARCHAR(11)
)

DECLARE @CustomerExemptionsList VARCHAR(8000)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_OMultiOrg
ELSE
	-- Already in a transaction
	SAVE TRAN USR_OMultiOrg
/* End standard stored procedure transaction header */

/*  standard Rules Header */
--Set  @SetDate = GetDate()

-- Date options
-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @description = [Desc], @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList (NOLOCK)
WHERE WLCode = @WLCode

Declare @BaseCurr char(3)
select @BaseCurr = IsNULL(BaseCurr,'') from SysParam

If(@period is null OR  ltrim(rtrim(@period)) = '') 
BEGIN
	SET @fromDate = dbo.ConvertSqlDateToInt(@startDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@endDate)
END
ELSE
BEGIN
	DECLARE @SQLStartDate datetime, @SQLEndDate datetime
	exec dbo.BSA_GetDateRange @SetDate, @period, 'PREV', 1, 
			@SQLStartDate OUTPUT, @SQLEndDate OUTPUT
	SET @fromDate = dbo.ConvertSqlDateToInt(@SQLStartDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@SQLEndDate)
END

--Oct.12.2021 bot request to add
IF LTRIM(RTRIM(@ExcludeRelList)) = '-NONE-' OR LTRIM(RTRIM(@ExcludeRelList)) = ''
	SELECT @ExcludeRelList = NULL
ELSE
SELECT @ExcludeRelList = ',' + 
	REPLACE(LTRIM(RTRIM(@ExcludeRelList)),' ','')+ ','

SELECT @riskClassList = dbo.BSA_fnListParams(@riskClassList)
SELECT @customerTypeList = dbo.BSA_fnListParams(@customerTypeList)
SELECT @accountTypeList = dbo.BSA_fnListParams(@accountTypeList)
SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)
SELECT @countryList = dbo.BSA_fnListParams(@countryList)
SELECT @branchList = dbo.BSA_fnListParams(@branchList)
SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)
SELECT @customerList = dbo.BSA_fnListParams(@customerList)
SELECT @ExcludeRelList = dbo.BSA_fnListParams(@ExcludeRelList)

SELECT @CustomerExemptionsList = dbo.BSA_GetExemptionCodes('Customer','Rule')
IF ltrim(rtrim(@CustomerExemptionsList)) = ''
	SET @CustomerExemptionsList = NULL

IF (@cashType = 2)
	SET @cashType = NULL

IF (@recvpay = 3)
	SET @recvpay = NULL

Insert Into @TransTbl(TranNo, Bene, ByOrder, BookDate, BaseAmt, Cust, OwnerBranch, OwnerDept, BeneCustId, ByOrderCustId)
Select distinct TranNo, Bene, ByOrder, BookDate, BaseAmt, Cust, a.Branch, Dept, BeneCustId, ByOrderCustId
FROM ActivityHist a (NOLOCK)
Join Customer (NOLOCK) ON 	a.cust = Customer.Id
Left Join Account Ac (NOLOCK) ON a.Account = ac.Id
LEFT JOIN Customer c1 (NOLOCK) ON a.BeneCustId = c1.Id 
LEFT JOIN Customer c2 (NOLOCK) ON a.ByOrderCustId = c2.Id 
WHERE  
	a.bookdate >= @fromDate AND 
	a.bookdate <= @toDate AND 
	a.recvpay = ISNULL(@recvPay, a.recvpay) AND
	a.BaseAmt >= @minIndAmt AND
	(@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
	a.byOrder IS NOT NULL and 
	a.bene IS NOT NULL AND  
	(@deptList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR,  a.dept) + ',',@deptList) > 0) AND
	(@branchList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR,  a.branch) + ',', @branchList) > 0) AND 
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
	(@accountTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, ac.type) + ',',@accountTypeList) > 0) AND
	(@countryList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, a.BeneCountry) + ',',@countryList) > 0 OR
			CHARINDEX(',' + 
				CONVERT(VARCHAR, a.ByOrderCountry) + ',',@countryList) > 0 ) AND
	((@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0)) AND
	--Dec.15.2021 fix customer type statement
	(@customerTypeList IS NULL OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Customer.type,''))) + ',', @customerTypeList) > 0) AND
	(Customer.Exemptionstatus IS NULL OR @CustomerExemptionsList IS NULL OR
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.Exemptionstatus)) + ',', @CustomerExemptionsList ) = 0) AND
	(CASHTRAN = ISNULL(@cashType, CASHTRAN)) AND
	--JUL-2021 add for AML enhance REQUEST ISSUE begin
	((@CustRule = 1 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) > 0)))
		OR 
	(@CustRule = 2 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) = 0)))
		OR @CustRule = -1
	)
	--JUL-2021 add for AML enhance REQUEST ISSUE end

--JUL-30-2021 AML Enhance Related Parties (reverse direction) 
--SEP.16,2022 add distinct select function
Insert Into @TransTbl(TranNo, Bene, ByOrder, BookDate, BaseAmt, Cust, OwnerBranch, OwnerDept, BeneCustId, ByOrderCustId)
Select distinct TranNo, Bene, ByOrder, BookDate, BaseAmt, pr.RelatedParty, a.Branch, Dept, BeneCustId, ByOrderCustId
FROM ActivityHist a (NOLOCK)
JOIN PartyRelation pr WITH (NOLOCK) ON a.Cust = pr.PartyID
JOIN Customer (NOLOCK) ON pr.RelatedParty = Customer.ID
Left Join Account Ac (NOLOCK) ON a.Account = ac.Id
LEFT JOIN Customer c1 (NOLOCK) ON a.BeneCustId = c1.Id 
LEFT JOIN Customer c2 (NOLOCK) ON a.ByOrderCustId = c2.Id 
WHERE  
	a.bookdate >= @fromDate AND 
	a.bookdate <= @toDate AND 
	a.recvpay = ISNULL(@recvPay, a.recvpay) AND
	a.BaseAmt >= @minIndAmt AND
	(@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
	a.byOrder IS NOT NULL and 
	a.bene IS NOT NULL AND  
	(@deptList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR,  a.dept) + ',',@deptList) > 0) AND
	(@branchList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR,  a.branch) + ',', @branchList) > 0) AND 
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
	(@accountTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, ac.type) + ',',@accountTypeList) > 0) AND
	(@countryList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, a.BeneCountry) + ',',@countryList) > 0 OR
			CHARINDEX(',' + 
				CONVERT(VARCHAR, a.ByOrderCountry) + ',',@countryList) > 0 ) AND
	((@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0)) AND
	--Dec.15.2021 fix customer type statement
	(@customerTypeList IS NULL OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Customer.type,''))) + ',', @customerTypeList) > 0) AND
	(Customer.Exemptionstatus IS NULL OR @CustomerExemptionsList IS NULL OR
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.Exemptionstatus)) + ',', @CustomerExemptionsList ) = 0) AND
	(CASHTRAN = ISNULL(@cashType, CASHTRAN)) AND
	--JUL-2021 add for AML enhance REQUEST ISSUE begin
	((@CustRule = 1 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) > 0)))
		OR 
	(@CustRule = 2 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) = 0)))
		OR @CustRule = -1
	)
	--Oct.12.2021 bot request to add
	AND ((ISNULL(@ExcludeRelList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND @UseRelCust = 1 AND a.Cust <> pr.RelatedParty AND pr.Deleted <> 1 
	AND Exists (select * from @TransTbl tt Where tt.Cust = pr.RelatedParty)
	AND Not Exists (select * from @TransTbl tt Where tt.Cust = pr.RelatedParty AND tt.TranNo = a.tranno)
	--JUL-2021 add for AML enhance REQUEST ISSUE end
	

INSERT INTO @tt (tranAmt, tranCnt, CustCount, BranchCount, DeptCount, a.BeneCustId)
SELECT DISTINCT SUM(BaseAmt) tranAmt, COUNT(tranNo) tranCnt, 
       Count(Distinct Cust), Count(Distinct OwnerBranch), Count(Distinct OwnerDept), a.Cust
FROM @TransTbl a 
GROUP BY a.Cust
HAVING (SUM(BaseAmt) >= @minSumAmount AND 
	(@maxSumAmount = -1 OR SUM(BaseAmt) <= @maxSumAmount) AND
	count(tranno) >= @minIndCount) AND 
	COUNT(DISTINCT a.ByOrderCustId) >= @NumByOrder

Update tt Set Cust = trn.Cust
From @tt tt Join @TransTbl trn 
On tt.BeneCustId = trn.BeneCustId
Where CustCount = 1

Update tt Set OwnerBranch = trn.OwnerBranch
From @tt tt Join @TransTbl trn 
On tt.BeneCustId = trn.BeneCustId
Where CustCount > 1 AND BranchCount = 1

Update tt Set OwnerDept = trn.OwnerDept
From @tt tt Join @TransTbl trn 
On tt.BeneCustId = trn.BeneCustId
Where CustCount > 1 AND DeptCount = 1



DECLARE	@cur CURSOR
DECLARE @Bene LONGNAME,
	@bookdate INT,
	@BeneCustId varchar(40)

--2017.10 mdy bookdate to firstdate of month by LN Request
SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())
--SET @bookdate = dbo.ConvertSqlDateToInt(@SetDate)

SET @cur = CURSOR FAST_FORWARD FOR 
SELECT a.tranAmt, a.tranCnt, a.Cust, a.OwnerBranch, a.OwnerDept, a.BeneCustId
FROM @tt a 

OPEN @cur 
FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @cust, @OwnerBranch, @Ownerdept, @BeneCustId

WHILE @@FETCH_STATUS = 0 BEGIN

	SET @desc = (select name from customer where id = @BeneCustId) + '(Bene) is '
		+ Case @recvPay
			WHEN 1 THEN 'Receiving from '
			WHEN 2 THEN 'Paying To '
			ELSE 'Receiving From/Paying To ' 
		  END
		+ 'multiple ByOrders ' 
		+ 'amount: ' + DBO.BSA_InternalizationMoneyToString(@tranAmt) + Space(1)+ @BaseCurr + ' over '
		+ CONVERT(VARCHAR, @tranCnt) + ' transactions'
		+ ' over a period from ' + Cast(dbo.BSA_InternalizationIntToShortDate(@fromDate) AS Char(11)) + ' to ' 
		+ Cast(dbo.BSA_InternalizationIntToShortDate(@toDate) AS Char(11))
		--SEP.16,2022 add related party count.
		+ convert(varchar(200),(select case when count(TranNo) > 0 then ' and count ' + convert(varchar(200),count(distinct cust)) + ' related party included.' else '' end from ActivityHist ach where ach.TranNo in (select TranNo from @TransTbl T1 where T1.Cust = @cust) and ach.Cust != @cust) ) --modify SEP.16,2022
	IF @testAlert = 1
	BEGIN
		EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
			@Cust, NULL, 1
		IF @stat <> 0 GOTO EndOfProc
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 
		BEGIN
			EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
				@Cust, NULL, 0
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE 
		IF @WLTYPE = 1 
		BEGIN
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
				ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
				RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
				ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
				EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
				OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
				REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
				WLCode, WLDESC, CREATETIME, OwnerBranch, OwnerDept )
				SELECT  NULL, @bookdate, @Cust, NULL,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, @desc, GETDATE(), @OwnerBranch, @OwnerDept

			set @ID = @@identity

		END	
	END		
	IF (@WLTYPE = 0) OR (@testAlert = 1)
	BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   SELECT Distinct 'Alert', @ID, TRANNO 
			FROM @TransTbl A
			Join @tt T ON 	a.cust = T.BeneCustId
			where (A.BeneCustId = @BeneCustId or A.Bene = @Bene or A.Cust = @BeneCustId)
			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC
	END 
	ELSE 
	IF @WLTYPE = 1 
	BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT Distinct 'SUSPACT', @ID, TRANNO 
				FROM @TransTbl A
				Join @tt T ON 	a.cust = T.BeneCustId
				where (A.BeneCustId = @BeneCustId or A.Bene = @Bene or A.Cust = @BeneCustId)
		SELECT @STAT = @@ERROR 
		IF @STAT <> 0 GOTO ENDOFPROC	
	END
	FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @cust, @OwnerBranch, @Ownerdept, @BeneCustId
END

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_OMultiOrg
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_OMultiOrg
RETURN @stat
GO
