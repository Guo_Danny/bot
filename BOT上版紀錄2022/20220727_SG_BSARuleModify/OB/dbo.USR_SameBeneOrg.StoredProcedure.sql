USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_SameBeneOrg]    Script Date: 7/27/2022 4:41:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







ALTER Procedure [dbo].[USR_SameBeneOrg](@WLCode SCode, @testAlert INT,
	@minIndAmt money, 
	@maxIndAmt money, 
	@minCount int,
	@activityTypeList varchar(8000),
	@riskClassList varchar(8000),
	@customerTypeList varchar(8000),
	@minSumAmount money,
	@customerList VARCHAR(8000),
	@CustRule int,
	@UseRelCust Bit,
	@ExcludeRelList varchar(8000),
	@ExcludeNoncust Bit
	)
AS
/* RULE AND PARAMETER DESCRIPTION
Description:Detects transactions of specified activity type, when the name in 
the beneficiary data field is the same as the name in the Byorder data field, 
for amounts between the specified minimum amount and maximum amount. 
Additional constraints include risk class and customer type. Alerts are 
grouped based on ByOrder. Designed to be run Pre-EOD.

	Parameters:		
	@WLCode [in] SCode = Rule Code				
	@testAlert = Test Alert or Not (1 for Test, 0 for No)
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
   	@maxIndAmt = The Maximum amount the individual transaction cannot exceed 
		to be included in the aggregation.  For no maximum use -1	
	@minCount = The Minimum transaction count	
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.    	
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@customerTypeList = A comma separated list of Customer Types to include 
		in the evaluation, use -ALL- for any customer type.	
	@minSumAmount = The Minimum aggregated transaction amount that must be 
		exceeded.  This includes those transactions that are between Min-Ind-Amt
		and Max-Ind-Amt.
	@CustomerList = Include or Exclude Customer List (comma separated list)
	@CustRule = include / exclude customer (-1:Disable  1: include  2: exclude)
	@UseRelCust = Include Related Parties (1=yes, 0=no) 
	OCT.12 add @ExcludeRelList = Exclude Relationship Types (comma separated list or '-NONE-')
	JUL.27 add @ExcludeNoncust = Exclude NonCust (1:Yes, 0:No) 
*/
	
/*  Declarations */
DECLARE	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@tranAmt MONEY,
	@activityCount INT,
	@currentTransactionCount INT,
	@SetDate datetime

DECLARE	@TT TABLE (
	tranAmt MONEY,
	tranNo INT,
	byOrder  VARCHAR(40),
	byorderid VARCHAR(40)
)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE *********************************
/* Start standard stored procedure transaction header */
SET @currentTransactionCount = @@TRANCOUNT	-- Save the current trancount
IF @currentTransactionCount = 0
	-- Transaction has not begun
	BEGIN TRAN USR_SameBeneOrg
ELSE
	-- Already in a transaction
	SAVE TRAN USR_SameBeneOrg
/* End standard stored procedure transaction header */

/* mark 2018.10
SELECT @WLType = WLType      
	FROM dbo.WatchList (NOLOCK)
	WHERE WLCode = @WLCode
*/
SELECT @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList (NOLOCK)
WHERE WLCode = @WLCode


/* modify by danny 2018.10 get fromdate */
DECLARE @SQLStartDate datetime, @SQLEndDate datetime, @endDate int, @fromDate int
exec dbo.BSA_GetDateRange @SetDate, 'Month', 'PREV', 1, 
	@SQLStartDate OUTPUT, @SQLEndDate OUTPUT
SET @fromDate = dbo.ConvertSqlDateToInt(@SQLStartDate)
SET @endDate  = dbo.ConvertSqlDateToInt(@SQLEndDate)


--Making the List variables Null, if value is '' or -ALL-
--Else Removing space and Trimming
IF (ISNULL(@riskClassList,'') = '' 
	OR UPPER(ISNULL(@riskClassList,'-ALL-')) = '-ALL-')
	SELECT @riskClassList = NULL
ELSE
	SELECT @riskClassList = ',' + 
		REPLACE(LTRIM(RTRIM(@riskClassList)),CHAR(32),'') + ','

IF(ISNULL(@customerTypeList,'') = '' 
	OR UPPER(ISNULL(@customerTypeList,'-ALL-')) = '-ALL-')
	SELECT @customerTypeList = NULL
ELSE
   	SELECT @customerTypeList = ',' + 
		REPLACE(LTRIM(RTRIM(@customerTypeList)),CHAR(32),'') + ','

IF (ISNULL(@activityTypeList,'') = '' 
	OR UPPER(ISNULL(@activityTypeList,'-ALL-')) = '-ALL-')
	SELECT @activityTypeList = NULL
ELSE
	SELECT @activityTypeList = ',' + 
		REPLACE(LTRIM(RTRIM(@activityTypeList)),CHAR(32),'') + ','
	SELECT @customerList = dbo.BSA_fnListParams(@customerList)
	
--Oct.12.2021 bot request to add
IF LTRIM(RTRIM(@ExcludeRelList)) = '-NONE-' OR LTRIM(RTRIM(@ExcludeRelList)) = ''
	SELECT @ExcludeRelList = NULL
ELSE
SELECT @ExcludeRelList = ',' + 
	REPLACE(LTRIM(RTRIM(@ExcludeRelList)),' ','')+ ','

SELECT @ExcludeRelList = dbo.BSA_fnListParams(@ExcludeRelList)

--JUL.27.2022 SG add exclude noncust
if isnull(@ExcludeNoncust,'') = ''
	select @ExcludeNoncust = 0

INSERT INTO @TT (tranAmt, tranNo, byOrder,byorderid)
SELECT distinct a.BaseAmt, a.tranNo, a.byOrder, a.ByOrderCustId
FROM Activity a (NOLOCK), Customer (NOLOCK)
WHERE a.cust = Customer.Id AND
    a.bookdate >= @fromDate AND
	a.bookdate <= @endDate AND
	a.BaseAmt >= @minIndAmt AND
    (@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
	a.byOrder IS NOT NULL and 
	a.bene IS NOT NULL AND  
	--Customer.Id not like 'noncust%' AND
	--a.byOrder = a.bene AND
	(a.byOrder = a.bene or a.byOrderCustId = a.beneCustId) AND
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
	(@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0) AND
	(@customerTypeList IS NULL OR
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.type)) + ',', @customerTypeList ) > 0)
	AND 
	--JUL-2021 add for AML enhance REQUEST ISSUE begin
	((@CustRule = 1 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) > 0)))
		OR 
	(@CustRule = 2 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) = 0)))
		OR @CustRule = -1
	)
	--JUL.27.2022 add exclude noncust
	and ((@ExcludeNoncust = 1 and isnull(a.ByOrderCustId,'') not like 'noncust%') or @ExcludeNoncust = 0)
union
SELECT distinct ah.BaseAmt, ah.tranNo, ah.byOrder, ah.ByOrderCustId
FROM ActivityHist ah (NOLOCK), Customer (NOLOCK)
WHERE ah.cust = Customer.Id AND
    ah.bookdate >= @fromDate AND
	ah.bookdate <= @endDate AND
	ah.BaseAmt >= @minIndAmt AND
    (@maxIndAmt = -1 OR ah.BaseAmt <= @maxIndAmt) AND
	ah.byOrder IS NOT NULL and 
	ah.bene IS NOT NULL AND  
	--Customer.Id not like 'noncust%' AND
	--a.byOrder = a.bene AND
	(ah.byOrder = ah.bene or ah.byOrderCustId = ah.beneCustId) AND
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, ah.type) + ',',@activityTypeList) > 0) AND
	(@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0) AND
	(@customerTypeList IS NULL OR
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.type)) + ',', @customerTypeList ) > 0)
	AND 
	--JUL-2021 add for AML enhance REQUEST ISSUE begin
	((@CustRule = 1 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) > 0)))
		OR 
	(@CustRule = 2 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) = 0)))
		OR @CustRule = -1
	)
	--JUL.27.2022 add exclude noncust
	and ((@ExcludeNoncust = 1 and isnull(ah.ByOrderCustId,'') not like 'noncust%') or @ExcludeNoncust = 0)

--JUL-2021 add for AML enhance REQUEST ISSUE begin
INSERT INTO @TT (tranAmt, tranNo, byOrder,byorderid)
SELECT distinct a.BaseAmt, a.tranNo, customer.name, pr.RelatedParty
FROM Activity a (NOLOCK)
JOIN PartyRelation pr WITH (NOLOCK) ON a.Cust = pr.PartyID
JOIN Customer (NOLOCK) ON pr.RelatedParty = Customer.ID
WHERE a.bookdate >= @fromDate AND
	a.bookdate <= @endDate AND
	a.BaseAmt >= @minIndAmt AND
    (@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
	a.byOrder IS NOT NULL and 
	a.bene IS NOT NULL AND  
	--Customer.Id not like 'noncust%' AND
	--a.byOrder = a.bene AND
	(a.byOrder = a.bene or a.byOrderCustId = a.beneCustId) AND
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
	(@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0) AND
	(@customerTypeList IS NULL OR
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.type)) + ',', @customerTypeList ) > 0)
	AND 
	--JUL-2021 add for AML enhance REQUEST ISSUE begin
	((@CustRule = 1 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) > 0)))
		OR 
	(@CustRule = 2 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) = 0)))
		OR @CustRule = -1
	)
	--JUL.27.2022 add exclude noncust
	and ((@ExcludeNoncust = 1 and isnull(a.ByOrderCustId,'') not like 'noncust%') or @ExcludeNoncust = 0)
	--Oct.12.2021 bot request to add
	AND ((ISNULL(@ExcludeRelList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND @UseRelCust = 1 AND a.Cust <> pr.RelatedParty AND pr.Deleted <> 1 
	AND Exists (select * from @TT tt Where tt.byorderid = pr.RelatedParty)
	AND Not Exists (select * from @TT tt Where tt.TranNo = a.tranno and tt.byorderid = pr.RelatedParty)
union
SELECT distinct ah.BaseAmt, ah.tranNo, customer.name, pr.RelatedParty
FROM ActivityHist ah (NOLOCK)
JOIN PartyRelation pr WITH (NOLOCK) ON ah.Cust = pr.PartyID
JOIN Customer (NOLOCK) ON pr.RelatedParty = Customer.ID
WHERE ah.bookdate >= @fromDate AND
	ah.bookdate <= @endDate AND
	ah.BaseAmt >= @minIndAmt AND
    (@maxIndAmt = -1 OR ah.BaseAmt <= @maxIndAmt) AND
	ah.byOrder IS NOT NULL and 
	ah.bene IS NOT NULL AND  
	--Customer.Id not like 'noncust%' AND
	--a.byOrder = a.bene AND
	(ah.byOrder = ah.bene or ah.byOrderCustId = ah.beneCustId) AND
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, ah.type) + ',',@activityTypeList) > 0) AND
	(@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0) AND
	(@customerTypeList IS NULL OR
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.type)) + ',', @customerTypeList ) > 0)
	AND 
	--JUL-2021 add for AML enhance REQUEST ISSUE begin
	((@CustRule = 1 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) > 0)))
		OR 
	(@CustRule = 2 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) = 0)))
		OR @CustRule = -1
	)
		--JUL.27.2022 add exclude noncust
	and ((@ExcludeNoncust = 1 and isnull(ah.ByOrderCustId,'') not like 'noncust%') or @ExcludeNoncust = 0)
	--Oct.12.2021 bot request to add
	AND ((ISNULL(@ExcludeRelList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
	AND @UseRelCust = 1 AND ah.Cust <> pr.RelatedParty AND pr.Deleted <> 1 
	AND Exists (select * from @TT tt Where tt.byorderid = pr.RelatedParty)
	AND Not Exists (select * from @TT tt Where tt.TranNo = ah.tranno and tt.byorderid = pr.RelatedParty)
--JUL-2021 add for AML enhance REQUEST ISSUE end

DECLARE	@cur CURSOR
DECLARE @byOrder LONGNAME,
	@bookdate INT

SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())

SET @cur = CURSOR FAST_FORWARD FOR 
SELECT SUM(tranAmt) tranAmt, COUNT(tranNo) tranCnt, a.byorderid
FROM @TT a GROUP BY  a.byorderid
HAVING count(tranno) >= @minCount and SUM(tranAmt) >= @minSumAmount

OPEN @cur 
FETCH NEXT FROM @cur INTO @tranAmt, @activityCount , @byOrder

WHILE @@FETCH_STATUS = 0 BEGIN

	SET @desc = @byOrder + '(ByOrder) is performing '
		+ CONVERT(VARCHAR, @activityCount ) + ' transaction(s) '  
		+ 'to self for a total amount of ' + CONVERT(VARCHAR, @tranAmt)
		+ convert(varchar(200),(select case when count(TranNo) > 0 then ' and count ' + convert(varchar(200),count(distinct cust)) + ' related party included.' else '' end from ActivityHist ach where ach.TranNo in (select TranNo from @TT T1 where T1.byorderid = @byOrder) and ach.Cust != @byOrder ) ) --modify SEP.15,2022
	IF @testAlert = 1
	BEGIN
		EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
		@byOrder, NULL, 1
		IF @stat <> 0 GOTO EndOfProc
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 --Alert
		BEGIN
			EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
				@byOrder, NULL, 0
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE 
		IF @WLTYPE = 1 --Case
		BEGIN
			EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT, 
				@WLCode, @desc, @bookdate, @byOrder, NULL
			IF @stat <> 0 GOTO EndOfProc   	
		END	
	END		
	IF (@WLTYPE = 0) OR (@testAlert = 1)
	BEGIN
		--Inserting records into SASACTIVITY for the @ID
		--obtained from API_InsAlert stored Procedure
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'Alert', @ID, TRANNO 
			FROM @TT t
			WHERE  t.byorderid = @byOrder
	
			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC
	END 
	ELSE 
	IF @WLTYPE = 1 --Case
	BEGIN
		--Inserting records into SASACTIVITY for the @ID
		--obtained from API_InsSuspiciosActivity stored Procedure
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'SUSPACT', @ID, TRANNO 
				FROM @TT t
			WHERE  t.byorderid = @byOrder

			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC 
	END
	FETCH NEXT FROM @cur INTO @tranAmt, @activityCount , @byOrder
END --Cursor While loop Ends

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_SameBeneOrg
  RETURN @stat
END	

IF @currentTransactionCount = 0
  COMMIT TRAN USR_SameBeneOrg
RETURN @stat
GO


