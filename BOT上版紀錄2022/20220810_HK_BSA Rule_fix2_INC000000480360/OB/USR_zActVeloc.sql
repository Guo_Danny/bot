USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_zActVeloc]    Script Date: 8/10/2022 9:20:04 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER Procedure [dbo].[USR_zActVeloc] (@WLCode SCode, @testAlert INT,
--DECLARE @WLCode SCode, @testAlert INT,
	@inActivityType varchar(8000),
	@outActivityType varchar(8000),
	@UseRecvPay smallint,
    @MonitorType smallint,     --add new
	@days  int,
	@Minpercent  int,
	@Maxpercent  int,
	@RiskClassList Varchar (8000),
	@CustTypeList Varchar (8000),
	@MinInAggAmt 	MONEY,
    @MinOutAggAmt 	MONEY,
  	@AmtCompareOper 	SMALLINT,
  	@MinInAmt 	MONEY,
  	@MinOutAmt 	MONEY,
  	@UseInOutCount 	INT,
  	@InTranCnt 	INT,
  	@OutTranCnt 	INT,
  	--@InOutCntRatio 	INT
	@DormOrNewOpenPeriod	INT,
	@AvgTranCntDays INT,
	@StDevRatio     FLOAT,
	@UseRound    INT,
	@Precision   INT

)
AS

/*
    @inActivityType = List of Incoming Activity Types separated by comma. -ALL- for All. 
	@outActivityType = List of Outgoing Activity Types separated by comma. -ALL- for All. 
	@UseRecvPay = Use Receive-Pay indicator ( 0 - Do not use, 1-Receive, 2-Pay) 
	@MonitorType = Dorm(1)/ New Open Account(2)/ ALL(0) .
	@Days = Last No Of Days 
	--@UsePercent = 0 -  Minimum Aggregate transaction Amount 
	--              1 -  Minimum Individual transaction Amount
	@MinPercent = Min Percentage difference in Deposits and Withdrawals 
	@MaxPercent = Max Percentage difference in Deposits and Withdrawals 
	@RiskClassList = List of Risk Classes separated by comma. -ALL- for All. 
	@CustTypeList = List of Customer Types separated by comma. -ALL- for All. 
	@MinInAggAmt = Minimum Aggregate Incoming Amount 
	@MinOutAggAmt = Minimum Aggregate Outgoing Amount
	@AmtCompareOper = Minimum Amount Comparison (1 - Incoming OR Outgoing, 2 - Incoming AND Outgoing)
	@MinInAmt = Minimum Individual Incoming transaction Amount 
	@MinOutAmt = Minimum Individual Outgoing transaction Amount 
	@UseInOutCount = Use Volume Ratio (1 - Incoming/Outgoing, 2 - Outgoing/Incoming, 3 - None) 
	@InTranCnt = Minimum Incoming Volume 
	@OutTranCnt = Minimum Outgoing Volume 
	@AvgTranCntDays = Days for AvgTranCount 
	@StDevRatio = Standard deviation Ratio
 	@UseRound = Indicate if only the high round amounts have to considered.
					0 - Do not round
					1 - Round
	@Precision = Precision for rounding. Specify 3 for 1000
*/

/*
SELECT @WLCode='zActVeloc', @testAlert=1,
	@inActivityType='-ALL-', 	@outActivityType='-ALL-',
	@UseRecvPay=0,	@MonitorType=0,	@Days=730, --@UsePercent=0,
	@MinPercent=75,	@MaxPercent=125,@RiskClassList ='-ALL-',	@CustTypeList='-ALL-',
	@MinInAggAmt=7000000,    @MinOutAggAmt=7000000,  	@AmtCompareOper=1,
  	@MinInAmt=6600,  	@MinOutAmt=6600,  	@UseInOutCount=3,  	@InTranCnt =1,  	@OutTranCnt=1,  	
	@AvgTranCntDays =30, @StDevRatio =1,	@UseRound =0,	@Precision=3

*/
	/*  Declarations */
	DECLARE	@description 	VARCHAR(2000),
		@desc 		VARCHAR(2000),
		@Id 		INT, 
		@WLType 	INT,
		@stat 		INT,
		@trnCnt 	INT,
		@MINDATE 	INT

	DECLARE @cust 		LONGNAME
	
	DECLARE	@TT TABLE (
		Cust VARCHAR(40),
		Recv MONEY,
		Pay MONEY,
		Mindate INT,
		Maxdate INT,
		PreLastBookDate INT,   --add new
		Descr VARCHAR(2000)
	)

	-- Temporary table of Activity Types that have not been specified as Exempt
	DECLARE @ActInType TABLE (
		Type	INT
	)

	DECLARE @ActOutType TABLE (
		Type	INT
)

--20210611 add for @AvgTranCntDays>0 , start
DECLARE	@TT1 TABLE (
	Cust VARCHAR(40),
	TotCount INT,
	AvgCount INT,
	StdevpCount Float
)

DECLARE	@TT2 TABLE (
	Cust VARCHAR(40),
	s_date int,
	e_date int,
	cur_date int,
	TrnCnt int
)
--20210611 add for @AvgTranCntDays>0 , end

--20210621 add for @AvgTranCntDays=0, Start
DECLARE @TT3 TABLE (
	Cust VARCHAR(40),
	TotCnt INT,
	TotAmt MONEY,
	AvgAmt MONEY,
	StdevpAmt MONEY
)

DECLARE @TT4 TABLE(
    Cust VARCHAR(40),
	TotCnt INT,
	TotAmt MONEY,
	AvgAmt MONEY,
	StdevpAmt MONEY,
	TotCount INT,
	AvgCount INT,
	StdevpCount Float	
)
--20210621 add for @AvgTranCntDays=0, End

--20210611 add for Monitortype , Start
DECLARE @mAct TABLE (
	Account VARCHAR(40),
	BookDate INT
)
--20210611 add for Monitortype , End


	DECLARE @STARTALRTDATE  DATETIME
	DECLARE @ENDALRTDATE    DATETIME
	
--202106 add , Start
    DECLARE @tmp INT, @PayAmt MONEY, @RecvAmt MONEY
	DECLARE @PayType INT, @RecvType INT
	DECLARE @RecentDate DATETIME    -- to calculate the last No. TransOfDays
	DECLARE @CurDate    DATETIME    --to calculate the current date
    DECLARE @LastDate   DATETIME    -- the begining date

    set @tmp = power(10, @precision)	
--202106 add , Start	

	SET NOCOUNT ON
	SET @stat = 0
	
	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE, @StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode

	SET @Days = ABS(@Days)
	
	IF ( @UseRecvPay < 0 or @UseRecvPay > 2)
		SET @UseRecvPay = 0

    IF ( @UseInOutCount < 0 OR @UseInOutCount > 3)
            SET @UseInOutCount = 3
	-- Call BSA_fnListParams for each of the Paramters that support comma separated values

	SELECT @inActivityType = dbo.BSA_fnListParams(@inActivityType)
	SELECT @outActivityType = dbo.BSA_fnListParams(@outActivityType)
	SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)

	--- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */
	SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN USR_zActVeloc
	ELSE
		-- Already in a transaction
		SAVE TRAN USR_zActVeloc
	/* End standard stored procedure transaction header */
	
	--When the transactions are picked up
	SET @minDate = dbo.ConvertSqlDateToInt(	DATEADD(d, -1 * @days, CONVERT(VARCHAR, @StartDate)))

	INSERT INTO @ActInType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	((@inActivityType IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@inActivityType) > 0) )

	INSERT INTO @ActOutType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	((@outActivityType IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@OutActivityType) > 0))

--20210621 add for Monitor Type, Start
	IF ( @MonitorType=1 )
	BEGIN
		INSERT INTO @mAct (Account, BookDate)
		SELECT Account, MAX(BookDate) 
		FROM ActivityHist A(NOLOCK)
		WHERE A.Account IS NOT NULL 
		AND CONVERT(DATE,CONVERT(VARCHAR,A.BookDate),111) < CONVERT(DATE,CONVERT(VARCHAR,@MINDATE),111)
		AND ( ((@UseRecvpay = 0 OR (@UseRecvpay = 1 AND recvpay = 1)) 
			AND (  BaseAmt >= @MinInAmt )) OR
			((@UseRecvpay = 0 OR (@UseRecvpay = 2 AND recvpay = 2)) 
			AND (  BaseAmt >= @MinOutAmt )))
		GROUP BY A.Account 
		--HAVING CONVERT(DATE,CONVERT(VARCHAR,MAX(A.BookDate) ),111)< CONVERT(DATE,CONVERT(VARCHAR,@MINDATE),111) 
	
		--SELECT * FROM @mAct
	END

	ELSE IF ( @MonitorType=2 )
	BEGIN
          INSERT INTO @mAct (Account, BookDate)
		  SELECT  Id, dbo.ConvertSqlDateToInt(OpenDate) FROM Account (NOLOCK)
		  WHERE dbo.ConvertSqlDateToInt(OpenDate) >= dbo.ConvertSqlDateToInt(DATEADD(DAY, -1 * @DormOrNewOpenPeriod, CONVERT(DATE,CONVERT(VARCHAR,@StartDate),111)))
		   --AND dbo.ConvertSqlDateToInt(OpenDate) <= dbo.ConvertSqlDateToInt(@StartDate)
		   AND Closed=0 
		   ORDER BY Id
		  --SELECT * FROM @mAct 
		  --SELECT * FROM ActivityHist Ach(NOLOCK) WHERE Account IN (SELECT ACCOUNT FROM @mAct)
	END

	ELSE IF ( @MonitorType=0 )
	BEGIN
	    INSERT INTO @mAct (Account, BookDate)
		  SELECT Account, MIN(BookDate) FROM ActivityHist A(NOLOCK) 
		  WHERE  CONVERT(Date, CONVERT(VARCHAR,BookDate),111)
			    BETWEEN CONVERT(Date, CONVERT(VARCHAR,@MINDATE),111)  
				AND CONVERT(Date,@startDate,111)
				AND ( ((@UseRecvpay = 0 OR (@UseRecvpay = 1 AND recvpay = 1)) AND EXISTS (SELECT Type From @ActInType Act WHERE Act.Type = A.Type)
					AND (  BaseAmt >= @MinInAmt )) OR
					((@UseRecvpay = 0 OR (@UseRecvpay = 2 AND recvpay = 2)) AND EXISTS (SELECT Type From @ActOutType Act WHERE Act.Type = A.Type)
					AND (  BaseAmt >= @MinOutAmt )))	  
			Group BY Account
		  --SELECT * FROM @mAct
	END
--20210621 add for Monitor Type, End

--20210621 add for @AvgTranCntDays>0, Start
	IF ( @AvgTranCntDays > 0 )
	BEGIN
		set @RecentDate= DATEADD(D, -1 * @AvgTranCntDays, @StartDate)
		set @LastDate = DATEADD(D, -1 * @Days, CONVERT(VARCHAR, @StartDate));
		set @CurDate= Dateadd(D, @AvgTranCntDays, @LastDate )
		--select @RecentDate as RecentDate, @LastDate as LastDate, @CurDate as CurDate, @StartDate as StartDate,Dateadd(DAY, @AvgTranCntDays, @curdate )

		while (Dateadd(D, @AvgTranCntDays, @curdate ) <= @StartDate)
		begin
			insert into @TT2 (Cust, s_date, e_date, cur_date, trncnt)
			select Cust, convert(varchar(8), @lastdate, 112),
			convert(varchar(8), @curdate, 112),
			BookDate, count(*)  
			from ActivityHist A(nolock)
			where CONVERT(Date, CONVERT(VARCHAR,BookDate),111) >= CONVERT(Date, CONVERT(VARCHAR,@lastdate),111)  
				AND CONVERT(Date, CONVERT(VARCHAR,BookDate),111) < CONVERT(Date,@curdate,111)
			AND ( ((@UseRecvpay = 0 OR (@UseRecvpay = 1 AND recvpay = 1)) AND EXISTS (SELECT Type From @ActInType Act WHERE Act.Type = A.Type)
				AND (  BaseAmt >= @MinInAmt )) OR
				((@UseRecvpay = 0 OR (@UseRecvpay = 2 AND recvpay = 2)) AND EXISTS (SELECT Type From @ActOutType Act WHERE Act.Type = A.Type)
				AND (  BaseAmt >= @MinOutAmt )))			
			group by Cust,BookDate

			set @lastdate = @curdate;
			set @curdate = DateAdd(D,@AvgTranCntDays, @curdate );

			--select @curdate currdate, @LastDate lastdate
		end

		insert into @TT1(Cust, TotCount, AvgCount, StdevpCount)
		select Cust,SUM(TotCnt), AVG(TotCnt), STDEVP(TotCnt)	from (
		select Cust, s_date, e_date, SUM(TrnCnt) TotCnt  from @TT2
		group by Cust, s_date, e_date
		) A 
		group by cust
		order by Cust

		--insert into @TT3
		insert into @TT3 (Cust, TotCnt, TotAmt, AvgAmt, StdevpAmt)
		select Cust, COUNT(TranNo), SUM(BaseAmt), AVG(BaseAmt), STDEVP(BaseAmt)
		from ActivityHist A (nolock)
		where CONVERT(Date, CONVERT(VARCHAR,BookDate),111)
			    BETWEEN CONVERT(Date, CONVERT(VARCHAR,@MINDATE),111)  
				AND CONVERT(Date,@startDate,111)
				AND ( ((@UseRecvpay = 0 OR (@UseRecvpay = 1 AND recvpay = 1)) AND EXISTS (SELECT Type From @ActInType Act WHERE Act.Type = A.Type)
					AND (  BaseAmt >= @MinInAmt )) OR
					((@UseRecvpay = 0 OR (@UseRecvpay = 2 AND recvpay = 2)) AND EXISTS (SELECT Type From @ActOutType Act WHERE Act.Type = A.Type)
					AND (  BaseAmt >= @MinOutAmt )))		
       
		group by Cust
 
        INSERT INTO @TT4 (Cust, TotCnt, TotAmt, AvgAmt, StdevpAmt, TotCount, AvgCount, StdevpCount )
	    select T3.Cust, T3.TotCnt, T3.TotAmt, T3.AvgAmt, T3.StdevpAmt,T1.TotCount, T1.AvgCount, T1.StdevpCount
		FROM @TT3 T3 
		INNER JOIN @TT1  T1 ON T3.CUST = T1.Cust
       
	    INSERT INTO @TT4 (Cust, TotCnt, TotAmt, AvgAmt, StdevpAmt, TotCount, AvgCount, StdevpCount )
	    select T3.Cust, T3.TotCnt, T3.TotAmt, T3.AvgAmt, T3.StdevpAmt,0, 0, 0
		FROM @TT3 T3
		WHERE T3.CUST NOT IN (SELECT T1.CUST FROM @TT1 T1)

	    INSERT INTO @TT4 (Cust, TotCnt, TotAmt, AvgAmt, StdevpAmt, TotCount, AvgCount, StdevpCount )
	    select T1.Cust, 0,0,0,0,  TotCount, AvgCount, StdevpCount 
		FROM @TT1 T1
		WHERE T1.CUST NOT IN (SELECT T3.CUST FROM @TT3 T3)

	END
    --20210611 add for @AvgTranCntDays>0, End	
	
	--20210621 add for @AvgTranCntDays=0, Start
	ELSE IF ( @AvgTranCntDays=0 )
	BEGIN
				
		insert into @TT3 (Cust, TotCnt, TotAmt, AvgAmt, StdevpAmt)
		select Cust, COUNT(TranNo), SUM(BaseAmt), AVG(BaseAmt), STDEVP(BaseAmt)
		from ActivityHist A (nolock)
		where CONVERT(Date, CONVERT(VARCHAR,BookDate),111)
			    BETWEEN CONVERT(Date, CONVERT(VARCHAR,@MINDATE),111)  
				AND CONVERT(Date,@startDate,111)
				AND ( ((@UseRecvpay = 0 OR (@UseRecvpay = 1 AND recvpay = 1)) AND EXISTS (SELECT Type From @ActInType Act WHERE Act.Type = A.Type)
					AND (  BaseAmt >= @MinInAmt )) OR
					((@UseRecvpay = 0 OR (@UseRecvpay = 2 AND recvpay = 2)) AND EXISTS (SELECT Type From @ActOutType Act WHERE Act.Type = A.Type)
					AND (  BaseAmt >= @MinOutAmt )))	
				   
		group by A.Cust

        INSERT INTO @TT4 (Cust, TotCnt, TotAmt, AvgAmt, StdevpAmt, TotCount, AvgCount, StdevpCount )
	    select T3.Cust, T3.TotCnt, T3.TotAmt, T3.AvgAmt, T3.StdevpAmt,0,0,0
		FROM @TT3 T3
	
	END
	--20210621 add for @AvgTranCntDays=0, Start

/*SELECT * FROM @TT1
SELECT * FROM @TT3
SELECT * FROM @TT2
SELECT * FROM @TT4
GOTO EndOfProc
*/
	
	IF ( (@UseRecvPay = 1) OR (@UseRecvPay = 2) )
	BEGIN
		INSERT INTO @TT(Cust, Recv, Pay, Mindate, Maxdate, PreLastBookDate, Descr)
		SELECT cust, SUM(recv) recv, SUM(pay) pay,MIN(mindate) mindate,  
		    MAX(maxdate) maxdate, MAX(PreLastBookDate) PreLastBookDate,
		    'Cust:  ''' + cust 
			+ ''' has incoming activity '''
			+ CONVERT(VARCHAR, SUM(recv))
			+ ''' followed by outgoing activity '''
			+ CONVERT(VARCHAR, SUM(pay))
			+ ''' where the outgoing activity is '''
			+ CONVERT(VARCHAR, (SUM(pay)/SUM(recv))*100 ) 
			+ ''' percent of the incoming activity from bookdate '''
			+ CONVERT(VARCHAR, MIN(mindate)) + ''' to '''
			+ CONVERT(VARCHAR, MAX(maxdate)) + ''' last no of days is ''' 
			+ CONVERT(VARCHAR, @DAYS)+ ''' between (''' + CONVERT(VARCHAR, @Mindate) + ''', '''
			--+ CONVERT(VARCHAR, MAX(maxdate)) + '''), previous bookdate or opendate is '''
			+ CONVERT(VARCHAR, dbo.ConvertSqlDateToInt(@StartDate)) + '''), ' +  
			    CASE When @MonitorType = 1 Then ' previous bookdate is ''' 
				      when @MonitorType = 2 Then ' Account opendate is ''' 
					  else ''
                 END 
			+ CONVERT(VARCHAR, MAX(PreLastBookDate))  + ''',UseRecvPay is '''   
			+ CONVERT(VARCHAR, @UseRecvPay) +  '''(' +  
			    CASE When @UseRecvPay = 1 Then 'Recv), Monitor Type is ''' 
				      when @UseRecvPay = 2 Then 'Pay), Monitor Type is ''' 
					  When @UseRecvPay = 0 THEN 'ALL), Monitor Type is '''
                 END
			+ CONVERT(VARCHAR, @MonitorType) +  '''(' +  
			    CASE When @MonitorType = 1 Then 'Dorm), Standard deviation Ratio is ''' 
				      when @MonitorType = 2 Then 'New Open Account), Standard deviation Ratio is ''' 
					  When @MonitorType = 0 THEN 'ALL), Standard deviation Ratio is '''
                 END
			+ CONVERT(VARCHAR, @StDevRatio) + ''', Days for AvgTranCount is ''' + CONVERT(VARCHAR, @AvgTranCntDays)
			+ ''', Use Round Dollar is ''' + CONVERT(VARCHAR, @UseRound) + 
			CASE WHEN @AvgTranCntDays > 0 THEN 
			     ''', TotCount is ''' + CONVERT(VARCHAR, MAX(TotCount)) + 
			     ''', AvgCount is ''' +	 CONVERT(VARCHAR, MAX(AvgCount)) + ''', StdevpCount is '''
			    + CONVERT(VARCHAR, MAX(StdevpCount))  +''''
			ELSE
			    ''', AvgAmount is ''' +	 CONVERT(VARCHAR, MAX(AvgAmt)) + ''', StdevpAmount is '''
			    + CONVERT(VARCHAR, MAX(StdevpAmt)) + '''' 
			END	Descr

           
		FROM (
			SELECT 	Ach.cust, 		
			CASE 	WHEN recvpay = 1 THEN SUM(BaseAmt) 
				ELSE 0
			END recv,
			CASE 	WHEN recvpay = 2 THEN SUM(BaseAmt) 
				ELSE 0
			END pay,
			CASE 	WHEN recvpay = 1 THEN Count(Ach.Cust) 
				ELSE 0
			END InCount,
			CASE 	WHEN recvpay = 2 THEN Count(Ach.Cust) 
				ELSE 0
			END OutCount,
			MAX(T3.TotAmt) AS TotAmt, MAX(T3.AvgAmt) As AvgAmt, MAX(T3.StdevpAmt) AS StdevpAmt,
			MAX(T3.TotCount) AS TotCount, MAX(T3.AvgCount) AS AvgCount, MAX(T3.StdevpCount) AS StdevpCount,
			MIN(Ach.BookDate) AS mindate,
			MAX(Ach.BookDate) AS maxdate,
			MAX(m.BookDate) AS PreLastBookDate

			FROM 	ActivityHist Ach
			INNER JOIN @mAct m ON m.Account = Ach.Account
			INNER JOIN @TT4 T3 ON T3.Cust = Ach.Cust

			WHERE	((recvpay = 1 AND EXISTS(SELECT Type From @ActInType Act WHERE Act.Type = Ach.Type)
						AND (@MinInAmt=0 OR BaseAmt >= @MinInAmt )) OR
					(recvpay = 2 AND EXISTS(SELECT Type From @ActOutType Act WHERE Act.Type = Ach.Type)
						AND (@MinOutAmt = 0 OR BaseAmt >= @MinOutAmt)))
			AND	Ach.BookDate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate)

--20210621 add for @Monitor Type = Dorm(1)/ New Open Account(2) / ALL (0), Start
				AND ( ( @MonitorType = 1 AND Ach.Account IN (select Account from @mAct mA where CONVERT(DATE,CONVERT(VARCHAR,Ach.BookDate),111) >=  DATEADD(DAY, @DormOrNewOpenPeriod, CONVERT(DATE,CONVERT(VARCHAR,mA.BookDate),111)) ) ) 
					--OR ( @MonitorType = 2 AND Account IN (select Id from Account where dbo.ConvertSqlDateToInt(OpenDate) >= @minDate ) )
					OR ( @MonitorType = 2 AND Ach.Account IN (select Account from @mAct) )
					OR ( @MonitorType = 0 AND Ach.Account IN (Select id FROM Account ) )
					 )
--20210621 add for @Monitor Type = Dorm(1)/ New Open Account(2) / ALL (0), End 	

--20210621 add for use round , start 
				AND 1 = 
					Case When @UseRound = 1 and (BaseAmt % @tmp = 0) Then 1 
						 When @UseRound = 0 Then 1
						 When @UseRound = 1 and (BaseAmt % @tmp <> 0) Then 0
						Else 1 End
--20210621 add for use round , end    

			GROUP 	BY Ach.Cust, RecvPay
	
			HAVING ( (RecvPay=1 AND  (SUM(BaseAmt) >  ( AVG(BaseAmt) + (STDEVP(BaseAmt)*@StDevRatio)) ))   
			      OR (RecvPay=2 AND  (SUM(BaseAmt) >  ( AVG(BaseAmt) + (STDEVP(BaseAmt)*@StDevRatio)) ))  )          
		
		) A, Customer (NOLOCK)
		WHERE   A.Cust = Customer.ID
			AND ((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0))
			AND ((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0))

--20210622 ADD for @AvgTranCntDay, Start 
				  AND ( (@AvgTranCntDays > 0 AND A.Cust IN (SELECT T4.Cust From @TT4 T4 WHERE T4.TotCount > (T4.AvgCount + (T4.StdevpCount * @StDevRatio))) )
					 OR (@AvgTranCntDays = 0 AND A.CUST IN (SELECT T4.Cust From @TT4 T4 WHERE T4.TotAmt > (T4.AvgAmt + (T4.StdevpAmt * @StDevRatio)))) ) 
--20210622 ADD for @AvgTranCntDay, End	
/*
--20210623 ADD ????????? %?? ,START
        AND (  A.pay BETWEEN A.recv * @MinPercent AND A.recv * @MaxPercent 
		AND A.recv BETWEEN A.pay * @MinPercent AND A.pay * @MaxPercent )			  
--20210623 ADD ????????? %?? ,END	
*/

		GROUP 	BY cust
		HAVING 	SUM(recv) <> 0 AND SUM(pay) <> 0 
			AND 	(( @AmtCompareOper = 1 AND (SUM(Recv)>= @MinInAggAmt OR SUM(Pay)>= @MinOutAggAmt))
				OR		( @AmtCompareOper = 2 AND (SUM(Recv)>= @MinInAggAmt AND SUM(Pay)>= @MinOutAggAmt)))
			AND (SUM(Pay) > SUM(Recv) *  @Minpercent/100) 
			AND (@Maxpercent = -1  OR (SUM(Pay) < SUM(Recv) *  @Maxpercent/100))
			-- If @UseInOutCount = 1 Then Incoming/Outgoing Volume Ratio will be considered 
			-- If @UseInOutCount = 2 Then Outgoing/Incoming Volume Ratio will be considered 
			-- If @UseInOutCount = 3 then Volume Ratio will be ignored
/*
--202106 delete @InOutCntRatio , Start
			AND ((@UseInOutCount = 1 AND (@InOutCntRatio = 0 OR SUM(A.InCount)/SUM(A.OutCount)>=@InOutCntRatio ))
				OR (@UseInOutCount = 2 AND (@InOutCntRatio = 0 OR SUM(A.OutCount)/SUM(A.InCount)>=@InOutCntRatio))
				OR (@UseInOutCount = 3))
--202106 delete @InOutCntRatio , End
*/
			AND	((@InTranCnt = 0 OR SUM(A.InCount) >=@InTranCnt) 
				AND (@OutTranCnt = 0 OR SUM(A.OutCount) >= @OutTranCnt)) 		    

--20210624 add for @useRecvPay=1,2 , Start
		    AND	( ( @UseRecvPay=1 AND SUM(Recv) >  ( MAX(AvgAmt) + (MAX(StdevpAmt) *  @StDevRatio))) 
			   OR  ( @UseRecvPay=2 AND SUM(pay) >  ( MAX(AvgAmt) + (MAX(StdevpAmt) *  @StDevRatio)))  )
--20210624 add for @useRecvPay=1,2 , END      

	END

	ELSE IF (@UseRecvPay = 0)
	BEGIN
		INSERT INTO @TT(Cust, Recv, Pay, Mindate, Maxdate, PreLastBookDate, Descr)
		SELECT cust, SUM(recv) recv, SUM(pay) pay,MIN(mindate) mindate,  
		    MAX(maxdate) maxdate, MAX(PreLastBookDate) PreLastBookDate,
		    'Cust:  ''' + cust 
			+ ''' has incoming activity '''
			+ CONVERT(VARCHAR, SUM(recv))
			+ ''' followed by outgoing activity '''
			+ CONVERT(VARCHAR, SUM(pay))
			+ ''' where the outgoing activity is '''
			+ CONVERT(VARCHAR, (SUM(pay)/SUM(recv))*100 ) 
			+ ''' percent of the incoming activity from bookdate '''
			+ CONVERT(VARCHAR, MIN(mindate)) + ''' to '''
			+ CONVERT(VARCHAR, MAX(maxdate)) + ''' last no of days is ''' 
			+ CONVERT(VARCHAR, @DAYS)+ ''' between (''' + CONVERT(VARCHAR, @Mindate) + ''', '''
			--+ CONVERT(VARCHAR, MAX(maxdate)) + '''), previous bookdate or opendate is '''
			+ CONVERT(VARCHAR, dbo.ConvertSqlDateToInt(@StartDate)) + '''), ' +  
			    CASE When @MonitorType = 1 Then ' previous bookdate is ''' 
				      when @MonitorType = 2 Then ' Account opendate is ''' 
					  else ''
                 END 
			+ CONVERT(VARCHAR, MAX(PreLastBookDate))  + ''',UseRecvPay is '''   
			+ CONVERT(VARCHAR, @UseRecvPay) +  '''(' +  
			    CASE When @UseRecvPay = 1 Then 'Recv), Monitor Type is ''' 
				      when @UseRecvPay = 2 Then 'Pay), Monitor Type is ''' 
					  When @UseRecvPay = 0 THEN 'ALL), Monitor Type is '''
                 END
			+ CONVERT(VARCHAR, @MonitorType) +  '''(' +  
			    CASE When @MonitorType = 1 Then 'Dorm), Standard deviation Ratio is ''' 
				      when @MonitorType = 2 Then 'New Open Account), Standard deviation Ratio is ''' 
					  When @MonitorType = 0 THEN 'ALL), Standard deviation Ratio is '''
                 END
			+ CONVERT(VARCHAR, @StDevRatio) + ''', Days for AvgTranCount is ''' + CONVERT(VARCHAR, @AvgTranCntDays)
			+ ''', Use Round Dollar is ''' + CONVERT(VARCHAR, @UseRound) + 
			CASE WHEN @AvgTranCntDays > 0 THEN 
			     ''', TotCount is ''' + CONVERT(VARCHAR, MAX(TotCount)) + 
				 ''', AvgCount is ''' +	 CONVERT(VARCHAR, MAX(AvgCount)) + ''', StdevpCount is '''
			    + CONVERT(VARCHAR, MAX(StdevpCount))  +''''
			ELSE 
			    ''', AvgAmount is ''' +	 CONVERT(VARCHAR, MAX(AvgAmt)) + ''', StdevpAmount is '''
			    + CONVERT(VARCHAR, MAX(StdevpAmt)) + '''' 
			END	Descr


		FROM (
			SELECT 	Ach.cust, 		
			SUM(BaseAmt) recv,
			 0 pay,
			 Count(Ach.Cust) InCount,
			 0 OutCount,
			MAX(T3.TotAmt) AS TotAmt, MAX(T3.AvgAmt) As AvgAmt, MAX(T3.StdevpAmt) AS StdevpAmt,
			MAX(T3.TotCount) AS TotCount, MAX(T3.AvgCount) AS AvgCount, MAX(T3.StdevpCount) AS StdevpCount,
			MIN(Ach.BookDate) AS mindate,
			MAX(Ach.BookDate) AS maxdate,
			MAX(m.BookDate) AS PreLastBookDate

			FROM 	ActivityHist Ach
			INNER JOIN @mAct m ON m.Account = Ach.Account
			INNER JOIN @TT4 T3 ON T3.Cust = Ach.Cust

			INNER	JOIN @ActInType Act ON Ach.Type = Act.Type
		    WHERE   RecvPay=1 AND  Ach.bookdate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate) 
				AND (@MinInAmt = 0 OR Baseamt>= @minInAmt)
--20210621 add for @Monitor Type = Dorm(1)/ New Open Account(2) / ALL (0), Start
				AND ( ( @MonitorType = 1 AND Ach.Account IN (select Account from @mAct mA where CONVERT(DATE,CONVERT(VARCHAR,Ach.BookDate),111) >=  DATEADD(DAY, @DormOrNewOpenPeriod, CONVERT(DATE,CONVERT(VARCHAR,mA.BookDate),111)) ) ) 
					--OR ( @MonitorType = 2 AND Account IN (select Id from Account where dbo.ConvertSqlDateToInt(OpenDate) >= @minDate ) )
					OR ( @MonitorType = 2 AND Ach.Account IN (select Account from @mAct) )
					OR ( @MonitorType = 0 AND Ach.Account IN (Select id FROM Account ) )
					 )
--20210621 add for @Monitor Type = Dorm(1)/ New Open Account(2) / ALL (0), End 	

--20210621 add for use round , start 
				AND 1 = 
					Case When @UseRound = 1 and (BaseAmt % @tmp = 0) Then 1 
						 When @UseRound = 0 Then 1
						 When @UseRound = 1 and (BaseAmt % @tmp <> 0) Then 0
						Else 1 End
--20210621 add for use round , end    

			GROUP 	BY Ach.cust
/*
--20210625 ADD FOR , START
			HAVING  ( (SUM(BaseAmt) >= ( AVG(BaseAmt) + (STDEVP(BaseAmt)*@StDevRatio)) )
			        OR (MAX(TotAmt) > = ( MAX(AvgAmt) + (MAX(StdevpAmt) * @StDevRatio)))
					OR (MAX(TotCount) > = ( MAX(AvgCount) + (MAX(StdevpCount) *@StDevRatio))) )
--20210625 ADD FOR , END			     
*/			
			UNION
	
			SELECT 	Ach.cust, 		
			0 recv,
			SUM(baseamt)  pay,
			0 InCount,
			Count(Ach.Cust) OutCount,
			MAX(T3.TotAmt) AS TotAmt, MAX(T3.AvgAmt) As AvgAmt, MAX(T3.StdevpAmt) AS StdevpAmt,
			MAX(T3.TotCount) AS TotCount, MAX(T3.AvgCount) AS AvgCount, MAX(T3.StdevpCount) AS StdevpCount,
			MIN(Ach.BookDate) AS mindate,
			MAX(Ach.BookDate) AS maxdate,
			MAX(m.BookDate) AS PreLastBookDate

			FROM 	ActivityHist Ach
			INNER JOIN @mAct m ON m.Account = Ach.Account
			INNER JOIN @TT4 T3 ON T3.Cust = Ach.Cust

			INNER	JOIN @ActOutType Act ON Ach.Type = Act.Type			
		    WHERE    RecvPay=2 AND  Ach.bookdate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate)
				AND (@MinOutAmt = 0 OR BaseAmt>= @MinOutAmt )
--20210610 add for @Monitor Type = Dorm(1)/ New Open Account(2) / ALL (0), Start
				AND ( ( @MonitorType = 1 AND Ach.Account IN (select Account from @mAct mA where CONVERT(DATE,CONVERT(VARCHAR,Ach.BookDate),111) >=  DATEADD(DAY, @DormOrNewOpenPeriod, CONVERT(DATE,CONVERT(VARCHAR,mA.BookDate),111)) ) ) 
					--OR ( @MonitorType = 2 AND Account IN (select Id from Account where dbo.ConvertSqlDateToInt(OpenDate) >= @minDate ) )
					OR ( @MonitorType = 2 AND Ach.Account IN (select Account from @mAct) )
					OR ( @MonitorType = 0 AND Ach.Account IN (Select id FROM Account ) )
					 )
--20210610 add for @Monitor Type = Dorm(1)/ New Open Account(2) / ALL (0), End 	

--20210610 add for use round , start 
				AND 1 = 
					Case When @UseRound = 1 and (BaseAmt % @tmp = 0) Then 1 
						 When @UseRound = 0 Then 1
						 When @UseRound = 1 and (BaseAmt % @tmp <> 0) Then 0
						Else 1 End
--20210610 add for use round , end    

			GROUP 	BY Ach.Cust
/*
--20210625 ADD FOR , START
			HAVING  ( (SUM(BaseAmt) >= ( AVG(BaseAmt) + (STDEVP(BaseAmt)*@StDevRatio)) )
			        OR (MAX(TotAmt) > = ( MAX(AvgAmt) + (MAX(StdevpAmt) * @StDevRatio)))
					OR (MAX(TotCount) > = ( MAX(AvgCount) + (MAX(StdevpCount) *@StDevRatio))) )
--20210625 ADD FOR , END	
*/

		) a , Customer (NOLOCK)

		WHERE   a.cust = Customer.ID
			AND ((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0))
			AND ((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0))

--20210622 ADD for @AvgTranCntDay, Start 
				  AND ( (@AvgTranCntDays > 0 AND A.Cust IN (SELECT T4.Cust From @TT4 T4 WHERE T4.TotCount > (T4.AvgCount + (T4.StdevpCount * @StDevRatio))) )
					 OR (@AvgTranCntDays = 0 AND A.CUST IN (SELECT T4.Cust From @TT4 T4 WHERE T4.TotAmt >  (T4.AvgAmt + (T4.StdevpAmt * @StDevRatio)))) ) 
--20210622 ADD for @AvgTranCntDay, End	

/*
--20210623 ADD ????????? %?? ,START
        AND (  A.pay BETWEEN A.recv * @MinPercent AND A.recv * @MaxPercent 
		AND A.recv BETWEEN A.pay * @MinPercent AND A.pay * @MaxPercent )			  
--20210623 ADD ????????? %?? ,END	
*/	
		GROUP 	BY cust

		HAVING 	SUM(recv) <> 0 AND SUM(pay) <> 0 
			AND 	(( @AmtCompareOper = 1 AND (SUM(Recv)>= @MinInAggAmt OR SUM(Pay)>= @MinOutAggAmt))
            	OR		( @AmtCompareOper = 2 AND (SUM(Recv)>= @MinInAggAmt AND SUM(Pay)>= @MinOutAggAmt)))
			AND 	(SUM(Pay) > SUM(Recv) *  @Minpercent/100) 
			AND     (@Maxpercent = -1 OR (SUM(Pay) < SUM(Recv) *  @Maxpercent/100))
			-- If @UseInOutCount = 1 Then Incoming/Outgoing Volume Ratio will be considered 
			-- If @UseInOutCount = 2 Then Outgoing/Incoming Volume Ratio will be considered 
			-- If @UseInOutCount = 3 then Volume Ratio will be ignored
/*
--202106 delete @InOutCntRatio , Start
			AND (@UseInOutCount = 1 AND (@InOutCntRatio = 0 OR SUM(A.InCount)/SUM(A.OutCount)>=@InOutCntRatio )
				OR (@UseInOutCount = 2 AND (@InOutCntRatio = 0 OR SUM(A.OutCount)/SUM(A.InCount)>=@InOutCntRatio))
				OR (@UseInOutCount = 3))
--202106 delete @InOutCntRatio , End
*/
			AND ((@InTranCnt = 0 OR SUM(A.InCount) >=@InTranCnt) 
				AND (@OutTranCnt = 0 OR SUM(A.OutCount) >= @OutTranCnt)) 

	END

/*
	--select * from @mAct
	SELECT * from @TT4
	select * from @TT
	GOTO EndOfProc
*/

	IF @testAlert = 1 
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, CUST, IsTest) 
		  SELECT @WLCode, descr, 0, GETDATE(), cust, 1 FROM @TT 
	
		SELECT @STAT = @@ERROR 

		IF @stat <> 0 GOTO EndOfProc
		SELECT @ENDALRTDATE = GETDATE()

		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	'Alert', AlertNo, TRANNO
			FROM 	ACTIVITYHIST A (NOLOCK) 
			INNER 	JOIN Alert AL ON AL.Cust = A.Cust
			AND	((@UseRecvpay = 0 OR recvpay = 1) AND (@MinInAmt = 0 OR Baseamt >= @MinInAmt) AND
			EXISTS(SELECT Type FROM @ActInType Act WHERE A.Type = Act.Type) OR
            ((@UseRecvpay = 0 OR recvpay = 2) AND (@MinOutAmt = 0 OR Baseamt >= @MinOutAmt)AND EXISTS
			(SELECT Type FROM @ActOutType Act WHERE A.Type = Act.Type)))
			WHERE	A.bookdate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate)
			AND 	AL.WLCode = @WLCode
			AND 	AL.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE		
			
			AND  (@UseRecvPay = 0  OR A.RecvPay=@UseRecvPay)	--add
--20210621 add for @Monitor Type = Dorm(1)/ New Open Account(2) / ALL (0), Start
				AND ( ( @MonitorType = 1 AND A.Account IN (select Account from @mAct mA where CONVERT(DATE,CONVERT(VARCHAR,A.BookDate),111) >=  DATEADD(DAY, @DormOrNewOpenPeriod, CONVERT(DATE,CONVERT(VARCHAR,mA.BookDate),111)) ) ) 
					--OR ( @MonitorType = 2 AND Account IN (select Id from Account where dbo.ConvertSqlDateToInt(OpenDate) >= @minDate ) )
					OR ( @MonitorType = 2 AND A.Account IN (select Account from @mAct) )
					OR ( @MonitorType = 0 AND A.Account IN (Select id FROM Account ) )
					 )
--20210621 add for @Monitor Type = Dorm(1)/ New Open Account(2) / ALL (0), End 	

--20210621 add for use round , start 
				AND 1 = 
					Case When @UseRound = 1 and (BaseAmt % @tmp = 0) Then 1 
						 When @UseRound = 0 Then 1
						 When @UseRound = 1 and (BaseAmt % @tmp <> 0) Then 0
						Else 1 End
--20210621 add for use round , end    

		SELECT @STAT = @@ERROR 
		IF @stat <> 0 GOTO EndOfProc

	END 
	ELSE 
	BEGIN
		IF @WLType = 0 
		BEGIN
			SELECT @STARTALRTDATE = GETDATE()
			INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, CUST) 
				SELECT @WLCode, descr, 0, GETDATE(), cust FROM @TT 
			SELECT @ENDALRTDATE = GETDATE()
	
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	'Alert', AlertNo, TRANNO
				FROM 	ACTIVITYHIST A (NOLOCK) 
				INNER 	JOIN Alert AL ON AL.Cust = A.Cust
				AND	((@UseRecvpay = 0 OR recvpay = 1)  AND (@MinInAmt = 0 OR Baseamt >= @MinInAmt) AND 
				EXISTS(SELECT Type FROM @ActInType Act WHERE A.Type = Act.Type) OR
                ((@UseRecvpay = 0 OR recvpay = 2) AND (@MinOutAmt = 0 OR Baseamt >= @MinOutAmt) AND EXISTS
				(SELECT Type FROM @ActOutType Act WHERE A.Type = Act.Type)))
				WHERE	A.bookdate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate)
				AND 	AL.WLCode = @WLCode
				AND 	AL.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE					
--20210621 add for @Monitor Type = Dorm(1)/ New Open Account(2) / ALL (0), Start
				AND ( ( @MonitorType = 1 AND A.Account IN (select Account from @mAct mA where CONVERT(DATE,CONVERT(VARCHAR,A.BookDate),111) >=  DATEADD(DAY, @DormOrNewOpenPeriod, CONVERT(DATE,CONVERT(VARCHAR,mA.BookDate),111)) ) ) 
					--OR ( @MonitorType = 2 AND Account IN (select Id from Account where dbo.ConvertSqlDateToInt(OpenDate) >= @minDate ) )
					OR ( @MonitorType = 2 AND A.Account IN (select Account from @mAct) )
					OR ( @MonitorType = 0 AND A.Account IN (Select id FROM Account ) )
					 )
--20210621 add for @Monitor Type = Dorm(1)/ New Open Account(2) / ALL (0), End 	

--20210621 add for use round , start 
				AND 1 = 
					Case When @UseRound = 1 and (BaseAmt % @tmp = 0) Then 1 
						 When @UseRound = 0 Then 1
						 When @UseRound = 1 and (BaseAmt % @tmp <> 0) Then 0
						Else 1 End
--20210621 add for use round , end 
	
			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE IF @WLType = 1 
		BEGIN	
			SELECT @STARTALRTDATE = GETDATE()
	                INSERT INTO SUSPICIOUSACTIVITY (BOOKDATE, CUST, SUSPTYPE, 
	                        INCNTTOLPERC, OUTCNTTOLPERC, INAMTTOLPERC, 
				OUTAMTTOLPERC, WLCode, WLDESC, Createtime)
			SELECT	dbo.ConvertSqlDateToInt(GetDate()), Cust, 'RULE', 0, 0, 0, 0, 
				@WLCode, descr, getdate()
			FROM @tt
			IF @stat <> 0 GOTO EndOfProc
			SELECT @ENDALRTDATE = GETDATE()
	
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	'SUSPACT', RecNo, TRANNO 
				FROM 	ACTIVITYHIST A (NOLOCK)
				INNER 	JOIN SuspiciousActivity S ON S.Cust = A.Cust
				AND	((@UseRecvpay = 0 OR recvpay = 1) AND (@MinInAmt = 0 OR BaseAmt >= @MinInAmt)AND 
				EXISTS(SELECT Type FROM @ActInType Act WHERE A.Type = Act.Type) OR
                ((@UseRecvpay = 0 OR recvpay = 2) AND (@MinOutAmt = 0 OR Baseamt >= @MinOutAmt) AND EXISTS
				(SELECT Type FROM @ActOutType Act WHERE A.Type = Act.Type)))
				WHERE	A.BookDate BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate)
				AND 	s.WLCode = @WLCode
			    AND 	s.createtime BETWEEN @STARTALRTDATE AND @ENDALRTDATE
--20210621 add for @Monitor Type = Dorm(1)/ New Open Account(2) / ALL (0), Start
				AND ( ( @MonitorType = 1 AND A.Account IN (select Account from @mAct mA where CONVERT(DATE,CONVERT(VARCHAR,A.BookDate),111) >=  DATEADD(DAY, @DormOrNewOpenPeriod, CONVERT(DATE,CONVERT(VARCHAR,mA.BookDate),111)) ) ) 
					--OR ( @MonitorType = 2 AND Account IN (select Id from Account where dbo.ConvertSqlDateToInt(OpenDate) >= @minDate ) )
					OR ( @MonitorType = 2 AND A.Account IN (select Account from @mAct) )
					OR ( @MonitorType = 0 AND A.Account IN (Select id FROM Account ) )
					 )
--20210621 add for @Monitor Type = Dorm(1)/ New Open Account(2) / ALL (0), End 	

--20210621 add for use round , start 
				AND 1 = 
					Case When @UseRound = 1 and (BaseAmt % @tmp = 0) Then 1 
						 When @UseRound = 0 Then 1
						 When @UseRound = 1 and (BaseAmt % @tmp <> 0) Then 0
						Else 1 End
--20210621 add for use round , end 
	
			SELECT @STAT = @@ERROR
			IF @stat <> 0 GOTO EndOfProc
		END
	END


EndOfProc:

	IF (@stat <> 0) BEGIN 
	  ROLLBACK TRAN USR_zActVeloc
	  RETURN @stat
	END	
	
	IF @trnCnt = 0
	  COMMIT TRAN USR_zActVeloc
	RETURN @stat
GO


