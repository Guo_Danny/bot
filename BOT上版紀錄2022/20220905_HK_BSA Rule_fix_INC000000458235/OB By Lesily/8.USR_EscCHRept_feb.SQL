USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_EscCHRept]    Script Date: 5/11/2021 5:57:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[USR_EscCHRept](@WLCode SCODE, @testAlert INT,
	@CTRAmount MONEY,     
	@LastNoOfMonths INT,  
	@MinTotalCTRAmount MONEY,
	@Tolerance INT,
	@TransOfDays INT,
	@STDEVCounts Float,
	@OrgBankCountryCode varchar(8000))
AS

/* Rule and Parameter Description
   Detects structuring of cash deposits to an account in order to avoid CTR 
	reporting. An Alert or SA case is created for each account that has cash 
	deposits being structured and the total amount deposited exceeds a specified 
	maximum  dollar amount over a certain time period.  Designed to be run 
	Post-EOD on a monthly or quarterly schedule.
 
   @CTRAmount	           -- Rule will take into account any cash transaction greater than 
                          or equal to (@CTRAmount - Tolerance percent of @CTRAmount)
   @LastNoOfMonths     -- Rule will consider all activity that occurred in this 
                          specified last number of months 
   @MinTotalCTRAmount     -- Rule checks if the total sum of all cash transactions in 
                          the specified last number of months is greater than or 
                          equal to (@MinTotalCTRAmount - Tolerance% of @MinTotalCTRAmount)
   @Tolerance  	       -- Tolerance as given above
   @TransOfDays	--no of day for tran counts
   @STDEVCounts	--times of Stdev tran counts
*/

/*  DECLARATIONS */
DECLARE	@CUR CURSOR,
	@DESCRIPTION VARCHAR(2000),
	@DESC VARCHAR(2000),
	@ID INT, 
	@WLTYPE INT,
	@STAT INT,
	@TRNCNT INT,
	@MINDATE INT -- start date , current date is end date
--MAY. 10, 2021 
declare @curdate datetime; --to calculate the current date
declare @recentdate datetime; -- to calculate the last No. TransOfDays
declare @lastdate datetime;-- the begining date

Declare @startAlrtDate datetime
Declare @endAlrtDate Datetime
Declare @POSSCTRAMT Money
Declare @CurrDate GenericDate
Declare @wrkDate Datetime

DECLARE	@TT TABLE (
    Cust VARCHAR(40),
	Account VARCHAR(40),
	TranNO INT,
	BaseAmt MONEY,
	BookDate Int,
	OrgBkCtyCode Varchar(8000)

)

DECLARE	@TT1 TABLE (
	Account VARCHAR(40),
	AvgCount INT,
	StdevpCount Float
)

DECLARE	@TT2 TABLE (
	Account VARCHAR(40),
	s_date int,
	e_date int,
	cur_date int,
	trncnt int)

--- ********************* Begin Rule Procedure **********************************
SET NOCOUNT ON
SET @STAT = 0


/*****************************************/
-- Date options
-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam
Declare @StartDate Datetime

Select @StartDate = 
	Case 	
		When UseSysDate in (0,1) Then
			-- use System date
			GetDate()
		When UseSysDate = 2 Then
			-- use business date
			(Select BusDate From dbo.SysParam)
		Else
			GetDate()
	End
From dbo.WatchList
Where WLCode = @WlCode

/******************************************/

SET @LastNoOfMonths = ABS(@LastNoOfMonths)

--20210420 add for orgbankcountrycode, start
SELECT @OrgBankCountryCode = dbo.BSA_fnListParams(@OrgBankCountryCode)
--20210420 add for orgbankcountrycode, end

SELECT @MINDATE = DBO.CONVERTSQLDATETOINT(
		DATEADD(M, -1 * @LastNoOfMonths, CONVERT(VARCHAR, @StartDate)))

/* START STANDARD STORED PROCEDURE TRANSACTION HEADER */
SET @TRNCNT = @@TRANCOUNT	-- SAVE THE CURRENT TRANCOUNT
IF @TRNCNT = 0
	-- TRANSACTION HAS NOT BEGUN
	BEGIN TRAN USR_EscCHRept
ELSE
	-- ALREADY IN A TRANSACTION
	SAVE TRAN USR_EscCHRept
/* END STANDARD STORED PROCEDURE TRANSACTION HEADER */


/*  STANDARD RULES HEADER */
SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE  
FROM WATCHLIST (NOLOCK) WHERE WLCode = @WLCode

--MAY. 10, 2021 begin
set @recentdate = @StartDate - @TransOfDays;
set @lastdate = DATEADD(M, -1 * @LastNoOfMonths, CONVERT(VARCHAR, @StartDate));
set @curdate = @lastdate + @TransOfDays;
while (@curdate + @TransOfDays <= @StartDate)
begin
insert into @TT2(Account, s_date, e_date, cur_date, trncnt)
select Account, convert(varchar(8), @lastdate, 112),
convert(varchar(8), @curdate, 112),
BookDate, count(*)  
from ActivityHist 
where BookDate >= convert(varchar(8), @lastdate,112) and BookDate < convert(varchar(8), @curdate, 112)
 and recvpay=1

group by Account,BookDate
set @lastdate = @curdate;
set @curdate = @curdate + @TransOfDays;

end

insert into @TT1(Account, AvgCount, StdevpCount)
select Account, avg(trncnt), STDEVP(trncnt) from (
select Account, s_date, e_date, SUM(trncnt) trncnt from @TT2
group by Account, s_date, e_date
) A group by Account
having STDEVP(trncnt) > 0 --modify SEP.12,2022
order by Account

--MAY. 10, 2021 end



SET @POSSCTRAMT = (100 - @Tolerance)* @CTRAmount/100
--should we consider maxtotamount
--SET @MinTotalCTRAmount = (100 - @Tolerance)* @MinTotalCTRAmount/100

INSERT INTO @TT (Cust,Account, TranNo, BaseAmt, Bookdate, OrgBkCtyCode)
SELECT Cust,Account,tranno, baseAmt, bookdate, ByOrderBankCountry
	FROM ACTIVITYHIST (NOLOCK)
	WHERE   RECVPAY = 1 
		AND BOOKDATE >= DBO.CONVERTSQLDATETOINT(@recentdate)
		AND BOOKDATE < DBO.CONVERTSQLDATETOINT(@StartDate)												
	    AND BASEAMT >= @POSSCTRAMT and BASEAMT <= @CTRAmount
		 
		  --20210420 add for HK requirement, add params of OrigBkCountryCode, start
		  AND ( (ISNULL(@OrgBankCountryCode,'') = '' OR 
		        CHARINDEX(',' + LTRIM(RTRIM(ByOrderBankCountry)) + ',', @OrgBankCountryCode) > 0) )
		 --20210420 add for HK requirement, add params of OrigBkCountryCode, end
		

IF @testAlert = 1 BEGIN
	SELECT @STARTALRTDATE = GetDate()
	Insert into Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
			LastModify, Cust, Account, IsTest) 
	  Select @WLCode, 'Account: ''' + ISNULL(T.Account,'')  
		+ ''' had ' + convert(varchar,Count(TranNo))
       + ''' incoming transactions close to CTR amount tolerance range and originated from '+  isnull(@OrgBankCountryCode,'countrynonspecified')
	   + ' between the period ''' + CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(@recentdate)) + ''' and ''' +
		 convert(varchar,DBO.CONVERTSQLDATETOINT(@StartDate))+ '''. Greater than (or equal to)  Avg Tran Counts:' 
		 + convert(varchar,CONVERT(decimal(12,2),Avgcount)) + ', plus ' 
		 + CONVERT(varchar,@STDEVCounts) + ' Times of STdev Tran Counts: '
		 +  convert(varchar,CONVERT(decimal(12,2),StdevpCount)) + ' for every '
		+ CONVERT(varchar,@TransOfDays) + ' days in last ' + CONVERT(varchar,@LastNoOfMonths)  
		+ ' months. The total deposited amount $''' + CONVERT(VARCHAR, Sum(BaseAmt))  
	+' , and so these transactions could be suspicious.'
			,0, GetDate(), Null, Null, Cust, T.ACCOUNT, 1
	  From @TT T
		join @TT1 T1 on T.Account = T1.Account 
	GROUP BY T.cust,T.ACCOUNT,T1.AvgCount,T1.StdevpCount
	    HAVING SUM(Baseamt) >= @MinTotalCTRAmount
		And  isnumeric(T1.StdevpCount) =1
		AND count(T.Tranno) > (T1.AvgCount + T1.StdevpCount * @STDEVCounts)
	Select @STAT = @@error	
	Select @ENDALRTDATE = GetDate()
	if @STAT <> 0  GOTO ENDOFPROC

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'Alert', AlertNo, TRANNO 
		FROM @TT t, Alert where
		IsNull(t.Account,'') = IsNull(Alert.Account,'') AND
		Alert.WLCode = @WLCode and
		Alert.CreateDate between @startAlrtDate and @endAlrtDate
	SELECT @STAT = @@ERROR
END ELSE BEGIN
	IF @WLTYPE = 0 BEGIN
	SELECT @STARTALRTDATE = GetDate()
	Insert into Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
			LastModify, Cust, Account) 
	  Select @WLCode, 'Account: ''' + ISNULL(T.Account,'')  
		+ ''' had ' + convert(varchar,Count(TranNo))
       + ''' incoming transactions close to CTR amount tolerance range and originated from '+  isnull(@OrgBankCountryCode,'countrynonspecified')
	   + ' between the period ''' + CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(@recentdate)) + ''' and ''' +
		 convert(varchar,DBO.CONVERTSQLDATETOINT(@StartDate))+ '''. Greater than (or equal to)  Avg Tran Counts:' 
		 + convert(varchar,CONVERT(decimal(12,2),Avgcount)) + ', plus ' 
		 + CONVERT(varchar,@STDEVCounts) + ' Times of STdev Tran Counts: '
		 +  convert(varchar,CONVERT(decimal(12,2),StdevpCount)) + ' for every '
		+ CONVERT(varchar,@TransOfDays) + ' days in last ' + CONVERT(varchar,@LastNoOfMonths)  
		+ ' months. The total deposited amount $''' + CONVERT(VARCHAR, Sum(BaseAmt))  
	+' , and so these transactions could be suspicious.'
			, 0, 
		GetDate(), Null, Null, T.cust, T.ACCOUNT 
	  From @TT T
		join @TT1 T1 on T.Account = T1.Account 
	GROUP BY T.cust,T.ACCOUNT,T1.AvgCount,StdevpCount
	    HAVING SUM(Baseamt) >= @MinTotalCTRAmount
		and T1.StdevpCount > 0
		AND count(T.tranno) > (T1.AvgCount + T1.StdevpCount * @STDEVCounts)
	Select @STAT = @@error	
	Select @ENDALRTDATE = GetDate()
	if @STAT <> 0  GOTO ENDOFPROC
	END ELSE IF @WLTYPE = 1 BEGIN
		select @wrkDate = GetDate()
		Exec @CurrDate = BSA_CvtDateToLong @wrkDate
		SELECT @STARTALRTDATE = GetDate()
		insert into SuspiciousActivity (ProfileNo, BookDate, Cust, Account, 
			Activity, SuspType, StartDate, EndDate, RecurType, 
			RecurValue, ActCurrReportAmt, ActInActCnt, ActOutActCnt, 
			ActInActAmt, ActOutActAmt, CurrReportAmt, ExpAvgInActCnt, 
			ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt, InCntTolPerc, 
			OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr, ReviewState, 
			ReviewTime, ReviewOper, App, AppTime, AppOper, 
			WLCode, WLDesc, CreateTime )
		Select	Null, @CurrDate, T.Cust, T.Account,
			Null, 'Rule', Null, Null, Null, Null,
			Null, Null, Null, Null, Null, Null, Null, Null,
			Null, Null, 0, 0, 0, 0, 
			Null, Null, Null, Null, 0, Null, Null,
			@WLCode, 'Account: ''' + ISNULL(T.Account,'')  
		+ ''' had ' + convert(varchar,Count(TranNo))
       + ''' incoming transactions close to CTR amount tolerance range and originated from '+  isnull(@OrgBankCountryCode,'countrynonspecified')
	   + ' between the period ''' + CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(@recentdate)) + ''' and ''' +
		 convert(varchar,DBO.CONVERTSQLDATETOINT(@StartDate))+ '''. Greater than (or equal to)  Avg Tran Counts:' 
		 + convert(varchar,CONVERT(decimal(12,2),Avgcount)) + ', plus ' 
		 + CONVERT(varchar,@STDEVCounts) + ' Times of STdev Tran Counts: '
		 +  convert(varchar,CONVERT(decimal(12,2),StdevpCount)) + ' for every '
		+ CONVERT(varchar,@TransOfDays) + ' days in last ' + CONVERT(varchar,@LastNoOfMonths)  
		+ ' months. The total deposited amount $''' + CONVERT(VARCHAR, Sum(BaseAmt))  
	+' , and so these transactions could be suspicious.', GetDate() 
	  From @TT T
		join @TT1 T1 on T.Account = T1.Account 
	GROUP BY T.cust,T.ACCOUNT,T1.AvgCount,T1.StdevpCount
	    HAVING SUM(Baseamt) >= @MinTotalCTRAmount
		And  isnumeric(T1.StdevpCount) =1 
		AND count(T.tranno) > (T1.AvgCount + T1.StdevpCount * @STDEVCounts)
	
	Select @STAT = @@error	
	Select @ENDALRTDATE = GetDate()
	if @STAT <> 0  GOTO ENDOFPROC
	END
	
	IF @WLTYPE = 0 BEGIN
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'Alert', AlertNo, TRANNO 
		FROM @TT t, Alert where
		IsNull(t.Account,'') = IsNull(Alert.Account,'') AND
		Alert.WLCode = @WLCode and
		Alert.CreateDate between @startAlrtDate and @endAlrtDate
		
	SELECT @STAT = @@ERROR
	END ELSE IF @WLTYPE = 1 BEGIN 
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'SUSPACT', RecNo, TRANNO 
		FROM @TT t, SuspiciousActivity s where
		IsNull(t.Account,'') = IsNull(s.Account,'') AND
		s.WLCode = @WLCode and
		s.CreateTime between @startAlrtDate and @endAlrtDate
	SELECT @STAT = @@ERROR 
	END
END

ENDOFPROC:
IF (@STAT <> 0) BEGIN 
  ROLLBACK TRAN USR_EscCHRept
  RETURN @STAT
END	

IF @TRNCNT = 0
  COMMIT TRAN USR_EscCHRept
RETURN @STAT
GO


