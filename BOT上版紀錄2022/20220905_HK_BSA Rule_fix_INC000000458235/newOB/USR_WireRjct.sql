USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_WireRjct]    Script Date: 7/5/2021 11:54:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE	[dbo].[USR_WireRjct]	(@WLCode SCode, 
									@TestAlert  INT,
									@activityTypeList VARCHAR(8000), 
									@CustTypeList VARCHAR(8000),
									@RecvPay SMALLINT,
									@minAmount MONEY, 
					   
									@NumMonths INT,
									@StdDev float,   --@avgCount INT,									
									@InclLastMth INT,
									@UseRound    INT,
									@Precision   INT
									)
AS

/* RULE AND PARAMETER DESCRIPTION
		Detects accounts where incoming or outgoing transactions of specified activity types exceeding 
		certain transaction count threshold and exceeding certain aggregated dollar amount threshold 
		per month and the monthly volume exceeding average monthly volume by a certain multiple.

		@activityTypeList - Type of activity to be detected.
		@CustTypeList - List of Customer Types that will be considered. 
						   -ALL-, blank or NULL for all Customer Types
		@minAmount   - minimum Aggregate Amount
										 
		--@avgCount   - Average Count of transactions. Only months that had transactions will be counted 
		--			for an activity period of 12 months.
		@StdDev -
		@NumMonths - Number of Months
		@InclLastMth - Include Last Month in Average 1-Yes 0-No
		@UseRound = Indicate if only the high round amounts have to considered.
					0 - Do not round
					1 - Round
		@Precision = Precision for rounding. Specify 3 for 1000
*/

/*  Declarations */

	DECLARE @description      VARCHAR(2000),
		@desc             VARCHAR(2000),
		@Id         	  INT, 
		@WLType     	  INT,
		@stat       	  INT,
		@trnCnt     	  INT,
		@minDate    	  INT,
		@maxDate    	  INT,
		@beginDate  	  INT,
		@MinDateAvg 	  INT,
		@MaxDateAvg 	  INT,
		@ActivityType	  INT,
		@Pos		  INT,
		@tmp INT	--JUL-02-2021 add tmp

	DECLARE @STARTALRTDATE  DATETIME
	DECLARE @ENDALRTDATE    DATETIME
	DECLARE     @TT TABLE 
	( 
		tranno INT,
		cust	VARCHAR(40),
		Account     VARCHAR(40),
		type    varchar(10),
		RecvPay	   	SMALLINT,
		bookdate	VARCHAR(40),
		BaseAmt     MONEY
				 
	)


	DECLARE @ActType TABLE
	(
		Type INT
	)


		DECLARE     @TT1 TABLE 
	(
		tranno INT,
		Account     VARCHAR(40),
		type    varchar(10),
		RecvPay     INT,
		TotActCnt	INT,
		StdDev      MONEY,    --AvgCount	INT,
		TotTranAmt  MONEY,
		AvgAmount	MONEY,
		BaseAmt     MONEY
	)



		DECLARE     @stdTT TABLE 
	(
		Account     VARCHAR(40),
		type    varchar(10),
		RecvPay     INT,
		AvgTranamt	float,
		StdDevamt   float,    --AvgCount	INT,
		stdavgAmt  MONEY
	)
	
	SET NOCOUNT ON
	SET @stat = 0

	DECLARE @StartDate DATETIME
	--set @StartDate =GETDATE()

	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam

	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE, @StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode

	SELECT @MINDATE = dbo.FirstDayOfMonth(DATEADD(MONTH, -1,@StartDate))

	SET @MinDate = dbo.FirstDayOfMonth(DATEADD(MONTH, -1,@StartDate))
	SET @MaxDate = dbo.FirstDayOfMonth(@StartDate)

	IF @InclLastmth = 1
	BEGIN
		SET @MaxDateAvg = @MaxDate
		SET @MinDateAvg = dbo.ConvertSqlDateToInt(DATEADD(MM, @NumMonths * -1, 
							dbo.BSA_ConvertintToSqlDate(@MaxDate,'MM/DD/YYYY')))
	END
	ELSE
	BEGIN
		SET @MaxDateAvg = @MinDate
		SET @MinDateAvg = dbo.ConvertSqlDateToInt(DATEADD(MM, @NumMonths * -1, 
							dbo.BSA_ConvertintToSqlDate(@MinDate,'MM/DD/YYYY')))
	END

	 --- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */

	SET @trnCnt = @@TRANCOUNT     -- Save the current trancount
	IF @trnCnt = 0
	-- Transaction has not begun
		BEGIN TRAN USR_WireRjct
	ELSE
	-- Already in a transaction
		SAVE TRAN USR_WireRjct
	/* End standard stored procedure transaction header */

	IF (@RecvPay = 3)
		SET @RecvPay = NULL

	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)
	
	-- Get all the Activity Types that have not been exempted.
	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	-- Consider all transactions for accounts where the aggregate of transactions exceed 
	-- the minimum threshold amount (sepcified as parameter) and the transactions 
	-- count exceed the minimum transaction count (specified as parameter)
	set @tmp = power(10, @precision)

insert into @TT(tranno, cust, Account,type, RecvPay, bookdate, BaseAmt)
select tranno, Cust, Account, ach.type,RecvPay, BookDate, BaseAmt  
	from ActivityHist ACH(nolock) 

		INNER JOIN Account A ON ACH.Account = A.Id
		INNER JOIN @ActType Act ON ACH.Type = Act.Type
		WHERE	BookDate >= @MinDate AND BookDate < @MaxDate			
			AND 	RecvPay = ISNULL(@RecvPay, RecvPay)
			AND EXISTS (Select Id from Customer where Id = ACH.Cust  AND ((ISNULL(@CustTypeList, '') = '' OR  
							CHARINDEX(',' + LTRIM(RTRIM([type])) + ',', @CustTypeList ) > 0)))
			and BaseAmt >= @minAmount
			--20210702 add for use round , start 
			AND 1 = 
			Case When @UseRound = 1 and (BaseAmt % @tmp = 0) Then 1 
				When @UseRound = 0 Then 1
				When @UseRound = 1 and (BaseAmt % @tmp <> 0) Then 0
			Else 1 End
			--20210702 add for use round , end    

insert into @stdtt (account,type,recvpay,AvgTranamt,stddevamt,stdavgamt) 
		SELECT ACH.Account, ACH.type,RecvPay, avg(BaseAmt) avgTranAmt, STDEV(baseAmt) stdeamt, avg(BaseAmt) + @StdDev * STDEV(Baseamt) stddevavg 
												   
		FROM	ActivityHist ACH (NOLOCK)
			INNER JOIN @ActType Act ON ACH.Type = Act.Type
	WHERE EXISTS (SELECT Account FROM @TT T WHERE ACH.account = T.account and ach.cust=t.cust)
		AND EXISTS (Select Id from Customer where Id = ACH.Cust  AND ((ISNULL(@CustTypeList, '') = '' OR  
						CHARINDEX(',' + LTRIM(RTRIM([type])) + ',', @CustTypeList ) > 0)))
		AND  RecvPay = ISNULL(@RecvPay, RecvPay)
		AND BookDate >= @MinDateAvg AND BookDate < @MaxDateAvg 
		GROUP BY ACH.Account, ach.type,RecvPay
		having isnumeric(STDEV(baseAmt) ) =1
		--Mar.3.2022 add
		and STDEV(baseAmt) > 0
															  



	INSERT INTO @TT1 (tranno,Account,type, RecvPay,StdDev,AvgAmount,BaseAmt)
	SELECT t.tranno,A.Account,A.type,A.RecvPay,stddevamt ,AvgTranamt,BaseAmt from @stdtt A 
	join @TT T on T.Account = A.Account and T.RecvPay = A.RecvPay and t.type=a.type
			where T.BaseAmt > A.stdavgamt 



			
			
				--GROUP BY A.Account,A.RecvPay, T.BaseAmt

	--1 means test, 0 means no 
	IF @testAlert = 1 Or @WLType = 0
	BEGIN

		SELECT @STARTALRTDATE = GETDATE()

		INSERT INTO Alert ( WLCode, [Desc], Status, CreateDate, LastOper, LastModify, Cust, Account, IsTest) 
			SELECT	@WLCode, 'Account: ''' + Account + ''' had ' +
				CASE 
			WHEN RecvPay = 1 THEN  
				' incoming' 
				WHEN RecvPay = 2 THEN 
				' Outgoing' 
			 
				END 
				 + ' transaction tranno:' + CAST(tranno as varchar) + ''', amount $''' + CAST(BaseAmt AS VARCHAR) +
				' exceeds'+ cast(origactivity as varchar)  + ' past average amount:  ' + CAST(AvgAmount AS VARCHAR) 
				+ ''' plus its ''' ++ CAST(CAST(@StdDev AS decimal(5,2)) AS VARCHAR) +
				''' times of statistical standard deviation amount ''' + CAST(CAST(StdDev AS decimal(20,2)) AS VARCHAR) +
				''' from ' + CAST(@MinDateAvg AS VARCHAR) + ' to ' + CAST(@MaxDateAvg AS VARCHAR) , 0, 
				GETDATE(), NULL, NULL, 
				(select top 1 cust from AccountOwner ao where ao.Account = t1.Account), Account , @testAlert
			FROM	@TT1 t1
			inner join activitymap atm on t1.type=atm.primecode

		   

		SELECT @STAT = @@ERROR 

		IF @stat <> 0 GOTO EndOfProc
		SELECT @ENDALRTDATE = GETDATE()

		INSERT INTO SasActivity (ObjectType, ObjectID, TranNo)
		SELECT distinct 'Alert', AlertNo, T.TranNo
		FROM 	ActivityHist AH (NOLOCK) 
			INNER	JOIN  @TT T  ON T.Account = AH.Account 
												
			INNER	JOIN Alert Al ON Al.Account = AH.Account				
		WHERE	Al.WLCode = @WLCode
			AND AH.TranNo = T.tranno
			and al.[Desc] like '%' + cast(T.tranno as varchar) + '%'
			and al.CreateDate >= @STARTALRTDATE
			and al.CreateDate <= @ENDALRTDATE
														   
											  
		 
		SELECT @STAT = @@ERROR 
	    IF @STAT <> 0  GOTO ENDOFPROC             
	END 
	ELSE 
	IF @WLTYPE = 1 -- Create Case
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		--2017.10 mdy bookdate to firstdate of month by LN Request 
		--Select @BeginDate =  dbo.ConvertSqlDateToInt(GETDATE())  
		 Select @BeginDate =  dbo.ConvertSqlDateToInt(@StartDate)
		INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
		ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
		RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
		ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
		EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
		OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
		REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
		WLCode, WLDESC, CREATETIME )
			SELECT 	NULL, @BeginDate, 
			    (select top 1 cust from AccountOwner ao where ao.Account = t1.Account), 
			    Account,NULL, 'RULE', NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, 'Account: ''' + Account + ''' had ' +
				CASE 
				WHEN RecvPay = 1 THEN  
				' incoming' 
				WHEN RecvPay = 2 THEN 
				' Outgoing' 
				END 
				 + ' transaction tranno:' + CAST(tranno as varchar) + ''', amount $''' + CAST(BaseAmt AS VARCHAR) +
				' exceeds'+ cast(origactivity as varchar)  + ' past average amount:  ' + CAST(AvgAmount AS VARCHAR) 
				+ ''' plus its ''' ++ CAST(CAST(@StdDev AS decimal(5,2)) AS VARCHAR) +
				''' times of statistical standard deviation amount ''' + CAST(CAST(StdDev AS decimal(20,2)) AS VARCHAR) +
				''' from ' + CAST(@MinDateAvg AS VARCHAR) + ' to ' + CAST(@MaxDateAvg AS VARCHAR) , GETDATE() 
			FROM 	@TT1  t1
			inner join activitymap atm on t1.type=atm.primecode

		SELECT @STAT = @@ERROR 

		IF @stat <> 0 GOTO EndOfProc
		SELECT @ENDALRTDATE = GETDATE()			
		
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)  
			SELECT distinct 'SUSPACT', RecNo, T.TRANNO  
			FROM ActivityHist AH (NOLOCK) 
				INNER	JOIN  @TT T  ON T.Account = AH.Account 
												 
				INNER	JOIN SuspiciousActivity SA ON AH.Account = SA.Account 
														   
		WHERE	SA.WLCode = @WLCode
															  
			AND AH.TranNo = T.tranno
			and SA.WLDESC like '%' + cast(T.tranno as varchar) + '%'
			and sa.CreateTime >= @STARTALRTDATE
			and sa.CreateTime <= @ENDALRTDATE
		   
		SELECT @STAT = @@ERROR 
		IF @STAT <> 0  GOTO ENDOFPROC
	END
EndOfProc:
IF (@stat <> 0) 
BEGIN 
	ROLLBACK TRAN USR_WireRjct
	RETURN @stat
END   

IF @trnCnt = 0
	COMMIT TRAN USR_WireRjct
	RETURN @stat


GO


