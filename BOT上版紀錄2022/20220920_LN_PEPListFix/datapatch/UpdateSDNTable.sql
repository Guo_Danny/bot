use ofac

GO

--check live list
select distinct ListType from SDNTable(nolock) where Status != 4

select count(*) from SDNTable(nolock) where ListType in (select Code from ListType(nolock) where Enabled = 0)

--update status = 4
begin tran
update SDNTable set Status = 4, LastModifDate = GETDATE()
from SDNTable(nolock) 
where ListType in (select Code from ListType(nolock) where Enabled = 0)
and Status != 4

commit

--disable build image button if need
update SDNDbStatus set MemImgBuildReq = 0