use PBSA
GO

declare @para_h varchar(500), @para_t varchar(5000), @para varchar(5000), @para_new varchar(5000);

select @para = Params from Watchlist where WLCode in ('idailytrn')

set @para_new = REPLACE(@para,'</Params>', '<Param Name="@PercMonthlyAmt" Alias="Minimum percentage of monthly income." Value="90" DataType="INT" /></Params>')

update Watchlist set Params = @para_new, LastOper = 'primeadmin' where WLCode = 'idailytrn'

select @para = Params from Watchlist where WLCode in ('odailytrn')

set @para_new = REPLACE(@para,'</Params>', '<Param Name="@PercMonthlyAmt" Alias="Minimum percentage of monthly income." Value="90" DataType="INT" /></Params>')

update Watchlist set Params = @para_new, LastOper = 'primeadmin' where WLCode = 'odailytrn'

