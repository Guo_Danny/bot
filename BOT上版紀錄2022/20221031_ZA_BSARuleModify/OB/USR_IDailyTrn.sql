USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_IDailyTrn]    Script Date: 4/9/2021 6:15:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[USR_IDailyTrn] ( 
	@WLCode 	SCode,
	@TestAlert 	INT,
	@activityTypeList VARCHAR(8000), 
	@minimumAmount 	MONEY, 
	@maximumAmount 	MONEY,
	@RiskClassList 	VARCHAR(8000),
	@BranchList 	VARCHAR(8000),
	@deptList 	VARCHAR(8000),
	@CustTypeList   VARCHAR(8000),
	@PercMonthlyAmt	INT

)
AS
	/* RULE AND PARAMETER DESCRIPTION
	   Detects Customers with type between specified amount range
	   Designed to be ran Pre-EOD.
		@activityTypeList	List of Activity Types that will be considered. 
				   -ALL-, blank or NULL for all Types
		@minimumAmount	- Minimum individual transaction amount
		@maximumAmount	- Maximum individual transaction amount. For no maximum use -1 
		@RiskClassList	- List of Risk Classes that will be considered. 
				   -ALL-, blank or NULL for all Risk Classes
		@BranchList	- List of Branches that will be considered. 
				   -ALL-, blank or NULL for all Branches
		@deptList	- List of Departments that will be considered. 
				   -ALL-, blank or NULL for all Departments
		@CustTypeList - List of Customer Types that will be considered. 
				   -ALL-, blank or NULL for all Customer Types

	*/
		
	/*  Declarations */
	DECLARE	@description 	VARCHAR(2000),
		@desc 		VARCHAR(2000),
		@Id 		INT, 
		@WLType 	INT,
		@stat 		INT,
		@trnCnt 	INT
	DECLARE @STARTALRTDATE  DATETIME
	DECLARE @ENDALRTDATE    DATETIME
	/*Oct.27, 2022 modify daily*/
	declare @mindate datetime, @maxdate datetime
	
	/*Oct.27, 2022 add TRANNO*/
	DECLARE	@TT TABLE (
		Cust 	VARCHAR(40),
		Recvpay 	INT,
		TotalAmt 	MONEY,
		BOOKDATE 	INT,
		TRANNO		INT
	)

	DECLARE	@CTT TABLE (
		Cust 	VARCHAR(40),
		TotalRecAmt	MONEY,
		TotalPayAmt	MONEY,
		TotalAmt 	MONEY,
		BOOKDATE 	INT
	)
	-- Temporary table of Activity Types that have not been specIFied as Exempt
	DECLARE @ActType TABLE (
		Type	INT
	)
	
	SET NOCOUNT ON
	SET @stat = 0

	--20210107 add
	If @maximumAmount = -1
	Begin
		set @maximumAmount = 99999999999999;
	End	
	
	--- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */
	SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN USR_IDailyTrn
	ELSE
		-- Already in a transaction
		SAVE TRAN USR_IDailyTrn
	/* End standard stored procedure transaction header */
	
	/*  standard Rules Header */
	--Oct.27, 2022 modify
	SELECT @description = [Desc], @WLType = WLType,
	@maxdate = 
	Case 	
		When UseSysDate in (0,1) Then
			-- use System date
			GetDate()
		When UseSysDate = 2 Then
			-- use business date
			(Select BusDate From dbo.SysParam)
		Else
			GetDate()
	End 
	FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

	--Oct.27, 2022 modify
	set @mindate = @maxdate - 30

	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
	SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)
    SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)

	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)
	
	--Oct.27, 2022 change bookdate to Tranno
	INSERT INTO @TT(Cust, RecvPay, TotalAmt, TRANNO)
		SELECT 	Cust, RecvPay, baseAmt, TranNo
		FROM 	activity (NOLOCK)
		INNER	JOIN Customer ON activity.cust = customer.id 
		INNER	JOIN @ActType Act ON Act.Type = Activity.Type
		WHERE 	((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) AND
			    ((ISNULL(@BranchList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.OwnerBranch)) + ',', @BranchList ) > 0)) AND
			    ((ISNULL(@DeptList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.OwnerDept)) + ',', @DeptList ) > 0))   AND	
				((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) 
        AND ISNUMERIC(Cust)>0
		and baseAmt >= @minimumAmount and baseAmt <= @maximumAmount

	--Oct.27, 2022 add activityhist
	INSERT INTO @TT(Cust, RecvPay, TotalAmt, TRANNO)
		SELECT 	Cust, RecvPay, baseAmt, TranNo
		FROM 	activityhist (NOLOCK) ach
		INNER	JOIN Customer ON ach.cust = customer.id 
		INNER	JOIN @ActType Act ON Act.Type = ach.Type
		WHERE 	((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) AND
			    ((ISNULL(@BranchList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.OwnerBranch)) + ',', @BranchList ) > 0)) AND
			    ((ISNULL(@DeptList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.OwnerDept)) + ',', @DeptList ) > 0))   AND	
				((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) 
        AND ISNUMERIC(Cust)>0
		and baseAmt >= @minimumAmount and baseAmt <= @maximumAmount
		and bookdate >= dbo.ConvertSqlDateToInt(@mindate) and bookdate < dbo.ConvertSqlDateToInt(@maxdate)
		and exists (select cust from @TT t where t.Cust = ach.Cust)

	Insert into @CTT(cust, TotalAmt, TotalRecAmt, TotalPayAmt)
	select cust, sum(TotalAmt), sum(recamt), sum(payamt) from (
	select cust, TotalAmt, 
	case when Recvpay = 1 then TotalAmt else 0 end recamt,
	case when Recvpay = 2 then TotalAmt else 0 end payamt
	from @TT) T
	join KYCData k on T.Cust = k.CustomerId
	where (k.MOMonthlyAmt > 0 or k.TCMonthlyAmt > 0)
	group by cust, k.MOMonthlyAmt, k.TCMonthlyAmt
	having (
	(sum(recamt) >= (@PercMonthlyAmt * k.MOMonthlyAmt)/100 and sum(recamt) >= (@PercMonthlyAmt * k.TCMonthlyAmt)/100)
	or
	(sum(payamt) >= (@PercMonthlyAmt * k.MOMonthlyAmt)/100 and sum(payamt) >= (@PercMonthlyAmt * k.TCMonthlyAmt)/100)
	)
	--Nov.2, 2022 according to the data structure, individual customers have MOMonthlyAmy but TCMonthlyAmt is 0 
	--and entity customers have TCMonthlyAmt but MOMonthlyAmy is 0.
	--So the sum amt must over both values.

	--1 means test, 0 means no
	IF @testAlert = 1 
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT, IsTest) 
			SELECT distinct @WLCode, case when len(cust)>0 Then 'Customer: ''' + Cust + ''' had ''$' 
				+ CONVERT(VARCHAR, TotalRecAmt) + ''' of incoming types specified, and ''$' 
				+ CONVERT(VARCHAR, TotalPayAmt) + ''' of outgoing types specified in the last 30 days ' 
				+ 'which over than ' + CONVERT(VARCHAR,@PercMonthlyAmt) + ' percent of this customer monthly amount: ' 
				+ CONVERT(VARCHAR,(case when k.MOMonthlyAmt > 0 then k.MOMonthlyAmt else k.TCMonthlyAmt end)) end, 0, 
				GETDATE(), NULL, NULL, Cust, NULL, 1 
		  	FROM 	@CTT c
			join KYCData k on c.Cust = k.CustomerId
		SELECT @STAT = @@ERROR	
		SELECT @ENDALRTDATE = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
		
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT distinct	'Alert', AlertNO, TRANNO 
			FROM 	@TT a
			INNER	JOIN @CTT T ON a.Cust = t.Cust
			INNER	JOIN Alert ON A.Cust = Alert.Cust
			where	Alert.WLCode = @WLCode 
			AND	Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		
		SELECT @STAT = @@ERROR 
		IF @STAT <> 0  GOTO ENDOFPROC
	END 
	ELSE 
	BEGIN 
		IF @WLTYPE = 0 
		BEGIN
			SELECT @STARTALRTDATE = GETDATE()
			INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT) 
		 		SELECT distinct @WLCode, case when len(cust)>0 Then 'Customer: ''' + Cust + ''' had ''$' 
				+ CONVERT(VARCHAR, TotalRecAmt) + ''' of incoming types specified, and ''$' 
				+ CONVERT(VARCHAR, TotalPayAmt) + ''' of outgoing types specified in the last 30 days ' 
				+ 'which over than ' + CONVERT(VARCHAR,@PercMonthlyAmt) + ' percent of this customer monthly amount: ' 
				+ CONVERT(VARCHAR,(case when k.MOMonthlyAmt > 0 then k.MOMonthlyAmt else k.TCMonthlyAmt end)) end, 0, 
				GETDATE(), NULL, NULL, Cust, NULL 
		  	FROM @CTT c
			join KYCData k on c.Cust = k.CustomerId
			SELECT @STAT = @@ERROR
			SELECT @ENDALRTDATE = GETDATE()
			IF @STAT <> 0  GOTO ENDOFPROC
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
			SELECT @STARTALRTDATE = GETDATE()
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
				ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
				RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
				ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
				EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
				OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
				REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
				WLCode, WLDESC, CREATETIME )
			SELECT distinct	NULL, dbo.ConvertSqlDateToInt(GETDATE()), Cust, NULL,
				NULL, 'RULE', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, case when len(cust)>0 Then 'Customer: ''' + Cust + ''' had ''$' 
				+ CONVERT(VARCHAR, TotalRecAmt) + ''' of incoming types specified, and ''$' 
				+ CONVERT(VARCHAR, TotalPayAmt) + ''' of outgoing types specified in the last 30 days ' 
				+ 'which over than ' + CONVERT(VARCHAR,@PercMonthlyAmt) + ' percent of this customer monthly amount: ' 
				+ CONVERT(VARCHAR,(case when k.MOMonthlyAmt > 0 then k.MOMonthlyAmt else k.TCMonthlyAmt end)) end, GETDATE() 
			FROM 	@CTT c
			join KYCData k on c.Cust = k.CustomerId
			SELECT @STAT = @@ERROR	
			SELECT @ENDALRTDATE = GETDATE()
			IF @STAT <> 0  GOTO ENDOFPROC
		END
		
		IF @WLTYPE = 0 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   		SELECT distinct	'Alert', AlertNO, TRANNO 
				FROM 	@TT a
				INNER	JOIN @CTT T ON a.Cust = t.Cust
				INNER	JOIN Alert ON A.Cust = Alert.Cust
				where	Alert.WLCode = @WLCode 
				AND	Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		
		SELECT @STAT = @@ERROR 
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   	SELECT distinct	'SUSPACT', RECNO, TRANNO 
			FROM 	@TT A 
			INNER	JOIN @CTT T ON A.Cust = T.Cust
			INNER	JOIN SUSPICIOUSACTIVITY S ON A.Cust = S.Cust
			WHERE	S.WLCode = @WLCode 
			AND		S.CREATETIME BETWEEN @STARTALRTDATE AND @ENDALRTDATE
		SELECT @STAT = @@ERROR 
		END
	
	END 
	
	EndOfProc:
	IF (@stat <> 0) BEGIN 
	  ROLLBACK TRAN USR_IDailyTrn
	  RETURN @stat
	END	
	
	IF @trnCnt = 0
	  COMMIT TRAN USR_IDailyTrn
	RETURN @stat
	
GO


