
set nocount on
declare @sTime datetime, @eTime datetime;
select @eTime=getdate(), @sTime=DATEADD(hour,-20,GETDATE()) 

select  ca.tranno, ca.ref, ca.cust, ca.account, map.origactivity,left(ca.instructions,3),ca.fxamt,ca.valuedate,ca.bookdate,
case when ca.recvpay = 1 then 'RECEIVE' else 'PAY' end as Direction , case  when 
cashtran =1 then 'CASH' else 'TRANSFER' end as Transfer, relatedref

from PBSA..[CurrentArchActivity] ca
inner join PBSA..activitymap map on ca.type=map.primecode
where trandate >= @stime and  trandate < @eTime

set nocount off  

