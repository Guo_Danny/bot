
set nocount on 
declare @sTime datetime, @eTime datetime;

select @eTime=getdate(), @sTime=DATEADD(hour,-20,GETDATE()) 
--select @sTime StartTime, @eTime EndTime

declare @activityfilecount int
select @activityfilecount = (
select count(1) from PBSA..Event where trntime >= @sTime
--and objectType='Task' 
and evtdetail like '%total additions%' and evtdetail like '%total errors during add:%')

if @activityfilecount = 0
select 'No New File'

