use OFAC
go

set nocount on

select CONCAT('script start:',getdate())

Create table #TT
(
	UID VARCHAR(200)
)

Create table #HEAD
(
	header VARCHAR(200)
)

declare @datapath varchar(500);
declare @sql varchar(max);

set @datapath = CONCAT('E:\prime\botosb\Dat\OFAC\AML_Sharing_List_Del_', $(input), '.d');

set @sql = 'BULK INSERT #TT FROM ''' + @datapath + ''' WITH(BATCHSIZE=50000,CHECK_CONSTRAINTS,CODEPAGE=''RAW'',DATAFILETYPE=''char'',FIRSTROW=1)'

EXEC (@sql)

declare @headpath varchar(500)
declare @sqlh varchar(max);

set @headpath = CONCAT('E:\prime\botosb\Dat\OFAC\AML_Sharing_List_Del_', $(input), '.h');

set @sqlh = 'BULK INSERT #HEAD FROM ''' + @headpath + ''' WITH(BATCHSIZE=50000,CHECK_CONSTRAINTS,CODEPAGE=''RAW'',DATAFILETYPE=''char'',FIRSTROW=1)'

EXEC (@sqlh)
go

--chk count
declare @cnt_h int, @cnt_d int;
declare @datapath varchar(500);
declare @chkcount int, @oper varchar(200), @logtext varchar(500);

select @cnt_d = count(UID) from #TT

select @cnt_h = convert(int,SUBSTRING(header, 55,10)) from #HEAD

select CONCAT('Head count:',@cnt_h,', Data count:',@cnt_d);

if @cnt_h = @cnt_d
begin

	update SDNTable set Status = 4, LastModifDate = getdate(), LastOper = 'Primeadmin', Remarks = CONCAT('Deleted from ',@datapath)
	from #TT t
	join sdntable(nolock) sdn on t.uid = sdn.listid
	where sdn.listtype like 'HQ%'


	update sdndbstatus set MemImgBuildReq = 0
	/*
	declare cur_oper CURSOR FOR
	select Oper from PSEC..OperRights
	where [Right] = 'CTRLCLIEN'

	open cur_oper;
	fetch next from cur_oper into @oper;
	while @@fetch_status = 0 
	begin
		set @logtext = 'HQ Sharing List Deleted process complete, Insert and Update processes may delay about 10 mins, for more detail please check ZXSDNDif report.'
		insert into PBSA..Notification (Subsystem,ObjectType, ObjectId,NotifyTime,SnoozeTime,NotifyOper,NotifyDescr,NotificationType,RecurType,RecurExpireDate,NextRecurDate)values
		('HQ Sharing List','HQShareList','HQShareList',GETDATE(), getdate(),@oper,@logtext,'HQShareList',0,null,null);
		
		fetch next from cur_oper into @oper;
	end
	*/
end
select CONCAT('script end:',getdate())

drop table #TT
drop table #HEAD

go

