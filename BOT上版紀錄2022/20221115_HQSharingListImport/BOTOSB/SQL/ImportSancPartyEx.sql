use PBSA
go

	select getdate(),'HQ Sharing List import process complete.' 

	declare @logtext varchar(500),@oper varchar(50);
	declare cur_oper CURSOR FOR
	select Oper from PSEC..OperRights
	where [Right] = 'CTRLCLIEN'

	open cur_oper;
	fetch next from cur_oper into @oper;
	while @@fetch_status = 0 
	begin
		set @logtext = 'HQ Sharing List import process complete, for more detail please check ZXSDNDif report.'
		insert into PBSA..Notification (Subsystem,ObjectType, ObjectId,NotifyTime,SnoozeTime,NotifyOper,NotifyDescr,NotificationType,RecurType,RecurExpireDate,NextRecurDate)values
		('HQ Sharing List','HQShareList','HQShareList',GETDATE(), getdate(),@oper,@logtext,'HQShareList',0,null,null);
		
		fetch next from cur_oper into @oper;
	end
	
	select ''
go

