use pbsa

go

declare @chkcount int;

select @chkcount = count(Code) from pbsa..NotificationType where Code = 'HQShareList'

if @chkcount = 0
	insert into pbsa..NotificationType(code, name, CreateOper, CreateDate) values('HQShareList','HQ Sharing List', 'Primeadmin', getdate())
