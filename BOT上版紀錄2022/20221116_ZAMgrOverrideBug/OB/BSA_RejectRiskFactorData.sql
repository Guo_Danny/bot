USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_RejectRiskFactorData]    Script Date: 4/27/2021 5:13:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[BSA_RejectRiskFactorData]( 
				@Cust OBJECTID,
				@RiskClass SCODE, 
				@Reason VARCHAR(200),
				@Oper SCODE
				)
  AS

	-- Start standard stored procedure transaction header

	DECLARE @trnCnt int
	SELECT @trnCnt = @@trancount	-- Save the current trancount

	
	IF @trnCnt = 0
		BEGIN TRAN BSA_RejectRiskFactorData  -- Transaction has not begun
	ELSE	
		SAVE TRAN BSA_RejectRiskFactorData    -- Already in a transaction

	-- End standard stored procedure transaction header
  
	DECLARE	@stat 	INT,
			@cnt 	INT,
			@dt		DateTime,
			@txt	VARCHAR(6000),
			@logtxt VARCHAR(6000),
			@oldRiskClass SCode,
			@oldLastModify SCode,
			@LastReview	DateTime

 --20200828 ZA REQUEST REASON TO CUSTOMER NOTES, START
 declare @strNotes varchar(6000), @tmpNotes varchar(6000)
 declare @Mint int , @iNewCust int, @RvDt Date, @IssueCty  varchar(100)
 --DECLARE @LOGTXT VARCHAR(6000), @TXT VARCHAR(6000)
 --20200828 ZA REQUEST REASON TO CUSTOMER NOTES, END

	IF EXISTS (SELECT 1 FROM RiskFactorData with (nolock) WHERE cust = @Cust AND Status = 0)
		BEGIN 
			
			set @dt = GetDate()

			--Set Last Review time after customer's LastModify time
			set @LastReview = DateAdd(s, 10, @dt)

			If dbo.BSA_CountMissingScores(@Cust) > 0
			Begin
			   Select @stat = 250010   -- DB_INV_PARAM
			   GoTo EndOfProc
		    End

			/*
				 Delete Old accepted scores other than the ones
				 recently accepted, for this customer
			*/
			Delete RiskFactorData
					Where Cust = @Cust And Status = 1

			SELECT @oldRiskClass = RiskClass FROM RiskScoreHist with (nolock)
					WHERE CustomerId = @Cust AND DateAccepted IS NULL
			
			--update proposed record from the Risk Score History	
			--update 2020.07 update for ZA override policy start
			UPDATE RiskScoreHist SET RiskClass = @RiskClass ,
										RiskScore = NULL, 
										AcceptOper = @Oper,
										--DateAccepted = @LastReview
										DateAccepted =NULL
										WHERE CustomerId = @Cust 
										AND DateAccepted IS NULL

			--Update RiskFactorData SET  Status = 1,		-- Reviewed/Accepted
			--							LastOper = @Oper,
			--							LastReview = @LastReview,
			--							LastModify = @LastReview
			--							WHERE Cust = @Cust AND Status = 0
			
			--2020.07 ZA asked to trigger the risk rating code value, if they override the risk class.

        ----20201210 manager override add onboard input field of last reveiw date(cdduser38) and Proposed RisK(CDDUser39), Start
		--reference Store procedure : BSA_GenerateRFData, so mask under
		
		-- get riskclass of Probation days, RiskFactor is new cust
		--for South Africa Request: new cust lastreview must be 12 months,start			
		select @iNewCust=0
		select @iNewCust=1 from Customer(nolock) where id=@Cust and ProbationStartDate is null and datediff(day,KYCDataCreateDate,getdate()) <10 
		if @iNewCust=1
		   select @Mint=RiskFactor from RiskClass where code=@RiskClass
		else 
		   select @Mint=probationdays from RiskClass where code=@RiskClass
		--for South Africa Request: new cust lastreview must be 12 months,end
		
		--for partyid exist and expirdate is closed, start
		select @RvDt='', @IssueCty=''
		select @RvDt=convert(date,min(ExpiryDate),101) from PartyID (nolock)
		       where CustomerId=@Cust 
			   and ExpiryDate>=convert(date,GETDATE(),101)
			   and ExpiryDate<convert(date,dateadd(MONTH,@Mint,GETDATE()),101)
		
		----20201127 if issue contry= ZA, TW then expirydate=29991231
		----select @IssueCty=IssueCountry from PartyID 
		----       where CustomerId=@Cust and IssueCountry in ('TW','ZA')
		----	   group by IssueCountry
		----if ( @RvDt ='' or @RvDt is null ) or (@IssueCty='ZA' or @IssueCty='TW' or @IssueCty='' or @IssueCty is null)   
		
		----20201214 mask override did not change @RvDt:  -- if ( @RvDt ='' or @RvDt is null ) 
		if isnull(@RvDt,'')='' or @Rvdt > convert(date,convert(char(10),dateadd(MONTH,@Mint,GETDATE()),101),101)
		   select @RvDt=convert(date,convert(char(10),dateadd(MONTH,@Mint,GETDATE()),101),101)
  		--for partyid exist and expirdate is closed, end			
        
		--20210201 override control update item is greater proposed risk, Start
			Declare @strCode varchar(50),@strInCodes varchar(5000), @strComm varchar(10)
			select @strcode='', @strInCodes=''
			declare cur CURSOR FOR
			select Code + ',' FROM RiskClass WHERE RFScoreHi > (select RFScoreHi from Riskclass where code = @oldRiskClass )
			order by RFScoreHi

			open cur;
			fetch next from cur into @strCode;
			WHILE @@fetch_status = 0 
			begin
				set @strInCodes = @strInCodes + @strCode;
				fetch next from cur into @strCode;
			end

			--select @StrIncodes
			close cur;
			DEALLOCATE cur;

        IF CHARINDEX(',',@strInCodes) > 0 and len(@strInCodes)>1 and CHARINDEX(@RiskClass, @strInCodes)>0
		  BEGIN
		    update KYCData set CDDUser25 = '10',IsPersonalRelationship=1,CDDUser38=convert(varchar(20),@RvDt,101), CDDuser39=@RiskClass where CustomerId = @Cust
          END
		ELSE
	      BEGIN
            ROLLBACK TRAN BSA_RejectRiskFactorData
			
	        SELECT @logtxt = 'UnSuccessfully: Risk Class Overridden'
	        SELECT @txt	= 'UnSuccessfully: RiskClass Proposed: ' + CONVERT(VARCHAR,ISNULL(@oldRiskClass,'NULL')) + '| RiskClass Overridden: ' + CONVERT(VARCHAR,ISNULL(@RiskClass,'NULL'))+
				  ' | Reason: ' + @Reason 
	        EXEC BSA_InsEvent @Oper, 'Mod', 'Cust', @Cust,@logtxt, @txt

			Select @stat = 250001   -- concurent update
			RETURN @stat		   
		  END
	   
	    ----update KYCData set CDDUser25 = '10',IsPersonalRelationship=1,CDDUser38=convert(varchar(20),@RvDt,101), CDDuser39=@RiskClass where CustomerId = @Cust  
	    ----20201210 manager override add onboard input field of last reveiw date(cdduser38) and Proposed RisK(CDDUser39), End

		--20210201 override control update item is greater proposed risk, End	
			
			--update 2020.07 update for ZA override policy End

			SELECT @stat = @@error, @cnt = @@rowcount
					IF @stat <> 0
						BEGIN
							GOTO EndOfProc
						END
					ELSE IF @cnt = 0 
						BEGIN
							SELECT @stat = 250006	-- DB_UNKNOWN
							GOTO EndOfProc
					END

			/**************************************************
				Assign the RiskClass to the Customer
				after modifying RiskFactorData to avoid
				conflicts with locked risk class actions
			**************************************************/
		    --20200513 add get riskclass of Probation days,begin
			/*
		    select @Mint=probationdays from RiskClass where code=@RiskClass
		    --20200513 add get riskclass of Probation days,end
			
			--20200514 add for customer update lastreview, reviewdate
			UPDATE Customer SET RiskClass = @RiskClass, 
								LastOper = @Oper, 
								LastModify = @dt, LastReview=@dt, ReviewDate=dateadd(Month,@Mint,@dt)								
								WHERE [ID] = @Cust
			
			--Check if customer was updated
			SELECT @stat = @@error, @cnt = @@rowcount
					IF @stat <> 0
						BEGIN
							GOTO EndOfProc
						END
					ELSE IF @cnt = 0 
						BEGIN
							SELECT @stat = 250006    -- DB_UNKNOWN
							GOTO EndOfProc --exit
						End
			*/

		END
	ELSE
		BEGIN
			SELECT @stat = 250006	-- Object does not exist -- DB_UNKNOWN
		END

EndOfProc:


	IF ( @stat = 0 and @cnt = 0 )
		SELECT @stat = 250001 -- concurent update
	
	IF ( @stat <> 0 ) 
		BEGIN 
			ROLLBACK TRAN BSA_RejectRiskFactorData
			RETURN @stat
		END	


	SELECT @logtxt = 'Risk Class Overridden'
	SELECT @txt	= 'RiskClass Proposed: ' + CONVERT(VARCHAR,ISNULL(@oldRiskClass,'NULL')) + '| RiskClass Overridden: ' + CONVERT(VARCHAR,ISNULL(@RiskClass,'NULL'))+
				  ' | Reason: ' + @Reason 

	EXEC BSA_InsEvent @Oper, 'Mod', 'Cust', @Cust,@logtxt, @txt

	IF @trnCnt = 0
	BEGIN
		COMMIT TRAN BSA_RejectRiskFactorData
    END

--20200828 ZA REQUEST REASON APPEND TO CUSTOMER NOTES , START
 Select @tmpNotes='', @strNotes=''
 Select @tmpNotes = Notes from Customer where Id=@Cust
 SELECT @strNotes=@tmpNotes  + char(13) + @Oper + ', ' + CONVERT(VARCHAR(20),GETDATE(),111) + ' ' + left(CONVERT(VARCHAR(20),GETDATE(),114),8) + ' , ' + @LOGTXT + ' : ' + @TXT 
 UPDATE Customer SET Notes = @strNotes where Id=@Cust
--20200828 ZA REQUEST REASON APPEND TO CUSTOMER NOTES , END

	RETURN @stat
GO


