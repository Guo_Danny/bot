use pbsa
go

declare @wlcode varchar(50), @baseamt int, @actType varchar(250),@sql varchar(max);
--以下參數為測試套設定之參數，上線執行前請修改為正式套之參數
set @wlcode = '210U1MnthBY'
set @baseamt = '60000'
set @actType = '2017,2027,2037,2047,2117,2127,2137,2147,2157,2167,2217,2227,2607'

--[dbo].[SAsActivity]
--在insert之前，先用select語法確認資料是否正確，確認無誤再將insert前的註解拿掉執行insert語法
set @sql = '
--insert into SAsActivity(ObjectType, ObjectID, TranNo, Included)
select ''SUSPACT'', T.RecNo, caa.TranNo, ''1'' from (
select RecNo, sa.Cust,sa.BookDate,
dbo.ConvertSqlDateToInt(dateadd(day, -1, convert(date,dbo.BSA_ConvertIntToShortDate(sa.bookdate)))) maxdate,
dbo.ConvertSqlDateToInt(dateadd(day, -31, convert(date,dbo.BSA_ConvertIntToShortDate(sa.bookdate)))) mindate
from SuspiciousActivity(nolock) sa
where WLCode = ''' + @wlcode + '''
and sa.wldesc is null
) T
join CurrentArchActivity(nolock) caa on T.Cust = caa.Cust and caa.BookDate >= T.mindate
 and caa.BookDate < T.maxdate
where caa.BaseAmt >=  ' + convert(varchar,@baseamt) + '
and caa.type in (' + @actType + ')
group by RecvPay, T.RecNo, caa.TranNo'

EXEC (@sql)

--[dbo].[SuspiciousActivity]
set @sql = '
select * from (
select T.RecNo, T.Cust, case when caa.RecvPay = ''1'' then ''incoming'' else ''outgoing'' end typ,sum(caa.BaseAmt) totalamt,max(caa.BookDate) maxdt, min(caa.BookDate) mindt,
''Customer: '' + convert(varchar,T.Cust) + '' had transactions totaling '' + convert(varchar,sum(caa.BaseAmt)) + 
'' for the Period from '' + convert(varchar,min(caa.BookDate)) + '' to '' + convert(varchar,max(caa.BookDate)) + '' of '' + case when caa.RecvPay = ''1'' then ''incoming'' else ''outgoing'' end + '' types specified '' wldesc
from (
select RecNo, sa.Cust,sa.BookDate,
dbo.ConvertSqlDateToInt(dateadd(day, -1, convert(date,dbo.BSA_ConvertIntToShortDate(sa.bookdate)))) maxdate,
dbo.ConvertSqlDateToInt(dateadd(day, -31, convert(date,dbo.BSA_ConvertIntToShortDate(sa.bookdate)))) mindate
from SuspiciousActivity(nolock) sa
where WLCode = ''' + @wlcode + '''
and sa.wldesc is null
) T
join CurrentArchActivity(nolock) caa on T.Cust = caa.Cust and caa.BookDate >= T.mindate
 and caa.BookDate < T.maxdate
where caa.BaseAmt >= ' + convert(varchar,@baseamt) + '
and caa.type in (' + @actType + ')
group by T.RecNo, T.Cust, RecvPay
) T2'

EXEC (@sql)


--執行以下update語法之前，請先select確認上面這段select資料是否正確
set @sql = '
update SuspiciousActivity set WLDesc = T2.wldesc
from (
select T.RecNo, T.Cust, case when caa.RecvPay = ''1'' then ''incoming'' else ''outgoing'' end typ,sum(caa.BaseAmt) totalamt,max(caa.BookDate) maxdt, min(caa.BookDate) mindt,
''Customer: '''''' + convert(varchar,T.Cust) + '''''' had transactions totaling '''''' + convert(varchar,sum(caa.BaseAmt)) + 
'''''' for the Period from '''''' + convert(varchar,min(caa.BookDate)) + '''''' to '''''' + convert(varchar,max(caa.BookDate)) + '''''' of '' + case when caa.RecvPay = ''1'' then ''incoming'' else ''outgoing'' end + '' types specified '' wldesc
from (
select RecNo, sa.Cust,sa.BookDate,
dbo.ConvertSqlDateToInt(dateadd(day, -1, convert(date,dbo.BSA_ConvertIntToShortDate(sa.bookdate)))) maxdate,
dbo.ConvertSqlDateToInt(dateadd(day, -31, convert(date,dbo.BSA_ConvertIntToShortDate(sa.bookdate)))) mindate
from SuspiciousActivity(nolock) sa
where WLCode = ''' + @wlcode + '''
and sa.wldesc is null
) T
join CurrentArchActivity(nolock) caa on T.Cust = caa.Cust and caa.BookDate >= T.mindate
 and caa.BookDate < T.maxdate
where caa.BaseAmt >= ' + convert(varchar,@baseamt) + '
and caa.type in (' + @actType + ')
group by T.RecNo, T.Cust, RecvPay
) T2 
join SuspiciousActivity(nolock) sa on T2.Cust = sa.Cust and T2.RecNo = sa.RecNo'

begin tran
EXEC (@sql)

--確認無誤再執行commit
--commit

