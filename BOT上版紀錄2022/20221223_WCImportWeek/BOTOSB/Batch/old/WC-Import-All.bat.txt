REM --sftp get world-check full data 
TASKKILL /F /FI "IMAGENAME EQ WINSCP.EXE" 
c:
cd\Program Files (x86)\WinSCP
"C:\Program Files (x86)\WinSCP\WinSCP.exe" /script="D:\Prime\BOTOSB\BATCH\WC-Import-All.txt"

d:
cd\worldcheck

7z e -y d:\worldcheck\premium-world-check.csv.gz

COPY /Y d:\worldcheck\premium-world-check.csv \\OSBAMLXXDBV\worldcheck\premium-world-check.csv
COPY /Y d:\worldcheck\world-check-deleted-v2.csv \\OSBAMLXXDBV\worldcheck\world-check-deleted-v2.csv

set hour=%time:~0,2%
if "%hour:~0,1%" == " " set hour=0%hour:~1,1%

COPY d:\worldcheck\premium-world-check.csv.gz  d:\worldcheck\backup\premium-world-check.csv.gz.%DATE:~7,2%

COPY d:\worldcheck\world-check-deleted-v2.csv  d:\worldcheck\backup\world-check-deleted-v2.csv.%DATE:~7,2%.%hour%

del /F d:\worldcheck\premium-world-check.csv

del /F d:\worldcheck\premium-world-check.csv.gz

del /F d:\worldcheck\world-check-deleted-v2.csv

cd\prime\botosb\batch

REM -- bulk insert to WorldCheck_TMP & increasing insert to WorldCheck
SQLCMD  -S OSBAMLXXDBV -E -i "D:\Prime\BOTOSB\SQL\WorldCheckImprtAll.SQL" >> "D:\Prime\BOTOSB\BatchLog\WC-Imprt-All.SQL.txt"
TIMEOUT /NOBREAK /T 10

REM --REBUILD INDEX
SQLCMD  -S OSBAMLXXDBV -E -i "D:\Prime\BOTOSB\SQL\WC-Rebuild-Index-All.SQL" >> "D:\Prime\BOTOSB\BatchLog\WC-Rebuild-Index-All.SQL.txt"


:End
exit