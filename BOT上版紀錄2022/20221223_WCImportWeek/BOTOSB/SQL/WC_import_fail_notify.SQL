﻿use OFAC
go

declare @SDate varchar(20), @logtext varchar(255), @logtext1 varchar(255), @oper varchar(255), @cont int, @StrDate datetime
select @Sdate=convert( varchar(10), getdate() -1,112);
set @logtext = 'WorldCheck Import Job Incomplete, Please contact Taipei center PRIME support team to fix import failed.'
--select @SDate;

    select @cont = count(*) from SDNChangeLog
	where logdate >= @Sdate
	and logtext like '%Complete WorldCheck Job%'

	--check sdn list create date and modify date
declare @mxDate1 datetime,@mxDate2 datetime
Select @mxDate1=max(isnull(listmodifdate,'')) from OFAC.dbo.SDNTable(nolock) where ListType not in ('USER','WPEP','HQ Sharing List') 
Select @mxDate2=max(ListCreatedate) from OFAC.dbo.SDNTable(nolock) where ListType not in ('USER','WPEP','HQ Sharing List') 

if @mxDate1 > @mxDate2
   SELECT @StrDate=@mxDate1
else
   SELECT @StrDate=@mxDate2

select @StrDate=convert(char(10),@strDate,111) + ' 00:00:00'

declare cur_oper CURSOR FOR

select Oper from psec..RoleMembers where [Role] like '%CASEAP%'

open cur_oper;
fetch next from cur_oper into @oper;
while @@fetch_status = 0 
begin

    --select @logtext,@oper;
    if @cont < 3
	begin
		--select @logtext,@oper;
		insert into PBSA..Notification (Subsystem,ObjectType, ObjectId,NotifyTime,SnoozeTime,NotifyOper,NotifyDescr,NotificationType,RecurType,RecurExpireDate,NextRecurDate)values
		('WorldCheck Import Fail','WCIMPORT','WCIMPORT',GETDATE(), getdate(),@oper,@logtext,'WCIMPORT',0,null,null);

		insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
		values (getdate(),'Prime',@logtext,'Ann','Event','Notifications',@oper);
	end

	--check sdn list create date and modify date
	if DATEDIFF(DAY,@StrDate,getdate()) >1
	begin
		select @logtext1 = concat('WorldCheck Dailay Import List incomplete, it is not insert into SDNTABLE on ' + convert(varchar(20),getdate(),111),'. Please contact Taipei center PRIME support team to fix import failed.')

		insert into PBSA..Notification (Subsystem,ObjectType, ObjectId,NotifyTime,SnoozeTime,NotifyOper,NotifyDescr,NotificationType,RecurType,RecurExpireDate,NextRecurDate)values
		('WorldCheck Import Fail','WCIMPORT','WCIMPORT',GETDATE(), getdate(),@oper,@logtext1,'WCIMPORT',0,null,null);

		insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
		values (getdate(),'Prime',@logtext1,'Ann','Event','Notifications',@oper);
		
		--Oct.24,2022 add 
		Select listtype,program, count(entnum) 
		from OFAC..SDNTABLE(nolock) where Status != 4 
		and listtype like '%WPEP'and Program not like 'PEP%' AND Program not in ('SOE','SIE','IOS')
		group by listtype,Program

		if @@ROWCOUNT > 0 
		begin 
			select @logtext1 = concat('WorldCheck Dailay Import List incomplete, it is not insert into SDNTABLE on ' + convert(varchar(20),getdate(),111),'. Please contact Taipei center PRIME support team to fix import failed.')
			
			insert into PBSA..Notification (Subsystem,ObjectType, ObjectId,NotifyTime,SnoozeTime,NotifyOper,NotifyDescr,NotificationType,RecurType,RecurExpireDate,NextRecurDate)values
			('WorldCheck Import Fail','WCIMPORT','WCIMPORT',GETDATE(), getdate(),@oper,@logtext1,'WCIMPORT',0,null,null);

			insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
			values (getdate(),'Prime',@logtext1,'Ann','Event','Notifications',@oper);
		end
	end
  fetch next from cur_oper into @oper;
end
close cur_oper;
DEALLOCATE cur_oper;

