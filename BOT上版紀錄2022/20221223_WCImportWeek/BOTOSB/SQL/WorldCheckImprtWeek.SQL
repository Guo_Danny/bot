USE OFAC
GO

select CONCAT(getdate(),' Shrink log Start')
ALTER DATABASE [OFAC] SET RECOVERY Simple WITH NO_WAIT
go
dbcc shrinkfile(OFACLog_BOTOSB,1024)
go
ALTER DATABASE [OFAC] SET RECOVERY Full WITH NO_WAIT
go
select CONCAT(getdate(),' Shrink log End')

select CONCAT(getdate(),' WorldCheck Week Start')

SET NOCOUNT ON;

--A1.truncate worldcheck_TMP table
truncate table worldcheck_TMP
GO

--A2.drop & create table:  worldcheck_TMP

DROP TABLE [dbo].[WorldCheck_TMP]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WorldCheck_TMP](
	[UID] [int] NOT NULL,
	[LastName] [varchar](max) DEFAULT '',
	[FirstName] [varchar](max) DEFAULT '',
	[Aliases] [varchar](max) DEFAULT '',
	[LowQAliases] [varchar](max) DEFAULT '',
	[AlterSpell] [varchar](max) DEFAULT '',
	[Category] [varchar](max) DEFAULT '',
	[Title] [varchar](max) DEFAULT '',
	[SubCategory] [varchar](max) DEFAULT '',
	[Position] [varchar](max) DEFAULT '',
	[Age] [int] DEFAULT '',
	[DOB] [varchar](50) DEFAULT '',
	[DOBS] [varchar](3000) DEFAULT '',
	[PlaceOfBirth] [varchar](max) DEFAULT '',
	[Deceased] [varchar](50) DEFAULT '',
	[Passports] [varchar](max) DEFAULT '',
	[SSN] [varchar](max) DEFAULT '',
	[IDENTIFICATION_NUMBERS] [varchar](max) DEFAULT '',
	[Location] [varchar](max) DEFAULT '',
	[Country] [varchar](max) DEFAULT '',
	[CITIZENSHIP] [varchar](max) DEFAULT '',
	[Company] [varchar](max) DEFAULT '',
	[Type] [varchar](max) DEFAULT '',
	[LinkedTo] [varchar](max) DEFAULT '',
	[FurtherInfo] [varchar](max) DEFAULT '',
	[Keywords] [varchar](max) DEFAULT '',
	[ExSources] [varchar](max) DEFAULT '',
	[UpdCategory] [varchar](max) DEFAULT '',
	[Entered] [varchar](50) DEFAULT '',
	[Updated] [varchar](50) DEFAULT '',
	[Editor] [varchar](max) DEFAULT '',
	[AgeDate] [varchar](250) DEFAULT '',
	[Entnum] [int] NULL,
	[UPDDate] [datetime] NULL,
	[DelFlag] [bit] NULL,
 CONSTRAINT [PK_WorldCheck_TMP] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


--A3.create index
CREATE UNIQUE NONCLUSTERED INDEX [PK_TempWorldCheckEnt] ON [dbo].[WorldCheck_TMP]
(
	[Entnum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [PK_TempWorldCheckUPD] ON [dbo].[WorldCheck_TMP]
(
	[UPDDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


--A4.1 del index-
Declare @iTotRec int

Select  @iTotRec=COUNT(name) from sys.indexes where name ='PK_TempWorldCheckUPD' and object_id in (select object_id from sys.all_objects where type_desc in ( 'USER_TABLE') and name='WorldCheck_TMP')

if @iTotRec > 0
DROP INDEX worldcheck_TMP.PK_TempWorldCheckUPD


select @iTotRec=COUNT(name) from sys.indexes where name ='PK_TempWorldCheckEnt' and object_id in (select object_id from sys.all_objects where type_desc in ( 'USER_TABLE') and name='WorldCheck_TMP')

if @iTotRec > 0
DROP INDEX worldcheck_TMP.PK_TempWorldCheckEnt


--A4.2 del column: entnum, upddate, delflag
select @iTotRec=COUNT(name) from sys.columns where name='ENTNUM' and object_id in (select object_id from sys.all_objects where type_desc in ( 'USER_TABLE') and name='WorldCheck_TMP')

if @iTotRec > 0
ALTER TABLE worldcheck_TMP DROP COLUMN ENTNUM;


select @iTotRec=COUNT(name) from sys.columns where name='UPDDATE' and object_id in (select object_id from sys.all_objects where type_desc in ( 'USER_TABLE') and name='WorldCheck_TMP')

if @iTotRec > 0
ALTER TABLE worldcheck_TMP DROP COLUMN UPDDATE;


select @iTotRec=COUNT(name) from sys.columns where name='DELFLAG' and object_id in (select object_id from sys.all_objects where type_desc in ( 'USER_TABLE') and name='WorldCheck_TMP')

if @iTotRec > 0
ALTER TABLE worldcheck_TMP DROP COLUMN DELFLAG;

select CONCAT(getdate(),' WorldCheck TMP Bulk insert Start')
--A5. BULK INSERT DOWNLOAD FILE TO TABLE WorldCheck_TMP
BULK INSERT [dbo].[WorldCheck_TMP] FROM 'E:\worldcheck\premium-world-check-week.csv' WITH(BATCHSIZE=50000,CHECK_CONSTRAINTS,CODEPAGE='RAW',DATAFILETYPE='char',FIELDTERMINATOR='\t',ROWTERMINATOR='0x0a',FIRSTROW=2)
select CONCAT(getdate(),' WorldCheck TMP Bulk insert End')

SELECT @iTotRec=COUNT(Uid) from WorldCheck_TMP 

if @iTotRec > 0
Select 'Bulk Insert WorldCheck_TMP Susccessfully, Total Record : ' + CAST(@iTotRec AS varchar(20))
GO

--A6. Add column entnum, upddate, delflag
ALTER TABLE [dbo].[WorldCheck_TMP] ADD [Entnum] [int] NULL,[UPDDate] [datetime] NULL,[DelFlag] [bit] NOT NULL DEFAULT 0;
GO

--A7. WorldCheck_Tmp UPDdate, update import datetime
declare @dtnow datetime;
set @dtnow = getdate();

update [dbo].[WorldCheck_TMP] set UPDdate = @dtnow;
GO

--A8. Create Index

CREATE NONCLUSTERED INDEX [PK_TempWorldCheckUPD] ON [dbo].[WorldCheck_TMP]
(
	[UPDDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


--A9. insert OFAC Event Log
declare @count varchar(8);

select @count = count(*) from [dbo].[WorldCheck_TMP]

insert into SDNChangeLog(LogDate,Oper,LogText,type,ObjectType,ObjectId)
values(getdate(),'Prime','worldcheck totoal insert : ' + @count,'Ann','Event','WORLDCHECKALL')
GO

--******************************************************************************************
--B1. truncate TempWorldCheckDel Table
truncate table TempWorldCheckDel
GO

--B2. Bulk Insert 
BULK INSERT [dbo].[TempWorldCheckDel] FROM 'E:\worldcheck\world-check-deleted-v2.csv' WITH(BATCHSIZE=50000,CHECK_CONSTRAINTS,CODEPAGE='RAW',DATAFILETYPE='char',FIELDTERMINATOR='\t',ROWTERMINATOR='0x0a',FIRSTROW=2)
GO

--B3. update DelFlag=1 in table WorldCheck_TMP 
declare @dtnow datetime;

set @dtnow = getdate();

update WorldCheck set WorldCheck.Updated = TempWorldCheckDel.Updated,WorldCheck.DelFlag = 1, WorldCheck.UPDDate = @dtnow
from TempWorldCheckDel
join worldcheck on TempWorldCheckDel.uid = worldcheck.uid
where isnull(worldcheck.delflag,0) = 0
GO

--B4. update Entnum in table WorldCheck_TMP
update [dbo].[WorldCheck_TMP]
set Entnum = UID + 20000000
GO

--B5. create index
CREATE UNIQUE INDEX [PK_TempWorldCheckEnt] ON [dbo].[WorldCheck_TMP]
(
	[entnum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

--B6. insert OFAC Event Log
declare @count varchar(8);

select @count = count(*) from [dbo].[TempWorldCheckDel]

insert into SDNChangeLog(LogDate,Oper,LogText,type,ObjectType,ObjectId)
values(getdate(),'Prime','worldcheckdel_v2 totoal update:' + @count,'Ann','Event','WORLDCHECKALL')
GO

SET NOCOUNT OFF;

--**********************************************************************************************
--C1. insert from  WorldCheck_Tmp, to WorldCheck
insert into WorldCheck ([UID]
      ,[LastName]
      ,[FirstName]
      ,[Aliases]
      ,[LowQAliases]
      ,[AlterSpell]
      ,[Category]
      ,[Title]
      ,[SubCategory]
      ,[Position]
      ,[Age]
      ,[DOB]
	  ,[DOBS]
      ,[PlaceOfBirth]
      ,[Deceased]
      ,[Passports]
      ,[SSN]
      ,[IDENTIFICATION_NUMBERS]
      ,[Location]
      ,[Country]
	  ,[CITIZENSHIP]
      ,[Company]
      ,[Type]
      ,[LinkedTo]
      ,[FurtherInfo]
      ,[Keywords]
      ,[ExSources]
      ,[UpdCategory]
      ,[Entered]
      ,[Updated]
      ,[Editor]
      ,[AgeDate]
      ,[Entnum]
      ,[UPDDate]
      ,[DelFlag])
select [UID]
      ,[LastName]
      ,[FirstName]
      ,[Aliases]
      ,[LowQAliases]
      ,[AlterSpell]
      ,[Category]
      ,[Title]
      ,[SubCategory]
      ,[Position]
      ,[Age]
      ,[DOB]
	  ,[DOBS]
      ,[PlaceOfBirth]
      ,[Deceased]
      ,[Passports]
      ,[SSN]
      ,[IDENTIFICATION_NUMBERS]
      ,[Location]
      ,[Country]
	  ,[CITIZENSHIP]
      ,[Company]
      ,[Type]
      ,[LinkedTo]
      ,[FurtherInfo]
      ,[Keywords]
      ,[ExSources]
      ,[UpdCategory]
      ,[Entered]
      ,[Updated]
      ,[Editor]
      ,[AgeDate]
      ,[Entnum]
      ,[UPDDate]
      ,[DelFlag] from WorldCheck_TMP(nolock) wct
where Entnum not in
(select Entnum from WorldCheck(nolock) wc where wct.uid = wc.uid)
GO

select CONCAT(getdate(),' WorldCheck Insert not exists success')

--C2. update WorldCheck upddate
UPDATE WorldCheck 
SET LastName = B.LastName,
    FirstName = B.FirstName,
    Aliases = B.Aliases,
    LowQAliases = B.LowQAliases,
    AlterSpell = B.AlterSpell,
    Category = B.Category,
    Title = B.Title,
    SubCategory = B.SubCategory,
    Position = B.Position,
    Age = B.Age,
    DOB = B.DOB,
	DOBS = B.DOBS,
    PlaceOfBirth = B.PlaceOfBirth,
    Deceased = B.Deceased,
    Passports = B.Passports,
    SSN = B.SSN,
    IDENTIFICATION_NUMBERS = B.IDENTIFICATION_NUMBERS,
    Location = B.Location,
    Country = B.Country,
	CITIZENSHIP = B.CITIZENSHIP,
    Company = B.Company,
    Type = B.Type,
    LinkedTo = B.LinkedTo,
    FurtherInfo = B.FurtherInfo,
    Keywords = B.Keywords,
    ExSources = B.ExSources,
    UpdCategory = B.UpdCategory,
    Entered = B.Entered,
    Updated = B.Updated,
    Editor = B.Editor,
    AgeDate = B.AgeDate,
    DELFlag = b.DelFlag,
    UPDDate = b.UPDDate
    FROM WorldCheck(nolock) wc JOIN WorldCheck_TMP(nolock) B
    ON wc.UID = B.UID
		where (CONVERT(datetime,b.Updated) >= DATEADD(MONTH,-3,getdate())
		and wc.UPDDate <= DATEADD(MONTH,-1,getdate())
		and B.Updated > wc.Updated) or isnull(wc.UPDDATE,'')='';
GO

select CONCAT(getdate(),' WorldCheck update exists success')

Select '..... Bulk Insert WorldCheck_TMP & WorldCheck Successfully .....'  
GO


/* update keywords data */

declare @dDate date, @dtDate datetime,@sDate varchar(100)
select @dDate=convert(date,max(upddate),110), @dtDate=max(upddate) from WorldCheck(nolock)
select @sDate=substring(convert(varchar(20),@dtDate,120),1,13) + ':0:0'
select @dtDate=dateadd(hour,+12,convert(datetime, @sDate,120))

update WorldCheck set UPDDate = @dtDate
where uid in (
select uid from WorldCheck(nolock) wc where isnull(DelFlag,0) = 0
and not exists (select uid from WCKeywords(nolock) wck where wck.UID = wc.UID)
and isnull(wc.Keywords,'') != ''
)


/* update pep data */

update WorldCheck set UPDDate = @dtDate
from WorldCheck(nolock) wc
join WorldCheckCountry(nolock) wcc on wc.Country = wcc.WorldCheck_Country
left join SDNTable(nolock) sdn on wc.Entnum = sdn.EntNum
where isnull(wc.DelFlag,0) = 0 
and wcc.Status = 1
and isnull(wc.SubCategory,'') != ''
and sdn.EntNum is null
GO

select CONCAT(getdate(),' WorldCheck Week End')

select CONCAT(getdate(),' Shrink log Start')
ALTER DATABASE [OFAC] SET RECOVERY Simple WITH NO_WAIT
go
dbcc shrinkfile(OFACLog_BOTOSB,1024)
go
ALTER DATABASE [OFAC] SET RECOVERY Full WITH NO_WAIT
go
select CONCAT(getdate(),' Shrink log End')
