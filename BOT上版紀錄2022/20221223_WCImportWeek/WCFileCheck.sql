use ofac
go

declare @lastupd datetime, @difday int, @oper varchar(50)

set @difday = 3

select @lastupd = max(UPDDate) from WorldCheck(nolock)

--set @lastupd = '2022-07-01'

if DATEDIFF(D,@lastupd,getdate()) >= @difday
begin
	declare cur_oper CURSOR FOR
		select Oper from PSEC..RoleMembers
		where [Role] like '%caseap%'
		open cur_oper;
		fetch next from cur_oper into @oper;
		while @@fetch_status = 0 
		begin
			insert into pbsa..Notification(Subsystem,ObjectType, ObjectId,NotifyTime,SnoozeTime,NotifyOper,NotifyDescr,NotificationType,RecurType,RecurExpireDate,NextRecurDate)
			values('WorldCheck File Check','WCFile','WCFile',GETDATE(), getdate(),@oper,'WorldCheck data file out of date!','WCFile',0,null,null);
			
			fetch next from cur_oper into @oper;
		end
		close cur_oper;
		DEALLOCATE cur_oper;
end







