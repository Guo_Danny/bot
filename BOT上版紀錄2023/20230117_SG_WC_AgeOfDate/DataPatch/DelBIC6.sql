use ofac
GO


--ALT 查尋原6碼BIC資料，並且復原該資料狀態
begin tran
--update s1 set s1.Status = 1, s1.LastModifDate = getdate(), Remarks = 'fix for bic6'
select s1.EntNum,s1.AltNum, s1.AltName, s1.Status, s2.AltName,s2.AltNum
from sdnalttable (nolock) s1 
join sdnalttable (nolock) s2 on s1.EntNum = s2.EntNum
where s2.ListID = 'BIC'
and len(s2.AltName) = 16 and substring(s2.AltName,11,6) = s1.AltName
and s1.Status = 4
--order by s1.EntNum
commit

--NOTES 查尋原6碼BIC資料，並且復原該資料狀態
begin tran
--update n1 set n1.Status = 1, LastModify = getdate()
select n1.EntNum,n1.NotesID, n1.Note, n1.Status, n2.Note,n2.NotesID
from Notes (nolock) n1 
join Notes (nolock) n2 on n1.EntNum = n2.EntNum
where n2.NoteType = '106'
and len(n2.note) = 16 and substring(n2.Note,11,6) = n1.Note
and n1.Status = 4
commit


--ALT 查詢BIC6資料 
select * from SDNAltTable (nolock) where AltName like '%{INT-BIC6}%'

--ALT 刪除錯誤BIC 
begin tran
--delete SDNAltTable where AltName like '%{INT-BIC6}%'
commit

--NOTES 查詢BIC6資料 
select * from notes (nolock) where Note like '%{INT-BIC6}%' and NoteType = 106

--NOTES 刪除錯誤BIC 
begin tran
--delete notes where Note like '%{INT-BIC6}%' and NoteType = 106
commit


