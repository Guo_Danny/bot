USE
OFAC
GO

--update SDNPROGRAM
update WCKeywordslistTable set Used = 1, SDNPROGRAM = 'FATF' where Abbreviation = 'FATF'

update WCKeywordslistTable set Used = 1, SDNPROGRAM = 'FATF-SD' where Abbreviation = 'FATF-SD'

--check sdn program for FATF
select sdn.* from SDNTable(nolock) sdn
join WCKeywordslistTable(nolock) wckl
join WCKeywords(nolock) wck on wck.Word = wckl.Abbreviation
join WorldCheck(nolock) wc on wc.UID = wck.UID on wc.Entnum = sdn.EntNum
where wckl.SDNPROGRAM = 'FATF'

--check sdn program for FATF-SD
select sdn.* from SDNTable(nolock) sdn
join WCKeywordslistTable(nolock) wckl
join WCKeywords(nolock) wck on wck.Word = wckl.Abbreviation
join WorldCheck(nolock) wc on wc.UID = wck.UID on wc.Entnum = sdn.EntNum
where wckl.SDNPROGRAM = 'FATF-SD'

--update worldcheck.upddate for FATF
begin tran

declare @update datetime
set @update = '2023-03-07 18:00:00'

update wc set wc.upddate = @update
from WCKeywordslistTable(nolock) wckl
join WCKeywords(nolock) wck on wck.Word = wckl.Abbreviation
join WorldCheck(nolock) wc on wc.UID = wck.UID
where wckl.SDNPROGRAM = 'FATF'

--commit

--update worldcheck.upddate for FATF-SD
begin tran

declare @update datetime
set @update = '2023-03-07 18:00:00'

update wc set wc.upddate = @update
from WCKeywordslistTable(nolock) wckl
join WCKeywords(nolock) wck on wck.Word = wckl.Abbreviation
join WorldCheck(nolock) wc on wc.UID = wck.UID
where wckl.SDNPROGRAM = 'FATF-SD'

--commit