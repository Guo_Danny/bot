﻿! Initialization File for BatchFilter Utility program


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
[Options]
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Must match the name of a valid File Image on disk. File Images are generally
! created from a Sanction Data Rule in OFAC Reporter. When created from a 
! Sanction Data Rule, it must match the Sanction Data Rule Code and cannot 
! exceed 11 characters. Always ensure that an up to date File Image has been
! built by OFAC Reporter.
! To indicate that Prime paths should be used to locate the File Image, specify
! the FileImage as the file name without a path or extension for example:
! StdFileImg. If a path and/or extension is specifed it will be used and Prime 
! paths will be ignored. This will allow processing without Prime software 
! installed.
FileImage = 0094SWIFT

! Indicate whether to use full SDN list (0)/ incremental SDN list (1)
!  	PartyListType = 0  - Full (All customers will be filtered against full list)
!  	PartyListType = 1  - Incremental (All customers will be filtered against Incremental list)
!  	PartyListType = 2  - Auto (New and modified customers will be filtered against Full list
!	    	             and already ofac filtered customers will be filtered
!						 against incremental list)
! This option will be applicable only when Party List Type Auto is selected 
! for the FileImage specified above.

!Please note that if PartyListType = 2 is selected, the CIF files should not contain duplicate records. 
! The combination of keyfield1 and keyfield2 identify a record uniquely.

! For incremental filtering to run properly, if the 'Source' in the ini file is same for multiple instances of the BatchFilter
! program, then make sure that these instances are not run simultaneously

PartyListType = 2

! OutputToFile determines if the match results should be written to a file. Use
! true or false. If true, a valid OutFileSpec should be specified.
OutputToFile = False

!OutputAllRecords - Determines if all records should be written to a file. 
!Use true or false. 
!If true all records will be written to the output file using tab-delimited format. 
!If a match is found the record is marked with character Y, other wise it is marked with character N.
OutputAllRecords = True

! OutputToDb determines if the match results should be written to Prime's 
! database. Use true or false.
OutputToDb = True

! Delegated determines if the match results should be written to Prime's 
! database with the status Delegated. Delegated transactions may not be 
! confirmed nor waived. They are writen to Prime's database archive after End 
! Day processing. Use true or false.
Delegated = False

! Branch and Dept should be selected from values in the Security Database. They
! cannot exceed 10 characters and spaces are not allowed.
Branch = 0094
Dept = AML

! Source cannot exceed 15 characters.
Source = BNFMM

! ClientId - A numeric value matching an existing Client Interface ID record in OFAC Reporter’s database 
!(defaults to 8888). Refer to the document ‘Setting up CIF Client Interface.doc’ for more details.
ClientId = 8888

!Trace – Allows additional information to be written to the log file. Use True or False. 
!True option allows to output additional information in the log and used by Prime developers for debugging purposes. 
!False is default option and is recommended for standard use.
Trace = true

!CIFRule –Specify the code of the CIF Rule created in Prime Compliance Suite. 
!When this option is used, there is no need to specify the options in the configuration file.

!OutputAllWithSystemReference –Accepted values are TRUE or FALSE. 
!When this option is selected the output file created with have a unique system generated reference for each input record.
OutputAllWithSystemReference = FALSE

!OutputToXML - Determines if the results should be written to a file in xml. Use true or false. 
!If true, OutputToFile should also be set to true. 
!The output xml will be format as described by batchfilteroutput.xsd.

!LogFileSpec - Indicates the name of the file that processing messages should be appended to. 
!If missing, BatchFilter.log is used. BatchFilter.log file is created in the BatchFilter.exe program folder.


MatchOutputToInput = False

EnableScanAudit = True

GenerateExactMatch=True

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
[File]
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! InFileSpec is used to locate the file(s) to be processed. You may use a 
! wildcard specification for InFileSpec.
InFileSpec = D:\Prime\BOTOSB\0094_CIF\0094BNF*.mm

! OutFileSpec is used to indicate where the match results should be written 
! when OutputToFile is set to true.
OutFileSpec = D:\Prime\BOTOSB\0094_CIF\0094BNFmm.out

! LogFileSpec is used to indicate the name of the file that processing messages
! should be appended to. If missing BatchFilter.log is used.
LogFileSpec = \\OSBAMLLADBV\Prime\BOTOSB\Logs\ofac\0094_BNFmm-BOTOSB.LOG

! The record type. Either Delimited or Fixed. Delimited records contain fields
! that are separated by a special character such as a tab. The length of a 
! Delimited record is may vary for each record and is not known until it is 
! processed. Fixed records contain fields of a constant size and therefore the 
! overall length of the record is known before processing.
RecType = Delimited

!RecordDelimiter - If the RecType is Delimited, this value must be the single ASCII value of the character 
!that delimits each record. If the RecType is Fixed, use 0.
RecordDelimiter = 10

!FieldDelimiter - Must be comma-separated list of ASCII values. 
!If one character is used as a delimiter, exclude the comma; for example "9".
!If multiple values are used, do not use spaces around the comma separator(s) for example "13,10". 
!If a delimiter is not used, place a blank after the equal sign.
FieldDelimiter = 124

!RecSize - Use 0 as the RecSize for Delimited record type. Otherwise, specify the size of each record.
RecSize = 0

!FldCnt - Indicates the maximum number of fields in a record. 
!There must be as many [DESCn] sections as indicated by FldCnt. 
!Maximum number of fields supported is 500. 
FldCnt = 13


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
[Keys]
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This field describes which of the following descriptors make up the customer
! ID and customer name to uniquely define each record. Fields numbers are 
! one (1) based.
KeyFld1 = 1
KeyFld2 = 2


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! There should be one DescN section for each field with a total number of DescN
! sections equal to FldCnt.
! Offsets are zero (0) based
! Offset and Len should both be 0 when RecType is Delimited 
! Values for Alg are: None, MatchFullName, MatchText
! Misspelling = False when misspelling check should be performed. When false,
! misspelling rules are not used.
! UseTextExclude indicates that Exclude List entries that do not identify 
! specific customer records should also be used to minimize false positives. 
! UseTextExclude is generally recommended.
! Values for FieldType are: CustId, Name, FullAddress, Street, City, Country, 
!						    Other
! FullAddress is a field that may contain street, city, state, etc.
! The following two tags are used to implement cross field matching rules.
! must be comma separated list of
! MustAlsoMatchAllOf = must be comma separated list of descriptor numbers
! MustAlsoMatchAnyOf = must be comma separated list of descriptor numbers
!   For Example: MustAlsoMatchAllOf = 5
!   For Example: MustAlsoMatchAnyOf = 3,4,5
!   Note that the list of cross fields is currently limited to 30.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
[Desc1]
!ID, CIFKEY: CIFKEY
Alg = None
Misspelling = False
UseTextExclude = True
FieldType=CustID

[Desc2]
!Name, ENAME: 英文戶名1 + 英文戶名2 + 英文戶名3; First Name/Full name (F3301) + Middle Name (F3301) + Last Name/Surname (F3301)
Alg = MatchText
Misspelling = False
UseTextExclude = True
FieldType=Name
MustMatchType=PartyName

[Desc3]
!DIRNAME1 :  別名 Alias (F3301)
Alg = MatchText
Misspelling = False
UseTextExclude = True
FieldType=Name
MustMatchType=PartyName

[Desc4]
!ID, CIFKEY: TXCD
Alg = None
Misspelling = False
UseTextExclude = True
FieldType=CustID

[Desc5]
!CNTRY: 註冊國／母國 Nationality (F3301)
Alg = MatchText
Misspelling = False
UseTextExclude = True
FieldType=Country
MustMatchType=Country,City
MustAlsoMatchAllOf=2

[Desc6]
!NAL: 註冊國／母國 Country1 (F3302)
Alg = MatchText
Misspelling = False
UseTextExclude = True
FieldType=Country
MustMatchType=Country,City
MustAlsoMatchAllOf=2

[Desc7]
!OCNTRY1: 註冊國／其他國家 Other Country1 (F3300)
Alg = MatchText
Misspelling = False
UseTextExclude = True
FieldType=Country
MustMatchType=Country,City
MustAlsoMatchAllOf=2

[Desc8]
!OCNTRY2: 註冊國／其他國家 Other Country2 (F3300)
Alg = MatchText
Misspelling = False
UseTextExclude = True
FieldType=Country
MustMatchType=Country,City
MustAlsoMatchAllOf=2

[Desc9]
!OCNTRY3: 註冊國／其他國家 Other Country3 (F3300)
Alg = MatchText
Misspelling = False
UseTextExclude = True
FieldType=Country
MustMatchType=Country,City
MustAlsoMatchAllOf=2

[Desc10]
!DB-BNFO-BIRTHDAY (F3300/F3301/F3302 Date Of Birth)
Alg = None
Misspelling = False
UseTextExclude = True
FieldType = DOB

[Desc11]
!DB-BNFO-LUNINO (F3300/F3301 Unification No)
Alg = None
Misspelling = False
UseTextExclude = True
FieldType=CustID

[Desc12]
!DB-BNFO-LUNINO2 (F3301 Unification No2)
Alg = None
Misspelling = False
UseTextExclude = True
FieldType=CustID

[Desc13]
!OFAC-BNF-CNTRY2+"*"+DB-NAL-NALNM2: (F3301 Nationality2)
Alg = MatchText
Misspelling = False
UseTextExclude = True
FieldType=Country
MustMatchType=Country,City
MustAlsoMatchAllOf=2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
[Processor]
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This field suggest the largest size in bytes for a mapped view of the file. 
MaxSuggView = 100000000
! This field suggest the minimum size in bytes before multi-processing is used.
MinSizeForMt = 100000000
! This field suggest the maximum number of threads when multi-processing is 
! used. Zero indicates that single processor will be used.
MaxSuggThreads = 1

