
--check sdn program for FATF
select sdn.* from SDNTable(nolock) sdn
join WCKeywordslistTable(nolock) wckl
join WCKeywords(nolock) wck on wck.Word = wckl.Abbreviation
join WorldCheck(nolock) wc on wc.UID = wck.UID on wc.Entnum = sdn.EntNum
where wckl.SDNPROGRAM = 'FATF'

--check sdn program for FATF-SD
select sdn.* from SDNTable(nolock) sdn
join WCKeywordslistTable(nolock) wckl
join WCKeywords(nolock) wck on wck.Word = wckl.Abbreviation
join WorldCheck(nolock) wc on wc.UID = wck.UID on wc.Entnum = sdn.EntNum
where wckl.SDNPROGRAM = 'FATF-SD'