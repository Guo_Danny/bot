use ofac
go

delete nt
from ofac..WorldCheck(nolock) wc
join ofac..Notes(nolock) nt on nt.EntNum = wc.Entnum
where wc.Keywords != nt.note
and nt.NoteType = 101