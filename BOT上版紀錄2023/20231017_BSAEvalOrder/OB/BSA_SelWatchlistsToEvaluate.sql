USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[BSA_SelWatchlistsToEvaluate]    Script Date: 10/18/2023 11:19:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--------------------------------------------------------------------------------
ALTER Procedure [dbo].[BSA_SelWatchlistsToEvaluate]( @IsPreEOD as bit )
   As
Begin
	Set Nocount On
	Declare @BusDate DATETIME

	Select @BusDate = BusDate From dbo.SysParam


	Select WLCode, Title, [Desc], WLType, SPName, SuspType, 
		Schedule, IsPreEOD, OwnerBranch, OwnerDept, OwnerOper, CreateOper, 
		CreateDate, LastOper, LastModify, LastEval, LastEvalStat, Ts, RuleType, 
		RuleFormat, ExecTime, CaseScore, UseAssignedScore, IsLicensed, 
		IsUserDefined, Params, RuleText, CreateType, UseSysDate,
		TemplateWLCode, OverRideExemption  From Watchlist
	Where Schedule > 0 AND IsLicensed = 1 AND IsUserDefined = 1
	And IsPreEOD = @IsPreEOD
	And (Case Schedule
		When 1 Then		-- EveryEOD
			1
		When 2 Then 	-- Daily
			Case When LastEval is Null then 1 
				When  UseSysDate In (0,1) And DateDiff(Day, LastEval, GetDate()) > 0 Then 1 
				When  UseSysDate = 2 And DateDiff(Day, LastEval, @BusDate) > 0 Then 1 
				Else 0 
			End
		When 3 Then 	-- Weekly
			Case When LastEval is Null then 1
				When  UseSysDate In (0,1) And DateDiff(Week, LastEval, GetDate()) > 0 Then 1
				When  UseSysDate = 2 And DateDiff(Week, LastEval, @BusDate) > 0 Then 1
				Else 0 
			End
		When 4 Then 	-- Monthly
			Case When LastEval is Null then 1 
				When  UseSysDate In (0,1) And DateDiff(Month, LastEval, GetDate()) > 0 Then 1
				When  UseSysDate = 2 And DateDiff(Month, LastEval, @BusDate) > 0 Then 1
				Else 0 
			End
		When 5 Then 	-- Quarterly
			Case When LastEval is Null then 1
				When  UseSysDate In (0,1) And DateDiff(Quarter, LastEval, GetDate()) > 0 Then 1 
				When  UseSysDate = 2 And DateDiff(Quarter, LastEval, @BusDate) > 0 Then 1 
				Else 0 
			End
		Else 0
		End) > 0
		Order by ExecTime
End
GO


