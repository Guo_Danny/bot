﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Threading;
using System.Xml;
using log4net;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace BlackListService
{
    public partial class UnisysAMLInterface : ServiceBase
    {
        System.Timers.Timer oTimer = new System.Timers.Timer();
        System.Timers.Timer oCIFTimer = new System.Timers.Timer();

        #region Setting
        //分行代碼長度
        private const int BranchCodeLen = 4;
        //檔案固定長度
        private int FileFixLen = 20;
        //cif檔長度
        private string CIFFileFixLen = "15";

        //取得檔案清單
        private List<string> oFileList = new List<string>();

        private List<FileSystemWatcher> oFileWatcherList = new List<FileSystemWatcher>();
        private List<BranchEntity> oBranchINList = new List<BranchEntity>();
        private List<BranchEntity> oBranchList = new List<BranchEntity>();
        private static readonly ILog log = LogManager.GetLogger("App.Logging");

        private AML_Data AMLData;
        private string Matchword;

        private string CIFFilePath;
        private string CIFFileErrorPath;
        private string CIFFilePathBackup;
        private string CIFReport;
        private string CIFReportBackup;
        private string BNFReport;
        private string BNFReportBackup;
        private string CIF_Copy;
        private string CIF_par;

        private string FileInPath;
        private string FileInErrorPath;
        private string FileInPathBackup;
        private string FileOutPath;
        private string FileOutPathBackup;

        private string OkCode;
        private string ProblemCode;
        private string ConfirmCode;
        private bool InitScanFlag = false;
        private string OFAC_ScanType;
        private string ScanTag_start = "";
        private string ScanTag_end = "";
        private string OFACWS_Enable = "N";
        private string OFAC_ScanType_keep = "";

        private string FileInPathBackupTmp;
        private string WSFileOutPath;

        private string SwallowWSRetry = "10";
        private string SwallowWSInterval = "10";
        private string SwallowWSTimeout = "300";
        #endregion

        #region InitSetting 取得初始設定
        private void InitSetting()
        {
            try
            {
                LogAdd("current version:" + FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion);
                LogAdd(String.Format("{0} Loading Setting", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                Fortify fortifyService = new Fortify();
                oBranchINList.Clear();
                oBranchList.Clear();
                oFileList.Clear();

                XmlDocument doc = new XmlDocument();
                doc.XmlResolver = null;
                doc.Load(fortifyService.PathManipulation(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + fortifyService.PathManipulation(@"\Setting.xml")));
                LogAdd(String.Format("{0} Loading XML File", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));

                #region Get Branch Setting
                XmlNodeList nodes = doc.SelectNodes("//Branch");

                LogAdd(String.Format("{0} Loading Branch Setting", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                foreach (XmlNode node in nodes)
                {
                    #region Setting Branch IN Directory
                    BranchEntity oInBranch = new BranchEntity();
                    oInBranch.BranchCode = node["Code"].InnerText.Replace("\r\n", "").Trim();
                    oInBranch.BranchName = node["Name"].InnerText.Replace("\r\n", "").Trim();
                    oInBranch.WatchType = "IN";
                    oInBranch.Path = node["In"].InnerText.Replace("\r\n", "").Trim();
                    oInBranch.Backup = node["Backup"].InnerText.Replace("\r\n", "").Trim();

                    try
                    {
                        oInBranch.OutputFormat = node["OutputFormat"].InnerText.Replace("\r\n", "").Trim();
                        if (oInBranch.OutputFormat == "")
                        {
                            Logerr(String.Format("Get Branch {0} File Output Format Error！", fortifyService.PathManipulation(oInBranch.BranchCode)));
                            return;
                        }
                    }
                    catch (Exception FileOutEx)
                    {
                        Logerr(String.Format("Get Branch {0} File Output Format Error！", fortifyService.PathManipulation(oInBranch.BranchCode)), FileOutEx);
                        return;
                    }

                    LogAdd(String.Format("Loading Setting {0} {1} {2}", fortifyService.PathManipulation(oInBranch.BranchCode), fortifyService.PathManipulation(oInBranch.WatchType), fortifyService.PathManipulation(oInBranch.Path)));

                    if (!Directory.Exists(oInBranch.Path))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", oInBranch.Path));
                        Logerr(String.Format("監看Branch {0} File IN目錄失敗！", fortifyService.PathManipulation(oInBranch.BranchCode)), oDirEx);
                        return;
                    }
                    #endregion

                    #region Setting Branch OK Directory
                    BranchEntity oBranchOK = new BranchEntity();
                    oBranchOK.BranchCode = node["Code"].InnerText.Replace("\r\n", "").Trim();
                    oBranchOK.BranchName = node["Name"].InnerText.Replace("\r\n", "").Trim();
                    oBranchOK.WatchType = "OK";
                    oBranchOK.Path = node["Ok"].InnerText.Replace("\r\n", "").Trim();

                    LogAdd(String.Format("Loading Setting {0} {1} {2}", fortifyService.PathManipulation(oBranchOK.BranchCode), fortifyService.PathManipulation(oBranchOK.WatchType), fortifyService.PathManipulation(oBranchOK.Path)));

                    oBranchList.Add(oBranchOK);
                    try
                    {
                        FileSystemWatcher oOKObj = new FileSystemWatcher();
                        oOKObj.InternalBufferSize = 65536;
                        oOKObj.Path = oBranchOK.Path;
                        oOKObj.Filter = "*.*";
                        oOKObj.NotifyFilter = NotifyFilters.FileName;
                        oOKObj.Created += new FileSystemEventHandler(oOKObj_Created);
                        oOKObj.EnableRaisingEvents = true;
                        oFileWatcherList.Add(oOKObj);
                    }
                    catch (Exception FileOKEx)
                    {
                        Logerr(String.Format("監看Branch {0} File OK目錄失敗！", fortifyService.PathManipulation(oBranchOK.BranchCode)), FileOKEx);
                        return;
                    }
                    #endregion

                    #region Setting Branch Problem Directory
                    BranchEntity oBranchProblem = new BranchEntity();
                    oBranchProblem.BranchCode = node["Code"].InnerText.Replace("\r\n", "").Trim();
                    oBranchProblem.BranchName = node["Name"].InnerText.Replace("\r\n", "").Trim();
                    oBranchProblem.WatchType = "Problem";
                    oBranchProblem.Path = node["Problem"].InnerText.Replace("\r\n", "").Trim();

                    LogAdd(String.Format("Loading Setting {0} {1} {2}", oBranchProblem.BranchCode, oBranchProblem.WatchType, oBranchProblem.Path));

                    oBranchList.Add(oBranchProblem);
                    try
                    {
                        FileSystemWatcher oProblemObj = new FileSystemWatcher();
                        oProblemObj.InternalBufferSize = 65536;
                        oProblemObj.Path = oBranchProblem.Path;
                        oProblemObj.Filter = "*.*";
                        oProblemObj.NotifyFilter = NotifyFilters.FileName;
                        oProblemObj.Created += new FileSystemEventHandler(oProblemObj_Created);
                        oProblemObj.EnableRaisingEvents = true;
                        oFileWatcherList.Add(oProblemObj);
                    }
                    catch (Exception FileProblemEx)
                    {
                        Logerr(String.Format("監看Branch {0} File Problem目錄失敗！", oBranchProblem.BranchCode), FileProblemEx);
                        return;
                    }
                    #endregion

                    #region Setting Branch Confirm Directory
                    BranchEntity oBranchConfirm = new BranchEntity();
                    oBranchConfirm.BranchCode = node["Code"].InnerText.Replace("\r\n", "").Trim();
                    oBranchConfirm.BranchName = node["Name"].InnerText.Replace("\r\n", "").Trim();
                    oBranchConfirm.WatchType = "Confirm";
                    oBranchConfirm.Path = node["Confirm"].InnerText.Replace("\r\n", "").Trim();
                    LogAdd(String.Format("Loading Setting {0} {1} {2}", oBranchConfirm.BranchCode, oBranchConfirm.WatchType, oBranchConfirm.Path));
                    oBranchList.Add(oBranchConfirm);
                    try
                    {
                        FileSystemWatcher oConfirmObj = new FileSystemWatcher();
                        oConfirmObj.InternalBufferSize = 65536;
                        oConfirmObj.Path = oBranchConfirm.Path;
                        oConfirmObj.Filter = "*.*";
                        oConfirmObj.NotifyFilter = NotifyFilters.FileName;
                        oConfirmObj.Created += new FileSystemEventHandler(oConfirmObj_Created);
                        oConfirmObj.EnableRaisingEvents = true;
                        oFileWatcherList.Add(oConfirmObj);
                    }
                    catch (Exception FileConfirmEx)
                    {
                        Logerr(String.Format("監看Branch {0} File Confirm目錄失敗！", oBranchConfirm.BranchCode), FileConfirmEx);
                        return;
                    }
                    #endregion

                    oInBranch.CIFIN = node["CIFIn"].InnerText.Replace("\r\n", "").Trim();
                    if (!Directory.Exists(oInBranch.CIFIN))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", oInBranch.CIFIN));
                        Logerr(String.Format("監看Branch {0} CIFIN目錄失敗！", oInBranch.BranchCode), oDirEx);
                        return;
                    }

                    oInBranch.CIFFilterTime = node["CIFFilterTime"].InnerText.Replace("\r\n", "").Trim();

                    oInBranch.CIFFilterSetting = node["CIFFilterSetting"].InnerText.Replace("\r\n", "").Trim();
                    if (!File.Exists(oInBranch.CIFFilterSetting))
                    {
                        FileNotFoundException oFileEx = new FileNotFoundException(String.Format("{0} Not Exist", oInBranch.CIFFilterSetting));
                        Logerr(String.Format("監看Branch {0} ini File Error！", oInBranch.BranchCode), oFileEx);
                        return;
                    }

                    oInBranch.CIFFilterCMD = node["CIFFilterCMD"].InnerText.Replace("\r\n", "").Trim();

                    //20170109 add by danny
                    oInBranch.CIFFilterCMDForF = node["CIFFilterCMDForF"].InnerText.Replace("\r\n", "").Trim();

                    //oInBranch.DivImage = node["Div_Image"].InnerText.Replace("\r\n", "").Trim();
                    oInBranch.CIFmmFilterCMD = node["CIFMMFilterCMD"].InnerText.Replace("\r\n", "").Trim();

                    Matchword = node["GetMatch"].InnerText.Replace("\r\n", "").Trim();

                    List<ValueSetting> oValueList = new List<ValueSetting>();
                    foreach (XmlNode subNode in node.SelectNodes("ValueSetting"))
                    {
                        ValueSetting oValueSetting = new ValueSetting();
                        oValueSetting.ID = subNode["ID"].InnerText.Replace("\r\n", "").Trim(); ;
                        oValueSetting.IDValue = subNode["Value"].InnerText.Replace("\r\n", "").Trim(); ;
                        oValueSetting.ValueFormat = subNode["ValueFormat"].InnerText.Replace("\r\n", "").Trim(); ;
                        oValueSetting.DateAdd = subNode["DateAdd"].InnerText.Replace("\r\n", "").Trim(); ;
                        oValueList.Add(oValueSetting);
                    }

                    #region Get WPEP Setting
                    //20170320 by danny get wpep settings
                    foreach (XmlNode node_wpep in node.SelectNodes("WPEP"))
                    {
                        oInBranch.WPEPEnable = node_wpep["Enable"].InnerText.Replace("\r\n", "").Trim();
                        oInBranch.WPEPReference = node_wpep["ReferenceNumber"].InnerText.Replace("\r\n", "").Trim();
                        oInBranch.WPEPBankNo = node_wpep["BankNo"].InnerText.Replace("\r\n", "").Trim();
                        oInBranch.WPEPBranchNo = node_wpep["BranchNo"].InnerText.Replace("\r\n", "").Trim();
                        oInBranch.WPEPEntNum = node_wpep["EntNum"].InnerText.Replace("\r\n", "").Trim();
                        oInBranch.WPEPretry = node_wpep["Retry"].InnerText.Replace("\r\n", "").Trim();
                        oInBranch.WPEPdelay = node_wpep["Delay"].InnerText.Replace("\r\n", "").Trim();
                        oInBranch.WPEPnation_col = node_wpep["Nation_col"].InnerText.Replace("\r\n", "").Trim();
                        oInBranch.WPEPengname_col = node_wpep["Engname_col"].InnerText.Replace("\r\n", "").Trim();
                        oInBranch.WPEPscore = node_wpep["Score"].InnerText.Replace("\r\n", "").Trim();
                        oInBranch.WPEPdob = node_wpep["Dob_col"].InnerText.Replace("\r\n", "").Trim();
                        oInBranch.WPEPswifttype1 = node_wpep["Swift_type1"].InnerText.Replace("\r\n", "").Trim();
                        oInBranch.WPEPswifttype2 = node_wpep["Swift_type2"].InnerText.Replace("\r\n", "").Trim();
                        oInBranch.WPEPCIF_TYPE = node_wpep["CIFType"].InnerText.Replace("\r\n", "").Trim();
                    }
                    #endregion

                    oInBranch.ValueSettingList = oValueList.ToArray();

                    oBranchINList.Add(oInBranch);
                }

                #endregion

                #region SWIFT
                LogAdd(String.Format("{0} Loading System Setting", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                XmlNodeList settingnodes = doc.SelectNodes("//Setting");
                foreach (XmlNode node in settingnodes)
                {

                    FileInPath = node["FileInPath"].InnerText.Replace("\r\n", "").Trim();

                    ScanTag_start = node["OFAC_ScanTag_S"].InnerText.Replace("\r\n", "").Trim();
                    ScanTag_end = node["OFAC_ScanTag_E"].InnerText.Replace("\r\n", "").Trim();
                    try
                    {
                        FileSystemWatcher oFileInObj = new FileSystemWatcher();
                        oFileInObj.InternalBufferSize = 65536;
                        oFileInObj.Path = FileInPath;
                        oFileInObj.Filter = "*.*";
                        oFileInObj.NotifyFilter = NotifyFilters.FileName;
                        oFileInObj.Created += new FileSystemEventHandler(oFileInObj_Created);
                        oFileInObj.EnableRaisingEvents = true;
                    }
                    catch (Exception FileINEx)
                    {
                        Logerr(String.Format("監看File In目錄 {0} 失敗！", FileInPath), FileINEx);
                        return;
                    }

                    FileInErrorPath = node["FileInErrorPath"].InnerText.Replace("\r\n", "").Trim();
                    if (!Directory.Exists(FileInErrorPath))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", FileInErrorPath));
                        Logerr(String.Format("監看FileInErrorPath目錄失敗！ {0}", FileInErrorPath));
                        return;
                    }


                    FileInPathBackup = node["FileInPathBackup"].InnerText.Replace("\r\n", "").Trim();
                    //2021 JUN 1 add tmp folder
                    FileInPathBackupTmp = node["FileInPathBackupTMP"].InnerText.Replace("\r\n", "").Trim();
                    if (!Directory.Exists(FileInPathBackup))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", FileInPathBackup));
                        Logerr(String.Format("監看FileInPathBackup目錄失敗！ {0}", FileInPathBackup));
                        return;
                    }

                    //2021 JUN 1 add tmp folder
                    if (!Directory.Exists(FileInPathBackupTmp))
                    {
                        Directory.CreateDirectory(fortifyService.PathManipulation(FileInPathBackupTmp));
                        LogAdd("Temp Folder for Swift not Exist!! Create Folder :" + FileInPathBackupTmp);
                    }

                    FileOutPath = node["FileOutPath"].InnerText.Replace("\r\n", "").Trim();
                    if ((!string.IsNullOrEmpty(FileOutPath)) && !Directory.Exists(FileOutPath))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", FileOutPath));
                        Logerr(String.Format("監看FileOutPath目錄失敗！ {0}", FileOutPath));
                        return;
                    }
                    else if (string.IsNullOrEmpty(FileOutPath))
                    {
                        //Oct.19, 2023 add log to check the path.
                        LogAdd(string.Format("FileOutPath is Null or Empty. Disable this out path."));
                    }

                    //Oct.18,2023 add WS file out path
                    WSFileOutPath = node["WSFileOutPath"].InnerText.Replace("\r\n", "").Trim();
                    if ((!string.IsNullOrEmpty(WSFileOutPath)) && !Directory.Exists(FileOutPath))
                    {
                        LogAdd("WSFileOutPath not exists, try to create.");
                        Directory.CreateDirectory(fortifyService.PathManipulation(WSFileOutPath));
                    }
                    else if (string.IsNullOrEmpty(WSFileOutPath))
                    {
                        LogAdd(string.Format("WSFileOutPath is Null or Empty. Disable this out path."));
                    }

                    //Nov.2, 2023 call swallow ws retry
                    SwallowWSRetry = node["SwallowWSRetry"].InnerText.Replace("\r\n", "").Trim();

                    SwallowWSInterval = node["SwallowWSInterval"].InnerText.Replace("\r\n", "").Trim();

                    SwallowWSTimeout = node["SwallowWSTimeout"].InnerText.Replace("\r\n", "").Trim();

                    FileOutPathBackup = node["FileOutPathBackup"].InnerText.Replace("\r\n", "").Trim();
                    if (!Directory.Exists(FileOutPathBackup))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", FileOutPathBackup));
                        Logerr(String.Format("監看FileOutPathBackup目錄失敗！ {0}", FileOutPathBackup));
                        return;
                    }

                    OkCode = node["OkCode"].InnerText.Replace("\r\n", "").Trim();
                    ProblemCode = node["ProblemCode"].InnerText.Replace("\r\n", "").Trim();
                    ConfirmCode = node["ConfirmCode"].InnerText.Replace("\r\n", "").Trim();

                    if (node["InitScanFlag"].InnerText.Replace("\r\n", "").Trim().ToUpper() == "Y")
                    {
                        InitScanFlag = true;
                        InitScanFile();
                    }

                    OFAC_ScanType = node["OFACWS_SCANTYPE"].InnerText.Replace("\r\n", "").Trim();
                    OFACWS_Enable = node["OFACWS"].InnerText.Replace("\r\n", "").Trim();
                    OFAC_ScanType_keep = OFAC_ScanType;
                    try
                    {
                        FileFixLen = int.Parse(node["FileFixLen"].InnerText.Replace("\r\n", "").Trim());
                    }
                    catch
                    {
                        Logerr(String.Format("Get File Fix Len Error！"));
                        return;
                    }

                }
                #endregion

                #region CIF
                LogAdd(String.Format("{0} Loading System CIFSetting", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                XmlNodeList CIFsettingnodes = doc.SelectNodes("//CIFSetting");
                foreach (XmlNode node in CIFsettingnodes)
                {
                    try
                    {
                        CIFFileFixLen = node["CIFFileFixLen"].InnerText.Replace("\r\n", "").Trim();
                    }
                    catch
                    {
                        Logerr(String.Format("Get CIF File Fix Len Error！"));
                        return;
                    }

                    CIFFilePath = node["CIFFilePath"].InnerText.Replace("\r\n", "").Trim();
                    CIFReport = node["CIFReport"].InnerText.Replace("\r\n", "").Trim();
                    CIFReportBackup = node["CIFReportBackup"].InnerText.Replace("\r\n", "").Trim();
                    BNFReport = node["BNFReport"].InnerText.Replace("\r\n", "").Trim();
                    BNFReportBackup = node["BNFReportBackup"].InnerText.Replace("\r\n", "").Trim();
                    CIF_Copy = node["CIF_Copy"].InnerText.Replace("\r\n", "").Trim();
                    CIF_par = node["CIF_par"].InnerText.Replace("\r\n", "").Trim();
                    try
                    {
                        FileSystemWatcher oCIFFileObj = new FileSystemWatcher();
                        oCIFFileObj.InternalBufferSize = 65536;
                        oCIFFileObj.Path = CIFFilePath;
                        oCIFFileObj.Filter = "*.*";
                        oCIFFileObj.NotifyFilter = NotifyFilters.FileName;
                        oCIFFileObj.Created += new FileSystemEventHandler(oCIFFileObj_Created);
                        oCIFFileObj.EnableRaisingEvents = true;
                    }
                    catch (Exception FileINEx)
                    {
                        Logerr(String.Format("監看CIFFile目錄 {0} 失敗！", CIFFilePath), FileINEx);
                        return;
                    }

                    CIFFileErrorPath = node["CIFFileErrorPath"].InnerText.Replace("\r\n", "").Trim();
                    if (!Directory.Exists(CIFFileErrorPath))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", CIFFileErrorPath));
                        Logerr(String.Format("監看CIFFileErrorPath目錄失敗！ {0}", CIFFileErrorPath));
                        return;
                    }


                    CIFFilePathBackup = node["CIFFilePathBackup"].InnerText.Replace("\r\n", "").Trim();
                    if (!Directory.Exists(CIFFilePathBackup))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", CIFFilePathBackup));
                        Logerr(String.Format("監看CIFFilePathBackup目錄失敗！ {0}", CIFFilePathBackup));
                        return;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logerr("Form Load Error", ex);
                return;
            }
        }


        #endregion

        #region InitScanFile
        private void InitScanFile()
        {
            Fortify fortifyService = new Fortify();

            string[] oFiles = System.IO.Directory.GetFiles(fortifyService.PathManipulation(FileInPath));

            for (int i = 0; i < oFiles.Length; i++)
            {
                try
                {
                    FileInfo oFile = new FileInfo(oFiles[i]);
                    string sFile = oFile.Name;
                    string sFileFullPath = oFile.FullName;

                    int iLoop = 1;
                    if (sFile.Length < 20)
                    {
                        Thread.Sleep(2000);
                        File.Copy(sFileFullPath, String.Format("{0}{1}", fortifyService.PathManipulation(FileInErrorPath), fortifyService.PathManipulation(sFile)), true);
                        LogAdd(String.Format("Backup FileIn File {0} Error ,File Len {1} ", fortifyService.PathManipulation(sFile), fortifyService.PathManipulation(sFile.Length.ToString())));
                        return;
                    }

                    while (iLoop > 0 && iLoop < 10)
                    {
                        try
                        {
                            Thread.Sleep(2000);
                            File.Copy(fortifyService.PathManipulation(sFileFullPath), String.Format("{0}{1}", fortifyService.PathManipulation(FileInPathBackup), fortifyService.PathManipulation(sFile)), true);
                            LogAdd(String.Format("Backup FileIn File {0} OK", fortifyService.PathManipulation(sFile)));
                            iLoop = 0;
                        }
                        catch
                        {
                            LogAdd(String.Format("Backup FileIn File {0} Loop {1}", fortifyService.PathManipulation(sFile), fortifyService.PathManipulation(iLoop.ToString())));
                            iLoop++;
                        }
                    }

                    if (iLoop != 0)
                        LogAdd(String.Format("Backup FileIn File Error  {0}", fortifyService.PathManipulation(sFile)));

                    LogAdd(String.Format("Check {0}", fortifyService.PathManipulation(sFile)));

                    string sBranchCode = sFile.Substring(0, BranchCodeLen);
                    bool bFlag = false;
                    for (int j = 0; j < oBranchINList.Count; j++)
                    {
                        if (sBranchCode == oBranchINList[j].BranchCode)
                        {

                            //20170109 edit by danny check CIF file name and move to CIF folder
                            if (sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F" && oBranchINList[i].CIFFilterCMDForF.Length > 0)
                            {
                                //Delete Exist File
                                if (File.Exists(String.Format("{0}{1}", CIFFilePath, sFile)))
                                {
                                    LogAdd(String.Format("File Delete {0}", fortifyService.PathManipulation(sFile)));
                                    File.Delete(String.Format("{0}{1}", fortifyService.PathManipulation(CIFFilePath), fortifyService.PathManipulation(sFile)));
                                }

                                File.Move(sFileFullPath, String.Format("{0}{1}", fortifyService.PathManipulation(CIFFilePath), fortifyService.PathManipulation(sFile)));
                                LogAdd(String.Format("Move {0} To CIF {1}", fortifyService.PathManipulation(sFile), fortifyService.PathManipulation(CIFFilePath)));
                            }
                            else
                            {
                                //Delete Exist File
                                if (File.Exists(String.Format("{0}{1}", oBranchINList[j].Path, sFile)))
                                {
                                    LogAdd(String.Format("File Delete {0}", fortifyService.PathManipulation(sFile)));
                                    File.Delete(String.Format("{0}{1}", fortifyService.PathManipulation(oBranchINList[j].Path), fortifyService.PathManipulation(sFile)));
                                }

                                File.Move(fortifyService.PathManipulation(sFileFullPath), String.Format("{0}{1}", fortifyService.PathManipulation(oBranchINList[j].Path), fortifyService.PathManipulation(sFile)));
                                LogAdd(String.Format("Move {0} To Branch {1}", fortifyService.PathManipulation(sFile), fortifyService.PathManipulation(sBranchCode)));
                            }
                                
                            bFlag = true;
                            break;
                        }
                    }

                    if (!bFlag)
                    {
                        File.Move(fortifyService.PathManipulation(sFileFullPath), String.Format("{0}{1}", fortifyService.PathManipulation(FileInErrorPath), fortifyService.PathManipulation(sFile)));
                        LogAdd(String.Format("Can not Find Branch Code,{0}", sBranchCode));
                    }

                }
                catch (Exception ex)
                {
                    Logerr(String.Format("oFileInObj_Created Error,{0}", oFiles[i]), ex);
                    return;
                }
            }


            for (int i = 0; i < oBranchList.Count; i++)
            {
                string sTypeCode = "";
                switch (oBranchList[i].WatchType)
                {
                    case "OK":
                        sTypeCode = OkCode;
                        break;
                    case "Problem":
                        sTypeCode = ProblemCode;
                        break;
                    case "Confirm":
                        sTypeCode = ConfirmCode;
                        break;
                }
                oProcessObj(oBranchList[i].Path, sTypeCode, oBranchList[i].WatchType);
            }
        }
        #endregion

        #region File In Object Create
        private void oFileInObj_Created(object sender, FileSystemEventArgs e)
        {
            Fortify fortifyService = new Fortify();
            OFAC_ScanType = OFAC_ScanType_keep;
            bool bolMatchresult = false;
            try
            {
                int iLoop = 1;

                string sFile = e.Name.ToString();
                string sFullPath = e.FullPath.ToString();
                string errPath = String.Format("{0}{1}", FileInErrorPath, sFile);
                
                if (sFile.Length != FileFixLen)
                {
                    Thread.Sleep(2000);
                    File.Copy(fortifyService.PathManipulation(sFullPath), fortifyService.PathManipulation(errPath), true);
                    LogAdd(String.Format("Backup FileIn File {0} Error ,File Len {1}", fortifyService.PathManipulation(sFile), fortifyService.PathManipulation(sFile).Length.ToString()));
                    return;
                }

                string file_for_fortify = fortifyService.PathManipulation(sFile);

                while (iLoop > 0 && iLoop < 10)
                {
                    try
                    {
                        Thread.Sleep(2000);
                        string backPath = String.Format("{0}{1}", FileInPathBackup, sFile);
                        string TmpbackPath = String.Format("{0}{1}", FileInPathBackupTmp, sFile);
                        File.Copy(fortifyService.PathManipulation(sFullPath), fortifyService.PathManipulation(backPath), true);
                        File.Copy(fortifyService.PathManipulation(sFullPath), fortifyService.PathManipulation(TmpbackPath), true);
                        LogAdd(String.Format("Backup FileIn File {0} OK", file_for_fortify));
                        iLoop = 0;
                    }
                    catch
                    {
                        LogAdd(String.Format("Backup FileIn File {0} Loop {1}", file_for_fortify, iLoop.ToString()));
                        iLoop++;
                    }
                }

                if (iLoop != 0)
                    LogAdd(String.Format("Backup FileIn File Error  {0}", file_for_fortify));

                LogAdd(String.Format("Check {0}", file_for_fortify));

                string sBranchCode = sFile.Substring(0, BranchCodeLen);
                bool bFlag = false;
                for (int i = 0; i < oBranchINList.Count; i++)
                {
                    if (sBranchCode == oBranchINList[i].BranchCode)
                    {
                        string filePath = "";
                        string path_for_fortify = "";
                        string dest_fortify = "";

                        //MAY. 13, 2021 check if the swift type in the enable list
                        //if not in the enable list then use the prime normal scan.
                        AMLData = new AML_Data(true);
                        DataTable dt_swifttag = AMLData.GetSwiftTag();
                        string strtag = "";
                        int ienable = 0;
                        string strCheck = AMLData.CheckCondidtion(sFile, "", dt_swifttag, ref strtag, ref ienable);
                        if (ienable == 0)
                        {
                            LogAdd("file : " + sFile + ", Enabled the webservice, but do not find the matched swift type in the enabled list!");
                        }

                        //20170608 by danny 呼叫WPEP服務
                        //2020.09 Config scan type
                        if (ienable != 0 && (string.IsNullOrEmpty(OFAC_ScanType) || OFAC_ScanType == "ALL" || OFAC_ScanType == "WS"))
                        {
                            bool bol_ws = SendWPEP(sFullPath, sFile, sBranchCode, ref bolMatchresult);
                            LogAdd("Check Match:" + bolMatchresult + ", Check webservice success:" + bol_ws);

                            path_for_fortify = fortifyService.PathManipulation(sFullPath);
                            //path_for_fortify = fortifyService.PathManipulation(filePath);
                            string path_ok_fortify = "";
                            string path_prb_fortify = "";

                            for (int j = 0; j < oBranchList.Count(); j++)
                            {
                                if (oBranchList[j].BranchCode == sBranchCode)
                                {
                                    switch (oBranchList[j].WatchType)
                                    {
                                        case "OK":
                                            path_ok_fortify = fortifyService.PathManipulation(oBranchList[j].Path + sFile + "-1W.ok");
                                            break;
                                        case "Problem":
                                            path_prb_fortify = fortifyService.PathManipulation(oBranchList[j].Path + sFile + "-1W.prb");
                                            break;
                                    }
                                }
                            }

                            /* Oct.30, 2023 mark for swallow ws that need return.
                            if(oBranchINList[i].WPEPEnable == "Y")
                            {
                                //SEP-17-2021 if call WC1 no need to generate file
                                LogAdd("WC1 No file.");
                            }
                            else
                            {
                            */
                            //JUN-18 2021 check WS fail then go normal scan.
                            if (bol_ws)
                            {
                                if (bolMatchresult)
                                {
                                    //move to prb folder
                                    //Delete Exist File
                                    if (File.Exists(path_prb_fortify))
                                    {
                                        LogAdd(String.Format("File Delete {0}", path_prb_fortify));
                                        File.Delete(path_prb_fortify);
                                    }
                                    LogAdd("Move " + path_for_fortify + " to " + path_prb_fortify);

                                    if (sFile.Substring(4, 1).ToUpper() == "F")
                                    {
                                        //check fedwire
                                        if (OFAC_ScanType == "ALL")
                                        {
                                            //AUG-2021 if All type don't move the file, just copy it.
                                            File.Copy(path_for_fortify, path_prb_fortify);
                                        }
                                        else
                                        {
                                            File.Move(path_for_fortify, path_prb_fortify);
                                        }
                                    }
                                    else
                                    {
                                        CaptureBlock4(path_for_fortify, path_prb_fortify, ScanTag_start, ScanTag_end);
                                    }
                                }
                                else
                                {
                                    //Delete Exist File
                                    if (File.Exists(path_ok_fortify))
                                    {
                                        LogAdd(String.Format("File Delete {0}", path_ok_fortify));
                                        File.Delete(path_ok_fortify);
                                    }
                                    LogAdd("Move " + path_for_fortify + " to " + path_ok_fortify);

                                    if (sFile.Substring(4, 1).ToUpper() == "F")
                                    {
                                        //check fedwire
                                        if (OFAC_ScanType == "ALL")
                                        {
                                            //AUG-2021 if All type don't move the file, just copy it.
                                            File.Copy(path_for_fortify, path_ok_fortify);
                                        }
                                        else
                                        {
                                            File.Move(path_for_fortify, path_ok_fortify);
                                        }
                                    }
                                    else
                                    {
                                        CaptureBlock4(path_for_fortify, path_ok_fortify, ScanTag_start, ScanTag_end);
                                    }


                                    //JUN. 2, delete tmp for ok file
                                    //string TmpbackPath = String.Format("{0}{1}", FileInPathBackupTmp, sFile);
                                    //File.Delete(fortifyService.PathManipulation(TmpbackPath));

                                }
                            }
                            else
                            {
                                if (AMLData.Chk_WSFail(sFile))
                                {
                                    //SEP-8-2021 get ofac case copy file to problem folder
                                    LogAdd("Query and got ofac case!");
                                    //Dec.08.2021 del ofac case! 

                                    if (File.Exists(path_prb_fortify))
                                    {
                                        File.Delete(path_prb_fortify);
                                    }
                                    File.Copy(path_for_fortify, path_prb_fortify);

                                }
                                LogAdd("Call Webservice fail, change Normal scan!");
                                OFAC_ScanType = "NORMAL";
                            }
                            //}
                        }

                        if (string.IsNullOrEmpty(OFAC_ScanType) || OFAC_ScanType == "ALL" || OFAC_ScanType == "NORMAL" || ienable == 0)
                        {
                            if (OFAC_ScanType == "ALL")
                                Thread.Sleep(10000);

                            //20170109 edit by danny check CIF file name and move to CIF folder
                            if (sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F" && oBranchINList[i].CIFFilterCMDForF.Length > 0)
                            {
                                filePath = String.Format("{0}{1}", CIFFilePath, sFile);
                                path_for_fortify = fortifyService.PathManipulation(filePath);
                                //Delete Exist File
                                if (File.Exists(filePath))
                                {
                                    LogAdd(String.Format("CIF File Delete {0}", path_for_fortify));
                                    File.Delete(path_for_fortify);
                                }

                                path_for_fortify = fortifyService.PathManipulation(sFullPath);
                                dest_fortify = fortifyService.PathManipulation(filePath);

                                File.Move(path_for_fortify, dest_fortify);
                                LogAdd(String.Format("Move {0} To CIF {1}", file_for_fortify, path_for_fortify));

                            }
                            else
                            {
                                filePath = String.Format("{0}{1}", oBranchINList[i].Path, sFile);
                                //path_for_fortify = fortifyService.PathManipulation(filePath);
                                path_for_fortify = fortifyService.PathManipulation(sFullPath);
                                dest_fortify = fortifyService.PathManipulation(filePath);

                                //Delete Exist File
                                if (File.Exists(filePath))
                                {
                                    LogAdd(String.Format("File Delete {0}", dest_fortify));
                                    File.Delete(dest_fortify);
                                }

                                if (sFile.Substring(4, 1).ToUpper() == "F" || (sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F"))
                                {
                                    //check fedwire
                                    File.Move(path_for_fortify, dest_fortify);
                                }
                                else
                                {
                                    CaptureBlock4(path_for_fortify, dest_fortify, ScanTag_start, ScanTag_end);
                                }

                                //File.Move(source_fortify, dest_fortify);
                                LogAdd(String.Format("Move {0} To Branch {1}", file_for_fortify, fortifyService.PathManipulation(sBranchCode)));
                            }
                        }

                        bFlag = true;
                        LogAdd("file:" + path_for_fortify + ", Exists:" + File.Exists(path_for_fortify).ToString());
                        if (File.Exists(path_for_fortify))
                        {
                            File.Delete(path_for_fortify);
                        }

                        break;
                    }

                }

                if (!bFlag)
                {
                    File.Move(fortifyService.PathManipulation(sFullPath), fortifyService.PathManipulation(errPath));
                    LogAdd(String.Format("Can not Find Branch Code,{0}", fortifyService.PathManipulation(sBranchCode)));
                }
            }
            catch (Exception ex)
            {
                Logerr(String.Format("oFileInObj_Created Error,{0}, {1}", fortifyService.PathManipulation(e.Name.ToString()), fortifyService.PathManipulation(ex.ToString())));
            }
        }
        #endregion

        #region CIFFile Object Create
        private void oCIFFileObj_Created(object sender, FileSystemEventArgs e)
        {
            Fortify fortifyService = new Fortify();
            try
            {
                int iLoop = 1;

                string sFile = e.Name.ToString();
                string sFullPath = e.FullPath.ToString();
                string errPath = String.Format("{0}{1}", CIFFileErrorPath, sFile);
                bool success = false;

                string path_for_fortify = "";
                string file_for_fortify = "";

                string fullpath_for_fortify = "";
                string errpath_for_fortify = "";
                string bak_for_fortify = "";
                //20210122 add flag for real time ci file.
                string str_cif_f = "";

                //20170109 edit by danny check file length for ci
                if (!string.IsNullOrEmpty(CIFFileFixLen) && !CIFFileFixLen.Contains(sFile.Length.ToString()) && sFile.Length != FileFixLen)
                {
                    Thread.Sleep(2000);

                    fullpath_for_fortify = fortifyService.PathManipulation(sFullPath);
                    errpath_for_fortify = fortifyService.PathManipulation(errPath);

                    File.Copy(fullpath_for_fortify, errpath_for_fortify, true);

                    file_for_fortify = fortifyService.PathManipulation(sFile);

                    LogAdd(String.Format("Backup CIFFile {0} Error ,File Len {1} ", file_for_fortify, sFile.Length.ToString()));
                    return;
                }

                Thread.Sleep(5000);

                //201808 copy CIF or BNF files and scan the copy file with other fileimg
                if (CIF_Copy == "Y")
                {
                    try
                    {
                        string[] array_CIF_par = CIF_par.Trim().Split('|');
                        for (int i = 0; i < array_CIF_par.Count(); i++)
                        {
                            //20190227 檢查0114CI與0114CIF 只有0114CIF才要變成0114CEG
                            string str_bno = "";
                            for (int j = 0; j < oBranchINList.Count; j++)
                            {
                                str_bno = oBranchINList[j].BranchCode;
                                
                                string[] array_CIF_parname = array_CIF_par[i].ToString().Trim().Split(' ');

                                //JUL. 1, 2022 check extension be4 copy

                                if (sFile.ToUpper().Contains(str_bno + array_CIF_parname[0].ToString()) && sFile.ToUpper().Contains(".MM"))
                                {
                                    string str_newsfile = sFile.Replace(array_CIF_parname[0], array_CIF_parname[1]);
                                    File.Copy(fortifyService.PathManipulation(sFullPath), fortifyService.PathManipulation(sFullPath.Replace(sFile, str_newsfile)), true);

                                    LogAdd("Copy CIF file " + fortifyService.PathManipulation(sFullPath) + " to Neg file" + fortifyService.PathManipulation(sFullPath.Replace(sFile, str_newsfile)) + ".");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logerr("Copy CIF/BNF Error:" + ex.ToString());
                    }
                }

                while (!success)
                {
                    try
                    {
                        if (IsFileReady(sFullPath))
                        {
                            string backPath = String.Format("{0}{1}", CIFFilePathBackup, sFile);

                            fullpath_for_fortify = fortifyService.PathManipulation(sFullPath);
                            bak_for_fortify = fortifyService.PathManipulation(backPath);

                            //20170119 polly: 因為CIF一天才一個檔，CIF即時掃描的 就不要備份放在這了
                            //因為CIF目錄中 正常只會出現CIF檔名的，所以判斷檔名是CI 就不做備份

                            file_for_fortify = fortifyService.PathManipulation(sFile);

                            if (sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F")
                            {
                                str_cif_f = sFile.Substring(14, 4).Trim().ToUpper();
                                LogAdd(String.Format("NO Backup SWIFT FileIn File {0}", file_for_fortify));
                            }
                            else
                            {
                                
                                File.Copy(fullpath_for_fortify, bak_for_fortify, true);
                                LogAdd(String.Format("Backup CIFFileIn File {0} OK", file_for_fortify));
                            }
                            iLoop = 0;
                            success = true;
                        }
                        else
                        {
                            LogAdd(String.Format("Backup CIFFile File {0} is not ready (Loop #{1})", fullpath_for_fortify, fortifyService.PathManipulation(iLoop.ToString())));
                            iLoop++;
                            Thread.Sleep(20000);
                        }
                    }
                    catch
                    {
                        LogAdd(String.Format("Backup CIFFile File {0} Loop {1}", fortifyService.PathManipulation(sFile), iLoop.ToString()));
                        iLoop++;
                    }
                }

                file_for_fortify = fortifyService.PathManipulation(sFile);

                if (iLoop != 0)
                    LogAdd(String.Format("Backup CIFFile File Error  {0}", file_for_fortify));

                LogAdd(String.Format("CIF Check {0}", file_for_fortify));

                string sBranchCode = sFile.Substring(0, BranchCodeLen);
                bool bFlag = false;

                for (int i = 0; i < oBranchINList.Count; i++)
                {
                    string[] array_mm = oBranchINList[i].CIFmmFilterCMD.Trim().Split('|');
                    string[] array_cif = oBranchINList[i].CIFFilterCMD.Trim().Split('|');

                    if (sBranchCode == oBranchINList[i].BranchCode)
                    {
                        /*
                        //20171213 edit by danny for div fileimage
                        if (sFullPath.ToUpper().Contains(".MM") && oBranchINList[i].DivImage == "Y")
                        {
                            for (int j = 0; j < array_div.Count(); j++)
                            {
                                File.Copy(fortifyService.PathManipulation(sFullPath), fortifyService.PathManipulation(sFullPath.ToUpper().Replace(".MM", array_div[j].Trim().Split(' ')[2])), true);
                            }
                        }
                        else if (sFullPath.ToUpper().Contains(".MM") && oBranchINList[i].DivImage != "Y")
                        {
                            for (int j = 0; j < array_div.Count(); j++)
                            {
                                File.Copy(fortifyService.PathManipulation(sFullPath), fortifyService.PathManipulation(sFullPath.ToUpper().Replace(".MM", array_div[j].Trim().Split(' ')[2])), true);
                            }
                        }
                        */

                        //Delete Exist File
                        string delFile = String.Format("{0}{1}", oBranchINList[i].CIFIN, sFile);

                        file_for_fortify = fortifyService.PathManipulation(delFile);

                        if (File.Exists(delFile))
                        {
                            LogAdd(String.Format("CIFFile Delete {0}", file_for_fortify));
                            File.Delete(file_for_fortify);
                        }

                        iLoop = 1;
                        while (iLoop > 0 && iLoop < 10)
                        {
                            try
                            {
                                Thread.Sleep(2000);
                                string cifinFile = String.Format("{0}{1}", oBranchINList[i].CIFIN, sFile);

                                string cifinfile_for_fortify = fortifyService.PathManipulation(cifinFile);

                                fullpath_for_fortify = fortifyService.PathManipulation(sFullPath);

                                File.Move(fullpath_for_fortify, cifinfile_for_fortify);
                                LogAdd(String.Format("Move CIF {0} To {1}", fullpath_for_fortify, cifinfile_for_fortify));

                                //去掉欄位內容中的2Byte空白(__)
                                String[] CIF = File.ReadAllLines(cifinfile_for_fortify, Encoding.Default);
                                List<string> NewCIF = new List<string>();

                                List<string> ParseCIF = new List<string>();

                                foreach (string item in CIF)
                                {
                                    ParseCIF = Get_cutstring(item.Replace("  ", ""));
                                    foreach (string strfill in ParseCIF)
                                    {

                                        string string2Fill = strfill;
                                        if (str_cif_f != "" && str_cif_f != "CIFD")
                                        {
                                            string2Fill = sFile + strfill.Substring(strfill.IndexOf("|"));

                                        }
                                        NewCIF.Add(string2Fill);
                                    }
                                }

                                File.WriteAllLines(cifinfile_for_fortify, NewCIF.ToArray(), Encoding.Default);
                                iLoop = 0;
                            }
                            catch
                            {
                                LogAdd(String.Format("Move CIFFile File {0} Loop {1}", fullpath_for_fortify, iLoop.ToString()));
                                iLoop++;
                            }
                        }
                        bFlag = true;

                        //執行BatchFilter
                        StringBuilder s = new StringBuilder();
                        Process p = new Process();
                        string[] g = oBranchINList[i].CIFFilterCMD.Trim().Split(' ');

                        //20171213 add by danny for scan div fileimage
                        for (int j = 0; j < array_cif.Count(); j++)
                        {
                            if (sFile.ToUpper().Contains(array_cif[j].Trim().Split(' ')[2].ToUpper()) && sFile.ToUpper().Contains(array_cif[j].Trim().Split(' ')[3].ToUpper()))
                            {
                                g = array_cif[j].Trim().Split(' ');
                                LogAdd(String.Format("CIFFile For different CIF, count:{0}, file:{1}", j + 1, file_for_fortify));
                            }
                        }

                        //20171213 add by danny for scan div fileimage
                        for (int j = 0; j < array_mm.Count(); j++)
                        {
                            if (sFile.ToUpper().Contains(array_mm[j].Trim().Split(' ')[2].ToUpper()) && sFile.ToUpper().Contains(array_mm[j].Trim().Split(' ')[3].ToUpper()))
                            {
                                g = array_mm[j].Trim().Split(' ');
                                LogAdd(String.Format("CIFFile For MM CIF, count:{0}, file:{1}", j + 1, file_for_fortify));
                            }
                        }

                        //20170109 add by danny check to use other ini setting
                        if (sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F" && oBranchINList[i].CIFFilterCMDForF.Length > 0)
                        {
                            //20201224 add by danny for real-time ci select different ini file.
                            string[] CIF_realtime = oBranchINList[i].CIFFilterCMDForF.Trim().Split('|');

                            for (int fc = 0; fc < CIF_realtime.Length; fc++)
                            {
                                string[] CIF_realtimes = CIF_realtime[fc].Trim().Split(' ');
                                if (CIF_realtimes[2].Trim().ToUpper() == sFile.Substring(14, 4).Trim().ToUpper())
                                {
                                    LogAdd(String.Format("Check config=" + CIF_realtimes[2].Trim().ToUpper() + ", Check file=" + sFile.Substring(14, 4).Trim().ToUpper()));
                                    g = CIF_realtimes;
                                }
                            }
                            LogAdd(String.Format("CIFFile ForF {0}", file_for_fortify));

                            //20170316 polly think we should write the sFile path down to event logs but still not implement
                        }

                        ProcessStartInfo pInfo = new ProcessStartInfo(g[0]);

                        pInfo.Arguments = g[1];
                        pInfo.UseShellExecute = false;
                        pInfo.RedirectStandardOutput = true;
                        pInfo.CreateNoWindow = true;

                        LogAdd(String.Format("Execute Batchfilter: {0} {1}", g[0], g[1]));

                        p.StartInfo = pInfo;
                        p.Start();

                        using (StreamReader myStreamReader = p.StandardOutput)
                        {
                            string result = "";

                            result = myStreamReader.ReadToEnd();
                            p.WaitForExit();
                            p.Close();
                            
                        }

                        file_for_fortify = fortifyService.PathManipulation(sFile);

                        LogAdd(String.Format("Scan CIFFile File {0} Completed", file_for_fortify));

                        foreach (FileInfo f in new DirectoryInfo(oBranchINList[i].CIFIN).GetFiles())
                        {
                            string exten = f.Extension.Replace(".", "").ToLower();
                            string outbakpath_fortify = "";

                            if (exten == "out")
                            {
                                //201801 有多個image時，掃黑結果會有多個.out檔 ，在搬到report目錄下，需分成不同副檔名
                                //暫時不需要，需要再改架構
                                /*if (oBranchINList[i].DivImage == "Y")
                                {
                                    for (int j = 0; j < array_div.Count(); j++)
                                    {
                                        if (f.Name.ToUpper().Contains(array_div[j].Trim().Split(' ')[2].Replace(".","")))
                                        {
                                            CIFReport = CIFReport.Replace("ZXCIFBAT.TXT", "ZXCIF" + array_div[j].Trim().Split(' ')[2].Substring(1, 1) + "BAT.TXT");
                                            CIFReportBackup = CIFReportBackup.Replace("ZXCIFBAT.TXT", "ZXCIF" + array_div[j].Trim().Split(' ')[2].Substring(1, 1) + "BAT.TXT");

                                            LogAdd("change ZXCIF file path=" + CIFReport);
                                            LogAdd("change ZXCIF backup file path=" + CIFReportBackup);
                                        }
                                    }
                                }*/

                                for (int j = 0; j < array_cif.Count(); j++)
                                {
                                    if (sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F" && oBranchINList[i].CIFFilterCMDForF.Length > 0)
                                    {
                                        LogAdd("CI file do not create report.");
                                        continue;
                                    }

                                    //201809, 修改讀取報表參數的架構
                                    //<CIFReport>A,A_back|B,B_back|C,C_back|....</CIFReport>
                                    string CIFReportP = "";
                                    string[] CIFReport_path = CIFReport.Trim().Split('|');
                                    foreach(string CIF_R_path in CIFReport_path)
                                    {
                                        CIFReportP = CIF_R_path.Trim().Split(',')[0];
                                        CIFReportBackup = CIF_R_path.Trim().Split(',')[1];

                                        if (sFile.ToUpper().Contains(array_cif[j].Trim().Split(' ')[2].ToUpper()) && CIFReportP.ToUpper().Contains(array_cif[j].Trim().Split(' ')[2].ToUpper()))
                                        {
                                            /* Nov.09 2022 mark to avoid IO error
                                            //201712 ZXCIFBAT 報表備份
                                            if (!File.Exists(CIFReportP))
                                            {
                                                f.CopyTo(CIFReportP, true);
                                            }
                                            else
                                            {
                                                if (!File.Exists(CIFReportBackup + "." + DateTime.Now.ToString("dd")))
                                                {
                                                    File.Move(CIFReportP, fortifyService.PathManipulation(CIFReportBackup + "." + DateTime.Now.ToString("dd")));
                                                    LogAdd("EOM Report Backup:" + fortifyService.PathManipulation(CIFReportBackup + "." + DateTime.Now.ToString("dd")));
                                                }
                                                else if (File.Exists(CIFReportP))
                                                {
                                                    File.Move(CIFReportP, fortifyService.PathManipulation(CIFReportBackup + "." + DateTime.Now.ToString("dd") + "." + DateTime.Now.ToString("mmss")));
                                                    LogAdd("EOM Report Backup:" + fortifyService.PathManipulation(CIFReportBackup + "." + DateTime.Now.ToString("dd") + "." + DateTime.Now.ToString("mmss")));
                                                }

                                                f.CopyTo(CIFReportP, true);
                                            }
                                            */
                                            //Nov.09 2022 to avoid IO error, copy to D:\ftproot\ReconReport\ZXCIFBAT.TXT
                                            //                                   and D:\ftproot\ReconReport\Backup\ZXCIFBAT.TXT.dd.mmss

                                            f.CopyTo(CIFReportP, true);
                                            f.CopyTo(fortifyService.PathManipulation(CIFReportBackup + "." + DateTime.Now.ToString("dd") + "." + DateTime.Now.ToString("mmss")), true);

                                            LogAdd(String.Format("EOM Report {0} Created", CIFReportP));
                                            LogAdd("EOM Report Backup:" + fortifyService.PathManipulation(CIFReportBackup + "." + DateTime.Now.ToString("dd") + "." + DateTime.Now.ToString("mmss")));
                                        }
                                    }

                                    /*
                                    if (sFile.ToUpper().Contains(array_cif[j].Trim().Split(' ')[2].ToUpper()) && CIFReport.ToUpper().Contains(array_cif[j].Trim().Split(' ')[2].ToUpper()))
                                    {
                                        //201712 ZXCIFBAT 報表備份
                                        if (!File.Exists(CIFReport))
                                        {
                                            f.CopyTo(CIFReport, true);
                                        }
                                        else
                                        {
                                            if (!File.Exists(CIFReportBackup + "." + DateTime.Now.ToString("dd")))
                                            {
                                                File.Move(CIFReport, fortifyService.PathManipulation(CIFReportBackup + "." + DateTime.Now.ToString("dd")));
                                                LogAdd("EOM CIF Report Backup:" + fortifyService.PathManipulation(CIFReportBackup + "." + DateTime.Now.ToString("dd")));
                                            }
                                            else if (File.Exists(CIFReport))
                                            {
                                                File.Move(CIFReport, fortifyService.PathManipulation(CIFReportBackup + "." + DateTime.Now.ToString("dd") + "." + DateTime.Now.ToString("mmss")));
                                                LogAdd("EOM CIF Report Backup:" + fortifyService.PathManipulation(CIFReportBackup + "." + DateTime.Now.ToString("dd") + "." + DateTime.Now.ToString("mmss")));
                                            }

                                            f.CopyTo(CIFReport, true);
                                        }

                                        LogAdd(String.Format("EOM CIF Report {0} Created", CIFReport));
                                    }
                                    else if (sFile.ToUpper().Contains(array_cif[j].Trim().Split(' ')[2].ToUpper()) && BNFReport.ToUpper().Contains(array_cif[j].Trim().Split(' ')[2].ToUpper()))
                                    {
                                        //201807 ZXBNFBAT 報表備份
                                        if (!File.Exists(BNFReport))
                                        {
                                            f.CopyTo(BNFReport, true);
                                        }
                                        else
                                        {
                                            if (!File.Exists(BNFReportBackup + "." + DateTime.Now.ToString("dd")))
                                            {
                                                File.Move(BNFReport, fortifyService.PathManipulation(BNFReportBackup + "." + DateTime.Now.ToString("dd")));
                                                LogAdd("EOM BNF Report Backup:" + fortifyService.PathManipulation(BNFReportBackup + "." + DateTime.Now.ToString("dd")));
                                            }
                                            else if (File.Exists(BNFReport))
                                            {
                                                File.Move(BNFReport, fortifyService.PathManipulation(BNFReportBackup + "." + DateTime.Now.ToString("dd") + "." + DateTime.Now.ToString("mmss")));
                                                LogAdd("EOM BNF Report Backup:" + fortifyService.PathManipulation(BNFReportBackup + "." + DateTime.Now.ToString("dd") + "." + DateTime.Now.ToString("mmss")));
                                            }

                                            f.CopyTo(BNFReport, true);
                                        }

                                        LogAdd(String.Format("EOM BNF Report {0} Created", BNFReport));
                                    }
                                    */
                                }
                                

                                path_for_fortify = fortifyService.PathManipulation(CIFFilePathBackup);

                                file_for_fortify = fortifyService.PathManipulation(sFile);

                                FileInfo backup = new FileInfo(string.Format("{0}\\{1}.out", path_for_fortify, file_for_fortify));

                                //20170119 polly: 因為CIF一天才一個檔，CIF即時掃描的 就不要備份放在這了
                                //改將out檔丟到Result目錄去
                                if (sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F" && oBranchINList[i].CIFFilterCMDForF.Length > 0)
                                {
                                    outbakpath_fortify = fortifyService.PathManipulation(FileOutPathBackup);
                                    backup = new FileInfo(string.Format("{0}\\{1}.out", outbakpath_fortify, file_for_fortify));
                                }

                                string strback = backup.FullName;
                                //2021 Jan. Get the original CI file to send back to the SWIFT OK or Problem folder
                                // for returning to core banking system.
                                string str_original_CI = FileInPathBackup + sFile;

                                if (!backup.Directory.Exists)
                                    backup.Directory.Create();

                                if (backup.Exists)
                                {
                                    //20170313 mark by danny because its not working 
                                    //backup.MoveTo(backup.Name + DateTime.Now.ToString("yyyyMMddHHmmss"));
                                    strback = backup.FullName + DateTime.Now.ToString("yyyyMMddHHmmss");
                                }
                                else
                                {
                                    if (System.IO.File.Exists(backup.FullName.Replace(".out",".prb")))
                                    {
                                        strback = backup.FullName.Replace(".out", ".prb") + DateTime.Now.ToString("yyyyMMddHHmmss");
                                    }
                                    if (System.IO.File.Exists(backup.FullName.Replace(".out", ".ok")))
                                    {
                                        strback = backup.FullName.Replace(".out", ".ok") + DateTime.Now.ToString("yyyyMMddHHmmss");
                                    }
                                }

                                LogAdd("file_name:" + strback);
                                //f.MoveTo(strback);
                                //MAY.19, 2023 to avoid file lock issue, change move to copy
                                f.CopyTo(strback, true);

                                DelFile(f);

                                //20170310 by danny 將ini裡的OutputAllWithSystemReference = true
                                //產出的.out檔會出現:RECONEX - SWIFT-03102017-165203-0~2~0114SWIFT~2~SWIFT~0114~AML~SYSTEM~03/10/2017 16:52:03
                                //利用這個out檔來抓matchcount
                                if (Matchword.Length > 0 && sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F" && oBranchINList[i].CIFFilterCMDForF.Length > 0)
                                {
                                    string strMatchCount = "";
                                    string line = "";
                                    System.IO.StreamReader file = new System.IO.StreamReader(strback);
                                    while ((line = file.ReadLine()) != null)
                                    {
                                        LogAdd("read:" + fortifyService.PathManipulation(line));
                                        if (line.LastIndexOf(Matchword) > -1)
                                        {
                                            if (line.Split('~').Count() > 0)
                                            {
                                                strMatchCount = line.Split('~')[1];
                                                break;
                                            }
                                            else
                                            {
                                                strMatchCount = "0";
                                            }
                                        }
                                    }
                                    LogAdd("Matchcount:" + fortifyService.PathManipulation(strMatchCount));
                                    file.Close();
                                    string pathdest = "";
                                    string pathsource = "";
                                    pathsource = fortifyService.PathManipulation(str_original_CI);
                                    LogAdd("ori=" + pathsource + ",to=" + oBranchList[1].Path + sFile);

                                    if (strMatchCount != "0")
                                    {
                                        //2021 Jan. Delete for duplicate
                                        //System.IO.File.Move(strback, strback.Replace(".out", ".prb"));
                                        pathdest = fortifyService.PathManipulation(oBranchList[i * 3 + 1].Path + sFile + "-1.prb");
                                        //need to calculate the index from config
                                        System.IO.File.Copy(pathsource, pathdest, true);
                                    }
                                    else
                                    {
                                        //2021 Jan. Delete for duplicate
                                        //System.IO.File.Move(strback, strback.Replace(".out", ".ok"));
                                        pathdest = fortifyService.PathManipulation(oBranchList[i * 3].Path + sFile + "-1.ok");
                                        //need to calculate the index from config
                                        System.IO.File.Copy(pathsource, pathdest, true);
                                    }

                                    //2021 Jan. Delete for duplicate
                                    System.IO.File.Delete(strback);

                                    AMLData = new AML_Data();

                                    if (AMLData.InsertRecon("SWIFT", "1", strMatchCount, sBranchCode, "AML", String.Format(("{0}-1.pri"), sFile), String.Format(("{0}-1.pri"), sFile), "SYSTEM"))
                                    {
                                        LogAdd("Recontable INS Sucess");
                                    }
                                    else
                                    {
                                        LogAdd("Recontable INS Fail");
                                    }
                                }
                            }
                            else if (exten == "in")
                            {
                                //f.Delete();
                                //MAY.19, 2023 to avoid file lock issue, change move to copy
                                DelFile(f);
                            }
                            else
                            {
                                //20170109 add by danny 
                                //f.Delete();
                                //MAY.19, 2023 to avoid file lock issue, change move to copy
                                DelFile(f);
                            }
                        }

                        break;
                    }
                }

                if (!bFlag)
                {
                    fullpath_for_fortify = fortifyService.PathManipulation(sFullPath);
                    errpath_for_fortify = fortifyService.PathManipulation(errPath);
                    File.Move(fullpath_for_fortify, errpath_for_fortify);
                    LogAdd(String.Format("Can't Find CIF Branch Code,{0}", fortifyService.PathManipulation(sBranchCode)));
                }

            }
            catch (Exception ex)
            {
                Logerr(String.Format("oCIFFileObj_Created Error,{0}", fortifyService.PathManipulation(e.Name.ToString())), ex);
            }
        }
        #endregion

        #region ProcessObj_Created
        private void oProcessObj(string sFilePath, string sTypeCode, string sTypeName)
        {
            try
            {
                Fortify fortifyService = new Fortify();
                string[] oFiles = System.IO.Directory.GetFiles(fortifyService.PathManipulation(sFilePath));

                for (int iFileList = 0; iFileList < oFiles.Length; iFileList++)
                {
                    FileInfo oFile = new FileInfo(oFiles[iFileList]);
                    string sFile = oFile.Name;
                    string sFileFullName = oFile.FullName;
                    try
                    {

                        if (sFile.Length < 20)
                        {
                            LogAdd(String.Format("File {0} Error ,File Len {1} Type {2}", sFile, sFile.Length.ToString(), sTypeName));
                            return;
                        }

                        int iLoop = 1;
                        LogAdd(String.Format("Process Text File {0}", sFileFullName));

                        string sStrValue = "";
                        string sORITime = sFile.Substring(14, 6);
                        string sCRTTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                        iLoop = 1;
                        while (iLoop > 0 && iLoop < 10)
                        {
                            iLoop = GetSTRData(FileInPathBackup + sFile.Substring(0, sFile.IndexOf("-")), ref sStrValue, iLoop);
                        }

                        sStrValue = sStrValue.Substring(2, 24) + sTypeCode + sORITime + sCRTTime + "  ";
                        string sBranchCode = sStrValue.Substring(2, 4);

                        //Oct.23, 2023 add for Swallow WS parallel mode
                        iLoop = 1;

                        while (iLoop > 0 && iLoop < 4 && !string.IsNullOrEmpty(FileOutPath))
                        {
                            //Oct.20, 2023 add for MX format
                            //Oct.25, 2023 F format CI file still use winswiftaml servive
                            if (sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F")
                            {
                                iLoop = AppendData(GetOutputFileName(FileOutPath, sBranchCode), sStrValue, iLoop);
                            }
                            else
                            {
                                sBranchCode = sFile.Substring(0, 4);
                                iLoop++;
                            }
                        }

                        if (iLoop > 1 && !string.IsNullOrEmpty(FileOutPath) && sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F")
                            LogAdd(String.Format("Process Append File IO Error {0}", sBranchCode));

                        //Oct.18,2023 add for Swallow WS
                        sStrValue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + sFile.Substring(0, sFile.IndexOf("-")) + sTypeCode;
                        iLoop = 1;
                        while (iLoop > 0 && iLoop < 4 && !string.IsNullOrEmpty(WSFileOutPath))
                        {
                            if (sFile.Substring(4, 2).ToUpper() != "CI")
                            {
                                sBranchCode = sFile.Substring(0, 4);
                                iLoop = AppendData(GetOutputFileName(WSFileOutPath, sBranchCode), sStrValue, iLoop);
                            }
                            else
                            {
                                iLoop++;
                            }
                        }


                        LogAdd(String.Format("Process Append File {0}", sBranchCode));
                    }
                    catch (Exception ex)
                    {
                        Logerr(String.Format("oProcessObj_Created Error,{0}", sFileFullName), ex);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Logerr("oProcessObj", ex);
            }
        }
        #endregion

        #region OK
        private void oOKObj_Created(object sender, FileSystemEventArgs e)
        {
            Fortify fortifyService = new Fortify();
            try
            {
                string sFile = e.Name.ToString();

                string path_dest = "";
                string path_source = "";

                string file_for_fortify = fortifyService.PathManipulation(sFile);

                if (sFile.Length < 20)
                {
                    LogAdd(String.Format("OK File {0} Error ,File Len {1}", file_for_fortify, file_for_fortify.Length.ToString()));
                    return;
                }

                int iLoop = 1;

                LogAdd(String.Format("Check OK file {0}", file_for_fortify));

                string sStrValue = "";
                string sORITime = sFile.Substring(14, 6);
                string sCRTTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                iLoop = 1;
                while (iLoop > 0 && iLoop < 10)
                {
                    iLoop = GetSTRData(FileInPathBackupTmp + sFile.Substring(0, sFile.IndexOf("-")), ref sStrValue, iLoop);
                }

                sStrValue = sStrValue.Substring(2, 24) + OkCode + sORITime + sCRTTime + "  ";
                string sBranchCode = sStrValue.Substring(2, 4);

                //Oct.23, 2023 add for Swallow WS parallel mode
                iLoop = 1;

                while (iLoop > 0 && iLoop < 4 && !string.IsNullOrEmpty(FileOutPath))
                {
                    //Oct.20, 2023 add for MX format
                    //Oct.25, 2023 F format CI file still use winswiftaml servive
                    if (sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F")
                    {
                        iLoop = AppendData(FileOutPath + sFile, sStrValue, iLoop);
                    }
                    else
                    {
                        sBranchCode = sFile.Substring(0, 4);
                        iLoop++;
                    }
                }

                file_for_fortify = fortifyService.PathManipulation(sBranchCode);

                if (iLoop != 0 && !string.IsNullOrEmpty(FileOutPath) && sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F")
                    LogAdd(String.Format("Process Append File IO Error {0}", file_for_fortify));

                //Oct.18,2023 add for Swallow WS
                sStrValue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + sFile.Substring(0, sFile.IndexOf("-")) + "N";
                iLoop = 1;
                while (iLoop > 0 && iLoop < 4 && !string.IsNullOrEmpty(WSFileOutPath))
                {
                    if (sFile.Substring(4, 2).ToUpper() != "CI")
                    {
                        sBranchCode = sFile.Substring(0, 4);
                        iLoop = AppendData(WSFileOutPath + sFile, sStrValue, iLoop);
                    }
                    else
                    {
                        iLoop++;
                    }
                }

                LogAdd(String.Format("Process Append File {0}", file_for_fortify));
                
                //Oct.19,2023 add for swallow ws

                if (!string.IsNullOrEmpty(FileOutPath) && File.Exists(FileOutPath + sFile))
                {
                    string outFile = FileOutPath + sFile;
                    string outFileBackup = FileOutPathBackup + sFile;

                    path_dest = fortifyService.PathManipulation(outFileBackup);

                    path_source = fortifyService.PathManipulation(outFile);

                    File.Copy(path_source, path_dest, true);
                }

                //May.22, 2023 call the swallow ws to return the ofac case final status.
                CaseApproveToSwallow(sFile.ToLower().Replace(".ok", ".pri"), sBranchCode);

                //JUN. 2, delete tmp file for ok.
                if (!string.IsNullOrEmpty(OFAC_ScanType) && OFAC_ScanType.ToUpper() != "ALL")
                {
                    File.Delete(fortifyService.PathManipulation(FileInPathBackupTmp + sFile.Substring(0, sFile.IndexOf("-"))));
                }
            }
            catch (Exception ex)
            {
                Logerr(String.Format("oOKObj_Created Error,{0}", fortifyService.PathManipulation(e.Name.ToString())), ex);
                return;
            }
        }
        #endregion

        #region Problem
        private void oProblemObj_Created(object sender, FileSystemEventArgs e)
        {
            Fortify fortifyService = new Fortify();

            try
            {
                string sFile = e.Name.ToString();

                string file_for_fostify = "";

                string source_fortify = "";
                string dest_fortify = "";

                file_for_fostify = fortifyService.PathManipulation(sFile);

                if (sFile.Length < 20)
                {
                    LogAdd(String.Format("Problem File {0} Error ,File Len {1}", file_for_fostify, file_for_fostify.Length.ToString()));
                    return;
                }

                int iLoop = 1;

                LogAdd(String.Format("Check Problem file {0}", file_for_fostify));

                string sStrValue = "";
                string sORITime = sFile.Substring(14, 6);
                string sCRTTime = DateTime.Now.ToString("yyyyMMddHHmmss");

                iLoop = 1;
                while (iLoop > 0 && iLoop < 10)
                {
                    iLoop = GetSTRData(FileInPathBackup + sFile.Substring(0, sFile.IndexOf("-")), ref sStrValue, iLoop);
                }

                sStrValue = sStrValue.Substring(2, 24) + ProblemCode + sORITime + sCRTTime + "  ";
                string sBranchCode = sStrValue.Substring(2, 4);

                //Oct.23, 2023 add for Swallow WS parallel mode
                iLoop = 1;

                while (iLoop > 0 && iLoop < 4 && !string.IsNullOrEmpty(FileOutPath))
                {
                    //Oct.20, 2023 add for MX format
                    //Oct.25, 2023 F format CI file still use winswiftaml servive
                    if (sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F")
                    {
                        iLoop = AppendData(FileOutPath + sFile, sStrValue, iLoop);
                    }
                    else
                    {
                        sBranchCode = sFile.Substring(0, 4);
                        iLoop++;
                    }
                }

                file_for_fostify = fortifyService.PathManipulation(sBranchCode);

                if (iLoop != 0 && !string.IsNullOrEmpty(FileOutPath) && sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F")
                    LogAdd(String.Format("Process Append File IO Error {0}", file_for_fostify));

                //Oct.18,2023 add for Swallow WS
                //Oct.25, 2023 F format CI file still use winswiftaml servive
                sStrValue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + sFile.Substring(0, sFile.IndexOf("-")) + "Y";
                iLoop = 1;
                while (iLoop > 0 && iLoop < 4 && !string.IsNullOrEmpty(WSFileOutPath))
                {
                    if (sFile.Substring(4, 2).ToUpper() != "CI")
                    {
                        sBranchCode = sFile.Substring(0, 4);
                        iLoop = AppendData(WSFileOutPath + sFile, sStrValue, iLoop);
                    }
                    else
                    {
                        iLoop++;
                    }
                }

                LogAdd(String.Format("Process Append File {0}", file_for_fostify));
                
                if(!string.IsNullOrEmpty(FileOutPath) && File.Exists(FileOutPath + sFile))
                {
                    string outFile = FileOutPath + sFile;
                    string outFileBackup = FileOutPathBackup + sFile;

                    source_fortify = fortifyService.PathManipulation(outFile);
                    dest_fortify = fortifyService.PathManipulation(outFileBackup);

                    File.Copy(source_fortify, dest_fortify, true);
                }
     
            }
            catch (Exception ex)
            {
                Logerr(String.Format("oProblemObj_Created Error,{0}", fortifyService.PathManipulation(e.Name.ToString())), ex);
                return;
            }
        }
        #endregion

        #region Confirm
        private void oConfirmObj_Created(object sender, FileSystemEventArgs e)
        {
            Fortify fortifyService = new Fortify();

            try
            {
                string sFile = e.Name.ToString();

                string file_for_fortify = fortifyService.PathManipulation(sFile);

                if (sFile.Length < 20)
                {
                    LogAdd(String.Format("Confirm File {0} Error ,File Len {1}", file_for_fortify, file_for_fortify.Length.ToString()));
                    return;
                }
                int iLoop = 1;


                LogAdd(String.Format("Check Confirm file {0}", file_for_fortify));

                string sStrValue = "";
                string sORITime = sFile.Substring(14, 6);
                string sCRTTime = DateTime.Now.ToString("yyyyMMddHHmmss");

                iLoop = 1;
                while (iLoop > 0 && iLoop < 10)
                {
                    iLoop = GetSTRData(FileInPathBackupTmp + sFile.Substring(0, sFile.IndexOf("-")), ref sStrValue, iLoop);
                }

                sStrValue = sStrValue.Substring(2, 24) + ConfirmCode + sORITime + sCRTTime + "  ";

                string sBranchCode = sStrValue.Substring(2, 4);

                //Oct.23, 2023 add for Swallow WS parallel mode
                iLoop = 1;

                while (iLoop > 0 && iLoop < 10 && !string.IsNullOrEmpty(FileOutPath))
                {
                    //Oct.20, 2023 add for MX format
                    //Oct.25, 2023 F format CI file still use winswiftaml servive
                    if (sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F")
                    {
                        iLoop = AppendData(FileOutPath + sFile, sStrValue, iLoop);
                    }
                    else
                    {
                        sBranchCode = sFile.Substring(0, 4);
                        iLoop++;
                    }
                }

                if (iLoop != 0 && !string.IsNullOrEmpty(FileOutPath) && sFile.Substring(4, 2).ToUpper() == "CI" && sFile.Substring(6, 1).ToUpper() != "F")
                    LogAdd(String.Format("Process Append File IO Error {0}", fortifyService.PathManipulation(sBranchCode)));

                //Oct.18,2023 add for Swallow WS
                sStrValue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + sFile.Substring(0, sFile.IndexOf("-")) + "C";
                iLoop = 1;
                while (iLoop > 0 && iLoop < 10 && !string.IsNullOrEmpty(WSFileOutPath))
                {
                    if (sFile.Substring(4, 2).ToUpper() != "CI")
                    {
                        sBranchCode = sFile.Substring(0, 4);
                        iLoop = AppendData(WSFileOutPath + sFile, sStrValue, iLoop);
                    }
                    else
                    {
                        iLoop++;
                    }
                }

                LogAdd(String.Format("Process Append File {0}", fortifyService.PathManipulation(sBranchCode)));
                
                if(!string.IsNullOrEmpty(FileOutPath) && File.Exists(FileOutPath + sFile))
                {
                    string outFile = FileOutPath + sFile;
                    string outFileBackup = FileOutPathBackup + sFile;
                    File.Copy(fortifyService.PathManipulation(outFile), fortifyService.PathManipulation(outFileBackup), true);
                }

                //May.22, 2023 call the swallow ws to return the ofac case final status.
                CaseApproveToSwallow(sFile.ToLower().Replace(".cnf", ".pri"), sBranchCode);

                //JUN. 2, delete tmp file for ok.
                //File.Delete(fortifyService.PathManipulation(FileInPathBackupTmp + sFile.Substring(0, sFile.IndexOf("-"))));
            }
            catch (Exception ex)
            {
                Logerr(String.Format("oConfirmObj_Created Error,{0}", fortifyService.PathManipulation(e.Name.ToString())), ex);
                return;
            }
        }
        #endregion

        #region GetSTRData
        private int GetSTRData(string sFile, ref string sStrValue, int iLoop)
        {
            if (iLoop != 1)
                Thread.Sleep(5000);

            Fortify fortifyService = new Fortify();
            try
            {
                string sFilePath = sFile;
                sStrValue = System.IO.File.ReadAllText(fortifyService.PathManipulation(sFilePath), System.Text.Encoding.Default);
                return 0;
            }
            catch (Exception ex)
            {
                iLoop++;
                return iLoop;
            }
        }
        #endregion

        #region AppendData
        private int AppendData(string sFile, string sStrValue, int iLoop)
        {
            Fortify fortifyService = new Fortify();

            if (iLoop != 1)
                Thread.Sleep(5000);

            try
            {
                string sFilePath = sFile;
                string sValue = sStrValue;
                File.WriteAllText(fortifyService.PathManipulation(sFilePath), sValue + "\r\n");
                return 0;
            }
            catch (Exception ex)
            {
                iLoop++;
                return iLoop;
            }
        }
        #endregion

        #region GetOutputFileName
        private string GetOutputFileName(string sPath, string sBankCode)
        {
            for (int i = 0; i < oBranchINList.Count; i++)
            {
                if (oBranchINList[i].BranchCode == sBankCode)
                {
                    return sPath + oBranchINList[i].OutputFormat;
                }
            }

            return "";
        }
        #endregion

        private void CaptureBlock4(string SourcePath, string DestPath, string BeginTags, string EndTags)
        {
            try
            {
                Fortify fortify = new Fortify();
                string source_fortify = fortify.PathManipulation(SourcePath);
                string dest_fortify = fortify.PathManipulation(DestPath);

                //2021, Jan. capture swift tag:4 
                String[] filedata = File.ReadAllLines(source_fortify, Encoding.Default);
                List<string> NewCIF = new List<string>();

                List<string> ParseCIF = new List<string>();

                bool bol_capture_start = false;

                foreach (string item in filedata)
                {
                    string strblock4beg = "";
                    string strblock4end = "";

                    /* SEP-14-2021 mark for block 2
                     * if (BeginTags.Split('|').Length > 1 && EndTags.Split('|').Length > 1)
                    {
                        //block 2
                        //LogAdd("check block 2.");
                        string strblock2beg = "";
                        string strblock2end = "";
                        strblock2beg = BeginTags.Split('|')[0];
                        strblock2end = EndTags.Split('|')[0];
                        if (item.ToUpper().Contains(strblock2beg))
                        {
                            //LogAdd("block2 data=" + item.IndexOf(strblock2beg) + "," + item.IndexOf(strblock2beg) + "," + item.IndexOf(strblock2end));

                            //LogAdd("block2 data=" + item.Substring(item.IndexOf(strblock2beg) + 4, (item.IndexOf(strblock2end) - item.IndexOf(strblock2beg))));
                            NewCIF.Add(item.Substring(item.IndexOf(strblock2beg) + 4, (item.IndexOf(strblock2end) - item.IndexOf(strblock2beg)) - 4));
                        }

                        strblock4beg = BeginTags.Split('|')[1];
                        strblock4end = EndTags.Split('|')[1];
                        if (item.ToUpper().Contains(strblock4beg))
                        {
                            LogAdd("line lenth:" + item.Length + ", scan tag location:" + item.LastIndexOf(strblock4beg));
                            //if line lenth - tag 4 location = 5 means tag4 at the end of the line, 
                            //so start tp capture data from the next line till the end tag -}. 

                            bol_capture_start = true;
                            // go to next line
                            continue;
                        }

                        if (item.ToUpper().Contains(strblock4end))
                        {
                            bol_capture_start = false;
                        }

                        if (bol_capture_start)
                        {
                            NewCIF.Add(item);
                        }
                    }
                    else
                
                    {
                    */
                    // no block 2
                    strblock4beg = BeginTags;
                    strblock4end = EndTags;
                    if (item.ToUpper().Contains(strblock4beg))
                    {
                        LogAdd("line lenth:" + item.Length + ", scan tag location:" + item.LastIndexOf(strblock4beg));
                        //if line lenth - tag 4 location = 5 means tag4 at the end of the line, 
                        //so start tp capture data from the next line till the end tag -}. 

                        bol_capture_start = true;
                        // go to next line
                        continue;

                        /* 2021, APR. 26 block 4 must connect with change line command
                        else if (item.Length - item.LastIndexOf(ScanTag_start) > 5)
                        {
                            NewCIF.Add(item.Substring(item.LastIndexOf(ScanTag_start) + 5));
                        }
                        */

                    }

                    if (item.ToUpper().Contains(strblock4end))
                    {
                        bol_capture_start = false;
                    }

                    if (bol_capture_start)
                    {
                        NewCIF.Add(item);
                    }
                    //}

                }
                if (NewCIF.Count == 0 || source_fortify.Contains("XR") || source_fortify.Contains("XS"))
                {
                    //for swift MX format 
                    NewCIF = filedata.ToList();
                }

                File.WriteAllLines(dest_fortify, NewCIF.ToArray(), Encoding.Default);

                //File.Delete(source_fortify);
            }
            catch (Exception ex)
            {
                Logerr("Capture Block 4 Error:" + ex.ToString());
            }
        }

        public string[] CaptureBlock4String(string SourcePath, string BeginTags, string EndTags, ref string UserMsg)
        {
            List<string> NewCIF = new List<string>();
            try
            {
                Fortify fortify = new Fortify();
                string source_fortify = fortify.PathManipulation(SourcePath);

                //2021, Jan. capture swift tag:4 
                String[] filedata = File.ReadAllLines(source_fortify, Encoding.Default);

                List<string> ParseCIF = new List<string>();

                bool bol_capture_start = false;

                UserMsg = "";

                foreach (string item in filedata)
                {
                    LogAdd("check data:" + item);
                    string strblock4beg = "";
                    string strblock4end = "";
                    /* SEP-14-2021 mark for block 2
                    /* if (BeginTags.Split('|').Length > 1 && EndTags.Split('|').Length > 1)
                    {
                        //block 2
                        //LogAdd("check block 2.");
                        string strblock2beg = "";
                        string strblock2end = "";
                        strblock2beg = BeginTags.Split('|')[0];
                        strblock2end = EndTags.Split('|')[0];
                        if (item.ToUpper().Contains(strblock2beg))
                        {
                            //LogAdd("block2 data=" + item.IndexOf(strblock2beg) + "," + item.IndexOf(strblock2beg) + "," + item.IndexOf(strblock2end));

                            //LogAdd("block2 data=" + item.Substring(item.IndexOf(strblock2beg) + 4, (item.IndexOf(strblock2end) - item.IndexOf(strblock2beg))));
                            NewCIF.Add(item.Substring(item.IndexOf(strblock2beg) + 4, (item.IndexOf(strblock2end) - item.IndexOf(strblock2beg)) - 4));
                        }

                        strblock4beg = BeginTags.Split('|')[1];
                        strblock4end = EndTags.Split('|')[1];

                        if (item.ToUpper().Contains(strblock4beg))
                        {
                            //LogAdd("strat Tag:" + BeginTag + ", line lenth:" + item.Length + ", scan tag location:" + item.LastIndexOf(ScanTag_start));
                            //if line lenth - tag 4 location = 5 means tag4 at the end of the line, 
                            //so start tp capture data from the next line till the end tag -}. 
                            bol_capture_start = true;
                            // go to next line
                            continue;
                        }

                        //JUL-09-2021 capture UserMsgReference
                        if (item.ToUpper().Contains(":20:"))
                        {
                            UserMsg = item.Replace(":20:", "");
                        }

                        if (item.ToUpper().Contains(strblock4end))
                        {
                            bol_capture_start = false;
                        }

                        if (bol_capture_start)
                        {
                            NewCIF.Add(item);
                        }
                    }
                    else
                    {
                    */
                    // no block 2
                    strblock4beg = BeginTags;
                    strblock4end = EndTags;
                    if (item.ToUpper().Contains(strblock4beg))
                    {
                        //LogAdd("strat Tag:" + BeginTag + ", line lenth:" + item.Length + ", scan tag location:" + item.LastIndexOf(ScanTag_start));
                        //if line lenth - tag 4 location = 5 means tag4 at the end of the line, 
                        //so start tp capture data from the next line till the end tag -}. 
                        bol_capture_start = true;
                        // go to next line
                        continue;
                    }

                    //JUL-09-2021 capture UserMsgReference
                    if (item.ToUpper().Contains(":20:"))
                    {
                        UserMsg = item.Replace(":20:", "");
                    }

                    if (item.ToUpper().Contains(strblock4end))
                    {
                        bol_capture_start = false;
                    }

                    if (bol_capture_start)
                    {
                        NewCIF.Add(item);
                    }
                    //}

                }
                if (NewCIF.Count == 0 || source_fortify.Contains("XR") || source_fortify.Contains("XS"))
                {
                    //for swift MX format 
                    NewCIF = filedata.ToList();
                }
            }
            catch (Exception ex)
            {
                Logerr("Capture Block 4 Error:" + ex.ToString());
            }
            //LogAdd("block 4 array lenth:" + NewCIF.Count());
            return NewCIF.ToArray();
        }

        /// <summary>
        /// call the wpep webservice to get scan result
        /// </summary>
        /// <param name="sfilepath"></param>
        /// <param name="sfilename"></param>
        /// <param name="sbranchcode"></param>
        private Boolean SendWPEP(string sfilepath,string sfilename,string sbranchcode, ref bool bolmatch)
        {
            bool return_r = false;
            try
            {
                //20170405 by danny declare params
                AMLData = new AML_Data();
                string strEngName = "";
                string strRef = "";
                int iscore = 0;
                //2021, Mar. add swift tag config table.
                DataTable dt_swifttag = new DataTable();
                dt_swifttag = AMLData.GetSwiftTag();

                for (int i = 0; i < oBranchINList.Count; i++)
                {
                    if (sbranchcode == oBranchINList[i].BranchCode)
                    {
                        //20170315 by danny check 路透黑名單
                        //20170607 by danny 將WPEP跟掃CIF電文拆開作業
                        if ((oBranchINList[i].WPEPEnable == "Y" || OFACWS_Enable == "Y") && sfilename.Substring(4, 2).ToUpper() == "CI" && sfilename.Substring(6, 1).ToUpper() != "F")
                        {
                            LogAdd("Call WPEP for CI DATA");

                            return_r = AMLData.checkPEPS(sfilepath, sfilename, string.Format("{0}" + DateTime.Now.ToString("yyyyMMddHmmss"), oBranchINList[i].WPEPReference), oBranchINList[i].BranchCode, oBranchINList[i].WPEPBranchNo, int.Parse(oBranchINList[i].WPEPretry), int.Parse(oBranchINList[i].WPEPdelay), oBranchINList[i].WPEPnation_col, oBranchINList[i].WPEPengname_col, ref strRef, ref strEngName, ref iscore, oBranchINList[i].WPEPscore, oBranchINList[i].WPEPdob, oBranchINList[i].WPEPCIF_TYPE, ref bolmatch, oBranchINList[i].WPEPEnable);
                            
                        }
                        //else if (oBranchINList[i].WPEPEnable == "Y" && (sfilename.Substring(4, 1).ToUpper() == "S" || sfilename.Substring(4, 1).ToUpper() == "F") && (oBranchINList[i].WPEPswifttype1.Split(',').Any(s => s == sfilename.Substring(14, 4).ToString().Trim()) || oBranchINList[i].WPEPswifttype2.Split(',').Any(s => s == sfilename.Substring(14, 4).ToString().Trim())))
                        else if ((oBranchINList[i].WPEPEnable == "Y" || OFACWS_Enable == "Y") && (sfilename.Substring(4, 1).ToUpper() == "S" || sfilename.Substring(4, 1).ToUpper() == "F") && (dt_swifttag.Select("MT='MT" + sfilename.Substring(14, 1).ToString().Trim() + "XX' ").Length > 0) || oBranchINList[i].WPEPswifttype2.Split(',').Any(s => s == sfilename.Substring(14, 4).ToString().Trim()))
                        {
                            LogAdd("Call WPEP for SWIFT DATA");

                            return_r = AMLData.checkPEPS(sfilepath, sfilename, string.Format("{0}" + DateTime.Now.ToString("yyyyMMddHmmss"), oBranchINList[i].WPEPReference), oBranchINList[i].BranchCode, oBranchINList[i].WPEPBranchNo, int.Parse(oBranchINList[i].WPEPretry), int.Parse(oBranchINList[i].WPEPdelay), oBranchINList[i].WPEPnation_col, oBranchINList[i].WPEPengname_col, ref strRef, ref strEngName, ref iscore, oBranchINList[i].WPEPscore, oBranchINList[i].WPEPdob, oBranchINList[i].WPEPCIF_TYPE, ref bolmatch, oBranchINList[i].WPEPEnable);
                        }
                        else if ((oBranchINList[i].WPEPEnable == "Y" || OFACWS_Enable == "Y") && (sfilename.Substring(4, 1).ToUpper() == "X") && ((dt_swifttag.Select("MT='M" + sfilename.Substring(4, 1).ToUpper() + "XXX' ").Length > 0) || (dt_swifttag.Select("MT='M" + sfilename.Substring(4, 1).ToUpper() + sfilename.Substring(14, 1).ToString().Trim() + "XX' ").Length > 0)))
                        {
                            LogAdd("Call WPEP for MX SWIFT");

                            return_r = AMLData.checkPEPS(sfilepath, sfilename, oBranchINList[i].BranchCode, int.Parse(oBranchINList[i].WPEPretry), int.Parse(oBranchINList[i].WPEPdelay), ref bolmatch);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logerr("checkPEPS Error:" + ex.ToString());
                
            }
            return return_r;
        }

        private List<string> Get_cutstring(string sfile)
        {
            //AMLData = new AML_Data();
            List<string> lst_return = new List<string>();
            /* Dec.13.2021 Lesliy think we no need to modify this since hk branch will change system.
            string str_newfile = "";
            for (int i = 0; i < sfile.Split('|').Length; i++)
            {
                //Nov.29.2021 針對CIF/BNF 第2欄Name做亂碼檢查 
                if (i == 1)
                {
                    for (int j = 0; j < sfile.Split('|')[1].Length; j++)
                    {
                        //LogAdd("chk byte length " + sfile.Split('|')[1].Substring(j, 1) + ":" + Convert.ToInt32(sfile.Split('|')[1][j]));
                        // 2byte
                        if (Convert.ToInt32(sfile.Split('|')[1][j]) < 32 || Convert.ToInt32(sfile.Split('|')[1][j]) > 126)
                        {
                            
                            //replace name to 空白

                            str_newfile += "**|";
                            break;

                        }
                        else
                        {
                            if (j == sfile.Split('|')[1].Length - 1)
                            {
                                str_newfile += sfile.Split('|')[i] + "|";
                            }
                        }
                    }
                }
                else
                {
                    str_newfile += sfile.Split('|')[i] + "|";
                }
            }
            
            if (str_newfile != "")
            {
                sfile = str_newfile.TrimEnd('|');
            }*/
            

            string str_datafirst = "";
            string str_datafirstbuff = "";

            string str_datasec = "";
            string str_datasec1 = "";
            string str_datasec2 = "";
            string str_datasectail = "";
            string str_datasecbuff = "";

            int ifill = 0;//需要補的個數
            int i_firstcount = 0;
            try
            {
                //20170713 by danny 
                //如果一筆record長度大於2000，cif batch會失敗，設定1970預留一些空間 ，切成兩筆同ID的資料，第二筆ID+*以免同ID不掃
                if (sfile.Length > 1970)
                {
                    str_datafirst = sfile.Substring(0, sfile.Substring(0,1970).LastIndexOf("|"));

                    str_datasec = sfile.Substring(0, 10) + "*";//第二行開頭ID+*

                    i_firstcount = str_datafirst.Split('|').Count();

                    for (int i = i_firstcount;i< 54; i ++)
                    {
                        str_datafirstbuff += "|";
                    }

                    lst_return.Add(str_datafirst + str_datafirstbuff);

                    if (i_firstcount > 21 && i_firstcount < 45)//計算目前抓到哪一欄，缺哪幾欄
                    {
                        ifill = 45 - i_firstcount;
                    }

                    int icont = 0;
                    foreach(string strsplit in str_datafirst.Split('|'))
                    {
                        if (icont >= 1 && icont < 21)// 24組名單之前的資料
                        {
                            str_datasec1 += strsplit + "|";
                        }
                        icont = icont + 1;
                    }

                    if (i_firstcount < 45)
                    {
                        for (int i = 0; i < 24 - ifill; i++)// 計算需要補多少欄
                        {
                            str_datasecbuff += "*****|";
                        }

                        for (int i = i_firstcount; i < 45; i++)//補24組剩下的
                        {
                            str_datasec2 += sfile.Split('|')[i] + "|";
                        }
                    }
                    else
                    {
                        for (int i = 0; i < 24; i++)// 直接補滿24欄空白
                        {
                            str_datasecbuff += "*****|";
                            str_datasec2 = "";
                        }
                    }

                    for (int i = 45 ; i < 54; i++)//補最後46~54欄
                    {
                        str_datasectail += sfile.Split('|')[i] + "|";
                    }

                    lst_return.Add(str_datasec + "|" + str_datasec1 + str_datasecbuff + str_datasec2 + str_datasectail.TrimEnd('|'));

                }
                else
                {
                    lst_return.Add(sfile);
                }
            }
            catch(Exception ex)
            {
                Logerr("Parse string fail :" + ex.ToString());
            }

            return lst_return;
        }

        private void LogAdd(string logstring)
        {
            Fortify fortifyService = new Fortify();
            log.Info(fortifyService.PathManipulation(logstring));
        }

        private void Logerr(string logstring)
        {
            Fortify fortifyService = new Fortify();
            log.Error(fortifyService.PathManipulation(logstring));
        }

        private void Logerr(string logstring,Exception ex)
        {
            Fortify fortifyService = new Fortify();
            log.Error(fortifyService.PathManipulation(logstring));
        }

        public UnisysAMLInterface()
        {
            InitializeComponent();
        }

        //For Debug
        public void Start(string[] args)
        {
            this.OnStart(args);
        }
        

        protected override void OnStart(string[] args)
        {
            bool is_createdNew1;
            Mutex mu1 = null;

            try
            {
                #region 檢查程式是否重複執行

                string mutexName1 = Process.GetCurrentProcess().MainModule.FileName
                            .Replace(Path.DirectorySeparatorChar, '_');
                mu1 = new Mutex(true, "Global\\" + mutexName1, out is_createdNew1);
                if (!is_createdNew1)
                    return;

                #endregion

                InitSetting();
                oTimer.Elapsed += new System.Timers.ElapsedEventHandler(oTimer_Elapsed);
                oTimer.Interval = 60000;
                oTimer.Enabled = true;

                LogAdd("This service has started.");
            }
            catch (Exception ex)
            {
                Logerr(String.Format("Starting service error"), ex);
            }
        }

        protected override void OnStop()
        {
            try
            {
                oTimer.Enabled = false;
                oCIFTimer.Enabled = false;
                LogAdd("This service has stopped.");
            }
            catch (Exception ex)
            {
                Logerr(String.Format("Stopping service error"), ex);
            }
        }

        #region Timer
        private void oTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string sNow = DateTime.Now.ToString("HHmm");
            try
            {
                for (int i = 0; i < oBranchINList.Count; i++)
                {
                    if (oBranchINList[i].Backup == sNow)
                    {

                        string sFile = GetOutputFileName(FileOutPath, oBranchINList[i].BranchCode);

                        File.Move(sFile, GetOutputFileName(FileOutPathBackup, oBranchINList[i].BranchCode) + "_" + DateTime.Now.ToString("yyyyMMdd"));
                        LogAdd(String.Format("Backup Branch {0} Output Data", oBranchINList[i].BranchCode));
                    }
                }
            }
            catch (Exception ex)
            {
                Logerr(String.Format("Clear Branch Data Error"), ex);
            }
        }
        #endregion

        public static bool IsFileReady(String sFilename)
        {
            try
            {
                Fortify fortifyService = new Fortify();

                using (FileStream inputStream = File.Open(fortifyService.PathManipulation(sFilename), FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    if (inputStream.Length > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Check if file is ready for delete
        /// if false then retry
        /// May.19, 2023 add
        /// </summary>
        /// <param name="finfo"></param>
        public void DelFile(FileInfo finfo)
        {
            try
            {
                string filefullpath = finfo.FullName;
                int iretry = 3;
                int idelay = 5; //second
                bool bolcheck = false;

                for (int icount = 0; icount < iretry; icount++)
                {
                    if (!bolcheck)
                    {
                        bolcheck = IsFileReady(filefullpath);
                        LogAdd(String.Format("Check {0} time, if File {1} ready to delete:{2}", icount.ToString(), filefullpath, bolcheck.ToString()));

                        if (bolcheck)
                        {
                            try
                            {
                                finfo.Delete();
                            }
                            catch (Exception e)
                            {
                                Logerr(String.Format("File {0} delete fail.", filefullpath));
                            }

                        }
                        else
                        {
                            LogAdd(String.Format("File {0} locked, Wait for {1} seconds and try again.", filefullpath, idelay.ToString()));
                            Thread.Sleep(idelay * 1000);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                Logerr(String.Format("DelFile {0} Error:{1}", finfo.FullName, ex.ToString()));
            }
        }

        public void CaseApproveToSwallow(string strFilename, string strBranch)
        {
            //May.22, 2023 ofac case approved by supervisor should call the swallow webservice
            //and return the case final status.
            //Oct.19, 2023 add Swallow ws
            try
            {
                AMLData = new AML_Data();
                string strResultCode = "";
                string str_logtext = "";
                if (AMLData.Chk_OfacCase(strFilename, ref strResultCode))
                {
                    int ws_timeout = 5;
                    int ws_retry = 10;
                    int ws_interval = 10;

                    //check ofac case exist, call the swallow ws to return the case status.
                    SwallowCaseReport.PrimeCaseReportService call_swallow_case_report = new SwallowCaseReport.PrimeCaseReportService();
                    
                    string input_tm = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    string input_tranid = strFilename;

                    //SSL TLS1.2
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate
                    {
                        return AMLData.CheckCert();
                    };

                    //for SSL
                    System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;

                    string swallow_response = "";

                    if (!string.IsNullOrEmpty(SwallowWSTimeout))
                    {
                        int.TryParse(SwallowWSTimeout, out ws_timeout);
                    }

                    if (!string.IsNullOrEmpty(SwallowWSRetry))
                    {
                        int.TryParse(SwallowWSRetry, out ws_retry);
                    }

                    if (!string.IsNullOrEmpty(SwallowWSInterval))
                    {
                        int.TryParse(SwallowWSInterval, out ws_interval);
                    }

                    call_swallow_case_report.Timeout = ws_timeout * 1000;

                    bool bol_chkresp = false;

                    for(int i = 0; i < ws_retry; i++)
                    {
                        try
                        {
                            swallow_response = call_swallow_case_report.OSB_CaseReport(input_tm, input_tranid, strResultCode);

                            bol_chkresp = true;
                        }
                        catch (Exception e)
                        {
                            LogAdd(string.Format("Call Swallow WS fail {0} times with exception = {1}", i + 1, e.ToString()));
                            str_logtext = string.Format("***** Call Swallow WS fail! *****");
                            
                            if(i == 0)
                            {
                                //Nov.7, 2023 Polly think only need log 1 time in event logs
                                AMLData.InsertOFACEvet(strFilename, str_logtext);
                            }
                        }
                        finally
                        {
                            call_swallow_case_report.Dispose();
                        }
                        
                        if(bol_chkresp)
                        {
                            string str_statusDesc = "";
                            switch(strResultCode)
                            {
                                case "R":
                                    str_statusDesc = "R (Case Waived & Approved)";
                                    break;
                                case "B":
                                    str_statusDesc = "B (BLOCK)";
                                    break;
                            }
                            LogAdd(String.Format("Send Ofac case Resullt = {0}. WS response = {1}", str_statusDesc, swallow_response));
                            str_logtext = string.Format("Send Ofac case Resullt = {0}", str_statusDesc);
                            AMLData.InsertOFACEvet(strFilename, str_logtext);

                            break;
                        }
                        else
                        {
                            Thread.Sleep(ws_interval * 1000); 
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logerr(string.Format("Call swallow ws process fail.{0}", e.ToString()));

                string str_logtext = "Call Swallow WS fail!";
                AMLData.InsertOFACEvet(strFilename, str_logtext);
            }
        }
    }
}
