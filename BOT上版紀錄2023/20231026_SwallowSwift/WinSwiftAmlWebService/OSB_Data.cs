﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Data;
using NLog;
using System.Runtime.Remoting.Messaging;

namespace WinSwiftAmlWebService
{
    public class OSB_Data
    {
        Database database;
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public DataSet QueryStatus(string input_TRANSACTIONID)
        {
            try
            {
                DataSet ds = new DataSet();

                database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
                //logger.Trace("[QueryStatus] database info:" + database.ConnectionString.ToString());
                
                string SQL = @"
                select * from (
                select a.SeqNumb, a.Ref,case when isnull(a.ConfirmState,0) = 2 and a.app = 0 then 'P'
                when isnull(a.ConfirmState,0) = 2 and a.app = 1 then 'R' 
                when isnull(a.ConfirmState,0) = 1 and a.app = 0 then 'P'
                when isnull(a.ConfirmState,0) = 1 and a.app = 1 then 'B'
                when isnull(a.ConfirmState,0) = 0 then 'P'
                else 'N'
                end scanresult 
                from ofac..FilterTranTable(nolock) a
                union
                select b.SeqNumb, b.Ref,case when isnull(b.ConfirmState,0) = 2 and b.app = 0 then 'P'
                when isnull(b.ConfirmState,0) = 2 and b.app = 1 then 'R' 
                when isnull(b.ConfirmState,0) = 1 and b.app = 0 then 'P'
                when isnull(b.ConfirmState,0) = 1 and b.app = 1 then 'B'
                when isnull(b.ConfirmState,0) = 0 then 'P'
                else 'N'
                end scanresult 
                from ofac..FilterTranhistTable(nolock) b
                ) T 
                where CHARINDEX('" + input_TRANSACTIONID.Trim() + "', T.Ref) > 0 order by SeqNumb desc;";

                using (DbCommand dBCommand = database.GetSqlStringCommand(SQL))
                {
                    //APR.21, 2023 use parameter to query hist table is very slow.
                    //database.AddInParameter(dBCommand, "@ref", DbType.String, input_TRANSACTIONID.Trim());

                    try
                    {
                        //May. 09, 2022 add exception catch
                        ds = database.ExecuteDataSet(dBCommand);
                        if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                        {
                            return ds;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        logger.Error("[QueryStatus] Error=" + e.ToString());
                        throw e;
                    }
                    finally
                    {
                        dBCommand.Dispose();
                    }
                }

                return ds;
            }
            catch (Exception e)
            {
                logger.Error("[QueryStatus] Error=" + e.ToString());
                throw e;
            }
        }

        public DataSet QueryReconStatus(string input_TRANSACTIONID)
        {
            try
            {
                DataSet ds = new DataSet();

                database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
                //logger.Trace("[QueryReconStatus] database info:" + database.ConnectionString.ToString());

                //APR.24, 2023 need to check recontable for WS_swift with no matched case
                string SQL = @"
                select Source, CaseReference, case when isnull(SancPartyCount, '') = 0 then 'N'
                else 'X' end scanresult
                from ofac..ReconTable(nolock) R
                where CHARINDEX('" + input_TRANSACTIONID.Trim() + "', T.Ref) > 0 and R.SancPartyCount = 0 order by SearchDate desc;";

                using (DbCommand dBCommand = database.GetSqlStringCommand(SQL))
                {
                    //APR.21, 2023 use parameter to query hist table is very slow.
                    //database.AddInParameter(dBCommand, "@ref", DbType.String, input_TRANSACTIONID.Trim());

                    try
                    {
                        //May. 09, 2022 add exception catch
                        ds = database.ExecuteDataSet(dBCommand);
                        logger.Trace(string.Format("[QueryReconStatus] dataset info: table_count={0}", ds.Tables.Count));
                        if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                        {
                            return ds;
                        }
                    }

                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        logger.Error("[QueryStatus] Error=" + e.ToString());
                        throw e;
                    }
                    finally
                    {
                        dBCommand.Dispose();
                    }

                    return ds;
                }
            }
            catch (Exception e)
            {
                logger.Error("[QueryReconStatus] Error=" + e.ToString());
                throw e;
            }
        }

        public bool UpdateCurAmt(string str_filename, string str_cur, string str_amt, string str_msgid)
        {
            //May.26,2023 update ofac case currency and amount
            try
            {
                bool bol_return = false;

                int icount = 0;

                database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
                //logger.Trace("[UpdateCurAmt] database info:" + database.ConnectionString.ToString());

                string SQL = @"update ofac..FilterTranTable set Currency = @cur, Amount = @amt, UserMessageReference = @msgid 
                                where ref = @ref 
                                and (isnull(Currency,'') != @cur or isnull(Amount,0) != @amt or isnull(UserMessageReference,'') != @msgid)";
                using (DbCommand dBCommand = database.GetSqlStringCommand(SQL))
                {
                    database.AddInParameter(dBCommand, "@cur", DbType.String, str_cur.Trim());
                    database.AddInParameter(dBCommand, "@amt", DbType.String, str_amt.Trim());
                    database.AddInParameter(dBCommand, "@msgid", DbType.String, str_msgid.Trim());
                    database.AddInParameter(dBCommand, "@ref", DbType.String, str_filename.Replace(".prb", ".pri").Trim());
                    
                    try
                    {
                        //May. 09, 2022 add exception catch
                        icount = database.ExecuteNonQuery(dBCommand);
                        logger.Trace(string.Format("[UpdateCurAmt] dataset info: update count={0}", icount.ToString()));
                        if (icount != 0)
                        {
                            bol_return = true;
                        }
                    }

                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        logger.Error("[UpdateCurAmt] Error=" + e.ToString());
                        throw e;
                    }
                    finally
                    {
                        dBCommand.Dispose();
                    }
                }
                return bol_return;
            }
            catch(Exception ex)
            {
                logger.Error("[UpdateCurAmt] Error=" + ex.ToString());
                throw ex;
            }
        }

        public bool UpdateUserMsg(string str_filename, string str_msgid)
        {
            //Oct.21,2023 update recontable UserMessageReference
            try
            {
                bool bol_return = false;

                int icount = 0;

                database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
                //logger.Trace("[UpdateCurAmt] database info:" + database.ConnectionString.ToString());

                string SQL = @"update ofac..ReconTable set UserMessageReference = @msgid where CaseReference = @ref 
                                and isnull(UserMessageReference,'') != @msgid ";
                using (DbCommand dBCommand = database.GetSqlStringCommand(SQL))
                {
                    database.AddInParameter(dBCommand, "@msgid", DbType.String, str_msgid.Trim());
                    database.AddInParameter(dBCommand, "@ref", DbType.String, str_filename.Replace(".prb", ".pri").Replace(".ok",".pri").Trim());

                    try
                    {
                        //May. 09, 2022 add exception catch
                        icount = database.ExecuteNonQuery(dBCommand);
                        logger.Trace(string.Format("[UpdateReconUsrMsgRef] Msgid={0}, Ref={1}: update count={2}", str_msgid.Trim(), str_filename.Replace(".prb", ".pri").Replace(".ok", ".pri").Trim(), icount.ToString()));
                        if (icount != 0)
                        {
                            bol_return = true;
                        }
                    }

                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        logger.Error("[UpdateReconUsrMsgRef] Error=" + e.ToString());
                        throw e;
                    }
                    finally
                    {
                        dBCommand.Dispose();
                    }
                }
                return bol_return;
            }
            catch (Exception ex)
            {
                logger.Error("[UpdateReconUsrMsgRef] Error=" + ex.ToString());
                throw ex;
            }
        }

        public bool CheckUser(string input_USERID)
        {
            //after the discuss with the vendor at APR.14, 2023. decide no use
            try
            {
                DataSet ds = new DataSet();
                database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
                bool bol_return = false;

                string SQL = @"
                select * from psec..Operator(nolock)
                where [Enable] = 1 and DeleteF = 0 and Approved = 1 
                and UserName = @Userid;
                ";

                using (DbCommand dBCommand = database.GetSqlStringCommand(SQL))
                {
                    database.AddInParameter(dBCommand, "@Userid", DbType.String, input_USERID.Trim());

                    try
                    {
                        //May. 09, 2022 add exception catch
                        ds = database.ExecuteDataSet(dBCommand);

                        if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0 && ds.Tables[0].Rows[0]["Code"].ToString() != "")
                        {
                            bol_return = true;
                        }
                    }

                    catch (Exception e)
                    {
                        throw e;
                    }
                    finally
                    {
                        dBCommand.Dispose();
                    }
                }

                return bol_return;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool CheckBranch(string input_BRANCH)
        {
            try
            {
                DataSet ds = new DataSet();
                database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
                bool bol_return = false;

                string SQL = @"
                    select * from Psec..Branch(nolock)
                    where Code = @Code;
                ";

                using (DbCommand dBCommand = database.GetSqlStringCommand(SQL))
                {
                    database.AddInParameter(dBCommand, "@Code", DbType.String, input_BRANCH.Trim());

                    try
                    {
                        //May. 09, 2022 add exception catch
                        ds = database.ExecuteDataSet(dBCommand);

                        if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0 && ds.Tables[0].Rows[0]["Code"].ToString() != "")
                        {
                            bol_return = true;
                        }
                    }

                    catch (Exception e)
                    {
                        throw e;
                    }
                    finally
                    {
                        dBCommand.Dispose();
                    }
                }

                return bol_return;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool CheckOrg(string input_ORGANIZATION)
        {
            bool bol_return = false;
            try
            {
                if (input_ORGANIZATION.Trim().ToUpper() == "BOTOSB")
                {
                    bol_return = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return bol_return;
        }

        public void InsertOFACEvet(string sfilename, string sLogtext)
        {
            //Nov.1, 2023 add ofac event log insert
            database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
            string SQL = "exec OFAC..OFS_InsEvent @oper, @type, @objectType, @objectId, @logText";

            using (DbCommand dBCommand = database.GetSqlStringCommand(SQL))
            {
                database.AddInParameter(dBCommand, "@oper", DbType.String, "primeadmin");
                database.AddInParameter(dBCommand, "@type", DbType.String, "Ann");
                database.AddInParameter(dBCommand, "@objectType", DbType.String, "Event");
                database.AddInParameter(dBCommand, "@objectId", DbType.String, sfilename.Trim());
                database.AddInParameter(dBCommand, "@logText", DbType.String, sLogtext.Trim());

                try
                {
                    IDataReader datareader = database.ExecuteReader(dBCommand);

                    while (datareader.Read())
                    {
                        string strresult = datareader[0].ToString();
                        logger.Info(string.Format("[InsertOFACEvet] Result:{0}" + strresult));
                    }
                }

                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    dBCommand.Dispose();
                }

            }
        }
    }
}