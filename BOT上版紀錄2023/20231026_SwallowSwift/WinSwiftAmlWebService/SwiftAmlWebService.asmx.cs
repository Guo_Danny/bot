﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using System.Configuration;
using System.IO;
using System.Data;
using NLog;
using System.Diagnostics.Eventing.Reader;

namespace WinSwiftAmlWebService
{
    /// <summary>
    /// Summary description for SwiftAmlWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SwiftAmlWebService : System.Web.Services.WebService
    {
        public OSB_QUERYRESULT R_StatusReturn = new OSB_QUERYRESULT();
        public OSB_SCANRESULT R_ScanReturn = new OSB_SCANRESULT();
        public OSB_ReturnToSwallow R_ReturnSwallow = new OSB_ReturnToSwallow();
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        [WebMethod]
        public OSB_QUERYRESULT OSB_Query(string input_TRANSACTIONTM, string input_TRANSACTIONID)
        {
            try
            {
                logger.Info(string.Format("[Query] Parameters:trantm={0}, tranid={1}", input_TRANSACTIONTM, input_TRANSACTIONID));
                OSB_Data OSBdb = new OSB_Data();

                string str_Resultpath = ConfigurationManager.AppSettings["ResultPath"];

                R_StatusReturn.TRANSACTIONTM = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                R_StatusReturn.TRANSACTIONID = input_TRANSACTIONID;
                R_StatusReturn.RESULTCODE = "X";

                //check input data
                /* JUL.05 2023 mark the check function
                if (!checkinput(input_TRANSACTIONTM, input_TRANSACTIONID))
                {
                    R_StatusReturn.RESULTCODE = "X";
                    return R_StatusReturn;
                }
                */

                //query case status
                DataSet ds = OSBdb.QueryStatus(input_TRANSACTIONID);

                if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                {
                    R_StatusReturn.RESULTCODE = ds.Tables[0].Rows[0]["scanresult"].ToString();
                }
                else
                {
                    //Nov.8, 2023 return code is different from scan and query,
                    //should not use checkoutfile function.
                    
                    logger.Info("[Query] Query not found");
                    
                    //current swift case not exists, need to check the backup folder
                    //R_StatusReturn.RESULTCODE = CheckOutFile(input_TRANSACTIONID, str_Resultpath, 5, 5, "", "", "");
                    
                    R_StatusReturn.RESULTCODE = "X";
                }

                string str_statusDesc = "";

                switch (R_StatusReturn.RESULTCODE)
                {
                    case "R":
                        str_statusDesc = "R (Case Waived & Approved)";
                        break;
                    case "B":
                        str_statusDesc = "B (Case Confirmed(BLOCK))";
                        break;
                    case "P":
                        str_statusDesc = "P (Reviewing)";
                        break;
                    case "X":
                        str_statusDesc = "X (Not Found)";
                        break;
                    default:
                        str_statusDesc = "X (Not Found)";
                        break;
                }

                string str_logtext = string.Format("Received the Query request {0} from SwiftAmlWebservice, return code = {1}", input_TRANSACTIONID, str_statusDesc);

                OSBdb.InsertOFACEvet(input_TRANSACTIONID, str_logtext);

                logger.Info(string.Format("[Query] Query {0} result = {1}", input_TRANSACTIONID, str_statusDesc));

            }
            catch (Exception e)
            {
                logger.Error("[Query] Error:" + e.Message.ToString());
            }
            return R_StatusReturn;
        }

        [WebMethod]
        public OSB_SCANRESULT OSB_Scan(string input_TRANSACTIONTM, 
            string input_TRANSACTIONID, string input_FORMATTYPE,string input_MessageID, string input_Currency, 
            string input_Amount, string input_TRANSACTIONTEXT)
        {
            try
            {
                //logger.Info(string.Format("[Scan] Parameters:trantm={0}, tranid={1}, format={2}, messageid={3}, currency={4}, amt={5}", 
                //    input_TRANSACTIONTM, input_TRANSACTIONID, input_FORMATTYPE, input_MessageID, input_Currency, input_Amount));

                logger.Info("[Scan] File name=" + input_TRANSACTIONID);
                OSB_Data OSBdb = new OSB_Data();
                string str_inputpath = ConfigurationManager.AppSettings["FileInPath"];
                string str_outputpath = ConfigurationManager.AppSettings["FileOutPath"];
                string str_backuppath = ConfigurationManager.AppSettings["FileBackupPath"];
                string str_S_R_LogPath = ConfigurationManager.AppSettings["SNDRCVLogPath"];
                string str_waitTimes = ConfigurationManager.AppSettings["WaitForScan"];
                string str_waitIterval = ConfigurationManager.AppSettings["WaitForScanInterval"];
                
                string str_Resultpath = ConfigurationManager.AppSettings["ResultPath"];
                //str_inputpath = str_inputpath + "\\" + input_BRANCH + "SR" + input_TRANSACTIONTM.Substring(0, 8) + "103" + " 000001 " + DateTime.Now.Hour.ToString().PadRight(6, '0');
                str_inputpath = str_inputpath + "\\" + input_TRANSACTIONID;


                R_ScanReturn.TRANSACTIONTM = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                R_ScanReturn.TRANSACTIONID = input_TRANSACTIONID;
                R_ScanReturn.RESULTCODE = "Y";

                if(!CheckType(input_FORMATTYPE, input_TRANSACTIONID))
                {
                    R_ScanReturn.RESULTCODE = "X";

                    return R_ScanReturn;
                }

                //check log folder exist
                CheckFolder(str_S_R_LogPath);

                //write send log
                AppenLog(str_S_R_LogPath, GetLogFileName("RCV", input_TRANSACTIONID.Substring(1, 3)), input_TRANSACTIONID);

                //check input data
                /* JUL.05, 2023 mark this check function
                if (!checkinput(input_TRANSACTIONTM, input_TRANSACTIONID) || !CheckType(input_FORMATTYPE, input_TRANSACTIONID))
                {
                    R_ScanReturn.RESULTCODE = "X";
                    return R_ScanReturn;
                }
                */

                StreamWriter sw = new StreamWriter(str_inputpath);

                sw.Write(input_TRANSACTIONTEXT);

                sw.Close();


                //query case status
                //delay and wait for screen process.
                int iwaitTimes = 10;
                if (string.IsNullOrEmpty(str_waitTimes) || !int.TryParse(str_waitTimes, out iwaitTimes))
                {
                    iwaitTimes = 10;
                }

                int iwaitInterval = 10;
                if (string.IsNullOrEmpty(str_waitIterval) || !int.TryParse(str_waitIterval, out iwaitInterval))
                {
                    iwaitInterval = 10;
                }

                R_ScanReturn.RESULTCODE = CheckOutFile(input_TRANSACTIONID, str_outputpath, iwaitTimes, iwaitInterval, input_Currency, input_Amount, input_MessageID);

                MoveBackup(str_outputpath, input_TRANSACTIONID, str_backuppath, str_Resultpath);
                if(R_ScanReturn.RESULTCODE == "Y" || R_ScanReturn.RESULTCODE == "N")
                {
                    AppenLog(str_S_R_LogPath, GetLogFileName("SND", input_TRANSACTIONID.Substring(1, 3)), input_TRANSACTIONID + R_ScanReturn.RESULTCODE);
                }

                //Nov.9, 2023 add return log
                string strResultDesc = "";

                switch(R_ScanReturn.RESULTCODE)
                {
                    case "N":
                        strResultDesc = "N (No Hit)";
                        break;
                    case "Y":
                        strResultDesc = "Y (Hit)";
                        break;
                }

                logger.Info(string.Format("[OSB_Scan] Scan ID = {0} return code = {1}", input_TRANSACTIONID, strResultDesc));

                OSBdb.InsertOFACEvet(input_TRANSACTIONID, strResultDesc);

                return R_ScanReturn;
            }
            catch (Exception e)
            {
                R_ScanReturn.RESULTCODE = "X";
                logger.Error("[Scan] Error:" + e.Message.ToString());
                return R_ScanReturn;
            }
        }

        public bool checkinput(string input_TRANSACTIONTM, string input_TRANSACTIONID)
        {
            bool bol_r = false;
            OSB_Data OSBdb = new OSB_Data();
            //set the UTC + 0 datetime
            DateTime dtime = DateTime.UtcNow;
            var offset = new DateTimeOffset(dtime, new TimeSpan(0, 0, 0));

            var t_utc = offset.ToUniversalTime();

            //set the UTC + 8 datetime from swallow
            DateTime iDate = DateTime.Parse(input_TRANSACTIONTM);
            var offset2 = new DateTimeOffset(iDate, new TimeSpan(8, 0, 0));
            
            var t_utcSwallow = offset2.ToUniversalTime();


            //check datetime diff
            if (Math.Abs((t_utc - t_utcSwallow).TotalSeconds) > 30)
            {
                bol_r = false;
                return bol_r;
            }
            else
            {
                bol_r = true;
            }
            return bol_r;
        }

        public bool CheckType(string input_FORMATTYPE, string input_TRANSACTIONID)
        {
            bool bol_r = false;

            if((input_TRANSACTIONID.Substring(4, 1) == "S" && input_FORMATTYPE == "MT") || (input_TRANSACTIONID.Substring(4, 1) == "F" && input_FORMATTYPE == "MT") || (input_TRANSACTIONID.Substring(4, 1) == "X" && input_FORMATTYPE == "MX"))
            {
                bol_r = true;
            }
            else
            {
                logger.Info(string.Format("[CheckType] Type={0} Missmatch!", input_FORMATTYPE));
                bol_r = false;
                return bol_r;
            }

            return bol_r;
        }

        public string CheckOutFile(string input_TRANSACTIONID, string str_outputpath, int int_wait, int int_interval, string str_cur, string str_amt, string str_msgid)
        {
            //APR.24, 2023 check the scan outgoing file extension.
            //May.26, 2023 update ofac case currency and amount.
            string str_result = "X";

            int i_delay = int_interval;
            logger.Info(string.Format("[CheckOutFIle] Check the out file. interval {0} wait {1} times.", int_interval, int_wait));
            for (int i = 0; i < int_wait; i++)
            {

                DirectoryInfo info = new DirectoryInfo(str_outputpath);
                FileInfo[] str_files = info.GetFiles().Where(s => s.Name.Contains(input_TRANSACTIONID.Trim())).OrderBy(p => p.CreationTime).ToArray();
                //return status order by create time asc 
                foreach (FileInfo str_file in str_files)
                {
                    logger.Info(string.Format("[CheckOutFIle] FileName={0}", str_file.FullName));
                    OSB_Data OSBdb = new OSB_Data();

                    //Oct.21, 2023 add update recon usermsgref
                    bool bol_rc = OSBdb.UpdateUserMsg(str_file.Name, str_msgid);
                    logger.Info(string.Format("[Update Recon UserMsgRef] set msgid = {0}, result count = {1}", str_msgid, bol_rc.ToString()));

                    if (str_file.Extension.ToLower() == ".prb")
                    {
                        str_result = "Y";
                        
                        if (!string.IsNullOrEmpty(str_cur) && !string.IsNullOrEmpty(str_amt))
                        {
                            bol_rc = OSBdb.UpdateCurAmt(str_file.Name, str_cur, str_amt, str_msgid);
                            logger.Info(string.Format("[Update Ofac Case] set cur = {0}, amt = {1}, result count = {2}", str_cur, str_amt, bol_rc.ToString()));
                        }

                        return str_result;
                    }
                    else if (str_file.Extension.ToLower() == ".ok")
                    {
                        str_result = "N";
                        
                        return str_result;
                    }
                    else if (str_file.Extension.ToLower() == ".cnf")
                    {
                        //str_result = "B"; JUL.05 2023 change returncode for the meeting on JUN.30 2023 
                        str_result = "Y";
                        
                        return str_result;
                    }
                }
                
                System.Threading.Thread.Sleep(i_delay * 1000);

                //logger.Info("[CheckOutFIle] Check the out file. wait " + i + " times.");
            }

            return str_result;
        }

        public void CheckFolder(string str_logpath)
        {
            try
            {
                if(!Directory.Exists(str_logpath))
                {
                    logger.Info(string.Format("[CheckPath] Folder not exist, create folder = {0}", str_logpath));
                    Directory.CreateDirectory(str_logpath);
                }
            }
            catch(Exception e)
            {
                logger.Error("[CheckPath] Error:" + e.ToString());
            }
        }

        public string GetLogFileName(string str_SendorRecv, string str_Branch)
        {
            string R_logname = "";

            try
            {
                //get log file name
                string str_logfilename = "Swallow." + str_Branch + "." + str_SendorRecv + "." + DateTime.Now.Day.ToString();

                if(File.Exists(str_logfilename))
                {
                    FileInfo loginfo = new FileInfo(str_logfilename);
                    if (loginfo.LastWriteTime.Month != DateTime.Now.Month)
                    {
                        logger.Info(string.Format("[GetLogFileName] file {0} exists, move to bak", str_logfilename));

                        File.Copy(str_logfilename, str_logfilename + ".BAK", true);
                        
                        R_logname = str_logfilename;

                        File.Delete(str_logfilename);

                        logger.Info(string.Format("[GetLogFileName] old file deleted !!"));
                    }
                    
                }

                R_logname = str_logfilename;
            }
            catch (Exception e)
            {
                logger.Error("[GetLogFileName] Error:" + e.ToString());
            }

            return R_logname;
        }

        public void AppenLog(string strLogpath,string str_fileName, string str_inputtext)
        {
            try
            {
                string str_FullName = strLogpath + "/" + str_fileName;

                using (StreamWriter w = File.AppendText(str_FullName))
                {
                    w.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + str_inputtext);
                }
            }
            catch(Exception e)
            {
                logger.Error("[AppendLog] Error:" + e.ToString());
            }
        }

        public void MoveBackup(string str_outputpath, string input_TRANSACTIONID, string str_backuppath, string str_resultpath)
        {
            try
            {
                DirectoryInfo info = new DirectoryInfo(str_outputpath);
                FileInfo[] str_files = info.GetFiles().Where(s => s.Name.Contains(input_TRANSACTIONID.Trim())).OrderBy(p => p.CreationTime).ToArray();

                foreach(FileInfo str_file in str_files)
                {
                    logger.Info(string.Format("[MoveBackup] Move File={0} To {1}", str_file.FullName, str_backuppath));
                    str_file.CopyTo(str_backuppath + "/" + str_file.Name);
                    str_file.CopyTo(str_resultpath + "/" + str_file.Name);

                    str_file.Delete();
                }
            }
            catch (Exception e)
            {
                logger.Error("[MoveBackup] Error:" + e.ToString());
            }
        }
    }
}
