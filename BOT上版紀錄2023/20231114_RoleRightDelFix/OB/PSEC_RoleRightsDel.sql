USE [Psec]
GO

/****** Object:  Trigger [dbo].[PSEC_RoleRightsDel]    Script Date: 11/16/2023 9:03:26 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER TRIGGER [dbo].[PSEC_RoleRightsDel] ON [dbo].[RoleRights]   FOR DELETE AS
	Declare @right Code
	Declare @role  Code
	Declare @branch Code
	Declare @Dept  Code
	Declare @Oper  Code
	Declare	@Scope int
	Declare @Status int
	IF @@rowcount > 0 
	BEGIN
		SELECT @right = [right] ,@role = [role],@branch =objbranch,@Dept=objdept,
			@Oper =ObjOper, @Scope = Scope, @Status = [Status]
			FROM Deleted
		If not Exists
		(Select * from  RoleRights r where r.[right] = @right
		 and IsNull(r.[objbranch],'') = IsNULL(RTRIM(@branch), '')
		 and IsNull(r.[objdept],'') = IsNULL(RTRIM(@dept), '')
		 and IsNull(r.[objoper],'') = IsNULL(RTRIM(@oper), '')
		 and r.[scope] = @scope
		 and r.[status] = @status
		)
			begin
				delete from operrights where [right] =@right
					and Oper in (Select oper from rolemembers  where [role] = @role)
					and IsNull([objbranch],'') = IsNULL(RTRIM(@branch), '')
					and IsNull([objdept],'') = IsNULL(RTRIM(@dept), '')
					and IsNull([objoper],'') = IsNULL(RTRIM(@oper), '')
					and [scope] = @scope
					and ([status] = @status or [status] = 0)--Nov.15, 2023 add status = 0
			end	
		else
			begin
				delete from operrights where [right] =@right
					and Oper in (
					Select oper from rolemembers  
					where [role] = @role
						/*except ----Nov.15, 2023
					Select oper from rolemembers  
					where [role] != @role											
					*/
					)
					and IsNull([objbranch],'') = IsNULL(RTRIM(@branch), '')
					and IsNull([objdept],'') = IsNULL(RTRIM(@dept), '')
					and IsNull([objoper],'') = IsNULL(RTRIM(@oper), '')
					and [scope] = @scope
					and ([status] = @status or [status] = 0)--Nov.15, 2023 add status = 0
			end
		INSERT INTO Event (TrnTime, Oper, Type, ObjectType, ObjectId , LogText,EvtDetail) 
		SELECT getDate(), 'Prime', 'Del', 'RoleRights', convert(varchar, convert(varchar, D.[Role])), '' ,
			'|Role : '+ltrim(rtrim(isnull(convert(char,D.[Role]),''))) 
			+ '|Right : '+ltrim(rtrim(isnull(convert(char,D.[Right]),''))) 
			+ '|ObjBranch : '+ltrim(rtrim(isnull(convert(char,D.[ObjBranch]),''))) 
			+ '|ObjDept : '+ltrim(rtrim(isnull(convert(char,D.ObjDept),''))) 
			+ '|ObjOper : '+ltrim(rtrim(isnull(convert(char,D.ObjOper),''))) 
			+ '|Scope : '+ltrim(rtrim(isnull(convert(char,D.Scope),''))) 
			+ '|CreateOper : '+ltrim(rtrim(isnull(convert(char,D.CreateOper),'')))
			+ '|CreateDate : '+ltrim(rtrim(isnull(convert(char,D.CreateDate),''))) 
			+ '|LastOper : '+ltrim(rtrim(isnull(convert(char,D.LastOper),''))) 
			+ '|LastModify : '+ltrim(rtrim(isnull(convert(char,D.LastModify),'')))
			+' |'
		FROM Deleted D
	END
GO


