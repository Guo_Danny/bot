use psec
go

--Query
select code, UserName,LastPwdChgDate, ExpirationDate from psec..Operator where Enable = 1


--Update expire date
declare @pwsexpperiod int

select @pwsexpperiod = ParameterValue from psec..OptionParameter where ParameterCode = 'PwdExpPer'

begin tran
update psec..Operator set ExpirationDate = DATEADD(day,@pwsexpperiod, LastPwdChgDate) where Enable = 1

commit
