USE [OFAC]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [MatchNameIdx_MatchTable]    Script Date: 8/16/2023 3:39:39 PM ******/
CREATE NONCLUSTERED INDEX [MatchNameIdx_MatchTable] ON [dbo].[MatchTable]
(
	[MatchName] ASC,
	[MatchText] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO


