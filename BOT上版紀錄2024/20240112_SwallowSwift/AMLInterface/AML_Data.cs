﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using log4net;
using System.Net;
using System.Security.Cryptography;
using System.IO;
using Newtonsoft.Json;
using System.Collections;
using System.Xml;
using System.Reflection;
using log4net.Repository.Hierarchy;

namespace BlackListService
{
    public class AML_Data
    {
        private Database database;

        private static readonly ILog log = LogManager.GetLogger("App.Logging");

        private string gatewayhost = String.Empty;
        private string gatewayurl = String.Empty;
        private string apikey = String.Empty;
        private string apiurl = String.Empty;
        private string apisecret = String.Empty;
        private string secondaryFields = String.Empty;
        private string entityType = String.Empty;
        private string customFields = String.Empty;
        private string typeid = String.Empty;
        private string groupId = String.Empty;
        private string providerTypes = String.Empty;
        private int basenum = 0;
        private string strProxy = String.Empty;
        private string str_1000 = String.Empty;
        private string TR1 = String.Empty;
        private string TR2 = String.Empty;
        private string TR3 = String.Empty;
        private string str_exclude = String.Empty;
        private int apiretry = 5;
        private int retrySec = 10;
        private string OFACWS_Enable = "N";
        private string OFACWS_DB = "N";
        private string OFACWS_Currency_Tag = "";
        private string OFACWS_SSL = "";
        private string OFACWS_FILEIMG = "";
        private string OFACWS_USR = "";
        private string OFACWS_PW = "";
        private string OFACWS_COMBINELINE = "";
        private DataTable dt_SwiftTag = new DataTable();
        private string ScanTag_start = "";
        private string ScanTag_end = "";
        private string OFACWS_SRC = "";
        private string OFACWS_NOTES = "";
        private int OFACWS_MAXMATCH = 100;

        public AML_Data(bool bol_log = false)
        {
            database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

            XmlDocument doc = new XmlDocument();
            Fortify fortifyService = new Fortify();

            doc.XmlResolver = null;
            doc.Load(fortifyService.PathManipulation(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + @"\Setting.xml"));
            XmlNodeList settingnodes = doc.SelectNodes("//Setting");
            foreach (XmlNode node in settingnodes)
            {
                gatewayhost = node["gatewayhost"].InnerText.Replace("\r\n", "").Trim();
                gatewayurl = node["gatewayurl"].InnerText.Replace("\r\n", "").Trim();
                apikey = node["apikey"].InnerText.Replace("\r\n", "").Trim();
                apiurl = node["apiurl"].InnerText.Replace("\r\n", "").Trim();
                apisecret = node["apisecret"].InnerText.Replace("\r\n", "").Trim();
                secondaryFields = node["secondaryFields"].InnerText.Replace("\r\n", "").Trim();
                entityType = node["entityType"].InnerText.Replace("\r\n", "").Trim();
                customFields = node["customFields"].InnerText.Replace("\r\n", "").Trim();
                typeid = node["typeID"].InnerText.Replace("\r\n", "").Trim();
                groupId = node["groupId"].InnerText.Replace("\r\n", "").Trim();
                providerTypes = node["providerTypes"].InnerText.Replace("\r\n", "").Trim();
                basenum = int.Parse(node["basenum"].InnerText.Replace("\r\n", "").Trim());
                strProxy = node["goproxy"].InnerText.Replace("\r\n", "").Trim();
                str_1000 = node["type1000"].InnerText.Replace("\r\n", "").Trim();
                TR1 = node["TradeMark"].InnerText.Replace("\r\n", "").Trim();
                TR2 = node["TradeMark2"].InnerText.Replace("\r\n", "").Trim();
                TR3 = node["TradeMark3"].InnerText.Replace("\r\n", "").Trim();
                str_exclude = node["excludenames"].InnerText.Replace("\r\n", "").Trim();
                apiretry = int.Parse(node["apiretry"].InnerText.Replace("\r\n", "").Trim());
                retrySec = int.Parse(node["apiretrySec"].InnerText.Replace("\r\n", "").Trim());

                OFACWS_Enable = node["OFACWS"].InnerText.Replace("\r\n", "").Trim();
                OFACWS_DB = node["OFACWS_DB"].InnerText.Replace("\r\n", "").Trim();
                OFACWS_Currency_Tag = node["OFACWS_CURRENCY"].InnerText.Replace("\r\n", "").Trim();
                OFACWS_SSL = node["OFACWS_SSL"].InnerText.Replace("\r\n", "").Trim();
                OFACWS_FILEIMG = node["OFACWS_FILEIMG"].InnerText.Replace("\r\n", "").Trim();
                OFACWS_USR = node["OFACWS_USR"].InnerText.Replace("\r\n", "").Trim();
                OFACWS_PW = node["OFACWS_PW"].InnerText.Replace("\r\n", "").Trim();
                OFACWS_COMBINELINE = node["OFACWS_COMBINELINE"].InnerText.Replace("\r\n", "").Trim();
                ScanTag_start = node["OFAC_ScanTag_S"].InnerText.Replace("\r\n", "").Trim();
                ScanTag_end = node["OFAC_ScanTag_E"].InnerText.Replace("\r\n", "").Trim();
                OFACWS_SRC = node["OFACWS_SRC"].InnerText.Replace("\r\n", "").Trim();
                OFACWS_NOTES = node["OFACWS_NOTES"].InnerText.Replace("\r\n", "").Trim();
                
                OFACWS_MAXMATCH = int.Parse(node["OFACWS_MAXMATCH"].InnerText.Replace("\r\n", "").Trim());
                
                if (bol_log)
                {
                    LogAdd("Load parameter:OFACWS_MAXMATCH=" + OFACWS_MAXMATCH);
                    LogAdd("parameter loaded");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imatchcount"></param>
        /// <param name="strRef"></param>
        /// <param name="strUserMsg"></param>
        public void Updatematchcount(int imatchcount, string strRef, string strUserMsg)
        {
            Fortify fortifyService = new Fortify();
            string sql = "update FilterTranTable set MatchCount = @match, UserMessageReference = @usermsg ";
            sql += "where Ref = @ref ;";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@match", fortifyService.PathManipulation(imatchcount.ToString())));
                cmd.Parameters.Add(new SqlParameter("@usermsg", fortifyService.PathManipulation(strUserMsg)));
                cmd.Parameters.Add(new SqlParameter("@ref", fortifyService.PathManipulation(strRef)));

                ExecuteSQL(cmd);
            }
        }

        /// <summary>
        /// CheckSdn for World-Check
        /// </summary>
        /// <param name="strEntnum"></param>
        /// <param name="strName"></param>
        /// <param name="strBranch"></param>
        /// <param name="strprogram"></param>
        /// <param name="strreferenceId"></param>
        /// <param name="strmatchedNameType"></param>
        /// <param name="strsources"></param>
        /// <param name="strcategories"></param>
        /// <returns></returns>
        public string CheckSdn(string strEntnum,string strName,string strBranch,string strprogram,string strreferenceId, string strmatchedNameType, string[] strsources,string[] strcategories)
        {
            string sql = "";
            string rEntnum = "";
            Fortify fortifyService = new Fortify();
            string sour = "";
            string cate = "";
            foreach (string s in strsources)
            {
                sour = sour + s + ",";
            }
            sour = sour.TrimEnd(',');

            foreach (string s in strcategories)
            {
                cate = cate + s + ",";
            }
            cate = cate.TrimEnd(',');

            strEntnum = (int.Parse(strEntnum) + basenum).ToString();

            sql = @"select entnum from ofac.dbo.sdntable (nolock) 
                      where entnum = @ent;";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.CommandText = sql;

                database.AddInParameter(cmd, "@ent", DbType.Int32, strEntnum);

                IDataReader reader = database.ExecuteReader(cmd);
                try
                {
                    while (reader.Read())
                    {
                        rEntnum = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }

                if (rEntnum == "")
                {
                    string remark = "";
                    remark += TR1 + " \r\n";
                    remark += TR2 + " \r\n";
                    remark += TR3 + " \r\n";
                    remark += "Added by World-Check one API. \r\n";
                    remark += "referenceId:" + strreferenceId + " \r\n";
                    remark += "matchedNameType:" + strmatchedNameType + " \r\n";
                    remark += "sources:" + sour + " \r\n";
                    remark += "categories:" + cate + " \r\n";

                    //insert sdntable
                    sql = "insert into OFAC..sdntable(entnum,name,listtype,program,type,primeadded,remarks,sdntype,datatype,userrec,deleted,duplicrec,ignorederived,status,";
                    sql += "Listcreatedate,listmodifdate,createdate,lastmodifdate,lastoper)";
                    sql += "select @entnum,@name,@listtype,@program,'other',0,@remark,1,1,1,0,0,0,1,";
                    sql += "getdate(),getdate(),getdate(),getdate(),'PrimeAdmin';";

                    cmd.CommandText = sql;

                    cmd.Parameters.Add(new SqlParameter("@entnum", fortifyService.PathManipulation(strEntnum)));
                    cmd.Parameters.Add(new SqlParameter("@name", fortifyService.PathManipulation(strName)));
                    cmd.Parameters.Add(new SqlParameter("@listtype", "WC1"));
                    cmd.Parameters.Add(new SqlParameter("@program", fortifyService.PathManipulation(strprogram)));
                    cmd.Parameters.Add(new SqlParameter("@remark", fortifyService.PathManipulation(remark)));

                    LogAdd("insert sdn ent:" + fortifyService.PathManipulation(strEntnum) + ",name:" + fortifyService.PathManipulation(strName));

                    ExecuteSQL(cmd);
                }
            }
            return rEntnum;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strSeqnumb"></param>
        /// <param name="strName"></param>
        /// <param name="strMatchName"></param>
        /// <param name="strBranch"></param>
        /// <param name="strProgram"></param>
        /// <param name="strBlocklisttype"></param>
        /// <returns></returns>
        public string CheckSdn(string strSeqnumb, string strName, string strMatchName, string strBranch, string strProgram, string strBlocklisttype)
        {
            string sql = "";
            string rEntnum = "";
            string sqllisttype = "";
            //string strBlocklisttype = "";
            Fortify fortifyService = new Fortify();

            LogAdd("original name=" + strName + ",MatchName=" + strMatchName + ",Program=" + strProgram);
            
            if(string.IsNullOrEmpty(strName))
            {
                return rEntnum;
            }

            /* Dec.11,2023 move to another function
            sqllisttype = @"select BlockListTypeList from OFAC..SanctionDataRule
                              where code like @branch ";

            using (DbCommand cmd = database.GetSqlStringCommand(sqllisttype))
            {
                cmd.CommandText = sqllisttype;

                database.AddInParameter(cmd, "@branch", DbType.String, OFACWS_FILEIMG);

                IDataReader reader = database.ExecuteReader(cmd);
                try
                {
                    while (reader.Read())
                    {
                        strBlocklisttype = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            LogAdd("BlockListType=" + strBlocklisttype);
            */

            //Test Dec.11,2023 change to a SQL SP
            sql = @"Exec OFAC..OFS_SelMatchEnt @name, @blocklisttype, @program, @SeqNumb, @MatchName ";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.CommandText = sql;

                database.AddInParameter(cmd, "@name", DbType.String, strName);
                if (!string.IsNullOrEmpty(strProgram) && strProgram.ToUpper() != "KEY WORD")
                {
                    database.AddInParameter(cmd, "@program", DbType.String, strProgram);
                }
                else
                {
                    database.AddInParameter(cmd, "@program", DbType.String, "");
                }

                //MAY.13,2022 BOT think do not insert duplicate match name in the same case
                if (!string.IsNullOrEmpty(strSeqnumb) && !string.IsNullOrEmpty(strMatchName))
                {
                    database.AddInParameter(cmd, "@SeqNumb", DbType.String, strSeqnumb);
                    database.AddInParameter(cmd, "@MatchName", DbType.String, strMatchName);
                }
                database.AddInParameter(cmd, "@blocklisttype", DbType.String, strBlocklisttype);
                IDataReader reader = database.ExecuteReader(cmd);
                try
                {
                    while (reader.Read())
                    {
                        rEntnum = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }

            /* Test Dec.11,2023 change to a SQL SP
            //JUN-22-2021 check sdn
            sql = @"select entnum from OFAC..sdntable (nolock) 
                      where rtrim(ltrim(name)) = @name and status != 4 
                      and CHARINDEX(rtrim(ListType),@blocklisttype) = 0";

            if (!string.IsNullOrEmpty(strProgram) && strProgram.ToUpper() != "KEY WORD")
            {
                sql += " and program = @program";
            }

            //MAY.13,2022 BOT think do not insert duplicate match name in the same case
            if(!string.IsNullOrEmpty(strSeqnumb) && !string.IsNullOrEmpty(strMatchName))
            {
                sql += " and EntNum not in (select entNum from ofac.dbo.MatchTable where SeqNumb = @SeqNumb and OriginalSDNName = @name and MatchName = @MatchName)";
            }

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.CommandText = sql;

                database.AddInParameter(cmd, "@name", DbType.String, strName);
                if (!string.IsNullOrEmpty(strProgram) && strProgram.ToUpper() != "KEY WORD")
                {
                    database.AddInParameter(cmd, "@program", DbType.String, strProgram);
                }

                //MAY.13,2022 BOT think do not insert duplicate match name in the same case
                if (!string.IsNullOrEmpty(strSeqnumb) && !string.IsNullOrEmpty(strMatchName))
                {
                    database.AddInParameter(cmd, "@SeqNumb", DbType.String, strSeqnumb);
                    database.AddInParameter(cmd, "@MatchName", DbType.String, strMatchName);
                }
                database.AddInParameter(cmd, "@blocklisttype", DbType.String, strBlocklisttype);
                IDataReader reader = database.ExecuteReader(cmd);
                try
                {
                    while (reader.Read())
                    {
                        rEntnum = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }

            if (rEntnum == "")
            {
                LogAdd("Chk sdnalt");
                //JUN-22-2021 check sdnalt
                sql = @"select sdn.entnum from OFAC..sdnalttable (nolock) sdna
                    join OFAC..sdntable (nolock) sdn on sdn.entnum = sdna.entnum 
                    where rtrim(ltrim(sdna.altname)) = @name and sdn.status != 4 
                    and CHARINDEX(rtrim(sdn.ListType),@blocklisttype) = 0 ";

                if (!string.IsNullOrEmpty(strProgram) && strProgram.ToUpper() != "KEY WORD")
                {
                    sql += " and program = @program";
                }
                
                //MAY.13,2022 BOT think do not insert duplicate match name in the same case
                if (!string.IsNullOrEmpty(strSeqnumb) && !string.IsNullOrEmpty(strMatchName))
                {
                    sql += " and sdn.EntNum not in (select entNum from ofac.dbo.MatchTable where SeqNumb = @SeqNumb and OriginalSDNName = @name and MatchName = @MatchName)";
                }

                using (DbCommand cmd = database.GetSqlStringCommand(sql))
                {
                    cmd.CommandText = sql;

                    database.AddInParameter(cmd, "@name", DbType.String, strName);
                    if (!string.IsNullOrEmpty(strProgram) && strProgram.ToUpper() != "KEY WORD")
                    {
                        database.AddInParameter(cmd, "@program", DbType.String, strProgram);
                    }
                    //MAY.13,2022 BOT think do not insert duplicate match name in the same case
                    if (!string.IsNullOrEmpty(strSeqnumb) && !string.IsNullOrEmpty(strMatchName))
                    {
                        database.AddInParameter(cmd, "@SeqNumb", DbType.String, strSeqnumb);
                        database.AddInParameter(cmd, "@MatchName", DbType.String, strMatchName);
                    }
                    database.AddInParameter(cmd, "@blocklisttype", DbType.String, strBlocklisttype);
                    IDataReader reader = database.ExecuteReader(cmd);
                    try
                    {
                        while (reader.Read())
                        {
                            rEntnum = reader[0].ToString();
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            */

            LogAdd("entnum=" + rEntnum);
            return rEntnum;
        }

        /// <summary>
        /// Inser Recon table reports
        /// </summary>
        /// <param name="strSource"></param>
        /// <param name="strTranSearchType"></param>
        /// <param name="strSancPartyCount"></param>
        /// <param name="strbranch"></param>
        /// <param name="strdepartment"></param>
        /// <param name="strCaseRef"></param>
        /// <param name="strUserMsgRef"></param>
        /// <param name="strSearchOper"></param>
        /// <returns></returns>
        public Boolean InsertRecon(string strSource, string strTranSearchType, string strSancPartyCount, string strbranch, string strdepartment, string strCaseRef, string strUserMsgRef, string strSearchOper)
        {
            Fortify fortifyService = new Fortify();

            Boolean bol = false;
            try
            {
                string sql = @"
insert into OFAC..ReconTable(Source,MsgRef,TranSearchType,SancPartyCount,Branch,Department,CaseReference,UserMessageReference,FileImage,SearchOper,
SearchDate,CreateDate)
select top 1 @Source,
case when @Source = 'Thomson Reuters World-Check One' then 'WC1' else @Source end + '-' + replace(CONVERT(VARCHAR(10),GETDATE(),110),'-','') + '-' + CAST ((select count(Source)+1 from ofac.dbo.ReconTable) as VARCHAR(20)),
@TranSearchType,@SancPartyCount,@branch,@department,@CaseRef,
@UserMsgRef,@fileimg,@SearchOper,getdate(),getdate() from OFAC..recontable where 1 = 1
";
                using (DbCommand cmd = database.GetSqlStringCommand(sql))
                {
                    cmd.Parameters.Add(new SqlParameter("@Source", fortifyService.PathManipulation(strSource)));
                    cmd.Parameters.Add(new SqlParameter("@TranSearchType", fortifyService.PathManipulation(strTranSearchType)));
                    cmd.Parameters.Add(new SqlParameter("@SancPartyCount", fortifyService.PathManipulation(strSancPartyCount)));
                    cmd.Parameters.Add(new SqlParameter("@branch", fortifyService.PathManipulation(strbranch)));
                    cmd.Parameters.Add(new SqlParameter("@department", fortifyService.PathManipulation(strdepartment)));
                    cmd.Parameters.Add(new SqlParameter("@CaseRef", fortifyService.PathManipulation(strCaseRef)));
                    cmd.Parameters.Add(new SqlParameter("@UserMsgRef", fortifyService.PathManipulation(strUserMsgRef)));
                    cmd.Parameters.Add(new SqlParameter("@fileimg", fortifyService.PathManipulation(string.Format("{0}SWIFT", strbranch))));
                    cmd.Parameters.Add(new SqlParameter("@SearchOper", fortifyService.PathManipulation(strSearchOper)));

                    ExecuteSQL(cmd);
                    bol = true;
                }
                return bol;
            }
            catch(Exception ex)
            {
                LogAdd("ex:" + ex.ToString());
                return bol;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strRef"></param>
        /// <param name="strbranch"></param>
        /// <param name="strmatchcount"></param>
        /// <param name="iseqnumb"></param>
        /// <returns></returns>
        public Boolean InsertFilterTran(string strRef,string strbranch, string strmatchcount, out int iseqnumb)
        {
            try
            {
                Fortify fortifyService = new Fortify();
                bool bol_r = false;
                string SQL = "";

                SQL = "insert into OFAC..FilterTranTable (Source,Ref,Branch,Dept,ReqTime,ClientId,TranTime,Result,MatchCount,MsgType,Currency,KeyField1,KeyField2,CIFFieldsDelimiter,LengthKF1,LengthKF2,TransactionType,SystemRef,UserMessageReference,FileImage,App,IsIncremental) ";
                SQL += "select top 1 @Source,@Ref,@Branch,@Dept,getdate(),@ClientId,getdate(),@Result,@MatchCount,@MsgType,@Currency,@KeyField1,@KeyField2,@CIFFieldsDelimiter,";
                SQL += "@LengthKF1,@LengthKF2,@TransactionType,@SystemRef,@Ref,@FileImage,@App,@IsIncremental ";

                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.Parameters.Add(new SqlParameter("@Source", "Thomson Reuters World-Check One"));
                    cmd.Parameters.Add(new SqlParameter("@Ref", fortifyService.PathManipulation(strRef)));
                    cmd.Parameters.Add(new SqlParameter("@branch", fortifyService.PathManipulation(strbranch)));
                    cmd.Parameters.Add(new SqlParameter("@Dept", "AML"));
                    cmd.Parameters.Add(new SqlParameter("@ClientId", "8888"));
                    cmd.Parameters.Add(new SqlParameter("@Result", "1"));
                    cmd.Parameters.Add(new SqlParameter("@MatchCount", fortifyService.PathManipulation(strmatchcount)));
                    cmd.Parameters.Add(new SqlParameter("@MsgType", "1"));
                    cmd.Parameters.Add(new SqlParameter("@Currency", "USD"));
                    cmd.Parameters.Add(new SqlParameter("@KeyField1", "0"));
                    cmd.Parameters.Add(new SqlParameter("@KeyField2", "0"));
                    cmd.Parameters.Add(new SqlParameter("@CIFFieldsDelimiter", "NULL"));
                    cmd.Parameters.Add(new SqlParameter("@LengthKF1", "0"));
                    cmd.Parameters.Add(new SqlParameter("@LengthKF2", "0"));
                    cmd.Parameters.Add(new SqlParameter("@TransactionType", "0"));
                    cmd.Parameters.Add(new SqlParameter("@SystemRef", string.Format("WC1-{0}-{1}-0",DateTime.Now.ToString("MMddyyyy"), DateTime.Now.ToString("Hmmss"))));
                    cmd.Parameters.Add(new SqlParameter("@FileImage", "WC1"));
                    cmd.Parameters.Add(new SqlParameter("@App", "0"));
                    cmd.Parameters.Add(new SqlParameter("@IsIncremental", "0"));

                    ExecuteSQL(cmd);
                    bol_r = true;
                }
                LogAdd("InsertFilterTran:"+ bol_r);

                // get seqnumb after insert sql executed
                iseqnumb = 0;
                SQL = "SELECT IDENT_CURRENT ('OFAC..FilterTranTable') AS Current_Identity;";
                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.CommandText = SQL;

                    IDataReader reader = database.ExecuteReader(cmd);
                    try
                    {
                        while (reader.Read())
                        {
                            iseqnumb = int.Parse(reader[0].ToString());
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }

                LogAdd("InsertFilterTran:" + bol_r + ", seqnum=" + iseqnumb);

                return bol_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strRef"></param>
        /// <param name="strbranch"></param>
        /// <param name="strmatchcount"></param>
        /// <param name="str_currency"></param>
        /// <param name="iseqnumb"></param>
        /// <param name="str_fileimg"></param>
        /// <returns></returns>
        public Boolean InsertFilterTran_OFACWS(string strRef, string strbranch, string strmatchcount, string str_currency, out int iseqnumb, string str_fileimg)
        {
            try
            {
                Fortify fortifyService = new Fortify();
                bool bol_r = false;
                string SQL = "";

                SQL = "DECLARE @seqNumb int ;exec [dbo].[OFS_AddFilterTran] ";
                SQL += "@Source,@Ref,@ClientId,@Branch,@Dept,@Result,@MatchCount,@MsgType,@now,@confirmState,@KeyField1,@KeyField2,@CIFFieldsDelimiter,";
                SQL += "@LengthKF1,@LengthKF2,@FileImage,@CIFRule,@SystemRef,@TransactionType,@amount,@IsIncremental,@Ref,@seqNumb output; ";
                SQL += "select @seqNumb;";

                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.Parameters.Add(new SqlParameter("@Source", "WS_SWIFT"));
                    cmd.Parameters.Add(new SqlParameter("@Ref", fortifyService.PathManipulation(strRef)));
                    cmd.Parameters.Add(new SqlParameter("@branch", fortifyService.PathManipulation(strbranch)));
                    cmd.Parameters.Add(new SqlParameter("@Dept", "AML"));
                    cmd.Parameters.Add(new SqlParameter("@ClientId", fortifyService.PathManipulation(strbranch.Substring(1))));
                    cmd.Parameters.Add(new SqlParameter("@Result", "1"));
                    cmd.Parameters.Add(new SqlParameter("@MatchCount", fortifyService.PathManipulation(strmatchcount)));
                    cmd.Parameters.Add(new SqlParameter("@MsgType", "1"));
                    cmd.Parameters.Add(new SqlParameter("@now", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@confirmState", "0"));
                    cmd.Parameters.Add(new SqlParameter("@CIFRule", string.Empty));
                    cmd.Parameters.Add(new SqlParameter("@KeyField1", "0"));
                    cmd.Parameters.Add(new SqlParameter("@KeyField2", "0"));
                    cmd.Parameters.Add(new SqlParameter("@CIFFieldsDelimiter", string.Empty));
                    cmd.Parameters.Add(new SqlParameter("@LengthKF1", "0"));
                    cmd.Parameters.Add(new SqlParameter("@LengthKF2", "0"));
                    cmd.Parameters.Add(new SqlParameter("@TransactionType", "0"));
                    cmd.Parameters.Add(new SqlParameter("@SystemRef", string.Format("WS-{0}-{1}-0", DateTime.Now.ToString("MMddyyyy"), DateTime.Now.ToString("Hmmss"))));
                    cmd.Parameters.Add(new SqlParameter("@FileImage", fortifyService.PathManipulation(str_fileimg)));
                    cmd.Parameters.Add(new SqlParameter("@amount", "0"));
                    cmd.Parameters.Add(new SqlParameter("@IsIncremental", "0"));

                    //ExecuteSQL(cmd);
                    iseqnumb = 0;
                    IDataReader reader = database.ExecuteReader(cmd);
                    try
                    {
                        while (reader.Read())
                        {
                            iseqnumb = int.Parse(reader[0].ToString());
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }

                    bol_r = true;
                }

                LogAdd("InsertFilterTran:" + bol_r + ", seqnum=" + iseqnumb);
                return bol_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strRef"></param>
        /// <param name="strentNum"></param>
        /// <param name="strEngName"></param>
        /// <param name="strOrgSDN"></param>
        /// <param name="Strength"></param>
        /// <param name="iseqnumb"></param>
        /// <returns></returns>
        public Boolean InsertMatchTable(string strRef,string strentNum,string strEngName, string strOrgSDN,string Strength, int iseqnumb)
        {
            try
            {
                Fortify fortifyService = new Fortify();

                strentNum = (int.Parse(strentNum) + basenum).ToString();

                bool bol_r = false;

                string SQL = "";
                SQL = "insert into OFAC..MatchTable (SeqNumb,MatchName,OriginalSDNName,MatchText,MatchCountry,MatchType,Score,entNum) ";
                SQL += "select top 1 @SeqNumb, @MatchName,@OriginalSDNName,@MatchText,@MatchCountry,@MatchType,@Score,@entNum ";
                SQL += "from OFAC..FilterTranTable where Ref = @Ref order by ReqTime DESC ";

                int iscore = 0;
                if (Strength.ToUpper() == "STRONG")
                {
                    iscore = 95;
                }
                else if (Strength.ToUpper() == "MEDIUM")
                {
                    iscore = 85;
                }
                else if (Strength.ToUpper() == "WEAK")
                {
                    iscore = 75;
                }
                else if (Strength.ToUpper() == "EXACT")
                {
                    iscore = 100;
                }

                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.Parameters.Add(new SqlParameter("@SeqNumb", fortifyService.PathManipulation(iseqnumb.ToString())));
                    cmd.Parameters.Add(new SqlParameter("@Ref", fortifyService.PathManipulation(strRef)));
                    cmd.Parameters.Add(new SqlParameter("@MatchName", fortifyService.PathManipulation(strEngName)));
                    cmd.Parameters.Add(new SqlParameter("@OriginalSDNName", fortifyService.PathManipulation(strOrgSDN)));
                    cmd.Parameters.Add(new SqlParameter("@MatchText", fortifyService.PathManipulation(strEngName)));
                    cmd.Parameters.Add(new SqlParameter("@MatchCountry", "WC1"));
                    cmd.Parameters.Add(new SqlParameter("@MatchType", "other"));
                    cmd.Parameters.Add(new SqlParameter("@Score", fortifyService.PathManipulation(iscore.ToString())));
                    cmd.Parameters.Add(new SqlParameter("@entNum", fortifyService.PathManipulation(strentNum)));

                    ExecuteSQL(cmd);
                    bol_r = true;
                }
                LogAdd(string.Format("InsertMatchTable:{0},strRef={1},strentNum={2},seqnumb={3}", fortifyService.PathManipulation(bol_r.ToString()), fortifyService.PathManipulation(strRef), fortifyService.PathManipulation(strentNum),fortifyService.PathManipulation(iseqnumb.ToString())));
                return bol_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strRef"></param>
        /// <param name="strentNum"></param>
        /// <param name="strEngName"></param>
        /// <param name="strOrgSDN"></param>
        /// <param name="iscore"></param>
        /// <param name="strMatchtext"></param>
        /// <param name="iseqnumb"></param>
        /// <returns></returns>
        public Boolean InsertMatchTable(string strRef, string strentNum, string strEngName, string strOrgSDN, string iscore, string strMatchtext, int iseqnumb)
        {
            try
            {
                Fortify fortifyService = new Fortify();

                //strentNum = (int.Parse(strentNum) + basenum).ToString();

                bool bol_r = false;
                string sqlcheck_matchtext = "";
                string str_backMatchtext = strMatchtext.Trim();
                sqlcheck_matchtext = @"select OFAC.dbo.[OFS_fnGetMatchText](@sSrhName, @sMatchName) ret";

                LogAdd("select OFAC.dbo.[OFS_fnGetMatchText]('" + strMatchtext.Trim() + "','" + strEngName.Trim() + "')");
                //Dec.09.2021 process the crlf in scan data
                //sqlcheck_matchtext = "select OFAC.dbo.[OFS_fnGetMatchText]('" + strMatchtext.Replace("^", "' + char(13) + char(10) + '").Trim() + "', '" + strEngName.Trim() + "') ret";

                //LogAdd("call fn SQL=" + sqlcheck_matchtext);

                using (DbCommand dBCommand = database.GetSqlStringCommand(sqlcheck_matchtext))
                {
                    database.AddInParameter(dBCommand, "@sSrhName", DbType.String, strMatchtext.Trim());
                    database.AddInParameter(dBCommand, "@sMatchName", DbType.String, strEngName.Trim());

                    try
                    {
                        //May. 09, 2022 add exception catch
                        DataSet ds = database.ExecuteDataSet(dBCommand);

                        if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                        {
                            strMatchtext = ds.Tables[0].Rows[0]["ret"].ToString();
                            LogAdd("chk_return:" + strMatchtext);
                        }

                        if(string.IsNullOrEmpty(strMatchtext.Trim()))
                        {
                            LogAdd("Call Match text function get empty");
                            strMatchtext = str_backMatchtext.Replace(" |", Environment.NewLine);
                        }
                    }

                    catch (Exception e)
                    {
                        Logerr("Call Match text function error:" + e.ToString());
                        strMatchtext = str_backMatchtext.Replace(" |", Environment.NewLine);
                    }
                    finally
                    {
                        dBCommand.Dispose();
                    }
                }

                LogAdd("return fnGetMatchText = " + strMatchtext.Trim());

                //Mar 14, 2022 Check Match name, avoid inert duplicate match name in one case
                sqlcheck_matchtext = "select count(*) MatchCnt from OFAC..MatchTable ";
                sqlcheck_matchtext += "where seqnumb = @SeqNumb and MatchName = @MatchName ";
                sqlcheck_matchtext += "and MatchText = @MatchText and entnum = @entnum ";

                //AUG-2021 HK requested
                if(strMatchtext.Replace("  ", " ").LastIndexOf(strEngName) >= 0)
                {
                    strMatchtext = strEngName;
                }

                int str_chkMatchName = 0;

                using (DbCommand dBCommand = database.GetSqlStringCommand(sqlcheck_matchtext))
                {
                    database.AddInParameter(dBCommand, "@SeqNumb", DbType.String, fortifyService.PathManipulation(iseqnumb.ToString()));
                    database.AddInParameter(dBCommand, "@MatchName", DbType.String, fortifyService.PathManipulation(strEngName));
                    database.AddInParameter(dBCommand, "@MatchText", DbType.String, fortifyService.PathManipulation(strMatchtext));
                    database.AddInParameter(dBCommand, "@entnum", DbType.String, fortifyService.PathManipulation(strentNum));

                    DataSet ds = database.ExecuteDataSet(dBCommand);
                    try
                    {
                        if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                        {
                            str_chkMatchName = int.Parse(ds.Tables[0].Rows[0]["MatchCnt"].ToString());
                        }
                    }
                    finally
                    {
                        dBCommand.Dispose();
                    }
                }

                string SQL = "";
                //Mar.09, 2022 LN request to correct the matchcountry data which is the sdn program.
                SQL = "insert into OFAC..MatchTable (SeqNumb,MatchName,OriginalSDNName,MatchText,MatchCountry,MatchType,Score,entNum) ";
                SQL += "select top 1 @SeqNumb, @MatchName,@OriginalSDNName,@MatchText,(select isnull(program,'') program from ofac..sdntable(nolock) where entnum = @entNum),(select isnull(type,'other') type from ofac..sdntable(nolock) where entnum = @entNum),@Score,@entNum ";
                SQL += "from OFAC..FilterTranTable where Ref = @Ref order by ReqTime DESC ";

                if(str_chkMatchName == 0)
                {
                    using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                    {

                        cmd.Parameters.Add(new SqlParameter("@SeqNumb", fortifyService.PathManipulation(iseqnumb.ToString())));
                        cmd.Parameters.Add(new SqlParameter("@Ref", fortifyService.PathManipulation(strRef)));
                        cmd.Parameters.Add(new SqlParameter("@MatchName", fortifyService.PathManipulation(strEngName)));
                        cmd.Parameters.Add(new SqlParameter("@OriginalSDNName", fortifyService.PathManipulation(strOrgSDN)));
                        cmd.Parameters.Add(new SqlParameter("@MatchText", fortifyService.PathManipulation(strMatchtext)));
                        //cmd.Parameters.Add(new SqlParameter("@MatchCountry", "OFACWS"));
                        cmd.Parameters.Add(new SqlParameter("@MatchType", "other"));
                        cmd.Parameters.Add(new SqlParameter("@Score", fortifyService.PathManipulation(iscore.ToString())));
                        cmd.Parameters.Add(new SqlParameter("@entNum", fortifyService.PathManipulation(strentNum)));

                        ExecuteSQL(cmd);
                        bol_r = true;
                    }

                    LogAdd(string.Format("InsertMatchTable:{0},strRef={1},strentNum={2}", fortifyService.PathManipulation(bol_r.ToString()), fortifyService.PathManipulation(strRef), fortifyService.PathManipulation(strentNum)));

                }
                else
                {
                    bol_r = true;
                    LogAdd("Match Name in current case exist, pass insert.");
                }
                return bol_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strRef"></param>
        /// <param name="strcif_file"></param>
        /// <param name="UserMsg"></param>
        /// <param name="iseqnumb"></param>
        /// <returns></returns>
        public Boolean InsertMsgTable(string strRef, string strcif_file, ref string UserMsg, int iseqnumb)
        {
            Fortify fortifyService = new Fortify();

            StringWriter strWriter = new StringWriter();

            bool bol_r = false;

            try
            {
                LogAdd("begin to insert MsgTable.");

                string strMsg = "";
                string line = "";
                String SQL = "";

                if (strcif_file.Contains("FR") || strcif_file.Contains("FS"))
                {
                    using (System.IO.StreamReader file = new System.IO.StreamReader(fortifyService.PathManipulation(strcif_file)))
                    {
                        while ((line = file.ReadLine()) != null)
                        {
                            strMsg += line;
                            strWriter.WriteLine(line);
                        }
                        file.Dispose();
                    }
                }
                else
                {
                    UnisysAMLInterface unisysAML = new UnisysAMLInterface();
                    string[] strTag4 = unisysAML.CaptureBlock4String(fortifyService.PathManipulation(strcif_file), ScanTag_start, ScanTag_end, ref UserMsg);

                    for (int i = 0; i < strTag4.Length; i++)
                    {
                        //LogAdd("block4 line:" + strTag4[i]);
                        strMsg += strTag4[i];
                        strWriter.WriteLine(strTag4[i]);
                    }
                }

                SQL = "";

                SQL = @"OFAC..OFS_AddFilterMsg";
                using (DbCommand dBCommand = database.GetStoredProcCommand(SQL))
                {
                    database.AddInParameter(dBCommand, "@seqNumb", DbType.Int64, fortifyService.PathManipulation(iseqnumb.ToString()));
                    database.AddInParameter(dBCommand, "@msgType", DbType.String, 1);
                    database.AddInParameter(dBCommand, "@msgLen", DbType.String, fortifyService.PathManipulation(strMsg).Length);
                    database.AddInParameter(dBCommand, "@msg", DbType.String, fortifyService.PathManipulation(strWriter.ToString()));
                    database.ExecuteScalar(dBCommand);
                    bol_r = true;
                }

                LogAdd("end insert MsgTable.");
                
            }
            catch (Exception ex)
            {
                strWriter.Dispose();

                throw ex;
            }
            finally
            {
                strWriter.Dispose();
            }

            return bol_r;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iCount"></param>
        /// <param name="iseqNum"></param>
        /// <param name="strUserMsg"></param>
        /// <returns></returns>
        public Boolean UpdateMatchCount(int iCount, int iseqNum, string strUserMsg)
        {
            try
            {
                Fortify fortifyService = new Fortify();
                bool bol_r = false;

                LogAdd(string.Format("Update match count:{0},strseqNum={1}", fortifyService.PathManipulation(iCount.ToString()), fortifyService.PathManipulation(iseqNum.ToString())));

                string SQL = "";
                SQL = "update OFAC..FilterTranTable set MatchCount = @count, UserMessageReference = @usermsg  where SeqNumb = @strseqNum ";

                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.Parameters.Add(new SqlParameter("@count", fortifyService.PathManipulation(iCount.ToString())));
                    cmd.Parameters.Add(new SqlParameter("@usermsg", fortifyService.PathManipulation(strUserMsg)));
                    cmd.Parameters.Add(new SqlParameter("@strseqNum", fortifyService.PathManipulation(iseqNum.ToString())));

                    ExecuteSQL(cmd);
                    bol_r = true;
                }

                return bol_r;

            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get the Currency from swift
        /// </summary>
        /// <param name="str_Ref"></param>
        /// <param name="str_file"></param>
        /// <param name="str_tag"></param>
        /// <returns></returns>
        public String GetCurrency(string str_Ref, string str_file, string str_tag)
        {
            Fortify fortifyService = new Fortify();
            System.IO.StreamReader file = new System.IO.StreamReader(fortifyService.PathManipulation(str_file));
            try
            {   
                string str_r = "USD";
   
                string line = "";
                //StringWriter strWriter = new StringWriter();

                string str_refMT = str_Ref.Substring(14, 4).Trim();

                while ((line = file.ReadLine()) != null)
                {
                    foreach(string stag in str_tag.Split(','))
                    {
                        if (line.Contains(stag))
                        {
                            if (stag == ":32A:" && (str_refMT == "103" || str_refMT == "202" || str_refMT == "202C" || str_refMT == "205" || str_refMT == "205C" || str_refMT.Substring(0,1) == "4"))
                            {
                                str_r = line.Substring(11, 3);
                            }
                            else if (stag == ":32B:" && (str_refMT == "203" || str_refMT == "210"  || str_refMT.Substring(0, 1) == "3" || str_refMT.Substring(0, 1) == "4" || str_refMT.Substring(0, 1) == "7"))
                            {
                                str_r = line.Substring(5, 3);
                            }
                            else if (stag == ":32D:" && (str_refMT == "190" || str_refMT == "290"))
                            {
                                str_r = line.Substring(11, 3);
                            }
                            else if ((stag == ":33B:" || stag == ":32C:" || stag == ":32H:" || stag == ":32M:" || stag == ":33E:" || stag == ":33H:" || stag == ":33T:" || stag == ":33S:" || stag == ":33V:" || stag == ":34B:" || stag == ":34H:" || stag == ":34G:" || stag == ":71F:") && (str_refMT.Substring(0, 1) == "3" || str_refMT.Substring(0, 1) == "5"))
                            {
                                str_r = line.Substring(5, 3);
                            }
                            else if (stag == ":34A:" && (str_refMT.Substring(0, 1) == "5"))
                            {
                                str_r = line.Substring(11, 3);
                            }
                            else if ((stag == ":33C:" || stag == ":32D:") && (str_refMT.Substring(0, 1) == "4"))
                            {
                                str_r = line.Substring(11, 3);
                            }
                            else if (stag == ":90B:" && (str_refMT.Substring(0, 1) == "5"))
                            {
                                str_r = line.Substring(17, 3);
                            }
                            else if ((stag == ":11A:" || stag == ":92F:" || stag == ":92H:") && (str_refMT.Substring(0, 1) == "5"))
                            {
                                str_r = line.Substring(12, 3);
                            }
                            else if ((stag == ":60M:" || stag == ":62M:" || stag == ":60F:" || stag == ":62F:") && (str_refMT.Substring(0, 1) == "9"))
                            {
                                str_r = line.Substring(11, 3);
                            }
                            else if ((stag == ":64:" || stag == ":65:") && (str_refMT.Substring(0, 1) == "9"))
                            {
                                str_r = line.Substring(10, 3);
                            }
                            else if ((stag == ":90C:" || stag == ":90D:") && (str_refMT.Substring(0, 1) == "9"))
                            {
                                str_r = line.Substring(10, 3);
                            }
                        }
                    }
                    //strWriter.WriteLine(line);
                }
                LogAdd("currency=" + str_r);
                return str_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                file.Close();
            }
        }
        /// <summary>
        /// Query & Get the Condiftion for Swift Tag which are enabled
        /// </summary>
        /// <returns></returns>
        public DataTable GetSwiftTag()
        {
            DataTable dtswifttag = new DataTable();
            try
            {
                
                string SQL = "";
                SQL = "select tag, mt from ofac.dbo.SwiftTag where Enable = 1";
                //Oct.26.2021 BOT request to disable the Condition check
                //SQL = "select tag, mt, Condition from SwiftTag where Enable = 1";
                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.CommandText = SQL;

                    DataSet ds = database.ExecuteDataSet(cmd);

                    dtswifttag = ds.Tables[0];

                    cmd.Dispose();
                }
            }
            catch(Exception ex)
            {
                Logerr("Get swift tag error:" + ex.ToString());
            }
            return dtswifttag;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strcif_file"></param>
        /// <param name="str_filename"></param>
        /// <param name="strReferenceNumber"></param>
        /// <param name="strBankNo"></param>
        /// <param name="strBranchNo"></param>
        /// <param name="intretry"></param>
        /// <param name="intdelay"></param>
        /// <param name="str_nation_col"></param>
        /// <param name="str_engname_col"></param>
        /// <param name="strRef"></param>
        /// <param name="strEngName"></param>
        /// <param name="iScore"></param>
        /// <param name="str_score"></param>
        /// <param name="str_dob_col"></param>
        /// <param name="str_cif_type"></param>
        /// <param name="bolmatch"></param>
        /// <param name="strWCO"></param>
        /// <returns></returns>
        public Boolean checkPEPS(string strcif_file, string str_filename, string strReferenceNumber, string strBankNo, string strBranchNo, int intretry, int intdelay, string str_nation_col, string str_engname_col, ref string strRef, ref string strEngName, ref int iScore, string str_score, string str_dob_col, string str_cif_type, ref bool bolmatch, string strWCO)
        {
            bool quit = false;
            bool bol_r = false;
            int intloop = 0;
            Fortify fortifyService = new Fortify();

            //fs = new FileStream(fortifyService.PathManipulation(strcif_file), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using (System.IO.TextReader file = new System.IO.StreamReader(new FileStream(fortifyService.PathManipulation(strcif_file), FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                while (!quit && intloop < intretry)
                {
                    try
                    {
                        intloop = intloop + 1;

                        string strNation = "";
                        string streName = "";
                        string strDob = "";
                        string strUserMsg = "";

                        int iDOB = 0;

                        string[] str_ciftp = str_cif_type.Split('|');

                        string[] str_nationtp = str_nation_col.Split('|');
                        string[] str_engnametp = str_engname_col.Split('|');

                        //string[] str_nation = str_nation_col.Split(',');
                        //string[] str_engname = str_engname_col.Split(',');

                        string line = "";
                        string line_steam = "";
                        string[] arry_list;
                        string str_split = "";
                        bool bol_keeplineflag = false;
                        //2021, Mar. add swift tag config table.
                        dt_SwiftTag = GetSwiftTag();
                        LogAdd(string.Format("Try:{0}", intloop));

                        //Oct.26.2021 add count flag for all no match tag, need go to normal scan.
                        int imatchcount = 0;

                        //2017/04/24 by danny 修改 取代換行 
                        //SEP-14-2021 BOT think I should not change the WC1 name chapture logic
                        if (strWCO == "Y")
                        {
                            //SEP-14-2021 for WC1
                            while ((line = file.ReadLine()) != null)
                            {
                                if (str_filename.Substring(4, 2).ToUpper() == "CI")
                                {
                                    line_steam += line;
                                }
                                else if (str_filename.Substring(4, 1).ToUpper() == "S" && str_filename.Substring(14, 4).ToString().Trim() != "1000")
                                {
                                    if ((line.LastIndexOf(":59") > -1 || line.LastIndexOf(":50") > -1) && line.LastIndexOf(":50A") == -1)
                                    {

                                        if (line.LastIndexOf(":59:") >= 0 || line.LastIndexOf(":50K:") >= 0)
                                        {
                                            if (line.Substring(line.LastIndexOf(":59:") + 4, 1) == "/" || line.Substring(line.LastIndexOf(":50K:") + 5, 1) == "/")
                                            {
                                                line = file.ReadLine();
                                                line_steam += line + ";";
                                            }
                                            else
                                            {
                                                line_steam += line.LastIndexOf(":59") >= 0 ? parstr(line, ":59") : parstr(line, ":50") + ";";
                                            }
                                        }
                                        else if (line.LastIndexOf(":59F:") >= 0 || line.LastIndexOf(":50F:") >= 0)
                                        {
                                            if (line.Substring(line.LastIndexOf(":59F") + 4, 2) == "1/")
                                            {
                                                line_steam += line.LastIndexOf(":59") >= 0 ? parstr(line, ":59F") : parstr(line, ":50") + ";";
                                            }
                                            else
                                            {
                                                line = file.ReadLine();
                                                line_steam += line.LastIndexOf(":59") >= 0 ? parstr(line, ":59F") : parstr(line, ":50F") + ";";
                                            }
                                        }
                                    }
                                }
                                else if (str_filename.Substring(4, 1).ToUpper() == "F" && str_filename.Substring(14, 4).ToString().Trim() == "1000")
                                {
                                    for (int i = 0; i < str_1000.Split(',').Count(); i++)
                                    {
                                        if (line.LastIndexOf(str_1000.Split(',')[i]) > -1)
                                        {
                                            line_steam += parstr(line, str_1000.Split(',')[i]) + ";";
                                        }
                                    }
                                }
                            }
                            file.Close();
                        }
                        else
                        {
                            while (bol_keeplineflag == false ? (line = file.ReadLine()) != null : true)
                            {
                                //line = line.TrimEnd();
                                if (str_filename.Substring(4, 2).ToUpper() == "CI")
                                {
                                    line_steam += line;
                                }
                                else if (str_filename.Substring(4, 1).ToUpper() == "S" && str_filename.Substring(14, 4).ToString().Trim() != "1000")
                                {
                                    //SS or SR
                                    string strCondition = null;
                                    string strtag = null;
                                    int ienable = 0;
                                    strCondition = CheckCondidtion(str_filename, line, dt_SwiftTag, ref strtag, ref ienable);
                                    //LogAdd("tag:" + strtag);

                                    //AUG-2021 AML enhancement HK requested combine all line in one
                                    if (!string.IsNullOrEmpty(OFACWS_COMBINELINE) && OFACWS_COMBINELINE == "Y")
                                    {
                                        //combine all line separate by space
                                        str_split = " ";
                                    }
                                    else
                                    {
                                        //scan each line
                                        str_split = ";";
                                    }

                                    if (strCondition != null)
                                    {
                                        imatchcount += 1;
                                        //use comma to spilt more than one condition
                                        foreach (string strcondition_spilt in strCondition.Split(','))
                                        {
                                            if (strcondition_spilt.Length == 0)
                                            {
                                                string newline = "";

                                                newline += line.Replace(strtag, "") + str_split;
                                                while ((line = file.ReadLine()) != null)
                                                {
                                                    newline += "|";

                                                    //line = line.TrimEnd();
                                                    
                                                    //FEB.08.2022 check tag's ":"  
                                                    //if (line.Contains(":") && line.Substring(line.IndexOf(":") + 1, line.Length - line.IndexOf(":") > 3 ? 3 : line.Length - line.IndexOf(":")).Contains(":"))
                                                    if (line.Substring(0,1).Contains(":") && line.Substring(1).IndexOf(":") < 5)
                                                    {
                                                        LogAdd("Check tag=" + line.Substring(0, line.Substring(1).IndexOf(":") + 1));
                                                        bol_keeplineflag = true;
                                                        break;
                                                    }
                                                    else if (line.Contains(ScanTag_end))
                                                    {
                                                        bol_keeplineflag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        bol_keeplineflag = false;
                                                        newline += line.Replace(strtag, "") + str_split;
                                                    }
                                                    
                                                }
                                                line_steam += newline + ";";
                                                LogAdd("newline=" + newline);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        bol_keeplineflag = false;
                                        //SEP.11 catch block 2
                                        /*
                                        if (ScanTag_start.Split('|').Length > 1 && ScanTag_end.Split('|').Length > 1 && line.Contains(ScanTag_start.Split('|')[0]) && line.Contains(ScanTag_end.Split('|')[0]))
                                        {
                                            string strblock2beg = ScanTag_start.Split('|')[0];
                                            string strblock2end = ScanTag_end.Split('|')[0];
                                            line_steam += line.Substring(line.IndexOf(strblock2beg) + 4, (line.IndexOf(strblock2end) - line.IndexOf(strblock2beg)) - 4) + ";";
                                        }
                                        */
                                    }
                                }
                                else if (str_filename.Substring(4, 1).ToUpper() == "F" && str_filename.Substring(14, 4).ToString().Trim() == "1000")
                                {
                                    //FS or FR
                                    for (int i = 0; i < str_1000.Split(',').Count(); i++)
                                    {
                                        if (line.LastIndexOf(str_1000.Split(',')[i]) > -1)
                                        {
                                            line_steam += parstr(line, str_1000.Split(',')[i]) + ";";
                                        }
                                    }
                                }
                            }
                        }


                        if (str_filename.Substring(4, 2).ToUpper() == "CI")
                        {
                            for (int i = 0; i < str_ciftp.Count(); i++)
                            {
                                if (str_filename.Substring(14, 4).ToUpper().Trim() == str_ciftp[i])
                                {
                                    arry_list = line_steam.Split('|');
                                    LogAdd(string.Format("sanction text col count : {0}", arry_list.Length));

                                    strDob = int.TryParse(str_dob_col, out iDOB) == true ? arry_list[int.Parse(str_dob_col)] : "";
                                    LogAdd(string.Format("DOB: {0}", fortifyService.PathManipulation(strDob)));

                                    string[] str_nation = str_nationtp[i].Split(',');

                                    foreach (string nationcol in str_nation)
                                    {
                                        if (int.Parse(nationcol) >= arry_list.Length)
                                        {
                                            break;
                                        }
                                        strNation += Checkstr(string.IsNullOrEmpty(arry_list[int.Parse(nationcol)]) == false ? arry_list[int.Parse(nationcol)] : "", "nation") + ';';
                                    }

                                    string[] str_engname = str_engnametp[i].Split(',');

                                    foreach (string engnamecol in str_engname)
                                    {
                                        if (int.Parse(engnamecol) >= arry_list.Length)
                                        {
                                            break;
                                        }
                                        streName += Checkstr(string.IsNullOrEmpty(arry_list[int.Parse(engnamecol)]) == false ? arry_list[int.Parse(engnamecol)].Replace('*', ' ') : "", "engname") + ';';
                                    }
                                }
                            }

                            line_steam = streName.TrimEnd(';');
                        }
                        else
                        {
                            line_steam = line_steam.TrimEnd(';');
                            LogAdd(string.Format("EnglishName:{0}", fortifyService.PathManipulation(line_steam)));
                        }

                        ArrayList wcresult = new ArrayList();

                        strRef = str_filename.Substring(0, 25) + DateTime.Now.ToString("HHmmss");

                        //20180912 call WCAPI
                        LogAdd("OFACWS=" + OFACWS_Enable);

                        if(strWCO == "Y")
                        {
                            //Dec.7, 2023 make wc1 api to a new function
                            CallWC1WS(strcif_file, str_filename, line_steam, strBankNo, ref bolmatch, ref bol_r);
                        }

                        if (OFACWS_Enable == "Y")
                        {
                            //20180912 call OFACWS
                            if (line_steam.TrimEnd(';') != string.Empty)
                                bol_r = Callofacwebservice(strBankNo, line_steam.TrimEnd(';'), strNation.TrimEnd(';'), iScore, strRef, strcif_file, ref bolmatch, str_filename);
                            if (imatchcount == 0 || line_steam.TrimEnd().Trim(';') == "")
                            {
                                LogAdd("Enable WS, but no match scan tag, change to normal scan.");
                                bol_r = false;
                            }
                        }

                        quit = true;

                    }
                    catch (Exception ex)
                    {
                        Logerr(ex.ToString());

                        if (ex.ToString().IndexOf("timed out") > 0)
                        {
                            quit = false;
                            System.Threading.Thread.Sleep(intdelay);
                        }
                        else
                        {
                            //client.Close();
                            quit = true;
                            //throw ex;20201217 mark for weak.
                        }
                    }
                }
                file.Close();
            }

            return bol_r;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strfilepath"></param>
        /// <param name="strfilename"></param>
        /// <param name="strBankNo"></param>
        /// <param name="intretry"></param>
        /// <param name="intdelay"></param>
        /// <param name="bolmatch"></param>
        /// <param name="strWCO"></param>
        /// <returns></returns>
        public bool checkPEPS(string strfilepath, string strfilename, string strBankNo, int intretry, int intdelay, ref bool bolmatch, string strWCO)
        {
            //May.24,2023 add new function for swift MX format.
            bool bol_return = false;
            bool quit = false;
            int intloop = 0;
            string str_input_name = "";
            string str_input_ctry = "";
            int imatchcount = 0;
            dt_SwiftTag = GetSwiftTag();

            while (!quit && intloop < intretry)
            {
                try
                {
                    intloop += 1;

                    using (StreamReader readStream = new StreamReader(strfilepath, Encoding.Default))
                    {
                        //load xml
                        string str_lines_header = "";
                        string str_lines_doc = "";
                        string line = "";
                        bool bol_check_part1 = false;
                        bool bol_check_part2 = false;

                        while (!readStream.EndOfStream)
                        {
                            //seprate hdr and doc
                            line = readStream.ReadLine();
                            if (line.Contains("<AppHdr"))
                            {
                                bol_check_part1 = true;
                            }
                            else if (line.Contains("<Document"))
                            {
                                bol_check_part1 = false;
                                bol_check_part2 = true;
                            }
                            else if(line.Contains("</Document>"))
                            {
                                str_lines_doc += line.Substring(0, line.LastIndexOf("</Document>") + 11);
                                break;
                            }

                            if (bol_check_part1)
                            {
                                str_lines_header += line;
                            }

                            if (bol_check_part2)
                            {
                                str_lines_doc += line.Substring(0, line.LastIndexOf("</Document>") + 11);
                            }
                        }
                        readStream.Close();

                        string strContent = str_lines_doc;
                        //LogAdd("[checkPEPS] - check loadXML:" + strContent);
                        XmlDocument xdoc = new XmlDocument();
                        xdoc.LoadXml(strContent);

                        string strCondition = null;
                        string strtag = null;
                        int ienable = 0;
                        strCondition = CheckCondidtion(strfilename, "", dt_SwiftTag, ref strtag, ref ienable);

                        if (!string.IsNullOrEmpty(strtag))
                        {
                            foreach (string strxmltag in strtag.Split(','))
                            {
                                imatchcount += 1;
                                XmlNodeList nodeList = xdoc.GetElementsByTagName(strxmltag);

                                foreach (XmlNode nodeRes in nodeList)
                                {
                                    str_input_name += nodeRes.InnerText + ";";
                                }
                            }
                        }

                        str_input_name = str_input_name.TrimEnd(';');
                    }

                    LogAdd(string.Format("[MX] EnglishName={0}", str_input_name));

                    if (strWCO == "Y")
                    {
                        //Dec.7, 2023 make wc1 api to a new function
                        CallWC1WS(strfilepath, strfilename, str_input_name, strBankNo, ref bolmatch, ref bol_return);
                    }
                    else if (OFACWS_Enable == "Y")
                    {
                        if (str_input_name != string.Empty)
                            bol_return = Callofacwebservice(strBankNo, str_input_name, str_input_ctry.TrimEnd(';'), 80, str_input_name, strfilepath, ref bolmatch, strfilename);
                    }
                    //20180912 call OFACWS
                    if (imatchcount == 0 || str_input_name.TrimEnd().Trim(';') == "")
                    {
                        LogAdd("Enable WS, but no match scan tag, change to normal scan.");
                        bol_return = false;
                    }

                    quit = true;
                }
                catch (Exception e)
                {
                    Logerr(e.ToString());

                    if (e.ToString().IndexOf("timed out") > 0)
                    {
                        quit = false;
                        System.Threading.Thread.Sleep(intdelay);
                    }
                    else
                    {
                        quit = true;
                    }
                }
            }

            return bol_return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="caseSystemId"></param>
        /// <param name="str_Mode"></param>
        /// <param name="strsanctiondata"></param>
        /// <param name="refstr"></param>
        /// <returns></returns>
        private string post_scansanctiondata(string caseSystemId, string str_Mode, string strsanctiondata, string refstr)
        {
            Fortify fortifyService = new Fortify();
            DateTime dateValue = DateTime.UtcNow; // get the datetime NOW GMT

            string date = dateValue.ToString("R"); // WC1 header requires GMT datetime stamp

            string requestendpoint = apiurl;
            if (str_Mode == "S")
            {
                requestendpoint = requestendpoint + "/" + caseSystemId + "/screeningRequest";
            }

            string strref = refstr;

            customFields = "[{\"typeId\":\""+ typeid + "\",\"value\":\"" + strref + "\"}]";

            //customFields = "[]";

            string postData = "{\"secondaryFields\":" + secondaryFields + ",\"entityType\":\"" + entityType + "\",\"customFields\":" + customFields + ",\"groupId\":\"" + groupId + "\",\"providerTypes\":" + providerTypes + ",\"name\":\"" + strsanctiondata + "\"}";

            string msg = postData;

            UTF8Encoding encoding = new UTF8Encoding();
            byte[] byte1 = encoding.GetBytes(fortifyService.PathManipulation(postData));

            // Assemble the POST request - NOTE every character including spaces have to be EXACT 
            // for the API server to decode the authorization signature       
            string strUrl = requestendpoint.Replace("https://" + gatewayhost, "");

            string dataToSign = "(request-target): post " + strUrl + "\n" +
                "host: " + gatewayhost + "\n" +   // no https only the host name
                "date: " + date + "\n" +          // GMT date as a string
                "content-type: " + "application/json" + "\n" +
                "content-length: " + byte1.Length + "\n" +
                 msg;

            LogAdd("---api secret---");
            LogAdd(fortifyService.PathManipulation(apisecret));
            LogAdd("---dataToSign---");
            LogAdd(fortifyService.PathManipulation(dataToSign));
            LogAdd("string hmac = generateAuthHeader(dataToSign, apisecret);");
            // The Request and API secret are now combined and encrypted
            string hmac = generateAuthHeader(dataToSign, apisecret);

            // Assemble the authorization string - This needs to match the dataToSign elements 
            // i.e. requires  host date content-type content-length
            //- NOTE every character including spaces have to be EXACT else decryption will fail with 401 Unauthorized
            string authorisation = "Signature keyId=\"" + apikey + "\",algorithm=\"hmac-sha256\",headers=\"(request-target) host date content-type content-length\",signature=\"" + hmac + "\"";

            LogAdd("---Hmac---");
            LogAdd(hmac);

            // Send the Request to the API server
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(requestendpoint);

            // Set the Headers
            WebReq.Method = "POST";
            WebReq.Headers.Add("Authorization", fortifyService.PathManipulation(authorisation));
            WebReq.Headers.Add("Cache-Control", "no-cache");
            WebReq.ContentLength = msg.Length;

            // Set the content type of the data being posted.
            Type type = WebReq.Headers.GetType();
            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            MethodInfo methodInfo = type.GetMethod("AddWithoutValidate", flags);
            object[] myPara = new object[2];
            myPara[0] = "Date";
            myPara[1] = dateValue.ToString("R");
            methodInfo.Invoke(WebReq.Headers, myPara);

            WebReq.ContentType = "application/json";
            WebReq.ContentLength = byte1.Length;

            //proxy
            if (strProxy!= String.Empty)
            {
                string[] strByPass = new string[] { @"192.\.168\..*", @"172\.17\..*" };
                WebReq.Proxy = new WebProxy(new Uri(strProxy), true, strByPass);
            }

            //SSL TLS1.2
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate 
            { 
                return CheckCert(); 
            };
            
            //for SSL
            if (OFACWS_SSL == "Y")
            {
                System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;
            }

            Stream newStream = WebReq.GetRequestStream();
            newStream.Write(byte1, 0, byte1.Length);

            bool bol_success = false;

            //2018.03 add api retry function
            for (int i = 0; i < apiretry; i++)
            {
                if (bol_success == true)
                {
                    break;
                }
                else
                {
                    System.Threading.Thread.Sleep(1000 * retrySec);
                    LogAdd("Try times:" + i);
                }
                try
                {
                    // Get the Response - Status OK
                    HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                    // Status information about the request

                    // Get the Response data
                    Stream Answer = WebResp.GetResponseStream();
                    StreamReader _Answer = new StreamReader(Answer);
                    string jsontxt = _Answer.ReadToEnd();

                    LogAdd("return value:" + jsontxt);

                    // convert json text to a pretty printout
                    var obj = JsonConvert.DeserializeObject(fortifyService.PathManipulation(jsontxt));
                    var f = JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented);

                    if (str_Mode == "Q")
                    {
                        wcapi wcapi = JsonConvert.DeserializeObject<wcapi>(fortifyService.PathManipulation(f));
                        caseSystemId = wcapi.caseSystemId;
                        LogAdd("caseSysID:" + fortifyService.PathManipulation(caseSystemId));
                    }
                    if (str_Mode == "S")
                    {
                        caseSystemId = f;
                    }
                    bol_success = true;
                }
                catch (WebException ex)
                {
                    Logerr(ex.ToString());
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        Logerr("WebException code:" + ((int)response.StatusCode).ToString());
                    }
                }
            }
            return caseSystemId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="caseSystemId"></param>
        /// <param name="StrEntID"></param>
        /// <returns></returns>
        private ArrayList wc_getresult(string caseSystemId, string StrEntID)
        {
            Fortify fortifyService = new Fortify();
            DateTime dateValue = DateTime.UtcNow; // get the datetime NOW GMT

            string date = dateValue.ToString("R"); // WC1 header requires GMT datetime stamp

            //set host and credentials to the WC1 API Pilot server WC1SampleClientAPI account

            string requestendpoint = apiurl + "/" + caseSystemId + "/results";
            
            if (StrEntID != "")
            {
                requestendpoint = apiurl.Replace("cases", "reference/profile/") + StrEntID;
            }

            // Assemble the GET request - NOTE every character including spaces have to be EXACT 
            // for the API server to decode the authorization signature       
            string strUrl = requestendpoint.Replace("https://" + gatewayhost, "");
            if (strUrl.IndexOf('?') > 0)
                strUrl = strUrl.Replace(strUrl.Substring(strUrl.IndexOf('?')), "");
            string dataToSign = "(request-target): get " + strUrl + "\n" +
                "host: " + gatewayhost + "\n" +   // no https only the host name
                "date: " + date;                  // GMT date as a string
            // The Request and API secret are now combined and encrypted
            string hmac = generateAuthHeader(dataToSign, apisecret);

            // Assemble the authorization string - This needs to match the dataToSign elements 
            // i.e. requires ONLY host date (no content body for a GET request)
            //- NOTE every character including spaces have to be EXACT else decryption will fail with 401 Unauthorized
            string authorisation = "Signature keyId=\"" + apikey + "\",algorithm=\"hmac-sha256\",headers=\"(request-target) host date\",signature=\"" + hmac + "\"";

            // Send the Request to the API server
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(requestendpoint);
            // Set the Headers
            WebReq.Method = "GET";
            WebReq.Headers.Add("Authorization", fortifyService.PathManipulation(authorisation));
            WebReq.Headers.Add("Cache-Control", "no-cache");
            Type type = WebReq.Headers.GetType();
            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            MethodInfo methodInfo = type.GetMethod("AddWithoutValidate", flags);
            object[] myPara = new object[2];
            myPara[0] = "Date";
            myPara[1] = dateValue.ToString("R");
            methodInfo.Invoke(WebReq.Headers, myPara);
            LogAdd("---Get Ready---");

            //proxy
            if (strProxy != String.Empty)
            {
                string[] strByPass = new string[] { @"192.\.168\..*", @"172\.17\..*" };
                WebReq.Proxy = new WebProxy(new Uri(strProxy), true, strByPass);
            }

            //SSL TLS1.2
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return CheckCert();
            };

            //for SSL
            if (OFACWS_SSL == "Y")
            {
                System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;
            }

            try
            {
                // Get the Response - Status OK
                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                // Status information about the request

                // Get the Response data
                Stream Answer = WebResp.GetResponseStream();
                StreamReader _Answer = new StreamReader(Answer);
                string jsontxt = _Answer.ReadToEnd();

                // convert json text to a pretty printout
                var obj = JsonConvert.DeserializeObject(fortifyService.PathManipulation(jsontxt));
                var f = JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented);

                ArrayList result = new ArrayList();

                if (StrEntID == "")
                {
                    result = JsonConvert.DeserializeObject<ArrayList>(fortifyService.PathManipulation(f));
                }
                else
                {
                    Profile prof = JsonConvert.DeserializeObject<Profile>(f);
                    
                    for (int i = 0; i < prof.names.Count(); i++)
                    {
                        result.Add(prof.names[i]);
                    }
                }

                LogAdd("---Get Finish---");
                return result;
            }
            catch (WebException ex)
            {
                Logerr(ex.ToString());
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    Logerr("WebException code:" + ((int)response.StatusCode).ToString());
                }
                return null;
            }
        }

        public class wcapi
        {
            public string caseId { get; set; }
            public string name { get; set; }

            public string[] providerTypes { get; set; }
            public custfields[] customFields { get; set; }
            public string[] secondaryFields { get; set; }
            public string groupId { get; set; }
            public string entityType { get; set; }
            public string caseSystemId { get; set; }
            public string caseScreeningState { get; set; }
            public string lifecycleState { get; set; }
            public creator creator { get; set; }
            public modifier modifier { get; set; }
            public string creationDate { get; set; }
            public string modificationDate { get; set; }
            public string outstandingActions { get; set; }
        }

        public class custfields
        {
            public string typeId { get; set; }
            public string value { get; set; }
            public string dateTimeValue { get; set; }
        }

        public class creator
        {
            public string userId { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string email { get; set; }
            public string status { get; set; }
        }

        public class modifier
        {
            public string userId { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string email { get; set; }
            public string status { get; set; }

        }

        public class ResultDetail
        {
            public string resultId { get; set; }
            public string referenceId { get; set; }
            public string matchStrength { get; set; }
            public string matchedTerm { get; set; }
            public string submittedTerm { get; set; }
            public string matchedNameType { get; set; }
            public string[] secondaryFieldResults { get; set; }
            public string[] sources { get; set; }
            public string[] categories { get; set; }
            public string creationDate { get; set; }
            public string modificationDate { get; set; }
            public string resolution { get; set; }
            public ResultReview resultReview { get; set; }
        }

        public class ResultReview
        {
            public string reviewRequired { get; set; }
            public string reviewRequiredDate { get; set; }
            public string reviewRemark { get; set; }
            public string reviewDate { get; set; }
        }

        public class Profile
        {
            public string entityId { get; set; }
            public Names[] names { get; set; }
        }

        public class Names
        {
            public string fullName { get; set; }
            public string givenName { get; set; }
            public LanguageCode languageCode { get; set; }
            public string lastName { get; set; }
            public string originalScript { get; set; }
            public string prefix { get; set; }
            public string suffix { get; set; }
            public string type { get; set; }
        }

        public class LanguageCode
        {
            public string code { get; set; }
            public string name { get; set; }
        }

        private static string generateAuthHeader(string dataToSign, string apisecret)
        {
            byte[] secretKey = Encoding.UTF8.GetBytes(apisecret);
            HMACSHA256 hmac = new HMACSHA256(secretKey);
            hmac.Initialize();

            byte[] bytes = Encoding.UTF8.GetBytes(dataToSign);
            byte[] rawHmac = hmac.ComputeHash(bytes);
            hmac.Clear();
            return (Convert.ToBase64String(rawHmac));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strBankNo"></param>
        /// <param name="scanname"></param>
        /// <param name="strcountry"></param>
        /// <param name="iScore"></param>
        /// <param name="strRef"></param>
        /// <param name="strcif_file"></param>
        /// <param name="bolmatch"></param>
        /// <param name="strfilename"></param>
        /// <returns></returns>
        private bool Callofacwebservice(string strBankNo, string scanname, string strcountry, int iScore, string strRef, string strcif_file, ref bool bolmatch, string strfilename)
        {
            OFACWebService OFACWS = new OFACWebService();
            OFACWebService.Response reponse = new OFACWebService.Response();
            OFACWebService.Matches matches = new OFACWebService.Matches();
            Fortify fortifyService = new Fortify();
            int r = 5;
            bool bol_r = false;
            bool bol_WSmatch = false;
            //if (strcountry == "")
            //    strcountry = "United States";

            LogAdd("bankno=" + strBankNo + ", scan_name=" + scanname + ",country =" + strcountry);

            int imaincase = 0;
            int i_matchcount = 0;
            int i_seqNumb = 0;
            int i_maxmatch = OFACWS_MAXMATCH;
            string strUserMsg = "";
            string str_currency = "";
            bool bolr = false;
            //Jan.09,2024 avoid duplicate name screening
            string str_keep = "";
            try
            {
                //AUG.17,2023 add muti file image
                foreach(string str_fileimg in OFACWS_FILEIMG.Split(','))
                {
                    LogAdd("OFAC_WS:Current file image:" + fortifyService.PathManipulation(str_fileimg));
                    foreach (string strname in scanname.Split(';'))
                    {
                        if (str_keep.Split('|').Any(sp => sp.ToUpper() == strname.ToUpper()))
                        {
                            LogAdd("[WC1] duplicate name = " + strname);
                            continue;
                        }
                        else
                        {
                            str_keep += strname + "|";
                        }

                        bolr = false;
                        string strname_chked = CheckCharacter(strname);
                        strname_chked = strname_chked.TrimEnd('|');
                        LogAdd("scan_name split=" + strname_chked);

                        for (int i = 0; i < r; i++)
                        {
                            if (bolr == false)
                            {
                                LogAdd("try ofac webservice:" + (i + 1));
                                string strMatch = "";
                                reponse = OFACWS.CallOFACWS(strBankNo, strname_chked, strcountry, iScore, OFACWS_DB, OFACWS_SSL, str_fileimg, OFACWS_USR, OFACWS_PW, ref strMatch);

                                if (reponse != null && reponse.GETSEARCHRESULTS.OUTPUT.MATCHES.MATCH.Count() > 0 && OFACWS_DB == "N")
                                {
                                    i_maxmatch = OFACWS_MAXMATCH;

                                    matches = reponse.GETSEARCHRESULTS.OUTPUT.MATCHES;


                                    LogAdd("text " + fortifyService.PathManipulation(strname_chked) + ",\r\n hit count:" + matches.MATCH.Count() + ",MaxMatch:" + i_maxmatch);

                                    //iallmatchcount = iallmatchcount + wcresult.Count;
                                    //create case
                                    LogAdd("check is first case:" + imaincase);
                                    if (imaincase == 0)
                                    {
                                        LogAdd("---case create start---");
                                        str_currency = GetCurrency(strfilename + "-1W.pri", strcif_file, OFACWS_Currency_Tag);
                                        InsertFilterTran_OFACWS(strfilename + "-1W.pri", strBankNo, matches.MATCH.Count().ToString(), str_currency, out i_seqNumb, str_fileimg);
                                        LogAdd("---case create end---");
                                    }

                                    //Jan.25.2022 add new control max match count limitation

                                    if (i_maxmatch > matches.MATCH.Count())
                                        i_maxmatch = matches.MATCH.Count();

                                    //Dec.11, 2023 add to get blocklisttype before loop
                                    string str_blockListtype = GetBlockListType();
                                    for (int j = 0; j < i_maxmatch; j++)
                                    {
                                        string strprogram = matches.MATCH[j].MATCHPROGRAM;
                                        string strentnum = "";

                                        //check sdn data
                                        strentnum = CheckSdn(i_seqNumb.ToString(), matches.MATCH[j].ORIGINALSDNNAME, matches.MATCH[j].MATCHNAME, strBankNo, matches.MATCH[j].MATCHPROGRAM, str_blockListtype);

                                        if (strentnum != "")
                                        {
                                            //count match number no duplicate
                                            i_matchcount = i_matchcount + 1;

                                            bol_WSmatch = true;

                                            //insert MatchTable
                                            InsertMatchTable(strfilename + "-1W.pri", strentnum, matches.MATCH[j].MATCHNAME.Trim(), matches.MATCH[j].ORIGINALSDNNAME.Trim(), "100", strname_chked.Trim(), i_seqNumb);
                                        }
                                        else
                                        {
                                            if (matches.MATCH.Count() > i_maxmatch)
                                            {
                                                //Feb.8, 2023 fix duplicate macth count release
                                                LogAdd("CheckSdn: No match or Duplicate for the Original Sdn Name:" + fortifyService.PathManipulation(matches.MATCH[j].ORIGINALSDNNAME));
                                                i_maxmatch++;
                                            }

                                        }
                                    }

                                    // insert MsgTable
                                    if (imaincase == 0)
                                        InsertMsgTable(strfilename + "-1W.pri", strcif_file, ref strUserMsg, i_seqNumb);

                                    imaincase += 1;
                                    bolr = true;
                                }
                                else if (strMatch == "N")
                                {
                                    bolr = true;
                                }
                            }
                        }

                    }
                }
                
                //Oct.21, 2023 add to check OFAC WS retrun match but duplicate case match count = 0.
                if(i_maxmatch > 0 && !bol_WSmatch && i_maxmatch != OFACWS_MAXMATCH)
                {
                    bolr = false;
                }

                UpdateMatchCount(i_matchcount, i_seqNumb, strUserMsg);

                InsertRecon("WS_SWIFT", "1", i_matchcount.ToString(), strBankNo, "AML", String.Format(("{0}-1W.pri"), strfilename), strUserMsg, "SYSTEM");

                bolmatch = bol_WSmatch;
                if (!bol_WSmatch)
                {
                    LogAdd("Total name, ofac webservice no match!");
                }

                //if get WS result success  
                if (bolr)
                {
                    bol_r = true;
                }

                return bol_r;
            }
            catch (Exception e)
            {
                Logerr("OFAC webservice error:" + e.ToString());
                return bol_r;
            }
        }


        private string Checkstr(string str_check,string str_flag)
        {
            string str_return = "";

            bool bolR = true;

            if (System.Text.Encoding.Default.GetBytes(str_check).Length > 1)
            {
                for (int i = 0; i < str_check.Length; i++)
                {
                    int AsciiCodeO = (int)System.Convert.ToChar(str_check[i]);

                    if (AsciiCodeO < 32 || AsciiCodeO > 126)
                    {
                        bolR = false;
                    }
                }
            }
            else
            {
                bolR = false;
            }

            if (bolR)
            {
                if (str_flag == "nation")
                {
                    str_return = str_check.Substring(0, 2);
                }
                else
                {
                    str_return = str_check.Replace('*',' ');
                }
            }
            else
            {
                str_return = "";
            }

            return str_return.Trim();
        }

        private string parstr(string str_line, string str_code)
        {
            string str_return = "";
            int istart = 0;
            int ifirst = 0;
            int ilast = 0;

            if (str_code == ":50" || str_code == ":59")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == ":")
                    {
                        str_return = str_line.Substring(i + 1);
                        break;
                    }
                }
            }
            else if (str_code == ":59F" || str_code == ":50F")
            {
                istart = 0;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "/")
                    {
                        str_return = str_line.Substring(i + 1);
                        break;
                    }
                }
            }
            else if (str_code == "{3100}" || str_code == "{3400}")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length + 9;

                
                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "*")
                    {
                        str_return = str_line.Substring(istart, i - istart);
                        break;
                    }
                    else if (str_line[i].ToString() == "{" || str_line[i + 1].ToString() == "{")
                    {
                        //第一個*後面有可能接下一個tag,那就跳過不抓
                        str_return = "";
                        break;
                    }

                    if (i- istart > 18)//maxlength 18
                    {
                        str_return = str_line.Substring(istart, 18);
                        break;
                    }
                }

            }
            else if (str_code == "{4000}" || str_code == "{4100}" || str_code == "{4200}" || str_code == "{4400}" || str_code == "{5000}" || str_code == "{5100}" || str_code == "{5200}")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "*")
                    {
                        if (ifirst == 0)
                        {
                            ifirst = i;
                        }
                        else
                        {
                            ilast = i;
                            str_return = str_line.Substring(ifirst + 1, ilast - ifirst - 1);
                            break;
                        }
                    }

                    if(ifirst != 0 && str_line[ifirst + 1].ToString() == "{")
                    {
                        //第一個*後面有可能接下一個tag,那就跳過不抓
                        str_return = "";
                        break;
                    }

                    if (ifirst != 0 && i - ifirst > 35)//maxlength 35
                    {
                        str_return = str_line.Substring(ifirst + 1, 35);
                        break;
                    }
                }
            }
            else if (str_code == "{5010}")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "*")
                    {
                        if (ifirst == 0 && str_line[i+1].ToString() == "1" && str_line[i+2].ToString()=="/")
                        {
                            ifirst = i;
                        }
                        else if (ifirst != 0)
                        {
                            ilast = i;
                            str_return = str_line.Substring(ifirst + 3, ilast - ifirst - 3);
                            break;
                        }
                    }

                    if (ifirst != 0 && str_line[ifirst + 1].ToString() == "{")
                    {
                        //第一個*後面有可能接下一個tag,那就跳過不抓
                        str_return = "";
                        break;
                    }

                    if (ifirst != 0 && i - ifirst > 35)//maxlength 35
                    {
                        str_return = str_line.Substring(ifirst + 1, 35);
                        break;
                    }
                }
            }
            else if (str_code == "{7050}" || str_code == "{7059}")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "*")
                    {
                        if (ifirst == 0)
                        {
                            ifirst = 1;
                        }
                        else if (ifirst == 1)
                        {
                            ifirst = i;
                        }
                        else
                        {
                            ilast = i;
                            str_return = str_line.Substring(ifirst + 1, ilast - ifirst - 1);
                            break;
                        }
                    }

                    if (ifirst != 0 && str_line[ifirst + 1].ToString() == "{")
                    {
                        //第二個*後面有可能接下一個tag,那就跳過不抓
                        str_return = "";
                        break;
                    }

                    if (ifirst > 1 && i - ifirst > 35)//maxlength 35
                    {
                        str_return = str_line.Substring(ifirst + 1, 35);
                        break;
                    }
                }
            }

            return str_return.Trim();
        }

        public string CheckCondidtion(string strfilename, string strinput, DataTable dtswifttag, ref string strtag, ref int ienable)
        {
            //May.24, 2023 swift type add MX format
            string strResult = null;
            string strswifttype = "T"; //MT

            DataRow[] dr_select;
            try
            {
                if (strfilename.Substring(4, 1) == "S")
                {
                    //XXXXSR or XXXXSS
                    strswifttype = "T";
                }
                else if (strfilename.Substring(4, 1) == "X")
                {
                    //XXXXXR or XXXXXS
                    strswifttype = "X";
                }
                //LogAdd("MT='MT" + strfilename.Substring(14, 1).ToString().Trim() + "XX' ");
                //dr_select = dtswifttag.Select("MT='MT" + strfilename.Substring(14, 1).ToString().Trim() + "XX' ");
                dr_select = dtswifttag.Select("MT='M" + strswifttype + strfilename.Substring(14, 1).ToString().Trim() + "XX' ");
                ienable = dr_select.Length;

                if (ienable == 0 && strswifttype == "X")
                {
                    //do not find MX0XX or MX1XX and etc, so check MXXXX
                    dr_select = dtswifttag.Select("MT='M" + strswifttype + "XXX' ");
                    ienable = dr_select.Length;
                }
                ienable = dr_select.Length;
                for (int i = 0; i < dr_select.Length; i++)
                {
                    //LogAdd("input=" + strinput + ", dbtag" + dr_select[i]["tag"].ToString().Trim());
                    if (strinput.LastIndexOf(dr_select[i]["tag"].ToString().Trim()) > -1)
                    {
                        //strResult = dr_select[i]["Condition"].ToString().Trim();
                        //Oct.26.2021 Bot request to disable the Condition check
                        strResult = "";
                        strtag = dr_select[i]["tag"].ToString().Trim();
                    }
                    //for MX
                    if (strswifttype == "X")
                    {
                        strtag = strtag + dr_select[i]["tag"].ToString().Trim() + ",";
                    }
                }
                if (strswifttype == "X")
                {
                    strtag = strtag.Replace("<", "").Replace(">", "").TrimEnd(',');
                    LogAdd(string.Format("Chk Condition Tag:{0}", strtag));
                }
            }
            catch (Exception ex)
            {
                Logerr("Parse Tag ERROR:" + ex.ToString());
            }
            finally
            {

            }

            return strResult;
        }

        public bool Chk_WSFail(string sfilename)
        {
            bool bolR = false;
            try
            {
                Fortify fortifyService = new Fortify();
                AML_Data AML_Para = new AML_Data();
                string SQL = "";
                string sRef = sfilename + "-1W.pri";

                string str_notes = AML_Para.OFACWS_NOTES;
                string str_src = AML_Para.OFACWS_SRC;

                LogAdd("call WS fail, check case generate or not." + fortifyService.PathManipulation(sRef));

                //get ofac case in 15 mins.
                SQL = "select count(seqnumb) cnt from ofac..FilterTranTable(nolock) where TranTime >= getdate() - 0.003 and ref = @ref";

                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.CommandText = SQL;
                    cmd.Parameters.Add(new SqlParameter("@ref", fortifyService.PathManipulation(sRef)));

                    DataSet ds = database.ExecuteDataSet(cmd);

                    if(int.Parse(ds.Tables[0].Rows[0]["cnt"].ToString()) > 0 )
                    {
                        bolR = true;
                    }

                    cmd.Dispose();
                }

                LogAdd("OFAC_WS Case generate=" + bolR.ToString());

                //Dec.08.2021 add
                /* Dec.27.2021 BOT think no confirm error reason, should not waive automatically
                if (bolR)
                {
                    SQL = @"update ofac..FilterTranTable set ConfirmOper = 'PrimeAdmin', 
ConfirmState = 2, 
ConfirmTime = getdate(), 
App = 1, 
AppTime = getdate(), 
AppOper = 'PrimeAdmin', 
AnnotTxt = @notes, 
source = @source
 where TranTime >= getdate() - 0.003 and ref = @ref";

                    using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                    {
                        cmd.CommandText = SQL;
                        cmd.Parameters.Add(new SqlParameter("@notes", fortifyService.PathManipulation(str_notes + " " + sRef)));
                        cmd.Parameters.Add(new SqlParameter("@source", fortifyService.PathManipulation(str_src)));
                        cmd.Parameters.Add(new SqlParameter("@ref", fortifyService.PathManipulation(sRef)));
                        database.ExecuteNonQuery(cmd);

                        cmd.Dispose();
                    }
                    LogAdd("Waived and Approved duplicate WS case.");
                }
                */
            }
            catch (Exception ex)
            {
                Logerr("Get WS Fail case error:" + ex.ToString());
            }
            return bolR;
        }

        public bool Chk_OfacCase(string sfilename,ref string str_scanresult)
        {
            ///May.22, 2023 Check if ofac case exist or not
            bool bolR = false;

            str_scanresult = "N";
            try
            {
                Fortify fortifyService = new Fortify();
                AML_Data AML_Para = new AML_Data();
                string SQL = "";
                string sRef = sfilename;

                LogAdd("check ofac case generate or not." + fortifyService.PathManipulation(sRef));

                //Oct.19, 2023 
                SQL = @"select a.SeqNumb, a.Ref,
                case when isnull(a.ConfirmState,0) = 2 and a.app = 0 then 'P'
                when isnull(a.ConfirmState,0) = 2 and a.app = 1 then 'R' 
                when isnull(a.ConfirmState,0) = 1 and a.app = 0 then 'P'
                when isnull(a.ConfirmState,0) = 1 and a.app = 1 then 'B'
                when isnull(a.ConfirmState,0) = 0 then 'P'
                else 'N'
                end scanresult 
                from ofac..FilterTranTable(nolock) a where ref = @ref";

                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.CommandText = SQL;
                    cmd.Parameters.Add(new SqlParameter("@ref", fortifyService.PathManipulation(sRef)));

                    DataSet ds = database.ExecuteDataSet(cmd);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        bolR = true;
                        str_scanresult = ds.Tables[0].Rows[0]["scanresult"].ToString();
                    }

                    cmd.Dispose();
                }

                LogAdd("Ofac Case Get scan result = " + str_scanresult);
            }
            catch (Exception ex)
            {
                Logerr("Ofac Case error:" + ex.ToString());
            }
            return bolR;
        }

        public string GetBlockListType() 
        {
            string strBlocklisttype = "";
            try
            {
                
                string sqllisttype = "";
                sqllisttype = @"select BlockListTypeList from OFAC..SanctionDataRule
                              where code like @branch ";

                using (DbCommand cmd = database.GetSqlStringCommand(sqllisttype))
                {
                    cmd.CommandText = sqllisttype;

                    database.AddInParameter(cmd, "@branch", DbType.String, OFACWS_FILEIMG);

                    IDataReader reader = database.ExecuteReader(cmd);
                    try
                    {
                        while (reader.Read())
                        {
                            strBlocklisttype = reader[0].ToString();
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                LogAdd("BlockListType=" + strBlocklisttype);
            }
            catch(Exception e)
            {
                Logerr("GetBlockListType Error:" + e.ToString());
            }
            return strBlocklisttype;
        }

        public void CallWC1WS(string strcif_file, string str_filename, string line_steam, string strBankNo, ref bool bolmatch, ref bool bol_r)
        {
            try
            {
                int imaincase = 0;
                int iallmatchcount = 0;
                string strUserMsg = "";
                ArrayList wcresult = new ArrayList();
                Fortify fortifyService = new Fortify();

                //20180912 call WC1API

                int iseqnumb = 0;

                //Jan.09,2024 avoid duplicate name.
                string str_keep = "";

                foreach (string s in line_steam.Split(';'))
                {
                    if (str_keep.Split('|').Any(sp => sp.ToUpper() == s.ToUpper()))
                    {
                        LogAdd("[WC1] duplicate name = " + s);
                        continue;
                    }
                    else
                    {
                        str_keep += s + "|";
                    }

                    bol_r = false;
                    /* Wait
                    if (strDob != "" && strNation.TrimEnd(';') != "" && line_steam != "")
                    {

                    }
                    else
                    */
                    if (line_steam != "")
                    {
                        if (s == "")
                        {
                            //prepare for pass string || !string.IsNullOrEmpty(str_exclude) && str_exclude.Split('|').ToList().Any(exclude => s.ToUpper().Contains(exclude))
                            //LogAdd("pass to send wc1 :" + s + ".");
                            continue;
                        }

                        LogAdd("----Create Case Start----");
                        string casesystemid = post_scansanctiondata("", "Q", s, str_filename + "-1W.pri");
                        LogAdd("----Create Case End----");

                        System.Threading.Thread.Sleep(500);

                        LogAdd("----Screen Text Start----");
                        var result = post_scansanctiondata(casesystemid, "S", s, str_filename + "-1W.pri");
                        LogAdd("----Screen Text End----");

                        System.Threading.Thread.Sleep(500);

                        LogAdd("----Get Result Start----");
                        wcresult = wc_getresult(casesystemid, "");
                        LogAdd("----Get Result End----");

                        if (wcresult != null && wcresult.Count > 0)
                        {
                            LogAdd("text " + fortifyService.PathManipulation(s) + ", hit count:" + wcresult.Count);

                            iallmatchcount = iallmatchcount + wcresult.Count;
                            //create case
                            LogAdd("check is first case:" + imaincase);
                            if (imaincase == 0)
                            {
                                LogAdd("---case create start---");
                                InsertFilterTran(str_filename + "-1W.pri", strBankNo, wcresult.Count.ToString(), out iseqnumb);
                                LogAdd("---case create end---");
                            }

                            for (int i = 0; i < wcresult.Count; i++)
                            {
                                ResultDetail resultdetail = JsonConvert.DeserializeObject<ResultDetail>(fortifyService.PathManipulation(wcresult[i].ToString()));

                                string strentnum = resultdetail.referenceId.Substring(resultdetail.referenceId.LastIndexOf('_') + 1);
                                string strprogram = resultdetail.sources[0].Substring(resultdetail.sources[0].LastIndexOf('_') + 1);

                                if (Checkstr(resultdetail.matchedTerm, "engname") != "")
                                {
                                    //check sdn data
                                    CheckSdn(strentnum, resultdetail.matchedTerm, strBankNo, strprogram, resultdetail.referenceId, resultdetail.matchedNameType, resultdetail.sources, resultdetail.categories);

                                    //insert MatchTable
                                    InsertMatchTable(str_filename + "-1W.pri", strentnum, resultdetail.submittedTerm, resultdetail.matchedTerm, resultdetail.matchStrength, iseqnumb);
                                }
                                else
                                {
                                    //check language non english word
                                    //iallmatchcount = iallmatchcount - 1;
                                    ArrayList wcnames = wc_getresult(casesystemid, resultdetail.referenceId);
                                    for (int j = 0; j < wcnames.Count; j++)
                                    {
                                        string wcnametype = ((BlackListService.AML_Data.Names)new System.Collections.ArrayList(wcnames)[j]).type;

                                        string wcfullname = ((BlackListService.AML_Data.Names)new System.Collections.ArrayList(wcnames)[j]).fullName;

                                        if (wcnametype.ToUpper() == "PRIMARY")
                                        {
                                            //check sdn data
                                            LogAdd("---check sdn non eng---");
                                            CheckSdn(strentnum, wcfullname, strBankNo, strprogram, resultdetail.referenceId, resultdetail.matchedNameType, resultdetail.sources, resultdetail.categories);

                                            //insert MatchTable
                                            LogAdd("---insert match non eng---");
                                            InsertMatchTable(str_filename + "-1W.pri", strentnum, resultdetail.submittedTerm, wcfullname, resultdetail.matchStrength, iseqnumb);
                                        }
                                    }
                                }
                            }

                            // insert MsgTable
                            if (imaincase == 0)
                                InsertMsgTable(str_filename + "-1W.pri", strcif_file, ref strUserMsg, iseqnumb);
                        }
                        else
                        {
                            imaincase = imaincase - 1;
                        }
                    }
                    imaincase = imaincase + 1;

                    if (iallmatchcount > 0)
                    {
                        bolmatch = true;
                    }

                    if (wcresult != null)
                    {
                        bol_r = true;
                    }
                    else
                    {
                        bol_r = false;
                    }
                }

                if (iallmatchcount > 0)
                {
                    //update the case total match count
                    Updatematchcount(iallmatchcount, str_filename + "-1W.pri", strUserMsg);

                    InsertRecon("Thomson Reuters World-Check One", "1", iallmatchcount.ToString(), strBankNo, "AML", String.Format(("{0}-1W.pri"), str_filename), strUserMsg, "SYSTEM");

                }
                // JUL-07-2021 BOT asked no need to insert recon table.    
                //InsertRecon("SWIFT", "1", iallmatchcount.ToString(), strBankNo, "AML", String.Format(("{0}-1W.pri"), str_filename), String.Format(("{0}-1W.pri"), str_filename), "SYSTEM", "SYSTEM");

            }
            catch (Exception e)
            {
                Logerr("CallWC1WS error:" + e.ToString());
            }
        }

        public string CheckCharacter(string strinput)
        {
            string stroutput = strinput;
            string strtmp = "";
            foreach (char strchar in strinput)
            {
                if (Convert.ToInt32(strchar) < 32 || Convert.ToInt32(strchar) > 126)
                {
                    strtmp += "";
                }
                else
                {
                    strtmp += strchar;
                }
            }

            if (strtmp != "")
                stroutput = strtmp.Trim();

            return stroutput;
        }

        private string InClauseSql(string name, List<string> param)
        {
            StringBuilder sql = new StringBuilder();

            for (int i = 0; i < param.Count(); i++)
                sql.Append(string.Format("@{ 0}{1},", name, i.ToString()));

            return sql.ToString().TrimEnd(',');
        }

        private int ExecuteSQL(DbCommand cmd)
        {
            using (DbConnection conn = database.CreateConnection())
            {
                conn.Open();
                DbTransaction trans = conn.BeginTransaction();

                try
                {
                    int rows = database.ExecuteNonQuery(cmd, trans);
                    trans.Commit();
                    conn.Close();
                    return rows;
                }
                catch
                {
                    trans.Rollback();
                    conn.Close();
                    throw;
                }
            }
        }

        public Boolean CheckCert()
        {
            Boolean bol_r = false;

            if("a" == "a")
            {
                bol_r = true;
            }

            return bol_r;
        }

        private void LogAdd(string logstring)
        {
            Fortify fortifyService = new Fortify();
            log.Info(fortifyService.PathManipulation(logstring));
        }

        private void Logerr(string logstring)
        {
            Fortify fortifyService = new Fortify();
            log.Error(fortifyService.PathManipulation(logstring));
        }

        private void Logerr(string logstring, Exception ex)
        {
            Fortify fortifyService = new Fortify();
            log.Error(fortifyService.PathManipulation(logstring), ex);
        }

        public void InsertOFACEvet(string sfilename, string sLogtext)
        {
            //Nov.1, 2023 add ofac event log insert
            database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
            string SQL = "exec OFAC..OFS_InsEvent @oper, @type, @objectType, @objectId, @logText";

            using (DbCommand dBCommand = database.GetSqlStringCommand(SQL))
            {
                database.AddInParameter(dBCommand, "@oper", DbType.String, "primeadmin");
                database.AddInParameter(dBCommand, "@type", DbType.String, "Ann");
                database.AddInParameter(dBCommand, "@objectType", DbType.String, "Event");
                database.AddInParameter(dBCommand, "@objectId", DbType.String, sfilename.Trim());
                database.AddInParameter(dBCommand, "@logText", DbType.String, sLogtext.Trim());

                try
                {
                    IDataReader datareader = database.ExecuteReader(dBCommand);

                    while (datareader.Read())
                    {
                        string strresult = datareader[0].ToString();
                        LogAdd(string.Format("[InsertOFACEvet] Result:{0}" + strresult));
                    }
                }

                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    dBCommand.Dispose();
                }

            }
        }
    }
}
