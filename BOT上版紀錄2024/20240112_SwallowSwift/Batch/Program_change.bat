REM backup
copy /y "D:\AML Interface\BlackListService.exe" "D:\Temp\Change_backup\AML Interface\20240306\old"
copy /y "D:\AML Interface\BlackListService.exe.config" "D:\Temp\Change_backup\AML Interface\20240306\old"
copy /y "D:\AML Interface\Setting.xml" "D:\Temp\Change_backup\AML Interface\20240306\old"


REM stop service
SC STOP "Unisys AML Interface" >> "D:\Temp\Change_backup\AML Interface\20240306\Program_change.log"
TIMEOUT /T 10

REM override new program files
copy /y "D:\Temp\Change_backup\AML Interface\20240306\new\BlackListService.exe" "D:\AML Interface\"
copy /y "D:\Temp\Change_backup\AML Interface\20240306\new\BlackListService.exe.config" "D:\AML Interface\"
copy /y "D:\Temp\Change_backup\AML Interface\20240306\new\Setting.xml" "D:\AML Interface\"

REM start service
SC start "Unisys AML Interface" >> "D:\Temp\Change_backup\AML Interface\20240306\Program_change.log"

