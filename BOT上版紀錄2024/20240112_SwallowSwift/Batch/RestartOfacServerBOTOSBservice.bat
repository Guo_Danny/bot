:Restart Service
echo %date% %time% Stop Service: 'OFAC Server BOTOSB' >>  "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"

echo %date% %time% TASKKILL: OFACSRV >>  "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"
TASKKILL /F /FI "IMAGENAME EQ ofacsrv*"   >>  "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"


echo %date% %time% TASKKILL: OFACDriver >>  "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"
TASKKILL /F /FI "IMAGENAME EQ ofacdriver*"   >>  "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"

sc start "OFAC Server BOTOSB"  >> "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"
timeout /t 10
echo %date% %time% Started Service: 'OFAC Server BOTOSB'  >> "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"

REM sc stop "OFAC Remoting BOTOSB" >> "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"
REMtimeout /t 30

echo %date% %time% TASKKILL: OfacRemoting* >>  "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"
TASKKILL /F /FI "IMAGENAME EQ OfacRemoting*"  >>  "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"
sc start "OfacRemotingService BOTOSB" >> "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"
echo %date% %time% Started Service: 'OfacRemotingService BOTOSB'   >> "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"


REM sc stop "SDN Database BOTOSB" >> "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"
REM timeout /t 30

echo %date% %time% TASKKILL: SDNDbSrv* >>  "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"
TASKKILL /F /FI "IMAGENAME EQ SDNDbSrv*"  >>  "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"
sc start "SDN Database BOTOSB" >> "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"

echo %date% %time% Started Service: 'SDN Database BOTOSB'   >> "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"

echo %date% %time% Stopped SwiftAmlAppPool >> "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"
C:\Windows\System32\inetsrv\appcmd.exe stop apppool "SwiftAmlAppPool"  >> D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log 
TIMEOUT /T 3

C:\Windows\System32\inetsrv\appcmd.exe start apppool "SwiftAmlAppPool"  >> D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log 

TIMEOUT /NOBREAK /T 3
echo %date% %time% Started SwiftAmlAppPool >> "D:\Prime\BOTOSB\BatchLog\ReStartOFACService.log"

:EndProc
