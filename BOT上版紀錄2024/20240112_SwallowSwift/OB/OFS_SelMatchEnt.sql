USE [OFAC]
GO

/****** Object:  StoredProcedure [dbo].[OFS_SelMatchEnt]    Script Date: 1/15/2024 11:26:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[OFS_SelMatchEnt] (
	@name varchar(50), --original name
	@blocklisttype varchar(50), --blocklisttype
	@program varchar(50),  --program
	@SeqNumb int, --seqnumb
	@MatchName varchar(50) --MatchName
)
  As
	declare @return_ent varchar(20)
	/*
	2023 add this SP for get Entnum by match name.
	*/

	select return_ent = entnum from OFAC..sdntable (nolock) 
	where rtrim(ltrim(name)) = @name and status != 4 
	and CHARINDEX(rtrim(ListType),@blocklisttype) = 0
	and program = (case when len(@program) > 0 then @program else Program end)
	and EntNum not in (
	select entNum from ofac.dbo.MatchTable where SeqNumb = (case when len(@SeqNumb) > 0 then @SeqNumb else SeqNumb end)
	and OriginalSDNName = @name and MatchName = @MatchName)

	if @return_ent is null begin
		select @return_ent = sdn.entnum from OFAC..sdnalttable (nolock) sdna
		join OFAC..sdntable (nolock) sdn on sdn.entnum = sdna.entnum 
		where rtrim(ltrim(sdna.altname)) = @name and sdn.status != 4 
		and CHARINDEX(rtrim(sdn.ListType),@blocklisttype) = 0
		and program = (case when len(@program) > 0 then @program else Program end)
		and sdn.EntNum not in (
		select entNum from ofac.dbo.MatchTable where SeqNumb = (case when len(@SeqNumb) > 0 then @SeqNumb else SeqNumb end)
		and OriginalSDNName = @name and MatchName = @MatchName)
	end

	if @return_ent is null begin
		set @return_ent = '0'
	end

	return @return_ent
GO


