Use OFAC
GO

--For NY and LA Disable MT swift messages
update SwiftTag set enable = 0

select * from SwiftTag where mt like 'MX%' order by 1 desc

insert into SwiftTag ( mt, tag, Enable, LastModify)
values('MXXXX', '<AnyBIC>',1, GETDATE())

insert into SwiftTag (mt, tag, Enable, LastModify)
values('MXXXX', '<BICFI>',1, GETDATE())

insert into SwiftTag (mt, tag, Enable, LastModify)
values('MXXXX', '<Nm>',1, GETDATE())

insert into SwiftTag (mt, tag, Enable, LastModify)
values('MXXXX', '<Ctry>',1, GETDATE())

insert into SwiftTag (mt, tag, Enable, LastModify)
values('MXXXX', '<Prtry>',1, GETDATE())

insert into SwiftTag (mt, tag, Enable, LastModify)
values('MXXXX', '<CtryOfBirth>',1, GETDATE())

insert into SwiftTag (mt, tag, Enable, LastModify)
values('MXXXX', '<Ustrd>',1, GETDATE())

insert into SwiftTag (mt, tag, Enable, LastModify)
values('MXXXX', '<InstrInf>',1, GETDATE())
