REM SET sDate = %date:~4,2%%date:~7,2%%date:~10,4%
SET sDate=%date:~10,4%%date:~4,2%%date:~7,2%
echo Wscript.echo dateadd("d",-1,date)>D:\Prime\BOTOSB\vbs.vbs
set sDate=
REM for /f %%a in ('cscript //nologo D:\Prime\BOTOSB\vbs.vbs') do del "D:\Prime\BOTOSB\vbs.vbs" && set sDate=%day%%%a
for /f %%a in ('cscript //nologo D:\Prime\BOTOSB\vbs.vbs') do del "D:\Prime\BOTOSB\vbs.vbs" && set sDate=%%a
echo %sDate%

SET MM=%sDate:~0,2%
set FLAG=2
REM 2/2/2024
if "%MM:~1,1%" =="/" SET MM=0%sDate:~0,1%&&SET FLAG=1

REM default 10/21/2024
SET DD=%sDate:~3,2%
REM 2/2/2024
if "%DD:~0,1%" =="/" SET DD=0%sDate:~2,1%

REM 2/21/2024
if "%DD:~1,1%" =="/" if "%FLAG%"=="1" SET DD=%sDate:~2,2%

REM 10/1/2024
if "%DD:~1,1%" =="/" if "%FLAG%"=="2" SET DD=0%sDate:~3,1%

set sDate=%date:~10,4%%MM%%DD%

ECHO %DATE% %sDate%

SET Bno=0138

REM GOTO InsOk2DB

REM -- duplicat result file --
d:
cd D:\AML Interface\Backup
CD D:\PRIME\TEMP\RESULT
if  %ERRORLEVEL%==0 GOTO PathExisted
D:
MD D:\PRIME\TEMP\RESULT
TIMEOUT /T 3

:PathExisted
REM --Del and CLEAR D:\PRIME\TEMP\RESULT\*.*
SET cmdStr=DEL /Q D:\PRIME\TEMP\RESULT\*.*
echo %cmdStr%
%cmdStr%
TIMEOUT /T 10

REM -- copy duplicate file to temp 
D:
CD D:\PRIME\TEMP\RESULT
echo 
SET cmdStr=COPY "D:\AML Interface\Backup\Result\%Bno%*%sDate%*.*"  "D:\PRIME\TEMP\RESULT\*.*"
echo %cmdStr%
%cmdStr%
TIMEOUT /T 10

REM -- FIND .PRB FILE
D:
CD\PRIME\TEMP\RESULT
del D:\PRIME\TEMP\dir-prb.OUT
DIR D:\PRIME\TEMP\RESULT\%Bno%*%sDate%*.prb /b /O:D >> "D:\PRIME\TEMP\dir-prb.OUT"
if %ERRORLEVEL% GTR 0 goto GetOkFromDB
TIMEOUT /T 2


:DelPrbFile
D:
CD\PRIME\TEMP\RESULT
FOR /F "delims=." %%G IN (D:\PRIME\TEMP\dir-prb.OUT) DO echo "%%G" | DEL "%%G*.*"
TIMEOUT /T 3


:GetOkFromDB
D:
CD\PRIME\TEMP\RESULT

DEL D:\PRIME\TEMP\dir-ok.OUT
DIR D:\PRIME\TEMP\RESULT\%Bno%*.ok /b /O:D >> D:\PRIME\TEMP\dir-ok.OUT

if %ERRORLEVEL% GTR 0 goto NoOKFile
TIMEOUT /T 2
CALL D:\PRIME\BOTOSB\ReconTableGetOkRec.bat


:DelOkFile
D:
CD\PRIME\TEMP\RESULT
FOR /F "skip=2 delims=." %%G IN (D:\PRIME\TEMP\zxrecon-ok.OUT) DO DEL "%%G*.*" 
TIMEOUT /T 5


:InsOk2DB
SET sDesc="PRIME ReconReport lost record, Do HotFix add .ok record into the OFAC Database."
EVENTCREATE /T ERROR /L SYSTEM /SO "Prime ReconReport" /ID 112 /D %sDesc%

FORFILES /P D:\PRIME\TEMP\RESULT /m %Bno%*%sDate%*.ok /C "cmd /c echo @file >D:\PRIME\TEMP\DIR-OK-INS.OUT | TIMEOUT /T 2 | CALL D:\PRIME\BOTOSB\ReconTableOkRecIns.bat" 

goto EndPro

:NoPrbFile
echo  ****  It is No *.prb file   *****
goto EndPro

:NoOKFile
echo  ****  It is No *.ok file   *****
goto EndPro

:EndPro
