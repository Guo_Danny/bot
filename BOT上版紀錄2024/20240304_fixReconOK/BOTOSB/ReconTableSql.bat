REM --- SELECT OFAC..FilterTranHistTable (CASE TABLE) NOT IN OFAC..ReconTable INTO OFAC..ReconTable ---
SQLCMD -S TOSBAMLDBV2 -E -i"D:\PRIME\BOTOSB\SupplementReconTableSql.sql" -o"D:\PRIME\BOTOSB\SupplementReconTable.TXT"
TIMEOUT /T 60

REM --- Check .Prb Record
FIND "Insert .PRB Total Records: 0" "D:\PRIME\BOTOSB\SupplementReconTable.TXT"
if %Errorlevel%==0 goto DoHotFixOk
SET sDesc="PRIME ReconReport lost record, Do HotFix add .prb record into the OFAC Database."
EVENTCREATE /T ERROR /L SYSTEM /SO "Prime ReconReport" /ID 112 /D %sDesc%

:DoHotFixOk
call "D:\PRIME\BOTOSB\ReconTableFixOk.bat"
TIMEOUT /T 30

REM Add delete duplicate Recontable records on 2024-03-05
SQLCMD -S TOSBAMLDBV2 -E -i"D:\PRIME\BOTOSB\SQL\DelDuplicateReconData.sql" -o"D:\PRIME\BOTOSB\BatchLog\DelDuplicateReconData.TXT"

set file="D:\PRIME\TEMP\dir-ok.OUT"
set maxbytesize=0
 
FOR /F "usebackq" %%A IN ('%file%') DO set size=%%~zA
 
if %size% GTR %maxbytesize% (
copy /Y \\TOSBAMLAPV2\Prime\BOTOSB\SupplementReconTable.TXT + \\TOSBAMLAPV2\Prime\BOTOSB\ZXRECON-INS-OK.OUT \\TOSBAMLDBV2\Prime\BOTOSB\Logs\OFAC\ReconFix.DAT
) ELSE (
copy /Y \\TOSBAMLAPV2\Prime\BOTOSB\SupplementReconTable.TXT \\TOSBAMLDBV2\Prime\BOTOSB\Logs\OFAC\ReconFix.DAT
)

REM --- TEST ---
SQLCMD -S TOSBAMLDBV2 -E -i"D:\Prime\BOTOSB\ReconTablesql.sql" -o"D:\PRIME\BOTOSB\ZXRECON.TXT"

call "D:\Prime\BOTOSB\Batch\ZXD0001.bat"

REM SQLCMD -S TOSBAMLDBV2 -U primeuser -P Passw0rd -i"D:\Prime\BOTOSB\ZXD0001.sql" -o"D:\PRIME\BOTOSB\ZXD0001.TXT" -h-1

REM --- NY:0083 ---
REM SQLCMD -S OSBAMLNYV -U primeuser -P Passw0rd "-i"ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"
REM --- LA:0094 ---
REM SQLCMD -S OSBAMLLAV -U primeuser -P Passw0rd "-i"ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"
REM --- HK:0114 ---
REM SQLCMD -S OSBAMLHKV -U primeuser -P Passw0rd "-i"ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"
REM --- SG:0138 ---
REM SQLCMD -S OSBAMLSGV -U primeuser -P Passw0rd "-i"ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"
REM --- TK:0117 ---
REM SQLCMD -S OSBAMLTKV -U primeuser -P Passw0rd "-i"ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"
REM --- ZA:0161 ---
REM SQLCMD -S OSBAMLZAV -U primeuser -P Passw0rd "-i"ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"
REM --- LN:0210 ---
REM SQLCMD -S OSBAMLLNV -U primeuser -P Passw0rd "-i"ReconTablesql.sql" -o"D:\ftproot\ReconReport\ZXRECON.TXT"

:CheckFileSize
REM forfiles /p "D:\PRIME\BOTOSB" /s /m zxrecon.txt /c "cmd /c if @fsize LSS 61444 DEL @file"
REM DIR "D:\PRIME\BOTOSB\ZxRecon.txt"
REM IF %ERRORLEVEL% NEQ 0 GOTO EndProc

TYPE D:\PRIME\BOTOSB\ZXRECON.TXT | FIND "Total Messages  =  0"
IF %ERRORLEVEL% == 0 GOTO EndProc


REM pause
set cmdcopy=copy /Y D:\PRIME\BOTOSB\ZXRECON.TXT D:\ftproot\ReconReport\ZXRECON.TXT
%cmdcopy% 
REM set cmdcopy=copy /Y D:\PRIME\BOTOSB\ZXD0001.TXT D:\ftproot\ReconReport\ZXD0001.TXT
REM %cmdcopy% 

REM --- BACKUP ZXRECON.TXT ---
REM SET sDate = %date:~4,2%%date:~7,2%%date:~10,4%
REM SET sDATE=%date:~0,2%%date:~3,2%
REM SET sDATE=%date:~10,4%%date:~4,2%%date:~7,2%
SET sDATE=%date:~7,2%
ECHO %SDATE%

REM set sbkfile=D:\ftproot\ReconReport\backup\ZXRECON.TXT.%%date:~4,2%%date:~7,2%%date:~10,4%
REM set cmdcopy =copy /Y D:\ftproot\ReconReport\ZXRECON.TXT D:\ftproot\ReconReport\backup\ZXRECON.TXT.%date:~4,2%%date:~7,2%%date:~10,4%
set cmdcopy=copy /Y D:\ftproot\ReconReport\ZXRECON.TXT D:\ftproot\ReconReport\backup\ZXRECON.TXT.%sdate%
%cmdcopy% 
REM set cmdcopy=copy /Y D:\ftproot\ReconReport\ZXD0001.TXT D:\ftproot\ReconReport\backup\ZXD0001.TXT.%sdate%
REM %cmdcopy% 

REM --- polly add on 2018-03-08 for Empty file goto end ---
:EndProc
REM --- polly add on 2018-03-08 for Empty file goto end ---

REM --run add same relationship party link
call "D:\Prime\BOTOSB\Batch\BatchSameRelationship.bat"

REM -- RUN HQ Sharing List
call "D:\Prime\BOTOSB\Batch\HQ\runHQ.bat"
