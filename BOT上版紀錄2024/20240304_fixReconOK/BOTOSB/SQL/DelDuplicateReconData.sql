use OFAC
go

delete r
from (
select R0.SancPartyCount, replace(replace(ltrim(rtrim(R0.CaseReference)),char(10),''),char(13),'') caseref, 
count(*) cnt
from ReconTable(nolock) R0 where R0.CaseReference is not null
and exists (select CaseReference from ReconTable(nolock) T0 
where T0.CaseReference = R0.CaseReference 
and T0.SearchOper = 'SYSTEM')
group by R0.SancPartyCount, replace(replace(ltrim(rtrim(R0.CaseReference)),char(10),''),char(13),'')
having count(*) > 1
) T1 
join ReconTable(nolock) r on replace(replace(ltrim(rtrim(r.CaseReference)),char(10),''),char(13),'') = T1.caseref
where r.SearchOper = 'prime'
