declare @brno varchar(50) 
set @brno = '0117'
begin tran

INSERT INTO [dbo].[Cifrules]([Code], [SDRDataCode], [Name], [Dept], [Branch], [FileName], [OutputFile], [RowDelimiter], [FieldDelimiter], [Delimited], [KeyField1], [KeyField2], [Delegated], [TextFileOpt], [DatabaseOpt], [NumberOfFields], [Source], [LogFileName], [CreateOper], [CreateDate], [LastOper], [LastModifyDate], [Remarks])
  VALUES('Loan', @brno + 'SWIFT', 'Self-Service Loan Batch Screening', 'AML', @brno , 'D:\FTPROOT\BOT' + @brno + '\Loan.TXT', 'D:\FTPROOT\BOT' + @brno + '\LoanResult_output.TXT', '13', '124', 1, 1, 2, 0, 1, 1, 2, 'CIF Filter', 'D:\FTPROOT\BOT' + @brno + '\Loan.log', 'primeadmin', Getdate(), NULL, NULL, '')


INSERT INTO [dbo].[CIFRulesAssociation]([CIFRule], [Branch], [Dept])
  VALUES('Loan', NULL, NULL)


INSERT INTO [dbo].[CIFFields]([CIFCode], [Algoritham], [Offset], [Length], [Misspelling], [FullMatchLine], [SkipDuplicateSDN], [UseFullExcludeList], [FieldNumber], [FieldType], [MustMatchTypeOpt], [MustMatchTypeValue], [CrossFieldMatchOpt], [CrossFieldMatchValue])
  VALUES('Loan', 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, '', 0, '')

INSERT INTO [dbo].[CIFFields]([CIFCode], [Algoritham], [Offset], [Length], [Misspelling], [FullMatchLine], [SkipDuplicateSDN], [UseFullExcludeList], [FieldNumber], [FieldType], [MustMatchTypeOpt], [MustMatchTypeValue], [CrossFieldMatchOpt], [CrossFieldMatchValue])
  VALUES('Loan', 2, 0, 0, 0, 1, 1, 0, 2, 2, 0, '', 0, '')

INSERT INTO [dbo].[CIFFields]([CIFCode], [Algoritham], [Offset], [Length], [Misspelling], [FullMatchLine], [SkipDuplicateSDN], [UseFullExcludeList], [FieldNumber], [FieldType], [MustMatchTypeOpt], [MustMatchTypeValue], [CrossFieldMatchOpt], [CrossFieldMatchValue])
  VALUES('Loan', 2, 0, 0, 0, 1, 1, 0, 3, 2, 0, '', 0, '')
  
INSERT INTO [dbo].[CIFFields]([CIFCode], [Algoritham], [Offset], [Length], [Misspelling], [FullMatchLine], [SkipDuplicateSDN], [UseFullExcludeList], [FieldNumber], [FieldType], [MustMatchTypeOpt], [MustMatchTypeValue], [CrossFieldMatchOpt], [CrossFieldMatchValue])
  VALUES('Loan', 2, 0, 0, 0, 1, 1, 0, 4, 2, 0, '', 0, '')

INSERT INTO [dbo].[CIFFields]([CIFCode], [Algoritham], [Offset], [Length], [Misspelling], [FullMatchLine], [SkipDuplicateSDN], [UseFullExcludeList], [FieldNumber], [FieldType], [MustMatchTypeOpt], [MustMatchTypeValue], [CrossFieldMatchOpt], [CrossFieldMatchValue])
  VALUES('Loan', 2, 0, 0, 0, 1, 1, 0, 5, 2, 0, '', 0, '')

INSERT INTO [dbo].[CIFFields]([CIFCode], [Algoritham], [Offset], [Length], [Misspelling], [FullMatchLine], [SkipDuplicateSDN], [UseFullExcludeList], [FieldNumber], [FieldType], [MustMatchTypeOpt], [MustMatchTypeValue], [CrossFieldMatchOpt], [CrossFieldMatchValue])
  VALUES('Loan', 2, 0, 0, 0, 1, 1, 0, 6, 2, 0, '', 0, '')

INSERT INTO [dbo].[CIFFields]([CIFCode], [Algoritham], [Offset], [Length], [Misspelling], [FullMatchLine], [SkipDuplicateSDN], [UseFullExcludeList], [FieldNumber], [FieldType], [MustMatchTypeOpt], [MustMatchTypeValue], [CrossFieldMatchOpt], [CrossFieldMatchValue])
  VALUES('Loan', 2, 0, 0, 0, 1, 1, 0, 7, 2, 0, '', 0, '')

INSERT INTO [dbo].[CIFFields]([CIFCode], [Algoritham], [Offset], [Length], [Misspelling], [FullMatchLine], [SkipDuplicateSDN], [UseFullExcludeList], [FieldNumber], [FieldType], [MustMatchTypeOpt], [MustMatchTypeValue], [CrossFieldMatchOpt], [CrossFieldMatchValue])
  VALUES('Loan', 2, 0, 0, 0, 1, 1, 0, 8, 2, 0, '', 0, '')

INSERT INTO [dbo].[CIFFields]([CIFCode], [Algoritham], [Offset], [Length], [Misspelling], [FullMatchLine], [SkipDuplicateSDN], [UseFullExcludeList], [FieldNumber], [FieldType], [MustMatchTypeOpt], [MustMatchTypeValue], [CrossFieldMatchOpt], [CrossFieldMatchValue])
  VALUES('Loan', 2, 0, 0, 0, 1, 1, 0, 9, 2, 0, '', 0, '')

INSERT INTO [dbo].[CIFFields]([CIFCode], [Algoritham], [Offset], [Length], [Misspelling], [FullMatchLine], [SkipDuplicateSDN], [UseFullExcludeList], [FieldNumber], [FieldType], [MustMatchTypeOpt], [MustMatchTypeValue], [CrossFieldMatchOpt], [CrossFieldMatchValue])
  VALUES('Loan', 2, 0, 0, 0, 1, 1, 0, 10, 2, 0, '', 0, '')

INSERT INTO [dbo].[CIFFields]([CIFCode], [Algoritham], [Offset], [Length], [Misspelling], [FullMatchLine], [SkipDuplicateSDN], [UseFullExcludeList], [FieldNumber], [FieldType], [MustMatchTypeOpt], [MustMatchTypeValue], [CrossFieldMatchOpt], [CrossFieldMatchValue])
  VALUES('Loan', 2, 0, 0, 0, 1, 1, 0, 11, 2, 0, '', 0, '')

INSERT INTO [dbo].[CIFFields]([CIFCode], [Algoritham], [Offset], [Length], [Misspelling], [FullMatchLine], [SkipDuplicateSDN], [UseFullExcludeList], [FieldNumber], [FieldType], [MustMatchTypeOpt], [MustMatchTypeValue], [CrossFieldMatchOpt], [CrossFieldMatchValue])
  VALUES('Loan', 2, 0, 0, 0, 1, 1, 0, 12, 2, 0, '', 0, '')

commit
