﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MFA_GA.aspx.cs" Inherits="MFAWeb.MFA_GA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>BOTOSB MFA Check</title>
    <link href="Styles.css" type="text/css" rel="stylesheet" />
</head>
<body style="background-color: #cccce5;">
    <form id="form1" runat="server">
        <div style="text-align:center;">
            <table style="width:100%;">
                <tr>
                    <td style="height:90px;width:42%;"></td>
                    <td style="width:58%;"></td>
                </tr>
                <tr>
                    <td style="text-align:right;">
                        <asp:Label runat="server" ID="lblUser" Text="User ID " CssClass="ltlstr"></asp:Label>
                    </td>
                    <td style="text-align:left;">
                        <asp:TextBox runat="server" ID="txtUserId"></asp:TextBox>
                        &nbsp;<asp:Button runat="server" ID="btn_checkuser" Text="Check User" OnClick="btn_checkuser_Click"/>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:right;">
                        <asp:Label runat="server" ID="lblPin" Text="Pin Code" CssClass="ltlstr"></asp:Label>
                        
                    </td>
                    <td style="text-align:left;">
                        <asp:TextBox runat="server" ID="txtpin"></asp:TextBox>
                        &nbsp;<asp:Button runat="server" ID="btn_pincheck" Text="Validate Pin" OnClick="btn_pincheck_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:ScriptManager runat="server" />
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:Timer ID="timer" runat="server" OnTick="timer_Tick" />
                                <asp:Label runat="server" ID="lblResult" CssClass="ltlstr"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr style="height:300px;">
                    <td colspan="2">
                        <asp:Literal runat="server" ID="ltlQRCode"></asp:Literal>
                        <br />
                        <asp:Literal runat="server" ID="ltlQRCodeContent" Visible="false"></asp:Literal>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
