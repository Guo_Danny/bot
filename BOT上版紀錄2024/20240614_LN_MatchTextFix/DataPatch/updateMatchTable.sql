USE
OFAC
GO

--Y
update ofac..MatchHistTable 
set MatchText = 'ASSET SOLUTIONS LONDON PDF BANK OF'+ char(13)+ char(10) +'NEW YORK ASSET SOLUTIONS' 
where MatchName = 'SLUTSKIY,LEONID' and SeqNumb = '65628'
and entnum = '20189656'


--N
update ofac..MatchHistTable 
set MatchText = '/045036070222'+ char(13)+ char(10) +'NATIONAL TAXATION BUREAU OF TAIPEI' 
where MatchName = 'TAJUDEEN BARUWA' and SeqNumb = '65966'
and entnum = '27603378'
