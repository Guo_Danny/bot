declare @StrDate datetime ,@_txt varchar(500),@_txt2 varchar(500),@listtype varchar(50),@modidate char(10), @count varchar(10);
declare @mxcreDate datetime,@mxmodDate datetime
Select @mxcreDate=max(Createdate) from OFAC.dbo.SDNTable(nolock) where ListType in ('WPEP','World-Check') or listtype like '%wpep'
Select @mxmodDate=max(Lastmodifdate) from OFAC.dbo.SDNTable(nolock) where  ListType in ('WPEP','World-Check') or listtype like '%wpep'

if @mxmodDate > @mxcreDate
   SELECT @StrDate=@mxmodDate
else
   SELECT @StrDate=@mxcreDate

--select @StrDate='2022-10-11 00:00:00'

select @StrDate=convert(char(10),@strDate,111) + ' 00:00:00'
--select @StrDate , @mxDate2 listModify, @mxDate2 listCreate,DATEDIFF(DAY,@StrDate,getdate()) diffDate

if DATEDIFF(DAY,@StrDate,getdate()) >1
begin
   select 'Please Check Daily World-Check Import Process, it is not insert into SDNTABLE on ' + convert(varchar(20),getdate(),111)
   goto SqlExit
end


--count for 0138wpep
set @_txt2 = '';

declare cur_count CURSOR FOR
select listtype,count(1)  from OFAC.dbo.sdntable(nolock)
--Nov.30 2022 exclude HQ sharing list
where Status != 4 and (listtype like '%WPEP')  group by Listtype;
open cur_count;
fetch next from cur_count into @listtype,@_txt2;
while @@fetch_status = 0
begin 
  insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
  values (getdate(),'Prime','Total Active '+ @listtype +' records :' + @_txt2,'Ann','Event','World-Check Count','Total Active '+ @listtype +' records :' + @_txt2);

  fetch next from cur_count into @listtype,@_txt2;
end
close cur_count;
deallocate cur_count;

-- negative news count
declare cur_negcount CURSOR FOR
select listtype,count(1)  from OFAC.dbo.sdntable(nolock)
where Status != 4 and listtype = 'WORLD-CHECK' and Program in ('negative news','Adverse News') group by Listtype;
open cur_negcount;
fetch next from cur_negcount into @listtype,@_txt2;
while @@fetch_status = 0
begin 
  insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
  values (getdate(),'Prime','Total Active Negative/Adverse News records :' + @_txt2,'Ann','Event','World-Check Count','Total Active Negative/Adverse News records :' + @_txt2);

  fetch next from cur_negcount into @listtype,@_txt2;
end
close cur_negcount;
deallocate cur_negcount;

-- Sanctions count
declare cur_negcount CURSOR FOR
select listtype,count(1)  from OFAC.dbo.sdntable(nolock)
where Status != 4 and listtype = 'WORLD-CHECK' and Program = 'Sanctions' group by Listtype;
open cur_negcount;
fetch next from cur_negcount into @listtype,@_txt2;
while @@fetch_status = 0
begin 
  insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
  values (getdate(),'Prime','Total Active Sanctions records :' + @_txt2,'Ann','Event','World-Check Count','Total Active Sanctions records :' + @_txt2);

  fetch next from cur_negcount into @listtype,@_txt2;
end
close cur_negcount;
deallocate cur_negcount;


SqlExit:

