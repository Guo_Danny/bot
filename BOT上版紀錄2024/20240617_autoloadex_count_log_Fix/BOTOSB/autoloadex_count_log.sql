﻿declare @StrDate datetime ,@_txt varchar(500),@_txt2 varchar(500),@listtype varchar(50),@modidate char(10), @count varchar(10);
declare @mxDate1 datetime,@mxDate2 datetime
Select @mxDate1=max(listmodifdate) from OFAC.dbo.SDNTable(nolock) where ListType not in ('USER','WPEP') and ListType not like 'HQ%'
Select @mxDate2=max(ListCreatedate) from OFAC.dbo.SDNTable(nolock) where ListType not in ('USER','WPEP') and ListType not like 'HQ%'

if @mxDate1 > @mxDate2
   SELECT @StrDate=@mxDate1
else
   SELECT @StrDate=@mxDate2
--select @StrDate='2022-10-11 00:00:00'

select @StrDate=convert(char(10),@strDate,111) + ' 00:00:00'
--select @StrDate , @mxDate2 listModify, @mxDate2 listCreate,DATEDIFF(DAY,@StrDate,getdate()) diffDate

if DATEDIFF(DAY,@StrDate,getdate()) >1
begin
   select 'Please Check Daily World-Check Import Process, it is not insert into SDNTABLE on ' + convert(varchar(20),getdate(),111)
   goto SqlExit
end

set @_txt2 = '';
declare cur CURSOR FOR
select ListType, convert(char(16),listmodifdate,111) listmodifdate, count(*)  from OFAC.dbo.SDNTable(nolock) where 
                            (ListModifDate >=@StrDate )and  
                            (ListModifDate <=getdate() or LastModifDate <=getdate()) 
                            and ListType not in ('USER') 
							and ListType not like 'HQ%'
							--Nov.30 2022 exclude HQ sharing list
group by ListType,convert(char(16),listmodifdate,111) 
order by listtype,convert(char(16),listmodifdate,111)

open cur;
fetch next from cur into @listtype,@modidate,@count;
WHILE @@fetch_status = 0 
begin
  set @_txt2 = @_txt2 + @listtype + ',' + @modidate + ',' + @count + char(13);
  fetch next from cur into @listtype,@modidate,@count;
end
close cur;
DEALLOCATE cur;

select 'Start Date:' + convert(char(16),@StrDate,111) + char(13) + 'ListType,ModifiDate,Count' + char(13) + @_txt2

insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
values (getdate(),'Prime','Starting load of the distribution dated ' + convert(char(16),@StrDate,111) + char(13) + 'ListType,ModifiDate,Count' + char(13) + @_txt2,'Ann','Event','Autoload','Starting load of the distribution dated ' + convert(char(16),@StrDate,111) + char(13) + 'ListType,ModifiDate,Count' + char(13) + @_txt2);


-- update listtype to WPEP where WC.subcatory is blank.
--201808 mark-- update OFAC.dbo.SDNTable set ListType = 'WPEP' where ListType = '0138WPEP' and Program = 'PEP';

--count for 0138wpep
set @_txt2 = '';

declare cur_count CURSOR FOR
select listtype,count(1)  from OFAC.dbo.sdntable(nolock)
--Nov.30 2022 exclude HQ sharing list
where Status != 4 and (listtype like '%WPEP')  group by Listtype;
open cur_count;
fetch next from cur_count into @listtype,@_txt2;
while @@fetch_status = 0
begin 
  insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
  values (getdate(),'Prime','Total Active '+ @listtype +' records :' + @_txt2,'Ann','Event','World-Check Count','Total Active '+ @listtype +' records :' + @_txt2);

  fetch next from cur_count into @listtype,@_txt2;
end
close cur_count;
deallocate cur_count;

-- negative news count
declare cur_negcount CURSOR FOR
select listtype,count(1)  from OFAC.dbo.sdntable(nolock)
where Status != 4 and listtype = 'WORLD-CHECK' and Program = 'negative news' group by Listtype;
open cur_negcount;
fetch next from cur_negcount into @listtype,@_txt2;
while @@fetch_status = 0
begin 
  insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
  values (getdate(),'Prime','Total Active Negative News records :' + @_txt2,'Ann','Event','World-Check Count','Total Active Negative News records :' + @_txt2);

  fetch next from cur_negcount into @listtype,@_txt2;
end
close cur_negcount;
deallocate cur_negcount;

-- Sanctions count
declare cur_negcount CURSOR FOR
select listtype,count(1)  from OFAC.dbo.sdntable(nolock)
where Status != 4 and listtype = 'WORLD-CHECK' and Program = 'Sanctions' group by Listtype;
open cur_negcount;
fetch next from cur_negcount into @listtype,@_txt2;
while @@fetch_status = 0
begin 
  insert into OFAC.dbo.SDNChangeLog(LogDate,Oper,LogText,[type],ObjectType,ObjectId,EvtDetail) 
  values (getdate(),'Prime','Total Active Sanctions records :' + @_txt2,'Ann','Event','World-Check Count','Total Active Negative News records :' + @_txt2);

  fetch next from cur_negcount into @listtype,@_txt2;
end
close cur_negcount;
deallocate cur_negcount;


SqlExit:

