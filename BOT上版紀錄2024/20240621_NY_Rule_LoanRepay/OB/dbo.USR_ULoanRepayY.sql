USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_ULoanRepayY]    Script Date: 6/20/2024 10:38:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[USR_ULoanRepayY] (@WLCode SCode, 
	@testAlert 	INT,
	@activityTypeIn VARCHAR(8000),
	@activityTypeOut VARCHAR(8000),
	@productType 	VARCHAR(8000),
	@percent  	INT,
	@month  	INT,
	@RiskClass 	VARCHAR(8000)
	)
AS
	/* RULE AND PARAMETER DESCRIPTION
	Detects a loan taken from product (account type) X and Y% is repaid within Z months.
	
		@activityType = activity type of loan
		@productType = account type e.g. checking, check,
		@percent = % of withdrawn from received
		@month = period to check,
		@RiskClass = risk class to filter on
	*/
	SET NOCOUNT ON
	
	-- /*  Declarations */
	DECLARE @description VARCHAR(2000),
		@iWLcode 	SCode, 
		@iTestAlert 	INT,
		@iActivityTypeIn VARCHAR(8000),
		@iActivityTypeOut VARCHAR(8000),
		@iProductType 	VARCHAR(8000),
		@iPercent  	int,
		@iMonth  	int,
		@iRiskClass 	VARCHAR(8000)

	--speeds up select statements if use of local variable
	SELECT @iWLcode = @WLcode , 
		@iTestAlert = @TestAlert ,
		@iActivityTypeIn = 	@ActivityTypeIn ,
		@iActivityTypeOut = 	@ActivityTypeOut ,
		@iProductType = 	@ProductType ,
		@iPercent  = ABS(@Percent)  ,
		@iMonth = ABS(@Month) , 
		@iRiskClass = @riskclass
	
	DECLARE	@desc VARCHAR(2000),
		@Id INT, 
		@WLType INT,  --0 for alert, 1 for case
		@stat INT,
		@trnCnt INT,
		@minDate INT,
		@cur CURSOR
	
	
	DECLARE @account LONGNAME,
		@currDate INT,
		@recvAmt MONEY,
		@date int,
		@payAmt MONEY
	
	declare @startAlrtDate datetime
	declare @endAlrtDate datetime
	declare @bookDate int
	
	
	
	-- This table contains the deposit entries/repayment
	DECLARE	@repayEntries TABLE (
		Account VARCHAR(40),
		TranNo  int,
		Amount  MONEY
	)
	
	-- Jun.21, 2024 add This table contains the deposit entries/repayment with ref
	DECLARE	@repayEntries_ref TABLE (
		Account VARCHAR(40),
		TranNo  int,
		Amount  MONEY,
		Ref     VARCHAR(35)
	)

	-- This table contains summary of the withdrawals
	DECLARE	@repaySummary TABLE (
		Account VARCHAR(40),
		Amount MONEY
	)

	-- Jun.21, 2024 add This table contains summary of the withdrawals with ref
	DECLARE	@repaySummary_ref TABLE (
		Account VARCHAR(40),
		Amount MONEY,
		Ref    VARCHAR(35)
	)	

	-- This table contains the withdrawals entries/loan
	DECLARE	@loanEntries TABLE (
		Account VARCHAR(40),
		TranNo  int,
		Amount  MONEY
	)

	-- Jun.21, 2024 add This table contains the withdrawals entries/loan with ref
	DECLARE	@loanEntries_ref TABLE (
		Account VARCHAR(40),
		TranNo  int,
		Amount  MONEY,
		Ref     VARCHAR(35)
	)

	-- 20240223 add This table contains summary of the withdrawals
	DECLARE	@LoanSummary TABLE (
		Account VARCHAR(40),
		Amount MONEY
	)

	-- Jun.21, 2024 add This table contains summary of the withdrawals with ref
	DECLARE	@LoanSummary_ref TABLE (
		Account VARCHAR(40),
		Amount MONEY,
		Ref    VARCHAR(35)
	)

	-- This tables to hold the accounts.
	DECLARE @accts TABLE (
		Account VARCHAR(40),
		Amount MONEY
	)

	-- Jun.21, 2024 add This tables to hold the accounts. with ref
	DECLARE @accts_ref TABLE (
		Account VARCHAR(40),
		Amount MONEY,
		Ref    VARCHAR(35)
	)

	-- Temporary table of Activity Types that have not been specIFied as Exempt
	DECLARE @ActInType TABLE (
		Type	INT
	)
	DECLARE @ActOutType TABLE (
		Type	INT
	)

	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @iActivityTypeIn = dbo.BSA_fnListParams(@iActivityTypeIn)
	SELECT @iActivityTypeOut = dbo.BSA_fnListParams(@iActivityTypeOut)
	SELECT @iRiskClass = dbo.BSA_fnListParams(@iRiskClass)
	SELECT @iProductType = dbo.BSA_fnListParams(@iProductType)

	SET @stat = 0
	/*****************************************/
	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE,@StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) Then
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 Then
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode
	
	/******************************************/
		

	DECLARE @txt VARCHAR(1000)
	IF @testAlert = 0 
	BEGIN
		SET @txt = 'Started Rule Evaluation'
		Exec BSA_InsEvent 'PRIME', 'Evl', 'Rule', @WLCode , @txt
	END
	--- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */
	SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN USR_ULoanRepayY
	ELSE
		-- Already in a transaction
		SAVE TRAN USR_ULoanRepayY
	/* End standard stored procedure transaction header */
	
	
	SET @minDate = dbo.ConvertSqlDateToInt(
		DATEADD(m, -1 * @iMonth, CONVERT(VARCHAR, @startDate)))

	SET @currDate = dbo.ConvertSqlDateToInt(GETDATE())

	INSERT INTO @ActInType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@iActivityTypeIn IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@iActivityTypeIn) > 0)	

	INSERT INTO @ActOutType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@iActivityTypeOut IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@iActivityTypeOut) > 0)	
		
	INSERT INTO @repayEntries (Account, TranNo, Amount) 
		SELECT 	a.Account, a.TranNo, a.BaseAmt
		FROM 	Activity a (NOLOCK) 
		INNER 	JOIN Account acct (NOLOCK) ON a.Account = acct.Id 
		INNER 	JOIN Customer cust  (NOLOCK) ON a.Cust = Cust.Id 
		INNER	JOIN @ActInType Act ON Act.Type = A.Type
		WHERE 	a.bookdate > @MINDATE  AND A.BookDate <= dbo.ConvertSqlDateToInt(@startDate)
		AND 	RecvPay = 1
		AND 	A.Account IS NOT NULL 
		AND 	(@iRiskClass IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, cust.RiskClass) + ',',@iRiskClass) > 0)
		AND 	(@iProductType IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, acct.Type) + ',',@iProductType) > 0)	
	
	-- Get all the accounts into temporary @aacts table so you left with unique values.
	-- This has to be after the first retrieval of withdrawals for current activity
	INSERT INTO @accts (account)
	SELECT DISTINCT Account FROM @repayEntries
	
	INSERT INTO @repayEntries (Account, TranNo, Amount) 
		SELECT 	ah.Account, ah.TranNo, ah.BaseAmt
		FROM 	ActivityHist ah (NOLOCK) 
		INNER 	JOIN @accts acct ON ah.Account = acct.Account 
		INNER 	JOIN Customer cust  (NOLOCK) ON ah.Cust = Cust.Id 
		INNER	JOIN @ActInType Act ON Act.Type = Ah.Type
		WHERE 	ah.bookdate > @MINDATE  AND Ah.BookDate <= dbo.ConvertSqlDateToInt(@startDate) 
		AND 	recvpay = 1 AND ah.Account IS NOT NULL AND
			(@iRiskClass IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, cust.RiskClass) + ',',@iRiskClass) > 0)
		
	-- Select the records into the summary table , where it has account and associates 
	-- repayment summary.
	INSERT INTO @repaySummary (Account,  Amount)
		SELECT Account, SUM(Amount) from @repayEntries group by Account
	
	INSERT INTO @loanEntries (Account, TranNo, Amount) 
		SELECT 	a.Account, a.TranNo, a.BaseAmt
		FROM 	Activity a (NOLOCK) 
		INNER 	JOIN @accts acct ON a.Account = acct.account
		INNER 	JOIN Customer cust  (NOLOCK) ON a.Cust = Cust.Id 
		INNER	JOIN @ActOutType Act ON Act.Type = A.Type
		WHERE 	a.bookdate > @MINDATE  AND A.BookDate <= dbo.ConvertSqlDateToInt(@startDate) 
		and 	recvpay = 2 AND a.Account IS NOT NULL AND
			(@iRiskClass IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, cust.RiskClass) + ',',@iRiskClass) > 0)

	INSERT INTO @loanEntries (Account, TranNo, Amount) 
		SELECT 	ah.Account, ah.TranNo, ah.BaseAmt
		FROM 	ActivityHist ah (NOLOCK) 
		INNER 	JOIN @accts acct ON ah.Account = acct.account
		INNER 	JOIN Customer cust  (NOLOCK)ON ah.Cust = Cust.Id 
		INNER	JOIN @ActOutType Act ON Act.Type = Ah.Type
		WHERE 	ah.bookdate > @MINDATE  AND Ah.BookDate <= dbo.ConvertSqlDateToInt(@startDate) 
		and 	recvpay = 2	AND ah.Account IS NOT NULL
		AND 	(@iRiskClass IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, cust.RiskClass) + ',',@iRiskClass) > 0)

	-- Select the records into the summary table , where it has account and associates 
	-- Loan summary.
	INSERT INTO @LoanSummary (Account,  Amount)
		SELECT Account, SUM(Amount) from @loanEntries group by Account	

	-- Clear Temporary accounts table for reuse.
	delete @accts

/* --20240219 mask	
	insert into @accts (Account, Amount)
		select A.Account, A.Amount from
		@repaySummary A  where exists (
		select A.Account from @loanEntries B 
		where A.account = B.Account and  
		 (B.Amount * @iPercent <= A.Amount * 100)
		)
*/
	--20240219 change, start
	insert into @accts (Account, Amount)
		select A.Account, A.Amount from @repaySummary A  inner join @LoanSummary B on A.account = B.Account 
		where (B.Amount * @iPercent <= A.Amount * 100)
		
	--20240219 change, end

	-- @iPercent <= ((A.Amount * 100) / B.PayAmt))
	
	--Jun.21,2024 add start

	INSERT INTO @repayEntries_ref (Account, TranNo, Amount, Ref) 
		SELECT 	a.Account, a.TranNo, a.BaseAmt, a.Ref
		FROM 	Activity a (NOLOCK) 
		INNER 	JOIN Account acct (NOLOCK) ON a.Account = acct.Id 
		INNER 	JOIN Customer cust  (NOLOCK) ON a.Cust = Cust.Id 
		INNER	JOIN @ActInType Act ON Act.Type = A.Type
		WHERE 	a.bookdate > @MINDATE  AND A.BookDate <= dbo.ConvertSqlDateToInt(@startDate)
		AND 	RecvPay = 1
		AND 	A.Account IS NOT NULL 
		AND 	(@iRiskClass IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, cust.RiskClass) + ',',@iRiskClass) > 0)
		AND 	(@iProductType IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, acct.Type) + ',',@iProductType) > 0)	
	
	-- Get all the accounts into temporary @aacts table so you left with unique values.
	-- This has to be after the first retrieval of withdrawals for current activity
	INSERT INTO @accts_ref (account, Ref)
	SELECT DISTINCT Account, Ref FROM @repayEntries_ref
	
	INSERT INTO @repayEntries_ref (Account, TranNo, Amount, Ref) 
		SELECT 	ah.Account, ah.TranNo, ah.BaseAmt, ah.Ref
		FROM 	ActivityHist ah (NOLOCK) 
		INNER 	JOIN @accts_ref acct ON ah.Account = acct.Account and ah.Ref = acct.Ref
		INNER 	JOIN Customer cust  (NOLOCK) ON ah.Cust = Cust.Id 
		INNER	JOIN @ActInType Act ON Act.Type = Ah.Type
		WHERE 	ah.bookdate > @MINDATE  AND Ah.BookDate <= dbo.ConvertSqlDateToInt(@startDate) 
		AND 	recvpay = 1 AND ah.Account IS NOT NULL AND
			(@iRiskClass IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, cust.RiskClass) + ',',@iRiskClass) > 0)
		
	-- Select the records into the summary table , where it has account and associates 
	-- repayment summary.
	INSERT INTO @repaySummary_ref (Account,  Amount, Ref)
		SELECT Account, SUM(Amount), Ref from @repayEntries_ref group by Account,Ref
	
	INSERT INTO @loanEntries_ref (Account, TranNo, Amount, Ref) 
		SELECT 	a.Account, a.TranNo, a.BaseAmt, a.Ref
		FROM 	Activity a (NOLOCK) 
		INNER 	JOIN @accts_ref acct ON a.Account = acct.account and a.Ref = acct.Ref
		INNER 	JOIN Customer cust  (NOLOCK) ON a.Cust = Cust.Id 
		INNER	JOIN @ActOutType Act ON Act.Type = A.Type
		WHERE 	a.bookdate > @MINDATE  AND A.BookDate <= dbo.ConvertSqlDateToInt(@startDate) 
		and 	recvpay = 2 AND a.Account IS NOT NULL AND
			(@iRiskClass IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, cust.RiskClass) + ',',@iRiskClass) > 0)

	INSERT INTO @loanEntries_ref (Account, TranNo, Amount, Ref) 
		SELECT 	ah.Account, ah.TranNo, ah.BaseAmt, ah.Ref
		FROM 	ActivityHist ah (NOLOCK) 
		INNER 	JOIN @accts_ref acct ON ah.Account = acct.account and ah.Ref = acct.Ref
		INNER 	JOIN Customer cust  (NOLOCK)ON ah.Cust = Cust.Id 
		INNER	JOIN @ActOutType Act ON Act.Type = Ah.Type
		WHERE 	ah.bookdate > @MINDATE  AND Ah.BookDate <= dbo.ConvertSqlDateToInt(@startDate) 
		and 	recvpay = 2	AND ah.Account IS NOT NULL
		AND 	(@iRiskClass IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, cust.RiskClass) + ',',@iRiskClass) > 0)

	-- Select the records into the summary table , where it has account and associates 
	-- Loan summary.
	INSERT INTO @LoanSummary_ref (Account,  Amount, Ref)
		SELECT Account, SUM(Amount), Ref from @loanEntries_ref group by Account, Ref

	-- Clear Temporary accounts table for reuse.
	delete @accts_ref

	insert into @accts_ref (Account, Amount, Ref)
	select A.Account, A.Amount, A.Ref from @repaySummary_ref A  
	inner join @LoanSummary_ref B on A.account = B.Account and a.Ref = b.Ref
	where (B.Amount * @iPercent <= A.Amount * 100)

	--Jun.21,2024 add End

	IF @testAlert = 1 
	BEGIN
 		SELECT @startAlrtDate = GETDATE()
		INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
					LASTMODIFY, CUST, ACCOUNT, IsTest)
		select @WLCODE,  'Account:  ''' + a.Account 
				+ '''' +  ' had ' +  CONVERT(VARCHAR, @iPercent) + '% or more amount'  +
				' repaid ( ' + CONVERT(VARCHAR,a.Amount ) + ' ) in the last ' +  
				CONVERT(VARCHAR, @imonth) + ' months.'
			 , 0, GETDATE(), NULL, NULL, NULL, a.Account, 1 from
		@accts a
		SELECT @STAT = @@ERROR	
		SELECT @endAlrtDate = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
	
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   SELECT 'ALERT', ALERTNO, a.TRANNO 
			FROM (
 					SELECT Account, TranNo, Amount FROM @loanEntries UNION 
					SELECT Account, TranNo, Amount FROM @repayEntries 
				) a 
				INNER JOIN @accts t ON a.account = t.account 	
		        INNER JOIN ALERT ON t.Account = ALERT.Account							
				
			WHERE 
			ALERT.WLCODE = @WLCODE AND
			ALERT.CREATEDATE BETWEEN @startAlrtDate AND @endAlrtDate

 		SELECT @STAT = @@ERROR 
		IF @STAT <> 0  GOTO ENDOFPROC
		
		--Jun.26,2024 add start
		SELECT @startAlrtDate = GETDATE()
		INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
					LASTMODIFY, CUST, ACCOUNT, IsTest)
		select @WLCODE,  'Account:  ''' + a.Account 
				+ '''' +  ' had ' +  CONVERT(VARCHAR, @iPercent) + '% or more amount'  +
				' repaid ( ' + CONVERT(VARCHAR,a.Amount ) + ' ) in the last ' +  
				CONVERT(VARCHAR, @imonth) + ' months.'
			 , 0, GETDATE(), NULL, NULL, NULL, a.Account, 1 from
		@accts_ref a
		SELECT @STAT = @@ERROR	
		SELECT @endAlrtDate = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
	
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   SELECT 'ALERT', ALERTNO, a.TRANNO 
			FROM (
 					SELECT Account, TranNo, Amount, Ref FROM @loanEntries_ref UNION 
					SELECT Account, TranNo, Amount, Ref FROM @repayEntries_ref 
				) a 
				INNER JOIN @accts_ref t ON a.account = t.account and a.Ref = t.Ref
		        INNER JOIN ALERT ON t.Account = ALERT.Account							
				
			WHERE 
			ALERT.WLCODE = @WLCODE AND
			ALERT.CREATEDATE BETWEEN @startAlrtDate AND @endAlrtDate

 		SELECT @STAT = @@ERROR 
		IF @STAT <> 0  GOTO ENDOFPROC
		--Jun.26,2024 add end
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 
		BEGIN
			SELECT @startAlrtDate = GETDATE()
	 		INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
						LASTMODIFY, CUST, ACCOUNT)
			select @WLCODE,  'Account:  ''' + a.Account 
					+ '''' +  ' had ' +  CONVERT(VARCHAR, @iPercent) + '% or more amount'  +
					' repaid ( ' + CONVERT(VARCHAR,a.Amount ) + ' ) in the last ' +  
					CONVERT(VARCHAR, @imonth) + ' months.'
				   , 0, GETDATE(), NULL, NULL, NULL, a.Account from
			@accts a
		 
			SELECT @STAT = @@ERROR	
			SELECT @endAlrtDate = GETDATE()
	 		IF @STAT <> 0  GOTO ENDOFPROC


 			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	 				SELECT 'ALERT', ALERTNO, a.TRANNO 
				FROM (
	 					SELECT Account, TranNo, Amount FROM @loanEntries UNION 
						SELECT Account, TranNo, Amount FROM @repayEntries ) a 
					INNER JOIN @accts t ON a.account = t.account 			
					INNER JOIN ALERT ON t.Account = ALERT.Account 

				WHERE  	
				ALERT.WLCODE = @WLCODE AND
				ALERT.CREATEDATE BETWEEN @startAlrtDate AND @endAlrtDate 

 			SELECT @STAT = @@ERROR 


			--Jun.26,2024 add start
			SELECT @startAlrtDate = GETDATE()
	 		INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
						LASTMODIFY, CUST, ACCOUNT)
			select @WLCODE,  'Account:  ''' + a.Account 
					+ '''' +  ' had ' +  CONVERT(VARCHAR, @iPercent) + '% or more amount'  +
					' repaid ( ' + CONVERT(VARCHAR,a.Amount ) + ' ) in the last ' +  
					CONVERT(VARCHAR, @imonth) + ' months.'
				   , 0, GETDATE(), NULL, NULL, NULL, a.Account from
			@accts_ref a
		 
			SELECT @STAT = @@ERROR	
			SELECT @endAlrtDate = GETDATE()
	 		IF @STAT <> 0  GOTO ENDOFPROC
 			
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	 				SELECT 'ALERT', ALERTNO, a.TRANNO 
				FROM (
	 					SELECT Account, TranNo, Amount, Ref FROM @loanEntries_ref UNION 
						SELECT Account, TranNo, Amount, Ref FROM @repayEntries_ref ) a 
					INNER JOIN @accts_ref t ON a.account = t.account and a.Ref = t.Ref
					INNER JOIN ALERT ON t.Account = ALERT.Account 

				WHERE  	
				ALERT.WLCODE = @WLCODE AND
				ALERT.CREATEDATE BETWEEN @startAlrtDate AND @endAlrtDate 

 			SELECT @STAT = @@ERROR 
			--Jun.26,2024 add end
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
			SELECT @startAlrtDate = GETDATE()
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			WLCODE, WLDESC, CREATETIME )

			SELECT	NULL, @currDate, Null, Account,
				NULL, 'Rule', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCODE,  'Account:  ''' + a.Account 
					+ '''' +  ' had ' +  CONVERT(VARCHAR, @iPercent) + '% or more amount'  +
					' repaid ( ' + CONVERT(VARCHAR,a.Amount ) + ' ) in the last ' +  
					CONVERT(VARCHAR, @imonth) + ' months.' , GETDATE() 
			FROM 	@accts A 
			
			SELECT @STAT = @@ERROR	
			SELECT @endAlrtDate = GETDATE()
			IF @STAT <> 0  GOTO ENDOFPROC

			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			   SELECT 'SUSPACT', RECNO, a.TRANNO 
				FROM (
					SELECT Account, TranNo, Amount FROM @loanEntries UNION 
					SELECT Account, TranNo, Amount FROM @repayEntries ) a 
					INNER JOIN @accts t ON a.account = t.account 					
					INNER JOIN suspiciousactivity s ON t.Account = S.Account 

				WHERE 			
				S.WLCODE = @WLCODE AND
				S.CREATETIME BETWEEN @startAlrtDate AND @endAlrtDate 

			SELECT @STAT = @@ERROR 

			--Jun.26,2024 add start
			SELECT @startAlrtDate = GETDATE()
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			WLCODE, WLDESC, CREATETIME )

			SELECT	NULL, @currDate, Null, Account,
				NULL, 'Rule', NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, 
				NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCODE,  'Account:  ''' + a.Account 
					+ '''' +  ' had ' +  CONVERT(VARCHAR, @iPercent) + '% or more amount'  +
					' repaid ( ' + CONVERT(VARCHAR,a.Amount ) + ' ) in the last ' +  
					CONVERT(VARCHAR, @imonth) + ' months.' , GETDATE() 
			FROM 	@accts_ref A 
			
			SELECT @STAT = @@ERROR	
			SELECT @endAlrtDate = GETDATE()
			IF @STAT <> 0  GOTO ENDOFPROC

			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			   SELECT 'SUSPACT', RECNO, a.TRANNO 
				FROM (
					SELECT Account, TranNo, Amount, Ref FROM @loanEntries_ref UNION 
					SELECT Account, TranNo, Amount, Ref FROM @repayEntries_ref ) a 
					INNER JOIN @accts_ref t ON a.account = t.account and a.Ref = t.Ref
					INNER JOIN suspiciousactivity s ON t.Account = S.Account 

				WHERE 			
				S.WLCODE = @WLCODE AND
				S.CREATETIME BETWEEN @startAlrtDate AND @endAlrtDate 

			SELECT @STAT = @@ERROR 
			--Jun.26,2024 add end
		END
		
		/*Jun.26,2024 mark
		IF @WLTYPE = 0 
		BEGIN
 			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	 		        SELECT 'ALERT', ALERTNO, a.TRANNO 
				FROM (
	 					SELECT Account, TranNo, Amount FROM @loanEntries UNION 
						SELECT Account, TranNo, Amount FROM @repayEntries ) a 
					INNER JOIN @accts t ON a.account = t.account 			
					INNER JOIN ALERT ON t.Account = ALERT.Account 

				WHERE  	
				ALERT.WLCODE = @WLCODE AND
				ALERT.CREATEDATE BETWEEN @startAlrtDate AND @endAlrtDate 

 			SELECT @STAT = @@ERROR 
 		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			   SELECT 'SUSPACT', RECNO, a.TRANNO 
				FROM (
					SELECT Account, TranNo, Amount FROM @loanEntries UNION 
					SELECT Account, TranNo, Amount FROM @repayEntries ) a 
					INNER JOIN @accts t ON a.account = t.account 					
					INNER JOIN suspiciousactivity s ON t.Account = S.Account 

				WHERE 			
				S.WLCODE = @WLCODE AND
				S.CREATETIME BETWEEN @startAlrtDate AND @endAlrtDate 

			SELECT @STAT = @@ERROR 
		END
		*/
	END
	
	EndOfProc:
	IF (@stat <> 0) BEGIN 
	  ROLLBACK TRAN USR_ULoanRepayY
	  RETURN @stat
	END	
	
	IF @trnCnt = 0
	  COMMIT TRAN USR_ULoanRepayY
	RETURN @stat
	
	
GO


