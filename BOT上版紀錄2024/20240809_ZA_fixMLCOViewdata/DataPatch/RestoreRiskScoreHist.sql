use PBSA
GO

--begin tran

update RiskScoreHist
set DateAccepted = null where customerid in (
select Cust from RiskFactorDataView
where RiskFactorDataView.Cust IN (SELECT id FROM CUSTOMER WHERE KYCSTATUS=2) or RiskFactorDataView.Cust in (select customerid from RiskScoreHist where isnull(DateAccepted,'')='' and riskscore is null and isnull(AcceptOper,'')<> '' ))
and customerid != '0161703601' and CreateDate <= '2024-8-2 00:00:00'
and DateAccepted is null

--commit