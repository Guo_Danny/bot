use PBSA
GO

declare @AuditMessage varchar(500)
set @AuditMessage = 'Fix Wrong CDD Records'
update customer set Notes = Notes + '. ' + @AuditMessage where id in (
select customerid from RiskScoreHist where customerid != '0161703601' and CreateDate <= '2024-8-2 00:00:00' and DateAccepted is null)
)

--Query
select * from RiskFactorDataView
	where Cust in (select customerid from RiskScoreHist where customerid != '0161703601' and CreateDate <= '2024-8-2 00:00:00'
	and DateAccepted is null)


--begin tran

update RiskScoreHist
set DateAccepted = getdate(), AcceptOper = 'Primeadmin' where customerid != '0161703601' and CreateDate <= '2024-8-2 00:00:00'
and DateAccepted is null


select * from RiskFactorDataView
	where Cust in (select customerid from RiskScoreHist where customerid != '0161703601' and CreateDate <= '2024-8-2 00:00:00'
	and DateAccepted is null)
--commit

