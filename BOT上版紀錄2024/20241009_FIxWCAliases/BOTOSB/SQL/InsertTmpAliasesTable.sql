/****** Script for SelectTopNRows command from SSMS  ******/
use ofac
go
declare @ListCreateDate datetime, @ListModifDate datetime,@CreateDate datetime, @LastModifDate datetime, 
@entnum varchar(15),@SDNName varchar(3000),@Aliases varchar(max), @maxalianum varchar(15), @location varchar(max), @sdntype varchar(30),
@AltType varchar(200), @Category varchar(200), @SDNName_tmp varchar(200), @Aliases_tmp varchar(max), @location_1 varchar(1000),@location_1_1 varchar(1000),@location_2 varchar(1000) ,@location_2_1 varchar(1000)
set @ListCreateDate = NULL;
set @ListModifDate = NULL;
set @CreateDate = NULL;
set @LastModifDate = NULL;
set @location_1_1 = '';
--select @maxalianum = case when alianum is null then 1 else alianum +1  end
-- from tmpaliasestable(nolock)
select @maxalianum = max(alianum)+1 from TmpAliasesTable(nolock)

if @maxalianum is null
begin
set  @maxalianum =1
end

--begin tran
--delete  from TmpAliasesTable where entnum in 
--(   select entnum from TmpAliasesTable(nolock)
--    group by entnum,altname    having count(1)  >1)


--Delete TmpAliasesTable筆數與SDNAltTable筆數不一致的資料
delete TmpAliasesTable 
where EntNum in (
select distinct t1.entnum from 
( select count(1) as countt1, entnum from (select distinct entnum,altname from TmpAliasesTable(nolock)) dt group by entnum ) T1  
inner join 
( select count(1) as countt2, entnum from (select entnum,altname from SDNAltTable(nolock) where EntNum >= 20000000 and (status !=4 or deleted = 1)
and isnull(listid,'') in ('alt','') and isnull(AltType,'') = 'a.k.a.' and entnum in 
(select entnum from sdntable(nolock) where program != 'other' and status !=4) ) dt2 group by entnum ) T2 on T1.entnum = T2.EntNum and t1.countt1 != t2.countt2 
inner join worldcheck(nolock) wc on t1.entnum= wc.entnum
)

--Oct.1.2024 1: 調整刪除TmpAliasesTable的SQL: 將殘留資料別名刪除， 條件為SDNAltTable中別名已經標注為刪除的資料。
--select t.EntNum, t.AltName, s.EntNum, s.AltName
delete TmpAliasesTable where EntNum in (
	select t.EntNum from (
		(select EntNum, AltName from TmpAliasesTable(nolock)) t
	left join 
		(select EntNum, AltName from SDNAltTable(nolock)
		where EntNum >= 20000000 and (status !=4 or deleted = 1)
		and isnull(listid,'') in ('alt','') and isnull(AltType,'') = 'a.k.a.' and entnum in 
		(select entnum from sdntable(nolock) where program != 'other' and status !=4)
		) s on t.EntNum = s.EntNum and t.AltName = s.AltName
	) where s.EntNum is null
)

--Oct.1.2024 2: 調整刪除TmpAliasesTable的SQL:  將殘留資料別名刪除，條件為WorldCheck.Aliases欄位已經沒有值了的資料。
delete TmpAliasesTable where EntNum in (
	select w.Entnum from WorldCheck(nolock) w
	where isnull(Aliases,'') = '' and exists (select Entnum from TmpAliasesTable(nolock) t where t.EntNum = w.Entnum)
)

--rollback 

declare cur CURSOR FOR
--select distinct sdn.EntNum,wc.Aliases
--from SDNTable(nolock) sdn
--join WorldCheck(nolock) wc on wc.Entnum = sdn.EntNum
--join SDNAltTable(nolock) sdna on sdna.EntNum = sdn.EntNum
--where isnull(wc.Aliases,'') != '' 
--and isnull(sdna.ListID,'') in ('ALT','')
--and isnull(sdna.AltType,'') = 'a.k.a.'
--and sdn.Status != 4
--and sdn.EntNum  >= 20000000
--and sdn.program != 'other'
--and sdn.entnum not in (select entnum from TmpAliasesTable(nolock))
--order by sdn.EntNum

	
	select entnum,aliases from worldcheck(nolock) where entnum not in 
	(select entnum from TmpAliasesTable(nolock) ) and isnull(DelFlag,'') != 1
	and entnum in (select entnum from sdntable(nolock) where status !=4 and program !='other')
	 and isnull(aliases,'') != '' and rtrim(aliases) !=''
	order by entnum 



open cur;
fetch next from cur into @entnum,@Aliases;
WHILE @@fetch_status = 0 
begin

	-- get aliases
	set @Aliases_tmp = @Aliases;
	WHILE CHARINDEX(';',@Aliases_tmp) > 0
		begin
			-- part1
			set @location_2 = SUBSTRING(@Aliases_tmp, 1, CHARINDEX(';', @Aliases_tmp) - 1);
			set @Aliases_tmp = SUBSTRING(@Aliases_tmp, CHARINDEX(';', @Aliases_tmp) + 1, LEN(@Aliases_tmp) - CHARINDEX(';', @Aliases_tmp));

			--print @location_2

			/*--insert to table*/
			insert into TmpAliasesTable(EntNum,alianum,AltName,aliatype)
			values (@entnum, @maxalianum,@location_2,'aliases')
			set @maxalianum = @maxalianum + 1;
		end
		--check for the tail string
		if len(@Aliases_tmp) > 0
		begin
			set @location_2 = @Aliases_tmp;
			insert into TmpAliasesTable(EntNum,alianum,AltName,aliatype)
			values (@entnum, @maxalianum,@location_2,'aliases')
			set @maxalianum = @maxalianum + 1;
		end
		--print @location_2
  fetch next from cur into @entnum,@Aliases;
end
close cur;
DEALLOCATE cur;

--Oct.1.2024 3: 調整更新SDNAltTable狀態為4、remark更新為Expired AKAs 的SQL: 條件為撈取TmpAliasesTable沒有的別名資料，且SDNAltTable有殘留別名的資料。
--begin tran
update SDNAltTable set Status = 4, Remarks = 'Expired AKAs', LastModifDate = getdate()
from (
	select s.EntNum, s.AltName from (
		(select EntNum, AltName from TmpAliasesTable(nolock)) t
	right join 
		(select EntNum, AltName from SDNAltTable(nolock)
		where EntNum >= 20000000 and (status !=4 or deleted = 1)
		and isnull(listid,'') in ('alt','') and isnull(AltType,'') = 'a.k.a.' and entnum in 
		(select entnum from sdntable(nolock) where program != 'other' and status !=4)
		) s on t.EntNum = s.EntNum and t.AltName = s.AltName
	) where t.EntNum is null
)X where SDNAltTable.EntNum = X.EntNum and SDNAltTable.AltName = X.AltName
and SDNAltTable.Status != 4 and isnull(SDNAltTable.Remarks,'') != 'Expired AKAs'

--rollback
update TmpAliasesTable
set aliatype = 'userblocked'
where AltName in( 'CASE', 'IND', 'District', 'via', 'acct', 'add', 'bbk',
 'MAC', 'SAC', 'PAC', 'INS', 'CRED', 'FEE', 'BNF', 'FAC', 'ACT', 'AKA', 'HKMA', 'USD', 'INC', 'ACC', 'BIC', 'SSI',
'UPD', 'GU', 'YORK', 'APR', 'MIO', 'MEI')
and aliatype != 'userblocked'

--commit

delete SDNAltTable where EntNum > 20000000 and LastModifDate < getdate() -30 and status = 4 and deleted != 1
