USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_HRCtyInMLE]    Script Date: 8/1/2024 9:57:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USR_HRCtyInMLE](@WLCode SCode, @testAlert INT,
	@period varchar(25),
	@startDate varchar(10),
	@endDate varchar(10),  
    @minIndCount int,
	@minIndAmt money, 
    @maxIndAmt money,
	@minSumAmount money,
	@maxSumAmount money,
	@riskClassList varchar(8000),
	@customerTypeList varchar(8000),
	@accountTypeList varchar(8000),
	@activityTypeList varchar(8000),
    @countryList varchar(8000),
	@UseMonitor smallint,
	@recvpay INT,
	@branchList varchar(8000),
    @deptList varchar(8000),
	@cashType smallint,
	@ExcludeTranbyCustCtryCode smallint,
	@IncludeAddressFields smallint,
	@Useround smallInt,
	@Precision smallInt,
	@UseRelatedParties smallInt,                  --20210420 add
	@IncludeRelationshipTypeList varchar(8000),   --20210420 add
	@ExcludeRelationshipTypeList varchar(8000)    --20210420 add
	)

AS
/* RULE AND PARAMETER DESCRIPTION
Detects a single Originator sending transactions to one or more 
Beneficiaries. A transaction is included in the evaluation based on its 
qualification with: specified transaction types, a specified time period,
the amount exceeds a certain amount, and the cumulative amount of all the
transactions meeting these constraints is over a specified amount.  
Additional constraints include Country, Customer Type, Risk Class, 
Account Types, Cash/Non-Cash, Branch and Department. The alert/case
will display the account information in the description since the account
may not be held at the processing institution. Designed to be run Post-EOD.

	@period = Specifies the previous period accordingly. It Can be left blank,
		Week, Month, Quarter and Semi Annual. If blank the StartDate and EndDate 
  		must be specified. 
	@startDate = Specifies the Starting Date range of transactions to evaluate,  
    		uses the book date. Relevant only if the Period is not specified.
	@endDate = Specifies the Ending Date range of the transactionAs book date.
		Relevant only if the Period is not specified.
        @minIndCount = The Minimum individual transaction count, after applying 
		the Min-Ind-Amt and Max-Ind-Amt parameters
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
   	@maxIndAmt = The Maximum amount the individual transaction cannot exceed 
		to be included in the aggregation.  For no maximum use -1
	@minSumAmount = The Minimum aggregated transaction amount that must be 
		exceeded.  This includes those transactions that are between Min-Ind-Amt
		and Max-Ind-Amt.
	@maxSumAmount = The Maximum aggregated transaction amount that cannot be 
		exceeded. For no maximum use -1
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@customerTypeList = A comma separated list of Customer Types to include 
		in the evaluation, use -ALL- for any customer type.
	@accountTypeList = A comma separated list of Account Types to include 
		in the evaluation, use -ALL- for any account type.
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.
        @countryList = A comma separated list of Countries to include in the 
		evaluation, use -ALL- for any country. Fields evaluated are 
		BeneCountry, BeneBankCountry, ByOrderCountry, ByOrderBankCountry, 
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3 and
		IntermediaryCountry4
	@UseMonitor = Use list of countries from Country table where the "Monitor" flag 
		is set to true instead of country list above (overrides country list).
	@recvpay = Receive or Pay. 1 for Receive, 2 for Payment, 3 for Both
	@branchList = A comma separated list of Branches to include in the evaluation.
	@deptList = A comma separated list of Departments to include in 
		the evaluation.
	@cashType = Specifies Cash or non-Cash. 1 for Cash, 0 for NonCash, 2 for both
	@ExcludeTranbyCustCtryCode = Exclude all the transactions that produce hit when 
		the country on the transaction is the same as the country of the customer.
		0 for Do not exclude, 
		1 for Exclude if customer country matches, 
		2 for Exclude if any customer country matches" Value="0"/>
	@IncludeAddressFields = Include Address Fields in search to match on country.
					0 - for Do not Include
					1 - for Include
	@UseRound = Indicate if only the high round amounts have to considered.
					0 - Do not round
					1 - Round
	@Precision = Precision for rounding. Specify 3 for 1000
	--2021, MAY. add for HK requirement
	@UseRelatedParties =  Include Related Parties (1=yes, 0=no)   
	@IncludeRelationshipTypeList
	          use -ALL- for any
	@ExcludeRelationshipTypeList
	         use -NONE- for any
	2020.10 modify for LN run on every eod
*/
	
/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@fromDate INT,
 	@toDate INT,
	@tranAmt MONEY,
	@tranCnt INT,
	@trnCnt INT
DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME

DECLARE @SetDate DATETIME
DECLARE @Countries varchar(2000)
DECLARE @OrigCountryList varchar(8000)
DECLARE @Accounts varchar(2000)
DECLARE @Country VARCHAR(200)
DECLARE @pos INT
DECLARE @tmp INT

set @tmp = power(10, @precision)

Declare @TT TABLE(
	BaseAmt MONEY,
	tranNo int,
	Cust VARCHAR(40),
	Account VARCHAR(40),
	benecountry VARCHAR(35), 
	beneBankcountry VARCHAR(35), 
	ByOrderCountry VARCHAR(35), 
	ByOrderBankCountry VARCHAR(35),
	IntermediaryCountry VARCHAR(35),
	IntermediaryCountry2 VARCHAR(35),
	IntermediaryCountry3 VARCHAR(35),
	IntermediaryCountry4 VARCHAR(35)	
)

Declare @TempAddrTable TABLE(
	BaseAmt MONEY,
	tranNo int,
	Cust VARCHAR(40),
	Account VARCHAR(40),
	benecountry VARCHAR(35), 
	beneBankcountry VARCHAR(35), 
	ByOrderCountry VARCHAR(35), 
	ByOrderBankCountry VARCHAR(35),
	IntermediaryCountry VARCHAR(35),
	IntermediaryCountry2 VARCHAR(35),
	IntermediaryCountry3 VARCHAR(35),
	IntermediaryCountry4 VARCHAR(35)	,
	BeneAddress	VARCHAR(120),
	BeneBankAddress VARCHAR(120),
	IntermediaryAddress VARCHAR(120),
	ByOrderAddress VARCHAR(120),
	ByOrderBankAddress VARCHAR(120),
	IntermediaryAddress2 VARCHAR(120),
	IntermediaryAddress3 VARCHAR(120),
	IntermediaryAddress4 VARCHAR(120)
)

Declare @TT1 TABLE(
	Cust VARCHAR(40),
	Account VARCHAR(40),
	country VARCHAR(35)	
)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_HRCtyInMLE
ELSE
	-- Already in a transaction
	SAVE TRAN USR_HRCtyInMLE
/* End standard stored procedure transaction header */

/*  standard Rules Header */
-- Date options

--SET @SetDate = GetDate()

-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @description = [Desc], @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList (NOLOCK)
WHERE WLCode = @WLCode

Declare @BaseCurr char(3)
select @BaseCurr = IsNULL(BaseCurr,'') from SysParam

If(@period is null OR  ltrim(rtrim(@period)) = '') 
BEGIN
	SET @fromDate = dbo.ConvertSqlDateToInt(@startDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@endDate)
END
ELSE
BEGIN
	DECLARE @SQLStartDate datetime, @SQLEndDate datetime
	exec dbo.BSA_GetDateRange @SetDate, @period, 'PREV', 1, 
			@SQLStartDate OUTPUT, @SQLEndDate OUTPUT
	SET @fromDate = dbo.ConvertSqlDateToInt(@SQLStartDate)
	SET @toDate = dbo.ConvertSqlDateToInt(@SQLEndDate)

	--2020.10 change to calculate the previous date
	set @fromDate = dbo.ConvertSqlDateToInt(DATEADD(MONTH,-1, @SetDate));
	set @toDate = dbo.ConvertSqlDateToInt(DATEADD(DAY,-1, @SetDate));
END

	SELECT @riskClassList = dbo.BSA_fnListParams(@riskClassList)
	SELECT @customerTypeList = dbo.BSA_fnListParams(@customerTypeList)
	SELECT @accountTypeList = dbo.BSA_fnListParams(@accountTypeList)
	SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)
	SELECT @OrigCountryList = @countryList
	SELECT @countryList = dbo.BSA_fnListParams(@countryList)
	IF @UseMonitor = 1
	BEGIN
		SET @countryList = ''
		SELECT @countryList = COALESCE(@countryList + ',', '') 
				+ rtrim(code)
 			FROM Country 
			WHERE Monitor = 1

		IF LEN(LTRIM(RTRIM(@countryList)))> 0
			SELECT @countryList=@countryList+','
		ELSE
			SELECT @countryList = 'NONE'
	END

	SELECT @branchList = dbo.BSA_fnListParams(@branchList)
	SELECT @deptList = dbo.BSA_fnListParams(@deptList)

IF (@cashType = 2)
	SET @cashType = NULL

IF (@recvpay = 3)
	SET @recvpay = NULL
-- To include Address Fields in the search criteria


IF (@IncludeAddressFields) = 1
BEGIN
	SET @pos = 1  
		WHILE (@pos <  LEN(@countryList)   OR  ISNULL(@countrylist,'') = '') 
        BEGIN
			
			SELECT @Country = LTRIM(RTRIM(SUBSTRING(@countryList,@pos,CHARINDEX(',',@countryList, @pos)- @pos)))  
			IF (LEN(LTRIM(RTRIM(@Country))) >0 Or ISNULL(@countrylist,'') = '')  
				INSERT INTO @TempAddrTable (BaseAmt, tranNo, Cust, Account, benecountry,
					beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
					IntermediaryCountry, IntermediaryCountry2, 
					IntermediaryCountry3, IntermediaryCountry4,BeneAddress,
					BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
					IntermediaryAddress2,IntermediaryAddress3 ,
					IntermediaryAddress4)
				SELECT BaseAmt, tranNo, a.Cust, a.account,
				benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
				IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
				IntermediaryCountry4,BeneAddress,BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
				IntermediaryAddress2,IntermediaryAddress3 ,IntermediaryAddress4
				FROM ActivityHist a (NOLOCK) JOIN Customer (NOLOCK) ON a.cust = Customer.Id
				LEFT JOIN Account ac (NOLOCK) ON a.Account = ac.Id 
				LEFT JOIN Customer c1 (NOLOCK) ON a.BeneCustId = c1.Id 
				LEFT JOIN Customer c2 (NOLOCK) ON a.ByOrderCustId = c2.Id 
				WHERE a.bookdate >= @fromDate AND 
					a.bookdate <= @toDate AND 
					a.recvpay = ISNULL(@recvPay, a.recvpay) AND
					a.BaseAmt >= @minIndAmt AND
					(@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
					(ISNULL(@deptList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.dept,''))) + ',', @deptList) > 0)
					AND (ISNULL(@branchList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.branch,''))) + ',', @branchList) > 0)
					AND (ISNULL(@activityTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.type,''))) + ',', @activityTypeList) > 0)
					AND (ISNULL(@accountTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ac.type,''))) + ',', @accountTypeList) > 0)
					AND (ISNULL(@riskClassList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Customer.RiskClass,''))) + ',', @riskClassList) > 0)
					AND	(ISNULL(@customerTypeList,'') = '' OR 
					(CHARINDEX(',E,', @customerTypeList) > 0 AND
					(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c1.type,''))) + ',', ',E,') > 0 OR
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c2.type,''))) + ',', ',E,') > 0)) OR
					(CHARINDEX(',I,', @customerTypeList) > 0 AND
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c1.type,''))) + ',', ',I,') > 0 AND
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c2.type,''))) + ',', ',I,') > 0))
					AND	(CASHTRAN = ISNULL(@cashType, CASHTRAN))
					AND 1 = 
					Case When @UseRound = 1 and (a.baseamt%@tmp = 0) Then 1 
					     When @UseRound = 0 Then 1
					     When @UseRound = 1 and (a.baseamt%@tmp <> 0) Then 0
					Else 1 End
					AND 
					( 
						(ISNULL(@countryList,'') = '') 	
						OR 
 							( CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.BeneBankAddress) > 0 
		   
																			 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.IntermediaryAddress) > 0 
 							OR 
																		   
		   
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.ByOrderBankAddress) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.IntermediaryAddress2) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.IntermediaryAddress3) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) ,a.IntermediaryAddress4 ) > 0 
							)
						OR
																						   
							(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.BeneBankCountry,''))) + ',', @countryList) > 0 OR
																							 
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry4,''))) + ',', @countryList) > 0 
							) 
					)
					and a.Cust in 
					(select cust from Activity act_curr(nolock) join Customer c_curr(nolock) on act_curr.Cust = c_curr.Id
						where act_curr.recvpay = ISNULL(@recvPay, act_curr.recvpay)
						AND act_curr.BaseAmt >= @minIndAmt 
						AND (@maxIndAmt = -1 OR act_curr.BaseAmt <= @maxIndAmt)
						AND (ISNULL(@activityTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.type,''))) + ',', @activityTypeList) > 0)
						AND (ISNULL(@riskClassList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c_curr.RiskClass,''))) + ',', @riskClassList) > 0)
						AND	(ISNULL(@customerTypeList,'') = '' OR 
					(CHARINDEX(',E,', @customerTypeList) > 0 AND
					(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c_curr.type,''))) + ',', ',E,') > 0) OR
					(CHARINDEX(',I,', @customerTypeList) > 0 AND
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c_curr.type,''))) + ',', ',I,') > 0)))
						AND	(act_curr.CASHTRAN = ISNULL(@cashType, act_curr.CASHTRAN))
						AND 1 = 
					Case When @UseRound = 1 and (act_curr.baseamt%@tmp = 0) Then 1 
					     When @UseRound = 0 Then 1
					     When @UseRound = 1 and (act_curr.baseamt%@tmp <> 0) Then 0
					Else 1 End
						AND 
					( 
						(ISNULL(@countryList,'') = '') 	
						OR 
 							( CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.BeneBankAddress) > 0 
		   
																					
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress) > 0 
 							OR 
																				  
		   
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.ByOrderBankAddress) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress2) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress3) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) ,act_curr.IntermediaryAddress4 ) > 0 
							)
						OR
							(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.BeneBankCountry,''))) + ',', @countryList) > 0 OR
																									
																									
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry4,''))) + ',', @countryList) > 0 
							) 
					)
					)
				union
				select BaseAmt, tranNo, act_curr.Cust, act_curr.account,
				benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
				IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
				IntermediaryCountry4,BeneAddress,BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
				IntermediaryAddress2,IntermediaryAddress3 ,IntermediaryAddress4 
				from Activity act_curr(nolock) join Customer c_curr(nolock) on act_curr.Cust = c_curr.Id
						where act_curr.recvpay = ISNULL(@recvPay, act_curr.recvpay)
						AND act_curr.BaseAmt >= @minIndAmt 
						AND (@maxIndAmt = -1 OR act_curr.BaseAmt <= @maxIndAmt)
						AND (ISNULL(@activityTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.type,''))) + ',', @activityTypeList) > 0)
						AND (ISNULL(@riskClassList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c_curr.RiskClass,''))) + ',', @riskClassList) > 0)
						AND	(ISNULL(@customerTypeList,'') = '' OR 
					(CHARINDEX(',E,', @customerTypeList) > 0 AND
					(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c_curr.type,''))) + ',', ',E,') > 0) OR
					(CHARINDEX(',I,', @customerTypeList) > 0 AND
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c_curr.type,''))) + ',', ',I,') > 0)))
						AND	(act_curr.CASHTRAN = ISNULL(@cashType, act_curr.CASHTRAN))
						AND 1 = 
					Case When @UseRound = 1 and (act_curr.baseamt%@tmp = 0) Then 1 
					     When @UseRound = 0 Then 1
					     When @UseRound = 1 and (act_curr.baseamt%@tmp <> 0) Then 0
					Else 1 End
						AND 
					( 
						(ISNULL(@countryList,'') = '') 	
						OR 
 							( CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.BeneBankAddress) > 0 
		   
																					
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress) > 0 
 							OR 
																				  
		   
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.ByOrderBankAddress) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress2) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress3) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) ,act_curr.IntermediaryAddress4 ) > 0 
							)
						OR
							(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.BeneBankCountry,''))) + ',', @countryList) > 0 OR
																									
																									
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry4,''))) + ',', @countryList) > 0 
							) 
				)
--begin  2021, MAY. HK requirement add party relation 
		if (isnull(@UseRelatedParties,0) = 1)
		begin
				INSERT INTO @TempAddrTable (BaseAmt, tranNo, Cust, Account, benecountry,
					beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
					IntermediaryCountry, IntermediaryCountry2, 
					IntermediaryCountry3, IntermediaryCountry4,BeneAddress,
					BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
					IntermediaryAddress2,IntermediaryAddress3 ,
					IntermediaryAddress4)
				SELECT BaseAmt, tranNo, pr.RelatedParty, a.account,
				benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
				IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
				IntermediaryCountry4,BeneAddress,BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
				IntermediaryAddress2,IntermediaryAddress3 ,IntermediaryAddress4
				FROM ActivityHist a (NOLOCK) 
				INNER	JOIN PartyRelation pr WITH (NOLOCK) ON a.Cust = pr.PartyID
				INNER	JOIN Customer RelCust(NOLOCK) ON RelCust.id = pr.RelatedParty
				LEFT JOIN Account ac (NOLOCK) ON a.Account = ac.Id 
				LEFT JOIN Customer c1 (NOLOCK) ON a.BeneCustId = c1.Id 
				LEFT JOIN Customer c2 (NOLOCK) ON a.ByOrderCustId = c2.Id 
				
				WHERE a.bookdate >= @fromDate AND 
					a.bookdate <= @toDate AND 
					a.recvpay = ISNULL(@recvPay, a.recvpay) AND
					a.BaseAmt >= @minIndAmt AND
					(@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
					(ISNULL(@deptList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.dept,''))) + ',', @deptList) > 0)
					AND (ISNULL(@branchList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.branch,''))) + ',', @branchList) > 0)
					AND (ISNULL(@activityTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.type,''))) + ',', @activityTypeList) > 0)
					AND (ISNULL(@accountTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ac.type,''))) + ',', @accountTypeList) > 0)
					AND (ISNULL(@riskClassList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.RiskClass,''))) + ',', @riskClassList) > 0)
					AND	(ISNULL(@customerTypeList,'') = '' OR 
					(CHARINDEX(',E,', @customerTypeList) > 0 AND
					(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c1.type,''))) + ',', ',E,') > 0 OR
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c2.type,''))) + ',', ',E,') > 0)) OR
					(CHARINDEX(',I,', @customerTypeList) > 0 AND
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c1.type,''))) + ',', ',I,') > 0 AND
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c2.type,''))) + ',', ',I,') > 0))
					AND	(CASHTRAN = ISNULL(@cashType, CASHTRAN))
					AND 1 = 
					Case When @UseRound = 1 and (a.baseamt%@tmp = 0) Then 1 
					     When @UseRound = 0 Then 1
					     When @UseRound = 1 and (a.baseamt%@tmp <> 0) Then 0
					Else 1 End
					AND 
					( 
						(ISNULL(@countryList,'') = '') 	
						OR 
 							( CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.BeneBankAddress) > 0 
 							OR 
																			 
		   
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.IntermediaryAddress) > 0 
 							OR 
																		   
		   
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.ByOrderBankAddress) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.IntermediaryAddress2) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , a.IntermediaryAddress3) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) ,a.IntermediaryAddress4 ) > 0 
							)
						OR
																						   
							(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.BeneBankCountry,''))) + ',', @countryList) > 0 OR
																							 
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry4,''))) + ',', @countryList) > 0 
							) 
					)
					and a.Cust in 
					(select pr.RelatedParty from Activity act_curr(nolock) 
					inner join PartyRelation pr (nolock) on act_curr.Cust = pr.PartyId
					inner join Customer RelCust(nolock) on RelCust.id = pr.RelatedParty
						where act_curr.recvpay = ISNULL(@recvPay, act_curr.recvpay)
						AND act_curr.BaseAmt >= @minIndAmt 
						AND (@maxIndAmt = -1 OR act_curr.BaseAmt <= @maxIndAmt)
						AND (ISNULL(@activityTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.type,''))) + ',', @activityTypeList) > 0)
						AND (ISNULL(@riskClassList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.RiskClass,''))) + ',', @riskClassList) > 0)
						AND	(ISNULL(@customerTypeList,'') = '' OR 
					(CHARINDEX(',E,', @customerTypeList) > 0 AND
					(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.type,''))) + ',', ',E,') > 0) OR
					(CHARINDEX(',I,', @customerTypeList) > 0 AND
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.type,''))) + ',', ',I,') > 0)))
						AND	(act_curr.CASHTRAN = ISNULL(@cashType, act_curr.CASHTRAN))
						AND 1 = 
					Case When @UseRound = 1 and (act_curr.baseamt%@tmp = 0) Then 1 
					     When @UseRound = 0 Then 1
					     When @UseRound = 1 and (act_curr.baseamt%@tmp <> 0) Then 0
					Else 1 End
						AND 
					( 
						(ISNULL(@countryList,'') = '') 	
						OR 
 							( CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.BeneBankAddress) > 0 
 							OR 
																					
		   
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress) > 0 
 							OR 
																				  
		   
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.ByOrderBankAddress) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress2) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress3) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) ,act_curr.IntermediaryAddress4 ) > 0 
							)
						OR
							(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.BeneCountry,''))) + ',', @countryList) > 0 OR 
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.BeneBankCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.ByOrderCountry,''))) + ',', @countryList) > 0  OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry4,''))) + ',', @countryList) > 0 
							) 
					)
					)
				union
				select BaseAmt, tranNo, pr.RelatedParty, act_curr.account,
				benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
				IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
				IntermediaryCountry4,BeneAddress,BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
				IntermediaryAddress2,IntermediaryAddress3 ,IntermediaryAddress4 
				from Activity act_curr(nolock) 
				inner join PartyRelation pr (nolock) on act_curr.Cust = pr.PartyId
				INNER	JOIN Customer RelCust(NOLOCK) ON RelCust.id = pr.RelatedParty
				LEFT JOIN Customer c1 (NOLOCK) ON act_curr.BeneCustId = c1.Id 
				LEFT JOIN Customer c2 (NOLOCK) ON act_curr.ByOrderCustId = c2.Id 
						where act_curr.recvpay = ISNULL(@recvPay, act_curr.recvpay)
						AND act_curr.BaseAmt >= @minIndAmt 
						AND (@maxIndAmt = -1 OR act_curr.BaseAmt <= @maxIndAmt)
						AND (ISNULL(@activityTypeList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.type,''))) + ',', @activityTypeList) > 0)
						AND (ISNULL(@riskClassList,'') = '' OR 
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.RiskClass,''))) + ',', @riskClassList) > 0)
						AND	(ISNULL(@customerTypeList,'') = '' OR 
					(CHARINDEX(',E,', @customerTypeList) > 0 AND
					(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c1.type,''))) + ',', ',E,') > 0 OR
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c2.type,''))) + ',', ',E,') > 0)) OR
					(CHARINDEX(',I,', @customerTypeList) > 0 AND
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c1.type,''))) + ',', ',I,') > 0 AND
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c2.type,''))) + ',', ',I,') > 0))
						AND	(act_curr.CASHTRAN = ISNULL(@cashType, act_curr.CASHTRAN))
						AND 1 = 
					Case When @UseRound = 1 and (act_curr.baseamt%@tmp = 0) Then 1 
					     When @UseRound = 0 Then 1
					     When @UseRound = 1 and (act_curr.baseamt%@tmp <> 0) Then 0
					Else 1 End
						AND 
					( 
						(ISNULL(@countryList,'') = '') 	
						OR 
 							( CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.BeneBankAddress) > 0 
		   
																					
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress) > 0 
 							OR 
																				  
		   
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.ByOrderBankAddress) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress2) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress3) > 0 
 							OR 
 							CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) ,act_curr.IntermediaryAddress4 ) > 0 
							)
						OR
							(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.BeneCountry,''))) + ',', @countryList) > 0 OR 
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.BeneBankCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.ByOrderCountry,''))) + ',', @countryList) > 0  OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
							CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry4,''))) + ',', @countryList) > 0 
							) 
				)
		end
--end 2021, MAY. HK requirement add party relation 
					IF ISNULL(@countrylist,'') = '' 
						BREAK
					ELSE 
						BEGIN
 						-- Get the next country code	
 						SELECT @pos = CHARINDEX(',',@countryList, @pos)+1
						END
        END
END
-- do not include address fields in the search criteria
ELSE
BEGIN
	INSERT INTO @TempAddrTable (BaseAmt, tranNo, Cust, Account, benecountry,
			beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
			IntermediaryCountry, IntermediaryCountry2, 
			IntermediaryCountry3, IntermediaryCountry4,BeneAddress,
			BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
			IntermediaryAddress2,IntermediaryAddress3 ,
			IntermediaryAddress4)	
		SELECT BaseAmt, tranNo, a.Cust, a.account,
		benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
		IntermediaryCountry4,BeneAddress,BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
		IntermediaryAddress2,IntermediaryAddress3 ,IntermediaryAddress4
	FROM ActivityHist a (NOLOCK) JOIN Customer (NOLOCK) on a.cust = Customer.Id
     LEFT JOIN Account ac (NOLOCK) ON a.Account = ac.Id 
	 LEFT JOIN Customer c1 (NOLOCK) ON a.BeneCustId = c1.Id 
	 LEFT JOIN Customer c2 (NOLOCK) ON a.ByOrderCustId = c2.Id 
      WHERE a.bookdate >= @fromDate AND 
		a.bookdate <= @toDate AND 
		a.recvpay = ISNULL(@recvPay, a.recvpay) AND
		a.BaseAmt >= @minIndAmt AND
	    	(@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
		(ISNULL(@deptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.dept,''))) + ',', @deptList) > 0)
		AND (ISNULL(@branchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.branch,''))) + ',', @branchList) > 0)
		AND (ISNULL(@activityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.type,''))) + ',', @activityTypeList) > 0)
		AND (ISNULL(@accountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ac.type,''))) + ',', @accountTypeList) > 0)
		AND (ISNULL(@riskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Customer.RiskClass,''))) + ',', @riskClassList) > 0)
		AND	(ISNULL(@customerTypeList,'') = '' OR 
		(CHARINDEX(',E,', @customerTypeList) > 0 AND
		(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c1.type,''))) + ',', ',E,') > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c2.type,''))) + ',', ',E,') > 0)) OR
		(CHARINDEX(',I,', @customerTypeList) > 0 AND
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c1.type,''))) + ',', ',I,') > 0 AND
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c2.type,''))) + ',', ',I,') > 0))
		AND	(CASHTRAN = ISNULL(@cashType, CASHTRAN))
		AND(ISNULL(@countryList,'') = '' OR 
																					  
		(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.BeneBankCountry,''))) + ',', @countryList) > 0 OR
																						
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry4,''))) + ',', @countryList) > 0 ))
		AND 1 = 
		Case When @UseRound = 1 and (a.baseamt%@tmp = 0) Then 1 
		     When @UseRound = 0 Then 1
		     When @UseRound = 1 and (a.baseamt%@tmp <> 0) Then 0
		Else 1 End
		and a.Cust in 
		(select cust from Activity act_curr(nolock) join Customer c_curr(nolock) on act_curr.Cust = c_curr.Id
			where act_curr.recvpay = ISNULL(@recvPay, act_curr.recvpay) 
			AND act_curr.BaseAmt >= @minIndAmt 
			AND (@maxIndAmt = -1 OR act_curr.BaseAmt <= @maxIndAmt)
			AND (ISNULL(@activityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.type,''))) + ',', @activityTypeList) > 0)
			AND (ISNULL(@riskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c_curr.RiskClass,''))) + ',', @riskClassList) > 0)
			AND	(ISNULL(@customerTypeList,'') = '' OR 
		(CHARINDEX(',E,', @customerTypeList) > 0 AND
		(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c_curr.type,''))) + ',', ',E,') > 0) OR
		(CHARINDEX(',I,', @customerTypeList) > 0 AND
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c_curr.type,''))) + ',', ',I,') > 0)))
			AND	(act_curr.CASHTRAN = ISNULL(@cashType, act_curr.CASHTRAN))
			AND 1 = 
		Case When @UseRound = 1 and (act_curr.baseamt%@tmp = 0) Then 1 
				When @UseRound = 0 Then 1
				When @UseRound = 1 and (act_curr.baseamt%@tmp <> 0) Then 0
		Else 1 End
			AND 
		( 
			(ISNULL(@countryList,'') = '') 	
			OR
				(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.BeneBankCountry,''))) + ',', @countryList) > 0 OR
																								 
																								 
				CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
				CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
				CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
				CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
				CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry4,''))) + ',', @countryList) > 0 
				) 
		)
		)
		union
		select BaseAmt, tranNo, act_curr.Cust, act_curr.account,
		benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
		IntermediaryCountry4,BeneAddress,BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
		IntermediaryAddress2,IntermediaryAddress3 ,IntermediaryAddress4 
		from Activity act_curr(nolock) join Customer c_curr(nolock) on act_curr.Cust = c_curr.Id
				where act_curr.recvpay = ISNULL(@recvPay, act_curr.recvpay)
				AND act_curr.BaseAmt >= @minIndAmt 
				AND (@maxIndAmt = -1 OR act_curr.BaseAmt <= @maxIndAmt)
				AND (ISNULL(@activityTypeList,'') = '' OR 
			CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.type,''))) + ',', @activityTypeList) > 0)
				AND (ISNULL(@riskClassList,'') = '' OR 
			CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c_curr.RiskClass,''))) + ',', @riskClassList) > 0)
				AND	(ISNULL(@customerTypeList,'') = '' OR 
			(CHARINDEX(',E,', @customerTypeList) > 0 AND
			(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c_curr.type,''))) + ',', ',E,') > 0) OR
			(CHARINDEX(',I,', @customerTypeList) > 0 AND
			CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c_curr.type,''))) + ',', ',I,') > 0)))
				AND	(act_curr.CASHTRAN = ISNULL(@cashType, act_curr.CASHTRAN))
				AND 1 = 
			Case When @UseRound = 1 and (act_curr.baseamt%@tmp = 0) Then 1 
					When @UseRound = 0 Then 1
					When @UseRound = 1 and (act_curr.baseamt%@tmp <> 0) Then 0
			Else 1 End
				AND 
			( 
				(ISNULL(@countryList,'') = '') 	
				OR 
 					( CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.BeneBankAddress) > 0 
		 
																				  
 					OR 
 					CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress) > 0 
 					OR 
																				
		 
 					CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.ByOrderBankAddress) > 0 
 					OR 
 					CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress2) > 0 
 					OR 
 					CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress3) > 0 
 					OR 
 					CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) ,act_curr.IntermediaryAddress4 ) > 0 
					)
				OR
					(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.BeneBankCountry,''))) + ',', @countryList) > 0 OR
																								  
																								  
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry4,''))) + ',', @countryList) > 0 
					) 
		)

--begin 2021, MAY. HK requirement add party relation 
if (isnull(@UseRelatedParties,0) = 1)
begin
	INSERT INTO @TempAddrTable (BaseAmt, tranNo, Cust, Account, benecountry,
			beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
			IntermediaryCountry, IntermediaryCountry2, 
			IntermediaryCountry3, IntermediaryCountry4,BeneAddress,
			BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
			IntermediaryAddress2,IntermediaryAddress3 ,
			IntermediaryAddress4)	
		SELECT BaseAmt, tranNo, pr.RelatedParty, a.account,
		benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
		IntermediaryCountry4,BeneAddress,BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
		IntermediaryAddress2,IntermediaryAddress3 ,IntermediaryAddress4
	FROM ActivityHist a (NOLOCK) 
	INNER	JOIN PartyRelation pr WITH (NOLOCK) ON a.Cust = pr.PartyID
	INNER	JOIN Customer RelCust(NOLOCK) ON RelCust.id = pr.RelatedParty
     LEFT JOIN Account ac (NOLOCK) ON a.Account = ac.Id 
	 LEFT JOIN Customer c1 (NOLOCK) ON a.BeneCustId = c1.Id 
	 LEFT JOIN Customer c2 (NOLOCK) ON a.ByOrderCustId = c2.Id 
      WHERE a.bookdate >= @fromDate AND 
		a.bookdate <= @toDate AND 
		a.recvpay = ISNULL(@recvPay, a.recvpay) AND
		a.BaseAmt >= @minIndAmt AND
	    	(@maxIndAmt = -1 OR a.BaseAmt <= @maxIndAmt) AND
		(ISNULL(@deptList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.dept,''))) + ',', @deptList) > 0)
		AND (ISNULL(@branchList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.branch,''))) + ',', @branchList) > 0)
		AND (ISNULL(@activityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.type,''))) + ',', @activityTypeList) > 0)
		AND (ISNULL(@accountTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ac.type,''))) + ',', @accountTypeList) > 0)
		AND (ISNULL(@riskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.RiskClass,''))) + ',', @riskClassList) > 0)
		AND	(ISNULL(@customerTypeList,'') = '' OR 
		(CHARINDEX(',E,', @customerTypeList) > 0 AND
		(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c1.type,''))) + ',', ',E,') > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c2.type,''))) + ',', ',E,') > 0)) OR
		(CHARINDEX(',I,', @customerTypeList) > 0 AND
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c1.type,''))) + ',', ',I,') > 0 AND
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c2.type,''))) + ',', ',I,') > 0))
		AND	(CASHTRAN = ISNULL(@cashType, CASHTRAN))
		AND(ISNULL(@countryList,'') = '' OR 
																					  
		(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.BeneBankCountry,''))) + ',', @countryList) > 0 OR
																						
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(a.IntermediaryCountry4,''))) + ',', @countryList) > 0 ))
		AND 1 = 
		Case When @UseRound = 1 and (a.baseamt%@tmp = 0) Then 1 
		     When @UseRound = 0 Then 1
		     When @UseRound = 1 and (a.baseamt%@tmp <> 0) Then 0
		Else 1 End
		AND Exists (select * from @TempAddrTable tt Where tt.Cust = pr.RelatedParty)
		AND Not Exists (select * from @TempAddrTable tt Where tt.Cust = pr.RelatedParty AND tt.TranNo = a.tranno)
		and a.Cust in 
		(select cust from Activity act_curr(nolock) 
		inner join PartyRelation pr (nolock) on act_curr.Cust = pr.PartyId
		inner join Customer RelCust(nolock) on RelCust.id = pr.RelatedParty
			where act_curr.recvpay = ISNULL(@recvPay, act_curr.recvpay) 
			AND act_curr.BaseAmt >= @minIndAmt 
			AND (@maxIndAmt = -1 OR act_curr.BaseAmt <= @maxIndAmt)
			AND (ISNULL(@activityTypeList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.type,''))) + ',', @activityTypeList) > 0)
			AND (ISNULL(@riskClassList,'') = '' OR 
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.RiskClass,''))) + ',', @riskClassList) > 0)
			AND	(ISNULL(@customerTypeList,'') = '' OR 
		(CHARINDEX(',E,', @customerTypeList) > 0 AND
		(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.type,''))) + ',', ',E,') > 0) OR
		(CHARINDEX(',I,', @customerTypeList) > 0 AND
		CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.type,''))) + ',', ',I,') > 0)))
			AND	(act_curr.CASHTRAN = ISNULL(@cashType, act_curr.CASHTRAN))
			AND 1 = 
		Case When @UseRound = 1 and (act_curr.baseamt%@tmp = 0) Then 1 
				When @UseRound = 0 Then 1
				When @UseRound = 1 and (act_curr.baseamt%@tmp <> 0) Then 0
		Else 1 End
			AND 
		( 
			(ISNULL(@countryList,'') = '') 	
			OR
				(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.BeneBankCountry,''))) + ',', @countryList) > 0 OR
																								 
																								 
				CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
				CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
				CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
				CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
				CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry4,''))) + ',', @countryList) > 0 
				) 
		)
		)
		union
		select BaseAmt, tranNo, pr.RelatedParty, act_curr.account,
		benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
		IntermediaryCountry4,BeneAddress,BeneBankAddress,IntermediaryAddress,ByOrderAddress,ByOrderBankAddress ,
		IntermediaryAddress2,IntermediaryAddress3 ,IntermediaryAddress4 
		from Activity act_curr(nolock) 
		inner join PartyRelation pr (nolock) on act_curr.Cust = pr.PartyId
		INNER	JOIN Customer RelCust(NOLOCK) ON RelCust.id = pr.RelatedParty
		LEFT JOIN Customer c1 (NOLOCK) ON act_curr.BeneCustId = c1.Id 
		LEFT JOIN Customer c2 (NOLOCK) ON act_curr.ByOrderCustId = c2.Id 
				where act_curr.recvpay = ISNULL(@recvPay, act_curr.recvpay)
				AND act_curr.BaseAmt >= @minIndAmt 
				AND (@maxIndAmt = -1 OR act_curr.BaseAmt <= @maxIndAmt)
				AND (ISNULL(@activityTypeList,'') = '' OR 
			CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.type,''))) + ',', @activityTypeList) > 0)
				AND (ISNULL(@riskClassList,'') = '' OR 
			CHARINDEX(',' + LTRIM(RTRIM(ISNULL(RelCust.RiskClass,''))) + ',', @riskClassList) > 0)
				AND	(ISNULL(@customerTypeList,'') = '' OR 
					(CHARINDEX(',E,', @customerTypeList) > 0 AND
					(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c1.type,''))) + ',', ',E,') > 0 OR
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c2.type,''))) + ',', ',E,') > 0)) OR
					(CHARINDEX(',I,', @customerTypeList) > 0 AND
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c1.type,''))) + ',', ',I,') > 0 AND
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(c2.type,''))) + ',', ',I,') > 0))
				AND	(act_curr.CASHTRAN = ISNULL(@cashType, act_curr.CASHTRAN))
				AND 1 = 
			Case When @UseRound = 1 and (act_curr.baseamt%@tmp = 0) Then 1 
					When @UseRound = 0 Then 1
					When @UseRound = 1 and (act_curr.baseamt%@tmp <> 0) Then 0
			Else 1 End
			AND Exists (select * from @TempAddrTable tt Where tt.Cust = pr.RelatedParty)
			AND Not Exists (select * from @TempAddrTable tt Where tt.Cust = pr.RelatedParty AND tt.TranNo = act_curr.tranno)
				AND 
			( 
				(ISNULL(@countryList,'') = '') 	
				OR 
 					( CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.BeneBankAddress) > 0 
		 
																				  
 					OR 
 					CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress) > 0 
 					OR 
																				
		 
 					CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.ByOrderBankAddress) > 0 
 					OR 
 					CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress2) > 0 
 					OR 
 					CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) , act_curr.IntermediaryAddress3) > 0 
 					OR 
 					CHARINDEX(LTRIM(RTRIM(ISNULL(@Country,''))) ,act_curr.IntermediaryAddress4 ) > 0 
					)
				OR
					(CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.BeneBankCountry,''))) + ',', @countryList) > 0 OR
																								  
																								  
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.ByOrderBankCountry,''))) + ',', @countryList) > 0 OR
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry,''))) + ',', @countryList) > 0 OR
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry2,''))) + ',', @countryList) > 0 OR
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry3,''))) + ',', @countryList) > 0 OR
					CHARINDEX(',' + LTRIM(RTRIM(ISNULL(act_curr.IntermediaryCountry4,''))) + ',', @countryList) > 0 
					) 
		)
end
--end 2021, MAY. HK requirement add party relation 
END


IF (@ExcludeTranbyCustCtryCode in (0,1,2))
BEGIN
	INSERT INTO @TT (BaseAmt, tranNo, Cust, Account, benecountry,
			beneBankcountry, ByOrderCountry, ByOrderBankCountry ,
			IntermediaryCountry, IntermediaryCountry2, 
			IntermediaryCountry3, IntermediaryCountry4)	
		SELECT DISTINCT BaseAmt, tranNo, a.Cust, a.account, 
		benecountry, beneBankcountry, ByOrderCountry, ByOrderBankCountry,
		IntermediaryCountry, IntermediaryCountry2, IntermediaryCountry3,
		IntermediaryCountry4
FROM @TempAddrTable a INNER JOIN Customer C ON A.Cust = C.ID		
END
									  
	 
															  
														
											  
											   
													  
																   
																  
					  
															 
										  
														
															
														   
															 
																
																 
																 
															  
  
   
									  
	 
															  
														
											  
											   
													  
																   
																  
					  
															 
														
										 
														  
																				
																		  
																		   
																					
															
	  
	
														   
																				   
																			 
																			 
																					   
																
	  
	
														 
																				  
																			 
																			 
																					   
															   

	  
	
															 
																					  
																				
																				 
																						  
																   
	  
	
															   
																						
																				  
																				  
																							
																	
	  
	
															   
																						
																				  
																				  
																							
																	 
	  
	
															   
																						
																				  
																				  
																							
																	 
	   
	
															   
																						 
																				  
																				  
																							
																	 
	 
  
   
--Inserting to @TT1 for building the description
INSERT INTO @TT1(Cust, Account, Country)
	SELECT Cust, Account, benecountry
	FROM @TT 
	WHERE 	
		BeneCountry IS NOT NULL 
	UNION ALL 
	SELECT Cust, Account, beneBankcountry
	FROM @TT 
	WHERE 	
		beneBankcountry IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, IntermediaryCountry
	FROM @TT 
	WHERE 	
		IntermediaryCountry IS NOT NULL 
	UNION ALL 
	SELECT Cust, Account, ByOrderCountry
	FROM @TT 
	WHERE 	
		ByOrderCountry IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, ByOrderBankCountry
	FROM @TT 
	WHERE 	
		ByOrderBankCountry IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, IntermediaryCountry2
	FROM @TT 
	WHERE 	
		IntermediaryCountry2 IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, IntermediaryCountry3
	FROM @TT 
	WHERE 	
		IntermediaryCountry3 IS NOT NULL
	UNION ALL 
	SELECT Cust, Account, IntermediaryCountry4
	FROM @TT 
	WHERE 	
		IntermediaryCountry4 is NOT NULL 
	UNION ALL 
	SELECT Cust, Account, NULL
	FROM @TT 
	WHERE 	
		IsNull(@CountryList, '') = ''


DECLARE	@cur CURSOR
DECLARE @Cust LONGNAME,
	@bookdate INT

--2017.12 mdy bookdate to firstdate of month by LN Request
--SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())
SET @bookdate = dbo.ConvertSqlDateToInt(@SetDate)

SET @cur = CURSOR FAST_FORWARD FOR 
SELECT DISTINCT SUM(BaseAmt) tranAmt, COUNT(tranNo) tranCnt, a.Cust
FROM @TT a 
GROUP BY  a.Cust
HAVING (SUM(BaseAmt) >= @minSumAmount AND 
	(@maxSumAmount = -1 OR SUM(BaseAmt) <= @maxSumAmount) AND
	count(tranno) >= @minIndCount)

OPEN @cur 
FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @Cust

WHILE @@FETCH_STATUS = 0 BEGIN

	-- Getting all the country field list
	set @Countries = ''
	set @Accounts = ''
	
	--Making the list(in case of -ALL-), Intersection list(in case of given country list)
	select @Countries = COALESCE(@Countries + ', ', '') 
			+ CONVERT(VARCHAR(50), Country)
			From @TT1 where Cust = @Cust and
			(@countryList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Country) + ',',@countryList) > 0)
        and Country is not null
			Group by Country
	select @Accounts = COALESCE(@Accounts + ', ', '')
		+ CONVERT(VARCHAR(50), Account)
		From @TT1 where Cust = @Cust
        and Account is not null
		Group by Account
	--Removing the first comma and space
	If(len(@Countries) > 0)
	Begin
		SET @Countries = substring(@Countries,3,len(@Countries))
	End	
	If(len(@Accounts) > 0)
	Begin
		SET @Accounts = substring(@Accounts,3,len(@Accounts))
	End		
	
	IF (@IncludeAddressFields) = 1
	BEGIN
			 --@OrigCountryList is the original countrylist parameter  without extra commas
			SET @desc = 'Customer ' + @Cust 
			+ ' is doing transactions which pass through the specified countries '
			+ ISNULL(@OrigCountryList,'') 
			+ ' in either the country or the address fields '
			+ CASE WHEN ISNULL(@Accounts,'') = '' THEN '' 
			  ELSE ' using the account(s) ' + ISNULL(@Accounts,'') END
			+ ' and the total amount is: ' + CONVERT(VARCHAR, @tranAmt) + ' over '
			+ CONVERT(VARCHAR, @tranCnt) + ' transactions '
			+ 'for a period from ' 
			+ Cast(dbo.BSA_ConvertIntToSqlDate(@fromDate, 'mm/dd/yyyy') AS Char(11)) 
			+ ' to ' 
			+ Cast(dbo.BSA_ConvertIntToSqlDate(@toDate, 'mm/dd/yyyy') AS Char(11))
			
	END
	ELSE 
	BEGIN
		SET @desc = 'Customer ' + @Cust 
			+ ' is doing transactions which pass through the country(s) ' 
			+ ISNULL(@Countries,'') 
			+ CASE WHEN ISNULL(@Accounts,'') = '' THEN '' 
			  ELSE ' using the account(s) ' + ISNULL(@Accounts,'') END
			+ ' and the total amount is: ' + CONVERT(VARCHAR, @tranAmt) + ' over '
			+ CONVERT(VARCHAR, @tranCnt) + ' transactions '
			+ 'for a period from ' 
			+ Cast(dbo.BSA_ConvertIntToSqlDate(@fromDate, 'mm/dd/yyyy') AS Char(11)) 
			+ ' to ' 
			+ Cast(dbo.BSA_ConvertIntToSqlDate(@toDate, 'mm/dd/yyyy') AS Char(11))
	END 
	IF @testAlert = 1
	BEGIN
		EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
		@Cust, NULL, 1
		IF @stat <> 0 GOTO EndOfProc
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 
		BEGIN
			EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
				@Cust, NULL, 0
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE 
		IF @WLTYPE = 1 
		BEGIN
			EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT, 
				@WLCode, @desc, @bookdate, @Cust, NULL
			IF @stat <> 0 GOTO EndOfProc   	
		END	
	END		
	IF (@WLTYPE = 0) OR (@testAlert = 1)
	BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'Alert', @ID, TRANNO 
				FROM @TT t
				WHERE t.cust = @cust
	
			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC
	END 
	ELSE 
	IF @WLTYPE = 1 
	BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'SUSPACT', @ID, TRANNO 
				FROM @TT t
				WHERE t.cust = @cust

			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC 
	END
	FETCH NEXT FROM @cur INTO @tranAmt, @tranCnt, @Cust
END

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_HRCtyInMLE
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_HRCtyInMLE
RETURN @stat
GO


