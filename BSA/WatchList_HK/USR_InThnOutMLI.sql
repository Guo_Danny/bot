USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_InThnOutMLI]    Script Date: 8/1/2024 10:04:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USR_InThnOutMLI] (@WLCode SCode, @testAlert INT,
	@day  int,
	@percent  int,
	@inActivityTypeList varchar(8000),
	@outActivityTypeList varchar(8000),
	@minSumAmt money,
	@riskClassList varchar(8000),
	@customerTypeList varchar(8000),
	@inboundCashType smallint,
	@outboundCashType smallint,
	@MinInAmt	INT,
	@MinInCnt	INT,
	@MinOutAmt	INT,
	@MinOutCnt	INT,
	@MaxInAmt	INT,
	@MaxInCnt	INT,
	@MaxOutAmt	INT,
	@MaxOutCnt	INT,
	@UseRelAcct Bit, 
	@UseRelCust Bit, 
    @ExcludeRelList varchar(8000),
	@TransOfDays INT,
	@STDEVCounts Float,
	@InThenOut INT
	)
AS
/* RULE AND PARAMETER DESCRIPTION
	Detects customers with incoming (receive) transactions of a 
	specified activity type and outgoing (pay) transactions of a specified 
	activity type where the total amounts differ by less than or equal to a 
	specified percentage.  This rule evaluates transactions that are within
	the specified number of days and the aggregate amount of the qualifying
	transactions exceeds the specified minimum amount.  Additional 
	constraints include Cash/Non-Cash for inbound and outbound activity types,
	Customer Type and Risk Class.  Designed to be run Post-EOD
	
	@day = Indicates the number of days that are included in the evaluation of 
		transactions. This includes business and non-business days prior to
		the day the rule is executed.
	@percent = Percentage of the difference in amounts between the deposits 
		(receive) and the withdrawals (pay)
	@inActivityType = A comma separated list of inbound activity types to 
		be evaluated
        @outActivityType = A comma separated list of outbound activity types to 
		be evaluated 
	@MinInAmt	= min sinle in amount
	@MinInCnt	= min in transaction count
	@MinOutAmt	= min sinle out amount
	@MinOutCnt	= min out transaction count
	@MaxInAmt	= max single in amount, if no set -1
	@MaxInCnt	= max in transaction count, if no set -1
	@MaxOutAmt	= max single out amount, if no set -1
	@MaxOutCnt	= max out transaction count, if no set -1
	@minSumAmt = The Minimum aggregated transaction amount that must be exceeded.  
	@riskClass = A comma separated list of Risk Classes to include in the evaluation,
		 use -ALL- for any risk class.
	@customerType = A comma separated list of Customer Types to include in the 
		evaluation, use -ALL- for any customer type.
	@inboundCashType = For the activity types specified in the parameter 
		InActivityType, use 1 to restrict them to only Cash transactions,
		use 0 to restrict them to Non-Cash, 2 for both
	@outboundCashType = For the activity types specified in the parameter 
		OutActivityType, use 1 to restrict them to only 
			    Cash transactions, use 0 to restrict them to Non-Cash, 2 for both
	@UseRelAcct = Include Related Accounts (1=yes, 0=no) 
	@UseRelCust = Include Related Parties (1=yes, 0=no) 
	@ExcludeRelList = Exclude Relationship Types (comma separated list or '-NONE-')
	@TransOfDays = to calculate the STDEV value for the period
	@STDEVCounts = the X times of the STDEV value.
	@InThenOut = For HK new rule, catch single in transaction then single out transaction.
	             0 = SgeInOut, 1 = InThenOut.
*/

/* for test
delete Alert where WLCode = 'SgeInOut'
select @WLCode = 'SgeInOut', 
	@testAlert = 1,
	@day = 365,
	@percent = 50,
	@inActivityTypeList = '-ALL-',
	@outActivityTypeList = '-ALL-',
	@minSumAmt= 0,
	@riskClassList  = '-ALL-',
	@customerTypeList = '-ALL-',
	@inboundCashType = 2,
	@outboundCashType = 2,
	@MinInAmt = 0,
	@MaxInAmt = -1,
	@MinInCnt = 1,
	@MaxInCnt = -1,
	@MinOutAmt = 0,
	@MaxOutAmt = -1,
	@MinOutCnt = 1,
	@MaxOutCnt = -1,
	@UseRelAcct = 0, 
	@UseRelCust = 0, 
    @ExcludeRelList = '-NONE-',
	@TransOfDays = 7,
	@STDEVCounts = 0,
	@InThenOut = 0
*/

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@MINDATE INT
Declare @cust LONGNAME
Declare @currDateTime datetime 
Declare @CurrBookDate int

DECLARE @SetDate DATETIME

DECLARE @TT table
(
	Cust VARCHAR(40),
	Recv MONEY,
	Recount	int,
	Pay MONEY,
	Paycount int,
	Mindate INT,
	Maxdate INT,
	Descr VARCHAR(2000)
)

DECLARE @TT1 table
(
	Cust	VARCHAR(35),
	TranNo	INT,
	BaseAmt MONEY, 
	RecvPay INT,
	BookDate INT)

DECLARE	@TT2 TABLE (
	Cust	VARCHAR(40),
	s_date int,
	e_date int,
	cur_date int,
	trncnt float
)

DECLARE	@stdavg TABLE (
	Cust	VARCHAR(40),
	AvgCount Float,
	StdevpCount Float,
	StdAvg Float
)




SET NOCOUNT ON
SET @stat = 0

IF (ISNULL(@inActivityTypeList,'') = '' 
		OR UPPER(ISNULL(@inActivityTypeList,'-ALL-')) = '-ALL-')
	SELECT @inActivityTypeList = NULL
ELSE
	SELECT @inActivityTypeList = ',' + 
		REPLACE(ltrim(rtrim(@inActivityTypeList)),CHAR(32),'') + ','

IF (ISNULL(@outActivityTypeList,'') = '' 
		OR UPPER(ISNULL(@outActivityTypeList,'-ALL-')) = '-ALL-')
	SELECT @outActivityTypeList = NULL
ELSE
	SELECT @outActivityTypeList = ',' + 
		REPLACE(ltrim(rtrim(@outActivityTypeList)),CHAR(32),'') + ','

IF (ISNULL(@riskClassList,'') = '' 
		OR UPPER(ISNULL(@riskClassList,'-ALL-')) = '-ALL-')
	SELECT @riskClassList = NULL
ELSE
	SELECT @riskClassList = ',' +
		REPLACE(ltrim(rtrim(@riskClassList)),CHAR(32),'') + ','

IF (ISNULL(@customerTypeList,'') = '' 
		OR UPPER(ISNULL(@customerTypeList,'-ALL-')) = '-ALL-')
	SELECT @customerTypeList = NULL
ELSE
	SELECT @customerTypeList = ',' +
		REPLACE(ltrim(rtrim(@customerTypeList)),CHAR(32),'') + ','

IF LTRIM(RTRIM(@ExcludeRelList)) = '-NONE-' OR LTRIM(RTRIM(@ExcludeRelList)) = ''
	SELECT @ExcludeRelList = NULL
ELSE
SELECT @ExcludeRelList = ',' + 
	REPLACE(LTRIM(RTRIM(@ExcludeRelList)),' ','')+ ','

IF (@inboundCashType = 2)
	SET @inboundCashType = NULL

IF (@OutboundCashType = 2)
	SET @OutboundCashType = NULL

--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_InThnOutMLI
ELSE
	-- Already in a transaction
	SAVE TRAN USR_InThnOutMLI
/* End standard stored procedure transaction header */

/*  standard Rules Header */

-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @description = [Desc], @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList WITH (NOLOCK)
WHERE WLCode = @WLCode

SELECT @day = ABS(@day)

--When the transactions are picked up
SET @minDate = dbo.ConvertSqlDateToInt(
	DATEADD(d, -1 * @day, CONVERT(VARCHAR, @SetDate)))


--JUL-06-2021 HK new BSA begin

declare @curdate datetime; --to calculate the current date
declare @recentdate datetime; -- to calculate the last No. TransOfDays
declare @lastdate datetime;-- the begining date

set @recentdate = @SetDate - @TransOfDays;
set @lastdate = @SetDate - @TransOfDays;
set @curdate = @SetDate;


if @InThenOut = 0 begin
	--SgeInOut
	while (dbo.ConvertSqlDateToInt(@lastdate) >= @minDate)
	begin
	insert into @TT2(Cust, s_date, e_date, cur_date, trncnt)
	select Cust, convert(varchar(8), @lastdate, 112),
	convert(varchar(8), @curdate, 112),
	BookDate, count(*)  
	from ActivityHist 
	where BookDate >= convert(varchar(8), @lastdate,112) and BookDate < convert(varchar(8), @curdate, 112)
	and RecvPay = 1
	group by Cust,BookDate

	set @curdate = @lastdate;
	set @lastdate = @lastdate - @TransOfDays;
	
	end
	
	insert into @stdavg(Cust, AvgCount, StdevpCount, StdAvg)
	select Cust, avg(trncnt), STDEVP(trncnt), avg(trncnt) + (@STDEVCounts * STDEVP(trncnt)) 
	from (
	select Cust, s_date, e_date, SUM(trncnt) trncnt from @TT2
	group by Cust, s_date, e_date
	) A group by Cust
	order by Cust

end else begin
	--InThenOut
	insert into @stdavg(Cust, AvgCount, StdevpCount, StdAvg)
	select Cust, avg(BaseAmt), STDEVP(BaseAmt), avg(BaseAmt) + (@STDEVCounts * STDEVP(BaseAmt)) 
	from ActivityHist 
	where BookDate >= convert(varchar(8), @minDate,112) and BookDate < convert(varchar(8), @curdate, 112)
	and	( 
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	)
	group by Cust

end

--JUL-06-2021 HK new BSA end

Insert Into @TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select cust, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist WITH (NOLOCK) 
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	AND BaseAmt >= @MinInAmt 
	and (@MaxInAmt = -1 OR BaseAmt <= @MaxInAmt)
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	AND BaseAmt >= @MinOutAmt
	and (@MaxOutAmt = -1 OR BaseAmt <= @MaxOutAmt)
	)
)
and BookDate >= dbo.ConvertSqlDateToInt(@recentdate) and BookDate < dbo.ConvertSqlDateToInt(@setdate)

-- Related Parties
Insert Into @TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select Distinct pr.PartyId, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist ah WITH (NOLOCK) JOIN PartyRelation pr ON ah.Cust = pr.RelatedParty
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	AND BaseAmt >= @MinInAmt 
	and (@MaxInAmt = -1 OR BaseAmt <= @MaxInAmt)
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	AND BaseAmt >= @MinOutAmt
	and (@MaxOutAmt = -1 OR BaseAmt <= @MaxOutAmt)
	)
)
and BookDate >= dbo.ConvertSqlDateToInt(@recentdate) and BookDate < dbo.ConvertSqlDateToInt(@setdate)
AND @UseRelCust = 1 AND ah.Cust <> pr.PartyID AND pr.Deleted <> 1  
AND ((ISNULL(@ExcludeRelList,'') = '' OR 
	CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
AND Exists (select * from @tt1 tt Where tt.Cust = pr.PartyId)
AND Not Exists (select * from @tt1 tt Where tt.Cust = pr.PartyId AND tt.TranNo = ah.tranno)

-- Related Parties (reverse direction)
Insert Into @TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select Distinct pr.RelatedParty, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist ah WITH (NOLOCK)  JOIN PartyRelation pr ON ah.Cust = pr.PartyID
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	AND BaseAmt >= @MinInAmt 
	and (@MaxInAmt = -1 OR BaseAmt <= @MaxInAmt)
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	AND BaseAmt >= @MinOutAmt
	and (@MaxOutAmt = -1 OR BaseAmt <= @MaxOutAmt)
	)
)
and BookDate >= dbo.ConvertSqlDateToInt(@recentdate) and BookDate < dbo.ConvertSqlDateToInt(@setdate)
AND @UseRelCust = 1 AND ah.Cust <> pr.RelatedParty AND pr.Deleted <> 1
AND ((ISNULL(@ExcludeRelList,'') = '' OR 
	CHARINDEX(',' + LTRIM(RTRIM(ISNULL(pr.Relationship,''))) + ',', @ExcludeRelList) = 0))
AND Exists (select * from @tt1 tt Where tt.Cust = pr.RelatedParty)
AND Not Exists (select * from @tt1 tt Where tt.Cust = pr.RelatedParty AND tt.TranNo = ah.tranno)

-- Related Accounts
Insert Into @TT1 (Cust, Tranno, BaseAmt, RecvPay, BookDate)
Select Distinct ao.Cust, Tranno, BaseAmt, RecvPay, BookDate 
From ActivityHist ah WITH (NOLOCK) JOIN AccountOwner ao 
On ah.Account = ao.Account and ah.Cust <> ao.Cust 
WHERE 
(	
	(
	recvpay = 1 
	AND (@inActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@inActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@inboundCashType, CASHTRAN)) 
	AND BaseAmt >= @MinInAmt 
	and (@MaxInAmt = -1 OR BaseAmt <= @MaxInAmt)
	)
	OR 
	(
	recvpay = 2 
	AND (@outActivityTypeList IS NULL OR 
		CHARINDEX(',' + CONVERT(VARCHAR, type) + ',',@outActivityTypeList) > 0) 
	AND (CASHTRAN = ISNULL(@OutboundCashType, CASHTRAN))
	AND BaseAmt >= @MinOutAmt
	and (@MaxOutAmt = -1 OR BaseAmt <= @MaxOutAmt)
	)
)
and BookDate >= dbo.ConvertSqlDateToInt(@recentdate) and BookDate < dbo.ConvertSqlDateToInt(@setdate)
AND @UseRelAcct = 1
AND ((ISNULL(@ExcludeRelList,'') = '' OR 
	CHARINDEX(',' + LTRIM(RTRIM(ISNULL(ao.Relationship,''))) + ',', @ExcludeRelList) = 0))
AND Exists (select * from @tt1 tt Where tt.Cust = ao.cust)
AND Not Exists (select * from @tt1 tt Where tt.Cust = ao.Cust AND tt.TranNo = ah.tranno)




if @InThenOut = 0 begin

	INSERT INTO @TT(Cust, recv, Recount, pay, Paycount, mindate, maxdate)
	SELECT cust, SUM(recv) recv, sum(recount), SUM(pay) pay, sum(paycount), MIN(mindate) mindate, 
		   MAX(maxdate) maxdate
	FROM  (
		SELECT cust, 		
		CASE WHEN recvpay = 1 THEN SUM(baseamt) 
			ELSE 0
			END recv,
		CASE WHEN recvpay = 2 THEN SUM(baseamt) 
			ELSE 0
			END pay,
		MIN(bookdate) AS mindate,
		MAX(bookdate) AS maxdate, 
		CASE WHEN recvpay = 1 then count(*) 
		else 0 end recount, 
		CASE WHEN recvpay = 2 then count(*) 
		else 0 end paycount
		FROM @TT1
		GROUP BY cust, recvpay
	) a, 
		Customer WITH (NOLOCK)
		WHERE   a.cust = Customer.Id
		AND ((@riskClassList IS NULL OR 
				CHARINDEX(',' + 
					LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0))
		AND ((@customerTypeList IS NULL OR
				CHARINDEX(',' + 
					LTRIM(RTRIM(Customer.type)) + ',', @customerTypeList ) > 0))
	GROUP BY cust
	HAVING SUM(recv) <> 0 AND SUM(pay) <> 0 
	and 1 = case when (SUM(recv) > SUM(pay) and (100 * SUM(pay) >= (100 - @percent)*SUM(recv))) then 1 
	when (SUM(recv) > SUM(pay) and (100 * SUM(pay) < (100 - @percent)*SUM(recv))) then 0
	when (SUM(pay) > SUM(recv) and (100 * SUM(recv) >= (100 - @percent)*SUM(pay))) then 1
	when (SUM(pay) > SUM(recv) and (100 * SUM(recv) < (100 - @percent)*SUM(pay))) then 0
	when SUM(pay) = SUM(recv) then 1
	when @percent = -1 then 1
	else 0 end

	update @TT set Descr = 'Cust:  ''' + T.cust 
	+ ''' has incoming transfer '''
	+ CONVERT(VARCHAR, T.Recv)
	+ ''' followed by an outgoing transfer '''
	+ CONVERT(VARCHAR, T.Pay)
	+ ''' where the percentage '
	+ 'differ by '
        + CASE 
          WHEN T.Pay >= T.Recv AND T.Pay <> 0 THEN 
           CONVERT(VARCHAR, ABS(100 - 100 * T.Recv/T.Pay)) 
          WHEN T.Pay < T.Recv AND T.Recv <> 0 THEN 
           CONVERT(VARCHAR, ABS(100 - 100 * T.Pay/T.Recv))
          END
	+ ' % from bookdate ''' 
	+ CONVERT(VARCHAR, T.Mindate) + ''' to '''
	+ CONVERT(VARCHAR, T.Maxdate) + '''' 
	+ ' and the incoming transaction count:' + CONVERT(VARCHAR, T.Recount) 
	+ ' is greater (or equal) than the average count:' + CAST(CAST(S.AvgCount AS decimal(20,2)) AS VARCHAR)
	+ ' plus ''' + CONVERT(VARCHAR, @STDEVCounts) + ''' times of STdev trasaction count:' + CAST(CAST(S.StdevpCount AS decimal(20,2)) AS VARCHAR) 
	+ ' where the average count calculate period from ' + convert(varchar, @minDate)
	+ ' to ' + convert(varchar, dbo.ConvertSqlDateToInt(@SetDate))
	from @TT T 
	join @stdavg S on S.Cust = T.Cust
	where T.Recount >= S.StdAvg

end else begin 

	INSERT INTO @TT(Cust, recv, Recount, pay, Paycount, mindate, maxdate)
	SELECT cust, SUM(recv) recv, sum(recount), SUM(pay) pay, sum(paycount), Max(mindate) mindate, 
		   MAX(maxdate) maxdate
	FROM  (
		SELECT cust, 		
		CASE WHEN recvpay = 1 THEN SUM(baseamt) 
			ELSE 0
			END recv,
		CASE WHEN recvpay = 2 THEN SUM(baseamt) 
			ELSE 0
			END pay,
		case when RecvPay = 1 then BookDate else '' end AS mindate,
		case when RecvPay = 2 then BookDate else '' end AS maxdate, 
		CASE WHEN recvpay = 1 then count(*) 
		else 0 end recount, 
		CASE WHEN recvpay = 2 then count(*) 
		else 0 end paycount
		FROM @TT1
		GROUP BY cust, recvpay, BookDate
	) a, 
		Customer WITH (NOLOCK)
		WHERE   a.cust = Customer.Id
		AND ((@riskClassList IS NULL OR 
				CHARINDEX(',' + 
					LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0))
		AND ((@customerTypeList IS NULL OR
				CHARINDEX(',' + 
					LTRIM(RTRIM(Customer.type)) + ',', @customerTypeList ) > 0))
	GROUP BY cust
	HAVING SUM(recv) <> 0 AND SUM(pay) <> 0 
	AND (@percent = -1 or 
	(100 * SUM(recv) >= (100 - @percent)*SUM(pay) and 100 * SUM(recv) < (100 + @percent)*SUM(pay)
	 OR  100 * SUM(pay) >= (100 - @percent)*SUM(recv) and 100 * SUM(pay) < (100 + @percent)*SUM(recv))
	)
	AND (SUM(recv) > @MinSumAmt OR SUM(pay) > @MinSumAmt)
	--JUL-08-2021. check for single recv and then single pay
	and Max(maxdate) > Max(mindate) and sum(recount) = 1 and sum(paycount) = 1 

	update @TT set Descr = 'Cust:  ''' + T.cust 
	+ ''' has incoming transfer '''
	+ CONVERT(VARCHAR, T.Recv) + ''' on ' + CONVERT(VARCHAR, T.Mindate)
	+ ' followed by an outgoing transfer '''
	+ CONVERT(VARCHAR, T.Pay) + ''' on ' + CONVERT(VARCHAR, T.Maxdate)
	+ ' where the percentage '
	+ 'differ by '
        + CASE 
          WHEN T.Pay >= T.Recv AND T.Pay <> 0 THEN 
           CONVERT(VARCHAR, ABS(100 - 100 * T.Recv/T.Pay)) 
          WHEN T.Pay < T.Recv AND T.Recv <> 0 THEN 
           CONVERT(VARCHAR, ABS(100 - 100 * T.Pay/T.Recv))
          END
	+ ' % ' 
	+ ' and the outgoing transaction amount:' + CONVERT(VARCHAR, T.Pay) 
	+ ' is greater (or equal) than the average amount:' + CAST(CAST(S.AvgCount AS decimal(20,2)) AS VARCHAR)
	+ ' plus ''' + CONVERT(VARCHAR, @STDEVCounts) + ''' times of STdev trasaction amount:' + CAST(CAST(S.StdevpCount AS decimal(20,2)) AS VARCHAR) 
	+ ' where the average Amount calculate period from ' + convert(varchar, @minDate)
	+ ' to ' + convert(varchar, dbo.ConvertSqlDateToInt(@SetDate))
	from @TT T 
	join @stdavg S on S.Cust = T.Cust
	where T.pay >= S.StdAvg

end

delete @TT where Recount < @MinInCnt or (@MaxInCnt != -1 and Recount > @MaxInCnt)
delete @TT where Paycount < @MinOutCnt or (@MaxOutCnt != -1 and Paycount > @MaxOutCnt)

delete @TT where isnull(Descr,'') = ''

Set @currDateTime = GETDATE()
set @CurrBookDate = dbo.ConvertSqlDateToInt(GetDate())

IF @testAlert = 1 BEGIN
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, CUST, IsTest) 
	  SELECT @WLCode, descr, 0, @currDateTime, cust, 1 FROM @TT 

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	  SELECT Distinct 'Alert', AlertNo, TRANNO 
		FROM @tt1 A 
         JOIN @tt T ON a.Cust = t.Cust
         JOIN Alert AL ON a.Cust = al.Cust
        WHERE 
	al.WLCode = @WLCode
	AND al.CreateDate between @currDateTime AND GETDATE()
	SELECT @STAT = @@ERROR 
	IF @stat <> 0 GOTO EndOfProc
END ELSE BEGIN
	IF @WLType = 0 BEGIN
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, CUST) 
	  SELECT @WLCode, descr, 0, @currDateTime, cust FROM @TT 
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	  SELECT Distinct 'Alert', AlertNo, TRANNO 
		FROM @tt1 A 
         JOIN @tt T ON a.Cust = t.Cust
         JOIN Alert AL ON a.Cust = al.Cust
        WHERE 
	al.WLCode = @WLCode
	AND al.CreateDate between @currDateTime AND GETDATE()

	SELECT @STAT = @@ERROR 
	IF @stat <> 0 GOTO EndOfProc
	END ELSE IF @WLType = 1 BEGIN	
                INSERT INTO SUSPICIOUSACTIVITY (BOOKDATE, CUST, SUSPTYPE, 
                        INCNTTOLPERC, OUTCNTTOLPERC, INAMTTOLPERC, 
			OUTAMTTOLPERC, WLCode, WLDESC, CreateTime)
		SELECT	@CurrBookDate, Cust, 'RULE', 0, 0, 0, 0, 
			@WLCode, descr, @currDateTime
		FROM @tt
		IF @stat <> 0 GOTO EndOfProc
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT Distinct 'SUSPACT', RecNo, TRANNO 
			FROM @tt1 A
        	JOIN @tt T ON a.Cust = t.Cust
             	JOIN SuspiciousActivity s ON a.Cust = s.Cust
        	WHERE 
	        s.WLCode = @WLCode
	        AND s.bookdate = @CurrBookDate
			AND s.createTime between @currDateTime AND GETDATE()

		SELECT @STAT = @@ERROR
		IF @stat <> 0 GOTO EndOfProc
	END
END
EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_InThnOutMLI

  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_InThnOutMLI

RETURN @stat


GO


