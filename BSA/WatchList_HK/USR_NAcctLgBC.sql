USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_NAcctLgBC]    Script Date: 8/1/2024 10:05:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USR_NAcctLgBC] (
	@WLCode 		SCode, 
	@TestAlert 		INT,
	@activityTypeList 	varchar(8000), 
	@day 			INT, 
	@minimumAmount 		MONEY, 
	@AccountList 		VARCHAR(8000), 
	@BranchList 		varchar(8000), 
	@DeptList 		varchar(8000), 
	@CorrespondentBankOnly 	VARCHAR(5),
	@NewAccount		INT,
	@NewOpenPeriod	INT
) AS
/* RULE PARAMETERS -  THESE MUST BE SET ACCORDING TO THE RULE REQUIREMENT*/

/* RULE AND PARAMETER DESCRIPTION
	Detects accounts that had transactions using a specified 
	activity type that exceeded a designated dollar amount over 
	a period of specified days.  Designed to be run Post-EOD. 

	@activityTypeList = Type of activity to be checked   
	@day = No of days activity to be considered
	@minimumAmount= The threshold amount 
	@AccountList = List of Accounts to be checked (Comma Separated or '-ALL-')
        @BranchList  = List of Branches to be checked (Comma Separated or '-ALL-')
        @DeptList    = List of Department to be checked (Comma Separated or '-ALL-')
	@CorrespondentBankOnly = If only Correspondent Banks have to be detected ('Yes' or 'No')
	@NewAccount = 1 for new open account, 0 for default.
	@NewOpenPeriod = calculate the new open account by the no of period.
*/

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@trnCnt INT,
	@MINDATE INT,
	@currentdate datetime

DECLARE @cur CURSOR
/* Cursor Variables */
DECLARE @account OBJECTID
DECLARE @bookDate INT
DECLARE @recvPay INT
DECLARE @tmpDate INT
DECLARE @totAmt MONEY

-- Temporary table of Activity Types that have not been specified as Exempt
DECLARE @ActType TABLE (
	Type	INT
)

DECLARE @TT TABLE (
	Cust	varchar(40),
	Account	varchar(40),
	RecvPay	int,
	totalamount	MONEY,
	opendate	datetime
)

SET NOCOUNT ON
SET @stat = 0

/*****************************************/
	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE,@StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) Then
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 Then
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode
	
	/******************************************/
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_NAcctLgBC
ELSE
	-- Already in a transaction
	SAVE TRAN USR_NAcctLgBC
/* End standard stored procedure transaction header */

--a week to to today
	SET @minDate = dbo.ConvertSqlDateToInt(
	DATEADD(d, -1 * @day, CONVERT(VARCHAR, @startDate)))
	
	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
	SELECT @DeptList = dbo.BSA_fnListParams(@DeptList)
	SELECT @AccountList = dbo.BSA_fnListParams(@AccountList)

	IF LTRIM(RTRIM(@CorrespondentBankOnly)) = 'Yes'
	        SELECT @CorrespondentBankOnly = 1
	ELSE
	        SELECT @CorrespondentBankOnly = NULL

	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)
	
	insert into @TT(Cust, Account, RecvPay, totalamount, opendate)
	SELECT 	ActHist.Cust, ActHist.Account, ActHist.RecvPay, sum(ActHist.BaseAmt), acct.OpenDate
	FROM 	ActivityHist ActHist
	INNER	JOIN Customer Cust ON ActHist.Cust = Cust.[Id] 
	INNER	JOIN Account Acct(NOLOCK) ON ActHist.Account = Acct.[Id]
	INNER	JOIN @ActType Act ON Act.Type = ActHist.Type
	WHERE 	ActHist.bookdate >= @MINDATE  AND ActHist.bookdate <= dbo.ConvertSqlDateToInt(@startDate) 
	AND 	ActHist.Account IS NOT NULL
	AND 	(ISNULL(@AccountList, '') = '' OR
	     	CHARINDEX(',' + ltrim(rtrim(ActHist.Account)) + ',', @AccountList ) > 0)
	AND 	(ISNULL(@CorrespondentBankOnly, '') = '' OR
		Cust.CorrBankRelation = @CorrespondentBankOnly)
	AND 	(ISNULL(@BranchList, '') = '' OR
	     	CHARINDEX(',' + ltrim(rtrim(acct.OwnerBranch)) + ',', @BranchList ) > 0)
	AND 	(ISNULL(@DeptList, '') = '' OR 
	     	CHARINDEX(',' + ltrim(rtrim(acct.OwnerDept)) + ',', @DeptList ) > 0)
	--JUL-05-2021 add, check new account start
	AND 1 = case when @NewAccount = 1 and (dateadd(d,@NewOpenPeriod,acct.OpenDate) >= dbo.BSA_ConvertIntToShortDate(ActHist.BookDate)) then 1
	when @NewAccount = 1 and (dateadd(d,@NewOpenPeriod,acct.OpenDate) < dbo.BSA_ConvertIntToShortDate(ActHist.BookDate)) then 0 
	when @NewAccount = 0 then 1
	else 1 end
	--JUL-05-2021 add, check new account end
	GROUP 	BY ActHist.Cust, ActHist.Account, ActHist.RecvPay, acct.OpenDate
	Having 	SUM(ActHist.BaseAmt) >= @minimumAmount

	set @currentdate = getdate()

	IF @testAlert = 1 
	BEGIN		
		insert into Alert(WLCode, [Desc], Status, CreateDate, LastOper, LastModify, Cust, Account, IsTest)
		select @WLCode, 
		case when @NewAccount = 1 then 'New Open Account: '''  
		else 'Account: ''' end + T.Account + 
		case when @CorrespondentBankOnly = 1 then ''' (Correspondent Bank Account) had transfers '
		else ''' had transfers ''' end 
		+ CONVERT(VARCHAR, T.totalamount) + ''' in ' +
		case when T.RecvPay = 1 then 'deposit over a period of ' + CONVERT(VARCHAR, @day) + ' days.'
		else 'withdrawal over a period of ' + CONVERT(VARCHAR, @day) + ' days.' end
		+ case when @NewAccount = 1 then ' Account open date is:' + convert(varchar(8),T.opendate,112) 
		else '' end,
		0, GETDATE(), Null, Null, T.Cust, T.Account, 1
		from @TT T	

		SELECT @STAT = @@ERROR 

		IF @stat <> 0 GOTO EndOfProc

		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	'Alert', al.AlertNo, ActHist.TRANNO 
			FROM ActivityHist ActHist (NOLOCK) 
			JOIN Customer Cust ON ActHist.Cust = Cust.[Id] 
			JOIN Account Acct(NOLOCK) ON ActHist.Account = Acct.[Id]
			JOIN @ActType Act ON Act.Type = ActHist.Type
			join @TT T on T.Account = ActHist.Account
			join Alert al on al.Account = ActHist.Account
			where al.WLCode = @WLCode
			and		al.CreateDate between @currentdate and getdate()
			and 	ActHist.bookdate >= @MINDATE  AND ActHist.bookdate <= dbo.ConvertSqlDateToInt(@startDate) 
			AND 	ActHist.Account IS NOT NULL
			AND 	(ISNULL(@AccountList, '') = '' OR
	     			CHARINDEX(',' + ltrim(rtrim(ActHist.Account)) + ',', @AccountList ) > 0)
			AND 	(ISNULL(@CorrespondentBankOnly, '') = '' OR
				Cust.CorrBankRelation = @CorrespondentBankOnly)
			AND 	(ISNULL(@BranchList, '') = '' OR
	     			CHARINDEX(',' + ltrim(rtrim(acct.OwnerBranch)) + ',', @BranchList ) > 0)
			AND 	(ISNULL(@DeptList, '') = '' OR 
	     			CHARINDEX(',' + ltrim(rtrim(acct.OwnerDept)) + ',', @DeptList ) > 0)
			--JUL-05-2021 add, check new account start
			AND 1 = case when @NewAccount = 1 and (dateadd(d,@NewOpenPeriod,acct.OpenDate) >= dbo.BSA_ConvertIntToShortDate(ActHist.BookDate)) then 1
			when @NewAccount = 1 and (dateadd(d,@NewOpenPeriod,acct.OpenDate) < dbo.BSA_ConvertIntToShortDate(ActHist.BookDate)) then 0 
			when @NewAccount = 0 then 1
			else 1 end

		SELECT @STAT = @@ERROR 
		IF @stat <> 0 GOTO EndOfProc

	END 
	ELSE 
	BEGIN		    
		IF @WLType = 0 
		BEGIN
			insert into Alert(WLCode, [Desc], Status, CreateDate, LastOper, LastModify, Cust, Account, IsTest)
			select @WLCode, 
			case when @NewAccount = 1 then 'New Open Account: '''  
			else 'Account: ''' end + T.Account + 
			case when @CorrespondentBankOnly = 1 then ''' (Correspondent Bank Account) had transfers '
			else ''' had transfers ''' end 
			+ CONVERT(VARCHAR, T.totalamount) + ''' in ' +
			case when T.RecvPay = 1 then 'deposit over a period of ' + CONVERT(VARCHAR, @day) + ' days.'
			else 'withdrawal over a period of ' + CONVERT(VARCHAR, @day) + ' days.' end
			+ case when @NewAccount = 1 then ' Account open date is:' + convert(varchar(8),T.opendate,112) 
			else '' end,
			0, GETDATE(), Null, Null, T.Cust, T.Account, 0
			from @TT T	

			SELECT @STAT = @@ERROR 

			IF @stat <> 0 GOTO EndOfProc

			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	'Alert', al.AlertNo, ActHist.TRANNO 
			FROM ActivityHist ActHist (NOLOCK) 
			JOIN Customer Cust ON ActHist.Cust = Cust.[Id] 
			JOIN Account Acct(NOLOCK) ON ActHist.Account = Acct.[Id]
			JOIN @ActType Act ON Act.Type = ActHist.Type
			join @TT T on T.Account = ActHist.Account
			join Alert al on al.Account = ActHist.Account
			where al.WLCode = @WLCode
			and		al.CreateDate between @currentdate and getdate()
			and 	ActHist.bookdate >= @MINDATE  AND ActHist.bookdate <= dbo.ConvertSqlDateToInt(@startDate) 
			AND 	ActHist.Account IS NOT NULL
			AND 	(ISNULL(@AccountList, '') = '' OR
	     			CHARINDEX(',' + ltrim(rtrim(ActHist.Account)) + ',', @AccountList ) > 0)
			AND 	(ISNULL(@CorrespondentBankOnly, '') = '' OR
				Cust.CorrBankRelation = @CorrespondentBankOnly)
			AND 	(ISNULL(@BranchList, '') = '' OR
	     			CHARINDEX(',' + ltrim(rtrim(acct.OwnerBranch)) + ',', @BranchList ) > 0)
			AND 	(ISNULL(@DeptList, '') = '' OR 
	     			CHARINDEX(',' + ltrim(rtrim(acct.OwnerDept)) + ',', @DeptList ) > 0)
			--JUL-05-2021 add, check new account start
			AND 1 = case when @NewAccount = 1 and (dateadd(d,@NewOpenPeriod,acct.OpenDate) >= dbo.BSA_ConvertIntToShortDate(ActHist.BookDate)) then 1
			when @NewAccount = 1 and (dateadd(d,@NewOpenPeriod,acct.OpenDate) < dbo.BSA_ConvertIntToShortDate(ActHist.BookDate)) then 0 
			when @NewAccount = 0 then 1
			else 1 end

			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE IF @WLType = 1 
		BEGIN	
			
			INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			WLCode, WLDESC, CREATETIME )
			select 	NULL, dbo.ConvertSqlDateToInt(@startDate), 
			    T.Cust, 
			    T.Account,NULL, 'RULE', NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, case when @NewAccount = 1 then 'New Open Account: '''  
			else 'Account: ''' end + T.Account + 
			case when @CorrespondentBankOnly = 1 then ''' (Correspondent Bank Account) had transfers '
			else ''' had transfers ''' end 
			+ CONVERT(VARCHAR, T.totalamount) + ''' in ' +
			case when T.RecvPay = 1 then 'deposit over a period of ' + CONVERT(VARCHAR, @day) + ' days.'
			else 'withdrawal over a period of ' + CONVERT(VARCHAR, @day) + ' days.' end 
			+ case when @NewAccount = 1 then ' Account open date is:' + convert(varchar(8),T.opendate,112) 
			else '' end, GETDATE() 
			FROM @TT T

			SELECT @STAT = @@ERROR 

			IF @stat <> 0 GOTO EndOfProc

			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	'SUSPACT', SA.RecNo, ActHist.TRANNO 
				FROM ActivityHist ActHist (NOLOCK) 
				JOIN Customer Cust ON ActHist.Cust = Cust.[Id] 
				JOIN Account Acct(NOLOCK) ON ActHist.Account = Acct.[Id]
				JOIN @ActType Act ON Act.Type = ActHist.Type
				join @TT T on T.Account = ActHist.Account
				join SuspiciousActivity SA on SA.Account = ActHist.Account
				where SA.WLCode = @WLCode
				and		SA.CreateTime between @currentdate and getdate()
				and 	ActHist.bookdate >= @MINDATE  AND ActHist.bookdate <= dbo.ConvertSqlDateToInt(@startDate) 
				AND 	ActHist.Account IS NOT NULL
				AND 	(ISNULL(@AccountList, '') = '' OR
	     				CHARINDEX(',' + ltrim(rtrim(ActHist.Account)) + ',', @AccountList ) > 0)
				AND 	(ISNULL(@CorrespondentBankOnly, '') = '' OR
					Cust.CorrBankRelation = @CorrespondentBankOnly)
				AND 	(ISNULL(@BranchList, '') = '' OR
	     				CHARINDEX(',' + ltrim(rtrim(acct.OwnerBranch)) + ',', @BranchList ) > 0)
				AND 	(ISNULL(@DeptList, '') = '' OR 
	     				CHARINDEX(',' + ltrim(rtrim(acct.OwnerDept)) + ',', @DeptList ) > 0)
				--JUL-05-2021 add, check new account start
				AND 1 = case when @NewAccount = 1 and (dateadd(d,@NewOpenPeriod,acct.OpenDate) >= dbo.BSA_ConvertIntToShortDate(ActHist.BookDate)) then 1
				when @NewAccount = 1 and (dateadd(d,@NewOpenPeriod,acct.OpenDate) < dbo.BSA_ConvertIntToShortDate(ActHist.BookDate)) then 0 
				when @NewAccount = 0 then 1
				else 1 end

			SELECT @STAT = @@ERROR 
			IF @stat <> 0 GOTO EndOfProc
		END
	END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_NAcctLgBC
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_NAcctLgBC
RETURN @stat



GO


