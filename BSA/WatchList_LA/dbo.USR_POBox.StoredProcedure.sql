/****** Object:  StoredProcedure [dbo].[USR_POBox]    Script Date: 10/05/2017 23:46:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USR_POBox](@WLCode SCode, @testAlert INT) 
AS

/*  Declarations */
DECLARE @cur CURSOR,
        @description VARCHAR(2000),
        @Id INT,
        @WLType INT,
        @stat INT,
        @LastCust VARCHAR(40),
        @trnCnt INT

SET NOCOUNT ON
SET @stat = 0
/* Start standard stored procedure transaction header */
SELECT @trnCnt = @@trancount    -- Save the current trancount
IF @trnCnt = 0
   -- Transaction has not begun
   BEGIN TRAN USR_POBox
Else
    -- Already in a transaction
    SAVE TRAN USR_POBox
/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @description = [Desc], @WLType = WLType
FROM WatchList (NOLOCK) WHERE WLCode = @WLCode
SET @cur = CURSOR FAST_FORWARD FOR 
Select ByOrder from Activity inner join ActivityType on Activity.Type = ActivityType.Type  inner join vwRuleNonExmActType V on Activity.Type = V.Type 

 where ( Activity.Type in (1012, 1022) and (Activity.BeneAddress like '%P.O.Box%' or Activity.ByOrderAddress like '%P.O.Box%' or Activity.BeneAddress like '%PO Box%' or Activity.ByOrderAddress like '%PO Box%')) AND (Activity.ByOrder is NOT NULL  )  Group By ByOrder ORDER BY 1


/* Cursor declarations */ 
Declare @ByOrder VarChar(35)
DECLARE @CUST VARCHAR(35)
DECLARE @Account VARCHAR(35)
DECLARE @Bookdate INT
SET @CUST = NULL
SET @Account = NULL
SET @BOOKDATE = dbo.ConvertSqlDateToInt(GetDate())


SELECT @LastCust = ''
OPEN @cur
FETCH NEXT FROM @cur INTO @ByOrder
WHILE @@FETCH_STATUS = 0
BEGIN
     IF @testAlert = 1 BEGIN
         EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @description, @CUST, @ACCOUNT, 1
         IF @stat <> 0 GOTO EndOfProc
     END ELSE BEGIN
      IF @WLType = 0 BEGIN
           EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @description, @CUST, @ACCOUNT
           IF @stat <> 0 GOTO EndOfProc
      END ELSE IF @WLType = 1 BEGIN
           EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT,
           @WLCode, @description, @BOOKDATE, @CUST, @ACCOUNT
           IF @stat <> 0 GOTO EndOfProc
      End
    END
      If @ID IS NOT NULL BEGIN
        IF @testAlert = 1 BEGIN
               INSERT INTO SAsActivity (ObjectType, ObjectID, TranNo)

                   SELECT 'Alert', @ID, TranNo  from Activity inner join ActivityType on Activity.Type = ActivityType.Type  inner join vwRuleNonExmActType V on Activity.Type = V.Type 

 where ( Activity.Type in (1012, 1022) and (Activity.BeneAddress like '%P.O.Box%' or Activity.ByOrderAddress like '%P.O.Box%' or Activity.BeneAddress like '%PO Box%' or Activity.ByOrderAddress like '%PO Box%')) AND Activity.ByOrder = @ByOrder
                   SELECT @stat = @@Error
                   IF @stat <> 0 GOTO EndOfProc
        END ELSE BEGIN
           IF @WLType = 0 BEGIN
               INSERT INTO SAsActivity (ObjectType, ObjectID, TranNo)
                   SELECT 'Alert', @ID, TranNo  from Activity inner join ActivityType on Activity.Type = ActivityType.Type  inner join vwRuleNonExmActType V on Activity.Type = V.Type 

 where ( Activity.Type in (1012, 1022) and (Activity.BeneAddress like '%P.O.Box%' or Activity.ByOrderAddress like '%P.O.Box%' or Activity.BeneAddress like '%PO Box%' or Activity.ByOrderAddress like '%PO Box%')) AND Activity.ByOrder = @ByOrder
                   SELECT @stat = @@Error
                   IF @stat <> 0 GOTO EndOfProc
           END ELSE IF @WLType = 1 BEGIN
               INSERT INTO SAsActivity (ObjectType, ObjectID, TranNo)
               SELECT 'SuspAct', @ID, TranNo  from Activity inner join ActivityType on Activity.Type = ActivityType.Type  inner join vwRuleNonExmActType V on Activity.Type = V.Type 

 where ( Activity.Type in (1012, 1022) and (Activity.BeneAddress like '%P.O.Box%' or Activity.ByOrderAddress like '%P.O.Box%' or Activity.BeneAddress like '%PO Box%' or Activity.ByOrderAddress like '%PO Box%')) AND Activity.ByOrder = @ByOrder
               SELECT @stat = @@ERROR
               IF @stat <> 0 GOTO EndOfProc
           END
        END
      END
      FETCH NEXT FROM @cur INTO @ByOrder
End


CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN
   ROLLBACK TRAN USR_POBox
   RETURN @stat
End

IF @trnCnt = 0
   COMMIT TRAN USR_POBox
RETURN @stat
GO
