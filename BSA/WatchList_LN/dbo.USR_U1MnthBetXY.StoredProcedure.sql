USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[USR_U1MnthBetXY]    Script Date: 10/6/2017 11:03:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[USR_U1MnthBetXY] (
	@WLCode 		SCode, 
	@TestAlert 		INT,
	@lastnoofdays	INT,
	@activityTypeList 	VARCHAR(8000), 
	@minimumAmount 		MONEY, 
	@maximumAmount 		MONEY,
	@CustTypeList		Varchar(8000),
	@RiskClassList 		Varchar(8000),
	@BranchList 		Varchar(8000),
	@customerList VARCHAR(8000),
	@CustRule int,
	@UseRelCust Bit

)
AS

	/* RULE AND PARAMETER DESCRIPTION
	    Detects accounts that had transactions using a specified 
		activity type and the total of these transactions were between a designated 
		dollar amount range within 1 calender month.  Designed to be run Post-EOD.
				@lastnoofdays = last number of days to calculate
				@activityTypeList	- List of activity types to be detected.
				@minimumAmount	- Minimum amount
				@maximumAmount	- Maximum amount
				@CustTypeList - List of Customer Types that will be considered. 
						   -ALL-, blank or NULL for all Customer Types
				@RiskClassList	- List of Risk Classes that will be considered. 
						   -ALL-, blank or NULL for all Risk Classes
				@BranchList	- List of Branches that will be considered. 
						   -ALL-, blank or NULL for all Branches
				[Return] INT = Return the error status.  0 for success.
				@CustomerList = Include or Exclude Customer List (comma separated list)
				@CustRule = include / exclude customer (-1:Disable  1: include  2: exclude)
				@UseRelCust = Include Related Parties (1=yes, 0=no) 
		
	*/

	/*  Declarations */
	DECLARE	@description 	VARCHAR(2000),
	    	@desc 		VARCHAR(2000),
	    	@Id 		INT, 
	    	@WLType 	INT,
	    	@stat 		INT,
	   		@trnCnt 	INT,
	    	@minDate 	INT,
	    	@maxDate 	INT
	DECLARE @STARTALRTDATE  DATETIME
	DECLARE @ENDALRTDATE    DATETIME
	
	If @maximumAmount = -1
	Begin
		set @maximumAmount = 99999999999999;
	End
	
	SET NOCOUNT ON
	SET @stat = 0
	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE,@StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode
	
	/*JUL-14-2021 for checking current activity
	SET @minDate = dbo.FirstDayOfMonth(DATEADD(MONTH, -1,@StartDate))
	SET @maxDate = dbo.FirstDayOfMonth(@StartDate)
	*/

	SET @minDate = dbo.ConvertSqlDateToInt(DATEADD(D, @lastnoofdays * -1 ,@StartDate))
	SET @maxDate = dbo.ConvertSqlDateToInt(@StartDate)
	
	--- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */
	SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
	    BEGIN TRAN USR_U1MnthBetXY
	ELSE
	    -- Already in a transaction
	    SAVE TRAN USR_U1MnthBetXY
	/* End standard stored procedure transaction header */
	
	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)
	SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	SELECT @BranchList = dbo.BSA_fnListParams(@BranchList)
	SELECT @customerList = dbo.BSA_fnListParams(@customerList)

	DECLARE	@TT TABLE 
	(
	   Cust 	VARCHAR(40),
	   Recvpay 	INT,
	   BaseAmt 	MONEY,
	   TranNo 	INT,
       SortValue 	INT,
	   bookdate int
	)
	
	DECLARE	@CurTT TABLE 
	(
	   Cust 	VARCHAR(40),
	   Recvpay 	INT,
	   BaseAmt 	MONEY,
	   TranNo 	INT,
       SortValue 	INT,
	   bookdate int
	)
	
	DECLARE	@TTMerge TABLE 
	(
	   Cust 	VARCHAR(40),
	   Recvpay 	INT,
	   TotalAmt 	MONEY,
	   SortValue 	INT,
	   Maxdt int,
	   Mindt int
	)
	-- Temporary table of Activity Types that have not been specified as Exempt
	DECLARE @ActType TABLE 
	(
	  Type		INT
	)	

	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)
	--JUL-2021 add current for aml enhancement 
	INSERT INTO @CurTT ( Cust, SortValue, BaseAmt, TranNo, RecvPay, bookdate)
		SELECT 	Cust, FLOOR(bookDate / 100) * 100 + 1 SortValue, BaseAmt, TranNo, RecvPay, bookdate
		FROM 	Activity A 
		INNER	JOIN Customer ON Customer.ID = A.Cust
		INNER	JOIN Account ON Account.ID = A.Account
		INNER	JOIN @ActType Act ON Act.Type = A.Type
		WHERE  ((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) 
		AND	((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) 
		AND
			((ISNULL(@BranchList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Account.Branch)) + ',', @BranchList ) > 0)) 
		AND 
		--JUL-2021 add for AML enhance REQUEST ISSUE begin
		((@CustRule = 1 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) > 0)))
			OR 
		(@CustRule = 2 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) = 0)))
			OR @CustRule = -1
		)
		--JUL-2021 add for AML enhance REQUEST ISSUE end
	
	--JUL-2021 add current for aml enhancement reverse dir begin
	INSERT INTO @CurTT ( Cust, SortValue, BaseAmt, TranNo, RecvPay, bookdate)
		SELECT 	pr.RelatedParty, FLOOR(bookDate / 100) * 100 + 1 SortValue, BaseAmt, TranNo, RecvPay, bookdate
		FROM 	Activity A 
		JOIN PartyRelation pr WITH (NOLOCK) ON a.Cust = pr.PartyID
		JOIN Customer (NOLOCK) ON pr.RelatedParty = Customer.ID
		INNER	JOIN Account ON Account.ID = A.Account
		INNER	JOIN @ActType Act ON Act.Type = A.Type
		WHERE  ((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) 
		AND	((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) 
		AND
			((ISNULL(@BranchList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Account.Branch)) + ',', @BranchList ) > 0)) 
		AND 
		((@CustRule = 1 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) > 0)))
			OR 
		(@CustRule = 2 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) = 0)))
			OR @CustRule = -1
		)
		AND @UseRelCust = 1 AND a.Cust <> pr.RelatedParty AND pr.Deleted <> 1 
		AND Exists (select * from @CurTT tt Where tt.Cust = pr.RelatedParty)
		AND Not Exists (select * from @CurTT tt Where tt.Cust = pr.RelatedParty AND tt.TranNo = a.tranno)
		--JUL-2021 add current for aml enhancement reverse dir end

	INSERT INTO @TT ( Cust, SortValue, BaseAmt, TranNo, RecvPay, bookdate)
		SELECT 	Cust, FLOOR(bookDate / 100) * 100 + 1 SortValue, BaseAmt, TranNo, RecvPay, bookdate
		FROM 	ActivityHist A 
		INNER	JOIN Customer ON Customer.ID = A.Cust
		INNER	JOIN Account ON Account.ID = A.Account
		INNER	JOIN @ActType Act ON Act.Type = A.Type
		WHERE   BookDate >= @minDate and BookDate < @maxdate 
		AND	((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) 
		AND	((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) 
		AND
			((ISNULL(@BranchList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Account.Branch)) + ',', @BranchList ) > 0)) 
		AND 
		--JUL-2021 add for AML enhance REQUEST ISSUE begin
		((@CustRule = 1 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) > 0)))
			OR 
		(@CustRule = 2 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) = 0)))
			OR @CustRule = -1
		)
		--JUL-2021 add for AML enhance REQUEST ISSUE end

	--JUL-2021 add archive for aml enhancement reverse dir begin
	INSERT INTO @TT ( Cust, SortValue, BaseAmt, TranNo, RecvPay, bookdate)
		SELECT 	pr.RelatedParty, FLOOR(bookDate / 100) * 100 + 1 SortValue, BaseAmt, TranNo, RecvPay, bookdate
		FROM 	ActivityHist A 
		JOIN PartyRelation pr WITH (NOLOCK) ON a.Cust = pr.PartyID
		JOIN Customer (NOLOCK) ON pr.RelatedParty = Customer.ID
		INNER	JOIN Account ON Account.ID = A.Account
		INNER	JOIN @ActType Act ON Act.Type = A.Type
		WHERE   BookDate >= @minDate and BookDate < @maxdate 
		AND	((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0)) 
		AND	((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0)) 
		AND
			((ISNULL(@BranchList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Account.Branch)) + ',', @BranchList ) > 0)) 
		AND 
		--JUL-2021 add for AML enhance REQUEST ISSUE begin
		((@CustRule = 1 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) > 0)))
			OR 
		(@CustRule = 2 and ((ISNULL(@customerList,'') = '' OR CHARINDEX(',' + LTRIM(RTRIM(Customer.id)) + ',', @customerList) = 0)))
			OR @CustRule = -1
		)
		AND @UseRelCust = 1 AND a.Cust <> pr.RelatedParty AND pr.Deleted <> 1 
		AND (Exists (select * from @TT tt Where tt.Cust = pr.RelatedParty) or exists (select * from @CurTT tt Where tt.Cust = pr.RelatedParty))
		AND Not Exists (select * from @TT tt Where tt.Cust = pr.RelatedParty AND tt.TranNo = a.tranno)
		AND Not Exists (select * from @CurTT tt Where tt.Cust = pr.RelatedParty AND tt.TranNo = a.tranno)
		--JUL-2021 add for AML enhance REQUEST ISSUE end

	--JUL-13-2021 delete TT1 where not exists in current activity TT2
	DELETE a FROM @TT a WHERE NOT EXISTS (SELECT 1 FROM @CurTT b WHERE a.Cust=b.Cust)

	--JUN-24-2021 add current activity
	insert into @TT ( Cust, SortValue, BaseAmt, TranNo, RecvPay, bookdate)
	SELECT distinct Cust, SortValue, BaseAmt, TranNo, RecvPay, bookdate
	from @CurTT 
		
	INSERT INTO @TTMerge (Cust, RecvPay, TotalAmt, Maxdt, Mindt)
		SELECT 	Cust, RecvPay, SUM(BaseAmt), Max(bookDate), Min(bookdate)
		FROM 	@TT
		WHERE 	Cust IS NOT NULL
		GROUP 	BY Cust, recvpay
		HAVING 	@minimumAmount <= SUM(baseamt)
		AND (isnull(@maximumAmount,-1) = -1 or SUM(baseamt) <= @maximumAmount)
	
	--1 means test, 0 means no
	IF @testAlert = 1 
	BEGIN
		  SELECT @STARTALRTDATE = GETDATE()
		   INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT, IsTest) 
		   SELECT @WLCode, CASE WHEN Recvpay =1 THEN 'Customer: ''' + Cust + ''' had transactions totaling ''' 
		    	+ CONVERT(VARCHAR, TotalAmt) + 
		        ''' for the Period from ''' + CONVERT(VARCHAR, Mindt) + ''' to ''' + CONVERT(VARCHAR, Maxdt) +
		        ''' of incoming types specified '  ELSE
		        'Customer: ''' + Cust + ''' had transactions totaling ''' 
		        + CONVERT(VARCHAR, TotalAmt) + 
		        ''' for the Period from ''' + CONVERT(VARCHAR, Mindt) + ''' to ''' + CONVERT(VARCHAR, Maxdt) +
		        ''' of outgoing types specified '  END , 0, 
		        GETDATE(), NULL, NULL, 
				Cust, (select top 1 Account from AccountOwner where Cust = m.Cust) , 1
		   FROM @TTMerge as m
	
	
		SELECT @STAT = @@ERROR	
		SELECT @ENDALRTDATE = GETDATE()
	
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	'Alert', AlertNo, TranNo
		    	FROM 	@TTMerge A 
			INNER 	JOIN @TT B ON A.Cust = B.Cust 
			AND	A.RecvPay = B.RecvPay 
			INNER 	JOIN Alert ON A.Cust = Alert.Cust 
			AND	Alert.WLCode = @WLCode 
			AND	Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
			WHERE 	A.RecvPay = CASE WHEN Alert.[desc] LIKE '%of outgoing type%' THEN 2
				ELSE 1 END	
		
		SELECT @STAT = @@ERROR 
	
		
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 
		BEGIN
			SELECT @STARTALRTDATE = GETDATE()
		   	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT) 
		   	SELECT 	@WLCode, CASE WHEN Recvpay =1 THEN 'Customer: ''' + Cust + ''' had transactions totaling ''' 
			    	+ CONVERT(VARCHAR, TotalAmt) + 
			        ''' for the Period from ''' + CONVERT(VARCHAR, Mindt) + ''' to ''' + CONVERT(VARCHAR, Maxdt) +
			        ''' of incoming types specified '  ELSE
			        'Customer: ''' + Cust + ''' had transactions totaling''' 
			        + CONVERT(VARCHAR, TotalAmt) + 
			        ''' for the Period from ''' + CONVERT(VARCHAR, Mindt) + ''' to ''' + CONVERT(VARCHAR, Maxdt) +
			        ''' of outgoing types specified '  END , 0, 
			        GETDATE(), NULL, NULL, 
					Cust, (select top 1 Account from AccountOwner where Cust = m.Cust) 
		   	FROM 	@TTMerge as m
		
			SELECT @STAT = @@ERROR	
			SELECT @ENDALRTDATE = GETDATE()
			IF @STAT <> 0  GOTO ENDOFPROC
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
		    SELECT @STARTALRTDATE = GETDATE()
		    	INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			        ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			        RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			        ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			        EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			        OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			        REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			        WLCode, WLDESC, CREATETIME )
		    	SELECT	NULL, dbo.ConvertSqlDateToInt(@StartDate), 
				    Cust, (select top 1 Account from AccountOwner where Cust = m.Cust),
			        NULL, 'RULE', NULL, NULL, NULL, NULL,
			        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			        NULL, NULL, 0, 0, 0, 0, 
			        NULL, NULL, NULL, NULL, 0, NULL, NULL,
			        @WLCode, CASE WHEN Recvpay =1 THEN 'Customer: ''' + Cust + ''' had transactions totaling ''' 
			        + CONVERT(VARCHAR, TotalAmt) + ''' of incoming types specified ' + 
			        ''' for the Period from ''' + CONVERT(VARCHAR, Mindt) + ''' to ''' + CONVERT(VARCHAR, Maxdt)
					ELSE
			        'Customer: ''' + Cust + ''' had transactions totaling''' 
			        + CONVERT(VARCHAR, TotalAmt) + 
			        ''' for the Period from ''' + CONVERT(VARCHAR, Mindt) + ''' to ''' + CONVERT(VARCHAR, Maxdt) +
			        ''' of outgoing types specified ' END , GETDATE() 
		   	FROM @TTMerge as m
	
			SELECT @STAT = @@ERROR	
			SELECT @ENDALRTDATE = GETDATE()
			IF @STAT <> 0  GOTO ENDOFPROC
		END
		
		IF @WLType = 0 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   		SELECT 	'Alert', AlertNo, TranNo
		    		FROM 	@TTMerge A 
				INNER 	JOIN @TT B ON A.Cust = B.Cust 
				AND	A.RecvPay = B.RecvPay 
				INNER 	JOIN Alert ON A.Cust = Alert.Cust 
				AND	Alert.WLCode = @WLCode 
				AND	 Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
				WHERE 	A.recvpay = CASE WHEN Alert.[desc] LIKE '%of outgoing type%' THEN 2
					ELSE 1 END	
		
			SELECT @STAT = @@ERROR 
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		    		SELECT 	'SUSPACT', RecNo, TRANNO 
		    		FROM 	@TTMerge A 
				INNER 	JOIN @TT B ON A.Cust = B.Cust 
				AND	A.RecvPay = B.RecvPay 
				INNER 	JOIN SuspiciousActivity S ON A.Cust = S.Cust 
				AND	S.WLCode = @WLCode 
				AND	S.CREATETIME BETWEEN @STARTALRTDATE AND @ENDALRTDATE
				WHERE 	A.recvpay = CASE WHEN S.wldesc LIKE '%of outgoing type%' THEN 2
					ELSE 1 END
			SELECT @STAT = @@ERROR 
		END
	END
	
	EndOfProc:
	IF (@stat <> 0) BEGIN 
	    ROLLBACK TRAN USR_U1MnthBetXY
	    RETURN @stat
	END	
	
	IF @trnCnt = 0
	    COMMIT TRAN USR_U1MnthBetXY
	RETURN @stat

GO
