USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_UOvrPayXtim]    Script Date: 11/11/2021 10:43:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[USR_UOvrPayXtim] (
	@WLCode SCode, 
	@testAlert INT,
	@activityType VARCHAR(8000),
	@productType VARCHAR(8000),
	@totalTimes INT,
	@month  INT,
	@RiskClass VARCHAR(8000))
AS
/* RULE AND PARAMETER DESCRIPTION
Detects partial refund of loan overpayment for product (account type) X, T times within Z months.

	@activityType = activity type of refund
	@productType = account type e.g. checking, check,
	@totalTimes = # of over payments
	@month = period to check,
	@RiskClass = risk class to filter on
*/
SET NOCOUNT ON

-- /*  Declarations */
DECLARE @iWLcode SCode, 
	@iTestAlert INT,
	@iActivityType VARCHAR(8000),
	@iTotalTimes INT,
	@iMonth  INT,
	@iproductType VARCHAR(8000),
	@iRiskClass VARCHAR(8000)

--speeds up select statements if use of local variable
SELECT @iWLcode = @WLcode , 
	@iTestAlert = @TestAlert ,
	@iActivityType = 	@ActivityType ,
	@iTotalTimes  = @TotalTimes  ,
	@iMonth = ABS(@Month) ,
	@iproductType = @productType ,
	@iRiskClass = @riskClass

DECLARE	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,  --0 for alert, 1 for case
	@stat INT,
	@trnCnt INT,
	@minDate INT,
	@startDate DATETIME,
	@endDate	DATETIME, 
	@currDate INT



DECLARE	@overPayAct TABLE (
		Account VARCHAR(40),
		TranNo 	INT
	)

DECLARE	@acctCnts TABLE (
		Account VARCHAR(40),
		Cnt INT
	)


DECLARE	@accts TABLE (
		Account VARCHAR(40)		
	)

SET @stat = 0

--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_UOvrPayXtim
ELSE
	-- Already in a transaction
	SAVE TRAN USR_UOvrPayXtim
/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @WLType = WLType  
FROM WatchList (NOLOCK) WHERE WLCode = @iWLCode

SET @minDate = dbo.ConvertSqlDateToInt(
	DATEADD(m, -1 * @iMonth, CONVERT(VARCHAR, GETDATE())))
SET @currDate = dbo.ConvertSqlDateToInt(GETDATE())

--first get all the accounts we need to deal with
INSERT INTO @accts (Account)
SELECT DISTINCT a.Account
	FROM Activity a (NOLOCK) 
			INNER JOIN Account acct (NOLOCK)
				ON a.Account = acct.Id 
			INNER JOIN Customer cust 
				ON a.Cust = Cust.Id 
	WHERE a.bookdate > @minDate AND 		
		a.Account IS NOT NULL AND
		a.Type IN (SELECT Id FROM dbo.BSA_fnIntegerStrSplitter(@iActivityType)) AND
		acct.Type IN (SELECT Id FROM dbo.BSA_fnStrSplitter(@iproductType)) AND
		cust.RiskClass IN (SELECT Id FROM dbo.BSA_fnStrSplitter(@iRiskClass )) 

--get all transactions of the accounts we need to deal with in current table
INSERT INTO @overPayAct(Account, TranNo)
SELECT a.Account, a.TranNo
	FROM Activity a (NOLOCK) 
		INNER JOIN @accts acct
			ON a.Account = acct.Account 
		INNER JOIN Customer cust 
			ON a.Cust = Cust.Id 
	WHERE a.bookdate > @minDate AND 
		a.Type IN (SELECT Id FROM dbo.BSA_fnIntegerStrSplitter(@iActivityType)) AND
		cust.RiskClass IN (SELECT Id FROM dbo.BSA_fnStrSplitter(@iRiskClass )) 

--get all transactions of the accounts we need to deal with in current table
INSERT INTO @overPayAct(Account, TranNo)
SELECT ah.Account, ah.TranNo
	FROM ActivityHist ah (NOLOCK) 
		INNER JOIN @accts acct
			ON ah.Account = acct.Account 
		INNER JOIN Customer cust 
			ON ah.Cust = Cust.Id 
	WHERE ah.bookdate > @minDate AND 
		ah.Type IN (SELECT Id FROM dbo.BSA_fnIntegerStrSplitter(@iActivityType)) AND
		cust.RiskClass IN (SELECT Id FROM dbo.BSA_fnStrSplitter(@iRiskClass )) 


INSERT INTO @acctCnts (Account, Cnt)
SELECT Account, COUNT(TranNo) FROM @overPayAct
GROUP BY Account
HAVING COUNT(TranNo) >= @iTotalTimes


IF @testAlert = 1 BEGIN
	SELECT @startDate = GETDATE()
	INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT, IsTest) 
	  SELECT @WLCODE, 'Account:  ''' + Account + ''' has partial refund of loans ' + 
		CONVERT(VARCHAR, cnt) + ' times.', 0, 
		GETDATE(), NULL, NULL, NULL, Account, 1
	  FROM @acctCnts 
	SELECT @STAT = @@ERROR	
	SELECT @endDate = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'ALERT', ALERTNO, TRANNO 
		FROM @acctCnts a 
			INNER JOIN @overPayAct o
				ON a.Account = o.Account
			INNER JOIN ALERT 
				ON a.Account = ALERT.Account 
		WHERE			
		ALERT.WLCODE = @WLCODE AND
		ALERT.CREATEDATE BETWEEN @startDate AND @endDate

	SELECT @STAT = @@ERROR 
END ELSE BEGIN
	IF @WLTYPE = 0 BEGIN
		SELECT @startDate = GETDATE()
		INSERT INTO ALERT ( WLCODE, [DESC], STATUS, CREATEDATE, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT) 
		  SELECT @WLCODE, 'Account:  ''' + Account + ''' has partial refund of loans  ' + 
			CONVERT(VARCHAR, cnt) + ' times.', 0, 
			GETDATE(), NULL, NULL, NULL, Account 
		  FROM @acctCnts 
		SELECT @STAT = @@ERROR	
		SELECT @endDate = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
	END ELSE IF @WLTYPE = 1 BEGIN
		SELECT @startDate = GETDATE()
		INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			WLCODE, WLDESC, CREATETIME )
		SELECT	NULL, @currDate, Null, Account,
			NULL, 'Rule', NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, 0, 0, 0, 0, 
			NULL, NULL, NULL, NULL, 0, NULL, NULL,
			@WLCODE, 'Account:  ''' + Account + ''' has partial refund of loans ' + 
		CONVERT(VARCHAR, cnt) + ' times.' , GETDATE() 
	FROM @acctCnts 
	SELECT @STAT = @@ERROR	
	SELECT @endDate = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC
	END
	
	IF @WLTYPE = 0 BEGIN
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'ALERT', ALERTNO, TRANNO 
		FROM @acctCnts a 			 
			INNER JOIN @overPayAct o
				ON a.Account = o.Account
			INNER JOIN ALERT 
				ON a.Account = ALERT.Account 
		WHERE	ALERT.WLCODE = @WLCODE AND
		ALERT.CREATEDATE BETWEEN @startDate AND @endDate

	SELECT @STAT = @@ERROR 
	END ELSE IF @WLTYPE = 1 BEGIN
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT 'SUSPACT', RECNO, TRANNO 
		FROM @acctCnts a 
			INNER JOIN @overPayAct o
				ON a.Account = o.Account
			INNER JOIN SUSPICIOUSACTIVITY S 
				ON a.Account = s.Account 
		WHERE	S.WLCODE = @WLCODE AND
		S.CREATETIME BETWEEN @startDate AND @endDate
	SELECT @STAT = @@ERROR 
	END
END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_UOvrPayXtim
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_UOvrPayXtim
RETURN @stat
GO


