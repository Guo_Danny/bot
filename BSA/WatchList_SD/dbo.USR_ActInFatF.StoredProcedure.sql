USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_ActInFat]    Script Date: 10/19/2018 11:13:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[USR_ActInFat] (@WLCode SCode, 
				     @testAlert INT, 
                                     @fatF VARCHAR(8000), 
                                     @WhereToSearch VARCHAR(7),
				     @minIndAmt money,
				     @activityTypeList varchar(8000),
				     @UseMonitor smallint) AS
	Declare @Desc 		VarChar(2000)
	Declare @ID 		int
	Declare @stat 		int
	Declare @trnCnt 	int
	DECLARE @WLType	INT
	Declare @TempTbl	Table( TranNo int, Cust VarChar(40), alertDate datetime )
	Declare @bookdate INT
	DECLARE @fatFCountry Table (country VARCHAR(40))
	DECLARE @commaLoc INT
	DECLARE @lastCommaLoc INT
	DECLARE @currentDate DATETIME
	DECLARE @cFatF VARCHAR(8000)

	SET @currentDate = GETDATE()
	SET @bookdate = dbo.ConvertSQLDateToInt(@currentDate)
	-- Start standard stored procedure transaction header
	select @trnCnt = @@trancount	-- Save the current trancount
	If @trnCnt = 0
		-- Transaction has not begun
		begin tran USR_ActInFat
	else
		-- Already in a transaction
		save tran USR_ActInFat
	-- End standard stored procedure transaction header

	DECLARE @ActType TABLE (
		Type	INT
	)

	SELECT @activityTypeList = dbo.BSA_fnListParams(@activityTypeList)

	INSERT INTO @ActType
	SELECT 	Type  FROM vwRuleNonExmActType
	WHERE	(@activityTypeList IS NULL 
		OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@activityTypeList) > 0)	

	SET @cfatf = @fatf + ','
	SET @lastCommaLoc = 0
	SET @commaLoc = CHARINDEX(',', @cFatF)
	WHILE @commaLoc <> 0 
	   BEGIN
		IF @commaLoc - @lastCommaLoc - 1 > 0
			INSERT INTO @fatFCountry 
				VALUES(SUBSTRING(@cFatF, @lastCommaLoc + 1, 
				@commaLoc - @lastCommaLoc - 1))
		SET @lastCommaLoc = @commaLoc
		SET @commaLoc = CHARINDEX(',', @cFatF, @lastCommaLoc + 1)
	   END

	IF @UseMonitor = 1
	BEGIN
		SET @cFatF = ''
		SELECT @cFatF = COALESCE(@cFatF + ',', '') 
				+ rtrim(code)
 			FROM Country 
			WHERE Monitor = 1

		IF LEN(LTRIM(RTRIM(@cFatF)))> 0
			SELECT @cFatF=@cFatF+','
		ELSE
			SELECT @cFatF = 'NONE'

		INSERT INTO @fatFCountry(country) (Select code from country where monitor = 1)
	END


	Select @WLTYPE = WLType, @Desc = [Desc] From Watchlist Where WLCode = @WLCode

	Insert into @TempTbl (TranNo, Cust, AlertDate)
		Select tranno, cust, @currentDate
	   From Activity a
	   INNER JOIN @ActType act ON a.Type = act.Type
		WHERE
		a.BaseAmt >= @minIndAmt AND
		((@WhereToSearch <> 'Address' AND (
			CHARINDEX(ByOrderCountry, @cFatF) > 0 OR 
			CHARINDEX(ByOrderBankCountry, @cFatF) > 0 OR 
			CHARINDEX(BeneCountry, @cFatF) > 0 OR 
			CHARINDEX(BeneBankCountry, @cFatF) > 0 OR 
			CHARINDEX(IntermediaryCountry, @cFatF) > 0 OR 
			CHARINDEX(IntermediaryCountry2, @cFatF) > 0 OR 
			CHARINDEX(IntermediaryCountry3, @cFatF) > 0 OR 
			CHARINDEX(IntermediaryCountry4, @cFatF) > 0 

		)) OR (@WhereToSearch <> 'Country' AND (
		EXISTS (SELECT country FROM @fatFCountry b WHERE
			(a.ByOrderAddress LIKE ('%' + b.country + '%') AND
			 ISNULL(a.ByOrderAddress, '') <> '') OR 
			(a.ByOrderBankAddress LIKE ('%' + b.country + '%') AND
			 ISNULL(a.ByOrderBankAddress, '') <> '') OR 
			(a.BeneAddress LIKE ('%' + b.country + '%') AND
			 ISNULL(a.BeneAddress, '') <> '') OR 
			(a.BeneBankAddress LIKE ('%' + b.country + '%') AND
			 ISNULL(a.BeneBankAddress, '') <> '') OR 
			(a.IntermediaryAddress LIKE ('%' + b.country + '%') AND
			 ISNULL(a.IntermediaryAddress, '') <> '') OR 
			(a.IntermediaryAddress2 LIKE ('%' + b.country + '%') AND
			 ISNULL(a.IntermediaryAddress2, '') <> '') OR 
			(a.IntermediaryAddress3 LIKE ('%' + b.country + '%') AND
			 ISNULL(a.IntermediaryAddress3, '') <> '') OR 
			(a.IntermediaryAddress4 LIKE ('%' + b.country + '%') AND
			 ISNULL(a.IntermediaryAddress4, '') <> ''))
		)))

IF EXISTS( SELECT * FROM @TempTbl) BEGIN

IF @testAlert = 1 BEGIN
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT, isTest) 
	  SELECT DISTINCT @WLCode, 'Customer: ''' + Cust + 
			''' has activity dealing with one of the following countries: ' + @cfatf + 
                        ' and exceeding minimum transaction amount of ' + convert(varchar(50), @minIndAmt), 0, 
			@currentDate, NULL, NULL, CUST, NULL, 1 
			FROM @TempTbl


	SELECT @STAT = @@ERROR	
	IF @STAT <> 0  GOTO ENDOFPROC

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT DISTINCT 'ALERT', AlertNo, TRANNO 
		FROM @tempTbl tt JOIN Alert a 
		ON tt.Cust = a.Cust AND a.CreateDate >= tt.alertDate
	
	SELECT @STAT = @@ERROR 
	IF @STAT <> 0  GOTO ENDOFPROC
END ELSE BEGIN
	IF @WLTYPE = 0 BEGIN

	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT, isTest) 
	  SELECT DISTINCT @WLCode, 'Customer: ''' + Cust + 
		''' has activity dealing with one of the following countries: ' + @cfatf+ 
                        ' and exceeding minimum transaction amount of ' + convert(varchar(50), @minIndAmt), 0, 
			@currentDate, NULL, NULL, CUST, NULL, 0
 	  FROM @TempTbl

	SELECT @STAT = @@ERROR	

	IF @STAT <> 0  GOTO ENDOFPROC
	END ELSE IF @WLTYPE = 1 BEGIN
		INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			WLCode, WLDESC, CREATETIME )
		SELECT DISTINCT NULL, @bookdate, Nullif(Cust,''), NULL,
			NULL, 'RULE', NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, 0, 0, 0, 0, 
			NULL, NULL, NULL, NULL, 0, NULL, NULL,
			@WLCode, 'Customer: ''' + Cust + 
			''' has activity dealing with one of the following countries: ' + @cfatf+ 
                        ' and exceeding minimum transaction amount of ' + convert(varchar(50), @minIndAmt), 
			@currentDate 
		FROM @TempTbl
		
	SELECT @STAT = @@ERROR	
	IF @STAT <> 0  GOTO ENDOFPROC
	END
	
	IF @WLTYPE = 0 BEGIN
	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	   SELECT DISTINCT 'ALERT', AlertNo, TRANNO 
		FROM @tempTbl tt JOIN Alert a 
		ON tt.Cust = a.Cust AND a.CreateDate >= tt.alertDate

	
	SELECT @STAT = @@ERROR 
	END ELSE IF @WLTYPE = 1 BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   SELECT DISTINCT 'SUSPACT', RecNo, TRANNO 
		FROM @tempTbl tt JOIN SuspiciousActivity a ON tt.Cust = a.Cust AND 
                 a.CreateTime >= tt.alertDate
	SELECT @STAT = @@ERROR 
	END
END
END

EndOfProc:

	If ( @stat <> 0 ) begin 
		rollback tran USR_ActInFat
		return @stat
	end	

	If @trnCnt = 0
		commit tran USR_ActInFat
	return @stat
GO
