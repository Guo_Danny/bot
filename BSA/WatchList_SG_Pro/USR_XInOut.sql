USE [PBSA]
GO

/****** Object:  StoredProcedure [dbo].[USR_XInOut]    Script Date: 9/5/2023 9:55:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[USR_XInOut] (
	@WLCode 		SCode, 
	@testAlert 		INT,
	@day  			INT,
	@percent  		INT,
	@ActivityTypeList 	VARCHAR(8000),
	@minAmount 		MONEY
)
AS
	/* RULE AND PARAMETER DESCRIPTION
		Detects customers who had incoming transactions for a selected 
		activity type and this is followed by outgoing transactions for the same 
		activity type, where the difference between the incoming dollar total and 
		outgoing dollar total is less than or equal to the designated percentage 
		within a specified time frame. Designed to be run Post-EOD.	
	
		@day -- No of days activity to be considered
		@percent -- percentage difference in amount of deposits or withdrawals
		@ActivityTypeList  -- Comma separated list of activity types to be considered
		@minAmount  -- Minimum amount to consider for selection	
		
	*/
	
	/*  Declarations */
	DECLARE	@description VARCHAR(2000),
		@desc VARCHAR(2000),
		@Id INT, 
		@WLType INT,
		@stat INT,
		@trnCnt INT,
		@MINDATE INT,
		@ArchDate INT,
		@lBound int,
		@uBound int,
		@STARTALRTDATE  DATETIME,
		@ENDALRTDATE    DATETIME
	DECLARE @cur CURSOR
	Declare @cust LONGNAME,
		@bookdate INT,
		@recv MONEY,
		@date int,
		@pay MONEY,
		@payAmt MONEY,
		@recvAmt MONEY
	DECLARE	@TT TABLE 
	(
		Cust 	 VARCHAR(40),
		BookDate INT,
		Recv 	 MONEY,
		Pay 	 MONEY
	)
	
	-- Temporary table of Activity Types that have not been specified as Exempt
	DECLARE @ActType TABLE 
	(
		Type	INT
	)

	SET NOCOUNT ON
	SET @stat = 0
	SET @lbound = 100 - @percent
	SET @ubound = 100 + @percent
	--- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */
	SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
	IF @trnCnt = 0
		-- Transaction has not begun
		BEGIN TRAN USR_XInOut
	ELSE
		-- Already in a transaction
		SAVE TRAN USR_XInOut
	/* End standard stored procedure transaction header */

	Declare @BaseCurr char(3)
	select @BaseCurr = IsNULL(BaseCurr,'') from SysParam
	
	/*  standard Rules Header */
	SELECT @description = [Desc], @WLType = WLType  
	FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE,@StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM 	dbo.WatchList
	WHERE 	WLCode = @WlCode

	set @BookDate = dbo.ConvertSqlDateToInt(@startDate)
	
	SELECT @day = ABS(@day)
	
	--a week to to today
	SET @minDate = dbo.ConvertSqlDateToInt(
		DATEADD(d, -1 * @day, CONVERT(VARCHAR, @startDate)))
	
	--2 weeks to weekago
	SET @archDate = dbo.ConvertSqlDateToInt(
		DATEADD(d, -1 * 2 * @day, CONVERT(VARCHAR, @startDate)))

	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	
	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	INSERT INTO @TT(Cust, Recv, Pay)
		SELECT Tbl.Cust, SUM(Tbl.Recv), SUM(Tbl.Pay)
		FROM (	
			SELECT 	Cust, BookDate,
				CASE WHEN recvpay = 1 THEN SUM(baseamt) 
				ELSE 0
				END recv,
				CASE WHEN recvpay = 2 THEN SUM(baseamt) 
				ELSE 0
				END pay
			FROM 	ActivityHist Ach(NOLOCK) 
			INNER	JOIN @ActType Act ON Act.Type = Ach.Type
			WHERE 	Ach.BookDate >=@minDate  AND Ach.BookDate < @BookDate
			AND 	BaseAmt >= @minAmount
			--2018.07 SG request to exclude noncust
			AND     Ach.Cust not like 'NONCUST%'
			GROUP 	BY Cust, BookDate, RecvPay	
	) Tbl
	GROUP BY Tbl.Cust
	
	SET @cur = CURSOR FAST_FORWARD FOR 
	SELECT 	Cust, Recv, pay
	FROM 	@TT
	OPEN @cur
	FETCH NEXT FROM @Cur INTO @cust, @recv, @pay
	WHILE @@FETCH_STATUS = 0
		BEGIN
	
		SET @date = dbo.ConvertSqlDateToInt(
			DATEADD(d, -1 * @day, CONVERT(VARCHAR, @startDate)))
			--2018.07 modify DATEADD(d, -1 * @day, CONVERT(VARCHAR, @bookdate)))
	
		SELECT 	@recvAmt = SUM(a.recv), @payAmt = SUM(a.pay)
		FROM 	@TT A
		WHERE	@cust = a.cust
		HAVING 	SUM(A.Recv) > 0 AND SUM(A.Pay) > 0 
	
		IF @@ROWCOUNT > 0 
		BEGIN
			IF @ubound >= 100 * @payAmt/@recvAmt AND 
			   @lbound <= 100 * @payAmt/@recvAmt 
			BEGIN
				SET @desc = 'Cust:  ''' + @cust 
					+ ''' has incoming transaction amount '
					+ DBO.BSA_InternalizationMoneyToString ( @recvAmt) + ' ' + @BaseCurr +
					+ ' followed by an outgoing transaction amount '
					+ DBO.BSA_InternalizationMoneyToString ( @payAmt) + ' ' + @BaseCurr +
					+ ' where the percentage '
					+ 'differs by '+ CONVERT(VARCHAR, @percent) 
					+ ' percent from bookdate ''' 
					+ dbo.BSA_InternalizationIntToShortDate (@date) + ''' to '''
					+ dbo.BSA_InternalizationIntToShortDate (@bookdate) + '''' 
					+ ' greater than ' 
					+ DBO.BSA_InternalizationMoneyToString ( @minAmount) + ' ' + @BaseCurr

				IF @testAlert = 1 
				BEGIN
					EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, 
							@desc, @cust, NULL, 1		
					IF @stat <> 0 GOTO EndOfProc
					
					INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
						SELECT 	'Alert', @ID, TRANNO 
						FROM 	ACTIVITYHIST A (NOLOCK) 
						INNER	JOIN @ActType Act ON Act.Type = A.Type
						WHERE 	@BookDate >= A.BookDate AND A.Bookdate >= @Date
						AND 	A.Cust = @Cust 
						AND 	A.BaseAmt >= @minAmount
						SELECT @STAT = @@ERROR 
						IF @stat <> 0 GOTO EndOfProc
				END 
				ELSE 
				BEGIN
					IF @WLType = 0 
					BEGIN
						EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, 
								@desc, @cust, NULL		
						IF @stat <> 0 GOTO EndOfProc
						INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
							SELECT 	'Alert', @ID, TRANNO 
							FROM 	ACTIVITYHIST A (NOLOCK) 
							INNER	JOIN @ActType Act ON Act.Type = A.Type
							WHERE 	@BookDate >= A.BookDate AND A.Bookdate >= @Date
							AND 	A.Cust = @Cust 
							AND 	A.BaseAmt >= @minAmount
							SELECT @STAT = @@ERROR 
						IF @stat <> 0 GOTO EndOfProc
					END 
					ELSE IF @WLType = 1 
					BEGIN	
						EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT, 
							@WLCode, @desc, @bookdate, @cust, Null
						IF @stat <> 0 GOTO EndOfProc
						INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
							SELECT 	'SUSPACT', @ID, TRANNO 
							FROM 	ACTIVITYHIST A (NOLOCK) 
							INNER	JOIN @ActType Act ON Act.Type = A.Type
							WHERE 	@BookDate >= A.BookDate and a.bookdate >= @date
							AND 	A.Cust = @Cust 
							AND 	A.BaseAmt >= @minAmount						
						SELECT @STAT = @@ERROR 
						IF @stat <> 0 GOTO EndOfProc
					END
					
				END
			END 
			ELSE 
			BEGIN
				GOTO SkipRec
			END
		END	
	SkipRec:
		FETCH NEXT FROM @cur INTO @cust, @recv, @pay
		END
	
	CLOSE @cur
	DEALLOCATE @cur
	
	EndOfProc:
	IF (@stat <> 0) BEGIN 
	  ROLLBACK TRAN USR_XInOut
	  RETURN @stat
	END	
	
	IF @trnCnt = 0
	  COMMIT TRAN USR_XInOut
	RETURN @stat
	

GO


