USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[USR_UBneEQByOrd]    Script Date: 10/6/2017 11:11:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[USR_UBneEQByOrd](@WLCode SCode, @testAlert INT,
	@minIndAmt money, 
	@minCount int,
	@activityTypeList varchar(8000),
	@riskClassList varchar(8000),
	@days int
	)
AS
/* RULE AND PARAMETER DESCRIPTION
Description:Detects activity when the name in the beneficiary data field is the
same as the name in the by order data field when transactions in the same 
amount occur in a specified time period and the amount exceeds the specified 
minimum amount. Checks for activity on current day and looks back for activity
for the specified period. Alerts are grouped based on ByOrder, Amount. 
Designed to be run Pre-EOD.


Parameters:		
	@WLCode [in] SCode = Rule Code				
	@testAlert = Test Alert or Not (1 for Test, 0 for No)
	@minIndAmt = The Minimum amount the individual transaction must exceed 
		to be included in the aggregation  
   	@minCount = The Minimum transaction count	
	@activityTypeList = A comma separated list of Activity Types to include 
		in the evaluation, use -ALL- for any activity type.    	
	@riskClassList = A comma separated list of Risk Classes to include in the 
		evaluation, use -ALL- for any risk class.
	@days = Number of days to look back	(Calendar Days)
*/
	
/*  Declarations */
DECLARE	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@tranAmt MONEY,
	@activityCount  INT,
	@currentTransactionCount INT,
	@IndAmount  MONEY,
	@fromDate INT

DECLARE @SetDate DATETIME
DECLARE	@TT TABLE (
	tranAmt MONEY,
	tranNo INT,
	byOrder  VARCHAR(40) 
	
)

SET NOCOUNT ON
SET @stat = 0
--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @currentTransactionCount = @@TRANCOUNT	-- Save the current trancount
IF @currentTransactionCount = 0
	-- Transaction has not begun
	BEGIN TRAN USR_UBneEQByOrd
ELSE
	-- Already in a transaction
	SAVE TRAN USR_UBneEQByOrd
/* End standard stored procedure transaction header */

-- If UseSysDate = 0 or 1 then use current/system date
-- if UseSysDate = 2 then use Business date from Sysparam

SELECT @WLType = WLType ,
       @SetDate =
       CASE
               WHEN UseSysDate in (0,1) THEN
                       -- use System date
                       GetDate()
               WHEN UseSysDate = 2 THEN
                       -- use business date
                       (SELECT BusDate FROM dbo.SysParam)
               ELSE
                       GetDate()
       END
FROM dbo.WatchList (NOLOCK)
WHERE WLCode = @WLCode

--Making the List variables Null, if value is '' or -ALL-
--Else Removing space and Trimming
IF (ISNULL(@riskClassList,'') = '' 
	OR UPPER(ISNULL(@riskClassList,'-ALL-')) = '-ALL-')
	SELECT @riskClassList = NULL
ELSE
	SELECT @riskClassList = ',' + 
		REPLACE(LTRIM(RTRIM(@riskClassList)),CHAR(32),'') + ','

IF (ISNULL(@activityTypeList,'') = '' 
	OR UPPER(ISNULL(@activityTypeList,'-ALL-')) = '-ALL-')
	SELECT @activityTypeList = NULL
ELSE
	SELECT @activityTypeList = ',' + 
		REPLACE(LTRIM(RTRIM(@activityTypeList)),CHAR(32),'') + ','

--Subtracting 1, to look at @days-1 days Transactions from ActivityHist Table
--and 1 day from Activity Table
SELECT @days = ABS(@days) - 1

--SET @fromDate = dbo.ConvertSqlDateToInt(
--	DATEADD(d, -1 * @days, CONVERT(VARCHAR, @SetDate))) 

/* modify by danny 2017.05.30 get fromdate */
DECLARE @SQLStartDate datetime, @SQLEndDate datetime, @endDate int
exec dbo.BSA_GetDateRange @SetDate, 'Month', 'PREV', 1, 
	@SQLStartDate OUTPUT, @SQLEndDate OUTPUT
SET @fromDate = dbo.ConvertSqlDateToInt(@SQLStartDate)
SET @endDate = dbo.ConvertSqlDateToInt(@SQLEndDate)

--Exists clause is used to pick from history, only if corresponding 
--record(s) present in Activity

INSERT INTO @TT (tranAmt, tranNo, byOrder)
SELECT a.BaseAmt, a.tranNo, a.byOrder
FROM Activity a (NOLOCK), Customer (NOLOCK)
WHERE a.cust = Customer.Id AND
	a.bookdate >= @fromDate AND
	a.bookdate <= @endDate AND
	a.BaseAmt >= @minIndAmt AND
    a.byOrder IS NOT NULL and 
	a.bene IS NOT NULL AND 
	(a.byOrder = a.bene or a.byOrderCustId = a.beneCustId) AND 
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
	(@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0)
Union
SELECT ah.BaseAmt, ah.tranNo, ah.byOrder
FROM ActivityHist ah (NOLOCK), Customer (NOLOCK)
WHERE ah.cust = Customer.Id AND
	ah.bookdate >= @fromDate AND 
	ah.BaseAmt >= @minIndAmt AND
    ah.byOrder IS NOT NULL and 
	ah.bene IS NOT NULL AND  
	(ah.byOrder = ah.bene or ah.byOrderCustId = ah.beneCustId) AND
	(@activityTypeList IS NULL OR 
		CHARINDEX(',' + 
			CONVERT(VARCHAR, ah.type) + ',',@activityTypeList) > 0) AND
	(@riskClassList IS NULL OR 
		CHARINDEX(',' + 
			LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0) AND
	Exists
	(SELECT a.tranNo
		FROM Activity a (NOLOCK), Customer (NOLOCK)
		WHERE
		a.bookdate >= @fromDate AND 
		a.Bene = ah.Bene AND
		a.ByOrder = ah.ByOrder AND
		a.cust = Customer.Id AND
		a.BaseAmt >= @minIndAmt AND
	    a.byOrder IS NOT NULL and 
		a.bene IS NOT NULL AND  
		(a.byOrder = a.bene or a.byOrderCustId = a.beneCustId) AND
		(@activityTypeList IS NULL OR 
			CHARINDEX(',' + 
				CONVERT(VARCHAR, a.type) + ',',@activityTypeList) > 0) AND
		(@riskClassList IS NULL OR 
			CHARINDEX(',' + 
				LTRIM(RTRIM(Customer.RiskClass)) + ',', @riskClassList ) > 0)
	)
DECLARE	@cur CURSOR
DECLARE @byOrder LONGNAME,
		@bookdate INT

SET @bookdate = dbo.ConvertSQLDateToInt(GETDATE())

SET @cur = CURSOR FAST_FORWARD FOR 
SELECT SUM(tranAmt) tranAmt, COUNT(tranNo) tranCnt, a.byOrder, a.TranAmt
FROM @TT a GROUP BY  a.byOrder, a.tranAmt
HAVING count(tranno) >= @minCount

OPEN @cur 
FETCH NEXT FROM @cur INTO @tranAmt, @activityCount , @byOrder, @IndAmount

WHILE @@FETCH_STATUS = 0 BEGIN

	SET @desc = @byOrder + '(ByOrder) is performing '
		+ CONVERT(VARCHAR, @activityCount ) + ' transaction(s) '  
		+ 'to self with the same amount ' + CONVERT(VARCHAR, @IndAmount)
		+ ' where the total amount is ' + CONVERT(VARCHAR, @tranAmt)
		+ ' over a period of a Month'
	IF @testAlert = 1
	BEGIN
		EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
		NULL, NULL, 1
		IF @stat <> 0 GOTO EndOfProc
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 --Alert
		BEGIN
			EXECUTE @stat = API_InsAlert @ID OUTPUT, @WLCode, @desc,
				NULL, NULL, 0
			IF @stat <> 0 GOTO EndOfProc
		END 
		ELSE 
		IF @WLTYPE = 1 --Case
		BEGIN
			EXECUTE @stat = API_InsSuspiciosActivity @ID OUTPUT, 
				@WLCode, @desc, @bookdate, Null, NULL
			IF @stat <> 0 GOTO EndOfProc   	
		END	
	END		
	IF (@WLTYPE = 0) OR (@testAlert = 1)
	BEGIN
		--Inserting records into SASACTIVITY for the @ID
		--obtained from API_InsAlert stored Procedure
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'Alert', @ID, TRANNO 
			FROM @TT t
			WHERE  t.byOrder = @byOrder AND
			t.tranAmt = @IndAmount
	
			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC
	END 
	ELSE 
	IF @WLTYPE = 1 --Case
	BEGIN
		--Inserting records into SASACTIVITY for the @ID
		--obtained from API_InsSuspiciosActivity stored Procedure
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 'SUSPACT', @ID, TRANNO 
				FROM @TT t
			WHERE  t.byOrder = @byOrder AND
			t.tranAmt = @IndAmount

			SELECT @STAT = @@ERROR 
			IF @STAT <> 0 GOTO ENDOFPROC 
	END
	FETCH NEXT FROM @cur INTO @tranAmt, @activityCount , @byOrder, @IndAmount
END --Cursor While loop Ends

CLOSE @cur
DEALLOCATE @cur

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_UBneEQByOrd
  RETURN @stat
END	

IF @currentTransactionCount = 0
  COMMIT TRAN USR_UBneEQByOrd
RETURN @stat

GO
