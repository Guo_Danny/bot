USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[USR_UAcctMthVol]    Script Date: 10/6/2017 11:03:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE	[dbo].[USR_UAcctMthVol]	(@WLCode SCode, 
									@TestAlert  INT,
									@activityTypeList VARCHAR(8000), 
									@CustTypeList VARCHAR(8000),
									@RecvPay SMALLINT,
									@minTotalAmount MONEY, 
									@minCount INT,
									@NumMonths INT,
									@avgCount INT,									
									@InclLastMth INT
									)
AS

/* RULE AND PARAMETER DESCRIPTION
		Detects accounts where incoming or outgoing transactions of specified activity types exceeding 
		certain transaction count threshold and exceeding certain aggregated dollar amount threshold 
		per month and the monthly volume exceeding average monthly volume by a certain multiple.

		@activityTypeList - Type of activity to be detected.
		@CustTypeList - List of Customer Types that will be considered. 
						   -ALL-, blank or NULL for all Customer Types
		@minTotalAmount   - minimum Aggregate Amount
		@minCount   - Minimum Transaction Count
		@avgCount   - Average Count of transactions. Only months that had transactions will be counted 
					for an activity period of 12 months.
		@NumMonths - Number of Months
		@InclLastMth - Include Last Month in Average 1-Yes 0-No
*/

/*  Declarations */

	DECLARE @description      VARCHAR(2000),
		@desc             VARCHAR(2000),
		@Id         	  INT, 
		@WLType     	  INT,
		@stat       	  INT,
		@trnCnt     	  INT,
		@minDate    	  INT,
		@maxDate    	  INT,
		@beginDate  	  INT,
		@MinDateAvg 	  INT,
		@MaxDateAvg 	  INT,
		@ActivityType	  INT,
		@Pos		  INT

	DECLARE @STARTALRTDATE  DATETIME
	DECLARE @ENDALRTDATE    DATETIME

	DECLARE     @TT TABLE 
	( 
		Account     VARCHAR(40),
		RecvPay	   	SMALLINT,
		BaseAmt     MONEY,
		TotActCnt   INT
	)
	DECLARE @ActType TABLE
	(
		Type INT
	)
	DECLARE     @TT1 TABLE 
	(
		Account     VARCHAR(40),
		RecvPay     INT,
		TotActCnt	INT,
		AvgCount	INT,
		TotTranAmt  MONEY,
		AvgAmount	MONEY
	)
	
	SET NOCOUNT ON
	SET @stat = 0

	DECLARE @StartDate DATETIME
	set @StartDate =GETDATE()

	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam

	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE, @StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode

	SELECT @MINDATE = dbo.FirstDayOfMonth(DATEADD(MONTH, -1,@StartDate))

	SET @MinDate = dbo.FirstDayOfMonth(DATEADD(MONTH, -1,@StartDate))
	SET @MaxDate = dbo.FirstDayOfMonth(@StartDate)

	IF @InclLastmth = 1
	BEGIN
		SET @MaxDateAvg = @MaxDate
		SET @MinDateAvg = dbo.ConvertSqlDateToInt(DATEADD(MM, @NumMonths * -1, 
							dbo.BSA_ConvertintToSqlDate(@MaxDate,'MM/DD/YYYY')))
	END
	ELSE
	BEGIN
		SET @MaxDateAvg = @MinDate
		SET @MinDateAvg = dbo.ConvertSqlDateToInt(DATEADD(MM, @NumMonths * -1, 
							dbo.BSA_ConvertintToSqlDate(@MinDate,'MM/DD/YYYY')))
	END

	 --- ********************* BEGIN RULE PROCEDURE **********************************
	/* Start standard stored procedure transaction header */

	SET @trnCnt = @@TRANCOUNT     -- Save the current trancount
	IF @trnCnt = 0
	-- Transaction has not begun
		BEGIN TRAN USR_UAcctMthVol
	ELSE
	-- Already in a transaction
		SAVE TRAN USR_UAcctMthVol
	/* End standard stored procedure transaction header */

	IF (@RecvPay = 3)
		SET @RecvPay = NULL

	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)
	
	-- Get all the Activity Types that have not been exempted.
	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	-- Consider all transactions for accounts where the aggregate of transactions exceed 
	-- the minimum threshold amount (sepcified as parameter) and the transactions 
	-- count exceed the minimum transaction count (specified as parameter)

	INSERT INTO @TT ( Account, RecvPay,BaseAmt, TotActCnt)
		SELECT  Account, RecvPay,SUM(BaseAmt) BaseAmt, Count(*) TotActCnt 
		FROM	ActivityHist ACH (NOLOCK)
		INNER JOIN Account A ON ACH.Account = A.Id
		INNER JOIN @ActType Act ON ACH.Type = Act.Type
		WHERE	BookDate >= @MinDate AND BookDate < @MaxDate			
			AND 	RecvPay = ISNULL(@RecvPay, RecvPay)
			AND EXISTS (Select Id from Customer where Id = ACH.Cust  AND ((ISNULL(@CustTypeList, '') = '' OR  
							CHARINDEX(',' + LTRIM(RTRIM([type])) + ',', @CustTypeList ) > 0)))
		GROUP	BY Account,RecvPay
		HAVING	SUM(BaseAmt) > @minTotalAmount
			AND	COUNT(*) > @minCount

	INSERT INTO @TT1 (Account, RecvPay,TotActCnt,AvgCount,TotTranAmt,AvgAmount)
		SELECT	A.Account,A.RecvPay,TotActCnt,ISNULL(AVG(TranCnt),1),BaseAmt,AVG(TranAmt) 
		FROM	
		(
			SELECT ACH.Account,RecvPay,
			COUNT(*)  TranCnt,SUM(BaseAmt) TranAmt 
			FROM	ActivityHist ACH (NOLOCK)
				INNER JOIN @ActType Act ON ACH.Type = Act.Type
			WHERE EXISTS (SELECT Account FROM @TT T WHERE ACH.account = T.account)
			AND EXISTS (Select Id from Customer where Id = ACH.Cust  AND ((ISNULL(@CustTypeList, '') = '' OR  
							CHARINDEX(',' + LTRIM(RTRIM([type])) + ',', @CustTypeList ) > 0)))
			AND  RecvPay = ISNULL(@RecvPay, RecvPay)
			AND BookDate >= @MinDateAvg AND BookDate < @MaxDateAvg
			GROUP	BY ACH.Account, RecvPay,FLOOR(BookDate / 100) * 100 + 1
		) A INNER JOIN @TT T ON T.Account = A.Account
		WHERE A.RecvPay = T.RecvPay
		GROUP 	BY A.Account,A.RecvPay,TotActCnt,BaseAmt
		HAVING  (BaseAmt)/AVG(TranAmt) > @avgCount

	--1 means test, 0 means no 
	IF @testAlert = 1 Or @WLType = 0
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		INSERT INTO Alert ( WLCode, [Desc], Status, CreateDate, LastOper, LastModify, Cust, Account, IsTest) 
			SELECT	@WLCode, 'Account: ''' + T1.Account + ''' had ''' + CONVERT(VARCHAR, T1.TotActCnt) + '''' +
				CASE 
				WHEN T.RecvPay = 1 THEN  
				' incoming' 
				WHEN T.RecvPay = 2 THEN 
				' Outgoing' 
				END 
				 + ' transactions amount totaling $''' + CAST(BaseAmt AS VARCHAR) +  
				''' and the amount ''' + CAST(TotTranAmt AS VARCHAR) + ''' exceeds monthly average ''' + CAST(AvgAmount AS VARCHAR) +
 				' from ' + CAST(@MinDateAvg AS VARCHAR) + ' to ' + CAST(@MaxDateAvg AS VARCHAR) , 0, 
				GETDATE(), NULL, NULL, 
				(select top 1 cust from AccountOwner where Account = T1.Account), T1.Account , @testAlert
			FROM	@TT1 T1
				INNER	JOIN @TT T ON T.Account = T1.Account 
			WHERE	T1.RecvPay = T.RecvPay And T.RecvPay = ISNULL(@RecvPay,T.RecvPay)

		SELECT @STAT = @@ERROR 

		IF @stat <> 0 GOTO EndOfProc
		SELECT @ENDALRTDATE = GETDATE()

		INSERT INTO SasActivity (ObjectType, ObjectID, TranNo)
		SELECT  'Alert', AlertNo, TranNo
		FROM 	ActivityHist AH (NOLOCK) 
			INNER	JOIN  @TT T  ON T.Account = AH.Account 
			INNER JOIN @ActType Act ON AH.Type = Act.Type
			INNER	JOIN Alert Al ON Al.Account = AH.Account				
		WHERE	Al.WLCode = @WLCode
			AND AH.BookDate >= @minDate AND AH.BookDate < @maxDate
			AND	Al.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
			AND AH.RecvPay = T.RecvPay And 
			T.RecvPay = CASE WHEN Al.[Desc] like '%incoming%' THEN 1
							WHEN Al.[Desc] like '%outgoing%' THEN 2
						END
		SELECT @STAT = @@ERROR 
	    IF @STAT <> 0  GOTO ENDOFPROC             
	END 
	ELSE 
	IF @WLTYPE = 1 -- Create Case
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		--2017.10 mdy bookdate to firstdate of month by LN Request 
		Select @BeginDate =  dbo.ConvertSqlDateToInt(GETDATE())  
		--Select @BeginDate =  dbo.ConvertSqlDateToInt(@StartDate)
		INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
		ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
		RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
		ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
		EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
		OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
		REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
		WLCode, WLDESC, CREATETIME )
			SELECT 	NULL, @BeginDate, 
			    (select top 1 cust from AccountOwner where Account = T1.Account), 
			    T1.Account,NULL, 'RULE', NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL,
				@WLCode, 'Account: ''' + T1.Account + ''' had ''' + CONVERT(VARCHAR, T1.TotActCnt) + '''' +
				CASE WHEN T.RecvPay = 1 THEN  
				' incoming' 
				WHEN T.RecvPay = 2 THEN 
				' Outgoing' 
				END 
				 + ' transactions amount totaling $''' + CAST(BaseAmt AS VARCHAR) +  
				''' and the amount ''' + CAST(TotTranAmt AS VARCHAR) + ''' exceeds monthly average ''' + CAST(AvgAmount AS VARCHAR) +
 				' from ' + CAST(@MinDateAvg AS VARCHAR) + ' to ' + CAST(@MaxDateAvg AS VARCHAR) , GETDATE() 
			FROM 	@TT1 T1
			INNER 	JOIN @TT T ON T.Account = T1.Account 
			WHERE	T1.RecvPay = T.RecvPay And T.RecvPay = ISNULL(@RecvPay,T.RecvPay)

		SELECT @STAT = @@ERROR 

		IF @stat <> 0 GOTO EndOfProc
		SELECT @ENDALRTDATE = GETDATE()			
		
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)  
			SELECT 'SUSPACT', RecNo, TRANNO  
			FROM ActivityHist AH (NOLOCK) 
				INNER	JOIN  @TT T  ON T.Account = AH.Account 
				INNER JOIN @ActType Act ON AH.Type = Act.Type
				INNER	JOIN SuspiciousActivity SA ON AH.Account = SA.Account 
			WHERE	AH.BookDate >= @minDate AND AH.BookDate < @maxDate
				AND SA.WLCode = @WLCode
				AND	SA.CreateTime BETWEEN @STARTALRTDATE AND @ENDALRTDATE 
				AND AH.RecvPay = T.RecvPay 
				And T.RecvPay = CASE WHEN SA.[WLDESC] like '%incoming%' THEN 1
									WHEN SA.[WLDESC] like '%outgoing%' THEN 2
								END
		SELECT @STAT = @@ERROR 
		IF @STAT <> 0  GOTO ENDOFPROC
	END
EndOfProc:
IF (@stat <> 0) 
BEGIN 
	ROLLBACK TRAN USR_UAcctMthVol
	RETURN @stat
END   

IF @trnCnt = 0
	COMMIT TRAN USR_UAcctMthVol
	RETURN @stat

GO
