USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[USR_ActXAmtCnt]    Script Date: 10/6/2017 11:07:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[USR_ActXAmtCnt]
(	@WLCode 		SCODE, 
	@testAlert 		INT,
	@LastNoOfDays 		INT,  
	@ActivityTypeList 	VARCHAR(8000),   
	@ThresholdAmt		MONEY	,
	@NoOfTransactions	INT
)
AS
	/* Rule Parameters - These must be set according to the rule requirement*/

	/* Rule AND Parameter Description 
		Detects acounts that had more than X Transactions,using the 
		specified activity types(cash,wires etc),
		that has aggregate equal or greater to dollar threshold and 
		within X time frame.
		An alert or SA case is generated for each account that has more 
		than X transactions AND the total
		amount equals or exceeds the treshold amount over a certain time period.
		Designed to be run Post-EOD on a weekly,monthly or quarterly schedule.

	   @LastNoOfDays 	    -- Rule will consider all activities that occurred 
	   			    -- in the specified no of Days
	   @ActivityTypeList	    -- List of Activity Types that will be considered. 
	   			    -- Blank or NULL for all activity.
	   @ThresholdAmt            -- Rule checks if the aggregate of transaction amount in the 
	   			    -- specified last number of days is greater than 
	   			    -- or equal to @ThresholdAmt will be considered.
	   @NoOfTransactions	    -- The no of transactions for each account greater than @NoOfTransactions
	   			    -- will be considered.
	*/

	/*  DECLARATIONS */
	DECLARE	@DESCRIPTION VARCHAR(2000),
		@DESC VARCHAR(2000),
		@ID INT, 
		@WLTYPE INT,
		@STAT INT,
		@TRNCNT INT,
		@MINDATE INT, -- start date , current date is end date
		@startAlrtDate DATETIME,
		@endAlrtDate DATETIME,
		@CurrDate GenericDate,
		@wrkDate DATETIME

	DECLARE	@TT TABLE 
	(
		Cust		VARCHAR(40),
		Account 	VARCHAR(40),
		TranNo		INT,
		BaseAmt 	MONEY,
		BookDate 	INT
	)
	-- Temporary table of Activity Types that have not been specified as Exempt
	DECLARE @ActType TABLE (
		Type	INT
	)
	--- ********************* Begin Rule Procedure **********************************
	SET NOCOUNT ON
	SET @STAT = 0

	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE, @StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM 	dbo.WatchList
	WHERE 	WLCode = @WlCode

	SET @LastNoOfDays = ABS(@LastNoOfDays)
	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)

	--if user specifies 0, then it just evaluates the current Day
	-- IF @LastNoOfDays <> 0 BEGIN
	-- 	SET @LastNoOfDays = @LastNoOfDays - 1
	-- END

	SELECT @MINDATE = DBO.CONVERTSQLDATETOINT(
			DATEADD(D, -1 * @LastNoOfDays, CONVERT(VARCHAR, @StartDate)))

	/* START STANDARD STORED PROCEDURE TRANSACTION HEADER */
	SET @TRNCNT = @@TRANCOUNT	-- SAVE THE CURRENT TRANCOUNT
	IF @TRNCNT = 0
		-- TRANSACTION HAS NOT BEGUN
		BEGIN TRAN USR_ActXAmtCnt
	ELSE
		-- ALREADY IN A TRANSACTION
		SAVE TRAN USR_ActXAmtCnt
	/* END STANDARD STORED PROCEDURE TRANSACTION HEADER */


	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	INSERT INTO @TT ( Cust,Account,TranNo, BaseAmt, BookDate)
		SELECT 	Cust,Account, TranNo,BaseAmt, BookDate
		FROM 	ACTIVITYHIST Ach(NOLOCK)
		INNER	JOIN @ActType Act ON Act.Type = Ach.Type
		WHERE 	BOOKDATE BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate) 
		        AND account is not null
	
	IF @testAlert = 1 
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		 INSERT INTO Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
				LastModify, Cust, Account, IsTest) 
		  SELECT @WLCode, 'Account: ''' +  Account 
			+ ''' had ''' + CONVERT(VARCHAR,Count(TranNo))
			+ ''' transactions totaling ' + CONVERT(VARCHAR,SUM(BASEAMT))  
			+ ' between the period ' + CONVERT(VARCHAR, @MINDATE) + ' and ' +
			 CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(GETDATE()))+ '. 
		The total transaction amount of activities for an account exceeds threshold amount of ' + CONVERT(VARCHAR, @ThresholdAmt) + ''  , 
			0, GETDATE(), Null, Null, Cust, Account, 1 
		FROM  	@TT 
		GROUP 	BY Cust,Account
		HAVING	SUM(BaseAmt) >= @ThresholdAmt
		AND	COUNT(TRANNO) > @NoOfTransactions

		SELECT @STAT = @@ERROR	
		SELECT @ENDALRTDATE = GETDATE()
		if @STAT <> 0  GOTO ENDOFPROC
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	'Alert', AlertNo, TRANNO 
			FROM 	@TT t, Alert WHERE
				(t.Account = Alert.Account or Alert.Account IS NULL) AND Alert.WLCode = @WLCode 
			AND 	t.Cust = Alert.Cust AND Alert.CreateDate BETWEEN @startAlrtDate AND @endAlrtDate
		   SELECT @STAT = @@ERROR
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 
		BEGIN
			SELECT @STARTALRTDATE = GETDATE()
			 INSERT INTO Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
					LastModify, Cust, Account) 
			  SELECT @WLCode, 'Account: ''' +  Account 
			+ ''' had ''' + CONVERT(VARCHAR,Count(TranNo))
			+ ''' transactions totaling ' + CONVERT(VARCHAR,SUM(BASEAMT))  
			+ ' between the period ' + CONVERT(VARCHAR, @MINDATE) + ' and ' +
			 CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(GETDATE()))+ '. 
		The total transaction amount of activities for an account exceeds threshold amount of ' + CONVERT(VARCHAR, @ThresholdAmt) + ''  ,
				0, GETDATE(), Null, Null, Cust, Account
			From 	@TT 
			GROUP 	BY Cust,Account
			HAVING	SUM(BaseAmt) >= @ThresholdAmt
			AND	COUNT(TRANNO) > @NoOfTransactions

			SELECT @STAT = @@ERROR	
			SELECT @ENDALRTDATE = GETDATE()
			if @STAT <> 0  GOTO ENDOFPROC
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
			SELECT @wrkDate = GETDATE()
			Exec @CurrDate = BSA_CvtDateToLong @wrkDate
			SELECT @STARTALRTDATE = GETDATE()
			INSERT INTo SuspiciousActivity (ProfileNo, BookDate, Cust, Account, 
				Activity, SuspType, StartDate, EndDate, RecurType, 
				RecurValue, ActCurrReportAmt, ActInActCnt, ActOutActCnt, 
				ActInActAmt, ActOutActAmt, CurrReportAmt, ExpAvgInActCnt, 
				ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt, InCntTolPerc, 
				OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr, ReviewState, 
				ReviewTime, ReviewOper, App, AppTime, AppOper, 
				WLCode, WLDesc, CreateTime )

			SELECT	Null, @CurrDate, Cust, Account,
				Null, 'Rule', Null, Null, Null, Null,
				Null, Null, Null, Null, Null, Null, Null, Null,
				Null, Null, 0, 0, 0, 0, 
				Null, Null, Null, Null, 0, Null, Null,
				@WLCode, 'Account: ''' +  Account 
				+ ''' had ''' + CONVERT(VARCHAR,Count(TranNo))
				+ ''' transactions totaling ' + CONVERT(VARCHAR,SUM(BASEAMT))  
				+ ' between the period ' + CONVERT(VARCHAR, @MINDATE) + ''' and ''' +
				 CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(GETDATE()))+ '. 
		The total transaction amount of activities for an account exceeds threshold amount of ' + CONVERT(VARCHAR, @ThresholdAmt) + '' , 
				GETDATE()
			FROM 	@TT 
			GROUP 	BY Cust,Account
			HAVING 	SUM(BaseAmt) >= @ThresholdAmt
			AND	COUNT(TRANNO) > @NoOfTransactions

			SELECT @STAT = @@ERROR	
			SELECT @ENDALRTDATE = GETDATE()
			if @STAT <> 0  GOTO ENDOFPROC
		END
	
		IF @WLTYPE = 0 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	'Alert', AlertNo, TRANNO 
				FROM 	@TT t, Alert WHERE
					(t.Account = Alert.Account or Alert.Account is NULL) AND Alert.WLCode = @WLCode 
				AND 	t.Cust = Alert.Cust AND Alert.CreateDate BETWEEN @startAlrtDate AND @endAlrtDate
			SELECT @STAT = @@ERROR
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN 
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	'SUSPACT', RecNo, TRANNO 
				FROM 	@TT t, SuspiciousActivity s 
				WHERE	(t.Account = s.Account or s.Account is NULL) AND t.Cust = s.Cust AND 
					s.WLCode = @WLCode AND 	s.createTime BETWEEN @startAlrtDate AND @endAlrtDate
			SELECT @STAT = @@ERROR
		END
	END

	ENDOFPROC:
	IF (@STAT <> 0) BEGIN 
	  ROLLBACK TRAN USR_ActXAmtCnt
	  RETURN @STAT
	END	

	IF @TRNCNT = 0
	  COMMIT TRAN USR_ActXAmtCnt
	RETURN @STAT

GO
