USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[USR_CustCntAmtI]    Script Date: 10/6/2017 11:07:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[USR_CustCntAmtI]
(	@WLCode 		SCODE, 
	@testAlert 		INT,
	@LastNoOfDays 		INT,  
	@ActivityTypeList 	VARCHAR(8000),   
	@ThresholdAmt		MONEY	,
	@NoOfTransactions	INT,
	@RiskClassList 		VARCHAR (8000),
	@CustTypeList 		VARCHAR (8000),
	@AmtCompareOper 	SMALLINT,
	@branchList varchar(8000),
	@deptList varchar(8000)

)
AS
	/* Rule Parameters - These must be set according to the rule requirement*/

	/* Rule AND Parameter Description 
		Detects Customers that had more than X Transactions, using a 
		specified activity type(cash,wires etc), that has aggregate 
		equal or greater to dollar threshold, within specified time 
		frame. An alert or SA case is generated for each customer 
		that has more than X transactions and/or the total amount 
		equals or exceeds the threshold amount over a certain time 
		period. Filtered by Customer Type and Risk Class. Designed 
		to be run Post-EOD on a weekly, monthly or quarterly schedule.

	   @LastNoOfDays 	    -- Rule will consider all activities that occurred 
	   			    -- in the specified no of Days
	   @ActivityTypeList	    -- List of Activity Types that will be considered. 
	   			    -- Blank or NULL for all activity.
	   @ThresholdAmt            -- Rule checks if the aggregate of transaction amount in the 
	   			    -- specified last number of days is greater than 
	   			    -- or equal to @ThresholdAmt will be considered.
	   @NoOfTransactions	    -- The no of transactions for each Customer greater than @NoOfTransactions
	   			    -- will be considered.
	   @RiskClassList           -- List of Risk Classes that will be considered. 
				        -ALL-, blank or NULL for all Risk Classes
	   @CustTypeList	    -- List of Customer types that will be considered. 
			                -ALL-, blank or NULL for all types
	   @AmtCompareOper	    -- Miminum In/Out Amount Comparison operator
				        (1 for OR, 2 for AND)
	   @branchList		    -- A comma separated list of Branches to include in the evaluation.
	   @deptList		    -- A comma separated list of Departments to include in the evaluation.

	*/

	/*  DECLARATIONS */
	DECLARE	@DESCRIPTION VARCHAR(2000),
		@DESC VARCHAR(2000),
		@ID INT, 
		@WLTYPE INT,
		@STAT INT,
		@TRNCNT INT,
		@MINDATE INT, -- start date , current date is end date
		@startAlrtDate DATETIME,
		@endAlrtDate DATETIME,
		@CurrDate GenericDate,
		@wrkDate DATETIME

	DECLARE	@TT TABLE 
	(
		Cust		VARCHAR(40),
		TranNo		INT,
		BaseAmt 	MONEY,
		BookDate 	INT
	)
	-- Temporary table of Activity Types that have not been specified as Exempt
	DECLARE @ActType TABLE (
		Type	INT
	)
	--- ********************* Begin Rule Procedure **********************************
	SET NOCOUNT ON
	SET @STAT = 0

	-- Date options
	-- If UseSysDate = 0 or 1 then use current/system date
	-- IF UseSysDate = 2 then use Business date FROM Sysparam
	DECLARE @StartDate DATETIME
	
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE, @StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM 	dbo.WatchList
	WHERE 	WLCode = @WlCode

	SET @LastNoOfDays = ABS(@LastNoOfDays)
	-- Call BSA_fnListParams for each of the Paramters that support comma separated values	
	SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
	SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)
	SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)
	SELECT @branchList = dbo.BSA_fnListParams(@branchList)
	SELECT @deptList = dbo.BSA_fnListParams(@deptList)

	--if user specifies 0, then it just evaluates the current Day
	-- IF @LastNoOfDays <> 0 BEGIN
	-- 	SET @LastNoOfDays = @LastNoOfDays - 1
	-- END

	SELECT @MINDATE = DBO.CONVERTSQLDATETOINT(
			DATEADD(D, -1 * @LastNoOfDays, CONVERT(VARCHAR, @StartDate)))

	/* START STANDARD STORED PROCEDURE TRANSACTION HEADER */
	SET @TRNCNT = @@TRANCOUNT	-- SAVE THE CURRENT TRANCOUNT
	IF @TRNCNT = 0
		-- TRANSACTION HAS NOT BEGUN
		BEGIN TRAN USR_CustCntAmtI
	ELSE
		-- ALREADY IN A TRANSACTION
		SAVE TRAN USR_CustCntAmtI
	/* END STANDARD STORED PROCEDURE TRANSACTION HEADER */


	INSERT INTO @ActType
		SELECT 	Type  FROM vwRuleNonExmActType
		WHERE	(@ActivityTypeList IS NULL OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

	INSERT INTO @TT ( Cust,TranNo, BaseAmt, BookDate)
		SELECT 	Cust, TranNo,BaseAmt, BookDate
		FROM 	ACTIVITYHIST Ach(NOLOCK)
		INNER	JOIN @ActType Act ON Act.Type = Ach.Type
		INNER   JOIN Customer ON Customer.ID = Ach.Cust
		WHERE 	BOOKDATE BETWEEN @MINDATE  AND dbo.ConvertSqlDateToInt(@startDate) 
			AND ((ISNULL(@RiskClassList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.RiskClass)) + ',', @RiskClassList ) > 0))
			AND ((ISNULL(@CustTypeList, '') = '' OR
				CHARINDEX(',' + LTRIM(RTRIM(Customer.Type)) + ',', @CustTypeList ) > 0))
			AND (ISNULL(@deptList,'') = '' OR 
				CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Ach.dept,''))) + ',', @deptList) > 0)
			AND (ISNULL(@branchList,'') = '' OR 
				CHARINDEX(',' + LTRIM(RTRIM(ISNULL(Ach.branch,''))) + ',', @branchList) > 0)

	IF @testAlert = 1 
	BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		 INSERT INTO Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
				LastModify, Cust, IsTest) 
		  SELECT @WLCode, 'Customer: ''' +  LTRIM(RTRIM(Cust)) 
			+ ''' had ''' + CONVERT(VARCHAR,Count(TranNo))
			+ ''' transactions totaling ' + CONVERT(VARCHAR,SUM(BASEAMT))  
			+ ' between the period ' + CONVERT(VARCHAR, @MINDATE) + ' and ' 
			+ CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(GETDATE()))+ '. '
			+ ' The transactions exceed the threshold amount of '
			+ CONVERT(VARCHAR, @ThresholdAmt) 
			+ CASE @AmtCompareOper
					WHEN 1 THEN ' or'
					ELSE ' and'
			  END
			+ ' the threshold Count of ' + CONVERT(VARCHAR, @NoOfTransactions) + '.'  , 
			0, GETDATE(), Null, Null, Cust, 1 
		FROM  	@TT 
		GROUP 	BY Cust
		HAVING	
			( @AmtCompareOper = 1 AND (SUM(BaseAmt)>=@ThresholdAmt OR Count(TRANNO)>@NoOfTransactions))
			OR
			( @AmtCompareOper = 2 AND (SUM(BaseAmt)>=@ThresholdAmt AND Count(TRANNO)>@NoOfTransactions))


		SELECT @STAT = @@ERROR	
		SELECT @ENDALRTDATE = GETDATE()
		if @STAT <> 0  GOTO ENDOFPROC
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
			SELECT 	'Alert', AlertNo, TRANNO 
			FROM 	@TT t, Alert WHERE
				t.Cust = Alert.Cust AND Alert.WLCode = @WLCode 
			AND 	Alert.CreateDate BETWEEN @startAlrtDate AND @endAlrtDate
		SELECT @STAT = @@ERROR
	END 
	ELSE 
	BEGIN
		IF @WLTYPE = 0 
		BEGIN
			SELECT @STARTALRTDATE = GETDATE()
			 INSERT INTO Alert ( WLCode, [Desc], Status, CreateDate, LastOper, 
					LastModify, Cust) 
			  SELECT @WLCode, 'Customer: ''' +  LTRIM(RTRIM(Cust)) 
			+ ''' had ''' + CONVERT(VARCHAR,Count(TranNo))
			+ ''' transactions totaling ' + CONVERT(VARCHAR,SUM(BASEAMT))  
			+ ' between the period ' + CONVERT(VARCHAR, @MINDATE) + ' and ' 
			+ CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(GETDATE()))+ '. '
			+ ' The transactions exceed the threshold amount of '
			+ CONVERT(VARCHAR, @ThresholdAmt) 
			+ CASE @AmtCompareOper
					WHEN 1 THEN ' or'
					ELSE ' and'
			  END
			+ ' the threshold Count of ' + CONVERT(VARCHAR, @NoOfTransactions) + '.'  ,
				0, GETDATE(), Null, Null, Cust
			From 	@TT 
			GROUP 	BY Cust
			HAVING	
				( @AmtCompareOper = 1 AND (SUM(BaseAmt)>=@ThresholdAmt OR Count(TRANNO)>@NoOfTransactions))
				OR
				( @AmtCompareOper = 2 AND (SUM(BaseAmt)>=@ThresholdAmt AND Count(TRANNO)>@NoOfTransactions))

			SELECT @STAT = @@ERROR	
			SELECT @ENDALRTDATE = GETDATE()
			if @STAT <> 0  GOTO ENDOFPROC
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN
			SELECT @wrkDate = GETDATE()
			Exec @CurrDate = BSA_CvtDateToLong @wrkDate
			SELECT @STARTALRTDATE = GETDATE()
			INSERT INTo SuspiciousActivity (ProfileNo, BookDate, Cust, Account, 
				Activity, SuspType, StartDate, EndDate, RecurType, 
				RecurValue, ActCurrReportAmt, ActInActCnt, ActOutActCnt, 
				ActInActAmt, ActOutActAmt, CurrReportAmt, ExpAvgInActCnt, 
				ExpAvgOutActCnt, ExpMaxInActAmt, ExpMaxOutActAmt, InCntTolPerc, 
				OutCntTolPerc, InAmtTolPerc, OutAmtTolPerc, Descr, ReviewState, 
				ReviewTime, ReviewOper, App, AppTime, AppOper, 
				WLCode, WLDesc, CreateTime )

			SELECT	Null, @CurrDate, Cust, Null,
				Null, 'Rule', Null, Null, Null, Null,
				Null, Null, Null, Null, Null, Null, Null, Null,
				Null, Null, 0, 0, 0, 0, 
				Null, Null, Null, Null, 0, Null, Null,
				@WLCode, 'Customer: ''' +  LTRIM(RTRIM(Cust)) 
				+ ''' had ''' + CONVERT(VARCHAR,Count(TranNo))
				+ ''' transactions totaling ' + CONVERT(VARCHAR,SUM(BASEAMT))  
				+ ' between the period ' + CONVERT(VARCHAR, @MINDATE) + ' and ' 
				+ CONVERT(VARCHAR, DBO.CONVERTSQLDATETOINT(GETDATE()))+ '. '
				+ ' The transactions exceed the threshold amount of '
				+ CONVERT(VARCHAR, @ThresholdAmt) 
				+ CASE @AmtCompareOper
						WHEN 1 THEN ' or'
						ELSE ' and'
				  END
				+ ' the threshold Count of ' + CONVERT(VARCHAR, @NoOfTransactions) + '.'  , 
				GETDATE()
			FROM 	@TT 
			GROUP 	BY Cust
			HAVING	
				( @AmtCompareOper = 1 AND (SUM(BaseAmt)>=@ThresholdAmt OR Count(TRANNO)>@NoOfTransactions))
				OR
				( @AmtCompareOper = 2 AND (SUM(BaseAmt)>=@ThresholdAmt AND Count(TRANNO)>@NoOfTransactions))

			SELECT @STAT = @@ERROR	
			SELECT @ENDALRTDATE = GETDATE()
			if @STAT <> 0  GOTO ENDOFPROC
		END
	
		IF @WLTYPE = 0 
		BEGIN
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	'Alert', AlertNo, TRANNO 
				FROM 	@TT t, Alert WHERE
					t.Cust = Alert.Cust AND Alert.WLCode = @WLCode 
				AND 	 Alert.CreateDate BETWEEN @startAlrtDate AND @endAlrtDate
			SELECT @STAT = @@ERROR
		END 
		ELSE IF @WLTYPE = 1 
		BEGIN 
			INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
				SELECT 	'SUSPACT', RecNo, TRANNO 
				FROM 	@TT t, SuspiciousActivity s 
				WHERE	t.Cust = s.Cust AND 
					s.WLCode = @WLCode AND 	s.createTime BETWEEN @startAlrtDate AND @endAlrtDate
			SELECT @STAT = @@ERROR
		END
	END

	ENDOFPROC:
	IF (@STAT <> 0) BEGIN 
	  ROLLBACK TRAN USR_CustCntAmtI
	  RETURN @STAT
	END	

	IF @TRNCNT = 0
	  COMMIT TRAN USR_CustCntAmtI
	RETURN @STAT

GO
