USE [PBSA]
GO
/****** Object:  StoredProcedure [dbo].[USR_PepAcctAct]    Script Date: 10/6/2017 11:07:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USR_PepAcctAct] (@WLCode SCode, @testAlert INT, @PEPFEPMSB VARCHAR(12),
			@CustTypeList VARCHAR(8000),
			@RiskClassList VARCHAR(8000),
			@ActivityTypeList VARCHAR(8000),
			@MinTotalAmount MONEY,
			@Days INT
			)
AS

/*  Declarations */
DECLARE	@description VARCHAR(2000),
	@desc VARCHAR(2000),
	@Id INT, 
	@WLType INT,
	@stat INT,
	@bookDate INT,
	@trnCnt INT,
	@minDate INT,
	@StartDate DATETIME

DECLARE @STARTALRTDATE  DATETIME
DECLARE @ENDALRTDATE    DATETIME
DECLARE @IndvSelection VARCHAR(10)
DECLARE	@Pos INT
DECLARE @List VARCHAR(100) 
DECLARE @CustomerExemptionsList VARCHAR(8000)

-- Temporary table of Activity Types that have not been specified as Exempt
DECLARE @ActType TABLE (
	Type	INT
)

DECLARE	@TT TABLE (
	AlertText VARCHAR(20),
	CustId VARCHAR(35),
	CustName VARCHAR(40),
	BookDate INT, 
	ActNo VARCHAR(35),
	TranNo INT,
	BaseAmt MONEY
)

CREATE TABLE #tmp (
	Code CHAR(3),
	flag BIT)

	--Insert the values into the temp table based on the user input

	SET @PEPFEPMSB = LTRIM(RTRIM(@PEPFEPMSB))+ ','
	SET @Pos = CHARINDEX(',', @PEPFEPMSB, 1)

	-- Logic for seperating into individual values based upon the User Selection 

	IF REPLACE(@PEPFEPMSB, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
	 		SET @IndvSelection = LTRIM(RTRIM(LEFT(@PEPFEPMSB, @Pos - 1)))
  			IF @IndvSelection <> ''
				INSERT INTO #tmp VALUES (@indvSelection, 1)

			SET @PEPFEPMSB = RIGHT(@PEPFEPMSB, LEN(@PEPFEPMSB) - @Pos)
			SET @Pos = CHARINDEX(',', @PEPFEPMSB, 1)
		END
	END	 
	
SET NOCOUNT ON
SET @stat = 0

-- Date options
-- If UseSysDate = 0 or 1 then use current/system date
-- IF UseSysDate = 2 then use Business date FROM Sysparam
	SELECT @DESCRIPTION = [DESC], @WLTYPE = WLTYPE, @StartDate = 
		CASE 	
			WHEN UseSysDate in (0,1) THEN
				-- use System date
				GETDATE()
			WHEN UseSysDate = 2 THEN
				-- use business date
				(SELECT BusDate FROM dbo.SysParam)
			ELSE
				GETDATE()
		END
	FROM dbo.WatchList
	WHERE WLCode = @WlCode

SELECT @MINDATE = DBO.CONVERTSQLDATETOINT(
			DATEADD(d, -1 * ABS(@days), CONVERT(VARCHAR, @StartDate)))

-- CALL BSA_fnListParams for each of the Paramters that support comma separated values
SELECT @ActivityTypeList = dbo.BSA_fnListParams(@ActivityTypeList)
SELECT @CustTypeList = dbo.BSA_fnListParams(@CustTypeList)
SELECT @RiskClassList = dbo.BSA_fnListParams(@RiskClassList)

-- Get the list of exemptions that are exempted on customer from rules
SELECT @CustomerExemptionsList = dbo.BSA_GetExemptionCodes('Customer','Rule')
IF ltrim(rtrim(@CustomerExemptionsList)) = ''
	SET @CustomerExemptionsList = NULL

--- ********************* BEGIN RULE PROCEDURE **********************************
/* Start standard stored procedure transaction header */
SET @trnCnt = @@TRANCOUNT	-- Save the current trancount
IF @trnCnt = 0
	-- Transaction has not begun
	BEGIN TRAN USR_PepAcctAct
ELSE
	-- Already in a transaction
	SAVE TRAN USR_PepAcctAct
/* End standard stored procedure transaction header */

/*  standard Rules Header */
SELECT @description = [Desc], @WLType = WLType  
FROM WatchList (NOLOCK) WHERE WLCode = @WLCode

INSERT INTO @ActType
	SELECT 	Type  FROM vwRuleNonExmActType
	WHERE	(@ActivityTypeList IS NULL 
		OR CHARINDEX(',' + CONVERT(VARCHAR, Type) + ',',@ActivityTypeList) > 0)

INSERT INTO @TT(AlertText,CustId, CustName, BookDate, ActNo, TranNo,BaseAmt)
 SELECT 
	AlertText = dbo.GetFepMsbPep(C.FEP,C.MSB,C.PEP) , 
	C.[Id],--Customer Id
	C.[Name], --Customer Name
        A.BookDate,--TransDate
	A.Account,--Account No.
	A.TranNo, --Transaction No.
	A.BaseAmt --Transaction Amount
FROM	ActivityHist A INNER JOIN
	Customer  C ON A.Cust = C.[Id]
	INNER JOIN @ActType act ON A.Type = act.Type
WHERE 	(C.MSB = (Select flag from #tmp where code = 'MSB') 
	OR   C.FEP = (Select flag from #tmp where code = 'FEP')
	OR   C.PEP = (Select flag from #tmp where code = 'PEP')) 
	AND (ISNULL(@CustTypeList, '') = '' OR CHARINDEX(',' + ltrim(rtrim(C.Type)) + 
		',',   @CustTypeList ) > 0) 
	AND (ISNULL(@RiskClassList, '') = '' OR CHARINDEX(',' + ltrim(rtrim(C.RiskClass)) + 
		',',   @RiskClassList ) > 0)
	AND	(C.Exemptionstatus IS NULL OR @CustomerExemptionsList IS NULL OR
		CHARINDEX(',' + LTRIM(RTRIM(C.Exemptionstatus)) + ',', @CustomerExemptionsList ) = 0)
	AND A.bookdate between @mindate and DBO.CONVERTSQLDATETOINT(@startdate)
	
		
IF @testAlert = 1 BEGIN
	SELECT @STARTALRTDATE = GETDATE()
	INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
			LASTMODIFY, CUST, ACCOUNT, IsTest) 
 	SELECT  @WLCode, 'Customer: '+ CustName +' with ID:'+ CustId + ' is a '+ t.AlertText +
	' and has total activity ' + CONVERT(VARCHAR,Sum(BaseAmt)) +  
 	' in last '+ CONVERT(VARCHAR, @days) + ' days.', 0,GETDATE(), NULL, NULL, CustId, NULL, 1 
  	FROM @TT t
	GROUP BY CustId,CustName,Alerttext
	HAVING (@MinTotalAmount = -1 OR SUM(BaseAmt) >= @MinTotalAmount)

	SELECT @STAT = @@ERROR	
	SELECT @ENDALRTDATE = GETDATE()
	IF @STAT <> 0  GOTO ENDOFPROC

	INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
	SELECT 'Alert', AlertNO, TRANNO 
	FROM @TT t, Alert
	WHERE   t.CustId = Alert.Cust   
		AND Alert.WLCode = @WLCode 
		AND Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE

	SELECT @STAT = @@ERROR 
END 
ELSE BEGIN
	IF @WLTYPE = 0 BEGIN
		SELECT @STARTALRTDATE = GETDATE()
 
		INSERT INTO Alert ( WLCode, [DESC], STATUS, CreateDate, LASTOPER, 
				LASTMODIFY, CUST, ACCOUNT) 
	 	SELECT @WLCode, 'Customer: '+ CustName +' with ID:'+ CustId + ' is a '+ t.AlertText +
		' and has total activity ' + CONVERT(VARCHAR,Sum(BaseAmt)) +  
 		' in last '+ CONVERT(VARCHAR, @days) + ' days.', 0, 
			GETDATE(), NULL, NULL, CustId, NULL 
	  	FROM @TT t
		GROUP BY CustId,CustName,Alerttext
		HAVING (@MinTotalAmount = -1 OR SUM(BaseAmt) >= @MinTotalAmount)
 
		SELECT @STAT = @@ERROR	
		SELECT @ENDALRTDATE = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
	END 
	ELSE IF @WLTYPE = 1 BEGIN
		SELECT @STARTALRTDATE = GETDATE()
		SELECT @BookDate =  dbo.ConvertSqlDateToInt(GETDATE())
		INSERT INTO SUSPICIOUSACTIVITY (PROFILENO, BOOKDATE, CUST, ACCOUNT, 
			ACTIVITY, SUSPTYPE, STARTDATE, ENDDATE, RECURTYPE, 
			RECURVALUE, ACTCURRREPORTAMT, ACTINACTCNT, ACTOUTACTCNT, 
			ACTINACTAMT, ACTOUTACTAMT, CURRREPORTAMT, EXPAVGINACTCNT, 
			EXPAVGOUTACTCNT, EXPMAXINACTAMT, EXPMAXOUTACTAMT, INCNTTOLPERC, 
			OUTCNTTOLPERC, INAMTTOLPERC, OUTAMTTOLPERC, DESCR, REVIEWSTATE, 
			REVIEWTIME, REVIEWOPER, APP, APPTIME, APPOPER, 
			WLCode, WLDESC, CREATETIME ) 
		SELECT	NULL, @bookDate, CustId, NULL,
			NULL, 'RULE', NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, 0, 0, 0, 0, 
			NULL, NULL, NULL, NULL, 0, NULL, NULL,
			@WLCode, 'Customer: '+ CustName +' with ID:'+ CustId + ' is a '+ t.AlertText +
			' and has total activity ' + CONVERT(VARCHAR,Sum(BaseAmt)) +  
 			' in last '+ CONVERT(VARCHAR, @days) + ' days.', GETDATE() 
		FROM @TT t
		GROUP BY CustId,CustName,Alerttext
		HAVING (@MinTotalAmount = -1 OR SUM(BaseAmt) >= @MinTotalAmount)
 
 
		SELECT @STAT = @@ERROR	
		SELECT @ENDALRTDATE = GETDATE()
		IF @STAT <> 0  GOTO ENDOFPROC
	END
	
	IF @WLTYPE = 0 BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   SELECT 'Alert', AlertNO, TRANNO 
		   FROM @TT t, Alert 
 		   WHERE t.CustId = Alert.Cust   
			AND Alert.WLCode = @WLCode
			AND Alert.CreateDate BETWEEN @STARTALRTDATE AND @ENDALRTDATE
 
		SELECT @STAT = @@ERROR 
	END 
	ELSE IF @WLTYPE = 1 BEGIN
		INSERT INTO SASACTIVITY (OBJECTTYPE, OBJECTID, TRANNO)
		   SELECT 'SUSPACT', RECNO, TRANNO 
			FROM @TT t, SUSPICIOUSACTIVITY S 
			WHERE t.CustId = s.Cust    
			AND S.WLCode = @WLCode 
			AND S.CREATETIME BETWEEN @STARTALRTDATE AND @ENDALRTDATE
 
		SELECT @STAT = @@ERROR 
	END
END

EndOfProc:
IF (@stat <> 0) BEGIN 
  ROLLBACK TRAN USR_PepAcctAct
  RETURN @stat
END	

IF @trnCnt = 0
  COMMIT TRAN USR_PepAcctAct
RETURN @stat

GO
