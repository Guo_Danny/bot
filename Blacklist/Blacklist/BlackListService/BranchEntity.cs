﻿using System;
using System.Data.SqlClient;
using System.Data;

namespace BlackListService
{
    /// <summary>
    /// This object represents the properties and methods of a BranchEntity.
    /// </summary>
    public class BranchEntity
    {
        #region Private member variables
        private string branchCode = String.Empty;
        private string branchName = String.Empty;
        private string watchType = String.Empty;
        private string path = String.Empty;
        private string backup = "";
        private string outputFormat = "";

        private string cIFIN = "";

        public string CIFIN
        {
            get { return cIFIN; }
            set { cIFIN = value; }
        }
        private string cIFFilterTime = "";

        public string CIFFilterTime
        {
            get { return cIFFilterTime; }
            set { cIFFilterTime = value; }
        }
        private string cIFFilterSetting = "";

        public string CIFFilterSetting
        {
            get { return cIFFilterSetting; }
            set { cIFFilterSetting = value; }
        }
        private ValueSetting[] valueSettingList;

        public ValueSetting[] ValueSettingList
        {
            get { return valueSettingList; }
            set { valueSettingList = value; }
        }
        private string cIFFilterCMD = "";

        public string CIFFilterCMD
        {
            get { return cIFFilterCMD; }
            set { cIFFilterCMD = value; }
        }

        private string cIFFilterCMDForF = "";

        public string CIFFilterCMDForF
        {
            get { return cIFFilterCMDForF; }
            set { cIFFilterCMDForF = value; }
        }

        //FOR CIF and WPEP
        private string WPEP_Enable = "N";
        public string WPEPEnable
        {
            get { return WPEP_Enable; }
            set { WPEP_Enable = value; }
        }

        private string WPEP_Reference = "";
        public string WPEPReference
        {
            get { return WPEP_Reference; }
            set { WPEP_Reference = value; }
        }

        private string WPEP_BankNo = "";
        public string WPEPBankNo
        {
            get { return WPEP_BankNo; }
            set { WPEP_BankNo = value; }
        }

        private string WPEP_BranchNo = "";
        public string WPEPBranchNo
        {
            get { return WPEP_BranchNo; }
            set { WPEP_BranchNo = value; }
        }

        private string WPEP_EntNum;
        public string WPEPEntNum
        {
            get { return WPEP_EntNum; }
            set { WPEP_EntNum = value; }
        }

        private string WPEP_retry;
        public string WPEPretry
        {
            get { return WPEP_retry; }
            set { WPEP_retry = value; }
        }

        private string WPEP_delay;
        public string WPEPdelay
        {
            get { return WPEP_delay; }
            set { WPEP_delay = value; }
        }

        private string WPEP_CIF_TYPE;
        public string WPEPCIF_TYPE
        {
            get { return WPEP_CIF_TYPE; }
            set { WPEP_CIF_TYPE = value; }
        }

        private string WPEP_nation_col;
        public string WPEPnation_col
        {
            get { return WPEP_nation_col; }
            set { WPEP_nation_col = value; }
        }

        private string WPEP_engname_col;
        public string WPEPengname_col
        {
            get { return WPEP_engname_col; }
            set { WPEP_engname_col = value; }
        }

        private string WPEP_score;
        public string WPEPscore
        {
            get { return WPEP_score; }
            set { WPEP_score = value; }
        }

        private string WPEP_dob;
        public string WPEPdob
        {
            get { return WPEP_dob; }
            set { WPEP_dob = value; }
        }

        private string WPEP_swifttype1;
        public string WPEPswifttype1
        {
            get { return WPEP_swifttype1; }
            set { WPEP_swifttype1 = value; }
        }

        private string WPEP_swifttype2;
        public string WPEPswifttype2
        {
            get { return WPEP_swifttype2; }
            set { WPEP_swifttype2 = value; }
        }

        /*
        private string Div_Image;
        public string DivImage
        {
            get { return Div_Image; }
            set { Div_Image = value; }
        }
        */

        private string CIFMMFilterCMD;
        public string CIFmmFilterCMD
        {
            get { return CIFMMFilterCMD; }
            set { CIFMMFilterCMD = value; }
        }

        //            ValueSetting
        #endregion

        #region Constructor
        public BranchEntity()
        {
        }


        #region IDataReader Constructor
        /// <summary>
        /// Loads the current datarow
        /// </summary>
        public BranchEntity(IDataReader reader)
        {
            this.LoadFromReader(reader);
        }
        #endregion

        #region DataEntity Constructor

        #endregion



        #endregion

        #region Private functions
        #region Load datareader
        /// <summary>
        /// Loads the current datareader
        /// </summary>
        protected void LoadFromReader(IDataReader reader)
        {
        }

        #endregion
        #endregion

        #region Public properties

        /// <summary>
        /// DataType string
        /// DataSize 10
        /// </summary>
        public string BranchCode
        {
            get { return branchCode; }
            set { branchCode = value; }
        }

        /// <summary>
        /// DataType string
        /// DataSize 10
        /// </summary>
        public string BranchName
        {
            get { return branchName; }
            set { branchName = value; }
        }

        /// <summary>
        /// DataType string
        /// DataSize 10
        /// </summary>
        public string WatchType
        {
            get { return watchType; }
            set { watchType = value; }
        }

        /// <summary>
        /// DataType string
        /// DataSize 10
        /// </summary>
        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Backup
        {
            get { return backup; }
            set { backup = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string OutputFormat
        {
            get { return outputFormat; }
            set { outputFormat = value; }
        }
        #endregion
    }
}
