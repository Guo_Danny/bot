﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Web;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Newtonsoft.Json;

namespace BlackListService
{
    public class OFACWebService
    {
        private static readonly ILog log = LogManager.GetLogger("App.Logging");

        public Response CallOFACWS(string bkno, string scanname, string strcountry, int iScore, string OFACWS_DB, string str_SSL)
        {
            Fortify fortifyService = new Fortify();

            string orgcode = "botosb";
            string username = "primeadmin";
            string password = "Passw0rd";

            string branch = "0161";
            string dept = "AML";
            string source = "WS_swift";
            string fileimg = "0161swift";
            string insertDB = "true";
            string partyid = "wire Payment";
            string use_excludename = "true";
            string use_misspelling = "true";
            string use_algorithm = "MatchText";
            string fullname = "PLM CONSORCIO, C.A.";
            string country = "United States";
            string DOB = "";
            //int iScore = 0;


            string service_result = "";
            branch = bkno;
            fileimg = bkno + "swift";
            fullname = scanname;
            country = strcountry;
            //use_algorithm = "MatchFullName";
            insertDB = (OFACWS_DB == "N") ? "false" : "true";
            //fullname = "Osama Bin Laden safia";
            //country = "Afghanistan";
            //DOB = "19/08/1949";
            //iScore = 80;

            string xmldata = "";
            xmldata = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            xmldata += "<n:GETSEARCHRESULTS xmlns:n=\"www.primeassociates.com/ComplianceManager/OFACReporter\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
            xmldata += "<n:INPUT>";
            xmldata += "<n:ORGANIZATION>" + orgcode + "</n:ORGANIZATION>";
            xmldata += "<n:BRANCH>" + branch + "</n:BRANCH>";
            xmldata += "<n:DEPT>" + dept + "</n:DEPT>";
            xmldata += "<n:SOURCE>" + source + "</n:SOURCE>";
            xmldata += "<n:FILEIMG>" + fileimg + "</n:FILEIMG>";
            xmldata += "<n:USEDB>" + insertDB + "</n:USEDB>";
            xmldata += "<n:DELEGATED>false</n:DELEGATED>";
            xmldata += "<n:RETURNORIGINALREQUEST>false</n:RETURNORIGINALREQUEST>";
            xmldata += "<n:SEARCHDATA>";
            xmldata += "<n:PARTYID>" + partyid + "</n:PARTYID>";
            xmldata += "<n:PARTYNAME n:USETEXTEXCLUDE=\"" + use_excludename + "\" n:MISSPELLING=\"" + use_misspelling + "\" n:ALGORITHM=\"" + use_algorithm + "\"><n:FULLNAME>" + fullname + "</n:FULLNAME></n:PARTYNAME>";
            xmldata += "<n:PARTYADDRESS>";
            xmldata += "<n:STREET n:USETEXTEXCLUDE=\"" + use_excludename + "\" n:MISSPELLING=\"" + use_misspelling + "\" n:ALGORITHM=\"None\"></n:STREET>";
            xmldata += "<n:CITY n:USETEXTEXCLUDE=\"" + use_excludename + "\" n:MISSPELLING=\"" + use_misspelling + "\" n:ALGORITHM=\"None\"></n:CITY>";
            xmldata += "<n:STATE n:USETEXTEXCLUDE=\"" + use_excludename + "\" n:MISSPELLING=\"" + use_misspelling + "\" n:ALGORITHM=\"None\"></n:STATE>";
            xmldata += "<n:POSTALCODE></n:POSTALCODE>";
            xmldata += "<n:COUNTRY n:USETEXTEXCLUDE=\"" + use_excludename + "\" n:MISSPELLING=\"" + use_misspelling + "\" n:ALGORITHM=\"None\">" + country + "</n:COUNTRY>";
            xmldata += "</n:PARTYADDRESS>";
            xmldata += "<n:DATEOFBIRTH n:USETEXTEXCLUDE=\"" + use_excludename + "\" n:MISSPELLING=\"" + use_excludename + "\" n:ALGORITHM=\"None\">" + DOB + "</n:DATEOFBIRTH>";
            xmldata += "</n:SEARCHDATA>";
            xmldata += "</n:INPUT>";
            xmldata += "</n:GETSEARCHRESULTS>";

            LogAdd("OFAC WS input xml:" + xmldata);

            OFAC_webservice.OFACReporterWS webservice = new OFAC_webservice.OFACReporterWS();

            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            //for SSL
            if (str_SSL == "Y")
            {
                System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;
            }

            service_result = webservice.getSearchResults(username, password, orgcode, xmldata);

            if (service_result.Contains("COUNT=\"0"))
            {
                LogAdd("no match for webservice");
                return null;
            }
            else
            {
                LogAdd("WS retrun:" + service_result);
            }

            // 透過XmlDocument讀入
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(service_result.Replace("<n:", "<").Replace("</n:", "</").Replace(" n:", " ").Replace("n=\"www.primeassociates.com/ComplianceManager/OFACReporter\"", "json='http://james.newtonking.com/projects/json'").Replace("<MATCH ", "<MATCH json:Array=\"true\" "));
            //

            // 讀取HEADER節點
            XmlNode responseNode = xmlDoc.LastChild;
            // 透過Json.NET將XmlNode轉為Json格式字串
            string jsonText = JsonConvert.SerializeXmlNode(responseNode);

            jsonText = jsonText.Replace("@", "");
            var obj = JsonConvert.DeserializeObject(fortifyService.PathManipulation(jsonText));

            var f = JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented);

            // 透過Json.NET反序列化為物件
            var responseObj = JsonConvert.DeserializeObject<Response>(fortifyService.PathManipulation(f));

            //no match return

            
            if (responseObj.GETSEARCHRESULTS.OUTPUT.MATCHES == null)
            {
                responseObj = null;
                return responseObj;
            }

            /* 20190417 remove score limite temporarily
            for (int i = 0; i < responseObj.GETSEARCHRESULTS.OUTPUT.MATCHES.MATCH.Count(); i++)
            {
                if (i == 0)
                    responseObj.GETSEARCHRESULTS.OUTPUT.MATCHES.MATCH[i].SCORE = 70;

                if (i == 1)
                    responseObj.GETSEARCHRESULTS.OUTPUT.MATCHES.MATCH[i].SCORE = 90;
            }

            if (responseObj != null)
                responseObj.GETSEARCHRESULTS.OUTPUT.MATCHES.MATCH.RemoveAll(x => x.SCORE < iScore);
            */

            return responseObj;
        }

        private void LogAdd(string logstring)
        {
            Fortify fortifyService = new Fortify();
            log.Info(fortifyService.PathManipulation(logstring));
        }

        public class Response
        {
            public Header GETSEARCHRESULTS { get; set; }
        }

        public class Header
        {
            public Ouput OUTPUT { get; set; }
        }

        public class Ouput
        {
            public string TRANSACTIONID { get; set; }
            public Matches MATCHES { get; set; }
            public Searchstate SEARCHSTATE { get; set; }
            public Approvestate APPROVESTATE { get; set; }
            public string ANNOTATION { get; set; }
            public string REQUESTTIME { get; set; }
            public string CIFRULE { get; set; }
        }

        public class Matches
        {
            public string COUNT { get; set; }
            public List<Match> MATCH { get; set; }
        }

        public class Match
        {
            public string N { get; set; }
            public string MATCHNAME { get; set; }
            public string ORIGINALSDNNAME { get; set; }
            public string MATCHTEXT { get; set; }
            public string MATCHPROGRAM { get; set; }
            public string MATCHTYPE { get; set; }
            public string MATCHREMARKS { get; set; }

        }

        public class Searchstate
        {
            public string STATUS { get; set; }
            public string DATETIME { get; set; }
        }

        public class Approvestate
        {
            public string APPROVED { get; set; }
            public string DATETIME { get; set; }
        }
    }
}
