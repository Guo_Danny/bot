﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using System.Diagnostics;
using System.IO;
using log4net;
using log4net.Config;

namespace BlackListService
{
    static class Program
    {
        private static readonly ILog log = LogManager.GetLogger("App.Logging");

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            Fortify fortifyService = new Fortify();

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(
                delegate (object sender, System.UnhandledExceptionEventArgs e)
                {
                    log.Error(String.Format("Service UnhandledException {0}", fortifyService.PathManipulation(e.ToString())));
                }
            );

            XmlConfigurator.Configure(new System.IO.FileInfo(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + @"\log4net.config"));

            /*
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                    new UnisysAMLInterface()
            };
            ServiceBase.Run(ServicesToRun);
            */
            ///*
            //For Debug
            if (Environment.UserInteractive)
            {
                UnisysAMLInterface s = new UnisysAMLInterface();

                s.Start(null);

                Console.WriteLine("Service Started，Press Enter To Stop Service...");
                Console.ReadLine();

                s.Stop();

                Console.WriteLine("Service Stop");
            }
            else {

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new UnisysAMLInterface()
                };
                ServiceBase.Run(ServicesToRun);
            }
            //*/
        }
    }
}
