﻿using System;
using System.Data.SqlClient;
using System.Data;

namespace Blacklist
{
    /// <summary>
    /// This object represents the properties and methods of a BranchEntity.
    /// </summary>
    public class BranchEntity
    {
        #region Private member variables
        private string branchCode = String.Empty;
        private string branchName = String.Empty;
        private string watchType = String.Empty;
        private string path = String.Empty;
        private string backup = "";
        private string outputFormat = "";

        private string cIFIN = "";

        public string CIFIN
        {
            get { return cIFIN; }
            set { cIFIN = value; }
        }
        private string cIFFilterTime = "";

        public string CIFFilterTime
        {
            get { return cIFFilterTime; }
            set { cIFFilterTime = value; }
        }
        private string cIFFilterSetting = "";

        public string CIFFilterSetting
        {
            get { return cIFFilterSetting; }
            set { cIFFilterSetting = value; }
        }
        private ValueSetting[] valueSettingList;

        public ValueSetting[] ValueSettingList
        {
            get { return valueSettingList; }
            set { valueSettingList = value; }
        }
        private string cIFFilterCMD = "";

        public string CIFFilterCMD
        {
            get { return cIFFilterCMD; }
            set { cIFFilterCMD = value; }
        }
//            ValueSetting
        #endregion

        #region Constructor
        public BranchEntity()
        {
        }


        #region IDataReader Constructor
        /// <summary>
        /// Loads the current datarow
        /// </summary>
        public BranchEntity(IDataReader reader)
        {
            this.LoadFromReader(reader);
        }
        #endregion

        #region DataEntity Constructor

        #endregion



        #endregion

        #region Private functions
        #region Load datareader
        /// <summary>
        /// Loads the current datareader
        /// </summary>
        protected void LoadFromReader(IDataReader reader)
        {
        }

        #endregion
        #endregion

        #region Public properties

        /// <summary>
        /// DataType string
        /// DataSize 10
        /// </summary>
        public string BranchCode
        {
            get { return branchCode; }
            set { branchCode = value; }
        }

        /// <summary>
        /// DataType string
        /// DataSize 10
        /// </summary>
        public string BranchName
        {
            get { return branchName; }
            set { branchName = value; }
        }

        /// <summary>
        /// DataType string
        /// DataSize 10
        /// </summary>
        public string WatchType
        {
            get { return watchType; }
            set { watchType = value; }
        }

        /// <summary>
        /// DataType string
        /// DataSize 10
        /// </summary>
        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Backup
        {
            get { return backup; }
            set { backup = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string OutputFormat
        {
            get { return outputFormat; }
            set { outputFormat = value; }
        }
        #endregion
    }
}
