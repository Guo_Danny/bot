﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using log4net;
using System.Diagnostics;

namespace Blacklist
{
    public partial class MainForm : Form
    {
        #region Setting
        //分行代碼長度
        private const int BranchCodeLen = 4;
        //檔案固定長度
        private int FileFixLen = 20;
        //cif檔長度
        private int CIFFileFixLen = 15;
        //程式執行標記
        private bool bRunFlag = false;

        
        public delegate void GetSendResult(string message);
        public event GetSendResult OnGetSendResult;

        //取得檔案清單
        private List<string> oFileList = new List<string>();
        //private List<string> oBackupList = new List<string>();

        private List<FileSystemWatcher> oFileWatcherList = new List<FileSystemWatcher>();
        /// <summary>
        /// 
        /// </summary>
        private List<BranchEntity> oBranchINList = new List<BranchEntity>();
        
        /// <summary>
        /// 
        /// </summary>
        private List<BranchEntity> oBranchList = new List<BranchEntity>();
        private static readonly ILog log = LogManager.GetLogger("App.Logging");

        private string CIFFilePath;
        private string CIFFileErrorPath;
        private string CIFFilePathBackup;

        private string FileInPath;
        private string FileInErrorPath;
        private string FileInPathBackup;
        private string FileOutPath;
        private string FileOutPathBackup;

        //private string FileOutClear;
        //private string FileOutputFormat;
        private string OkCode;
        private string ProblemCode;
        private string ConfirmCode;
        private bool InitScanFlag = false;
        #endregion

        #region Form Load
        public MainForm()
        {
            InitializeComponent();
        }
        
        private void MainForm_Load(object sender, EventArgs e)
        {

            bool is_createdNew1;
            bool is_createdNew2;
            Mutex mu1 = null;
            Mutex mu2 = null;

            try
            {
                #region 檢查程式是否重複執行

                // 第一關：在同目錄執行相同程式的情況下不允許重複執行   
                string mutexName1 = Process.GetCurrentProcess().MainModule.FileName
                            .Replace(Path.DirectorySeparatorChar, '_');
                mu1 = new Mutex(true, "Global\\" + mutexName1, out is_createdNew1);
                if (!is_createdNew1)
                    return;

                //// 第二關：在完全相同的傳入參數下不允許重複執行，避免數據重複計算   
                //string mutexName2 = "Args_" + String.Join("_", args)
                //                        .Replace(Path.DirectorySeparatorChar, '_');
                //mu2 = new Mutex(true, "Global\\" + mutexName2, out is_createdNew2);
                //if (!is_createdNew2)
                //    return;

                #endregion

                InitSetting();
                oTimer.Start();
                oCIFTimer.Start();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }   

      
        }
        #endregion

        #region InitSetting 取得初始設定
        private void InitSetting()
        {
            try
            {
                this.OnGetSendResult += new GetSendResult(MainForm_OnGetSendResult);

                txtMessage.Items.Add(String.Format("{0} Loading Setting",DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                log.Info(String.Format("{0} Loading Setting", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));

                oBranchINList.Clear();
                oBranchList.Clear();
                oFileList.Clear();

                XmlDocument doc = new XmlDocument();
                doc.Load(Application.StartupPath + @"\Setting.xml");
                txtMessage.Items.Add(String.Format("{0} Loading XML File", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));

                #region Get Branch Setting
                XmlNodeList nodes = doc.SelectNodes("//Branch");

                txtMessage.Items.Add(String.Format("{0} Loading Branch Setting", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                foreach (XmlNode node in nodes)
                { //                        string s = node.OuterXml;
                    #region Setting Branch IN Directory
                    BranchEntity oInBranch = new BranchEntity();
                    oInBranch.BranchCode = node["Code"].InnerText.Replace("\r\n", "").Trim();
                    oInBranch.BranchName = node["Name"].InnerText.Replace("\r\n", "").Trim();
                    oInBranch.WatchType = "IN";
                    oInBranch.Path = node["In"].InnerText.Replace("\r\n", "").Trim();
                    oInBranch.Backup = node["Backup"].InnerText.Replace("\r\n", "").Trim(); ;

                    try
                    {
                        oInBranch.OutputFormat = node["OutputFormat"].InnerText.Replace("\r\n", "").Trim();
                        if (oInBranch.OutputFormat == "")
                        {
                            log.Error(String.Format("Get Branch {0} File Output Format Error！", oInBranch.BranchCode));
                            this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Get Branch {0} File Output Format Error！", oInBranch.BranchCode) });
                            return;
                        }
                    }
                    catch (Exception FileOutEx)
                    {
                        log.Error(String.Format("Get Branch {0} File Output Format Error！", oInBranch.BranchCode), FileOutEx);
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Get Branch {0} File Output Format Error！", oInBranch.BranchCode) });
                        return;
                    }

                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Loading Setting {0} {1} {2}", oInBranch.BranchCode, oInBranch.WatchType, oInBranch.Path) });
                      

                    if (!Directory.Exists(oInBranch.Path))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", oInBranch.Path));
                        log.Error(String.Format("監看Branch {0} File IN目錄失敗！", oInBranch.BranchCode), oDirEx);
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看Branch {0} File IN目錄失敗！", oInBranch.BranchCode) });
                        return;
                    }
                    #endregion

                    #region Setting Branch OK Directory
                    BranchEntity oBranchOK = new BranchEntity();
                    oBranchOK.BranchCode = node["Code"].InnerText.Replace("\r\n", "").Trim();
                    oBranchOK.BranchName = node["Name"].InnerText.Replace("\r\n", "").Trim();
                    oBranchOK.WatchType = "OK";
                    oBranchOK.Path = node["Ok"].InnerText.Replace("\r\n", "").Trim();

                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Loading Setting {0} {1} {2}", oBranchOK.BranchCode, oBranchOK.WatchType, oBranchOK.Path) });
                    
                    oBranchList.Add(oBranchOK);
                    try
                    {
                        FileSystemWatcher oOKObj = new FileSystemWatcher();
                        oOKObj.Path = oBranchOK.Path;
                        oOKObj.Filter = "*.*";
                        oOKObj.NotifyFilter = NotifyFilters.FileName;
                        oOKObj.Created += new FileSystemEventHandler(oOKObj_Created);
                        oOKObj.EnableRaisingEvents = true;
                        oFileWatcherList.Add(oOKObj);
                    }
                    catch (Exception FileOKEx)
                    {
                        log.Error(String.Format("監看Branch {0} File OK目錄失敗！", oBranchOK.BranchCode), FileOKEx);
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看Branch {0} File OK目錄失敗！", oBranchOK.BranchCode) });
                        return;
                    }
                    #endregion

                    #region Setting Branch Problem Directory
                    BranchEntity oBranchProblem = new BranchEntity();
                    oBranchProblem.BranchCode = node["Code"].InnerText.Replace("\r\n", "").Trim();
                    oBranchProblem.BranchName = node["Name"].InnerText.Replace("\r\n", "").Trim();
                    oBranchProblem.WatchType = "Problem";
                    oBranchProblem.Path = node["Problem"].InnerText.Replace("\r\n", "").Trim();

                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Loading Setting {0} {1} {2}", oBranchProblem.BranchCode, oBranchProblem.WatchType, oBranchProblem.Path) });

                    oBranchList.Add(oBranchProblem);
                    try
                    {
                        FileSystemWatcher oProblemObj = new FileSystemWatcher();
                        oProblemObj.Path = oBranchProblem.Path;
                        oProblemObj.Filter = "*.*";
                        oProblemObj.NotifyFilter = NotifyFilters.FileName;
                        oProblemObj.Created += new FileSystemEventHandler(oProblemObj_Created);
                        oProblemObj.EnableRaisingEvents = true;
                        oFileWatcherList.Add(oProblemObj);
                    }
                    catch (Exception FileProblemEx)
                    {
                        log.Error(String.Format("監看Branch {0} File Problem目錄失敗！", oBranchProblem.BranchCode), FileProblemEx);
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看Branch {0} File Problem目錄失敗！", oBranchProblem.BranchCode) });
                        return;
                    }
                    #endregion

                    #region Setting Branch Confirm Directory
                    BranchEntity oBranchConfirm = new BranchEntity();
                    oBranchConfirm.BranchCode = node["Code"].InnerText.Replace("\r\n", "").Trim();
                    oBranchConfirm.BranchName = node["Name"].InnerText.Replace("\r\n", "").Trim();
                    oBranchConfirm.WatchType = "Confirm";
                    oBranchConfirm.Path = node["Confirm"].InnerText.Replace("\r\n", "").Trim();
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Loading Setting {0} {1} {2}", oBranchConfirm.BranchCode, oBranchConfirm.WatchType, oBranchConfirm.Path) });                    
                    oBranchList.Add(oBranchConfirm);
                    try
                    {
                        FileSystemWatcher oConfirmObj = new FileSystemWatcher();
                        oConfirmObj.Path = oBranchConfirm.Path;
                        oConfirmObj.Filter = "*.*";
                        oConfirmObj.NotifyFilter = NotifyFilters.FileName;
                        oConfirmObj.Created += new FileSystemEventHandler(oConfirmObj_Created);
                        oConfirmObj.EnableRaisingEvents = true;
                        oFileWatcherList.Add(oConfirmObj);
                    }
                    catch (Exception FileConfirmEx)
                    {
                        log.Error(String.Format("監看Branch {0} File Confirm目錄失敗！", oBranchConfirm.BranchCode), FileConfirmEx);
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看Branch {0} File Confirm目錄失敗！", oBranchConfirm.BranchCode) });
                        return;
                    }
                    #endregion       


                    //<CIFIn>D:\Prime\BOTOSB\0114_CIF\</CIFIn>
                    //<CIFFilterTime>0810</CIFFilterTime>
                    //<CIFFilterSetting>D:\Prime\BOTOSB\config\OFAC\0114_Batchfilter.ini</CIFFilterSetting>
                    //<ValueSetting>
                    //<ID>InFileSpec</ID>
                    //<Value> = D:\Prime\BOTOSB\0114_CIF\0114CIF{0}.*</Value>
                    //<ValueFormat>yyyyMMdd</ValueFormat>
                    //<DateAdd>-1</DateAdd>
                    //</ValueSetting>
                    //<ValueSetting>
                    //<ID>OutFileSpec</ID>
                    //<Value> = D:\Prime\BOTOSB\0114_CIF\0114CIF{0}.out</Value>
                    //<ValueFormat>yyyyMMdd</ValueFormat>
                    //<DateAdd>-1</DateAdd>
                    //</ValueSetting>
                    //<CIFFilterCMD>Batchfilter D:\Prime\BOTOSB\config\OFAC\0114_Batchfilter.ini</CIFFilterCMD>

                    oInBranch.CIFIN = node["CIFIn"].InnerText.Replace("\r\n", "").Trim();
                    if (!Directory.Exists(oInBranch.CIFIN))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", oInBranch.CIFIN));
                        log.Error(String.Format("監看Branch {0} CIFIN目錄失敗！", oInBranch.BranchCode), oDirEx);
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看Branch {0} CIFIN目錄失敗！", oInBranch.BranchCode) });
                        return;
                    }

                    oInBranch.CIFFilterTime = node["CIFFilterTime"].InnerText.Replace("\r\n", "").Trim();

                    oInBranch.CIFFilterSetting = node["CIFFilterSetting"].InnerText.Replace("\r\n", "").Trim();
                    if (!File.Exists(oInBranch.CIFFilterSetting))
                    {
                        FileNotFoundException oFileEx = new FileNotFoundException(String.Format("{0} Not Exist", oInBranch.CIFFilterSetting));
                        log.Error(String.Format("監看Branch {0} ini File Error！", oInBranch.BranchCode), oFileEx);
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看Branch {0} ini File Error！", oInBranch.CIFFilterSetting) });
                        return;
                    }

                    oInBranch.CIFFilterCMD = node["CIFFilterCMD"].InnerText.Replace("\r\n", "").Trim();

                    List<ValueSetting> oValueList = new List<ValueSetting>();
                    foreach (XmlNode subNode in node.SelectNodes("ValueSetting"))
                    {
                        ValueSetting oValueSetting = new ValueSetting();
                        oValueSetting.ID = subNode["ID"].InnerText.Replace("\r\n", "").Trim(); ;
                        oValueSetting.IDValue = subNode["Value"].InnerText.Replace("\r\n", "").Trim(); ;
                        oValueSetting.ValueFormat = subNode["ValueFormat"].InnerText.Replace("\r\n", "").Trim(); ;
                        oValueSetting.DateAdd = subNode["DateAdd"].InnerText.Replace("\r\n", "").Trim(); ;
                        oValueList.Add(oValueSetting);
                    }
                    
                    oInBranch.ValueSettingList = oValueList.ToArray();

                    oBranchINList.Add(oInBranch);
                }

                #endregion
   
                #region Get Setting Nodes
                txtMessage.Items.Add(String.Format("{0} Loading System Setting", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                XmlNodeList settingnodes = doc.SelectNodes("//Setting");
                foreach (XmlNode node in settingnodes)
                {

                    FileInPath = node["FileInPath"].InnerText.Replace("\r\n", "").Trim();
                    try
                    {
                        FileSystemWatcher oFileInObj = new FileSystemWatcher();
                        oFileInObj.Path = FileInPath;
                        oFileInObj.Filter = "*.*";
                        oFileInObj.NotifyFilter = NotifyFilters.FileName;
                        oFileInObj.Created += new FileSystemEventHandler(oFileInObj_Created);
                        oFileInObj.EnableRaisingEvents = true;
                    }
                    catch (Exception FileINEx)
                    {
                        log.Error(String.Format("監看File In目錄 {0} 失敗！", FileInPath), FileINEx);
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看File In目錄 {0} 失敗！", FileInPath) });
                        return;
                    }

                    FileInErrorPath = node["FileInErrorPath"].InnerText.Replace("\r\n", "").Trim();
                    if (!Directory.Exists(FileInErrorPath))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", FileInErrorPath));
                        log.Error(String.Format("監看FileInErrorPath目錄失敗！ {0}", FileInErrorPath));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看FileInErrorPath目錄失敗！ {0}", FileInErrorPath) });
                        return;
                    }


                    FileInPathBackup = node["FileInPathBackup"].InnerText.Replace("\r\n", "").Trim();
                    if (!Directory.Exists(FileInPathBackup))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", FileInPathBackup));
                        log.Error(String.Format("監看FileInPathBackup目錄失敗！ {0}", FileInPathBackup));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看FileInPathBackup目錄失敗！ {0}", FileInPathBackup) });
                        return;
                    }

                    FileOutPath = node["FileOutPath"].InnerText.Replace("\r\n", "").Trim();
                    if (!Directory.Exists(FileOutPath))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", FileOutPath));
                        log.Error(String.Format("監看FileOutPath目錄失敗！ {0}", FileOutPath));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看FileOutPath目錄失敗！ {0}", FileOutPath) });
                        return;
                    }

                    FileOutPathBackup = node["FileOutPathBackup"].InnerText.Replace("\r\n", "").Trim();
                    if (!Directory.Exists(FileOutPathBackup))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", FileOutPathBackup));
                        log.Error(String.Format("監看FileOutPathBackup目錄失敗！ {0}", FileOutPathBackup));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看FileOutPathBackup目錄失敗！ {0}", FileOutPathBackup) });
                        return;
                    }

                    //FileOutClear = node["FileOutClear"].InnerText.Replace("\r\n", "").Trim();
                    OkCode = node["OkCode"].InnerText.Replace("\r\n", "").Trim();
                    ProblemCode = node["ProblemCode"].InnerText.Replace("\r\n", "").Trim();
                    ConfirmCode = node["ConfirmCode"].InnerText.Replace("\r\n", "").Trim();
                    //FileOutputFormat = node["OutputFormat"].InnerText.Replace("\r\n", "").Trim();

                    if (node["InitScanFlag"].InnerText.Replace("\r\n", "").Trim().ToUpper() == "Y")
                    {
                        InitScanFlag = true;
                        InitScanFile();                    
                    }

                    try
                    {
                        FileFixLen = int.Parse(node["FileFixLen"].InnerText.Replace("\r\n", "").Trim());
                    }
                    catch
                    {
                        log.Error(String.Format("Get File Fix Len Error！"));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Get File Fix Len Error！") });
                        return;
                    }
                }
                #endregion

                #region Get CIFSetting Nodes
                txtMessage.Items.Add(String.Format("{0} Loading System CIFSetting", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                XmlNodeList CIFsettingnodes = doc.SelectNodes("//CIFSetting");
                foreach (XmlNode node in CIFsettingnodes)
                {
                    try
                    {
                        CIFFileFixLen = int.Parse(node["CIFFileFixLen"].InnerText.Replace("\r\n", "").Trim());
                    }
                    catch
                    {
                        log.Error(String.Format("Get CIF File Fix Len Error！"));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Get CIF File Fix Len Error！") });
                        return;
                    }

                    CIFFilePath = node["CIFFilePath"].InnerText.Replace("\r\n", "").Trim();
                    try
                    {
                        FileSystemWatcher oCIFFileObj = new FileSystemWatcher();
                        oCIFFileObj.Path = CIFFilePath;
                        oCIFFileObj.Filter = "*.*";
                        oCIFFileObj.NotifyFilter = NotifyFilters.FileName;
                        oCIFFileObj.Created += new FileSystemEventHandler(oCIFFileObj_Created);
                        oCIFFileObj.EnableRaisingEvents = true;
                    }
                    catch (Exception FileINEx)
                    {
                        log.Error(String.Format("監看CIFFile目錄 {0} 失敗！", CIFFilePath), FileINEx);
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看CIFFile目錄 {0} 失敗！", CIFFilePath) });
                        return;
                    }

                    CIFFileErrorPath = node["CIFFileErrorPath"].InnerText.Replace("\r\n", "").Trim();
                    if (!Directory.Exists(CIFFileErrorPath))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", CIFFileErrorPath));
                        log.Error(String.Format("監看CIFFileErrorPath目錄失敗！ {0}", CIFFileErrorPath));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看CIFFileErrorPath目錄失敗！ {0}", CIFFileErrorPath) });
                        return;
                    }


                    CIFFilePathBackup = node["CIFFilePathBackup"].InnerText.Replace("\r\n", "").Trim();
                    if (!Directory.Exists(CIFFilePathBackup))
                    {
                        DirectoryNotFoundException oDirEx = new DirectoryNotFoundException(String.Format("{0} Not Exist", CIFFilePathBackup));
                        log.Error(String.Format("監看CIFFilePathBackup目錄失敗！ {0}", CIFFilePathBackup));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("監看CIFFilePathBackup目錄失敗！ {0}", CIFFilePathBackup) });
                        return;
                    }

                  
                    //if (node["InitScanFlag"].InnerText.Replace("\r\n", "").Trim().ToUpper() == "Y")
                    //{
                    //    InitScanFlag = true;
                    //    InitScanFile();
                    //}
                }
                #endregion


                //log.Info("Start BackgroundWork");
                //txtMessage.Items.Add(String.Format("{0} Start BackgroundWork",DateTime.Now.ToLocalTime()));


                //BackgroundWorker oBkWork = new BackgroundWorker();
                //oBkWork.DoWork += new DoWorkEventHandler(oBkWork_DoWork);
                //oBkWork.RunWorkerAsync();


                //System.Windows.Forms.Timer oTimer = new System.Windows.Forms.Timer();
                //oTimer.Interval = 60000000;
                //oTimer.Enabled = true;
                //oTimer.Start();

                bRunFlag = true;
                btnRerun.Visible = false;
            }
            catch (Exception ex)
            {
                log.Error("Form Load Error", ex);
                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Form Load Error {0}",ex.Message.ToString() ) });
                return;
            }
        }


        #endregion

        #region InitScanFile
        private void InitScanFile()
        {
            //e.Name = "XXXX.txt"
            //e.FullPath = "D:\\BlackList\\FileIn\\XXXX.txt"
            //((FileSystemWatcher)sender).Path = "D:\\BlackList\\FileIn\\"

            string[] oFiles = System.IO.Directory.GetFiles(FileInPath);

            for (int i = 0; i < oFiles.Length; i++)
            {            
                try
                {
                    FileInfo oFile = new FileInfo(oFiles[i]);
                    string sFile = oFile.Name;
                    string sFileFullPath = oFile.FullName;

                    int iLoop = 1;
                    if (sFile.Length < 20)
                    {
                        Thread.Sleep(2000);
                        File.Copy(sFileFullPath, String.Format("{0}{1}", FileInErrorPath, sFile), true);
                        log.Info(String.Format("Backup FileIn File {0} Error ,File Len {1} ", sFile, sFile.Length.ToString()));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup FileIn File {0} Error ,File Len {1} ", sFile, sFile.Length.ToString()) });
                        return;
                    }

                    while (iLoop > 0 && iLoop < 10)
                    {
                        try
                        {
                            Thread.Sleep(2000);
                            File.Copy(sFileFullPath, String.Format("{0}{1}", FileInPathBackup, sFile), true);
                            log.Info(String.Format("Backup FileIn File {0} OK", sFile));
                            this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup FileIn File {0} OK", sFile) });
                            iLoop = 0;
                        }
                        catch
                        {
                            log.Info(String.Format("Backup FileIn File Error {0}", sFile));
                            this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup FileIn File {0} Loop {1}", sFile, iLoop.ToString()) });
                            iLoop++;
                        }
                    }

                    if (iLoop != 0)
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup FileIn File Error  {0}", sFile) });

                    log.Info(String.Format("Check {0}", sFile));
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Check {0}", sFile) });

                    string sBranchCode = sFile.Substring(0, BranchCodeLen);
                    bool bFlag = false;
                    for (int j = 0; j < oBranchINList.Count; j++)
                    {
                        if (sBranchCode == oBranchINList[j].BranchCode)
                        {
                            //Delete Exist File
                            if (File.Exists(String.Format("{0}{1}", oBranchINList[j].Path, sFile)))
                            {
                                log.Info(String.Format("File Delete {0}", sFile));
                                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("File Delete {0}", sFile) });
                                File.Delete(String.Format("{0}{1}", oBranchINList[j].Path, sFile));
                            }

                            File.Move(sFileFullPath, String.Format("{0}{1}", oBranchINList[j].Path, sFile));
                            log.Info(String.Format("Move {0} To Branch {1}", sFile, sBranchCode));
                            this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Move {0} To Branch {1}", sFile, sBranchCode) });
                            bFlag = true;
                            break;
                        }
                    }

                    if (!bFlag)
                    {
                        File.Move(sFileFullPath, String.Format("{0}{1}", FileInErrorPath, sFile));
                        log.Info(String.Format("Can Find Branch Code,{0}", sBranchCode));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Can Find Branch Code,{0}", sBranchCode) });
                    }

                }
                catch (Exception ex)
                {
                    log.Error(String.Format("oFileInObj_Created Error,{0}", oFiles[i]), ex);
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("oFileInObj_Created Error,{0}", oFiles[i]) });
                    return;
                }
            }


            for (int i = 0; i < oBranchList.Count; i++)
            {
                string sTypeCode = "";
                switch (oBranchList[i].WatchType)
                {
                    case "OK":
                        sTypeCode = OkCode;
                        break;
                    case "Problem":
                        sTypeCode = ProblemCode ;
                        break;
                    case "Confirm":
                        sTypeCode = ConfirmCode;
                        break;
                }
                oProcessObj(oBranchList[i].Path, sTypeCode, oBranchList[i].WatchType);
            }
        }
        #endregion

        #region File In Object Create
        private void oFileInObj_Created(object sender, FileSystemEventArgs e)
        {
            //e.Name = "XXXX.txt"
            //e.FullPath = "D:\\BlackList\\FileIn\\XXXX.txt"
            //((FileSystemWatcher)sender).Path = "D:\\BlackList\\FileIn\\"
            try
            {
                int iLoop = 1;

                string sFile = e.Name.ToString();

                if (sFile.Length != FileFixLen)
                {
                    Thread.Sleep(2000);
                    File.Copy(e.FullPath, String.Format("{0}{1}", FileInErrorPath, sFile), true);
                    log.Info(String.Format("Backup FileIn File {0} Error ,File Len {1} ", sFile, sFile.Length.ToString()));
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup FileIn File {0} Error ,File Len {1} ", sFile, sFile.Length.ToString()) });
                    return;
                }

                //if (sFile.Substring(4, 3).ToUpper() != "CIF")
                //{ 
                
                
                //}







                while (iLoop > 0 && iLoop < 10)
                {
                    try
                    {
                        Thread.Sleep(2000);
                        File.Copy(e.FullPath, String.Format("{0}{1}", FileInPathBackup, sFile), true);
                        log.Info(String.Format("Backup FileIn File {0} OK", sFile));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup FileIn File {0} OK", sFile) });
                        iLoop = 0;
                    }
                    catch
                    {
                        log.Info(String.Format("Backup FileIn File Error {0}", sFile));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup FileIn File {0} Loop {1}", sFile, iLoop.ToString()) });
                        iLoop++;
                    }
                }

                if (iLoop != 0)
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup FileIn File Error  {0}", sFile) });

                log.Info(String.Format("Check {0}", sFile));
                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Check {0}", sFile) });

                string sBranchCode = sFile.Substring(0, BranchCodeLen);
                bool bFlag = false;
                for (int i = 0; i < oBranchINList.Count; i++)
                {
                    if (sBranchCode == oBranchINList[i].BranchCode)
                    {
                        //Delete Exist File
                        if (File.Exists(String.Format("{0}{1}", oBranchINList[i].Path, sFile)))
                        {
                            log.Info(String.Format("File Delete {0}", sFile));
                            this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("File Delete {0}", sFile) });
                            File.Delete(String.Format("{0}{1}", oBranchINList[i].Path, sFile));
                        }

                        File.Move(e.FullPath, String.Format("{0}{1}", oBranchINList[i].Path, sFile));
                        log.Info(String.Format("Move {0} To Branch {1}", sFile, sBranchCode));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Move {0} To Branch {1}", sFile, sBranchCode) });
                        bFlag = true;
                        break;
                    }
                }

                if (!bFlag)
                {
                    File.Move(e.FullPath, String.Format("{0}{1}", FileInErrorPath, sFile));
                    log.Info(String.Format("Can Find Branch Code,{0}", sBranchCode));
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Can Find Branch Code,{0}", sBranchCode) });
                }

            }
            catch (Exception ex)
            {
                log.Error(String.Format("oFileInObj_Created Error,{0}", e.Name.ToString()), ex);
                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("oFileInObj_Created Error,{0}", e.Name.ToString()) });
                return;
            }
        }
        #endregion

        #region CIFFile Object Create
        private void oCIFFileObj_Created(object sender, FileSystemEventArgs e)
        {
            //e.Name = "XXXX.txt"
            //e.FullPath = "D:\\BlackList\\FileIn\\XXXX.txt"
            //((FileSystemWatcher)sender).Path = "D:\\BlackList\\FileIn\\"
            try
            {
                int iLoop = 1;

                string sFile = e.Name.ToString();

                if (sFile.Length != CIFFileFixLen)
                {
                    Thread.Sleep(2000);
                    File.Copy(e.FullPath, String.Format("{0}{1}", CIFFileErrorPath, sFile), true);
                    log.Info(String.Format("Backup CIFFile {0} Error ,File Len {1} ", sFile, sFile.Length.ToString()));
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup CIFFile File {0} Error ,File Len {1} ", sFile, sFile.Length.ToString()) });
                    return;
                }

                while (iLoop > 0 && iLoop < 10)
                {
                    try
                    {
                        Thread.Sleep(2000);
                        File.Copy(e.FullPath, String.Format("{0}{1}", CIFFilePathBackup, sFile), true);
                        log.Info(String.Format("Backup CIFFileIn File {0} OK", sFile));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup CIFFile File {0} OK", sFile) });
                        iLoop = 0;
                    }
                    catch
                    {
                        log.Info(String.Format("Backup CIFFile File Error {0}", sFile));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup CIFFile File {0} Loop {1}", sFile, iLoop.ToString()) });
                        iLoop++;
                    }
                }

                if (iLoop != 0)
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup CIFFile File Error  {0}", sFile) });

                log.Info(String.Format("CIF Check {0}", sFile));
                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("CIF Check {0}", sFile) });

                string sBranchCode = sFile.Substring(0, BranchCodeLen);
                bool bFlag = false;
                
                for (int i = 0; i < oBranchINList.Count; i++)
                {
                    if (sBranchCode == oBranchINList[i].BranchCode)
                    {
                        //Delete Exist File
                        if (File.Exists(String.Format("{0}{1}", oBranchINList[i].CIFIN , sFile)))
                        {
                            log.Info(String.Format("CIFFile Delete {0}", sFile));
                            this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("CIFFile Delete {0}", sFile) });
                            File.Delete(String.Format("{0}{1}", oBranchINList[i].CIFIN, sFile));
                        }

                        File.Move(e.FullPath, String.Format("{0}{1}", oBranchINList[i].CIFIN, sFile));
                        log.Info(String.Format("Move CIF {0} To Branch {1}", sFile, sBranchCode));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Move CIF {0} To Branch {1}", sFile, sBranchCode) });
                        bFlag = true;
                        break;
                    }
                }

                if (!bFlag)
                {
                    File.Move(e.FullPath, String.Format("{0}{1}", CIFFileErrorPath, sFile));
                    log.Info(String.Format("Can't Find CIF Branch Code,{0}", sBranchCode));
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Can't Find CIF Branch Code,{0}", sBranchCode) });
                }

            }
            catch (Exception ex)
            {
                log.Error(String.Format("oCIFFileObj_Created Error,{0}", e.Name.ToString()), ex);
                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("oCIFFileObj_Created Error,{0}", e.Name.ToString()) });
                return;
            }
        }
        #endregion

        #region ProcessObj_Created
        private void oProcessObj(string sFilePath,string sTypeCode,string sTypeName)
        {
            string[] oFiles = System.IO.Directory.GetFiles(sFilePath);

            for (int iFileList = 0; iFileList < oFiles.Length; iFileList++)
            {
                FileInfo oFile = new FileInfo(oFiles[iFileList]);
                string sFile = oFile.Name;
                string sFileFullName = oFile.FullName;
                try
                {

                    if (sFile.Length < 20)
                    {
                        log.Info(String.Format("File {0} Error ,File Len {1} Type {2}", sFile, sFile.Length.ToString(), sTypeName));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("File {0} Error ,File Len {1} Type {2}", sFile, sFile.Length.ToString(), sTypeName) });
                        return;
                    }

                    int iLoop = 1;
                    log.Info(String.Format("Check {0}", sFile));
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Text File {0}", sFileFullName) });

                    string sStrValue = "";
                    string sORITime = sFile.Substring(14, 6);
                    string sCRTTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                    iLoop = 1;
                    while (iLoop > 0 && iLoop < 10)
                    {
                        iLoop = GetSTRData(sFileFullName, ref sStrValue, iLoop);
                    }

                    //sStrValue = System.IO.File.ReadAllText(e.FullPath, System.Text.Encoding.Default);
                    sStrValue = sStrValue.Substring(2, 24) + sTypeCode + sORITime + sCRTTime + "  ";
                    string sBranchCode = sStrValue.Substring(2, 4);

                    iLoop = 1;
                    while (iLoop > 0 && iLoop < 4)
                    {
                        iLoop = AppendData(GetOutputFileName(FileOutPath, sBranchCode), sStrValue, iLoop);
                    }

                    if (iLoop != 0)
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Append File IO Error {0}", sBranchCode) });


                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Append File {0}", sBranchCode) });
                    //File.Delete(e.FullPath);
                    //this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Delete File {0}", sBranchCode) });
                }
                catch (Exception ex)
                {
                    log.Error(String.Format("oProcessObj_Created Error,{0}", sFileFullName ), ex);
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("oProcessObj_Created_Created Error,{0}", sFileFullName) });
                    return;
                }
            }


        }
        #endregion

        #region OK
        private void oOKObj_Created(object sender, FileSystemEventArgs e)
        {
            //e.Name = "XXXX.txt"
            //e.FullPath = "D:\\Blacklist\\JP0001\\Ok\\XXXX.txt"
            //((FileSystemWatcher)sender).Path = "D:\\Blacklist\\JP0001\\Ok\\"
            try
            {
                string sFile = e.Name.ToString();

                if (sFile.Length < 20)
                {
                    log.Info(String.Format("OK File {0} Error ,File Len {1} ", sFile, sFile.Length.ToString()));
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("OK File {0} Error ,File Len {1} ", sFile, sFile.Length.ToString()) });
                    return;
                }

                int iLoop = 1;

                #region Backup File
                //while (iLoop > 0 && iLoop < 10)
                //{
                //    try
                //    {
                //        Thread.Sleep(2000);
                //        File.Copy(e.FullPath, String.Format("{0}{1}", FileOutPathBackup, sFile), true);
                //        log.Info(String.Format("Backup FileOut OK File {0}", sFile));
                //        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup FileOut OK File {0}", sFile) });
                //        iLoop = 0;
                //    }
                //    catch
                //    {
                //        log.Info(String.Format("Backup FileOut Fail File {0} Loop {1}", sFile,iLoop.ToString()));
                //        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup FileOut Fail File {0} Loop {1}", sFile, iLoop.ToString()) });
                //        iLoop++;
                //    }
                
                //}

                //if (iLoop != 0)
                //    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup FileOut Fail Error {0}", sFile) });
                #endregion

                log.Info(String.Format("Check {0}", sFile));
                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Text File {0}", e.FullPath) });

                string sStrValue = "";
                string sORITime = sFile.Substring(14, 6);
                string sCRTTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                iLoop = 1;
                while (iLoop > 0 && iLoop < 10)
                {
                    iLoop = GetSTRData(e.FullPath,ref sStrValue,iLoop);
                }

                //sStrValue = System.IO.File.ReadAllText(e.FullPath, System.Text.Encoding.Default);
                sStrValue = sStrValue.Substring(2, 24) + OkCode + sORITime + sCRTTime + "  ";
                string sBranchCode = sStrValue.Substring(2, 4);

                iLoop = 1;
                
                while (iLoop > 0 && iLoop < 4)
                {
                    iLoop = AppendData(FileOutPath +sFile , sStrValue, iLoop);
                }

                if(iLoop != 0)
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Append File IO Error {0}", sBranchCode) });


                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Append File {0}", sBranchCode) });
                //File.Delete(e.FullPath);
                //this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Delete File {0}", sBranchCode) });
                File.Copy(FileOutPath + sFile, FileOutPathBackup + sFile, true);

            }
            catch (Exception ex)
            {
                log.Error(String.Format("oOKObj_Created Error,{0}", e.Name.ToString()), ex);
                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("oOKObj_Created Error,{0}", e.Name.ToString() ) });
                return;
            }
        }
        #endregion

        #region Problem
        private void oProblemObj_Created(object sender, FileSystemEventArgs e)
        {
            //e.Name = "XXXX.txt"
            //e.FullPath = "D:\\Blacklist\\JP0001\\Ok\\XXXX.txt"
            //((FileSystemWatcher)sender).Path = "D:\\Blacklist\\JP0001\\Ok\\"
            try
            {
                string sFile = e.Name.ToString();

                if (sFile.Length < 20)
                {
                    log.Info(String.Format("Problem File {0} Error ,File Len {1} ", sFile, sFile.Length.ToString()));
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("OK File {0} Error ,File Len {1} ", sFile, sFile.Length.ToString()) });
                    return;
                }


                int iLoop = 1;




                log.Info(String.Format("Check {0}", sFile));
                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Check {0}", sFile) });

                string sStrValue = "";
                string sORITime = sFile.Substring(14, 6);
                string sCRTTime = DateTime.Now.ToString("yyyyMMddHHmmss");

                iLoop = 1;
                while (iLoop > 0 && iLoop < 10)
                {
                    iLoop = GetSTRData(e.FullPath,ref sStrValue , iLoop);
                }

                //sStrValue = System.IO.File.ReadAllText(e.FullPath, System.Text.Encoding.Default);                
                //string sStrValue = System.IO.File.ReadAllText(e.FullPath, System.Text.Encoding.Default);

                sStrValue = sStrValue.Substring(2, 24) + ProblemCode + sORITime + sCRTTime + "  ";
                string sBranchCode = sStrValue.Substring(2, 4);
                iLoop = 1;
                while (iLoop > 0 && iLoop < 4)
                {
                  //  iLoop = AppendData(GetOutputFileName(FileOutPath , sBranchCode), sStrValue , iLoop);
                    iLoop = AppendData(FileOutPath + sFile, sStrValue, iLoop);
                    //FileOutPathBackup
                }               

                if(iLoop != 0)
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Append File IO Error {0}", sBranchCode) });


                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Append File {0}", sBranchCode) });
                //File.Delete(e.FullPath);
                //this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Delete File {0}", sBranchCode) });
                File.Copy(FileOutPath + sFile, FileOutPathBackup + sFile,true);
            }
            catch (Exception ex)
            {
                log.Error(String.Format("oProblemObj_Created Error,{0}", e.Name.ToString()), ex);
                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("oProbelmObj_Created Error,{0}", e.Name.ToString()) });
                return;
            }
        }
        #endregion

        #region Confirm
        private void oConfirmObj_Created(object sender, FileSystemEventArgs e)
        {
            //e.Name = "XXXX.txt"
            //e.FullPath = "D:\\Blacklist\\JP0001\\Ok\\XXXX.txt"
            //((FileSystemWatcher)sender).Path = "D:\\Blacklist\\JP0001\\Ok\\"
            try
            {
                string sFile = e.Name.ToString();
                if (sFile.Length < 20)
                {
                    log.Info(String.Format("Confirm File {0} Error ,File Len {1} ", sFile, sFile.Length.ToString()));
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("OK File {0} Error ,File Len {1} ", sFile, sFile.Length.ToString()) });
                    return;
                }
                int iLoop = 1;


                log.Info(String.Format("Check {0}", sFile));
                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Check {0}", sFile) });

                string sStrValue = "";
                string sORITime = sFile.Substring(14, 6);
                string sCRTTime = DateTime.Now.ToString("yyyyMMddHHmmss");

                iLoop = 1;
                while (iLoop > 0 && iLoop < 10)
                {
                    iLoop = GetSTRData(e.FullPath,ref sStrValue, iLoop);
                }

                //sStrValue = System.IO.File.ReadAllText(e.FullPath, System.Text.Encoding.Default);

                sStrValue = sStrValue.Substring(2, 24) + ConfirmCode + sORITime + sCRTTime + "  ";

                string sBranchCode = sStrValue.Substring(2, 4);

                iLoop = 1;
                while (iLoop > 0 && iLoop < 10)
                {
         //           iLoop = AppendData(GetOutputFileName(FileOutPath , sBranchCode ), sStrValue , iLoop);
                    iLoop = AppendData(FileOutPath + sFile, sStrValue, iLoop);
                }

                if (iLoop != 0)
                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Append File IO Error {0}", sBranchCode) });


                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Append File {0}", sBranchCode) });
                //File.Delete(e.FullPath);
                //this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Delete File {0}", sBranchCode) });
                File.Copy(FileOutPath + sFile, FileOutPathBackup + sFile, true);

            }
            catch (Exception ex)
            {
                log.Error(String.Format("oConfirmObj_Created Error,{0}", e.Name.ToString()), ex);
                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("oConfirmObj_Created Error,{0}", e.Name.ToString()) });
                return;
            }
        }
        #endregion

        #region Btn Return Click
        private void btnRerun_Click(object sender, EventArgs e)
        {
            if (bRunFlag)
                return;

            InitSetting();
        }
        #endregion

        #region GetSTRData
        private int GetSTRData(string sFile, ref string sStrValue, int iLoop)
        {
            if (iLoop != 1)
                Thread.Sleep(5000);

            try
            {
                sStrValue = System.IO.File.ReadAllText(sFile, System.Text.Encoding.Default);
                return 0;
            }
            catch (Exception ex)
            {
                iLoop++;
                return iLoop;
            }
        }
        #endregion

        #region AppendData
        private int AppendData(string sFile, string sStrValue, int iLoop)
        {
            if (iLoop != 1)
                Thread.Sleep(5000);

            try
            {
                File.WriteAllText(sFile, sStrValue + "\r\n");
                return 0;

                //原：檔案附加上去
                //File.AppendAllText(sFile, sStrValue + "\r\n");
                //return 0;
            }
            catch (Exception ex)
            {
                iLoop++;
                return iLoop;
            }
        }
        #endregion

        #region GetOutputFileName
        private string GetOutputFileName(string sPath,string sBankCode)
        {
            ////YYYYMMDD
            ////YYYYMMDDHHMM
            //string sYYYY = DateTime.Now.ToString("yyyy");
            //string sYYYYMM = DateTime.Now.ToString("yyyyMM");
            //string sMMDD = DateTime.Now.ToString("MMdd");
            //string sYYYYMMDD = DateTime.Now.ToString("yyyyMMdd");
            //string sYYYYMMDDHHMMSS = DateTime.Now.ToString("yyyyMMddHHmmss");
            //string sYYYYMMDDHHMM = DateTime.Now.ToString("yyyyMMddHHmm");

            //string sOutFile = FileOutputFormat.Replace("[CODE]", sBankCode).Replace("[YYYYMMDD]", sYYYYMMDD).Replace("[YYYYMMDDHHMMSS]", sYYYYMMDDHHMMSS).Replace("[YYYYMMDDHHMM]", sYYYYMMDDHHMM);
            //sOutFile = sOutFile.Replace("[YYYY]", sYYYY).Replace("[YYYYMM]", sYYYYMM).Replace("[MMDD]", sMMDD);
            //return sPath + sOutFile;

            for (int i = 0; i < oBranchINList.Count; i++)
            {
                if (oBranchINList[i].BranchCode == sBankCode)
                {
                    return sPath + oBranchINList[i].OutputFormat;
                }
            }

            return "";


        }
        #endregion

        #region GetSendResult
        private void MainForm_OnGetSendResult(string messageID)
        {
            txtMessage.Items.Add(String.Format("{0} {1}", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), messageID));
        }
        #endregion

        #region Get Log
        private void btnGetLog_Click(object sender, EventArgs e)
        {
            SaveFileDialog oSFDialog = new SaveFileDialog();
            oSFDialog.FileName = DateTime.Now.ToString("yyyyMMdd");
            oSFDialog.Filter = "Text File|*.txt";
            if (oSFDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK )
            {
               
                for (int i = 0; i < txtMessage.Items.Count; i++)
                {
                    if(i==0)
                       System.IO.File.WriteAllText(oSFDialog.FileName, txtMessage.Items[i].ToString() + "\r\n");
                    else 
                       System.IO.File.AppendAllText(oSFDialog.FileName, txtMessage.Items[i].ToString()+"\r\n");
                }
                MessageBox.Show("OK!!");
            }
        }
        #endregion

        #region Timer
        private void oTimer_Tick(object sender, EventArgs e)
        {
            string sNow = DateTime.Now.ToString("HHmm");
            try
            {
                for (int i = 0; i < oBranchINList.Count; i++)
                {
                    if (oBranchINList[i].Backup == sNow)
                    {

                        string sFile = GetOutputFileName(FileOutPath, oBranchINList[i].BranchCode);

                        File.Move(sFile, GetOutputFileName(FileOutPathBackup, oBranchINList[i].BranchCode) + "_" + DateTime.Now.ToString("yyyyMMdd"));
                        log.Info(String.Format("Backup Branch {0} Output Data", oBranchINList[i].BranchCode));
                        this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup Branch {0} Output Data", oBranchINList[i].BranchCode) });

                        //if(!System.IO.File.Exists(sFile))
                        //{
                        //    log.Info(String.Format("Backup To Branch {0} No File", oBranchINList[i].BranchCode));
                        //    return;
                        //}

                        //string sStrValue = System.IO.File.ReadAllText(sFile, System.Text.Encoding.Default);

                        //System.IO.File.AppendAllText(GetOutputFileName(FileOutPathBackup, oBranchINList[i].BranchCode), sStrValue, System.Text.Encoding.Default);

                        //log.Info(String.Format("Backup To Branch {0}", oBranchINList[i].BranchCode));
                        //this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Backup To Branch {0}", oBranchINList[i].BranchCode) });

                        //System.IO.File.WriteAllText(GetOutputFileName(FileOutPath, oBranchINList[i].BranchCode), "", System.Text.Encoding.Default);
                        //log.Info(String.Format("Clear Branch {0} Data", oBranchINList[i].BranchCode));
                        //this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Clear Branch {0} Data", oBranchINList[i].BranchCode) });
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info(String.Format("Clear Branch Data Error ", ex.Message.ToString()));
                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Clear Branch Data Error ", ex.Message.ToString()) });
            }
        }
        #endregion

        #region CIFTimer
        private void oCIFTimer_Tick(object sender, EventArgs e)
        {
            string sNow = DateTime.Now.ToString("HHmm");
            try
            {
                for (int i = 0; i < oBranchINList.Count; i++)
                {
                    if (oBranchINList[i].CIFFilterTime  == sNow)
                    {
                        string sFile = oBranchINList[i].CIFFilterSetting;

                        if (!File.Exists(oBranchINList[i].CIFFilterSetting))
                        {
                            FileNotFoundException oFileEx = new FileNotFoundException(String.Format("{0} Not Exist", oBranchINList[i].CIFFilterSetting));
                            log.Error(String.Format("Run CIF Get Branch {0} ini File Error！", oBranchINList[i].BranchCode), oFileEx);
                            this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Run CIF Get Branch {0} ini File Error！", oBranchINList[i].CIFFilterSetting) });
                        }
                        else
                        {
                            log.Info(String.Format("Get Branch {0} Ini File {1}", oBranchINList[i].BranchCode, sFile));
                            this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Get Branch {0} Ini File {1}", oBranchINList[i].BranchCode, sFile) });

                            string[] oLineList = System.IO.File.ReadAllLines(sFile);
                            log.Info(String.Format("Read Branch {0} Ini File {1}", oBranchINList[i].BranchCode, sFile));
                            this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Read Branch {0} Ini File {1}", oBranchINList[i].BranchCode, sFile) });

                            for (int j = 0; j < oBranchINList[i].ValueSettingList.Length; j++)
                            {
                                #region Check Need Replace Value
                                string sCheckValue = oBranchINList[i].ValueSettingList[j].ID.ToString().Trim();
                                for (int k = 0; k < oLineList.Length; k++)
                                {
                                    if (oLineList[k].Length >= sCheckValue.Length)
                                    {
                                        if (sCheckValue == oLineList[k].Substring(0, sCheckValue.Length))
                                        {
                                            //<ID>OutFileSpec</ID>
                                            //<Value> = D:\Prime\BOTOSB\0114_CIF\0114CIF{0}.out</Value>
                                            //<ValueFormat>yyyyMMdd</ValueFormat>
                                            //<DateAdd>-1</DateAdd>
                                            int iDateAdd = 0;
                                            DateTime dDate = DateTime.Now;
                                            try
                                            {
                                                iDateAdd = int.Parse(oBranchINList[i].ValueSettingList[j].DateAdd);
                                                dDate = dDate.AddDays(iDateAdd);
                                            }
                                            catch
                                            {

                                            }
                                            oLineList[k] = sCheckValue + " " + String.Format(oBranchINList[i].ValueSettingList[j].IDValue, dDate.ToString(oBranchINList[i].ValueSettingList[j].ValueFormat));

                                            log.Info(String.Format("{0} Replace Branch {1} Ini File {2}", j+1, oBranchINList[i].BranchCode, oLineList[k]));
                                            this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("{0} Replace Branch {1} Ini File {2}", j + 1, oBranchINList[i].BranchCode, oLineList[k]) });

                                            break;
                                        }

                                    }
                                }
                                #endregion
                            }

                            log.Info(String.Format("Write Branch {0} Ini File {1}", oBranchINList[i].BranchCode, sFile));
                            this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Write Branch {0} Ini File {1}", oBranchINList[i].BranchCode, sFile) });

                            System.IO.File.WriteAllLines(sFile, oLineList);
                            Thread.Sleep(2000);
                            string[] oObj = oBranchINList[i].CIFFilterCMD.Trim().Split(' ');

                            if (oObj.Length > 1)
                            {
                                try
                                {
                                    Process.Start(oObj[0].Trim(), oObj[1].Trim());
                                    log.Info(String.Format("Process Branch {0} Cmd {1}", oBranchINList[i].BranchCode, oBranchINList[i].CIFFilterCMD));
                                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Branch {0} Cmd {1}", oBranchINList[i].BranchCode, oBranchINList[i].CIFFilterCMD) });                                
                                }
                                catch (Exception ex)
                                {
                                    log.Info(String.Format("Error Process Branch {0} Cmd {1}", oBranchINList[i].BranchCode, oBranchINList[i].CIFFilterCMD));
                                    this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Error Process Branch {0} Cmd {1}", oBranchINList[i].BranchCode, oBranchINList[i].CIFFilterCMD) });
                                }
                            }    
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info(String.Format("Clear CIF Branch Data Error ", ex.Message.ToString()));
                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Clear CIF Branch Data Error ", ex.Message.ToString()) });
            }
        }
        #endregion

    }
}


#region Old Function
//private void oBkWork_DoWork(object sender, DoWorkEventArgs e)
//{

//    while ((true))
//    {
//        Thread.Sleep(10000);

//        try
//        {
//            if (oFileList.Count > 0)
//            {

//                //File content layout
//                //01  FD-RSUT-REC.                                                    
//                //    03 FD-RSUT-ITEM                  PIC  X(01).                    S: SWIFT    F: FED
//                //    03 FD-RSUT-KIND                  PIC  X(01).                    R: 來電        S:去電
//                //    03 FD-RSUT-RECKEY            PIC  X(22).                    
//                //    03 FD-RSUT-RECKEY-R              REDEFINES FD-RSUT-RECKEY.      
//                //       05 FD-RSUT-BRNO               PIC  9(04).                    分行別
//                //       05 FD-RSUT-DATE                PIC  9(08).                    日期
//                //       05 FD-RSUT-MSGTYP           PIC  X(04).                    MESSAGE TYPE
//                //       05 FD-RSUT-SEQ                  PIC  9(06).                    電文序號
//                //    03 FD-RSUT-CKSTA                 PIC  X(01).                    Y : 檢核OK, P: 疑似黑名單  , D: 確認黑名單　
//                //    03 FD-RSUT-CRTIME                PIC  X(14).                    
//                //    03 FILLER                                PIC  X(02). 
//                //File content sample
//                //SR011420100104103 088888Y
//                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Text File {0}\r\n", oFileList[0]) });
//                string sStrValue = System.IO.File.ReadAllText(oFileList[0], System.Text.Encoding.Default);
//                string sBranchCode = sStrValue.Substring(2, 4);
//                sStrValue = sStrValue.Substring(0, 25);
//                File.AppendAllText(GetOutputFileName(FileOutPath , sBranchCode ), sStrValue + "\r\n");
//                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Append File {0}\r\n", sBranchCode) });
//                File.Delete(oFileList[0]);
//                this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { String.Format("Process Delete File {0}\r\n", sBranchCode) });
//                oFileList.RemoveAt(0);
//            }
//        }
//        catch (Exception ex)
//        {
//            log.Error("oBkWork_DoWork Error", ex);
//            this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { "oBkWork_DoWork Error" });
//            e.Cancel = true;
//            this.BeginInvoke(new GetSendResult(OnGetSendResult), new object[] { "oBkWork_DoWork Cancel" });
//        }
//    }
//}
#endregion