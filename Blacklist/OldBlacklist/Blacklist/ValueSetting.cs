﻿using System;
using System.Data.SqlClient;
using System.Data;

namespace Blacklist
{
    /// <summary>
    /// This object represents the properties and methods of a BranchEntity.
    /// </summary>
    public class ValueSetting
    {
        #region Private member variables
        private string iD = String.Empty;
        private string iDValue = String.Empty;
        private string valueFormat = String.Empty;
        private string dateAdd = String.Empty;
        #endregion

        #region Constructor
        public ValueSetting()
        {
        }


        #region IDataReader Constructor
        /// <summary>
        /// Loads the current datarow
        /// </summary>
        public ValueSetting(IDataReader reader)
        {
            this.LoadFromReader(reader);
        }
        #endregion

        #region DataEntity Constructor

        #endregion



        #endregion

        #region Private functions
        #region Load datareader
        /// <summary>
        /// Loads the current datareader
        /// </summary>
        protected void LoadFromReader(IDataReader reader)
        {
        }

        #endregion
        #endregion

        #region Public properties

        /// <summary>
        /// DataType string
        /// DataSize 10
        /// </summary>
        public string ID
        {
            get { return iD; }
            set { iD = value; }
        }

        /// <summary>
        /// DataType string
        /// DataSize 10
        /// </summary>
        public string IDValue
        {
            get { return iDValue; }
            set { iDValue = value; }
        }

        /// <summary>
        /// DataType string
        /// DataSize 10
        /// </summary>
        public string ValueFormat
        {
            get { return valueFormat; }
            set { valueFormat = value; }
        }

        /// <summary>
        /// DataType string
        /// DataSize 10
        /// </summary>
        public string DateAdd
        {
            get { return dateAdd; }
            set { dateAdd = value; }
        }
        #endregion
    }
}
