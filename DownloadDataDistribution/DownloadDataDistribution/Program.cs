﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Configuration;
using System.Diagnostics;

namespace AutoloadAutoAssistance
{
    class Program
    {

        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Start");
            CookieContainer cookieContainer = new CookieContainer();

            // 1. 登入
            ///////////////////////////////////////////////////
            string URI = ConfigurationManager.AppSettings.Get("LoginLink");
            // 用户名和密码
            string userName = ConfigurationManager.AppSettings.Get("LoginName");
            string password = ConfigurationManager.AppSettings.Get("LoginPassword");

            string formatString =
                 "username={0}&password={1}";
            string postString =
                    string.Format(formatString, userName, password);

            // 将提交的字符串数据转换成字节数组
            byte[] postData = Encoding.ASCII.GetBytes(postString);
            Console.WriteLine("HttpWebRequest");
            // 设置提交的相关参数
            HttpWebRequest request = WebRequest.Create(URI) as HttpWebRequest;
            request.Method = "POST";
            request.KeepAlive = false;
            request.ContentType = "application/x-www-form-urlencoded";
            request.CookieContainer = cookieContainer;
            request.ContentLength = postData.Length;
            //Proxy
            Console.WriteLine("Proxy1");
            WebProxy myProxy = new WebProxy(ConfigurationManager.AppSettings.Get("ProxyServer"));
            request.Proxy = myProxy;
            Console.WriteLine("Stream write");
            // 提交请求数据
            Stream outputStream = request.GetRequestStream();
            outputStream.Write(postData, 0, postData.Length);
            outputStream.Close();

            Console.WriteLine("Response1");
            // 接收返回的页面
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new System.IO.StreamReader(responseStream, Encoding.GetEncoding("UTF-8"));
            string srcString = reader.ReadToEnd();

            // 3. 打开 Default.aspx 页面
            ///////////////////////////////////////////////////
            // 设置打开页面的参数
            Console.WriteLine("HttpWebRequest2");
            URI = ConfigurationManager.AppSettings.Get("DownloadLink");
            request = WebRequest.Create(URI) as HttpWebRequest;
            request.Method = "GET";
            request.KeepAlive = false;
            request.CookieContainer = cookieContainer;
            Console.WriteLine("Proxy2");
            //Proxy
            request.Proxy = myProxy;
            // 接收返回的页面
            Console.WriteLine("Response2");
            response = request.GetResponse() as HttpWebResponse;
            string downloadexe = response.ResponseUri.ToString();
            Console.WriteLine("Create file directory");
            if (!Directory.Exists(ConfigurationManager.AppSettings.Get("DownLoadPath")))
            {
                Directory.CreateDirectory(ConfigurationManager.AppSettings.Get("DownLoadPath"));
            }
            if (File.Exists(ConfigurationManager.AppSettings.Get("DownLoadPath")))
                File.Delete(ConfigurationManager.AppSettings.Get("DownLoadPath"));

            Console.WriteLine("Set Web client proxy");
            WebClient wc = new WebClient();
            wc.Proxy = myProxy;
            Console.WriteLine("Path combined");
            string downloadpath= Path.Combine(ConfigurationManager.AppSettings.Get("DownLoadPath"), ConfigurationManager.AppSettings.Get("FileName"));
            Console.WriteLine("File download webclient");
            wc.DownloadFile(downloadexe, downloadpath);
            Console.WriteLine("End");

        }
   
    }
}
