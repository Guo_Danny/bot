﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace Unisys.AML.Common
{
    public class Log
    {
        private static readonly ILog log = LogManager.GetLogger("App.Logging");

        public static void WriteToLog(string level, string message, params object[] arguments)
        {
            message = String.Format(message, arguments);

            Fortify fortifyService = new Fortify();

            string path_for_fortify = fortifyService.PathManipulation(message);

            switch (level)
            {
                case "Fatal":
                    {
                        if (log.IsFatalEnabled)
                            log.Fatal(path_for_fortify);
                        break;
                    }
                case "Error":
                    {
                        if (log.IsErrorEnabled)
                            log.Error(path_for_fortify);
                        break;
                    }
                case "Warn":
                    {
                        if (log.IsWarnEnabled)
                            log.Warn(path_for_fortify);
                        break;
                    }
                case "Info":
                    {
                        if (log.IsInfoEnabled)
                            log.Info(path_for_fortify);
                        break;
                    }
                case "Debug":
                    {
                        if (log.IsDebugEnabled)
                            log.Debug(path_for_fortify);
                        break;
                    }
            }
        }
    }
}
