﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Unisys.AML.Domain;
using System.Data;

namespace Unisys.AML.DataAccess
{
    public interface ICustomerRepository
    {
        Customer GetByID(int id);
        List<Customer> GetByName(string name, string filter = "");
        List<Customer> GetByNameLike(string name);
        string GetSequence(string objectType);
        void insertlog(string str_id, string str_logtext);
        void Insert(string id, string parent, string name, string ownerBranch, string ownerDept, string riskClass, string createOper, 
                    string type, int status, string indOrBusType, string address, string country);
        void Insertlog2(string str_type, string str_objType, string str_id, string str_logtext);
        void RefreshMode(string strMode);
        DataSet CheckRelation(string strPartyId, string strRelation, string strRelatedParty, string strRelatedSource);
        void InsertRelation(string strPartyId, string strRelation, string strRelatedParty, string strRelatedSource);

    }
}
