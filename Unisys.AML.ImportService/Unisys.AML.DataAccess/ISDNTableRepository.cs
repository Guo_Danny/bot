﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Unisys.AML.Domain;

namespace Unisys.AML.DataAccess
{
    public interface ISDNTableRepository
    {
        bool Export(string listType, string program, string country, string outFile);
        SDNTable SelectByListModifDate(string listType, string program, string country, int modifDay);
        List<SDNTable> Select(string listType, string program, string country, bool order=false);
        bool Insert(List<SDNTable> SDNTables, string listType, string program);
        bool Delete(string listType, string program);
        bool Delete(List<SDNTable> SDNTables, string listType, string program);
        bool Update(List<SDNTable> SDNTables, string listType, string program);
    }
}
