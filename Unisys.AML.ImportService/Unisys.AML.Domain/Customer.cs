using System;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;

namespace Unisys.AML.Domain
{
    /// <summary>
    /// This object represents the properties and methods of a Customer.
    /// </summary>
    [Serializable]
    public class Customer
    {
        public string Id { get; set; }
        public string Parent { get; set; }
        public string Name { get; set; }
    }
}
