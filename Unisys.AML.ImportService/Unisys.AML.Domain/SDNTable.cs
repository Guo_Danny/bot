using System;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;

namespace Unisys.AML.Domain
{
    /// <summary>
    /// This object represents the properties and methods of a SDNTable.
    /// </summary>
    [Serializable]
    public class SDNTable
    {
        public string EntNum { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Program { get; set; }
        public string Type { get; set; }
        public string Remarks { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string SDNType { get; set; }
        public string DataType { get; set; }
        public string UserRec { get; set; }
        public string Deleted { get; set; }
        public string DuplicRec { get; set; }
        public string IgnoreDerived { get; set; }
        public string Status { get; set; }
        public string Country { get; set; }
    }
}
