﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Unisys.AML.Service;

namespace Unisys.AML.ImportService.Test
{
    [TestFixture]
    public class TestClass
    {
        [TestCase]
        public void GetSequenceTest()
        {
            CustomerService customerService = new CustomerService();
            string result = customerService.GetSequence("CUST");
            Assert.AreEqual(40, result);
        }

        [TestCase]
        public void InsertTest()
        {
            CustomerService customerService = new CustomerService();
            string result = customerService.GetSequence("CUST");
            customerService.Insert(result, result, "TEST", "0138", "AML", "DefClass", "PRIMEADMIN", "I", 1, "I","address","country");
        }
    }
}
