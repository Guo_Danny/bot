﻿using System;
using System.Configuration;

namespace Unisys.AML.DirectoryWatcher
{
    public class DirectoryToWatch : ConfigurationElement
    {
        [ConfigurationProperty("path", IsRequired = true)]
        public string Path
        {
            get
            {
                return (string)base["path"];
            }
        }

        [ConfigurationProperty("fileSetsToWatch", IsRequired = true)]
        public FileSetToWatchCollection FileSetsToWatch
        {
            get
            {
                return (FileSetToWatchCollection)base["fileSetsToWatch"];
            }
        }
    }
}
