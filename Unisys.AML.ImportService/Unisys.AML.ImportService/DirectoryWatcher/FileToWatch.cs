﻿using System;
using System.Configuration;
using System.Configuration.Internal;
using System.Xml;

namespace Unisys.AML.DirectoryWatcher
{
    public class FileToWatch : ConfigurationElement
    {
        [ConfigurationProperty("path", DefaultValue = "")]
        public string Path
        {
            get
            {
                return (string)base["path"];
            }
        }

        [ConfigurationProperty("type", DefaultValue = "")]
        public string Type
        {
            get
            {
                return (string)base["type"];
            }
        }
    }
}
