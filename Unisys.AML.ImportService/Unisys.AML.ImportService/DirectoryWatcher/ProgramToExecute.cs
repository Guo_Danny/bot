﻿using System;
using System.Configuration;
using System.Configuration.Internal;
using System.Xml;

namespace Unisys.AML.DirectoryWatcher
{
    public class ProgramToExecute : ConfigurationElement
    {
        [ConfigurationProperty("path", DefaultValue = "")]
        public string Path
        {
            get
            {
                return (string)base["path"];
            }
        }

        [ConfigurationProperty("arguments", DefaultValue = "")]
        public string Arguments
        {
            get
            {
                return (string)base["arguments"];
            }
        }

        [ConfigurationProperty("delay", DefaultValue = "0")]
        public string Delay
        {
            get
            {
                return (string)base["delay"];
            }
        }

        [ConfigurationProperty("audit", DefaultValue = "false")]
        public string Audit
        {
            get
            {
                return (string)base["audit"];
            }
        }
    }
}
