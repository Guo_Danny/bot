﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using Unisys.AML.Common;
using Unisys.AML.DirectoryWatcher;
using Unisys.AML.Domain;
using Unisys.AML.Service;
using System.Text;

namespace Unisys.AML.ImportService
{
    public partial class ImportService : ServiceBase
    {
        private WatchInformation watchInformation = null;
        private Dictionary<string, Dictionary<string, FileSystemWatcher>> watchers = new Dictionary<string, Dictionary<string, FileSystemWatcher>>();

        private AutoMode Mode;
        private string CustomerFilter;
        private string import_duplicate;

        enum AutoMode
        {
            SG,
            US
        }

        internal void Start(object p)
        {
            OnStart(null);
        }

        public static IScheduler Scheduler { get; private set; }

        public ImportService()
        {
            InitializeComponent();

            switch (ConfigurationManager.AppSettings["AutoCustomer"])
            {
                case "0":
                    Mode = AutoMode.US;
                    break;
                case "1":
                    Mode = AutoMode.SG;
                    break;
                default:
                    Mode = AutoMode.SG;
                    break;
            }
            CustomerFilter = ConfigurationManager.AppSettings["AutoCustomerFilter"];

            import_duplicate = ConfigurationManager.AppSettings["duplicate_allow"];
        }

        protected override void OnStart(string[] args)
        {
            Log.WriteToLog("Info", "Starting up the Import service.");
            try
            {
                StartScheduler();

                watchInformation = (WatchInformation)ConfigurationManager.GetSection("watchInformation");
                foreach (DirectoryToWatch directoryToWatch in watchInformation.DirectoriesToWatch)
                {
                    foreach (FileSetToWatch fileSetToWatch in directoryToWatch.FileSetsToWatch)
                    {
                        FileSystemWatcher watcher = new FileSystemWatcher(directoryToWatch.Path);
                        watcher.Filter = fileSetToWatch.MatchExpression;
                        watcher.Created += new FileSystemEventHandler(watcher_OnChanged);
                        if (!watchers.ContainsKey(directoryToWatch.Path))
                            watchers[directoryToWatch.Path] = new Dictionary<string, FileSystemWatcher>();
                        watchers[directoryToWatch.Path][watcher.Filter] = watcher;
                        watcher.EnableRaisingEvents = true;
                        Log.WriteToLog("Info", "Added watcher for the path \"{0}\" and \"{1}\".", directoryToWatch.Path, fileSetToWatch.MatchExpression);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred while starting the service. Type: {0} Message: {1}", ex.GetType().FullName, ex.Message);
                throw;
            }
        }

        protected override void OnStop()
        {
            Log.WriteToLog("Info", "Stopping the Directory Watcher service.");
            try
            {
                foreach (string directory in watchers.Keys)
                {
                    foreach (string filter in watchers[directory].Keys)
                        watchers[directory][filter].EnableRaisingEvents = false;
                }

                StopScheduler();
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred while stoping the service. Type: {0}\nMessage: {1}", ex.GetType().FullName, ex.Message);
                throw;
            }
        }

        private void StartScheduler()
        {
            try
            {
                Scheduler = new StdSchedulerFactory().GetScheduler();

                if (!Scheduler.IsStarted)
                {
                    Scheduler.Start();
                    while (!Scheduler.IsStarted)
                    {
                        Log.WriteToLog("Info", "Waiting for scheduler to start.");
                        Thread.Sleep(1000);
                    }
                    Log.WriteToLog("Info", "IsStarted={0}", Scheduler.IsStarted);
                    Log.WriteToLog("Info", "SchedulerInstanceId={0}", Scheduler.SchedulerInstanceId);
                    Log.WriteToLog("Info", "SchedulerName={0}", Scheduler.SchedulerName);
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "StartScheduler: Message: {0}", ex.Message);
            }
        }

        private void StopScheduler()
        {
            try
            {
                if (Scheduler != null)
                {
                    Log.WriteToLog("Info", "Shutting down scheduler");
                    Scheduler.Shutdown(false);
                    while (!Scheduler.IsShutdown)
                    {
                        Log.WriteToLog("Info", "Waiting for scheduler to shutdown.");
                        Thread.Sleep(1000);
                    }
                    Log.WriteToLog("Info", "IsShutdown={0}", Scheduler.IsShutdown);
                    Log.WriteToLog("Info", "The scheduler has been shutdown.");
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "StopScheduler: Message: {0}", ex.Message);
            }
        }

        private void AddCommandLineJob(string programPath, string programArguments, string filePath, string programDelay)
        {
            int delay = int.Parse(programDelay);
            Log.WriteToLog("Debug", "ProgramPath={0} ProgramArguments={1} ProgramDelay={2}", programPath, programArguments, delay);

            //201905 for relation import 
            if (filePath.ToUpper().Contains(programPath.ToUpper()))
            {
                Thread.Sleep(delay * 1000);

                string str_logpath = ConfigurationManager.AppSettings["RelationParty_log"].ToString();

                InsertRelationParty(ConfigurationManager.AppSettings["ImportRelationParty_Mode"].ToString(), filePath, str_logpath);
            }
            else
            {
                CommandLineJob commandLineJob = new CommandLineJob();
                commandLineJob.ProgramPath = programPath;
                commandLineJob.ProgramArguments = programArguments;
                commandLineJob.FilePath = filePath;
                Scheduler.ScheduleJob(
                    JobBuilder.Create<CommandLineJob>()
                    .UsingJobData(commandLineJob.BuildJobDataMap())
                    .Build(),
                    TriggerBuilder.Create()
                    .StartAt(DateTime.UtcNow.AddSeconds(delay + 1))
                    .Build());
            }
            
        }

        protected void watcher_OnChanged(object source, FileSystemEventArgs e)
        {
            bool success = false;
            int retries = 100;
            int recordCount = 0;

            try
            {
                FileSystemWatcher watcher = (FileSystemWatcher)source;

                Fortify fortifyService = new Fortify();

                string targetFilePath = Path.Combine(fortifyService.PathManipulation(watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].FileToWatch.Path), fortifyService.PathManipulation(e.Name));

                string path_for_fortify = fortifyService.PathManipulation(targetFilePath);

                if (watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].FileToWatch.Type == "Move")
                {
                    Log.WriteToLog("Debug", "DirectoryToWatch.Type={0}", watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].FileToWatch.Type);
                    if (File.Exists(targetFilePath))
                    {
                        try
                        {
                            //2018/06 modify check duplicate file name and decide abort or not?
                            if (import_duplicate == "N" && (fortifyService.PathManipulation(e.FullPath).ToUpper().Contains("ACTIVITY") || fortifyService.PathManipulation(e.FullPath).ToUpper().Contains("ACCOUNT") || fortifyService.PathManipulation(e.FullPath).ToUpper().Contains("CUSTOMER")))
                            {
                                bool bol_success = false;
                                for (int i = 0; i < 5; i++)
                                {
                                    if (!bol_success)
                                        try
                                        {
                                            Log.WriteToLog("Info", "Duplicate file detected try={0}", i + 1);
                                            FileStream fs = null;
                                            bool inUse = true;
                                            try
                                            {
                                                fs = new FileStream(fortifyService.PathManipulation(e.FullPath), FileMode.Open, FileAccess.Read, FileShare.None);
                                                inUse = false;
                                            }
                                            catch
                                            {

                                            }
                                            finally
                                            {
                                                if (fs != null)
                                                    fs.Close();
                                                Log.WriteToLog("Info", "file:{0}, inuse={1};", fortifyService.PathManipulation(e.FullPath), inUse.ToString());
                                            }

                                            if (!inUse)
                                            {
                                                File.Copy(fortifyService.PathManipulation(e.FullPath), string.Format("{0}.{1}", path_for_fortify, DateTime.Now.ToString("HHmmss")), true);
                                                Log.WriteToLog("Info", "import file duplicate,{0}; Stop Import Process!! ", targetFilePath);
                                                File.Delete(fortifyService.PathManipulation(e.FullPath));
                                                Log.WriteToLog("Info", "import file duplicate {0}; file deleted!", fortifyService.PathManipulation(e.FullPath));
                                                return;
                                            }
                                            else
                                            {
                                                File.Copy(fortifyService.PathManipulation(e.FullPath), string.Format("{0}.{1}", path_for_fortify, DateTime.Now.ToString("HHmmss")), true);
                                                Log.WriteToLog("Info", "import file duplicate,{0}; Stop Import Process!! ", targetFilePath);
                                                File.Delete(fortifyService.PathManipulation(e.FullPath));
                                                Log.WriteToLog("Info", "import file duplicate {0}; file deleted!", fortifyService.PathManipulation(e.FullPath));
                                                return;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Log.WriteToLog("Error", "File Copy or Delete Error:{0}, Retry!", ex.ToString());
                                        }
                                }
                            }

                            File.Move(path_for_fortify, string.Format("{0}.{1}", path_for_fortify, DateTime.Now.ToString("HHmmss")));
                        }
                        catch (Exception ex)
                        {
                            Log.WriteToLog("Error", "Exception occurred while moving the file. File: {0} Message: {1}", targetFilePath, ex.Message);
                        }
                    }

                    while (!success)
                    {
                        try
                        {
                            Thread.Sleep(1000);
                            if (File.Exists(e.FullPath))
                                File.Move(fortifyService.PathManipulation(e.FullPath), path_for_fortify);
                            else
                                Log.WriteToLog("Info", "File {0} not found", e.FullPath);
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            if (--retries == 0)
                            {
                                Log.WriteToLog("Error", "Exception occurred while moving the file. File: {0} Message: {1}", e.FullPath, ex.Message);
                                break;
                            }
                        }

                        if (success)
                        {
                            Log.WriteToLog("Info", "Move {0} To {1}", e.FullPath, targetFilePath);
                        }
                    }
                }
                else if (watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].FileToWatch.Type.Contains("Write"))
                {
                    int i_dely = int.Parse(ConfigurationManager.AppSettings["customer_dely"]);
                    Thread.Sleep(1000 * i_dely);

                    Log.WriteToLog("Debug", "DirectoryToWatch.Type={0}", watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].FileToWatch.Type);
                    if (Path.GetExtension(targetFilePath) != ConfigurationManager.AppSettings["FileExtension"])
                        targetFilePath = targetFilePath + ConfigurationManager.AppSettings["FileExtension"];
                    recordCount = WriteCsvFileToFile(e.FullPath, targetFilePath, watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].FileToWatch.Type);
                    success = true;
                }

                if (!success)
                    return;

                ProgramToExecute programToExecute = watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].ProgramToExecute;
                if (programToExecute != null && programToExecute.Path != string.Empty)
                {
                    AddCommandLineJob(programToExecute.Path, programToExecute.Arguments, targetFilePath, programToExecute.Delay);
                    if (programToExecute.Audit == "true")
                        AuditLog.WriteToLog(e.FullPath, programToExecute.Arguments, "Success", recordCount.ToString());
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "watcher_OnChanged: Message: {0}", ex.Message);
            }
        }

        public int WriteCsvFileToFile(string sourceFilePath, string targetFilePath, string type)
        {
            int recordCount = 0;
            string custID = string.Empty;
            CsvFile csvFile = new CsvFile();
            CustomerService customerService = new CustomerService();
            Fortify fortifyService = new Fortify();

            //20170905 edit by danny 
            //fix SG activity import deadlock problem.
            int idely = int.Parse(ConfigurationManager.AppSettings["activity_dely"]);
            // for import retry dely
            int idely2 = int.Parse(ConfigurationManager.AppSettings["activity_dely2"]);
            int int_try = int.Parse(ConfigurationManager.AppSettings["import_try"]);
            int int_try_limit = int.Parse(ConfigurationManager.AppSettings["import_try_limit"]);

            string str_activity_for_i = ConfigurationManager.AppSettings["act_I"].ToString();

            try
            {
                csvFile.Populate(sourceFilePath, false, true);
                recordCount = csvFile.RecordCount;

                string watcherPath = Path.GetDirectoryName(sourceFilePath) + "\\";

                if (type.Contains("Replace"))
                {
                    Log.WriteToLog("Info", "Wait modification for " + idely + " sec");
                    Thread.Sleep(idely * 1000);

                    for (int i = 0; i < csvFile.RecordCount; i++)
                    {
                        bool bol_try = false;

                        for (int j = 0; j < int_try; j++)
                        {
                            try
                            {
                                if (i * j >= int_try_limit) //try 超過一定次數就跳出不要再try
                                    break;
                                if (bol_try == true)
                                    break;
                                if (j > 0)
                                    Thread.Sleep(idely2 * 1000);

                                string RecvPay = csvFile[i, 5];                 // 1: Recive, 2:Pay
                                string CustomerID = csvFile[i, 0].Trim();       // Customer ID

                                string ByOrderCustId = csvFile[i, 37].Trim();   // Originator Customer ID
                                string ByOrder = csvFile[i, 38].Trim();         // Originator Customer Name
                                string ByOrderAddress = csvFile[i, 40].Trim();  // Originator Address
                                string ByOrderCountry = csvFile[i, 44].Trim();  // Originator Country

                                string BeneCustId = csvFile[i, 11].Trim();      // Benbeneficiary Customer ID
                                string Bene = csvFile[i, 12].Trim();            // Benbeneficiary Customer Name
                                string BeneAddress = csvFile[i, 14].Trim();     // Benbeneficiary Address
                                string BeneCountry = csvFile[i, 27].Trim();     // Benbeneficiary Country

                                string Branch = csvFile[i, 55];
                                string Department = csvFile[i, 56];

                                Log.WriteToLog("Debug", "Ori({0}):RecvPay={1}, CustomerID={2}, ByOrderCustId={3}, ByOrder={4}, BeneCustId={5}, Bene={6}",
                                                        (i + 1).ToString().PadLeft(4, '0'), RecvPay, CustomerID, ByOrderCustId, ByOrder, BeneCustId, Bene);

                                if (string.IsNullOrEmpty(CustomerID) || RecvPay == "1")
                                {
                                    if (string.IsNullOrEmpty(BeneCustId) && !string.IsNullOrEmpty(Bene))
                                        csvFile[i, 11] = GetCustID(Bene, Branch, Department, BeneAddress, BeneCountry);
                                }

                                if (string.IsNullOrEmpty(CustomerID) || RecvPay == "2")
                                {
                                    if (string.IsNullOrEmpty(ByOrderCustId) && !string.IsNullOrEmpty(ByOrder))
                                        csvFile[i, 37] = GetCustID(ByOrder, Branch, Department, ByOrderAddress, ByOrderCountry);
                                }

                                if(str_activity_for_i=="Y")
                                {
                                    //2019.08 SD request change the bene and byord
                                    if (string.IsNullOrEmpty(CustomerID))
                                    {
                                        if (RecvPay == "1")                 //RecvPay=receive
                                            csvFile[i, 0] = csvFile[i, 11]; //BeneCustId
                                        else if (RecvPay == "2")            //RecvPay=pay
                                            csvFile[i, 0] = csvFile[i, 37]; //ByOrderCustId
                                    }
                                    else
                                    {
                                        if (Mode == AutoMode.SG)
                                        {
                                            if (RecvPay == "1")                 //RecvPay=receive
                                                csvFile[i, 11] = csvFile[i, 0]; //BeneCustId
                                            else if (RecvPay == "2")            //RecvPay=pay
                                                csvFile[i, 37] = csvFile[i, 0]; //ByOrderCustId
                                        }
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(CustomerID))
                                    {
                                        if (RecvPay == "1")                 //RecvPay=receive
                                            csvFile[i, 0] = csvFile[i, 37]; //ByOrderCustId
                                        else if (RecvPay == "2")            //RecvPay=pay
                                            csvFile[i, 0] = csvFile[i, 11]; //BeneCustId
                                    }
                                    else
                                    {
                                        if (Mode == AutoMode.SG)
                                        {
                                            if (RecvPay == "1")                 //RecvPay=receive
                                                csvFile[i, 37] = csvFile[i, 0]; //ByOrderCustId
                                            else if (RecvPay == "2")            //RecvPay=pay
                                                csvFile[i, 11] = csvFile[i, 0]; //BeneCustId
                                        }
                                    }
                                }

                                

                                Log.WriteToLog("Debug", "Mod({0}):RecvPay={1}, CustomerID={2}, ByOrderCustId={3}, ByOrder={4}, BeneCustId={5}, Bene={6}",
                                                        (i + 1).ToString().PadLeft(4, '0'), RecvPay, csvFile[i, 0], csvFile[i, 37], ByOrder, csvFile[i, 11], Bene);
                                bol_try = true;
                            }
                            catch(Exception e)
                            {
                                Log.WriteToLog("Error", "Activity try {0}, Modify Error:{1}", (j + 1), e.ToString());
                                customerService.InsertLog("", string.Format("BSA Activity import fail, retry {0}, please check the amlimport log for detail message.", (j + 1)));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                recordCount = 0;
                Log.WriteToLog("Error", "WriteCsvFileToFile: {0} To: {1} Message: {2}", sourceFilePath, targetFilePath, ex.Message);
            }
            finally
            {
                using (CsvWriter writer = new CsvWriter())
                {
                    writer.WriteCsv(csvFile, targetFilePath);
                }

                if (type.Contains("Replace"))
                    File.Copy(fortifyService.PathManipulation(targetFilePath), fortifyService.PathManipulation(sourceFilePath) + ".mod");

                Log.WriteToLog("Info", "Write {0} To {1}", sourceFilePath, targetFilePath);
                
            }
            return recordCount;
        }

        public string GetCustomerByNameLike(string name)
        {
            string custID = string.Empty;
            try
            {
                string custName = name.Length >= int.Parse(ConfigurationManager.AppSettings["CustNameLen"]) ? name.Substring(0, int.Parse(ConfigurationManager.AppSettings["CustNameLen"])) : name;
                Log.WriteToLog("Debug", "custName={0}", custName);

                CustomerService customerService = new CustomerService();
                List<Customer> customers = customerService.GetByNameLike(custName);
                if (customers != null && customers.Count > 0)
                {
                    Log.WriteToLog("Info", "customers.Count={0}", customers.Count);
                    if (customers.Count == 1)
                    {
                        custID = customers[0].Id;
                        Log.WriteToLog("Debug", "customer.Id={0} customer.Name={1}", custID, customers[0].Name);
                        return custID;
                    }
                    else if (customers.Count > 1)
                    {
                        var foundCustomer = customers.FirstOrDefault(x => x.Name == name);
                        if (foundCustomer != null)
                        {
                            custID = foundCustomer.Id;
                            Log.WriteToLog("Debug", "foundCustomer.Id={0} foundCustomer.Name={1}", custID, foundCustomer.Name);
                            return custID;
                        }
                        //Customer noncustCustomer = customers.SingleOrDefault(x => x.Id.Contains("NONCUST"));
                        //if (noncustCustomer != null)
                        //{
                        //    custID = noncustCustomer.Id;
                        //    Log.WriteToLog("Debug", "noncustCustomer.Id={0} noncustCustomer.Name={1}", custID, noncustCustomer.Name);
                        //    return custID;
                        //}
                        //custID = customers[0].Id;
                        //Log.WriteToLog("Debug", "customer[0].Id={0} customer[0].Name={1}", custID, customers[0].Name);
                        return custID;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "GetCustomerByName: Message: {0}", ex.Message);
                throw ex;
            }
        }

        public string GetCustomerByName(string name, string ownerBranch, string ownerDept, string address, string country)
        {
            string custID = string.Empty;

            try
            {
                CustomerService customerService = new CustomerService();
                List<Customer> customers = customerService.GetByName(name, CustomerFilter);

                if (customers != null && customers.Count > 0)
                {
                    if (customers.Count == 1)
                    {
                        custID = customers[0].Id;
                        Log.WriteToLog("Debug", "customer.Id={0} customer.Name={1} search from 1 Record.", custID, customers[0].Name);
                        return custID;
                    }
                    else
                    {
                        Customer noncustCustomer = customers.FirstOrDefault(x => x.Id.Contains("NONCUST"));
                        if (noncustCustomer == null)
                        {
                            custID = customers[0].Id;
                            Log.WriteToLog("Debug", "customer[0].Id={0} customer[0].Name={1}", custID, customers[0].Name);
                        }
                        else
                        {
                            custID = noncustCustomer.Id;
                            Log.WriteToLog("Debug", "noncustCustomer.Id={0} noncustCustomer.Name={1}", custID, noncustCustomer.Name);
                        }
                        return custID;
                    }
                }
                else
                {
                        string type = ConfigurationManager.AppSettings["AutoCustomerType"];
                        string indOrBusType = ConfigurationManager.AppSettings["AutoCustomerIndOrBusType"];
                        return AddCustomer(name, ownerBranch, ownerDept, "DefClass", "PRIMEADMIN",
                                           type, 1,
                                           indOrBusType,
                                           address,
                                           country);
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "GetCustomerByName: Message: {0}", ex.Message);
                return null;
            }
        }

        public string AddCustomer(string name, string ownerBranch, string ownerDept, string riskClass, string createOper, 
                                  string type, int status, string indOrBusType, string address, string country)
        {
            try
            {
                CustomerService customerService = new CustomerService();
                string id = customerService.GetSequence("CUST");
                string EntityCustomer = ConfigurationManager.AppSettings["EntityCustomer"];

                if (!string.IsNullOrEmpty(EntityCustomer) && EntityCustomer.Split('|').ToList().Any(s => name.ToUpper().Contains(s)))
                {
                    type = "E";
                    indOrBusType = "B";
                }

                customerService.Insert(id, id, name, ownerBranch, ownerDept, riskClass, createOper, type, 
                                       status, indOrBusType, address, country);
                return id;
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "InsertCustomer: Message: {0}", ex.Message);
                return null;
            }
        }

        private string GetCustID(string name, string branch, string department, string address, string country)
        {
            string CustID = string.Empty;

            if (!string.IsNullOrEmpty(name))
            {
                switch (Mode)
                {
                    case AutoMode.SG:
                        CustID = GetCustomerByName(name, branch, department, address, country);
                        break;
                    case AutoMode.US:
                        CustID = GetCustomerByNameLike(name);
                        break;
                    default:
                        break;
                }
            }

            return CustID;
        }

        private void InsertRelationParty(string strMode, string sourceFilePath, string Log_Path)
        {
            try
            {
                Log.WriteToLog("Info", "RelationParty Import Start.");

                if (File.Exists(Log_Path))
                {
                    File.Move(Log_Path, Log_Path + "." + DateTime.Now.ToString("MMddHHmmss"));
                }

                int i_countduplicate = 0;
                int i_countinsert = 0;
                int i_countError = 0;
                int iexists = 0;
                string strPartyId = "";
                string strRelation = "";
                string strRelatedParty = "";
                string strRelatedSource = "";
                CustomerService customerService = new CustomerService();

                customerService.RefreshMode(strMode);
                Log.WriteToLog("Info", "Import Relation refresh Mode=", strMode);

                CsvFile csvFile = new CsvFile();
                csvFile.Populate(sourceFilePath, false, true);

                StreamWriter sw = new System.IO.StreamWriter(Log_Path);
                StringBuilder sb = new StringBuilder();

                sb.AppendLine(string.Format("[RelationParty] RelationParty Import Processed File {0} Start at {1}", Log_Path, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"))).AppendLine();

                for (int i = 0; i < csvFile.RecordCount; i++)
                {
                    strPartyId = csvFile[i, 0];
                    strRelation = csvFile[i, 1];
                    strRelatedParty = csvFile[i, 2];
                    strRelatedSource = csvFile[i, 3];
                    try
                    {
                        if (strPartyId != "" && strRelation != "" && strRelatedParty != "")
                        {
                            iexists = customerService.CheckRelation(strPartyId, strRelation, strRelatedParty, strRelatedSource);

                            if (iexists > 0)
                            {
                                i_countduplicate = i_countduplicate + 1;
                                Log.WriteToLog("Info", "Import Relation Party duplicate PartyId={0},RelatedParty={1}", strPartyId, strRelatedParty);
                                sb.AppendLine(string.Format("[RelationParty][Duplicates] {0}~{1}~{2}~{3}", strPartyId, strRelation, strRelatedParty, strRelatedSource));
                            }
                            else
                            {
                                i_countinsert = i_countinsert + 1;
                                customerService.InsertRelation(strPartyId, strRelation, strRelatedParty, strRelatedSource);
                                sb.AppendLine(string.Format("[RelationParty][Inserts]    {0}~{1}~{2}~{3}", strPartyId, strRelation, strRelatedParty, strRelatedSource));
                            }
                        }
                        else
                        {
                            i_countError = i_countError + 1;
                            Log.WriteToLog("Error", "Import Relation Lost Data:PartyId={0}, Relation:{1}, RelatedParty:{2}", strPartyId, strRelation, strRelatedParty);
                            sb.AppendLine(string.Format("[RelationParty][Errors]     {0}~{1}~{2}~{3}", strPartyId, strRelation, strRelatedParty, strRelatedSource));
                        }
                    }
                    catch(Exception e)
                    {
                        i_countError = i_countError + 1;
                        i_countinsert = i_countinsert - 1;
                        Log.WriteToLog("Error", "Import Relation PartyId={0} Error:{1}", strPartyId, e.ToString());
                        sb.AppendLine(string.Format("[RelationParty][Errors]     {0}~{1}~{2}~{3}", strPartyId, strRelation, strRelatedParty, strRelatedSource));

                    }
                }

                Log.WriteToLog("Info", "Import Relation counts, Insert={0}, Duplicates={1}, Errors={2}, Total={3}", i_countinsert, i_countduplicate, i_countError, i_countinsert + i_countduplicate + i_countError);
                customerService.InsertLog2("Imp", "Event", "", string.Format("Import Relation counts, Insert={0}, Duplicates={1}, Errors={2}, Total={3}", i_countinsert, i_countduplicate, i_countError, i_countinsert + i_countduplicate + i_countError));
                sb.AppendLine().AppendLine(string.Format("[RelationParty][Summary] Insert={0}, Duplicates={1}, Errors={2}, Total={3}", i_countinsert, i_countduplicate, i_countError, i_countinsert + i_countduplicate + i_countError)).AppendLine();
                sb.AppendLine(string.Format("[RelationParty] RelationParty Import End at {0}", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                Log.WriteToLog("Info", "RelationParty Import End.");

                sw.WriteLine(sb.ToString());
                sw.Close();
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "ImportRelationParty: Message: {0}", ex.Message);

                Log.WriteToLog("Info", "RelationParty Import End.");
            }
        }
    }
}
