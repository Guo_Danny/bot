﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Quartz;
using Unisys.AML.Common;
using Unisys.AML.DirectoryWatcher;

namespace Unisys.AML.ImportService
{
    [DisallowConcurrentExecution]
    public class CommandLineJob : QuartzJobBase
    {
        public string ProgramPath { get; set; }
        public string ProgramArguments { get; set; }
        public string FilePath { get; set; }

        public CommandLineJob()
        {
        }

        protected override void InternalExecute(IJobExecutionContext context)
        {
            Log.WriteToLog("Info", "CommandLineJob ProgramPath:{0} ProgramArguments:{1} FilePath:{2}", ProgramPath, ProgramArguments, FilePath);

            try
            {
                if (ProgramPath != string.Empty && ProgramPath.ToUpper().Contains("EXE"))
                {
                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    p.StartInfo.FileName = ProgramPath;
                    p.StartInfo.WorkingDirectory = @"D:\";
                    p.StartInfo.Arguments = ProgramArguments + FilePath;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardInput = true;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.RedirectStandardError = true;
                    p.StartInfo.CreateNoWindow = true;

                    //Log.WriteToLog("Info", "filename=" + p.StartInfo.FileName + ", Arguments=" + p.StartInfo.Arguments);
                    Log.WriteToLog("Info", "Added program to execute \"{0}\" argument \"{1}\".", p.StartInfo.FileName, p.StartInfo.Arguments);
                    p.Start();

                    using (StreamReader myStreamReader = p.StandardOutput)
                    {
                        string result = myStreamReader.ReadToEnd();
                        p.WaitForExit();
                        p.Close();
                        Log.WriteToLog("Info", "Command finished!");
                    }
                    
                    File.Delete(FilePath);
                }
                else if (ProgramPath != string.Empty)
                {
                    Thread executionThread = new Thread(new ParameterizedThreadStart(RunPrograms));
                    ProcessStartInfo startInfo = new ProcessStartInfo(ProgramPath);
                    startInfo.Arguments = ProgramArguments;
                    startInfo.UseShellExecute = false;
                    startInfo.RedirectStandardInput = false;
                    ExecutionInstance executionInstance = new ExecutionInstance(startInfo, FilePath);
                    executionThread.Start(executionInstance);
                    Log.WriteToLog("Info", "Added program to execute \"{0}\" argument \"{1}\".", ProgramPath, ProgramArguments);
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred. Type: {0} Message: {1}", ex.GetType().FullName, ex.Message);
            }
        }

        public static void RunPrograms(object source)
        {
            try
            {
                bool success = false;
                int retries = 100;

                ExecutionInstance executionInstance = (ExecutionInstance)source;
                FileStream fileStream = null;

                while (!success)
                {
                    try
                    {
                        fileStream = File.Open(executionInstance.FilePath, FileMode.Open, FileAccess.Read, FileShare.None);
                        success = true;
                    }
                    catch (Exception ex)
                    {
                        Log.WriteToLog("Error", "Unhandled exception occurred while waiting for \"{0}\" to become available. Type: {1} Message: {2}", executionInstance.FilePath, ex.GetType().FullName, ex.Message);
                        Thread.Sleep(1000);
                        if (--retries == 0)
                        {
                            Log.WriteToLog("Error", "Exception occurred while opening the file. File: {0} Message: {1}", executionInstance.FilePath, ex.Message);
                            break;
                        }
                    }
                }

                fileStream.Close();

                try
                {
                    Log.WriteToLog("Info", "Running program in response to event for {2}: {0}{1}.", executionInstance.StartInfo.FileName, (executionInstance.StartInfo.Arguments != "" ? " " + executionInstance.StartInfo.Arguments : ""), executionInstance.FilePath);
                    Process executionProcess = Process.Start(executionInstance.StartInfo);
                    executionProcess.WaitForExit();
                    executionProcess.Close();

                    string path = Path.GetDirectoryName(executionInstance.FilePath);
                    string[] filePaths = Directory.GetFiles(path);
                    foreach (string filePath in filePaths)
                    {

                        if (!filePath.ToUpper().Contains("ACTIVITY"))
                        {
                            File.Delete(filePath);
                            Log.WriteToLog("Info", "Delete {0}", filePath);
                        }
                        else
                        {
                            FileInfo finfo = new FileInfo(filePath);

                            if (finfo.Length == 0)
                            {
                                File.Delete(filePath);
                                Log.WriteToLog("Info", "empty file Deleted:{0}", filePath);
                            }
                        }
                        
                    }
                    string file = Path.GetFileName(executionInstance.FilePath);
                    if (file.ToLower().Contains("sancparty"))
                    {
                        string tmp = path + @"\..\..\Logs\OFAC\ImportSancParty.tmp";
                        if (File.Exists(tmp))
                        {
                            File.Delete(tmp);
                            Log.WriteToLog("Info", "Delete {0}", tmp);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.WriteToLog("Error", "Exception occurred while running {0}. Type: {1} Message: {2}", (executionInstance.StartInfo != null ? "\"" + executionInstance.StartInfo.FileName + "\"" : ""), ex.GetType().FullName, ex.Message);
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred. Type: {0} Message: {1}", ex.GetType().FullName, ex.Message);
            }
        }
    }
}
