﻿using System;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Quartz;

namespace Unisys.AML.ImportService
{
    public abstract class QuartzJobBase : IJob 
    {
        private static readonly Newtonsoft.Json.JsonSerializerSettings JsonSettings;
        static QuartzJobBase()
        {
            JsonSettings = new JsonSerializerSettings();
            JsonSettings.TypeNameHandling = TypeNameHandling.Auto;
            JsonSettings.TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                ReadFromJobDataMap(context.MergedJobDataMap);
                InternalExecute(context);
            }
            catch (Exception ex)
            {
                // Jobs should throw JobExecutionException if error occurred.
                // Wrap internal exceptions with JobExecutionException.
                JobExecutionException jex = new JobExecutionException(ex);
                throw jex;
            }
        }
        protected abstract void InternalExecute(IJobExecutionContext context);

        #region JobDataMap & Serialization

        public JobDataMap BuildJobDataMap()
        {
            JobDataMap data = new JobDataMap();

            foreach (var prop in GetType().GetProperties())
            {
                object value = prop.GetValue(this, null);
                string s = GetPropertyValue(prop);
                data.Add(prop.Name, s);
            }

            return data;
        }
        private void ReadFromJobDataMap(JobDataMap data)
        {
            PropertyInfo[] properties = GetType().GetProperties();

            foreach (var key in data.Keys)
            {
                var p = properties.Where(x => x.Name == key).SingleOrDefault();

                if (p != null)
                {
                    SetPropertyValue(p, data.GetString(key));
                }
            }
        }

        private string GetPropertyValue(PropertyInfo property)
        {
            object value = property.GetValue(this, null);
            return JsonConvert.SerializeObject(value, Formatting.None, JsonSettings);
        }
        private void SetPropertyValue(PropertyInfo property, string value)
        {
            object obj = JsonConvert.DeserializeObject(value, property.PropertyType, JsonSettings);
            property.SetValue(this, obj, null);
        }

        #endregion
    }
}
