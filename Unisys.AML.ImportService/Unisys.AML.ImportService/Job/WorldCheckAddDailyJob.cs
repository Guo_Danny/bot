﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
//using Microsoft.VisualBasic.FileIO;
using System.Data;
using System.Data.SqlClient;
using Unisys.AML.Common;
using Unisys.AML.Service;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Data.Common;

namespace Unisys.AML.ImportService
{
    [DisallowConcurrentExecution]
    public class WorldCheckAddDailyJob : IJob
    {
        private StringBuilder logs;

        public WorldCheckAddDailyJob()
        {
        }

        public string connectionString = ConfigurationManager.ConnectionStrings["OFAC"].ConnectionString;

        public virtual void Execute(IJobExecutionContext context)
        {
            try
            {
                logs = new StringBuilder();
                LogAdd(string.Format("***** Start WorldCheckAdd Daily Job (Relationship, SDNAltTable, URLs) [{0}] *****", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                JobDataMap jobDataMap = context.MergedJobDataMap;

                string currentDir = Environment.CurrentDirectory;
                DirectoryInfo directory = new DirectoryInfo(currentDir);
                string BCPFile_TempRelationship = jobDataMap.GetString("BCPFile_TempRelationship");
                string BCPFile_Relationship = jobDataMap.GetString("BCPFile_Relationship");
                string BCPFmt_TempRelationship = jobDataMap.GetString("BCPFmt_TempRelationship");
                string BCPFmt_Relationship = jobDataMap.GetString("BCPFmt_Relationship");

                string BCPFile_TempSDNAltTable = jobDataMap.GetString("BCPFile_TempSDNAltTable");
                string BCPFile_SDNAltTable = jobDataMap.GetString("BCPFile_SDNAltTable");
                string BCPFmt_TempSDNAltTable = jobDataMap.GetString("BCPFmt_TempSDNAltTable");
                string BCPFmt_SDNAltTable = jobDataMap.GetString("BCPFmt_SDNAltTable");

                string BCPFile_TempURLs = jobDataMap.GetString("BCPFile_TempURLs");
                string BCPFile_URLs = jobDataMap.GetString("BCPFile_URLs");
                string BCPFmt_TempURLs = jobDataMap.GetString("BCPFmt_TempURLs");
                string BCPFmt_URLs = jobDataMap.GetString("BCPFmt_URLs");

                string BCPFile_Error = jobDataMap.GetString("BCPFile_Error");
                string BCPServer = jobDataMap.GetString("BCPServer");
                string BCPUser = jobDataMap.GetString("BCPUser");
                string BCPPwd = jobDataMap.GetString("BCPPwd");
                string BaseNum = jobDataMap.GetString("BaseNum");


                UpdRelationshipDailyDataIntoSqlServer(BCPFile_TempRelationship, BCPFile_Relationship, BCPFmt_TempRelationship, BCPFile_Error,
                    BCPFmt_Relationship, BCPServer, BCPUser, BCPPwd, BaseNum);

                UpdSDNAltTableDailyDataIntoSqlServer(BCPFile_TempSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TempSDNAltTable, BCPFile_Error,
                    BCPFmt_SDNAltTable, BCPServer, BCPUser, BCPPwd);

                UpdURLsDailyDataIntoSqlServer(BCPFile_TempURLs, BCPFile_URLs, BCPFmt_TempURLs, BCPFile_Error, BCPFmt_URLs, BCPServer, BCPUser, BCPPwd);

                LogAdd(string.Format("***** Complete WorldCheckAdd Daily Job (Relationship, SDNAltTable, URLs) [{0}] *****", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                string b = BuildFileImg();

                LogAdd(b);
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred. Type: {0} Message: {1}", ex.GetType().FullName, ex.Message);
            }
        }

        public void UpdRelationshipDailyDataIntoSqlServer(string BCPFile_TempRelationship, string BCPFile_Relationship, string BCPFmt_TempRelationship,
            string BCPFile_Error, string BCPFmt_Relationship, string BCPServer, string BCPUser, string BCPPwd, string BaseNum)
        {
            try
            {
                LogAdd("[Daily] Expand to Relationship -- Start");
                string query = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;
                string queryLog1 = string.Empty;
                string queryLog2 = string.Empty;
                string LogContent1 = string.Empty;
                string LogContent2 = string.Empty;
                string LinkedTo = string.Empty;
                string RelationShipId = string.Empty;
                string EntNum = string.Empty;
                string RelatedID = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");

                int RelationShipIdMax = 0;
                int File_TotalNum = 0;
                int File_UpdDelNum = 0;
                int File_UpdAddNum = 0;
                int File_FinalAddNum = 0;
                int finalLinkedToSplit = 0;
                int rowCount = 0;

                DateTime NowDateTime = DateTime.Now;

                if (File.Exists(BCPFile_TempRelationship)) File.Delete(BCPFile_TempRelationship);

                StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TempRelationship, false, Encoding.GetEncoding(28591));

                if (File.Exists(BCPFile_Relationship)) File.Delete(BCPFile_Relationship);

                StreamWriter fileDWriter = new StreamWriter(BCPFile_Relationship, false, Encoding.GetEncoding(28591));

                queryMax = string.Format(@" SELECT MAX(RelationshipId)+1 AS RelationshipIdMax FROM OFAC.dbo.Relationship ");

                //query = string.Format(@" SELECT Entnum,LinkedTo FROM OFAC.dbo.WorldCheck WHERE LinkedTo IS NOT NULL AND Updated > DATEADD(Daily,-1,GETDATE()) ");

                query = string.Format(@" SELECT WC.Entnum, WC.LinkedTo, 
 CASE ISNULL(WC.DelFlag, '0') 
 WHEN '0' THEN '1' 
 WHEN '1' THEN '4' END AS Status 
 FROM OFAC.dbo.WorldCheck WC
 JOIN WorldCheckCountry WCC ON WC.Country = WCC.WorldCheck_Country
 WHERE WCC.Status = 1
 AND WC.SubCategory IS NOT NULL
 AND WC.LinkedTo != '' 
 AND WC.UPDDate > DATEADD(DAY, -1, GETDATE()) ");
                Database db;
                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 0;
                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("RelationshipIdMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            RelationShipIdMax = Convert.ToInt32(dataReaderMax["RelationshipIdMax"]);
                        }
                    }
                }

                IDataReader dataReaderDT = db.ExecuteReader(dbCommand);
                DataTable dt = new DataTable();
                dt.Load(dataReaderDT);
                rowCount = dt.Rows.Count;
                dataReaderDT.Close();
                int intBaseNum = Convert.ToInt32(BaseNum);
                using (IDataReader dataReader = db.ExecuteReader(dbCommand))
                {
                    while (dataReader.Read())
                    {
                        EntNum = dataReader["Entnum"].ToString().Trim();
                        LinkedTo = dataReader["LinkedTo"].ToString().Trim();
                        Status = dataReader["Status"].ToString().Trim();
                        string[] LinkedToSplit = dataReader["LinkedTo"].ToString().Trim().Split(';');
                        int i;
                        for (i = 0; i < LinkedToSplit.Length; checked(i)++)
                        {
                            if (LinkedToSplit[i].Trim() != "")
                            {

                                finalLinkedToSplit = Convert.ToInt32(LinkedToSplit[i].Trim()) + intBaseNum;
                                StringBuilder builder = new StringBuilder();
                                builder.AppendFormat("{0}\t", RelationShipIdMax.ToString());
                                builder.AppendFormat("{0}\t", EntNum);
                                builder.AppendFormat("{0}\t", finalLinkedToSplit.ToString());
                                builder.AppendFormat("{0}", Status);
                                TempfileDWriter.WriteLine(builder.ToString());
                                RelationShipIdMax++;
                                File_TotalNum++;
                            }
                        }
                    }
                    dataReader.Close();
                }

                TempfileDWriter.Close();

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TempRelationship ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                if (rowCount != 0)
                {
                    Process p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.TempRelationship in " + BCPFile_TempRelationship + "  -f " + BCPFmt_TempRelationship + " -S " + BCPServer + " -U " + BCPUser + " -P " + BCPPwd + " -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();

                    string queryUpdateDelDetail =
                        string.Format(@" SELECT * FROM OFAC.dbo.Relationship
                                     WHERE EXISTS (SELECT * FROM OFAC.dbo.TempRelationship WHERE Relationship.EntNum = TempRelationship.EntNum AND Relationship.RelatedID = TempRelationship.RelatedID AND TempRelationship.Status='4')
 AND Relationship.RelationshipPToR='WPEP' ");

                    DbCommand dbCommandUpdateDelDetial = db.GetSqlStringCommand(queryUpdateDelDetail);
                    dbCommandUpdateDelDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateDelDetail = db.ExecuteReader(dbCommandUpdateDelDetial))
                    {
                        while (drUpdateDelDetail.Read())
                        {
                            //if (File_UpdDelNum == 0) { LogAdd("[Daily] Relationship - Set Status to 4: "); };
                            //LogAdd(
                            //    drUpdateDelDetail["RelationshipId"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["EntNum"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["RelatedID"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["RelationshipPToR"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["RelationshipRToP"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListCreateDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateOper"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastOper"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastModify"].ToString().Trim());
                            File_UpdDelNum++;
                        }
                    }

                    string queryUpdateDel =
                        string.Format(@"  UPDATE OFAC.dbo.Relationship
 SET Status='4', LastModify=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempRelationship WHERE Relationship.EntNum = TempRelationship.EntNum AND Relationship.RelatedID = TempRelationship.RelatedID AND TempRelationship.Status='4')
 AND Relationship.RelationshipPToR='WPEP' ");

                    DbCommand dbCommandUpdateDel = db.GetSqlStringCommand(queryUpdateDel);
                    dbCommandUpdateDel.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateDel);

                    string queryUpdateAddDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.Relationship
                                 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempRelationship WHERE Relationship.EntNum = TempRelationship.EntNum AND Relationship.RelatedID = TempRelationship.RelatedID AND TempRelationship.Status='1')
 AND Relationship.RelationshipPToR='WPEP' ");

                    DbCommand dbCommandUpdateAddDetial = db.GetSqlStringCommand(queryUpdateAddDetail);
                    dbCommandUpdateAddDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateAddDetail = db.ExecuteReader(dbCommandUpdateAddDetial))
                    {
                        while (drUpdateAddDetail.Read())
                        {
                            //if (File_UpdAddNum == 0) { LogAdd("[Daily] Relationship - Set Status to 1: "); };
                            //LogAdd(
                            //    drUpdateAddDetail["RelationshipId"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["EntNum"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["RelatedID"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["RelationshipPToR"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["RelationshipRToP"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListCreateDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateOper"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastOper"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastModify"].ToString().Trim());
                            File_UpdAddNum++;
                        }
                    }

                    string queryUpdateAdd =
                        string.Format(@"  UPDATE OFAC.dbo.Relationship
 SET Status='1', LastModify=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempRelationship WHERE Relationship.EntNum = TempRelationship.EntNum AND Relationship.RelatedID = TempRelationship.RelatedID AND TempRelationship.Status='1')
 AND Relationship.RelationshipPToR='WPEP' ");

                    DbCommand dbCommandUpdateAdd = db.GetSqlStringCommand(queryUpdateAdd);
                    dbCommandUpdateAdd.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateAdd);

                    string queryAdd =
                        string.Format(@" SELECT DISTINCT RelationshipId, EntNum, RelatedID, Status FROM OFAC.dbo.TempRelationship
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.Relationship WHERE TempRelationship.EntNum = EntNum AND TempRelationship.RelatedID = RelatedID)
                                     ORDER BY RelationshipId, EntNum, RelatedID ");

                    DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                    dbCommandAdd.CommandTimeout = 0;
                    using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                    {
                        while (Tempdr.Read())
                        {
                            RelationShipId = Tempdr["RelationShipId"].ToString().Trim();
                            EntNum = Tempdr["EntNum"].ToString().Trim();
                            RelatedID = Tempdr["RelatedID"].ToString().Trim();
                            Status = Tempdr["Status"].ToString().Trim();
                            StringBuilder builder = new StringBuilder();
                            builder.AppendFormat("{0}\t", RelationShipId);
                            builder.AppendFormat("{0}\t", EntNum);
                            builder.AppendFormat("{0}\t", RelatedID);
                            builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}", "WPEP", "WPEP", Status, NowDateTime.ToString(), NowDateTime.ToString(), "Prime", NowDateTime.ToString(), "Prime", NowDateTime.ToString());
                            fileDWriter.WriteLine(builder.ToString());
                            File_FinalAddNum++;
                        }
                        Tempdr.Close();
                    }

                    fileDWriter.Close();

                    p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.RelationShip in " + BCPFile_Relationship + "  -f " + BCPFmt_Relationship + " -S " + BCPServer + " -U " + BCPUser + " -P " + BCPPwd + " -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();
                }

                LogAdd("[Daily] TempRelationship - Import: [" + File_TotalNum + "]; Relationship - Set Status to 4: [" + File_UpdDelNum + "], Set Status to 1: [" + File_UpdAddNum + "], Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[Daily] Expand to Relationship -- Completion");

                OFACEventLog("Relationship total update : " + (File_UpdDelNum + File_UpdAddNum).ToString());
                OFACEventLog("Relationship total insert : " + File_FinalAddNum.ToString());
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred. Type: {0} Message: {1}", ex.GetType().FullName, ex.Message);
            }
        }

        public void UpdSDNAltTableDailyDataIntoSqlServer(string BCPFile_TempSDNAltTable, string BCPFile_SDNAltTable, string BCPFmt_TempSDNAltTable,
            string BCPFile_Error, string BCPFmt_SDNAltTable, string BCPServer, string BCPUser, string BCPPwd)
        {
            try
            {
                LogAdd("[Daily] Expand to SDNAltTable -- Start");

                string query = string.Empty;
                string queryLog1 = string.Empty;
                string queryLog2 = string.Empty;
                string LogContent1 = string.Empty;
                string LogContent2 = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;
                string Aliases = string.Empty;
                string AltNum = string.Empty;
                string EntNum = string.Empty;
                string AltType = string.Empty;
                string AltName = string.Empty;
                string UID = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");

                int AltNumMax = 0;
                int File_TotalNum = 0;
                int File_UpdDelNum = 0;
                int File_UpdAddNum = 0;
                int File_FinalAddNum = 0;
                int rowCount = 0;

                DateTime NowDateTime = DateTime.Now;

                Database db;

                if (File.Exists(BCPFile_TempSDNAltTable)) File.Delete(BCPFile_TempSDNAltTable);

                StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TempSDNAltTable, false, Encoding.GetEncoding(28591));

                if (File.Exists(BCPFile_SDNAltTable)) File.Delete(BCPFile_SDNAltTable);

                StreamWriter fileDWriter = new StreamWriter(BCPFile_SDNAltTable, false, Encoding.GetEncoding(28591));

                queryMax = string.Format(@" SELECT MAX(AltNum)+1 AS AltNumMax FROM OFAC.dbo.SDNAltTable ");

                //query = string.Format(@" SELECT Entnum, Aliases FROM OFAC.dbo.WorldCheck WHERE Aliases IS NOT NULL AND Updated > DATEADD(Daily,-1,GETDATE()) ");

                query = string.Format(@" SELECT WC.Entnum, WC.Aliases, 
 CASE ISNULL(WC.DelFlag, '0') 
 WHEN '0' THEN '1' 
 WHEN '1' THEN '4' END AS Status 
 FROM OFAC.dbo.WorldCheck WC
 JOIN WorldCheckCountry WCC ON WC.Country = WCC.WorldCheck_Country
 WHERE WCC.Status = 1
 AND WC.SubCategory IS NOT NULL
 AND WC.Aliases != '' 
 AND WC.UPDDate > DATEADD(DAY, -1, GETDATE()) ");

                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 0;
                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("AltNumMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            AltNumMax = Convert.ToInt32(dataReaderMax["AltNumMax"]);
                        }
                    }
                }
                IDataReader dataReaderDT = db.ExecuteReader(dbCommand);
                DataTable dt = new DataTable();
                dt.Load(dataReaderDT);
                rowCount = dt.Rows.Count;
                dataReaderDT.Close();
                using (IDataReader dr = db.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        EntNum = dr["Entnum"].ToString().Trim();
                        Status = dr["Status"].ToString().Trim();
                        string[] AliasesSplit = dr["Aliases"].ToString().Trim().Split(';');
                        int i;
                        for (i = 0; i < AliasesSplit.Length; i++)
                        {
                            if (AliasesSplit[i] != "")
                            {
                                StringBuilder builder = new StringBuilder();
                                builder.AppendFormat("{0}\t", EntNum);
                                builder.AppendFormat("{0}\t", AltNumMax.ToString());
                                builder.AppendFormat("{0}\t", AliasesSplit[i]);
                                builder.AppendFormat("{0}", Status);
                                TempfileDWriter.WriteLine(builder.ToString());
                                AltNumMax++;
                                File_TotalNum++;
                            }
                        }
                    }
                    dr.Close();
                }
                TempfileDWriter.Close();

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TempSDNAltTable ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                if (rowCount != 0)
                {
                    Process p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.TempSDNAltTable in " + BCPFile_TempSDNAltTable + "  -f " + BCPFmt_TempSDNAltTable + " -S " + BCPServer + " -U " + BCPUser + " -P " + BCPPwd + " -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();

                    string queryUpdateDelDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.SDNAltTable
                                 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempSDNAltTable WHERE SDNAltTable.EntNum = TempSDNAltTable.EntNum AND SDNAltTable.AltName = TempSDNAltTable.AltName AND TempSDNAltTable.Status='4')
 AND SDNAltTable.AltType='WPEP' ");

                    DbCommand dbCommandUpdateDelDetial = db.GetSqlStringCommand(queryUpdateDelDetail);
                    dbCommandUpdateDelDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateDelDetail = db.ExecuteReader(dbCommandUpdateDelDetial))
                    {
                        while (drUpdateDelDetail.Read())
                        {
                            //if (File_UpdDelNum == 0) { LogAdd("[Daily] SDNAltTable - Set Status to 4: "); };
                            //LogAdd(
                            //    drUpdateDelDetail["EntNum"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["AltNum"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["AltType"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["AltName"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["FirstName"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["MiddleName"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastName"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListID"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["Remarks"].ToString().Trim() + ", " +
                            //    drUpdateDelDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListCreateDate"].ToString().Trim() + ", " +
                            //    drUpdateDelDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastModifDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastOper"].ToString().Trim());
                            File_UpdDelNum++;
                        }
                    }

                    string queryUpdateDel =
                        string.Format(@"  UPDATE OFAC.dbo.SDNAltTable
 SET Status='4', LastModifDate=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempSDNAltTable WHERE SDNAltTable.EntNum = TempSDNAltTable.EntNum AND SDNAltTable.AltName = TempSDNAltTable.AltName AND TempSDNAltTable.Status='4')
 AND SDNAltTable.AltType='WPEP' ");

                    DbCommand dbCommandUpdateDel = db.GetSqlStringCommand(queryUpdateDel);
                    dbCommandUpdateDel.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateDel);

                    string queryUpdateAddDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.SDNAltTable
                                 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempSDNAltTable WHERE SDNAltTable.EntNum = TempSDNAltTable.EntNum AND SDNAltTable.AltName = TempSDNAltTable.AltName AND TempSDNAltTable.Status='1')
 AND SDNAltTable.AltType='WPEP' ");

                    DbCommand dbCommandUpdateAddDetial = db.GetSqlStringCommand(queryUpdateAddDetail);
                    dbCommandUpdateAddDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateAddDetail = db.ExecuteReader(dbCommandUpdateAddDetial))
                    {
                        while (drUpdateAddDetail.Read())
                        {
                            //if (File_UpdAddNum == 0) { LogAdd("[Daily] SDNAltTable - Set Status to 1: "); };
                            //LogAdd(
                            //    drUpdateAddDetail["EntNum"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["AltNum"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["AltType"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["AltName"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["FirstName"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["MiddleName"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastName"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListID"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["Remarks"].ToString().Trim() + ", " +
                            //    drUpdateAddDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListCreateDate"].ToString().Trim() + ", " +
                            //    drUpdateAddDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastModifDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastOper"].ToString().Trim());
                            File_UpdAddNum++;
                        }
                    }

                    string queryUpdateAdd =
                        string.Format(@" UPDATE OFAC.dbo.SDNAltTable
 SET Status='1', LastModifDate=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempSDNAltTable WHERE SDNAltTable.EntNum = TempSDNAltTable.EntNum AND SDNAltTable.AltName = TempSDNAltTable.AltName AND TempSDNAltTable.Status='1')
 AND SDNAltTable.AltType='WPEP' ");

                    DbCommand dbCommandUpdateAdd = db.GetSqlStringCommand(queryUpdateAdd);
                    dbCommandUpdateAdd.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateAdd);

                    string queryAdd =
                        string.Format(@" SELECT DISTINCT EntNum, AltNum, AltName, Status FROM OFAC.dbo.TempSDNAltTable 
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.SDNAltTable WHERE TempSDNAltTable.EntNum = EntNum AND TempSDNAltTable.AltName = AltName) 
                                     ORDER BY EntNum, AltNum, AltName ");

                    DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                    dbCommandAdd.CommandTimeout = 0;
                    using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                    {
                        while (Tempdr.Read())
                        {
                            EntNum = Tempdr["EntNum"].ToString().Trim();
                            AltNum = Tempdr["AltNum"].ToString().Trim();
                            AltName = Tempdr["AltName"].ToString().Trim();
                            Status = Tempdr["Status"].ToString().Trim();
                            StringBuilder builder = new StringBuilder();
                            builder.AppendFormat("{0}\t", EntNum);
                            builder.AppendFormat("{0}\t", AltNum);
                            builder.AppendFormat("{0}\t", "WPEP");
                            builder.AppendFormat("{0}\t", AltName);
                            builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}", "", "", "", "", "", "0", Status, NowDateTime.ToString(), NowDateTime.ToString(), NowDateTime.ToString(), NowDateTime.ToString(), "Prime");
                            fileDWriter.WriteLine(builder.ToString());
                            File_FinalAddNum++;
                        }
                        Tempdr.Close();
                    }

                    fileDWriter.Close();

                    p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.SDNAltTable in " + BCPFile_SDNAltTable + "  -f " + BCPFmt_SDNAltTable + " -S " + BCPServer + " -U " + BCPUser + " -P " + BCPPwd + " -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    if (File.Exists(BCPFile_TempSDNAltTable)) File.Delete(BCPFile_TempSDNAltTable);
                    p.Start();
                    p.WaitForExit();
                }

                LogAdd("[Daily] TempSDNAltTable - Import: [" + File_TotalNum + "]; SDNAltTable - Set Status to 4: [" + File_UpdDelNum + "], Set Status to 1: [" + File_UpdAddNum + "], Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[Daily] Expand to SDNAltTable -- Completion");

                OFACEventLog("SDNAltTable total update : " + (File_UpdDelNum + File_UpdAddNum).ToString());
                OFACEventLog("SDNAltTable total insert : " + File_FinalAddNum.ToString());
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred. Type: {0} Message: {1}", ex.GetType().FullName, ex.Message);
            }
        }

        public void UpdURLsDailyDataIntoSqlServer(string BCPFile_TempURLs, string BCPFile_URLs, string BCPFmt_TempURLs, string BCPFile_Error,
            string BCPFmt_URLs, string BCPServer, string BCPUser, string BCPPwd)
        {
            try
            {
                LogAdd("[Daily] Expand to URLs -- Start");

                string query = string.Empty;
                string queryLog1 = string.Empty;
                string queryLog2 = string.Empty;
                string LogContent1 = string.Empty;
                string LogContent2 = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;
                string Aliases = string.Empty;
                string URLsID = string.Empty;
                string EntNum = string.Empty;
                string AltType = string.Empty;
                string URL = string.Empty;
                string Description = string.Empty;
                string UID = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");

                int URLsIDMax = 0;
                int File_TotalNum = 0;
                int File_UpdDelNum = 0;
                int File_UpdAddNum = 0;
                int File_FinalAddNum = 0;
                int rowCount = 0;

                DateTime NowDateTime = DateTime.Now;

                Database db;

                if (File.Exists(BCPFile_TempURLs)) File.Delete(BCPFile_TempURLs);

                StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TempURLs, false, Encoding.GetEncoding(28591));

                if (File.Exists(BCPFile_URLs)) File.Delete(BCPFile_URLs);

                StreamWriter fileDWriter = new StreamWriter(BCPFile_URLs, false, Encoding.GetEncoding(28591));

                queryMax = string.Format(@" SELECT MAX(URLsID)+1 AS URLsIDMax FROM OFAC.dbo.URLs ");

                //query = string.Format(@" SELECT Entnum, ExSources FROM OFAC.dbo.WorldCheck WHERE ExSources IS NOT NULL AND Updated > DATEADD(Daily,-1,GETDATE()) ");

                query = string.Format(@" SELECT WC.Entnum, WC.ExSources, 
 CASE ISNULL(WC.DelFlag, '0') 
 WHEN '0' THEN '1' 
 WHEN '1' THEN '4' END AS Status 
 FROM OFAC.dbo.WorldCheck WC
 JOIN WorldCheckCountry WCC ON WC.Country = WCC.WorldCheck_Country
 WHERE WCC.Status = 1
 AND WC.SubCategory IS NOT NULL
 AND WC.ExSources != '' 
 AND WC.UPDDate > DATEADD(DAY, -1, GETDATE()) ");

                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 0;
                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("URLsIDMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            URLsIDMax = Convert.ToInt32(dataReaderMax["URLsIDMax"]);
                        }
                    }
                }
                IDataReader dataReaderDT = db.ExecuteReader(dbCommand);
                DataTable dt = new DataTable();
                dt.Load(dataReaderDT);
                rowCount = dt.Rows.Count;
                dataReaderDT.Close();
                using (IDataReader dr = db.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        EntNum = dr["Entnum"].ToString().Trim();
                        Status = dr["Status"].ToString().Trim();
                        string[] ExSourcesSplit = dr["ExSources"].ToString().Trim().Split(' ');
                        int i;
                        for (i = 0; i < ExSourcesSplit.Length; i++)
                        {
                            if (ExSourcesSplit[i] != "")
                            {
                                StringBuilder builder = new StringBuilder();
                                builder.AppendFormat("{0}\t", URLsIDMax.ToString());
                                builder.AppendFormat("{0}\t", EntNum);
                                builder.AppendFormat("{0}\t", ExSourcesSplit[i]);
                                builder.AppendFormat("{0}", Status);
                                TempfileDWriter.WriteLine(builder.ToString());
                                URLsIDMax++;
                                File_TotalNum++;
                            }
                        }
                    }
                    dr.Close();
                }
                TempfileDWriter.Close();

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TempURLs ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                if (rowCount != 0)
                {
                    Process p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.TempURLs in " + BCPFile_TempURLs + "  -f " + BCPFmt_TempURLs + " -S " + BCPServer + " -U " + BCPUser + " -P " + BCPPwd + " -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();

                    string queryUpdateDelDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.URLs
                                 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempURLs WHERE URLs.EntNum = TempURLs.EntNum AND URLs.URL = TempURLs.URL AND TempURLs.Status='4')
");

                    DbCommand dbCommandUpdateDelDetial = db.GetSqlStringCommand(queryUpdateDelDetail);
                    dbCommandUpdateDelDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateDelDetail = db.ExecuteReader(dbCommandUpdateDelDetial))
                    {
                        while (drUpdateDelDetail.Read())
                        {
                            //if (File_UpdDelNum == 0) { LogAdd("[Daily] URLs - Set Status to 4: "); };
                            //LogAdd(
                            //    drUpdateDelDetail["URLsID"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["EntNUm"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["URL"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["Description"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListCreateDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateOper"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateDate"].ToString().Trim() + ", " +
                            //    drUpdateDelDetail["LastOper"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastModify"].ToString().Trim());
                            File_UpdDelNum++;
                        }
                    }

                    string queryUpdateDel =
                        string.Format(@" UPDATE OFAC.dbo.URLs
 SET Status='4', LastModify=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempURLs WHERE URLs.EntNum = TempURLs.EntNum AND URLs.URL = TempURLs.URL AND TempURLs.Status='4')
");

                    DbCommand dbCommandUpdateDel = db.GetSqlStringCommand(queryUpdateDel);
                    dbCommandUpdateDel.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateDel);

                    string queryUpdateAddDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.URLs
                                  WHERE EXISTS (SELECT * FROM OFAC.dbo.TempURLs WHERE URLs.EntNum = TempURLs.EntNum AND URLs.URL = TempURLs.URL AND TempURLs.Status='1') ");

                    DbCommand dbCommandUpdateAddDetial = db.GetSqlStringCommand(queryUpdateAddDetail);
                    dbCommandUpdateAddDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateAddDetail = db.ExecuteReader(dbCommandUpdateAddDetial))
                    {
                        while (drUpdateAddDetail.Read())
                        {
                            //if (File_UpdAddNum == 0) { LogAdd("[Daily] URLs - Set Status to 1: "); };
                            //LogAdd(
                            //    drUpdateAddDetail["URLsID"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["EntNUm"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["URL"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["Description"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListCreateDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateOper"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateDate"].ToString().Trim() + ", " +
                            //    drUpdateAddDetail["LastOper"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastModify"].ToString().Trim());
                            File_UpdAddNum++;
                        }
                    }

                    string queryUpdateAdd =
                        string.Format(@" UPDATE OFAC.dbo.URLs
 SET Status='1', LastModify=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempURLs WHERE URLs.EntNum = TempURLs.EntNum AND URLs.URL = TempURLs.URL AND TempURLs.Status='1')
 ");

                    DbCommand dbCommandUpdateAdd = db.GetSqlStringCommand(queryUpdateAdd);
                    dbCommandUpdateAdd.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateAdd);

                    string queryAdd =
                        string.Format(@" SELECT DISTINCT URLsID, EntNum, URL, Status FROM OFAC.dbo.TempURLs 
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.URLs WHERE TempURLs.EntNum = EntNum AND TempURLs.URL = URL) 
                                     ORDER BY URLsID, EntNum, URL ");

                    DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                    dbCommandAdd.CommandTimeout = 0;
                    using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                    {
                        while (Tempdr.Read())
                        {
                            URLsID = Tempdr["URLsID"].ToString().Trim();
                            EntNum = Tempdr["EntNum"].ToString().Trim();
                            URL = Tempdr["URL"].ToString().Trim();
                            Description = Tempdr["URL"].ToString().Trim();
                            Status = Tempdr["Status"].ToString().Trim();
                            StringBuilder builder = new StringBuilder();
                            builder.AppendFormat("{0}\t", URLsID);
                            builder.AppendFormat("{0}\t", EntNum);
                            builder.AppendFormat("{0}\t", URL);
                            builder.AppendFormat("{0}\t", Description);
                            builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}", Status, NowDateTime.ToString(), NowDateTime.ToString(), "Prime", NowDateTime.ToString(), "Prime", NowDateTime.ToString());
                            fileDWriter.WriteLine(builder.ToString());
                            File_FinalAddNum++;
                        }
                        Tempdr.Close();
                    }

                    fileDWriter.Close();

                    p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.URLs in " + BCPFile_URLs + "  -f " + BCPFmt_URLs + " -S " + BCPServer + " -U " + BCPUser + " -P " + BCPPwd + " -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();
                }
                LogAdd("[Daily] TempURLs - Import: [" + File_TotalNum + "]; URLs - Set Status to 4: [" + File_UpdDelNum + "], Set Status to 1: [" + File_UpdAddNum + "], Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[Daily] Expand to URLs -- Completion");

                OFACEventLog("URLs total update : " + (File_UpdDelNum + File_UpdAddNum).ToString());
                OFACEventLog("URLs total insert : " + File_FinalAddNum.ToString());
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred. Type: {0} Message: {1}", ex.GetType().FullName, ex.Message);
            }
        }

        /// <summary>
        /// 寫入OFAC Event
        /// </summary>
        public void OFACEventLog(string log)
        {
            string sql = @"
insert into SDNChangeLog (logdate, oper, logtext, type, objecttype, objectid) 
values 
(getdate(), 'Prime', @Log, 'Ann', 'Event', 'WORLDCHECK')
";

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                using (var cmd = new SqlCommand(sql, sqlConnection))
                {
                    cmd.Parameters.Add(new SqlParameter("@Log", log.Replace(@"\n", Environment.NewLine)));
                    cmd.ExecuteNonQuery();
                }

                sqlConnection.Close();
            }
        }

        private void LogAdd(string log)
        {
            logs.AppendLine(log);
            Log.WriteToLog("Info", log);
        }

        private string BuildFileImg()
        {
            StringBuilder s = new StringBuilder();
            Process p = new Process();
            ProcessStartInfo pInfo = new ProcessStartInfo("D:\\Prime\\Exe\\BuildFileImg.exe");

            s.AppendLine(string.Format("{0} | ========== Start Build File Image ==========",
                                       DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")))
             .AppendLine();

            pInfo.UseShellExecute = false;
            pInfo.RedirectStandardOutput = true;
            pInfo.CreateNoWindow = true;

            p.StartInfo = pInfo;
            p.Start();

            using (StreamReader myStreamReader = p.StandardOutput)
            {
                string result = myStreamReader.ReadToEnd();
                p.WaitForExit();
                p.Close();

                s.AppendLine(result);
            }

            s.AppendLine(string.Format("{0} | ========== End Build File Image ==========",
                                        DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));
            return s.ToString();
        }
    }
}
