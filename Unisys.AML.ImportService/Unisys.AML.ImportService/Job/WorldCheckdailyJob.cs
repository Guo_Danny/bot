﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic.FileIO;
using System.Data;
using System.Data.SqlClient;
using Unisys.AML.Common;
using Unisys.AML.Service;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Data.Common;

namespace Unisys.AML.ImportService
{
    [DisallowConcurrentExecution]
    public class WorldCheckdailyJob : IJob
    {
        private StringBuilder logs;

        public WorldCheckdailyJob()
        {
        }

        public string connectionString = ConfigurationManager.ConnectionStrings["OFAC"].ConnectionString;
        public string str_wc_imp_order = "";

        List<GetWCPara> getWC_para = new List<GetWCPara>();
        protected const int _batchSize = 50000;

        public virtual void Execute(IJobExecutionContext context)
        {
            try
            {
                logs = new StringBuilder();
                LogAdd(string.Format("***** Start WorldCheck Job [{0}] *****", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                JobDataMap jobDataMap = context.MergedJobDataMap;

                //wc
                string str_day_path = jobDataMap.GetString("world-check-day");
                string str_daydel_path = jobDataMap.GetString("world-check-deleted-day");
                string str_wc_entnum = jobDataMap.GetString("BaseNum");
                string str_wc_listtype = jobDataMap.GetString("wc_listtype");
                string str_UPDDate = jobDataMap.GetString("wc_UPDDate");
                string str_buildtime = jobDataMap.GetString("buildtime");
                string str_wc_excludecountry = jobDataMap.GetString("wc_excludecountry");
                string str_wc_yearlimit = jobDataMap.GetString("wc_yearlimit");
                string str_wc_fixedcountry = jobDataMap.GetString("wc_fixedcountry");
                string str_wc_divisionimg = jobDataMap.GetString("wc_divisionimg");
                string str_wc_subcate_space = jobDataMap.GetString("wc_subcate_space");
                string str_wc_import_un = jobDataMap.GetString("wc_import_UN");
                string str_wc_import_SQL = jobDataMap.GetString("wc_import_SQL");
                string str_servicedelay = jobDataMap.GetString("OFACservicerestartDelay");
                string str_servicerestart = jobDataMap.GetString("OFACservicerestart");
                string str_updateDB = jobDataMap.GetString("UpdateDBStatus");
                string str_wc_import_crime = jobDataMap.GetString("wc_import_CRIME");
                string str_wc_import_crime_SQL = jobDataMap.GetString("wc_import_CRIME_SQL");

                int int_noofsql = int.Parse(jobDataMap.GetString("No_Of_SQL"));

                str_wc_imp_order = jobDataMap.GetString("wc_import_order");

                getWC_para.Clear();

                //20181022 先給10組 不夠用就要再改程式
                for (int i = 0; i < int_noofsql; i++)
                {
                    GetWCPara WCparaSet = new GetWCPara();

                    WCparaSet.WCSQL = jobDataMap.GetString("WCSQL" + (i + 1));
                    WCparaSet.WCListtype = jobDataMap.GetString("WCListtype" + (i + 1));
                    WCparaSet.WCProgram = jobDataMap.GetString("WCprogram" + (i + 1));

                    getWC_para.Add(WCparaSet);
                    
                    LogAdd("WCSQL:" + getWC_para[i].WCSQL);
                    LogAdd("WCListtype:" + getWC_para[i].WCListtype);
                    LogAdd("WCprogram:" + getWC_para[i].WCProgram);
                    LogAdd("WCPara_setCount:"+getWC_para.Count().ToString());
                }

                //relation
                string currentDir = Environment.CurrentDirectory;
                DirectoryInfo directory = new DirectoryInfo(currentDir);
                string BCPFile_TempRelationship = jobDataMap.GetString("BCPFile_TempRelationship");
                string BCPFile_Relationship = jobDataMap.GetString("BCPFile_Relationship");
                string BCPFmt_TempRelationship = jobDataMap.GetString("BCPFmt_TempRelationship");
                string BCPFmt_Relationship = jobDataMap.GetString("BCPFmt_Relationship");

                string BCP_SDNAltTable_Enable = jobDataMap.GetString("BCPEnb_SDNAltTable");
                string BCPFile_TmpSDNAltTable = jobDataMap.GetString("BCPFile_TmpSDNAltTable");
                string BCPFile_SDNAltTable = jobDataMap.GetString("BCPFile_SDNAltTable");
                string BCPFmt_TmpSDNAltTable = jobDataMap.GetString("BCPFmt_TmpSDNAltTable");
                string BCPFmt_SDNAltTable = jobDataMap.GetString("BCPFmt_SDNAltTable");

                string BCPFile_TempURLs = jobDataMap.GetString("BCPFile_TempURLs");
                string BCPFile_URLs = jobDataMap.GetString("BCPFile_URLs");
                string BCPFmt_TempURLs = jobDataMap.GetString("BCPFmt_TempURLs");
                string BCPFmt_URLs = jobDataMap.GetString("BCPFmt_URLs");

                string BCPFile_Error = jobDataMap.GetString("BCPFile_Error");
                string BCPServer = jobDataMap.GetString("BCPServer");
                string BCPUser = jobDataMap.GetString("BCPUser");
                string BCPPwd = jobDataMap.GetString("BCPPwd");
                string BaseNum = jobDataMap.GetString("BaseNum");

                UpdCsvDataIntoSqlServer(str_day_path, str_wc_entnum);

                DelCsvDataIntoSqlServer(str_daydel_path);

                UpdateKeyWord(str_UPDDate, int.Parse(str_wc_entnum));

                UpdateSDN(str_wc_listtype, str_UPDDate, str_wc_yearlimit, str_wc_excludecountry, str_wc_entnum, str_wc_fixedcountry, str_wc_divisionimg, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);

                UpdRelationshipDailyDataIntoSqlServer(BCPFile_TempRelationship, BCPFile_Relationship, BCPFmt_TempRelationship, BCPFile_Error,
                    BCPFmt_Relationship, BCPServer, BCPUser, BCPPwd, BaseNum, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);

                if (BCP_SDNAltTable_Enable == "Y")
                {
                    UpdSDNAltTableDailyDataIntoSqlServer(BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                        BCPFmt_SDNAltTable, BCPServer, BCPUser, BCPPwd, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);
                }

                UpdURLsDailyDataIntoSqlServer(BCPFile_TempURLs, BCPFile_URLs, BCPFmt_TempURLs, BCPFile_Error, BCPFmt_URLs, BCPServer, BCPUser, BCPPwd, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);

                insertDobs(str_UPDDate, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);

                InsertAddr(str_UPDDate, str_wc_import_crime);

                UpdateAttribute(str_UPDDate, str_wc_import_crime);

                LogAdd(string.Format("***** Complete WorldCheck Job [{0}] *****", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                //2018.05 update DB status
                if (str_updateDB == "Y")
                {
                    //set MemImgBuildReq = 0
                    UpdDBStatus();
                }

                //build img 一天build一次
                LogAdd("hour=" + DateTime.Now.Hour + ",cong=" + int.Parse(str_buildtime));
                if (DateTime.Now.Hour == int.Parse(str_buildtime))
                {
                    string b = "";

                    b = BuildFileImg();
                    LogAdd(b);

                    //restart the OFAC service
                    if (str_servicerestart == "Y")
                    {
                        StopService();
                        System.Threading.Thread.Sleep(int.Parse(str_servicedelay) * (1000));
                        StartService();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred. Type: {0} Message: {1}", ex.GetType().FullName, ex.Message);
            }
        }

        public void UpdCsvDataIntoSqlServer(string fileName, string ent_num)
        {
            // 日更新檔
            var createdCount = 0;

            var dt = new DateTime();

            dt = DateTime.Now;

            using (var textFieldParser = new TextFieldParser(fileName, System.Text.Encoding.GetEncoding(28591)))
            {
                textFieldParser.TextFieldType = FieldType.Delimited;
                textFieldParser.Delimiters = new[] { "\t" };

                var dataTable = new DataTable("TempWorldCheckDay");

                // Add the columns in the temp table
                dataTable.Columns.Add("UID");
                dataTable.Columns.Add("LastName");
                dataTable.Columns.Add("FirstName");
                dataTable.Columns.Add("Aliases");
                dataTable.Columns.Add("LowQAliases");
                dataTable.Columns.Add("AlterSpell");
                dataTable.Columns.Add("Category");
                dataTable.Columns.Add("Title");
                dataTable.Columns.Add("SubCategory");
                dataTable.Columns.Add("Position");
                dataTable.Columns.Add("Age");
                dataTable.Columns.Add("DOB");
                dataTable.Columns.Add("PlaceOfBirth");
                dataTable.Columns.Add("Deceased");
                dataTable.Columns.Add("Passports");
                dataTable.Columns.Add("SSN");
                dataTable.Columns.Add("IDENTIFICATION");
                dataTable.Columns.Add("Location");
                dataTable.Columns.Add("Country");
                dataTable.Columns.Add("Company");
                dataTable.Columns.Add("Type");
                dataTable.Columns.Add("LinkedTo");
                dataTable.Columns.Add("FurtherInfo");
                dataTable.Columns.Add("Keywords");
                dataTable.Columns.Add("ExSources");
                dataTable.Columns.Add("UpdCategory");
                dataTable.Columns.Add("Entered");
                dataTable.Columns.Add("Updated");
                dataTable.Columns.Add("Editor");
                dataTable.Columns.Add("AgeDate");

                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();

                    using (var cmd = new SqlCommand("truncate table TempWorldCheckDay;", sqlConnection))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    // Create the bulk copy object
                    var sqlBulkCopy = new SqlBulkCopy(sqlConnection)
                    {
                        DestinationTableName = "TempWorldCheckDay"
                    };

                    // Loop through the CSV and load each set of 100,000 records into a DataTable
                    // Then send it to the LiveTable
                    while (!textFieldParser.EndOfData)
                    {
                        createdCount++;

                        if (createdCount == 1)
                            textFieldParser.ReadFields();

                        try
                        {
                            dataTable.Rows.Add(textFieldParser.ReadFields());
                            //dr = dataTable.Rows[createdCount - 1];
                        }
                        catch (Exception)
                        {
                        }

                        if (createdCount % _batchSize == 0)
                        {
                            InsertDataTable(sqlBulkCopy, sqlConnection, dataTable);
                        }
                    }
                    // Don't forget to send the last batch under _batchSize
                    if (dataTable.Rows.Count > 0)
                        InsertDataTable(sqlBulkCopy, sqlConnection, dataTable);


                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        sqlCommand.CommandText = @"
                        UPDATE WorldCheck 
                        SET LastName = B.LastName,
                            FirstName = B.FirstName,
                            Aliases = B.Aliases,
                            LowQAliases = B.LowQAliases,
                            AlterSpell = B.AlterSpell,
                            Category = B.Category,
                            Title = B.Title,
                            SubCategory = B.SubCategory,
                            Position = B.Position,
                            Age = B.Age,
                            DOB = B.DOB,
                            PlaceOfBirth = B.PlaceOfBirth,
                            Deceased = B.Deceased,
                            Passports = B.Passports,
                            SSN = B.SSN,
                            Location = B.Location,
                            Country = B.Country,
                            Company = B.Company,
                            Type = B.Type,
                            LinkedTo = B.LinkedTo,
                            FurtherInfo = B.FurtherInfo,
                            Keywords = B.Keywords,
                            ExSources = B.ExSources,
                            UpdCategory = B.UpdCategory,
                            Entered = B.Entered,
                            Updated = B.Updated,
                            Editor = B.Editor,
                            AgeDate = B.AgeDate,
                            DELFlag = 0,
                            UPDDate = @date
                                FROM WorldCheck wc JOIN TempWorldCheckday B
                                ON wc.UID = B.UID;
                          ";
                        sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Update:{0}", UpdateCount);
                        OFACEventLog("worldcheckday totoal update:" + UpdateCount);
                    }

                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        sqlCommand.CommandText = @"
                        insert into WorldCheck
([UID],[LastName],[FirstName],[Aliases],[LowQAliases],[AlterSpell],[Category],[Title],[SubCategory],[Position],[Age]
,[DOB],[PlaceOfBirth],[Deceased],[Passports],[SSN],[Location],[Country],[Company],[Type],[LinkedTo],[FurtherInfo]
,[Keywords],[ExSources],[UpdCategory],[Entered],[Updated],[Editor],[AgeDate],[Entnum],[UPDDate],[DELFlag])
select [UID],[LastName],[FirstName],[Aliases],[LowQAliases],[AlterSpell],[Category],[Title],[SubCategory],[Position],[Age]
,[DOB],[PlaceOfBirth],[Deceased],[Passports],[SSN],[Location],[Country],[Company],[Type],[LinkedTo],[FurtherInfo]
,[Keywords],[ExSources],[UpdCategory],[Entered],[Updated],[Editor],[AgeDate],[UID]+@entnum,@date,0
from TempWorldCheckDay tempwc
where not EXISTS (select [UID] from WorldCheck where uid = tempwc.uid);
                          ";
                        sqlCommand.Parameters.AddWithValue("@entnum", int.Parse(ent_num));
                        sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                        int InsetCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Insert:{0}", InsetCount);
                        OFACEventLog("worldcheckday totoal insert:" + InsetCount);
                    }

                    sqlConnection.Close();
                    sqlConnection.Dispose();
                }
            }
        }

        public void DelCsvDataIntoSqlServer(string fileName)
        {
            // 日刪除檔

            var createdCount = 0;

            var dt = new DateTime();
            dt = DateTime.Now;

            using (var textFieldParser = new TextFieldParser(fileName, System.Text.Encoding.GetEncoding(28591)))
            {
                textFieldParser.TextFieldType = FieldType.Delimited;
                textFieldParser.Delimiters = new[] { "\t" };

                var dataTable = new DataTable("TempWorldCheckDel");

                // Add the columns in the temp table
                dataTable.Columns.Add("UID");
                dataTable.Columns.Add("DATE");

                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();

                    using (var cmd = new SqlCommand("truncate table TempWorldCheckDel;", sqlConnection))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    var sqlBulkCopy = new SqlBulkCopy(sqlConnection)
                    {
                        DestinationTableName = "TempWorldCheckDel"
                    };

                    while (!textFieldParser.EndOfData)
                    {
                        createdCount++;

                        if (createdCount == 1)
                            textFieldParser.ReadFields();

                        try
                        {
                            dataTable.Rows.Add(textFieldParser.ReadFields());
                        }
                        catch (Exception)
                        {
                        }

                        if (createdCount % _batchSize == 0)
                        {
                            InsertDataTable(sqlBulkCopy, sqlConnection, dataTable);
                        }
                    }
                    // Don't forget to send the last batch under _batchSize
                    if (dataTable.Rows.Count > 0)
                        InsertDataTable(sqlBulkCopy, sqlConnection, dataTable);

                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        sqlCommand.CommandText = @"
                        UPDATE WorldCheck
                        SET WorldCheck.Updated = TempWorldCheckDel.Updated,
                            WorldCheck.DelFlag = 1,
                            WorldCheck.UPDDate = @date
                                FROM TempWorldCheckDel
                                where WorldCheck.UID = TempWorldCheckDel.UID;
                          ";
                        sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Update:{0}", UpdateCount);
                        OFACEventLog("worldcheckdaydel totoal update:" + UpdateCount);
                    }

                    sqlConnection.Close();
                    sqlConnection.Dispose();
                }
            }
        }

        public void UpdateSDN(string wc_listtype, string str_UPDDate, string str_wc_yearlimit, string wc_excludecountry, string entnum,
            string wc_fixedcountry, string wc_div, string wc_subcate_space, string wc_import_un, string wc_import_SQL, string wc_import_crime, string wc_import_crime_SQL)
        {

            //2019.01 轉檔順序 ADVERSE NEWS -->CAATSA---> DPL --> 311 --> UN (*依序轉入 UN 最大)
            int wc_yearlimit = 6;
            if (!int.TryParse(str_wc_yearlimit, out wc_yearlimit))
                wc_yearlimit = 6;
            wc_yearlimit = -wc_yearlimit;
            LogAdd("[Daily] Update SDNTable -- Start YearLimit : " + wc_yearlimit);

            // update sdn data
            var dt = new DateTime();
            dt = DateTime.Now;

            if (string.IsNullOrEmpty(str_UPDDate))
            {
                str_UPDDate = "30";
            }

            string query = "";

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                //已經存在SDN的做更新
                LogAdd("[Daily] Update SDNTable with WorldCheck --  Start");

                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                        update SDNTable
                          set Name = ltrim(case when isnull(wc.FirstName,'') = '-' then '' else isnull(wc.FirstName,'') end + ' ' +isnull(wc.LastName,'')),
                          FirstName = case when isnull(wc.FirstName,'') = '-' then '' else isnull(wc.FirstName,'') end,
                          LastName = case when isnull(wc.[type],'') = 'E' then '' else isnull(wc.LastName,'') end,
                          ListType = 'WPEP',
                          Program = case when wc.SubCategory = '' then 'PEP' else wc.SubCategory end,
                          Title = SUBSTRING(isnull(wc.Position,''),0,200),
                          [type] = case when isnull(wc.[type],'') in ('M','F','I','U') then 'individual' 
                                        when wc.[type] = 'E' and wc.category like '%vessel%' then 'Vessel'
                                        when wc.[type] = 'E' and wc.category not like '%vessel%' then 'Other' 
                                        else 'other' end,
                          Dob = wc.dob,
                          PrimeAdded = '0',
                          Country = wc.Country,
                          Remarks = wc.FurtherInfo + ',Keywords:' + isnull(wc.Keywords,'')+',ExSources:'+isnull(wc.ExSources,''),
                          Remarks1 = SUBSTRING(isnull(wc.Keywords,''),0,255),
                          Remarks2 = SUBSTRING(isnull(Location,''),0,255),
                          sdntype =
                          case 
                          when wc.[type] = 'E' and wc.category not like '%vessel%' then '4'
                          when wc.[type] = 'E' and wc.category like '%vessel%' then '3' 
                          when wc.[type] in ('m','f', 'i','u') then '1' end,
                          DataType = '1',UserRec = '1',Deleted = '0',DuplicRec = '0',
                          IgnoreDerived = '0',
                          ListCreateDate = wc.Entered,
                          ListModifDate = wc.Updated,
                          LastModifDate = @date,
                          LastOper = 'Prime'
                        FROM SDNTable A JOIN WorldCheck wc
	                      ON A.EntNum = wc.Entnum 
                        where wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                        and isnull(wc.DelFlag,'0') = '0';
                          ";
                    sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 360;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    LogAdd("[Daily] Update SDNTable with WorldCheck --  Completed. Count : " + UpdateCount);

                    //顯示更新筆數
                    Console.WriteLine("Update:{0}", UpdateCount);
                    OFACEventLog("SDN_WCday totoal update:" + UpdateCount);
                }

                //不存在的才做insert
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    LogAdd("[Daily] Insert SDNTable with WorldCheck --  Start.");

                    query = @"
                        insert into SDNTable(EntNum,
                          Name,
                          FirstName,LastName,
                          ListType,
                          Program,
                          Title,
                          [type],
                          Dob,PrimeAdded,
                          Country,
                          Remarks,
                          Remarks1,
                          Remarks2,
                          SDNType,DataType,
                          UserRec,Deleted,DuplicRec,IgnoreDerived,Status,ListCreateDate,ListModifDate,CreateDate,LastModifDate,LastOper)
                        select distinct wc.Entnum, 
                            ltrim(case when isnull(FirstName,'') = '-' then '' else isnull(FirstName,'') end + ' ' +isnull(LastName,'')) name, 
                            isnull(case when isnull(wc.FirstName,'') = '-' then '' else isnull(wc.FirstName,'') end,'') FirstName, 
                            case when isnull(wc.[type],'') = 'E' then '' else isnull(wc.LastName,'') end, 
                            'WPEP' listtype,
                            case when SubCategory = '' then 'PEP' else SubCategory end,
                            SUBSTRING(isnull(Position,''),0,200) Position,
                            case when isnull(wc.[type],'') in ('M','F','I','U') then 'individual' 
                                when wc.[type] = 'E' and wc.category like '%vessel%' then 'Vessel'
                                when wc.[type] = 'E' and wc.category not like '%vessel%' then 'Other' 
                                else 'other' end,
                            dob,'0' primeadded,
                            Country,
                            FurtherInfo + ',Keywords:' + isnull(Keywords,'')+',isnull(ExSources,''):'+ExSources,
                            SUBSTRING(isnull(Keywords,''),0,255) Keywords,
                            SUBSTRING(isnull(Location,''),0,255) Location,
                            case when wc.[type] = 'E' and wc.category not like '%vessel%' then '4'
                            when wc.[type] = 'E' and wc.category like '%vessel%' then '3' 
                            when wc.[type] in ('m','f', 'i','u') then '1' end,
                            '1' datatype, '1' userrec,'0' deleted,'0' duplicRec,
                            '0' ignorederived,'1' [status],Entered,Updated,@date createdate,@date lastmodifdate,'Prime' lastoper
                        from [dbo].[WorldCheck] wc
                        where wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                        and isnull(DelFlag,'0') = '0'
                        AND (SubCategory is not null and SubCategory != '') 
                        and not EXISTS (select Entnum from SDNTable where EntNum = wc.Entnum)

                        union

                        select distinct wc.Entnum, 
                            ltrim(case when isnull(FirstName,'') = '-' then '' else isnull(FirstName,'') end + ' ' +isnull(LastName,'')) name, 
                            isnull(case when isnull(wc.FirstName,'') = '-' then '' else isnull(wc.FirstName,'') end,'') FirstName, 
                            case when isnull(wc.[type],'') = 'E' then '' else isnull(wc.LastName,'') end, 
                            'WPEP' listtype,
                            case when SubCategory = '' then 'PEP' else SubCategory end,
                            SUBSTRING(isnull(Position,''),0,200) Position,
                            case when isnull(wc.[type],'') in ('M','F','I','U') then 'individual' 
                                when wc.[type] = 'E' and wc.category like '%vessel%' then 'Vessel'
                                when wc.[type] = 'E' and wc.category not like '%vessel%' then 'Other' 
                                else 'other' end,
                            dob,'0' primeadded,
                            Country,
                            FurtherInfo + ',Keywords:' + isnull(Keywords,'')+',isnull(ExSources,''):'+ExSources,
                            SUBSTRING(isnull(Keywords,''),0,255) Keywords,
                            SUBSTRING(isnull(Location,''),0,255) Location,
                            case when wc.[type] = 'E' and wc.category not like '%vessel%' then '4'
                            when wc.[type] = 'E' and wc.category like '%vessel%' then '3' 
                            when wc.[type] in ('m','f', 'i','u') then '1' end,
                            '1' datatype, '1' userrec,'0' deleted,'0' duplicRec,
                            '0' ignorederived,'1' [status],Entered,Updated,@date createdate,@date lastmodifdate,'Prime' lastoper
                        from [dbo].[WorldCheck] wc
                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                        where wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                        and isnull(DelFlag,'0') = '0'
                        and (SubCategory is null or SubCategory = '')
                        AND (wcl.Used = 1  ";
                    if (wc_import_crime == "Y")
                    {
                        query += "or Category like '%CRIME%' ";
                    }
                        
                    query += ")";
                    query += @"
                        and not EXISTS (select Entnum from SDNTable where EntNum = wc.Entnum)
                          ";

                    //query += "AND ((SubCategory is not null and SubCategory != '') ";

                    //2019.01 修改為抓table
                    //query += "or (wcl.Used = 1) ";

                    //if (wc_subcate_space == "N")
                    //{
                    //    query += "and SubCategory != '' ";
                    //}

                    //query += ") Or (keywords like '%USTREAS.311%') Or (furtherinfo like '%denied persons list%' and furtherinfo like '%US DEPARTMENT OF COMMERCE%') ";

                    //if (wc_import_un == "Y")
                    //{
                    //    query += wc_import_SQL;
                    //}
                    //if (wc_import_crime == "Y")
                    //{
                    //    query += wc_import_crime_SQL;
                    //}

                    //for (int i = 0; i < getWC_para.Count(); i++)
                    //{
                    //    LogAdd("WCpara_count:" + getWC_para.Count().ToString());
                    //    LogAdd("WCSQL:"+ getWC_para[i].WCSQL);
                    //    LogAdd("WCListtype:" + getWC_para[i].WCListtype);
                    //    LogAdd("WCProgram:" + getWC_para[i].WCProgram);

                    //    if (getWC_para[i].WCSQL != "")
                    //    {
                    //        query += getWC_para[i].WCSQL;
                    //    }
                    //}

                    //query += " ) and not EXISTS (select Entnum from SDNTable where EntNum = wc.Entnum) ";

                    sqlCommand.CommandText = query;
                    sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 360;
                    int InsetCount = sqlCommand.ExecuteNonQuery();
                    LogAdd("[Daily] Insert SDNTable with WorldCheck --  Completed. Count : " + InsetCount);
                    LogAdd("Insert SDNTable SQL: " + query);
                    //顯示更新筆數
                    Console.WriteLine("Insert:{0}", InsetCount);
                    OFACEventLog("SDN_WCday totoal insert:" + InsetCount);
                }

                //更新刪除資料
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    LogAdd("[Daily] Delete SDNTable with WorldCheck --  Start. ");

                    sqlCommand.CommandText = @"
                          update SDNTable
                            set Status = 4, LastModifDate = @date, LastOper = 'Prime'
                          from SDNTable A JOIN WorldCheck wc
                            ON A.EntNum = wc.Entnum 
                          where wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and isnull(wc.DelFlag,'0') = 1;
                          ";
                    sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 360;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    LogAdd("[Daily] Delete SDNTable with WorldCheck @min=" + str_UPDDate + ". --  Completed. Count : " + UpdateCount);

                    Console.WriteLine("Update:{0}", UpdateCount);
                    OFACEventLog("SDN_WCdaydel totoal update:" + UpdateCount);
                }

                if (wc_fixedcountry != "Y")
                {
                    //wtire exclude country
                    OFACEventLog(string.Format("Exclude WorldCheckCountry : {0} ", wc_excludecountry));

                    //更新LOG For country對應表的status set status = 1
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Get WorldCheckCountry Status 0 to 1 from Activity --  Start");

                        string SQL = @"select code,status from WorldCheckCountry where code in(
                                         select DISTINCT cust.Country from (select * from pbsa.dbo.Activity (nolock) union select * from pbsa.dbo.ActivityHist (nolock)) act 
                                         JOIN pbsa.dbo.Customer cust on act.Cust = cust.Id
                                         where BookDate >= convert(varchar(8),DATEADD(YEAR,@yearlimit,getdate()),112)
                                         and (cust.Id like @branch or cust.Id like 'noncust%')
                                         and Cust.country not in (@excludecountry)
                                       )
                                       and Status = 0;";
                        sqlCommand.CommandText = SQL;
                        sqlCommand.Parameters.AddWithValue("@branch", wc_listtype.Substring(0, 4) + "%");
                        sqlCommand.Parameters.AddWithValue("@yearlimit", wc_yearlimit);
                        string[] arr_excludecountry = wc_excludecountry.Split(',');
                        string strexcludecountry = string.Empty;
                        for (int i = 0; i < arr_excludecountry.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(arr_excludecountry[i]))
                            {
                                strexcludecountry += "@excludecountry" + i + ",";
                                sqlCommand.Parameters.AddWithValue("@excludecountry" + i, arr_excludecountry[i]);
                            }
                        }
                        if (strexcludecountry.Length > 0)
                            strexcludecountry = strexcludecountry.TrimEnd(',');
                        else
                            strexcludecountry = "''";
                        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@excludecountry", strexcludecountry);
                        sqlCommand.CommandTimeout = 360;
                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        string country = string.Empty;
                        try
                        {
                            while (reader.Read())
                            {
                                country += reader[0].ToString() + ",";
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                        LogAdd("[Daily] Get WorldCheckCountry Status 0 to 1 from Activity --  Completed. ");

                        if (country.TrimEnd(',').Trim().Length > 0)
                        {
                            OFACEventLog(string.Format("Update WorldCheckCountry Enable : {0}", country.TrimEnd(',')));
                        }
                    }

                    //更新WorldCheckCountry set status = 1
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update WorldCheckCountry Status 0 to 1 from Activity --  Start. ");

                        sqlCommand.CommandText = @"
                              update WorldCheckCountry set Status = 1,lastdate = @mdydate
                              where Code in (
                                select DISTINCT cust.Country from  (select * from pbsa.dbo.Activity (nolock) union select * from pbsa.dbo.ActivityHist (nolock)) act 
                                JOIN pbsa.dbo.Customer cust on act.Cust = cust.Id
                              where BookDate >= convert(varchar(8),DATEADD(YEAR,@yearlimit,getdate()),112)
                              and (cust.Id like @branch or cust.Id like 'noncust%')
                               and cust.country not in (@excludecountry)
                              )
                              and status = 0;
                              ";
                        sqlCommand.Parameters.AddWithValue("@branch", wc_listtype.Substring(0, 4) + "%");
                        sqlCommand.Parameters.AddWithValue("@yearlimit", wc_yearlimit);
                        sqlCommand.Parameters.AddWithValue("@mdydate", DateTime.Now.ToString("yyyy-MM-dd"));
                        string[] arr_excludecountry = wc_excludecountry.Split(',');
                        string strexcludecountry = string.Empty;
                        for (int i = 0; i < arr_excludecountry.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(arr_excludecountry[i]))
                            {
                                strexcludecountry += "@excludecountry" + i + ",";
                                sqlCommand.Parameters.AddWithValue("@excludecountry" + i, arr_excludecountry[i]);
                            }
                        }
                        if (strexcludecountry.Length > 0)
                            strexcludecountry = strexcludecountry.TrimEnd(',');
                        else
                            strexcludecountry = "''";
                        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@excludecountry", strexcludecountry);

                        //LogAdd("WroldcheckCountry 0 to 1 SQL : " + sqlCommand.CommandText);

                        sqlCommand.CommandTimeout = 360;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        LogAdd("[Daily] Update WorldCheckCountry Status 0 to 1 from Activity --  Complete. Count: " + UpdateCount);

                        OFACEventLog("Update WorldCheckCountry set status = 1 totoal update:" + UpdateCount);
                    }

                    //更新LOG For country對應表的status set status = 0
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Get WorldCheckCountry Status 1 to 0 from Activity --  Start. ");

                        string SQL = @"select code,status from WorldCheckCountry  where Code not in (
                                            select DISTINCT cust.Country from (select * from pbsa.dbo.Activity (nolock) union select * from pbsa.dbo.ActivityHist (nolock)) act 
                                            JOIN pbsa.dbo.Customer cust on act.Cust = cust.Id
                                          where BookDate >= convert(varchar(8),DATEADD(YEAR,@yearlimit,getdate()),112)
                                          and (cust.Id like @branch or cust.Id like 'noncust%')
                                           and cust.country not in (@excludecountry)
                                          )
                                          and status = 1;";
                        sqlCommand.CommandText = SQL;
                        sqlCommand.Parameters.AddWithValue("@branch", wc_listtype.Substring(0, 4) + "%");
                        sqlCommand.Parameters.AddWithValue("@yearlimit", wc_yearlimit);
                        string[] arr_excludecountry = wc_excludecountry.Split(',');
                        string strexcludecountry = string.Empty;
                        for (int i = 0; i < arr_excludecountry.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(arr_excludecountry[i]))
                            {
                                strexcludecountry += "@excludecountry" + i + ",";
                                sqlCommand.Parameters.AddWithValue("@excludecountry" + i, arr_excludecountry[i]);
                            }
                        }
                        if (strexcludecountry.Length > 0)
                            strexcludecountry = strexcludecountry.TrimEnd(',');
                        else
                            strexcludecountry = "''";
                        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@excludecountry", strexcludecountry);
                        sqlCommand.CommandTimeout = 360;

                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        string country0 = string.Empty;
                        try
                        {
                            while (reader.Read())
                            {
                                country0 += reader[0].ToString() + ",";
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                        LogAdd("[Daily] Get WorldCheckCountry Status 1 to 0 from Activity --  Complete.");

                        if (country0.TrimEnd(',').Trim().Length > 0)
                        {
                            OFACEventLog(string.Format("Update WorldCheckCountry Disable : {0}", country0.TrimEnd(',')));
                        }
                    }

                    //更新WorldCheckCountry set status = 0
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update WorldCheckCountry Status 1 to 0 from Activity --  Start. ");

                        sqlCommand.CommandText = @"
                          update WorldCheckCountry set Status = 0,lastdate = @mdydate
                          where Code not in (
                            select DISTINCT cust.Country from (select * from pbsa.dbo.Activity (nolock) union select * from pbsa.dbo.ActivityHist (nolock)) act 
                            JOIN pbsa.dbo.Customer cust on act.Cust = cust.Id
                          where BookDate >= convert(varchar(8),DATEADD(YEAR,@yearlimit,getdate()),112)
                          and (cust.Id like @branch or cust.Id like 'noncust%')
                           and cust.country not in (@excludecountry)
                          )
                          and status = 1;
                          ";
                        sqlCommand.Parameters.AddWithValue("@branch", wc_listtype.Substring(0, 4) + "%");
                        sqlCommand.Parameters.AddWithValue("@yearlimit", wc_yearlimit);
                        sqlCommand.Parameters.AddWithValue("@mdydate", DateTime.Now.ToString("yyyy-MM-dd"));
                        string[] arr_excludecountry = wc_excludecountry.Split(',');
                        string strexcludecountry = string.Empty;
                        for (int i = 0; i < arr_excludecountry.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(arr_excludecountry[i]))
                            {
                                strexcludecountry += "@excludecountry" + i + ",";
                                sqlCommand.Parameters.AddWithValue("@excludecountry" + i, arr_excludecountry[i]);
                            }
                        }
                        if (strexcludecountry.Length > 0)
                            strexcludecountry = strexcludecountry.TrimEnd(',');
                        else
                            strexcludecountry = "''";
                        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@excludecountry", strexcludecountry);

                        sqlCommand.CommandTimeout = 360;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        LogAdd("[Daily] Update WorldCheckCountry Status 1 to 0 from Activity --  Complete. Count: " + UpdateCount);

                        OFACEventLog("Update WorldCheckCountry set status = 0 totoal update:" + UpdateCount);
                    }
                }

                if (wc_div != "Y")
                {
                    //更新listtype
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update SDNTable ListType --  Start");

                        sqlCommand.CommandText = @"
                          update SDNTable
                            set SDNTable.listtype = (select case when wcc.[status] = '1' and CHARINDEX(rtrim(a.Type),rtrim(wcc.Type)) > 0 then @wc_listtype 
                                when wcc.[status] = '1' and wcc.Type is null then @wc_listtype
                                else 'WPEP' end
                            from WorldCheckCountry wcc
                            where a.country = wcc.WorldCheck_Country),
                            LastModifDate = @date, LastOper = 'Prime'
                          from SDNTable a join WorldCheck wc
                            on a.EntNum = wc.Entnum
                          where a.country in (select WorldCheck_Country from WorldCheckCountry)
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE());
                          ";
                        sqlCommand.Parameters.AddWithValue("@wc_listtype", wc_listtype);
                        sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                        sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                        sqlCommand.CommandTimeout = 360;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Update:{0}", UpdateCount);
                        LogAdd("[Daily] Update SDNTable ListType --  Completed. Count : " + UpdateCount);
                        OFACEventLog("SDN_ListType totoal update:" + UpdateCount);
                    }


                    //更新listtype BY worldcheck country
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update SDNTable ListType by country --  Start.");

                        sqlCommand.CommandText = @"
                          update SDNTable
                            set SDNTable.listtype =  case when wcc.[status] = '1' and CHARINDEX(rtrim(a.Type),rtrim(wcc.Type)) > 0 then @wc_listtype 
                                when wcc.[status] = '1' and wcc.Type is null then @wc_listtype
                                else 'WPEP' end,
                            LastModifDate = @date, LastOper = 'Prime'
                               from SDNTable (nolock) a join WorldCheckCountry wcc on a.Country = wcc.WorldCheck_Country
                                JOIN WorldCheck wc  ON A.EntNum = wc.Entnum 
                              where wcc.lastdate >= convert(nchar(10),GETDATE(), 112);
                          ";
                        sqlCommand.Parameters.AddWithValue("@wc_listtype", wc_listtype);
                        sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                        sqlCommand.CommandTimeout = 500;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Update:{0}", UpdateCount);
                        LogAdd("[Daily] Update SDNTable ListType by country --  Completed. Count : " + UpdateCount);
                        OFACEventLog("SDN_ListType totoal update by country:" + UpdateCount);
                    }

                    //更新LOG For country status = 1 的所有國家
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Get WorldCheckCountry where Status = 1 --  Start. ");

                        string SQL = @"select code from WorldCheckCountry (nolock)
                                 where Status = 1;";
                        sqlCommand.CommandText = SQL;
                        sqlCommand.CommandTimeout = 360;

                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        string country1 = string.Empty;
                        try
                        {
                            while (reader.Read())
                            {
                                country1 += reader[0].ToString() + ",";
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                        LogAdd("[Daily] Get WorldCheckCountry where Status = 1 --  Complete.");

                        if (country1.TrimEnd(',').Trim().Length > 0)
                        {
                            OFACEventLog(string.Format("Show WorldCheckCountry All : {0}", country1.TrimEnd(',')));
                        }
                    }
                }
                else if (wc_div == "Y")
                {
                    //更新listtype For SD
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update SDNTable ListType for SD --  Start.");

                        sqlCommand.CommandText = @"
                          update SDNTable
                            set SDNTable.listtype = (select wcc.listtype
                            from WorldCheckCountry wcc
                            where a.country = wcc.WorldCheck_Country),
                            LastModifDate = @date, LastOper = 'Prime'
                          from SDNTable a join WorldCheck wc
                            on a.EntNum = wc.Entnum
                          where a.country in (select WorldCheck_Country from WorldCheckCountry)
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE());
                          ";
                        sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                        sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                        sqlCommand.CommandTimeout = 500;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Update:{0}", UpdateCount);
                        LogAdd("[Daily] Update SDNTable ListType for SD --  Completed. Count : " + UpdateCount);
                        OFACEventLog("SDN_ListType totoal update for SD:" + UpdateCount);
                    }
                }

                //2019.01 使用table來update 不同的program
                //2019.01 轉檔順序 ADVERSE NEWS -->CAATSA---> DPL --> 311 --> UN (*依序轉入 UN 最大)
                if (!string.IsNullOrEmpty(str_wc_imp_order))
                {
                    string SQL = "";
                    foreach (string str_order in str_wc_imp_order.Split('|'))
                    {
                        using (var sqlCommand = new SqlCommand("", sqlConnection))
                        {
                            LogAdd("[Daily] Update SDNTable ListType for " + str_order + " --  Start.");

                            SQL = @"
                            update SDNTable set ListType = 'WORLD-CHECK',Program = @order
                            from SDNTable a join WorldCheck wc on a.EntNum = wc.Entnum
                            left join WCKeywords wck on wc.UID = wck.uid
                            left join WCKeywordslistTable wckl on wck.Word = wckl.Abbreviation
                            where isnull(wc.DelFlag,'0') = '0'
                            and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                            and ((wckl.Used = 1 and wckl.SDNPROGRAM = @order) ";
                            if (wc_import_crime_SQL == str_order)
                            {
                                SQL += "or wc.Category like '%CRIME%' ";
                            }
                            SQL += ")";
                            sqlCommand.CommandText = SQL;
                            sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                            sqlCommand.Parameters.AddWithValue("@order", str_order);
                            sqlCommand.CommandTimeout = 500;
                            int UpdateCount = sqlCommand.ExecuteNonQuery();
                            //顯示更新筆數
                            LogAdd("[Daily] Update SDNTable ListType for " + str_order + " SQL=" + SQL);
                            LogAdd("[Daily] Update SDNTable ListType for " + str_order + " --  Completed. Count : " + UpdateCount);
                            OFACEventLog("SDN_ListType totoal update for " + str_order + " type:" + UpdateCount);
                        }
                    }
                }

                /*
                //更新Program For FINCEN311
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    LogAdd("[Daily] Update SDNTable ListType for FINCEN311 --  Start.");

                    sqlCommand.CommandText = @"
                          update SDNTable set ListType = 'WORLD-CHECK',Program = 'FINCEN311'
                          from SDNTable a join WorldCheck wc
                            on a.EntNum = wc.Entnum
                          where isnull(wc.DelFlag,'0') = '0'
                          and (wc.keywords like '%USTREAS.311%') 
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE());
                          ";
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 500;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Update:{0}", UpdateCount);
                    LogAdd("[Daily] Update SDNTable ListType for FINCEN311 --  Completed. Count : " + UpdateCount);
                    OFACEventLog("SDN_ListType totoal update for FINCEN311:" + UpdateCount);
                }

                //更新Program For DPL
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    LogAdd("[Daily] Update SDNTable ListType for DPL --  Start.");

                    sqlCommand.CommandText = @"
                          update SDNTable set ListType = 'WORLD-CHECK', Program = 'DPL'
                          from SDNTable a join WorldCheck wc
                            on a.EntNum = wc.Entnum
                          where isnull(wc.DelFlag,'0') = '0'
                          and (furtherinfo like '%denied persons list%' and furtherinfo like '%US DEPARTMENT OF COMMERCE%') 
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE());
                          ";
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 500;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Update:{0}", UpdateCount);
                    LogAdd("[Daily] Update SDNTable ListType for DPL --  Completed. Count : " + UpdateCount);
                    OFACEventLog("SDN_ListType totoal update for DPL:" + UpdateCount);
                }

                //更新Program For Negative news. note:不會將PEP的黑名單改成負面消息的黑名單
                if (wc_import_crime == "Y")
                {
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update SDNTable ListType for Negative news --  Start.");

                        sqlCommand.CommandText = "";
                        string str_SQL = @"
                          update OFAC.dbo.SDNTable set ListType = 'WORLD-CHECK', Program = 'Negative news' 
                          from OFAC.dbo.SDNTable a join OFAC.dbo.WorldCheck wc 
                            on a.EntNum = wc.Entnum 
                          where isnull(wc.DelFlag,'0') = '0' 
                          and (1 = 0 ";
                        str_SQL += wc_import_crime_SQL;
                        str_SQL += @") and (listtype = 'WPEP' or program = 'PEP' or program is null)
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE()); 
                          ";
                        sqlCommand.CommandText = str_SQL;
                        //sqlCommand.Parameters.AddWithValue("@wc_listtype", wc_listtype);
                        sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                        sqlCommand.CommandTimeout = 500;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Update:{0}", UpdateCount);
                        LogAdd("[Daily] Update SDNTable ListType for Negative news --  Completed. Count : " + UpdateCount);
                        LogAdd("[SQL] SQL for Negative news:" + str_SQL);
                        OFACEventLog("SDN_ListType totoal update for Negative news:" + UpdateCount);
                    }
                }

                if (wc_import_un == "Y")
                {
                    //更新Program For UN
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update SDNTable ListType for UN --  Start.");
                        string str_SQL = @"update SDNTable set ListType = 'WORLD-CHECK',Program = 'UN'
                          from SDNTable a join WorldCheck wc
                            on a.EntNum = wc.Entnum
                          where isnull(wc.DelFlag,'0') = '0' 
                          and (1 = 0 ";
                        str_SQL += wc_import_SQL;
                        str_SQL += ") and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE());";

                        sqlCommand.CommandText = str_SQL;
                        sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                        sqlCommand.CommandTimeout = 500;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Update:{0}", UpdateCount);
                        LogAdd("[Daily] Update SDNTable ListType for UN --  Completed. Count : " + UpdateCount);
                        LogAdd("[SQL] SQL for UN:" + str_SQL);
                        OFACEventLog("SDN_ListType totoal update for UN:" + UpdateCount);
                    }
                }
                
                */
                for (int i = 0; i < getWC_para.Count(); i++)
                {
                    if (getWC_para[i].WCSQL != "" && getWC_para[i].WCListtype != "" && getWC_para[i].WCProgram != "")
                    {
                        //更新Program by parameter
                        using (var sqlCommand = new SqlCommand("", sqlConnection))
                        {
                            LogAdd("[Daily] Update SDNTable ListType for " + getWC_para[i].WCProgram + " --  Start.");
                            string str_SQL = @"update SDNTable set ListType = @listtype,Program = @program
                          from SDNTable a join WorldCheck wc
                            on a.EntNum = wc.Entnum
                          where isnull(wc.DelFlag,'0') = '0' 
                          and (1 = 0 ";
                            str_SQL += getWC_para[i].WCSQL;
                            str_SQL += ") and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE());";

                            sqlCommand.CommandText = str_SQL;
                            sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                            sqlCommand.Parameters.AddWithValue("@listtype", getWC_para[i].WCListtype);
                            sqlCommand.Parameters.AddWithValue("@program", getWC_para[i].WCProgram);
                            sqlCommand.CommandTimeout = 500;
                            int UpdateCount = sqlCommand.ExecuteNonQuery();
                            //顯示更新筆數
                            Console.WriteLine("Update:{0}", UpdateCount);
                            LogAdd("[Daily] Update SDNTable ListType for " + getWC_para[i].WCProgram + " --  Completed. Count : " + UpdateCount);
                            LogAdd("[SQL] SQL for " + getWC_para[i].WCProgram + ":" + str_SQL);
                            OFACEventLog("SDN_ListType totoal update for " + getWC_para[i].WCProgram + ":" + UpdateCount);
                        }
                    }
                }

                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        public void UpdRelationshipDailyDataIntoSqlServer(string BCPFile_TempRelationship, string BCPFile_Relationship, string BCPFmt_TempRelationship,
            string BCPFile_Error, string BCPFmt_Relationship, string BCPServer, string BCPUser, string BCPPwd, string BaseNum, string wc_subcate_space, string wc_import_un, string wc_import_SQL,
            string wc_import_crime, string wc_import_crime_SQL)
        {
            try
            {
                LogAdd("[Daily] Expand to Relationship -- Start");
                string query = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;
                string queryLog1 = string.Empty;
                string queryLog2 = string.Empty;
                string LogContent1 = string.Empty;
                string LogContent2 = string.Empty;
                string LinkedTo = string.Empty;
                string RelationShipId = string.Empty;
                string EntNum = string.Empty;
                string RelatedID = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");

                int RelationShipIdMax = 0;
                int File_TotalNum = 0;
                int File_UpdDelNum = 0;
                int File_UpdAddNum = 0;
                int File_FinalAddNum = 0;
                int finalLinkedToSplit = 0;
                int rowCount = 0;

                DateTime NowDateTime = DateTime.Now;

                if (File.Exists(BCPFile_TempRelationship)) File.Delete(BCPFile_TempRelationship);

                StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TempRelationship, false, Encoding.GetEncoding(28591));

                if (File.Exists(BCPFile_Relationship)) File.Delete(BCPFile_Relationship);

                StreamWriter fileDWriter = new StreamWriter(BCPFile_Relationship, false, Encoding.GetEncoding(28591));

                queryMax = string.Format(@" SELECT MAX(RelationshipId)+1 AS RelationshipIdMax FROM OFAC.dbo.Relationship ");

                //query = string.Format(@" SELECT Entnum,LinkedTo FROM OFAC.dbo.WorldCheck WHERE LinkedTo IS NOT NULL AND Updated > DATEADD(Daily,-1,GETDATE()) ");

                //2019.01 change to table config
                query = @" SELECT distinct WC.Entnum, WC.LinkedTo, 
                                         CASE ISNULL(WC.DelFlag, '0') 
                                         WHEN '0' THEN '1' 
                                         WHEN '1' THEN '4' END AS Status 
                                        from [dbo].[WorldCheck] wc
                                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                                         WHERE WC.LinkedTo != ''
                                         AND WC.UPDDate > DATEADD(DAY, -1, GETDATE()) 
                                         AND (SubCategory is not null and SubCategory != '') 

                                        union

                                        SELECT distinct WC.Entnum, WC.LinkedTo, 
                                         CASE ISNULL(WC.DelFlag, '0') 
                                         WHEN '0' THEN '1' 
                                         WHEN '1' THEN '4' END AS Status 
                                        from [dbo].[WorldCheck] wc
                                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                                         WHERE WC.LinkedTo != ''
                                         AND WC.UPDDate > DATEADD(DAY, -1, GETDATE()) 
                                         and (SubCategory is null or SubCategory = '')
                                         AND (wcl.Used = 1 ";
                if (wc_import_crime == "Y")
                {
                    query += @"          or Category like '%CRIME%'";
                }
                                        query += ")";
                
                /*
                query += "AND ((SubCategory is not null ";

                if (wc_subcate_space == "N")
                {
                    query += "and SubCategory != '' ";
                }

                query += ") Or (keywords like '%USTREAS.311%') Or (furtherinfo like '%denied persons list%' and furtherinfo like '%US DEPARTMENT OF COMMERCE%') ";

                if (wc_import_un == "Y")
                {
                    query += wc_import_SQL;
                }
                if (wc_import_crime == "Y")
                {
                    query += wc_import_crime_SQL;
                }

                //20181022. for parameter
                for (int i = 0; i < getWC_para.Count(); i++)
                {
                    if (getWC_para[i].WCSQL != "" && getWC_para[i].WCListtype != "" && getWC_para[i].WCProgram != "")
                    {
                        query += getWC_para[i].WCSQL;
                    }
                }

                query += " );";
                */

                Database db;
                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 0;
                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("RelationshipIdMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            RelationShipIdMax = Convert.ToInt32(dataReaderMax["RelationshipIdMax"]);
                        }
                    }
                }

                IDataReader dataReaderDT = db.ExecuteReader(dbCommand);
                DataTable dt = new DataTable();
                dt.Load(dataReaderDT);
                rowCount = dt.Rows.Count;
                dataReaderDT.Close();
                int intBaseNum = Convert.ToInt32(BaseNum);
                using (IDataReader dataReader = db.ExecuteReader(dbCommand))
                {
                    while (dataReader.Read())
                    {
                        EntNum = dataReader["Entnum"].ToString().Trim();
                        LinkedTo = dataReader["LinkedTo"].ToString().Trim();
                        Status = dataReader["Status"].ToString().Trim();
                        string[] LinkedToSplit = dataReader["LinkedTo"].ToString().Trim().Split(';');
                        int i;
                        for (i = 0; i < LinkedToSplit.Length; checked(i)++)
                        {
                            if (LinkedToSplit[i].Trim() != "")
                            {

                                finalLinkedToSplit = Convert.ToInt32(LinkedToSplit[i].Trim()) + intBaseNum;
                                StringBuilder builder = new StringBuilder();
                                builder.AppendFormat("{0}\t", RelationShipIdMax.ToString());
                                builder.AppendFormat("{0}\t", EntNum);
                                builder.AppendFormat("{0}\t", finalLinkedToSplit.ToString());
                                builder.AppendFormat("{0}", Status);
                                TempfileDWriter.WriteLine(builder.ToString());
                                RelationShipIdMax++;
                                File_TotalNum++;
                            }
                        }
                    }
                    dataReader.Close();
                }

                TempfileDWriter.Close();

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TempRelationship ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                if (rowCount != 0)
                {
                    Process p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.TempRelationship in " + BCPFile_TempRelationship + "  -f " + BCPFmt_TempRelationship + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();

                    string queryUpdateDelDetail =
                        string.Format(@" SELECT * FROM OFAC.dbo.Relationship
                                     WHERE EXISTS (SELECT * FROM OFAC.dbo.TempRelationship WHERE Relationship.EntNum = TempRelationship.EntNum AND Relationship.RelatedID = TempRelationship.RelatedID AND TempRelationship.Status='4')
 AND Relationship.RelationshipPToR='WPEP' ");

                    DbCommand dbCommandUpdateDelDetial = db.GetSqlStringCommand(queryUpdateDelDetail);
                    dbCommandUpdateDelDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateDelDetail = db.ExecuteReader(dbCommandUpdateDelDetial))
                    {
                        while (drUpdateDelDetail.Read())
                        {
                            //if (File_UpdDelNum == 0) { LogAdd("[Daily] Relationship - Set Status to 4: "); };
                            //LogAdd(
                            //    drUpdateDelDetail["RelationshipId"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["EntNum"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["RelatedID"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["RelationshipPToR"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["RelationshipRToP"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListCreateDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateOper"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastOper"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastModify"].ToString().Trim());
                            File_UpdDelNum++;
                        }
                    }

                    string queryUpdateDel =
                        string.Format(@"  UPDATE OFAC.dbo.Relationship
 SET Status='4', LastModify=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempRelationship WHERE Relationship.EntNum = TempRelationship.EntNum AND Relationship.RelatedID = TempRelationship.RelatedID AND TempRelationship.Status='4')
 AND Relationship.RelationshipPToR='WPEP' ");

                    DbCommand dbCommandUpdateDel = db.GetSqlStringCommand(queryUpdateDel);
                    dbCommandUpdateDel.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateDel);

                    string queryUpdateAddDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.Relationship
                                 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempRelationship WHERE Relationship.EntNum = TempRelationship.EntNum AND Relationship.RelatedID = TempRelationship.RelatedID AND TempRelationship.Status='1')
 AND Relationship.RelationshipPToR='WPEP' ");

                    DbCommand dbCommandUpdateAddDetial = db.GetSqlStringCommand(queryUpdateAddDetail);
                    dbCommandUpdateAddDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateAddDetail = db.ExecuteReader(dbCommandUpdateAddDetial))
                    {
                        while (drUpdateAddDetail.Read())
                        {
                            //if (File_UpdAddNum == 0) { LogAdd("[Daily] Relationship - Set Status to 1: "); };
                            //LogAdd(
                            //    drUpdateAddDetail["RelationshipId"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["EntNum"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["RelatedID"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["RelationshipPToR"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["RelationshipRToP"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListCreateDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateOper"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastOper"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastModify"].ToString().Trim());
                            File_UpdAddNum++;
                        }
                    }

                    string queryUpdateAdd =
                        string.Format(@"  UPDATE OFAC.dbo.Relationship
 SET Status='1', LastModify=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempRelationship WHERE Relationship.EntNum = TempRelationship.EntNum AND Relationship.RelatedID = TempRelationship.RelatedID AND TempRelationship.Status='1')
 AND Relationship.RelationshipPToR='WPEP' ");

                    DbCommand dbCommandUpdateAdd = db.GetSqlStringCommand(queryUpdateAdd);
                    dbCommandUpdateAdd.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateAdd);

                    string queryAdd =
                        string.Format(@" SELECT DISTINCT RelationshipId, EntNum, RelatedID, Status FROM OFAC.dbo.TempRelationship
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.Relationship WHERE TempRelationship.EntNum = EntNum AND TempRelationship.RelatedID = RelatedID)
                                     ORDER BY RelationshipId, EntNum, RelatedID ");

                    DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                    dbCommandAdd.CommandTimeout = 0;
                    using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                    {
                        while (Tempdr.Read())
                        {
                            RelationShipId = Tempdr["RelationShipId"].ToString().Trim();
                            EntNum = Tempdr["EntNum"].ToString().Trim();
                            RelatedID = Tempdr["RelatedID"].ToString().Trim();
                            Status = Tempdr["Status"].ToString().Trim();
                            StringBuilder builder = new StringBuilder();
                            builder.AppendFormat("{0}\t", RelationShipId);
                            builder.AppendFormat("{0}\t", EntNum);
                            builder.AppendFormat("{0}\t", RelatedID);
                            builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}", "WPEP", "WPEP", Status, NowDateTime.ToString(), NowDateTime.ToString(), "Prime", NowDateTime.ToString(), "Prime", NowDateTime.ToString());
                            fileDWriter.WriteLine(builder.ToString());
                            File_FinalAddNum++;
                        }
                        Tempdr.Close();
                    }

                    fileDWriter.Close();

                    p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.RelationShip in " + BCPFile_Relationship + "  -f " + BCPFmt_Relationship + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();
                }

                //201904 update status where not exists
                string sqlDel = @" update OFAC..Relationship set Status = 4 , LastModify=SYSDATETIME()
                from OFAC..Relationship (nolock) rela
                where EntNum in (select EntNum from OFAC..temprelationship)
                and not exists 
                (select * from OFAC..temprelationship (nolock) trela where rela.EntNum = trela.EntNum and trela.RelatedID = rela.RelatedID) ";
                dbCommandDel = db.GetSqlStringCommand(sqlDel);
                dbCommandDel.CommandTimeout = 0;
                int icount = db.ExecuteReader(dbCommandDel).RecordsAffected;
                LogAdd("[Daily] update Relationship set status = 4 where not exists:" + icount);

                LogAdd("[Daily] TempRelationship - Import: [" + File_TotalNum + "]; Relationship - Set Status to 4: [" + File_UpdDelNum + "], Set Status to 1: [" + File_UpdAddNum + "], Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[Daily] Expand to Relationship -- Completion");

                OFACEventLog("Relationship total update : " + (File_UpdDelNum + File_UpdAddNum).ToString());
                OFACEventLog("Relationship total insert : " + File_FinalAddNum.ToString());

            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred. Type: {0} Message: {1}", ex.GetType().FullName, ex.Message);
            }
        }

        public void UpdSDNAltTableDailyDataIntoSqlServer(string BCPFile_TmpSDNAltTable, string BCPFile_SDNAltTable, string BCPFmt_TmpSDNAltTable,
            string BCPFile_Error, string BCPFmt_SDNAltTable, string BCPServer, string BCPUser, string BCPPwd, string wc_subcate_space, string wc_import_un, string wc_import_SQL,
            string wc_import_crime, string wc_import_crime_SQL)
        {
            try
            {
                LogAdd("[Daily] Expand to SDNAltTable -- Start");

                string query = string.Empty;
                string queryLog1 = string.Empty;
                string queryLog2 = string.Empty;
                string LogContent1 = string.Empty;
                string LogContent2 = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;
                string Aliases = string.Empty;
                string AltNum = string.Empty;
                string EntNum = string.Empty;
                string AltType = string.Empty;
                string AltName = string.Empty;
                string UID = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");

                int AltNumMax = 0;
                int File_TotalNum = 0;
                int File_UpdDelNum = 0;
                int File_UpdAddNum = 0;
                int File_FinalAddNum = 0;
                int rowCount = 0;

                DateTime NowDateTime = DateTime.Now;

                Database db;

                if (File.Exists(BCPFile_TmpSDNAltTable)) File.Delete(BCPFile_TmpSDNAltTable);

                StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TmpSDNAltTable, false, Encoding.GetEncoding(28591));

                if (File.Exists(BCPFile_SDNAltTable)) File.Delete(BCPFile_SDNAltTable);

                StreamWriter fileDWriter = new StreamWriter(BCPFile_SDNAltTable, false, Encoding.GetEncoding(28591));

                queryMax = string.Format(@" SELECT MAX(AltNum)+1 AS AltNumMax FROM OFAC.dbo.SDNAltTable ");

                //query = string.Format(@" SELECT Entnum, Aliases FROM OFAC.dbo.WorldCheck WHERE Aliases IS NOT NULL AND Updated > DATEADD(Daily,-1,GETDATE()) ");

                //2019.01 change to table config
                query = @" SELECT WC.Entnum, WC.Aliases, 
                                        CASE ISNULL(WC.DelFlag, '0') 
                                        WHEN '0' THEN '1' 
                                        WHEN '1' THEN '4' END AS Status 
                                        FROM OFAC.dbo.WorldCheck WC
                                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                                        WHERE WC.Aliases != ''  
                                        AND WC.UPDDate > DATEADD(DAY, -1, GETDATE()) 
                                        AND (SubCategory is not null and SubCategory != '') 
                                union
                                        SELECT WC.Entnum, WC.Aliases, 
                                        CASE ISNULL(WC.DelFlag, '0') 
                                        WHEN '0' THEN '1' 
                                        WHEN '1' THEN '4' END AS Status 
                                        FROM OFAC.dbo.WorldCheck WC
                                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                                        WHERE WC.Aliases != ''  
                                        AND WC.UPDDate > DATEADD(DAY, -1, GETDATE()) 
                                         AND (SubCategory is null or SubCategory = '')
                                         AND (wcl.Used = 1 ";
                if (wc_import_crime == "Y")
                {
                    query += @"          or Category like '%CRIME%'";
                }
                query += ")";
                /*
                query += "AND ((SubCategory is not null ";

                if (wc_subcate_space == "N")
                {
                    query += "and SubCategory != '' ";
                }

                query += ") Or (keywords like '%USTREAS.311%') Or (furtherinfo like '%denied persons list%' and furtherinfo like '%US DEPARTMENT OF COMMERCE%') ";

                if (wc_import_un == "Y")
                {
                    query += wc_import_SQL;
                }
                if (wc_import_crime == "Y")
                {
                    query += wc_import_crime_SQL;
                }

                //20181022. for parameter
                for (int i = 0; i < getWC_para.Count(); i++)
                {
                    if (getWC_para[i].WCSQL != "" && getWC_para[i].WCListtype != "" && getWC_para[i].WCProgram != "")
                    {
                        query += getWC_para[i].WCSQL;
                    }
                }

                query += " );";
                */
                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 0;
                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("AltNumMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            AltNumMax = Convert.ToInt32(dataReaderMax["AltNumMax"]);
                        }
                    }
                }
                IDataReader dataReaderDT = db.ExecuteReader(dbCommand);
                DataTable dt = new DataTable();
                dt.Load(dataReaderDT);
                rowCount = dt.Rows.Count;
                dataReaderDT.Close();
                using (IDataReader dr = db.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        EntNum = dr["Entnum"].ToString().Trim();
                        Status = dr["Status"].ToString().Trim();
                        string[] AliasesSplit = dr["Aliases"].ToString().Trim().Split(';');
                        int i;
                        for (i = 0; i < AliasesSplit.Length; i++)
                        {
                            if (AliasesSplit[i] != "")
                            {
                                StringBuilder builder = new StringBuilder();
                                builder.AppendFormat("{0}\t", EntNum);
                                builder.AppendFormat("{0}\t", AltNumMax.ToString());
                                builder.AppendFormat("{0}\t", AliasesSplit[i]);
                                builder.AppendFormat("{0}", Status);
                                TempfileDWriter.WriteLine(builder.ToString());
                                AltNumMax++;
                                File_TotalNum++;
                            }
                        }
                    }
                    dr.Close();
                }
                TempfileDWriter.Close();

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TmpSDNAltTable ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                if (rowCount != 0)
                {
                    Process p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.TmpSDNAltTable in " + BCPFile_TmpSDNAltTable + "  -f " + BCPFmt_TmpSDNAltTable + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();

                    /* 20171016 danny updated : do not update the alternate name's status
                     * because HK hit lot of swift reserved word on alternate name.
                    string queryUpdateDelDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.SDNAltTable
                                 WHERE EXISTS (SELECT * FROM OFAC.dbo.TmpSDNAltTable WHERE SDNAltTable.EntNum = TmpSDNAltTable.EntNum AND SDNAltTable.AltName = TmpSDNAltTable.AltName AND TmpSDNAltTable.Status='4')
 AND SDNAltTable.AltType='WPEP' ");

                    DbCommand dbCommandUpdateDelDetial = db.GetSqlStringCommand(queryUpdateDelDetail);
                    dbCommandUpdateDelDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateDelDetail = db.ExecuteReader(dbCommandUpdateDelDetial))
                    {
                        while (drUpdateDelDetail.Read())
                        {
                            //if (File_UpdDelNum == 0) { LogAdd("[Daily] SDNAltTable - Set Status to 4: "); };
                            //LogAdd(
                            //    drUpdateDelDetail["EntNum"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["AltNum"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["AltType"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["AltName"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["FirstName"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["MiddleName"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastName"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListID"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["Remarks"].ToString().Trim() + ", " +
                            //    drUpdateDelDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListCreateDate"].ToString().Trim() + ", " +
                            //    drUpdateDelDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastModifDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastOper"].ToString().Trim());
                            File_UpdDelNum++;
                        }
                    }

                    string queryUpdateDel =
                        string.Format(@"  UPDATE OFAC.dbo.SDNAltTable
 SET Status='4', LastModifDate=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TmpSDNAltTable WHERE SDNAltTable.EntNum = TmpSDNAltTable.EntNum AND SDNAltTable.AltName = TmpSDNAltTable.AltName AND TmpSDNAltTable.Status='4')
 AND SDNAltTable.AltType='WPEP' ");

                    DbCommand dbCommandUpdateDel = db.GetSqlStringCommand(queryUpdateDel);
                    dbCommandUpdateDel.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateDel);

                    string queryUpdateAddDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.SDNAltTable
                                 WHERE EXISTS (SELECT * FROM OFAC.dbo.TmpSDNAltTable WHERE SDNAltTable.EntNum = TmpSDNAltTable.EntNum AND SDNAltTable.AltName = TmpSDNAltTable.AltName AND TmpSDNAltTable.Status='1')
 AND SDNAltTable.AltType='WPEP' ");

                    DbCommand dbCommandUpdateAddDetial = db.GetSqlStringCommand(queryUpdateAddDetail);
                    dbCommandUpdateAddDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateAddDetail = db.ExecuteReader(dbCommandUpdateAddDetial))
                    {
                        while (drUpdateAddDetail.Read())
                        {
                            //if (File_UpdAddNum == 0) { LogAdd("[Daily] SDNAltTable - Set Status to 1: "); };
                            //LogAdd(
                            //    drUpdateAddDetail["EntNum"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["AltNum"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["AltType"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["AltName"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["FirstName"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["MiddleName"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastName"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListID"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["Remarks"].ToString().Trim() + ", " +
                            //    drUpdateAddDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListCreateDate"].ToString().Trim() + ", " +
                            //    drUpdateAddDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastModifDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastOper"].ToString().Trim());
                            File_UpdAddNum++;
                        }
                    }

                    string queryUpdateAdd =
                        string.Format(@" UPDATE OFAC.dbo.SDNAltTable
 SET Status='1', LastModifDate=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TmpSDNAltTable WHERE SDNAltTable.EntNum = TmpSDNAltTable.EntNum AND SDNAltTable.AltName = TmpSDNAltTable.AltName AND TmpSDNAltTable.Status='1')
 AND SDNAltTable.AltType='WPEP' ");

                    DbCommand dbCommandUpdateAdd = db.GetSqlStringCommand(queryUpdateAdd);
                    dbCommandUpdateAdd.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateAdd);
                    */
                    string queryAdd =
                        string.Format(@" SELECT DISTINCT EntNum, AltNum, AltName, Status FROM OFAC.dbo.TmpSDNAltTable 
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.SDNAltTable WHERE TmpSDNAltTable.EntNum = EntNum AND TmpSDNAltTable.AltName = AltName) 
                                     ORDER BY EntNum, AltNum, AltName ");

                    DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                    dbCommandAdd.CommandTimeout = 0;
                    using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                    {
                        while (Tempdr.Read())
                        {
                            EntNum = Tempdr["EntNum"].ToString().Trim();
                            AltNum = Tempdr["AltNum"].ToString().Trim();
                            AltName = Tempdr["AltName"].ToString().Trim();
                            Status = Tempdr["Status"].ToString().Trim();
                            StringBuilder builder = new StringBuilder();
                            builder.AppendFormat("{0}\t", EntNum);
                            builder.AppendFormat("{0}\t", AltNum);
                            builder.AppendFormat("{0}\t", "WPEP");
                            builder.AppendFormat("{0}\t", AltName);
                            builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}", "", "", "", "", "", "0", Status, NowDateTime.ToString(), NowDateTime.ToString(), NowDateTime.ToString(), NowDateTime.ToString(), "Prime");
                            fileDWriter.WriteLine(builder.ToString());
                            File_FinalAddNum++;
                        }
                        Tempdr.Close();
                    }

                    fileDWriter.Close();

                    p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.SDNAltTable in " + BCPFile_SDNAltTable + "  -f " + BCPFmt_SDNAltTable + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    if (File.Exists(BCPFile_TmpSDNAltTable)) File.Delete(BCPFile_TmpSDNAltTable);
                    p.Start();
                    p.WaitForExit();
                }

                //201904 update status where not exists
                string sqlDel = @" update SDNAltTable set Status = 4 ,LastModifDate=SYSDATETIME()
                from OFAC..SDNAltTable (nolock) sdna
                where EntNum in (select EntNum from OFAC..TmpSDNAltTable)
                and not exists 
                (select * from OFAC..TmpSDNAltTable (nolock) tsdna where sdna.EntNum = tsdna.EntNum and tsdna.AltName = sdna.AltName) ";
                dbCommandDel = db.GetSqlStringCommand(sqlDel);
                dbCommandDel.CommandTimeout = 0;
                int icount = db.ExecuteReader(dbCommandDel).RecordsAffected;
                LogAdd("[Daily] update SDNAltTable set status = 4 where not exists:" + icount);

                LogAdd("[Daily] TmpSDNAltTable - Import: [" + File_TotalNum + "]; SDNAltTable - Set Status to 4: [" + File_UpdDelNum + "], Set Status to 1: [" + File_UpdAddNum + "], Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[Daily] Expand to SDNAltTable -- Completion");

                OFACEventLog("SDNAltTable total update : " + (File_UpdDelNum + File_UpdAddNum).ToString());
                OFACEventLog("SDNAltTable total insert : " + File_FinalAddNum.ToString());

                //2018.07 alt type fix
                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    //修正WC修改過catory 或subcatory等欄位，導致program = pep的名單program = Other
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update SDN program = other --  Start.");

                        sqlCommand.CommandText = @"
                          update OFAC.dbo.SDNTable set listtype = 'WORLD-CHECK', program = 'OTHER' where EntNum > 20000000 and Program in ('PEP','') and Status != 4;
                          ";
                        sqlCommand.CommandTimeout = 500;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Update:{0}", UpdateCount);
                        LogAdd("[Daily] Update SDN program = other --  Completed. Count : " + UpdateCount);
                        OFACEventLog("Update SDN program = other totoal updated:" + UpdateCount);
                    }

                    //更新SDNAltTable AltType
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update SDNAltTable AltType --  Start.");

                        sqlCommand.CommandText = @"
                          update OFAC.dbo.SDNAltTable set AltType = substring(c.Program,0,9) from OFAC.dbo.WorldCheck(nolock) wc
                            inner join OFAC.dbo.SDNAltTable(nolock) b on wc.Entnum = b.EntNum
                            inner join OFAC.dbo.SDNTable(nolock) c on wc.Entnum = c.EntNum
                            where isnull(wc.DelFlag,0) = 0 
                            and c.Status != 4
                            and substring(c.Program,0,9) != b.AltType;
                          ";
                        sqlCommand.CommandTimeout = 500;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Update:{0}", UpdateCount);
                        LogAdd("[Daily] Update SDNAltTable AltType --  Completed. Count : " + UpdateCount);
                        OFACEventLog("SDNALT_AltType totoal update:" + UpdateCount);
                    }
                }

            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred. Type: {0} Message: {1}", ex.GetType().FullName, ex.Message);
            }
        }

        public void UpdURLsDailyDataIntoSqlServer(string BCPFile_TempURLs, string BCPFile_URLs, string BCPFmt_TempURLs, string BCPFile_Error,
            string BCPFmt_URLs, string BCPServer, string BCPUser, string BCPPwd, string wc_subcate_space, string wc_import_un, string wc_import_SQL,
            string wc_import_crime, string wc_import_crime_SQL)
        {
            try
            {
                LogAdd("[Daily] Expand to URLs -- Start");

                string query = string.Empty;
                string queryLog1 = string.Empty;
                string queryLog2 = string.Empty;
                string LogContent1 = string.Empty;
                string LogContent2 = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;
                string Aliases = string.Empty;
                string URLsID = string.Empty;
                string EntNum = string.Empty;
                string AltType = string.Empty;
                string URL = string.Empty;
                string Description = string.Empty;
                string UID = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");

                int URLsIDMax = 0;
                int File_TotalNum = 0;
                int File_UpdDelNum = 0;
                int File_UpdAddNum = 0;
                int File_FinalAddNum = 0;
                int rowCount = 0;

                DateTime NowDateTime = DateTime.Now;

                Database db;

                if (File.Exists(BCPFile_TempURLs)) File.Delete(BCPFile_TempURLs);

                StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TempURLs, false, Encoding.GetEncoding(28591));

                if (File.Exists(BCPFile_URLs)) File.Delete(BCPFile_URLs);

                StreamWriter fileDWriter = new StreamWriter(BCPFile_URLs, false, Encoding.GetEncoding(28591));

                queryMax = string.Format(@" SELECT MAX(URLsID)+1 AS URLsIDMax FROM OFAC.dbo.URLs ");

                //query = string.Format(@" SELECT Entnum, ExSources FROM OFAC.dbo.WorldCheck WHERE ExSources IS NOT NULL AND Updated > DATEADD(Daily,-1,GETDATE()) ");

                //2019.01 change to table config
                query = @" SELECT WC.Entnum, WC.ExSources, 
                                        CASE ISNULL(WC.DelFlag, '0') 
                                        WHEN '0' THEN '1' 
                                        WHEN '1' THEN '4' END AS Status 
                                        FROM OFAC.dbo.WorldCheck WC
                                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                                        WHERE WC.ExSources != ''
                                        AND WC.UPDDate > DATEADD(DAY, -1, GETDATE()) 
                                        AND (SubCategory is not null and SubCategory != '') 
                                union
                                        SELECT WC.Entnum, WC.ExSources, 
                                        CASE ISNULL(WC.DelFlag, '0') 
                                        WHEN '0' THEN '1' 
                                        WHEN '1' THEN '4' END AS Status 
                                        FROM OFAC.dbo.WorldCheck WC
                                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                                        WHERE WC.ExSources != ''
                                        AND WC.UPDDate > DATEADD(DAY, -1, GETDATE()) 
                                        AND (SubCategory is null or SubCategory = '')
                                         AND (wcl.Used = 1 ";
                if (wc_import_crime == "Y")
                {
                    query += @"          or Category like '%CRIME%'";
                }
                query += ")";

                /*
                query += "AND ((SubCategory is not null ";

                if (wc_subcate_space == "N")
                {
                    query += "and SubCategory != '' ";
                }

                query += ") Or (keywords like '%USTREAS.311%') Or (furtherinfo like '%denied persons list%' and furtherinfo like '%US DEPARTMENT OF COMMERCE%') ";

                if (wc_import_un == "Y")
                {
                    query += wc_import_SQL;
                }
                if (wc_import_crime == "Y")
                {
                    query += wc_import_crime_SQL;
                }

                //20181022. for parameter
                for (int i = 0; i < getWC_para.Count(); i++)
                {
                    if (getWC_para[i].WCSQL != "" && getWC_para[i].WCListtype != "" && getWC_para[i].WCProgram != "")
                    {
                        query += getWC_para[i].WCSQL;
                    }
                }

                query += " );";
                */

                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 0;
                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("URLsIDMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            URLsIDMax = Convert.ToInt32(dataReaderMax["URLsIDMax"]);
                        }
                    }
                }
                IDataReader dataReaderDT = db.ExecuteReader(dbCommand);
                DataTable dt = new DataTable();
                dt.Load(dataReaderDT);
                rowCount = dt.Rows.Count;
                dataReaderDT.Close();
                using (IDataReader dr = db.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        EntNum = dr["Entnum"].ToString().Trim();
                        Status = dr["Status"].ToString().Trim();
                        string[] ExSourcesSplit = dr["ExSources"].ToString().Trim().Split(' ');
                        int i;
                        for (i = 0; i < ExSourcesSplit.Length; i++)
                        {
                            if (ExSourcesSplit[i] != "")
                            {
                                StringBuilder builder = new StringBuilder();
                                builder.AppendFormat("{0}\t", URLsIDMax.ToString());
                                builder.AppendFormat("{0}\t", EntNum);
                                builder.AppendFormat("{0}\t", ExSourcesSplit[i]);
                                builder.AppendFormat("{0}", Status);
                                TempfileDWriter.WriteLine(builder.ToString());
                                URLsIDMax++;
                                File_TotalNum++;
                            }
                        }
                    }
                    dr.Close();
                }
                TempfileDWriter.Close();

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TempURLs ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                if (rowCount != 0)
                {
                    Process p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.TempURLs in " + BCPFile_TempURLs + "  -f " + BCPFmt_TempURLs + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();

                    string queryUpdateDelDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.URLs
                                 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempURLs WHERE URLs.EntNum = TempURLs.EntNum AND URLs.URL = TempURLs.URL AND TempURLs.Status='4')
");

                    DbCommand dbCommandUpdateDelDetial = db.GetSqlStringCommand(queryUpdateDelDetail);
                    dbCommandUpdateDelDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateDelDetail = db.ExecuteReader(dbCommandUpdateDelDetial))
                    {
                        while (drUpdateDelDetail.Read())
                        {
                            //if (File_UpdDelNum == 0) { LogAdd("[Daily] URLs - Set Status to 4: "); };
                            //LogAdd(
                            //    drUpdateDelDetail["URLsID"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["EntNUm"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["URL"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["Description"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListCreateDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateOper"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateDate"].ToString().Trim() + ", " +
                            //    drUpdateDelDetail["LastOper"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastModify"].ToString().Trim());
                            File_UpdDelNum++;
                        }
                    }

                    string queryUpdateDel =
                        string.Format(@" UPDATE OFAC.dbo.URLs
 SET Status='4', LastModify=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempURLs WHERE URLs.EntNum = TempURLs.EntNum AND URLs.URL = TempURLs.URL AND TempURLs.Status='4')
");

                    DbCommand dbCommandUpdateDel = db.GetSqlStringCommand(queryUpdateDel);
                    dbCommandUpdateDel.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateDel);

                    string queryUpdateAddDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.URLs
                                  WHERE EXISTS (SELECT * FROM OFAC.dbo.TempURLs WHERE URLs.EntNum = TempURLs.EntNum AND URLs.URL = TempURLs.URL AND TempURLs.Status='1') ");

                    DbCommand dbCommandUpdateAddDetial = db.GetSqlStringCommand(queryUpdateAddDetail);
                    dbCommandUpdateAddDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateAddDetail = db.ExecuteReader(dbCommandUpdateAddDetial))
                    {
                        while (drUpdateAddDetail.Read())
                        {
                            //if (File_UpdAddNum == 0) { LogAdd("[Daily] URLs - Set Status to 1: "); };
                            //LogAdd(
                            //    drUpdateAddDetail["URLsID"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["EntNUm"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["URL"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["Description"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListCreateDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateOper"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateDate"].ToString().Trim() + ", " +
                            //    drUpdateAddDetail["LastOper"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastModify"].ToString().Trim());
                            File_UpdAddNum++;
                        }
                    }

                    string queryUpdateAdd =
                        string.Format(@" UPDATE OFAC.dbo.URLs
 SET Status='1', LastModify=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempURLs WHERE URLs.EntNum = TempURLs.EntNum AND URLs.URL = TempURLs.URL AND TempURLs.Status='1')
 ");

                    DbCommand dbCommandUpdateAdd = db.GetSqlStringCommand(queryUpdateAdd);
                    dbCommandUpdateAdd.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateAdd);

                    string queryAdd =
                        string.Format(@" SELECT DISTINCT URLsID, EntNum, URL, Status FROM OFAC.dbo.TempURLs 
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.URLs WHERE TempURLs.EntNum = EntNum AND TempURLs.URL = URL) 
                                     ORDER BY URLsID, EntNum, URL ");

                    DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                    dbCommandAdd.CommandTimeout = 0;
                    using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                    {
                        while (Tempdr.Read())
                        {
                            URLsID = Tempdr["URLsID"].ToString().Trim();
                            EntNum = Tempdr["EntNum"].ToString().Trim();
                            URL = Tempdr["URL"].ToString().Trim();
                            Description = Tempdr["URL"].ToString().Trim();
                            Status = Tempdr["Status"].ToString().Trim();
                            StringBuilder builder = new StringBuilder();
                            builder.AppendFormat("{0}\t", URLsID);
                            builder.AppendFormat("{0}\t", EntNum);
                            builder.AppendFormat("{0}\t", URL);
                            builder.AppendFormat("{0}\t", Description);
                            builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}", Status, NowDateTime.ToString(), NowDateTime.ToString(), "Prime", NowDateTime.ToString(), "Prime", NowDateTime.ToString());
                            fileDWriter.WriteLine(builder.ToString());
                            File_FinalAddNum++;
                        }
                        Tempdr.Close();
                    }

                    fileDWriter.Close();

                    p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.URLs in " + BCPFile_URLs + "  -f " + BCPFmt_URLs + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();
                }

                //201904 update status where not exists
                string sqlDel = @" update OFAC..URLs set Status = 4, LastModify=SYSDATETIME()
                from OFAC..URLs (nolock) url
                where EntNum in (select EntNum from OFAC..tempURLs)
                and not exists 
                (select * from OFAC..tempURLs (nolock) turl where url.EntNum = turl.EntNum and turl.[url] = url.[url]) ";
                dbCommandDel = db.GetSqlStringCommand(sqlDel);
                dbCommandDel.CommandTimeout = 0;
                int icount = db.ExecuteReader(dbCommandDel).RecordsAffected;
                LogAdd("[Daily] update URLs set status = 4 where not exists:" + icount);

                LogAdd("[Daily] TempURLs - Import: [" + File_TotalNum + "]; URLs - Set Status to 4: [" + File_UpdDelNum + "], Set Status to 1: [" + File_UpdAddNum + "], Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[Daily] Expand to URLs -- Completion");

                OFACEventLog("URLs total update : " + (File_UpdDelNum + File_UpdAddNum).ToString());
                OFACEventLog("URLs total insert : " + File_FinalAddNum.ToString());
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred. Type: {0} Message: {1}", ex.GetType().FullName, ex.Message);
            }
        }

        public void insertDobs(string str_UPDDate, string wc_subcate_space, string wc_import_un, string wc_import_SQL, string wc_import_crime, string wc_import_crime_SQL)
        {
            // update sdn data
            var dt = new DateTime();
            dt = DateTime.Now;
            int iDOBsId = 0;
            string query = "";
            SqlDataAdapter da;
            DataSet ds;

            if (string.IsNullOrEmpty(str_UPDDate))
            {
                str_UPDDate = "30";
            }

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                //清除暫存Dob
                using (var sqlCommand = new SqlCommand("truncate table TempLoadDOBs", sqlConnection))
                {
                    sqlCommand.ExecuteNonQuery();
                }

                //get 目前最大流水號
                using (var sqlCommand = new SqlCommand("select max(DOBsId) DOBsId from DOBs", sqlConnection))
                {
                    da = new SqlDataAdapter(sqlCommand);
                    ds = new DataSet();

                    da.Fill(ds);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            if (r["DOBsId"].ToString().Trim().Length > 0)
                            {
                                iDOBsId = int.Parse(r["DOBsId"].ToString().Trim());
                            }
                        }
                    }
                }

                //insert tempdob
                //2018.07 fix ent must exist in sdntable
                //2019.01 change to table config
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    query = @"
                          select  CASE ISNULL(WC.DelFlag, '0') 
                           WHEN '0' THEN '1' 
                           WHEN '1' THEN '4' END AS delstatus,
                          wc.*
                            from worldcheck wc
                            left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                            left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                          where wc.DOB != ''
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and exists (select * from OFAC.dbo.SDNTABLE(nolock) where wc.entnum = OFAC.dbo.SDNTABLE.entnum )
                            AND (SubCategory is not null and SubCategory != '') 
                    union
                          select  CASE ISNULL(WC.DelFlag, '0') 
                           WHEN '0' THEN '1' 
                           WHEN '1' THEN '4' END AS delstatus,
                          wc.*
                            from worldcheck wc
                            left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                            left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                          where wc.DOB != ''
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and exists (select * from OFAC.dbo.SDNTABLE(nolock) where wc.entnum = OFAC.dbo.SDNTABLE.entnum )
                          and (SubCategory is null or SubCategory = '')
                                         AND (wcl.Used = 1 ";
                    if (wc_import_crime == "Y")
                    {
                        query += @"          or Category like '%CRIME%'";
                    }
                    query += ")";

                    /*
                    query += "AND ((SubCategory is not null ";

                    if (wc_subcate_space == "N")
                    {
                        query += "and SubCategory != '' ";
                    }

                    query += ") Or (keywords like '%USTREAS.311%') Or (furtherinfo like '%denied persons list%' and furtherinfo like '%US DEPARTMENT OF COMMERCE%') ";

                    if (wc_import_un == "Y")
                    {
                        query += wc_import_SQL;
                    }
                    if (wc_import_crime == "Y")
                    {
                        query += wc_import_crime_SQL;
                    }

                    //20181022. for parameter
                    for (int i = 0; i < getWC_para.Count(); i++)
                    {
                        if (getWC_para[i].WCSQL != "" && getWC_para[i].WCListtype != "" && getWC_para[i].WCProgram != "")
                        {
                            query += getWC_para[i].WCSQL;
                        }
                    }

                    query += " );";
                    */

                    sqlCommand.CommandText = query;
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 300;

                    da = new SqlDataAdapter(sqlCommand);
                    ds = new DataSet();

                    int rownum = 1;
                    da.Fill(ds);

                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            string entnum = r["entnum"].ToString().Trim();
                            string dob = r["dob"].ToString().Trim();
                            string Entered = r["Entered"].ToString().Trim();
                            string Updated = r["Updated"].ToString().Trim();
                            string delstatus = r["delstatus"].ToString().Trim();

                            sqlCommand.CommandText = @"
                             insert into TempLoadDOBs(DOBsId,EntNum,dob,Status,ListCreateDate,ListModifDate,CreateOper,CreateDate,LastOper,LastModify)
                             select top 1 @dobid,@Entnum,@dob,@delstatus, @Entered,@Updated,'Prime',@date,'Prime',@date
                             from DOBs;
                            ";

                            sqlCommand.Parameters.Clear();
                            sqlCommand.Parameters.AddWithValue("@dobid", iDOBsId + rownum);
                            sqlCommand.Parameters.AddWithValue("@Entnum", entnum);
                            sqlCommand.Parameters.AddWithValue("@dob", dob.Substring(5, 2) + dob.Substring(8, 2) + dob.Substring(0, 4));
                            sqlCommand.Parameters.AddWithValue("@delstatus", delstatus);
                            sqlCommand.Parameters.AddWithValue("@Entered", Entered);
                            sqlCommand.Parameters.AddWithValue("@Updated", Updated);
                            sqlCommand.Parameters.AddWithValue("@date", dt);

                            sqlCommand.ExecuteNonQuery();

                            rownum = rownum + 1;
                        }
                    }
                }

                //存在DOBS 更新狀態=1
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    update DOBs set status = '1' where entnum in (select entnum from TempLoadDOBs where status = '1');";
                    sqlCommand.CommandTimeout = 300;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Update Dob=1:{0}", UpdateCount);
                    OFACEventLog("DOBs daily Update status = 1:" + UpdateCount);
                }

                //存在DOBS 更新狀態=4
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    update DOBs set status = '4' where entnum in (select entnum from TempLoadDOBs where status = '4');";
                    sqlCommand.CommandTimeout = 300;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Update Dob=4:{0}", UpdateCount);
                    OFACEventLog("DOBs daily Update status = 4:" + UpdateCount);
                }

                //不存在DOB insert
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    insert into DOBs(DOBsId,EntNum,dob,Status,ListCreateDate,ListModifDate,CreateOper,CreateDate,LastOper,LastModify)
                    select DOBsId,EntNum,dob,Status,ListCreateDate,ListModifDate,CreateOper,CreateDate,LastOper,LastModify
                      from TempLoadDOBs where not exists (select * from dobs where dobs.EntNum = TempLoadDOBs.EntNum)
                    ;";
                    sqlCommand.CommandTimeout = 300;
                    int InsertCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Insert:{0}", InsertCount);
                    OFACEventLog("DOBs daily totoal Insert:" + InsertCount);

                }
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        public void InsertAddr(string str_UPDDate, string wc_import_crime)
        {
            // update sdn data
            var dt = new DateTime();
            dt = DateTime.Now;
            int iaddrnum = 0;
            string query = "";
            SqlDataAdapter da;
            DataSet ds;

            if (string.IsNullOrEmpty(str_UPDDate))
            {
                str_UPDDate = "30";
            }

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                //清除暫存Dob
                using (var sqlCommand = new SqlCommand("truncate table OFAC..TmpAddrTable", sqlConnection))
                {
                    sqlCommand.ExecuteNonQuery();
                }

                //get 目前最大流水號
                using (var sqlCommand = new SqlCommand("select max(AddrNum) AddrNum from OFAC..SDNAddrTable", sqlConnection))
                {
                    da = new SqlDataAdapter(sqlCommand);
                    ds = new DataSet();

                    da.Fill(ds);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            if (r["AddrNum"].ToString().Trim().Length > 0)
                            {
                                iaddrnum = int.Parse(r["AddrNum"].ToString().Trim());
                            }
                        }
                    }
                }

                //2018.07 fix ent must exist in sdntable
                //2019.01 change to table config
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    query = @"
                          select distinct CASE ISNULL(WC.DelFlag, '0') 
                           WHEN '0' THEN '1' 
                           WHEN '1' THEN '4' END AS delstatus,
                           (select Customer_country from OFAC..WorldCheckCountry where worldcheck_country = wc.country) customer_country,
                           (select CreateDate from SDNTable (nolock) where SDNTable.EntNum = wc.Entnum) as CreateDate,
                           (select LastModifDate from SDNTable (nolock) where SDNTable.EntNum = wc.Entnum) as LastModifDate,
                          wc.*
                            from worldcheck wc
                            left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                            left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                          where wc.Country != ''
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and exists (select * from OFAC.dbo.SDNTABLE(nolock) where wc.entnum = OFAC.dbo.SDNTABLE.entnum )
                            AND (SubCategory is not null and SubCategory != '') 
                    union
                          select distinct CASE ISNULL(WC.DelFlag, '0') 
                           WHEN '0' THEN '1' 
                           WHEN '1' THEN '4' END AS delstatus,
                           (select Customer_country from OFAC..WorldCheckCountry where worldcheck_country = wc.country) customer_country,
                           (select CreateDate from SDNTable (nolock) where SDNTable.EntNum = wc.Entnum) as CreateDate,
                           (select LastModifDate from SDNTable (nolock) where SDNTable.EntNum = wc.Entnum) as LastModifDate,
                          wc.*
                            from worldcheck wc
                            left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                            left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                          where wc.Country != ''
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and exists (select * from OFAC.dbo.SDNTABLE(nolock) where wc.entnum = OFAC.dbo.SDNTABLE.entnum )
                          and (SubCategory is null or SubCategory = '')
                                         AND (wcl.Used = 1 ";
                    if (wc_import_crime == "Y")
                    {
                        query += @"          or Category like '%CRIME%'";
                    }
                    query += ")";

                    sqlCommand.CommandText = query;
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 300;

                    da = new SqlDataAdapter(sqlCommand);
                    ds = new DataSet();

                    int rownum = 1;
                    da.Fill(ds);

                    LogAdd(string.Format("[Daily] SDNAddrTable - number count:{0}", ds.Tables[0].Rows.Count));

                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            string entnum = r["entnum"].ToString().Trim();
                            string country = r["customer_country"].ToString().Trim();
                            string Entered = r["Entered"].ToString().Trim();
                            string Updated = r["Updated"].ToString().Trim();
                            string delstatus = r["delstatus"].ToString().Trim();
                            string location = r["location"].ToString().Trim();
                            string createdate = r["CreateDate"].ToString().Trim();
                            string modifdate = r["LastModifDate"].ToString().Trim();
                            string address = String.Empty;
                            string address2 = String.Empty;
                            string address3 = String.Empty;
                            string address4 = String.Empty;
                            string city = String.Empty;
                            string state = String.Empty;

                            //modify because tempsdnaddrtable schema different from sdnaddrtable
                            sqlCommand.CommandText = @"
                             insert into OFAC..TmpAddrTable(AddrNum,EntNum,Address,Address2,Address3,Address4,City,State,Country,deleted,status,ListCreatedate,ListModifDate,CreateDate,LastModifDate,LastOper)
                             select top 1 @AddrNum,@Entnum,@Address,@Address2,@Address3,@Address4,@City,@State,(select Customer_Country from WorldCheckCountry where WorldCheck_Country = @Country and @Country != ''),
                             0,@delstatus, @Entered,@Updated,@createdate,@modifdate,'Prime'
                             from OFAC..SDNAddrTable;
                            ";

                            //splite by ; for number count
                            String[] A_location_count = location.Split(';');
                            LogAdd("addr_ent=" + entnum);
                            for (int i = 0; i < A_location_count.Count(); i++)
                            {
                                //splite by ~ for address city,state country
                                String[] A_location_type = A_location_count[i].Split('~');
                                String[] A_location_city_state = new String[2];
                                for (int j = 0; j < A_location_type.Count(); j++)
                                {
                                    switch (j)
                                    {
                                        case 0:
                                            address4 = A_location_type[j].Trim().Length > 200 ? A_location_type[j].Trim().Substring(200, 50) : "";
                                            LogAdd("address4=" + address4);
                                            address3 = A_location_type[j].Trim().Length > 150 ? A_location_type[j].Trim().Substring(150, 50) : "";
                                            LogAdd("address3=" + address3);
                                            address2 = A_location_type[j].Trim().Length > 100 ? A_location_type[j].Trim().Substring(100, 50) : "";
                                            LogAdd("address2=" + address2);
                                            address = A_location_type[j].Trim().Length > 100 ? A_location_type[j].Trim().Substring(0, 100) : A_location_type[j].Trim();
                                            LogAdd("address=" + address);
                                            break;

                                        case 1:

                                            //splite by , for city state
                                            A_location_city_state = A_location_type[j].Split(',');
                                            city = A_location_city_state[0].Trim().Length > 50 ? A_location_city_state[0].Trim().Substring(0, 50) : A_location_city_state[0].Trim();
                                            state = A_location_city_state[1].Trim().Length > 50 ? A_location_city_state[1].Trim().Substring(0, 50) : A_location_city_state[1].Trim();

                                            break;
                                        case 2:
                                            country = A_location_type[j].Trim().Length > 100 ? A_location_type[j].Trim().Substring(0, 100) : A_location_type[j].Trim();
                                            break;
                                        default:

                                            break;
                                    }

                                }

                                if ((address.Trim() == "") && (city.Trim() == "") && (state.Trim() == "") && (country.Trim() == ""))
                                {
                                    continue;
                                }

                                sqlCommand.Parameters.Clear();
                                sqlCommand.Parameters.AddWithValue("@AddrNum", iaddrnum + rownum);
                                sqlCommand.Parameters.AddWithValue("@Entnum", entnum);
                                sqlCommand.Parameters.AddWithValue("@Address", address);
                                sqlCommand.Parameters.AddWithValue("@Address2", address2);
                                sqlCommand.Parameters.AddWithValue("@Address3", address3);
                                sqlCommand.Parameters.AddWithValue("@Address4", address4);
                                sqlCommand.Parameters.AddWithValue("@City", city);
                                sqlCommand.Parameters.AddWithValue("@State", state);
                                sqlCommand.Parameters.AddWithValue("@Country", country);
                                sqlCommand.Parameters.AddWithValue("@delstatus", delstatus);
                                sqlCommand.Parameters.AddWithValue("@Entered", Entered);
                                sqlCommand.Parameters.AddWithValue("@Updated", Updated);
                                sqlCommand.Parameters.AddWithValue("@createdate", createdate);
                                sqlCommand.Parameters.AddWithValue("@modifdate", modifdate);
                                sqlCommand.Parameters.AddWithValue("@date", dt);

                                sqlCommand.ExecuteNonQuery();

                                rownum = rownum + 1;
                            }
                        }
                    }
                }

                //存在Addr 更新狀態=1
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    update OFAC..SDNAddrTable set status = '1' where entnum in (select entnum from OFAC..TmpAddrTable where Status = 1) and status != 1;";
                    sqlCommand.CommandTimeout = 300;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Update Addr=1:{0}", UpdateCount);
                    OFACEventLog("Addr daily Update status = 1:" + UpdateCount);
                }

                //存在Addr 更新狀態=4
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    update OFAC..SDNAddrTable set status = '4' where entnum in (select entnum from OFAC..TmpAddrTable where Status = 4) and status != 4;";
                    sqlCommand.CommandTimeout = 300;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Update Addr=4:{0}", UpdateCount);
                    OFACEventLog("Addr daily Update status = 4:" + UpdateCount);
                }

                //不存在Addr insert
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    insert into OFAC..SDNAddrTable(AddrNum,EntNum,Address,Address2,Address3,Address4,City,State,Country,Deleted,Status,ListCreateDate,ListModifDate,CreateDate,LastModifDate,LastOper)
                    select AddrNum,EntNum,Address,Address2,Address3,Address4,City,State,Country,'0',Status,ListCreatedate,ListModifDate,CreateDate,LastModifDate,LastOper
                      from OFAC..TmpAddrTable where not exists 
                      (select * from OFAC..SDNAddrTable 
                        where OFAC..SDNAddrTable.EntNum = OFAC..TmpAddrTable.EntNum
                        and isnull(OFAC.dbo.SDNAddrTable.Address,'') = isnull(OFAC.dbo.TmpAddrTable.Address, '')
                        and isnull(OFAC.dbo.SDNAddrTable.City,'') = isnull(OFAC.dbo.TmpAddrTable.City,'')
                        and isnull(OFAC.dbo.SDNAddrTable.State,'') = isnull(OFAC.dbo.TmpAddrTable.State,'')
                        and isnull(OFAC.dbo.SDNAddrTable.Country,'') = isnull(OFAC.dbo.TmpAddrTable.Country,'')
                      )
                    ;";
                    sqlCommand.CommandTimeout = 300;
                    sqlCommand.Parameters.AddWithValue("@date", dt);

                    int InsertCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Insert:{0}", InsertCount);
                    OFACEventLog("Addr daily totoal Insert:" + InsertCount);
                    LogAdd(string.Format("[Daily] SDNAddrTable - Insert:{0}", InsertCount));
                }

                //201904 update status where not exists
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    string sqlDel = @" update OFAC..SDNAddrTable set Status = 4 , LastModifDate=SYSDATETIME()
                from OFAC..SDNAddrTable (nolock) sdnaddr
                where EntNum in (select EntNum from OFAC..TmpAddrTable)
                and not exists 
                (select * from OFAC..TmpAddrTable (nolock) tsdnaddr where sdnaddr.EntNum = tsdnaddr.EntNum and sdnaddr.Address = tsdnaddr.Address
                    and sdnaddr.Address = tsdnaddr.Address
                    and sdnaddr.City = tsdnaddr.City
                    and sdnaddr.State = tsdnaddr.State
                    and sdnaddr.Country = tsdnaddr.Country
                ) ";

                    sqlCommand.CommandText = sqlDel;
                    sqlCommand.CommandTimeout = 300;

                    int InsertCount = sqlCommand.ExecuteNonQuery();
                    LogAdd("[Daily] update SDNAddrTable set status = 4 where not exists:" + InsertCount);
                }


                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        public void UpdateAttribute(string str_UPDDate, string wc_import_crime)
        {
            // update sdn Attribute
            var dt = new DateTime();
            dt = DateTime.Now;
            string NotesID = string.Empty;
            string EntNum = string.Empty;

            string Status = string.Empty;
            string ListCreateDate = string.Empty;
            string ListModifDate = string.Empty;
            string CreateDate = string.Empty;
            string LastOper = string.Empty;
            string LastModify = string.Empty;
            string Note = string.Empty;
            string NoteType = string.Empty;
            int NotesIdMax = 0;
            string query = "";
            SqlDataAdapter da;
            DataSet ds;

            if (string.IsNullOrEmpty(str_UPDDate))
            {
                str_UPDDate = "30";
            }

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                //清除暫存Notes
                using (var sqlCommand = new SqlCommand("truncate table OFAC..TempLoadNotes", sqlConnection))
                {
                    sqlCommand.ExecuteNonQuery();
                }

                //get 目前最大流水號
                using (var sqlCommand = new SqlCommand("SELECT MAX(NotesID)+1 AS NotesIdMax FROM OFAC.dbo.Notes;", sqlConnection))
                {
                    da = new SqlDataAdapter(sqlCommand);
                    ds = new DataSet();

                    da.Fill(ds);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            if (r["NotesIdMax"].ToString().Trim().Length > 0)
                            {
                                NotesIdMax = int.Parse(r["NotesIdMax"].ToString().Trim());
                            }
                        }
                    }
                }

                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    query = @"
                          select distinct ISNULL(wc.Company,'') Company,ISNULL(wc.Category,'') Category,ISNULL(wc.Country,'') Country,
                                     ISNULL(wc.Age,'') Age,ISNULL(wc.Passports,'') Passports,ISNULL(wc.SSN,'') SSN,ISNULL(wc.PlaceOfBirth,'') PlaceOfBirth,
                                     ISNULL(wc.Deceased,'') Deceased, 
                                     CASE ISNULL(WC.DelFlag, '0') 
                                     WHEN '0' THEN '1' 
                                     WHEN '1' THEN '4' END AS Status,
                                     wc.*
                            from worldcheck wc
                            left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                            left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                          where 1 = 1
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and exists (select * from OFAC.dbo.SDNTABLE(nolock) where wc.entnum = OFAC.dbo.SDNTABLE.entnum )
                            AND (SubCategory is not null and SubCategory != '') 
                    union
                          select distinct ISNULL(wc.Company,'') Company,ISNULL(wc.Category,'') Category,ISNULL(wc.Country,'') Country,
                                     ISNULL(wc.Age,'') Age,ISNULL(wc.Passports,'') Passports,ISNULL(wc.SSN,'') SSN,ISNULL(wc.PlaceOfBirth,'') PlaceOfBirth,
                                     ISNULL(wc.Deceased,'') Deceased, 
                                     CASE ISNULL(WC.DelFlag, '0') 
                                     WHEN '0' THEN '1' 
                                     WHEN '1' THEN '4' END AS Status,
                                     wc.*
                            from worldcheck wc
                            left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                            left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                          where 1 = 1
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and exists (select * from OFAC.dbo.SDNTABLE(nolock) where wc.entnum = OFAC.dbo.SDNTABLE.entnum )
                          and (SubCategory is null or SubCategory = '')
                                         AND (wcl.Used = 1 ";
                    if (wc_import_crime == "Y")
                    {
                        query += @"          or Category like '%CRIME%'";
                    }
                    query += ")";

                    sqlCommand.CommandText = query;
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 300;

                    da = new SqlDataAdapter(sqlCommand);
                    ds = new DataSet();

                    da.Fill(ds);

                    LogAdd(string.Format("[Daily] Notes - number count:{0}", ds.Tables[0].Rows.Count));

                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            EntNum = r["Entnum"].ToString().Trim();
                            Status = r["Status"].ToString().Trim();
                            ListCreateDate = r["Entered"].ToString().Trim();
                            ListModifDate = r["Updated"].ToString().Trim();
                            CreateDate = dt.ToString("yyyy-MM-dd HH:mm:ss");
                            LastOper = "PRIMEADMIN";
                            LastModify = dt.ToString("yyyy-MM-dd HH:mm:ss");

                            int i;

                            for (i = 0; i < r.ItemArray.Length; i++)
                            {
                                if (r[i].ToString().Trim() != "" && i <= 7)
                                {
                                    switch (i)
                                    {
                                        case 0:
                                            Note = r[i].ToString().Trim().Length > 1000 ? r[i].ToString().Trim().Substring(0, 1000) : r[i].ToString().Trim();
                                            NoteType = "50";
                                            break;
                                        case 1:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "48";

                                            break;
                                        case 2:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "8";

                                            break;

                                        case 3:
                                            if (r[i].ToString().Trim() == "0")
                                                continue;
                                            Note = r[i].ToString().Trim();
                                            NoteType = "47";

                                            break;

                                        case 4:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "6";
 
                                            break;

                                        case 5:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "3";

                                            break;

                                        case 6:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "1";

                                            break;

                                        case 7:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "49";

                                            break;
                                    }

                                    //modify because tempsdnaddrtable schema different from sdnaddrtable
                                    sqlCommand.CommandText = @"
                                insert into OFAC..TempLoadNotes (NotesID,EntNum,Note,NoteType,Status,ListCreateDate,ListModifDate,CreateOper,CreateDate,LastOper,LastModify)
                                select top 1 @NotesID,@EntNum,@Note,@NoteType,@Status,@ListCreateDate,@ListModifDate,@CreateOper,@CreateDate,@LastOper,@LastModify
                                from OFAC..Notes;";
                                    sqlCommand.Parameters.Clear();
                                    sqlCommand.Parameters.AddWithValue("@NotesID", NotesIdMax);
                                    sqlCommand.Parameters.AddWithValue("@Entnum", EntNum);
                                    sqlCommand.Parameters.AddWithValue("@Note", Note);
                                    sqlCommand.Parameters.AddWithValue("@NoteType", NoteType);
                                    sqlCommand.Parameters.AddWithValue("@Status", Status);
                                    sqlCommand.Parameters.AddWithValue("@ListCreateDate", ListCreateDate);
                                    sqlCommand.Parameters.AddWithValue("@ListModifDate", ListModifDate);
                                    sqlCommand.Parameters.AddWithValue("@CreateOper", LastOper);
                                    sqlCommand.Parameters.AddWithValue("@CreateDate", CreateDate);
                                    sqlCommand.Parameters.AddWithValue("@LastOper", LastOper);
                                    sqlCommand.Parameters.AddWithValue("@LastModify", LastModify);

                                    sqlCommand.ExecuteNonQuery();

                                    NotesIdMax = NotesIdMax + 1;
                                }
                            }
                        }
                    }
                }

                //存在Notes 更新狀態=1
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    update OFAC..Notes set status = '1' where entnum in (select entnum from OFAC..TempLoadNotes where Status = 1) and status != 1;";
                    sqlCommand.CommandTimeout = 300;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Update Notes=1:{0}", UpdateCount);
                    OFACEventLog("Notes daily Update status = 1:" + UpdateCount);
                }

                //存在Notes 更新狀態=4
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    update OFAC..Notes set status = '4' where entnum in (select entnum from OFAC..TempLoadNotes where Status = 4) and status != 4;";
                    sqlCommand.CommandTimeout = 300;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Update Notes=4:{0}", UpdateCount);
                    OFACEventLog("Notes daily Update status = 4:" + UpdateCount);
                }

                //不存在Notes insert
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    insert into OFAC..Notes(NotesID,EntNum,Note,NoteType,Status,ListCreateDate,ListModifDate,CreateOper,CreateDate,LastOper,LastModify)
                    select NotesID,EntNum,Note,NoteType,Status,ListCreateDate,ListModifDate,CreateOper,CreateDate,LastOper,LastModify
                      from OFAC..TempLoadNotes where not exists 
                      (select * from OFAC..Notes 
                        where OFAC..Notes.EntNum = OFAC..TempLoadNotes.EntNum
                        and isnull(OFAC.dbo.Notes.Note,'') = isnull(OFAC.dbo.TempLoadNotes.Note, '')
                        and isnull(OFAC.dbo.Notes.NoteType,'') = isnull(OFAC.dbo.TempLoadNotes.NoteType,'')
                      )
                    ;";
                    sqlCommand.CommandTimeout = 300;

                    int InsertCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Insert:{0}", InsertCount);
                    OFACEventLog("Notes daily totoal Insert:" + InsertCount);
                    LogAdd(string.Format("[Daily] Notes - Insert:{0}", InsertCount));
                }

                //201904 update status where not exists
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    string sqlDel = @" update OFAC..Notes set Status = 4 , LastModify= getdate()
                from OFAC..Notes (nolock) note
                where EntNum in (select EntNum from OFAC..TempLoadNotes)
                and not exists 
                (select * from OFAC..TempLoadNotes (nolock) tnote where note.EntNum = tnote.EntNum and note.Note = tnote.Note
                    and note.NoteType = tnote.NoteType
                ) ";

                    sqlCommand.CommandText = sqlDel;
                    sqlCommand.CommandTimeout = 300;

                    int InsertCount = sqlCommand.ExecuteNonQuery();
                    LogAdd("[Daily] update Notes set status = 4 where not exists:" + InsertCount);
                }

                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        /// <summary>
        /// 更新WCKeyword 資料
        /// </summary>
        /// <param name="str_UPDDate"></param>
        /// <param name="strBaseNum"></param>
        public void UpdateKeyWord(string str_UPDDate, int strBaseNum)
        {
            try
            {
                Database db;
                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
                string str_datetime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string sqldel = "";
                string sqlins = "";

                if (string.IsNullOrEmpty(str_UPDDate))
                {
                    str_UPDDate = "30";
                }

                sqlins = "select UID, Keywords from WorldCheck(nolock) ";
                sqlins += " where UPDDate >= dateadd(MINUTE,@min * -1,GETDATE()) ";
                sqlins += "and isnull(DelFlag,0) = 0 ";
                sqlins += "and Keywords is not null and Keywords != '' ";

                LogAdd("Select SQL:" + sqlins);

                DbCommand dbCommandIns = db.GetSqlStringCommand(sqlins);
                dbCommandIns.CommandTimeout = 0;
                //dbCommandIns.Parameters.AddWithValue("@min", str_datetime);
                var parameter = dbCommandIns.CreateParameter();
                parameter.ParameterName = "@min";
                parameter.Value = str_UPDDate;
                dbCommandIns.Parameters.Add(parameter);

                IDataReader dataReaderDT = db.ExecuteReader(dbCommandIns);
                DataTable dt = new DataTable();
                dt.Load(dataReaderDT);

                DbCommand dbCommandSelect = db.GetSqlStringCommand("select * from WCKeywords where 0 =1;");
                dbCommandSelect.CommandTimeout = 0;
                IDataReader dataReaderDTAdd = db.ExecuteReader(dbCommandSelect);
                DataTable dtadd = new DataTable();
                dtadd.Load(dataReaderDTAdd);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int j = 0; j < dt.Rows[i]["Keywords"].ToString().Split('~').Count(); j++)
                    {
                        DataRow row = dtadd.NewRow();
                        row["UID"] = dt.Rows[i]["UID"].ToString();
                        row["Entnum"] = int.Parse(dt.Rows[i]["UID"].ToString()) + strBaseNum;
                        row["Word"] = dt.Rows[i]["Keywords"].ToString().Split('~')[j].ToString().Trim();
                        row["Deleted"] = false;
                        row["Status"] = 0;
                        row["CreateOper"] = "PrimeAdmin";
                        row["CreateDate"] = str_datetime;
                        row["LastModifDate"] = str_datetime;
                        row["LastOper"] = "PrimeAdmin";

                        dtadd.Rows.Add(row);
                        dtadd.AcceptChanges();
                    }
                }

                dbCommandSelect.Dispose();

                dataReaderDT.Close();
                var sqlConnection = new SqlConnection(connectionString);

                var sqlBulkCopy = new SqlBulkCopy(sqlConnection)
                {
                    DestinationTableName = "WCKeywords"
                };

                sqldel = "delete WCKeywords where UID in ( ";
                sqldel += "select UID from WorldCheck(nolock) ";
                sqldel += "where UPDDate >= dateadd(MINUTE,@min * -1,GETDATE()) ";
                sqldel += "and isnull(DelFlag,0) = 0) ";

                LogAdd("del SQL:" + sqldel);

                LogAdd("WCK data count:"+ dtadd.Rows.Count);
                //LogAdd("WCK show data: UID=" + dtadd.Rows[0]["UID"].ToString() + ",Entnum=" + dtadd.Rows[0]["Entnum"].ToString() + ",Word=" + dtadd.Rows[0]["Word"].ToString() + ",createdate=" + dtadd.Rows[0]["CreateDate"].ToString() + ".");
                //LogAdd("WCK show data: Delete=" + dtadd.Rows[0]["Deleted"].ToString() + ",Status=" + dtadd.Rows[0]["Status"].ToString() + ".");
                if (dtadd.Rows.Count > 0)
                {
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        sqlConnection.Open();
                        sqlCommand.CommandText = sqldel;
                        sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                        int DelCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        OFACEventLog("WorldCheck Keywords delete:" + DelCount);
                        LogAdd("WorldCheck Keywords delete:" + DelCount);
                    }

                    InsertDataTable(sqlBulkCopy, sqlConnection, dtadd);
                }

                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            catch(Exception ex)
            {
                Log.WriteToLog("Error", "Update WCKeywords fail:" + ex.ToString());
            }
        }

        protected void InsertDataTable(SqlBulkCopy sqlBulkCopy, SqlConnection sqlConnection, DataTable dataTable)
        {
            Console.WriteLine("WriteToServer " + dataTable.Rows.Count.ToString() + " Records.");
            sqlBulkCopy.WriteToServer(dataTable);
            dataTable.Rows.Clear();
        }

        /// <summary>
        /// 寫入OFAC Event
        /// </summary>
        public void OFACEventLog(string log)
        {
            string sql = @"
insert into SDNChangeLog (logdate, oper, logtext, type, objecttype, objectid) 
values 
(getdate(), 'Prime', @Log, 'Ann', 'Event', 'WORLDCHECK')
";

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                using (var cmd = new SqlCommand(sql, sqlConnection))
                {
                    cmd.Parameters.Add(new SqlParameter("@Log", log.Replace(@"\n", Environment.NewLine)));
                    cmd.ExecuteNonQuery();
                }

                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        private void LogAdd(string log)
        {
            logs.AppendLine(log);
            Log.WriteToLog("Info", log);
        }

        private string BuildFileImg()
        {
            StringBuilder s = new StringBuilder();
            Process p = new Process();
            ProcessStartInfo pInfo = new ProcessStartInfo("D:\\Prime\\Exe\\BuildFileImg.exe");

            s.AppendLine(string.Format("{0} | ========== Start Build File Image ==========",
                                       DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")))
             .AppendLine();

            pInfo.UseShellExecute = false;
            pInfo.RedirectStandardOutput = true;
            pInfo.CreateNoWindow = true;

            p.StartInfo = pInfo;
            p.Start();

            using (StreamReader myStreamReader = p.StandardOutput)
            {
                string result = myStreamReader.ReadToEnd();
                p.WaitForExit();
                p.Close();

                s.AppendLine(result);
            }

            s.AppendLine(string.Format("{0} | ========== End Build File Image ==========",
                                        DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));
            return s.ToString();
        }

        private void StopService()
        {
            try
            {
                //stop OFAC service
                Process p = new Process();
                //ProcessStartInfo pInfo = new ProcessStartInfo();

                LogAdd(string.Format("{0} | ========== Stop OFAC Server BOTOSB service begin ==========",
                                           DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                string m_ServiceName = "OFAC Server BOTOSB";
                System.ServiceProcess.ServiceController service = new System.ServiceProcess.ServiceController(m_ServiceName);

                // 設定一個 Timeout 時間，若超過 60 秒啟動不成功就宣告失敗!
                TimeSpan timeout = TimeSpan.FromMilliseconds(1000 * 60);

                // 若該服務不是「停用」的狀態，才將其停止運作，否則會引發 Exception
                if (service.Status != System.ServiceProcess.ServiceControllerStatus.Stopped &&
                    service.Status != System.ServiceProcess.ServiceControllerStatus.StopPending)
                {
                    service.Stop();
                    service.WaitForStatus(System.ServiceProcess.ServiceControllerStatus.Stopped, timeout);
                }

                service.Dispose();

                LogAdd(string.Format("{0} | ========== Stop OFAC Server BOTOSB service finished ==========",
                                            DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Stop OFAC service ERROR:" + ex.ToString());
            }
        }

        private void StartService()
        {
            try
            {
                //stop OFAC service
                Process p = new Process();
                //ProcessStartInfo pInfo = new ProcessStartInfo();

                LogAdd(string.Format("{0} | ========== Start OFAC Server BOTOSB service begin ==========",
                                           DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                string m_ServiceName = "OFAC Server BOTOSB";
                System.ServiceProcess.ServiceController service = new System.ServiceProcess.ServiceController(m_ServiceName);

                // 設定一個 Timeout 時間，若超過 60 秒啟動不成功就宣告失敗!
                TimeSpan timeout = TimeSpan.FromMilliseconds(1000 * 60);

                service.Start();
                service.WaitForStatus(System.ServiceProcess.ServiceControllerStatus.Running, timeout);

                service.Dispose();

                LogAdd(string.Format("{0} | ========== Start OFAC Server BOTOSB service finished ==========",
                                            DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Stop OFAC service ERROR:" + ex.ToString());
            }
        }

        private void UpdDBStatus()
        {
            //2018.05
            try
            {
                string sql = @"update OFAC.dbo.SDNDbStatus set MemImgBuildReq = 0;";

                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();

                    using (var cmd = new SqlCommand(sql, sqlConnection))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    sqlConnection.Close();
                    sqlConnection.Dispose();
                }

                OFACEventLog("Update DBstatus to 0 ");

                Log.WriteToLog("Info", "Update DBstatus to 0 ");
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Update DBstatus ERROR:" + ex.ToString());
            }
        }

        public class GetWCPara
        {
            public string WCSQL { get; set; }
            public string WCListtype { get; set; }
            public string WCProgram { get; set; }
        }

    }
}
