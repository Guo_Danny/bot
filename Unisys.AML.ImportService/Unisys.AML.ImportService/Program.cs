﻿using log4net.Config;
using System;
using System.IO;
using System.ServiceProcess;
using Unisys.AML.Common;

namespace Unisys.AML.ImportService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            XmlConfigurator.Configure(new System.IO.FileInfo(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + @"\log4net.config"));

            if (Environment.UserInteractive)
            {
                ImportService s = new ImportService();

                s.Start(null);

                Console.WriteLine("Service Started，Press Enter To Stop Service...");
                Console.ReadLine();

                s.Stop();

                Console.WriteLine("Service Stop");
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new ImportService()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception exception = (Exception)e.ExceptionObject;
            Log.WriteToLog("Error", "An unhandled exception was generated during execution.\n\nType:{0}\nMessage:\n{1}", exception.GetType().FullName, exception.Message);
        }
    }
}
