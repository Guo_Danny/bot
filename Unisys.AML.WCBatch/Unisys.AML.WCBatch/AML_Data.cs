﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.Security.Cryptography;
using System.IO;
using Newtonsoft.Json;
using System.Collections;
using System.Xml;
using System.Reflection;

namespace Unisys.AML.WCBatch
{
    class AML_Data
    {
        private Database database;

        //private static readonly ILog log = LogManager.GetLogger("App.Logging");

        private string gatewayhost = String.Empty;
        private string gatewayurl = String.Empty;
        private string apikey = String.Empty;
        private string apiurl = String.Empty;
        private string apisecret = String.Empty;
        private string secondaryFields = String.Empty;
        private string entityType = String.Empty;
        private string customFields = String.Empty;
        private string groupId = String.Empty;
        private string providerTypes = String.Empty;
        private int basenum = 0;
        private string strProxy = String.Empty;
        private string str_1000 = String.Empty;
        private string TR1 = String.Empty;
        private string TR2 = String.Empty;
        private string TR3 = String.Empty;
        private string str_exclude = String.Empty;
        private string SQLpath = String.Empty;

        public AML_Data()
        {
            database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

            gatewayhost = ConfigurationManager.AppSettings["gatewayhost"];
            gatewayurl = ConfigurationManager.AppSettings["gatewayurl"];
            apikey = ConfigurationManager.AppSettings["apikey"];
            apiurl = ConfigurationManager.AppSettings["apiurl"];
            apisecret = ConfigurationManager.AppSettings["apisecret"];
            secondaryFields = ConfigurationManager.AppSettings["secondaryFields"];
            entityType = ConfigurationManager.AppSettings["entityType"];
            customFields = ConfigurationManager.AppSettings["customFields"];
            groupId = ConfigurationManager.AppSettings["groupId"];
            providerTypes = ConfigurationManager.AppSettings["providerTypes"];
            basenum = int.Parse(ConfigurationManager.AppSettings["basenum"]);
            strProxy = ConfigurationManager.AppSettings["goproxy"];
            str_1000 = ConfigurationManager.AppSettings["type1000"];
            TR1 = ConfigurationManager.AppSettings["TradeMark"];
            TR2 = ConfigurationManager.AppSettings["TradeMark2"];
            TR3 = ConfigurationManager.AppSettings["TradeMark3"];
            str_exclude = ConfigurationManager.AppSettings["excludenames"];
            SQLpath = ConfigurationManager.AppSettings["SQL_path"];

            if (!File.Exists(SQLpath))
            {
                File.Create(SQLpath);
            }
            else
            {
                File.Delete(SQLpath);

                File.Create(SQLpath);
            }
        }

        public void Updatematchcount(int imatchcount, string strRef)
        {
            string sql = "update [OFAC].[dbo].FilterTranTable set MatchCount = @match ";
            sql += "where Ref = @ref ;";

            string sqlT = "update [OFAC].[dbo].FilterTranTable set MatchCount = '" + imatchcount + "' ";
            sqlT += "where Ref = '"+ strRef + "' ;";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@match", imatchcount));
                cmd.Parameters.Add(new SqlParameter("@ref", strRef));

                ExecuteSQL(cmd);

                //System.IO.File.WriteAllText(SQLpath, sqlT);

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(SQLpath, true))
                {
                    file.WriteLine(sqlT);
                }
            }
        }

        public string CheckSdn(string strEntnum, string strName, string strBranch, string strprogram, string strreferenceId, string strmatchedNameType, string[] strsources, string[] strcategories)
        {
            string sql = "";
            string sqlT = "";
            string rEntnum = "";
            Fortify fortifyService = new Fortify();
            string sour = "";
            string cate = "";
            foreach (string s in strsources)
            {
                sour = sour + s + ",";
            }
            sour = sour.TrimEnd(',');

            foreach (string s in strcategories)
            {
                cate = cate + s + ",";
            }
            cate = cate.TrimEnd(',');

            strEntnum = (int.Parse(strEntnum) + basenum).ToString();

            sql = @"use OFAC;select entnum from [OFAC].[dbo].sdntable (nolock) 
                      where entnum = @ent;";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.CommandText = sql;

                database.AddInParameter(cmd, "@ent", DbType.Int32, strEntnum);

                IDataReader reader = database.ExecuteReader(cmd);
                try
                {
                    while (reader.Read())
                    {
                        rEntnum = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }

                if (rEntnum == "")
                {
                    string remark = "";
                    remark += TR1 + " \r\n";
                    remark += TR2 + " \r\n";
                    remark += TR3 + " \r\n";
                    remark += "Added by World-Check one API. \r\n";
                    remark += "referenceId:" + strreferenceId + " \r\n";
                    remark += "matchedNameType:" + strmatchedNameType + " \r\n";
                    remark += "sources:" + sour + " \r\n";
                    remark += "categories:" + cate + " \r\n";

                    //insert sdntable
                    sql = "use OFAC;insert into [OFAC].[dbo].sdntable(entnum,name,listtype,program,type,primeadded,remarks,sdntype,datatype,userrec,deleted,duplicrec,ignorederived,status,";
                    sql += "Listcreatedate,listmodifdate,createdate,lastmodifdate,lastoper)";
                    sql += "select @entnum,@name,@listtype,@program,'other',0,@remark,1,1,1,0,0,0,1,";
                    sql += "getdate(),getdate(),getdate(),getdate(),'PrimeAdmin';";

                    sqlT = "use OFAC;insert into [OFAC].[dbo].sdntable(entnum,name,listtype,program,type,primeadded,remarks,sdntype,datatype,userrec,deleted,duplicrec,ignorederived,status,";
                    sqlT += "Listcreatedate,listmodifdate,createdate,lastmodifdate,lastoper)";
                    sqlT += "select '"+ strEntnum + "','"+ strName + "','WC1','"+ strprogram + "','other',0,'"+ remark + "',1,1,1,0,0,0,1,";
                    sqlT += "getdate(),getdate(),getdate(),getdate(),'PrimeAdmin';";

                    cmd.CommandText = sql;

                    cmd.Parameters.Add(new SqlParameter("@entnum", strEntnum));
                    cmd.Parameters.Add(new SqlParameter("@name", strName));
                    cmd.Parameters.Add(new SqlParameter("@listtype", "WC1"));
                    cmd.Parameters.Add(new SqlParameter("@program", strprogram));
                    cmd.Parameters.Add(new SqlParameter("@remark", remark));

                    Log.WriteToLog("Info", "insert sdn ent:" + fortifyService.PathManipulation(strEntnum) + ",name:" + fortifyService.PathManipulation(strName));
                    
                    ExecuteSQL(cmd);

                    //System.IO.File.WriteAllText(SQLpath, sqlT);
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(SQLpath, true))
                    {
                        file.WriteLine(sqlT);
                    }
                }
                else
                {
                    Log.WriteToLog("Info", "entnum:{0} already exist.", rEntnum);
                }
            }
            return rEntnum;
        }

        /// <summary>
        /// Inser Recon table reports
        /// </summary>
        /// <param name="strSource"></param>
        /// <param name="strTranSearchType"></param>
        /// <param name="strSancPartyCount"></param>
        /// <param name="strbranch"></param>
        /// <param name="strdepartment"></param>
        /// <param name="strCaseRef"></param>
        /// <param name="strUserMsgRef"></param>
        /// <param name="strSearchOper"></param>
        /// <param name="strCreater"></param>
        /// <returns></returns>
        public Boolean InsertRecon(string strSource, string strTranSearchType, string strSancPartyCount, string strbranch, string strdepartment, string strCaseRef, string strUserMsgRef, string strSearchOper, string strCreater)
        {
            Boolean bol = false;

            string sql = @"
insert into ReconTable(Source,MsgRef,TranSearchType,SancPartyCount,Branch,Department,CaseReference,UserMessageReference,FileImage,SearchOper,
SearchDate,CreateDate)
select top 1 @Source,
@Source + '-' + replace(CONVERT(VARCHAR(10),GETDATE(),110),'-','') + '-' + CAST ((select count(Source)+1 from ReconTable) as VARCHAR(20)),
@TranSearchType,@SancPartyCount,@branch,@department,@CaseRef,
@UserMsgRef,@fileimg,@SearchOper,getdate(),getdate() from recontable where 1 = 1
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@Source", strSource));
                cmd.Parameters.Add(new SqlParameter("@TranSearchType", strTranSearchType));
                cmd.Parameters.Add(new SqlParameter("@SancPartyCount", strSancPartyCount));
                cmd.Parameters.Add(new SqlParameter("@branch", strbranch));
                cmd.Parameters.Add(new SqlParameter("@department", strdepartment));
                cmd.Parameters.Add(new SqlParameter("@CaseRef", strCaseRef));
                cmd.Parameters.Add(new SqlParameter("@UserMsgRef", strUserMsgRef));
                cmd.Parameters.Add(new SqlParameter("@fileimg", string.Format("{0}Swift", strbranch)));
                cmd.Parameters.Add(new SqlParameter("@SearchOper", strSearchOper));

                ExecuteSQL(cmd);
                bol = true;
            }
            return bol;
        }

        /// <summary>
        /// Insert Case
        /// </summary>
        /// <param name="strRef"></param>
        /// <param name="strbranch"></param>
        /// <returns></returns>
        public Boolean InsertFilterTran(string strRef, string strbranch, string strmatchcount)
        {
            try
            {
                bool bol_r = false;
                int filtercount = 0;
                string SQL = "";

                string SQLT = "";

                SQL = "use ofac;insert into [OFAC].[dbo].FilterTranTable (Source,Ref,Branch,Dept,ReqTime,ClientId,TranTime,Result,MatchCount,MsgType,Currency,KeyField1,KeyField2,CIFFieldsDelimiter,LengthKF1,LengthKF2,TransactionType,SystemRef,FileImage,App,IsIncremental) ";
                SQL += "select top 1 @Source,@Ref,@Branch,@Dept,getdate(),@ClientId,getdate(),@Result,@MatchCount,@MsgType,@Currency,@KeyField1,@KeyField2,@CIFFieldsDelimiter,";
                SQL += "@LengthKF1,@LengthKF2,@TransactionType,@SystemRef,@FileImage,@App,@IsIncremental ";
                SQL += "from [OFAC].[dbo].FilterTranHistTable where 1 = 1; ";

                SQLT = "use ofac;insert into [OFAC].[dbo].FilterTranTable (Source,Ref,Branch,Dept,ReqTime,ClientId,TranTime,Result,MatchCount,MsgType,Currency,KeyField1,KeyField2,CIFFieldsDelimiter,LengthKF1,LengthKF2,TransactionType,SystemRef,FileImage,App,IsIncremental) ";
                SQLT += "select top 1 "+ "'Thomson Reuters World-Check One Batch'" + ",'"+ strRef + "','"+ strbranch + "','AML',getdate(),'8888',getdate(),'1','"+ strmatchcount + "','1','USD','0','1','|',";
                SQLT += "'0','0','1','"+ string.Format("WC1-{0}-{1}-0", DateTime.Now.ToString("MMddyyyy"), DateTime.Now.ToString("Hmmss")) + "','WC1','0','0' ";
                SQLT += "from [OFAC].[dbo].FilterTranHistTable; ";


                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.Parameters.Add(new SqlParameter("@Source", "Thomson Reuters World-Check One Batch"));
                    cmd.Parameters.Add(new SqlParameter("@Ref", strRef));
                    cmd.Parameters.Add(new SqlParameter("@branch", strbranch));
                    cmd.Parameters.Add(new SqlParameter("@Dept", "AML"));
                    cmd.Parameters.Add(new SqlParameter("@ClientId", "8888"));
                    cmd.Parameters.Add(new SqlParameter("@Result", "1"));
                    cmd.Parameters.Add(new SqlParameter("@MatchCount", strmatchcount));
                    cmd.Parameters.Add(new SqlParameter("@MsgType", "1"));
                    cmd.Parameters.Add(new SqlParameter("@Currency", "USD"));
                    cmd.Parameters.Add(new SqlParameter("@KeyField1", "0"));
                    cmd.Parameters.Add(new SqlParameter("@KeyField2", "1"));
                    cmd.Parameters.Add(new SqlParameter("@CIFFieldsDelimiter", "|"));
                    cmd.Parameters.Add(new SqlParameter("@LengthKF1", "0"));
                    cmd.Parameters.Add(new SqlParameter("@LengthKF2", "0"));
                    cmd.Parameters.Add(new SqlParameter("@TransactionType", "1"));
                    cmd.Parameters.Add(new SqlParameter("@SystemRef", string.Format("WC1-{0}-{1}-0", DateTime.Now.ToString("MMddyyyy"), DateTime.Now.ToString("Hmmss"))));
                    cmd.Parameters.Add(new SqlParameter("@FileImage", "WC1"));
                    cmd.Parameters.Add(new SqlParameter("@App", "0"));
                    cmd.Parameters.Add(new SqlParameter("@IsIncremental", "0"));

                    filtercount = ExecuteSQL(cmd);
                    //System.IO.File.WriteAllText(SQLpath, SQLT);
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(SQLpath, true))
                    {
                        file.WriteLine(SQLT);
                    }

                    cmd.CommandText = "use ofac; select SeqNumb from [OFAC].[dbo].FilterTranTable where Ref = '" + strRef + "'; ";
                    string strSeqNumb = "";

                    IDataReader reader = database.ExecuteReader(cmd);
                    try
                    {
                        while (reader.Read())
                        {
                            strSeqNumb = reader[0].ToString();
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                    Log.WriteToLog("Info", "FilterTran SeqNum:" + strSeqNumb);

                    bol_r = true;
                }
                Log.WriteToLog("Info", "InsertFilterTran:" + filtercount);

                return bol_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strRef"></param>
        /// <param name="strentNum"></param>
        /// <param name="strEngName"></param>
        /// <param name="strOrgSDN"></param>
        /// <param name="Strength"></param>
        /// <returns></returns>
        public Boolean InsertMatchTable(string strRef, string strentNum, string strEngName, string strOrgSDN, string Strength)
        {
            try
            {
                Fortify fortifyService = new Fortify();

                strentNum = (int.Parse(strentNum) + basenum).ToString();

                bool bol_r = false;

                string SQL = "";
                string SQLT = "";
                SQL = "use ofac;insert into [OFAC].[dbo].MatchTable (SeqNumb,MatchName,OriginalSDNName,MatchText,MatchCountry,MatchType,Score,entNum) ";
                SQL += "select top 1 SeqNumb, @MatchName,@OriginalSDNName,@MatchText,@MatchCountry,@MatchType,@Score,@entNum ";
                SQL += "from [OFAC].[dbo].FilterTranTable where Ref = @Ref order by ReqTime DESC ";

                int iscore = 0;
                if (Strength.ToUpper() == "STRONG")
                {
                    iscore = 95;
                }
                else if (Strength.ToUpper() == "MEDIUM")
                {
                    iscore = 85;
                }
                else if (Strength.ToUpper() == "WEAK")
                {
                    iscore = 75;
                }
                else if (Strength.ToUpper() == "EXACT")
                {
                    iscore = 100;
                }

                SQLT = "use ofac;insert into [OFAC].[dbo].MatchTable (SeqNumb,MatchName,OriginalSDNName,MatchText,MatchCountry,MatchType,Score,entNum) ";
                SQLT += "select top 1 SeqNumb, '" + strEngName + "','" + strOrgSDN + "','" + strEngName + "','WC1','other',"+ iscore + ",'"+ strentNum + "' ";
                SQLT += "from [OFAC].[dbo].FilterTranTable where Ref = '" + strRef + "' order by ReqTime DESC ";


                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.Parameters.Add(new SqlParameter("@Ref", strRef));
                    cmd.Parameters.Add(new SqlParameter("@MatchName", strEngName));
                    cmd.Parameters.Add(new SqlParameter("@OriginalSDNName", strOrgSDN));
                    cmd.Parameters.Add(new SqlParameter("@MatchText", strEngName));
                    cmd.Parameters.Add(new SqlParameter("@MatchCountry", "WC1"));
                    cmd.Parameters.Add(new SqlParameter("@MatchType", "other"));
                    cmd.Parameters.Add(new SqlParameter("@Score", iscore));
                    cmd.Parameters.Add(new SqlParameter("@entNum", strentNum));

                    ExecuteSQL(cmd);
                    //System.IO.File.WriteAllText(SQLpath, SQLT);
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(SQLpath, true))
                    {
                        file.WriteLine(SQLT);
                    }
                    bol_r = true;
                }
                Log.WriteToLog("Info", string.Format("InsertMatchTable:{0},strRef={1},strentNum={2}", fortifyService.PathManipulation(bol_r.ToString()), fortifyService.PathManipulation(strRef), fortifyService.PathManipulation(strentNum)));
                return bol_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// InsertMsgTable
        /// </summary>
        /// <param name="strRef"></param>
        /// <param name="strcif_file"></param>
        /// <returns></returns>
        public Boolean InsertMsgTable(string strRef, string strcif_file)
        {
            try
            {
                bool bol_r = false;
                string strMsg = "";
                System.IO.StreamReader file = new System.IO.StreamReader(strcif_file);
                string line = "";
                while ((line = file.ReadLine()) != null)
                {
                    strMsg += line;
                }

                string SQL = "";
                string SQLT = "";
                SQL = "use OFAC;insert into [OFAC].[dbo].MsgTable(SeqNumb,MsgType,MsgLen,Msg) ";
                SQL += "select top 1 SeqNumb , @MsgType,@MsgLen,@Msg ";
                SQL += "from [OFAC].[dbo].FilterTranTable where Ref = @Ref order by ReqTime DESC";

                SQLT = "use OFAC;insert into [OFAC].[dbo].MsgTable(SeqNumb,MsgType,MsgLen,Msg) ";
                SQLT += "select top 1 SeqNumb , '1',"+ strMsg.Length + ",'"+ strMsg + "' ";
                SQLT += "from [OFAC].[dbo].FilterTranTable where Ref = '" + strRef + "' order by ReqTime DESC";

                using (DbCommand cmd = database.GetSqlStringCommand(SQL))
                {
                    cmd.Parameters.Add(new SqlParameter("@Ref", strRef));
                    cmd.Parameters.Add(new SqlParameter("@MsgType", "1"));
                    cmd.Parameters.Add(new SqlParameter("@MsgLen", strMsg.Length));
                    cmd.Parameters.Add(new SqlParameter("@Msg", strMsg));

                    ExecuteSQL(cmd);
                    //System.IO.File.WriteAllText(SQLpath, SQLT);
                    using (System.IO.StreamWriter filesql = new System.IO.StreamWriter(SQLpath, true))
                    {
                        filesql.WriteLine(SQLT);
                    }
                    bol_r = true;
                }

                file.Close();
                return bol_r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Check World-Check API
        /// </summary>
        /// <param name="strcif_file"></param>
        /// <param name="str_filename"></param>
        /// <param name="strReferenceNumber"></param>
        /// <param name="strBankNo"></param>
        /// <param name="strBranchNo"></param>
        /// <param name="intretry"></param>
        /// <param name="intdelay"></param>
        /// <param name="str_nation_col"></param>
        /// <param name="str_engname_col"></param>
        /// <param name="strRef"></param>
        /// <param name="strEngName"></param>
        /// <param name="iScore"></param>
        /// <param name="str_score"></param>
        /// <param name="str_dob_col"></param>
        /// <returns></returns>
        public Boolean checkPEPS(string strcif_file, string str_filename, string strReferenceNumber, string strBankNo, string strBranchNo, int intretry, int intdelay, string str_nation_col, string str_engname_col, ref string strRef, ref string strEngName, ref int iScore, string str_score, string str_dob_col)
        {
            bool quit = false;
            bool bol_r = false;
            int intloop = 0;
            Fortify fortifyService = new Fortify();

            while (!quit && intloop < intretry)
            {
                try
                {
                    intloop = intloop + 1;

                    string strNation = "";
                    string streName = "";
                    string strDob = "";

                    int iDOB = 0;

                    string[] str_nation = str_nation_col.Split(',');
                    string[] str_engname = str_engname_col.Split(',');

                    System.IO.StreamReader file = new System.IO.StreamReader(strcif_file);
                    string line = "";
                    string line_steam = "";
                    string[] arry_list;

                    Log.WriteToLog("Info", string.Format("Try:{0}", intloop));

                    //2017/04/24 by danny 修改 取代換行 
                    while ((line = file.ReadLine()) != null)
                    {
                        if (str_filename.Substring(4, 2).ToUpper() == "CI")
                        {
                            line_steam += line;
                        }
                        else if (str_filename.Substring(4, 1).ToUpper() == "S" && str_filename.Substring(14, 4).ToString().Trim() != "1000")
                        {
                            if ((line.LastIndexOf(":59") > -1 || line.LastIndexOf(":50") > -1) && line.LastIndexOf(":50A") == -1)
                            {

                                if (line.LastIndexOf(":59:") >= 0 || line.LastIndexOf(":50K") >= 0)
                                {
                                    if (line.Substring(line.LastIndexOf(":59:") + 4, 1) == "/" || line.Substring(line.LastIndexOf(":50K") + 5, 1) == "/")
                                    {
                                        line = file.ReadLine();
                                        line_steam += line + ";";
                                    }
                                    else
                                    {
                                        line_steam += line.LastIndexOf(":59") >= 0 ? parstr(line, ":59") : parstr(line, ":50") + ";";
                                    }
                                }
                                else if (line.LastIndexOf(":59F") >= 0 || line.LastIndexOf(":50F") >= 0)
                                {
                                    if (line.Substring(line.LastIndexOf(":59F") + 4, 2) == "1/")
                                    {
                                        line_steam += line.LastIndexOf(":59") >= 0 ? parstr(line, ":59F") : parstr(line, ":50") + ";";
                                    }
                                    else
                                    {
                                        line = file.ReadLine();
                                        line_steam += line.LastIndexOf(":59") >= 0 ? parstr(line, ":59F") : parstr(line, ":50F") + ";";
                                    }
                                }
                            }
                        }
                        else if (str_filename.Substring(4, 1).ToUpper() == "F" && str_filename.Substring(14, 4).ToString().Trim() == "1000")
                        {
                            for (int i = 0; i < str_1000.Split(',').Count(); i++)
                            {
                                if (line.LastIndexOf(str_1000.Split(',')[i]) > -1)
                                {
                                    line_steam += parstr(line, str_1000.Split(',')[i]) + ";";
                                }
                            }
                        }
                    }
                    file.Close();

                    if (str_filename.Substring(4, 2).ToUpper() == "CI")
                    {
                        arry_list = line_steam.Split('|');
                        Log.WriteToLog("Info", string.Format("sanction text col count : {0}", arry_list.Length));

                        strDob = int.TryParse(str_dob_col, out iDOB) == true ? arry_list[int.Parse(str_dob_col)] : "";
                        Log.WriteToLog("Info", string.Format("DOB: {0}", fortifyService.PathManipulation(strDob)));

                        foreach (string nationcol in str_nation)
                        {
                            if (int.Parse(nationcol) >= arry_list.Length)
                            {
                                break;
                            }
                            strNation += Checkstr(string.IsNullOrEmpty(arry_list[int.Parse(nationcol)]) == false ? arry_list[int.Parse(nationcol)] : "", "nation") + ';';
                        }


                        foreach (string engnamecol in str_engname)
                        {
                            if (int.Parse(engnamecol) >= arry_list.Length)
                            {
                                break;
                            }
                            streName += Checkstr(string.IsNullOrEmpty(arry_list[int.Parse(engnamecol)]) == false ? arry_list[int.Parse(engnamecol)].Replace('*', ' ') : "", "engname") + ';';
                        }

                        line_steam = streName.TrimEnd(';');
                    }
                    else
                    {
                        line_steam = line_steam.TrimEnd(';');
                        Log.WriteToLog("Info", "EnglishName:{0}", fortifyService.PathManipulation(line_steam));
                    }

                    ArrayList wcresult = new ArrayList();

                    strRef = str_filename.Substring(0, 25) + DateTime.Now.ToString("HHmmss");

                    int imaincase = 0;
                    int iallmatchcount = 0;

                    foreach (string s in line_steam.Split(';'))
                    {
                        if (line_steam != "")
                        {
                            if (s == "")
                            {
                                //prepare for pass string || !string.IsNullOrEmpty(str_exclude) && str_exclude.Split('|').ToList().Any(exclude => s.ToUpper().Contains(exclude))
                                //Log.WriteToLog("Info", "pass to send wc1 :" + s + ".");
                                continue;
                            }

                            Log.WriteToLog("Info", "file={0},Count={1},Names={2}", str_filename, imaincase + 1, s);

                            imaincase = imaincase + 1;

                            //continue;

                            string casesystemid = post_scansanctiondata("", "Q", s);

                            System.Threading.Thread.Sleep(1000);

                            var result = post_scansanctiondata(casesystemid, "S", s);

                            System.Threading.Thread.Sleep(1000);

                            wcresult = wc_getresult(casesystemid, "");

                            if (wcresult != null && wcresult.Count > 0)
                            {
                                bol_r = true;
                                Log.WriteToLog("Info", "text " + fortifyService.PathManipulation(s) + ", hit count:" + wcresult.Count);

                                iallmatchcount = iallmatchcount + wcresult.Count;
                                //create case
                                Log.WriteToLog("Info", "check is first case:" + imaincase);
                                if (imaincase == 0)
                                {
                                    Log.WriteToLog("Info", "---case create start---");
                                    InsertFilterTran(strRef, strBankNo, wcresult.Count.ToString());
                                    Log.WriteToLog("Info", "---case create end---");
                                }

                                for (int i = 0; i < wcresult.Count; i++)
                                {
                                    ResultDetail resultdetail = JsonConvert.DeserializeObject<ResultDetail>(fortifyService.PathManipulation(wcresult[i].ToString()));

                                    string strentnum = resultdetail.referenceId.Substring(resultdetail.referenceId.LastIndexOf('_') + 1);
                                    string strprogram = resultdetail.sources[0].Substring(resultdetail.sources[0].LastIndexOf('_') + 1);

                                    if (Checkstr(resultdetail.matchedTerm, "engname") != "")
                                    {
                                        //check sdn data
                                        CheckSdn(strentnum, resultdetail.matchedTerm, strBankNo, strprogram, resultdetail.referenceId, resultdetail.matchedNameType, resultdetail.sources, resultdetail.categories);

                                        //insert MatchTable
                                        InsertMatchTable(strRef, strentnum, resultdetail.submittedTerm, resultdetail.matchedTerm, resultdetail.matchStrength);
                                    }
                                    else
                                    {
                                        //check language non english word
                                        //iallmatchcount = iallmatchcount - 1;
                                        ArrayList wcnames = wc_getresult(casesystemid, resultdetail.referenceId);
                                        for (int j = 0; j < wcnames.Count; j++)
                                        {
                                            string wcnametype = ((Unisys.AML.WCBatch.AML_Data.Names)new System.Collections.ArrayList(wcnames)[j]).type;

                                            string wcfullname = ((Unisys.AML.WCBatch.AML_Data.Names)new System.Collections.ArrayList(wcnames)[j]).fullName;

                                            if (wcnametype.ToUpper() == "PRIMARY")
                                            {
                                                //check sdn data
                                                Log.WriteToLog("Info", "---check sdn non eng---");
                                                CheckSdn(strentnum, wcfullname, strBankNo, strprogram, resultdetail.referenceId, resultdetail.matchedNameType, resultdetail.sources, resultdetail.categories);

                                                //insert MatchTable
                                                Log.WriteToLog("Info", "---insert match non eng---");
                                                InsertMatchTable(strRef, strentnum, resultdetail.submittedTerm, wcfullname, resultdetail.matchStrength);
                                            }
                                        }
                                    }
                                }

                                // insert MsgTable
                                if (imaincase == 0)
                                    InsertMsgTable(strRef, strcif_file);
                            }
                            else
                            {
                                imaincase = imaincase - 1;
                            }
                        }
                        if (s != "")
                        {
                            imaincase = imaincase + 1;
                        }
                        //update the case total match count
                        if (iallmatchcount != 0)
                        {
                            Updatematchcount(iallmatchcount, strRef);
                        }
                    }

                    Log.WriteToLog("Info", "file={0},total name count={1}", str_filename, imaincase);

                    quit = true;

                }
                catch (Exception ex)
                {
                    Log.WriteToLog("Error", ex.ToString());

                    if (ex.ToString().IndexOf("timed out") > 0)
                    {
                        quit = false;
                        System.Threading.Thread.Sleep(intdelay);
                    }
                    else
                    {
                        //client.Close();
                        quit = true;
                        throw ex;
                    }
                }
            }
            return bol_r;
        }

        private string post_scansanctiondata(string caseSystemId, string str_Mode, string strsanctiondata)
        {
            Fortify fortifyService = new Fortify();
            DateTime dateValue = DateTime.UtcNow; // get the datetime NOW GMT

            string date = dateValue.ToString("R"); // WC1 header requires GMT datetime stamp

            string requestendpoint = apiurl;
            if (str_Mode == "S")
            {
                requestendpoint = requestendpoint + "/" + caseSystemId + "/screeningRequest";
            }

            string postData = "{\"secondaryFields\":" + secondaryFields + ",\"entityType\":\"" + entityType + "\",\"customFields\":" + customFields + ",\"groupId\":\"" + groupId + "\",\"providerTypes\":" + providerTypes + ",\"name\":\"" + strsanctiondata + "\"}";

            string msg = postData;

            Log.WriteToLog("Info", "---postData={0}---", postData);

            UTF8Encoding encoding = new UTF8Encoding();
            byte[] byte1 = encoding.GetBytes(postData);

            // Assemble the POST request - NOTE every character including spaces have to be EXACT 
            // for the API server to decode the authorization signature       
            string strUrl = requestendpoint.Replace("https://" + gatewayhost, "");

            string dataToSign = "(request-target): post " + strUrl + "\n" +
                "host: " + gatewayhost + "\n" +   // no https only the host name
                "date: " + date + "\n" +          // GMT date as a string
                "content-type: " + "application/json" + "\n" +
                "content-length: " + byte1.Length + "\n" +
                 msg;

            //Log.WriteToLog("Info", "---api secret---");
            //Log.WriteToLog("Info", apisecret);
            //Log.WriteToLog("Info", "---dataToSign---");
            //Log.WriteToLog("Info", fortifyService.PathManipulation(dataToSign), "");
            //Log.WriteToLog("Info", "string hmac = generateAuthHeader(dataToSign, apisecret);");
            // The Request and API secret are now combined and encrypted
            string hmac = generateAuthHeader(dataToSign, apisecret);

            // Assemble the authorization string - This needs to match the dataToSign elements 
            // i.e. requires  host date content-type content-length
            //- NOTE every character including spaces have to be EXACT else decryption will fail with 401 Unauthorized
            string authorisation = "Signature keyId=\"" + apikey + "\",algorithm=\"hmac-sha256\",headers=\"(request-target) host date content-type content-length\",signature=\"" + hmac + "\"";

            //Log.WriteToLog("Info", "---Hmac---");
            //Log.WriteToLog("Info", hmac);

            // Send the Request to the API server
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(requestendpoint);
            // Set the Headers
            WebReq.Method = "POST";
            WebReq.Headers.Add("Authorization", authorisation);
            WebReq.Headers.Add("Cache-Control", "no-cache");
            WebReq.ContentLength = msg.Length;

            // Set the content type of the data being posted.
            Type type = WebReq.Headers.GetType();
            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            MethodInfo methodInfo = type.GetMethod("AddWithoutValidate", flags);
            object[] myPara = new object[2];
            myPara[0] = "Date";
            myPara[1] = dateValue.ToString("R");
            methodInfo.Invoke(WebReq.Headers, myPara);

            WebReq.ContentType = "application/json";
            WebReq.ContentLength = byte1.Length;

            //proxy
            if (strProxy != String.Empty)
            {
                string[] strByPass = new string[] { @"192.\.168\..*", @"172\.17\..*" };
                WebReq.Proxy = new WebProxy(new Uri(strProxy), true, strByPass);
            }

            Stream newStream = WebReq.GetRequestStream();
            newStream.Write(byte1, 0, byte1.Length);
            try
            {
                // Get the Response - Status OK
                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                // Status information about the request

                // Get the Response data
                Stream Answer = WebResp.GetResponseStream();
                StreamReader _Answer = new StreamReader(Answer);
                string jsontxt = _Answer.ReadToEnd();

                // convert json text to a pretty printout
                var obj = JsonConvert.DeserializeObject(fortifyService.PathManipulation(jsontxt));
                var f = JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented);

                if (str_Mode == "Q")
                {
                    wcapi wcapi = JsonConvert.DeserializeObject<wcapi>(fortifyService.PathManipulation(f));
                    caseSystemId = wcapi.caseSystemId;
                    Log.WriteToLog("Info", "caseSysID:" + fortifyService.PathManipulation(caseSystemId));
                }
                if (str_Mode == "S")
                {
                    caseSystemId = f;
                }
                return caseSystemId;

            }
            catch (WebException ex)
            {
                Log.WriteToLog("Error", ex.ToString());
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    Log.WriteToLog("Error", "WebException code:" + ((int)response.StatusCode).ToString());
                }
                return caseSystemId;
            }
        }

        private ArrayList wc_getresult(string caseSystemId, string StrEntID)
        {
            Fortify fortifyService = new Fortify();
            DateTime dateValue = DateTime.UtcNow; // get the datetime NOW GMT

            string date = dateValue.ToString("R"); // WC1 header requires GMT datetime stamp

            //set host and credentials to the WC1 API Pilot server WC1SampleClientAPI account

            string requestendpoint = apiurl + "/" + caseSystemId + "/results";

            if (StrEntID != "")
            {
                requestendpoint = apiurl.Replace("cases", "reference/profile/") + StrEntID;
            }

            // Assemble the GET request - NOTE every character including spaces have to be EXACT 
            // for the API server to decode the authorization signature       
            string strUrl = requestendpoint.Replace("https://" + gatewayhost, "");
            if (strUrl.IndexOf('?') > 0)
                strUrl = strUrl.Replace(strUrl.Substring(strUrl.IndexOf('?')), "");
            string dataToSign = "(request-target): get " + strUrl + "\n" +
                "host: " + gatewayhost + "\n" +   // no https only the host name
                "date: " + date;                  // GMT date as a string
            // The Request and API secret are now combined and encrypted
            string hmac = generateAuthHeader(dataToSign, apisecret);

            // Assemble the authorization string - This needs to match the dataToSign elements 
            // i.e. requires ONLY host date (no content body for a GET request)
            //- NOTE every character including spaces have to be EXACT else decryption will fail with 401 Unauthorized
            string authorisation = "Signature keyId=\"" + apikey + "\",algorithm=\"hmac-sha256\",headers=\"(request-target) host date\",signature=\"" + hmac + "\"";

            // Send the Request to the API server
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(requestendpoint);
            // Set the Headers
            WebReq.Method = "GET";
            WebReq.Headers.Add("Authorization", authorisation);
            WebReq.Headers.Add("Cache-Control", "no-cache");
            Type type = WebReq.Headers.GetType();
            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            MethodInfo methodInfo = type.GetMethod("AddWithoutValidate", flags);
            object[] myPara = new object[2];
            myPara[0] = "Date";
            myPara[1] = dateValue.ToString("R");
            methodInfo.Invoke(WebReq.Headers, myPara);
            Log.WriteToLog("Info", "---Get Ready---");

            //proxy
            if (strProxy != String.Empty)
            {
                string[] strByPass = new string[] { @"192.\.168\..*", @"172\.17\..*" };
                WebReq.Proxy = new WebProxy(new Uri(strProxy), true, strByPass);
            }

            try
            {
                // Get the Response - Status OK
                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                // Status information about the request

                // Get the Response data
                Stream Answer = WebResp.GetResponseStream();
                StreamReader _Answer = new StreamReader(Answer);
                string jsontxt = _Answer.ReadToEnd();

                // convert json text to a pretty printout
                var obj = JsonConvert.DeserializeObject(fortifyService.PathManipulation(jsontxt));
                var f = JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented);

                ArrayList result = new ArrayList();

                if (StrEntID == "")
                {
                    result = JsonConvert.DeserializeObject<ArrayList>(fortifyService.PathManipulation(f));
                }
                else
                {
                    Profile prof = JsonConvert.DeserializeObject<Profile>(f);

                    for (int i = 0; i < prof.names.Count(); i++)
                    {
                        result.Add(prof.names[i]);
                    }
                }

                Log.WriteToLog("Info", "---Get Finish---");
                return result;
            }
            catch (WebException ex)
            {
                Log.WriteToLog("Error", ex.ToString());
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    Log.WriteToLog("Error", "WebException code:" + ((int)response.StatusCode).ToString());
                }
                return null;
            }
        }

        public class wcapi
        {
            public string caseId { get; set; }
            public string name { get; set; }

            public string[] providerTypes { get; set; }
            public string[] customFields { get; set; }
            public string[] secondaryFields { get; set; }
            public string groupId { get; set; }
            public string entityType { get; set; }
            public string caseSystemId { get; set; }
            public string caseScreeningState { get; set; }
            public string lifecycleState { get; set; }
            public creator creator { get; set; }
            public modifier modifier { get; set; }
            public string creationDate { get; set; }
            public string modificationDate { get; set; }
            public string outstandingActions { get; set; }
        }

        public class creator
        {
            public string userId { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string email { get; set; }
            public string status { get; set; }
        }

        public class modifier
        {
            public string userId { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string email { get; set; }
            public string status { get; set; }

        }

        public class ResultDetail
        {
            public string resultId { get; set; }
            public string referenceId { get; set; }
            public string matchStrength { get; set; }
            public string matchedTerm { get; set; }
            public string submittedTerm { get; set; }
            public string matchedNameType { get; set; }
            public string[] secondaryFieldResults { get; set; }
            public string[] sources { get; set; }
            public string[] categories { get; set; }
            public string creationDate { get; set; }
            public string modificationDate { get; set; }
            public string resolution { get; set; }
            public ResultReview resultReview { get; set; }
        }

        public class ResultReview
        {
            public string reviewRequired { get; set; }
            public string reviewRequiredDate { get; set; }
            public string reviewRemark { get; set; }
            public string reviewDate { get; set; }
        }

        public class Profile
        {
            public string entityId { get; set; }
            public Names[] names { get; set; }
        }

        public class Names
        {
            public string fullName { get; set; }
            public string givenName { get; set; }
            public LanguageCode languageCode { get; set; }
            public string lastName { get; set; }
            public string originalScript { get; set; }
            public string prefix { get; set; }
            public string suffix { get; set; }
            public string type { get; set; }
        }

        public class LanguageCode
        {
            public string code { get; set; }
            public string name { get; set; }
        }

        private static string generateAuthHeader(string dataToSign, string apisecret)
        {
            byte[] secretKey = Encoding.UTF8.GetBytes(apisecret);
            HMACSHA256 hmac = new HMACSHA256(secretKey);
            hmac.Initialize();

            byte[] bytes = Encoding.UTF8.GetBytes(dataToSign);
            byte[] rawHmac = hmac.ComputeHash(bytes);

            hmac.Dispose();

            return (Convert.ToBase64String(rawHmac));

            
        }

        private string Checkstr(string str_check, string str_flag)
        {
            string str_return = "";

            bool bolR = true;

            if (System.Text.Encoding.Default.GetBytes(str_check).Length > 1)
            {
                for (int i = 0; i < str_check.Length; i++)
                {
                    int AsciiCodeO = (int)System.Convert.ToChar(str_check[i]);

                    if (AsciiCodeO < 32 || AsciiCodeO > 126)
                    {
                        bolR = false;
                    }
                }
            }
            else
            {
                bolR = false;
            }

            if (bolR)
            {
                if (str_flag == "nation")
                {
                    str_return = str_check.Substring(0, 2);
                }
                else
                {
                    str_return = str_check.Replace('*', ' ');
                }
            }
            else
            {
                str_return = "";
            }

            return str_return.Trim();
        }

        private string parstr(string str_line, string str_code)
        {
            string str_return = "";
            int istart = 0;
            int ifirst = 0;
            int ilast = 0;

            if (str_code == ":50" || str_code == ":59")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == ":")
                    {
                        str_return = str_line.Substring(i + 1);
                        break;
                    }
                }
            }
            else if (str_code == ":59F" || str_code == ":50F")
            {
                istart = 0;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "/")
                    {
                        str_return = str_line.Substring(i + 1);
                        break;
                    }
                }
            }
            else if (str_code == "{3100}" || str_code == "{3400}")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length + 9;


                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "*")
                    {
                        str_return = str_line.Substring(istart, i - istart);
                        break;
                    }
                    else if (str_line[i].ToString() == "{" || str_line[i + 1].ToString() == "{")
                    {
                        //第一個*後面有可能接下一個tag,那就跳過不抓
                        str_return = "";
                        break;
                    }

                    if (i - istart > 18)//maxlength 18
                    {
                        str_return = str_line.Substring(istart, 18);
                        break;
                    }
                }

            }
            else if (str_code == "{4000}" || str_code == "{4100}" || str_code == "{4200}" || str_code == "{4400}" || str_code == "{5000}" || str_code == "{5100}" || str_code == "{5200}")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "*")
                    {
                        if (ifirst == 0)
                        {
                            ifirst = i;
                        }
                        else
                        {
                            ilast = i;
                            str_return = str_line.Substring(ifirst + 1, ilast - ifirst - 1);
                            break;
                        }
                    }

                    if (ifirst != 0 && str_line[ifirst + 1].ToString() == "{")
                    {
                        //第一個*後面有可能接下一個tag,那就跳過不抓
                        str_return = "";
                        break;
                    }

                    if (ifirst != 0 && i - ifirst > 35)//maxlength 35
                    {
                        str_return = str_line.Substring(ifirst + 1, 35);
                        break;
                    }
                }
            }
            else if (str_code == "{5010}")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "*")
                    {
                        if (ifirst == 0 && str_line[i + 1].ToString() == "1" && str_line[i + 2].ToString() == "/")
                        {
                            ifirst = i;
                        }
                        else if (ifirst != 0)
                        {
                            ilast = i;
                            str_return = str_line.Substring(ifirst + 3, ilast - ifirst - 3);
                            break;
                        }
                    }

                    if (ifirst != 0 && str_line[ifirst + 1].ToString() == "{")
                    {
                        //第一個*後面有可能接下一個tag,那就跳過不抓
                        str_return = "";
                        break;
                    }

                    if (ifirst != 0 && i - ifirst > 35)//maxlength 35
                    {
                        str_return = str_line.Substring(ifirst + 1, 35);
                        break;
                    }
                }
            }
            else if (str_code == "{7050}" || str_code == "{7059}")
            {
                istart = str_line.LastIndexOf(str_code) + str_code.Length;

                for (int i = istart; i < str_line.Length; i++)
                {
                    if (str_line[i].ToString() == "*")
                    {
                        if (ifirst == 0)
                        {
                            ifirst = 1;
                        }
                        else if (ifirst == 1)
                        {
                            ifirst = i;
                        }
                        else
                        {
                            ilast = i;
                            str_return = str_line.Substring(ifirst + 1, ilast - ifirst - 1);
                            break;
                        }
                    }

                    if (ifirst > 1 && i - ifirst > 35)//maxlength 35
                    {
                        str_return = str_line.Substring(ifirst + 1, 35);
                        break;
                    }
                }
            }

            return str_return.Trim();
        }

        private string InClauseSql(string name, List<string> param)
        {
            StringBuilder sql = new StringBuilder();

            for (int i = 0; i < param.Count(); i++)
                sql.Append(string.Format("@{ 0}{1},", name, i.ToString()));

            return sql.ToString().TrimEnd(',');
        }

        private int ExecuteSQL(DbCommand cmd)
        {
            using (DbConnection conn = database.CreateConnection())
            {
                conn.Open();
                DbTransaction trans = conn.BeginTransaction();

                try
                {
                    int rows = database.ExecuteNonQuery(cmd, trans);
                    trans.Commit();
                    conn.Close();
                    return rows;
                }
                catch
                {
                    trans.Rollback();
                    conn.Close();
                    throw;
                }
            }
        }
    }
}
