﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.IO.Compression;

namespace Unisys.AML.WCBatch
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string filepath = ConfigurationManager.AppSettings["file_path"];
                string zipfilepath = ConfigurationManager.AppSettings["zipfile_path"];
                string extractepath = ConfigurationManager.AppSettings["extract_path"];
                string backpath = ConfigurationManager.AppSettings["filebackup_path"];
                string filedate = ConfigurationManager.AppSettings["file_date"];

                if (!Directory.Exists(extractepath))
                {
                    Directory.CreateDirectory(extractepath);
                    Log.WriteToLog("Info", "===batch folder created:{0} at {1}===", extractepath, DateTime.Now.ToString());
                }

                if (!Directory.Exists(backpath))
                {
                    Directory.CreateDirectory(backpath);
                    Log.WriteToLog("Info", "===backup folder created:{0} at {1}===", backpath, DateTime.Now.ToString());
                }

                if (!Directory.Exists(backpath + "\\PRB"))
                {
                    Directory.CreateDirectory(backpath + "\\PRB");
                    Log.WriteToLog("Info", "===batch folder created:{0} at {1}===", backpath + "\\PRB", DateTime.Now.ToString());
                }

                if (!Directory.Exists(backpath + "\\OK"))
                {
                    Directory.CreateDirectory(backpath + "\\OK");
                    Log.WriteToLog("Info", "===backup folder created:{0} at {1}===", backpath + "\\OK", DateTime.Now.ToString());
                }

                AML_Data AMLData = new AML_Data();
                string strEngName = "";
                string strRef = "";
                string sfilepath = "";
                string sfilename = "";
                int iscore = 0;
                int iprb = 0;
                int iok = 0;
                string WPEPswifttype1 = "103,202C";
                string WPEPswifttype2 = "110,111,707,1000";

                Log.WriteToLog("Info", "===batch statr:{0}===", DateTime.Now.ToString());
                
                foreach (FileInfo fzip in new DirectoryInfo(zipfilepath).GetFiles())
                {
                    try
                    {
                        if (fzip.Name.Contains(filedate))
                        {
                            if (File.Exists(extractepath + "\\" +fzip.Name))
                            {
                                File.Delete(extractepath + "\\" + fzip.Name);
                            }
                            ZipFile.ExtractToDirectory(fzip.FullName, extractepath);
                        }
                        
                    }
                    catch(Exception ex)
                    {
                        Log.WriteToLog("Error", "unzip fail name={0}, errormsg={1}", fzip, ex.ToString());
                    }
                }

                int filecount = 0;

                foreach (DirectoryInfo d1 in new DirectoryInfo(extractepath).GetDirectories())
                {
                    try
                    {
                        foreach (DirectoryInfo d2 in new DirectoryInfo(d1.FullName).GetDirectories())
                        {
                            foreach (DirectoryInfo d3 in new DirectoryInfo(d2.FullName).GetDirectories())
                            {
                                foreach (DirectoryInfo d4 in new DirectoryInfo(d3.FullName).GetDirectories())
                                {
                                    if (d4.ToString().ToUpper() == "SWIFT")
                                    {
                                        foreach (FileInfo f5 in new DirectoryInfo(d4.FullName).GetFiles())
                                        {
                                            sfilename = f5.Name;
                                            sfilepath = f5.FullName.ToString();

                                            if ((sfilename.Substring(4, 2).ToUpper() == "CI" && sfilename.Substring(6, 1).ToUpper() != "F") || ((sfilename.Substring(4, 1).ToUpper() == "S" || sfilename.Substring(4, 1).ToUpper() == "F") && (WPEPswifttype1.IndexOf(sfilename.Substring(14, 4).ToString().Trim()) > -1 || WPEPswifttype2.IndexOf(sfilename.Substring(14, 4).ToString().Trim()) > -1)))
                                            {
                                                Log.WriteToLog("Info", "===process file:{0}===", sfilename);
                                                //bool bol_r = false;
                                                bool bol_r = AMLData.checkPEPS(sfilepath, sfilename, "", "0094", "", 5, 60, "4,45,46,47,52,53", "1,14,15,16,17,18,19,20,51", ref strRef, ref strEngName, ref iscore, "80", "6");
                                                if (bol_r)
                                                {
                                                    if (File.Exists(backpath + "\\PRB\\" + f5.Name))
                                                    {
                                                        File.Move(backpath + "\\PRB\\" + f5.Name, backpath + "\\PRB\\" + f5.Name + "." + DateTime.Now.ToString("yyyyMMddHHmmss"));
                                                    }
                                                    f5.MoveTo(backpath + "\\PRB\\" + f5.Name);
                                                    iprb += 1;
                                                }
                                                else
                                                {
                                                    if (File.Exists(backpath + "\\OK\\" + f5.Name))
                                                    {
                                                        File.Move(backpath + "\\OK\\" + f5.Name, backpath + "\\OK\\" + f5.Name + "." + DateTime.Now.ToString("yyyyMMddHHmmss"));
                                                    }
                                                    f5.MoveTo(backpath + "\\OK\\" + f5.Name);
                                                    iok += 1;
                                                }
                                                filecount = filecount + 1;
                                                Log.WriteToLog("Info", "===current process file count:{0}===", filecount);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.WriteToLog("Error", "file name={0}, errormsg={1}", sfilename, ex.ToString());
                    }

                }
                Log.WriteToLog("Info", "===total process file count:{0}===", filecount);
                Log.WriteToLog("Info", "===total hit count:{0}===", iprb);
                Log.WriteToLog("Info", "===total ok count:{0}===", iok);
                Log.WriteToLog("Info", "===batch finished:{0}===", DateTime.Now.ToString());
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Batch Error:{0}", ex.ToString());
            }
        }
    }
}
